﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Accounting.Core.Domain;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Accounting.Core.DAO;
using Accounting.Frm.FReport;

namespace Accounting
{
    public partial class FRContractStatusPurchase : CustormForm
    {
        #region các hàm service, list sử dụng
        //commnet by cuongpv
        //private static IAccountService AccountService
        //{
        //    get { return IoC.Resolve<IAccountService>(); }
        //}
        //private static IAccountingObjectService AccountingObjectService
        //{
        //    get { return IoC.Resolve<IAccountingObjectService>(); }
        //}
        //private static IAccountingObjectGroupService AccountingObjectGroupService
        //{
        //    get { return IoC.Resolve<IAccountingObjectGroupService>(); }
        //}
        //private static IMaterialGoodsService MaterialGoodsService
        //{
        //    get { return IoC.Resolve<IMaterialGoodsService>(); }
        //}
        //private static IMaterialGoodsCategoryService MaterialGoodsCategoryService
        //{
        //    get { return IoC.Resolve<IMaterialGoodsCategoryService>(); }
        //}
        //private static IDepartmentService DepartmentService
        //{
        //    get { return IoC.Resolve<IDepartmentService>(); }
        //}

        //private static ICurrencyService CurrencyService
        //{
        //    get { return IoC.Resolve<ICurrencyService>(); }
        //}

        //readonly List<MaterialGoodsReport> _lstMaterialGoodsReport = ReportUtils.LstMaterialGoodsReport;
        //readonly List<AccountingObjectReport> _lstCustomerReport = ReportUtils.LstProviderNCC;
        //readonly List<Department> _lstDepartment = DepartmentService.GetAll_ByIsActive_OrderByDepartmentCode(true);
        //readonly List<AccountingObject> _lstEmployee = AccountingObjectService.GetAccountingObjects(2, true);
        //readonly List<MaterialGoodsCategory> _lstMaterialGoodsCategory = MaterialGoodsCategoryService.GetAll_OrderBy();
        //readonly List<AccountingObjectGroup> _lstAccountingObjectGroup = AccountingObjectGroupService.GetListByIsActiveOrderCode(true);
        //readonly List<Currency> _lstCurrency = CurrencyService.GetIsActive(true);
        //end comment by cuongpv
        string _subSystemCode;
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        List<EMContractReport> lstContractReportBuy = ReportUtils.lstEMContractReportBuy;//add by cuongpv
        #endregion
        public FRContractStatusPurchase(string subSystemCode)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            _subSystemCode = subSystemCode;

            #region comment by cuongpv
            //foreach(var item in _lstCustomerReport)
            //{
            //    item.Check = false;
            //}
            //ugridAccountingObject.SetDataBinding(_lstCustomerReport.ToList(), "");
            //ugridAccountingObject.DisplayLayout.Bands[0].Columns["ID"].Hidden = true;
            //ugridAccountingObject.DisplayLayout.Bands[0].Columns["ObjectType"].Hidden = true;
            //ugridAccountingObject.DisplayLayout.Bands[0].Columns["AccountingObjectCategory"].Hidden = true;
            //ugridAccountingObject.DisplayLayout.Bands[0].Columns["AccountObjectGroupID"].Hidden = true;
            //ugridAccountingObject.DisplayLayout.Bands[0].Columns["AccountingObjectCode"].Header.Caption = "Mã đối tượng";
            //ugridAccountingObject.DisplayLayout.Bands[0].Columns["AccountingObjectName"].Header.Caption = "Tên đối tượng";
            //ugridAccountingObject.DisplayLayout.Bands[0].Columns["Address"].Header.Caption = "Địa chỉ";
            //ugridAccountingObject.DisplayLayout.Bands[0].Columns["Check"].Header.CheckBoxVisibility = HeaderCheckBoxVisibility.Always;
            //ugridAccountingObject.DisplayLayout.Bands[0].Columns["Check"].Header.Caption = "";

            //foreach(var item in _lstMaterialGoodsReport)
            //{
            //    item.Check = false;
            //}
            //ugridEMContractBuy.SetDataBinding(_lstMaterialGoodsReport.ToList(), "");
            //ugridEMContractBuy.DisplayLayout.Bands[0].Columns["ID"].Hidden = true;
            //ugridEMContractBuy.DisplayLayout.Bands[0].Columns["MaterialGoodsGSTID"].Hidden = true;
            //ugridEMContractBuy.DisplayLayout.Bands[0].Columns["MaterialGoodsCategoryID"].Hidden = true;
            //ugridEMContractBuy.DisplayLayout.Bands[0].Columns["RepositoryID"].Hidden = true;
            //ugridEMContractBuy.DisplayLayout.Bands[0].Columns["MaterialGoodsCode"].Header.Caption = "Mã VTHH";
            //ugridEMContractBuy.DisplayLayout.Bands[0].Columns["MaterialGoodsName"].Header.Caption = "Tên VTHH";
            //ugridEMContractBuy.DisplayLayout.Bands[0].Columns["Check"].Header.CheckBoxVisibility = HeaderCheckBoxVisibility.Always;
            //ugridEMContractBuy.DisplayLayout.Bands[0].Columns["Check"].Header.Caption = "";
            //ReportUtils.ProcessControls(this);
            //ugridAccountingObject.DisplayLayout.Bands[0].Columns["AccountingObjectCode"].CellActivation = Activation.NoEdit;
            //ugridAccountingObject.DisplayLayout.Bands[0].Columns["AccountingObjectName"].CellActivation = Activation.NoEdit;
            //ugridAccountingObject.DisplayLayout.Bands[0].Columns["Address"].CellActivation = Activation.NoEdit;
            //ugridEMContractBuy.DisplayLayout.Bands[0].Columns["MaterialGoodsCode"].CellActivation = Activation.NoEdit;
            //ugridEMContractBuy.DisplayLayout.Bands[0].Columns["MaterialGoodsName"].CellActivation = Activation.NoEdit;
            #endregion
            //add by cuonpgv
            ReportUtils.ProcessControls(this);
            Utils.ConfigGrid(ugridEMContractBuy, ConstDatabase.FEMContractBuy_TableName);

            foreach (var item in lstContractReportBuy)
            {
                item.Check = false;
            }
            ugridEMContractBuy.SetDataBinding(lstContractReportBuy.ToList(), "");
            UltraGridBand band = ugridEMContractBuy.DisplayLayout.Bands[0];
            band.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.Default;
            band.Columns["ID"].Hidden = true;
            band.Columns["ContractCode"].Header.Caption = "Mã hợp đồng";
            band.Columns["ContractName"].Header.Caption = "Tên hợp đồng";
            UltraGridColumn ugc = band.Columns["Check"];
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            band.Columns["Check"].CellActivation = Activation.AllowEdit;
            band.Columns["Check"].Header.Fixed = true;
            band.Columns["Check"].Header.Caption = "";
            ugridEMContractBuy.DisplayLayout.Override.AllowAddNew = AllowAddNew.No;
            foreach (var c in band.Columns)
            {
                if (c.Key != "Check") c.CellActivation = Activation.NoEdit;
            }
            //end add by cuongpv
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dtBeginDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }

            if((DateTime)dtBeginDate.Value > (DateTime)dtEndDate.Value)
            {
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            //commnet by cuongpv
            //List<Guid?> lstA = new List<Guid?>();
            //List<Guid?> lstM = new List<Guid?>();
            //for (int i = 0; i < ugridAccountingObject.Rows.Count; i++)
            //{
            //    if (bool.Parse(ugridAccountingObject.Rows[i].Cells["Check"].Value.ToString()) == true)
            //    {
            //        lstA.Add((Guid?)ugridAccountingObject.Rows[i].Cells["ID"].Value);
            //    }
            //}

            //if(lstA.Count == 0)
            //{
            //    MSG.Warning("Bạn chưa chọn đối tượng");
            //    return;
            //}

            //for (int i = 0; i < ugridEMContractBuy.Rows.Count; i++)
            //{
            //    if (bool.Parse(ugridEMContractBuy.Rows[i].Cells["Check"].Value.ToString()) == true)
            //    {
            //        lstM.Add((Guid?)ugridEMContractBuy.Rows[i].Cells["ID"].Value);
            //    }
            //}

            //if(lstM.Count == 0)
            //{
            //    MSG.Warning("Bạn chưa chọn vật tư hàng hóa");
            //    return;
            //}
            //end comment by cuongpv

            //add by cuongpv
            List<Guid?> lstIDContractBuy = new List<Guid?>();
            for (int i = 0; i < ugridEMContractBuy.Rows.Count; i++)
            {
                if (bool.Parse(ugridEMContractBuy.Rows[i].Cells["Check"].Value.ToString()) == true)
                {
                    lstIDContractBuy.Add((Guid?)ugridEMContractBuy.Rows[i].Cells["ID"].Value);
                }
            }

            if (lstIDContractBuy.Count == 0)
            {
                MSG.Warning("Bạn chưa chọn hợp đồng");
                return;
            }
            //end add by cuongpv

            ReportProcedureSDS sp = new ReportProcedureSDS();
            List<RContract> data = new List<RContract>();
            data = sp.GetContract((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, false);
            FRTINHHINHTHUCHIENHDMex fm = new FRTINHHINHTHUCHIENHDMex((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, lstIDContractBuy);
            fm.Show(this);
            
        }      
    }
}