﻿namespace Accounting
{
    partial class FRS11DNN
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FRS11DNN));
            this.reportManager1 = new PerpetuumSoft.Reporting.Components.ReportManager(this.components);
            this.fileReportSlot1 = new PerpetuumSoft.Reporting.Components.FileReportSlot(this.components);
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.chk = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.lblBeginDate = new Infragistics.Win.Misc.UltraLabel();
            this.lblEndDate = new Infragistics.Win.Misc.UltraLabel();
            this.cbbDateTime = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.dtBeginDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.dtEndDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraPanel3 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.btnOk = new Infragistics.Win.Misc.UltraButton();
            this.btnExit = new Infragistics.Win.Misc.UltraButton();
            this.ugridDepartment = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtBeginDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDate)).BeginInit();
            this.ultraPanel3.ClientArea.SuspendLayout();
            this.ultraPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ugridDepartment)).BeginInit();
            this.SuspendLayout();
            // 
            // reportManager1
            // 
            this.reportManager1.DataSources = new PerpetuumSoft.Reporting.Components.ObjectPointerCollection(new string[0], new object[0]);
            this.reportManager1.OwnerForm = this;
            this.reportManager1.Reports.AddRange(new PerpetuumSoft.Reporting.Components.ReportSlot[] {
            this.fileReportSlot1});
            // 
            // fileReportSlot1
            // 
            this.fileReportSlot1.FilePath = "";
            this.fileReportSlot1.ReportName = "";
            this.fileReportSlot1.ReportScriptType = typeof(PerpetuumSoft.Reporting.Rendering.ReportScriptBase);
            // 
            // ultraPanel1
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            this.ultraPanel1.Appearance = appearance3;
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraGroupBox1);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(779, 116);
            this.ultraPanel1.TabIndex = 48;
            // 
            // ultraGroupBox1
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.BackColor2 = System.Drawing.Color.Transparent;
            this.ultraGroupBox1.Appearance = appearance4;
            this.ultraGroupBox1.Controls.Add(this.chk);
            this.ultraGroupBox1.Controls.Add(this.lblBeginDate);
            this.ultraGroupBox1.Controls.Add(this.lblEndDate);
            this.ultraGroupBox1.Controls.Add(this.cbbDateTime);
            this.ultraGroupBox1.Controls.Add(this.dtBeginDate);
            this.ultraGroupBox1.Controls.Add(this.dtEndDate);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(779, 116);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.Text = "Chọn kỳ báo cáo";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // chk
            // 
            this.chk.Location = new System.Drawing.Point(356, 80);
            this.chk.Margin = new System.Windows.Forms.Padding(4);
            this.chk.Name = "chk";
            this.chk.Size = new System.Drawing.Size(415, 25);
            this.chk.TabIndex = 51;
            this.chk.Text = "In theo số lượng báo cáo trên TSCĐ";
            this.chk.Visible = false;
            // 
            // lblBeginDate
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.TextHAlignAsString = "Left";
            appearance5.TextVAlignAsString = "Bottom";
            this.lblBeginDate.Appearance = appearance5;
            this.lblBeginDate.Location = new System.Drawing.Point(35, 52);
            this.lblBeginDate.Margin = new System.Windows.Forms.Padding(4);
            this.lblBeginDate.Name = "lblBeginDate";
            this.lblBeginDate.Size = new System.Drawing.Size(36, 21);
            this.lblBeginDate.TabIndex = 43;
            this.lblBeginDate.Text = "Từ";
            // 
            // lblEndDate
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextHAlignAsString = "Left";
            appearance6.TextVAlignAsString = "Bottom";
            this.lblEndDate.Appearance = appearance6;
            this.lblEndDate.Location = new System.Drawing.Point(184, 52);
            this.lblEndDate.Margin = new System.Windows.Forms.Padding(4);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.Size = new System.Drawing.Size(33, 21);
            this.lblEndDate.TabIndex = 44;
            this.lblEndDate.Text = "Đến";
            // 
            // cbbDateTime
            // 
            this.cbbDateTime.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbDateTime.Location = new System.Drawing.Point(32, 23);
            this.cbbDateTime.Margin = new System.Windows.Forms.Padding(4);
            this.cbbDateTime.Name = "cbbDateTime";
            this.cbbDateTime.NullText = "<chọn dữ liệu>";
            this.cbbDateTime.Size = new System.Drawing.Size(731, 25);
            this.cbbDateTime.TabIndex = 45;
            // 
            // dtBeginDate
            // 
            appearance7.TextHAlignAsString = "Center";
            appearance7.TextVAlignAsString = "Middle";
            this.dtBeginDate.Appearance = appearance7;
            this.dtBeginDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtBeginDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtBeginDate.Location = new System.Drawing.Point(32, 79);
            this.dtBeginDate.Margin = new System.Windows.Forms.Padding(4);
            this.dtBeginDate.MaskInput = "";
            this.dtBeginDate.Name = "dtBeginDate";
            this.dtBeginDate.Size = new System.Drawing.Size(132, 24);
            this.dtBeginDate.TabIndex = 46;
            this.dtBeginDate.Value = null;
            // 
            // dtEndDate
            // 
            appearance8.TextHAlignAsString = "Center";
            appearance8.TextVAlignAsString = "Middle";
            this.dtEndDate.Appearance = appearance8;
            this.dtEndDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtEndDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtEndDate.Location = new System.Drawing.Point(181, 79);
            this.dtEndDate.Margin = new System.Windows.Forms.Padding(4);
            this.dtEndDate.MaskInput = "";
            this.dtEndDate.Name = "dtEndDate";
            this.dtEndDate.Size = new System.Drawing.Size(132, 24);
            this.dtEndDate.TabIndex = 47;
            this.dtEndDate.Value = null;
            // 
            // ultraPanel3
            // 
            // 
            // ultraPanel3.ClientArea
            // 
            this.ultraPanel3.ClientArea.Controls.Add(this.ultraGroupBox3);
            this.ultraPanel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraPanel3.Location = new System.Drawing.Point(0, 463);
            this.ultraPanel3.Margin = new System.Windows.Forms.Padding(4);
            this.ultraPanel3.Name = "ultraPanel3";
            this.ultraPanel3.Size = new System.Drawing.Size(779, 44);
            this.ultraPanel3.TabIndex = 53;
            this.ultraPanel3.TabStop = false;
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Controls.Add(this.btnOk);
            this.ultraGroupBox3.Controls.Add(this.btnExit);
            this.ultraGroupBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraGroupBox3.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(779, 44);
            this.ultraGroupBox3.TabIndex = 54;
            this.ultraGroupBox3.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.Image = global::Accounting.Properties.Resources.apply_16;
            this.btnOk.Appearance = appearance1;
            this.btnOk.Location = new System.Drawing.Point(553, 7);
            this.btnOk.Margin = new System.Windows.Forms.Padding(4);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(100, 28);
            this.btnOk.TabIndex = 56;
            this.btnOk.Text = "Đồng ý";
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.Image = global::Accounting.Properties.Resources.cancel_16;
            this.btnExit.Appearance = appearance2;
            this.btnExit.Location = new System.Drawing.Point(672, 7);
            this.btnExit.Margin = new System.Windows.Forms.Padding(4);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(100, 28);
            this.btnExit.TabIndex = 57;
            this.btnExit.Text = "Hủy bỏ";
            // 
            // ugridDepartment
            // 
            this.ugridDepartment.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.ugridDepartment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ugridDepartment.Location = new System.Drawing.Point(0, 116);
            this.ugridDepartment.Margin = new System.Windows.Forms.Padding(4);
            this.ugridDepartment.Name = "ugridDepartment";
            this.ugridDepartment.Size = new System.Drawing.Size(779, 347);
            this.ugridDepartment.TabIndex = 52;
            this.ugridDepartment.TabStop = false;
            this.ugridDepartment.Text = "Danh sách phòng ban";
            this.ugridDepartment.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            // 
            // FRS11DNN
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(779, 507);
            this.Controls.Add(this.ugridDepartment);
            this.Controls.Add(this.ultraPanel3);
            this.Controls.Add(this.ultraPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FRS11DNN";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sổ theo dõi TSCĐ tại nơi sử dụng";
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtBeginDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDate)).EndInit();
            this.ultraPanel3.ClientArea.ResumeLayout(false);
            this.ultraPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ugridDepartment)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private PerpetuumSoft.Reporting.Components.ReportManager reportManager1;
        private PerpetuumSoft.Reporting.Components.FileReportSlot fileReportSlot1;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraLabel lblBeginDate;
        private Infragistics.Win.Misc.UltraLabel lblEndDate;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbDateTime;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtBeginDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtEndDate;
        private Infragistics.Win.Misc.UltraPanel ultraPanel3;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.Misc.UltraButton btnOk;
        private Infragistics.Win.Misc.UltraButton btnExit;
        private Infragistics.Win.UltraWinGrid.UltraGrid ugridDepartment;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chk;
    }
}