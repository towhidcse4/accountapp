﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;

namespace Accounting
{
    public partial class FRB02DNN : CustormForm
    {
        string _subSystemCode;
        public FRB02DNN(string subSystemCode)
        {
            InitializeComponent();
            _subSystemCode = subSystemCode;
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\B02-DNN.rst", path);
        }

        private void ultraButton1_Click(object sender, EventArgs e)
        {

            Accounting.Core.IService.ISynthesisReportService synthesisReportService = FX.Core.IoC.Resolve<Accounting.Core.IService.ISynthesisReportService>();
            List<Assets> dataAsset = synthesisReportService.ReportBalanceSheet(DateTime.Parse("1/1/1753"), DateTime.Now);


            List<B02DNN> data = new List<B02DNN>();
            data = dataAsset.Select(c => new B02DNN
            {
                ItemCode = c.ItemCode,
                ItemName = c.ItemName,
                Description = c.Description,
                AmountCurrentYear = c.AmountEnding,
                AmountLastYear = c.AmountBeginning,
                IsBold = c.IsBold,
                IsItalic = c.IsItalic
            }).ToList();
            var rD = new B02DNNDetail();
            rD.Period = ReportUtils.GetPeriod(DateTime.Now.AddDays(-1000), DateTime.Now);

            var f = new ReportForm<B02DNN>(fileReportSlot1, _subSystemCode);
            f.AddDatasource("B02DNN", data, true);
            f.AddDatasource("Detail", rD);
            f.LoadReport();
            f.WindowState = FormWindowState.Maximized;
            f.Show();
        }
    }
}
