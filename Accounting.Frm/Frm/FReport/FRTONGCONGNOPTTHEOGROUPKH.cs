﻿using Accounting.Core.DAO;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.Report;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRTONGCONGNOPTTHEOGROUPKH : Form
    {
        List<AccountingObjectReport> _lstAccountingObjectReport = new List<AccountingObjectReport>();
        readonly List<AccountReport> _lstAccountPayReport = ReportUtils.LstAccountPayReport;
        private readonly List<AccountingObjectReport> _lstnew = ReportUtils.LstCustomers;
        private readonly IAccountService _IAccountService;
        List<AccountingObjectGroup> accountingObjectGroups = new List<AccountingObjectGroup>();
        private readonly IAccountingObjectGroupService accountingObjectGroupService;
        private string _subSystemCode;
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        private string list_MA_NHOMKH = "";
        public FRTONGCONGNOPTTHEOGROUPKH()
        {
            InitializeComponent();
            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\TongCongNoPhaiThuGroup.rst", path);
            _IAccountService = IoC.Resolve<IAccountService>();
            accountingObjectGroupService = IoC.Resolve<IAccountingObjectGroupService>();
            foreach (var item in _lstnew) { item.Check = false; }
            _lstAccountingObjectReport = _lstnew;
            ugridAccountingObject.SetDataBinding(_lstAccountingObjectReport.ToList(), "");
            cbbFromAccount.DataSource = _lstAccountPayReport;
            cbbFromAccount.DisplayMember = "AccountNumber";
            Utils.ConfigGrid(cbbFromAccount, ConstDatabase.Account_TableName);
            ReportUtils.ProcessControls(this);
            cbbAccountingObjectGroupID.ValueChanged += cbbAccountingObjectGroupID_ValueChanged;

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dtBeginDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }
        }
        private bool _check1 = false;
        private void cbbAccountingObjectGroupID_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                _check1 = ((AccountingObjectGroup)cbbAccountingObjectGroupID.SelectedRow.ListObject).AccountingObjectGroupCode != "TC";
                Filter();
            }
            catch { }
            if(cbbAccountingObjectGroupID.Text=="")
            {
                _lstAccountingObjectReport = _lstnew;
                ugridAccountingObject.DataSource = _lstAccountingObjectReport;
            }
        }
        private void Filter()
        {
            _lstAccountingObjectReport = _lstnew;
            AccountingObjectGroup accountingObjectGroup;
            if (_check1)
            {
                accountingObjectGroup = (AccountingObjectGroup)cbbAccountingObjectGroupID.SelectedRow.ListObject;
                _lstAccountingObjectReport = _lstAccountingObjectReport.Where(c => c.AccountObjectGroupID == accountingObjectGroup.ID).ToList();
            }
            ugridAccountingObject.SetDataBinding(_lstAccountingObjectReport.ToList(), "");
        }
        private void btnOk_Click(object sender, EventArgs e)
        {

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            foreach (var item in ugridAccountingObject.Rows)
            {
                item.Cells["Check"].Value = false;
            }
            Close();
        }

        private void reportManager1_GetReportParameter(object sender, PerpetuumSoft.Reporting.Components.GetReportParameterEventArgs e)
        {
            string typeCurrency = cbbCurrency.Value != null ? cbbCurrency.Value.ToString() : string.Empty;
            e.Parameters["CurrencyID"].Value = typeCurrency;
        }

        private void ultraButton1_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }

            if ((DateTime)dtBeginDate.Value > (DateTime)dtEndDate.Value)
            {
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            ReportProcedureSDS sp = new ReportProcedureSDS();
            string acount = cbbFromAccount.Value != null ? cbbFromAccount.Value.ToString() : string.Empty;

            if (acount == null || acount == string.Empty)
            {
                MSG.Warning("Tài khoản không được để trống");
                return;
            }

            string list_acount = "";

            try
            {
                var dsacc = _lstAccountingObjectReport != null ? _lstAccountingObjectReport.Where(c => c.Check).Select(c => c.ID.ToString()) : new List<string>();
                list_acount = string.Join(",", dsacc.ToArray());
            }
            catch (Exception ex)
            {
                list_acount = "";
            }


            try
            {
                if (_check1)
                {
                    list_MA_NHOMKH = cbbAccountingObjectGroupID.Value.ToString();
                }
                else
                {
                    var list_nhom = accountingObjectGroupService.GetListByIsActiveOrderCode(true);
                    if (list_nhom != null && list_nhom.Count > 0)
                    {
                        list_MA_NHOMKH = string.Join(",", list_nhom.Select(t => t.ID).ToArray());
                    }
                }
            }
            catch (Exception ex)
            {
                list_MA_NHOMKH = "";
            }

            string currencyID = cbbCurrency.Value != null ? cbbCurrency.Value.ToString() : string.Empty;
            if (string.IsNullOrEmpty(currencyID))
            {
                MSG.Warning("Bạn chưa chọn loại tiền, xin kiểm tra lại");
                return;
            }

            List<SA_GetReceivableSummaryByGroup> data = sp.GetTongCongNoPhaiThuTheoNhomKH((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, currencyID, acount, list_acount, list_MA_NHOMKH);
            if (data.Count == 0)
                MSG.Warning("Không có dữ liệu");
            else
            {
                var rD = new SA_GetReceivableSummaryByGroup_period();
                rD.Period = ReportUtils.GetPeriod((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value);
                var f = new FRTONGCONGNOPTTHEOGROUPKHTruocIn(data, rD.Period, currencyID);
                //f.AddDatasource("DBTHNOPHAITHUGROUP", data, true);
                //f.AddDatasource("Detail", rD);
                //f.LoadReport();
                //f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
        }
    }
}
