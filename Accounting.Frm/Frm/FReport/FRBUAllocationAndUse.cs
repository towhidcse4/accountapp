﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.IService;
using Accounting.Core.Domain;
using FX.Data;
using FX.Core;

namespace Accounting
{
    public partial class FRBUAllocationAndUse : CustormForm
    {
        string _subSystemCode;
        private IBankService _iIBankService; 
        public FRBUAllocationAndUse(string subSystemCode)
        {
            _iIBankService = IoC.Resolve<IBankService>();
            WaitingFrm.StartWaiting();
            InitializeComponent();
            _subSystemCode = subSystemCode;
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\BUAllocationAndUse.rst", path);
            fileReportSlot2.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\TESTQ.rst", path);
            WaitingFrm.StopWaiting();
        }

        private void ultraButton1_Click(object sender, EventArgs e)
        {
            List<BUAllocationAndUse> data = new List<BUAllocationAndUse>();
            for (int i = 0; i < 200; i++)
            {
                data.Add(new BUAllocationAndUse(i));
            }
            var rD = new BUAllocationAndUseDetail();
            rD.Period = "Kỳ báo cáo";
            var f = new ReportForm<BUAllocationAndUse>(fileReportSlot1, _subSystemCode);
            f.AddDatasource("BUAllocationAndUse", data, true);
            f.AddDatasource("Detail", rD);
            f.LoadReport();            
            f.WindowState = FormWindowState.Maximized;
            f.Show();
        }

        private void ultraButton2_Click(object sender, EventArgs e)
        {
            List<Bank> data = new List<Bank>();
            data = _iIBankService.Query.ToList();
            var rD = new BUAllocationAndUseDetail();
            rD.Period = "Kỳ báo cáo";
            var f = new ReportForm<Bank>(fileReportSlot2, _subSystemCode);
            f.AddDatasource("Bank1", data, true);
            f.AddDatasource("Detail", rD);
            f.LoadReport();
            f.WindowState = FormWindowState.Maximized;
            f.Show();
        }
    }
}
