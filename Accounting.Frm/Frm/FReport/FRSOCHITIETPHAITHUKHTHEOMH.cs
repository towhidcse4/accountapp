﻿using Accounting.Core.DAO;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.Report;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Accounting.Frm.FReport
{
    public partial class FRSOCHITIETPHAITHUKHTHEOMH : Form
    {
        List<MaterialGoodsReport> _lstMaterialGoodsReport = new List<MaterialGoodsReport>();
        private readonly List<MaterialGoodsReport> _lstNew = ReportUtils.LstMaterialGoodsReport;
        private IMaterialGoodsService _IMaterialGoodsService;
        private readonly IAccountingObjectGroupService accountingObjectGroupService;
        List<AccountingObjectReport> _lstAccountingObjectReport = new List<AccountingObjectReport>();
        readonly List<AccountReport> _lstAccountPayReport = ReportUtils.LstAccountPayReport;
        private readonly List<AccountingObjectReport> _lstnew = ReportUtils.LstCustomers.Where(t => t.ObjectType == 0 || t.ObjectType == 2).ToList();
        private readonly IAccountService _IAccountService;
        private string list_MA_NHOMKH = "";
        private string _subSystemCode;
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        public FRSOCHITIETPHAITHUKHTHEOMH()
        {
            InitializeComponent();
            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\SoChiTietPhaiThuKhachHangTheoMatHang.rst", path);

            _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
            foreach (var item in _lstNew)
            {
                item.Check = false;
            }
            _lstMaterialGoodsReport = _lstNew;

            ugridMaterialGoodsCategory.SetDataBinding(_lstMaterialGoodsReport.ToList(), "");
         
            //
            _IAccountService = IoC.Resolve<IAccountService>();
            foreach (var item in _lstnew) { item.Check = false; }
            _lstAccountingObjectReport = _lstnew;
            ugridAccountingObject.SetDataBinding(_lstAccountingObjectReport.ToList(), "");
            cbbFromAccount.DataSource = _lstAccountPayReport;
            cbbFromAccount.DisplayMember = "AccountNumber";
            Utils.ConfigGrid(cbbFromAccount, ConstDatabase.Account_TableName);
            ReportUtils.ProcessControls(this);
            foreach (var item in cbbCurrency.Rows)
            {
                if (((Currency)item.ListObject).ID == "VND")
                {
                    cbbCurrency.SelectedRow = item;
                    break;
                }
                cbbCurrency.SelectedRow = cbbCurrency.Rows[0];
            }
            ConfigCombocbbNhomKH(cbbAccountingObjectGroupID);
            cbbAccountingObjectGroupID.ValueChanged += cbbAccountingObjectGroupID_ValueChanged;
            ugridAccountingObject.DisplayLayout.Bands[0].Columns["Address"].Hidden = true;

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dtBeginDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }

        }
        private static void ConfigCombocbbNhomKH(UltraCombo cbb)
        {
            var accountingObjectGroupService = IoC.Resolve<IAccountingObjectGroupService>();
            var accountingObjectGroups = new List<AccountingObjectGroup>();
            AccountingObjectGroup item = new AccountingObjectGroup
            {
                AccountingObjectGroupCode = "Khác",
                AccountingObjectGroupName = "Khác"
            };
            AccountingObjectGroup itemTC = new AccountingObjectGroup
            {
                AccountingObjectGroupCode = "Tất cả",//edit by cuongpv 20190419 TC -> Tất cả
                AccountingObjectGroupName = "Tất cả"
            };
            accountingObjectGroups.Add(itemTC);
            accountingObjectGroups.Add(item);
            accountingObjectGroups.AddRange(accountingObjectGroupService.GetListByIsActiveOrderCode(true));
            cbb.DataSource = accountingObjectGroups;
            cbb.DisplayMember = "AccountingObjectGroupName";
            Utils.ConfigGrid(cbb, ConstDatabase.AccountingObjectGroup_TableName);
        }
        private bool _check1 = false;
        private void cbbAccountingObjectGroupID_ValueChanged(object sender, EventArgs e)
        {
            _check1 = ((AccountingObjectGroup)cbbAccountingObjectGroupID.SelectedRow.ListObject).AccountingObjectGroupCode != "Tất cả";//edit by cuongpv 20190419 TC -> Tất cả
            Filter();
        }
        private void Filter()
        {
            _lstAccountingObjectReport = _lstnew;
            AccountingObjectGroup accountingObjectGroup;
            if (_check1)
            {
                if (((AccountingObjectGroup)cbbAccountingObjectGroupID.SelectedRow.ListObject).AccountingObjectGroupCode == "Khác")
                {
                    _lstAccountingObjectReport = _lstAccountingObjectReport.Where(c => c.AccountObjectGroupID == Guid.Empty).ToList();
                }
                else
                {
                    accountingObjectGroup = (AccountingObjectGroup)cbbAccountingObjectGroupID.SelectedRow.ListObject;
                    _lstAccountingObjectReport = _lstAccountingObjectReport.Where(c => c.AccountObjectGroupID == accountingObjectGroup.ID).ToList();
                }
            }
            ugridAccountingObject.SetDataBinding(_lstAccountingObjectReport.ToList(), "");
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }

            if ((DateTime)dtBeginDate.Value > (DateTime)dtEndDate.Value)
            {
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            ReportProcedureSDS sp = new ReportProcedureSDS();
            string acount = cbbFromAccount.Value != null ? cbbFromAccount.Value.ToString() : string.Empty;

            if (acount == null || acount == string.Empty)
            {
                MSG.Warning("Tài khoản không được để trống");
                return;
            }

            string list_acount = "";

            try
            {
                var dsacc = _lstAccountingObjectReport != null ? _lstAccountingObjectReport.Where(c => c.Check).Select(c => c.ID.ToString()) : new List<string>();
                list_acount = string.Join(",", dsacc.ToArray());
                list_acount = "," + list_acount + ",";
            }
            catch (Exception ex)
            {
                list_acount = "";
            }

            string list_item = "";

            try
            {
                var dsitemChoose = list_item != null ? _lstMaterialGoodsReport.Where(c => c.Check).Select(c => c.ID.ToString()) : new List<string>();
                list_item = string.Join(",", dsitemChoose.ToArray());
                list_item = "," + list_item + ",";
            }
            catch (Exception ex)
            {
                list_item = "";
            }


            try
            {
                if (_check1)
                {
                    list_MA_NHOMKH = cbbAccountingObjectGroupID.Value.ToString();
                }
                else
                {
                    var list_nhom = accountingObjectGroupService.GetListByIsActiveOrderCode(true);
                    if (list_nhom != null && list_nhom.Count > 0)
                    {
                        list_MA_NHOMKH = string.Join(",", list_nhom.Select(t => t.ID).ToArray());
                    }
                }
            }
            catch (Exception ex)
            {
                list_MA_NHOMKH = "";
            }

            string currencyID = cbbCurrency.Value != null ? cbbCurrency.Value.ToString() : string.Empty;
            if (string.IsNullOrEmpty(currencyID))
            {
                MSG.Warning("Bạn chưa chọn loại tiền, xin kiểm tra lại");
                return;
            }

            List<GetChiTietPhaiThuKhachHangTheoMatHang> data = sp.GetChiTietPhaiThuKhachHangTheoMatHang((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, currencyID, acount, list_acount, list_MA_NHOMKH, list_item);
            if (data.Count == 0)
                MSG.Warning("Không có dữ liệu");
            else
            {
                var rD = new GetChiTietPhaiThuKhachHangTheoMatHang_period();
                string txtThoiGian = "Tài khoản: " + acount + "; Loại tiền: " + cbbCurrency.Value.ToString();
                rD.Period = txtThoiGian + "; " + ReportUtils.GetPeriod((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value);
                var f = new ReportForm<GetChiTietPhaiThuKhachHangTheoMatHang>(fileReportSlot1, _subSystemCode);
                f.AddDatasource("DBTHNOPHAITHUGROUP", data, true);
                f.AddDatasource("Detail", rD);
                f.SetHyperlinkFunction("Accounting.ReportUtils", "Hyperlink");
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
