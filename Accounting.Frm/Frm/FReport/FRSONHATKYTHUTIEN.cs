﻿using Accounting.Core.DAO;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.Report;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRSONHATKYTHUTIEN : CustormForm
    {
        readonly List<AccountReport> _lstAccountPayReport = ReportUtils.LstAccountCashrReport;
        readonly List<AccountReport> _lstAccountBankReport = ReportUtils.LstAccountBankReport;
        private string _subSystemCode;
        private readonly IBankAccountDetailService _IBankAccountDetailService;
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }

        public FRSONHATKYTHUTIEN()
        {
            InitializeComponent();
            List<AccountReport> listAccount = new List<AccountReport>();
            _IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            _subSystemCode = "";
            listAccount.AddRange(_lstAccountPayReport);
            listAccount.AddRange(_lstAccountBankReport);
            cbbFromAccount.DataSource = listAccount;
            cbbFromAccount.DisplayMember = "AccountNumber";
            cbbFromAccount.SelectedRow = cbbFromAccount.Rows[0];
            Utils.ConfigGrid(cbbFromAccount, ConstDatabase.Account_TableName);
            ReportUtils.ProcessControls(this);
            cbbBankAccountDetail.Value = "";
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dtBeginDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }

            if ((DateTime)dtBeginDate.Value > (DateTime)dtEndDate.Value)
            {
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }


            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            int check = ckCongGop.Checked == false ? 0 : 1;
            string tknganhang = cbbBankAccountDetail.Value == null ? "" : cbbBankAccountDetail.Value.ToString();
            var bank_account = _IBankAccountDetailService.Query.FirstOrDefault(t => t.BankAccount == tknganhang);
            string bank_account_id = bank_account == null ? "" : bank_account.ID.ToString();

            FRSONHATKYTHUTIENex fm = new FRSONHATKYTHUTIENex((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, cbbFromAccount.Value.ToString(), cbbCurrency.Value.ToString(), bank_account_id, check);
            fm.Show(this);
        }

        private void cbbFromAccount_ValueChanged(object sender, EventArgs e)
        {
            string item = cbbFromAccount.Value.ToString();
            cbbBankAccountDetail.Value = "";
            cbbBankAccountDetail.Enabled = false;
            if (!item.StartsWith("111"))
            {
                cbbBankAccountDetail.Enabled = true;
            }
        }
    }
}
