﻿using Accounting.Core.DAO;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.Report;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinTree;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRSoChiTietLaiLoTheoCongTrinh : Form
    {
        private readonly List<CostSetTongHopCP> _lstnew = ReportUtils.LstCostSetTongHopCP;
        private List<CostSetTongHopCP> _lstCostSetCP = new List<CostSetTongHopCP>();

        private string _subSystemCode;
        private ICostSetService _ICostSetService
        {
            get { return IoC.Resolve<ICostSetService>(); }
        }
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        public FRSoChiTietLaiLoTheoCongTrinh()
        {
            InitializeComponent();
            _subSystemCode = "";
            _lstCostSetCP = _lstnew;
            ugridCostSetCP.SetDataBinding(_lstCostSetCP.Where(x => x.TypeId == 1).OrderBy(n => n.costSetCode).ToList(), "");
            ReportUtils.ProcessControls(this);
            foreach (var row in ugridCostSetCP.Rows)
            {
                row.Cells["Check"].Value = false;
            }
            ugridCostSetCP.DisplayLayout.Bands[0].Columns["CostSetCode"].Header.Caption = "Mã công trình";
            ugridCostSetCP.DisplayLayout.Bands[0].Columns["CostSetName"].Header.Caption = "Tên công trình";
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dtBeginDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống!");
                return;
            }

            if (dtBeginDate.DateTime.Date > dtEndDate.DateTime.Date)
            {
                //MessageBox.Show("Ngày bắt đầu không thể lớn hơn ngày kết thúc", "Cảnh báo");
                MSG.Warning("Từ ngày không được lớn hơn đến ngày!");
                return;
            }
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            var lstCostSet = _lstnew.Where(x => x.Check).ToList();
            if (lstCostSet.Count == 0)
            {
                MSG.Warning("Bạn chưa chọn Mã công trình!");
                return;
            }
            ReportProcedureSDS sp = new ReportProcedureSDS();
            string list_item = "";

            try
            {
                var dsitemChoose = lstCostSet != null ? lstCostSet.Select(c => c.iD.ToString()) : new List<string>();
                list_item = string.Join(",", dsitemChoose.ToArray());
                list_item = "," + list_item + ",";
            }
            catch (Exception ex)
            {
                list_item = "";
            }

            FRSOCHITIETLAILOTHEOCTex fm = new FRSOCHITIETLAILOTHEOCTex((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, list_item);
            fm.Show(this);
            
        }
    }
}
