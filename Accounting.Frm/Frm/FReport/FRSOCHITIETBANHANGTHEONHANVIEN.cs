﻿using Accounting.Core.DAO;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.Report;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
  
    public partial class FRSOCHITIETBANHANGTHEONHANVIEN : Form
    {
        List<AccountingObjectReport> _lstEmployee = new List<AccountingObjectReport>();
        //readonly List<AccountReport> _lstAccountPayReport = ReportUtils.LstAccountPayReport;
        private readonly List<AccountingObjectReport> _lstnewEmployee = ReportUtils.LstEmployee;

        List<MaterialGoodsReport> _lstMaterialGoodsReport = new List<MaterialGoodsReport>();
        private readonly List<MaterialGoodsReport> _lstNew = ReportUtils.LstMaterialGoodsReport;
        private IMaterialGoodsService _IMaterialGoodsService;
        private readonly IAccountingObjectGroupService accountingObjectGroupService;
        private List<AccountingObjectReport> _lstAccountingObjectReport = new List<AccountingObjectReport>();
        private List<AccountingObjectReport> _lstAccountingObjectReportIntial = new List<AccountingObjectReport>();
        readonly List<AccountReport> _lstAccountPayReport = ReportUtils.LstAccountPayReport;
        private readonly List<AccountingObjectReport> _lstnew = new List<AccountingObjectReport>();
        private readonly IAccountService _IAccountService;
        private string list_MA_NHOMKH = "";
        private string _subSystemCode;
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        public FRSOCHITIETBANHANGTHEONHANVIEN()
        {
            InitializeComponent();
            _subSystemCode = "";
            _lstEmployee = _lstnewEmployee;
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\SoChiTietBanHangTheoNhanVien.rst", path);

            _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
            foreach (var item in _lstNew)
            {
                item.Check = false;
            }
            _lstMaterialGoodsReport = _lstNew;

            ugridMaterialGoodsCategory.SetDataBinding(_lstMaterialGoodsReport.ToList(), "");
            _lstnew = ReportUtils.LstCustomers.Where(t => t.ObjectType == 0 || t.ObjectType == 2).ToList();

            //
            _IAccountService = IoC.Resolve<IAccountService>();
            foreach (var item in _lstnew) { item.Check = false; }
            _lstAccountingObjectReport = _lstnew;
            ugridAccountingObject.SetDataBinding(_lstAccountingObjectReport.ToList(), "");
           
          
            foreach (var item in _lstEmployee) { item.Check = false; }
            _lstEmployee = _lstEmployee.OrderBy(n => n.AccountingObjectCode).OrderBy(n => n.DepartmentName).ToList();

            ugridEmployee.SetDataBinding(_lstEmployee.ToList(), "");
            ReportUtils.ProcessControls(this);
            ugridEmployee.DisplayLayout.Bands[0].Columns["AccountingObjectCode"].Header.Caption = "Mã nhân viên";
            ugridEmployee.DisplayLayout.Bands[0].Columns["AccountingObjectName"].Header.Caption = "Tên nhân viên";
            ugridEmployee.DisplayLayout.Bands[0].Columns["DepartmentName"].Hidden = true;
            ugridAccountingObject.DisplayLayout.Bands[0].Columns["Address"].Hidden = true;
            ugridMaterialGoodsCategory.DisplayLayout.Bands[0].Columns["MaterialGoodsCode"].Header.Caption = "Mã hàng";
            ugridMaterialGoodsCategory.DisplayLayout.Bands[0].Columns["MaterialGoodsName"].Header.Caption = "Tên hàng";


            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dtBeginDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }


        }


        private void cbbEmployee_RowSelected(object sender, RowSelectedEventArgs e)
        {
            //if (e.Row != null)
            //{
            //    var selectMGC = (AccountingObjectReport)e.Row.ListObject;
            //    if (selectMGC.AccountingObjectName == "Tất cả")//edit by cuongpv <<Tất cả>> -> Tất cả
            //    {
            //        _lstAccountingObjectReport = _lstAccountingObjectReportIntial.ToList();
            //        ugridAccountingObject.SetDataBinding(_lstAccountingObjectReportIntial, "");
            //    }
            //    //else if (selectMGC.MaterialGoodsCategoryName == "<<Loại khác>>") //coment by cuongpv
            //    //{
            //    //    _lstMaterialGoods = _lstMaterialGoodsInitial.Where(c => c.MaterialGoodsCategoryID == null).ToList();
            //    //    uGridSelectMG.SetDataBinding(_lstMaterialGoods, "");
            //    //}
            //    else
            //    {
            //        _lstAccountingObjectReport = _lstAccountingObjectReportIntial.Where(c => c.AccountObjectGroupID == selectMGC.ID).ToList();
            //        ugridAccountingObject.SetDataBinding(_lstAccountingObjectReportIntial, "");
            //    }
            //}
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
          
            Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }

            if ((DateTime)dtBeginDate.Value > (DateTime)dtEndDate.Value)
            {
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            ReportProcedureSDS sp = new ReportProcedureSDS();
        

            string list_acount = "";
            var lstAccountingObjectReport = _lstAccountingObjectReport.Where(x => x.Check).ToList();
            if (lstAccountingObjectReport.Count == 0)
            {
                MSG.Warning("Bạn chưa chọn mã đối tượng!");
                return;
            }
            try
            {
                var dsacc = lstAccountingObjectReport != null ? lstAccountingObjectReport.Select(c => c.ID.ToString()) : new List<string>();
                list_acount = string.Join(",", dsacc.ToArray());
                list_acount = "," + list_acount + ",";
            }
            catch (Exception ex)
            {
                list_acount = "";
            }

            var lstEmployee = _lstEmployee.Where(x => x.Check).ToList();
            if (lstEmployee.Count == 0)
            {
                MSG.Warning("Bạn chưa chọn mã nhân viên!");
                return;
            }

            string list_employees = "";
            try
            {
                var dsitemChoose = lstEmployee != null ? lstEmployee.Select(c => c.ID.ToString()) : new List<string>();
                list_employees = string.Join(",", dsitemChoose.ToArray());
                list_employees = "," + list_employees + ",";
            }
            catch (Exception ex)
            {
                list_employees = "";
            }

            string list_item = "";
            var lstMaterialGoodsReport = _lstMaterialGoodsReport.Where(x => x.Check).ToList();
            if (lstMaterialGoodsReport.Count == 0)
            {
                MSG.Warning("Bạn chưa chọn mã hàng!");
                return;
            }
            try
            {
                var dsitemChoose = lstMaterialGoodsReport != null ? lstMaterialGoodsReport.Select(c => c.ID.ToString()) : new List<string>();
                list_item = string.Join(",", dsitemChoose.ToArray());
                list_item = "," + list_item + ",";
            }
            catch (Exception ex)
            {
                list_item = "";
            }
      

            List<GetChiTietBanHangTheoNhanVien> data = sp.GetChiTietBanHangTheoNhanVien((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, list_employees, list_acount, list_item);
            if (data.Count == 0)
                MSG.Warning("Không có dữ liệu");
            else
            {
                var rD = new GetChiTietBanHangTheoNhanVien_period();
                rD.Period = ReportUtils.GetPeriod((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value);
                var f = new ReportForm<GetChiTietBanHangTheoNhanVien>(fileReportSlot1, _subSystemCode);
                f.AddDatasource("DBTHNOPHAITHUGROUP", data, true);
                f.AddDatasource("Detail", rD);
                f.SetHyperlinkFunction("Accounting.ReportUtils", "Hyperlink");
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
        }
    }
}
