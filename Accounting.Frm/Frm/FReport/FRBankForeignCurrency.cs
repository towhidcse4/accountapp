﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Accounting.Core.Domain;

namespace Accounting
{
    public partial class FRBankForeignCurrency : CustormForm
    {

        readonly List<AccountReport> _lstAccountBankReport = new List<AccountReport>();
        private List<BankAccountDetail> _lstBankAccountDetail = ReportUtils.LstBankAccountDetailDb;
        string _subSystemCode;
        public FRBankForeignCurrency(string subSystemCode)
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();
            _subSystemCode = subSystemCode;
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\BankForeignCurrency.rst", path);
            ReportUtils.LstAccountBankReport.Clear();
            _lstAccountBankReport = ReportUtils.LstAccountBankReport;
            ugridAccount.SetDataBinding(_lstAccountBankReport.ToList(), "");
            ReportUtils.ProcessControls(this);
            btnExit.Click += btnExit_Click;
            btnOk.Click += btnOk_Click;
            WaitingFrm.StopWaiting();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }
            List<BankForeignCurrency> data = new List<BankForeignCurrency>();
            var generalLedgerService = IoC.Resolve<IGeneralLedgerService>();
            List<Guid> lstGuids = new List<Guid>();
            if (cbbBankAccountDetail.SelectedRow == cbbBankAccountDetail.Rows[0])
            {
                if (_lstBankAccountDetail.Count > 0)
                {
                    foreach (var bankAccountDetail in _lstBankAccountDetail)
                    {
                        lstGuids.Add(bankAccountDetail.ID);
                    }
                }

            }
            else
            {
                lstGuids.Add(_lstBankAccountDetail.Where(p => p.BankName == cbbBankAccountDetail.Text).Select(c => c.ID).SingleOrDefault());
            }
            List<string> lstAccountNumber = _lstAccountBankReport.Where(c => c.Check).Select(c => c.AccountNumber).ToList();
            if (lstAccountNumber.Count == 0)
            {
                MSG.Warning(resSystem.Report_05);
                return;
            }
            if (lstAccountNumber.Count > 0)
            {
                foreach (var accountNumber in lstAccountNumber)
                {
                    data = generalLedgerService.RForegignCurrencyCash((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, accountNumber, ((Currency)cbbCurrency.SelectedRow.ListObject).ID, lstGuids, chk.Checked);
                }
            }
            
            var rd = new BankForeignCurrencyDetail();
            rd.Period = ReportUtils.GetPeriod(DateTime.Now, DateTime.Now.AddMonths(30));
            var f = new ReportForm<BankForeignCurrency>(fileReportSlot1, _subSystemCode);
            f.AddDatasource("BankForeignCurrency", data, true);
            f.AddDatasource("detail", rd);
            f.LoadReport();
            f.WindowState = FormWindowState.Maximized;
            f.Show();
        }
    }
}
