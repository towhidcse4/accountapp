﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.Core.ServiceImp;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Type = Accounting.Core.Domain.Type;

namespace Accounting.Frm.FReport
{
    public partial class FRBangKeChungTuChuaLapChungTuGhiSo : CustormForm
    {
        private readonly ITypeService _typeSrv;


        private readonly IPPInvoiceService _ppInvoiceSrv;
        private readonly IAccountingObjectService _accountingObjectSrv;
        private readonly IInvoiceTypeService _invoiceTypeSrv;
        private readonly IGoodsServicePurchaseService _goodsServicePurchaseSrv;
        private readonly List<Item> _lstItems = Utils.ObjConstValue.SelectTimes;
        // ngày hoạch toán
        DateTime ngayHoachToan = DateTime.ParseExact(ConstFrm.DbStartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
        public List<GVoucherListDetail> VoucherListDetails { get { return _dsVoucherListDetails; } }
        public DateTime DateFrom { get { return _dteFrom; } }
        public DateTime DateTo { get { return _dteTo; } }
        private List<GVoucherListDetail> _dsVoucherListDetails = new List<GVoucherListDetail>();
        private DateTime _dteFrom;
        List<Core.Domain.Type> listTypes = new List<Type>();
        List<GenCode> lstGenCode = new List<GenCode>();
        private string _subSystemCode;
        private DateTime _dteTo;
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        public FRBangKeChungTuChuaLapChungTuGhiSo()
        {
            _typeSrv = IoC.Resolve<ITypeService>();
            Utils.ClearCacheByType<GeneralLedger>();
            _ppInvoiceSrv = IoC.Resolve<IPPInvoiceService>();
            _accountingObjectSrv = IoC.Resolve<IAccountingObjectService>();
            _invoiceTypeSrv = IoC.Resolve<IInvoiceTypeService>();
            _goodsServicePurchaseSrv = IoC.Resolve<IGoodsServicePurchaseService>();
            InitializeComponent();

            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot2.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\BangKeChungTuChuaLapChungTuGhiSo.rst", path);

            // load dữ liệu cho Combobox Hình thức mua hàng
            // load dữ liệu cho Combobox Chọn thời gian
            cbbDateTime.DataSource = _lstItems;
            cbbDateTime.DisplayMember = "Name";
            if (_lstItems.Count > 0)
            {
                cbbDateTime.SelectedRow = cbbDateTime.Rows[0];
            }
            Utils.ConfigGrid(cbbDateTime, ConstDatabase.ConfigXML_TableName);
            // load ngày bắt đầu và ngày kết thúc (note: mặc định ban đầu load theo ngày hoạch toán)
            dteTo.DateTime = dteFrom.DateTime = ngayHoachToan;
            // load dữ liệu cho Grid Mẫu thuế
            // load dữ liệu cho Grid Danh sách
            // load dữ liệu Combobox Đối tượng. Note: đối tượng là nhà cung cấp
            listTypes = _typeSrv.GetAll();

            List<int> lstRemove = new List<int> { 85, 86, 31, 30, 18, 435, 56, 20 };
            // get list generate 
            lstGenCode = Utils.IGenCodeService.Query.Where(o => o.IsReset == true && !lstRemove.Contains(o.TypeGroupID)).ToList();
            foreach (var item in lstGenCode)
            {
                item.Check = false;
            }
            ViewDanhSach(lstGenCode);

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dteFrom.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dteTo.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dteFrom.DateTime;
                dateTimeCacheHistory.DtEndDate = dteTo.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }

        }

        #region Xử lý sự kiện trên form
        private void cbbDateTime_ValueChanged(object sender, EventArgs e)
        {
            if (cbbDateTime.SelectedRow != null)
            {
                var model = cbbDateTime.SelectedRow.ListObject as Item;
                DateTime dtBegin;
                DateTime dtEnd;
                Utils.GetDateBeginDateEnd(ngayHoachToan.Year,ngayHoachToan, model, out dtBegin, out dtEnd);
                dteFrom.DateTime = dtBegin;
                dteTo.DateTime = dtEnd;
            }

        }

        #endregion

        #region Hàm xử lý riêng của Form
        private void ViewDanhSach(List<GenCode> model)
        {
            uGridDanhSach.DataSource = model;
            Utils.ConfigGrid(uGridDanhSach, ConstDatabase.GenCode_TableName);
            uGridDanhSach.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGridDanhSach.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            uGridDanhSach.Text = "";
            // xét style cho các cột có dạng checkbox
            uGridDanhSach.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
            uGridDanhSach.DisplayLayout.Bands[0].Columns["Check"].Hidden = false;
            uGridDanhSach.DisplayLayout.Bands[0].Columns["CurrentValue"].Hidden = true;
            uGridDanhSach.DisplayLayout.Bands[0].Columns["Check"].Header.Caption = "";
            uGridDanhSach.DisplayLayout.Bands[0].Columns["Check"].Header.VisiblePosition = 0;
            uGridDanhSach.DisplayLayout.Bands[0].Columns["Prefix"].Header.VisiblePosition = 1;
            UltraGridBand band = uGridDanhSach.DisplayLayout.Bands[0];
            UltraGridColumn ugc = band.Columns["Check"];
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            band.Columns["Check"].CellActivation = Activation.AllowEdit;
            band.Columns["Check"].Header.Fixed = true;
            #region Colums Style
            foreach (var item in uGridDanhSach.DisplayLayout.Bands[0].Columns)
            {
                if (item.Key.Equals("Check"))
                    item.Width = 30;
                if (item.Key.Equals("VoucherTypeID"))
                    this.ConfigCbbToGrid(uGridDanhSach, item.Key, _typeSrv.GetAll(), "ID", "TypeName", ConstDatabase.Type_TableName);
            }
            #endregion
            #region Đếm số dòng
            uGridDanhSach.DisplayLayout.Override.AllowRowSummaries = Infragistics.Win.UltraWinGrid.AllowRowSummaries.
            Default; //Ẩn ký hiệu tổng trên Header Caption
            if (band.Summaries.Count != 0) band.Summaries.Clear();
            Infragistics.Win.UltraWinGrid.SummarySettings summary = band.Summaries.Add("Count", Infragistics.Win.UltraWinGrid.SummaryType.Count, band.Columns["Check"]);
            summary.DisplayFormat = "Số dòng dữ liệu: {0:N0}";
            summary.SummaryPosition = Infragistics.Win.UltraWinGrid.SummaryPosition.Left;
            uGridDanhSach.DisplayLayout.Override.SummaryFooterCaptionVisible = Infragistics.Win.DefaultableBoolean.False;   //ẩn 
            summary.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.BottomFixed | Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.GroupByRowsFooter;
            uGridDanhSach.DisplayLayout.Override.GroupBySummaryDisplayStyle = Infragistics.Win.UltraWinGrid.GroupBySummaryDisplayStyle.SummaryCells;
            uGridDanhSach.DisplayLayout.Bands[0].SummaryFooterCaption = "Số dòng dữ liệu: ";
            uGridDanhSach.DisplayLayout.Override.SummaryFooterAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
            #endregion
        }
        #endregion

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (dteFrom.Value == null || dteTo.Value == null)
                {
                    MSG.Warning("Ngày không được để trống");
                    return;
                }
                if ((DateTime)dteFrom.Value > (DateTime)dteTo.Value)
                {
                    MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                    return;
                }

                DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
                if (dateTimeCacheHistory != null)
                {
                    dateTimeCacheHistory.DtBeginDate = dteFrom.DateTime;
                    dateTimeCacheHistory.DtEndDate = dteTo.DateTime;
                    dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                    _IDateTimeCacheHistoryService.BeginTran();
                    _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                    _IDateTimeCacheHistoryService.CommitTran();
                }
                else
                {
                    dateTimeCacheHistory = new DateTimeCacheHistory();
                    dateTimeCacheHistory.ID = Guid.NewGuid();
                    dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                    dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                    dateTimeCacheHistory.SubSystemCode = this.Name;
                    dateTimeCacheHistory.DtBeginDate = dteFrom.DateTime;
                    dateTimeCacheHistory.DtEndDate = dteTo.DateTime;
                    _IDateTimeCacheHistoryService.BeginTran();
                    _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                    _IDateTimeCacheHistoryService.CommitTran();
                }
                var lst = (uGridDanhSach.DataSource as List<GenCode>).Where(t => t.Check).ToList();
                if (lst.Count == 0)
                {
                    MSG.Warning("Chưa chọn loại chứng từ");
                    return;
                }
                List<GVoucherListDetail> lstDetails1 = new List<GVoucherListDetail>();
                List<GVoucherListDetail> lstDetails2 = new List<GVoucherListDetail>();
                FRBANGKECHUNGTUCHUALAPCTGSex fm = new FRBANGKECHUNGTUCHUALAPCTGSex((DateTime)dteFrom.Value, (DateTime)dteTo.Value, lst);
                fm.Show(this);
                
            }
            catch (Exception)
            {
                WaitingFrm.StopWaiting();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cbbTypeiD_ItemNotInList(object sender, Infragistics.Win.UltraWinEditors.ValidationErrorEventArgs e)
        {
            //MSG.Warning("Loại chứng từ không tồn tại "+ cbbTypeiD.Text + " xin vui lòng kiểm tra lại");
            //cbbTypeiD.Focus();
        }
    }


}
