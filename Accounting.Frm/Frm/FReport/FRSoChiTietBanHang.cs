﻿using Accounting.Core.DAO;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRSoChiTietBanHang : Form
    {
        private readonly IMaterialGoodsCategoryService _IMaterialGoodsCategoryService;
        private readonly IGeneralLedgerService _IGeneralLedgerService;
        private List<MaterialGoods> _lstMaterialGoods = new List<MaterialGoods>();
        private List<MaterialGoods> _lstMaterialGoodsInitial = new List<MaterialGoods>();
        private readonly IMaterialGoodsService _IMaterialGoodsService;
        private string _subSystemCode; public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        public FRSoChiTietBanHang()
        {
            InitializeComponent();
            _IMaterialGoodsCategoryService = IoC.Resolve<IMaterialGoodsCategoryService>();
            _IGeneralLedgerService = IoC.Resolve<IGeneralLedgerService>();
            _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
            ReportUtils.ProcessControls(this);
            //Load combobox vật tư
            List<MaterialGoodsCategory> listMgcDb = _IMaterialGoodsCategoryService.GetAll_ByIsActive(true);
            List<MaterialGoodsCategory> listMgCDisplay = new List<MaterialGoodsCategory>();
            MaterialGoodsCategory itemMgCfirst = new MaterialGoodsCategory { MaterialGoodsCategoryName = "Tất cả", MaterialGoodsCategoryCode = "Tất cả" };//edit by cuongpv 20190419 <<Tất cả>> -> Tất cả
            //MaterialGoodsCategory itemMgclast = new MaterialGoodsCategory { MaterialGoodsCategoryName = "<<Loại khác>>", MaterialGoodsCategoryCode = "<<Loại khác>>" }; //comment by cuongpv 20190419
            listMgCDisplay.Add(itemMgCfirst);
            listMgCDisplay.AddRange(listMgcDb);
            //listMgCDisplay.Add(itemMgclast); //comment by cuongpv 20190419
            cbbMgCategory.DataSource = listMgCDisplay;
            cbbMgCategory.DisplayMember = "MaterialGoodsCategoryName";
            cbbMgCategory.ValueMember = "ID";
            Utils.ConfigGrid(cbbMgCategory, ConstDatabase.MaterialGoodsCategory_TableName);
            //Gọi báo cáo
            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\SoChiTietBanHang.rst", path);
            //Load Gridview
            _lstMaterialGoodsInitial = _IMaterialGoodsService.GetAll().Where(c => c.IsActive).OrderBy(n => n.MaterialGoodsCode).ToList();
            _lstMaterialGoods = Utils.CloneObject(_lstMaterialGoodsInitial);
            uGridSelectMG.SetDataBinding(_lstMaterialGoods, "");
            Utils.ConfigGrid(uGridSelectMG, ConstDatabase.MaterialGoods_TableName_FRSUpdatingIW);
            ConfigGridColumnCheck_Produce(uGridSelectMG, "Status");
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dtBeginDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }
        }

        private void ConfigGridColumnCheck_Produce(UltraGrid ultraGrid, string columnKey = "Status")
        {
            ultraGrid.DisplayLayout.Bands[0].Summaries.Clear();
            ultraGrid.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
            ultraGrid.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.True;
            ultraGrid.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            ultraGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            UltraGridBand band = ultraGrid.DisplayLayout.Bands[0];
            UltraGridColumn ugc = band.Columns[columnKey];
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            ultraGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            ultraGrid.DisplayLayout.Bands[0].Columns[columnKey].CellActivation = Activation.NoEdit;
            for (int i = 0; i < ultraGrid.DisplayLayout.Bands[0].Columns.Count; i++)
            {
                if (ultraGrid.DisplayLayout.Bands[0].Columns[i].Key == columnKey)
                    ultraGrid.DisplayLayout.Bands[0].Columns[i].CellActivation = Activation.AllowEdit;
                else
                    ultraGrid.DisplayLayout.Bands[0].Columns[i].CellActivation = Activation.NoEdit;
            }

            band.Columns[columnKey].Header.VisiblePosition = 0;
            band.Columns[columnKey].Width = 40;
            band.Columns[columnKey].Header.Fixed = true;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }

            if ((DateTime)dtBeginDate.Value > (DateTime)dtEndDate.Value)
            {
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            string dsID = "";
            DateTime ngaybd = new DateTime((int)DateTime.Now.Year, 1, 1);
            DateTime ngaykt = new DateTime((int)DateTime.Now.Year, 12, 31);

            ReportProcedureSDS sp = new ReportProcedureSDS();
            List<ListInventoryItemID> data = new List<ListInventoryItemID>();
            List<MaterialGoods> listMaterialGoods = new List<MaterialGoods>();
            List<ListInventoryItemID> list_sum_goc = new List<ListInventoryItemID>();
            if (!_lstMaterialGoods.Any(c => c.Status))
            {
                MSG.Error(resSystem.Report_03);
                return;
            }
            else
            {
                listMaterialGoods = _lstMaterialGoods.Where(c => c.Status).ToList();
                var listItem1 = listMaterialGoods.Select(t => t.ID.ToString());
                dsID = string.Join(",", listItem1.ToArray());
            }
            Decimal sumgiagoc = 0;
            //_IGeneralLedgerService.Query.Where(t => (t.PostedDate >= ngaybd && t.PostedDate <= ngaykt) && t.Account == "632" && dsID.Contains(t.)).ToList();
            data = sp.GetSoChiTietBanHang((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, dsID, "", "", "", false, ref sumgiagoc, ref list_sum_goc);
            if (data.Count == 0)
            {
                MSG.Warning("Không có dữ liệu để lên báo cáo");
            }
            else
            {
                var list = (from a in data
                            join b in list_sum_goc on a.InventoryItemID equals b.InventoryItemID into gr
                            from b in gr.DefaultIfEmpty()
                            select new ListInventoryItemID
                            {
                                RefID = a.RefID,
                                TypeID = a.TypeID,
                                Dien_giai = a.Dien_giai,
                                DON_GIA = a.DON_GIA,
                                DVT = a.DVT,
                                KHAC = a.KHAC,
                                InventoryItemID = a.InventoryItemID,
                                MaterialGoodsName = a.MaterialGoodsName,
                                ngay_CT = a.ngay_CT,
                                Ngay_HD = a.Ngay_HD,
                                ngay_HT = a.ngay_HT,
                                SL = a.SL,
                                SO_HD = a.SO_HD,
                                So_Hieu = a.So_Hieu,
                                TK_DOIUNG = a.TK_DOIUNG,
                                TT = a.TT,
                                SUM_GIA_GOC = b == null ? 0 : b.SUM_GIA_GOC
                            }).ToList();

                var rD = new ListInventoryItemID_period();
                rD.Period = ReportUtils.GetPeriod((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value);
                rD.GiaBanGoc = sumgiagoc == null ? 0 : sumgiagoc;
                //sumgiagoc.Sum(t => t.DebitAmount);
                var f = new ReportForm<ListInventoryItemID>(fileReportSlot1, _subSystemCode);
                f.AddDatasource("DBSoChiTietBanHang", list, true);
                f.AddDatasource("Detail", rD);
                f.SetHyperlinkFunction("Accounting.ReportUtils", "Hyperlink");
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            foreach (var item in uGridSelectMG.Rows)
            {
                item.Cells["Status"].Value = false;
            }
            Close();
        }

        private void ultraGroupBox1_Click(object sender, EventArgs e)
        {

        }
        private void uMgCategory_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (e.Row != null)
            {
                var selectMGC = (MaterialGoodsCategory)e.Row.ListObject;
                if (selectMGC.MaterialGoodsCategoryName == "Tất cả")//edit by cuongpv <<Tất cả>> -> Tất cả
                {
                    _lstMaterialGoods = _lstMaterialGoodsInitial.Where(c => c.IsActive).ToList();
                    uGridSelectMG.SetDataBinding(_lstMaterialGoods, "");
                }
                //else if (selectMGC.MaterialGoodsCategoryName == "<<Loại khác>>") //coment by cuongpv
                //{
                //    _lstMaterialGoods = _lstMaterialGoodsInitial.Where(c => c.MaterialGoodsCategoryID == null).ToList();
                //    uGridSelectMG.SetDataBinding(_lstMaterialGoods, "");
                //}
                else
                {
                    _lstMaterialGoods = _lstMaterialGoodsInitial.Where(c => c.MaterialGoodsCategoryID == selectMGC.ID && c.IsActive).ToList();
                    uGridSelectMG.SetDataBinding(_lstMaterialGoods, "");
                }
            }
        }

        private void Form_FormClosed(object sender, FormClosedEventArgs e)
        {
            foreach (var item in uGridSelectMG.Rows)
            {
                item.Cells["Status"].Value = false;
            }
        }
    }
}
