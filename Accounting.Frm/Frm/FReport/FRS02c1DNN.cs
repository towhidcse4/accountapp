﻿using Accounting.Core.DAO;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.Report;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRS02c1DNN : Form
    {
        string _subSystemCode;
        readonly List<Account> _lstAccount = new List<Account>();
        IAccountService _IAccountService;
        public static IGVoucherListDetailService IGVoucherListDetailService
        {
            get { return IoC.Resolve<IGVoucherListDetailService>(); }
        }
        readonly List<AccountReport> _lstAccountReport = ReportUtils.LstAccountBankReport;
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        public FRS02c1DNN()
        {
            InitializeComponent();
            _subSystemCode = "";
            _IAccountService = IoC.Resolve<IAccountService>();
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\S02c1_DNN.rst", path);
            _lstAccount = _IAccountService.GetListAccountIsActive(true);
            _lstAccountReport = _lstAccount.Select(c => new AccountReport()
            {
                AccountNumber = c.AccountNumber,
                AccountGroupID = c.AccountGroupID,
                AccountGroupKindView = c.AccountGroupKindView,
                AccountGroupKind = c.AccountGroupKind,
                AccountName = c.AccountName,
                AccountNameGlobal = c.AccountNameGlobal,
                IsActive = c.IsActive,
                Description = c.Description,
                DetailType = c.DetailType,
                ID = c.ID,
                ParentID = c.ParentID,
                IsParentNode = c.IsParentNode,
                Grade = c.Grade
            }).OrderBy(c => c.AccountNumber).ToList();
            ugridAccount.SetDataBinding(_lstAccountReport.ToList(), "");
            ReportUtils.ProcessControls(this);
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dtBeginDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }

            if ((DateTime)dtBeginDate.Value > (DateTime)dtEndDate.Value)
            {
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            bool check = cbCongGopButToan.Checked;
            string listaccount = "";
            if (!_lstAccountReport.Any(c => c.Check))
            {
                MSG.Warning(resSystem.Report_01);
                return;
            }
            var dsaccount = _lstAccountReport.Where(t => t.Check).Select(c => c.AccountNumber.ToString()).ToList();
            listaccount = string.Join(",", dsaccount.ToArray());
            List<S02C1DNN> dataHeader = new List<S02C1DNN>();
            List<S02C1DNN> data = new List<S02C1DNN>();
            ReportProcedureSDS db = new ReportProcedureSDS();
            //data = db.GetSoCaiS02c1DNN("", 1, (DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, listaccount, check, ref dataHeader).OrderBy(o => o.PostedDate).ToList();
            data = IGVoucherListDetailService.GetS02C1DNN((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, dsaccount, check);
            DateTime begindt = new DateTime(((DateTime)dtBeginDate.Value).Year, ((DateTime)dtBeginDate.Value).Month, ((DateTime)dtBeginDate.Value).Day, 00, 00, 00);
            var lstopAcc = Utils.ListGeneralLedger.Where(x => x.No == "OPN" && dsaccount.Any(d => x.Account.StartsWith(d))).ToList();
            dataHeader = (from a in data
                          where a.RefDate < begindt
                          group a by a.AccountNumber into g
                          select new S02C1DNN
                          {
                              AccountNumber = g.Key,
                              OpenningCreditAmount = (g.Sum(x => x.CreditAmount) + lstopAcc.Where(x => x.Account.StartsWith(g.Key)).Sum(t => t.CreditAmount)
                              - (g.Sum(x => x.DebitAmount) + lstopAcc.Where(x => x.Account.StartsWith(g.Key)).Sum(t => t.DebitAmount))) > 0 ?
                              (g.Sum(x => x.CreditAmount) + lstopAcc.Where(x => x.Account.StartsWith(g.Key)).Sum(t => t.CreditAmount)
                              - (g.Sum(x => x.DebitAmount) + lstopAcc.Where(x => x.Account.StartsWith(g.Key)).Sum(t => t.DebitAmount))) : 0,
                              OpenningDebitAmount = (g.Sum(x => x.DebitAmount) + lstopAcc.Where(x => x.Account.StartsWith(g.Key)).Sum(t => t.DebitAmount)
                              - (g.Sum(x => x.CreditAmount) + lstopAcc.Where(x => x.Account.StartsWith(g.Key)).Sum(t => t.CreditAmount))) > 0 ?
                              (g.Sum(x => x.DebitAmount) + lstopAcc.Where(x => x.Account.StartsWith(g.Key)).Sum(t => t.DebitAmount)
                              - (g.Sum(x => x.CreditAmount) + lstopAcc.Where(x => x.Account.StartsWith(g.Key)).Sum(t => t.CreditAmount))) : 0
                          }).ToList();
            data = data.Where(x => x.RefDate >= begindt).ToList();
            foreach (var acc in dsaccount)
            {
                if (!data.Any(x => x.AccountNumber == acc))
                {
                    var dt = new S02C1DNN();
                    dt.AccountNumber = acc;
                    dt.CreditAmount = 0;
                    dt.DebitAmount = 0;
                    data.Add(dt);
                }
                if (!dataHeader.Any(x => x.AccountNumber == acc) && data.Any(x => x.AccountNumber == acc))
                {
                    var header = new S02C1DNN();
                    header.AccountNumber = acc;
                    header.OpenningCreditAmount = (lstopAcc.Where(x => x.Account.StartsWith(acc)).Sum(t => t.CreditAmount) - 
                        lstopAcc.Where(x => x.Account.StartsWith(acc)).Sum(t => t.DebitAmount)) > 0 ? 
                        (lstopAcc.Where(x => x.Account.StartsWith(acc)).Sum(t => t.CreditAmount) -
                        lstopAcc.Where(x => x.Account.StartsWith(acc)).Sum(t => t.DebitAmount)) : 0;
                    header.OpenningDebitAmount = (lstopAcc.Where(x => x.Account.StartsWith(acc)).Sum(t => t.DebitAmount) -
                        lstopAcc.Where(x => x.Account.StartsWith(acc)).Sum(t => t.CreditAmount)) > 0 ?
                        (lstopAcc.Where(x => x.Account.StartsWith(acc)).Sum(t => t.DebitAmount) -
                        lstopAcc.Where(x => x.Account.StartsWith(acc)).Sum(t => t.CreditAmount)) : 0;
                    dataHeader.Add(header);
                }
                
            }
            
            if (data.Count == 0 && dataHeader.Count == 0)
            {
                MSG.Warning("Không có dữ liệu để lên báo cáo!");
            }
            else
            {
                var rD = new S02C1DNNDetail();
                rD.Period = ReportUtils.GetPeriod((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value);
                var f = new ReportForm<S02C1DNN>(fileReportSlot1, _subSystemCode);
                f.AddDatasource("S02C1DNNHeader", dataHeader, true);
                f.AddDatasource("S02C1DNN", data, true);
                f.AddDatasource("Detail", rD);
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
