﻿using Accounting.Core.DAO;
using Accounting.Core.Domain.obj.Report;
using Accounting.TextMessage;
using Infragistics.Documents.Excel;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRTONGCONGNOPTTHEOGROUPKHTruocIn : Form
    {
        List<SA_GetReceivableSummaryByGroup> data1 = new List<SA_GetReceivableSummaryByGroup>();
        string tieude1;
        string currencyID1;
        public FRTONGCONGNOPTTHEOGROUPKHTruocIn(List<SA_GetReceivableSummaryByGroup> data, string tieude, string currencyID)
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\TongCongNoPhaiThuGroup.rst", path);

            tieude1 = tieude;
            data1 = new List<SA_GetReceivableSummaryByGroup>(data);
            currencyID1 = currencyID;
            List<SA_GetReceivableSummaryByGroup> listGroup = (from a in data
                                                 group a by a.AccountObjectGroupList into g
                                                 select new SA_GetReceivableSummaryByGroup
                                                 {
                                                     Ordercode = -1,
                                                     AccountObjectGroupList = g.Key,

                                                     OpenCreditAmount = g.Sum(t => t.OpenCreditAmount),
                                                     OpenCreditAmountOC = g.Sum(t => t.OpenCreditAmountOC),
                                                      OpenDebitAmount = g.Sum(t => t.OpenDebitAmount),
                                                     OpenDebitAmountOC = g.Sum(t => t.OpenDebitAmountOC),
                                                      CloseCreditAmount = g.Sum(t => t.CloseCreditAmount),
                                                     CloseCreditAmountOC = g.Sum(t => t.CloseCreditAmountOC),
                                                      CloseDebitAmount = g.Sum(t => t.CloseDebitAmount),
                                                     CloseDebitAmountOC = g.Sum(t => t.CloseDebitAmountOC),
                                                      DebitAmount = g.Sum(t => t.DebitAmount),
                                                     DebitAmountOC = g.Sum(t => t.DebitAmountOC),
                                                      CreditAmount = g.Sum(t => t.CreditAmount),
                                                     CreditAmountOC = g.Sum(t => t.CreditAmountOC)
                                                 }).ToList();
            SA_GetReceivableSummaryByGroup tongcong = new SA_GetReceivableSummaryByGroup();
            foreach (var item in listGroup)
            {
                item.AccountingObjectName = data.FirstOrDefault(n => n.AccountObjectGroupList == item.AccountObjectGroupList).AccountingObjectGroupName;
                data.Add(item);
                tongcong.OpenCreditAmount += item.OpenCreditAmount;
                tongcong.OpenCreditAmountOC += item.OpenCreditAmountOC;
                tongcong.OpenDebitAmount += item.OpenDebitAmount;
                tongcong.OpenDebitAmountOC += item.OpenDebitAmountOC;
                tongcong.DebitAmount += item.DebitAmount;
                tongcong.DebitAmountOC += item.DebitAmountOC;
                tongcong.CreditAmount += item.CreditAmount;
                tongcong.CreditAmountOC += item.CreditAmountOC;
                tongcong.CloseCreditAmount += item.CloseCreditAmount;
                tongcong.CloseCreditAmountOC += item.CloseCreditAmountOC;
                tongcong.CloseDebitAmount += item.CloseDebitAmount;
                tongcong.CloseDebitAmountOC += item.CloseDebitAmountOC;
            }
            data = data.OrderBy(n => n.AccountObjectGroupList).ThenBy(n => n.Ordercode).ToList();
            tongcong.Ordercode = -1;
            tongcong.AccountingObjectName = "Tổng cộng";
            //data.Add(tongcong);
            uGridDuLieu.DataSource = data;
            Utils.ConfigGrid(uGridDuLieu, ConstDatabase.SA_GetReceivableSummaryByGroup_TableName);
            uGridDuLieu.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            UltraGridBand parentBand = this.uGridDuLieu.DisplayLayout.Bands[0];
            parentBand.RowLayoutStyle = RowLayoutStyle.GroupLayout;
            parentBand.Override.HeaderPlacement = HeaderPlacement.FixedOnTop;

            UltraGridGroup parentBandGroupPeriod;
            if (parentBand.Groups.Exists("ParentBandGroupPeriod"))
            {
                parentBandGroupPeriod = parentBand.Groups["ParentBandGroupPeriod"];
                parentBandGroupPeriod.Header.Caption = tieude;
            }
            else
                parentBandGroupPeriod = parentBand.Groups.Add("ParentBandGroupPeriod", tieude);
            parentBandGroupPeriod.Header.Appearance.FontData.Italic = DefaultableBoolean.True;
            parentBandGroupPeriod.Header.Appearance.FontData.SizeInPoints = 10;
            parentBandGroupPeriod.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupPeriod.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPeriod.Header.Appearance.BorderColor = Color.Black;
            UltraGridGroup parentBandGroupTitle;
            if (parentBand.Groups.Exists("ParentBandGroupTitle"))
                parentBandGroupTitle = parentBand.Groups["ParentBandGroupTitle"];
            else
                parentBandGroupTitle = parentBand.Groups.Add("ParentBandGroupTitle", "TỔNG HỢP CÔNG NỢ PHẢI THU THEO NHÓM KHÁCH HÀNG");
            parentBandGroupTitle.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroupTitle.Header.Appearance.FontData.SizeInPoints = 12;
            parentBandGroupTitle.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupTitle.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupTitle.Header.Appearance.BorderColor = Color.Black;
            parentBandGroupPeriod.RowLayoutGroupInfo.ParentGroup = parentBandGroupTitle;

            UltraGridGroup parentBandGroupDK;
            if (parentBand.Groups.Exists("ParentBandGroupDK"))
                parentBandGroupDK = parentBand.Groups["ParentBandGroupDK"];
            else
                parentBandGroupDK = parentBand.Groups.Add("ParentBandGroupDK", "Đầu kỳ");
           // parentBandGroupDK.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroupDK.Header.Appearance.FontData.SizeInPoints = 10;
            parentBandGroupDK.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupDK.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;

            //parentBandGroupSO_HD.Header.Appearance.BorderColor = Color.Black;
            parentBandGroupDK.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            //  parentBandGroupDK.RowLayoutGroupInfo.SpanY = 2;

            UltraGridGroup parentBandGroupPS;
            if (parentBand.Groups.Exists("ParentBandGroupPS"))
                parentBandGroupPS = parentBand.Groups["ParentBandGroupPS"];
            else
                parentBandGroupPS = parentBand.Groups.Add("ParentBandGroupPS", "Phát sinh");
           // parentBandGroupPS.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroupPS.Header.Appearance.FontData.SizeInPoints = 10;
            parentBandGroupPS.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupPS.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;

            //parentBandGroupSO_HD.Header.Appearance.BorderColor = Color.Black;
            parentBandGroupPS.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            //  parentBandGroupDK.RowLayoutGroupInfo.SpanY = 2;

            UltraGridGroup parentBandGroupCK;
            if (parentBand.Groups.Exists("ParentBandGroupCK"))
                parentBandGroupCK = parentBand.Groups["ParentBandGroupCK"];
            else
                parentBandGroupCK = parentBand.Groups.Add("ParentBandGroupCK", "Cuối kỳ");
           // parentBandGroupCK.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroupCK.Header.Appearance.FontData.SizeInPoints = 10;
            parentBandGroupCK.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupCK.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;

            //parentBandGroupSO_HD.Header.Appearance.BorderColor = Color.Black;
            parentBandGroupCK.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            //  parentBandGroupDK.RowLayoutGroupInfo.SpanY = 2;
            uGridDuLieu.DisplayLayout.Bands[0].Columns["AccountingObjectCode"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            uGridDuLieu.DisplayLayout.Bands[0].Columns["AccountingObjectName"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;

            parentBand.Columns["AccountingObjectCode"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["AccountingObjectCode"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["AccountingObjectCode"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["AccountingObjectCode"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["AccountingObjectCode"].Header.Appearance.BorderColor = Color.Black;
            //parentBand.Columns["AccountingObjectCode"].RowLayoutColumnInfo.SpanX
            parentBand.Columns["AccountingObjectCode"].RowLayoutColumnInfo.SpanX = 150;
            parentBand.Columns["AccountingObjectCode"].RowLayoutColumnInfo.SpanY = 64;
                parentBand.Columns["AccountingObjectCode"].RowLayoutColumnInfo.OriginX = 0;
                parentBand.Columns["AccountingObjectCode"].RowLayoutColumnInfo.OriginY = 0;

            parentBand.Columns["AccountingObjectName"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["AccountingObjectName"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["AccountingObjectName"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["AccountingObjectName"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["AccountingObjectName"].Header.Appearance.BorderColor = Color.Black;
            parentBand.Columns["AccountingObjectName"].RowLayoutColumnInfo.SpanY = 64;
            parentBand.Columns["AccountingObjectName"].RowLayoutColumnInfo.SpanX = 150;
            parentBand.Columns["AccountingObjectName"].RowLayoutColumnInfo.OriginX = parentBand.Columns["AccountingObjectCode"].RowLayoutColumnInfo.SpanX;
            parentBand.Columns["AccountingObjectName"].RowLayoutColumnInfo.OriginY = 0;

            Utils.AddSumColumn(uGridDuLieu, "AccountingObjectName", false);
            uGridDuLieu.DisplayLayout.Bands[0].Summaries[1].DisplayFormat = " ";

            if (currencyID=="VND")
            {
                uGridDuLieu.DisplayLayout.Bands[0].Columns["OpenDebitAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGridDuLieu.DisplayLayout.Bands[0].Columns["OpenCreditAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGridDuLieu.DisplayLayout.Bands[0].Columns["DebitAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGridDuLieu.DisplayLayout.Bands[0].Columns["CreditAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGridDuLieu.DisplayLayout.Bands[0].Columns["CloseDebitAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGridDuLieu.DisplayLayout.Bands[0].Columns["CloseCreditAmount"].FormatNumberic(ConstDatabase.Format_TienVND);

                uGridDuLieu.DisplayLayout.Bands[0].Columns["OpenDebitAmountOC"].Hidden=true;
                uGridDuLieu.DisplayLayout.Bands[0].Columns["OpenCreditAmountOC"].Hidden = true;
                uGridDuLieu.DisplayLayout.Bands[0].Columns["DebitAmountOC"].Hidden = true;
                uGridDuLieu.DisplayLayout.Bands[0].Columns["CreditAmountOC"].Hidden = true;
                uGridDuLieu.DisplayLayout.Bands[0].Columns["CloseDebitAmountOC"].Hidden = true;
                uGridDuLieu.DisplayLayout.Bands[0].Columns["CloseCreditAmountOC"].Hidden = true;

                Utils.AddSumColumn1(tongcong.OpenCreditAmount, uGridDuLieu, "OpenCreditAmount", false, constDatabaseFormat: 1);
                Utils.AddSumColumn1(tongcong.OpenDebitAmount, uGridDuLieu, "OpenDebitAmount", false, constDatabaseFormat: 1);
                Utils.AddSumColumn1(tongcong.CreditAmount, uGridDuLieu, "CreditAmount", false, constDatabaseFormat: 1);
                Utils.AddSumColumn1(tongcong.DebitAmount, uGridDuLieu, "DebitAmount", false, constDatabaseFormat: 1);
                Utils.AddSumColumn1(tongcong.CloseCreditAmount, uGridDuLieu, "CloseCreditAmount", false, constDatabaseFormat: 1);
                Utils.AddSumColumn1(tongcong.CloseDebitAmount, uGridDuLieu, "CloseDebitAmount", false, constDatabaseFormat: 1);

                uGridDuLieu.DisplayLayout.Bands[0].Columns["OpenDebitAmount"].RowLayoutColumnInfo.ParentGroup=parentBandGroupDK;
                uGridDuLieu.DisplayLayout.Bands[0].Columns["OpenCreditAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupDK;
                uGridDuLieu.DisplayLayout.Bands[0].Columns["DebitAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPS;
                uGridDuLieu.DisplayLayout.Bands[0].Columns["CreditAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPS;
                uGridDuLieu.DisplayLayout.Bands[0].Columns["CloseDebitAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupCK;
                uGridDuLieu.DisplayLayout.Bands[0].Columns["CloseCreditAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupCK;

                parentBand.Columns["OpenDebitAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["OpenDebitAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["OpenDebitAmount"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["OpenDebitAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["OpenDebitAmount"].Header.Appearance.BorderColor = Color.Black;

                parentBand.Columns["OpenCreditAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["OpenCreditAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["OpenCreditAmount"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["OpenCreditAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["OpenCreditAmount"].Header.Appearance.BorderColor = Color.Black;

                parentBand.Columns["DebitAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["DebitAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["DebitAmount"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["DebitAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["DebitAmount"].Header.Appearance.BorderColor = Color.Black;

                parentBand.Columns["CreditAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["CreditAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["CreditAmount"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["CreditAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["CreditAmount"].Header.Appearance.BorderColor = Color.Black;

                parentBand.Columns["CloseDebitAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["CloseDebitAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["CloseDebitAmount"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["CloseDebitAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["CloseDebitAmount"].Header.Appearance.BorderColor = Color.Black;

                parentBand.Columns["CloseCreditAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["CloseCreditAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["CloseCreditAmount"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["CloseCreditAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["CloseCreditAmount"].Header.Appearance.BorderColor = Color.Black;
            }
            else
            {
                uGridDuLieu.DisplayLayout.Bands[0].Columns["OpenDebitAmountOC"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGridDuLieu.DisplayLayout.Bands[0].Columns["OpenCreditAmountOC"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGridDuLieu.DisplayLayout.Bands[0].Columns["DebitAmountOC"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGridDuLieu.DisplayLayout.Bands[0].Columns["CreditAmountOC"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGridDuLieu.DisplayLayout.Bands[0].Columns["CloseDebitAmountOC"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGridDuLieu.DisplayLayout.Bands[0].Columns["CloseCreditAmountOC"].FormatNumberic(ConstDatabase.Format_TienVND);

                uGridDuLieu.DisplayLayout.Bands[0].Columns["OpenDebitAmount"].Hidden = true;
                uGridDuLieu.DisplayLayout.Bands[0].Columns["OpenCreditAmount"].Hidden = true;
                uGridDuLieu.DisplayLayout.Bands[0].Columns["DebitAmount"].Hidden = true;
                uGridDuLieu.DisplayLayout.Bands[0].Columns["CreditAmount"].Hidden = true;
                uGridDuLieu.DisplayLayout.Bands[0].Columns["CloseDebitAmount"].Hidden = true;
                uGridDuLieu.DisplayLayout.Bands[0].Columns["CloseCreditAmount"].Hidden = true;

                Utils.AddSumColumn1(tongcong.OpenCreditAmountOC, uGridDuLieu, "OpenCreditAmountOC", false, constDatabaseFormat: 0);
                Utils.AddSumColumn1(tongcong.OpenDebitAmountOC, uGridDuLieu, "OpenDebitAmountOC", false, constDatabaseFormat: 0);
                Utils.AddSumColumn1(tongcong.CreditAmountOC, uGridDuLieu, "CreditAmountOC", false, constDatabaseFormat: 0);
                Utils.AddSumColumn1(tongcong.DebitAmountOC, uGridDuLieu, "DebitAmountOC", false, constDatabaseFormat: 0);
                Utils.AddSumColumn1(tongcong.CloseCreditAmountOC, uGridDuLieu, "CloseCreditAmountOC", false, constDatabaseFormat: 0);
                Utils.AddSumColumn1(tongcong.CloseDebitAmountOC, uGridDuLieu, "CloseDebitAmountOC", false, constDatabaseFormat: 0);

                uGridDuLieu.DisplayLayout.Bands[0].Columns["OpenDebitAmountOC"].RowLayoutColumnInfo.ParentGroup = parentBandGroupDK;
                uGridDuLieu.DisplayLayout.Bands[0].Columns["OpenCreditAmountOC"].RowLayoutColumnInfo.ParentGroup = parentBandGroupDK;
                uGridDuLieu.DisplayLayout.Bands[0].Columns["DebitAmountOC"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPS;
                uGridDuLieu.DisplayLayout.Bands[0].Columns["CreditAmountOC"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPS;
                uGridDuLieu.DisplayLayout.Bands[0].Columns["CloseDebitAmountOC"].RowLayoutColumnInfo.ParentGroup = parentBandGroupCK;
                uGridDuLieu.DisplayLayout.Bands[0].Columns["CloseCreditAmountOC"].RowLayoutColumnInfo.ParentGroup = parentBandGroupCK;

                parentBand.Columns["OpenDebitAmountOC"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["OpenDebitAmountOC"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["OpenDebitAmountOC"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["OpenDebitAmountOC"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["OpenDebitAmountOC"].Header.Appearance.BorderColor = Color.Black;

                parentBand.Columns["OpenCreditAmountOC"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["OpenCreditAmountOC"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["OpenCreditAmountOC"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["OpenCreditAmountOC"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["OpenCreditAmountOC"].Header.Appearance.BorderColor = Color.Black;

                parentBand.Columns["DebitAmountOC"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["DebitAmountOC"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["DebitAmountOC"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["DebitAmountOC"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["DebitAmountOC"].Header.Appearance.BorderColor = Color.Black;

                parentBand.Columns["CreditAmountOC"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["CreditAmountOC"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["CreditAmountOC"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["CreditAmountOC"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["CreditAmountOC"].Header.Appearance.BorderColor = Color.Black;

                parentBand.Columns["CloseDebitAmountOC"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["CloseDebitAmountOC"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["CloseDebitAmountOC"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["CloseDebitAmountOC"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["CloseDebitAmountOC"].Header.Appearance.BorderColor = Color.Black;

                parentBand.Columns["CloseCreditAmountOC"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["CloseCreditAmountOC"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["CloseCreditAmountOC"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["CloseCreditAmountOC"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["CloseCreditAmountOC"].Header.Appearance.BorderColor = Color.Black;
            }
            uGridDuLieu.DisplayLayout.Bands[0].Columns["AccountingObjectCode"].Header.VisiblePosition = 0;
            uGridDuLieu.DisplayLayout.Bands[0].Columns["AccountingObjectName"].Header.VisiblePosition = 1;
           
                // parentBandGroupDK.RowLayoutGroupInfo.SpanY = 64;
            parentBandGroupDK.RowLayoutGroupInfo.OriginX = parentBand.Columns["AccountingObjectName"].RowLayoutColumnInfo.SpanX+ parentBand.Columns["AccountingObjectName"].RowLayoutColumnInfo.OriginX;
            parentBandGroupDK.RowLayoutGroupInfo.OriginY = 0;
            parentBandGroupDK.RowLayoutGroupInfo.SpanX = 300;

            parentBandGroupPS.RowLayoutGroupInfo.OriginX = parentBandGroupDK.RowLayoutGroupInfo.OriginX + parentBandGroupDK.RowLayoutGroupInfo.SpanX;
            parentBandGroupPS.RowLayoutGroupInfo.OriginY = 0;
            parentBandGroupPS.RowLayoutGroupInfo.SpanX = 300;

            parentBandGroupCK.RowLayoutGroupInfo.OriginX = parentBandGroupPS.RowLayoutGroupInfo.OriginX + parentBandGroupPS.RowLayoutGroupInfo.SpanX;
            parentBandGroupCK.RowLayoutGroupInfo.OriginY = 0;
            parentBandGroupCK.RowLayoutGroupInfo.SpanX = 300;
            SetDoSoAm(uGridDuLieu);
            //Utils.AddSumColumn1(-2, uGridDuLieu, "CreditAmount", false, constDatabaseFormat: 1);
            this.WindowState = FormWindowState.Maximized;
            if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "IsMinimized").Data == "1")
            {
                this.WindowState = FormWindowState.Normal;


                this.Width = Screen.PrimaryScreen.WorkingArea.Width - 200;
                this.Height = Screen.PrimaryScreen.WorkingArea.Height - 50;
                this.Location = new System.Drawing.Point(((Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2), ((Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2));

            }
            if (Utils.isDemo)
            {
                ultraButton2.Visible = false;
            }
            WaitingFrm.StopWaiting();
        }

        private void ultraButton2_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog
            {
                Title = "Chọn nơi sao lưu ",
                Filter = "File (*.xls)|*.xls",
                InitialDirectory = "@C:\\",
                FileName = "CongNoPhaiThuTheoNhomKhachHang.xls"
            };
            if (save.ShowDialog() == DialogResult.OK)
            {
                string duongdan = save.FileName;
                try
                {
                    ultraGridExcelExporter1.Export(uGridDuLieu, duongdan);
                    if (MSG.Question("Kết xuất thành công, Bạn có muốn mở file vừa kết xuất không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(save.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
                catch
                {
                    MSG.Warning("Lỗi khi kết xuất !");
                }
            }
        }

        private void ultraButton3_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ultraButton1_Click(object sender, EventArgs e)
        {
           
            //ReportProcedureSDS sp = new ReportProcedureSDS();
            

            //List<SA_GetReceivableSummaryByGroup> data = sp.GetTongCongNoPhaiThuTheoNhomKH((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, currencyID, acount, list_acount, list_MA_NHOMKH);
            //if (data.Count == 0)
            //    MSG.Warning("Không có dữ liệu");
            //else
            //{
                var rD = new SA_GetReceivableSummaryByGroup_period();
                rD.Period = tieude1;
                var f = new ReportForm<SA_GetReceivableSummaryByGroup>(fileReportSlot1, "");
                f.AddDatasource("DBTHNOPHAITHUGROUP", data1, true);
                f.AddDatasource("Detail", rD);
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            //}
        }

        private void ultraGridExcelExporter1_InitializeColumn(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.InitializeColumnEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Column.Format))
            {
                e.ExcelFormatStr = e.Column.Format;

            }
        }
        private void SetDoSoAm(UltraGrid ultraGrid)
        {
            foreach (var item in ultraGrid.Rows)
            {
                if ((int)item.Cells["Ordercode"].Value == -1)
                {
                    
                    item.Cells["AccountingObjectName"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    item.Cells["OpenDebitAmount"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    item.Cells["OpenCreditAmount"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    item.Cells["DebitAmount"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    item.Cells["CreditAmount"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    item.Cells["CloseDebitAmount"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    item.Cells["CloseCreditAmount"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    item.Cells["OpenDebitAmountOC"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    item.Cells["OpenCreditAmountOC"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    item.Cells["DebitAmountOC"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    item.Cells["CreditAmountOC"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    item.Cells["CloseDebitAmountOC"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    item.Cells["CloseCreditAmountOC"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    item.Cells["AccountingObjectName"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    



                }
                //else
                //{
                //    if ((int)item.Cells["Ordercode"].Value == 0)
                //    {
                //        item.Cells["NGAY_HD"].Hidden = false;
                //        item.Cells["SO_HD"].Hidden = false;
                //        item.Cells["VATRATENAME"].Hidden = true;
                //        item.Cells["TKTHUE"].Hidden = false;
                //        item.Cells["THUE_GTGT"].Hidden = false;
                //        item.Cells["DOANH_SO"].Hidden = false;
                //        //item.Cells["GT_CHUATHUE"].Hidden = false;
                //        item.Cells["MAT_HANG"].Hidden = false;
                //        item.Cells["MST"].Hidden = false;
                //        item.Cells["NGUOI_MUA"].Hidden = false;
                //    }
                //    //if ((int)item.Cells["Ordercode"].Value == 1)
                //    //{
                //    //    item.Cells["NGAY_HD"].Hidden = true;
                //    //    item.Cells["SO_HD"].Hidden = true;
                //    //    item.Cells["VATRATENAME"].Hidden = false;
                //    //    item.Cells["TKTHUE"].Hidden = true;
                //    //    item.Cells["THUE_GTGT"].Hidden = false;
                //    //    item.Cells["DOANH_SO"].Hidden = false;
                //    //    //item.Cells["GT_CHUATHUE"].Hidden = false;
                //    //    item.Cells["VATRATENAME"].Appearance.FontData.Bold = DefaultableBoolean.True;
                //    //    item.Cells["THUE_GTGT"].Appearance.FontData.Bold = DefaultableBoolean.True;
                //    //    item.Cells["DOANH_SO"].Appearance.FontData.Bold = DefaultableBoolean.True;
                //    //    item.Cells["MAT_HANG"].Hidden = true;
                //    //    item.Cells["MST"].Hidden = true;
                //    //    item.Cells["NGUOI_MUA"].Hidden = true;
                //    //}
                //}
                if (currencyID1 == "VND")
                {
                    if ((decimal)item.Cells["OpenDebitAmount"].Value < 0)
                        item.Cells["OpenDebitAmount"].Appearance.ForeColor = Color.Red;


                    if ((decimal)item.Cells["OpenCreditAmount"].Value < 0)
                        item.Cells["OpenCreditAmount"].Appearance.ForeColor = Color.Red;
                    if ((decimal)item.Cells["DebitAmount"].Value < 0)
                        item.Cells["DebitAmount"].Appearance.ForeColor = Color.Red;


                    if ((decimal)item.Cells["CreditAmount"].Value < 0)
                        item.Cells["CreditAmount"].Appearance.ForeColor = Color.Red;
                    if ((decimal)item.Cells["CloseDebitAmount"].Value < 0)
                        item.Cells["CloseDebitAmount"].Appearance.ForeColor = Color.Red;


                    if ((decimal)item.Cells["CloseCreditAmount"].Value < 0)
                        item.Cells["CloseCreditAmount"].Appearance.ForeColor = Color.Red;
                }
                else
                {
                    if ((decimal)item.Cells["OpenDebitAmountOC"].Value < 0)
                        item.Cells["OpenDebitAmountOC"].Appearance.ForeColor = Color.Red;


                    if ((decimal)item.Cells["OpenCreditAmountOC"].Value < 0)
                        item.Cells["OpenCreditAmountOC"].Appearance.ForeColor = Color.Red;
                    if ((decimal)item.Cells["DebitAmountOC"].Value < 0)
                        item.Cells["DebitAmountOC"].Appearance.ForeColor = Color.Red;


                    if ((decimal)item.Cells["CreditAmountOC"].Value < 0)
                        item.Cells["CreditAmountOC"].Appearance.ForeColor = Color.Red;
                    if ((decimal)item.Cells["CloseDebitAmountOC"].Value < 0)
                        item.Cells["CloseDebitAmountOC"].Appearance.ForeColor = Color.Red;


                    if ((decimal)item.Cells["CloseCreditAmountOC"].Value < 0)
                        item.Cells["CloseCreditAmountOC"].Appearance.ForeColor = Color.Red;
                }
               

            }
        }

        private void reportManager1_GetReportParameter(object sender, PerpetuumSoft.Reporting.Components.GetReportParameterEventArgs e)
        {
            string typeCurrency = currencyID1 != null ? currencyID1 : string.Empty;
            e.Parameters["CurrencyID"].Value = typeCurrency;
        }

        private void uGridDuLieu_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            UltraGridCell cell = e.Row.Cells[0];
            cell.Row.Height = 25;
        }

        private void ultraGridExcelExporter1_SummaryCellExported(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.SummaryCellExportedEventArgs e)
        {
            Worksheet ws = e.CurrentWorksheet;
            WorksheetRow wsRow = ws.Rows[e.CurrentRowIndex];
            WorksheetCell wsCell = wsRow.Cells[e.CurrentColumnIndex];
            if (!e.Summary.SummaryText.Contains("Số dòng") && !e.Summary.SummaryText.Contains(" "))
            {
                string s = e.Summary.SummaryText.Trim('.');
                if (s.Contains("("))
                    wsCell.Value = -decimal.Parse(s.Trim('(').Trim(')'));
                else
                    wsCell.Value = decimal.Parse(s);

                wsCell.CellFormat.FormatString = "###,###,###,###,##0;(###,###,###,###,##0)";
            }
        }

        private void ultraGridExcelExporter1_BeginExport(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.BeginExportEventArgs e)
        {
            foreach (var row in uGridDuLieu.Rows)
            {
                foreach (var column in uGridDuLieu.DisplayLayout.Bands[0].Columns)
                {
                    e.Rows[row.Index].Cells[column].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                    e.Rows[row.Index].Cells[column].Appearance.BorderColor = Color.Black;
                    e.Rows[row.Index].Cells[column].Column.Width = 200;
                }
            }
            e.Rows.Band.Summaries[0].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[0].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[1].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[1].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[2].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[2].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[3].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[3].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[4].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[4].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[5].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[5].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[6].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[6].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[7].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[7].Appearance.BorderColor = Color.Black;
        }
    }
}
