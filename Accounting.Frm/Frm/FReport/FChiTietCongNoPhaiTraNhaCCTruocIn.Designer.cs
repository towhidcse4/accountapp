﻿namespace Accounting.Frm.FReport
{
    partial class FChiTietCongNoPhaiTraNhaCCTruocIn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance("AccountObjectName");
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn1 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Ngay_HT", 0);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn2 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ngayCtu", 1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn3 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SoCtu", 2);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn4 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DienGiai", 3);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn5 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TK_CONGNO", 4);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn6 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("TkDoiUng", 5);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn7 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("AccountObjectName", 6);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn8 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("", 7);
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn9 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("", 8);
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn10 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("", 9);
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn11 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("", 10);
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance("AccountObjectName");
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance("AccountObjectName");
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FChiTietCongNoPhaiTraNhaCCTruocIn));
            this.ugridDuLieu = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraButton1 = new Infragistics.Win.Misc.UltraButton();
            this.reportManager1 = new PerpetuumSoft.Reporting.Components.ReportManager(this.components);
            this.fileReportSlot1 = new PerpetuumSoft.Reporting.Components.FileReportSlot(this.components);
            this.ultraGridExcelExporter1 = new Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter(this.components);
            this.ultraButton2 = new Infragistics.Win.Misc.UltraButton();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraButton3 = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.ugridDuLieu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // ugridDuLieu
            // 
            appearance1.StyleResourceName = "AccountObjectName";
            this.ugridDuLieu.DisplayLayout.AddNewBox.Appearance = appearance1;
            this.ugridDuLieu.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            ultraGridColumn1.DataType = typeof(System.DateTime);
            ultraGridColumn1.Format = "dd/MM/yyyy";
            ultraGridColumn1.Header.Caption = "trung";
            ultraGridColumn1.Header.VisiblePosition = 0;
            ultraGridColumn1.MaskInput = "dd/MM/yyyy";
            ultraGridColumn1.Width = 73;
            ultraGridColumn2.DataType = typeof(System.DateTime);
            ultraGridColumn2.Format = "dd/MM/yyyy";
            ultraGridColumn2.Header.VisiblePosition = 1;
            ultraGridColumn2.MaskInput = "dd/MM/yyyy";
            ultraGridColumn2.Width = 150;
            ultraGridColumn3.Header.VisiblePosition = 2;
            ultraGridColumn3.Width = 117;
            ultraGridColumn4.Header.VisiblePosition = 4;
            ultraGridColumn4.Width = 117;
            ultraGridColumn5.Header.VisiblePosition = 5;
            ultraGridColumn5.Width = 117;
            ultraGridColumn6.Header.VisiblePosition = 6;
            ultraGridColumn6.Width = 117;
            ultraGridColumn7.Header.VisiblePosition = 3;
            ultraGridColumn7.Width = 141;
            appearance2.TextHAlignAsString = "Right";
            ultraGridColumn8.CellAppearance = appearance2;
            ultraGridColumn8.Header.VisiblePosition = 7;
            ultraGridColumn8.Width = 117;
            appearance3.TextHAlignAsString = "Right";
            ultraGridColumn9.CellAppearance = appearance3;
            ultraGridColumn9.Header.VisiblePosition = 8;
            ultraGridColumn9.Width = 117;
            appearance4.TextHAlignAsString = "Right";
            ultraGridColumn10.CellAppearance = appearance4;
            ultraGridColumn10.Header.VisiblePosition = 9;
            ultraGridColumn10.Width = 117;
            appearance5.TextHAlignAsString = "Right";
            ultraGridColumn11.CellAppearance = appearance5;
            ultraGridColumn11.Header.VisiblePosition = 10;
            ultraGridColumn11.PadChar = 'u';
            ultraGridColumn11.PromptChar = 'u';
            ultraGridColumn11.Width = 117;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn1,
            ultraGridColumn2,
            ultraGridColumn3,
            ultraGridColumn4,
            ultraGridColumn5,
            ultraGridColumn6,
            ultraGridColumn7,
            ultraGridColumn8,
            ultraGridColumn9,
            ultraGridColumn10,
            ultraGridColumn11});
            this.ugridDuLieu.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            appearance6.StyleResourceName = "AccountObjectName";
            this.ugridDuLieu.DisplayLayout.GroupByBox.Appearance = appearance6;
            this.ugridDuLieu.DisplayLayout.GroupByBox.Hidden = true;
            appearance7.StyleResourceName = "AccountObjectName";
            this.ugridDuLieu.DisplayLayout.Override.GroupByColumnAppearance = appearance7;
            this.ugridDuLieu.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ugridDuLieu.DisplayLayout.Override.SummaryDisplayArea = ((Infragistics.Win.UltraWinGrid.SummaryDisplayAreas)((((((Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.Top | Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.TopFixed) 
            | Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.Bottom) 
            | Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.BottomFixed) 
            | Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.InGroupByRows) 
            | Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.HideDataRowFooters)));
            this.ugridDuLieu.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.ugridDuLieu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ugridDuLieu.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ugridDuLieu.Location = new System.Drawing.Point(3, 0);
            this.ugridDuLieu.Name = "ugridDuLieu";
            this.ugridDuLieu.Size = new System.Drawing.Size(1321, 485);
            this.ugridDuLieu.TabIndex = 43;
            this.ugridDuLieu.Text = "Dữ liệu";
            this.ugridDuLieu.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.ugridDuLieu_InitializeLayout);
            this.ugridDuLieu.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.ugridDuLieu_InitializeRow);
            this.ugridDuLieu.InitializeGroupByRow += new Infragistics.Win.UltraWinGrid.InitializeGroupByRowEventHandler(this.ugridDuLieu_InitializeGroupByRow);
            this.ugridDuLieu.SummaryValueChanged += new Infragistics.Win.UltraWinGrid.SummaryValueChangedEventHandler(this.ugridDuLieu_SummaryValueChanged);
            this.ugridDuLieu.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.ugridDuLieu_DoubleClickRow);
            // 
            // ultraButton1
            // 
            this.ultraButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance8.Image = global::Accounting.Properties.Resources.ubtnPrint;
            this.ultraButton1.Appearance = appearance8;
            this.ultraButton1.Location = new System.Drawing.Point(1096, 21);
            this.ultraButton1.Name = "ultraButton1";
            this.ultraButton1.Size = new System.Drawing.Size(90, 26);
            this.ultraButton1.TabIndex = 44;
            this.ultraButton1.Text = "In báo cáo";
            this.ultraButton1.Click += new System.EventHandler(this.ultraButton1_Click);
            // 
            // reportManager1
            // 
            this.reportManager1.DataSources = new PerpetuumSoft.Reporting.Components.ObjectPointerCollection(new string[0], new object[0]);
            this.reportManager1.OwnerForm = this;
            this.reportManager1.Reports.AddRange(new PerpetuumSoft.Reporting.Components.ReportSlot[] {
            this.fileReportSlot1});
            this.reportManager1.GetReportParameter += new PerpetuumSoft.Reporting.Components.GetReportParameterEventHandler(this.reportManager1_GetReportParameter);
            // 
            // fileReportSlot1
            // 
            this.fileReportSlot1.FilePath = "";
            this.fileReportSlot1.ReportName = "";
            this.fileReportSlot1.ReportScriptType = typeof(PerpetuumSoft.Reporting.Rendering.ReportScriptBase);
            // 
            // ultraGridExcelExporter1
            // 
            this.ultraGridExcelExporter1.BeginExport += new Infragistics.Win.UltraWinGrid.ExcelExport.BeginExportEventHandler(this.ultraGridExcelExporter1_BeginExport);
            this.ultraGridExcelExporter1.SummaryCellExported += new Infragistics.Win.UltraWinGrid.ExcelExport.SummaryCellExportedEventHandler(this.ultraGridExcelExporter1_SummaryCellExported);
            this.ultraGridExcelExporter1.InitializeColumn += new Infragistics.Win.UltraWinGrid.ExcelExport.InitializeColumnEventHandler(this.ultraGridExcelExporter1_InitializeColumn);
            // 
            // ultraButton2
            // 
            this.ultraButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance10.Image = global::Accounting.Properties.Resources.Excel;
            this.ultraButton2.Appearance = appearance10;
            this.ultraButton2.Location = new System.Drawing.Point(979, 21);
            this.ultraButton2.Name = "ultraButton2";
            this.ultraButton2.Size = new System.Drawing.Size(93, 26);
            this.ultraButton2.TabIndex = 45;
            this.ultraButton2.Text = "Xuất excel";
            this.ultraButton2.Click += new System.EventHandler(this.ultraButton2_Click);
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.ugridDuLieu);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(1327, 488);
            this.ultraGroupBox1.TabIndex = 46;
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.ultraButton3);
            this.ultraGroupBox2.Controls.Add(this.ultraButton2);
            this.ultraGroupBox2.Controls.Add(this.ultraButton1);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraGroupBox2.Location = new System.Drawing.Point(0, 488);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(1327, 65);
            this.ultraGroupBox2.TabIndex = 47;
            // 
            // ultraButton3
            // 
            this.ultraButton3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance9.Image = global::Accounting.Properties.Resources.cancel_16;
            this.ultraButton3.Appearance = appearance9;
            this.ultraButton3.Location = new System.Drawing.Point(1213, 21);
            this.ultraButton3.Name = "ultraButton3";
            this.ultraButton3.Size = new System.Drawing.Size(90, 26);
            this.ultraButton3.TabIndex = 55;
            this.ultraButton3.Text = "Huỷ bỏ";
            this.ultraButton3.Click += new System.EventHandler(this.ultraButton3_Click);
            // 
            // FChiTietCongNoPhaiTraNhaCCTruocIn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1327, 553);
            this.Controls.Add(this.ultraGroupBox1);
            this.Controls.Add(this.ultraGroupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FChiTietCongNoPhaiTraNhaCCTruocIn";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sổ chi tiết công nợ phải trả nhà cung cấp";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FChiTietCongNoPhaiTraNhaCCTruocIn_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.ugridDuLieu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinGrid.UltraGrid ugridDuLieu;
        private Infragistics.Win.Misc.UltraButton ultraButton1;
        private PerpetuumSoft.Reporting.Components.FileReportSlot fileReportSlot1;
        private PerpetuumSoft.Reporting.Components.ReportManager reportManager1;
        private Infragistics.Win.Misc.UltraButton ultraButton2;
        private Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter ultraGridExcelExporter1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.Misc.UltraButton ultraButton3;
    }
}