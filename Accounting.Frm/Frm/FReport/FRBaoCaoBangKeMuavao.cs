﻿using Accounting.Core.DAO;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.Report;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRBaoCaoBangKeMuavao : CustormForm
    {
        private string _subSystemCode;
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        public FRBaoCaoBangKeMuavao()
        {
            InitializeComponent();
            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\BangKeHoaDonThueMuaBan.rst", path);
            ReportUtils.ProcessControls(this);
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dtBeginDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }

            if ((DateTime)dtBeginDate.Value > (DateTime)dtEndDate.Value)
            {
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            ReportProcedureSDS sp = new ReportProcedureSDS();
            List<BangKeMuaVao_Model> data = new List<BangKeMuaVao_Model>();
            int check = cbb_CongGop.Checked == true ? 1 : 0;
            data = sp.GetProc_GetBkeInv_Buy((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, check, 0, "");
            data.ForEach(t =>
            {
                t.THUE_SUAT_DETAIL = Convert.ToInt32(t.THUE_SUAT);
                if (t.FLAG == -1)
                {
                    t.GT_CHUATHUE = -t.GT_CHUATHUE;
                    t.THUE_GTGT = -t.THUE_GTGT;
                }
            });
            if (data.Count == 0)
            {
                MSG.Warning("Không có dữ liệu để lên báo cáo!");
            }
            else
            {
                List<BangKeMuaVao_Model> listGroup = (from a in data
                                                      group a by a.GOODSSERVICEPURCHASECODE into g
                                                      select new BangKeMuaVao_Model
                                                      {
                                                          GOODSSERVICEPURCHASECODE = g.Key,
                                                          GT_CHUATHUE = g.Sum(t => t.GT_CHUATHUE),
                                                          THUE_GTGT = g.Sum(t => t.THUE_GTGT)
                                                      }).ToList();
                var rD = new BangKeMuaVao_ModelDetail();
                rD.Period = ReportUtils.GetPeriod((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value);
                var f = new ReportForm<BangKeMuaVao_Model>(fileReportSlot1, _subSystemCode);
                f.AddDatasource("DBBANGKEMUAVAO", data.OrderBy(n => n.NGAY_HD).ThenBy(n => n.SO_HD).ToList());// set định dạng ngày đd/mm/yyyy
                f.AddDatasource("DBGROUPCODE", listGroup);
                f.AddDatasource("Detail", rD);
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ultraButton1_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }

            if ((DateTime)dtBeginDate.Value > (DateTime)dtEndDate.Value)
            {
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            ReportProcedureSDS sp = new ReportProcedureSDS();
            List<BangKeMuaVao_Model> data = new List<BangKeMuaVao_Model>();
            int check = cbb_CongGop.Checked == true ? 1 : 0;
            data = sp.GetProc_GetBkeInv_Buy((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, check, 0, "");
            data.ForEach(t =>
            {
                t.THUE_SUAT_DETAIL = Convert.ToInt32(t.THUE_SUAT);
                if (t.FLAG == -1)
                {
                    t.GT_CHUATHUE = -t.GT_CHUATHUE;
                    t.THUE_GTGT = -t.THUE_GTGT;
                }
            });
            if (data.Count == 0)
            {
                MSG.Warning("Không có dữ liệu để lên báo cáo!");
            }
            else
            {
                List<BangKeMuaVao_Model> listGroup = (from a in data
                                                      group a by a.GOODSSERVICEPURCHASECODE into g
                                                      select new BangKeMuaVao_Model
                                                      {
                                                          GOODSSERVICEPURCHASECODE = g.Key,
                                                          GT_CHUATHUE = g.Sum(t => t.GT_CHUATHUE),
                                                          THUE_GTGT = g.Sum(t => t.THUE_GTGT)
                                                      }).ToList();
                var rD = new BangKeMuaVao_ModelDetail();
                rD.Period = ReportUtils.GetPeriod((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value);
                var f = new FRBaoCaoBangKeMuavaoTruocIn(data, rD.Period,_subSystemCode, check, (DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value);
               // f.AddDatasource("DBBANGKEMUAVAO",
                 data=   data.OrderBy(n => n.NGAY_HD).ThenBy(n => n.SO_HD).ToList();
                //f.AddDatasource("DBGROUPCODE", listGroup);
                //f.AddDatasource("Detail", rD);
                //f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
        }
    }
}
