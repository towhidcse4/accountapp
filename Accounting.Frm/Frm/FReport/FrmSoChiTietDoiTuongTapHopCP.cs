﻿using Accounting.Core.DAO;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.Report;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinTree;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Accounting.Frm.FReport
{
    public partial class FrmSoChiTietDoiTuongTapHopCP : Form
    {
        private readonly List<AccountTongHopCP> _lstnew2 = ReportUtils.LstAccount;
        private readonly List<CostSetTongHopCP> _lstnew = ReportUtils.LstCostSetTongHopCP;
        private List<AccountTongHopCP> _lstAccountReport = new List<AccountTongHopCP>();
        private List<CostSetTongHopCP> _lstCostSetCP = new List<CostSetTongHopCP>();
        private ICostSetService _ICostSetService
        {
            get { return IoC.Resolve<ICostSetService>(); }
        }
        private string _subSystemCode;
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        public FrmSoChiTietDoiTuongTapHopCP()
        {
            InitializeComponent();
            string path = System.IO.Path.GetDirectoryName(
                 System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\SoChiTietTheoDoiTuongTHCP.rst", path);
            //Gridivew vật tư
            _lstAccountReport = _lstnew2;
            ugridAccountCP.SetDataBinding(_lstAccountReport.OrderBy(n => n.AccountNumber).ToList(), "");
    
            _lstCostSetCP = _lstnew;
            ugridCostSetCP.SetDataBinding(_lstCostSetCP.OrderBy(n => n.costSetCode).ToList(), "");
            ReportUtils.ProcessControls(this);
            foreach (var row in ugridCostSetCP.Rows)
            {
                row.Cells["Check"].Value = false;
            }
            foreach (var row in ugridAccountCP.Rows)
            {
                row.Cells["Check"].Value = false;
            }


            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dtBeginDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }

        }





        private void btnOk_Click_1(object sender, EventArgs e)
        {
            Close();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }

            if (dtBeginDate.DateTime.Date > dtEndDate.DateTime.Date)
            {
                //MessageBox.Show("Ngày bắt đầu không thể lớn hơn ngày kết thúc", "Cảnh báo");
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            var lstAccount = _lstnew2.Where(x => x.Check).ToList();
            var lstCostSet = _lstnew.Where(x => x.Check).ToList();
            if (lstAccount.Count == 0)
            {
                MSG.Warning("Bạn chưa chọn tài khoản!");
                return;
            }
            if (lstCostSet.Count == 0)
            {
                MSG.Warning("Bạn chưa chọn Mã đối tượng THCP!");
                return;
            }
          

            ReportProcedureSDS sp = new ReportProcedureSDS();
            string list_item = "";

            try
            {
                var dsitemChoose = lstCostSet != null ? lstCostSet.Select(c => c.iD.ToString()) : new List<string>();
                list_item = string.Join(",", dsitemChoose.ToArray());
                list_item = "," + list_item + ",";
            }
            catch (Exception ex)
            {
                list_item = "";
            }
            string list_item2 = "";

            try
            {
                var dsitemChoose = lstAccount != null ? lstAccount.Select(c => c.AccountNumber.ToString()) : new List<string>();
                list_item2 = string.Join(",", dsitemChoose.ToArray());
                list_item2 = "," + list_item2 + ",";
            }
            catch (Exception ex)
            {
                list_item = "";
            }
            List<CostSetBook> data = sp.GETCCHITIETDOITUONGTHCP((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, list_item,list_item2);
            if (data.Count == 0)
                MSG.Warning("Không có dữ liệu");
            else
            {
                var rD = new CostSetBook_Period();
                rD.Period = ReportUtils.GetPeriod((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value);
                var f = new ReportForm<CostSetBook>(fileReportSlot1, _subSystemCode);
                f.AddDatasource("THCP", data, true);
                f.AddDatasource("Detail", rD);
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
        }
    }
}
