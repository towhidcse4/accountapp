﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Accounting.Core.DAO;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.Report;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;

namespace Accounting.Frm.FReport
{
    public partial class FRSoTheoDoiCongNoPhaiThuTheoMatHang : Form
    {
        List<MaterialGoodsReport> _lstMaterialGoodsReport = new List<MaterialGoodsReport>();
        private readonly List<MaterialGoodsReport> _lstNew = ReportUtils.LstMaterialGoodsReport;
        private IMaterialGoodsService _IMaterialGoodsService;
        private string _strSubSystemCode;
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        public FRSoTheoDoiCongNoPhaiThuTheoMatHang()
        {
            InitializeComponent();
            _strSubSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileRSlot_TheoDoiCongNoPhaiThu.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\SoTheoDoiCongNoPhaiThuTheoMH.rst", path);

            _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
            foreach (var item in _lstNew)
            {
                item.Check = false;
            }
            _lstMaterialGoodsReport = _lstNew;

            ugridMaterialGoodsCategory.SetDataBinding(_lstMaterialGoodsReport.ToList(), "");
            ReportUtils.ProcessControls(this);
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dtBeginDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }

            if ((DateTime)dtBeginDate.Value > (DateTime)dtEndDate.Value)
            {
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            ReportProcedureSDS sp = new ReportProcedureSDS();

            string list_item = "";

            try
            {
                var dsitemChoose = list_item != null ? _lstMaterialGoodsReport.Where(c => c.Check).Select(c => c.ID.ToString()) : new List<string>();
                list_item = string.Join(",", dsitemChoose.ToArray());
                list_item = "," + list_item + ",";
            }
            catch (Exception ex)
            {
                list_item = "";
            }
            FSOTHEODOICONGNOPHAITHUTHEOMATHANGex fm = new FSOTHEODOICONGNOPHAITHUTHEOMATHANGex((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, list_item);
            fm.Show(this);
            
            
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            foreach (var item in ugridMaterialGoodsCategory.Rows)
            {
                item.Cells["Check"].Value = false;
            }
            Close();
        }
    }
}
