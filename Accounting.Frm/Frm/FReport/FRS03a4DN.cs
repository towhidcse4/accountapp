﻿using System;
using System.Collections.Generic;
using System.Linq;
using FX.Core;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;

namespace Accounting
{
    public partial class FRS03a4DNN : CustormForm
    {
        string _subSystemCode;
        public FRS03a4DNN(string subSystemCode)
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();
            _subSystemCode = subSystemCode;
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\S03a4-DNN.rst", path);
            ReportUtils.ProcessControls(this);
            btnExit.Click += btnExit_Click;
            btnOk.Click += btnOk_Click;
            WaitingFrm.StopWaiting();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }
            var saInvoiceSrv = IoC.Resolve<ISAInvoiceService>();
            List<S03A4DNN> data = new List<S03A4DNN>();
            //for (int i = 0; i < 200; i++)
            //{
            //    data.Add(new S03A4DNN(i));
            //}
            data = saInvoiceSrv.ReportS03A4Dns((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value);
            var rD = new S03A4DNNDetail();
            rD.Period = ReportUtils.GetPeriod(DateTime.Now, DateTime.UtcNow);
            var f = new ReportForm<S03A4DNN>(fileReportSlot1, _subSystemCode);
            f.AddDatasource("S03a4DNN", data, true);
            f.AddDatasource("Detail", rD);
            f.LoadReport();
            f.WindowState = FormWindowState.Maximized;
            f.Show();
        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
