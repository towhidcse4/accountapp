﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;

namespace Accounting
{
    public partial class FRBankCompare : CustormForm
    {
        readonly List<BankAccountDetailReport> _listBankAcc = new List<BankAccountDetailReport>();
        string _subSystemCode;
        public FRBankCompare(string subSystemCode)
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();
            _subSystemCode = subSystemCode;
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\BankCompare.rst", path);
            ReportUtils.LstBankAccountDetailReport.Clear();
            _listBankAcc = ReportUtils.LstBankAccountDetailReport;
            if (_listBankAcc.Count > 0)
            {
                int i = 1;
                foreach (var bankAccountDetailReport in _listBankAcc)
                {
                    bankAccountDetailReport.Order += i;
                    i++;
                }
            }
            ugridBank.SetDataBinding(_listBankAcc.ToList(), "");
            ReportUtils.ProcessControls(this);
            btnExit.Click += btnExit_Click;
            btnOk.Click += btnOk_Click;
            WaitingFrm.StopWaiting();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }
            var generalLedgerService = IoC.Resolve<IGeneralLedgerService>();
            List<Guid> lstGuidBankAcc = _listBankAcc.Where(c => c.Check).Select(c => c.ID).ToList();
            if (lstGuidBankAcc.Count == 0)
            {
                MSG.Warning(resSystem.Report_05);
                return;
            }
            List<BankCompare> data1 = generalLedgerService.RBankCompare((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, ((Currency)cbbCurrency.SelectedRow.ListObject).ID, lstGuidBankAcc);
            var rD = new BankCompareDetail();
            rD.Period = ReportUtils.GetPeriod(DateTime.Now, DateTime.Now.AddMonths(30));
            var f = new ReportForm<BankCompare>(fileReportSlot1, _subSystemCode);
            f.AddDatasource("BankCompare", data1, true);
            f.AddDatasource("Detail", rD);
            f.LoadReport();
            f.WindowState = FormWindowState.Maximized;
            f.Show();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
