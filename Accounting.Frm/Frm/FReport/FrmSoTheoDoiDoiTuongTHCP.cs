﻿using Accounting.Core.DAO;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.Report;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinTree;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FrmSoTheoDoiDoiTuongTHCP : Form
    {
        private readonly List<CostSetTongHopCP> _lstnew = ReportUtils.LstCostSetTongHopCP;
        private readonly List<ExpenseItemTongHopCP> _lstnew2 = ReportUtils.LstExpenseCP;
        private List<CostSetTongHopCP> _lstCostSetCP = new List<CostSetTongHopCP>();
        private List<ExpenseItemTongHopCP> dsExpenseItem = new List<ExpenseItemTongHopCP>();
        private IExpenseItemService _IExpenseItemService
        {
            get { return IoC.Resolve<IExpenseItemService>(); }
        }
        string _subSystemCode;
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        public FrmSoTheoDoiDoiTuongTHCP()
        {
            InitializeComponent();
            this.uTreeCP.InitializeDataNode += new Infragistics.Win.UltraWinTree.InitializeDataNodeEventHandler(this.uTree_InitializeDataNode);
            this.uTreeCP.AfterDataNodesCollectionPopulated += new Infragistics.Win.UltraWinTree.AfterDataNodesCollectionPopulatedEventHandler(Utils.uTree_AfterDataNodesCollectionPopulated);
            this.uTreeCP.ColumnSetGenerated += new Infragistics.Win.UltraWinTree.ColumnSetGeneratedEventHandler(this.uTree_ColumnSetGenerated);
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\SoTheoDoiDoiTuongTHCP.rst", path);
            //Gridivew mã đối tượng
            _lstCostSetCP = _lstnew;
            ugridCostSetCP.SetDataBinding(_lstCostSetCP.OrderBy(n => n.costSetCode).ToList(), "");
            ReportUtils.ProcessControls(this);
            //GridTree KMCP
            dsExpenseItem = _lstnew2;
            DataSet ds = Utils.ToDataSet<ExpenseItemTongHopCP>(dsExpenseItem, ConstDatabase.ExpenseItemTongHopCP_TableName);
            uTreeCP.SetDataBinding(ds, ConstDatabase.ExpenseItemTongHopCP_TableName);
            ConfigTree(uTreeCP);
            foreach (var node in uTreeCP.Nodes)
            {
                node.Cells["Check"].Value = false;
            }
            foreach (var row in ugridCostSetCP.Rows)
            {
                row.Cells["Check"].Value = false;
            }
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dtBeginDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }

        }
        private void uTree_ColumnSetGenerated(object sender, ColumnSetGeneratedEventArgs e)
        {
            Utils.uTree_ColumnSetGenerated(sender, e, ConstDatabase.ExpenseItemTongHopCP_TableName);
        }
        private void uTree_InitializeDataNode(object sender, InitializeDataNodeEventArgs e)
        {
            Utils.uTree_InitializeDataNode(sender, e, ConstDatabase.ExpenseItemTongHopCP_TableName);
        }
        void ConfigTree(Infragistics.Win.UltraWinTree.UltraTree ultraTree)
        {//hàm chung
            Utils.ConfigTree(ultraTree, TextMessage.ConstDatabase.ExpenseItemTongHopCP_TableName);
        }
        //su kien uTreeCP_Click
        private void uTreeCP_Click(object sender, EventArgs e)
        {
            if (uTreeCP.SelectedNodes.Count == 0 && uTreeCP.ActiveNode != null)
            {
                uTreeCP.ActiveNode.Selected = true;
            }
            if (uTreeCP.SelectedNodes.Count > 0)
            {
                if ((bool)uTreeCP.SelectedNodes[0].Cells["Check"].Value == false)
                {
                    uTreeCP.SelectedNodes[0].Cells["Check"].Value = true;
                }
                else
                {
                    uTreeCP.SelectedNodes[0].Cells["Check"].Value = false;
                }
                var id = (Guid)uTreeCP.SelectedNodes[0].Cells["ID"].Value;
                _lstnew2.FirstOrDefault(x => x.ID == id).Check = (bool)uTreeCP.SelectedNodes[0].Cells["Check"].Value;
                var lstID = new List<Guid>();
                getListID(lstID, id);
                foreach (var node in uTreeCP.Nodes)
                {
                    if (lstID.Any(d => d == (Guid)node.Cells["ID"].Value))
                    {
                        _lstnew2.FirstOrDefault(x => x.ID == (Guid)node.Cells["ID"].Value).Check = (bool)uTreeCP.SelectedNodes[0].Cells["Check"].Value;
                        node.Cells["Check"].Value = uTreeCP.SelectedNodes[0].Cells["Check"].Value;
                    }
                }
            }
        }
        void getListID(List<Guid> lstID, Guid parent)
        {
            foreach (var x in _lstnew2)
            {
                if (x.ParentID == parent)
                {
                    lstID.Add(x.ID);
                    if (x.IsParentNode) getListID(lstID, x.ID);
                }
            }
        }
        //su kien click btnOk
        private void btnExit_Click_1(object sender, EventArgs e)
        {

            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }

            if (dtBeginDate.DateTime.Date > dtEndDate.DateTime.Date)
            {
                //MessageBox.Show("Ngày bắt đầu không thể lớn hơn ngày kết thúc", "Cảnh báo");
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            var lstCss = ((List<CostSetTongHopCP>)ugridCostSetCP.DataSource).Where(c => c.Check).Select(x => x.iD.ToString()).ToList();
            if (lstCss.Count == 0)
            {
                MSG.Warning("Bạn chưa chọn mã đối tượng THCP!");
                return;
            }
            var lstEx = _lstnew2.Where(x => x.Check).Select(x => x.ID.ToString()).ToList();
            if (lstEx.Count == 0)
            {
                MSG.Warning("Bạn chưa chọn KMCP!");
                return;
            }
            //ham
            string GetStringByListString(List<string> lstStr)
            {
                var str = "";
                foreach (var x in lstStr)
                {
                    str = str + "," + x;
                }
                return str + ",";
            }
            //add by Mr An        
            ReportProcedureSDS sp = new ReportProcedureSDS();

            List<CostSetExpense> data = sp.GeTDOITUONGTHCPTheoKMCP((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, GetStringByListString(lstCss), GetStringByListString(lstEx));
            if (data.Count == 0)
            {
                MSG.Warning("Không có dữ liệu để lên báo cáo");
            }
            else
            {
                var rD = new CostSetExpense_Period();
                rD.Period = ReportUtils.GetPeriod((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value);
                var f = new ReportForm<CostSetExpense>(fileReportSlot1, _subSystemCode);
                f.AddDatasource("KMCP", data,true);
                f.AddDatasource("Detail", rD);
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }

        }
        //su kien click btnExit
        private void btnExit_Click_2(object sender, EventArgs e)
        {
            Close();
        }
    }
}
