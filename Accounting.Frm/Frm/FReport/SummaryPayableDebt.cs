﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;

namespace Accounting
{
    public partial class SummaryPayableDebt : Form
    {
        private readonly ICurrencyService _currencySrv;
        private readonly List<Item> _lstItems = Utils.ObjConstValue.SelectTimes;
        public SummaryPayableDebt()
        {
            _currencySrv = IoC.Resolve<ICurrencyService>();
            InitializeComponent();
            dtBeginDate.DateTime = dtEndDate.DateTime = DateTime.Now;
            // load dữ liệu cho Combobox Chọn thời gian
            cbbDateTime.DataSource = _lstItems;
            cbbDateTime.DisplayMember = "Name";
            Utils.ConfigGrid(cbbDateTime, ConstDatabase.ConfigXML_TableName);
            // load dữ liệu cho combobox loại tiền
            cbbCurentcy.DataSource = _currencySrv.GetAll();
            Utils.ConfigGrid(cbbCurentcy, ConstDatabase.Currency_TableName);
        }

        private void cbbDateTime_ValueChanged(object sender, EventArgs e)
        {
            if (cbbDateTime.SelectedRow != null)
            {
                var model = cbbDateTime.SelectedRow.ListObject as Item;
                DateTime dtBegin;
                DateTime dtEnd;
                Utils.GetDateBeginDateEnd(DateTime.Now.Year,DateTime.Now, model, out dtBegin, out dtEnd);
                dtBeginDate.DateTime = dtBegin;
                dtEndDate.DateTime = dtEnd;
            }

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
