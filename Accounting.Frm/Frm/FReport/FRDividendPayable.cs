﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;

namespace Accounting
{
    public partial class FRDividendPayable : CustormForm
    {
        string _subSystemCode;
        public FRDividendPayable(string subSystemCode)
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();
            _subSystemCode = subSystemCode;
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\DividendPayable.rst", path);
            WaitingFrm.StopWaiting();
        }

        private void ultraButton1_Click(object sender, EventArgs e)
        {
            List<DividendPayable> data = new List<DividendPayable>();
            for (int i = 0; i < 200; i++)
            {
                //   data.Add(new DividenPayable(i));
            }
            var rD = new DividendPayableDetail();
            var f = new ReportForm<DividendPayable>(fileReportSlot1, _subSystemCode);
            f.AddDatasource("DividendPayable", data, true);
            f.AddDatasource("Detail", rD);
            f.LoadReport();
            f.WindowState = FormWindowState.Maximized;
            f.Show();
        }
    }
}
