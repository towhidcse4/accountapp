﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;

namespace Accounting
{
    public partial class FRB01DNN : CustormForm
    {
        private string _subSystemCode;
        public FRB01DNN(string subSystemCode)
        {
            InitializeComponent();
            _subSystemCode = subSystemCode;
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\B01-DNN.rst", path);
        }

        private void ultraButton1_Click(object sender, EventArgs e)
        {

            Accounting.Core.IService.ISynthesisReportService synthesisReportService = FX.Core.IoC.Resolve<Accounting.Core.IService.ISynthesisReportService>();
            List<Assets> ddd = synthesisReportService.ReportBalanceSheet(DateTime.Parse("1/1/1753"), DateTime.Now);


            List<B01DNN> dataB01DN = new List<B01DNN>();
            List<B02DNN> dataB02DN = new List<B02DNN>();
            dataB02DN.Add(new B02DNN());
            List<B03DNN> dataB03DN = new List<B03DNN>();
            dataB03DN.Add(new B03DNN());
            dataB01DN = ddd.Select(c => new B01DNN
            {
                ItemCode = c.ItemCode,
                ItemName = c.ItemName,
                Description = c.Description,
                ClosingAmount = c.AmountEnding,
                OpenningAmount = c.AmountBeginning,
                GroupID = c.GroupID,
                GroupName = c.GroupName,
                IsBold = c.IsBold,
                IsItalic = c.IsItalic
            }).ToList();
            dataB02DN = ddd.Select(c => new B02DNN
            {
                ItemCode = c.ItemCode,
                ItemName = c.ItemName,
                Description = c.Description,
                AmountCurrentYear = c.AmountEnding,
                AmountLastYear = c.AmountBeginning,
                IsBold = c.IsBold,
                IsItalic = c.IsItalic
            }).ToList();
            //dataB03DN = ddd.Select(c => new B03DNN
            //{
            //    ItemCode = c.ItemCode,
            //    ItemName = c.ItemName,
            //    Description = c.Description,
            //    AmountCurrentYear = c.AmountEnding,
            //    AmountLastYear = c.AmountBeginning,
            //    IsBold = c.IsBold,
            //    IsItalic = c.IsItalic
            //}).ToList();

            var details = new B01DNNDetail();

            details.Period = ReportUtils.GetPeriod(DateTime.Today.AddDays(DateTime.Now.DayOfYear * -1 + 1), DateTime.Now.AddDays(365 - DateTime.Now.DayOfYear));
            details.Date = DateTime.Now;
            details.ShowB02DN = true;
            //details.ShowB03DN = true;
            var f = new ReportForm<B01DNN>(fileReportSlot1, _subSystemCode);
            f.AddDatasource("B01DNN", dataB01DN, true);
            f.AddDatasource("B02DNN", dataB02DN);
            f.AddDatasource("B03DNN", dataB03DN);
            f.AddDatasource("Detail", details);
            f.LoadReport();
            f.WindowState = FormWindowState.Maximized;
            f.Show();
        }
    }
}
