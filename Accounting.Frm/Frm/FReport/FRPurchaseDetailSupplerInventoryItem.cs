﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FRPurchaseDetailSupplerInventoryItem : CustormForm
    {
        readonly IPPInvoiceService _ppInvoiceService = IoC.Resolve<IPPInvoiceService>();
        private readonly List<AccountingObjectReport> _lstnew = ReportUtils.LstProvider;
        private List<AccountingObjectReport> _lstAccountingObjectReport = new List<AccountingObjectReport>();
        private readonly List<MaterialGoodsReport> _lstMaterialGoodsReports = ReportUtils.LstMaterialGoodsReport;
        string _subSystemCode;
        public FRPurchaseDetailSupplerInventoryItem(string subSystemCode)
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();
            this.Text = "Sổ chi tiết mua hàng";
            _subSystemCode = subSystemCode;
            _lstAccountingObjectReport = _lstnew;
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\PurchaseDetail_Suppler_InventoryItem.rst", path);
            ugridAccountingObject.SetDataBinding(_lstAccountingObjectReport.ToList(), "");
            ugridMaterialGoodsCategory.SetDataBinding(_lstMaterialGoodsReports.ToList(), "");
            ReportUtils.ProcessControls(this);
            btnExit.Click += btnExit_Click;
            btnOk.Click += btnOk_Click;
            cbbAccountingObjectCategoryID.ValueChanged += cbbAccountingObjectCategoryID_ValueChanged;
            cbbAccountingObjectGroupID.ValueChanged += cbbAccountingObjectGroupID_ValueChanged;
            WaitingFrm.StopWaiting();
        }
        #region lọc nhóm nhà cung cấp
        private bool _check1 = false;
        private bool _check2 = false;
        private void cbbAccountingObjectGroupID_ValueChanged(object sender, EventArgs e)
        {
            _check1 = ((AccountingObjectGroup)cbbAccountingObjectGroupID.SelectedRow.ListObject).AccountingObjectGroupCode != "TC";
            Filter();
        }

        private void cbbAccountingObjectCategoryID_ValueChanged(object sender, EventArgs e)
        {
            _check2 = ((AccountingObjectCategory)cbbAccountingObjectCategoryID.SelectedRow.ListObject).AccountingObjectCategoryCode != "TC";
            Filter();
        }

        private void Filter()
        {
            _lstAccountingObjectReport = _lstnew;
            AccountingObjectCategory accountingObjectCategory;
            AccountingObjectGroup accountingObjectGroup;
            if (_check1 && !_check2)
            {
                accountingObjectGroup = (AccountingObjectGroup)cbbAccountingObjectGroupID.SelectedRow.ListObject;
                _lstAccountingObjectReport = _lstAccountingObjectReport.Where(c => c.AccountObjectGroupID == accountingObjectGroup.ID).ToList();
            }
            else if (_check2 && !_check1)
            {
                accountingObjectCategory = (AccountingObjectCategory)cbbAccountingObjectCategoryID.SelectedRow.ListObject;
                _lstAccountingObjectReport = _lstAccountingObjectReport.Where(c => c.AccountingObjectCategory == accountingObjectCategory.ID.ToString()).ToList();
            }
            else if (_check1 && _check2)
            {
                accountingObjectGroup = (AccountingObjectGroup)cbbAccountingObjectGroupID.SelectedRow.ListObject;
                accountingObjectCategory = (AccountingObjectCategory)cbbAccountingObjectCategoryID.SelectedRow.ListObject;
                _lstAccountingObjectReport = _lstAccountingObjectReport.Where(c => c.AccountingObjectCategory == accountingObjectCategory.ID.ToString() && c.AccountObjectGroupID == accountingObjectGroup.ID).ToList();
            }
            ugridAccountingObject.SetDataBinding(_lstAccountingObjectReport.ToList(), "");
        }
        #endregion
        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void btnOk_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }
            if (!_lstAccountingObjectReport.Any(c => c.Check))
            {
                MSG.Warning(resSystem.Report_02);
            }
            else if (!_lstMaterialGoodsReports.Any(c => c.Check))
            {
                MSG.Error(resSystem.Report_03);
            }
            else
            {
                var dsAccountingObject = _lstAccountingObjectReport.Where(c => c.Check).Select(c => c.ID).ToList();
                List<Guid> dsMaterialGood = _lstMaterialGoodsReports.Where(c => c.Check).Select(d => d.ID).ToList();
                var dspur = _ppInvoiceService.ReportPurchaseDetailSupplerInventoryItem((DateTime)dtBeginDate.Value,
                    (DateTime)dtEndDate.Value, dsMaterialGood, dsAccountingObject, chk.Checked);
                var details = new PurchaseDetailSupplerInventoryItemDetail();
                details.Period = ReportUtils.GetPeriod((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value);
                var f = new ReportForm<PurchaseDetailSupplerInventoryItem>(fileReportSlot1, _subSystemCode);
                f.AddDatasource("PurchaseDetailSupplerInventoryItem", dspur, true);
                f.AddDatasource("Detail", details);
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();

            }

        }

    }
}
