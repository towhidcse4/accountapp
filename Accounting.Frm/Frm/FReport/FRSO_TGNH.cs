﻿using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.DAO;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace Accounting.Frm.FReport
{
    public partial class FRSO_TGNH : Form
    {
        private readonly IAccountService _IAccountService;
        private readonly IBankAccountDetailService _IBankAccountDetailService;
        private string _subSystemCode;
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        public FRSO_TGNH()
        {
            InitializeComponent();
            _IAccountService = IoC.Resolve<IAccountService>();
            _IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            _subSystemCode = "";
            var listacount = _IAccountService.GetAll().Where(p => p.IsActive == true && p.AccountNumber.StartsWith("112")).OrderBy(p => p.AccountNumber).ToList();
            cbbFromAccount.DataSource = _IAccountService.GetAll().Where(p => p.IsActive == true && p.AccountNumber.StartsWith("112")).OrderBy(p => p.AccountNumber).ToList();
            cbbFromAccount.DisplayMember = "AccountNumber";
            ReportUtils.ProcessControls(this);
            Utils.ConfigGrid(cbbFromAccount, ConstDatabase.Account_TableName);
            cbbFromAccount.SelectedRow = cbbFromAccount.Rows[0];
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dtBeginDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }

            if ((DateTime)dtBeginDate.Value > (DateTime)dtEndDate.Value)
            {
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            string acount = cbbFromAccount.Value != null ? cbbFromAccount.Value.ToString() : string.Empty;
            string acountBank = cbbBankAccountDetail.Value == null ? "" : cbbBankAccountDetail.Value.ToString();
            string tentk = cbbBankAccountDetail.Value == null ? "" : cbbBankAccountDetail.Value.ToString();

            if (cbbBankAccountDetail.Value == null || cbbBankAccountDetail.Value.ToString() == "Tất cả")
            {
                acountBank = "";
            }

            FRSO_TGNHex fm = new FRSO_TGNHex((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, acount, cbbCurrency.Value.ToString(), acountBank);
            fm.Show(this);           
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void reportManager1_GetReportParameter(object sender, PerpetuumSoft.Reporting.Components.GetReportParameterEventArgs e)
        {
            string typeCurrency = cbbCurrency.Value != null ? cbbCurrency.Value.ToString() : string.Empty;
            e.Parameters["CurrencyID"].Value = typeCurrency;
        }
       
    }
}
