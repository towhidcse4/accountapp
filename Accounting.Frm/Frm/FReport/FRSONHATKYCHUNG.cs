﻿using Accounting.Core.DAO;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.Report;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRSONHATKYCHUNG  : Form
    {
        string _subSystemCode;
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        List<GL_GeneralDiaryBook_S03a> data1 = new List<GL_GeneralDiaryBook_S03a>();
        public FRSONHATKYCHUNG()
        {
            InitializeComponent();
            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\SoNhatKyChung.rst", path);
            ReportUtils.ProcessControls(this);


            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dtBeginDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }


            //uGridDuLieu.DataSource = data1;
            //Utils.ConfigGrid(uGridDuLieu, ConstDatabase.GL_GeneralDiaryBook_S03a_TableName);
            //UltraGridBand parentBand = this.uGridDuLieu.DisplayLayout.Bands[0];
            //parentBand.RowLayoutStyle = RowLayoutStyle.GroupLayout;
            //UltraGridGroup parentBandGroup1 = parentBand.Groups.Add("ParentBandGroup1", "SỔ NHẬT KÝ CHUNG");
            //parentBandGroup1.Header.Appearance.TextHAlign = HAlign.Center;
            //parentBandGroup1.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            //string tieude = string.Format("Từ ngày ... đến ngày ...");
            //UltraGridGroup parentBandGroup = parentBand.Groups.Add("ParentBandGroup", tieude);
            //parentBandGroup.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            //parentBandGroup.Header.Appearance.TextHAlign = HAlign.Center;
            //parentBandGroup.RowLayoutGroupInfo.ParentGroup = parentBandGroup1;
            //parentBand.Columns[0].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            //parentBand.Columns[1].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            //parentBand.Columns[2].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            //parentBand.Columns[3].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            //parentBand.Columns[4].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            //parentBand.Columns[5].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            //parentBand.Columns[6].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            //parentBand.Columns[7].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            //this.WindowState = FormWindowState.Maximized;
            //if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "IsMinimized").Data == "1")
            //{
            //    this.WindowState = FormWindowState.Normal;


            //    this.Width = Screen.PrimaryScreen.WorkingArea.Width - 200;
            //    this.Height = Screen.PrimaryScreen.WorkingArea.Height - 50;
            //    this.Location = new System.Drawing.Point(((Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2), ((Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2));

            //}

        }

        private void ultraButton1_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }

            if ((DateTime)dtBeginDate.Value > (DateTime)dtEndDate.Value)
            {
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            List<GL_GeneralDiaryBook_S03a> data = new List<GL_GeneralDiaryBook_S03a>();
            int check = cbCongGopButToan.Checked == false ? 0 : 1;
            int check2 = cbHienThiLyke.Checked == false ? 0 : 1;
            ReportProcedureSDS db = new ReportProcedureSDS();
            data = db.Get_GL_GeneralDiaryBook_S03a((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, check, check2);
            if (data.Count == 0)
            {
                MSG.Warning("Không có dữ liệu để lên báo cáo");
            }
            else
            {
                var rD = new GL_GeneralDiaryBook_S03a_Detail();
                rD.Period = ReportUtils.GetPeriod((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value);
                var f = new ReportForm<GL_GeneralDiaryBook_S03a>(fileReportSlot1, _subSystemCode);
                f.AddDatasource("S03ADNN", data);
                f.AddDatasource("Detail", rD);
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
        }

        private void ultraButton2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ultraButton4_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }

            if ((DateTime)dtBeginDate.Value > (DateTime)dtEndDate.Value)
            {
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }


            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            List<GL_GeneralDiaryBook_S03a> data = new List<GL_GeneralDiaryBook_S03a>();
            int check = cbCongGopButToan.Checked == false ? 0 : 1;
            int check2 = cbHienThiLyke.Checked == false ? 0 : 1;
            ReportProcedureSDS db = new ReportProcedureSDS();
            data = db.Get_GL_GeneralDiaryBook_S03a((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, check, check2);
            if (data.Count == 0)
            {
                MSG.Warning("Không có dữ liệu để lên báo cáo");
            }
            else
            {
                var f = new FRSONHATKYCHUNGTruocIn(ReportUtils.GetPeriod((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value), _subSystemCode, data, (DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, check, check2);
                f.Show();
            }
        }

        //private void ultraButton3_Click(object sender, EventArgs e)
        //{
        //    SaveFileDialog save = new SaveFileDialog
        //    {
        //        Title = "Chọn nơi sao lưu ",
        //        Filter = "File (*.xls)|*.xls",
        //        InitialDirectory = "@C:\\",
        //        FileName = "SoNhatKyChung.xls"
        //    };
        //    if (save.ShowDialog() == DialogResult.OK)
        //    {
        //        string duongdan = save.FileName;
        //        try
        //        {
        //            ultraGridExcelExporter1.Export(uGridDuLieu, duongdan);
        //            if (MSG.Question("Kết xuất thành công, Bạn có muốn mở file vừa kết xuất không?") == System.Windows.Forms.DialogResult.Yes)
        //            {
        //                try
        //                {
        //                    System.Diagnostics.Process.Start(save.FileName);
        //                }
        //                catch (Exception ex)
        //                {
        //                    MSG.Error(ex.Message);
        //                }

        //            }
        //        }
        //        catch
        //        {
        //            MSG.Warning("Lỗi khi kết xuất !");
        //        }
        //    }
        //}

        private void ultraGridExcelExporter1_InitializeColumn(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.InitializeColumnEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Column.Format))
            {
                e.ExcelFormatStr = e.Column.Format;
            }
        }

        private void uGridDuLieu_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            if (e.Row.ListObject.HasProperty("TypeID"))
            {
                if (!e.Row.ListObject.GetProperty("TypeID").IsNullOrEmpty())
                {
                    int typeid = e.Row.ListObject.GetProperty("TypeID").ToInt();
                    //List<int> lstTypeID = new List<int> { 100, 101, 102, 103, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 133, 134, 140, 141, 142, 143, 144, 150, 160, 161, 162, 163, 170, 173, 180, 200, 210, 260, 261, 262, 263, 264, 220, 230, 240, 300, 310, 320, 321, 322, 323, 324, 325, 330, 340, 400, 401, 402, 403, 404, 410, 411, 412, 413, 414, 415, 420, 435, 510, 520, 530, 540, 550, 560, 600, 610, 620, 630, 640, 650, 660, 670, 840, 430, 902, 903, 904, 905, 906, 431, 433, 434, 432, 900, 901, 850, 860, 907 };  //danh sach form dung viewvoucherselected
                    //if(!lstTypeID.Contains(typeid))
                    //{ typeid = -typeid; }

                    if (e.Row.ListObject.HasProperty("ReferenceID"))
                    {
                        if (!e.Row.ListObject.GetProperty("ReferenceID").IsNullOrEmpty())
                        {
                            Guid id = (Guid)e.Row.ListObject.GetProperty("ReferenceID");
                            //List<int> lstTypeFrmNotStand = new List<int> { 1, 500, 510, 600, 620, 9998, 9999, 430, 907, 840, 690 };  //danh sách những loại frm dùng hàm mặc định CreateInstance(type, temp, DsObject, status)
                            //if(lstTypeID.Contains(typeid))
                                 var f = Utils.ViewVoucherSelected1(id, typeid);

                            // var f = Utils.ViewVoucherSelected1(id, typeid);
                            //     this.Close();
                            
                                if (f.IsDisposed)
                                {
                                    ultraButton4_Click(sender, e);
                                }
                            
                        }
                    }
                }

            }
        }

        private void FRSONHATKYCHUNG_Load(object sender, EventArgs e)
        {

        }

    }
}
