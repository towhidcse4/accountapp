﻿using Accounting.Core.Domain.obj.Report;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRSOCAIS03DNTruocIn : Form
    {
        public FRSOCAIS03DNTruocIn(List<GL_GetGLAccountLedgerDiaryBook> dataHeader, List<GL_GetGLAccountLedgerDiaryBook> data)
        {
            InitializeComponent();
            foreach(var item in data)
            {
                dataHeader.Add(item);
            }
            dataHeader.OrderBy(n => n.AccountNumber);
            ultraGrid1.DataSource = dataHeader;
            ultraGrid1.DataSource = data;
           //for(int i=1; i<dataHeader.Count;i++)
           // {
           //     ultraGrid1.DisplayLayout.Bands[i].AddNew();
           // }
        }
    }
}
