﻿using Accounting.Core.DAO;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.Report;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRSOCAIS03DN : Form
    {
        string _subSystemCode;
        readonly List<Account> _lstAccount = new List<Account>();
        IAccountService _IAccountService;
        readonly List<AccountReport> _lstAccountReport = ReportUtils.LstAccountBankReport;
        string path = "";
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        public FRSOCAIS03DN()
        {
            InitializeComponent();
            _subSystemCode = "";
            _IAccountService = IoC.Resolve<IAccountService>();
             path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\SoCai_S03bDN.rst", path);
            _lstAccount = _IAccountService.GetListAccountIsActive(true);
            _lstAccountReport = _lstAccount.Select(c => new AccountReport()
            {
                AccountNumber = c.AccountNumber,
                AccountGroupID = c.AccountGroupID,
                AccountGroupKindView = c.AccountGroupKindView,
                AccountGroupKind = c.AccountGroupKind,
                AccountName = c.AccountName,
                AccountNameGlobal = c.AccountNameGlobal,
                IsActive = c.IsActive,
                Description = c.Description,
                DetailType = c.DetailType,
                ID = c.ID,
                ParentID = c.ParentID,
                IsParentNode = c.IsParentNode,
                Grade = c.Grade
            }).OrderBy(c => c.AccountNumber).ToList();
            ugridAccount.SetDataBinding(_lstAccountReport.ToList(), "");
            ReportUtils.ProcessControls(this);
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dtBeginDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }

            if((DateTime)dtBeginDate.Value > (DateTime)dtEndDate.Value)
            {
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            int check = cbCongGopButToan.Checked == false ? 0 : 1;
            string listaccount = "";
            if (!_lstAccountReport.Any(c => c.Check))
            {
                MSG.Warning(resSystem.Report_01);
                return;
            }
            var dsaccount = _lstAccountReport.Where(t => t.Check).Select(c => c.AccountNumber.ToString());
            listaccount = string.Join(",", dsaccount.ToArray());
            List<GL_GetGLAccountLedgerDiaryBook> dataHeader = new List<GL_GetGLAccountLedgerDiaryBook>();
            List<GL_GetGLAccountLedgerDiaryBook> data = new List<GL_GetGLAccountLedgerDiaryBook>();
            ReportProcedureSDS db = new ReportProcedureSDS();
            data = db.GetSoCaiS03bDN("", 1, (DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, listaccount, check, ref dataHeader).OrderBy(o => o.PostedDate).ToList();
            if(data.Count == 0 && dataHeader.Count == 0)
            {
                MSG.Warning("Không có dữ liệu để lên báo cáo!");
            }else
            {if (data.Count == 0 && dataHeader.Count != 0)
                {
                     path = System.IO.Path.GetDirectoryName(
               System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
                    fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\SoCai_S03bDN1.rst", path);
                    var rD = new GL_GetGLAccountLedgerDiaryBookDetail();
                    rD.Period = ReportUtils.GetPeriod((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value);
                    var f = new ReportForm<GL_GetGLAccountLedgerDiaryBook>(fileReportSlot1, _subSystemCode);
                    f.AddDatasource("S03BDNNHeader", dataHeader, true);
                    f.AddDatasource("S03BDNN", data, true);
                    f.SetHyperlinkFunction("Accounting.ReportUtils", "Hyperlink");
                    f.AddDatasource("Detail", rD);
                    f.LoadReport();
                    f.WindowState = FormWindowState.Maximized;
                    f.Show();
                }
                else
                {
                    path = System.IO.Path.GetDirectoryName(
               System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
                    fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\SoCai_S03bDN.rst", path);
                    var rD = new GL_GetGLAccountLedgerDiaryBookDetail();
                    rD.Period = ReportUtils.GetPeriod((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value);
                    var f = new ReportForm<GL_GetGLAccountLedgerDiaryBook>(fileReportSlot1, _subSystemCode);
                    f.AddDatasource("S03BDNNHeader", dataHeader, true);
                    f.AddDatasource("S03BDNN", data, true);
                    f.SetHyperlinkFunction("Accounting.ReportUtils", "Hyperlink");
                    f.AddDatasource("Detail", rD);
                    f.LoadReport();
                    f.WindowState = FormWindowState.Maximized;
                    f.Show();
                }
            }            
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ultraButton1_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }

            if ((DateTime)dtBeginDate.Value > (DateTime)dtEndDate.Value)
            {
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }

            int check = cbCongGopButToan.Checked == false ? 0 : 1;
            string listaccount = "";
            if (!_lstAccountReport.Any(c => c.Check))
            {
                MSG.Warning(resSystem.Report_01);
                return;
            }
            var dsaccount = _lstAccountReport.Where(t => t.Check).Select(c => c.AccountNumber.ToString());
            listaccount = string.Join(",", dsaccount.ToArray());
            List<GL_GetGLAccountLedgerDiaryBook> dataHeader = new List<GL_GetGLAccountLedgerDiaryBook>();
            List<GL_GetGLAccountLedgerDiaryBook> data = new List<GL_GetGLAccountLedgerDiaryBook>();
            ReportProcedureSDS db = new ReportProcedureSDS();
            data = db.GetSoCaiS03bDN("", 1, (DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, listaccount, check, ref dataHeader).OrderBy(o => o.PostedDate).ToList();
            if (data.Count == 0 && dataHeader.Count == 0)
            {
                MSG.Warning("Không có dữ liệu để lên báo cáo!");
            }
            else
            {
                var rD = new GL_GetGLAccountLedgerDiaryBookDetail();
                rD.Period = ReportUtils.GetPeriod((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value);
                var f = new FRSOCAIS03DNTruocIn(data, dataHeader);
                f.Show();
            }
            }
    }
}
