﻿namespace Accounting
{
    partial class FFormulaOperandUsing
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraPanel3 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox_DienGiai = new Infragistics.Win.Misc.UltraGroupBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.ultraPanel4 = new Infragistics.Win.Misc.UltraPanel();
            this.bt_giup = new Infragistics.Win.Misc.UltraButton();
            this.bt_huybo = new Infragistics.Win.Misc.UltraButton();
            this.bt_dongngoac = new Infragistics.Win.Misc.UltraButton();
            this.bt_dongy = new Infragistics.Win.Misc.UltraButton();
            this.bt_nhan = new Infragistics.Win.Misc.UltraButton();
            this.bt_kt = new Infragistics.Win.Misc.UltraButton();
            this.bt_c = new Infragistics.Win.Misc.UltraButton();
            this.bt_backspace = new Infragistics.Win.Misc.UltraButton();
            this.bt_mongoac = new Infragistics.Win.Misc.UltraButton();
            this.bt_chia = new Infragistics.Win.Misc.UltraButton();
            this.bt_tru = new Infragistics.Win.Misc.UltraButton();
            this.bt_cong = new Infragistics.Win.Misc.UltraButton();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel2 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            this.ultraPanel3.ClientArea.SuspendLayout();
            this.ultraPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox_DienGiai)).BeginInit();
            this.ultraGroupBox_DienGiai.SuspendLayout();
            this.ultraPanel4.ClientArea.SuspendLayout();
            this.ultraPanel4.SuspendLayout();
            this.ultraPanel2.ClientArea.SuspendLayout();
            this.ultraPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraPanel3);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraPanel4);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(761, 188);
            this.ultraPanel1.TabIndex = 0;
            // 
            // ultraPanel3
            // 
            // 
            // ultraPanel3.ClientArea
            // 
            this.ultraPanel3.ClientArea.Controls.Add(this.ultraGroupBox_DienGiai);
            this.ultraPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel3.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel3.Name = "ultraPanel3";
            this.ultraPanel3.Size = new System.Drawing.Size(761, 105);
            this.ultraPanel3.TabIndex = 0;
            // 
            // ultraGroupBox_DienGiai
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.BackColor2 = System.Drawing.Color.Transparent;
            this.ultraGroupBox_DienGiai.Appearance = appearance1;
            this.ultraGroupBox_DienGiai.Controls.Add(this.richTextBox1);
            this.ultraGroupBox_DienGiai.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox_DienGiai.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox_DienGiai.Name = "ultraGroupBox_DienGiai";
            this.ultraGroupBox_DienGiai.Size = new System.Drawing.Size(761, 105);
            this.ultraGroupBox_DienGiai.TabIndex = 3;
            this.ultraGroupBox_DienGiai.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox1.Location = new System.Drawing.Point(3, 0);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(755, 102);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "";
            this.richTextBox1.Leave += new System.EventHandler(this.richTextBox1_Leave);
            this.richTextBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.richTextBox1_MouseDown);
            // 
            // ultraPanel4
            // 
            // 
            // ultraPanel4.ClientArea
            // 
            this.ultraPanel4.ClientArea.Controls.Add(this.bt_giup);
            this.ultraPanel4.ClientArea.Controls.Add(this.bt_huybo);
            this.ultraPanel4.ClientArea.Controls.Add(this.bt_dongngoac);
            this.ultraPanel4.ClientArea.Controls.Add(this.bt_dongy);
            this.ultraPanel4.ClientArea.Controls.Add(this.bt_nhan);
            this.ultraPanel4.ClientArea.Controls.Add(this.bt_kt);
            this.ultraPanel4.ClientArea.Controls.Add(this.bt_c);
            this.ultraPanel4.ClientArea.Controls.Add(this.bt_backspace);
            this.ultraPanel4.ClientArea.Controls.Add(this.bt_mongoac);
            this.ultraPanel4.ClientArea.Controls.Add(this.bt_chia);
            this.ultraPanel4.ClientArea.Controls.Add(this.bt_tru);
            this.ultraPanel4.ClientArea.Controls.Add(this.bt_cong);
            this.ultraPanel4.ClientArea.Controls.Add(this.ultraLabel2);
            this.ultraPanel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraPanel4.Location = new System.Drawing.Point(0, 105);
            this.ultraPanel4.Name = "ultraPanel4";
            this.ultraPanel4.Size = new System.Drawing.Size(761, 83);
            this.ultraPanel4.TabIndex = 1;
            // 
            // bt_giup
            // 
            this.bt_giup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bt_giup.Location = new System.Drawing.Point(676, 6);
            this.bt_giup.Name = "bt_giup";
            this.bt_giup.Size = new System.Drawing.Size(75, 23);
            this.bt_giup.TabIndex = 5;
            this.bt_giup.Text = "Giúp";
            // 
            // bt_huybo
            // 
            this.bt_huybo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bt_huybo.Location = new System.Drawing.Point(595, 6);
            this.bt_huybo.Name = "bt_huybo";
            this.bt_huybo.Size = new System.Drawing.Size(75, 23);
            this.bt_huybo.TabIndex = 4;
            this.bt_huybo.Text = "Đóng";
            this.bt_huybo.Click += new System.EventHandler(this.bt_huybo_Click);
            // 
            // bt_dongngoac
            // 
            this.bt_dongngoac.Location = new System.Drawing.Point(132, 6);
            this.bt_dongngoac.Name = "bt_dongngoac";
            this.bt_dongngoac.Size = new System.Drawing.Size(24, 23);
            this.bt_dongngoac.TabIndex = 9;
            this.bt_dongngoac.Text = ")";
            this.bt_dongngoac.Click += new System.EventHandler(this.bt_dongngoac_Click);
            // 
            // bt_dongy
            // 
            this.bt_dongy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bt_dongy.Location = new System.Drawing.Point(514, 6);
            this.bt_dongy.Name = "bt_dongy";
            this.bt_dongy.Size = new System.Drawing.Size(75, 23);
            this.bt_dongy.TabIndex = 3;
            this.bt_dongy.Text = "Đồng ý";
            this.bt_dongy.Click += new System.EventHandler(this.bt_dongy_Click);
            // 
            // bt_nhan
            // 
            this.bt_nhan.Location = new System.Drawing.Point(60, 6);
            this.bt_nhan.Name = "bt_nhan";
            this.bt_nhan.Size = new System.Drawing.Size(24, 23);
            this.bt_nhan.TabIndex = 8;
            this.bt_nhan.Text = "*";
            this.bt_nhan.Click += new System.EventHandler(this.bt_nhan_Click);
            // 
            // bt_kt
            // 
            this.bt_kt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bt_kt.Location = new System.Drawing.Point(433, 6);
            this.bt_kt.Name = "bt_kt";
            this.bt_kt.Size = new System.Drawing.Size(75, 23);
            this.bt_kt.TabIndex = 1;
            this.bt_kt.Text = "Kiểm tra";
            // 
            // bt_c
            // 
            this.bt_c.Location = new System.Drawing.Point(243, 6);
            this.bt_c.Name = "bt_c";
            this.bt_c.Size = new System.Drawing.Size(47, 23);
            this.bt_c.TabIndex = 7;
            this.bt_c.Text = "C";
            this.bt_c.Click += new System.EventHandler(this.bt_c_Click);
            // 
            // bt_backspace
            // 
            this.bt_backspace.Location = new System.Drawing.Point(196, 6);
            this.bt_backspace.Name = "bt_backspace";
            this.bt_backspace.Size = new System.Drawing.Size(47, 23);
            this.bt_backspace.TabIndex = 6;
            this.bt_backspace.Text = "<---";
            this.bt_backspace.Click += new System.EventHandler(this.bt_backspace_Click);
            // 
            // bt_mongoac
            // 
            this.bt_mongoac.Location = new System.Drawing.Point(108, 6);
            this.bt_mongoac.Name = "bt_mongoac";
            this.bt_mongoac.Size = new System.Drawing.Size(24, 23);
            this.bt_mongoac.TabIndex = 5;
            this.bt_mongoac.Text = "(";
            this.bt_mongoac.Click += new System.EventHandler(this.bt_mongoac_Click);
            // 
            // bt_chia
            // 
            this.bt_chia.Location = new System.Drawing.Point(84, 6);
            this.bt_chia.Name = "bt_chia";
            this.bt_chia.Size = new System.Drawing.Size(24, 23);
            this.bt_chia.TabIndex = 4;
            this.bt_chia.Text = "/";
            this.bt_chia.Click += new System.EventHandler(this.bt_chia_Click);
            // 
            // bt_tru
            // 
            this.bt_tru.Location = new System.Drawing.Point(36, 6);
            this.bt_tru.Name = "bt_tru";
            this.bt_tru.Size = new System.Drawing.Size(24, 23);
            this.bt_tru.TabIndex = 3;
            this.bt_tru.Text = "-";
            this.bt_tru.Click += new System.EventHandler(this.bt_tru_Click);
            // 
            // bt_cong
            // 
            this.bt_cong.Location = new System.Drawing.Point(12, 6);
            this.bt_cong.Name = "bt_cong";
            this.bt_cong.Size = new System.Drawing.Size(24, 23);
            this.bt_cong.TabIndex = 2;
            this.bt_cong.Text = "+";
            this.bt_cong.Click += new System.EventHandler(this.bt_cong_Click);
            // 
            // ultraLabel2
            // 
            this.ultraLabel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraLabel2.Location = new System.Drawing.Point(12, 36);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(737, 13);
            this.ultraLabel2.TabIndex = 1;
            this.ultraLabel2.Text = "Lưu ý: Để xây dựng công thức đúng bạn cần phải thiết lập các biến theo quy tắc gi" +
    "ống như cột Công thức ví dụ bên dưới";
            // 
            // ultraPanel2
            // 
            // 
            // ultraPanel2.ClientArea
            // 
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraGrid1);
            this.ultraPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel2.Location = new System.Drawing.Point(0, 188);
            this.ultraPanel2.Name = "ultraPanel2";
            this.ultraPanel2.Size = new System.Drawing.Size(761, 339);
            this.ultraPanel2.TabIndex = 1;
            // 
            // ultraGrid1
            // 
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            appearance2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid1.DisplayLayout.Appearance = appearance2;
            this.ultraGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance3.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance3.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.GroupByBox.Appearance = appearance3;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance4;
            this.ultraGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance5.BackColor2 = System.Drawing.SystemColors.Control;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance5;
            this.ultraGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance6;
            appearance7.BackColor = System.Drawing.SystemColors.Highlight;
            appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance7;
            this.ultraGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.Override.CardAreaAppearance = appearance8;
            appearance9.BorderColor = System.Drawing.Color.Silver;
            appearance9.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid1.DisplayLayout.Override.CellAppearance = appearance9;
            this.ultraGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance11.TextHAlignAsString = "Left";
            this.ultraGrid1.DisplayLayout.Override.HeaderAppearance = appearance11;
            this.ultraGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid1.DisplayLayout.Override.RowAppearance = appearance12;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance13;
            this.ultraGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid1.Location = new System.Drawing.Point(0, 0);
            this.ultraGrid1.Name = "ultraGrid1";
            this.ultraGrid1.Size = new System.Drawing.Size(761, 339);
            this.ultraGrid1.TabIndex = 0;
            this.ultraGrid1.Text = "ultraGrid1";
            this.ultraGrid1.ClickCell += new Infragistics.Win.UltraWinGrid.ClickCellEventHandler(this.ultraGrid1_ClickCell);
            // 
            // FFormulaOperandUsing
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(761, 527);
            this.Controls.Add(this.ultraPanel2);
            this.Controls.Add(this.ultraPanel1);
            this.MaximumSize = new System.Drawing.Size(777, 566);
            this.MinimumSize = new System.Drawing.Size(777, 566);
            this.Name = "FFormulaOperandUsing";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FFormulaOperandUsing";
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            this.ultraPanel3.ClientArea.ResumeLayout(false);
            this.ultraPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox_DienGiai)).EndInit();
            this.ultraGroupBox_DienGiai.ResumeLayout(false);
            this.ultraPanel4.ClientArea.ResumeLayout(false);
            this.ultraPanel4.ResumeLayout(false);
            this.ultraPanel2.ClientArea.ResumeLayout(false);
            this.ultraPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraPanel ultraPanel2;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid1;
        private Infragistics.Win.Misc.UltraPanel ultraPanel3;
        private Infragistics.Win.Misc.UltraPanel ultraPanel4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraButton bt_dongngoac;
        private Infragistics.Win.Misc.UltraButton bt_nhan;
        private Infragistics.Win.Misc.UltraButton bt_c;
        private Infragistics.Win.Misc.UltraButton bt_backspace;
        private Infragistics.Win.Misc.UltraButton bt_mongoac;
        private Infragistics.Win.Misc.UltraButton bt_chia;
        private Infragistics.Win.Misc.UltraButton bt_tru;
        private Infragistics.Win.Misc.UltraButton bt_cong;
        private Infragistics.Win.Misc.UltraButton bt_giup;
        private Infragistics.Win.Misc.UltraButton bt_huybo;
        private Infragistics.Win.Misc.UltraButton bt_dongy;
        private Infragistics.Win.Misc.UltraButton bt_kt;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox_DienGiai;
        private System.Windows.Forms.RichTextBox richTextBox1;

    }
}