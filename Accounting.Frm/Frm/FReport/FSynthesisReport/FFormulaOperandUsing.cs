﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FFormulaOperandUsing : Form
    {
        private static IFormulaOperandUsingService FormulaOperandUsingService
        {
            get { return IoC.Resolve<IFormulaOperandUsingService>(); }
        }
        private static ISynthesisReportService _iSynthesisReportService
        {
            get { return IoC.Resolve<ISynthesisReportService>(); }
        }
        List<FormulaOperandUsing> _lstFormulaOperandUsings = new List<FormulaOperandUsing>();
        readonly SynthesisReport _synthesisReport = new SynthesisReport();
        private readonly string _nameColumn = "";
        public bool _isClose = true;
        public FFormulaOperandUsing(SynthesisReport synthesisReport, string nameSynthesisReport, string name)
        {
            InitializeComponent();
            _synthesisReport = synthesisReport;
            _nameColumn = name;
            PropertyInfo propItem = _synthesisReport.GetType().GetProperty(_nameColumn);
            string s = (string)propItem.GetValue(_synthesisReport, null);
            if (propItem.CanWrite && s !=null )
            {
                richTextBox1.Text = s.Length>0? FormatExpression(s):s;
            }
            richTextBox1.SelectionStart = richTextBox1.TextLength;

            ultraGroupBox_DienGiai.Text = "";
            ultraGroupBox_DienGiai.Text = nameSynthesisReport + ": ";
            ultraGroupBox_DienGiai.Text += "Chỉ tiêu - " + synthesisReport.ItemName;
            _lstFormulaOperandUsings =
                FormulaOperandUsingService.GetAll().Where(c => c.TypeID == synthesisReport.TypeID).ToList();
            ultraGrid1.DataSource = _lstFormulaOperandUsings;
            Utils.ConfigGrid(ultraGrid1, ConstDatabase.FormulaOperandUsing_TableName);
            ultraGrid1.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            var lst = _lstFormulaOperandUsings.Select(c => c.FormulaOperandUsingName).ToList();
            for (int i = 0; i < lst.Count; i++)
            {
                lst[i] += "[]";
            }
            //listBox1.Items.AddRange(items: lst.ToArray());
            richTextBox1.TextChanged += ultraTextEditor1_TextChanged;
            //_rtb = richTextBox1.Location;
        }
        private void ultraTextEditor1_TextChanged(object sender, EventArgs e)
        {
        }

     Point _selectPoint = new Point(0, 0);
        private void bt_cong_Click(object sender, EventArgs e)
        {
            int vitri = richTextBox1.GetCharIndexFromPosition(_selectPoint);
            richTextBox1.Text = richTextBox1.Text.Insert(vitri, "+");
        }

        private void bt_tru_Click(object sender, EventArgs e)
        {
            int vitri = richTextBox1.GetCharIndexFromPosition(_selectPoint);
            richTextBox1.Text = richTextBox1.Text.Insert(vitri, "-");
        }

        private void bt_nhan_Click(object sender, EventArgs e)
        {
            int vitri = richTextBox1.GetCharIndexFromPosition(_selectPoint);
            richTextBox1.Text = richTextBox1.Text.Insert(vitri, "*");
        }

        private void bt_chia_Click(object sender, EventArgs e)
        {
            int vitri = richTextBox1.GetCharIndexFromPosition(_selectPoint);
            richTextBox1.Text = richTextBox1.Text.Insert(vitri, "/");
        }

        private void bt_mongoac_Click(object sender, EventArgs e)
        {
            int vitri = richTextBox1.GetCharIndexFromPosition(_selectPoint);
            richTextBox1.Text = richTextBox1.Text.Insert(vitri, "(");
        }

        private void bt_dongngoac_Click(object sender, EventArgs e)
        {
            int vitri = richTextBox1.GetCharIndexFromPosition(_selectPoint);
            richTextBox1.Text = richTextBox1.Text.Insert(vitri, ")");
        }

        private void bt_backspace_Click(object sender, EventArgs e)
        {
            if (richTextBox1.Text != "")
                richTextBox1.Text = richTextBox1.Text.Substring(0, richTextBox1.TextLength - 1);
        }

        private void bt_c_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
        }
        private void ultraGrid1_ClickCell(object sender, ClickCellEventArgs e)
        {
            string s = richTextBox1.Text.Trim();
            if (e.Cell.Row.Index != -1)
            {

                List<string> pheptoan = new List<string>() { "+", "-", "*", "/", "(", ")" };
                if (string.IsNullOrEmpty(s) || pheptoan.Contains(s.Substring(s.Length - 2).Trim()))
                {
                    richTextBox1.Text += ultraGrid1.Rows[e.Cell.Row.Index].Cells["FormulaOperandUsingName"].Value.ToString() + "[]";
                    richTextBox1.Focus();
                    richTextBox1.SelectionStart = richTextBox1.TextLength - 1;
                }
                else if (!pheptoan.Contains(s.Substring(s.Length - 2).Trim()))
                {
                    MessageBox.Show("Bạn chưa nhập phép toán tương ứng");
                }

            }
        }
        private void richTextBox1_Leave(object sender, EventArgs e)
        {
            richTextBox1.Text = FormatExpression(richTextBox1.Text) + " ";
        }
        public static string FormatExpression(string expression)
        {
            expression = expression.Replace(" ", "");
            expression = Regex.Replace(expression, @"(\+|\-|\*|\/|\%){3,}", match => match.Value[0].ToString());
            expression = Regex.Replace(expression, @"(\+|\-|\*|\/|\%)(\+|\*|\/|\%)", match => match.Value[0].ToString());
            expression = Regex.Replace(expression, @"\+|\-|\*|\/|\%|\)|\(", match => String.Format(" {0} ", match.Value));
            expression = expression.Replace("  ", " ");
            expression = expression.Trim();
            return expression;
        }
        private void bt_dongy_Click(object sender, EventArgs e)
        {
            #region Fill dữ liệu control vào obj
            PropertyInfo propItem = _synthesisReport.GetType().GetProperty(_nameColumn);
            if (propItem != null && propItem.CanWrite)
            {
                propItem.SetValue(_synthesisReport, richTextBox1.Text, null);
            }
            #endregion

            try
            {
                if (
                    MessageBox.Show("Bạn muốn lưu công thức trên vào cơ sở dữ liệu không?",
                        "Lưu dữ liệu vào cơ sở dữ liệu", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) ==
                    DialogResult.Yes)
                {
                    #region Thao tác CSDL
                    _iSynthesisReportService.BeginTran();
                    _iSynthesisReportService.Update(_synthesisReport);
                    _iSynthesisReportService.CommitTran();
                    #endregion

                    #region xử lý form, kết thúc form
                    this.Close();
                    _isClose = false;
                    #endregion
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void bt_huybo_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void richTextBox1_MouseDown(object sender, MouseEventArgs e)
        {
            _selectPoint = new Point(e.X, e.Y);
        }
    }
}
