﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.Core.ServiceImp;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FSynthesisReport : Form
    {
        private static ISynthesisReportService _iSynthesisReportService
        {
            get { return IoC.Resolve<ISynthesisReportService>(); }
        }
        List<SynthesisReport> _lstFSynthesisReports = new List<SynthesisReport>();
        List<SynthesisReport> _lstFSynthesisReportsOld = new List<SynthesisReport>();
        List<SynthesisReport> _lstFSynthesisReportsBackUp = new List<SynthesisReport>();
        class TypeSynthesisReport
        {
            public string Name { get; set; }
            public int Type { get; set; }
        }
        public FSynthesisReport()
        {
            InitializeComponent();
            _lstFSynthesisReportsOld = _iSynthesisReportService.GetAll_ByIsDefault();
            List<TypeSynthesisReport> typeSynthesisReports = new List<TypeSynthesisReport>
            {
                new TypeSynthesisReport() {Name = "Bảng cân đối kế toán", Type = 1},
                new TypeSynthesisReport() {Name = "Báo cáo kết quả hoạt động kinh doanh", Type = 2},
                new TypeSynthesisReport() {Name = "Báo cáo lưu chuyển tiền tệ", Type = 3},
                new TypeSynthesisReport() {Name = "Tình hình thực hiện nghĩa vụ với nhà nước", Type = 4},
                new TypeSynthesisReport() {Name = "Thuyết minh báo cáo tài chính", Type = 5},
                new TypeSynthesisReport() {Name = "Tờ khai thuế thu nhập doah nghiệp", Type = 6}
            };
            cbb_loaibaocao.DataSource = typeSynthesisReports;
            cbb_loaibaocao.DisplayMember = "Name";
            cbb_loaibaocao.ValueMember = "Type";
            cbb_loaibaocao.SelectionChanged += cbb_loaibaocao_SelectionChanged;
            cbb_loaibaocao.SelectedIndex = 0;
        }

        private void cbb_loaibaocao_SelectionChanged(object sender, EventArgs e)
        {
            _lstFSynthesisReports =
                _lstFSynthesisReportsOld.Where(
                    c => c.TypeID == ((TypeSynthesisReport)cbb_loaibaocao.SelectedItem.ListObject).Type).ToList();
            uGrid.SetDataBinding(_lstFSynthesisReports, "");
            ConfigGrid(uGrid, ((TypeSynthesisReport)cbb_loaibaocao.SelectedItem.ListObject).Type);
        }

        private void ConfigGrid(UltraGrid ultraGrid, int type)
        {

            UltraTextEditor txtEditor = new UltraTextEditor();
            EditorButton btn = new EditorButton();
            btn.Appearance.Image = Properties.Resources.edit_validated_icon;
            txtEditor.ButtonsRight.Add(btn);
            txtEditor.EditorButtonClick += btn_EditorButtonClick;
            const string formula = "Formula";
            int k = 0;
            foreach (var row in ultraGrid.Rows)
            {
                row.Activation = Activation.AllowEdit;
                row.Appearance.BackColor = Color.White;
                for (int i = 1; i <= 30; i++)
                {
                    string key = formula + i;
                    if (row.Index == 0) ultraGrid.DisplayLayout.Bands[0].Columns[key].EditorComponent = txtEditor;
                    if (_lstFSynthesisReports[k].IsUsedFormula)
                    {
                        row.Cells[key].Activation = Activation.AllowEdit;
                        row.Cells[key].Appearance.BackColor = Color.White;
                    }
                    else
                    {
                        row.Cells[key].Activation = Activation.NoEdit;
                        row.Cells[key].Appearance.BackColor = Color.FromArgb(225, 225, 225);
                    }
                }
                k++;
            }
            var valueList = new ValueList();
            valueList.ValueListItems.Add(0, "Không cộng");
            valueList.ValueListItems.Add(1, "Cộng");
            valueList.ValueListItems.Add(-1, "Trừ");
            ultraGrid.DisplayLayout.Bands[0].Columns["FormulaOnGeneralIndicator"].ValueList = valueList;
            valueList = new ValueList();
            valueList.ValueListItems.Add(1, "Cộng");
            valueList.ValueListItems.Add(-1, "Trừ");
            ultraGrid.DisplayLayout.Bands[0].Columns["FormulaOfIndicator"].ValueList = valueList;
            switch (type)
            {
                case 1:
                    Utils.ConfigGrid(ultraGrid, ConstDatabase.BalanceSheet_TableName, false);
                    break;
                case 4:
                    Utils.ConfigGrid(ultraGrid, ConstDatabase.TheImplementationOfStateObligations_TableName, false);
                    break;
                case 5:
                    Utils.ConfigGrid(ultraGrid, ConstDatabase.InterpretFinancialStatements_TableName, false);
                    break;
                case 6:
                    Utils.ConfigGrid(ultraGrid, ConstDatabase.BusinessIncomeTaxDeclaration_TableName, false);
                    break;
                default:
                    break;
            }
            //bật ghim cột
            ultraGrid.DisplayLayout.UseFixedHeaders = true;
            ultraGrid.UseOsThemes = DefaultableBoolean.True;
            ultraGrid.DisplayLayout.Bands[0].Columns["ItemCode"].Header.Fixed = true;
            ultraGrid.DisplayLayout.Bands[0].Columns["ItemName"].Header.Fixed = true;
            ultraGrid.DisplayLayout.Bands[0].Columns["ItemIndex"].Header.Fixed = true;
            //ultraGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
        }

        void btn_EditorButtonClick(object sender, EditorButtonEventArgs e)
        {
            SynthesisReport synthesisReport = (SynthesisReport)uGrid.ActiveCell.Row.ListObject;
            if (synthesisReport.IsUsedFormula)
            {
                FFormulaOperandUsing frm = new FFormulaOperandUsing(synthesisReport, ((TypeSynthesisReport)cbb_loaibaocao.SelectedItem.ListObject).Name, uGrid.ActiveCell.Column.Key);
                frm.ShowDialog(this);
                if (!frm._isClose)
                {
                    _lstFSynthesisReportsOld = _iSynthesisReportService.GetAll_ByIsDefault();
                    cbb_loaibaocao_SelectionChanged(sender, e);
                }
            }
        }
        private void bt_luu_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (var synthesisReports in _lstFSynthesisReports)
                {
                    if (!CheckError(synthesisReports))
                    {
                        return;
                    }
                    #region Thao tác CSDL
                    _iSynthesisReportService.BeginTran();
                    _iSynthesisReportService.Update(synthesisReports);
                    _iSynthesisReportService.CommitTran();
                    #endregion
                }
                MessageBox.Show("Lưu vào cơ sở dữ liệu thành công!");
                _lstFSynthesisReportsOld = _iSynthesisReportService.GetAll_ByIsDefault();
                cbb_loaibaocao_SelectionChanged(sender, e);
            }
            catch (Exception)
            {

                throw;
            }

        }
        bool CheckError(SynthesisReport synthesisReports)
        {
            if (string.IsNullOrEmpty(synthesisReports.ItemName))
            {
                MSG.Error(string.Format(resSystem.MSGREP_FSynthesisReportItemCode, synthesisReports.ItemCode));
                return false;
            }
            if (string.IsNullOrEmpty(synthesisReports.ItemIndex.ToString()))
            {
                MSG.Error(string.Format(resSystem.MSGREP_FSynthesisReportItemIndex, synthesisReports.ItemCode));
                return false;
            }
            if (synthesisReports.ItemIndex < 0)
            {
                MSG.Error(string.Format(resSystem.MSGREP_FSynthesisReportItemIndex3, synthesisReports.ItemIndex));
                return false;
            }
            if (synthesisReports.ItemIndex > _lstFSynthesisReports.Count + 1)
            {
                MSG.Error(string.Format((resSystem.MSGREP_FSynthesisReportItemIndex2), synthesisReports.ItemIndex));
                return false;
            }
            return true;
        }

        private void uGrid_CellChange(object sender, CellEventArgs e)
        {
            const string formula = "Formula";
            if (e.Cell.Column.Key == "IsUsedFormula")
            {
                for (int i = 1; i <= 30; i++)
                {
                    string key = formula + i;
                    if (bool.Parse(e.Cell.Row.Cells["IsUsedFormula"].Text))
                    {
                        e.Cell.Row.Cells[key].Activation = Activation.AllowEdit;
                        e.Cell.Row.Cells[key].Appearance.BackColor = Color.White;
                    }
                    else
                    {
                        e.Cell.Row.Cells[key].Activation = Activation.NoEdit;
                        e.Cell.Row.Cells[key].Appearance.BackColor = Color.FromArgb(225, 225, 225);
                    }
                }
            }
        }

        private void bt_laymacdinh_Click(object sender, EventArgs e)
        {
            _lstFSynthesisReportsBackUp = _iSynthesisReportService.GetAll_ByIsDefault(false);
            foreach (var synthesisReport in _lstFSynthesisReportsOld)
            {
                #region Thao tác CSDL
                _iSynthesisReportService.BeginTran();
                _iSynthesisReportService.Delete(synthesisReport);
                _iSynthesisReportService.CommitTran();
                #endregion
            }
            foreach (var synthesisReport in _lstFSynthesisReportsBackUp)
            {
                #region Thao tác CSDL

                SynthesisReport newSynthesisReport = Utils.CloneObject(synthesisReport);
                newSynthesisReport.ID = Guid.NewGuid();
                newSynthesisReport.IsDefault = true;
                _iSynthesisReportService.BeginTran();
                _iSynthesisReportService.CreateNew(newSynthesisReport);
                _iSynthesisReportService.CommitTran();
                #endregion
            }
            MessageBox.Show("Thành công");
            _lstFSynthesisReportsOld = _iSynthesisReportService.GetAll_ByIsDefault();
            cbb_loaibaocao_SelectionChanged(sender, e);
        }

    }
}
