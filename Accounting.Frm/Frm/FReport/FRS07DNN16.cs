﻿using System;
using System.Collections.Generic;
using System.Linq;
using FX.Core;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;

namespace Accounting
{
    public partial class FRS07DNN16 : CustormForm
    {
        readonly List<MaterialGoodsReport> _lstnew = ReportUtils.LstMaterialGoodsReport;
        private List<MaterialGoodsReport> _lstMaterialGoodsReports = new List<MaterialGoodsReport>();
        private readonly List<string> lstAccountNumber = new List<string> { "152", "153", "155", "156", "157" };
        List<Account> lstAccount = new List<Account>();
        //readonly List<MaterialGoodsReport> _lstMaterialGoods = ReportUtils.LstMaterialGoodsReport;
        string _subSystemCode;
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        public FRS07DNN16(string subSystemCode)
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();
            _subSystemCode = subSystemCode;
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\S07-DNN16.rst", path);
            ReportUtils.ProcessControls(this);
            btnExit.Click += btnExit_Click;
            btnOk.Click += btnOk_Click;
            var Acc = new Account
            {
                AccountNumber = "Tất cả",
                AccountName = "Tất cả",
                ID = Guid.Empty,
                IsActive = true
            };
            lstAccount.Add(Acc);
            lstAccount.AddRange(Utils.ListAccount.Where(o => o.AccountNumber.StartsWith("152") || o.AccountNumber.StartsWith("153") || o.AccountNumber.StartsWith("155")
            || o.AccountNumber.StartsWith("156") || o.AccountNumber.StartsWith("157")).ToList());
            this.ConfigCombo(lstAccount, cbbAccount, "AccountNumber", "AccountNumber");
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dtBeginDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }

            WaitingFrm.StopWaiting();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }
            if (dtBeginDate.DateTime.Date > dtEndDate.DateTime.Date)
            {
                //MessageBox.Show("Ngày bắt đầu không thể lớn hơn ngày kết thúc", "Cảnh báo");
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            List<S07DNN16> data = new List<S07DNN16>();
            var repositoryLedger = IoC.Resolve<IRepositoryLedgerService>();
            List<Guid> lstGuidMaterialGoodsCategory = new List<Guid>();
            Account accSelected = cbbAccount.SelectedRow != null ? (Account)cbbAccount.SelectedRow.ListObject : new Account();
            List<string> listAccount = cbbAccount.SelectedRow != null ? accSelected.AccountNumber == "Tất cả" ? ((List<Account>)cbbAccount.DataSource).Where(c=>c.AccountNumber != "Tất cả").Select(x=>x.AccountNumber).ToList() : new List<string> { accSelected.AccountNumber } : ((List<Account>)cbbAccount.DataSource).Where(c => c.AccountNumber != "Tất cả").Select(x => x.AccountNumber).ToList();

            data = repositoryLedger.RS07DNN16((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, listAccount);
            if(data.Count == 0)
            {
                MSG.Information("Không có dữ liệu để lên báo cáo!");
                return;
            }
            var rd = new S07DNN16Detail();
            rd.Period = ReportUtils.GetPeriod(dtBeginDate.DateTime, dtEndDate.DateTime);
            var f = new ReportForm<S07DNN16>(fileReportSlot1, _subSystemCode);
            f.AddDatasource("S07DNN", data, true);
            f.AddDatasource("detail", rd);
            f.SetHyperlinkFunction("Accounting.ReportUtils", "Hyperlink");
            f.LoadReport();
            f.WindowState = FormWindowState.Maximized;
            f.Show();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
