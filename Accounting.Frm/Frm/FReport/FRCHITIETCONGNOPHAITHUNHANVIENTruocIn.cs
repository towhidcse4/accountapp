﻿using Accounting.Core.DAO;
using Accounting.Core.Domain.obj.Report;
using Accounting.TextMessage;
using Infragistics.Documents.Excel;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRCHITIETCONGNOPHAITHUNHANVIENTruocIn : Form
    {
        DateTime begin1;
        DateTime end1;
        string acount1;
        string list_acount1;
        public FRCHITIETCONGNOPHAITHUNHANVIENTruocIn(DateTime begin, DateTime end, string acount, string list_acount)
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();
            begin1 = begin;
            end1 = end;
            acount1 = acount;
            list_acount1 = list_acount;
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\ChiTietCongNoPhaiThuNV.rst", path);
            ReportProcedureSDS sp = new ReportProcedureSDS();
            List<SA_ReceivableDetail> data = sp.GetCHICONGNOPHAITHUNV(begin, end, acount, list_acount, false);//editby cuongpv cbbCurrency.Value.ToString() -> currencyID
            GanTKDuNoTKDuCo(data);
            List<SA_ReceivableDetail> listCongNhom = data.Where(m => m.IDGroup == 1).ToList();
            
            List<SA_ReceivableDetail> listChiTiet = data.Where(m => m.IDGroup == 2).ToList();
            //List<SA_ReceivableDetail> listtieude = new List<SA_ReceivableDetail>();
            foreach (var item in listCongNhom)
            {
                item.RefNo = "Cộng nhóm :";
                item.JournalMemo = listChiTiet.FirstOrDefault(n => n.AccountObjectID == item.AccountObjectID).AccountObjectName;
                item.ClosingCreditAmount = listChiTiet.Where(n => n.AccountObjectID == item.AccountObjectID).Sum(n => n.ClosingCreditAmount);
                item.ClosingDebitAmount = listChiTiet.Where(n => n.AccountObjectID == item.AccountObjectID).Sum(n => n.ClosingDebitAmount);
                listChiTiet.Add(item);
                SA_ReceivableDetail sA_ReceivableDetail = new SA_ReceivableDetail();
                sA_ReceivableDetail.IDGroup = 3;
                sA_ReceivableDetail.AccountObjectID = item.AccountObjectID;
                sA_ReceivableDetail.JournalMemo = item.JournalMemo;
                sA_ReceivableDetail.RefNo = "Tên nhân viên :";
                listChiTiet.Add(sA_ReceivableDetail);


            }
            decimal? SumDebitAmount = listCongNhom.Sum(n => n.DebitAmount);
            decimal? SumCreditAmount = listCongNhom.Sum(n => n.CreditAmount);
            decimal? SumClosingCreditAmountAmount = listCongNhom.Sum(n => n.ClosingCreditAmount);
            decimal? SumClosingDebitAmountAmount = listCongNhom.Sum(n => n.ClosingDebitAmount);
            //listtieude = listCongNhom.CloneObject<List<SA_ReceivableDetail>>();
            //foreach (var item in listtieude)
            //{
            //    item.RefNo = "Tên nhân viên :";
            //    item.IDGroup = 3;
            //    item.JournalMemo = listChiTiet.FirstOrDefault(n => n.AccountObjectID == item.AccountObjectID).AccountObjectName;
            //    item.ClosingCreditAmount = null;
            //    item.ClosingDebitAmount = null;
            //    item.DebitAmount = null;
            //    item.CreditAmount = null;
            //    listChiTiet.Add(item);

            //}
            var rD = new SA_ReceivableDetail_period();
            string txtThoiGian = "Tài khoản: " + acount;
            rD.Period = txtThoiGian + "; " + ReportUtils.GetPeriod(begin, end);
            listChiTiet = listChiTiet.OrderByDescending(n => n.AccountObjectID).ThenByDescending(n=>n.IDGroup).ToList();
            uGridDuLieu.DataSource = listChiTiet;
            Utils.ConfigGrid(uGridDuLieu, ConstDatabase.SA_ReceivableDetail_TableNameIn);
            uGridDuLieu.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            UltraGridBand parentBand = this.uGridDuLieu.DisplayLayout.Bands[0];
            parentBand.RowLayoutStyle = RowLayoutStyle.GroupLayout;
            UltraGridGroup parentBandGroup1 = parentBand.Groups.Add("ParentBandGroup1", "CHI TIẾT CÔNG NỢ NHÂN VIÊN");
            parentBandGroup1.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroup1.Header.Appearance.TextHAlign = HAlign.Center;

            UltraGridGroup parentBandGroup = parentBand.Groups.Add("ParentBandGroup", rD.Period);
            parentBandGroup.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroup.RowLayoutGroupInfo.ParentGroup = parentBandGroup1;
            parentBandGroup.Header.Appearance.TextHAlign = HAlign.Center;

            UltraGridGroup parentBandGroupPS = parentBand.Groups.Add("ParentBandGroupPS", "Phát sinh");
            parentBandGroupPS.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroupPS.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupPS.RowLayoutGroupInfo.ParentGroup = parentBandGroup;

            UltraGridGroup parentBandGroupSD = parentBand.Groups.Add("ParentBandGroupCK", "Số dư");
            parentBandGroupSD.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroupSD.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupSD.RowLayoutGroupInfo.ParentGroup = parentBandGroup;

            parentBandGroupSD.Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBandGroupSD.Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBandGroupSD.Header.Appearance.ForeColor = Color.Black;
            parentBandGroupSD.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupSD.Header.Appearance.BorderColor = Color.Black;

            parentBandGroupPS.Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPS.Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBandGroupPS.Header.Appearance.ForeColor = Color.Black;
            parentBandGroupPS.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPS.Header.Appearance.BorderColor = Color.Black;

            parentBandGroup.Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBandGroup.Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBandGroup.Header.Appearance.ForeColor = Color.Black;
            parentBandGroup.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroup.Header.Appearance.BorderColor = Color.Black;

            parentBandGroup1.Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBandGroup1.Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBandGroup1.Header.Appearance.ForeColor = Color.Black;
            parentBandGroup1.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroup1.Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[0].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[0].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[0].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[0].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[0].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[1].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[1].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[1].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[1].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[1].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[2].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[2].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[2].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[2].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[2].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[3].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[3].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[3].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[3].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[3].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[4].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[4].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[4].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[4].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[4].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[5].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[5].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[5].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[5].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[5].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[6].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[6].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[6].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[6].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[6].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[7].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[7].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[7].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[7].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[7].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[8].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[8].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[8].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[8].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[8].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[9].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[9].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[9].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[9].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[9].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[10].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[10].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[10].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[10].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[10].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[0].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[1].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[2].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[3].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[4].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[5].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[6].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[7].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[8].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[9].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[10].RowLayoutColumnInfo.ParentGroup = parentBandGroup;

            uGridDuLieu.DisplayLayout.Bands[0].Columns["DebitAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPS;
            uGridDuLieu.DisplayLayout.Bands[0].Columns["CreditAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPS;
            uGridDuLieu.DisplayLayout.Bands[0].Columns["ClosingDebitAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupSD;
            uGridDuLieu.DisplayLayout.Bands[0].Columns["ClosingCreditAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupSD;

            parentBand.Columns[1].RowLayoutColumnInfo.SpanY = parentBand.Columns[1].RowLayoutColumnInfo.SpanY + parentBandGroupPS.RowLayoutGroupInfo.SpanY;
            parentBand.Columns[0].RowLayoutColumnInfo.SpanY = parentBand.Columns[0].RowLayoutColumnInfo.SpanY + parentBandGroupPS.RowLayoutGroupInfo.SpanY;
            parentBand.Columns[2].RowLayoutColumnInfo.SpanY = parentBand.Columns[2].RowLayoutColumnInfo.SpanY + parentBandGroupPS.RowLayoutGroupInfo.SpanY;
            parentBand.Columns[3].RowLayoutColumnInfo.SpanY = parentBand.Columns[3].RowLayoutColumnInfo.SpanY + parentBandGroupPS.RowLayoutGroupInfo.SpanY;
            parentBand.Columns[4].RowLayoutColumnInfo.SpanY = parentBand.Columns[4].RowLayoutColumnInfo.SpanY + parentBandGroupPS.RowLayoutGroupInfo.SpanY;
            parentBand.Columns[5].RowLayoutColumnInfo.SpanY = parentBand.Columns[5].RowLayoutColumnInfo.SpanY + parentBandGroupPS.RowLayoutGroupInfo.SpanY;

            parentBand.Columns[0].RowLayoutColumnInfo.OriginX = 0;
            parentBand.Columns[0].RowLayoutColumnInfo.OriginY = 0;
            //parentBand.Columns[0].RowLayoutColumnInfo.SpanX = 40;
            parentBand.Columns[1].RowLayoutColumnInfo.OriginX = parentBand.Columns[0].RowLayoutColumnInfo.SpanX;
            parentBand.Columns[1].RowLayoutColumnInfo.OriginY = 0;
            //parentBand.Columns[1].RowLayoutColumnInfo.SpanX = 40;
            parentBand.Columns[2].RowLayoutColumnInfo.OriginX = parentBand.Columns[1].RowLayoutColumnInfo.OriginX + parentBand.Columns[1].RowLayoutColumnInfo.SpanX;
            parentBand.Columns[2].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns[3].RowLayoutColumnInfo.OriginX = parentBand.Columns[2].RowLayoutColumnInfo.OriginX + parentBand.Columns[2].RowLayoutColumnInfo.SpanX;
            parentBand.Columns[3].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns[4].RowLayoutColumnInfo.OriginX = parentBand.Columns[3].RowLayoutColumnInfo.OriginX + parentBand.Columns[3].RowLayoutColumnInfo.SpanX;
            parentBand.Columns[4].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns[5].RowLayoutColumnInfo.OriginX = parentBand.Columns[4].RowLayoutColumnInfo.OriginX + parentBand.Columns[4].RowLayoutColumnInfo.SpanX;
            parentBand.Columns[5].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns[6].RowLayoutColumnInfo.OriginX = parentBand.Columns[5].RowLayoutColumnInfo.OriginX + parentBand.Columns[5].RowLayoutColumnInfo.SpanX;
            parentBand.Columns[6].RowLayoutColumnInfo.OriginY = 0;

            parentBandGroupPS.RowLayoutGroupInfo.OriginX = parentBand.Columns[6].RowLayoutColumnInfo.OriginX + parentBand.Columns[6].RowLayoutColumnInfo.SpanX;
            parentBandGroupPS.RowLayoutGroupInfo.OriginY = 0;
            parentBandGroupPS.RowLayoutGroupInfo.SpanX = 300;
            parentBandGroupSD.RowLayoutGroupInfo.OriginX = parentBandGroupPS.RowLayoutGroupInfo.OriginX + parentBandGroupPS.RowLayoutGroupInfo.SpanX;
            parentBandGroupSD.RowLayoutGroupInfo.OriginY = 0;
            parentBandGroupSD.RowLayoutGroupInfo.SpanX = 300;

            parentBand.Columns["CreditAmount"].CellAppearance.TextHAlign = HAlign.Right;
            parentBand.Columns["DebitAmount"].CellAppearance.TextHAlign = HAlign.Right;
            parentBand.Columns["ClosingCreditAmount"].CellAppearance.TextHAlign = HAlign.Right;
            parentBand.Columns["ClosingDebitAmount"].CellAppearance.TextHAlign = HAlign.Right;

            parentBand.Columns["CreditAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            parentBand.Columns["DebitAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            parentBand.Columns["ClosingCreditAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            parentBand.Columns["ClosingDebitAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            SetDoSoAmVaInDam(uGridDuLieu);

            Utils.AddSumColumn(uGridDuLieu, "RefDate", false);
            Utils.AddSumColumn(uGridDuLieu, "RefNo", false);
            Utils.AddSumColumn(uGridDuLieu, "JournalMemo", false);
            Utils.AddSumColumn(uGridDuLieu, "AccountNumber", false);
            Utils.AddSumColumn(uGridDuLieu, "CorrespondingAccountNumber", false);

            uGridDuLieu.DisplayLayout.Bands[0].Summaries[1].DisplayFormat = " ";
            uGridDuLieu.DisplayLayout.Bands[0].Summaries[2].DisplayFormat = " ";
            uGridDuLieu.DisplayLayout.Bands[0].Summaries[3].DisplayFormat = " ";
            uGridDuLieu.DisplayLayout.Bands[0].Summaries[4].DisplayFormat = " ";
            uGridDuLieu.DisplayLayout.Bands[0].Summaries[5].DisplayFormat = " ";

            Utils.AddSumColumn1(SumCreditAmount, uGridDuLieu, "CreditAmount" ,false,"",1);
            Utils.AddSumColumn1(SumDebitAmount, uGridDuLieu, "DebitAmount", false, "", 1);
            Utils.AddSumColumn1(SumClosingCreditAmountAmount, uGridDuLieu, "ClosingCreditAmount", false, "", 1);
            Utils.AddSumColumn1(SumClosingDebitAmountAmount, uGridDuLieu, "ClosingDebitAmount", false, "", 1);


            this.WindowState = FormWindowState.Maximized;
            if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "IsMinimized").Data == "1")
            {
                this.WindowState = FormWindowState.Normal;


                this.Width = Screen.PrimaryScreen.WorkingArea.Width - 200;
                this.Height = Screen.PrimaryScreen.WorkingArea.Height - 50;
                this.Location = new System.Drawing.Point(((Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2), ((Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2));

            }
            if (Utils.isDemo)
            {
                ultraButton2.Visible = false;
            }
            WaitingFrm.StopWaiting();
        }
        private void SetDoSoAmVaInDam(UltraGrid ultraGrid)
        {
            foreach (var item in ultraGrid.Rows)
            {
                if ((int)item.Cells["IDGroup"].Value == 3)
                {
                   
                    item.Cells["RefNo"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    item.Cells["JournalMemo"].Appearance.FontData.Bold = DefaultableBoolean.True;


                }
                else
                {
                    if ((int)item.Cells["IDGroup"].Value == 1)
                    {
                        item.Cells["RefNo"].Appearance.FontData.Bold = DefaultableBoolean.True;
                        item.Cells["JournalMemo"].Appearance.FontData.Bold = DefaultableBoolean.True;
                        item.Cells["DebitAmount"].Appearance.FontData.Bold = DefaultableBoolean.True;
                        item.Cells["CreditAmount"].Appearance.FontData.Bold = DefaultableBoolean.True;
                        item.Cells["ClosingDebitAmount"].Appearance.FontData.Bold = DefaultableBoolean.True;
                        item.Cells["ClosingCreditAmount"].Appearance.FontData.Bold = DefaultableBoolean.True;

                    }
                   
                }
                if (item.Cells["CreditAmount"].Value != null)
                {
                    if ((decimal)item.Cells["CreditAmount"].Value < 0)
                        item.Cells["CreditAmount"].Appearance.ForeColor = Color.Red;
                }
                if (item.Cells["DebitAmount"].Value != null)
                {
                    if ((decimal)item.Cells["DebitAmount"].Value < 0)
                        item.Cells["DebitAmount"].Appearance.ForeColor = Color.Red;

                }
                if (item.Cells["ClosingDebitAmount"].Value != null)
                {
                    if ((decimal)item.Cells["ClosingDebitAmount"].Value < 0)
                        item.Cells["ClosingDebitAmount"].Appearance.ForeColor = Color.Red;

                }
                if (item.Cells["ClosingCreditAmount"].Value != null)
                {
                    if ((decimal)item.Cells["ClosingCreditAmount"].Value < 0)
                        item.Cells["ClosingCreditAmount"].Appearance.ForeColor = Color.Red;
                }
            }
        }

        private void GanTKDuNoTKDuCo(List<SA_ReceivableDetail> list)
        {
            foreach (var item in list)
            {
                if (!item.AccountNumber.IsNullOrEmpty())
                {
                    int? AccountGroupKind = Utils.ListAccount.FirstOrDefault(n => n.AccountNumber == item.AccountNumber).AccountGroupKind;
                    if (AccountGroupKind == null) { continue; }
                    if (AccountGroupKind == 0)
                    {
                        if (item.ClosingCreditAmount != null)
                        {
                            if (item.ClosingCreditAmount > 0)
                            {
                                item.ClosingDebitAmount = -item.ClosingCreditAmount;
                                item.ClosingCreditAmount = 0;
                            }
                        }
                    }
                    if (AccountGroupKind == 1)
                    {
                        if (item.ClosingDebitAmount != null)
                        {
                            if (item.ClosingDebitAmount > 0)
                            {
                                item.ClosingCreditAmount = -item.ClosingDebitAmount;
                                item.ClosingDebitAmount = 0;
                            }
                        }
                    }
                    if (AccountGroupKind == 3)
                    {
                        if (item.ClosingDebitAmount != null)
                        {
                            if (item.AccountNumber[0].ToInt() == 5)
                            {
                                if (item.ClosingDebitAmount > 0)
                                {
                                    item.ClosingCreditAmount = -item.ClosingDebitAmount;
                                    item.ClosingDebitAmount = 0;
                                }
                            }
                            if (item.AccountNumber[0].ToInt() == 7)
                            {
                                if (item.ClosingDebitAmount > 0)
                                {
                                    item.ClosingCreditAmount = -item.ClosingDebitAmount;
                                    item.ClosingDebitAmount = 0;
                                }
                            }
                        }
                        if (item.ClosingCreditAmount != null)
                        {
                            if (item.AccountNumber[0].ToInt() == 6)
                            {
                                if (item.ClosingCreditAmount > 0)
                                {
                                    item.ClosingDebitAmount = -item.ClosingCreditAmount;
                                    item.ClosingCreditAmount = 0;
                                }
                            }
                            if (item.AccountNumber[0].ToInt() == 8)
                            {
                                if (item.ClosingCreditAmount > 0)
                                {
                                    item.ClosingDebitAmount = -item.ClosingCreditAmount;
                                    item.ClosingCreditAmount = 0;
                                }
                            }
                        }
                    }

                }
            }

        }

        private void ultraButton3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ultraButton1_Click(object sender, EventArgs e)
        {
            ReportProcedureSDS sp = new ReportProcedureSDS();
            List<SA_ReceivableDetail> data = sp.GetCHICONGNOPHAITHUNV(begin1, end1, acount1, list_acount1, false);//editby cuongpv cbbCurrency.Value.ToString() -> currencyID
            GanTKDuNoTKDuCo(data);
            List<SA_ReceivableDetail> listCongNhom = data.Where(m => m.IDGroup == 1).ToList();
            List<SA_ReceivableDetail> listChiTiet = data.Where(m => m.IDGroup == 2).ToList();
            foreach (var item in listCongNhom)
            {
                item.ClosingCreditAmount = listChiTiet.Where(n => n.AccountObjectID == item.AccountObjectID).Sum(n => n.ClosingCreditAmount);
                item.ClosingDebitAmount = listChiTiet.Where(n => n.AccountObjectID == item.AccountObjectID).Sum(n => n.ClosingDebitAmount);
                
            }
            var rD = new SA_ReceivableDetail_period();
            string txtThoiGian = "Tài khoản: " + acount1;
            rD.Period = txtThoiGian + "; " + ReportUtils.GetPeriod(begin1, end1);
            var f = new ReportForm<SA_ReceivableDetail>(fileReportSlot1, "");
            f.AddDatasource("dbCongNhom", listCongNhom, true);
            f.AddDatasource("dbChiTiet", listChiTiet, true);
            f.AddDatasource("Detail", rD);
            f.LoadReport();
            f.WindowState = FormWindowState.Maximized;
            f.Show();
        }

        private void ultraButton2_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog
            {
                Title = "Chọn nơi sao lưu ",
                Filter = "File (*.xls)|*.xls",
                InitialDirectory = "@C:\\",
                FileName = "ChiTietCongNoNhanVien.xls"
            };
            if (save.ShowDialog() == DialogResult.OK)
            {
                string duongdan = save.FileName;
                try
                {
                    ultraGridExcelExporter1.Export(uGridDuLieu, duongdan);
                    if (MSG.Question("Kết xuất thành công, Bạn có muốn mở file vừa kết xuất không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(save.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
                catch
                {
                    MSG.Warning("Lỗi khi kết xuất !");
                }
            }
        }

        private void ultraGridExcelExporter1_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.ExcelExportInitializeRowEventArgs e)
        {

        }

        private void ultraGridExcelExporter1_InitializeColumn(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.InitializeColumnEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Column.Format))
            {
                e.ExcelFormatStr = e.Column.Format;
            }
        }

        private void uGridDuLieu_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            UltraGridCell cell = e.Row.Cells[0];
            cell.Row.Height = 25;
        }

        private void ultraGridExcelExporter1_SummaryCellExported(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.SummaryCellExportedEventArgs e)
        {
            Worksheet ws = e.CurrentWorksheet;
            WorksheetRow wsRow = ws.Rows[e.CurrentRowIndex];
            WorksheetCell wsCell = wsRow.Cells[e.CurrentColumnIndex];
            if (!e.Summary.SummaryText.Contains("Số dòng") && !e.Summary.SummaryText.Contains(" "))
            {
                string s = e.Summary.SummaryText.Trim('.');
                if (s.Contains("("))
                    wsCell.Value = -decimal.Parse(s.Trim('(').Trim(')'));
                else
                    wsCell.Value = decimal.Parse(s);

                wsCell.CellFormat.FormatString = "###,###,###,###,##0;(###,###,###,###,##0)";
            }
        }

        private void ultraGridExcelExporter1_BeginExport(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.BeginExportEventArgs e)
        {
            foreach (var row in uGridDuLieu.Rows)
            {
                foreach (var column in uGridDuLieu.DisplayLayout.Bands[0].Columns)
                {
                    e.Rows[row.Index].Cells[column].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                    e.Rows[row.Index].Cells[column].Appearance.BorderColor = Color.Black;
                    e.Rows[row.Index].Cells[column].Column.Width = 200;
                }
            }
            e.Rows.Band.Summaries[0].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[0].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[1].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[1].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[2].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[2].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[3].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[3].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[4].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[4].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[5].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[5].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[6].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[6].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[7].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[7].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[8].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[8].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[9].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[9].Appearance.BorderColor = Color.Black;

        }

        private void uGridDuLieu_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (e.Row.ListObject != null)
            { 
                if (e.Row.ListObject.HasProperty("RefType"))
                {
                    if (!e.Row.ListObject.GetProperty("RefType").IsNullOrEmpty())
                    {
                        int typeid = e.Row.ListObject.GetProperty("RefType").ToInt();

                        if (e.Row.ListObject.HasProperty("RefID"))
                        {
                            if (!e.Row.ListObject.GetProperty("RefID").IsNullOrEmpty())
                            {
                                Guid id = (Guid)e.Row.ListObject.GetProperty("RefID");
                                var f = Utils.ViewVoucherSelected1(id, typeid);
                              
                                // this.Close(); 
                                //if (f != null)
                                //{
                                //    if (f.IsDisposed)
                                //    {
                                        
                                //        new FRCHITIETCONGNOPHAITHUNHANVIENTruocIn(begin1, end1, acount1, list_acount1).Show();
                                //    }
                                //}
                            }
                        }
                    }

                }
        }
        }
    }
}
