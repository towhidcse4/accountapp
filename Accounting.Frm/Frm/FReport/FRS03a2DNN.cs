﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;

namespace Accounting
{
    public partial class FRS03a2DNN : CustormForm
    {
        readonly List<AccountReport> _lstAccountBankReport = new List<AccountReport>();
        string _subSystemCode;
        public FRS03a2DNN(string subSystemCode)
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();
            _subSystemCode = subSystemCode;
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\S03a2-DNN.rst", path);
            ReportUtils.LstAccountBankReport.Clear();
            _lstAccountBankReport = ReportUtils.LstAccountBankReport;
            ugridAccount.SetDataBinding(_lstAccountBankReport.ToList(), "");
            ReportUtils.ProcessControls(this);
            btnExit.Click += btnExit_Click;
            btnOk.Click += btnOk_Click;
            WaitingFrm.StopWaiting();
        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }
            var generalLedgerService = IoC.Resolve<IGeneralLedgerService>();
            List<string> lstAccountNumber = _lstAccountBankReport.Where(c => c.Check).Select(c => c.AccountNumber).ToList();
            if (lstAccountNumber.Count == 0)
            {
                MSG.Warning(resSystem.Report_01);
                return;
            }
            List<S03A2DNN> lstS03A2Dnns = new List<S03A2DNN>();
            if (lstAccountNumber.Count > 0)
            {
                foreach (var account in lstAccountNumber)
                {
                    lstS03A2Dnns = generalLedgerService.RS03A2DNN((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, account, ((Currency)cbbCurrency.SelectedRow.ListObject).ID, chk.Checked).ToList();
                }
            }
            //List<S03A1DNN> data = new List<S03A1DNN>();
            //for (int i = 0; i < 200; i++)
            //{
            //    data.Add(new S03A1DNN(i));
            //}
            var rD = new S03A2DNNDetail();
            rD.Period = ReportUtils.GetPeriod(DateTime.Now.AddDays(-300), DateTime.Now);
            var f = new ReportForm<S03A2DNN>(fileReportSlot1, _subSystemCode);
            f.AddDatasource("S03a2DNN", lstS03A2Dnns, true);
            f.AddDatasource("Detail", rD);
            f.LoadReport();
            f.WindowState = FormWindowState.Maximized;
            f.Show();
        }
    }
}
