﻿namespace Accounting.Frm.FReport
{
    partial class FRCPPeriod
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem4 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem5 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem6 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FRCPPeriod));
            this.Panel1 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.btnOk = new Infragistics.Win.Misc.UltraButton();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.uGridDS = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbMethod1 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.cbbPeriod = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.reportManager1 = new PerpetuumSoft.Reporting.Components.ReportManager(this.components);
            this.fileReportSlot1 = new PerpetuumSoft.Reporting.Components.FileReportSlot(this.components);
            this.Panel1.ClientArea.SuspendLayout();
            this.Panel1.SuspendLayout();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMethod1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPeriod)).BeginInit();
            this.SuspendLayout();
            // 
            // Panel1
            // 
            // 
            // Panel1.ClientArea
            // 
            this.Panel1.ClientArea.Controls.Add(this.ultraPanel1);
            this.Panel1.ClientArea.Controls.Add(this.uGridDS);
            this.Panel1.ClientArea.Controls.Add(this.ultraGroupBox2);
            this.Panel1.Location = new System.Drawing.Point(0, 0);
            this.Panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(1068, 553);
            this.Panel1.TabIndex = 7;
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.btnOk);
            this.ultraPanel1.ClientArea.Controls.Add(this.btnClose);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 484);
            this.ultraPanel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(1068, 69);
            this.ultraPanel1.TabIndex = 49;
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.Image = global::Accounting.Properties.Resources.apply_16;
            appearance1.ImageHAlign = Infragistics.Win.HAlign.Left;
            this.btnOk.Appearance = appearance1;
            this.btnOk.Location = new System.Drawing.Point(781, 18);
            this.btnOk.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(131, 37);
            this.btnOk.TabIndex = 4;
            this.btnOk.Text = "Đồng ý";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.Image = global::Accounting.Properties.Resources.cancel_16;
            this.btnClose.Appearance = appearance2;
            this.btnClose.Location = new System.Drawing.Point(920, 18);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(131, 37);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "Hủy bỏ";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // uGridDS
            // 
            this.uGridDS.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridDS.Location = new System.Drawing.Point(0, 134);
            this.uGridDS.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.uGridDS.Name = "uGridDS";
            this.uGridDS.Size = new System.Drawing.Size(1064, 342);
            this.uGridDS.TabIndex = 48;
            this.uGridDS.TabStop = false;
            this.uGridDS.Text = "ultraGrid2";
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.cbbMethod1);
            this.ultraGroupBox2.Controls.Add(this.cbbPeriod);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel3);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel5);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            appearance5.FontData.BoldAsString = "True";
            appearance5.FontData.SizeInPoints = 10F;
            this.ultraGroupBox2.HeaderAppearance = appearance5;
            this.ultraGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(1068, 138);
            this.ultraGroupBox2.TabIndex = 47;
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbMethod1
            // 
            this.cbbMethod1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbMethod1.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            valueListItem1.DataValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            valueListItem1.DisplayText = "Phương pháp giản đơn";
            valueListItem2.DataValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            valueListItem2.DisplayText = "Phương pháp hệ số";
            valueListItem3.DataValue = new decimal(new int[] {
            2,
            0,
            0,
            0});
            valueListItem3.DisplayText = "Phương pháp tỷ lệ";
            valueListItem4.DataValue = new decimal(new int[] {
            3,
            0,
            0,
            0});
            valueListItem4.DisplayText = "Công trình, vụ việc, quy trình công nghệ sản xuất";
            valueListItem5.DataValue = new decimal(new int[] {
            4,
            0,
            0,
            0});
            valueListItem5.DisplayText = "Đơn hàng";
            valueListItem6.DataValue = new decimal(new int[] {
            5,
            0,
            0,
            0});
            valueListItem6.DisplayText = "Hợp đồng";
            this.cbbMethod1.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2,
            valueListItem3,
            valueListItem4,
            valueListItem5,
            valueListItem6});
            this.cbbMethod1.Location = new System.Drawing.Point(212, 12);
            this.cbbMethod1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbbMethod1.Name = "cbbMethod1";
            this.cbbMethod1.Size = new System.Drawing.Size(377, 24);
            this.cbbMethod1.TabIndex = 68;
            this.cbbMethod1.ValueChanged += new System.EventHandler(this.cbbMethod1_ValueChanged);
            // 
            // cbbPeriod
            // 
            this.cbbPeriod.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbPeriod.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbPeriod.Location = new System.Drawing.Point(16, 74);
            this.cbbPeriod.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbbPeriod.Name = "cbbPeriod";
            this.cbbPeriod.NullText = "<chọn dữ liệu>";
            this.cbbPeriod.Size = new System.Drawing.Size(1048, 25);
            this.cbbPeriod.TabIndex = 66;
            this.cbbPeriod.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbPeriod_RowSelected);
            // 
            // ultraLabel3
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextHAlignAsString = "Left";
            appearance3.TextVAlignAsString = "Bottom";
            this.ultraLabel3.Appearance = appearance3;
            this.ultraLabel3.Location = new System.Drawing.Point(12, 17);
            this.ultraLabel3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(176, 21);
            this.ultraLabel3.TabIndex = 65;
            this.ultraLabel3.Text = "Loại/ Phương pháp";
            // 
            // ultraLabel5
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextHAlignAsString = "Left";
            appearance4.TextVAlignAsString = "Bottom";
            this.ultraLabel5.Appearance = appearance4;
            this.ultraLabel5.Location = new System.Drawing.Point(12, 46);
            this.ultraLabel5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(177, 21);
            this.ultraLabel5.TabIndex = 63;
            this.ultraLabel5.Text = "Chọn kỳ tính giá thành";
            // 
            // reportManager1
            // 
            this.reportManager1.DataSources = new PerpetuumSoft.Reporting.Components.ObjectPointerCollection(new string[0], new object[0]);
            this.reportManager1.OwnerForm = this;
            this.reportManager1.Reports.AddRange(new PerpetuumSoft.Reporting.Components.ReportSlot[] {
            this.fileReportSlot1});
            // 
            // fileReportSlot1
            // 
            this.fileReportSlot1.FilePath = "";
            this.fileReportSlot1.ReportName = "";
            this.fileReportSlot1.ReportScriptType = typeof(PerpetuumSoft.Reporting.Rendering.ReportScriptBase);
            // 
            // FRCPPeriod
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 554);
            this.Controls.Add(this.Panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FRCPPeriod";
            this.Text = "Thẻ tính giá thành";
            this.Panel1.ClientArea.ResumeLayout(false);
            this.Panel1.ResumeLayout(false);
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridDS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ultraGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMethod1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPeriod)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel Panel1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDS;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbPeriod;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraButton btnOk;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private PerpetuumSoft.Reporting.Components.ReportManager reportManager1;
        private PerpetuumSoft.Reporting.Components.FileReportSlot fileReportSlot1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbMethod1;
    }
}