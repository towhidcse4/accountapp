﻿using Accounting.Core.DAO;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.Report;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRSOCHITIETTIENVAY : Form
    {
        readonly List<AccountReport> _lstAccountReport = ReportUtils.LstAccountTienVay;
        List<AccountingObjectReport> _lstAccountingObjectReport = new List<AccountingObjectReport>();
        private readonly List<AccountingObjectReport> _lstnew = ReportUtils.AccountingOJReportKH_NCC_NV_Khac;
        private string _subSystemCode;
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }

        public FRSOCHITIETTIENVAY()
        {
            InitializeComponent();
            ugridAccount.SetDataBinding(_lstAccountReport.ToList(), "");
            _lstAccountingObjectReport = _lstnew;
            ugridAccountingObject.SetDataBinding(_lstAccountingObjectReport.OrderBy(n => n.AccountingObjectCode).ToList(), "");
            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\SoChiTietTienVay.rst", path);
            ReportUtils.ProcessControls(this);
            UltraGridBand band = ugridAccountingObject.DisplayLayout.Bands[0];
            //foreach (var item in band.Columns)
            //{
            //    if (item.Header.Caption == "Mã đối tượng")
            //    {
            //        item.Header.Caption = "Mã NCC";
            //    }
            //    if (item.Header.Caption == "Tên đối tượng")
            //    {
            //        item.Header.Caption = "Tên NCC";
            //    }
            //}
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dtBeginDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }

            if((DateTime)dtBeginDate.Value > (DateTime)dtEndDate.Value)
            {
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            ReportProcedureSDS sp = new ReportProcedureSDS();
            List<GLBookDetailMoneyBorrow> data = new List<GLBookDetailMoneyBorrow>();
            List<string> lstGuids = _lstAccountReport.Where(c => c.Check).Select(c => c.AccountNumber).ToList();
            if(lstGuids.Count == 0)
            {
                MSG.Warning("Bạn chưa chọn tài khoản");
                return;
            }
            var list_ncc = _lstAccountingObjectReport.Where(t => t.Check).Select(v => v.ID).ToList();
            if (list_ncc.Count == 0)
            {
                MSG.Error("Bạn chưa chọn nhà cung cấp");
                return;
            }
            string listtk = "";
            string listncc = "";
            listtk = "," + string.Join(",", lstGuids) + ",";
            listncc = "," + string.Join(",", list_ncc) + ",";
            data = sp.Get_SoChiTietTienVay((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, listtk, listncc);
            //data = data.Where(t => t.CreditAmount != 0 || t.DebitAmount != 0).ToList(); comment by cuongpv
            if (data.Count == 0)
            {
                MSG.Warning("Không có dữ liệu để lên báo cáo");
            }
            else
            {
                var rD = new GLBookDetailMoneyBorrowDetail();
                var f = new ReportForm<GLBookDetailMoneyBorrow>(fileReportSlot1, _subSystemCode);
                rD.Period = ReportUtils.GetPeriod((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value);
                f.AddDatasource("DBSOCHITV", data);
                f.AddDatasource("Detail", rD);
                f.SetHyperlinkFunction("Accounting.ReportUtils", "Hyperlink");
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
                //string a = "";//comment by cuongpv
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            foreach (var item in ugridAccount.Rows)
            {
                item.Cells["Check"].Value = false;
            }
            foreach (var item in ugridAccountingObject.Rows)
            {
                item.Cells["Check"].Value = false;
            }
            Close();
        }

        private void Form_FormClosed(object sender, FormClosedEventArgs e)
        {
            foreach (var item in ugridAccount.Rows)
            {
                item.Cells["Check"].Value = false;
            }
            foreach (var item in ugridAccountingObject.Rows)
            {
                item.Cells["Check"].Value = false;
            }
        }
    }
}
