﻿using Accounting.Core.DAO;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRCPPeriod : CustormForm
    {
        #region các hàm service, list sử dụng
        private ICPPeriodService _ICPPeriodService
        {
            get { return IoC.Resolve<ICPPeriodService>(); }
        }
        private ICPPeriodDetailService ICPPeriodDetailService
        {
            get { return IoC.Resolve<ICPPeriodDetailService>(); }
        }
        private IMaterialGoodsService _IMaterialGoodsService
        {
            get { return IoC.Resolve<IMaterialGoodsService>(); }
        }
        private ICPResultService ICPResultService
        {
            get { return IoC.Resolve<ICPResultService>(); }
        }
        private ICPUncompleteService ICPUncompleteService
        {
            get { return IoC.Resolve<ICPUncompleteService>(); }
        }
        private ICPUncompleteDetailService ICPUncompleteDetailService
        {
            get { return IoC.Resolve<ICPUncompleteDetailService>(); }
        }
        private ICPExpenseListService ICPExpenseListService
        {
            get { return IoC.Resolve<ICPExpenseListService>(); }
        }
        private ICPAllocationGeneralExpenseService ICPAllocationGeneralExpenseService
        {
            get { return IoC.Resolve<ICPAllocationGeneralExpenseService>(); }
        }
        private ICPAllocationGeneralExpenseDetailService ICPAllocationGeneralExpenseDetailService
        {
            get { return IoC.Resolve<ICPAllocationGeneralExpenseDetailService>(); }
        }

        List<CPPeriod> lstPeriod;
        List<CPPeriod> lstPeriod2;
        List<CostSet> lstCostSet;
        List<CostSet> lstCostSet2;
        List<Guid> _ListCostSet;
        List<Guid> _ListCostSet2;
        List<SelectCostSet> lstSCostset;
        List<CPExpenseList> lstExpenseList;
        List<ExpenseItem> lstExpenseItem;
        List<RSInwardOutwardDetail> lstRSInwardOutwardDetail;
        List<CPAllocationGeneralExpenseDetail> lstCPAllocationGeneralExpenseDetail;
        List<CPAllocationGeneralExpense> lstAllocationGeneralExpense;


        static Dictionary<int, string> DicCostSetType;
        string _subSystemCode;
        string path;
        string linkReport012 = "{0}\\Frm\\FReport\\Template\\GiaThanhGD-HS-TL.rst";
        string linkReport345 = "{0}\\Frm\\FReport\\Template\\GiaThanhCT-DH-HD.rst";

        public static Dictionary<int, string> dicCostSetType
        {
            get { DicCostSetType = DicCostSetType ?? (Dictionary<int, string>)BuildConfig(1); return DicCostSetType; }
        }

        #endregion
        public FRCPPeriod(string subSystemCode)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            _subSystemCode = subSystemCode;
            lstExpenseList = ICPExpenseListService.GetAll();
            lstExpenseItem = Utils.ListExpenseItem.ToList();
            lstRSInwardOutwardDetail = Utils.ListRSInwardOutwardDetail.ToList();
            lstCPAllocationGeneralExpenseDetail = ICPAllocationGeneralExpenseDetailService.GetAll();
            lstAllocationGeneralExpense = ICPAllocationGeneralExpenseService.GetAll();

            path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");

            lstPeriod = new List<CPPeriod>();
            lstPeriod = _ICPPeriodService.GetAll();
            lstPeriod = lstPeriod.GroupBy(x => x.Type).Select(y => y.First()).ToList();

            lstPeriod2 = new List<CPPeriod>();
            cbbPeriod.DataSource = lstPeriod2;
            Utils.ConfigGrid(cbbPeriod, ConstDatabase.CPPeriod_TableName);
            cbbPeriod.DisplayLayout.Bands[0].Columns["TypeString"].Hidden = true;
            cbbPeriod.DisplayMember = "Name";
            cbbPeriod.ValueMember = "Name";

            lstCostSet = new List<CostSet>();
            uGridDS.DataSource = new BindingList<CostSet>(lstCostSet);
            Utils.ConfigGrid(uGridDS, ConstDatabase.CostSet_TableName);
            uGridDS.DisplayLayout.Bands[0].Columns["Description"].Hidden = true;
            uGridDS.DisplayLayout.Bands[0].Columns["IsActive"].Hidden = true;
            uGridDS.DisplayLayout.Bands[0].Columns["CostSetTypeView"].Hidden = true;

        }

        private void cbbMethod1_ValueChanged(object sender, EventArgs e)
        {
            lstPeriod2 = new List<CPPeriod>();
            lstPeriod2 = Utils.ListCPPeriod.Where(x => x.Type == cbbMethod1.SelectedIndex).OrderByDescending(x => x.FromDate).ToList();
            cbbPeriod.DataSource = lstPeriod2;
            Utils.ConfigGrid(cbbPeriod, ConstDatabase.CPPeriod_TableName);
            cbbPeriod.DisplayLayout.Bands[0].Columns["TypeString"].Hidden = true;
            cbbPeriod.DisplayMember = "Name";
            cbbPeriod.ValueMember = "Name";

            lstCostSet = new List<CostSet>();
            if (cbbMethod1.SelectedIndex == 5)
                uGridDS.DataSource = new BindingList<EMContract>(new List<EMContract>());
            else
                uGridDS.DataSource = new BindingList<CostSet>(lstCostSet);

            var gridBand = uGridDS.DisplayLayout.Bands[0];

            if (cbbMethod1.SelectedIndex == 0)
            {
                Utils.ConfigGrid(uGridDS, ConstDatabase.CostSet_TableName);
                uGridDS.DisplayLayout.Bands[0].Columns["Description"].Hidden = true;
                uGridDS.DisplayLayout.Bands[0].Columns["IsActive"].Hidden = true;
                uGridDS.DisplayLayout.Bands[0].Columns["CostSetTypeView"].Hidden = true;
                gridBand.Columns["Description"].Hidden = true;
                gridBand.Columns["IsActive"].Hidden = true;
                gridBand.Columns["CostSetTypeView"].Hidden = true;
            }
            else if (cbbMethod1.SelectedIndex == 1 || cbbMethod1.SelectedIndex == 2)
            {
                List<SelectCostSet> listcostset = new List<SelectCostSet>();
                uGridDS.DataSource = new BindingList<SelectCostSet>(listcostset);
                Utils.ConfigGrid(uGridDS, ConstDatabase.SelectCostSet_TableName);
            }
            else if (cbbMethod1.SelectedIndex == 3)
            {
                Utils.ConfigGrid(uGridDS, ConstDatabase.CostSet_TableName);
                gridBand.Columns["Description"].Hidden = true;
                gridBand.Columns["IsActive"].Hidden = true;
                gridBand.Columns["CostSetTypeView"].Hidden = true;
                gridBand.Columns["CostSetCode"].Header.Caption = "Mã đối tượng";
                gridBand.Columns["CostSetName"].Header.Caption = "Tên đối tượng";
                gridBand.Columns["Select"].Header.VisiblePosition = 0;
                gridBand.Columns["Select"].Hidden = false;
                gridBand.Columns["Select"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
                gridBand.Columns["Select"].AutoSizeMode = ColumnAutoSizeMode.AllRowsInBand;
                gridBand.Columns["Select"].CellClickAction = CellClickAction.Edit;
                uGridDS.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
                uGridDS.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
                gridBand.Columns["Select"].Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
                gridBand.Columns["Select"].Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
                gridBand.Columns["Select"].Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
                gridBand.Columns["Select"].Header.Fixed = true;
            }
            else if (cbbMethod1.SelectedIndex == 4)
            {
                Utils.ConfigGrid(uGridDS, ConstDatabase.CostSet_TableName);
                gridBand.Columns["Description"].Hidden = true;
                gridBand.Columns["IsActive"].Hidden = true;
                gridBand.Columns["CostSetTypeView"].Hidden = true;
                gridBand.Columns["CostSetCode"].Header.Caption = "Số đơn hàng";
                gridBand.Columns["CostSetName"].Header.Caption = "Tên đơn hàng";
                gridBand.Columns["Select"].Header.VisiblePosition = 0;
                gridBand.Columns["Select"].Hidden = false;
                gridBand.Columns["Select"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
                gridBand.Columns["Select"].AutoSizeMode = ColumnAutoSizeMode.AllRowsInBand;
                gridBand.Columns["Select"].CellClickAction = CellClickAction.Edit;
                uGridDS.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
                uGridDS.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
                gridBand.Columns["Select"].Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
                gridBand.Columns["Select"].Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
                gridBand.Columns["Select"].Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
                gridBand.Columns["Select"].Header.Fixed = true;
            }
            else if (cbbMethod1.SelectedIndex == 5)
            {
                Utils.ConfigGrid(uGridDS, ConstDatabase.EMContract_TableName);
                foreach (var col in uGridDS.DisplayLayout.Bands[0].Columns)
                {
                    if (col.Key == "Select" || col.Key == "Code" || col.Key == "SignedDate" || col.Key == "AccountingObjectName")
                        col.Hidden = false;
                    else
                        col.Hidden = true;
                }
                var gridBands = uGridDS.DisplayLayout.Bands[0];
                gridBands.Columns["Select"].Header.VisiblePosition = 0;
                gridBands.Columns["Select"].Hidden = false;
                gridBands.Columns["Select"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
                gridBands.Columns["Select"].Width = 100;
                gridBands.Columns["Select"].CellClickAction = CellClickAction.Edit;
                //uGridDS.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
                uGridDS.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
                gridBands.Columns["Select"].Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
                gridBands.Columns["Select"].Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
                gridBands.Columns["Select"].Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
                gridBands.Columns["Select"].Header.Fixed = true;
                SummarySettings summary = gridBands.Summaries.Add("Count1", SummaryType.Count, gridBands.Columns["Select"]);
                summary.DisplayFormat = "Số dòng = {0:N0}";
                summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
                summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed | SummaryDisplayAreas.GroupByRowsFooter;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (!checkerror()) return;

            List<RCost> data = new List<RCost>();

            List<CPOPN> cpo = Utils.ListCPOPN.ToList();
            List<CPResult> cpr = ICPResultService.GetAll();
            List<CPResult> cpr1 = new List<CPResult>();
            List<CPUncomplete> cpu = ICPUncompleteService.GetAll();
            List<RSInwardOutward> io = Utils.ListRSInwardOutward.ToList();
            List<RSInwardOutwardDetail> iod = Utils.ListRSInwardOutwardDetail.ToList();
            List<CPUncompleteDetail> cpud = ICPUncompleteDetailService.GetAll();
            List<MaterialGoods> lstmg = Utils.ListMaterialGoods.ToList();
            List<CPPeriodDetail> cPPeriodDetails = ICPPeriodDetailService.GetAll();
            var ddSoGia = Utils.ListSystemOption.FirstOrDefault(c => c.Code == "DDSo_TienVND");
            int lamTronGia = 0;
            if (ddSoGia != null)
                lamTronGia = Convert.ToInt32(ddSoGia.Data);

            var md = cbbPeriod.SelectedRow.ListObject as CPPeriod ?? null;
            cpr = cpr.Where(x => x.CPPeriodID == md.ID).ToList();
            var lst5 = (from a in cpr
                        group a by new { a.CostSetID } into t
                        select new
                        {
                            CostSetID = t.Key.CostSetID,
                            DirectMaterialAmount = t.Sum(x => x.DirectMaterialAmount),
                            DirectLaborAmount = t.Sum(x => x.DirectLaborAmount),
                            GeneralExpensesAmount = t.Sum(x => x.GeneralExpensesAmount),
                            TotalCostAmount = t.Sum(x => x.TotalCostAmount)
                        }).ToList();
            foreach (var model in lst5)
            {
                CPResult item = new CPResult();
                item.CostSetID = model.CostSetID;
                item.DirectMaterialAmount = model.DirectMaterialAmount;
                item.DirectLaborAmount = model.DirectLaborAmount;
                item.GeneralExpensesAmount = model.GeneralExpensesAmount;
                item.TotalCostAmount = Math.Round(model.DirectMaterialAmount, lamTronGia) + Math.Round(model.DirectLaborAmount, lamTronGia) + Math.Round(model.GeneralExpensesAmount, lamTronGia);
                cpr1.Add(item);
            }

            if (md.Type == 0)
            {
                lstCostSet2 = new List<CostSet>();
                _ListCostSet2 = new List<Guid>();
                for (int i = 0; i < uGridDS.Rows.Count; i++)
                {
                    if (uGridDS.Rows[i].Cells["Select"].Value == null)
                    {
                        uGridDS.Rows[i].Cells["Select"].Value = false;
                    }
                    if (bool.Parse(uGridDS.Rows[i].Cells["Select"].Value.ToString()) == true)
                    {
                        CostSet cs = uGridDS.Rows[i].ListObject as CostSet;
                        lstCostSet2.Add(cs);
                        _ListCostSet2.Add(cs.ID);
                    }
                }

                List<CPExpenseList> lstCPExpenseList1 = (from g in lstExpenseList
                                                         join h in lstExpenseItem on g.ExpenseItemID equals h.ID
                                                         where g.TypeVoucher == 0 && h.ExpenseType == 0 && _ListCostSet2.Any(x => x == g.CostSetID) && g.CPPeriodID == md.ID
                                                         select g).ToList();
                List<CPExpenseList> lstCPExpenseList2 = (from g in lstExpenseList
                                                         join h in lstExpenseItem on g.ExpenseItemID equals h.ID
                                                         where g.TypeVoucher == 1 && h.ExpenseType == 0 && _ListCostSet2.Any(x => x == g.CostSetID) && g.CPPeriodID == md.ID
                                                         select g).ToList();
                List<CPExpenseList> lstCPExpenseList3 = (from g in lstExpenseList
                                                         join h in lstExpenseItem on g.ExpenseItemID equals h.ID
                                                         where g.TypeVoucher == 0 && h.ExpenseType == 1 && _ListCostSet2.Any(x => x == g.CostSetID) && g.CPPeriodID == md.ID
                                                         select g).ToList();
                List<CPExpenseList> lstCPExpenseList4 = (from g in lstExpenseList
                                                         join h in lstExpenseItem on g.ExpenseItemID equals h.ID
                                                         where g.TypeVoucher == 1 && h.ExpenseType == 1 && _ListCostSet2.Any(x => x == g.CostSetID) && g.CPPeriodID == md.ID
                                                         select g).ToList();
                List<CPExpenseList> lstCPExpenseList5 = (from g in lstExpenseList
                                                         join h in lstExpenseItem on g.ExpenseItemID equals h.ID
                                                         where g.TypeVoucher == 0 && h.ExpenseType == 2 && _ListCostSet2.Any(x => x == g.CostSetID) && g.CPPeriodID == md.ID
                                                         select g).ToList();
                List<CPExpenseList> lstCPExpenseList6 = (from g in lstExpenseList
                                                         join h in lstExpenseItem on g.ExpenseItemID equals h.ID
                                                         where g.TypeVoucher == 1 && h.ExpenseType == 2 && _ListCostSet2.Any(x => x == g.CostSetID) && g.CPPeriodID == md.ID
                                                         select g).ToList();
                List<CPAllocationGeneralExpenseDetail> lstCPAllocationGeneralExpenseDetail1 = (from g in lstCPAllocationGeneralExpenseDetail
                                                                                               join i in lstAllocationGeneralExpense on g.CPAllocationGeneralExpenseID equals i.ID
                                                                                               join h in lstExpenseItem on g.ExpenseItemID equals h.ID
                                                                                               where h.ExpenseType == 0 && _ListCostSet2.Any(x => x == g.CostSetID)
                                                                                               && i.CPPeriodID == md.ID
                                                                                               select g).ToList();
                List<CPAllocationGeneralExpenseDetail> lstCPAllocationGeneralExpenseDetail2 = (from g in lstCPAllocationGeneralExpenseDetail
                                                                                               join i in lstAllocationGeneralExpense on g.CPAllocationGeneralExpenseID equals i.ID
                                                                                               join h in lstExpenseItem on g.ExpenseItemID equals h.ID
                                                                                               where h.ExpenseType == 1 && _ListCostSet2.Any(x => x == g.CostSetID)
                                                                                               && i.CPPeriodID == md.ID
                                                                                               select g).ToList();
                List<CPAllocationGeneralExpenseDetail> lstCPAllocationGeneralExpenseDetail3 = (from g in lstCPAllocationGeneralExpenseDetail
                                                                                               join i in lstAllocationGeneralExpense on g.CPAllocationGeneralExpenseID equals i.ID
                                                                                               join h in lstExpenseItem on g.ExpenseItemID equals h.ID
                                                                                               where h.ExpenseType == 2 && _ListCostSet2.Any(x => x == g.CostSetID)
                                                                                               && i.CPPeriodID == md.ID
                                                                                               select g).ToList();

                List<CPPeriod> periods = lstPeriod2.Where(x => x.ToDate < md.FromDate).OrderByDescending(x => x.ToDate).ToList();
                if (periods.Count > 0)
                {
                    if (lstCostSet2.Count > 0)
                    {
                        foreach (var item in lstCostSet2)
                        {
                            CPPeriod period1 = (from g in periods join h in cPPeriodDetails on g.ID equals h.CPPeriodID where h.CostSetID == item.ID select g).FirstOrDefault();
                            if (period1 != null)
                            {
                                List<CPUncomplete> cpu1 = cpu.Where(x => x.CPPeriodID == period1.ID).ToList();
                                if (cpu1.Count > 0)
                                {
                                    foreach (var item2 in cpu1)
                                    {
                                        if (item.ID == item2.CostSetID)
                                        {
                                            RCost data2 = new RCost();
                                            data2.FromDate = md.FromDate;
                                            data2.ToDate = md.ToDate;
                                            data2.ID = (Guid)item2.CostSetID;
                                            data2.CostSetName = item.CostSetName;
                                            data2.UncompleteFirstAmount = Math.Round(item2.DirectMaterialAmount, lamTronGia) + Math.Round(item2.GeneralExpensesAmount, lamTronGia) + Math.Round(item2.DirectLaborAmount, lamTronGia);
                                            data2.UncompleteFirstMaterial = (Decimal)item2.DirectMaterialAmount;
                                            data2.UncompleteFirstLabor = (Decimal)item2.DirectLaborAmount;
                                            data2.UncompleteFirstCost = (Decimal)item2.GeneralExpensesAmount;
                                            data.Add(data2);
                                        }
                                    }
                                }
                                else
                                {
                                    if (cpo.Count > 0)
                                    {
                                        foreach (var item2 in cpo)
                                        {
                                            if (item.ID == item2.CostSetID)
                                            {
                                                RCost data2 = new RCost();
                                                data2.FromDate = md.FromDate;
                                                data2.ToDate = md.ToDate;
                                                data2.ID = (Guid)item2.CostSetID;
                                                data2.CostSetName = item.CostSetName;
                                                data2.UncompleteFirstAmount = Math.Round(item2.DirectMaterialAmount??0, lamTronGia) + Math.Round(item2.GeneralExpensesAmount??0, lamTronGia) + Math.Round(item2.DirectLaborAmount??0, lamTronGia);
                                                data2.UncompleteFirstMaterial = (Decimal)item2.DirectMaterialAmount;
                                                data2.UncompleteFirstLabor = (Decimal)item2.DirectLaborAmount;
                                                data2.UncompleteFirstCost = (Decimal)item2.GeneralExpensesAmount;
                                                data.Add(data2);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        RCost data2 = new RCost();
                                        data2.FromDate = md.FromDate;
                                        data2.ToDate = md.ToDate;
                                        data2.ID = item.ID;
                                        data2.CostSetName = item.CostSetName;
                                        data2.UncompleteFirstAmount = 0;
                                        data2.UncompleteFirstMaterial = 0;
                                        data2.UncompleteFirstLabor = 0;
                                        data2.UncompleteFirstCost = 0;
                                        data.Add(data2);
                                    }
                                }
                            }
                            else
                            {
                                if (cpo.Count > 0)
                                {
                                    foreach (var item2 in cpo)
                                    {
                                        if (item.ID == item2.CostSetID)
                                        {
                                            RCost data2 = new RCost();
                                            data2.FromDate = md.FromDate;
                                            data2.ToDate = md.ToDate;
                                            data2.ID = (Guid)item2.CostSetID;
                                            data2.CostSetName = item.CostSetName;
                                            data2.UncompleteFirstAmount = Math.Round(item2.DirectMaterialAmount ?? 0, lamTronGia) + Math.Round(item2.GeneralExpensesAmount ?? 0, lamTronGia) + Math.Round(item2.DirectLaborAmount ?? 0, lamTronGia);
                                            data2.UncompleteFirstMaterial = (Decimal)item2.DirectMaterialAmount;
                                            data2.UncompleteFirstLabor = (Decimal)item2.DirectLaborAmount;
                                            data2.UncompleteFirstCost = (Decimal)item2.GeneralExpensesAmount;
                                            data.Add(data2);
                                        }
                                    }
                                }
                                else
                                {
                                    RCost data2 = new RCost();
                                    data2.FromDate = md.FromDate;
                                    data2.ToDate = md.ToDate;
                                    data2.ID = item.ID;
                                    data2.CostSetName = item.CostSetName;
                                    data2.UncompleteFirstAmount = 0;
                                    data2.UncompleteFirstMaterial = 0;
                                    data2.UncompleteFirstLabor = 0;
                                    data2.UncompleteFirstCost = 0;
                                    data.Add(data2);
                                }
                            }
                        }
                    }
                    else
                    {
                        MSG.Warning("Bạn chưa chọn đối tượng ở danh sách để xem thẻ tính giá thành !");
                        return;
                    }
                }
                else
                {
                    if (lstCostSet2.Count > 0)
                    {
                        foreach (var item in lstCostSet2)
                        {
                            if (cpo.Count > 0)
                            {
                                foreach (var item2 in cpo)
                                {
                                    if (item.ID == item2.CostSetID)
                                    {
                                        RCost data2 = new RCost();
                                        data2.FromDate = md.FromDate;
                                        data2.ToDate = md.ToDate;
                                        data2.ID = (Guid)item2.CostSetID;
                                        data2.CostSetName = item.CostSetName;
                                        data2.UncompleteFirstAmount = Math.Round(item2.DirectMaterialAmount ?? 0, lamTronGia) + Math.Round(item2.GeneralExpensesAmount ?? 0, lamTronGia) + Math.Round(item2.DirectLaborAmount ?? 0, lamTronGia);
                                        data2.UncompleteFirstMaterial = (Decimal)item2.DirectMaterialAmount;
                                        data2.UncompleteFirstLabor = (Decimal)item2.DirectLaborAmount;
                                        data2.UncompleteFirstCost = (Decimal)item2.GeneralExpensesAmount;
                                        data.Add(data2);
                                    }
                                }
                            }
                            else
                            {
                                RCost data2 = new RCost();
                                data2.FromDate = md.FromDate;
                                data2.ToDate = md.ToDate;
                                data2.ID = item.ID;
                                data2.CostSetName = item.CostSetName;
                                data2.UncompleteFirstAmount = 0;
                                data2.UncompleteFirstMaterial = 0;
                                data2.UncompleteFirstLabor = 0;
                                data2.UncompleteFirstCost = 0;
                                data.Add(data2);
                            }
                        }
                    }
                    else
                    {
                        MSG.Warning("Bạn chưa chọn đối tượng ở danh sách để xem thẻ tính giá thành !");
                        return;
                    }
                }

                foreach(var item in data)
                {
                    Decimal amount = (from g in lstCPExpenseList1 where g.CostSetID == item.ID select g.Amount).Sum();
                    Decimal amount2 = (from g in lstCPExpenseList2 where g.CostSetID == item.ID select g.Amount).Sum();
                    Decimal amount3 = (from g in lstCPExpenseList3 where g.CostSetID == item.ID select g.Amount).Sum();
                    Decimal amount4 = (from g in lstCPExpenseList4 where g.CostSetID == item.ID select g.Amount).Sum();
                    Decimal amount5 = (from g in lstCPExpenseList5 where g.CostSetID == item.ID select g.Amount).Sum();
                    Decimal amount6 = (from g in lstCPExpenseList6 where g.CostSetID == item.ID select g.Amount).Sum();
                    Decimal allocatedAmount = (from g in lstCPAllocationGeneralExpenseDetail1 where g.CostSetID == item.ID select g.AllocatedAmount).Sum();
                    Decimal allocatedAmount1 = (from g in lstCPAllocationGeneralExpenseDetail2 where g.CostSetID == item.ID select g.AllocatedAmount).Sum();
                    Decimal allocatedAmount2 = (from g in lstCPAllocationGeneralExpenseDetail3 where g.CostSetID == item.ID select g.AllocatedAmount).Sum();

                    item.IncurredMaterial = amount - amount2 + allocatedAmount;
                    item.IncurredLabor = amount3 - amount4 + allocatedAmount1;
                    item.IncurredCost = amount5 - amount6 + allocatedAmount2;
                    item.IncurredAmount = Math.Round(item.IncurredMaterial, lamTronGia) + Math.Round(item.IncurredLabor, lamTronGia) + Math.Round(item.IncurredCost, lamTronGia);
                }
            }

            else if (md.Type == 1 || md.Type == 2)
            {
                lstSCostset = new List<SelectCostSet>();
                _ListCostSet = new List<Guid>();
                for (int i = 0; i < uGridDS.Rows.Count; i++)
                {
                    if (uGridDS.Rows[i].Cells["Select"].Value == null)
                    {
                        uGridDS.Rows[i].Cells["Select"].Value = false;
                    }
                    if (bool.Parse(uGridDS.Rows[i].Cells["Select"].Value.ToString()) == true)
                    {
                        SelectCostSet cs = uGridDS.Rows[i].ListObject as SelectCostSet;
                        lstSCostset.Add(cs);
                        _ListCostSet.Add(cs.ID);
                    }
                }

                List<CPExpenseList> lstCPExpenseList1 = (from g in lstExpenseList
                                                         join h in lstExpenseItem on g.ExpenseItemID equals h.ID
                                                         where g.TypeVoucher == 0 && h.ExpenseType == 0 && _ListCostSet.Any(x => x == g.CostSetID) && g.CPPeriodID == md.ID
                                                         select g).ToList();
                List<CPExpenseList> lstCPExpenseList2 = (from g in lstExpenseList
                                                         join h in lstExpenseItem on g.ExpenseItemID equals h.ID
                                                         where g.TypeVoucher == 1 && h.ExpenseType == 0 && _ListCostSet.Any(x => x == g.CostSetID) && g.CPPeriodID == md.ID
                                                         select g).ToList();
                List<CPExpenseList> lstCPExpenseList3 = (from g in lstExpenseList
                                                         join h in lstExpenseItem on g.ExpenseItemID equals h.ID
                                                         where g.TypeVoucher == 0 && h.ExpenseType == 1 && _ListCostSet.Any(x => x == g.CostSetID) && g.CPPeriodID == md.ID
                                                         select g).ToList();
                List<CPExpenseList> lstCPExpenseList4 = (from g in lstExpenseList
                                                         join h in lstExpenseItem on g.ExpenseItemID equals h.ID
                                                         where g.TypeVoucher == 1 && h.ExpenseType == 1 && _ListCostSet.Any(x => x == g.CostSetID) && g.CPPeriodID == md.ID
                                                         select g).ToList();
                List<CPExpenseList> lstCPExpenseList5 = (from g in lstExpenseList
                                                         join h in lstExpenseItem on g.ExpenseItemID equals h.ID
                                                         where g.TypeVoucher == 0 && h.ExpenseType == 2 && _ListCostSet.Any(x => x == g.CostSetID) && g.CPPeriodID == md.ID
                                                         select g).ToList();
                List<CPExpenseList> lstCPExpenseList6 = (from g in lstExpenseList
                                                         join h in lstExpenseItem on g.ExpenseItemID equals h.ID
                                                         where g.TypeVoucher == 1 && h.ExpenseType == 2 && _ListCostSet.Any(x => x == g.CostSetID) && g.CPPeriodID == md.ID
                                                         select g).ToList();
                List<CPAllocationGeneralExpenseDetail> lstCPAllocationGeneralExpenseDetail1 = (from g in lstCPAllocationGeneralExpenseDetail
                                                                                               join i in lstAllocationGeneralExpense on g.CPAllocationGeneralExpenseID equals i.ID
                                                                                               join h in lstExpenseItem on g.ExpenseItemID equals h.ID
                                                                                               where h.ExpenseType == 0 && _ListCostSet.Any(x => x == g.CostSetID)
                                                                                               && i.CPPeriodID == md.ID
                                                                                               select g).ToList();
                List<CPAllocationGeneralExpenseDetail> lstCPAllocationGeneralExpenseDetail2 = (from g in lstCPAllocationGeneralExpenseDetail
                                                                                               join i in lstAllocationGeneralExpense on g.CPAllocationGeneralExpenseID equals i.ID
                                                                                               join h in lstExpenseItem on g.ExpenseItemID equals h.ID
                                                                                               where h.ExpenseType == 1 && _ListCostSet.Any(x => x == g.CostSetID)
                                                                                               && i.CPPeriodID == md.ID
                                                                                               select g).ToList();
                List<CPAllocationGeneralExpenseDetail> lstCPAllocationGeneralExpenseDetail3 = (from g in lstCPAllocationGeneralExpenseDetail
                                                                                               join i in lstAllocationGeneralExpense on g.CPAllocationGeneralExpenseID equals i.ID
                                                                                               join h in lstExpenseItem on g.ExpenseItemID equals h.ID
                                                                                               where h.ExpenseType == 2 && _ListCostSet.Any(x => x == g.CostSetID)
                                                                                               && i.CPPeriodID == md.ID
                                                                                               select g).ToList();

                List<CPPeriod> periods = lstPeriod2.Where(x => x.ToDate < md.FromDate).OrderByDescending(x => x.ToDate).ToList();
                if (periods.Count > 0)
                {
                    if (lstCostSet.Count > 0)
                    {
                        foreach (var item in lstSCostset)
                        {
                            CPPeriod period1 = (from g in periods join h in cPPeriodDetails on g.ID equals h.CPPeriodID where h.CostSetID == item.ID select g).FirstOrDefault();
                            if (period1 != null)
                            {
                                List<CPUncomplete> cpu1 = cpu.Where(x => x.CPPeriodID == period1.ID).ToList();
                                if (cpu1.Count > 0)
                                {
                                    foreach (var item2 in cpu1)
                                    {
                                        if (item.ID == item2.CostSetID)
                                        {
                                            RCost data2 = new RCost();
                                            data2.FromDate = md.FromDate;
                                            data2.ToDate = md.ToDate;
                                            data2.ID = (Guid)item2.CostSetID;
                                            data2.CostSetName = item.CostSetName;
                                            data2.UncompleteFirstAmount = Math.Round(item2.DirectMaterialAmount, lamTronGia) + Math.Round(item2.GeneralExpensesAmount, lamTronGia) + Math.Round(item2.DirectLaborAmount, lamTronGia);
                                            data2.UncompleteFirstMaterial = (Decimal)item2.DirectMaterialAmount;
                                            data2.UncompleteFirstLabor = (Decimal)item2.DirectLaborAmount;
                                            data2.UncompleteFirstCost = (Decimal)item2.GeneralExpensesAmount;
                                            data.Add(data2);
                                        }
                                    }
                                }
                                else
                                {
                                    if (cpo.Count > 0)
                                    {
                                        foreach (var item2 in cpo)
                                        {
                                            if (item.ID == item2.CostSetID)
                                            {
                                                RCost data2 = new RCost();
                                                data2.FromDate = md.FromDate;
                                                data2.ToDate = md.ToDate;
                                                data2.ID = (Guid)item2.CostSetID;
                                                data2.CostSetName = item.CostSetName;
                                                data2.UncompleteFirstAmount = Math.Round(item2.DirectMaterialAmount ?? 0, lamTronGia) + Math.Round(item2.GeneralExpensesAmount ?? 0, lamTronGia) + Math.Round(item2.DirectLaborAmount ?? 0, lamTronGia);
                                                data2.UncompleteFirstMaterial = (Decimal)item2.DirectMaterialAmount;
                                                data2.UncompleteFirstLabor = (Decimal)item2.DirectLaborAmount;
                                                data2.UncompleteFirstCost = (Decimal)item2.GeneralExpensesAmount;
                                                data.Add(data2);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        RCost data2 = new RCost();
                                        data2.FromDate = md.FromDate;
                                        data2.ToDate = md.ToDate;
                                        data2.ID = item.ID;
                                        data2.CostSetName = item.CostSetName;
                                        data2.UncompleteFirstAmount = 0;
                                        data2.UncompleteFirstMaterial = 0;
                                        data2.UncompleteFirstLabor = 0;
                                        data2.UncompleteFirstCost = 0;
                                        data.Add(data2);
                                    }
                                }
                            }
                            else
                            {
                                if (cpo.Count > 0)
                                {
                                    foreach (var item2 in cpo)
                                    {
                                        if (item.ID == item2.CostSetID)
                                        {
                                            RCost data2 = new RCost();
                                            data2.FromDate = md.FromDate;
                                            data2.ToDate = md.ToDate;
                                            data2.ID = (Guid)item2.CostSetID;
                                            data2.CostSetName = item.CostSetName;
                                            data2.UncompleteFirstAmount = Math.Round(item2.DirectMaterialAmount ?? 0, lamTronGia) + Math.Round(item2.GeneralExpensesAmount ?? 0, lamTronGia) + Math.Round(item2.DirectLaborAmount ?? 0, lamTronGia);
                                            data2.UncompleteFirstMaterial = (Decimal)item2.DirectMaterialAmount;
                                            data2.UncompleteFirstLabor = (Decimal)item2.DirectLaborAmount;
                                            data2.UncompleteFirstCost = (Decimal)item2.GeneralExpensesAmount;
                                            data.Add(data2);
                                        }
                                    }
                                }
                                else
                                {
                                    RCost data2 = new RCost();
                                    data2.FromDate = md.FromDate;
                                    data2.ToDate = md.ToDate;
                                    data2.ID = item.ID;
                                    data2.CostSetName = item.CostSetName;
                                    data2.UncompleteFirstAmount = 0;
                                    data2.UncompleteFirstMaterial = 0;
                                    data2.UncompleteFirstLabor = 0;
                                    data2.UncompleteFirstCost = 0;
                                    data.Add(data2);
                                }
                            }
                        }
                    }
                    else
                    {
                        MSG.Warning("Bạn chưa chọn đối tượng ở danh sách để xem thẻ tính giá thành !");
                        return;
                    }
                }
                else
                {
                    if (lstCostSet.Count > 0)
                    {
                        foreach (var item in lstSCostset)
                        {
                            if (cpo.Count > 0)
                            {
                                foreach (var item2 in cpo)
                                {
                                    if (item.ID == item2.CostSetID)
                                    {
                                        RCost data2 = new RCost();
                                        data2.FromDate = md.FromDate;
                                        data2.ToDate = md.ToDate;
                                        data2.ID = (Guid)item2.CostSetID;
                                        data2.CostSetName = item.CostSetName;
                                        data2.UncompleteFirstAmount = Math.Round(item2.DirectMaterialAmount ?? 0, lamTronGia) + Math.Round(item2.GeneralExpensesAmount ?? 0, lamTronGia) + Math.Round(item2.DirectLaborAmount ?? 0, lamTronGia);
                                        data2.UncompleteFirstMaterial = (Decimal)item2.DirectMaterialAmount;
                                        data2.UncompleteFirstLabor = (Decimal)item2.DirectLaborAmount;
                                        data2.UncompleteFirstCost = (Decimal)item2.GeneralExpensesAmount;
                                        data.Add(data2);
                                    }
                                }
                            }
                            else
                            {
                                RCost data2 = new RCost();
                                data2.FromDate = md.FromDate;
                                data2.ToDate = md.ToDate;
                                data2.ID = item.ID;
                                data2.CostSetName = item.CostSetName;
                                data2.UncompleteFirstAmount = 0;
                                data2.UncompleteFirstMaterial = 0;
                                data2.UncompleteFirstLabor = 0;
                                data2.UncompleteFirstCost = 0;
                                data.Add(data2);
                            }
                        }
                    }
                    else
                    {
                        MSG.Warning("Bạn chưa chọn đối tượng ở danh sách để xem thẻ tính giá thành !");
                        return;
                    }
                }

                foreach (var item in data)
                {
                    Decimal amount = (from g in lstCPExpenseList1 where g.CostSetID == item.ID select g.Amount).Sum();
                    Decimal amount2 = (from g in lstCPExpenseList2 where g.CostSetID == item.ID select g.Amount).Sum();
                    Decimal amount3 = (from g in lstCPExpenseList3 where g.CostSetID == item.ID select g.Amount).Sum();
                    Decimal amount4 = (from g in lstCPExpenseList4 where g.CostSetID == item.ID select g.Amount).Sum();
                    Decimal amount5 = (from g in lstCPExpenseList5 where g.CostSetID == item.ID select g.Amount).Sum();
                    Decimal amount6 = (from g in lstCPExpenseList6 where g.CostSetID == item.ID select g.Amount).Sum();
                    Decimal allocatedAmount = (from g in lstCPAllocationGeneralExpenseDetail1 where g.CostSetID == item.ID select g.AllocatedAmount).Sum();
                    Decimal allocatedAmount1 = (from g in lstCPAllocationGeneralExpenseDetail2 where g.CostSetID == item.ID select g.AllocatedAmount).Sum();
                    Decimal allocatedAmount2 = (from g in lstCPAllocationGeneralExpenseDetail3 where g.CostSetID == item.ID select g.AllocatedAmount).Sum();

                    item.IncurredMaterial = amount - amount2 + allocatedAmount;
                    item.IncurredLabor = amount3 - amount4 + allocatedAmount1;
                    item.IncurredCost = amount5 - amount6 + allocatedAmount2;
                    item.IncurredAmount = Math.Round(item.IncurredMaterial, lamTronGia) + Math.Round(item.IncurredLabor, lamTronGia) + Math.Round(item.IncurredCost, lamTronGia);
                }
            }

            else if (md.Type == 3 || md.Type == 4)
            {
                List<CostSet> lstCostSet = (uGridDS.DataSource as BindingList<CostSet>).ToList().Where(x => x.Select == true).ToList();
                if (lstCostSet.Count == 0)
                {
                    MSG.Warning("Bạn chưa chọn đối tượng ở danh sách để xem thẻ tính giá thành !");
                    return;
                }
                data = _ICPPeriodService.GetListRCost(lstCostSet, md.ID);
            }
            else if (md.Type == 5)
            {
                List<EMContract> lstCostSet = (uGridDS.DataSource as BindingList<EMContract>).ToList().Where(x => x.Select == true).ToList();
                if (lstCostSet.Count == 0)
                {
                    MSG.Warning("Bạn chưa chọn đối tượng ở danh sách để xem thẻ tính giá thành !");
                    return;
                }
                data = _ICPPeriodService.GetListRCost(lstCostSet, md.ID);
            }

            if (md.Type == 0 || md.Type == 1 || md.Type == 2)
            {
                if (data.Count > 0)
                {
                    foreach (var rp in data)
                    {
                        foreach (var cr in cpr1)
                        {
                            if (rp.ID == cr.CostSetID)
                            {
                                rp.CostAmount = cr.TotalCostAmount;
                                rp.CostMaterial = cr.DirectMaterialAmount;
                                rp.CostLabor = cr.DirectLaborAmount;
                                rp.CostOfCost = cr.GeneralExpensesAmount;
                            }
                        }

                        cpu = cpu.Where(x => x.CPPeriodID == md.ID).ToList();
                        foreach (var cu in cpu)
                        {
                            if (rp.ID == cu.CostSetID)
                            {
                                rp.UncompleteLastAmount = Math.Round(cu.DirectMaterialAmount, lamTronGia) + Math.Round(cu.DirectLaborAmount, lamTronGia) + Math.Round(cu.GeneralExpensesAmount, lamTronGia);
                                rp.UncompleteLastMaterial = cu.DirectMaterialAmount;
                                rp.UncompleteLastLabor = cu.DirectLaborAmount;
                                rp.UncompleteLastCost = cu.GeneralExpensesAmount;
                            }
                        }
                    }
                }
                //if (data.Count > 0)
                //    foreach (var item in data)
                //    {
                //        item.IncurredAmount = item.CostAmount + item.UncompleteLastAmount - item.UncompleteFirstAmount;
                //        item.IncurredCost = item.CostOfCost + item.UncompleteLastCost - item.UncompleteFirstCost;
                //        item.IncurredLabor = item.CostLabor + item.UncompleteLastLabor - item.UncompleteFirstLabor;
                //        item.IncurredMaterial = item.CostMaterial + item.UncompleteLastMaterial - item.UncompleteFirstMaterial;
                //    }
            }


            #region Show report
            if (data.Count > 0)
            {
                var f = new ReportForm<RCost>(fileReportSlot1, _subSystemCode);
                f.AddDatasource("Data", data, true);
                f.SetHyperlinkFunction("Accounting.ReportUtils", "Hyperlink");
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
            else
            {
                MSG.Warning("Không có dữ liệu!");
                return;
            }
            #endregion

        }

        private bool checkerror()
        {
            bool result = true;
            if (cbbMethod1.Value == null)
            {
                MSG.Warning("Chưa chọn Loại/ Phương pháp tính giá thành !");
                result = false;
            }
            else if (cbbPeriod.Value == null)
            {
                MSG.Warning("Chưa chọn kỳ tính giá thành !");
                result = false;
            }


            return result;
        }
        #region Config
        private void cbbPeriod_RowSelected(object sender, RowSelectedEventArgs e)
        {
            UltraGridRow row = this.cbbPeriod.SelectedRow;
            if (row != null)
            {
                var md = cbbPeriod.SelectedRow.ListObject as CPPeriod;
                List<CPPeriodDetail> cPPeriodDetails = md.CPPeriodDetails.ToList();
                if (md.Type == 0)
                {
                    lstCostSet = Utils.ListCostSet.ToList().Where(c => cPPeriodDetails.Any(b => b.CostSetID == c.ID)).ToList();
                    lstCostSet.ForEach(x => x.Select = false);
                    uGridDS.DataSource = new BindingList<CostSet>(lstCostSet);
                    uGridDS.DisplayLayout.Bands[0].Columns["Select"].CellClickAction = CellClickAction.Edit;
                    uGridDS.DisplayLayout.Bands[0].Columns["Select"].Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;

                    fileReportSlot1.FilePath = string.Format(linkReport012, path);
                }
                else if (md.Type == 1 || md.Type == 2)
                {
                    List<SelectCostSet> listcostset = new List<SelectCostSet>();
                    lstCostSet = Utils.ListCostSet.ToList().Where(c => cPPeriodDetails.Any(b => b.CostSetID == c.ID)).ToList();
                    lstCostSet.ForEach(x => x.Select = false);
                    foreach (var item in lstCostSet)
                    {
                        SelectCostSet scs = new SelectCostSet();
                        scs.ID = item.ID;
                        scs.CostSetCode = item.CostSetCode;
                        scs.CostSetName = item.CostSetName;
                        scs.CostSetType = item.CostSetType;
                        scs.CostSetTypeView = item.CostSetTypeView;
                        listcostset.Add(scs);
                    }
                    listcostset = listcostset.OrderBy(x => x.CostSetCode).ToList();
                    listcostset.ForEach(x => x.Select = false);
                    uGridDS.DataSource = new BindingList<SelectCostSet>(listcostset);
                    uGridDS.DisplayLayout.Bands[0].Columns["Select"].CellClickAction = CellClickAction.Edit;
                    uGridDS.DisplayLayout.Bands[0].Columns["Select"].Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
                    fileReportSlot1.FilePath = string.Format(linkReport012, path);
                }

                else if (md.Type == 3)
                {
                    lstCostSet = Utils.ListCostSet.ToList().Where(c => md.CPPeriodDetails.Any(b => b.CostSetID == c.ID)).ToList();
                    lstCostSet.ForEach(x => x.Select = false);
                    foreach (var item in lstCostSet)
                    {
                        item.CostSetTypeView = dicCostSetType[item.CostSetType];
                    }
                    uGridDS.DataSource = new BindingList<CostSet>(lstCostSet);
                    uGridDS.DisplayLayout.Bands[0].Columns["Select"].CellClickAction = CellClickAction.Edit;
                    fileReportSlot1.FilePath = string.Format(linkReport345, path);
                }
                else if (md.Type == 4)
                {
                    lstCostSet = Utils.ListCostSet.ToList().Where(c => md.CPPeriodDetails.Any(b => b.CostSetID == c.ID)).ToList();
                    lstCostSet.ForEach(x => x.Select = false);
                    foreach (var item in lstCostSet)
                    {
                        item.CostSetTypeView = dicCostSetType[item.CostSetType];
                    }
                    uGridDS.DataSource = new BindingList<CostSet>(lstCostSet);
                    uGridDS.DisplayLayout.Bands[0].Columns["Select"].CellClickAction = CellClickAction.Edit;
                    fileReportSlot1.FilePath = string.Format(linkReport345, path);
                }
                else if (md.Type == 5)
                {
                    List<EMContract> lst = Utils.ListEmContract.ToList().Where(c => md.CPPeriodDetails.Any(b => b.ContractID == c.ID)).ToList();
                    uGridDS.DataSource = new BindingList<EMContract>(lst);
                    lst.ForEach(x => x.Select = false);
                    Utils.ConfigGrid(uGridDS, ConstDatabase.EMContract_TableName);
                    foreach (var col in uGridDS.DisplayLayout.Bands[0].Columns)
                    {
                        if (col.Key == "Select" || col.Key == "Code" || col.Key == "SignedDate" || col.Key == "AccountingObjectName") col.Hidden = false;
                        else col.Hidden = true;
                    }
                    var gridBand = uGridDS.DisplayLayout.Bands[0];
                    gridBand.Columns["Select"].Header.VisiblePosition = 0;
                    gridBand.Columns["Select"].Hidden = false;
                    gridBand.Columns["Select"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
                    gridBand.Columns["Select"].AutoSizeMode = ColumnAutoSizeMode.AllRowsInBand;
                    gridBand.Columns["Select"].CellClickAction = CellClickAction.Edit;
                    uGridDS.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
                    uGridDS.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
                    gridBand.Columns["Select"].Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
                    gridBand.Columns["Select"].Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
                    gridBand.Columns["Select"].Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
                    gridBand.Columns["Select"].Header.Fixed = true;
                    gridBand.Columns["Description"].Hidden = true;
                    gridBand.Columns["IsActive"].Hidden = true;
                    fileReportSlot1.FilePath = string.Format(linkReport345, path);
                }
            }

        }

        static object BuildConfig(int obj)
        {
            Dictionary<int, string> temp = new Dictionary<int, string>();
            switch (obj)
            {
                case 1:
                    {
                        temp.Add(0, "Đơn hàng");
                        temp.Add(1, "Công trình, vụ việc");
                        temp.Add(2, "Phân xưởng, phòng ban");
                        temp.Add(3, "Quy trình công nghệ sản xuất");
                        temp.Add(4, "Sản phẩm");
                        temp.Add(5, "Khác");
                    }
                    break;
            }
            return temp;
        }
        #endregion


    }
}