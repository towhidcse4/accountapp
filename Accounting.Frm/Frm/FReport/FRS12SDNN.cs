﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.IService;
using Accounting.Core.ServiceImp;
using Accounting.TextMessage;
using FX.Core;
using Accounting.Core.Domain;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FRS12SDNN : CustormForm
    {
        readonly List<FixedAssetReport> _lstS23Dns = ReportUtils.LstFixedAssetReport;
        string _subSystemCode;
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        public FRS12SDNN(string subSystemCode)
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();
            _subSystemCode = subSystemCode;
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = String.Format("{0}\\Frm\\FReport\\Template\\S12-DNN.rst", path); ;
            dtEndDate.DateTime = DateTime.Now;
            _lstS23Dns = (Utils.IFixedAssetService.GetAll().Select(c => new FixedAssetReport()
                                {
                                    ID = c.ID,
                                    FixedAssetCode = c.FixedAssetCode,
                                    FixedAssetName = c.FixedAssetName,
                                    FixedAssetCategoryID = c.FixedAssetCategoryID
                                })).ToList();
            ugridFixedAsset.SetDataBinding(_lstS23Dns.OrderBy(n => n.FixedAssetCode).ToList(), "");
            ReportUtils.ProcessControls(this);
            btnExit.Click += btnExit_Click;
            btnOk.Click += btnOk_Click;

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if ( dateTimeCacheHistory.DtEndDate != null)
                {
                    
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }

            WaitingFrm.StopWaiting();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            ReportUtils.ClearCheckBox(ugridFixedAsset);
            Close();
        }
        private void btnOk_Click(object sender, EventArgs e)
        {
            if (dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
               
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
               
                dateTimeCacheHistory.SubSystemCode = this.Name;
              
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            var fixedAssetLedgerService = IoC.Resolve<IFixedAssetLedgerService>();
            List<Guid> lstFixAssetGuids = new List<Guid>();
            if (_lstS23Dns.Count > 0)
            {
                foreach (var fixAsset in _lstS23Dns.Where(p => p.Check))
                {
                    lstFixAssetGuids.Add(fixAsset.ID);
                }
            }
            if (lstFixAssetGuids.Count == 0)
            {
                MSG.Warning("Bạn chưa chọn tài sản để lập báo cáo");
                return;
            }
            if (dtEndDate.Value == null)
            {
                MSG.Warning("Bạn chưa chọn ngày để lập báo cáo");
                return;
            }
            S12DNN data = new S12DNN();
            data = fixedAssetLedgerService.ReportS12DNN((DateTime)dtEndDate.Value, lstFixAssetGuids);
            if (data == null || (data.Attacments.Count == 0 && data.Infos.Count == 0 && data.Vouchers.Count == 0))
            {
                MSG.Warning("Không có dữ liệu để xuất báo cáo");
                return;
            }
            var rD = new S12DNNDetails();
            var f = new ReportForm<RFAInfo>(fileReportSlot1, _subSystemCode);
            f.AddDatasource("FIXED_ASSET", data.Infos, true);
            f.AddDatasource("Vouchers", data.Vouchers);
            f.AddDatasource("Attackments", data.Attacments);
            f.AddDatasource("Detail", rD);
            f.LoadReport();
            f.WindowState = FormWindowState.Maximized;
            ReportUtils.ClearCheckBox(ugridFixedAsset);
            ugridFixedAsset.SetDataBinding(_lstS23Dns.ToList(), "");
            f.Show();
        }
    }
}
