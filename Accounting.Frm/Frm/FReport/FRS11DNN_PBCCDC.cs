﻿using System;
using System.Collections.Generic;
using System.Linq;
using FX.Core;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using Accounting.Core.DAO;
using Accounting.Frm.FReport;

namespace Accounting
{
    public partial class FRS11DNN_PBCCDC : CustormForm
    {
        readonly List<ToolsReport> _lstToolsReport = new List<ToolsReport>();
        string _subSystemCode;
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        public FRS11DNN_PBCCDC(string subSystemCode)
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();
            _subSystemCode = subSystemCode;
            
            _lstToolsReport = Utils.ListTIInit.Select(c => new ToolsReport() {
                Check = false,
                ID = c.ID,
                ToolsCode = c.ToolsCode,
                ToolsName = c.ToolsName
            }).ToList();
            ugridTools.SetDataBinding(_lstToolsReport.ToList(), "");
            ReportUtils.ProcessControls(this);
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dtBeginDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }

            WaitingFrm.StopWaiting();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }
            //List<TIInit> data = new List<TIInit>();
            //List<Guid> lstGuidTools = new List<Guid>();
            //foreach (var tools in _lstToolsReport.Where(p => p.Check))
            //{
            //    lstGuidTools.Add(tools.ID);
            //}
            //if (lstGuidTools.Count == 0)
            //{
            //    MSG.Warning("Bạn chưa chọn công cụ dụng cụ để lập báo cáo");
            //    return;
            //}
            ReportProcedureSDS sp = new ReportProcedureSDS();

            string list_item = "";

            try
            {
                var dsitemChoose = list_item != null ? _lstToolsReport.Where(c => c.Check).Select(c => c.ID.ToString()) : new List<string>();
                list_item = string.Join(",", dsitemChoose.ToArray());
            }
            catch (Exception ex)
            {
                list_item = "";
            }

            if (list_item == "")
            {
                MSG.Warning("Bạn chưa chọn công cụ dụng cụ để lập báo cáo");
                return;
            }

            if (dtEndDate.Value == null || dtBeginDate.Value == null)
            {
                MSG.Warning("Bạn chưa chọn ngày để lập báo cáo");
                return;
            }
            DateTime from = new DateTime(((DateTime)dtBeginDate.Value).Year, ((DateTime)dtBeginDate.Value).Month, ((DateTime)dtBeginDate.Value).Day);
            DateTime to = new DateTime(((DateTime)dtEndDate.Value).Year, ((DateTime)dtEndDate.Value).Month, ((DateTime)dtEndDate.Value).Day);
            if (from > to)
            {
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            FSOCCDCex fm = new FSOCCDCex((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, list_item);
            fm.Show(this);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            ReportUtils.ClearCheckBox(ugridTools);
            Close();
        }
    }
}
