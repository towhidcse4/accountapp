﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FRPayableSummary : CustormForm
    {
        //readonly List<AccountReport> _lstAccountReceivablesReport = ReportUtils.LstAccountReceivablesReport;
        //private readonly List<AccountingObjectReport> _lstnew = ReportUtils.LstProvider;
        //List<AccountingObjectReport> _lstAccountingObjectReport = new List<AccountingObjectReport>();
        string _subSystemCode;
        public FRPayableSummary(string subSystemCode)
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();
            _subSystemCode = subSystemCode;
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\PayableSummary.rst", path);
           // LoadDuLieu();
            //_lstAccountingObjectReport = _lstnew;
            //ugridAccount.SetDataBinding(_lstAccountReceivablesReport.ToList(), "");
            //ugridAccountingObject.SetDataBinding(_lstAccountingObjectReport.ToList(), "");
            ReportUtils.ProcessControls(this);
            btnExit.Click += btnExit_Click;
            btnOk.Click += btnOk_Click;
            WaitingFrm.StopWaiting();
        }
        private void LoadDuLieu(bool configGrid = true)
        {
            #region hiển thị và xử lý hiển thị
            ViewTabIndex(Utils.IAccountingObjectService.GetListPayVendor(Utils.StringToDateTime(Utils.GetDbStartDate()) ?? DateTime.Now, null, null), configGrid);
            #endregion
            if (uGrid.Rows.Count > 0) uGrid.Rows[0].Selected = true;
        }
        /// <summary>
        /// Dùng view danh sách khách hàng có phát sinh
        /// </summary>
        /// <param name="lstCustomerDebit">Danh sách khách hàng có phát sinh</param>
        /// <param name="configGrid"></param>
        public void ViewTabIndex(List<PayVendor> lstCustomerDebit, bool configGrid = true)
        {
            uGrid.DisplayLayout.Bands[0].Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            uGrid.DataSource = lstCustomerDebit;
            uGrid.DisplayLayout.Bands[0].Summaries.Clear();
            if (configGrid) Utils.ConfigGrid(uGrid, ConstDatabase.PayVendor_TableName);
            //Tổng số hàng
            uGrid.DisplayLayout.Override.AllowRowSummaries = AllowRowSummaries.Default; //Ẩn ký hiệu tổng trên Header Caption
            UltraGridBand band = uGrid.DisplayLayout.Bands[0];
            Utils.FormatNumberic(band.Columns["SoDuDauNam"], ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(band.Columns["SoPhatSinh"], ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(band.Columns["SoDaTra"], ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(band.Columns["SoConPhaiTra"], ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGrid, "SoDuDauNam", false);
            Utils.AddSumColumn(uGrid, "SoPhatSinh", false);
            Utils.AddSumColumn(uGrid, "SoDaTra", false);
            Utils.AddSumColumn(uGrid, "SoConPhaiTra", false);
        }        
        public void ViewTabDetails(List<VoucherVendor> voucherVendor)
        {
            uGridDetail.DisplayLayout.Bands[0].Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            uGridDetail.DataSource = voucherVendor;
            Utils.ConfigGrid(uGridDetail, ConstDatabase.VoucherVendor_TableName);
            uGridDetail.DisplayLayout.CaptionVisible = DefaultableBoolean.False;
            //Tổng số hàng
            uGridDetail.DisplayLayout.Override.AllowRowSummaries = AllowRowSummaries.Default; //Ẩn ký hiệu tổng trên Header Caption
            UltraGridBand band = uGridDetail.DisplayLayout.Bands[0];
            Utils.FormatNumberic(band.Columns["SoPhatSinh"], ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(band.Columns["SoDaTra"], ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridDetail, "SoDaTra", false);
            Utils.AddSumColumn(uGridDetail, "SoPhatSinh", false);
        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void btnOk_Click(object sender, EventArgs e)
        {
            //if (!_lstAccountReceivablesReport.Any(c => c.Check))
            //{
            //    MSG.Warning(resSystem.Report_01);
            //    return;
            //}
            //if (!_lstAccountingObjectReport.Any(c => c.Check))
            //{
            //    MSG.Warning(resSystem.Report_02);
            //    return;
            //}
            //var iGeneralLedgerService = IoC.Resolve<IGeneralLedgerService>();
            //List<string> lstGuids = _lstAccountReceivablesReport.Where(c => c.Check).Select(c => c.AccountNumber).ToList();
            //var dsacc = _lstAccountingObjectReport.Where(c => c.Check).Select(c => c.ID).ToList();
            //List<PayableReceiptableSummary> data = iGeneralLedgerService.ReportsPayableReceiptableSummary((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, idCurentcy, lstGuids, dsacc);
            //data.Add(new PayableReceiptableSummary());
            //var rD = new PayableReceiptableSummaryDetail();
            //rD.Period = ReportUtils.GetPeriod((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value);
            //var f = new ReportForm<PayableReceiptableSummary>(fileReportSlot1, _subSystemCode);
            //f.AddDatasource("PayableSummary", data, true);
            //f.AddDatasource("Detail", rD);
            //f.LoadReport();
            //f.WindowState = FormWindowState.Maximized;
            //f.Show();
        }
    }
}
