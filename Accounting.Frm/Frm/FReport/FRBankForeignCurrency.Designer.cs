﻿namespace Accounting
{
    partial class FRBankForeignCurrency
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FRBankForeignCurrency));
            this.reportManager1 = new PerpetuumSoft.Reporting.Components.ReportManager(this.components);
            this.fileReportSlot1 = new PerpetuumSoft.Reporting.Components.FileReportSlot(this.components);
            this.ugridAccount = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraPanel3 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.btnOk = new Infragistics.Win.Misc.UltraButton();
            this.btnExit = new Infragistics.Win.Misc.UltraButton();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbCurrency = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbbBankAccountDetail = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.chk = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.lblBeginDate = new Infragistics.Win.Misc.UltraLabel();
            this.lblEndDate = new Infragistics.Win.Misc.UltraLabel();
            this.cbbDateTime = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.dtBeginDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.dtEndDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            ((System.ComponentModel.ISupportInitialize)(this.ugridAccount)).BeginInit();
            this.ultraPanel3.ClientArea.SuspendLayout();
            this.ultraPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).BeginInit();
            this.ultraGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankAccountDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtBeginDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDate)).BeginInit();
            this.SuspendLayout();
            // 
            // reportManager1
            // 
            this.reportManager1.DataSources = new PerpetuumSoft.Reporting.Components.ObjectPointerCollection(new string[0], new object[0]);
            this.reportManager1.OwnerForm = this;
            this.reportManager1.Reports.AddRange(new PerpetuumSoft.Reporting.Components.ReportSlot[] {
            this.fileReportSlot1});
            // 
            // fileReportSlot1
            // 
            this.fileReportSlot1.FilePath = "";
            this.fileReportSlot1.ReportName = "";
            this.fileReportSlot1.ReportScriptType = typeof(PerpetuumSoft.Reporting.Rendering.ReportScriptBase);
            // 
            // ugridAccount
            // 
            this.ugridAccount.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.ugridAccount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ugridAccount.Location = new System.Drawing.Point(0, 94);
            this.ugridAccount.Name = "ugridAccount";
            this.ugridAccount.Size = new System.Drawing.Size(584, 282);
            this.ugridAccount.TabIndex = 42;
            this.ugridAccount.Text = "Danh sách tài khoản";
            this.ugridAccount.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            // 
            // ultraPanel3
            // 
            // 
            // ultraPanel3.ClientArea
            // 
            this.ultraPanel3.ClientArea.Controls.Add(this.ultraGroupBox3);
            this.ultraPanel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraPanel3.Location = new System.Drawing.Point(0, 376);
            this.ultraPanel3.Name = "ultraPanel3";
            this.ultraPanel3.Size = new System.Drawing.Size(584, 36);
            this.ultraPanel3.TabIndex = 43;
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Controls.Add(this.btnOk);
            this.ultraGroupBox3.Controls.Add(this.btnExit);
            this.ultraGroupBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraGroupBox3.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(584, 36);
            this.ultraGroupBox3.TabIndex = 0;
            this.ultraGroupBox3.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.Location = new System.Drawing.Point(415, 6);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 11;
            this.btnOk.Text = "Đồng ý";
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.Location = new System.Drawing.Point(504, 6);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 10;
            this.btnExit.Text = "Hủy bỏ";
            // 
            // ultraPanel1
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            this.ultraPanel1.Appearance = appearance1;
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraGroupBox4);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraGroupBox1);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(584, 94);
            this.ultraPanel1.TabIndex = 44;
            // 
            // ultraGroupBox4
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.BackColor2 = System.Drawing.Color.Transparent;
            this.ultraGroupBox4.Appearance = appearance2;
            this.ultraGroupBox4.Controls.Add(this.cbbCurrency);
            this.ultraGroupBox4.Controls.Add(this.cbbBankAccountDetail);
            this.ultraGroupBox4.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox4.Controls.Add(this.chk);
            this.ultraGroupBox4.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance29.FontData.BoldAsString = "True";
            appearance29.FontData.SizeInPoints = 13F;
            this.ultraGroupBox4.HeaderAppearance = appearance29;
            this.ultraGroupBox4.Location = new System.Drawing.Point(254, 0);
            this.ultraGroupBox4.Name = "ultraGroupBox4";
            this.ultraGroupBox4.Size = new System.Drawing.Size(330, 94);
            this.ultraGroupBox4.TabIndex = 43;
            this.ultraGroupBox4.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbCurrency
            // 
            appearance3.BackColor = System.Drawing.SystemColors.Window;
            appearance3.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbCurrency.DisplayLayout.Appearance = appearance3;
            this.cbbCurrency.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbCurrency.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance4.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance4.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance4.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbCurrency.DisplayLayout.GroupByBox.Appearance = appearance4;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbCurrency.DisplayLayout.GroupByBox.BandLabelAppearance = appearance5;
            this.cbbCurrency.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance6.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance6.BackColor2 = System.Drawing.SystemColors.Control;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance6.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbCurrency.DisplayLayout.GroupByBox.PromptAppearance = appearance6;
            this.cbbCurrency.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbCurrency.DisplayLayout.MaxRowScrollRegions = 1;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            appearance7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbCurrency.DisplayLayout.Override.ActiveCellAppearance = appearance7;
            appearance8.BackColor = System.Drawing.SystemColors.Highlight;
            appearance8.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbCurrency.DisplayLayout.Override.ActiveRowAppearance = appearance8;
            this.cbbCurrency.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbCurrency.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            this.cbbCurrency.DisplayLayout.Override.CardAreaAppearance = appearance9;
            appearance10.BorderColor = System.Drawing.Color.Silver;
            appearance10.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbCurrency.DisplayLayout.Override.CellAppearance = appearance10;
            this.cbbCurrency.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbCurrency.DisplayLayout.Override.CellPadding = 0;
            appearance11.BackColor = System.Drawing.SystemColors.Control;
            appearance11.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance11.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance11.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance11.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbCurrency.DisplayLayout.Override.GroupByRowAppearance = appearance11;
            appearance12.TextHAlignAsString = "Left";
            this.cbbCurrency.DisplayLayout.Override.HeaderAppearance = appearance12;
            this.cbbCurrency.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbCurrency.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.Color.Silver;
            this.cbbCurrency.DisplayLayout.Override.RowAppearance = appearance13;
            this.cbbCurrency.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbCurrency.DisplayLayout.Override.TemplateAddRowAppearance = appearance14;
            this.cbbCurrency.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbCurrency.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbCurrency.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbCurrency.Location = new System.Drawing.Point(89, 37);
            this.cbbCurrency.Name = "cbbCurrency";
            this.cbbCurrency.Size = new System.Drawing.Size(208, 22);
            this.cbbCurrency.TabIndex = 53;
            // 
            // cbbBankAccountDetail
            // 
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            appearance15.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbBankAccountDetail.DisplayLayout.Appearance = appearance15;
            this.cbbBankAccountDetail.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbBankAccountDetail.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance16.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance16.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance16.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbBankAccountDetail.DisplayLayout.GroupByBox.Appearance = appearance16;
            appearance17.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbBankAccountDetail.DisplayLayout.GroupByBox.BandLabelAppearance = appearance17;
            this.cbbBankAccountDetail.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance18.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance18.BackColor2 = System.Drawing.SystemColors.Control;
            appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance18.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbBankAccountDetail.DisplayLayout.GroupByBox.PromptAppearance = appearance18;
            this.cbbBankAccountDetail.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbBankAccountDetail.DisplayLayout.MaxRowScrollRegions = 1;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            appearance19.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbBankAccountDetail.DisplayLayout.Override.ActiveCellAppearance = appearance19;
            appearance20.BackColor = System.Drawing.SystemColors.Highlight;
            appearance20.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbBankAccountDetail.DisplayLayout.Override.ActiveRowAppearance = appearance20;
            this.cbbBankAccountDetail.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbBankAccountDetail.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance21.BackColor = System.Drawing.SystemColors.Window;
            this.cbbBankAccountDetail.DisplayLayout.Override.CardAreaAppearance = appearance21;
            appearance22.BorderColor = System.Drawing.Color.Silver;
            appearance22.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbBankAccountDetail.DisplayLayout.Override.CellAppearance = appearance22;
            this.cbbBankAccountDetail.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbBankAccountDetail.DisplayLayout.Override.CellPadding = 0;
            appearance23.BackColor = System.Drawing.SystemColors.Control;
            appearance23.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance23.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance23.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbBankAccountDetail.DisplayLayout.Override.GroupByRowAppearance = appearance23;
            appearance24.TextHAlignAsString = "Left";
            this.cbbBankAccountDetail.DisplayLayout.Override.HeaderAppearance = appearance24;
            this.cbbBankAccountDetail.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbBankAccountDetail.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.Color.Silver;
            this.cbbBankAccountDetail.DisplayLayout.Override.RowAppearance = appearance25;
            this.cbbBankAccountDetail.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbBankAccountDetail.DisplayLayout.Override.TemplateAddRowAppearance = appearance26;
            this.cbbBankAccountDetail.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbBankAccountDetail.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbBankAccountDetail.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbBankAccountDetail.Location = new System.Drawing.Point(89, 63);
            this.cbbBankAccountDetail.Name = "cbbBankAccountDetail";
            this.cbbBankAccountDetail.Size = new System.Drawing.Size(208, 22);
            this.cbbBankAccountDetail.TabIndex = 52;
            // 
            // ultraLabel2
            // 
            appearance27.BackColor = System.Drawing.Color.Transparent;
            appearance27.TextHAlignAsString = "Left";
            appearance27.TextVAlignAsString = "Bottom";
            this.ultraLabel2.Appearance = appearance27;
            this.ultraLabel2.Location = new System.Drawing.Point(6, 65);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(77, 18);
            this.ultraLabel2.TabIndex = 51;
            this.ultraLabel2.Text = "TK ngân hàng";
            // 
            // chk
            // 
            this.chk.Location = new System.Drawing.Point(16, 11);
            this.chk.Name = "chk";
            this.chk.Size = new System.Drawing.Size(314, 20);
            this.chk.TabIndex = 50;
            this.chk.Text = "Cộng các bút toán giống nhau";
            // 
            // ultraLabel1
            // 
            appearance28.BackColor = System.Drawing.Color.Transparent;
            appearance28.TextHAlignAsString = "Left";
            appearance28.TextVAlignAsString = "Bottom";
            this.ultraLabel1.Appearance = appearance28;
            this.ultraLabel1.Location = new System.Drawing.Point(6, 41);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(67, 18);
            this.ultraLabel1.TabIndex = 48;
            this.ultraLabel1.Text = "Loại tiền";
            // 
            // ultraGroupBox1
            // 
            appearance30.BackColor = System.Drawing.Color.Transparent;
            appearance30.BackColor2 = System.Drawing.Color.Transparent;
            this.ultraGroupBox1.Appearance = appearance30;
            this.ultraGroupBox1.Controls.Add(this.lblBeginDate);
            this.ultraGroupBox1.Controls.Add(this.lblEndDate);
            this.ultraGroupBox1.Controls.Add(this.cbbDateTime);
            this.ultraGroupBox1.Controls.Add(this.dtBeginDate);
            this.ultraGroupBox1.Controls.Add(this.dtEndDate);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(254, 94);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.Text = "Chọn kỳ báo cáo";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // lblBeginDate
            // 
            appearance31.BackColor = System.Drawing.Color.Transparent;
            appearance31.TextHAlignAsString = "Left";
            appearance31.TextVAlignAsString = "Bottom";
            this.lblBeginDate.Appearance = appearance31;
            this.lblBeginDate.Location = new System.Drawing.Point(26, 42);
            this.lblBeginDate.Name = "lblBeginDate";
            this.lblBeginDate.Size = new System.Drawing.Size(27, 17);
            this.lblBeginDate.TabIndex = 43;
            this.lblBeginDate.Text = "Từ";
            // 
            // lblEndDate
            // 
            appearance32.BackColor = System.Drawing.Color.Transparent;
            appearance32.TextHAlignAsString = "Left";
            appearance32.TextVAlignAsString = "Bottom";
            this.lblEndDate.Appearance = appearance32;
            this.lblEndDate.Location = new System.Drawing.Point(138, 42);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.Size = new System.Drawing.Size(25, 17);
            this.lblEndDate.TabIndex = 44;
            this.lblEndDate.Text = "Đến";
            // 
            // cbbDateTime
            // 
            this.cbbDateTime.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbDateTime.Location = new System.Drawing.Point(24, 19);
            this.cbbDateTime.Name = "cbbDateTime";
            this.cbbDateTime.NullText = "<chọn dữ liệu>";
            this.cbbDateTime.Size = new System.Drawing.Size(211, 22);
            this.cbbDateTime.TabIndex = 45;
            // 
            // dtBeginDate
            // 
            appearance33.TextHAlignAsString = "Center";
            appearance33.TextVAlignAsString = "Middle";
            this.dtBeginDate.Appearance = appearance33;
            this.dtBeginDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtBeginDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtBeginDate.Location = new System.Drawing.Point(24, 64);
            this.dtBeginDate.MaskInput = "";
            this.dtBeginDate.Name = "dtBeginDate";
            this.dtBeginDate.Size = new System.Drawing.Size(99, 21);
            this.dtBeginDate.TabIndex = 47;
            this.dtBeginDate.Value = null;
            // 
            // dtEndDate
            // 
            appearance34.TextHAlignAsString = "Center";
            appearance34.TextVAlignAsString = "Middle";
            this.dtEndDate.Appearance = appearance34;
            this.dtEndDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtEndDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtEndDate.Location = new System.Drawing.Point(136, 64);
            this.dtEndDate.MaskInput = "";
            this.dtEndDate.Name = "dtEndDate";
            this.dtEndDate.Size = new System.Drawing.Size(99, 21);
            this.dtEndDate.TabIndex = 46;
            this.dtEndDate.Value = null;
            // 
            // FRBankForeignCurrency
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 412);
            this.Controls.Add(this.ugridAccount);
            this.Controls.Add(this.ultraPanel1);
            this.Controls.Add(this.ultraPanel3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FRBankForeignCurrency";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tiền gửi ngân hàng bằng ngoại tệ";
            ((System.ComponentModel.ISupportInitialize)(this.ugridAccount)).EndInit();
            this.ultraPanel3.ClientArea.ResumeLayout(false);
            this.ultraPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).EndInit();
            this.ultraGroupBox4.ResumeLayout(false);
            this.ultraGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankAccountDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtBeginDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDate)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private PerpetuumSoft.Reporting.Components.ReportManager reportManager1;
        private PerpetuumSoft.Reporting.Components.FileReportSlot fileReportSlot1;
        private Infragistics.Win.UltraWinGrid.UltraGrid ugridAccount;
        private Infragistics.Win.Misc.UltraPanel ultraPanel3;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.Misc.UltraButton btnOk;
        private Infragistics.Win.Misc.UltraButton btnExit;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox4;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbCurrency;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbBankAccountDetail;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chk;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraLabel lblBeginDate;
        private Infragistics.Win.Misc.UltraLabel lblEndDate;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbDateTime;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtBeginDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtEndDate;
    }
}