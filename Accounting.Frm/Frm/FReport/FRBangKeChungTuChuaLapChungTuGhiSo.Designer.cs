﻿namespace Accounting.Frm.FReport
{
    partial class FRBangKeChungTuChuaLapChungTuGhiSo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.lblDateTime = new Infragistics.Win.Misc.UltraLabel();
            this.lblBeginDate = new Infragistics.Win.Misc.UltraLabel();
            this.lblEndDate = new Infragistics.Win.Misc.UltraLabel();
            this.cbbDateTime = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.dteTo = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.dteFrom = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.btnOk = new Infragistics.Win.Misc.UltraButton();
            this.btnCancel = new Infragistics.Win.Misc.UltraButton();
            this.pafHeader = new Infragistics.Win.Misc.UltraPanel();
            this.grbInfomation = new Infragistics.Win.Misc.UltraGroupBox();
            this.palFooter = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.palBody2 = new Infragistics.Win.Misc.UltraPanel();
            this.uGridDanhSach = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.reportManager1 = new PerpetuumSoft.Reporting.Components.ReportManager(this.components);
            this.fileReportSlot2 = new PerpetuumSoft.Reporting.Components.FileReportSlot(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.cbbDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFrom)).BeginInit();
            this.pafHeader.ClientArea.SuspendLayout();
            this.pafHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbInfomation)).BeginInit();
            this.grbInfomation.SuspendLayout();
            this.palFooter.ClientArea.SuspendLayout();
            this.palFooter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            this.palBody2.ClientArea.SuspendLayout();
            this.palBody2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDanhSach)).BeginInit();
            this.SuspendLayout();
            // 
            // lblDateTime
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.TextHAlignAsString = "Left";
            appearance1.TextVAlignAsString = "Middle";
            this.lblDateTime.Appearance = appearance1;
            this.lblDateTime.AutoSize = true;
            this.lblDateTime.Location = new System.Drawing.Point(29, 28);
            this.lblDateTime.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblDateTime.Name = "lblDateTime";
            this.lblDateTime.Size = new System.Drawing.Size(95, 21);
            this.lblDateTime.TabIndex = 1;
            this.lblDateTime.Text = "Kỳ báo cáo";
            // 
            // lblBeginDate
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.TextHAlignAsString = "Left";
            appearance2.TextVAlignAsString = "Middle";
            this.lblBeginDate.Appearance = appearance2;
            this.lblBeginDate.Location = new System.Drawing.Point(281, 30);
            this.lblBeginDate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblBeginDate.Name = "lblBeginDate";
            this.lblBeginDate.Size = new System.Drawing.Size(36, 21);
            this.lblBeginDate.TabIndex = 3;
            this.lblBeginDate.Text = "Từ";
            // 
            // lblEndDate
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextHAlignAsString = "Left";
            appearance3.TextVAlignAsString = "Middle";
            this.lblEndDate.Appearance = appearance3;
            this.lblEndDate.Location = new System.Drawing.Point(465, 30);
            this.lblEndDate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.Size = new System.Drawing.Size(33, 21);
            this.lblEndDate.TabIndex = 4;
            this.lblEndDate.Text = "Đến";
            // 
            // cbbDateTime
            // 
            this.cbbDateTime.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbDateTime.Location = new System.Drawing.Point(131, 23);
            this.cbbDateTime.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbbDateTime.Name = "cbbDateTime";
            this.cbbDateTime.NullText = "<chọn dữ liệu>";
            this.cbbDateTime.Size = new System.Drawing.Size(143, 25);
            this.cbbDateTime.TabIndex = 33;
            this.cbbDateTime.ValueChanged += new System.EventHandler(this.cbbDateTime_ValueChanged);
            // 
            // dteTo
            // 
            appearance4.TextHAlignAsString = "Center";
            appearance4.TextVAlignAsString = "Middle";
            this.dteTo.Appearance = appearance4;
            this.dteTo.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteTo.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteTo.Location = new System.Drawing.Point(507, 23);
            this.dteTo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dteTo.MaskInput = "";
            this.dteTo.Name = "dteTo";
            this.dteTo.Size = new System.Drawing.Size(132, 24);
            this.dteTo.TabIndex = 35;
            this.dteTo.Value = null;
            // 
            // dteFrom
            // 
            appearance5.TextHAlignAsString = "Center";
            appearance5.TextVAlignAsString = "Middle";
            this.dteFrom.Appearance = appearance5;
            this.dteFrom.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteFrom.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteFrom.Location = new System.Drawing.Point(325, 23);
            this.dteFrom.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dteFrom.MaskInput = "";
            this.dteFrom.Name = "dteFrom";
            this.dteFrom.Size = new System.Drawing.Size(132, 24);
            this.dteFrom.TabIndex = 36;
            this.dteFrom.Value = null;
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance6.Image = global::Accounting.Properties.Resources.apply_16;
            this.btnOk.Appearance = appearance6;
            this.btnOk.Location = new System.Drawing.Point(447, 15);
            this.btnOk.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(100, 28);
            this.btnOk.TabIndex = 41;
            this.btnOk.Text = "Đồng ý";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance7.Image = global::Accounting.Properties.Resources.cancel_16;
            this.btnCancel.Appearance = appearance7;
            this.btnCancel.Location = new System.Drawing.Point(555, 15);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 28);
            this.btnCancel.TabIndex = 42;
            this.btnCancel.Text = "Hủy bỏ";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // pafHeader
            // 
            // 
            // pafHeader.ClientArea
            // 
            this.pafHeader.ClientArea.Controls.Add(this.grbInfomation);
            this.pafHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pafHeader.Location = new System.Drawing.Point(0, 0);
            this.pafHeader.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pafHeader.Name = "pafHeader";
            this.pafHeader.Size = new System.Drawing.Size(679, 68);
            this.pafHeader.TabIndex = 43;
            // 
            // grbInfomation
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.BackColor2 = System.Drawing.Color.Transparent;
            this.grbInfomation.Appearance = appearance8;
            this.grbInfomation.Controls.Add(this.lblDateTime);
            this.grbInfomation.Controls.Add(this.lblBeginDate);
            this.grbInfomation.Controls.Add(this.lblEndDate);
            this.grbInfomation.Controls.Add(this.cbbDateTime);
            this.grbInfomation.Controls.Add(this.dteFrom);
            this.grbInfomation.Controls.Add(this.dteTo);
            this.grbInfomation.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance9.FontData.BoldAsString = "True";
            appearance9.FontData.SizeInPoints = 8F;
            this.grbInfomation.HeaderAppearance = appearance9;
            this.grbInfomation.Location = new System.Drawing.Point(0, 0);
            this.grbInfomation.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grbInfomation.Name = "grbInfomation";
            this.grbInfomation.Size = new System.Drawing.Size(679, 68);
            this.grbInfomation.TabIndex = 38;
            this.grbInfomation.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // palFooter
            // 
            appearance10.BackColor = System.Drawing.Color.Transparent;
            appearance10.BackColor2 = System.Drawing.Color.Transparent;
            appearance10.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance10.BackColorDisabled = System.Drawing.Color.Transparent;
            appearance10.BackColorDisabled2 = System.Drawing.Color.Transparent;
            appearance10.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance10.BorderColor = System.Drawing.Color.Transparent;
            appearance10.BorderColor2 = System.Drawing.Color.Transparent;
            appearance10.BorderColor3DBase = System.Drawing.Color.Transparent;
            this.palFooter.Appearance = appearance10;
            // 
            // palFooter.ClientArea
            // 
            this.palFooter.ClientArea.Controls.Add(this.ultraGroupBox1);
            this.palFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.palFooter.Location = new System.Drawing.Point(0, 425);
            this.palFooter.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.palFooter.Name = "palFooter";
            this.palFooter.Size = new System.Drawing.Size(679, 55);
            this.palFooter.TabIndex = 46;
            // 
            // ultraGroupBox1
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            appearance11.BackColor2 = System.Drawing.Color.Transparent;
            this.ultraGroupBox1.Appearance = appearance11;
            this.ultraGroupBox1.Controls.Add(this.btnOk);
            this.ultraGroupBox1.Controls.Add(this.btnCancel);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance12.FontData.BoldAsString = "True";
            appearance12.FontData.SizeInPoints = 8F;
            this.ultraGroupBox1.HeaderAppearance = appearance12;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(679, 55);
            this.ultraGroupBox1.TabIndex = 43;
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.palBody2);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 68);
            this.ultraPanel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(679, 357);
            this.ultraPanel1.TabIndex = 47;
            // 
            // palBody2
            // 
            // 
            // palBody2.ClientArea
            // 
            this.palBody2.ClientArea.Controls.Add(this.uGridDanhSach);
            this.palBody2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palBody2.Location = new System.Drawing.Point(0, 0);
            this.palBody2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.palBody2.Name = "palBody2";
            this.palBody2.Size = new System.Drawing.Size(679, 357);
            this.palBody2.TabIndex = 45;
            // 
            // uGridDanhSach
            // 
            this.uGridDanhSach.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridDanhSach.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridDanhSach.Location = new System.Drawing.Point(0, 0);
            this.uGridDanhSach.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.uGridDanhSach.Name = "uGridDanhSach";
            this.uGridDanhSach.Size = new System.Drawing.Size(679, 357);
            this.uGridDanhSach.TabIndex = 39;
            this.uGridDanhSach.TabStop = false;
            this.uGridDanhSach.Text = "ultraGrid1";
            this.uGridDanhSach.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            // 
            // reportManager1
            // 
            this.reportManager1.DataSources = new PerpetuumSoft.Reporting.Components.ObjectPointerCollection(new string[0], new object[0]);
            this.reportManager1.OwnerForm = this;
            this.reportManager1.Reports.AddRange(new PerpetuumSoft.Reporting.Components.ReportSlot[] {
            this.fileReportSlot2});
            // 
            // fileReportSlot2
            // 
            this.fileReportSlot2.FilePath = "";
            this.fileReportSlot2.ReportName = "";
            this.fileReportSlot2.ReportScriptType = typeof(PerpetuumSoft.Reporting.Rendering.ReportScriptBase);
            // 
            // FRBangKeChungTuChuaLapChungTuGhiSo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(679, 480);
            this.Controls.Add(this.ultraPanel1);
            this.Controls.Add(this.palFooter);
            this.Controls.Add(this.pafHeader);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FRBangKeChungTuChuaLapChungTuGhiSo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bảng kê chứng từ chưa lập chứng từ ghi sổ";
            ((System.ComponentModel.ISupportInitialize)(this.cbbDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFrom)).EndInit();
            this.pafHeader.ClientArea.ResumeLayout(false);
            this.pafHeader.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grbInfomation)).EndInit();
            this.grbInfomation.ResumeLayout(false);
            this.grbInfomation.PerformLayout();
            this.palFooter.ClientArea.ResumeLayout(false);
            this.palFooter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            this.palBody2.ClientArea.ResumeLayout(false);
            this.palBody2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridDanhSach)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Infragistics.Win.Misc.UltraLabel lblDateTime;
        private Infragistics.Win.Misc.UltraLabel lblBeginDate;
        private Infragistics.Win.Misc.UltraLabel lblEndDate;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbDateTime;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteTo;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteFrom;
        private Infragistics.Win.Misc.UltraButton btnOk;
        private Infragistics.Win.Misc.UltraButton btnCancel;
        private Infragistics.Win.Misc.UltraPanel pafHeader;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraPanel palFooter;
        private Infragistics.Win.Misc.UltraGroupBox grbInfomation;
        private Infragistics.Win.Misc.UltraPanel palBody2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDanhSach;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private PerpetuumSoft.Reporting.Components.ReportManager reportManager1;
        private PerpetuumSoft.Reporting.Components.FileReportSlot fileReportSlot2;
    }
}