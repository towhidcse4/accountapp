﻿namespace Accounting.Frm.FReport
{
    partial class FRCHITIETCONGNOPHAITHUKHACHHANG
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            this.btnOk = new Infragistics.Win.Misc.UltraButton();
            this.btnExit = new Infragistics.Win.Misc.UltraButton();
            this.ugridAccountingObject = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraPanel2 = new Infragistics.Win.Misc.UltraPanel();
            this.cbbAccountingObjectGroupID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.lblBeginDate = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.lblEndDate = new Infragistics.Win.Misc.UltraLabel();
            this.cbbDateTime = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.dtBeginDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.dtEndDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.cbbFromAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbbCurrency = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.fileReportSlot1 = new PerpetuumSoft.Reporting.Components.FileReportSlot(this.components);
            this.reportManager1 = new PerpetuumSoft.Reporting.Components.ReportManager(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ugridAccountingObject)).BeginInit();
            this.ultraPanel2.ClientArea.SuspendLayout();
            this.ultraPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectGroupID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtBeginDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbFromAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).BeginInit();
            this.ultraGroupBox4.SuspendLayout();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.Image = global::Accounting.Properties.Resources.apply_16;
            this.btnOk.Appearance = appearance1;
            this.btnOk.Location = new System.Drawing.Point(575, 506);
            this.btnOk.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(100, 39);
            this.btnOk.TabIndex = 43;
            this.btnOk.Text = "Đồng ý";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.Image = global::Accounting.Properties.Resources.cancel_16;
            this.btnExit.Appearance = appearance2;
            this.btnExit.Location = new System.Drawing.Point(683, 506);
            this.btnExit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(100, 41);
            this.btnExit.TabIndex = 44;
            this.btnExit.Text = "Hủy bỏ";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // ugridAccountingObject
            // 
            this.ugridAccountingObject.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.ugridAccountingObject.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.ugridAccountingObject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ugridAccountingObject.Location = new System.Drawing.Point(0, 0);
            this.ugridAccountingObject.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ugridAccountingObject.Name = "ugridAccountingObject";
            this.ugridAccountingObject.Size = new System.Drawing.Size(796, 370);
            this.ugridAccountingObject.TabIndex = 7;
            this.ugridAccountingObject.TabStop = false;
            this.ugridAccountingObject.Text = "Danh sách khách hàng";
            this.ugridAccountingObject.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            // 
            // ultraPanel2
            // 
            // 
            // ultraPanel2.ClientArea
            // 
            this.ultraPanel2.ClientArea.Controls.Add(this.ugridAccountingObject);
            this.ultraPanel2.Location = new System.Drawing.Point(1, 127);
            this.ultraPanel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ultraPanel2.Name = "ultraPanel2";
            this.ultraPanel2.Size = new System.Drawing.Size(796, 370);
            this.ultraPanel2.TabIndex = 15;
            // 
            // cbbAccountingObjectGroupID
            // 
            appearance3.BackColor = System.Drawing.SystemColors.Window;
            appearance3.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbAccountingObjectGroupID.DisplayLayout.Appearance = appearance3;
            this.cbbAccountingObjectGroupID.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbAccountingObjectGroupID.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance4.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance4.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance4.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObjectGroupID.DisplayLayout.GroupByBox.Appearance = appearance4;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountingObjectGroupID.DisplayLayout.GroupByBox.BandLabelAppearance = appearance5;
            this.cbbAccountingObjectGroupID.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance6.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance6.BackColor2 = System.Drawing.SystemColors.Control;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance6.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountingObjectGroupID.DisplayLayout.GroupByBox.PromptAppearance = appearance6;
            this.cbbAccountingObjectGroupID.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbAccountingObjectGroupID.DisplayLayout.MaxRowScrollRegions = 1;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            appearance7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbAccountingObjectGroupID.DisplayLayout.Override.ActiveCellAppearance = appearance7;
            appearance8.BackColor = System.Drawing.SystemColors.Highlight;
            appearance8.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbAccountingObjectGroupID.DisplayLayout.Override.ActiveRowAppearance = appearance8;
            this.cbbAccountingObjectGroupID.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbAccountingObjectGroupID.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObjectGroupID.DisplayLayout.Override.CardAreaAppearance = appearance9;
            appearance10.BorderColor = System.Drawing.Color.Silver;
            appearance10.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbAccountingObjectGroupID.DisplayLayout.Override.CellAppearance = appearance10;
            this.cbbAccountingObjectGroupID.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbAccountingObjectGroupID.DisplayLayout.Override.CellPadding = 0;
            appearance11.BackColor = System.Drawing.SystemColors.Control;
            appearance11.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance11.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance11.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance11.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObjectGroupID.DisplayLayout.Override.GroupByRowAppearance = appearance11;
            appearance12.TextHAlignAsString = "Left";
            this.cbbAccountingObjectGroupID.DisplayLayout.Override.HeaderAppearance = appearance12;
            this.cbbAccountingObjectGroupID.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbAccountingObjectGroupID.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.Color.Silver;
            this.cbbAccountingObjectGroupID.DisplayLayout.Override.RowAppearance = appearance13;
            this.cbbAccountingObjectGroupID.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbAccountingObjectGroupID.DisplayLayout.Override.TemplateAddRowAppearance = appearance14;
            this.cbbAccountingObjectGroupID.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccountingObjectGroupID.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbAccountingObjectGroupID.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbAccountingObjectGroupID.Location = new System.Drawing.Point(171, 79);
            this.cbbAccountingObjectGroupID.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbbAccountingObjectGroupID.Name = "cbbAccountingObjectGroupID";
            this.cbbAccountingObjectGroupID.Size = new System.Drawing.Size(249, 25);
            this.cbbAccountingObjectGroupID.TabIndex = 6;
            this.cbbAccountingObjectGroupID.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.cbbAccountingObjectGroupID_InitializeLayout);
            // 
            // lblBeginDate
            // 
            appearance15.BackColor = System.Drawing.Color.Transparent;
            appearance15.TextHAlignAsString = "Left";
            appearance15.TextVAlignAsString = "Bottom";
            this.lblBeginDate.Appearance = appearance15;
            this.lblBeginDate.Location = new System.Drawing.Point(19, 52);
            this.lblBeginDate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblBeginDate.Name = "lblBeginDate";
            this.lblBeginDate.Size = new System.Drawing.Size(36, 21);
            this.lblBeginDate.TabIndex = 43;
            this.lblBeginDate.Text = "Từ";
            // 
            // ultraLabel1
            // 
            appearance16.BackColor = System.Drawing.Color.Transparent;
            appearance16.TextHAlignAsString = "Left";
            appearance16.TextVAlignAsString = "Bottom";
            this.ultraLabel1.Appearance = appearance16;
            this.ultraLabel1.Location = new System.Drawing.Point(21, 22);
            this.ultraLabel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(89, 22);
            this.ultraLabel1.TabIndex = 54;
            this.ultraLabel1.Text = "Loại tiền";
            // 
            // ultraLabel3
            // 
            appearance17.BackColor = System.Drawing.Color.Transparent;
            appearance17.TextHAlignAsString = "Left";
            appearance17.TextVAlignAsString = "Bottom";
            this.ultraLabel3.Appearance = appearance17;
            this.ultraLabel3.Location = new System.Drawing.Point(21, 81);
            this.ultraLabel3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(141, 22);
            this.ultraLabel3.TabIndex = 53;
            this.ultraLabel3.Text = "Nhóm khách hàng";
            // 
            // lblEndDate
            // 
            appearance18.BackColor = System.Drawing.Color.Transparent;
            appearance18.TextHAlignAsString = "Left";
            appearance18.TextVAlignAsString = "Bottom";
            this.lblEndDate.Appearance = appearance18;
            this.lblEndDate.Location = new System.Drawing.Point(168, 52);
            this.lblEndDate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.Size = new System.Drawing.Size(33, 21);
            this.lblEndDate.TabIndex = 44;
            this.lblEndDate.Text = "Đến";
            // 
            // cbbDateTime
            // 
            this.cbbDateTime.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbDateTime.Location = new System.Drawing.Point(16, 23);
            this.cbbDateTime.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbbDateTime.Name = "cbbDateTime";
            this.cbbDateTime.NullText = "<chọn dữ liệu>";
            this.cbbDateTime.Size = new System.Drawing.Size(281, 25);
            this.cbbDateTime.TabIndex = 1;
            // 
            // dtBeginDate
            // 
            appearance19.TextHAlignAsString = "Center";
            appearance19.TextVAlignAsString = "Middle";
            this.dtBeginDate.Appearance = appearance19;
            this.dtBeginDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtBeginDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtBeginDate.Location = new System.Drawing.Point(16, 79);
            this.dtBeginDate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dtBeginDate.MaskInput = "";
            this.dtBeginDate.Name = "dtBeginDate";
            this.dtBeginDate.Size = new System.Drawing.Size(132, 24);
            this.dtBeginDate.TabIndex = 2;
            this.dtBeginDate.Value = null;
            // 
            // dtEndDate
            // 
            appearance20.TextHAlignAsString = "Center";
            appearance20.TextVAlignAsString = "Middle";
            this.dtEndDate.Appearance = appearance20;
            this.dtEndDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtEndDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtEndDate.Location = new System.Drawing.Point(165, 79);
            this.dtEndDate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dtEndDate.MaskInput = "";
            this.dtEndDate.Name = "dtEndDate";
            this.dtEndDate.Size = new System.Drawing.Size(132, 24);
            this.dtEndDate.TabIndex = 3;
            this.dtEndDate.Value = null;
            // 
            // cbbFromAccount
            // 
            this.cbbFromAccount.AutoSize = false;
            appearance21.BackColor = System.Drawing.SystemColors.Window;
            appearance21.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbFromAccount.DisplayLayout.Appearance = appearance21;
            this.cbbFromAccount.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbFromAccount.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance22.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance22.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance22.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbFromAccount.DisplayLayout.GroupByBox.Appearance = appearance22;
            appearance23.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbFromAccount.DisplayLayout.GroupByBox.BandLabelAppearance = appearance23;
            this.cbbFromAccount.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance24.BackColor2 = System.Drawing.SystemColors.Control;
            appearance24.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance24.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbFromAccount.DisplayLayout.GroupByBox.PromptAppearance = appearance24;
            this.cbbFromAccount.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbFromAccount.DisplayLayout.MaxRowScrollRegions = 1;
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbFromAccount.DisplayLayout.Override.ActiveCellAppearance = appearance25;
            appearance26.BackColor = System.Drawing.SystemColors.Highlight;
            appearance26.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbFromAccount.DisplayLayout.Override.ActiveRowAppearance = appearance26;
            this.cbbFromAccount.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbFromAccount.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance27.BackColor = System.Drawing.SystemColors.Window;
            this.cbbFromAccount.DisplayLayout.Override.CardAreaAppearance = appearance27;
            appearance28.BorderColor = System.Drawing.Color.Silver;
            appearance28.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbFromAccount.DisplayLayout.Override.CellAppearance = appearance28;
            this.cbbFromAccount.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbFromAccount.DisplayLayout.Override.CellPadding = 0;
            appearance29.BackColor = System.Drawing.SystemColors.Control;
            appearance29.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance29.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance29.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance29.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbFromAccount.DisplayLayout.Override.GroupByRowAppearance = appearance29;
            appearance30.TextHAlignAsString = "Left";
            this.cbbFromAccount.DisplayLayout.Override.HeaderAppearance = appearance30;
            this.cbbFromAccount.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbFromAccount.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            appearance31.BorderColor = System.Drawing.Color.Silver;
            this.cbbFromAccount.DisplayLayout.Override.RowAppearance = appearance31;
            this.cbbFromAccount.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance32.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbFromAccount.DisplayLayout.Override.TemplateAddRowAppearance = appearance32;
            this.cbbFromAccount.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbFromAccount.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbFromAccount.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbFromAccount.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbFromAccount.Location = new System.Drawing.Point(171, 48);
            this.cbbFromAccount.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbbFromAccount.Name = "cbbFromAccount";
            this.cbbFromAccount.Size = new System.Drawing.Size(249, 27);
            this.cbbFromAccount.TabIndex = 5;
            // 
            // cbbCurrency
            // 
            appearance33.BackColor = System.Drawing.SystemColors.Window;
            appearance33.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbCurrency.DisplayLayout.Appearance = appearance33;
            this.cbbCurrency.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbCurrency.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance34.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance34.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance34.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance34.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbCurrency.DisplayLayout.GroupByBox.Appearance = appearance34;
            appearance35.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbCurrency.DisplayLayout.GroupByBox.BandLabelAppearance = appearance35;
            this.cbbCurrency.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance36.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance36.BackColor2 = System.Drawing.SystemColors.Control;
            appearance36.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance36.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbCurrency.DisplayLayout.GroupByBox.PromptAppearance = appearance36;
            this.cbbCurrency.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbCurrency.DisplayLayout.MaxRowScrollRegions = 1;
            appearance37.BackColor = System.Drawing.SystemColors.Window;
            appearance37.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbCurrency.DisplayLayout.Override.ActiveCellAppearance = appearance37;
            appearance38.BackColor = System.Drawing.SystemColors.Highlight;
            appearance38.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbCurrency.DisplayLayout.Override.ActiveRowAppearance = appearance38;
            this.cbbCurrency.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbCurrency.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance39.BackColor = System.Drawing.SystemColors.Window;
            this.cbbCurrency.DisplayLayout.Override.CardAreaAppearance = appearance39;
            appearance40.BorderColor = System.Drawing.Color.Silver;
            appearance40.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbCurrency.DisplayLayout.Override.CellAppearance = appearance40;
            this.cbbCurrency.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbCurrency.DisplayLayout.Override.CellPadding = 0;
            appearance41.BackColor = System.Drawing.SystemColors.Control;
            appearance41.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance41.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance41.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance41.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbCurrency.DisplayLayout.Override.GroupByRowAppearance = appearance41;
            appearance42.TextHAlignAsString = "Left";
            this.cbbCurrency.DisplayLayout.Override.HeaderAppearance = appearance42;
            this.cbbCurrency.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbCurrency.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance43.BackColor = System.Drawing.SystemColors.Window;
            appearance43.BorderColor = System.Drawing.Color.Silver;
            this.cbbCurrency.DisplayLayout.Override.RowAppearance = appearance43;
            this.cbbCurrency.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance44.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbCurrency.DisplayLayout.Override.TemplateAddRowAppearance = appearance44;
            this.cbbCurrency.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbCurrency.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbCurrency.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbCurrency.Location = new System.Drawing.Point(171, 17);
            this.cbbCurrency.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbbCurrency.Name = "cbbCurrency";
            this.cbbCurrency.Size = new System.Drawing.Size(249, 25);
            this.cbbCurrency.TabIndex = 4;
            // 
            // ultraGroupBox4
            // 
            appearance45.BackColor = System.Drawing.Color.Transparent;
            appearance45.BackColor2 = System.Drawing.Color.Transparent;
            this.ultraGroupBox4.Appearance = appearance45;
            this.ultraGroupBox4.Controls.Add(this.cbbFromAccount);
            this.ultraGroupBox4.Controls.Add(this.cbbCurrency);
            this.ultraGroupBox4.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox4.Controls.Add(this.ultraLabel3);
            this.ultraGroupBox4.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox4.Controls.Add(this.cbbAccountingObjectGroupID);
            this.ultraGroupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance47.FontData.BoldAsString = "True";
            appearance47.FontData.SizeInPoints = 13F;
            this.ultraGroupBox4.HeaderAppearance = appearance47;
            this.ultraGroupBox4.Location = new System.Drawing.Point(345, 0);
            this.ultraGroupBox4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ultraGroupBox4.Name = "ultraGroupBox4";
            this.ultraGroupBox4.Size = new System.Drawing.Size(451, 123);
            this.ultraGroupBox4.TabIndex = 2;
            this.ultraGroupBox4.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel2
            // 
            appearance46.BackColor = System.Drawing.Color.Transparent;
            appearance46.TextHAlignAsString = "Left";
            appearance46.TextVAlignAsString = "Bottom";
            this.ultraLabel2.Appearance = appearance46;
            this.ultraLabel2.Location = new System.Drawing.Point(21, 52);
            this.ultraLabel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(141, 22);
            this.ultraLabel2.TabIndex = 52;
            this.ultraLabel2.Text = "Tài khoản (*)";
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraGroupBox4);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraGroupBox1);
            this.ultraPanel1.Location = new System.Drawing.Point(1, 1);
            this.ultraPanel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(796, 123);
            this.ultraPanel1.TabIndex = 14;
            // 
            // ultraGroupBox1
            // 
            appearance48.BackColor = System.Drawing.Color.Transparent;
            appearance48.BackColor2 = System.Drawing.Color.Transparent;
            this.ultraGroupBox1.Appearance = appearance48;
            this.ultraGroupBox1.Controls.Add(this.lblBeginDate);
            this.ultraGroupBox1.Controls.Add(this.lblEndDate);
            this.ultraGroupBox1.Controls.Add(this.cbbDateTime);
            this.ultraGroupBox1.Controls.Add(this.dtBeginDate);
            this.ultraGroupBox1.Controls.Add(this.dtEndDate);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(345, 123);
            this.ultraGroupBox1.TabIndex = 1;
            this.ultraGroupBox1.Text = "Chọn kỳ báo cáo";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // fileReportSlot1
            // 
            this.fileReportSlot1.FilePath = "";
            this.fileReportSlot1.ReportName = "";
            this.fileReportSlot1.ReportScriptType = typeof(PerpetuumSoft.Reporting.Rendering.ReportScriptBase);
            // 
            // reportManager1
            // 
            this.reportManager1.DataSources = new PerpetuumSoft.Reporting.Components.ObjectPointerCollection(new string[0], new object[0]);
            this.reportManager1.OwnerForm = this;
            this.reportManager1.Reports.AddRange(new PerpetuumSoft.Reporting.Components.ReportSlot[] {
            this.fileReportSlot1});
            // 
            // FRCHITIETCONGNOPHAITHUKHACHHANG
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(797, 559);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.ultraPanel2);
            this.Controls.Add(this.ultraPanel1);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FRCHITIETCONGNOPHAITHUKHACHHANG";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Chi tiết công nợ phải thu khách hàng";
            ((System.ComponentModel.ISupportInitialize)(this.ugridAccountingObject)).EndInit();
            this.ultraPanel2.ClientArea.ResumeLayout(false);
            this.ultraPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectGroupID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtBeginDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbFromAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).EndInit();
            this.ultraGroupBox4.ResumeLayout(false);
            this.ultraGroupBox4.PerformLayout();
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private Infragistics.Win.Misc.UltraButton btnOk;
        private Infragistics.Win.Misc.UltraButton btnExit;
        private Infragistics.Win.UltraWinGrid.UltraGrid ugridAccountingObject;
        private Infragistics.Win.Misc.UltraPanel ultraPanel2;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox4;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbFromAccount;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbCurrency;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectGroupID;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraLabel lblBeginDate;
        private Infragistics.Win.Misc.UltraLabel lblEndDate;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbDateTime;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtBeginDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtEndDate;
        private PerpetuumSoft.Reporting.Components.FileReportSlot fileReportSlot1;
        private PerpetuumSoft.Reporting.Components.ReportManager reportManager1;
    }
}