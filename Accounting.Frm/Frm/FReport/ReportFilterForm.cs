﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinMaskedEdit;
using Accounting.Core.Domain;

namespace Accounting
{
    public partial class FReportFilter : Form
    {
        private const string V1 = "Value1";
        private const string V2 = "Value2";
        public IList<ReportFilter> LstFilter { get; set; }
        public FReportFilter()
        {
            InitializeComponent();

        }
        public FReportFilter(IList<ReportFilter> listRF)
        {
            InitializeComponent();
            LstFilter = listRF;
            grfillter.SetDataBinding(LstFilter.Where(c => c.Show.Equals(true)).ToList(), "");
            GridDsachTkReport(grfillter);
            btnCancel.Click += btnCancel_Click;
            btnOk.Click += btnOk_Click;
        }
        private void GridDsachTkReport(UltraGrid ulg)
        {
            Utils.ConfigGrid(grfillter, ConstDatabase.ReportFilter_TableName, false);
            ValueList valueValueList = GetValue(LstFilter);
            ValueList operatorValueList = GetOperator(LstFilter);
            for (int i = 0; i < LstFilter.Count(c => c.Show.Equals(true)); i++)
            {
                int j = valueValueList.FindStringExact(LstFilter[i].Name);
                EmbeddableEditorBase editor;
                if (j >= 0)
                {
                    editor = (EmbeddableEditorBase)valueValueList.ValueListItems[i].Tag;
                    grfillter.Rows[i].Cells["Value1"].Editor = editor;
                        grfillter.Rows[i].Cells["Value2"].Editor = editor;

                }
                int z = operatorValueList.FindStringExact(LstFilter[i].Name);
                if (z >= 0)
                {
                    editor = (EmbeddableEditorBase)operatorValueList.ValueListItems[i].Tag;
                    grfillter.Rows[i].Cells["Operator"].Editor = editor;
                }
            }
            var valueList = new ValueList();
            valueList.ValueListItems.Add("", "Mặc định");
            valueList.ValueListItems.Add("asc", "Tăng dần");
            valueList.ValueListItems.Add("des", "Giảm dần");
            grfillter.DisplayLayout.Bands[0].Columns["Sort"].ValueList = valueList;
            valueList = new ValueList();
            valueList.ValueListItems.Add("and", "Và");
            valueList.ValueListItems.Add("or", "Hoặc");
            grfillter.DisplayLayout.Bands[0].Columns["Condition"].ValueList = valueList;
            ulg.DisplayLayout.Bands[0].Summaries.Clear();
            ulg.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
            ulg.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.True;
            ulg.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            ulg.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            int k = 0;
            foreach (var row in grfillter.Rows)
            {
                if (!string.IsNullOrEmpty(LstFilter[k].Operator))
                {
                    row.Cells[V1].Activation = Activation.AllowEdit;
                    row.Cells[V1].Appearance.BackColor = Color.White;
                    if ((LstFilter[k].FieldType.Name.ToLower() == "datetime"
                        || LstFilter[k].FieldType.Name.ToLower() == "int")
                        && ReferenceEquals(LstFilter[k].Operator, "betwen"))
                    {
                        row.Cells[V2].Activation = Activation.AllowEdit;
                        row.Cells[V2].Appearance.BackColor = Color.White;

                    }
                    else
                    {
                        row.Cells[V2].Activation = Activation.NoEdit;
                        row.Cells[V2].Appearance.BackColor = Color.FromArgb(225, 225, 225);
                    }
                }
                else if (string.IsNullOrEmpty(LstFilter[k].Operator))
                {
                    row.Cells[V1].Activation = Activation.NoEdit;
                    row.Cells[V1].Appearance.BackColor = Color.FromArgb(225, 225, 225);
                    if (LstFilter[k].FieldType.Name.ToLower() == "datetime"
                        || LstFilter[k].FieldType.Name.ToLower() == "int")
                    {
                        row.Cells[V2].Activation = Activation.NoEdit;
                        row.Cells[V2].Appearance.BackColor = Color.FromArgb(225, 225, 225);
                    }

                }
                k++;
            }
        }
        private ValueList GetValue(IEnumerable<ReportFilter> lsttype)
        {
            ValueList editorsValueList = null;
            editorsValueList = new ValueList();
            foreach (var type in lsttype)
            {
                DefaultEditorOwnerSettings editorSettings = null;
                EmbeddableEditorBase editor = null;
                switch (type.FieldType.Name.ToLower())
                {
                    case "int":
                        editorSettings = new DefaultEditorOwnerSettings { DataType = typeof(int) };
                        editor = new EditorWithMask(new DefaultEditorOwner(editorSettings));
                        editorSettings.MaskInput = "-nnnnnnnn";
                        editorsValueList.ValueListItems.Add(editor, type.Name).Tag = editor;
                        break;
                    case "double":
                        editorSettings = new DefaultEditorOwnerSettings
                        {
                            DataType = typeof(double),
                            MaskInput = "-nnnnnnnn.nnnn"
                        };
                        editor = new EditorWithMask(new DefaultEditorOwner(editorSettings));
                        editorsValueList.ValueListItems.Add(editor, "Double").Tag = editor;
                        break;
                    case "decimal":
                        editorSettings = new DefaultEditorOwnerSettings
                        {
                            DataType = typeof(decimal),
                            MaskInput = "-nnnnnnnn.nnnn",
                            Format = "#,###,###,##0"
                        };
                        editor = new EditorWithMask(new DefaultEditorOwner(editorSettings));

                        editorsValueList.ValueListItems.Add(editor, type.Name).Tag = editor;
                        break;
                    case "datetime":
                        // Add an item for editing date.
                        editorSettings = new DefaultEditorOwnerSettings
                        {
                            DataType = typeof(DateTime),
                            MaskInput = "dd/mm/yyyy"
                        };
                        editor = new DateTimeEditor(new DefaultEditorOwner(editorSettings));
                        editorsValueList.ValueListItems.Add(editor, type.Name).Tag = editor;
                        break;
                    default:
                        //mặc định load là loại string
                        editorSettings = new DefaultEditorOwnerSettings { DataType = typeof(string) };
                        editor = new EditorWithText(new DefaultEditorOwner(editorSettings));
                        //editorsValueList.ValueListItems.Add("Text Editor").Tag = editor;
                        editorsValueList.ValueListItems.Add(type.Name).Tag = editor;
                        break;
                }
            }
            return editorsValueList;
        }
        private ValueList GetOperator(IEnumerable<ReportFilter> lsttype)
        {
            //if (null != this._editorsValueList)
            //    return this._editorsValueList;
            ValueList editorsValueList = null;
            editorsValueList = new ValueList();
            foreach (var type in lsttype)
            {
                DefaultEditorOwnerSettings editorSettings = null;
                EmbeddableEditorBase editor = null;
                ValueList valueList = null;
                switch (type.FieldType.Name.ToLower())
                {
                    case "string":
                        editorSettings = new DefaultEditorOwnerSettings();
                        valueList = new ValueList();
                        valueList.ValueListItems.Add("", "");
                        valueList.ValueListItems.Add("==", "Bằng");
                        valueList.ValueListItems.Add("!=", "Khác");
                        valueList.ValueListItems.Add("contain", "Có chứa");
                        editorSettings.ValueList = valueList;
                        editorSettings.DataType = typeof(string);
                        editor = new EditorWithCombo(new DefaultEditorOwner(editorSettings));
                        editorsValueList.ValueListItems.Add(editor, type.Name).Tag = editor;
                        break;
                    case "bool":
                        editorSettings = new DefaultEditorOwnerSettings();
                        editorSettings.DataType = typeof(bool);
                        valueList = new ValueList();
                        valueList.ValueListItems.Add("", "");
                        valueList.ValueListItems.Add("true", "Đúng");
                        valueList.ValueListItems.Add("false", "Sai");
                        editorSettings.ValueList = valueList;
                        editor = new OptionSetEditor(new DefaultEditorOwner(editorSettings));
                        editorsValueList.ValueListItems.Add(editor, type.Name).Tag = editor;
                        break;
                    default:
                        editorSettings = new DefaultEditorOwnerSettings();
                        valueList = new ValueList();
                        valueList.ValueListItems.Add("", "");
                        valueList.ValueListItems.Add("==", "Bằng");
                        valueList.ValueListItems.Add(">", "Lớn hơn");
                        valueList.ValueListItems.Add("<", "Nhỏ hơn");
                        valueList.ValueListItems.Add("betwen", "Trong khoảng");
                        editorSettings.ValueList = valueList;
                        editorSettings.DataType = typeof(string);
                        editor = new EditorWithCombo(new DefaultEditorOwner(editorSettings));
                        editorsValueList.ValueListItems.Add(editor, type.Name).Tag = editor;
                        break;
                }
            }
            return editorsValueList;
        }
        private void btnOk_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void grfillter_CellChange(object sender, CellEventArgs e)
        {
            if (e.Cell.Column.Key == "Operator" && !ReferenceEquals(e.Cell.Row.Cells["Operator"].Editor.Value, ""))
            {
                e.Cell.Row.Cells["Value1"].Activation = Activation.AllowEdit;
                e.Cell.Row.Cells["Value1"].Appearance.BackColor = Color.White;
                if (ReferenceEquals(e.Cell.Row.Cells["Operator"].Editor.Value, "betwen"))
                {
                    e.Cell.Row.Cells["Value2"].Activation = Activation.AllowEdit;
                    e.Cell.Row.Cells["Value2"].Appearance.BackColor = Color.White;

                }
                else
                {
                    e.Cell.Row.Cells["Value2"].Value = null;
                    //default(DateTime);
                    e.Cell.Row.Cells["Value2"].Activation = Activation.NoEdit;
                    e.Cell.Row.Cells["Value2"].Appearance.BackColor = Color.FromArgb(225, 225, 225);
                }
            }
            else if (e.Cell.Column.Key == "Operator" && ReferenceEquals((e.Cell.Row.Cells["Operator"].Editor).Value, ""))
            {
                e.Cell.Row.Cells["Value2"].Value = null;
                e.Cell.Row.Cells["Value1"].Value = null;
                e.Cell.Row.Cells["Value1"].Activation = Activation.NoEdit;
                e.Cell.Row.Cells["Value1"].Appearance.BackColor = Color.FromArgb(225, 225, 225);
                if ((((System.Type)(e.Cell.Row.Cells["FieldType"].Value)).Name.ToLower() == "datetime"
                    || ((System.Type)(e.Cell.Row.Cells["FieldType"].Value)).Name.ToLower() == "int"))
                {
                    e.Cell.Row.Cells["Value2"].Activation = Activation.NoEdit;
                    e.Cell.Row.Cells["Value2"].Appearance.BackColor = Color.FromArgb(225, 225, 225);
                }

            }
        }

        private void grfillter_AfterCellActivate(object sender, EventArgs e)
        {
            //
        }
    }
}
