﻿namespace Accounting
{
    partial class FReportSettingFont
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FReportSettingFont));
            this.SelectFontDialog = new System.Windows.Forms.FontDialog();
            this.txtTitle = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtSubTitle = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtRowTitle = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtDetailText = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtDetailNumber = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtSummaryDetailNumber = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtSummaryFooterText = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtSummaryFooterNumber = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtSignerDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtSignerSubTitle = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtSignerTitle = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtSignerName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.btnTitle = new Infragistics.Win.Misc.UltraButton();
            this.btnSubTitle = new Infragistics.Win.Misc.UltraButton();
            this.btnRowTitle = new Infragistics.Win.Misc.UltraButton();
            this.btnDetailText = new Infragistics.Win.Misc.UltraButton();
            this.btnDetailNumber = new Infragistics.Win.Misc.UltraButton();
            this.btnDetailSum = new Infragistics.Win.Misc.UltraButton();
            this.btnSignerDate = new Infragistics.Win.Misc.UltraButton();
            this.btnSignerTitle = new Infragistics.Win.Misc.UltraButton();
            this.btnSignerSubTitle = new Infragistics.Win.Misc.UltraButton();
            this.btnSignerName = new Infragistics.Win.Misc.UltraButton();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel17 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.lblBeginDate = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel18 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.btnSummaryFooterText = new Infragistics.Win.Misc.UltraButton();
            this.btnSummaryFooterNumber = new Infragistics.Win.Misc.UltraButton();
            this.ultraGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel19 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox5 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.numAlignBottom = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.numAlignRight = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.numAlignLeft = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.numAlignTop = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.ultraGroupBox6 = new Infragistics.Win.Misc.UltraGroupBox();
            this.chkShowProductInfo = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.btnDefault = new Infragistics.Win.Misc.UltraButton();
            this.btnOk = new Infragistics.Win.Misc.UltraButton();
            this.btnCancel = new Infragistics.Win.Misc.UltraButton();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRowTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDetailText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDetailNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSummaryDetailNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSummaryFooterText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSummaryFooterNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSignerDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSignerSubTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSignerTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSignerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).BeginInit();
            this.ultraGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).BeginInit();
            this.ultraGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numAlignBottom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAlignRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAlignLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAlignTop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox6)).BeginInit();
            this.ultraGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowProductInfo)).BeginInit();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtTitle
            // 
            this.txtTitle.Location = new System.Drawing.Point(99, 24);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(445, 21);
            this.txtTitle.TabIndex = 9;
            // 
            // txtSubTitle
            // 
            this.txtSubTitle.Location = new System.Drawing.Point(99, 52);
            this.txtSubTitle.Name = "txtSubTitle";
            this.txtSubTitle.Size = new System.Drawing.Size(445, 21);
            this.txtSubTitle.TabIndex = 10;
            // 
            // txtRowTitle
            // 
            this.txtRowTitle.Location = new System.Drawing.Point(99, 81);
            this.txtRowTitle.Name = "txtRowTitle";
            this.txtRowTitle.Size = new System.Drawing.Size(445, 21);
            this.txtRowTitle.TabIndex = 11;
            // 
            // txtDetailText
            // 
            this.txtDetailText.Location = new System.Drawing.Point(99, 26);
            this.txtDetailText.Name = "txtDetailText";
            this.txtDetailText.Size = new System.Drawing.Size(445, 21);
            this.txtDetailText.TabIndex = 12;
            // 
            // txtDetailNumber
            // 
            this.txtDetailNumber.Location = new System.Drawing.Point(99, 51);
            this.txtDetailNumber.Name = "txtDetailNumber";
            this.txtDetailNumber.Size = new System.Drawing.Size(445, 21);
            this.txtDetailNumber.TabIndex = 13;
            // 
            // txtSummaryDetailNumber
            // 
            this.txtSummaryDetailNumber.Location = new System.Drawing.Point(99, 80);
            this.txtSummaryDetailNumber.Name = "txtSummaryDetailNumber";
            this.txtSummaryDetailNumber.Size = new System.Drawing.Size(445, 21);
            this.txtSummaryDetailNumber.TabIndex = 12;
            // 
            // txtSummaryFooterText
            // 
            this.txtSummaryFooterText.Location = new System.Drawing.Point(99, 25);
            this.txtSummaryFooterText.Name = "txtSummaryFooterText";
            this.txtSummaryFooterText.Size = new System.Drawing.Size(445, 21);
            this.txtSummaryFooterText.TabIndex = 14;
            // 
            // txtSummaryFooterNumber
            // 
            this.txtSummaryFooterNumber.Location = new System.Drawing.Point(99, 55);
            this.txtSummaryFooterNumber.Name = "txtSummaryFooterNumber";
            this.txtSummaryFooterNumber.Size = new System.Drawing.Size(445, 21);
            this.txtSummaryFooterNumber.TabIndex = 15;
            // 
            // txtSignerDate
            // 
            this.txtSignerDate.Location = new System.Drawing.Point(99, 27);
            this.txtSignerDate.Name = "txtSignerDate";
            this.txtSignerDate.Size = new System.Drawing.Size(445, 21);
            this.txtSignerDate.TabIndex = 16;
            // 
            // txtSignerSubTitle
            // 
            this.txtSignerSubTitle.Location = new System.Drawing.Point(99, 85);
            this.txtSignerSubTitle.Name = "txtSignerSubTitle";
            this.txtSignerSubTitle.Size = new System.Drawing.Size(445, 21);
            this.txtSignerSubTitle.TabIndex = 17;
            // 
            // txtSignerTitle
            // 
            this.txtSignerTitle.Location = new System.Drawing.Point(99, 56);
            this.txtSignerTitle.Name = "txtSignerTitle";
            this.txtSignerTitle.Size = new System.Drawing.Size(445, 21);
            this.txtSignerTitle.TabIndex = 18;
            // 
            // txtSignerName
            // 
            this.txtSignerName.Location = new System.Drawing.Point(99, 114);
            this.txtSignerName.Name = "txtSignerName";
            this.txtSignerName.Size = new System.Drawing.Size(445, 21);
            this.txtSignerName.TabIndex = 19;
            // 
            // btnTitle
            // 
            this.btnTitle.Location = new System.Drawing.Point(550, 23);
            this.btnTitle.Name = "btnTitle";
            this.btnTitle.Size = new System.Drawing.Size(30, 21);
            this.btnTitle.TabIndex = 12;
            this.btnTitle.Text = "...";
            this.btnTitle.Click += new System.EventHandler(this.btnTitle_Click);
            // 
            // btnSubTitle
            // 
            this.btnSubTitle.Location = new System.Drawing.Point(550, 52);
            this.btnSubTitle.Name = "btnSubTitle";
            this.btnSubTitle.Size = new System.Drawing.Size(30, 21);
            this.btnSubTitle.TabIndex = 13;
            this.btnSubTitle.Text = "...";
            this.btnSubTitle.Click += new System.EventHandler(this.btnSubTitle_Click);
            // 
            // btnRowTitle
            // 
            this.btnRowTitle.Location = new System.Drawing.Point(550, 81);
            this.btnRowTitle.Name = "btnRowTitle";
            this.btnRowTitle.Size = new System.Drawing.Size(30, 21);
            this.btnRowTitle.TabIndex = 14;
            this.btnRowTitle.Text = "...";
            this.btnRowTitle.Click += new System.EventHandler(this.btnRowTitle_Click);
            // 
            // btnDetailText
            // 
            this.btnDetailText.Location = new System.Drawing.Point(550, 26);
            this.btnDetailText.Name = "btnDetailText";
            this.btnDetailText.Size = new System.Drawing.Size(30, 21);
            this.btnDetailText.TabIndex = 14;
            this.btnDetailText.Text = "...";
            this.btnDetailText.Click += new System.EventHandler(this.btnDetailText_Click);
            // 
            // btnDetailNumber
            // 
            this.btnDetailNumber.Location = new System.Drawing.Point(550, 51);
            this.btnDetailNumber.Name = "btnDetailNumber";
            this.btnDetailNumber.Size = new System.Drawing.Size(30, 21);
            this.btnDetailNumber.TabIndex = 15;
            this.btnDetailNumber.Text = "...";
            this.btnDetailNumber.Click += new System.EventHandler(this.btnDetailNumber_Click);
            // 
            // btnDetailSum
            // 
            this.btnDetailSum.Location = new System.Drawing.Point(550, 81);
            this.btnDetailSum.Name = "btnDetailSum";
            this.btnDetailSum.Size = new System.Drawing.Size(30, 21);
            this.btnDetailSum.TabIndex = 16;
            this.btnDetailSum.Text = "...";
            this.btnDetailSum.Click += new System.EventHandler(this.btnDetailSum_Click);
            // 
            // btnSignerDate
            // 
            this.btnSignerDate.Location = new System.Drawing.Point(550, 27);
            this.btnSignerDate.Name = "btnSignerDate";
            this.btnSignerDate.Size = new System.Drawing.Size(30, 21);
            this.btnSignerDate.TabIndex = 20;
            this.btnSignerDate.Text = "...";
            this.btnSignerDate.Click += new System.EventHandler(this.btnSignerDate_Click);
            // 
            // btnSignerTitle
            // 
            this.btnSignerTitle.Location = new System.Drawing.Point(550, 56);
            this.btnSignerTitle.Name = "btnSignerTitle";
            this.btnSignerTitle.Size = new System.Drawing.Size(30, 21);
            this.btnSignerTitle.TabIndex = 21;
            this.btnSignerTitle.Text = "...";
            this.btnSignerTitle.Click += new System.EventHandler(this.btnSignerTitle_Click);
            // 
            // btnSignerSubTitle
            // 
            this.btnSignerSubTitle.Location = new System.Drawing.Point(550, 85);
            this.btnSignerSubTitle.Name = "btnSignerSubTitle";
            this.btnSignerSubTitle.Size = new System.Drawing.Size(30, 21);
            this.btnSignerSubTitle.TabIndex = 22;
            this.btnSignerSubTitle.Text = "...";
            this.btnSignerSubTitle.Click += new System.EventHandler(this.btnSignerSubTitle_Click);
            // 
            // btnSignerName
            // 
            this.btnSignerName.Location = new System.Drawing.Point(550, 114);
            this.btnSignerName.Name = "btnSignerName";
            this.btnSignerName.Size = new System.Drawing.Size(30, 21);
            this.btnSignerName.TabIndex = 23;
            this.btnSignerName.Text = "...";
            this.btnSignerName.Click += new System.EventHandler(this.btnSignerName_Click);
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.ultraLabel17);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox1.Controls.Add(this.lblBeginDate);
            this.ultraGroupBox1.Controls.Add(this.btnRowTitle);
            this.ultraGroupBox1.Controls.Add(this.txtSubTitle);
            this.ultraGroupBox1.Controls.Add(this.btnSubTitle);
            this.ultraGroupBox1.Controls.Add(this.btnTitle);
            this.ultraGroupBox1.Controls.Add(this.txtRowTitle);
            this.ultraGroupBox1.Controls.Add(this.txtTitle);
            this.ultraGroupBox1.Location = new System.Drawing.Point(12, 12);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(588, 110);
            this.ultraGroupBox1.TabIndex = 19;
            this.ultraGroupBox1.Text = "Tiêu đề";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel17
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.TextHAlignAsString = "Left";
            appearance1.TextVAlignAsString = "Bottom";
            this.ultraLabel17.Appearance = appearance1;
            this.ultraLabel17.Location = new System.Drawing.Point(6, 81);
            this.ultraLabel17.Name = "ultraLabel17";
            this.ultraLabel17.Size = new System.Drawing.Size(87, 19);
            this.ultraLabel17.TabIndex = 41;
            this.ultraLabel17.Text = "Tiêu đề cột";
            // 
            // ultraLabel1
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.TextHAlignAsString = "Left";
            appearance2.TextVAlignAsString = "Bottom";
            this.ultraLabel1.Appearance = appearance2;
            this.ultraLabel1.Location = new System.Drawing.Point(6, 52);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(87, 19);
            this.ultraLabel1.TabIndex = 40;
            this.ultraLabel1.Text = "Tiêu đề phụ";
            // 
            // lblBeginDate
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextHAlignAsString = "Left";
            appearance3.TextVAlignAsString = "Bottom";
            this.lblBeginDate.Appearance = appearance3;
            this.lblBeginDate.Location = new System.Drawing.Point(6, 23);
            this.lblBeginDate.Name = "lblBeginDate";
            this.lblBeginDate.Size = new System.Drawing.Size(87, 19);
            this.lblBeginDate.TabIndex = 39;
            this.lblBeginDate.Text = "Tiêu đề báo cáo";
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel3);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel18);
            this.ultraGroupBox2.Controls.Add(this.txtSummaryDetailNumber);
            this.ultraGroupBox2.Controls.Add(this.btnDetailSum);
            this.ultraGroupBox2.Controls.Add(this.txtDetailNumber);
            this.ultraGroupBox2.Controls.Add(this.txtDetailText);
            this.ultraGroupBox2.Controls.Add(this.btnDetailNumber);
            this.ultraGroupBox2.Controls.Add(this.btnDetailText);
            this.ultraGroupBox2.Location = new System.Drawing.Point(12, 126);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(588, 110);
            this.ultraGroupBox2.TabIndex = 20;
            this.ultraGroupBox2.Text = "Nội dung";
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel2
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextHAlignAsString = "Left";
            appearance4.TextVAlignAsString = "Bottom";
            this.ultraLabel2.Appearance = appearance4;
            this.ultraLabel2.Location = new System.Drawing.Point(6, 79);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(87, 19);
            this.ultraLabel2.TabIndex = 44;
            this.ultraLabel2.Text = "Phần tổng cộng";
            // 
            // ultraLabel3
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.TextHAlignAsString = "Left";
            appearance5.TextVAlignAsString = "Bottom";
            this.ultraLabel3.Appearance = appearance5;
            this.ultraLabel3.Location = new System.Drawing.Point(6, 50);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(87, 19);
            this.ultraLabel3.TabIndex = 43;
            this.ultraLabel3.Text = "Phần kiểu số";
            // 
            // ultraLabel18
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextHAlignAsString = "Left";
            appearance6.TextVAlignAsString = "Bottom";
            this.ultraLabel18.Appearance = appearance6;
            this.ultraLabel18.Location = new System.Drawing.Point(6, 25);
            this.ultraLabel18.Name = "ultraLabel18";
            this.ultraLabel18.Size = new System.Drawing.Size(87, 19);
            this.ultraLabel18.TabIndex = 42;
            this.ultraLabel18.Text = "Phần kiểu chữ";
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Controls.Add(this.ultraLabel4);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel5);
            this.ultraGroupBox3.Controls.Add(this.btnSummaryFooterText);
            this.ultraGroupBox3.Controls.Add(this.btnSummaryFooterNumber);
            this.ultraGroupBox3.Controls.Add(this.txtSummaryFooterNumber);
            this.ultraGroupBox3.Controls.Add(this.txtSummaryFooterText);
            this.ultraGroupBox3.Location = new System.Drawing.Point(12, 238);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(588, 85);
            this.ultraGroupBox3.TabIndex = 21;
            this.ultraGroupBox3.Text = "Cuối trang";
            this.ultraGroupBox3.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel4
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.TextHAlignAsString = "Left";
            appearance7.TextVAlignAsString = "Bottom";
            this.ultraLabel4.Appearance = appearance7;
            this.ultraLabel4.Location = new System.Drawing.Point(6, 54);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(87, 19);
            this.ultraLabel4.TabIndex = 45;
            this.ultraLabel4.Text = "Phần tổng cộng";
            // 
            // ultraLabel5
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.TextHAlignAsString = "Left";
            appearance8.TextVAlignAsString = "Bottom";
            this.ultraLabel5.Appearance = appearance8;
            this.ultraLabel5.Location = new System.Drawing.Point(6, 24);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(87, 19);
            this.ultraLabel5.TabIndex = 44;
            this.ultraLabel5.Text = "Phần kiểu chữ";
            // 
            // btnSummaryFooterText
            // 
            this.btnSummaryFooterText.Location = new System.Drawing.Point(550, 24);
            this.btnSummaryFooterText.Name = "btnSummaryFooterText";
            this.btnSummaryFooterText.Size = new System.Drawing.Size(30, 21);
            this.btnSummaryFooterText.TabIndex = 23;
            this.btnSummaryFooterText.Text = "...";
            this.btnSummaryFooterText.Click += new System.EventHandler(this.btnSummaryFooterText_Click);
            // 
            // btnSummaryFooterNumber
            // 
            this.btnSummaryFooterNumber.Location = new System.Drawing.Point(550, 54);
            this.btnSummaryFooterNumber.Name = "btnSummaryFooterNumber";
            this.btnSummaryFooterNumber.Size = new System.Drawing.Size(30, 21);
            this.btnSummaryFooterNumber.TabIndex = 22;
            this.btnSummaryFooterNumber.Text = "...";
            this.btnSummaryFooterNumber.Click += new System.EventHandler(this.btnSummaryFooterNumber_Click);
            // 
            // ultraGroupBox4
            // 
            this.ultraGroupBox4.Controls.Add(this.ultraLabel19);
            this.ultraGroupBox4.Controls.Add(this.ultraLabel6);
            this.ultraGroupBox4.Controls.Add(this.ultraLabel7);
            this.ultraGroupBox4.Controls.Add(this.ultraLabel8);
            this.ultraGroupBox4.Controls.Add(this.txtSignerName);
            this.ultraGroupBox4.Controls.Add(this.btnSignerName);
            this.ultraGroupBox4.Controls.Add(this.btnSignerSubTitle);
            this.ultraGroupBox4.Controls.Add(this.btnSignerDate);
            this.ultraGroupBox4.Controls.Add(this.txtSignerSubTitle);
            this.ultraGroupBox4.Controls.Add(this.txtSignerTitle);
            this.ultraGroupBox4.Controls.Add(this.btnSignerTitle);
            this.ultraGroupBox4.Controls.Add(this.txtSignerDate);
            this.ultraGroupBox4.Location = new System.Drawing.Point(12, 326);
            this.ultraGroupBox4.Name = "ultraGroupBox4";
            this.ultraGroupBox4.Size = new System.Drawing.Size(588, 146);
            this.ultraGroupBox4.TabIndex = 22;
            this.ultraGroupBox4.Text = "Chữ ký";
            this.ultraGroupBox4.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel19
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.TextHAlignAsString = "Left";
            appearance9.TextVAlignAsString = "Bottom";
            this.ultraLabel19.Appearance = appearance9;
            this.ultraLabel19.Location = new System.Drawing.Point(6, 113);
            this.ultraLabel19.Name = "ultraLabel19";
            this.ultraLabel19.Size = new System.Drawing.Size(87, 19);
            this.ultraLabel19.TabIndex = 48;
            this.ultraLabel19.Text = "(Họ và tên)";
            // 
            // ultraLabel6
            // 
            appearance10.BackColor = System.Drawing.Color.Transparent;
            appearance10.TextHAlignAsString = "Left";
            appearance10.TextVAlignAsString = "Bottom";
            this.ultraLabel6.Appearance = appearance10;
            this.ultraLabel6.Location = new System.Drawing.Point(6, 84);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(87, 19);
            this.ultraLabel6.TabIndex = 47;
            this.ultraLabel6.Text = "(Ký, họ tên)";
            // 
            // ultraLabel7
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            appearance11.TextHAlignAsString = "Left";
            appearance11.TextVAlignAsString = "Bottom";
            this.ultraLabel7.Appearance = appearance11;
            this.ultraLabel7.Location = new System.Drawing.Point(6, 55);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(87, 19);
            this.ultraLabel7.TabIndex = 46;
            this.ultraLabel7.Text = "Chức danh";
            // 
            // ultraLabel8
            // 
            appearance12.BackColor = System.Drawing.Color.Transparent;
            appearance12.TextHAlignAsString = "Left";
            appearance12.TextVAlignAsString = "Bottom";
            this.ultraLabel8.Appearance = appearance12;
            this.ultraLabel8.Location = new System.Drawing.Point(6, 30);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(87, 19);
            this.ultraLabel8.TabIndex = 45;
            this.ultraLabel8.Text = "Ngày tháng";
            // 
            // ultraGroupBox5
            // 
            this.ultraGroupBox5.Controls.Add(this.ultraLabel11);
            this.ultraGroupBox5.Controls.Add(this.ultraLabel12);
            this.ultraGroupBox5.Controls.Add(this.ultraLabel9);
            this.ultraGroupBox5.Controls.Add(this.ultraLabel10);
            this.ultraGroupBox5.Controls.Add(this.numAlignBottom);
            this.ultraGroupBox5.Controls.Add(this.numAlignRight);
            this.ultraGroupBox5.Controls.Add(this.numAlignLeft);
            this.ultraGroupBox5.Controls.Add(this.numAlignTop);
            this.ultraGroupBox5.Location = new System.Drawing.Point(12, 478);
            this.ultraGroupBox5.Name = "ultraGroupBox5";
            this.ultraGroupBox5.Size = new System.Drawing.Size(270, 91);
            this.ultraGroupBox5.TabIndex = 23;
            this.ultraGroupBox5.Text = "Căn lề";
            this.ultraGroupBox5.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel11
            // 
            appearance13.BackColor = System.Drawing.Color.Transparent;
            appearance13.TextHAlignAsString = "Left";
            appearance13.TextVAlignAsString = "Bottom";
            this.ultraLabel11.Appearance = appearance13;
            this.ultraLabel11.Location = new System.Drawing.Point(151, 51);
            this.ultraLabel11.Name = "ultraLabel11";
            this.ultraLabel11.Size = new System.Drawing.Size(42, 19);
            this.ultraLabel11.TabIndex = 50;
            this.ultraLabel11.Text = "Lề phải";
            // 
            // ultraLabel12
            // 
            appearance14.BackColor = System.Drawing.Color.Transparent;
            appearance14.TextHAlignAsString = "Left";
            appearance14.TextVAlignAsString = "Bottom";
            this.ultraLabel12.Appearance = appearance14;
            this.ultraLabel12.Location = new System.Drawing.Point(151, 26);
            this.ultraLabel12.Name = "ultraLabel12";
            this.ultraLabel12.Size = new System.Drawing.Size(42, 19);
            this.ultraLabel12.TabIndex = 49;
            this.ultraLabel12.Text = "Lề trái";
            // 
            // ultraLabel9
            // 
            appearance15.BackColor = System.Drawing.Color.Transparent;
            appearance15.TextHAlignAsString = "Left";
            appearance15.TextVAlignAsString = "Bottom";
            this.ultraLabel9.Appearance = appearance15;
            this.ultraLabel9.Location = new System.Drawing.Point(6, 51);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(42, 19);
            this.ultraLabel9.TabIndex = 48;
            this.ultraLabel9.Text = "Lề dưới";
            // 
            // ultraLabel10
            // 
            appearance16.BackColor = System.Drawing.Color.Transparent;
            appearance16.TextHAlignAsString = "Left";
            appearance16.TextVAlignAsString = "Bottom";
            this.ultraLabel10.Appearance = appearance16;
            this.ultraLabel10.Location = new System.Drawing.Point(6, 26);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(42, 19);
            this.ultraLabel10.TabIndex = 47;
            this.ultraLabel10.Text = "Lề trên";
            // 
            // numAlignBottom
            // 
            this.numAlignBottom.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.numAlignBottom.Location = new System.Drawing.Point(54, 52);
            this.numAlignBottom.MaskInput = "{double:1.1}";
            this.numAlignBottom.Name = "numAlignBottom";
            this.numAlignBottom.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Double;
            this.numAlignBottom.Size = new System.Drawing.Size(59, 21);
            this.numAlignBottom.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always;
            this.numAlignBottom.TabIndex = 35;
            // 
            // numAlignRight
            // 
            this.numAlignRight.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.numAlignRight.Location = new System.Drawing.Point(199, 52);
            this.numAlignRight.MaskInput = "{double:1.1}";
            this.numAlignRight.Name = "numAlignRight";
            this.numAlignRight.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Double;
            this.numAlignRight.Size = new System.Drawing.Size(59, 21);
            this.numAlignRight.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always;
            this.numAlignRight.TabIndex = 34;
            // 
            // numAlignLeft
            // 
            this.numAlignLeft.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.numAlignLeft.Location = new System.Drawing.Point(199, 27);
            this.numAlignLeft.MaskInput = "{double:1.1}";
            this.numAlignLeft.Name = "numAlignLeft";
            this.numAlignLeft.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Double;
            this.numAlignLeft.Size = new System.Drawing.Size(59, 21);
            this.numAlignLeft.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always;
            this.numAlignLeft.TabIndex = 33;
            // 
            // numAlignTop
            // 
            this.numAlignTop.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.numAlignTop.Location = new System.Drawing.Point(54, 27);
            this.numAlignTop.MaskInput = "{double:1.1}";
            this.numAlignTop.Name = "numAlignTop";
            this.numAlignTop.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Double;
            this.numAlignTop.Size = new System.Drawing.Size(59, 21);
            this.numAlignTop.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always;
            this.numAlignTop.TabIndex = 32;
            // 
            // ultraGroupBox6
            // 
            this.ultraGroupBox6.Controls.Add(this.chkShowProductInfo);
            this.ultraGroupBox6.Location = new System.Drawing.Point(296, 478);
            this.ultraGroupBox6.MaximumSize = new System.Drawing.Size(304, 91);
            this.ultraGroupBox6.MinimumSize = new System.Drawing.Size(304, 91);
            this.ultraGroupBox6.Name = "ultraGroupBox6";
            this.ultraGroupBox6.Size = new System.Drawing.Size(304, 91);
            this.ultraGroupBox6.TabIndex = 24;
            this.ultraGroupBox6.Text = "Tùy chọn hiển thị thông tin";
            this.ultraGroupBox6.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // chkShowProductInfo
            // 
            this.chkShowProductInfo.BackColor = System.Drawing.Color.Transparent;
            this.chkShowProductInfo.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkShowProductInfo.Location = new System.Drawing.Point(6, 28);
            this.chkShowProductInfo.Name = "chkShowProductInfo";
            this.chkShowProductInfo.Size = new System.Drawing.Size(290, 20);
            this.chkShowProductInfo.TabIndex = 0;
            this.chkShowProductInfo.Text = "Hiện thông tin sản phẩm";
            this.chkShowProductInfo.CheckedChanged += new System.EventHandler(this.chkShowProductInfo_CheckedChanged);
            // 
            // btnDefault
            // 
            appearance17.Image = global::Accounting.Properties.Resources.font_type1;
            this.btnDefault.Appearance = appearance17;
            this.btnDefault.Location = new System.Drawing.Point(12, 573);
            this.btnDefault.Name = "btnDefault";
            this.btnDefault.Size = new System.Drawing.Size(93, 31);
            this.btnDefault.TabIndex = 25;
            this.btnDefault.Text = "Mặc định";
            this.btnDefault.Click += new System.EventHandler(this.btnDefault_Click);
            // 
            // btnOk
            // 
            appearance18.Image = global::Accounting.Properties.Resources.apply_16;
            this.btnOk.Appearance = appearance18;
            this.btnOk.Location = new System.Drawing.Point(440, 573);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 31);
            this.btnOk.TabIndex = 26;
            this.btnOk.Text = "Đồng ý";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            appearance19.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnCancel.Appearance = appearance19;
            this.btnCancel.Location = new System.Drawing.Point(521, 573);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 31);
            this.btnCancel.TabIndex = 27;
            this.btnCancel.Text = "Đóng";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.btnOk);
            this.ultraPanel1.ClientArea.Controls.Add(this.btnDefault);
            this.ultraPanel1.ClientArea.Controls.Add(this.btnCancel);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraGroupBox1);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraGroupBox2);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraGroupBox3);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraGroupBox4);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraGroupBox5);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraGroupBox6);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(608, 607);
            this.ultraPanel1.TabIndex = 24;
            // 
            // FReportSettingFont
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(608, 607);
            this.Controls.Add(this.ultraPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FReportSettingFont";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tùy chỉnh";
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRowTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDetailText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDetailNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSummaryDetailNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSummaryFooterText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSummaryFooterNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSignerDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSignerSubTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSignerTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSignerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ultraGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            this.ultraGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).EndInit();
            this.ultraGroupBox4.ResumeLayout(false);
            this.ultraGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).EndInit();
            this.ultraGroupBox5.ResumeLayout(false);
            this.ultraGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numAlignBottom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAlignRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAlignLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAlignTop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox6)).EndInit();
            this.ultraGroupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkShowProductInfo)).EndInit();
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FontDialog SelectFontDialog;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtRowTitle;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSubTitle;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTitle;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDetailText;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSummaryDetailNumber;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDetailNumber;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSummaryFooterNumber;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSummaryFooterText;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSignerName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSignerTitle;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSignerSubTitle;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSignerDate;
        private Infragistics.Win.Misc.UltraButton btnRowTitle;
        private Infragistics.Win.Misc.UltraButton btnSubTitle;
        private Infragistics.Win.Misc.UltraButton btnTitle;
        private Infragistics.Win.Misc.UltraButton btnDetailSum;
        private Infragistics.Win.Misc.UltraButton btnDetailNumber;
        private Infragistics.Win.Misc.UltraButton btnDetailText;
        private Infragistics.Win.Misc.UltraButton btnSignerName;
        private Infragistics.Win.Misc.UltraButton btnSignerSubTitle;
        private Infragistics.Win.Misc.UltraButton btnSignerTitle;
        private Infragistics.Win.Misc.UltraButton btnSignerDate;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox4;
        private Infragistics.Win.Misc.UltraButton btnSummaryFooterText;
        private Infragistics.Win.Misc.UltraButton btnSummaryFooterNumber;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox5;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor numAlignBottom;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor numAlignRight;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor numAlignLeft;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor numAlignTop;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox6;
        private Infragistics.Win.Misc.UltraButton btnDefault;
        private Infragistics.Win.Misc.UltraButton btnOk;
        private Infragistics.Win.Misc.UltraButton btnCancel;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel17;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel lblBeginDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel18;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel19;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
        private Infragistics.Win.Misc.UltraLabel ultraLabel12;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkShowProductInfo;
    }
}