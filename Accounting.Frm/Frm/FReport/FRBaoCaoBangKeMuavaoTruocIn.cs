﻿using Accounting.Core.Domain.obj.Report;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using Accounting.Core.DAO;
using Infragistics.Documents.Excel;

namespace Accounting.Frm.FReport
{
    public partial class FRBaoCaoBangKeMuavaoTruocIn : Form
    {
        List<BangKeMuaVao_Model> data1 = new List<BangKeMuaVao_Model>();

        string tieude1;
        string _subSystemCode1;
        int check1;
        DateTime begin1;
        DateTime end1;
        public FRBaoCaoBangKeMuavaoTruocIn(List<BangKeMuaVao_Model> dataformtruoc, string tieude, string _subSystemCode, int check, DateTime begin, DateTime end)
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();
            ReportProcedureSDS sp = new ReportProcedureSDS();
            List<BangKeMuaVao_Model> data = new List<BangKeMuaVao_Model>();
            
            data = sp.GetProc_GetBkeInv_Buy(begin, end, check, 0, "");
            data.ForEach(t =>
            {
                t.THUE_SUAT_DETAIL = Convert.ToInt32(t.THUE_SUAT);
                if (t.FLAG == -1)
                {
                    t.GT_CHUATHUE = -t.GT_CHUATHUE;
                    t.THUE_GTGT = -t.THUE_GTGT;
                }
            });
            begin1 = begin;
            end1 = end;
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\BangKeHoaDonThueMuaBan.rst", path);
            //data1 = data.CloneObject<List<BangKeMuaVao_Model>>();
            List<BangKeMuaVao_Model> listGroup = (from a in data
                                                  group a by a.GOODSSERVICEPURCHASECODE into g
                                                  select new BangKeMuaVao_Model
                                                  { Ordercode = 1,
                                                      GOODSSERVICEPURCHASECODE = g.Key,

                                                      GT_CHUATHUE = g.Sum(t => t.GT_CHUATHUE),
                                                      THUE_GTGT = g.Sum(t => t.THUE_GTGT)
                                                  }).ToList();
           // data1 = new List<BangKeMuaVao_Model>(data);
            check1 = check;
            decimal sumGT_CHUATHUE = 0;
            decimal sumTHUE_GTGT = 0;
            foreach (var item in listGroup)
            {
                sumGT_CHUATHUE += item.GT_CHUATHUE;
                sumTHUE_GTGT += item.THUE_GTGT;
                data.Add(item);
            }
            string GOODSSERVICEPURCHASECODE = "";
            List<string> code = new List<string>();
            //if (data[0].GOODSSERVICEPURCHASECODE != null)
            //{ GOODSSERVICEPURCHASECODE = data[0].GOODSSERVICEPURCHASECODE; }
            for (int i = 0; i < data.Count; i++)
            {
                if (!code.Contains(data[i].GOODSSERVICEPURCHASECODE))
                {
                    code.Add(data[i].GOODSSERVICEPURCHASECODE);
                    BangKeMuaVao_Model bangKeMuaVao_Model = new BangKeMuaVao_Model();
                    bangKeMuaVao_Model.GOODSSERVICEPURCHASECODE = data[i].GOODSSERVICEPURCHASECODE;
                    data.FirstOrDefault(n => n.GOODSSERVICEPURCHASECODE == data[i].GOODSSERVICEPURCHASECODE && n.Ordercode == 1).GOODSSERVICEPURCHASENAME = data[i].GOODSSERVICEPURCHASENAME;
                    data.FirstOrDefault(n => n.GOODSSERVICEPURCHASECODE == data[i].GOODSSERVICEPURCHASECODE && n.Ordercode == 1).SO_HD = data[i].GOODSSERVICEPURCHASENAME;
                    bangKeMuaVao_Model.GOODSSERVICEPURCHASENAME = data[i].GOODSSERVICEPURCHASENAME;
                    //bangKeMuaVao_Model.TEN_NBAN = data[i].GOODSSERVICEPURCHASENAME;
                    bangKeMuaVao_Model.SO_HD = data[i].GOODSSERVICEPURCHASENAME;
                    bangKeMuaVao_Model.Ordercode = -1;
                    data.Add(bangKeMuaVao_Model);

                }

                //if (item.Ordercode != 0)
                //{
                //    item.Ordercode = 1;
                //}
                if (data[i].THUE_SUAT_DETAIL == -1)
                {
                    data[i].THUE_SUAT_STRING = "Không chịu thuế";
                }
                if (data[i].THUE_SUAT_DETAIL == -2)
                {
                    data[i].THUE_SUAT_STRING = "Không tính thuế";
                }
                if (data[i].THUE_SUAT_DETAIL != -2 && data[i].THUE_SUAT_DETAIL != -1 && data[i].THUE_SUAT_DETAIL != 0)
                {
                    data[i].THUE_SUAT_STRING = data[i].THUE_SUAT_DETAIL.ToString() + "%";
                }
            }
            data = data.OrderBy(n => n.GOODSSERVICEPURCHASECODE).ThenBy(n => n.Ordercode).ThenBy(n => n.NGAY_HD).ThenBy(n => n.SO_HD).ToList();
            tieude1 = tieude;
            _subSystemCode1 = _subSystemCode;

            uGridDuLieu.DataSource = data;
            
            Utils.ConfigGrid(uGridDuLieu, ConstDatabase.BangKeMuaVao_Model_TableName);
            uGridDuLieu.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            Utils.AddSumColumn(uGridDuLieu, "NGAY_HD", false);
            Utils.AddSumColumn(uGridDuLieu, "TEN_NBAN", false);
            Utils.AddSumColumn(uGridDuLieu, "MST", false);
            Utils.AddSumColumn(uGridDuLieu, "MAT_HANG", false);
            Utils.AddSumColumn(uGridDuLieu, "TK_THUE", false);
            Utils.AddSumColumn(uGridDuLieu, "THUE_SUAT_STRING", false);

            uGridDuLieu.DisplayLayout.Bands[0].Summaries[1].DisplayFormat = " ";
            uGridDuLieu.DisplayLayout.Bands[0].Summaries[2].DisplayFormat = " ";
            uGridDuLieu.DisplayLayout.Bands[0].Summaries[3].DisplayFormat = " ";
            uGridDuLieu.DisplayLayout.Bands[0].Summaries[4].DisplayFormat = " ";
            uGridDuLieu.DisplayLayout.Bands[0].Summaries[5].DisplayFormat = " ";
            uGridDuLieu.DisplayLayout.Bands[0].Summaries[6].DisplayFormat = " ";

            Utils.AddSumColumn1(sumGT_CHUATHUE, uGridDuLieu, "GT_CHUATHUE", false, constDatabaseFormat:1);
            Utils.AddSumColumn1(sumTHUE_GTGT, uGridDuLieu, "THUE_GTGT", false, constDatabaseFormat:1);
            uGridDuLieu.DisplayLayout.Bands[0].Override.GroupByRowDescriptionMask = "Nhóm HHDV :[value] ( [count] )";
            uGridDuLieu.DisplayLayout.Bands[0].Columns["GT_CHUATHUE"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGridDuLieu.DisplayLayout.Bands[0].Columns["THUE_GTGT"].FormatNumberic(ConstDatabase.Format_TienVND);
            SetDoSoAm(uGridDuLieu);
            UltraGridBand parentBand = this.uGridDuLieu.DisplayLayout.Bands[0];
            parentBand.RowLayoutStyle = RowLayoutStyle.GroupLayout;
            parentBand.Override.HeaderPlacement = HeaderPlacement.FixedOnTop;

            UltraGridGroup parentBandGroupPeriod;
            if (parentBand.Groups.Exists("ParentBandGroupPeriod"))
            {
                parentBandGroupPeriod = parentBand.Groups["ParentBandGroupPeriod"];
                parentBandGroupPeriod.Header.Caption = tieude;
            }
            else
                parentBandGroupPeriod = parentBand.Groups.Add("ParentBandGroupPeriod", tieude);
            parentBandGroupPeriod.Header.Appearance.FontData.Italic = DefaultableBoolean.True;
            parentBandGroupPeriod.Header.Appearance.FontData.SizeInPoints = 10;
            parentBandGroupPeriod.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupPeriod.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPeriod.Header.Appearance.BorderColor = Color.Black;
            UltraGridGroup parentBandGroupTitle;
            if (parentBand.Groups.Exists("ParentBandGroupTitle"))
                parentBandGroupTitle = parentBand.Groups["ParentBandGroupTitle"];
            else
                parentBandGroupTitle = parentBand.Groups.Add("ParentBandGroupTitle", "BẢNG KÊ HOÁ ĐƠN, CHỨNG TỪ HÀNG HOÁ MUA VÀO (MẪU QUẢN TRỊ)");
            parentBandGroupTitle.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroupTitle.Header.Appearance.FontData.SizeInPoints = 12;
            parentBandGroupTitle.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupTitle.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupTitle.Header.Appearance.BorderColor = Color.Black;
            parentBandGroupPeriod.RowLayoutGroupInfo.ParentGroup = parentBandGroupTitle;


            //UltraGridGroup parentBandGroupSO_HD;
            //if (parentBand.Groups.Exists("ParentBandGroupSO_HD"))
            //    parentBandGroupSO_HD = parentBand.Groups["ParentBandGroupSO_HD"];
            //else
            //    parentBandGroupSO_HD = parentBand.Groups.Add("ParentBandGroupSO_HD", "");
            //parentBandGroupSO_HD.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            //parentBandGroupSO_HD.Header.Appearance.FontData.SizeInPoints = 2;
            //parentBandGroupSO_HD.Header.Appearance.TextHAlign = HAlign.Center;
            //parentBandGroupSO_HD.Header.Appearance.BorderAlpha = Alpha.Transparent;

            ////parentBandGroupSO_HD.Header.Appearance.BorderColor = Color.Black;
            //parentBandGroupSO_HD.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            //parentBandGroupSO_HD.RowLayoutGroupInfo.SpanY = 2;


            //UltraGridGroup parentBandGroupNGAY_HD;
            //if (parentBand.Groups.Exists("ParentBandGroupNGAY_HD"))
            //    parentBandGroupNGAY_HD = parentBand.Groups["ParentBandGroupNGAY_HD"];
            //else
            //    parentBandGroupNGAY_HD = parentBand.Groups.Add("ParentBandGroupNGAY_HD", "");
            //parentBandGroupNGAY_HD.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            //parentBandGroupNGAY_HD.Header.Appearance.FontData.SizeInPoints = 2;
            //parentBandGroupNGAY_HD.Header.Appearance.TextHAlign = HAlign.Center;
            //parentBandGroupNGAY_HD.Header.Appearance.BorderAlpha = Alpha.Transparent;
            ////parentBandGroupNGAY_HD.Header.Appearance.BorderColor = Color.Black;
            //parentBandGroupNGAY_HD.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            //parentBandGroupNGAY_HD.RowLayoutGroupInfo.SpanY = 2;

            //UltraGridGroup parentBandGroupTEN_NBAN;
            //if (parentBand.Groups.Exists("ParentBandGroupTEN_NBAN"))
            //    parentBandGroupTEN_NBAN = parentBand.Groups["ParentBandGroupTEN_NBAN"];
            //else
            //    parentBandGroupTEN_NBAN = parentBand.Groups.Add("ParentBandGroupTEN_NBAN", "");
            //parentBandGroupTEN_NBAN.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            //parentBandGroupTEN_NBAN.Header.Appearance.FontData.SizeInPoints = 2;
            //parentBandGroupTEN_NBAN.Header.Appearance.TextHAlign = HAlign.Center;
            //parentBandGroupTEN_NBAN.Header.Appearance.BorderAlpha = Alpha.Transparent;
            ////parentBandGroupTEN_NBAN.Header.Appearance.BorderColor = Color.Black;
            //parentBandGroupTEN_NBAN.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            //parentBandGroupTEN_NBAN.RowLayoutGroupInfo.SpanY = 2;

            //UltraGridGroup parentBandGroupMST;
            //if (parentBand.Groups.Exists("ParentBandGroupMST"))
            //    parentBandGroupMST = parentBand.Groups["ParentBandGroupMST"];
            //else
            //    parentBandGroupMST = parentBand.Groups.Add("ParentBandGroupMST", "");
            //parentBandGroupMST.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            //parentBandGroupMST.Header.Appearance.FontData.SizeInPoints = 2;
            //parentBandGroupMST.Header.Appearance.TextHAlign = HAlign.Center;
            //parentBandGroupMST.Header.Appearance.BorderAlpha = Alpha.Transparent;
            ////parentBandGroupMST.Header.Appearance.BorderColor = Color.Black;
            //parentBandGroupMST.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            //parentBandGroupMST.RowLayoutGroupInfo.SpanY = 2;


            //UltraGridGroup parentBandGroupMAT_HANG;
            //if (parentBand.Groups.Exists("ParentBandGroupMAT_HANG"))
            //    parentBandGroupMAT_HANG = parentBand.Groups["ParentBandGroupMAT_HANG"];
            //else
            //    parentBandGroupMAT_HANG = parentBand.Groups.Add("ParentBandGroupMAT_HANG", "");
            //parentBandGroupMAT_HANG.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            //parentBandGroupMAT_HANG.Header.Appearance.FontData.SizeInPoints = 2;
            //parentBandGroupMAT_HANG.Header.Appearance.TextHAlign = HAlign.Center;
            //parentBandGroupMAT_HANG.Header.Appearance.BorderAlpha = Alpha.Transparent;
            ////parentBandGroupMAT_HANG.Header.Appearance.BorderColor = Color.Black;
            //parentBandGroupMAT_HANG.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            //parentBandGroupMAT_HANG.RowLayoutGroupInfo.SpanY = 2;


            //UltraGridGroup parentBandGroupGT_CHUATHUE;
            //if (parentBand.Groups.Exists("ParentBandGroupGT_CHUATHUE"))
            //    parentBandGroupGT_CHUATHUE = parentBand.Groups["ParentBandGroupGT_CHUATHUE"];
            //else
            //    parentBandGroupGT_CHUATHUE = parentBand.Groups.Add("ParentBandGroupGT_CHUATHUE", "");
            //parentBandGroupGT_CHUATHUE.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            //parentBandGroupGT_CHUATHUE.Header.Appearance.FontData.SizeInPoints = 2;
            //parentBandGroupGT_CHUATHUE.Header.Appearance.TextHAlign = HAlign.Center;
            //parentBandGroupGT_CHUATHUE.Header.Appearance.BorderAlpha = Alpha.Transparent;
            ////parentBandGroupGT_CHUATHUE.Header.Appearance.BorderColor = Color.Black;
            //parentBandGroupGT_CHUATHUE.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            //parentBandGroupGT_CHUATHUE.RowLayoutGroupInfo.SpanY = 2;


            //UltraGridGroup parentBandGroupTHUE_SUAT_STRING;
            //if (parentBand.Groups.Exists("ParentBandGroupTHUE_SUAT_STRING"))
            //    parentBandGroupTHUE_SUAT_STRING = parentBand.Groups["ParentBandGroupTHUE_SUAT_STRING"];
            //else
            //    parentBandGroupTHUE_SUAT_STRING = parentBand.Groups.Add("ParentBandGroupTHUE_SUAT_STRING", "");
            //parentBandGroupTHUE_SUAT_STRING.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            //parentBandGroupTHUE_SUAT_STRING.Header.Appearance.FontData.SizeInPoints = 2;
            //parentBandGroupTHUE_SUAT_STRING.Header.Appearance.TextHAlign = HAlign.Center;
            //parentBandGroupTHUE_SUAT_STRING.Header.Appearance.BorderAlpha = Alpha.Transparent;
            ////parentBandGroupTHUE_SUAT_STRING.Header.Appearance.BorderColor = Color.Black;
            //parentBandGroupTHUE_SUAT_STRING.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            //parentBandGroupTHUE_SUAT_STRING.RowLayoutGroupInfo.SpanY = 2;


            //UltraGridGroup parentBandGroupTHUE_GTGT;
            //if (parentBand.Groups.Exists("ParentBandGroupTHUE_GTGT"))
            //    parentBandGroupTHUE_GTGT = parentBand.Groups["ParentBandGroupTHUE_GTGT"];
            //else
            //    parentBandGroupTHUE_GTGT = parentBand.Groups.Add("ParentBandGroupTHUE_GTGT", "");
            //parentBandGroupTHUE_GTGT.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            //parentBandGroupTHUE_GTGT.Header.Appearance.FontData.SizeInPoints = 2;
            //parentBandGroupTHUE_GTGT.Header.Appearance.TextHAlign = HAlign.Center;
            //parentBandGroupTHUE_GTGT.Header.Appearance.BorderAlpha = Alpha.Transparent;
            ////parentBandGroupTHUE_GTGT.Header.Appearance.BorderColor = Color.Black;
            //parentBandGroupTHUE_GTGT.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            //parentBandGroupTHUE_GTGT.RowLayoutGroupInfo.SpanY = 2;

            //UltraGridGroup parentBandGroupTK_THUE;
            //if (parentBand.Groups.Exists("ParentBandGroupTK_THUE"))
            //    parentBandGroupTK_THUE = parentBand.Groups["ParentBandGroupTK_THUE"];
            //else
            //    parentBandGroupTK_THUE = parentBand.Groups.Add("ParentBandGroupTK_THUE", "");
            //parentBandGroupTK_THUE.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            //parentBandGroupTK_THUE.Header.Appearance.FontData.SizeInPoints = 2;
            //parentBandGroupTK_THUE.Header.Appearance.TextHAlign = HAlign.Center;
            //parentBandGroupTK_THUE.Header.Appearance.BorderAlpha = Alpha.Transparent;
            ////parentBandGroupTK_THUE.Header.Appearance.BorderColor = Color.Black;
            //parentBandGroupTK_THUE.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            //parentBandGroupTK_THUE.RowLayoutGroupInfo.SpanY = 2;

            ////UltraGridGroup parentBandGroupTEN_NBAN;
            ////if (parentBand.Groups.Exists("ParentBandGroupTEN_NBAN"))
            ////    parentBandGroupTEN_NBAN = parentBand.Groups["ParentBandGroupTEN_NBAN"];
            ////else
            ////    parentBandGroupTEN_NBAN = parentBand.Groups.Add("ParentBandGroupTEN_NBAN", "Số hóa đơn");
            ////parentBandGroupTEN_NBAN.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            ////parentBandGroupTEN_NBAN.Header.Appearance.FontData.SizeInPoints = 12;
            ////parentBandGroupTEN_NBAN.Header.Appearance.TextHAlign = HAlign.Center;
            ////parentBandGroupTEN_NBAN.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            ////parentBandGroupTEN_NBAN.Header.Appearance.BorderColor = Color.Black;
            ////parentBandGroupTEN_NBAN.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;

            //parentBand.Columns["GOODSSERVICEPURCHASENAME"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            //parentBand.Columns["GOODSSERVICEPURCHASENAME"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            //parentBand.Columns["GOODSSERVICEPURCHASENAME"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            //parentBand.Columns["GOODSSERVICEPURCHASENAME"].Header.Appearance.ForeColor = Color.Black;
            //parentBand.Columns["GOODSSERVICEPURCHASENAME"].Header.Appearance.BorderAlpha = Alpha.Transparent;
            //parentBand.Columns["GOODSSERVICEPURCHASENAME"].Header.Appearance.BorderColor = Color.Black;
            ////parentBand.Columns["GOODSSERVICEPURCHASENAME"].Header.


            parentBand.Columns["SO_HD"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["SO_HD"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SO_HD"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["SO_HD"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["SO_HD"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SO_HD"].Header.Appearance.BorderColor = Color.Black;
            //parentBand.Columns["SO_HD"].Header.Appearance.BorderColor2 = Color.Red;
            //parentBand.Columns["SO_HD"].Header.Appearance.BorderColor3DBase = Color.Orange;
            //parentBand.Columns["SO_HD"].
            parentBand.Columns["SO_HD"].RowLayoutColumnInfo.OriginX = 0;
            parentBand.Columns["SO_HD"].RowLayoutColumnInfo.OriginY = 0;
            //parentBand.Columns["SO_HD"].RowLayoutColumnInfo.SpanX = parentBand.Columns["TK_THUE"].RowLayoutColumnInfo.SpanX + parentBand.Columns["THUE_GTGT"].RowLayoutColumnInfo.SpanX + parentBand.Columns["THUE_SUAT_STRING"].RowLayoutColumnInfo.SpanX + parentBand.Columns["GT_CHUATHUE"].RowLayoutColumnInfo.SpanX + parentBand.Columns["MAT_HANG"].RowLayoutColumnInfo.SpanX + parentBand.Columns["MST"].RowLayoutColumnInfo.SpanX + parentBand.Columns["NGAY_HD"].RowLayoutColumnInfo.SpanX + parentBand.Columns["SO_HD"].RowLayoutColumnInfo.SpanX;
            //parentBand.Columns["SO_HD"].RowLayoutColumnInfo.SpanY = parentBand.Columns["TK_THUE"].RowLayoutColumnInfo.SpanY;
            parentBand.Columns["SO_HD"].Width = 170;

            parentBand.Columns["NGAY_HD"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["NGAY_HD"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["NGAY_HD"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["NGAY_HD"].Header.Appearance.BackColor2 = Color.Black;
           parentBand.Columns["NGAY_HD"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["NGAY_HD"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["NGAY_HD"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["TEN_NBAN"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["TEN_NBAN"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["TEN_NBAN"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["TEN_NBAN"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["TEN_NBAN"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["TEN_NBAN"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["MST"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["MST"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["MST"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["MST"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["MST"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["MST"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["MAT_HANG"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["MAT_HANG"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["MAT_HANG"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["MAT_HANG"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["MAT_HANG"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["MAT_HANG"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["GT_CHUATHUE"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["GT_CHUATHUE"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GT_CHUATHUE"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["GT_CHUATHUE"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["GT_CHUATHUE"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GT_CHUATHUE"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["THUE_SUAT_STRING"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["THUE_SUAT_STRING"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["THUE_SUAT_STRING"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["THUE_SUAT_STRING"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["THUE_SUAT_STRING"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["THUE_SUAT_STRING"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["THUE_GTGT"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["THUE_GTGT"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["THUE_GTGT"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["THUE_GTGT"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["THUE_GTGT"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["THUE_GTGT"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["TK_THUE"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["TK_THUE"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["TK_THUE"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["TK_THUE"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["TK_THUE"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["TK_THUE"].Header.Appearance.BorderColor = Color.Black;


            //parentBand.Columns["GOODSSERVICEPURCHASENAME"].Header.Appearance.BackColorAlpha = Alpha.Transparent;

            // parentBand.Columns["GOODSSERVICEPURCHASENAME"].

            //parentBand.Columns["GOODSSERVICEPURCHASENAME"].Header.VisiblePosition = 1;
            //parentBand.Columns["SO_HD"].Header.VisiblePosition = 0;
            //parentBand.Columns["NGAY_HD"].Header.VisiblePosition = 2;

            //parentBand.Columns["TEN_NBAN"].Header.VisiblePosition = 3;
            //parentBand.Columns["MST"].Header.VisiblePosition = 4;
            //parentBand.Columns["MAT_HANG"].Header.VisiblePosition = 5;
            //parentBand.Columns["GT_CHUATHUE"].Header.VisiblePosition = 6;
            //parentBand.Columns["THUE_SUAT_STRING"].Header.VisiblePosition = 7;
            //parentBand.Columns["THUE_GTGT"].Header.VisiblePosition = 8;
            //parentBand.Columns["TK_THUE"].Header.VisiblePosition = 9;

            //parentBand.Columns["GOODSSERVICEPURCHASENAME"].RowLayoutColumnInfo.OriginX = 0;
            //parentBand.Columns["GOODSSERVICEPURCHASENAME"].RowLayoutColumnInfo.OriginY = 0;
            //parentBand.Columns["GOODSSERVICEPURCHASENAME"].RowLayoutColumnInfo.SpanX = parentBand.Columns["MAT_HANG"].RowLayoutColumnInfo.SpanX + parentBand.Columns["MST"].RowLayoutColumnInfo.SpanX + parentBand.Columns["NGAY_HD"].RowLayoutColumnInfo.SpanX + parentBand.Columns["SO_HD"].RowLayoutColumnInfo.SpanX + parentBand.Columns["TEN_NBAN"].RowLayoutColumnInfo.SpanX;
            //parentBand.Columns["GOODSSERVICEPURCHASENAME"].RowLayoutColumnInfo.SpanY = 2;//parentBand.Columns["TK_THUE"].RowLayoutColumnInfo.SpanY;
            //parentBand.Columns["GOODSSERVICEPURCHASENAME"].Header.Appearance.FontData.SizeInPoints = 2;


            parentBand.Columns["Ngay_HD"].Width = 100;
            if (Utils.isDemo)
            {
                ultraButton2.Visible = false;
            }
            WaitingFrm.StopWaiting();
        }
        private void SetDoSoAm(UltraGrid ultraGrid)
        {
            foreach (var item in ultraGrid.Rows)
            {
                if ((int)item.Cells["Ordercode"].Value == -1)
                {
                    
                    item.Cells["SO_HD"].Appearance.FontData.Bold = DefaultableBoolean.True;

                    
    
                }
                else
                {
                    if ((int)item.Cells["Ordercode"].Value == 0)
                    {
                       
                    }
                    if ((int)item.Cells["Ordercode"].Value == 1)
                    {
                       
                        item.Cells["SO_HD"].Appearance.FontData.Bold = DefaultableBoolean.True;
                        item.Cells["THUE_GTGT"].Appearance.FontData.Bold = DefaultableBoolean.True;
                        item.Cells["GT_CHUATHUE"].Appearance.FontData.Bold = DefaultableBoolean.True;
                       
                    }
                }
                if ((decimal)item.Cells["GT_CHUATHUE"].Value < 0)
                    item.Cells["GT_CHUATHUE"].Appearance.ForeColor = Color.Red;


                if ((decimal)item.Cells["THUE_GTGT"].Value < 0)
                    item.Cells["THUE_GTGT"].Appearance.ForeColor = Color.Red;

            }
        }

        private void ultraButton2_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog
            {
                Title = "Chọn nơi sao lưu ",
                Filter = "File (*.xls)|*.xls",
                InitialDirectory = "@C:\\",
                FileName = "BaoCaoBangKeMuaVao.xls"
            };
            if (save.ShowDialog() == DialogResult.OK)
            {
                string duongdan = save.FileName;
                try
                {
                    ultraGridExcelExporter1.Export(uGridDuLieu, duongdan);
                    if (MSG.Question("Kết xuất thành công, Bạn có muốn mở file vừa kết xuất không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(save.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
                catch
                {
                    MSG.Warning("Lỗi khi kết xuất !");
                }
            }
        }

        private void ultraGridExcelExporter1_InitializeColumn(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.InitializeColumnEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Column.Format))
            {
                e.ExcelFormatStr = e.Column.Format;

            }
            //if (e.Column.Key == "GOODSSERVICEPURCHASENAME")
            //{
            //    e.Column.Header.Appearance.BackColorAlpha = Alpha.Transparent;
            //}
        }

        private void ultraButton3_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ultraButton1_Click(object sender, EventArgs e)
        {
            //if (dtBeginDate.Value == null || dtEndDate.Value == null)
            //{
            //    MSG.Warning("Ngày không được để trống");
            //    return;
            //}

            //if ((DateTime)dtBeginDate.Value > (DateTime)dtEndDate.Value)
            //{
            //    MSG.Warning("Từ ngày không được lớn hơn đến ngày");
            //    return;
            //}

            //ReportProcedureSDS sp = new ReportProcedureSDS();
            //List<BangKeMuaVao_Model> data = new List<BangKeMuaVao_Model>();
            //int check = cbb_CongGop.Checked == true ? 1 : 0;
            //data = sp.GetProc_GetBkeInv_Buy((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, check, 0, "");
            //data.ForEach(t =>
            //{
            //    t.THUE_SUAT_DETAIL = Convert.ToInt32(t.THUE_SUAT);
            //    if (t.FLAG == -1)
            //    {
            //        t.GT_CHUATHUE = -t.GT_CHUATHUE;
            //        t.THUE_GTGT = -t.THUE_GTGT;
            //    }
            //});
            //if (data.Count == 0)
            //{
            //    MSG.Warning("Không có dữ liệu để lên báo cáo!");
            //}
            //else
            ReportProcedureSDS sp1 = new ReportProcedureSDS();
            

            data1 = sp1.GetProc_GetBkeInv_Buy(begin1, end1, check1, 0, "");
            data1.ForEach(t =>
            {
                t.THUE_SUAT_DETAIL = Convert.ToInt32(t.THUE_SUAT);
                if (t.FLAG == -1)
                {
                    t.GT_CHUATHUE = -t.GT_CHUATHUE;
                    t.THUE_GTGT = -t.THUE_GTGT;
                }
            });
            List<BangKeMuaVao_Model> listGroup = (from a in data1
                                                  group a by a.GOODSSERVICEPURCHASECODE into g
                                                  select new BangKeMuaVao_Model
                                                  {
                                                      GOODSSERVICEPURCHASECODE = g.Key,
                                                      GT_CHUATHUE = g.Sum(t => t.GT_CHUATHUE),
                                                      THUE_GTGT = g.Sum(t => t.THUE_GTGT)
                                                  }).ToList();
            var rD = new BangKeMuaVao_ModelDetail();
            rD.Period = tieude1;
            data1 = data1.OrderBy(n => n.GOODSSERVICEPURCHASECODE).ThenBy(n => n.Ordercode).ThenBy(n => n.NGAY_HD).ThenBy(n => n.SO_HD).ToList();
            var f = new ReportForm<BangKeMuaVao_Model>(fileReportSlot1, _subSystemCode1);
            f.AddDatasource("DBBANGKEMUAVAO", data1);// set định dạng ngày đd/mm/yyyy
            f.AddDatasource("DBGROUPCODE", listGroup);
            f.AddDatasource("Detail", rD);
            f.LoadReport();
            f.WindowState = FormWindowState.Maximized;
            f.Show();

        }

        private void uGridDuLieu_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (check1 == 0)
            {
                if (e.Row.ListObject != null)
                {
                    if (e.Row.ListObject.HasProperty("TypeID"))
                    {
                        if (!e.Row.ListObject.GetProperty("TypeID").IsNullOrEmpty())
                        {
                            int typeid = e.Row.ListObject.GetProperty("TypeID").ToInt();

                            if (e.Row.ListObject.HasProperty("RefID"))
                            {
                                if (!e.Row.ListObject.GetProperty("RefID").IsNullOrEmpty())
                                {
                                    Guid id = (Guid)e.Row.ListObject.GetProperty("RefID");
                                    var f = Utils.ViewVoucherSelected1(id, typeid);
                                    //this.Close();
                                    //if (f.IsDisposed)
                                    //{
                                    //    new FRBaoCaoBangKeMuavaoTruocIn(data1, tieude1, _subSystemCode1, check1, begin1, end1).Show();
                                    //}
                                }
                            }
                        }

                    }
                }
            }
            else
            {
                List<BangKeMuaVao_Model> list = new List<BangKeMuaVao_Model>();
                ReportProcedureSDS sp = new ReportProcedureSDS();
                list = sp.GetProc_GetBkeInv_Buy(begin1, end1, 0, 0, "");
                list.ForEach(t =>
                {
                    t.THUE_SUAT_DETAIL = Convert.ToInt32(t.THUE_SUAT);
                    if (t.FLAG == -1)
                    {
                        t.GT_CHUATHUE = -t.GT_CHUATHUE;
                        t.THUE_GTGT = -t.THUE_GTGT;
                    }
                });
                string GoodsServicePurchaseCode=null;
                string GoodsServicePurchaseName=null;
                string sohd=null;
                DateTime? NgayHD=null; string tenNBan=null; string mst=null; string mathang=null; string tkthue=null; int? flag=null; decimal? thuesuat=null;
                if (e.Row.ListObject.HasProperty("GOODSSERVICEPURCHASECODE"))
                    {
                    
                    if (e.Row.ListObject.GetProperty("GOODSSERVICEPURCHASECODE") == null)
                    {
                        GoodsServicePurchaseCode = null;
                    }
                    else
                    {
                        GoodsServicePurchaseCode = e.Row.ListObject.GetProperty("GOODSSERVICEPURCHASECODE").ToString();
                    }
                }
                if (e.Row.ListObject.HasProperty("GOODSSERVICEPURCHASENAME"))
                    {
                    
                    if (e.Row.ListObject.GetProperty("GOODSSERVICEPURCHASENAME") == null)
                    {
                        GoodsServicePurchaseName = null;
                    }
                    else
                    {
                        GoodsServicePurchaseName = e.Row.ListObject.GetProperty("GOODSSERVICEPURCHASENAME").ToString();
                    }
                }
                if (e.Row.ListObject.HasProperty("SO_HD"))
                {
                    
                    if (e.Row.ListObject.GetProperty("SO_HD") == null)
                    {
                        sohd = null;
                    }
                    else
                    {
                        sohd = e.Row.ListObject.GetProperty("SO_HD").ToString();
                    }
                }
                if (e.Row.ListObject.HasProperty("NGAY_HD"))
                      { 
                    if (e.Row.ListObject.GetProperty("NGAY_HD") == null)
                    {
                        NgayHD = null;
                    }
                    else
                    {
                        NgayHD = (DateTime)e.Row.ListObject.GetProperty("NGAY_HD");
                    }

                }

                if (e.Row.ListObject.HasProperty("TEN_NBAN"))
                      {
                   
                    if (e.Row.ListObject.GetProperty("TEN_NBAN") == null)
                    {
                        tenNBan = null;
                    }
                    else
                    {
                        tenNBan = e.Row.ListObject.GetProperty("TEN_NBAN").ToString();
                    }

                }
                if (e.Row.ListObject.HasProperty("MST"))
                {
                   
                    if (e.Row.ListObject.GetProperty("MST") == null)
                    {
                        mst = null;
                    }
                    else
                    {
                        mst = e.Row.ListObject.GetProperty("MST").ToString();
                    }

                }
                if (e.Row.ListObject.HasProperty("MAT_HANG"))
                {
                    
                    if (e.Row.ListObject.GetProperty("MAT_HANG") == null)
                    {
                        mathang = null;
                    }
                    else
                    {
                        mathang = e.Row.ListObject.GetProperty("MAT_HANG").ToString();
                    }

                }
                if (e.Row.ListObject.HasProperty("TK_THUE"))
                {
                   
                    if (e.Row.ListObject.GetProperty("TK_THUE") == null)
                    {
                        tkthue = null;
                    }
                    else
                    {
                        tkthue = e.Row.ListObject.GetProperty("TK_THUE").ToString();
                    }

                }
                if (e.Row.ListObject.HasProperty("FLAG"))
                {
                    
                    if (e.Row.ListObject.GetProperty("FLAG") == null)
                    {
                        flag = null;
                    }
                    else
                    {
                        flag = (int)e.Row.ListObject.GetProperty("FLAG");
                    }

                }
                if (e.Row.ListObject.HasProperty("THUE_SUAT"))
                {
                    
                    if (e.Row.ListObject.GetProperty("THUE_SUAT") == null)
                    {
                        thuesuat = null;
                    }
                    else
                    {
                        thuesuat = (decimal)e.Row.ListObject.GetProperty("THUE_SUAT");
                    }

                }
                List<BangKeMuaVao_Model> list1 = new List<BangKeMuaVao_Model>();
                list1.ForEach(t =>
                {
                    t.THUE_SUAT_DETAIL = Convert.ToInt32(t.THUE_SUAT);
                    if (t.FLAG == -1)
                    {
                        t.GT_CHUATHUE = -t.GT_CHUATHUE;
                        t.THUE_GTGT = -t.THUE_GTGT;
                    }
                });
                list1 = list.Where(n => n.THUE_SUAT == thuesuat && n.FLAG == flag && n.TK_THUE == tkthue  && n.MST == mst && n.TEN_NBAN == tenNBan && n.NGAY_HD == NgayHD && n.SO_HD == sohd && n.GOODSSERVICEPURCHASENAME == GoodsServicePurchaseName && n.GOODSSERVICEPURCHASECODE == GoodsServicePurchaseCode).ToList();
                if(list1.Count==0)
                {
                    MSG.Information("Không có chứng từ");
                }
                else
                {
                    if(list1.Count==1)
                    {
                        
                        
                            if (!list1[0].TypeID.IsNullOrEmpty())
                            {
                                int typeid = list1[0].TypeID;

                               
                                
                                    if (!list1[0].RefID.IsNullOrEmpty())
                                    {
                                string refid = list1[0].RefID.ToString();
                                Guid id = Guid.Parse(refid);
                                        var f = Utils.ViewVoucherSelected1(id, typeid);
                                //        this.Close();
                                ////this.Close();
                                //if (f.IsDisposed)
                                //{
                                //    new FRBaoCaoBangKeMuavaoTruocIn(data1, tieude1, _subSystemCode1, check1, begin1, end1).Show();
                                //}
                            }
                                
                            }

                        
                    }
                    else
                    {
                        var f = new DanhSachChungTu(list1);
                        f.Show();
                    }
                }


            }
        }

        private void uGridDuLieu_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            UltraGridCell cell = e.Row.Cells[0];
            cell.Row.Height = 25;
        }

        private void ultraGridExcelExporter1_SummaryCellExported(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.SummaryCellExportedEventArgs e)
        {
            Worksheet ws = e.CurrentWorksheet;
            WorksheetRow wsRow = ws.Rows[e.CurrentRowIndex];
            WorksheetCell wsCell = wsRow.Cells[e.CurrentColumnIndex];
            if (!e.Summary.SummaryText.Contains("Số dòng") && !e.Summary.SummaryText.Contains(" "))
            {
                string s = e.Summary.SummaryText.Trim('.');
                if (s.Contains("("))
                    wsCell.Value = -decimal.Parse(s.Trim('(').Trim(')'));
                else
                    wsCell.Value = decimal.Parse(s);

                wsCell.CellFormat.FormatString = "###,###,###,###,##0;(###,###,###,###,##0)";
            }
        }

        private void ultraGridExcelExporter1_BeginExport(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.BeginExportEventArgs e)
        {
            foreach (var row in uGridDuLieu.Rows)
            {
                foreach (var column in uGridDuLieu.DisplayLayout.Bands[0].Columns)
                {
                    e.Rows[row.Index].Cells[column].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                    e.Rows[row.Index].Cells[column].Appearance.BorderColor = Color.Black;
                    e.Rows[row.Index].Cells[column].Column.Width = 200;
                }
            }
            e.Rows.Band.Summaries[0].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[0].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[1].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[1].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[2].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[2].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[3].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[3].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[4].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[4].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[5].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[5].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[6].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[6].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[7].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[7].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[8].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[8].Appearance.BorderColor = Color.Black;
        }
    } }
