﻿using Accounting.Core.Domain.obj.Report;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class DanhSachChungTuBanRa : Form
    {
        public DanhSachChungTuBanRa(List<BangKeBanRa_Model> data)
        {
            InitializeComponent();
            ultraGrid1.DataSource = data;
            Utils.ConfigGrid(ultraGrid1, ConstDatabase.BangKeMuaVao_Model_TableName1);
            ultraGrid1.DisplayLayout.Bands[0].Columns["THUE_GTGT"].FormatNumberic(ConstDatabase.Format_TienVND);
            ultraGrid1.DisplayLayout.Bands[0].Columns["DOANH_SO"].FormatNumberic(ConstDatabase.Format_TienVND);
            SetDoSoAm(ultraGrid1);
            Utils.ConfigGrid(ultraGrid1, ConstDatabase.BangKeBanRa_Model_TableName1);
        }

        private void ultraGrid1_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            if (e.Row.ListObject != null)
            {
                if (e.Row.ListObject.HasProperty("RefType"))
                {
                    if (!e.Row.ListObject.GetProperty("RefType").IsNullOrEmpty())
                    {
                        int typeid = e.Row.ListObject.GetProperty("RefType").ToInt();

                        if (e.Row.ListObject.HasProperty("RefID"))
                        {
                            if (!e.Row.ListObject.GetProperty("RefID").IsNullOrEmpty())
                            {
                                Guid id = (Guid)e.Row.ListObject.GetProperty("RefID");
                                var f = Utils.ViewVoucherSelected1(id, typeid);
                                //this.Close();
                                //if (f.IsDisposed)
                                //{
                                //    new FRBaoCaoBangKeMuavaoTruocIn(data1, tieude1, _subSystemCode1, check1).Show();
                                //}
                            }
                        }
                    }

                }
            }
        }
        private void SetDoSoAm(UltraGrid ultraGrid)
        {
            foreach (var item in ultraGrid.Rows)
            {

                if ((decimal)item.Cells["DOANH_SO"].Value < 0)
                    item.Cells["DOANH_SO"].Appearance.ForeColor = Color.Red;


                if ((decimal)item.Cells["THUE_GTGT"].Value < 0)
                    item.Cells["THUE_GTGT"].Appearance.ForeColor = Color.Red;

            }
        }
    }
}
