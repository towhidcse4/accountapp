﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;

namespace Accounting
{
    public partial class FRS03aDNN : CustormForm
    {
        string _subSystemCode;
        public FRS03aDNN(string subSystemCode)
        {
            InitializeComponent();
            _subSystemCode = subSystemCode;
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\S03a-DNN.rst", path);
        }

        private void ultraButton1_Click(object sender, EventArgs e)
        {
            List<S03ADNN> data = new List<S03ADNN>();
            for (int i = 0; i < 200; i++)
            {
                data.Add(new S03ADNN(i));
            }
            var rD = new S03ADNNDetail();
            rD.Period = "Kỳ báo cáo";
            var f = new ReportForm<S03ADNN>(fileReportSlot1, _subSystemCode);
            f.AddDatasource("S03aDNN", data, true);
            f.AddDatasource("Detail", rD);
            f.LoadReport();
            f.WindowState = FormWindowState.Maximized;
            f.Show();
        }
    }
}
