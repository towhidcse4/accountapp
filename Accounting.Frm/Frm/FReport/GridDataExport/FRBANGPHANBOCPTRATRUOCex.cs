﻿using Accounting.Core.DAO;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.Report;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Documents.Excel;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRBANGPHANBOCPTRATRUOCex : Form
    {
        private string _subSystemCode;
        ReportProcedureSDS sp = new ReportProcedureSDS();
        List<PrepaidExpenseAllocation_Report> data = new List<PrepaidExpenseAllocation_Report>();
        DateTime FromDate;
        DateTime ToDate;
        string period = "";
        public IPrepaidExpenseService IPrepaidExpenseService { get { return IoC.Resolve<IPrepaidExpenseService>(); } }
        public FRBANGPHANBOCPTRATRUOCex(DateTime FromDate1, DateTime ToDate1)
        {
            InitializeComponent();
            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\BangPhanBoChiPhiTraTruoc.rst", path);
            ReportUtils.ProcessControls(this);

            List<PrepaidExpenseAllocation_Report> data1 = IPrepaidExpenseService.GetReport_ByDate(FromDate1, ToDate1);
            period = ReportUtils.GetPeriod(FromDate1, ToDate1);

            FromDate = FromDate1;
            ToDate = ToDate1;
            data = data1;

            configGrid(data1);

            if (Utils.isDemo)
                btnExport.Visible = false;
            else
                btnExport.Visible = true;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (data.Count == 0)
                MSG.Warning("Không có dữ liệu");
            else
            {
                var rD = new PrepaidExpenseAllocation_Report_DNDetail();
                rD.Period = ReportUtils.GetPeriod(FromDate, ToDate);
                var f = new ReportForm<PrepaidExpenseAllocation_Report>(fileReportSlot1, _subSystemCode);
                f.AddDatasource("DBPBCPTT", data, true);
                f.AddDatasource("Detail", rD);
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog
            {
                Title = "Chọn nơi sao lưu ",
                Filter = "File (*.xls)|*.xls",
                InitialDirectory = "@C:\\",
                FileName = "bangphanbochiphitratruoc.xls"
            };
            if (save.ShowDialog() == DialogResult.OK)
            {
                string duongdan = save.FileName;
                try
                {
                    ultraGridExcelExporter1.Export(uGrid, duongdan);
                    if (MSG.Question("Kết xuất thành công, Bạn có muốn mở file vừa kết xuất không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(save.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
                catch
                {
                    MSG.Warning("Lỗi khi kết xuất !");
                }
            }
        }     

        private void configGrid(List<PrepaidExpenseAllocation_Report> lst)
        {
            uGrid.DataSource = lst;
            Utils.ConfigGrid(uGrid, ConstDatabase.PrepaidExpenseAllocation_Report_TableName);
            uGrid.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;

            Utils.AddSumColumn(uGrid, "TenCP", false);
            Utils.AddSumColumn(uGrid, "NgayGhiNhan", false);
            Utils.AddSumColumn(uGrid, "SoKyPB", false);
            Utils.AddSumColumn(uGrid, "SoKyDaPB", false);
            Utils.AddSumColumn(uGrid, "SoKyPBConLai", false);

            uGrid.DisplayLayout.Bands[0].Summaries[1].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[2].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[3].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[4].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[5].DisplayFormat = " ";

            uGrid.DisplayLayout.Bands[0].Columns["MaCP"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["MaCP"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["MaCP"].Width = 150;
            uGrid.DisplayLayout.Bands[0].Columns["MaCP"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["TenCP"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["TenCP"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["TenCP"].CellAppearance.TextVAlign = VAlign.Middle;
            uGrid.DisplayLayout.Bands[0].Columns["TenCP"].Width = 250;

            uGrid.DisplayLayout.Bands[0].Columns["NgayGhiNhan"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["NgayGhiNhan"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["NgayGhiNhan"].CellAppearance.TextVAlign = VAlign.Middle;
            uGrid.DisplayLayout.Bands[0].Columns["NgayGhiNhan"].CellAppearance.TextHAlign = HAlign.Center;
            uGrid.DisplayLayout.Bands[0].Columns["NgayGhiNhan"].Width = 120;

            uGrid.DisplayLayout.Bands[0].Columns["SoTien"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["SoTien"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["SoTien"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["SoTien"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["SoTien"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["SoKyPB"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["SoKyPB"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["SoKyPB"].Width = 100;
            uGrid.DisplayLayout.Bands[0].Columns["SoKyPB"].CellAppearance.TextVAlign = VAlign.Middle;
            uGrid.DisplayLayout.Bands[0].Columns["SoKyPB"].CellAppearance.TextHAlign = HAlign.Right;

            uGrid.DisplayLayout.Bands[0].Columns["SoKyDaPB"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["SoKyDaPB"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["SoKyDaPB"].Width = 100;
            uGrid.DisplayLayout.Bands[0].Columns["SoKyDaPB"].CellAppearance.TextVAlign = VAlign.Middle;
            uGrid.DisplayLayout.Bands[0].Columns["SoKyDaPB"].CellAppearance.TextHAlign = HAlign.Right;

            uGrid.DisplayLayout.Bands[0].Columns["SoKyPBConLai"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["SoKyPBConLai"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["SoKyPBConLai"].Width = 100;
            uGrid.DisplayLayout.Bands[0].Columns["SoKyPBConLai"].CellAppearance.TextVAlign = VAlign.Middle;
            uGrid.DisplayLayout.Bands[0].Columns["SoKyPBConLai"].CellAppearance.TextHAlign = HAlign.Right;

            uGrid.DisplayLayout.Bands[0].Columns["LuyKeDaPB"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["LuyKeDaPB"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["LuyKeDaPB"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["LuyKeDaPB"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["LuyKeDaPB"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["SoTienConLai"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["SoTienConLai"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["SoTienConLai"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["SoTienConLai"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["SoTienConLai"].CellAppearance.TextVAlign = VAlign.Middle;

            Utils.AddSumColumn1(lst.Where(n => n.SoTien != null).Sum(n => n.SoTien), uGrid, "SoTien", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.LuyKeDaPB != null).Sum(n => n.LuyKeDaPB), uGrid, "LuyKeDaPB", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.SoTienConLai != null).Sum(n => n.SoTienConLai), uGrid, "SoTienConLai", false, constDatabaseFormat: 1);
            GridLayout();
        }
        private void GridLayout()
        {
            UltraGridBand parentBand = this.uGrid.DisplayLayout.Bands[0];
            parentBand.RowLayoutStyle = RowLayoutStyle.GroupLayout;

            UltraGridGroup parentBandGroupPeriod;
            if (parentBand.Groups.Exists("ParentBandGroupPeriod"))
            {
                parentBandGroupPeriod = parentBand.Groups["ParentBandGroupPeriod"];
                parentBandGroupPeriod.Header.Caption = period;
            }
            else
                parentBandGroupPeriod = parentBand.Groups.Add("ParentBandGroupPeriod", period);
            parentBandGroupPeriod.Header.Appearance.FontData.Italic = DefaultableBoolean.True;
            parentBandGroupPeriod.Header.Appearance.FontData.SizeInPoints = 10;
            parentBandGroupPeriod.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupPeriod.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPeriod.Header.Appearance.BorderColor = Color.Black;

            UltraGridGroup parentBandGroupTitle;
            if (parentBand.Groups.Exists("ParentBandGroupTitle"))
                parentBandGroupTitle = parentBand.Groups["ParentBandGroupTitle"];
            else
                parentBandGroupTitle = parentBand.Groups.Add("ParentBandGroupTitle", "BẢNG PHÂN BỔ CHI PHÍ TRẢ TRƯỚC");
            parentBandGroupTitle.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroupTitle.Header.Appearance.FontData.SizeInPoints = 12;
            parentBandGroupTitle.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupTitle.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupTitle.Header.Appearance.BorderColor = Color.Black;

            parentBandGroupPeriod.RowLayoutGroupInfo.ParentGroup = parentBandGroupTitle;

            parentBand.Columns["MaCP"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["MaCP"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["MaCP"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["MaCP"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["MaCP"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["MaCP"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["TenCP"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["TenCP"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["TenCP"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["TenCP"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["TenCP"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["TenCP"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["NgayGhiNhan"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["NgayGhiNhan"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["NgayGhiNhan"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["NgayGhiNhan"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["NgayGhiNhan"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["NgayGhiNhan"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["SoTien"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["SoTien"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SoTien"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["SoTien"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["SoTien"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SoTien"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["SoKyPB"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["SoKyPB"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SoKyPB"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["SoKyPB"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["SoKyPB"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SoKyPB"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["SoKyDaPB"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["SoKyDaPB"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SoKyDaPB"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["SoKyDaPB"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["SoKyDaPB"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SoKyDaPB"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["SoKyPBConLai"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["SoKyPBConLai"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SoKyPBConLai"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["SoKyPBConLai"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["SoKyPBConLai"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SoKyPBConLai"].Header.Appearance.BorderColor = Color.Black;           

            parentBand.Columns["LuyKeDaPB"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["LuyKeDaPB"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["LuyKeDaPB"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["LuyKeDaPB"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["LuyKeDaPB"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["LuyKeDaPB"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["SoTienConLai"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["SoTienConLai"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SoTienConLai"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["SoTienConLai"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["SoTienConLai"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SoTienConLai"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["MaCP"].RowLayoutColumnInfo.OriginX = 0;
            parentBand.Columns["MaCP"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["MaCP"].RowLayoutColumnInfo.SpanX = 150;
            parentBand.Columns["MaCP"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["TenCP"].RowLayoutColumnInfo.OriginX = 150;
            parentBand.Columns["TenCP"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["TenCP"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["TenCP"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["NgayGhiNhan"].RowLayoutColumnInfo.OriginX = 400;
            parentBand.Columns["NgayGhiNhan"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["NgayGhiNhan"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["NgayGhiNhan"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["SoTien"].RowLayoutColumnInfo.OriginX = 520;
            parentBand.Columns["SoTien"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["SoTien"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["SoTien"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["SoKyPB"].RowLayoutColumnInfo.OriginX = 770;
            parentBand.Columns["SoKyPB"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["SoKyPB"].RowLayoutColumnInfo.SpanX = 100;
            parentBand.Columns["SoKyPB"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["SoKyDaPB"].RowLayoutColumnInfo.OriginX = 870;
            parentBand.Columns["SoKyDaPB"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["SoKyDaPB"].RowLayoutColumnInfo.SpanX = 100;
            parentBand.Columns["SoKyDaPB"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["SoKyPBConLai"].RowLayoutColumnInfo.OriginX = 970;
            parentBand.Columns["SoKyPBConLai"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["SoKyPBConLai"].RowLayoutColumnInfo.SpanX = 100;
            parentBand.Columns["SoKyPBConLai"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["LuyKeDaPB"].RowLayoutColumnInfo.OriginX = 1070;
            parentBand.Columns["LuyKeDaPB"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["LuyKeDaPB"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["LuyKeDaPB"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["SoTienConLai"].RowLayoutColumnInfo.OriginX = 1320;
            parentBand.Columns["SoTienConLai"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["SoTienConLai"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["SoTienConLai"].RowLayoutColumnInfo.SpanY = 32;
        }

        private void uGrid_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            UltraGridCell cell = e.Row.Cells[0];
            cell.Row.Height = 25;
           
            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("SoTien"))
            {
                if (cell.Row.Cells["SoTien"].Value != null && (decimal)cell.Row.Cells["SoTien"].Value == 0)
                    cell.Row.Cells["SoTien"].Value = null;
                if ((cell.Row.Cells["SoTien"].Value != null && (decimal)cell.Row.Cells["SoTien"].Value < 0))
                    cell.Row.Cells["SoTien"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("LuyKeDaPB"))
            {
                if (cell.Row.Cells["LuyKeDaPB"].Value != null && (decimal)cell.Row.Cells["LuyKeDaPB"].Value == 0)
                    cell.Row.Cells["LuyKeDaPB"].Value = null;
                if ((cell.Row.Cells["LuyKeDaPB"].Value != null && (decimal)cell.Row.Cells["LuyKeDaPB"].Value < 0))
                    cell.Row.Cells["LuyKeDaPB"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("SoTienConLai"))
            {
                if (cell.Row.Cells["SoTienConLai"].Value != null && (decimal)cell.Row.Cells["SoTienConLai"].Value == 0)
                    cell.Row.Cells["SoTienConLai"].Value = null;
                if ((cell.Row.Cells["SoTienConLai"].Value != null && (decimal)cell.Row.Cells["SoTienConLai"].Value < 0))
                    cell.Row.Cells["SoTienConLai"].Appearance.ForeColor = Color.Red;
            }

        }

        private void ultraGridExcelExporter1_InitializeColumn(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.InitializeColumnEventArgs e)
        {
            e.ExcelFormatStr = e.Column.Format;
        }

        private void ultraGridExcelExporter1_BeginExport(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.BeginExportEventArgs e)
        {
            foreach (var row in uGrid.Rows)
            {
                foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
                {
                    e.Rows[row.Index].Cells[column].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                    e.Rows[row.Index].Cells[column].Appearance.BorderColor = Color.Black;
                    e.Rows[row.Index].Cells[column].Column.Width = 200;
                }
            }
            e.Rows.Band.Summaries[0].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[0].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[1].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[1].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[2].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[2].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[3].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[3].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[4].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[4].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[5].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[5].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[6].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[6].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[7].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[7].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[8].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[8].Appearance.BorderColor = Color.Black;
        }

        private void FRB01aDNN_SizeChanged(object sender, EventArgs e)
        {
            var state = this.WindowState;
            switch (state)
            {
                case FormWindowState.Maximized:
                    break;
                case FormWindowState.Minimized:
                    break;
                case FormWindowState.Normal:
                    this.Left = (Screen.PrimaryScreen.Bounds.Width - this.Width) / 2;
                    this.Top = (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2;
                    this.Location = new Point((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2, (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
                    break;
                default:
                    break;
            }
        }

        private void ultraGridExcelExporter1_SummaryCellExported(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.SummaryCellExportedEventArgs e)
        {
            Worksheet ws = e.CurrentWorksheet;
            WorksheetRow wsRow = ws.Rows[e.CurrentRowIndex];
            WorksheetCell wsCell = wsRow.Cells[e.CurrentColumnIndex];
            if (!e.Summary.SummaryText.Contains("Số dòng") && !e.Summary.SummaryText.Contains(" "))
            {
                string s = e.Summary.SummaryText.Trim('.');
                if (s.Contains("("))
                    wsCell.Value = -decimal.Parse(s.Trim('(').Trim(')'));
                else
                    wsCell.Value = decimal.Parse(s);

                wsCell.CellFormat.FormatString = "###,###,###,###,##0;(###,###,###,###,##0)";
            }
        }
    }
}
