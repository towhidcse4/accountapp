﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.AppStyling;
using System.IO;
using FX.Core;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinMaskedEdit;
using Infragistics.Win.UltraWinTabs;
using Accounting;
using Accounting.Core.DAO;
using System.Diagnostics;
using Accounting.Core.Domain.obj.Report;
using Infragistics.Documents.Excel;

namespace Accounting.Frm.FReport
{
    public partial class FSOCHITIETMUAHANGex : CustormForm
    {
        private string _subSystemCode;
        private List<PO_GetDetailBook> data = new List<PO_GetDetailBook>();
        private string dsAccountID;
        private string dsVatTuID;
        private DateTime FromDate;
        private DateTime ToDate;
        private string period = "";
        int _soDVT = 1;
        ReportProcedureSDS sp = new ReportProcedureSDS();
        public FSOCHITIETMUAHANGex(DateTime FromDate1, DateTime ToDate1, string dsAccountID1, string dsVatTuID1)
        {
            InitializeComponent();
            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\SoChiTietMuaHang.rst", path);

            List<PO_GetDetailBook> data1 = sp.Get_SoChiTietMuaHang(FromDate1, ToDate1, dsAccountID1, dsVatTuID1);
            period = ReportUtils.GetPeriod(FromDate1, ToDate1);

            data = data1;
            dsAccountID = dsAccountID1;
            dsVatTuID = dsVatTuID1;
            FromDate = FromDate1;
            ToDate = ToDate1;

            configGrid(data);
            if (Utils.isDemo)
                btnExport.Visible = false;
            else
                btnExport.Visible = true;
        }

        private void configGrid(List<PO_GetDetailBook> lst)
        {
            uGrid.DataSource = lst;
            Utils.ConfigGrid(uGrid, ConstDatabase.PO_GetDetailBook_TableName); uGrid.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            uGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;

            var dtToCount = lst.Where(m => m.DVT != null).ToList();
            _soDVT = dtToCount.Where(m => m.DVT.ToString().Trim() != "").Select(m => m.DVT).Distinct().ToList().Count;



            if (_soDVT == 1)
            {
                Utils.AddSumColumn(uGrid, "NgayCTu", false);
                Utils.AddSumColumn(uGrid, "SoCTu", false);
                Utils.AddSumColumn(uGrid, "NgayHoaDon", false);
                Utils.AddSumColumn(uGrid, "SoHoaDon", false);
                Utils.AddSumColumn(uGrid, "Mahang", false);
                Utils.AddSumColumn(uGrid, "Tenhang", false);
                Utils.AddSumColumn(uGrid, "DVT", false);
                Utils.AddSumColumn(uGrid, "DonGia", false);

                uGrid.DisplayLayout.Bands[0].Summaries[1].DisplayFormat = " ";
                uGrid.DisplayLayout.Bands[0].Summaries[2].DisplayFormat = " ";
                uGrid.DisplayLayout.Bands[0].Summaries[3].DisplayFormat = " ";
                uGrid.DisplayLayout.Bands[0].Summaries[4].DisplayFormat = " ";
                uGrid.DisplayLayout.Bands[0].Summaries[5].DisplayFormat = " ";
                uGrid.DisplayLayout.Bands[0].Summaries[6].DisplayFormat = " ";
                uGrid.DisplayLayout.Bands[0].Summaries[7].DisplayFormat = " ";
                uGrid.DisplayLayout.Bands[0].Summaries[8].DisplayFormat = " ";

                Utils.AddSumColumn1(lst.Where(n => n.SoLuongMua != null).Sum(n => n.SoLuongMua), uGrid, "SoLuongMua", false, constDatabaseFormat: 5);
                Utils.AddSumColumn1(lst.Where(n => n.GiaTriMua != null).Sum(n => n.GiaTriMua), uGrid, "GiaTriMua", false, constDatabaseFormat: 1);
                Utils.AddSumColumn1(lst.Where(n => n.ChietKhau != null).Sum(n => n.ChietKhau), uGrid, "ChietKhau", false, constDatabaseFormat: 8);
                Utils.AddSumColumn1(lst.Where(n => n.GiaTriTraLai != null).Sum(n => n.GiaTriTraLai), uGrid, "GiaTriTraLai", false, constDatabaseFormat: 1);
                Utils.AddSumColumn1(lst.Where(n => n.GiaTriGiamGia != null).Sum(n => n.GiaTriGiamGia), uGrid, "GiaTriGiamGia", false, constDatabaseFormat: 1);
                Utils.AddSumColumn1(lst.Where(n => n.SoLuongTraLai != null).Sum(n => n.SoLuongTraLai), uGrid, "SoLuongTraLai", false, constDatabaseFormat: 5);
            }
            else
            {
                Utils.AddSumColumn(uGrid, "NgayCTu", false);
                Utils.AddSumColumn(uGrid, "SoCTu", false);
                Utils.AddSumColumn(uGrid, "NgayHoaDon", false);
                Utils.AddSumColumn(uGrid, "SoHoaDon", false);
                Utils.AddSumColumn(uGrid, "Mahang", false);
                Utils.AddSumColumn(uGrid, "Tenhang", false);
                Utils.AddSumColumn(uGrid, "DVT", false);
                Utils.AddSumColumn(uGrid, "DonGia", false);
                Utils.AddSumColumn(uGrid, "SoLuongMua", false);
                Utils.AddSumColumn(uGrid, "SoLuongTraLai", false);

                uGrid.DisplayLayout.Bands[0].Summaries[1].DisplayFormat = " ";
                uGrid.DisplayLayout.Bands[0].Summaries[2].DisplayFormat = " ";
                uGrid.DisplayLayout.Bands[0].Summaries[3].DisplayFormat = " ";
                uGrid.DisplayLayout.Bands[0].Summaries[4].DisplayFormat = " ";
                uGrid.DisplayLayout.Bands[0].Summaries[5].DisplayFormat = " ";
                uGrid.DisplayLayout.Bands[0].Summaries[6].DisplayFormat = " ";
                uGrid.DisplayLayout.Bands[0].Summaries[7].DisplayFormat = " ";
                uGrid.DisplayLayout.Bands[0].Summaries[8].DisplayFormat = " ";
                uGrid.DisplayLayout.Bands[0].Summaries[9].DisplayFormat = " ";
                uGrid.DisplayLayout.Bands[0].Summaries[10].DisplayFormat = " ";

                Utils.AddSumColumn1(lst.Where(n => n.GiaTriMua != null).Sum(n => n.GiaTriMua), uGrid, "GiaTriMua", false, constDatabaseFormat: 1);
                Utils.AddSumColumn1(lst.Where(n => n.ChietKhau != null).Sum(n => n.ChietKhau), uGrid, "ChietKhau", false, constDatabaseFormat: 8);
                Utils.AddSumColumn1(lst.Where(n => n.GiaTriTraLai != null).Sum(n => n.GiaTriTraLai), uGrid, "GiaTriTraLai", false, constDatabaseFormat: 1);
                Utils.AddSumColumn1(lst.Where(n => n.GiaTriGiamGia != null).Sum(n => n.GiaTriGiamGia), uGrid, "GiaTriGiamGia", false, constDatabaseFormat: 1);
            }

            GridLayout();

            uGrid.DisplayLayout.Bands[0].Columns["NgayHachToan"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["NgayHachToan"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["NgayHachToan"].Width = 120;
            uGrid.DisplayLayout.Bands[0].Columns["NgayHachToan"].CellAppearance.TextHAlign = HAlign.Center;
            uGrid.DisplayLayout.Bands[0].Columns["NgayHachToan"].CellActivation = Activation.NoEdit;

            uGrid.DisplayLayout.Bands[0].Columns["NgayCTu"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["NgayCTu"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["NgayCTu"].Width = 120;
            uGrid.DisplayLayout.Bands[0].Columns["NgayCTu"].CellAppearance.TextHAlign = HAlign.Center;
            uGrid.DisplayLayout.Bands[0].Columns["NgayCTu"].CellActivation = Activation.NoEdit;

            uGrid.DisplayLayout.Bands[0].Columns["SoCTu"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["SoCTu"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["SoCTu"].Width = 120;
            uGrid.DisplayLayout.Bands[0].Columns["SoCTu"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["NgayHoaDon"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["NgayHoaDon"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["NgayHoaDon"].Width = 120;
            uGrid.DisplayLayout.Bands[0].Columns["NgayHoaDon"].CellAppearance.TextHAlign = HAlign.Center;
            uGrid.DisplayLayout.Bands[0].Columns["NgayHoaDon"].CellActivation = Activation.NoEdit;

            uGrid.DisplayLayout.Bands[0].Columns["SoHoaDon"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["SoHoaDon"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["SoHoaDon"].Width = 120;
            uGrid.DisplayLayout.Bands[0].Columns["SoHoaDon"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["Mahang"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["Mahang"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["Mahang"].Width = 100;
            uGrid.DisplayLayout.Bands[0].Columns["Mahang"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["Tenhang"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["Tenhang"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["Tenhang"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["Tenhang"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["DVT"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["DVT"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["DVT"].Width = 80;
            uGrid.DisplayLayout.Bands[0].Columns["DVT"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["SoLuongMua"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["SoLuongMua"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["SoLuongMua"].Width = 100;
            uGrid.DisplayLayout.Bands[0].Columns["SoLuongMua"].FormatNumberic(ConstDatabase.Format_Quantity);
            uGrid.DisplayLayout.Bands[0].Columns["SoLuongMua"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["DonGia"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["DonGia"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["DonGia"].Width = 180;
            uGrid.DisplayLayout.Bands[0].Columns["DonGia"].FormatNumberic(ConstDatabase.Format_DonGiaQuyDoi);
            uGrid.DisplayLayout.Bands[0].Columns["DonGia"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["GiaTriMua"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["GiaTriMua"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["GiaTriMua"].Width = 180;
            uGrid.DisplayLayout.Bands[0].Columns["GiaTriMua"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["GiaTriMua"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["ChietKhau"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["ChietKhau"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["ChietKhau"].Width = 150;
            uGrid.DisplayLayout.Bands[0].Columns["ChietKhau"].FormatNumberic(ConstDatabase.Format_Coefficient);
            uGrid.DisplayLayout.Bands[0].Columns["ChietKhau"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["SoLuongTraLai"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["SoLuongTraLai"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["SoLuongTraLai"].Width = 180;
            uGrid.DisplayLayout.Bands[0].Columns["SoLuongTraLai"].FormatNumberic(ConstDatabase.Format_Quantity);
            uGrid.DisplayLayout.Bands[0].Columns["SoLuongTraLai"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["GiaTriTraLai"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["GiaTriTraLai"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["GiaTriTraLai"].Width = 180;
            uGrid.DisplayLayout.Bands[0].Columns["GiaTriTraLai"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["GiaTriTraLai"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["GiaTriGiamGia"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["GiaTriGiamGia"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["GiaTriGiamGia"].Width = 180;
            uGrid.DisplayLayout.Bands[0].Columns["GiaTriGiamGia"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["GiaTriGiamGia"].CellAppearance.TextVAlign = VAlign.Middle;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (data.Count == 0)
            {
                MSG.Warning("Không có dữ liệu để lên báo cáo");
            }
            else
            {
                //check so luong dvt neu = 1 thi co tong so luong, con lai thi ko co tong so luong
                var dtToCount = data.Where(m => m.DVT != null).ToList();
                _soDVT = dtToCount.Where(m => m.DVT.ToString().Trim() != "").Select(m => m.DVT).Distinct().ToList().Count;

                data = data.OrderBy(m => m.NgayCTu).ToList();
                var rD = new PO_GetDetailBook_Detail();
                rD.Period = ReportUtils.GetPeriod(FromDate, ToDate);
                var f = new ReportForm<PO_GetDetailBook>(fileReportSlot1, _subSystemCode);
                f.AddDatasource("DBSOCTMUAHANG", data);
                f.AddDatasource("Detail", rD);
                f.SetHyperlinkFunction("Accounting.ReportUtils", "Hyperlink");
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }


        private void btnExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog
            {
                Title = "Chọn nơi sao lưu ",
                Filter = "File (*.xls)|*.xls",
                InitialDirectory = "@C:\\",
                FileName = "sochitietmuahang.xls"
            };
            if (save.ShowDialog() == DialogResult.OK)
            {
                string duongdan = save.FileName;
                try
                {
                    ultraGridExcelExporter1.Export(uGrid, duongdan);
                    if (MSG.Question("Kết xuất thành công, Bạn có muốn mở file vừa kết xuất không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(save.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
                catch
                {
                    MSG.Warning("Lỗi khi kết xuất !");
                }
            }
        }
        private void GridLayout()
        {
            UltraGridBand parentBand = this.uGrid.DisplayLayout.Bands[0];
            parentBand.RowLayoutStyle = RowLayoutStyle.GroupLayout;

            UltraGridGroup parentBandGroupPeriod;
            if (parentBand.Groups.Exists("ParentBandGroupPeriod"))
            {
                parentBandGroupPeriod = parentBand.Groups["ParentBandGroupPeriod"];
                parentBandGroupPeriod.Header.Caption = period;
            }
            else
                parentBandGroupPeriod = parentBand.Groups.Add("ParentBandGroupPeriod", period);
            parentBandGroupPeriod.Header.Appearance.FontData.Italic = DefaultableBoolean.True;
            parentBandGroupPeriod.Header.Appearance.FontData.SizeInPoints = 10;
            parentBandGroupPeriod.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupPeriod.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPeriod.Header.Appearance.BorderColor = Color.Black;

            UltraGridGroup parentBandGroupTitle;
            if (parentBand.Groups.Exists("ParentBandGroupTitle"))
                parentBandGroupTitle = parentBand.Groups["ParentBandGroupTitle"];
            else
                parentBandGroupTitle = parentBand.Groups.Add("ParentBandGroupTitle", "SỔ CHI TIẾT MUA HÀNG");
            parentBandGroupTitle.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroupTitle.Header.Appearance.FontData.SizeInPoints = 12;
            parentBandGroupTitle.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupTitle.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupTitle.Header.Appearance.BorderColor = Color.Black;

            parentBandGroupPeriod.RowLayoutGroupInfo.ParentGroup = parentBandGroupTitle;

            parentBand.Columns["NgayHachToan"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["NgayHachToan"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["NgayHachToan"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["NgayHachToan"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["NgayHachToan"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["NgayHachToan"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["NgayCTu"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["NgayCTu"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["NgayCTu"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["NgayCTu"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["NgayCTu"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["NgayCTu"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["SoCTu"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["SoCTu"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SoCTu"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["SoCTu"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["SoCTu"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SoCTu"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["NgayHoaDon"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["NgayHoaDon"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["NgayHoaDon"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["NgayHoaDon"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["NgayHoaDon"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["NgayHoaDon"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["SoHoaDon"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["SoHoaDon"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SoHoaDon"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["SoHoaDon"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["SoHoaDon"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SoHoaDon"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["Mahang"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["Mahang"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Mahang"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["Mahang"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["Mahang"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Mahang"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["Tenhang"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["Tenhang"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Tenhang"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["Tenhang"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["Tenhang"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Tenhang"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["DVT"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["DVT"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["DVT"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["DVT"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["DVT"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["DVT"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["SoLuongMua"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["SoLuongMua"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SoLuongMua"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["SoLuongMua"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["SoLuongMua"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SoLuongMua"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["DonGia"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["DonGia"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["DonGia"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["DonGia"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["DonGia"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["DonGia"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["GiaTriMua"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["GiaTriMua"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GiaTriMua"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["GiaTriMua"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["GiaTriMua"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GiaTriMua"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["ChietKhau"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["ChietKhau"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ChietKhau"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["ChietKhau"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["ChietKhau"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ChietKhau"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["SoLuongTraLai"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["SoLuongTraLai"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SoLuongTraLai"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["SoLuongTraLai"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["SoLuongTraLai"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SoLuongTraLai"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["GiaTriTraLai"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["GiaTriTraLai"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GiaTriTraLai"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["GiaTriTraLai"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["GiaTriTraLai"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GiaTriTraLai"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["GiaTriGiamGia"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["GiaTriGiamGia"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GiaTriGiamGia"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["GiaTriGiamGia"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["GiaTriGiamGia"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GiaTriGiamGia"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["NgayHachToan"].RowLayoutColumnInfo.OriginX = 0;
            parentBand.Columns["NgayHachToan"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["NgayHachToan"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["NgayHachToan"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["NgayCTu"].RowLayoutColumnInfo.OriginX = 120;
            parentBand.Columns["NgayCTu"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["NgayCTu"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["NgayCTu"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["SoCTu"].RowLayoutColumnInfo.OriginX = 240;
            parentBand.Columns["SoCTu"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["SoCTu"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["SoCTu"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["NgayHoaDon"].RowLayoutColumnInfo.OriginX = 360;
            parentBand.Columns["NgayHoaDon"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["NgayHoaDon"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["NgayHoaDon"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["SoHoaDon"].RowLayoutColumnInfo.OriginX = 480;
            parentBand.Columns["SoHoaDon"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["SoHoaDon"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["SoHoaDon"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["Mahang"].RowLayoutColumnInfo.OriginX = 600;
            parentBand.Columns["Mahang"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["Mahang"].RowLayoutColumnInfo.SpanX = 100;
            parentBand.Columns["Mahang"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["Tenhang"].RowLayoutColumnInfo.OriginX = 700;
            parentBand.Columns["Tenhang"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["Tenhang"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["Tenhang"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["DVT"].RowLayoutColumnInfo.OriginX = 950;
            parentBand.Columns["DVT"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["DVT"].RowLayoutColumnInfo.SpanX = 80;
            parentBand.Columns["DVT"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["SoLuongMua"].RowLayoutColumnInfo.OriginX = 1030;
            parentBand.Columns["SoLuongMua"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["SoLuongMua"].RowLayoutColumnInfo.SpanX = 150;
            parentBand.Columns["SoLuongMua"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["DonGia"].RowLayoutColumnInfo.OriginX = 1180;
            parentBand.Columns["DonGia"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["DonGia"].RowLayoutColumnInfo.SpanX = 180;
            parentBand.Columns["DonGia"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["GiaTriMua"].RowLayoutColumnInfo.OriginX = 1360;
            parentBand.Columns["GiaTriMua"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["GiaTriMua"].RowLayoutColumnInfo.SpanX = 180;
            parentBand.Columns["GiaTriMua"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["ChietKhau"].RowLayoutColumnInfo.OriginX = 1540;
            parentBand.Columns["ChietKhau"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["ChietKhau"].RowLayoutColumnInfo.SpanX = 150;
            parentBand.Columns["ChietKhau"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["SoLuongTraLai"].RowLayoutColumnInfo.OriginX = 1690;
            parentBand.Columns["SoLuongTraLai"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["SoLuongTraLai"].RowLayoutColumnInfo.SpanX = 150;
            parentBand.Columns["SoLuongTraLai"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["GiaTriTraLai"].RowLayoutColumnInfo.OriginX = 1840;
            parentBand.Columns["GiaTriTraLai"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["GiaTriTraLai"].RowLayoutColumnInfo.SpanX = 180;
            parentBand.Columns["GiaTriTraLai"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["GiaTriGiamGia"].RowLayoutColumnInfo.OriginX = 2020;
            parentBand.Columns["GiaTriGiamGia"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["GiaTriGiamGia"].RowLayoutColumnInfo.SpanX = 180;
            parentBand.Columns["GiaTriGiamGia"].RowLayoutColumnInfo.SpanY = 32;

        }

        private void ultraGridExcelExporter1_InitializeColumn(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.InitializeColumnEventArgs e)
        {
            e.ExcelFormatStr = e.Column.Format;
        }

        private void ultraGridExcelExporter1_BeginExport(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.BeginExportEventArgs e)
        {
            foreach (var row in uGrid.Rows)
            {
                foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
                {
                    e.Rows[row.Index].Cells[column].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                    e.Rows[row.Index].Cells[column].Appearance.BorderColor = Color.Black;
                    e.Rows[row.Index].Cells[column].Column.Width = 200;
                }
            }
            e.Rows.Band.Summaries[0].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[0].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[1].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[1].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[2].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[2].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[3].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[3].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[4].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[4].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[5].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[5].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[6].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[6].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[7].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[7].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[8].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[8].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[9].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[9].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[10].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[10].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[11].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[11].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[12].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[12].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[13].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[13].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[14].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[14].Appearance.BorderColor = Color.Black;


        }
        private void reportManager1_GetReportParameter(object sender, PerpetuumSoft.Reporting.Components.GetReportParameterEventArgs e)
        {
            e.Parameters["Sodvt"].Value = _soDVT.ToString();
        }

        private void uGrid_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            UltraGridCell cell = e.Row.Cells[0];
            cell.Row.Height = 25;

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("SoLuongMua"))
            {
                if (cell.Row.Cells["SoLuongMua"].Value != null && (decimal)cell.Row.Cells["SoLuongMua"].Value == 0)
                    cell.Row.Cells["SoLuongMua"].Value = null;
                if ((cell.Row.Cells["SoLuongMua"].Value != null && (decimal)cell.Row.Cells["SoLuongMua"].Value < 0))
                    cell.Row.Cells["SoLuongMua"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("DonGia"))
            {
                if (cell.Row.Cells["DonGia"].Value != null && (decimal)cell.Row.Cells["DonGia"].Value == 0)
                    cell.Row.Cells["DonGia"].Value = null;
                if ((cell.Row.Cells["DonGia"].Value != null && (decimal)cell.Row.Cells["DonGia"].Value < 0))
                    cell.Row.Cells["DonGia"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("GiaTriMua"))
            {
                if (cell.Row.Cells["GiaTriMua"].Value != null && (decimal)cell.Row.Cells["GiaTriMua"].Value == 0)
                    cell.Row.Cells["GiaTriMua"].Value = null;
                if ((cell.Row.Cells["GiaTriMua"].Value != null && (decimal)cell.Row.Cells["GiaTriMua"].Value < 0))
                    cell.Row.Cells["GiaTriMua"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("ChietKhau"))
            {
                if (cell.Row.Cells["ChietKhau"].Value != null && (decimal)cell.Row.Cells["ChietKhau"].Value == 0)
                    cell.Row.Cells["ChietKhau"].Value = null;
                if ((cell.Row.Cells["ChietKhau"].Value != null && (decimal)cell.Row.Cells["ChietKhau"].Value < 0))
                    cell.Row.Cells["ChietKhau"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("GiaTriTraLai"))
            {
                if (cell.Row.Cells["GiaTriTraLai"].Value != null && (decimal)cell.Row.Cells["GiaTriTraLai"].Value == 0)
                    cell.Row.Cells["GiaTriTraLai"].Value = null;
                if ((cell.Row.Cells["GiaTriTraLai"].Value != null && (decimal)cell.Row.Cells["GiaTriTraLai"].Value < 0))
                    cell.Row.Cells["GiaTriTraLai"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("GiaTriGiamGia"))
            {
                if (cell.Row.Cells["GiaTriGiamGia"].Value != null && (decimal)cell.Row.Cells["GiaTriGiamGia"].Value == 0)
                    cell.Row.Cells["GiaTriGiamGia"].Value = null;
                if ((cell.Row.Cells["GiaTriGiamGia"].Value != null && (decimal)cell.Row.Cells["GiaTriGiamGia"].Value < 0))
                    cell.Row.Cells["GiaTriGiamGia"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("SoLuongTraLai"))
            {
                if (cell.Row.Cells["SoLuongTraLai"].Value != null && (decimal)cell.Row.Cells["SoLuongTraLai"].Value == 0)
                    cell.Row.Cells["SoLuongTraLai"].Value = null;
                if ((cell.Row.Cells["SoLuongTraLai"].Value != null && (decimal)cell.Row.Cells["SoLuongTraLai"].Value < 0))
                    cell.Row.Cells["SoLuongTraLai"].Appearance.ForeColor = Color.Red;
            }

        }

        private void FSOKTCTTIENMATex_SizeChanged(object sender, EventArgs e)
        {
            var state = this.WindowState;
            switch (state)
            {
                case FormWindowState.Maximized:
                    break;
                case FormWindowState.Minimized:
                    break;
                case FormWindowState.Normal:
                    this.Left = (Screen.PrimaryScreen.Bounds.Width - this.Width) / 2;
                    this.Top = (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2;
                    this.Location = new Point((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2, (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
                    break;
                default:
                    break;
            }
        }

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                PO_GetDetailBook temp = (PO_GetDetailBook)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                if (temp != null)
                    editFuntion(temp);

            }
        }

        private void editFuntion(PO_GetDetailBook temp)
        {

            var f = Utils.ViewVoucherSelected1(temp.RefID, temp.TypeID);
            //List<PO_GetDetailBook> data1 = sp.Get_SoChiTietMuaHang(FromDate, ToDate, dsAccountID, dsVatTuID).ToList();
            //configGrid(data1);

        }

        private void FSOKTCTTIENMATex_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }

        private void ultraGridExcelExporter1_SummaryCellExported(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.SummaryCellExportedEventArgs e)
        {
            Worksheet ws = e.CurrentWorksheet;
            WorksheetRow wsRow = ws.Rows[e.CurrentRowIndex];
            WorksheetCell wsCell = wsRow.Cells[e.CurrentColumnIndex];
            if (!e.Summary.SummaryText.Contains("Số dòng") && !e.Summary.SummaryText.Contains(" "))
            {
                string s = e.Summary.SummaryText.Trim('.');
                if (s.Contains("("))
                    wsCell.Value = -decimal.Parse(s.Trim('(').Trim(')'));
                else
                    wsCell.Value = decimal.Parse(s);

                wsCell.CellFormat.FormatString = "###,###,###,###,##0;(###,###,###,###,##0)";
            }
        }
    }

}
