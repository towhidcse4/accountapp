﻿using Accounting.Core.DAO;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.Report;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Documents.Excel;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRBANGKECHUNGTUCHUALAPCTGSex : Form
    {
        private string _subSystemCode;
        ReportProcedureSDS sp = new ReportProcedureSDS();
        List<GVoucherListDetail> data = new List<GVoucherListDetail>();
        List<GVoucherListDetail> lstDetails1 = new List<GVoucherListDetail>();
        List<GVoucherListDetail> lstDetails2 = new List<GVoucherListDetail>();
        List<GVoucherListDetail> lstDetails = new List<GVoucherListDetail>();
        string period = "";
        DateTime FromDate;
        DateTime ToDate;
        List<GenCode> lstItem;
        public FRBANGKECHUNGTUCHUALAPCTGSex(DateTime FromDate1, DateTime ToDate1, List<GenCode> lstItem1)
        {
            InitializeComponent();
            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\BangKeChungTuChuaLapChungTuGhiSo.rst", path);
            ReportUtils.ProcessControls(this);

            lstDetails = IoC.Resolve<IGeneralLedgerService>().GetRecordingVouchers(lstItem1.Select(n => n.TypeGroupID).ToList(), FromDate1, ToDate1);

            List <GVoucherListDetail> data1 = lstDetails;
            lstDetails = lstDetails.OrderBy(n => n.Type).ThenByDescending(n => n.VoucherDate).ThenBy(n => n.VoucherNo).ThenBy(n => n.OrderPriority).ToList();

            lstDetails2 = lstDetails.GroupBy(n => n.Type).Select(t => new GVoucherListDetail
            {
                Type = t.Key,
                VoucherDescription = "Cộng nhóm",
                VoucherAmount = t.Where(n => n.VoucherAmount != null).Sum(n => n.VoucherAmount),
                ordercode = 3

            }).ToList();

            lstDetails1 = lstDetails.GroupBy(n => n.Type).Select(t => new GVoucherListDetail
            {
                Type = t.Key,
                VoucherDescription = "Loại chứng từ: " + t.Key,
                ordercode = 1

            }).ToList();

            foreach (var item in lstDetails1)
            {
                data1.Add(item);
            }
            foreach (var item in lstDetails2)
            {
                data1.Add(item);
            }

            data1 = data1.OrderByDescending(n => n.VoucherDate).ThenBy(n => n.VoucherNo).ThenBy(n => n.OrderPriority).ToList();
            data1 = data1.OrderBy(n => n.Type).ThenBy(n => n.ordercode).ToList();
            period = "Từ ngày " + FromDate1.ToString("dd/MM/yyyy") + " đến ngày " + ToDate1.ToString("dd/MM/yyyy");

            FromDate = FromDate1;
            ToDate = ToDate1;
            data = data1;
            lstItem = lstItem1;

            configGrid(data1);

            if (Utils.isDemo)
                btnExport.Visible = false;
            else
                btnExport.Visible = true;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (data.Count == 0)
            {
                MSG.Warning("Không tìm thấy chứng từ trong khoảng thời gian đã chọn");
                return;
            }
            else
            {
                var rD = new GVoucherListDetail_period();
                rD.Period = "Từ ngày " + FromDate.ToString("dd/MM/yyyy") + " đến ngày " + ToDate.ToString("dd/MM/yyyy");
                var f = new ReportForm<GVoucherListDetail>(fileReportSlot1, _subSystemCode);
                f.AddDatasource("Period", rD);
                f.AddDatasource("Data", lstDetails);
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog
            {
                Title = "Chọn nơi sao lưu ",
                Filter = "File (*.xls)|*.xls",
                InitialDirectory = "@C:\\",
                FileName = "bangkechungtuchualapchungtughiso.xls"
            };
            if (save.ShowDialog() == DialogResult.OK)
            {
                string duongdan = save.FileName;
                try
                {
                    ultraGridExcelExporter1.Export(uGrid, duongdan);
                    if (MSG.Question("Kết xuất thành công, Bạn có muốn mở file vừa kết xuất không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(save.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
                catch
                {
                    MSG.Warning("Lỗi khi kết xuất !");
                }
            }
        }     

        private void configGrid(List<GVoucherListDetail> lst)
        {
            uGrid.DataSource = lst;
            Utils.ConfigGrid(uGrid, ConstDatabase.BANGKECTCHUALAPCTGS_TableName);
            uGrid.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;

            lst.ForEach(t =>
            {
                if (t.VoucherDescription == null)
                    t.VoucherDescription = "";
                else
                    t.VoucherDescription = t.VoucherDescription;
            });

            Utils.AddSumColumn(uGrid, "VoucherNo", false);
            Utils.AddSumColumn(uGrid, "VoucherDescription", false);
            Utils.AddSumColumn(uGrid, "VoucherDebitAccount", false);
            Utils.AddSumColumn(uGrid, "VoucherCreditAccount", false);

            uGrid.DisplayLayout.Bands[0].Summaries[1].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[2].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[3].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[4].DisplayFormat = " ";

            uGrid.DisplayLayout.Bands[0].Columns["VoucherDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["VoucherDate"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["VoucherDate"].Width = 120;
            uGrid.DisplayLayout.Bands[0].Columns["VoucherDate"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["VoucherNo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["VoucherNo"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["VoucherNo"].CellAppearance.TextVAlign = VAlign.Middle;
            uGrid.DisplayLayout.Bands[0].Columns["VoucherNo"].Width = 120;

            uGrid.DisplayLayout.Bands[0].Columns["VoucherDescription"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["VoucherDescription"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["VoucherDescription"].CellAppearance.TextVAlign = VAlign.Middle;
            uGrid.DisplayLayout.Bands[0].Columns["VoucherDescription"].Width = 200;

            uGrid.DisplayLayout.Bands[0].Columns["VoucherDebitAccount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["VoucherDebitAccount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["VoucherDebitAccount"].Width = 120;
            uGrid.DisplayLayout.Bands[0].Columns["VoucherDebitAccount"].CellAppearance.TextVAlign = VAlign.Middle;
            uGrid.DisplayLayout.Bands[0].Columns["VoucherDebitAccount"].CellAppearance.TextHAlign = HAlign.Center;

            uGrid.DisplayLayout.Bands[0].Columns["VoucherCreditAccount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["VoucherCreditAccount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["VoucherCreditAccount"].Width = 120;
            uGrid.DisplayLayout.Bands[0].Columns["VoucherCreditAccount"].CellAppearance.TextVAlign = VAlign.Middle;
            uGrid.DisplayLayout.Bands[0].Columns["VoucherCreditAccount"].CellAppearance.TextHAlign = HAlign.Center;

            uGrid.DisplayLayout.Bands[0].Columns["VoucherAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["VoucherAmount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["VoucherAmount"].Width = 200;
            uGrid.DisplayLayout.Bands[0].Columns["VoucherAmount"].CellAppearance.TextVAlign = VAlign.Middle;
            uGrid.DisplayLayout.Bands[0].Columns["VoucherAmount"].FormatNumberic(ConstDatabase.Format_TienVND);

            Utils.AddSumColumn1(lst.Where(n => n.VoucherAmount != null).Sum(n => n.VoucherAmount), uGrid, "VoucherAmount", false, constDatabaseFormat:1);
            GridLayout();
        }
        private void GridLayout()
        {
            UltraGridBand parentBand = this.uGrid.DisplayLayout.Bands[0];
            parentBand.RowLayoutStyle = RowLayoutStyle.GroupLayout;

            UltraGridGroup parentBandGroupPeriod;
            if (parentBand.Groups.Exists("ParentBandGroupPeriod"))
            {
                parentBandGroupPeriod = parentBand.Groups["ParentBandGroupPeriod"];
                parentBandGroupPeriod.Header.Caption = period;
            }
            else
                parentBandGroupPeriod = parentBand.Groups.Add("ParentBandGroupPeriod", period);
            parentBandGroupPeriod.Header.Appearance.FontData.Italic = DefaultableBoolean.True;
            parentBandGroupPeriod.Header.Appearance.FontData.SizeInPoints = 10;
            parentBandGroupPeriod.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupPeriod.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPeriod.Header.Appearance.BorderColor = Color.Black;

            UltraGridGroup parentBandGroupTitle;
            if (parentBand.Groups.Exists("ParentBandGroupTitle"))
                parentBandGroupTitle = parentBand.Groups["ParentBandGroupTitle"];
            else
                parentBandGroupTitle = parentBand.Groups.Add("ParentBandGroupTitle", "BẢNG KÊ CHỨNG TỪ CHƯA LẬP CHỨNG TỪ GHI SỔ");
            parentBandGroupTitle.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroupTitle.Header.Appearance.FontData.SizeInPoints = 12;
            parentBandGroupTitle.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupTitle.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupTitle.Header.Appearance.BorderColor = Color.Black;

            parentBandGroupPeriod.RowLayoutGroupInfo.ParentGroup = parentBandGroupTitle;

            parentBand.Columns["VoucherDate"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["VoucherDate"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["VoucherDate"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["VoucherDate"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["VoucherDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["VoucherDate"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["VoucherNo"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["VoucherNo"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["VoucherNo"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["VoucherNo"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["VoucherNo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["VoucherNo"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["VoucherDescription"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["VoucherDescription"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["VoucherDescription"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["VoucherDescription"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["VoucherDescription"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["VoucherDescription"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["VoucherDebitAccount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["VoucherDebitAccount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["VoucherDebitAccount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["VoucherDebitAccount"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["VoucherDebitAccount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["VoucherDebitAccount"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["VoucherCreditAccount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["VoucherCreditAccount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["VoucherCreditAccount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["VoucherCreditAccount"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["VoucherCreditAccount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["VoucherCreditAccount"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["VoucherAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["VoucherAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["VoucherAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["VoucherAmount"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["VoucherAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["VoucherAmount"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["VoucherDate"].RowLayoutColumnInfo.OriginX = 0;
            parentBand.Columns["VoucherDate"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["VoucherDate"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["VoucherDate"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["VoucherNo"].RowLayoutColumnInfo.OriginX = 120;
            parentBand.Columns["VoucherNo"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["VoucherNo"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["VoucherNo"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["VoucherDescription"].RowLayoutColumnInfo.OriginX = 240;
            parentBand.Columns["VoucherDescription"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["VoucherDescription"].RowLayoutColumnInfo.SpanX = 200;
            parentBand.Columns["VoucherDescription"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["VoucherDebitAccount"].RowLayoutColumnInfo.OriginX = 440;
            parentBand.Columns["VoucherDebitAccount"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["VoucherDebitAccount"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["VoucherDebitAccount"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["VoucherCreditAccount"].RowLayoutColumnInfo.OriginX = 560;
            parentBand.Columns["VoucherCreditAccount"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["VoucherCreditAccount"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["VoucherCreditAccount"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["VoucherAmount"].RowLayoutColumnInfo.OriginX = 680;
            parentBand.Columns["VoucherAmount"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["VoucherAmount"].RowLayoutColumnInfo.SpanX = 200;
            parentBand.Columns["VoucherAmount"].RowLayoutColumnInfo.SpanY = 32;
            
        }

        private void uGrid_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            UltraGridCell cell = e.Row.Cells[0];
            cell.Row.Height = 25;

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("VoucherDescription"))
                if (cell.Row.Cells["VoucherDescription"].Value != null && (cell.Row.Cells["VoucherDescription"].Value.ToString().Contains("Loại chứng từ: ") || cell.Row.Cells["VoucherDescription"].Value.ToString() == "Cộng nhóm" ))
                {
                    cell.Row.Cells["VoucherDate"].Value = null;
                    cell.Row.Cells["VoucherDescription"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    cell.Row.Cells["VoucherAmount"].Appearance.FontData.Bold = DefaultableBoolean.True;

                }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("VoucherAmount"))
            {
                if (cell.Row.Cells["VoucherAmount"].Value != null && (decimal)cell.Row.Cells["VoucherAmount"].Value == 0)
                    cell.Row.Cells["VoucherAmount"].Value = null;
                if ((cell.Row.Cells["VoucherAmount"].Value != null && (decimal)cell.Row.Cells["VoucherAmount"].Value < 0))
                    cell.Row.Cells["VoucherAmount"].Appearance.ForeColor = Color.Red;
            }
           

        }

        private void ultraGridExcelExporter1_InitializeColumn(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.InitializeColumnEventArgs e)
        {
            e.ExcelFormatStr = e.Column.Format;
        }

        private void ultraGridExcelExporter1_BeginExport(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.BeginExportEventArgs e)
        {
            foreach (var row in uGrid.Rows)
            {
                foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
                {
                    e.Rows[row.Index].Cells[column].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                    e.Rows[row.Index].Cells[column].Appearance.BorderColor = Color.Black;
                    e.Rows[row.Index].Cells[column].Column.Width = 200;
                }
            }
            e.Rows.Band.Summaries[0].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[0].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[1].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[1].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[2].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[2].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[3].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[3].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[4].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[4].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[5].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[5].Appearance.BorderColor = Color.Black;
        }

        private void FRB01aDNN_SizeChanged(object sender, EventArgs e)
        {
            var state = this.WindowState;
            switch (state)
            {
                case FormWindowState.Maximized:
                    break;
                case FormWindowState.Minimized:
                    break;
                case FormWindowState.Normal:
                    this.Left = (Screen.PrimaryScreen.Bounds.Width - this.Width) / 2;
                    this.Top = (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2;
                    this.Location = new Point((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2, (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
                    break;
                default:
                    break;
            }
        }

        private void ultraGridExcelExporter1_SummaryCellExported(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.SummaryCellExportedEventArgs e)
        {
            Worksheet ws = e.CurrentWorksheet;
            WorksheetRow wsRow = ws.Rows[e.CurrentRowIndex];
            WorksheetCell wsCell = wsRow.Cells[e.CurrentColumnIndex];
            if (!e.Summary.SummaryText.Contains("Số dòng") && !e.Summary.SummaryText.Contains(" "))
            {
                string s = e.Summary.SummaryText.Trim('.');
                if (s.Contains("("))
                    wsCell.Value = -decimal.Parse(s.Trim('(').Trim(')'));
                else
                    wsCell.Value = decimal.Parse(s);

                wsCell.CellFormat.FormatString = "###,###,###,###,##0;(###,###,###,###,##0)";
            }
        }

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                GVoucherListDetail temp = (GVoucherListDetail)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                if (temp != null)
                    editFuntion(temp);

            }
        }

        private void editFuntion(GVoucherListDetail temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.VoucherID, temp.VoucherTypeID);
            //lstDetails = IoC.Resolve<IGeneralLedgerService>().GetRecordingVouchers(lstItem.Select(n => n.TypeGroupID).ToList(), FromDate, ToDate);
            //data = lstDetails;
            //lstDetails = lstDetails.OrderBy(n => n.Type).ThenByDescending(n => n.VoucherDate).ThenBy(n => n.VoucherNo).ThenBy(n => n.OrderPriority).ToList();

            //lstDetails2 = lstDetails.GroupBy(n => n.Type).Select(t => new GVoucherListDetail
            //{
            //    Type = t.Key,
            //    VoucherDescription = "Cộng nhóm",
            //    VoucherAmount = t.Where(n => n.VoucherAmount != null).Sum(n => n.VoucherAmount),
            //    ordercode = 3

            //}).ToList();

            //lstDetails1 = lstDetails.GroupBy(n => n.Type).Select(t => new GVoucherListDetail
            //{
            //    Type = t.Key,
            //    VoucherDescription = "Loại chứng từ: " + t.Key,
            //    ordercode = 1

            //}).ToList();

            //foreach (var item in lstDetails1)
            //{
            //    data.Add(item);
            //}
            //foreach (var item in lstDetails2)
            //{
            //    data.Add(item);
            //}
            //data = data.OrderByDescending(n => n.VoucherDate).ThenBy(n => n.VoucherNo).ThenBy(n => n.OrderPriority).ToList();
            //data = data.OrderBy(n => n.Type).ThenBy(n => n.ordercode).ToList();
            //configGrid(data);
        }
    }
}
