﻿using Accounting.Core.DAO;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.Report;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Documents.Excel;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRTongHopCPTheoKhoanMucCPex : Form
    {
        private string _subSystemCode;
        private IExpenseItemService _IExpenseItemService
        {
            get { return IoC.Resolve<IExpenseItemService>(); }
        }
        List<ExpenseItem> data = new List<ExpenseItem>();
        List<string> lstAcc;
        DateTime FromDate;
        DateTime ToDate;
        List<ExpenseItemTongHopCP> lst;
        string period = "";
        public FRTongHopCPTheoKhoanMucCPex(DateTime FromDate1, DateTime ToDate1, List<string> lstAcc1, List<ExpenseItemTongHopCP> lst1)
        {
            InitializeComponent();
            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\TongHopCPTheoKMCP.rst", path);
            ReportUtils.ProcessControls(this);

            List<ExpenseItem> data1 = _IExpenseItemService.ReportExpenseItem(lstAcc1, lst1, FromDate1, ToDate1);
            period = ReportUtils.GetPeriod(FromDate1, ToDate1);
            FromDate = FromDate1;
            ToDate = ToDate1;
            lstAcc = lstAcc1;
            lst = lst1;
            data = data1;
            int stt = 1;
            data1.ForEach(t =>
            {
                t.STT = stt;
                stt++;
            });
            configGrid(data1);
            if (Utils.isDemo)
                btnExport.Visible = false;
            else
                btnExport.Visible = true;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (data.Count == 0)
            {
                MSG.Warning("Không có dữ liệu để lên báo cáo");
            }
            else
            {
                var rD = new ExpenseItem_Period();
                rD.Period = ReportUtils.GetPeriod(FromDate, ToDate);
                rD.acc = GetAccount(lstAcc).TrimStart(',');
                var f = new ReportForm<ExpenseItem>(fileReportSlot1, _subSystemCode);
                f.AddDatasource("KMCP", data, true);
                f.AddDatasource("Detail", rD);
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
        }
        string GetAccount(List<string> Acc)
        {
            var acc = "";
            foreach (var x in Acc)
            {
                acc = acc + ", " + x;
            }
            return acc;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog
            {
                Title = "Chọn nơi sao lưu ",
                Filter = "File (*.xls)|*.xls",
                InitialDirectory = "@C:\\",
                FileName = "tonghopcptheokhoanmuccp.xls"
            };
            if (save.ShowDialog() == DialogResult.OK)
            {
                string duongdan = save.FileName;
                try
                {
                    ultraGridExcelExporter1.Export(uGrid, duongdan);
                    if (MSG.Question("Kết xuất thành công, Bạn có muốn mở file vừa kết xuất không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(save.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
                catch
                {
                    MSG.Warning("Lỗi khi kết xuất !");
                }
            }
        }     

        private void configGrid(List<ExpenseItem> lst)
        {
            uGrid.DataSource = lst;
            Utils.ConfigGrid(uGrid, ConstDatabase.TongHopCPTheoKhoanMucCP_TableName);
            uGrid.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;

            Utils.AddSumColumn(uGrid, "ExpenseItemCode", false);
            Utils.AddSumColumn(uGrid, "ExpenseItemName", false);

            uGrid.DisplayLayout.Bands[0].Summaries[1].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[2].DisplayFormat = " ";

            uGrid.DisplayLayout.Bands[0].Columns["STT"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["STT"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["STT"].Width = 70;
            uGrid.DisplayLayout.Bands[0].Columns["STT"].CellAppearance.TextVAlign = VAlign.Middle;
            uGrid.DisplayLayout.Bands[0].Columns["STT"].CellAppearance.TextHAlign = HAlign.Center;

            uGrid.DisplayLayout.Bands[0].Columns["ExpenseItemCode"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["ExpenseItemCode"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["ExpenseItemCode"].CellAppearance.TextVAlign = VAlign.Middle;
            uGrid.DisplayLayout.Bands[0].Columns["ExpenseItemCode"].Width = 120;

            uGrid.DisplayLayout.Bands[0].Columns["ExpenseItemName"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["ExpenseItemName"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["ExpenseItemName"].CellAppearance.TextVAlign = VAlign.Middle;
            uGrid.DisplayLayout.Bands[0].Columns["ExpenseItemName"].Width = 200;

            uGrid.DisplayLayout.Bands[0].Columns["Amount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["Amount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["Amount"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["Amount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["Amount"].CellAppearance.TextVAlign = VAlign.Middle;

            Utils.AddSumColumn1(lst.Where(n => n.Amount != null).Sum(n => n.Amount), uGrid, "Amount", false, constDatabaseFormat: 1);
            GridLayout();
        }
        private void GridLayout()
        {
            UltraGridBand parentBand = this.uGrid.DisplayLayout.Bands[0];
            parentBand.RowLayoutStyle = RowLayoutStyle.GroupLayout;

            UltraGridGroup parentBandGroupPeriod;
            if (parentBand.Groups.Exists("ParentBandGroupPeriod"))
            {
                parentBandGroupPeriod = parentBand.Groups["ParentBandGroupPeriod"];
                parentBandGroupPeriod.Header.Caption = period;
            }
            else
                parentBandGroupPeriod = parentBand.Groups.Add("ParentBandGroupPeriod", period);
            parentBandGroupPeriod.Header.Appearance.FontData.Italic = DefaultableBoolean.True;
            parentBandGroupPeriod.Header.Appearance.FontData.SizeInPoints = 10;
            parentBandGroupPeriod.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupPeriod.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPeriod.Header.Appearance.BorderColor = Color.Black;

            UltraGridGroup parentBandGroupTitle;
            if (parentBand.Groups.Exists("ParentBandGroupTitle"))
                parentBandGroupTitle = parentBand.Groups["ParentBandGroupTitle"];
            else
                parentBandGroupTitle = parentBand.Groups.Add("ParentBandGroupTitle", "TỔNG HỢP CHI PHÍ THEO KHOẢN MỤC CHI PHÍ ");
            parentBandGroupTitle.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroupTitle.Header.Appearance.FontData.SizeInPoints = 12;
            parentBandGroupTitle.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupTitle.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupTitle.Header.Appearance.BorderColor = Color.Black;

            parentBandGroupPeriod.RowLayoutGroupInfo.ParentGroup = parentBandGroupTitle;

            parentBand.Columns["STT"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["STT"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["STT"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["STT"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["STT"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["STT"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["ExpenseItemCode"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["ExpenseItemCode"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ExpenseItemCode"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["ExpenseItemCode"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["ExpenseItemCode"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ExpenseItemCode"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["ExpenseItemName"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["ExpenseItemName"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ExpenseItemName"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["ExpenseItemName"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["ExpenseItemName"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ExpenseItemName"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["Amount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["Amount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Amount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["Amount"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["Amount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Amount"].Header.Appearance.BorderColor = Color.Black;
            
            parentBand.Columns["STT"].RowLayoutColumnInfo.OriginX = 0;
            parentBand.Columns["STT"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["STT"].RowLayoutColumnInfo.SpanX = 70;
            parentBand.Columns["STT"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["ExpenseItemCode"].RowLayoutColumnInfo.OriginX = 70;
            parentBand.Columns["ExpenseItemCode"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["ExpenseItemCode"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["ExpenseItemCode"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["ExpenseItemName"].RowLayoutColumnInfo.OriginX = 190;
            parentBand.Columns["ExpenseItemName"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["ExpenseItemName"].RowLayoutColumnInfo.SpanX = 200;
            parentBand.Columns["ExpenseItemName"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["Amount"].RowLayoutColumnInfo.OriginX = 390;
            parentBand.Columns["Amount"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["Amount"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["Amount"].RowLayoutColumnInfo.SpanY = 32;
        }

        private void uGrid_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            UltraGridCell cell = e.Row.Cells[0];
            cell.Row.Height = 25;

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("Amount"))
            {
                if (cell.Row.Cells["Amount"].Value != null && (decimal)cell.Row.Cells["Amount"].Value == 0)
                    cell.Row.Cells["Amount"].Value = null;
                if ((cell.Row.Cells["Amount"].Value != null && (decimal)cell.Row.Cells["Amount"].Value < 0))
                    cell.Row.Cells["Amount"].Appearance.ForeColor = Color.Red;
            }

        }

        private void ultraGridExcelExporter1_InitializeColumn(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.InitializeColumnEventArgs e)
        {
            e.ExcelFormatStr = e.Column.Format;
        }

        private void ultraGridExcelExporter1_BeginExport(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.BeginExportEventArgs e)
        {
            foreach (var row in uGrid.Rows)
            {
                foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
                {
                    e.Rows[row.Index].Cells[column].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                    e.Rows[row.Index].Cells[column].Appearance.BorderColor = Color.Black;
                    e.Rows[row.Index].Cells[column].Column.Width = 200;
                }
            }
            e.Rows.Band.Summaries[0].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[0].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[1].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[1].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[2].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[2].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[3].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[3].Appearance.BorderColor = Color.Black;
        }

        private void FRB01aDNN_SizeChanged(object sender, EventArgs e)
        {
            var state = this.WindowState;
            switch (state)
            {
                case FormWindowState.Maximized:
                    break;
                case FormWindowState.Minimized:
                    break;
                case FormWindowState.Normal:
                    this.Left = (Screen.PrimaryScreen.Bounds.Width - this.Width) / 2;
                    this.Top = (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2;
                    this.Location = new Point((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2, (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
                    break;
                default:
                    break;
            }
        }

        private void ultraGridExcelExporter1_SummaryCellExported(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.SummaryCellExportedEventArgs e)
        {
            Worksheet ws = e.CurrentWorksheet;
            WorksheetRow wsRow = ws.Rows[e.CurrentRowIndex];
            WorksheetCell wsCell = wsRow.Cells[e.CurrentColumnIndex];
            if (!e.Summary.SummaryText.Contains("Số dòng") && !e.Summary.SummaryText.Contains(" "))
            {
                string s = e.Summary.SummaryText.Trim('.');
                if (s.Contains("("))
                    wsCell.Value = -decimal.Parse(s.Trim('(').Trim(')'));
                else
                    wsCell.Value = decimal.Parse(s);

                wsCell.CellFormat.FormatString = "###,###,###,###,##0;(###,###,###,###,##0)";
            }
        }
    }
}
