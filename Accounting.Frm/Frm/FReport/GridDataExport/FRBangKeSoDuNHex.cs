﻿using Accounting.Core.DAO;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Documents.Excel;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRBangKeSoDuNHex : Form
    {
        private string _subSystemCode;
        List<OverBalanceBook> data = new List<OverBalanceBook>();
        ReportProcedureSDS sp = new ReportProcedureSDS();
        string period = "";
        string account = "";
        string currency = "";
        DateTime FromDate;
        DateTime ToDate;
        public FRBangKeSoDuNHex(DateTime FromDate1, DateTime ToDate1, string account1, string currency1)
        {
            InitializeComponent();
            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\BangKeSoDuNH.rst", path);

            List<OverBalanceBook> data1 = sp.GetBangKeSoDuNganHang(FromDate1, ToDate1, account1, "", currency1, "", true, true);
            string txtThoiGian = (account1 != "" ? "Tài khoản: " + account1 + "; " : "") + (currency1 != null ? "Loại tiền: " + currency1 : null) + "; ";
            period = txtThoiGian + ReportUtils.GetPeriod(FromDate1, ToDate1);
            data = data1;
            account = account1;
            currency = currency1;
            FromDate = FromDate1;
            ToDate = ToDate1;

            configGrid(data);

            if (Utils.isDemo)
                btnExport.Visible = false;
            else
                btnExport.Visible = true;

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (data.Count == 0)
            {
                MSG.Warning("Không có dữ liệu để lên báo cáo");
            }
            else
            {
                var rD = new OverBalanceBookDetail();
                string txtThoiGian = (account != "" ? "Tài khoản: " + account + "; " : "") + (currency != null ? "Loại tiền: " + currency : null) + "; ";
                rD.Period = txtThoiGian + ReportUtils.GetPeriod(FromDate, ToDate);
                var f = new ReportForm<OverBalanceBook>(fileReportSlot1, _subSystemCode);
                f.AddDatasource("DBBangKeSoDu", data, true);
                f.AddDatasource("Detail", rD);
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void reportManager1_GetReportParameter(object sender, PerpetuumSoft.Reporting.Components.GetReportParameterEventArgs e)
        {
            e.Parameters["CurrencyID"].Value = currency;
        }

        private void configGrid(List<OverBalanceBook> lst)
        {

            uGrid.DataSource = lst;
            Utils.ConfigGrid(uGrid, ConstDatabase.OverBalanceBook_TableName);
            uGrid.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;

            GridLayout();

            Utils.AddSumColumn(uGrid, "BankAccount", false);
            Utils.AddSumColumn(uGrid, "BankName", false);
            Utils.AddSumColumn(uGrid, "BankBranchName", false);

            uGrid.DisplayLayout.Bands[0].Summaries[1].DisplayFormat = " ";        
            uGrid.DisplayLayout.Bands[0].Summaries[2].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[3].DisplayFormat = " ";

            uGrid.DisplayLayout.Bands[0].Columns["RowNum"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["RowNum"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["RowNum"].Width = 100;
            uGrid.DisplayLayout.Bands[0].Columns["RowNum"].CellAppearance.TextHAlign = HAlign.Center;

            uGrid.DisplayLayout.Bands[0].Columns["BankAccount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["BankAccount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["BankAccount"].Width = 120;
            uGrid.DisplayLayout.Bands[0].Columns["BankAccount"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["BankName"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["BankName"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["BankName"].Width = 220;
            uGrid.DisplayLayout.Bands[0].Columns["BankName"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["BankBranchName"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["BankBranchName"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["BankBranchName"].Width = 180;
            uGrid.DisplayLayout.Bands[0].Columns["BankBranchName"].CellAppearance.TextVAlign = VAlign.Middle;

            if (currency == "VND")
            {
                uGrid.DisplayLayout.Bands[0].Columns["OpenAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                uGrid.DisplayLayout.Bands[0].Columns["OpenAmount"].Header.Appearance.BorderColor = Color.Black;
                uGrid.DisplayLayout.Bands[0].Columns["OpenAmount"].Width = 200;
                uGrid.DisplayLayout.Bands[0].Columns["OpenAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGrid.DisplayLayout.Bands[0].Columns["OpenAmount"].CellAppearance.TextVAlign = VAlign.Middle;

                uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"].Header.Appearance.BorderColor = Color.Black;
                uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"].Width = 200;
                uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"].CellAppearance.TextVAlign = VAlign.Middle;

                uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"].Header.Appearance.BorderColor = Color.Black;
                uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"].Width = 200;
                uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"].CellAppearance.TextVAlign = VAlign.Middle;

                uGrid.DisplayLayout.Bands[0].Columns["CloseAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                uGrid.DisplayLayout.Bands[0].Columns["CloseAmount"].Header.Appearance.BorderColor = Color.Black;
                uGrid.DisplayLayout.Bands[0].Columns["CloseAmount"].Width = 200;
                uGrid.DisplayLayout.Bands[0].Columns["CloseAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGrid.DisplayLayout.Bands[0].Columns["CloseAmount"].CellAppearance.TextVAlign = VAlign.Middle;

                Utils.AddSumColumn1(lst.Where(n => n.OpenAmount != null).Sum(n => n.OpenAmount), uGrid, "OpenAmount", false, constDatabaseFormat: 1);
                Utils.AddSumColumn1(lst.Where(n => n.DebitAmount != null).Sum(n => n.DebitAmount), uGrid, "DebitAmount", false, constDatabaseFormat: 1);
                Utils.AddSumColumn1(lst.Where(n => n.CreditAmount != null).Sum(n => n.CreditAmount), uGrid, "CreditAmount", false, constDatabaseFormat: 1);
                Utils.AddSumColumn1(lst.Where(n => n.CloseAmount != null).Sum(n => n.CloseAmount), uGrid, "CloseAmount", false, constDatabaseFormat: 1);
            }
            else
            {
                uGrid.DisplayLayout.Bands[0].Columns["OpenAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                uGrid.DisplayLayout.Bands[0].Columns["OpenAmount"].Header.Appearance.BorderColor = Color.Black;
                uGrid.DisplayLayout.Bands[0].Columns["OpenAmount"].Width = 200;
                uGrid.DisplayLayout.Bands[0].Columns["OpenAmount"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                uGrid.DisplayLayout.Bands[0].Columns["OpenAmount"].CellAppearance.TextVAlign = VAlign.Middle;

                uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"].Header.Appearance.BorderColor = Color.Black;
                uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"].Width = 200;
                uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"].CellAppearance.TextVAlign = VAlign.Middle;

                uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"].Header.Appearance.BorderColor = Color.Black;
                uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"].Width = 200;
                uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"].CellAppearance.TextVAlign = VAlign.Middle;

                uGrid.DisplayLayout.Bands[0].Columns["CloseAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                uGrid.DisplayLayout.Bands[0].Columns["CloseAmount"].Header.Appearance.BorderColor = Color.Black;
                uGrid.DisplayLayout.Bands[0].Columns["CloseAmount"].Width = 200;
                uGrid.DisplayLayout.Bands[0].Columns["CloseAmount"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                uGrid.DisplayLayout.Bands[0].Columns["CloseAmount"].CellAppearance.TextVAlign = VAlign.Middle;

                Utils.AddSumColumn1(lst.Where(n => n.OpenAmount != null).Sum(n => n.OpenAmount), uGrid, "OpenAmount", false, constDatabaseFormat: 2);
                Utils.AddSumColumn1(lst.Where(n => n.DebitAmount != null).Sum(n => n.DebitAmount), uGrid, "DebitAmount", false, constDatabaseFormat: 2);
                Utils.AddSumColumn1(lst.Where(n => n.CreditAmount != null).Sum(n => n.CreditAmount), uGrid, "CreditAmount", false, constDatabaseFormat: 2);
                Utils.AddSumColumn1(lst.Where(n => n.CloseAmount != null).Sum(n => n.CloseAmount), uGrid, "CloseAmount", false, constDatabaseFormat: 2);                
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog
            {
                Title = "Chọn nơi sao lưu ",
                Filter = "File (*.xls)|*.xls",
                InitialDirectory = "@C:\\",
                FileName = "bangkesodunganhang.xls"
            };
            if (save.ShowDialog() == DialogResult.OK)
            {
                string duongdan = save.FileName;
                try
                {
                    ultraGridExcelExporter1.Export(uGrid, duongdan);
                    if (MSG.Question("Kết xuất thành công, Bạn có muốn mở file vừa kết xuất không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(save.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
                catch
                {
                    MSG.Warning("Lỗi khi kết xuất !");
                }
            }
        }

        private void GridLayout()
        {
            UltraGridBand parentBand = this.uGrid.DisplayLayout.Bands[0];
            parentBand.RowLayoutStyle = RowLayoutStyle.GroupLayout;

            UltraGridGroup parentBandGroupPeriod;
            if (parentBand.Groups.Exists("ParentBandGroupPeriod"))
            {
                parentBandGroupPeriod = parentBand.Groups["ParentBandGroupPeriod"];
                parentBandGroupPeriod.Header.Caption = period;
            }
            else
                parentBandGroupPeriod = parentBand.Groups.Add("ParentBandGroupPeriod", period);
            parentBandGroupPeriod.Header.Appearance.FontData.Italic = DefaultableBoolean.True;
            parentBandGroupPeriod.Header.Appearance.FontData.SizeInPoints = 10;
            parentBandGroupPeriod.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupPeriod.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPeriod.Header.Appearance.BorderColor = Color.Black;

            UltraGridGroup parentBandGroupTitle;
            if (parentBand.Groups.Exists("ParentBandGroupTitle"))
                parentBandGroupTitle = parentBand.Groups["ParentBandGroupTitle"];
            else
                parentBandGroupTitle = parentBand.Groups.Add("ParentBandGroupTitle", "BẢNG KÊ SỐ DƯ NGÂN HÀNG");
            parentBandGroupTitle.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroupTitle.Header.Appearance.FontData.SizeInPoints = 12;
            parentBandGroupTitle.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupTitle.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupTitle.Header.Appearance.BorderColor = Color.Black;

            parentBandGroupPeriod.RowLayoutGroupInfo.ParentGroup = parentBandGroupTitle;

            parentBand.Columns["RowNum"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["RowNum"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["RowNum"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["RowNum"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["RowNum"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["RowNum"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["BankAccount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["BankAccount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["BankAccount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["BankAccount"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["BankAccount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["BankAccount"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["BankName"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["BankName"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["BankName"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["BankName"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["BankName"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["BankName"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["BankBranchName"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["BankBranchName"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["BankBranchName"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["BankBranchName"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["BankBranchName"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["BankBranchName"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["OpenAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["OpenAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["OpenAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["OpenAmount"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["OpenAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["OpenAmount"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["DebitAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["DebitAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["DebitAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["DebitAmount"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["DebitAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["DebitAmount"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["CreditAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["CreditAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["CreditAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["CreditAmount"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["CreditAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["CreditAmount"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["CloseAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["CloseAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["CloseAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["CloseAmount"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["CloseAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["CloseAmount"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["RowNum"].RowLayoutColumnInfo.OriginX = 0;
            parentBand.Columns["RowNum"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["RowNum"].RowLayoutColumnInfo.SpanX = 100;
            parentBand.Columns["RowNum"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["BankAccount"].RowLayoutColumnInfo.OriginX = 100;
            parentBand.Columns["BankAccount"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["BankAccount"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["BankAccount"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["BankName"].RowLayoutColumnInfo.OriginX = 220;
            parentBand.Columns["BankName"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["BankName"].RowLayoutColumnInfo.SpanX = 220;
            parentBand.Columns["BankName"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["BankBranchName"].RowLayoutColumnInfo.OriginX = 440;
            parentBand.Columns["BankBranchName"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["BankBranchName"].RowLayoutColumnInfo.SpanX = 180;
            parentBand.Columns["BankBranchName"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["OpenAmount"].RowLayoutColumnInfo.OriginX = 620;
            parentBand.Columns["OpenAmount"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["OpenAmount"].RowLayoutColumnInfo.SpanX = 200;
            parentBand.Columns["OpenAmount"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["DebitAmount"].RowLayoutColumnInfo.OriginX = 820;
            parentBand.Columns["DebitAmount"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["DebitAmount"].RowLayoutColumnInfo.SpanX = 200;
            parentBand.Columns["DebitAmount"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["CreditAmount"].RowLayoutColumnInfo.OriginX = 1020;
            parentBand.Columns["CreditAmount"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["CreditAmount"].RowLayoutColumnInfo.SpanX = 200;
            parentBand.Columns["CreditAmount"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["CloseAmount"].RowLayoutColumnInfo.OriginX = 1220;
            parentBand.Columns["CloseAmount"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["CloseAmount"].RowLayoutColumnInfo.SpanX = 200;
            parentBand.Columns["CloseAmount"].RowLayoutColumnInfo.SpanY = 32;
        }

        private void uGrid_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            UltraGridCell cell = e.Row.Cells[0];
            cell.Row.Height = 25;

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("DebitAmount"))
            {
                if (cell.Row.Cells["DebitAmount"].Value != null && (decimal)cell.Row.Cells["DebitAmount"].Value == 0)
                    cell.Row.Cells["DebitAmount"].Value = null;
                if ((cell.Row.Cells["DebitAmount"].Value != null && (decimal)cell.Row.Cells["DebitAmount"].Value < 0))
                    cell.Row.Cells["DebitAmount"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("CreditAmount"))
            {
                if (cell.Row.Cells["CreditAmount"].Value != null && (decimal)cell.Row.Cells["CreditAmount"].Value == 0)
                    cell.Row.Cells["CreditAmount"].Value = null;
                if ((cell.Row.Cells["CreditAmount"].Value != null && (decimal)cell.Row.Cells["CreditAmount"].Value < 0))
                    cell.Row.Cells["CreditAmount"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("CloseAmount"))
            {
                if (cell.Row.Cells["CloseAmount"].Value != null && (decimal)cell.Row.Cells["CloseAmount"].Value == 0)
                    cell.Row.Cells["CloseAmount"].Value = null;
                if ((cell.Row.Cells["CloseAmount"].Value != null && (decimal)cell.Row.Cells["CloseAmount"].Value < 0))
                    cell.Row.Cells["CloseAmount"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("OpenAmount"))
            {
                if (cell.Row.Cells["OpenAmount"].Value != null && (decimal)cell.Row.Cells["OpenAmount"].Value == 0)
                    cell.Row.Cells["OpenAmount"].Value = null;
                if ((cell.Row.Cells["OpenAmount"].Value != null && (decimal)cell.Row.Cells["OpenAmount"].Value < 0))
                    cell.Row.Cells["OpenAmount"].Appearance.ForeColor = Color.Red;
            }
        }

        private void ultraGridExcelExporter1_InitializeColumn(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.InitializeColumnEventArgs e)
        {
            e.ExcelFormatStr = e.Column.Format;
        }

        private void ultraGridExcelExporter1_BeginExport(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.BeginExportEventArgs e)
        {
            foreach (var row in uGrid.Rows)
            {
                foreach(var column in uGrid.DisplayLayout.Bands[0].Columns)
                {
                    e.Rows[row.Index].Cells[column].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                    e.Rows[row.Index].Cells[column].Appearance.BorderColor = Color.Black;
                    e.Rows[row.Index].Cells[column].Column.Width = 200;
                }
            }
            e.Rows.Band.Summaries[0].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[0].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[1].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[1].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[2].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[2].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[3].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[3].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[4].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[4].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[5].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[5].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[6].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[6].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[7].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[7].Appearance.BorderColor = Color.Black;
        }

        private void FRSO_TGNH_SizeChanged(object sender, EventArgs e)
        {
            var state = this.WindowState;
            switch (state)
            {
                case FormWindowState.Maximized:
                    break;
                case FormWindowState.Minimized:
                    break;
                case FormWindowState.Normal:
                    this.Left = (Screen.PrimaryScreen.Bounds.Width - this.Width) / 2;
                    this.Top = (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2;
                    this.Location = new Point((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2, (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
                    break;
                default:
                    break;
            }
        }

        private void ultraGridExcelExporter1_SummaryCellExported(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.SummaryCellExportedEventArgs e)
        {
            Worksheet ws = e.CurrentWorksheet;
            WorksheetRow wsRow = ws.Rows[e.CurrentRowIndex];
            WorksheetCell wsCell = wsRow.Cells[e.CurrentColumnIndex];
            if (!e.Summary.SummaryText.Contains("Số dòng") && !e.Summary.SummaryText.Contains(" "))
            {
                string s = e.Summary.SummaryText.Trim('.');
                if (s.Contains("("))
                    wsCell.Value = -decimal.Parse(s.Trim('(').Trim(')'));
                else
                    wsCell.Value = decimal.Parse(s);

                wsCell.CellFormat.FormatString = "###,###,###,###,##0;(###,###,###,###,##0)";
            }
        }
    }
}
