﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.AppStyling;
using System.IO;
using FX.Core;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinMaskedEdit;
using Infragistics.Win.UltraWinTabs;
using Accounting;
using Accounting.Core.DAO;
using System.Diagnostics;
using Infragistics.Documents.Excel;
using Accounting.Core.Domain.obj.Report;

namespace Accounting.Frm.FReport
{
    public partial class FRTONGHOPCONGNONVex : CustormForm
    {
        private string _subSystemCode;
        List<TongHopCongNoNhanVien> data = new List<TongHopCongNoNhanVien>();
        string period = "";
        DateTime FromDate;
        DateTime ToDate;
        string account;
        string lstAccount;
        ReportProcedureSDS sp = new ReportProcedureSDS();
        public FRTONGHOPCONGNONVex(DateTime FromDate1, DateTime ToDate1, string account1, string lstAccount1)
        {
            InitializeComponent();
            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\TongHopCongNoNhanVien.rst", path);


            List<TongHopCongNoNhanVien> data1 = sp.GetTongHopCongNoNhanVien(FromDate1, ToDate1, account1, lstAccount1);
            
            period = "Từ ngày " + FromDate1.ToString("dd/MM/yyy") + " đến ngày " + ToDate1.ToString("dd/MM/yyy");

            data = data1;
            FromDate = FromDate1;
            ToDate = ToDate1;
            account = account1;
            lstAccount = lstAccount1;

            configGrid(data1);

            if (Utils.isDemo)
                btnExport.Visible = false;
            else
                btnExport.Visible = true;
        }

        private void configGrid(List<TongHopCongNoNhanVien> lst)
        {
            uGrid.DataSource = lst;
            Utils.ConfigGrid(uGrid, ConstDatabase.TONGHOPCONGNONV_TableName);
            uGrid.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;

            foreach (var item in lst)
            {
                if (item.NoDK != null && item.NoPS != null && item.CoPS != null && item.CoDK != null)
                {
                    decimal CK = (decimal)item.NoDK - (decimal)item.CoDK + (decimal)item.NoPS - (decimal)item.CoPS;
                    if (CK > 0)
                    {
                        item.NoCK = CK;
                    }
                    else if (CK < 0)
                    {
                        item.CoCK = -CK;
                    }
                }
            }

            Utils.AddSumColumn(uGrid, "AccountingObjectName", false);
            Utils.AddSumColumn(uGrid, "TK", false);

            uGrid.DisplayLayout.Bands[0].Summaries[1].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[2].DisplayFormat = " ";

            GridLayout();

            uGrid.DisplayLayout.Bands[0].Columns["NoDK"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["CoDK"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["NoPS"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["CoPS"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["NoCK"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["CoCK"].FormatNumberic(ConstDatabase.Format_TienVND);

            uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectCode"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectCode"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectCode"].Width = 120;
            uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectCode"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectName"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectName"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectName"].Width = 200;
            uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectName"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["TK"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["TK"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["TK"].Width = 100;
            uGrid.DisplayLayout.Bands[0].Columns["TK"].CellAppearance.TextVAlign = VAlign.Middle;
            uGrid.DisplayLayout.Bands[0].Columns["TK"].CellAppearance.TextHAlign = HAlign.Center;

            uGrid.DisplayLayout.Bands[0].Columns["NoDK"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["NoDK"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["NoDK"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["NoDK"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["CoDK"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["CoDK"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["CoDK"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["CoDK"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["NoPS"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["NoPS"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["NoPS"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["NoPS"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["CoPS"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["CoPS"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["CoPS"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["CoPS"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["NoCK"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["NoCK"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["NoCK"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["NoCK"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["CoCK"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["CoCK"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["CoCK"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["CoCK"].CellAppearance.TextVAlign = VAlign.Middle;

            Utils.AddSumColumn1(lst.Where(p => p.NoDK != null).Sum(n => n.NoDK), uGrid, "NoDK", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(p => p.CoDK != null).Sum(n => n.CoDK), uGrid, "CoDK", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(p => p.NoPS != null).Sum(n => n.NoPS), uGrid, "NoPS", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(p => p.CoPS != null).Sum(n => n.CoPS), uGrid, "CoPS", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(p => p.NoCK != null).Sum(n => n.NoCK), uGrid, "NoCK", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(p => p.CoCK != null).Sum(n => n.CoCK), uGrid, "CoCK", false, constDatabaseFormat: 1);

        }
        private void GanTKDuNoTKDuCo(List<TongHopCongNoNhanVien> list)
        {
            foreach (var item in list)
            {
                int? AccountGroupKind = Utils.ListAccount.FirstOrDefault(n => n.AccountNumber == item.TK).AccountGroupKind;
                if (AccountGroupKind == null) { continue; }
                if (AccountGroupKind == 0)
                {
                    if (item.CoCK != null && item.CoCK > 0)
                    {
                        item.NoCK = -item.CoCK;
                        item.CoCK = 0;
                    }
                }
                if (AccountGroupKind == 1)
                {
                    if (item.NoCK!= null &&  item.NoCK > 0)
                    {
                        item.CoCK = -item.NoCK;
                        item.NoCK = 0;
                    }
                }
                if (AccountGroupKind == 3)
                {
                    if (item.TK[0].ToInt() == 5)
                    {
                        if (item.NoCK != null && item.NoCK > 0)
                        {
                            item.CoCK = -item.NoCK;
                            item.NoCK = 0;
                        }
                    }
                    if (item.TK[0].ToInt() == 7)
                    {
                        if (item.NoCK != null && item.NoCK > 0)
                        {
                            item.CoCK = -item.NoCK;
                            item.NoCK = 0;
                        }
                    }
                    if (item.TK[0].ToInt() == 6)
                    {
                        if (item.CoCK != null && item.CoCK > 0)
                        {
                            item.NoCK = -item.CoCK;
                            item.CoCK = 0;
                        }
                    }
                    if (item.TK[0].ToInt() == 8)
                    {
                        if (item.CoCK != null && item.CoCK > 0)
                        {
                            item.NoCK = -item.CoCK;
                            item.CoCK = 0;
                        }
                    }
                }

            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (data.Count == 0)
            {
                MSG.Warning("Không có dữ liệu để xem báo cáo");
                return;
            }
            else
            {
                foreach (var item in data)
                {
                    if (item.NoDK != null && item.NoPS != null && item.CoPS != null && item.CoDK != null)
                    {
                        decimal CK = (decimal)item.NoDK - (decimal)item.CoDK + (decimal)item.NoPS - (decimal)item.CoPS;
                        if (CK > 0)
                        {
                            item.NoCK = CK;
                        }
                        else if (CK < 0)
                        {
                            item.CoCK = -CK;
                        }
                    }
                }

                var rD = new SoTheoDoiThanhToanNgoaiTe_period();
                rD.Period = "Từ ngày " + FromDate.ToString("dd/MM/yyy") + " đến ngày " + ToDate.ToString("dd/MM/yyy");
                GanTKDuNoTKDuCo(data);
                var f = new ReportForm<TongHopCongNoNhanVien>(fileReportSlot1, _subSystemCode);
                f.AddDatasource("Period", rD);
                f.AddDatasource("Data", data);
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }       
        private void btnExport_Click(object sender, EventArgs e)
        {

            SaveFileDialog save = new SaveFileDialog
            {
                Title = "Chọn nơi sao lưu ",
                Filter = "File (*.xls)|*.xls",
                InitialDirectory = "@C:\\",
                FileName = "tonghopcongnonhanvien.xls"
            };
            if (save.ShowDialog() == DialogResult.OK)
            {
                string duongdan = save.FileName;
                try
                {
                    ultraGridExcelExporter1.Export(uGrid, duongdan);
                    if (MSG.Question("Kết xuất thành công, Bạn có muốn mở file vừa kết xuất không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(save.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
                catch
                {
                    MSG.Warning("Lỗi khi kết xuất !");
                }
            }
        }
        private void GridLayout()
        {
            UltraGridBand parentBand = this.uGrid.DisplayLayout.Bands[0];
            parentBand.RowLayoutStyle = RowLayoutStyle.GroupLayout;

            UltraGridGroup parentBandGroupDK;
            if (parentBand.Groups.Exists("ParentBandGroupDK"))
                parentBandGroupDK = parentBand.Groups["ParentBandGroupDK"];
            else
                parentBandGroupDK = parentBand.Groups.Add("ParentBandGroupDK", "Đầu kỳ");
            parentBand.Columns["NoDK"].RowLayoutColumnInfo.ParentGroup = parentBandGroupDK;
            parentBand.Columns["NoDK"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["NoDK"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["NoDK"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["NoDK"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["NoDK"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["CoDK"].RowLayoutColumnInfo.ParentGroup = parentBandGroupDK;
            parentBand.Columns["CoDK"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["CoDK"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["CoDK"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["CoDK"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["CoDK"].Header.Appearance.BorderColor = Color.Black;

            UltraGridGroup parentBandGroupPS;
            if (parentBand.Groups.Exists("ParentBandGroupPS"))
                parentBandGroupPS = parentBand.Groups["ParentBandGroupPS"];
            else
                parentBandGroupPS = parentBand.Groups.Add("ParentBandGroupPS", "Phát sinh");
            parentBand.Columns["NoPS"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPS;
            parentBand.Columns["NoPS"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["NoPS"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["NoPS"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["NoPS"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["NoPS"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["CoPS"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPS;
            parentBand.Columns["CoPS"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["CoPS"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["CoPS"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["CoPS"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["CoPS"].Header.Appearance.BorderColor = Color.Black;

            UltraGridGroup parentBandGroupCK;
            if (parentBand.Groups.Exists("ParentBandGroupCK"))
                parentBandGroupCK = parentBand.Groups["ParentBandGroupCK"];
            else
                parentBandGroupCK = parentBand.Groups.Add("ParentBandGroupCK", "Cuối kỳ");
            parentBand.Columns["NoCK"].RowLayoutColumnInfo.ParentGroup = parentBandGroupCK;
            parentBand.Columns["NoCK"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["NoCK"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["NoCK"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["NoCK"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["NoCK"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["CoCK"].RowLayoutColumnInfo.ParentGroup = parentBandGroupCK;
            parentBand.Columns["CoCK"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["CoCK"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["CoCK"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["CoCK"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["CoCK"].Header.Appearance.BorderColor = Color.Black;

            UltraGridGroup parentBandGroupPeriod;
            if (parentBand.Groups.Exists("ParentBandGroupPeriod"))
            {
                parentBandGroupPeriod = parentBand.Groups["ParentBandGroupPeriod"];
                parentBandGroupPeriod.Header.Caption = period;
            }
            else
                parentBandGroupPeriod = parentBand.Groups.Add("ParentBandGroupPeriod", period);
            parentBandGroupPeriod.Header.Appearance.FontData.Italic = DefaultableBoolean.True;
            parentBandGroupPeriod.Header.Appearance.FontData.SizeInPoints = 10;
            parentBandGroupPeriod.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupPeriod.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPeriod.Header.Appearance.BorderColor = Color.Black;

            UltraGridGroup parentBandGroupTitle;
            if (parentBand.Groups.Exists("ParentBandGroupTitle"))
                parentBandGroupTitle = parentBand.Groups["ParentBandGroupTitle"];
            else
                parentBandGroupTitle = parentBand.Groups.Add("ParentBandGroupTitle", "TỔNG HỢP CÔNG NỢ NHÂN VIÊN");
            parentBandGroupTitle.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroupTitle.Header.Appearance.FontData.SizeInPoints = 12;
            parentBandGroupTitle.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupTitle.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupTitle.Header.Appearance.BorderColor = Color.Black;


            parentBandGroupDK.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            parentBandGroupDK.Header.Appearance.FontData.SizeInPoints = 9;
            parentBandGroupDK.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupDK.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupDK.Header.Appearance.BorderColor = Color.Black;
            parentBandGroupDK.Header.Appearance.BackColorAlpha = Alpha.Default;

            parentBandGroupPS.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            parentBandGroupPS.Header.Appearance.FontData.SizeInPoints = 9;
            parentBandGroupPS.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupPS.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPS.Header.Appearance.BorderColor = Color.Black;
            parentBandGroupPS.Header.Appearance.BackColorAlpha = Alpha.Default;

            parentBandGroupCK.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            parentBandGroupCK.Header.Appearance.FontData.SizeInPoints = 9;
            parentBandGroupCK.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupCK.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupCK.Header.Appearance.BorderColor = Color.Black;
            parentBandGroupCK.Header.Appearance.BackColorAlpha = Alpha.Default;

            parentBandGroupPeriod.RowLayoutGroupInfo.ParentGroup = parentBandGroupTitle;

            parentBand.Columns["AccountingObjectCode"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["AccountingObjectCode"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["AccountingObjectCode"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["AccountingObjectCode"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["AccountingObjectCode"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["AccountingObjectCode"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["AccountingObjectName"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["AccountingObjectName"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["AccountingObjectName"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["AccountingObjectName"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["AccountingObjectName"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["AccountingObjectName"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["TK"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["TK"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["TK"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["TK"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["TK"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["TK"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["AccountingObjectCode"].RowLayoutColumnInfo.OriginX = 0;
            parentBand.Columns["AccountingObjectCode"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["AccountingObjectCode"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["AccountingObjectCode"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["AccountingObjectName"].RowLayoutColumnInfo.OriginX = 120;
            parentBand.Columns["AccountingObjectName"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["AccountingObjectName"].RowLayoutColumnInfo.SpanX = 200;
            parentBand.Columns["AccountingObjectName"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["TK"].RowLayoutColumnInfo.OriginX = 320;
            parentBand.Columns["TK"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["TK"].RowLayoutColumnInfo.SpanX = 100;
            parentBand.Columns["TK"].RowLayoutColumnInfo.SpanY = 32;

            parentBandGroupDK.RowLayoutGroupInfo.OriginX = 420;
            parentBandGroupDK.RowLayoutGroupInfo.OriginY = 0;
            parentBandGroupDK.RowLayoutGroupInfo.SpanX = 500;
            parentBandGroupDK.RowLayoutGroupInfo.SpanY = 32;

            parentBandGroupPS.RowLayoutGroupInfo.OriginX = 920;
            parentBandGroupPS.RowLayoutGroupInfo.OriginY = 0;
            parentBandGroupPS.RowLayoutGroupInfo.SpanX = 500;
            parentBandGroupPS.RowLayoutGroupInfo.SpanY = 32;

            parentBandGroupCK.RowLayoutGroupInfo.OriginX = 1420;
            parentBandGroupCK.RowLayoutGroupInfo.OriginY = 0;
            parentBandGroupCK.RowLayoutGroupInfo.SpanX = 500;
            parentBandGroupCK.RowLayoutGroupInfo.SpanY = 32;

        }

        private void ultraGridExcelExporter1_InitializeColumn(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.InitializeColumnEventArgs e)
        {
            e.ExcelFormatStr = e.Column.Format;
        }

        private void ultraGridExcelExporter1_BeginExport(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.BeginExportEventArgs e)
        {
            foreach (var row in uGrid.Rows)
            {
                foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
                {
                    e.Rows[row.Index].Cells[column].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                    e.Rows[row.Index].Cells[column].Appearance.BorderColor = Color.Black;
                    e.Rows[row.Index].Cells[column].Column.Width = 200;
                }
            }
            e.Rows.Band.Summaries[0].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[0].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[1].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[1].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[2].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[2].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[3].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[3].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[4].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[4].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[5].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[5].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[6].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[6].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[7].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[7].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[8].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[8].Appearance.BorderColor = Color.Black;

        }

        private void uGrid_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            UltraGridCell cell = e.Row.Cells[0];
            cell.Row.Height = 25;
            
            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("NoDK"))
            {
                if (cell.Row.Cells["NoDK"].Value != null && (decimal)cell.Row.Cells["NoDK"].Value == 0)
                    cell.Row.Cells["NoDK"].Value = null;
                if ((cell.Row.Cells["NoDK"].Value != null && (decimal)cell.Row.Cells["NoDK"].Value < 0))
                    cell.Row.Cells["NoDK"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("CoDK"))
            {
                if (cell.Row.Cells["CoDK"].Value != null && (decimal)cell.Row.Cells["CoDK"].Value == 0)
                    cell.Row.Cells["CoDK"].Value = null;
                if ((cell.Row.Cells["CoDK"].Value != null && (decimal)cell.Row.Cells["CoDK"].Value < 0))
                    cell.Row.Cells["CoDK"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("NoPS"))
            {
                if (cell.Row.Cells["NoPS"].Value != null && (decimal)cell.Row.Cells["NoPS"].Value == 0)
                    cell.Row.Cells["NoPS"].Value = null;
                if ((cell.Row.Cells["NoPS"].Value != null && (decimal)cell.Row.Cells["NoPS"].Value < 0))
                    cell.Row.Cells["NoPS"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("CoPS"))
            {
                if (cell.Row.Cells["CoPS"].Value != null && (decimal)cell.Row.Cells["CoPS"].Value == 0)
                    cell.Row.Cells["CoPS"].Value = null;
                if ((cell.Row.Cells["CoPS"].Value != null && (decimal)cell.Row.Cells["CoPS"].Value < 0))
                    cell.Row.Cells["CoPS"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("NoCK"))
            {
                if (cell.Row.Cells["NoCK"].Value != null && (decimal)cell.Row.Cells["NoCK"].Value == 0)
                    cell.Row.Cells["NoCK"].Value = null;
                if ((cell.Row.Cells["NoCK"].Value != null && (decimal)cell.Row.Cells["NoCK"].Value < 0))
                    cell.Row.Cells["NoCK"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("CoCK"))
            {
                if (cell.Row.Cells["CoCK"].Value != null && (decimal)cell.Row.Cells["CoCK"].Value == 0)
                    cell.Row.Cells["CoCK"].Value = null;
                if ((cell.Row.Cells["CoCK"].Value != null && (decimal)cell.Row.Cells["CoCK"].Value < 0))
                    cell.Row.Cells["CoCK"].Appearance.ForeColor = Color.Red;
            }

        }

        private void FRBANGCANDOITAIKHOAN_SizeChanged(object sender, EventArgs e)
        {
            var state = this.WindowState;
            switch (state)
            {
                case FormWindowState.Maximized:
                    break;
                case FormWindowState.Minimized:
                    break;
                case FormWindowState.Normal:
                    this.Left = (Screen.PrimaryScreen.Bounds.Width - this.Width) / 2;
                    this.Top = (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2;
                    this.Location = new Point((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2, (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
                    break;
                default:
                    break;
            }
        }

        private void ultraGridExcelExporter1_SummaryCellExported(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.SummaryCellExportedEventArgs e)
        {
            Worksheet ws = e.CurrentWorksheet;
            WorksheetRow wsRow = ws.Rows[e.CurrentRowIndex];
            WorksheetCell wsCell = wsRow.Cells[e.CurrentColumnIndex];
            if (!e.Summary.SummaryText.Contains("Số dòng") && !e.Summary.SummaryText.Contains(" "))
            {
                string s = e.Summary.SummaryText.Trim('.');
                if (s.Contains("("))
                    wsCell.Value = -decimal.Parse(s.Trim('(').Trim(')'));
                else
                    wsCell.Value = decimal.Parse(s);

                wsCell.CellFormat.FormatString = "###,###,###,###,##0;(###,###,###,###,##0)";
            }
        }
    }

}
