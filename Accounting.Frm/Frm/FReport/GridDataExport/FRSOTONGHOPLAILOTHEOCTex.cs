﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.AppStyling;
using System.IO;
using FX.Core;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinMaskedEdit;
using Infragistics.Win.UltraWinTabs;
using Accounting;
using Accounting.Core.DAO;
using System.Diagnostics;
using Infragistics.Documents.Excel;

namespace Accounting.Frm.FReport
{
    public partial class FRSOTONGHOPLAILOTHEOCTex : CustormForm
    {
        private string _subSystemCode;
        List<CTBook> data = new List<CTBook>();
        string period = "";
        DateTime FromDate;
        DateTime ToDate;
        string lstItem;
        ReportProcedureSDS sp = new ReportProcedureSDS();
        public FRSOTONGHOPLAILOTHEOCTex(DateTime FromDate1, DateTime ToDate1, string lstItem1)
        {
            InitializeComponent();
            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\SoTongHopLaiLoTheoCongTrinh.rst", path);

            List<CTBook> data1 = sp.SOTONGHOPCONGTRINH(FromDate1, ToDate1, lstItem1);
            
            period = ReportUtils.GetPeriod(FromDate1, ToDate1);

            data = data1;
            FromDate = FromDate1;
            ToDate = ToDate1;
            lstItem = lstItem1;

            configGrid(data1);

            if (Utils.isDemo)
                btnExport.Visible = false;
            else
                btnExport.Visible = true;
        }

        private void configGrid(List<CTBook> lst)
        {

            uGrid.DataSource = lst;
            Utils.ConfigGrid(uGrid, ConstDatabase.SOTONGHOPCONGTRINH_TableName);
            uGrid.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;

            GridLayout();

            Utils.AddSumColumn(uGrid, "CostSetName", false);
            uGrid.DisplayLayout.Bands[0].Summaries[1].DisplayFormat = " ";

            uGrid.DisplayLayout.Bands[0].Columns["DoanhThu"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["GiamTru"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["ChiPhi"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["GiaVon"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["DoDang"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["LaiLo"].FormatNumberic(ConstDatabase.Format_TienVND);

            uGrid.DisplayLayout.Bands[0].Columns["CostSetCode"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["CostSetCode"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["CostSetCode"].Width = 120;
            uGrid.DisplayLayout.Bands[0].Columns["CostSetCode"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["CostSetName"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["CostSetName"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["CostSetName"].Width = 180;
            uGrid.DisplayLayout.Bands[0].Columns["CostSetName"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["DoanhThu"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["DoanhThu"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["DoanhThu"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["DoanhThu"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["GiamTru"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["GiamTru"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["GiamTru"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["GiamTru"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["ChiPhi"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["ChiPhi"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["ChiPhi"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["ChiPhi"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["GiaVon"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["GiaVon"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["GiaVon"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["GiaVon"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["DoDang"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["DoDang"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["DoDang"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["DoDang"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["LaiLo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["LaiLo"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["LaiLo"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["LaiLo"].CellAppearance.TextVAlign = VAlign.Middle;


            Utils.AddSumColumn1(lst.Where(p => p.DoanhThu != null).Sum(n => n.DoanhThu), uGrid, "DoanhThu", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(p => p.GiamTru != null).Sum(n => n.GiamTru), uGrid, "GiamTru", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(p => p.ChiPhi != null).Sum(n => n.ChiPhi), uGrid, "ChiPhi", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(p => p.GiaVon != null).Sum(n => n.GiaVon), uGrid, "GiaVon", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(p => p.DoDang != null).Sum(n => n.DoDang), uGrid, "DoDang", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(p => p.LaiLo != null).Sum(n => n.LaiLo), uGrid, "LaiLo", false, constDatabaseFormat: 1);

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (data.Count == 0)
                MSG.Warning("Không có dữ liệu");
            else
            {
                var rD = new CTBook_Period();
                rD.Period = ReportUtils.GetPeriod(FromDate, ToDate);
                var f = new ReportForm<CTBook>(fileReportSlot1, _subSystemCode);
                f.AddDatasource("CTBOOK", data, true);
                f.AddDatasource("Detail", rD);
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {

            SaveFileDialog save = new SaveFileDialog
            {
                Title = "Chọn nơi sao lưu ",
                Filter = "File (*.xls)|*.xls",
                InitialDirectory = "@C:\\",
                FileName = "sotonghoplailotheocongtrinh.xls"
            };
            if (save.ShowDialog() == DialogResult.OK)
            {
                string duongdan = save.FileName;
                try
                {
                    ultraGridExcelExporter1.Export(uGrid, duongdan);
                    if (MSG.Question("Kết xuất thành công, Bạn có muốn mở file vừa kết xuất không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(save.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
                catch
                {
                    MSG.Warning("Lỗi khi kết xuất !");
                }
            }
        }
        private void GridLayout()
        {
            UltraGridBand parentBand = this.uGrid.DisplayLayout.Bands[0];
            parentBand.RowLayoutStyle = RowLayoutStyle.GroupLayout;

            UltraGridGroup parentBandGroupPeriod;
            if (parentBand.Groups.Exists("ParentBandGroupPeriod"))
            {
                parentBandGroupPeriod = parentBand.Groups["ParentBandGroupPeriod"];
                parentBandGroupPeriod.Header.Caption = period;
            }
            else
                parentBandGroupPeriod = parentBand.Groups.Add("ParentBandGroupPeriod", period);
            parentBandGroupPeriod.Header.Appearance.FontData.Italic = DefaultableBoolean.True;
            parentBandGroupPeriod.Header.Appearance.FontData.SizeInPoints = 10;
            parentBandGroupPeriod.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupPeriod.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPeriod.Header.Appearance.BorderColor = Color.Black;

            UltraGridGroup parentBandGroupTitle;
            if (parentBand.Groups.Exists("ParentBandGroupTitle"))
                parentBandGroupTitle = parentBand.Groups["ParentBandGroupTitle"];
            else
                parentBandGroupTitle = parentBand.Groups.Add("ParentBandGroupTitle", "SỔ TỔNG HỢP LÃI LỖ THEO CÔNG TRÌNH");
            parentBandGroupTitle.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroupTitle.Header.Appearance.FontData.SizeInPoints = 12;
            parentBandGroupTitle.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupTitle.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupTitle.Header.Appearance.BorderColor = Color.Black;

            parentBandGroupPeriod.RowLayoutGroupInfo.ParentGroup = parentBandGroupTitle;

            parentBand.Columns["CostSetCode"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["CostSetCode"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["CostSetCode"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["CostSetCode"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["CostSetCode"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["CostSetCode"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["CostSetName"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["CostSetName"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["CostSetName"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["CostSetName"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["CostSetName"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["CostSetName"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["DoanhThu"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["DoanhThu"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["DoanhThu"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["DoanhThu"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["DoanhThu"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["DoanhThu"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["GiamTru"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["GiamTru"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GiamTru"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["GiamTru"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["GiamTru"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GiamTru"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["ChiPhi"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["ChiPhi"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ChiPhi"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["ChiPhi"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["ChiPhi"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ChiPhi"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["GiaVon"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["GiaVon"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GiaVon"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["GiaVon"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["GiaVon"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GiaVon"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["DoDang"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["DoDang"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["DoDang"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["DoDang"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["DoDang"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["DoDang"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["LaiLo"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["LaiLo"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["LaiLo"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["LaiLo"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["LaiLo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["LaiLo"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["CostSetCode"].RowLayoutColumnInfo.OriginX = 0;
            parentBand.Columns["CostSetCode"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["CostSetCode"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["CostSetCode"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["CostSetName"].RowLayoutColumnInfo.OriginX = 120;
            parentBand.Columns["CostSetName"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["CostSetName"].RowLayoutColumnInfo.SpanX = 180;
            parentBand.Columns["CostSetName"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["DoanhThu"].RowLayoutColumnInfo.OriginX = 300;
            parentBand.Columns["DoanhThu"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["DoanhThu"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["DoanhThu"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["GiamTru"].RowLayoutColumnInfo.OriginX = 550;
            parentBand.Columns["GiamTru"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["GiamTru"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["GiamTru"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["ChiPhi"].RowLayoutColumnInfo.OriginX = 800;
            parentBand.Columns["ChiPhi"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["ChiPhi"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["ChiPhi"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["GiaVon"].RowLayoutColumnInfo.OriginX = 1050;
            parentBand.Columns["GiaVon"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["GiaVon"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["GiaVon"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["DoDang"].RowLayoutColumnInfo.OriginX = 1300;
            parentBand.Columns["DoDang"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["DoDang"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["DoDang"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["LaiLo"].RowLayoutColumnInfo.OriginX = 1550;
            parentBand.Columns["LaiLo"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["LaiLo"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["LaiLo"].RowLayoutColumnInfo.SpanY = 32;

        }

        private void ultraGridExcelExporter1_InitializeColumn(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.InitializeColumnEventArgs e)
        {
            e.ExcelFormatStr = e.Column.Format;
        }

        private void ultraGridExcelExporter1_BeginExport(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.BeginExportEventArgs e)
        {
            foreach (var row in uGrid.Rows)
            {
                foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
                {
                    e.Rows[row.Index].Cells[column].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                    e.Rows[row.Index].Cells[column].Appearance.BorderColor = Color.Black;
                    e.Rows[row.Index].Cells[column].Column.Width = 200;
                }
            }
            e.Rows.Band.Summaries[0].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[0].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[1].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[1].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[2].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[2].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[3].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[3].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[4].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[4].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[5].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[5].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[6].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[6].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[7].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[7].Appearance.BorderColor = Color.Black;
        }

        private void uGrid_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            UltraGridCell cell = e.Row.Cells[0];
            cell.Row.Height = 25;

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("DoanhThu"))
            {
                if (cell.Row.Cells["DoanhThu"].Value != null && (decimal)cell.Row.Cells["DoanhThu"].Value == 0)
                    cell.Row.Cells["DoanhThu"].Value = null;
                if ((cell.Row.Cells["DoanhThu"].Value != null && (decimal)cell.Row.Cells["DoanhThu"].Value < 0))
                    cell.Row.Cells["DoanhThu"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("GiamTru"))
            {
                if (cell.Row.Cells["GiamTru"].Value != null && (decimal)cell.Row.Cells["GiamTru"].Value == 0)
                    cell.Row.Cells["GiamTru"].Value = null;
                if ((cell.Row.Cells["GiamTru"].Value != null && (decimal)cell.Row.Cells["GiamTru"].Value < 0))
                    cell.Row.Cells["GiamTru"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("ChiPhi"))
            {
                if (cell.Row.Cells["ChiPhi"].Value != null && (decimal)cell.Row.Cells["ChiPhi"].Value == 0)
                    cell.Row.Cells["ChiPhi"].Value = null;
                if ((cell.Row.Cells["ChiPhi"].Value != null && (decimal)cell.Row.Cells["ChiPhi"].Value < 0))
                    cell.Row.Cells["ChiPhi"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("GiaVon"))
            {
                if (cell.Row.Cells["GiaVon"].Value != null && (decimal)cell.Row.Cells["GiaVon"].Value == 0)
                    cell.Row.Cells["GiaVon"].Value = null;
                if ((cell.Row.Cells["GiaVon"].Value != null && (decimal)cell.Row.Cells["GiaVon"].Value < 0))
                    cell.Row.Cells["GiaVon"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("DoDang"))
            {
                if (cell.Row.Cells["DoDang"].Value != null && (decimal)cell.Row.Cells["DoDang"].Value == 0)
                    cell.Row.Cells["DoDang"].Value = null;
                if ((cell.Row.Cells["DoDang"].Value != null && (decimal)cell.Row.Cells["DoDang"].Value < 0))
                    cell.Row.Cells["DoDang"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("LaiLo"))
            {
                if (cell.Row.Cells["LaiLo"].Value != null && (decimal)cell.Row.Cells["LaiLo"].Value == 0)
                    cell.Row.Cells["LaiLo"].Value = null;
                if ((cell.Row.Cells["LaiLo"].Value != null && (decimal)cell.Row.Cells["LaiLo"].Value < 0))
                    cell.Row.Cells["LaiLo"].Appearance.ForeColor = Color.Red;
            }

        }

        private void FRBANGCANDOITAIKHOAN_SizeChanged(object sender, EventArgs e)
        {
            var state = this.WindowState;
            switch (state)
            {
                case FormWindowState.Maximized:
                    break;
                case FormWindowState.Minimized:
                    break;
                case FormWindowState.Normal:
                    this.Left = (Screen.PrimaryScreen.Bounds.Width - this.Width) / 2;
                    this.Top = (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2;
                    this.Location = new Point((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2, (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
                    break;
                default:
                    break;
            }
        }

        private void ultraGridExcelExporter1_SummaryCellExported(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.SummaryCellExportedEventArgs e)
        {
            Worksheet ws = e.CurrentWorksheet;
            WorksheetRow wsRow = ws.Rows[e.CurrentRowIndex];
            WorksheetCell wsCell = wsRow.Cells[e.CurrentColumnIndex];
            if (!e.Summary.SummaryText.Contains("Số dòng") && !e.Summary.SummaryText.Contains(" "))
            {
                string s = e.Summary.SummaryText.Trim('.');
                if (s.Contains("("))
                    wsCell.Value = -decimal.Parse(s.Trim('(').Trim(')'));
                else
                    wsCell.Value = decimal.Parse(s);

                wsCell.CellFormat.FormatString = "###,###,###,###,##0;(###,###,###,###,##0)";
            }
        }
    }

}
