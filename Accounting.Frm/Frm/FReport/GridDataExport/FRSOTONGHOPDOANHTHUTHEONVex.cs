﻿using Accounting.Core.DAO;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.Report;
using Accounting.TextMessage;
using Infragistics.Documents.Excel;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRSOTONGHOPDOANHTHUTHEONVex : Form
    {
        private string _subSystemCode;
        ReportProcedureSDS sp = new ReportProcedureSDS();
        List<AccountingObjectMaterial_Book> data = new List<AccountingObjectMaterial_Book>();
        string period = "";
        DateTime FromDate;
        DateTime ToDate;
        string lstNV_id_tostring = "";
        string strlstVattu_idSelect = "";
        public FRSOTONGHOPDOANHTHUTHEONVex(DateTime FromDate1, DateTime ToDate1, string lstNV_id_tostring1, string strlstVattu_idSelect1)
        {
            InitializeComponent();
            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(
                 System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\SoTongHopDoanhThuTheoNhanVien.rst", path);

            List<AccountingObjectMaterial_Book> data1 = sp.getTongHopDOANHTHUNV(FromDate1, ToDate1, lstNV_id_tostring1, strlstVattu_idSelect1);
            period = ReportUtils.GetPeriod(FromDate1, ToDate1);

            FromDate = FromDate1;
            ToDate = ToDate1;
            lstNV_id_tostring = lstNV_id_tostring1;
            strlstVattu_idSelect = strlstVattu_idSelect1;
            data = data1;

            configGrid(data1);
            if (Utils.isDemo)
                btnExport.Visible = false;
            else
                btnExport.Visible = true;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            List<AccountingObjectMaterial_Book> data = sp.getTongHopDOANHTHUNV(FromDate, ToDate, lstNV_id_tostring, strlstVattu_idSelect);
            data.ForEach(t =>
            {
                if (t.MaterialGoodsName == null)
                    t.MaterialGoodsName = "";
                else
                    t.MaterialGoodsName = t.MaterialGoodsName;
            });

            data = data.Where(t => t.MaterialGoodsName != "Cộng nhóm" && !t.MaterialGoodsName.Contains("Mã nhân viên: ")).ToList();
            if (data.Count == 0)
                MSG.Warning("Không có dữ liệu");
            else
            {
                var rD = new AccountingObjectMaterial_Period();
                rD.Period = ReportUtils.GetPeriod(FromDate, ToDate);
                var f = new ReportForm<AccountingObjectMaterial_Book>(fileReportSlot1, _subSystemCode);
                f.AddDatasource("DoanhThuNV", data, true);
                f.AddDatasource("Detail", rD);
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog
            {
                Title = "Chọn nơi sao lưu ",
                Filter = "File (*.xls)|*.xls",
                InitialDirectory = "@C:\\",
                FileName = "sotonghopdoanhthutheonv.xls"
            };
            if (save.ShowDialog() == DialogResult.OK)
            {
                string duongdan = save.FileName;
                try
                {
                    ultraGridExcelExporter1.Export(uGrid, duongdan);
                    if (MSG.Question("Kết xuất thành công, Bạn có muốn mở file vừa kết xuất không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(save.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
                catch
                {
                    MSG.Warning("Lỗi khi kết xuất !");
                }
            }
        }     

        private void configGrid(List<AccountingObjectMaterial_Book> lst)
        {
            uGrid.DataSource = lst;
            Utils.ConfigGrid(uGrid, ConstDatabase.Proc_SoTongHopDoanhThuTheoNhanVien_TableName);
            uGrid.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;

            data.ForEach(t =>
            {
                if (t.MaterialGoodsName == null)
                    t.MaterialGoodsName = "";
                else
                    t.MaterialGoodsName = t.MaterialGoodsName;
            });

            Utils.AddSumColumn(uGrid, "MaterialGoodsName", false);
            Utils.AddSumColumn(uGrid, "GhiChu", false);

            uGrid.DisplayLayout.Bands[0].Summaries[1].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[2].DisplayFormat = " ";

            Utils.AddSumColumn1(lst.Where(t => t.MaterialGoodsName != "Cộng nhóm" && t.DoanhSo != null).Sum(t => t.DoanhSo), uGrid, "DoanhSo", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(t => t.MaterialGoodsName != "Cộng nhóm" && t.GiamTruDoanhThu != null).Sum(t => t.GiamTruDoanhThu), uGrid, "GiamTruDoanhThu", false, constDatabaseFormat: 1);

            uGrid.DisplayLayout.Bands[0].Columns["MaterialGoodsCode"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["MaterialGoodsCode"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["MaterialGoodsCode"].Width = 120;
            uGrid.DisplayLayout.Bands[0].Columns["MaterialGoodsCode"].CellAppearance.TextHAlign = HAlign.Left;
            uGrid.DisplayLayout.Bands[0].Columns["MaterialGoodsCode"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["MaterialGoodsName"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["MaterialGoodsName"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["MaterialGoodsName"].CellAppearance.TextHAlign = HAlign.Center;
            uGrid.DisplayLayout.Bands[0].Columns["MaterialGoodsName"].Width = 400;
            uGrid.DisplayLayout.Bands[0].Columns["MaterialGoodsName"].CellAppearance.TextHAlign = HAlign.Left;
            uGrid.DisplayLayout.Bands[0].Columns["MaterialGoodsName"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["DoanhSo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["DoanhSo"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["DoanhSo"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["DoanhSo"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["DoanhSo"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["GiamTruDoanhThu"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["GiamTruDoanhThu"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["GiamTruDoanhThu"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["GiamTruDoanhThu"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["GiamTruDoanhThu"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["GhiChu"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["GhiChu"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["GhiChu"].Width = 180;

            GridLayout();
        }
        private void GridLayout()
        {
            UltraGridBand parentBand = this.uGrid.DisplayLayout.Bands[0];
            parentBand.RowLayoutStyle = RowLayoutStyle.GroupLayout;

            UltraGridGroup parentBandGroupPeriod;
            if (parentBand.Groups.Exists("ParentBandGroupPeriod"))
            {
                parentBandGroupPeriod = parentBand.Groups["ParentBandGroupPeriod"];
                parentBandGroupPeriod.Header.Caption = period;
            }
            else
                parentBandGroupPeriod = parentBand.Groups.Add("ParentBandGroupPeriod", period);
            parentBandGroupPeriod.Header.Appearance.FontData.Italic = DefaultableBoolean.True;
            parentBandGroupPeriod.Header.Appearance.FontData.SizeInPoints = 10;
            parentBandGroupPeriod.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupPeriod.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPeriod.Header.Appearance.BorderColor = Color.Black;

            UltraGridGroup parentBandGroupTitle;
            if (parentBand.Groups.Exists("ParentBandGroupTitle"))
                parentBandGroupTitle = parentBand.Groups["ParentBandGroupTitle"];
            else
                parentBandGroupTitle = parentBand.Groups.Add("ParentBandGroupTitle", "SỔ TỔNG HỢP DOANH THU THEO NHÂN VIÊN");
            parentBandGroupTitle.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroupTitle.Header.Appearance.FontData.SizeInPoints = 12;
            parentBandGroupTitle.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupTitle.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupTitle.Header.Appearance.BorderColor = Color.Black;

            parentBandGroupPeriod.RowLayoutGroupInfo.ParentGroup = parentBandGroupTitle;

            parentBand.Columns["MaterialGoodsCode"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["MaterialGoodsCode"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["MaterialGoodsCode"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["MaterialGoodsCode"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["MaterialGoodsCode"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["MaterialGoodsCode"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["MaterialGoodsName"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["MaterialGoodsName"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["MaterialGoodsName"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["MaterialGoodsName"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["MaterialGoodsName"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["MaterialGoodsName"].Header.Appearance.BorderColor = Color.Black;            

            parentBand.Columns["DoanhSo"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["DoanhSo"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["DoanhSo"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["DoanhSo"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["DoanhSo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["DoanhSo"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["GiamTruDoanhThu"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["GiamTruDoanhThu"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GiamTruDoanhThu"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["GiamTruDoanhThu"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["GiamTruDoanhThu"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GiamTruDoanhThu"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["GhiChu"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["GhiChu"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GhiChu"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["GhiChu"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["GhiChu"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GhiChu"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["MaterialGoodsCode"].RowLayoutColumnInfo.OriginX = 0;
            parentBand.Columns["MaterialGoodsCode"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["MaterialGoodsCode"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["MaterialGoodsCode"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["MaterialGoodsName"].RowLayoutColumnInfo.OriginX = 120;
            parentBand.Columns["MaterialGoodsName"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["MaterialGoodsName"].RowLayoutColumnInfo.SpanX = 400;
            parentBand.Columns["MaterialGoodsName"].RowLayoutColumnInfo.SpanY = 32;           

            parentBand.Columns["DoanhSo"].RowLayoutColumnInfo.OriginX = 520;
            parentBand.Columns["DoanhSo"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["DoanhSo"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["DoanhSo"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["GiamTruDoanhThu"].RowLayoutColumnInfo.OriginX = 770;
            parentBand.Columns["GiamTruDoanhThu"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["GiamTruDoanhThu"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["GiamTruDoanhThu"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["GhiChu"].RowLayoutColumnInfo.OriginX = 1020;
            parentBand.Columns["GhiChu"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["GhiChu"].RowLayoutColumnInfo.SpanX = 150;
            parentBand.Columns["GhiChu"].RowLayoutColumnInfo.SpanY = 32;
        }

        private void uGrid_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            UltraGridCell cell = e.Row.Cells[0];
            cell.Row.Height = 25;

            if(uGrid.DisplayLayout.Bands[0].Columns.Exists("MaterialGoodsName"))
            {
                if(cell.Row.Cells["MaterialGoodsName"].Value.ToString() == "Cộng nhóm" || cell.Row.Cells["MaterialGoodsName"].Value.ToString().Contains("Mã nhân viên: "))
                {
                    cell.Row.Cells["MaterialGoodsName"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    cell.Row.Cells["MaterialGoodsName"].Appearance.TextHAlign = HAlign.Left;
                    cell.Row.Cells["DoanhSo"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    cell.Row.Cells["GiamTruDoanhThu"].Appearance.FontData.Bold = DefaultableBoolean.True;
                }
            }
            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("DoanhSo"))
            {
                if (cell.Row.Cells["DoanhSo"].Value != null && (decimal)cell.Row.Cells["DoanhSo"].Value == 0)
                    cell.Row.Cells["DoanhSo"].Value = null;
                if ((cell.Row.Cells["DoanhSo"].Value != null && (decimal)cell.Row.Cells["DoanhSo"].Value < 0))
                    cell.Row.Cells["DoanhSo"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("GiamTruDoanhThu"))
            {
                if (cell.Row.Cells["GiamTruDoanhThu"].Value != null && (decimal)cell.Row.Cells["GiamTruDoanhThu"].Value == 0)
                    cell.Row.Cells["GiamTruDoanhThu"].Value = null;
                if ((cell.Row.Cells["GiamTruDoanhThu"].Value != null && (decimal)cell.Row.Cells["GiamTruDoanhThu"].Value < 0))
                    cell.Row.Cells["GiamTruDoanhThu"].Appearance.ForeColor = Color.Red;
            }

        }

        private void ultraGridExcelExporter1_InitializeColumn(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.InitializeColumnEventArgs e)
        {
            e.ExcelFormatStr = e.Column.Format;
        }

        private void ultraGridExcelExporter1_BeginExport(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.BeginExportEventArgs e)
        {
            foreach (var row in uGrid.Rows)
            {
                foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
                {
                    e.Rows[row.Index].Cells[column].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                    e.Rows[row.Index].Cells[column].Appearance.BorderColor = Color.Black;
                    e.Rows[row.Index].Cells[column].Column.Width = 200;
                }
            }
            e.Rows.Band.Summaries[0].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[0].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[1].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[1].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[2].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[2].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[3].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[3].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[4].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[4].Appearance.BorderColor = Color.Black;

        }

        private void FRB01aDNN_SizeChanged(object sender, EventArgs e)
        {
            var state = this.WindowState;
            switch (state)
            {
                case FormWindowState.Maximized:
                    break;
                case FormWindowState.Minimized:
                    break;
                case FormWindowState.Normal:
                    this.Left = (Screen.PrimaryScreen.Bounds.Width - this.Width) / 2;
                    this.Top = (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2;
                    this.Location = new Point((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2, (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
                    break;
                default:
                    break;
            }
        }

        private void ultraGridExcelExporter1_SummaryCellExported(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.SummaryCellExportedEventArgs e)
        {
            Worksheet ws = e.CurrentWorksheet;
            WorksheetRow wsRow = ws.Rows[e.CurrentRowIndex];
            WorksheetCell wsCell = wsRow.Cells[e.CurrentColumnIndex];
            if (!e.Summary.SummaryText.Contains("Số dòng") && !e.Summary.SummaryText.Contains(" "))
            {
                string s = e.Summary.SummaryText.Trim('.');
                if (s.Contains("("))
                    wsCell.Value = -decimal.Parse(s.Trim('(').Trim(')'));
                else
                    wsCell.Value = decimal.Parse(s);

                wsCell.CellFormat.FormatString = "###,###,###,###,##0;(###,###,###,###,##0)";
            }
        }
    }
}
