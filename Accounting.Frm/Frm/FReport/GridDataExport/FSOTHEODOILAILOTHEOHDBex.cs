﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.AppStyling;
using System.IO;
using FX.Core;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinMaskedEdit;
using Infragistics.Win.UltraWinTabs;
using Accounting;
using Accounting.Core.DAO;
using System.Diagnostics;
using Accounting.Core.Domain.obj.Report;
using Infragistics.Documents.Excel;

namespace Accounting.Frm.FReport
{
    public partial class FSOTHEODOILAILOTHEOHDBex : CustormForm
    {
        private string _subSystemCode;
        private List<SA_SoTheoDoiLaiLoTheoHopDongBan> data = new List<SA_SoTheoDoiLaiLoTheoHopDongBan>();
        private DateTime FromDate;
        private DateTime ToDate;
        private string period = "";
        string lst_account = "";
        ReportProcedureSDS sp = new ReportProcedureSDS();
        public FSOTHEODOILAILOTHEOHDBex(DateTime FromDate1, DateTime ToDate1, string lst_account1)
        {
            InitializeComponent();
            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\SoTheoDoiLaiLoTheoHopDongBan.rst", path);

            List<SA_SoTheoDoiLaiLoTheoHopDongBan> data1 = sp.GetSoTheoDoiLaiLoTheoHopDongBan(FromDate1, ToDate1, lst_account1);
            period = ReportUtils.GetPeriod(FromDate1, ToDate1);

            data = data1;
            FromDate = FromDate1;
            ToDate = ToDate1;
            lst_account = lst_account1;

            configGrid(data1);
            if (Utils.isDemo)
                btnExport.Visible = false;
            else
                btnExport.Visible = true;
        }

        private void configGrid(List<SA_SoTheoDoiLaiLoTheoHopDongBan> lst)
        {
            uGrid.DataSource = lst;
            Utils.ConfigGrid(uGrid, ConstDatabase.SA_SoTheoDoiLaiLoTheoHopDongBan_TableName); uGrid.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            uGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;

            uGrid.DisplayLayout.Bands[0].Columns["GiaTriHopDong"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["DoanhThuThucTe"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["GiamTruDoanhThu"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["GiaVon"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["LaiLo"].FormatNumberic(ConstDatabase.Format_TienVND);

            Utils.AddSumColumn(uGrid, "SoHopDong", false);
            Utils.AddSumColumn(uGrid, "DoiTuong", false);
            Utils.AddSumColumn(uGrid, "TrichYeu", false);

            uGrid.DisplayLayout.Bands[0].Summaries[1].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[2].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[3].DisplayFormat = " ";

            Utils.AddSumColumn1(lst.Where(n => n.GiaTriHopDong != null).Sum(n => n.GiaTriHopDong), uGrid, "GiaTriHopDong", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.DoanhThuThucTe != null).Sum(n => n.DoanhThuThucTe), uGrid, "DoanhThuThucTe", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.GiamTruDoanhThu != null).Sum(n => n.GiamTruDoanhThu), uGrid, "GiamTruDoanhThu", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.GiaVon != null).Sum(n => n.GiaVon), uGrid, "GiaVon", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.LaiLo != null).Sum(n => n.LaiLo), uGrid, "LaiLo", false, constDatabaseFormat: 1);

            uGrid.DisplayLayout.Bands[0].Columns["NgayKy"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["NgayKy"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["NgayKy"].Width = 120;
            uGrid.DisplayLayout.Bands[0].Columns["NgayKy"].CellAppearance.TextHAlign = HAlign.Center;

            uGrid.DisplayLayout.Bands[0].Columns["SoHopDong"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["SoHopDong"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["SoHopDong"].Width = 120;
            uGrid.DisplayLayout.Bands[0].Columns["SoHopDong"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["DoiTuong"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["DoiTuong"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["DoiTuong"].Width = 150;
            uGrid.DisplayLayout.Bands[0].Columns["DoiTuong"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["TrichYeu"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["TrichYeu"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["TrichYeu"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["TrichYeu"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["GiaTriHopDong"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["GiaTriHopDong"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["GiaTriHopDong"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["GiaTriHopDong"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["DoanhThuThucTe"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["DoanhThuThucTe"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["DoanhThuThucTe"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["DoanhThuThucTe"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["GiamTruDoanhThu"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["GiamTruDoanhThu"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["GiamTruDoanhThu"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["GiamTruDoanhThu"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["GiaVon"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["GiaVon"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["GiaVon"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["GiaVon"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["LaiLo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["LaiLo"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["LaiLo"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["LaiLo"].CellAppearance.TextVAlign = VAlign.Middle;

            GridLayout();

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (data.Count == 0)
                MSG.Warning("Không có dữ liệu");
            else
            {
                var rD = new SA_SoTheoDoiLaiLoTheoHopDongBan_period();
                rD.Period = ReportUtils.GetPeriod(FromDate, ToDate);
                var f = new ReportForm<SA_SoTheoDoiLaiLoTheoHopDongBan>(fileReportSlot1, _subSystemCode);
                f.AddDatasource("DBTHEODOILAILOHDB", data, true);
                f.AddDatasource("Detail", rD);
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }


        private void btnExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog
            {
                Title = "Chọn nơi sao lưu ",
                Filter = "File (*.xls)|*.xls",
                InitialDirectory = "@C:\\",
                FileName = "sotheodoilailotheohopdongban.xls"
            };
            if (save.ShowDialog() == DialogResult.OK)
            {
                string duongdan = save.FileName;
                try
                {
                    ultraGridExcelExporter1.Export(uGrid, duongdan);
                    if (MSG.Question("Kết xuất thành công, Bạn có muốn mở file vừa kết xuất không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(save.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
                catch
                {
                    MSG.Warning("Lỗi khi kết xuất !");
                }
            }
        }
        private void GridLayout()
        {
            UltraGridBand parentBand = this.uGrid.DisplayLayout.Bands[0];
            parentBand.RowLayoutStyle = RowLayoutStyle.GroupLayout;

            UltraGridGroup parentBandGroupPeriod;
            if (parentBand.Groups.Exists("ParentBandGroupPeriod"))
            {
                parentBandGroupPeriod = parentBand.Groups["ParentBandGroupPeriod"];
                parentBandGroupPeriod.Header.Caption = period;
            }
            else
                parentBandGroupPeriod = parentBand.Groups.Add("ParentBandGroupPeriod", period);
            parentBandGroupPeriod.Header.Appearance.FontData.Italic = DefaultableBoolean.True;
            parentBandGroupPeriod.Header.Appearance.FontData.SizeInPoints = 10;
            parentBandGroupPeriod.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupPeriod.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPeriod.Header.Appearance.BorderColor = Color.Black;

            UltraGridGroup parentBandGroupTitle;
            if (parentBand.Groups.Exists("ParentBandGroupTitle"))
                parentBandGroupTitle = parentBand.Groups["ParentBandGroupTitle"];
            else
                parentBandGroupTitle = parentBand.Groups.Add("ParentBandGroupTitle", "SỔ THEO DÕI LÃI LỖ THEO HỢP ĐỒNG BÁN");
            parentBandGroupTitle.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroupTitle.Header.Appearance.FontData.SizeInPoints = 12;
            parentBandGroupTitle.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupTitle.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupTitle.Header.Appearance.BorderColor = Color.Black;

            parentBandGroupPeriod.RowLayoutGroupInfo.ParentGroup = parentBandGroupTitle;

            parentBand.Columns["NgayKy"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["NgayKy"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["NgayKy"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["NgayKy"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["NgayKy"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["NgayKy"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["SoHopDong"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["SoHopDong"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SoHopDong"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["SoHopDong"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["SoHopDong"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SoHopDong"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["DoiTuong"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["DoiTuong"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["DoiTuong"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["DoiTuong"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["DoiTuong"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["DoiTuong"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["TrichYeu"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["TrichYeu"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["TrichYeu"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["TrichYeu"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["TrichYeu"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["TrichYeu"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["GiaTriHopDong"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["GiaTriHopDong"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GiaTriHopDong"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["GiaTriHopDong"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["GiaTriHopDong"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GiaTriHopDong"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["DoanhThuThucTe"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["DoanhThuThucTe"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["DoanhThuThucTe"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["DoanhThuThucTe"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["DoanhThuThucTe"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["DoanhThuThucTe"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["GiamTruDoanhThu"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["GiamTruDoanhThu"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GiamTruDoanhThu"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["GiamTruDoanhThu"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["GiamTruDoanhThu"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GiamTruDoanhThu"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["GiaVon"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["GiaVon"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GiaVon"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["GiaVon"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["GiaVon"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GiaVon"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["LaiLo"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["LaiLo"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["LaiLo"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["LaiLo"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["LaiLo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["LaiLo"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["NgayKy"].RowLayoutColumnInfo.OriginX = 0;
            parentBand.Columns["NgayKy"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["NgayKy"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["NgayKy"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["SoHopDong"].RowLayoutColumnInfo.OriginX = 120;
            parentBand.Columns["SoHopDong"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["SoHopDong"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["SoHopDong"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["DoiTuong"].RowLayoutColumnInfo.OriginX = 240;
            parentBand.Columns["DoiTuong"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["DoiTuong"].RowLayoutColumnInfo.SpanX = 150;
            parentBand.Columns["DoiTuong"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["TrichYeu"].RowLayoutColumnInfo.OriginX = 390;
            parentBand.Columns["TrichYeu"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["TrichYeu"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["TrichYeu"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["GiaTriHopDong"].RowLayoutColumnInfo.OriginX = 640;
            parentBand.Columns["GiaTriHopDong"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["GiaTriHopDong"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["GiaTriHopDong"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["DoanhThuThucTe"].RowLayoutColumnInfo.OriginX = 890;
            parentBand.Columns["DoanhThuThucTe"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["DoanhThuThucTe"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["DoanhThuThucTe"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["GiamTruDoanhThu"].RowLayoutColumnInfo.OriginX = 1140;
            parentBand.Columns["GiamTruDoanhThu"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["GiamTruDoanhThu"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["GiamTruDoanhThu"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["GiaVon"].RowLayoutColumnInfo.OriginX = 1390;
            parentBand.Columns["GiaVon"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["GiaVon"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["GiaVon"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["LaiLo"].RowLayoutColumnInfo.OriginX = 1640;
            parentBand.Columns["LaiLo"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["LaiLo"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["LaiLo"].RowLayoutColumnInfo.SpanY = 32;

        }

        private void ultraGridExcelExporter1_InitializeColumn(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.InitializeColumnEventArgs e)
        {
            e.ExcelFormatStr = e.Column.Format;
        }

        private void ultraGridExcelExporter1_BeginExport(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.BeginExportEventArgs e)
        {
            foreach (var row in uGrid.Rows)
            {
                foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
                {
                    e.Rows[row.Index].Cells[column].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                    e.Rows[row.Index].Cells[column].Appearance.BorderColor = Color.Black;
                    e.Rows[row.Index].Cells[column].Column.Width = 200;
                }
            }
            e.Rows.Band.Summaries[0].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[0].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[1].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[1].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[2].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[2].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[3].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[3].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[4].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[4].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[5].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[5].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[6].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[6].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[7].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[7].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[8].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[8].Appearance.BorderColor = Color.Black;
        }

        private void uGrid_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            UltraGridCell cell = e.Row.Cells[0];
            cell.Row.Height = 25;

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("GiaTriHopDong"))
            {
                if (cell.Row.Cells["GiaTriHopDong"].Value != null && (decimal)cell.Row.Cells["GiaTriHopDong"].Value == 0)
                    cell.Row.Cells["GiaTriHopDong"].Value = null;
                if ((cell.Row.Cells["GiaTriHopDong"].Value != null && (decimal)cell.Row.Cells["GiaTriHopDong"].Value < 0))
                    cell.Row.Cells["GiaTriHopDong"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("DoanhThuThucTe"))
            {
                if (cell.Row.Cells["DoanhThuThucTe"].Value != null && (decimal)cell.Row.Cells["DoanhThuThucTe"].Value == 0)
                    cell.Row.Cells["DoanhThuThucTe"].Value = null;
                if ((cell.Row.Cells["DoanhThuThucTe"].Value != null && (decimal)cell.Row.Cells["DoanhThuThucTe"].Value < 0))
                    cell.Row.Cells["DoanhThuThucTe"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("GiamTruDoanhThu"))
            {
                if (cell.Row.Cells["GiamTruDoanhThu"].Value != null && (decimal)cell.Row.Cells["GiamTruDoanhThu"].Value == 0)
                    cell.Row.Cells["GiamTruDoanhThu"].Value = null;
                if ((cell.Row.Cells["GiamTruDoanhThu"].Value != null && (decimal)cell.Row.Cells["GiamTruDoanhThu"].Value < 0))
                    cell.Row.Cells["GiamTruDoanhThu"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("GiaVon"))
            {
                if (cell.Row.Cells["GiaVon"].Value != null && (decimal)cell.Row.Cells["GiaVon"].Value == 0)
                    cell.Row.Cells["GiaVon"].Value = null;
                if ((cell.Row.Cells["GiaVon"].Value != null && (decimal)cell.Row.Cells["GiaVon"].Value < 0))
                    cell.Row.Cells["GiaVon"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("LaiLo"))
            {
                if (cell.Row.Cells["LaiLo"].Value != null && (decimal)cell.Row.Cells["LaiLo"].Value == 0)
                    cell.Row.Cells["LaiLo"].Value = null;
                if ((cell.Row.Cells["LaiLo"].Value != null && (decimal)cell.Row.Cells["LaiLo"].Value < 0))
                    cell.Row.Cells["LaiLo"].Appearance.ForeColor = Color.Red;
            }

        }

        private void FSOKTCTTIENMATex_SizeChanged(object sender, EventArgs e)
        {
            var state = this.WindowState;
            switch (state)
            {
                case FormWindowState.Maximized:
                    break;
                case FormWindowState.Minimized:
                    break;
                case FormWindowState.Normal:
                    this.Left = (Screen.PrimaryScreen.Bounds.Width - this.Width) / 2;
                    this.Top = (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2;
                    this.Location = new Point((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2, (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
                    break;
                default:
                    break;
            }
        }

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                SA_SoTheoDoiLaiLoTheoHopDongBan temp = (SA_SoTheoDoiLaiLoTheoHopDongBan)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                if (temp != null)
                    editFuntion(temp);

            }
        }

        private void editFuntion(SA_SoTheoDoiLaiLoTheoHopDongBan temp)
        {

            var f = Utils.ViewVoucherSelected1(temp.ContractID, temp.RefType);
            //List<SA_SoTheoDoiLaiLoTheoHopDongBan> data1 = sp.GetSoTheoDoiLaiLoTheoHopDongBan(FromDate, ToDate, lst_account).ToList();
            //configGrid(data1);

        }

        private void ultraGridExcelExporter1_SummaryCellExported(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.SummaryCellExportedEventArgs e)
        {
            Worksheet ws = e.CurrentWorksheet;
            WorksheetRow wsRow = ws.Rows[e.CurrentRowIndex];
            WorksheetCell wsCell = wsRow.Cells[e.CurrentColumnIndex];
            if (!e.Summary.SummaryText.Contains("Số dòng") && !e.Summary.SummaryText.Contains(" "))
            {
                string s = e.Summary.SummaryText.Trim('.');
                if (s.Contains("("))
                    wsCell.Value = -decimal.Parse(s.Trim('(').Trim(')'));
                else
                    wsCell.Value = decimal.Parse(s);

                wsCell.CellFormat.FormatString = "###,###,###,###,##0;(###,###,###,###,##0)";
            }
        }
    }

}
