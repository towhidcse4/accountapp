﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.AppStyling;
using System.IO;
using FX.Core;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinMaskedEdit;
using Infragistics.Win.UltraWinTabs;
using Accounting;
using Accounting.Core.DAO;
using System.Diagnostics;
using Infragistics.Documents.Excel;

namespace Accounting.Frm.FReport
{
    public partial class FSOKTCTTIENMATex : CustormForm
    {
        private string _subSystemCode;
        private List<FU_GetCashDetailBook> data = new List<FU_GetCashDetailBook>();
        private List<string> lstGuids;
        private string CurrencyID;
        private string listTK;
        private string _listTK;
        private DateTime FromDate;
        private DateTime ToDate;
        private string period = "";
        ReportProcedureSDS sp = new ReportProcedureSDS();
        public FSOKTCTTIENMATex(DateTime FromDate1, DateTime ToDate1, string listTK1, string _listTK1, string CurrencyID1, List<string> lstGuids1)
        {
            InitializeComponent();
            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\SoKTCTTienMat.rst", path);

            List<FU_GetCashDetailBook> data1 = sp.GetFU_GetCashDetailBook(FromDate1, ToDate1, _listTK1, "", CurrencyID1).ToList();
            if (lstGuids1.Count() == 1)
                period = "Loại tiền: " + CurrencyID1 + "; " + "Tài khoản: " + listTK1 + "; " + ReportUtils.GetPeriod(FromDate1, ToDate1);
            else
                period = "Loại tiền: " + CurrencyID1 + "; " + ReportUtils.GetPeriod(FromDate1, ToDate1);

            data = data1;
            lstGuids = lstGuids1;
            CurrencyID = CurrencyID1;
            listTK = listTK1;
            _listTK = _listTK1;
            FromDate = FromDate1;
            ToDate = ToDate1;

            configGrid(data);
            if (Utils.isDemo)
                btnExport.Visible = false;
            else
                btnExport.Visible = true;
        }

        private void configGrid(List<FU_GetCashDetailBook> lst)
        {
            uGrid.DataSource = lst;
            Utils.ConfigGrid(uGrid, ConstDatabase.FU_GetCashDetailBook_TableName);
            uGrid.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            uGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;

            lst.ForEach(t =>
            {
                if (t.JournalMemo == null)
                    t.JournalMemo = "";
                else
                    t.JournalMemo = t.JournalMemo;
            });

            var SumSoduton = lst.Where(t => t.JournalMemo == "Số tồn đầu kỳ" && t.ClosingAmount != null).Sum(t => t.ClosingAmount);
            var SumNo = lst.Where(n => n.JournalMemo == "Cộng nhóm" && n.DebitAmount != null).Sum(t => t.DebitAmount);
            var SumCo = lst.Where(n => n.JournalMemo == "Cộng nhóm" && n.CreditAmount != null).Sum(t => t.CreditAmount);
            var SumSodutonOC = lst.Where(t => t.JournalMemo == "Số tồn đầu kỳ" && t.ClosingAmountOC != null).Sum(t => t.ClosingAmountOC);
            var SumNoOC = lst.Where(n => n.JournalMemo == "Cộng nhóm" && n.DebitAmountOC != null).Sum(t => t.DebitAmountOC);
            var SumCoOC = lst.Where(n => n.JournalMemo == "Cộng nhóm" && n.CreditAmountOC != null).Sum(t => t.CreditAmountOC);

            Utils.AddSumColumn(uGrid, "RefDate", false);
            Utils.AddSumColumn(uGrid, "ReceiptRefNo", false);
            Utils.AddSumColumn(uGrid, "PaymentRefNo", false);
            Utils.AddSumColumn(uGrid, "JournalMemo", false);
            Utils.AddSumColumn(uGrid, "AccountNumber", false);
            Utils.AddSumColumn(uGrid, "CorrespondingAccountNumber", false);
            Utils.AddSumColumn(uGrid, "ContactName", false);

            uGrid.DisplayLayout.Bands[0].Summaries[1].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[2].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[3].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[4].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[5].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[6].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[7].DisplayFormat = " ";

            if (CurrencyID == "VND")
            {
                uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGrid.DisplayLayout.Bands[0].Columns["ClosingAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGrid.DisplayLayout.Bands[0].Columns["DebitAmountOC"].Hidden = true;
                uGrid.DisplayLayout.Bands[0].Columns["CreditAmountOC"].Hidden = true;
                uGrid.DisplayLayout.Bands[0].Columns["ClosingAmountOC"].Hidden = true;

                var Total = SumNo - SumCo + SumSoduton;
                Utils.AddSumColumn1(lst.Where(n => n.DebitAmount != null && n.JournalMemo != "Cộng nhóm").Sum(n => n.DebitAmount), uGrid, "DebitAmount", false, constDatabaseFormat: 1);
                Utils.AddSumColumn1(lst.Where(n => n.CreditAmount != null && n.JournalMemo != "Cộng nhóm").Sum(n => n.CreditAmount), uGrid, "CreditAmount", false, constDatabaseFormat: 1);
                Utils.AddSumColumn1(Total, uGrid, "ClosingAmount", false, constDatabaseFormat: 1);

            }
            else
            {
                uGrid.DisplayLayout.Bands[0].Columns["DebitAmountOC"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                uGrid.DisplayLayout.Bands[0].Columns["CreditAmountOC"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                uGrid.DisplayLayout.Bands[0].Columns["ClosingAmountOC"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"].Hidden = true;
                uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"].Hidden = true;
                uGrid.DisplayLayout.Bands[0].Columns["ClosingAmount"].Hidden = true;

                var Total = SumNoOC - SumCoOC + SumSodutonOC;
                Utils.AddSumColumn1(lst.Where(n => n.CreditAmountOC != null && n.JournalMemo != "Cộng nhóm").Sum(n => n.CreditAmountOC), uGrid, "CreditAmountOC", false, constDatabaseFormat: 2);
                Utils.AddSumColumn1(lst.Where(n => n.DebitAmountOC != null && n.JournalMemo != "Cộng nhóm").Sum(n => n.DebitAmountOC), uGrid, "DebitAmountOC", false, constDatabaseFormat: 2);
                Utils.AddSumColumn1(Total, uGrid, "ClosingAmountOC", false, constDatabaseFormat: 2);
            }

            GridLayout();
            
            uGrid.DisplayLayout.Bands[0].Columns["PostedDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["PostedDate"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["PostedDate"].Width = 120;

            uGrid.DisplayLayout.Bands[0].Columns["RefDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["RefDate"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["RefDate"].Width = 120;

            uGrid.DisplayLayout.Bands[0].Columns["ReceiptRefNo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["ReceiptRefNo"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["ReceiptRefNo"].Width = 120;
            uGrid.DisplayLayout.Bands[0].Columns["ReceiptRefNo"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["PaymentRefNo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["PaymentRefNo"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["PaymentRefNo"].Width = 120;
            uGrid.DisplayLayout.Bands[0].Columns["PaymentRefNo"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["JournalMemo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["JournalMemo"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["JournalMemo"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["JournalMemo"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["AccountNumber"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["AccountNumber"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["AccountNumber"].Width = 80;
            uGrid.DisplayLayout.Bands[0].Columns["AccountNumber"].CellAppearance.TextHAlign = HAlign.Center;

            uGrid.DisplayLayout.Bands[0].Columns["CorrespondingAccountNumber"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["CorrespondingAccountNumber"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["CorrespondingAccountNumber"].Width = 80;
            uGrid.DisplayLayout.Bands[0].Columns["CorrespondingAccountNumber"].CellAppearance.TextHAlign = HAlign.Center;

            uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["DebitAmountOC"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["DebitAmountOC"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["DebitAmountOC"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["DebitAmountOC"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["CreditAmountOC"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["CreditAmountOC"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["CreditAmountOC"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["CreditAmountOC"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["ClosingAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["ClosingAmount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["ClosingAmount"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["ClosingAmount"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["ClosingAmountOC"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["ClosingAmountOC"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["ClosingAmountOC"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["ClosingAmountOC"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["ContactName"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["ContactName"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["ContactName"].Width = 150;
            uGrid.DisplayLayout.Bands[0].Columns["ContactName"].CellAppearance.TextVAlign = VAlign.Middle;

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            List<FU_GetCashDetailBook> data = sp.GetFU_GetCashDetailBook(FromDate, ToDate, _listTK, "", CurrencyID).ToList();
            if (data.Count == 0)
            {
                MSG.Warning("Không có dữ liệu để lên báo cáo!");
                return;
            }
            else
            {
                data.ForEach(t =>
                {
                    if (t.JournalMemo == null)
                        t.JournalMemo = "";
                    else
                        t.JournalMemo = t.JournalMemo;
                });

                var SumSoduton = data.Where(t => t.JournalMemo == "Số tồn đầu kỳ").Sum(t => t.ClosingAmount);
                var SumSodutonOC = data.Where(t => t.JournalMemo == "Số tồn đầu kỳ").Sum(t => t.ClosingAmountOC);//add by cuongpv

                var rD = new FU_GetCashDetailBookDetail();
                if (lstGuids.Count() == 1)
                    rD.Period = "Loại tiền: " + CurrencyID + "; " + "Tài khoản: " + listTK + "; " + ReportUtils.GetPeriod(FromDate, ToDate);
                else
                    rD.Period = "Loại tiền: " + CurrencyID + "; " + ReportUtils.GetPeriod(FromDate, ToDate);
                rD.SumTonDK = SumSoduton;
                rD.SumTonDKQC = SumSodutonOC;//add by cuongpv
                data = data.Where(t => !t.JournalMemo.Contains("Cộng nhóm")).ToList();
                var f = new ReportForm<FU_GetCashDetailBook>(fileReportSlot1, _subSystemCode);
                f.AddDatasource("DBSoKTCTQ", data, true);
                f.AddDatasource("Detail", rD);
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }


        private void btnExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog
            {
                Title = "Chọn nơi sao lưu ",
                Filter = "File (*.xls)|*.xls",
                InitialDirectory = "@C:\\",
                FileName = "soketoanchitietquytienmat.xls"
            };
            if (save.ShowDialog() == DialogResult.OK)
            {
                string duongdan = save.FileName;
                try
                {
                    ultraGridExcelExporter1.Export(uGrid, duongdan);
                    if (MSG.Question("Kết xuất thành công, Bạn có muốn mở file vừa kết xuất không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(save.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
                catch (Exception ex)
                {
                    MSG.Warning("Lỗi khi kết xuất !");
                }
            }
        }
        private void GridLayout()
        {
            UltraGridBand parentBand = this.uGrid.DisplayLayout.Bands[0];
            parentBand.RowLayoutStyle = RowLayoutStyle.GroupLayout;

            UltraGridGroup parentBandGroupPeriod;
            if (parentBand.Groups.Exists("ParentBandGroupPeriod"))
            {
                parentBandGroupPeriod = parentBand.Groups["ParentBandGroupPeriod"];
                parentBandGroupPeriod.Header.Caption = period;
            }
            else
                parentBandGroupPeriod = parentBand.Groups.Add("ParentBandGroupPeriod", period);
            parentBandGroupPeriod.Header.Appearance.FontData.Italic = DefaultableBoolean.True;
            parentBandGroupPeriod.Header.Appearance.FontData.SizeInPoints = 10;
            parentBandGroupPeriod.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupPeriod.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPeriod.Header.Appearance.BorderColor = Color.Black;

            UltraGridGroup parentBandGroupTitle;
            if (parentBand.Groups.Exists("ParentBandGroupTitle"))
                parentBandGroupTitle = parentBand.Groups["ParentBandGroupTitle"];
            else
                parentBandGroupTitle = parentBand.Groups.Add("ParentBandGroupTitle", "SỔ KẾ TOÁN CHI TIẾT QUỸ TIỀN MẶT");
            parentBandGroupTitle.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroupTitle.Header.Appearance.FontData.SizeInPoints = 12;
            parentBandGroupTitle.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupTitle.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupTitle.Header.Appearance.BorderColor = Color.Black;

            UltraGridGroup parentBandGroupPS;
            if (parentBand.Groups.Exists("ParentBandGroupPS"))
                parentBandGroupPS = parentBand.Groups["ParentBandGroupPS"];
            else
                parentBandGroupPS = parentBand.Groups.Add("ParentBandGroupPS", "Số phát sinh");
            parentBandGroupPS.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            parentBandGroupPS.Header.Appearance.FontData.SizeInPoints = 9;
            parentBandGroupPS.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupPS.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPS.Header.Appearance.BorderColor = Color.Black;
            parentBandGroupPS.Header.Appearance.BackColorAlpha = Alpha.Default;

            parentBandGroupPeriod.RowLayoutGroupInfo.ParentGroup = parentBandGroupTitle;

            if (CurrencyID == "VND")
            {
                parentBand.Columns["DebitAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPS;
                parentBand.Columns["DebitAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["DebitAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["DebitAmount"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["DebitAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["DebitAmount"].Header.Appearance.BorderColor = Color.Black;

                parentBand.Columns["CreditAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPS;
                parentBand.Columns["CreditAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["CreditAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["CreditAmount"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["CreditAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["CreditAmount"].Header.Appearance.BorderColor = Color.Black;

                parentBand.Columns["ClosingAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
                parentBand.Columns["ClosingAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["ClosingAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["ClosingAmount"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["ClosingAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["ClosingAmount"].Header.Appearance.BorderColor = Color.Black;

            }
            else
            {
                parentBand.Columns["DebitAmountOC"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPS;
                parentBand.Columns["DebitAmountOC"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["DebitAmountOC"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["DebitAmountOC"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["DebitAmountOC"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["DebitAmountOC"].Header.Appearance.BorderColor = Color.Black;

                parentBand.Columns["CreditAmountOC"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPS;
                parentBand.Columns["CreditAmountOC"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["CreditAmountOC"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["CreditAmountOC"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["CreditAmountOC"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["CreditAmountOC"].Header.Appearance.BorderColor = Color.Black;

                parentBand.Columns["ClosingAmountOC"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
                parentBand.Columns["ClosingAmountOC"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["ClosingAmountOC"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["ClosingAmountOC"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["ClosingAmountOC"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["ClosingAmountOC"].Header.Appearance.BorderColor = Color.Black;
            }

            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["PostedDate"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["PostedDate"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["PostedDate"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["PostedDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["PostedDate"].Header.Appearance.BorderColor = Color.Black;


            parentBand.Columns["RefDate"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["RefDate"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["RefDate"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["RefDate"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["RefDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["RefDate"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["ReceiptRefNo"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["ReceiptRefNo"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ReceiptRefNo"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["ReceiptRefNo"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["ReceiptRefNo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ReceiptRefNo"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["PaymentRefNo"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["PaymentRefNo"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["PaymentRefNo"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["PaymentRefNo"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["PaymentRefNo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["PaymentRefNo"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["JournalMemo"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["JournalMemo"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["JournalMemo"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["JournalMemo"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["JournalMemo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["JournalMemo"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["AccountNumber"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["AccountNumber"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["AccountNumber"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["AccountNumber"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["AccountNumber"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["AccountNumber"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["CorrespondingAccountNumber"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["CorrespondingAccountNumber"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["CorrespondingAccountNumber"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["CorrespondingAccountNumber"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["CorrespondingAccountNumber"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["CorrespondingAccountNumber"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["ContactName"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["ContactName"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ContactName"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["ContactName"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["ContactName"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ContactName"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.OriginX = 0;
            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["RefDate"].RowLayoutColumnInfo.OriginX = 120;
            parentBand.Columns["RefDate"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["RefDate"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["RefDate"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["ReceiptRefNo"].RowLayoutColumnInfo.OriginX = 240;
            parentBand.Columns["ReceiptRefNo"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["ReceiptRefNo"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["ReceiptRefNo"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["PaymentRefNo"].RowLayoutColumnInfo.OriginX = 360;
            parentBand.Columns["PaymentRefNo"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["PaymentRefNo"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["PaymentRefNo"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["JournalMemo"].RowLayoutColumnInfo.OriginX = 480;
            parentBand.Columns["JournalMemo"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["JournalMemo"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["JournalMemo"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["AccountNumber"].RowLayoutColumnInfo.OriginX = 730;
            parentBand.Columns["AccountNumber"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["AccountNumber"].RowLayoutColumnInfo.SpanX = 80;
            parentBand.Columns["AccountNumber"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["CorrespondingAccountNumber"].RowLayoutColumnInfo.OriginX = 810;
            parentBand.Columns["CorrespondingAccountNumber"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["CorrespondingAccountNumber"].RowLayoutColumnInfo.SpanX = 80;
            parentBand.Columns["CorrespondingAccountNumber"].RowLayoutColumnInfo.SpanY = 32;

            parentBandGroupPS.RowLayoutGroupInfo.OriginX = 890;
            parentBandGroupPS.RowLayoutGroupInfo.OriginY = 0;
            parentBandGroupPS.RowLayoutGroupInfo.SpanX = 500;
            parentBandGroupPS.RowLayoutGroupInfo.SpanY = 32;

            if (CurrencyID == "VND")
            {

                parentBand.Columns["ClosingAmount"].RowLayoutColumnInfo.OriginX = 1390;
                parentBand.Columns["ClosingAmount"].RowLayoutColumnInfo.OriginY = 0;
                parentBand.Columns["ClosingAmount"].RowLayoutColumnInfo.SpanX = 250;
                parentBand.Columns["ClosingAmount"].RowLayoutColumnInfo.SpanY = 32;
            }
            else
            {

                parentBand.Columns["ClosingAmountOC"].RowLayoutColumnInfo.OriginX = 1390;
                parentBand.Columns["ClosingAmountOC"].RowLayoutColumnInfo.OriginY = 0;
                parentBand.Columns["ClosingAmountOC"].RowLayoutColumnInfo.SpanX = 250;
                parentBand.Columns["ClosingAmountOC"].RowLayoutColumnInfo.SpanY = 32;
            }

            parentBand.Columns["ContactName"].RowLayoutColumnInfo.OriginX = 1640;
            parentBand.Columns["ContactName"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["ContactName"].RowLayoutColumnInfo.SpanX = 150;
            parentBand.Columns["ContactName"].RowLayoutColumnInfo.SpanY = 32;


        }

        private void ultraGridExcelExporter1_InitializeColumn(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.InitializeColumnEventArgs e)
        {
            e.ExcelFormatStr = e.Column.Format;
        }

        private void ultraGridExcelExporter1_BeginExport(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.BeginExportEventArgs e)
        {
            foreach (var row in uGrid.Rows)
            {
                foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
                {
                    e.Rows[row.Index].Cells[column].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                    e.Rows[row.Index].Cells[column].Appearance.BorderColor = Color.Black;
                    e.Rows[row.Index].Cells[column].Column.Width = 200;
                }
            }
            e.Rows.Band.Summaries[0].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[0].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[1].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[1].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[2].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[2].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[3].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[3].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[4].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[4].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[5].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[5].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[6].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[6].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[7].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[7].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[8].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[8].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[9].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[9].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[10].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[10].Appearance.BorderColor = Color.Black;
        }
        private void reportManager1_GetReportParameter(object sender, PerpetuumSoft.Reporting.Components.GetReportParameterEventArgs e)
        {
            e.Parameters["CurrencyID"].Value = CurrencyID;
        }

        private void uGrid_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            UltraGridCell cell = e.Row.Cells[0];
            cell.Row.Height = 25;
            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("JournalMemo"))
                if (cell.Row.Cells["JournalMemo"].Value != null && (cell.Row.Cells["JournalMemo"].Value.ToString() == "Số tồn đầu kỳ" || cell.Row.Cells["JournalMemo"].Value.ToString() == "Cộng nhóm"))
                {
                    cell.Row.Cells["PostedDate"].Value = null;
                    cell.Row.Cells["RefDate"].Value = null;
                    cell.Row.Cells["AccountNumber"].Value = null;
                    cell.Row.Cells["JournalMemo"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    cell.Row.Cells["DebitAmount"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    cell.Row.Cells["DebitAmountOC"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    cell.Row.Cells["CreditAmount"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    cell.Row.Cells["CreditAmountOC"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    cell.Row.Cells["ClosingAmount"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    cell.Row.Cells["ClosingAmountOC"].Appearance.FontData.Bold = DefaultableBoolean.True;
                }
            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("DebitAmount"))
            {
                if (cell.Row.Cells["DebitAmount"].Value != null && (decimal)cell.Row.Cells["DebitAmount"].Value == 0)
                    cell.Row.Cells["DebitAmount"].Value = null;
                if ((cell.Row.Cells["DebitAmount"].Value != null && (decimal)cell.Row.Cells["DebitAmount"].Value < 0))
                    cell.Row.Cells["DebitAmount"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("DebitAmountOC"))
            {
                if (cell.Row.Cells["DebitAmountOC"].Value != null && (decimal)cell.Row.Cells["DebitAmountOC"].Value == 0)
                    cell.Row.Cells["DebitAmountOC"].Value = null;
                if ((cell.Row.Cells["DebitAmountOC"].Value != null && (decimal)cell.Row.Cells["DebitAmountOC"].Value < 0))
                    cell.Row.Cells["DebitAmountOC"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("CreditAmount"))
            {
                if (cell.Row.Cells["CreditAmount"].Value != null && (decimal)cell.Row.Cells["CreditAmount"].Value == 0)
                    cell.Row.Cells["CreditAmount"].Value = null;
                if ((cell.Row.Cells["CreditAmount"].Value != null && (decimal)cell.Row.Cells["CreditAmount"].Value < 0))
                    cell.Row.Cells["CreditAmount"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("CreditAmountOC"))
            {
                if (cell.Row.Cells["CreditAmountOC"].Value != null && (decimal)cell.Row.Cells["CreditAmountOC"].Value == 0)
                    cell.Row.Cells["CreditAmountOC"].Value = null;
                if ((cell.Row.Cells["CreditAmountOC"].Value != null && (decimal)cell.Row.Cells["CreditAmountOC"].Value < 0))
                    cell.Row.Cells["CreditAmountOC"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("ClosingAmount"))
            {
                if (cell.Row.Cells["ClosingAmount"].Value != null && (decimal)cell.Row.Cells["ClosingAmount"].Value == 0)
                    cell.Row.Cells["ClosingAmount"].Value = null;
                if ((cell.Row.Cells["ClosingAmount"].Value != null && (decimal)cell.Row.Cells["ClosingAmount"].Value < 0))
                    cell.Row.Cells["ClosingAmount"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("ClosingAmountOC"))
            {
                if (cell.Row.Cells["ClosingAmountOC"].Value != null && (decimal)cell.Row.Cells["ClosingAmountOC"].Value == 0)
                    cell.Row.Cells["ClosingAmountOC"].Value = null;
                if ((cell.Row.Cells["ClosingAmountOC"].Value != null && (decimal)cell.Row.Cells["ClosingAmountOC"].Value < 0))
                    cell.Row.Cells["ClosingAmountOC"].Appearance.ForeColor = Color.Red;
            }

        }

        private void FSOKTCTTIENMATex_SizeChanged(object sender, EventArgs e)
        {
            var state = this.WindowState;
            switch (state)
            {
                case FormWindowState.Maximized:
                    break;
                case FormWindowState.Minimized:
                    break;
                case FormWindowState.Normal:
                    this.Left = (Screen.PrimaryScreen.Bounds.Width - this.Width) / 2;
                    this.Top = (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2;
                    this.Location = new Point((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2, (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
                    break;
                default:
                    break;
            }
        }

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                FU_GetCashDetailBook temp = (FU_GetCashDetailBook)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                if (temp != null)
                    editFuntion(temp);

            }
        }

        private void editFuntion(FU_GetCashDetailBook temp)
        {

            var f = Utils.ViewVoucherSelected1(temp.RefID, temp.RefType);
            //List<FU_GetCashDetailBook> data1 = sp.GetFU_GetCashDetailBook(FromDate, ToDate, _listTK, "", CurrencyID).ToList();
            //configGrid(data1);

        }

        private void ultraGridExcelExporter1_SummaryCellExported(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.SummaryCellExportedEventArgs e)
        {
            Worksheet ws = e.CurrentWorksheet;
            WorksheetRow wsRow = ws.Rows[e.CurrentRowIndex];
            WorksheetCell wsCell = wsRow.Cells[e.CurrentColumnIndex];
            if (!e.Summary.SummaryText.Contains("Số dòng") && !e.Summary.SummaryText.Contains(" "))
            {
                string s = e.Summary.SummaryText.Trim('.');
                if (s.Contains("("))
                    wsCell.Value = -decimal.Parse(s.Trim('(').Trim(')'));
                else
                    wsCell.Value = decimal.Parse(s);

                wsCell.CellFormat.FormatString = "###,###,###,###,##0;(###,###,###,###,##0)";
            }
        }
    }

}
