﻿using Accounting.Core.DAO;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.Report;
using Accounting.TextMessage;
using Infragistics.Documents.Excel;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRSOQUYTMex : Form
    {
        private string _subSystemCode;
        List<CACashBookInCABook> data = new List<CACashBookInCABook>();
        string period = "";
        string CurrencyID = "";
        string accountid;
        DateTime FromDate;
        DateTime ToDate;
        ReportProcedureSDS sp = new ReportProcedureSDS();
        public FRSOQUYTMex(DateTime FromDate1, DateTime ToDate1, string accountid1, string CurrencyID1)
        {
            InitializeComponent();
            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\SoQuyTM.rst", path);
            ReportUtils.ProcessControls(this);

            List<CACashBookInCABook> data1 = sp.Get_SoQuyTM(FromDate1, ToDate1, accountid1, CurrencyID1);
            period = "Loại tiền: " + CurrencyID1 + "; " + ReportUtils.GetPeriod(FromDate1, ToDate1);

            CurrencyID = CurrencyID1;
            accountid = accountid1;
            FromDate = FromDate1;
            ToDate = ToDate1;
            data = data1;

            configGrid(data);

            if (Utils.isDemo)
                btnExport.Visible = false;
            else
                btnExport.Visible = true;
        }

        private void configGrid(List<CACashBookInCABook> list)
        {

            uGrid.DataSource = list;
            Utils.ConfigGrid(uGrid, ConstDatabase.CACashBookInCABook_TableName);
            uGrid.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;

            list.ForEach(t =>
            {
                if (t.JournalMemo == null)
                    t.JournalMemo = "";
                else
                    t.JournalMemo = t.JournalMemo;
            });

            GridLayout();

            Utils.AddSumColumn(uGrid, "RefDate", false);
            Utils.AddSumColumn(uGrid, "ReceiptRefNo", false);
            Utils.AddSumColumn(uGrid, "PaymentRefNo", false);
            Utils.AddSumColumn(uGrid, "JournalMemo", false);
            Utils.AddSumColumn(uGrid, "Note", false);

            uGrid.DisplayLayout.Bands[0].Summaries[1].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[2].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[3].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[4].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[5].DisplayFormat = " ";

            if (CurrencyID == "VND")
            {
                uGrid.DisplayLayout.Bands[0].Columns["TotalReceiptFBCurrencyID"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGrid.DisplayLayout.Bands[0].Columns["TotalPaymentFBCurrencyID"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGrid.DisplayLayout.Bands[0].Columns["ClosingFBCurrencyID"].FormatNumberic(ConstDatabase.Format_TienVND);

                Utils.AddSumColumn1(list.Where(n => n.TotalReceiptFBCurrencyID != null).Sum(n => n.TotalReceiptFBCurrencyID), uGrid, "TotalReceiptFBCurrencyID", false, constDatabaseFormat: 1);
                Utils.AddSumColumn1(list.Where(n => n.TotalPaymentFBCurrencyID != null).Sum(n => n.TotalPaymentFBCurrencyID), uGrid, "TotalPaymentFBCurrencyID", false, constDatabaseFormat: 1);
                Utils.AddSumColumn1(list.Count != 0 ? list[list.Count() - 1].ClosingFBCurrencyID : 0, uGrid, "ClosingFBCurrencyID", false, constDatabaseFormat: 1);
            }
            else
            {
                uGrid.DisplayLayout.Bands[0].Columns["TotalReceiptFBCurrencyID"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                uGrid.DisplayLayout.Bands[0].Columns["TotalPaymentFBCurrencyID"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                uGrid.DisplayLayout.Bands[0].Columns["ClosingFBCurrencyID"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);

                Utils.AddSumColumn1(list.Where(n => n.TotalReceiptFBCurrencyID != null).Sum(n => n.TotalReceiptFBCurrencyID), uGrid, "TotalReceiptFBCurrencyID", false, constDatabaseFormat: 2);
                Utils.AddSumColumn1(list.Where(n => n.TotalPaymentFBCurrencyID != null).Sum(n => n.TotalPaymentFBCurrencyID), uGrid, "TotalPaymentFBCurrencyID", false, constDatabaseFormat: 2);
                Utils.AddSumColumn1(list.Count != 0 ? list[list.Count() - 1].ClosingFBCurrencyID : 0, uGrid, "ClosingFBCurrencyID", false, constDatabaseFormat: 2);
            }

            uGrid.DisplayLayout.Bands[0].Columns["PostedDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["PostedDate"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["PostedDate"].Width = 120;

            uGrid.DisplayLayout.Bands[0].Columns["RefDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["RefDate"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["RefDate"].Width = 120;

            uGrid.DisplayLayout.Bands[0].Columns["ReceiptRefNo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["ReceiptRefNo"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["ReceiptRefNo"].Width = 120;
            uGrid.DisplayLayout.Bands[0].Columns["ReceiptRefNo"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["PaymentRefNo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["PaymentRefNo"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["PaymentRefNo"].Width = 120;
            uGrid.DisplayLayout.Bands[0].Columns["PaymentRefNo"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["JournalMemo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["JournalMemo"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["JournalMemo"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["JournalMemo"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["TotalReceiptFBCurrencyID"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["TotalReceiptFBCurrencyID"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["TotalReceiptFBCurrencyID"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["TotalReceiptFBCurrencyID"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["TotalPaymentFBCurrencyID"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["TotalPaymentFBCurrencyID"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["TotalPaymentFBCurrencyID"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["TotalPaymentFBCurrencyID"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["ClosingFBCurrencyID"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["ClosingFBCurrencyID"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["ClosingFBCurrencyID"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["ClosingFBCurrencyID"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["Note"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["Note"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["Note"].Width = 150;
        }
        private void btnOk_Click(object sender, EventArgs e)
        {
            if (data.Count == 0)
            {
                MSG.Warning("Không có dữ liệu để lên báo cáo");
            }
            else
            {
                data.ForEach(t =>
                {
                    if (t.JournalMemo == null)
                        t.JournalMemo = "";
                    else
                        t.JournalMemo = t.JournalMemo;
                });

                data[data.Count() - 1].Sum_SoTon = data[data.Count() - 1].ClosingFBCurrencyID;
                var rD = new CACashBookInCABookDetail();
                rD.Period = "Loại tiền: " + CurrencyID + "; " + ReportUtils.GetPeriod(FromDate, ToDate);
                var f = new ReportForm<CACashBookInCABook>(fileReportSlot1, _subSystemCode);
                f.AddDatasource("DBSOQUYTM", data, true);
                f.AddDatasource("Detail", rD);
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void reportManager1_GetReportParameter(object sender, PerpetuumSoft.Reporting.Components.GetReportParameterEventArgs e)
        {
            e.Parameters["CurrencyID"].Value = CurrencyID;
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog
            {
                Title = "Chọn nơi sao lưu ",
                Filter = "File (*.xls)|*.xls",
                InitialDirectory = "@C:\\",
                FileName = "soquytienmat.xls"
            };
            if (save.ShowDialog() == DialogResult.OK)
            {
                string duongdan = save.FileName;
                try
                {
                    ultraGridExcelExporter1.Export(uGrid, duongdan);
                    if (MSG.Question("Kết xuất thành công, Bạn có muốn mở file vừa kết xuất không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(save.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
                catch
                {
                    MSG.Warning("Lỗi khi kết xuất !");
                }
            }
        }

        private void ultraGridExcelExporter1_InitializeColumn(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.InitializeColumnEventArgs e)
        {
            e.ExcelFormatStr = e.Column.Format;
        }

        private void ultraGridExcelExporter1_BeginExport(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.BeginExportEventArgs e)
        {
            foreach (var row in uGrid.Rows)
            {
                foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
                {
                    e.Rows[row.Index].Cells[column].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                    e.Rows[row.Index].Cells[column].Appearance.BorderColor = Color.Black;
                    e.Rows[row.Index].Cells[column].Column.Width = 200;
                }
            }
            e.Rows.Band.Summaries[0].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[0].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[1].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[1].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[2].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[2].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[3].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[3].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[4].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[4].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[5].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[5].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[6].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[6].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[7].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[7].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[8].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[8].Appearance.BorderColor = Color.Black;
        }

        private void uGrid_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            UltraGridCell cell = e.Row.Cells[0];
            cell.Row.Height = 25;
            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("JournalMemo"))
                if (cell.Row.Cells["JournalMemo"].Value != null && cell.Row.Cells["JournalMemo"].Value.ToString() == "Số dư đầu kỳ")
                {
                    cell.Row.Cells["PostedDate"].Value = null;
                    cell.Row.Cells["RefDate"].Value = null;
                    cell.Row.Cells["JournalMemo"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    cell.Row.Cells["TotalReceiptFBCurrencyID"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    cell.Row.Cells["TotalPaymentFBCurrencyID"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    cell.Row.Cells["ClosingFBCurrencyID"].Appearance.FontData.Bold = DefaultableBoolean.True;
                }
            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("TotalReceiptFBCurrencyID"))
            {
                if (cell.Row.Cells["TotalReceiptFBCurrencyID"].Value != null && (decimal)cell.Row.Cells["TotalReceiptFBCurrencyID"].Value == 0M)
                    cell.Row.Cells["TotalReceiptFBCurrencyID"].Value = null;
                if ((cell.Row.Cells["TotalReceiptFBCurrencyID"].Value != null && (decimal)cell.Row.Cells["TotalReceiptFBCurrencyID"].Value < 0))
                    cell.Row.Cells["TotalReceiptFBCurrencyID"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("TotalPaymentFBCurrencyID"))
            {
                if (cell.Row.Cells["TotalPaymentFBCurrencyID"].Value != null && (decimal)cell.Row.Cells["TotalPaymentFBCurrencyID"].Value == 0M)
                    cell.Row.Cells["TotalPaymentFBCurrencyID"].Value = null;
                if ((cell.Row.Cells["TotalPaymentFBCurrencyID"].Value != null && (decimal)cell.Row.Cells["TotalPaymentFBCurrencyID"].Value < 0))
                    cell.Row.Cells["TotalPaymentFBCurrencyID"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("ClosingFBCurrencyID"))
            {
                if (cell.Row.Cells["ClosingFBCurrencyID"].Value != null && (decimal)cell.Row.Cells["ClosingFBCurrencyID"].Value == 0M)
                    cell.Row.Cells["ClosingFBCurrencyID"].Value = null;
                if ((cell.Row.Cells["ClosingFBCurrencyID"].Value != null && (decimal)cell.Row.Cells["ClosingFBCurrencyID"].Value < 0))
                    cell.Row.Cells["ClosingFBCurrencyID"].Appearance.ForeColor = Color.Red;
            }

        }

        private void GridLayout()
        {
            UltraGridBand parentBand = this.uGrid.DisplayLayout.Bands[0];
            parentBand.RowLayoutStyle = RowLayoutStyle.GroupLayout;

            UltraGridGroup parentBandGroupPeriod;
            if (parentBand.Groups.Exists("ParentBandGroupPeriod"))
            {
                parentBandGroupPeriod = parentBand.Groups["ParentBandGroupPeriod"];
                parentBandGroupPeriod.Header.Caption = period;
            }
            else
                parentBandGroupPeriod = parentBand.Groups.Add("ParentBandGroupPeriod", period);
            parentBandGroupPeriod.Header.Appearance.FontData.Italic = DefaultableBoolean.True;
            parentBandGroupPeriod.Header.Appearance.FontData.SizeInPoints = 10;
            parentBandGroupPeriod.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupPeriod.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPeriod.Header.Appearance.BorderColor = Color.Black;

            UltraGridGroup parentBandGroupTitle;
            if (parentBand.Groups.Exists("ParentBandGroupTitle"))
                parentBandGroupTitle = parentBand.Groups["ParentBandGroupTitle"];
            else
                parentBandGroupTitle = parentBand.Groups.Add("ParentBandGroupTitle", "SỔ QUỸ TIỀN MẶT");
            parentBandGroupTitle.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroupTitle.Header.Appearance.FontData.SizeInPoints = 12;
            parentBandGroupTitle.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupTitle.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupTitle.Header.Appearance.BorderColor = Color.Black;

            UltraGridGroup parentBandGroupCT;
            if (parentBand.Groups.Exists("ParentBandGroupCT"))
                parentBandGroupCT = parentBand.Groups["ParentBandGroupCT"];
            else
                parentBandGroupCT = parentBand.Groups.Add("ParentBandGroupCT", "Số hiệu chứng từ");
            parentBandGroupCT.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            parentBandGroupCT.Header.Appearance.FontData.SizeInPoints = 9;
            parentBandGroupCT.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupCT.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupCT.Header.Appearance.BorderColor = Color.Black;
            parentBandGroupCT.Header.Appearance.BackColorAlpha = Alpha.Default;

            UltraGridGroup parentBandGroupST;
            if (parentBand.Groups.Exists("ParentBandGroupST"))
                parentBandGroupST = parentBand.Groups["ParentBandGroupST"];
            else
                parentBandGroupST = parentBand.Groups.Add("ParentBandGroupST", "Số tiền");
            parentBandGroupST.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            parentBandGroupST.Header.Appearance.FontData.SizeInPoints = 9;
            parentBandGroupST.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupST.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupST.Header.Appearance.BorderColor = Color.Black;
            parentBandGroupST.Header.Appearance.BackColorAlpha = Alpha.Default;

            parentBandGroupPeriod.RowLayoutGroupInfo.ParentGroup = parentBandGroupTitle;


            parentBand.Columns["TotalReceiptFBCurrencyID"].RowLayoutColumnInfo.ParentGroup = parentBandGroupST;
            parentBand.Columns["TotalReceiptFBCurrencyID"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["TotalReceiptFBCurrencyID"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["TotalReceiptFBCurrencyID"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["TotalReceiptFBCurrencyID"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["TotalReceiptFBCurrencyID"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["TotalPaymentFBCurrencyID"].RowLayoutColumnInfo.ParentGroup = parentBandGroupST;
            parentBand.Columns["TotalPaymentFBCurrencyID"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["TotalPaymentFBCurrencyID"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["TotalPaymentFBCurrencyID"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["TotalPaymentFBCurrencyID"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["TotalPaymentFBCurrencyID"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["ClosingFBCurrencyID"].RowLayoutColumnInfo.ParentGroup = parentBandGroupST;
            parentBand.Columns["ClosingFBCurrencyID"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ClosingFBCurrencyID"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["ClosingFBCurrencyID"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["ClosingFBCurrencyID"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ClosingFBCurrencyID"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["PostedDate"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["PostedDate"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["PostedDate"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["PostedDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["PostedDate"].Header.Appearance.BorderColor = Color.Black;


            parentBand.Columns["RefDate"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["RefDate"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["RefDate"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["RefDate"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["RefDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["RefDate"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["ReceiptRefNo"].RowLayoutColumnInfo.ParentGroup = parentBandGroupCT;
            parentBand.Columns["ReceiptRefNo"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ReceiptRefNo"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["ReceiptRefNo"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["ReceiptRefNo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ReceiptRefNo"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["PaymentRefNo"].RowLayoutColumnInfo.ParentGroup = parentBandGroupCT;
            parentBand.Columns["PaymentRefNo"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["PaymentRefNo"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["PaymentRefNo"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["PaymentRefNo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["PaymentRefNo"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["JournalMemo"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["JournalMemo"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["JournalMemo"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["JournalMemo"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["JournalMemo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["JournalMemo"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["Note"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["Note"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Note"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["Note"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["Note"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Note"].Header.Appearance.BorderColor = Color.Black;


            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.OriginX = 0;
            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.SpanX = 80;
            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.SpanY = 80;


            parentBand.Columns["RefDate"].RowLayoutColumnInfo.OriginX = 80;
            parentBand.Columns["RefDate"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["RefDate"].RowLayoutColumnInfo.SpanX = 80;
            parentBand.Columns["RefDate"].RowLayoutColumnInfo.SpanY = 80;


            parentBandGroupCT.RowLayoutGroupInfo.OriginX = 160;
            parentBandGroupCT.RowLayoutGroupInfo.OriginY = 0;
            parentBandGroupCT.RowLayoutGroupInfo.SpanX = 160;
            parentBandGroupCT.RowLayoutGroupInfo.SpanY = 80;

            parentBand.Columns["JournalMemo"].RowLayoutColumnInfo.OriginX = 320;
            parentBand.Columns["JournalMemo"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["JournalMemo"].RowLayoutColumnInfo.SpanX = 300;
            parentBand.Columns["JournalMemo"].RowLayoutColumnInfo.SpanY = 80;

            parentBandGroupST.RowLayoutGroupInfo.OriginX = 620;
            parentBandGroupST.RowLayoutGroupInfo.OriginY = 0;
            parentBandGroupST.RowLayoutGroupInfo.SpanX = 600;
            parentBandGroupST.RowLayoutGroupInfo.SpanY = 80;

            parentBand.Columns["Note"].RowLayoutColumnInfo.OriginX = 1220;
            parentBand.Columns["Note"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["Note"].RowLayoutColumnInfo.SpanX = 160;
            parentBand.Columns["Note"].RowLayoutColumnInfo.SpanY = 80;


        }

        private void FRSOQUYTM_SizeChanged(object sender, EventArgs e)
        {
            var state = this.WindowState;
            switch (state)
            {
                case FormWindowState.Maximized:
                    uGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
                    break;
                case FormWindowState.Minimized:
                    break;
                case FormWindowState.Normal:
                    this.Left = (Screen.PrimaryScreen.Bounds.Width - this.Width) / 2;
                    this.Top = (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2;
                    this.Location = new Point((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2, (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
                    break;
                default:
                    break;
            }
        }

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                CACashBookInCABook temp = (CACashBookInCABook)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                if (temp != null)
                    editFuntion(temp);

            }
        }

        private void editFuntion(CACashBookInCABook temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID, temp.RefType);
            //List<CACashBookInCABook> data1 = sp.Get_SoQuyTM(FromDate, ToDate, accountid, CurrencyID).ToList();
            //configGrid(data1);

        }

        private void ultraGridExcelExporter1_SummaryCellExported(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.SummaryCellExportedEventArgs e)
        {
            Worksheet ws = e.CurrentWorksheet;
            WorksheetRow wsRow = ws.Rows[e.CurrentRowIndex];
            WorksheetCell wsCell = wsRow.Cells[e.CurrentColumnIndex];
            if (!e.Summary.SummaryText.Contains("Số dòng") && !e.Summary.SummaryText.Contains(" "))
            {
                string s = e.Summary.SummaryText.Trim('.');
                if (s.Contains("("))
                    wsCell.Value = -decimal.Parse(s.Trim('(').Trim(')'));
                else
                    wsCell.Value = decimal.Parse(s);

                wsCell.CellFormat.FormatString = "###,###,###,###,##0;(###,###,###,###,##0)";
            }
        }
    }
}
