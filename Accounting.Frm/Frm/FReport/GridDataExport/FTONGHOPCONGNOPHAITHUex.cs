﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.AppStyling;
using System.IO;
using FX.Core;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinMaskedEdit;
using Infragistics.Win.UltraWinTabs;
using Accounting;
using Accounting.Core.DAO;
using System.Diagnostics;
using Accounting.Core.Domain.obj.Report;
using Infragistics.Documents.Excel;

namespace Accounting.Frm.FReport
{
    public partial class FTONGHOPCONGNOPHAITHUex : CustormForm
    {
        private string _subSystemCode;
        private List<SA_GetReceivableSummary> data = new List<SA_GetReceivableSummary>();

        private string CurrencyID;
        private string Acount;
        private string list_acount;
        private DateTime FromDate;
        private DateTime ToDate;
        private string period = "";
        ReportProcedureSDS sp = new ReportProcedureSDS();
        public FTONGHOPCONGNOPHAITHUex(DateTime FromDate1, DateTime ToDate1, string CurrencyID1, string Acount1, string list_acount1)
        {
            InitializeComponent();
            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\TongHopCongNoPhaiThu.rst", path);

            List<SA_GetReceivableSummary> data1 = sp.GetTongCongNoPhaiThu(FromDate1, ToDate1, CurrencyID1, Acount1, list_acount1, "", false).ToList();

            string txtThoiGian = "Tài khoản: " + Acount1 + "; Loại tiền: " + CurrencyID1;
            period = txtThoiGian + "; " + ReportUtils.GetPeriod(FromDate1, ToDate1);

            data = data1;
            Acount = Acount1;
            CurrencyID = CurrencyID1;
            list_acount = list_acount1;
            FromDate = FromDate1;
            ToDate = ToDate1;

            configGrid(data);
            if (Utils.isDemo)
                btnExport.Visible = false;
            else
                btnExport.Visible = true;

        }

        private void configGrid(List<SA_GetReceivableSummary> lst)
        {
            uGrid.DataSource = lst;
            Utils.ConfigGrid(uGrid, ConstDatabase.SA_GetReceivableSummary_TableName);
            uGrid.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            uGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;

            Utils.AddSumColumn(uGrid, "AccountingObjectName", false);
            Utils.AddSumColumn(uGrid, "AccountNumber", false);

            uGrid.DisplayLayout.Bands[0].Summaries[1].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[2].DisplayFormat = " ";

            if (CurrencyID == "VND")
            {
                uGrid.DisplayLayout.Bands[0].Columns["OpenningDebitAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGrid.DisplayLayout.Bands[0].Columns["OpenningCreditAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGrid.DisplayLayout.Bands[0].Columns["CloseDebitAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGrid.DisplayLayout.Bands[0].Columns["CloseCreditAmount"].FormatNumberic(ConstDatabase.Format_TienVND);

                uGrid.DisplayLayout.Bands[0].Columns["OpenningDebitAmountOC"].Hidden = true;
                uGrid.DisplayLayout.Bands[0].Columns["OpenningCreditAmountOC"].Hidden = true;
                uGrid.DisplayLayout.Bands[0].Columns["DebitAmountOC"].Hidden = true;
                uGrid.DisplayLayout.Bands[0].Columns["CreditAmountOC"].Hidden = true;
                uGrid.DisplayLayout.Bands[0].Columns["CloseDebitAmountOC"].Hidden = true;
                uGrid.DisplayLayout.Bands[0].Columns["CloseCreditAmountOC"].Hidden = true;

                Utils.AddSumColumn1(lst.Where(n => n.DebitAmount != null).Sum(n => n.DebitAmount), uGrid, "DebitAmount", false, constDatabaseFormat: 1);
                Utils.AddSumColumn1(lst.Where(n => n.CreditAmount != null).Sum(n => n.CreditAmount), uGrid, "CreditAmount", false, constDatabaseFormat: 1);
                Utils.AddSumColumn1(lst.Where(n => n.OpenningDebitAmount != null).Sum(n => n.OpenningDebitAmount), uGrid, "OpenningDebitAmount", false, constDatabaseFormat: 1);
                Utils.AddSumColumn1(lst.Where(n => n.OpenningCreditAmount != null).Sum(n => n.OpenningCreditAmount), uGrid, "OpenningCreditAmount", false, constDatabaseFormat: 1);
                Utils.AddSumColumn1(lst.Where(n => n.CloseDebitAmount != null).Sum(n => n.CloseDebitAmount), uGrid, "CloseDebitAmount", false, constDatabaseFormat: 1);
                Utils.AddSumColumn1(lst.Where(n => n.CloseCreditAmount != null).Sum(n => n.CloseCreditAmount), uGrid, "CloseCreditAmount", false, constDatabaseFormat: 1);

            }
            else
            {
                uGrid.DisplayLayout.Bands[0].Columns["OpenningDebitAmountOC"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                uGrid.DisplayLayout.Bands[0].Columns["OpenningCreditAmountOC"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                uGrid.DisplayLayout.Bands[0].Columns["DebitAmountOC"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                uGrid.DisplayLayout.Bands[0].Columns["CreditAmountOC"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                uGrid.DisplayLayout.Bands[0].Columns["CloseDebitAmountOC"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                uGrid.DisplayLayout.Bands[0].Columns["CloseCreditAmountOC"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);

                uGrid.DisplayLayout.Bands[0].Columns["OpenningDebitAmount"].Hidden = true;
                uGrid.DisplayLayout.Bands[0].Columns["OpenningCreditAmount"].Hidden = true;
                uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"].Hidden = true;
                uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"].Hidden = true;
                uGrid.DisplayLayout.Bands[0].Columns["CloseDebitAmount"].Hidden = true;
                uGrid.DisplayLayout.Bands[0].Columns["CloseCreditAmount"].Hidden = true;

                Utils.AddSumColumn1(lst.Where(n => n.DebitAmountOC != null).Sum(n => n.DebitAmountOC), uGrid, "DebitAmountOC", false, constDatabaseFormat: 2);
                Utils.AddSumColumn1(lst.Where(n => n.CreditAmountOC != null).Sum(n => n.CreditAmountOC), uGrid, "CreditAmountOC", false, constDatabaseFormat: 2);
                Utils.AddSumColumn1(lst.Where(n => n.OpenningDebitAmountOC != null).Sum(n => n.OpenningDebitAmountOC), uGrid, "OpenningDebitAmountOC", false, constDatabaseFormat: 2);
                Utils.AddSumColumn1(lst.Where(n => n.OpenningCreditAmountOC != null).Sum(n => n.OpenningCreditAmountOC), uGrid, "OpenningCreditAmountOC", false, constDatabaseFormat: 2);
                Utils.AddSumColumn1(lst.Where(n => n.CloseDebitAmountOC != null).Sum(n => n.CloseDebitAmountOC), uGrid, "CloseDebitAmountOC", false, constDatabaseFormat: 2);
                Utils.AddSumColumn1(lst.Where(n => n.CloseCreditAmountOC != null).Sum(n => n.CloseCreditAmountOC), uGrid, "CloseCreditAmountOC", false, constDatabaseFormat: 2);

            }

            uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectCode"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectCode"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectCode"].Width = 150;
            uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectCode"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectName"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectName"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectName"].Width = 200;
            uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectName"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["AccountNumber"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["AccountNumber"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["AccountNumber"].Width = 100;
            uGrid.DisplayLayout.Bands[0].Columns["AccountNumber"].CellAppearance.TextHAlign = HAlign.Center;

            uGrid.DisplayLayout.Bands[0].Columns["OpenningDebitAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["OpenningDebitAmount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["OpenningDebitAmount"].Width = 200;
            uGrid.DisplayLayout.Bands[0].Columns["OpenningDebitAmount"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["OpenningCreditAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["OpenningCreditAmount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["OpenningCreditAmount"].Width = 200;
            uGrid.DisplayLayout.Bands[0].Columns["OpenningCreditAmount"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"].Width = 200;
            uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"].Width = 200;
            uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["CloseDebitAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["CloseDebitAmount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["CloseDebitAmount"].Width = 200;
            uGrid.DisplayLayout.Bands[0].Columns["CloseDebitAmount"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["CloseCreditAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["CloseCreditAmount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["CloseCreditAmount"].Width = 200;
            uGrid.DisplayLayout.Bands[0].Columns["CloseCreditAmount"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["OpenningDebitAmountOC"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["OpenningDebitAmountOC"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["OpenningDebitAmountOC"].Width = 200;
            uGrid.DisplayLayout.Bands[0].Columns["OpenningDebitAmountOC"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["OpenningCreditAmountOC"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["OpenningCreditAmountOC"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["OpenningCreditAmountOC"].Width = 200;
            uGrid.DisplayLayout.Bands[0].Columns["OpenningCreditAmountOC"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["DebitAmountOC"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["DebitAmountOC"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["DebitAmountOC"].Width = 200;
            uGrid.DisplayLayout.Bands[0].Columns["DebitAmountOC"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["CreditAmountOC"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["CreditAmountOC"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["CreditAmountOC"].Width = 200;
            uGrid.DisplayLayout.Bands[0].Columns["CreditAmountOC"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["CloseDebitAmountOC"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["CloseDebitAmountOC"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["CloseDebitAmountOC"].Width = 200;
            uGrid.DisplayLayout.Bands[0].Columns["CloseDebitAmountOC"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["CloseCreditAmountOC"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["CloseCreditAmountOC"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["CloseCreditAmountOC"].Width = 200;
            uGrid.DisplayLayout.Bands[0].Columns["CloseCreditAmountOC"].CellAppearance.TextVAlign = VAlign.Middle;

            GridLayout();

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (data.Count == 0)
                MSG.Warning("Không có dữ liệu");
            else
            {
                var rD = new SA_GetReceivableSummary_period();
                string txtThoiGian = "Tài khoản: " + Acount + "; Loại tiền: " + CurrencyID;
                rD.Period = txtThoiGian + "; " + ReportUtils.GetPeriod(FromDate, ToDate);
                var f = new ReportForm<SA_GetReceivableSummary>(fileReportSlot1, _subSystemCode);
                f.AddDatasource("DBTONGTHU", data, true);
                f.AddDatasource("Detail", rD);
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }


        private void btnExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog
            {
                Title = "Chọn nơi sao lưu ",
                Filter = "File (*.xls)|*.xls",
                InitialDirectory = "@C:\\",
                FileName = "tonghopcongnophaithu.xls"
            };
            if (save.ShowDialog() == DialogResult.OK)
            {
                string duongdan = save.FileName;
                try
                {
                    ultraGridExcelExporter1.Export(uGrid, duongdan);
                    if (MSG.Question("Kết xuất thành công, Bạn có muốn mở file vừa kết xuất không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(save.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
                catch
                {
                    MSG.Warning("Lỗi khi kết xuất !");
                }
            }
        }
        private void GridLayout()
        {
            UltraGridBand parentBand = this.uGrid.DisplayLayout.Bands[0];
            parentBand.RowLayoutStyle = RowLayoutStyle.GroupLayout;

            UltraGridGroup parentBandGroupPeriod;
            if (parentBand.Groups.Exists("ParentBandGroupPeriod"))
            {
                parentBandGroupPeriod = parentBand.Groups["ParentBandGroupPeriod"];
                parentBandGroupPeriod.Header.Caption = period;
            }
            else
                parentBandGroupPeriod = parentBand.Groups.Add("ParentBandGroupPeriod", period);
            parentBandGroupPeriod.Header.Appearance.FontData.Italic = DefaultableBoolean.True;
            parentBandGroupPeriod.Header.Appearance.FontData.SizeInPoints = 10;
            parentBandGroupPeriod.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupPeriod.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPeriod.Header.Appearance.BorderColor = Color.Black;

            UltraGridGroup parentBandGroupTitle;
            if (parentBand.Groups.Exists("ParentBandGroupTitle"))
                parentBandGroupTitle = parentBand.Groups["ParentBandGroupTitle"];
            else
                parentBandGroupTitle = parentBand.Groups.Add("ParentBandGroupTitle", "TỔNG HỢP CÔNG NỢ PHẢI THU");
            parentBandGroupTitle.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroupTitle.Header.Appearance.FontData.SizeInPoints = 12;
            parentBandGroupTitle.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupTitle.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupTitle.Header.Appearance.BorderColor = Color.Black;

            UltraGridGroup parentBandGroupDK;
            if (parentBand.Groups.Exists("ParentBandGroupPS"))
                parentBandGroupDK = parentBand.Groups["ParentBandGroupDK"];
            else
                parentBandGroupDK = parentBand.Groups.Add("ParentBandGroupDK", "Đầu kỳ");
            parentBandGroupDK.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            parentBandGroupDK.Header.Appearance.FontData.SizeInPoints = 9;
            parentBandGroupDK.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupDK.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupDK.Header.Appearance.BorderColor = Color.Black;
            parentBandGroupDK.Header.Appearance.BackColorAlpha = Alpha.Default;

            UltraGridGroup parentBandGroupPS;
            if (parentBand.Groups.Exists("ParentBandGroupPS"))
                parentBandGroupPS = parentBand.Groups["ParentBandGroupPS"];
            else
                parentBandGroupPS = parentBand.Groups.Add("ParentBandGroupPS", "Phát sinh");
            parentBandGroupPS.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            parentBandGroupPS.Header.Appearance.FontData.SizeInPoints = 9;
            parentBandGroupPS.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupPS.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPS.Header.Appearance.BorderColor = Color.Black;
            parentBandGroupPS.Header.Appearance.BackColorAlpha = Alpha.Default;

            UltraGridGroup parentBandGroupCK;
            if (parentBand.Groups.Exists("ParentBandGroupCK"))
                parentBandGroupCK = parentBand.Groups["ParentBandGroupCK"];
            else
                parentBandGroupCK = parentBand.Groups.Add("ParentBandGroupCK", "Cuối kỳ");
            parentBandGroupCK.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            parentBandGroupCK.Header.Appearance.FontData.SizeInPoints = 9;
            parentBandGroupCK.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupCK.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupCK.Header.Appearance.BorderColor = Color.Black;
            parentBandGroupCK.Header.Appearance.BackColorAlpha = Alpha.Default;

            parentBandGroupPeriod.RowLayoutGroupInfo.ParentGroup = parentBandGroupTitle;

            if (CurrencyID == "VND")
            {
                parentBand.Columns["OpenningDebitAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupDK;
                parentBand.Columns["OpenningDebitAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["OpenningDebitAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["OpenningDebitAmount"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["OpenningDebitAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["OpenningDebitAmount"].Header.Appearance.BorderColor = Color.Black;

                parentBand.Columns["OpenningCreditAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupDK;
                parentBand.Columns["OpenningCreditAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["OpenningCreditAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["OpenningCreditAmount"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["OpenningCreditAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["OpenningCreditAmount"].Header.Appearance.BorderColor = Color.Black;

                parentBand.Columns["DebitAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPS;
                parentBand.Columns["DebitAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["DebitAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["DebitAmount"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["DebitAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["DebitAmount"].Header.Appearance.BorderColor = Color.Black;

                parentBand.Columns["CreditAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPS;
                parentBand.Columns["CreditAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["CreditAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["CreditAmount"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["CreditAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["CreditAmount"].Header.Appearance.BorderColor = Color.Black;

                parentBand.Columns["CloseDebitAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupCK;
                parentBand.Columns["CloseDebitAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["CloseDebitAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["CloseDebitAmount"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["CloseDebitAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["CloseDebitAmount"].Header.Appearance.BorderColor = Color.Black;

                parentBand.Columns["CloseCreditAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupCK;
                parentBand.Columns["CloseCreditAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["CloseCreditAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["CloseCreditAmount"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["CloseCreditAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["CloseCreditAmount"].Header.Appearance.BorderColor = Color.Black;

            }
            else
            {
                parentBand.Columns["OpenningDebitAmountOC"].RowLayoutColumnInfo.ParentGroup = parentBandGroupDK;
                parentBand.Columns["OpenningDebitAmountOC"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["OpenningDebitAmountOC"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["OpenningDebitAmountOC"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["OpenningDebitAmountOC"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["OpenningDebitAmountOC"].Header.Appearance.BorderColor = Color.Black;

                parentBand.Columns["OpenningCreditAmountOC"].RowLayoutColumnInfo.ParentGroup = parentBandGroupDK;
                parentBand.Columns["OpenningCreditAmountOC"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["OpenningCreditAmountOC"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["OpenningCreditAmountOC"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["OpenningCreditAmountOC"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["OpenningCreditAmountOC"].Header.Appearance.BorderColor = Color.Black;

                parentBand.Columns["DebitAmountOC"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPS;
                parentBand.Columns["DebitAmountOC"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["DebitAmountOC"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["DebitAmountOC"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["DebitAmountOC"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["DebitAmountOC"].Header.Appearance.BorderColor = Color.Black;

                parentBand.Columns["CreditAmountOC"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPS;
                parentBand.Columns["CreditAmountOC"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["CreditAmountOC"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["CreditAmountOC"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["CreditAmountOC"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["CreditAmountOC"].Header.Appearance.BorderColor = Color.Black;

                parentBand.Columns["CloseDebitAmountOC"].RowLayoutColumnInfo.ParentGroup = parentBandGroupCK;
                parentBand.Columns["CloseDebitAmountOC"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["CloseDebitAmountOC"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["CloseDebitAmountOC"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["CloseDebitAmountOC"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["CloseDebitAmountOC"].Header.Appearance.BorderColor = Color.Black;

                parentBand.Columns["CloseCreditAmountOC"].RowLayoutColumnInfo.ParentGroup = parentBandGroupCK;
                parentBand.Columns["CloseCreditAmountOC"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["CloseCreditAmountOC"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["CloseCreditAmountOC"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["CloseCreditAmountOC"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["CloseCreditAmountOC"].Header.Appearance.BorderColor = Color.Black;

            }

            parentBand.Columns["AccountingObjectCode"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["AccountingObjectCode"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["AccountingObjectCode"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["AccountingObjectCode"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["AccountingObjectCode"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["AccountingObjectCode"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["AccountingObjectName"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["AccountingObjectName"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["AccountingObjectName"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["AccountingObjectName"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["AccountingObjectName"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["AccountingObjectName"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["AccountNumber"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["AccountNumber"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["AccountNumber"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["AccountNumber"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["AccountNumber"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["AccountNumber"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["AccountingObjectCode"].RowLayoutColumnInfo.OriginX = 0;
            parentBand.Columns["AccountingObjectCode"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["AccountingObjectCode"].RowLayoutColumnInfo.SpanX = 150;
            parentBand.Columns["AccountingObjectCode"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["AccountingObjectName"].RowLayoutColumnInfo.OriginX = 150;
            parentBand.Columns["AccountingObjectName"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["AccountingObjectName"].RowLayoutColumnInfo.SpanX = 200;
            parentBand.Columns["AccountingObjectName"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["AccountNumber"].RowLayoutColumnInfo.OriginX = 350;
            parentBand.Columns["AccountNumber"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["AccountNumber"].RowLayoutColumnInfo.SpanX = 100;
            parentBand.Columns["AccountNumber"].RowLayoutColumnInfo.SpanY = 32;

            parentBandGroupDK.RowLayoutGroupInfo.OriginX = 450;
            parentBandGroupDK.RowLayoutGroupInfo.OriginY = 0;
            parentBandGroupDK.RowLayoutGroupInfo.SpanX = 400;
            parentBandGroupDK.RowLayoutGroupInfo.SpanY = 32;

            parentBandGroupPS.RowLayoutGroupInfo.OriginX = 850;
            parentBandGroupPS.RowLayoutGroupInfo.OriginY = 0;
            parentBandGroupPS.RowLayoutGroupInfo.SpanX = 400;
            parentBandGroupPS.RowLayoutGroupInfo.SpanY = 32;

            parentBandGroupCK.RowLayoutGroupInfo.OriginX = 1250;
            parentBandGroupCK.RowLayoutGroupInfo.OriginY = 0;
            parentBandGroupCK.RowLayoutGroupInfo.SpanX = 400;
            parentBandGroupCK.RowLayoutGroupInfo.SpanY = 32;



        }

        private void ultraGridExcelExporter1_InitializeColumn(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.InitializeColumnEventArgs e)
        {
            e.ExcelFormatStr = e.Column.Format;
        }

        private void ultraGridExcelExporter1_BeginExport(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.BeginExportEventArgs e)
        {
            foreach (var row in uGrid.Rows)
            {
                foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
                {
                    e.Rows[row.Index].Cells[column].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                    e.Rows[row.Index].Cells[column].Appearance.BorderColor = Color.Black;
                    e.Rows[row.Index].Cells[column].Column.Width = 200;
                }
            }
            e.Rows.Band.Summaries[0].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[0].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[1].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[1].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[2].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[2].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[3].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[3].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[4].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[4].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[5].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[5].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[6].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[6].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[7].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[7].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[8].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[8].Appearance.BorderColor = Color.Black;

        }
        private void reportManager1_GetReportParameter(object sender, PerpetuumSoft.Reporting.Components.GetReportParameterEventArgs e)
        {
            e.Parameters["CurrencyID"].Value = CurrencyID;
        }

        private void uGrid_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            UltraGridCell cell = e.Row.Cells[0];
            cell.Row.Height = 25;

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("DebitAmount"))
            {
                if (cell.Row.Cells["DebitAmount"].Value != null && (decimal)cell.Row.Cells["DebitAmount"].Value == 0)
                    cell.Row.Cells["DebitAmount"].Value = null;
                if ((cell.Row.Cells["DebitAmount"].Value != null && (decimal)cell.Row.Cells["DebitAmount"].Value < 0))
                    cell.Row.Cells["DebitAmount"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("DebitAmountOC"))
            {
                if (cell.Row.Cells["DebitAmountOC"].Value != null && (decimal)cell.Row.Cells["DebitAmountOC"].Value == 0)
                    cell.Row.Cells["DebitAmountOC"].Value = null;
                if ((cell.Row.Cells["DebitAmountOC"].Value != null && (decimal)cell.Row.Cells["DebitAmountOC"].Value < 0))
                    cell.Row.Cells["DebitAmountOC"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("CreditAmount"))
            {
                if (cell.Row.Cells["CreditAmount"].Value != null && (decimal)cell.Row.Cells["CreditAmount"].Value == 0)
                    cell.Row.Cells["CreditAmount"].Value = null;
                if ((cell.Row.Cells["CreditAmount"].Value != null && (decimal)cell.Row.Cells["CreditAmount"].Value < 0))
                    cell.Row.Cells["CreditAmount"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("CreditAmountOC"))
            {
                if (cell.Row.Cells["CreditAmountOC"].Value != null && (decimal)cell.Row.Cells["CreditAmountOC"].Value == 0)
                    cell.Row.Cells["CreditAmountOC"].Value = null;
                if ((cell.Row.Cells["CreditAmountOC"].Value != null && (decimal)cell.Row.Cells["CreditAmountOC"].Value < 0))
                    cell.Row.Cells["CreditAmountOC"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("CloseDebitAmount"))
            {
                if (cell.Row.Cells["CloseDebitAmount"].Value != null && (decimal)cell.Row.Cells["CloseDebitAmount"].Value == 0)
                    cell.Row.Cells["CloseDebitAmount"].Value = null;
                if ((cell.Row.Cells["CloseDebitAmount"].Value != null && (decimal)cell.Row.Cells["CloseDebitAmount"].Value < 0))
                    cell.Row.Cells["CloseDebitAmount"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("CloseDebitAmountOC"))
            {
                if (cell.Row.Cells["CloseDebitAmountOC"].Value != null && (decimal)cell.Row.Cells["CloseDebitAmountOC"].Value == 0)
                    cell.Row.Cells["CloseDebitAmountOC"].Value = null;
                if ((cell.Row.Cells["CloseDebitAmountOC"].Value != null && (decimal)cell.Row.Cells["CloseDebitAmountOC"].Value < 0))
                    cell.Row.Cells["CloseDebitAmountOC"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("CloseCreditAmount"))
            {
                if (cell.Row.Cells["CloseCreditAmount"].Value != null && (decimal)cell.Row.Cells["CloseCreditAmount"].Value == 0)
                    cell.Row.Cells["CloseCreditAmount"].Value = null;
                if ((cell.Row.Cells["CloseCreditAmount"].Value != null && (decimal)cell.Row.Cells["CloseCreditAmount"].Value < 0))
                    cell.Row.Cells["CloseCreditAmount"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("CloseCreditAmountOC"))
            {
                if (cell.Row.Cells["CloseCreditAmountOC"].Value != null && (decimal)cell.Row.Cells["CloseCreditAmountOC"].Value == 0)
                    cell.Row.Cells["CloseCreditAmountOC"].Value = null;
                if ((cell.Row.Cells["CloseCreditAmountOC"].Value != null && (decimal)cell.Row.Cells["CloseCreditAmountOC"].Value < 0))
                    cell.Row.Cells["CloseCreditAmountOC"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("OpenningDebitAmount"))
            {
                if (cell.Row.Cells["OpenningDebitAmount"].Value != null && (decimal)cell.Row.Cells["OpenningDebitAmount"].Value == 0)
                    cell.Row.Cells["OpenningDebitAmount"].Value = null;
                if ((cell.Row.Cells["OpenningDebitAmount"].Value != null && (decimal)cell.Row.Cells["OpenningDebitAmount"].Value < 0))
                    cell.Row.Cells["OpenningDebitAmount"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("OpenningDebitAmountOC"))
            {
                if (cell.Row.Cells["OpenningDebitAmountOC"].Value != null && (decimal)cell.Row.Cells["OpenningDebitAmountOC"].Value == 0)
                    cell.Row.Cells["OpenningDebitAmountOC"].Value = null;
                if ((cell.Row.Cells["OpenningDebitAmountOC"].Value != null && (decimal)cell.Row.Cells["OpenningDebitAmountOC"].Value < 0))
                    cell.Row.Cells["OpenningDebitAmountOC"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("OpenningCreditAmount"))
            {
                if (cell.Row.Cells["OpenningCreditAmount"].Value != null && (decimal)cell.Row.Cells["OpenningCreditAmount"].Value == 0)
                    cell.Row.Cells["OpenningCreditAmount"].Value = null;
                if ((cell.Row.Cells["OpenningCreditAmount"].Value != null && (decimal)cell.Row.Cells["OpenningCreditAmount"].Value < 0))
                    cell.Row.Cells["OpenningCreditAmount"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("OpenningCreditAmountOC"))
            {
                if (cell.Row.Cells["OpenningCreditAmountOC"].Value != null && (decimal)cell.Row.Cells["OpenningCreditAmountOC"].Value == 0)
                    cell.Row.Cells["OpenningCreditAmountOC"].Value = null;
                if ((cell.Row.Cells["OpenningCreditAmountOC"].Value != null && (decimal)cell.Row.Cells["OpenningCreditAmountOC"].Value < 0))
                    cell.Row.Cells["OpenningCreditAmountOC"].Appearance.ForeColor = Color.Red;
            }

        }

        private void FSOKTCTTIENMATex_SizeChanged(object sender, EventArgs e)
        {
            var state = this.WindowState;
            switch (state)
            {
                case FormWindowState.Maximized:
                    break;
                case FormWindowState.Minimized:
                    break;
                case FormWindowState.Normal:
                    this.Left = (Screen.PrimaryScreen.Bounds.Width - this.Width) / 2;
                    this.Top = (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2;
                    this.Location = new Point((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2, (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
                    break;
                default:
                    break;
            }
        }

        private void FSOKTCTTIENMATex_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }

        private void ultraGridExcelExporter1_SummaryCellExported(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.SummaryCellExportedEventArgs e)
        {
            Worksheet ws = e.CurrentWorksheet;
            WorksheetRow wsRow = ws.Rows[e.CurrentRowIndex];
            WorksheetCell wsCell = wsRow.Cells[e.CurrentColumnIndex];
            if (!e.Summary.SummaryText.Contains("Số dòng") && !e.Summary.SummaryText.Contains(" "))
            {
                string s = e.Summary.SummaryText.Trim('.');
                if (s.Contains("("))
                    wsCell.Value = -decimal.Parse(s.Trim('(').Trim(')'));
                else
                    wsCell.Value = decimal.Parse(s);

                wsCell.CellFormat.FormatString = "###,###,###,###,##0;(###,###,###,###,##0)";
            }
        }
    }

}
