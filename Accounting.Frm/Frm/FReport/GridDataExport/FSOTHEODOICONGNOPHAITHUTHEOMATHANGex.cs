﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.AppStyling;
using System.IO;
using FX.Core;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinMaskedEdit;
using Infragistics.Win.UltraWinTabs;
using Accounting;
using Accounting.Core.DAO;
using System.Diagnostics;
using Accounting.Core.Domain.obj.Report;
using Infragistics.Documents.Excel;

namespace Accounting.Frm.FReport
{
    public partial class FSOTHEODOICONGNOPHAITHUTHEOMATHANGex : CustormForm
    {
        private string _subSystemCode;
        private List<SA_SoTheoDoiCongNoPhaiThuTheoMatHang> data = new List<SA_SoTheoDoiCongNoPhaiThuTheoMatHang>();
        private string list_item;
        private DateTime FromDate;
        private DateTime ToDate;
        private string period = "";
        ReportProcedureSDS sp = new ReportProcedureSDS();
        public FSOTHEODOICONGNOPHAITHUTHEOMATHANGex(DateTime FromDate1, DateTime ToDate1, string list_item1)
        {
            InitializeComponent();
            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\SoTheoDoiCongNoPhaiThuTheoMH.rst", path);

            List<SA_SoTheoDoiCongNoPhaiThuTheoMatHang> data1 = sp.GetSoTheoDoiCongNoPhaiThuTheoMatHang(FromDate1, ToDate1, list_item1);
            period = ReportUtils.GetPeriod(FromDate1, ToDate1);

            data = data1;
            list_item = list_item1;
            FromDate = FromDate1;
            ToDate = ToDate1;

            configGrid(data1);
            if (Utils.isDemo)
                btnExport.Visible = false;
            else
                btnExport.Visible = true;
        }

        private void configGrid(List<SA_SoTheoDoiCongNoPhaiThuTheoMatHang> lst)
        {
            uGrid.DataSource = lst;
            Utils.ConfigGrid(uGrid, ConstDatabase.SA_SoTheoDoiCongNoPhaiThuTheoMatHang_TableName);
            uGrid.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            uGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;

            uGrid.DisplayLayout.Bands[0].Columns["GiaTriHang"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["Thue"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["ChietKhau"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["GiamGia"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["TraLai"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["DaThanhToan"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["ConLai"].FormatNumberic(ConstDatabase.Format_TienVND);

            Utils.AddSumColumn(uGrid, "MaterialGoodsName", false);
            uGrid.DisplayLayout.Bands[0].Summaries[1].DisplayFormat = " ";

            Utils.AddSumColumn1(lst.Where(n => n.GiaTriHang != null).Sum(n => n.GiaTriHang), uGrid, "GiaTriHang", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.Thue != null).Sum(n => n.Thue), uGrid, "Thue", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.ChietKhau != null).Sum(n => n.ChietKhau), uGrid, "ChietKhau", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.GiamGia != null).Sum(n => n.GiamGia), uGrid, "GiamGia", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.TraLai != null).Sum(n => n.TraLai), uGrid, "TraLai", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.DaThanhToan != null).Sum(n => n.DaThanhToan), uGrid, "DaThanhToan", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.ConLai != null).Sum(n => n.ConLai), uGrid, "ConLai", false, constDatabaseFormat: 1);

            uGrid.DisplayLayout.Bands[0].Columns["MaterialGoodsCode"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["MaterialGoodsCode"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["MaterialGoodsCode"].Width = 150;
            uGrid.DisplayLayout.Bands[0].Columns["MaterialGoodsCode"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["MaterialGoodsName"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["MaterialGoodsName"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["MaterialGoodsName"].Width = 200;
            uGrid.DisplayLayout.Bands[0].Columns["MaterialGoodsName"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["GiaTriHang"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["GiaTriHang"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["GiaTriHang"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["GiaTriHang"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["Thue"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["Thue"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["Thue"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["Thue"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["ChietKhau"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["ChietKhau"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["ChietKhau"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["ChietKhau"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["GiamGia"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["GiamGia"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["GiamGia"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["GiamGia"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["TraLai"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["TraLai"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["TraLai"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["TraLai"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["DaThanhToan"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["DaThanhToan"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["DaThanhToan"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["DaThanhToan"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["ConLai"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["ConLai"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["ConLai"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["ConLai"].CellAppearance.TextVAlign = VAlign.Middle;

            GridLayout();

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (data.Count == 0)
                MSG.Warning("Không có dữ liệu");
            else
            {
                var rD = new SA_SoTheoDoiCongNoPhaiThuTheoMatHang_period();
                rD.Period = ReportUtils.GetPeriod(FromDate, ToDate);
                var f = new ReportForm<SA_SoTheoDoiCongNoPhaiThuTheoMatHang>(fileReportSlot1, _subSystemCode);
                f.AddDatasource("DBTHEODOICONGNOPT", data, true);
                f.AddDatasource("Detail", rD);
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }


        private void btnExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog
            {
                Title = "Chọn nơi sao lưu ",
                Filter = "File (*.xls)|*.xls",
                InitialDirectory = "@C:\\",
                FileName = "sotheodoicongnophaithutheomathang.xls"
            };
            if (save.ShowDialog() == DialogResult.OK)
            {
                string duongdan = save.FileName;
                try
                {
                    ultraGridExcelExporter1.Export(uGrid, duongdan);
                    if (MSG.Question("Kết xuất thành công, Bạn có muốn mở file vừa kết xuất không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(save.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
                catch
                {
                    MSG.Warning("Lỗi khi kết xuất !");
                }
            }
        }
        private void GridLayout()
        {
            UltraGridBand parentBand = this.uGrid.DisplayLayout.Bands[0];
            parentBand.RowLayoutStyle = RowLayoutStyle.GroupLayout;

            UltraGridGroup parentBandGroupPeriod;
            if (parentBand.Groups.Exists("ParentBandGroupPeriod"))
            {
                parentBandGroupPeriod = parentBand.Groups["ParentBandGroupPeriod"];
                parentBandGroupPeriod.Header.Caption = period;
            }
            else
                parentBandGroupPeriod = parentBand.Groups.Add("ParentBandGroupPeriod", period);
            parentBandGroupPeriod.Header.Appearance.FontData.Italic = DefaultableBoolean.True;
            parentBandGroupPeriod.Header.Appearance.FontData.SizeInPoints = 10;
            parentBandGroupPeriod.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupPeriod.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPeriod.Header.Appearance.BorderColor = Color.Black;

            UltraGridGroup parentBandGroupTitle;
            if (parentBand.Groups.Exists("ParentBandGroupTitle"))
                parentBandGroupTitle = parentBand.Groups["ParentBandGroupTitle"];
            else
                parentBandGroupTitle = parentBand.Groups.Add("ParentBandGroupTitle", "SỔ THEO DÕI CÔNG NỢ PHẢI THU THEO MẶT HÀNG");
            parentBandGroupTitle.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroupTitle.Header.Appearance.FontData.SizeInPoints = 12;
            parentBandGroupTitle.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupTitle.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupTitle.Header.Appearance.BorderColor = Color.Black;

            parentBandGroupPeriod.RowLayoutGroupInfo.ParentGroup = parentBandGroupTitle;

            parentBand.Columns["MaterialGoodsCode"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["MaterialGoodsCode"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["MaterialGoodsCode"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["MaterialGoodsCode"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["MaterialGoodsCode"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["MaterialGoodsCode"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["MaterialGoodsName"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["MaterialGoodsName"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["MaterialGoodsName"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["MaterialGoodsName"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["MaterialGoodsName"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["MaterialGoodsName"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["GiaTriHang"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["GiaTriHang"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GiaTriHang"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["GiaTriHang"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["GiaTriHang"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GiaTriHang"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["Thue"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["Thue"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Thue"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["Thue"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["Thue"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Thue"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["ChietKhau"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["ChietKhau"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ChietKhau"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["ChietKhau"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["ChietKhau"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ChietKhau"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["GiamGia"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["GiamGia"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GiamGia"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["GiamGia"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["GiamGia"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GiamGia"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["TraLai"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["TraLai"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["TraLai"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["TraLai"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["TraLai"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["TraLai"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["DaThanhToan"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["DaThanhToan"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["DaThanhToan"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["DaThanhToan"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["DaThanhToan"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["DaThanhToan"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["ConLai"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["ConLai"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ConLai"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["ConLai"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["ConLai"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ConLai"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["MaterialGoodsCode"].RowLayoutColumnInfo.OriginX = 0;
            parentBand.Columns["MaterialGoodsCode"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["MaterialGoodsCode"].RowLayoutColumnInfo.SpanX = 150;
            parentBand.Columns["MaterialGoodsCode"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["MaterialGoodsName"].RowLayoutColumnInfo.OriginX = 150;
            parentBand.Columns["MaterialGoodsName"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["MaterialGoodsName"].RowLayoutColumnInfo.SpanX = 200;
            parentBand.Columns["MaterialGoodsName"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["GiaTriHang"].RowLayoutColumnInfo.OriginX = 350;
            parentBand.Columns["GiaTriHang"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["GiaTriHang"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["GiaTriHang"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["Thue"].RowLayoutColumnInfo.OriginX = 600;
            parentBand.Columns["Thue"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["Thue"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["Thue"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["ChietKhau"].RowLayoutColumnInfo.OriginX = 850;
            parentBand.Columns["ChietKhau"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["ChietKhau"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["ChietKhau"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["GiamGia"].RowLayoutColumnInfo.OriginX = 1100;
            parentBand.Columns["GiamGia"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["GiamGia"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["GiamGia"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["TraLai"].RowLayoutColumnInfo.OriginX = 1350;
            parentBand.Columns["TraLai"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["TraLai"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["TraLai"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["DaThanhToan"].RowLayoutColumnInfo.OriginX = 1600;
            parentBand.Columns["DaThanhToan"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["DaThanhToan"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["DaThanhToan"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["ConLai"].RowLayoutColumnInfo.OriginX = 1850;
            parentBand.Columns["ConLai"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["ConLai"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["ConLai"].RowLayoutColumnInfo.SpanY = 32;

        }

        private void ultraGridExcelExporter1_InitializeColumn(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.InitializeColumnEventArgs e)
        {
            e.ExcelFormatStr = e.Column.Format;
        }

        private void ultraGridExcelExporter1_BeginExport(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.BeginExportEventArgs e)
        {
            foreach (var row in uGrid.Rows)
            {
                foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
                {
                    e.Rows[row.Index].Cells[column].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                    e.Rows[row.Index].Cells[column].Appearance.BorderColor = Color.Black;
                    e.Rows[row.Index].Cells[column].Column.Width = 200;
                }
            }
            e.Rows.Band.Summaries[0].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[0].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[1].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[1].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[2].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[2].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[3].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[3].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[4].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[4].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[5].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[5].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[6].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[6].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[7].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[7].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[8].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[8].Appearance.BorderColor = Color.Black;

        }

        private void uGrid_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            UltraGridCell cell = e.Row.Cells[0];
            cell.Row.Height = 25;

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("GiaTriHang"))
            {
                if (cell.Row.Cells["GiaTriHang"].Value != null && (decimal)cell.Row.Cells["GiaTriHang"].Value == 0)
                    cell.Row.Cells["GiaTriHang"].Value = null;
                if ((cell.Row.Cells["GiaTriHang"].Value != null && (decimal)cell.Row.Cells["GiaTriHang"].Value < 0))
                    cell.Row.Cells["GiaTriHang"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("Thue"))
            {
                if (cell.Row.Cells["Thue"].Value != null && (decimal)cell.Row.Cells["Thue"].Value == 0)
                    cell.Row.Cells["Thue"].Value = null;
                if ((cell.Row.Cells["Thue"].Value != null && (decimal)cell.Row.Cells["Thue"].Value < 0))
                    cell.Row.Cells["Thue"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("ChietKhau"))
            {
                if (cell.Row.Cells["ChietKhau"].Value != null && (decimal)cell.Row.Cells["ChietKhau"].Value == 0)
                    cell.Row.Cells["ChietKhau"].Value = null;
                if ((cell.Row.Cells["ChietKhau"].Value != null && (decimal)cell.Row.Cells["ChietKhau"].Value < 0))
                    cell.Row.Cells["ChietKhau"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("GiamGia"))
            {
                if (cell.Row.Cells["GiamGia"].Value != null && (decimal)cell.Row.Cells["GiamGia"].Value == 0)
                    cell.Row.Cells["GiamGia"].Value = null;
                if ((cell.Row.Cells["GiamGia"].Value != null && (decimal)cell.Row.Cells["GiamGia"].Value < 0))
                    cell.Row.Cells["GiamGia"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("TraLai"))
            {
                if (cell.Row.Cells["TraLai"].Value != null && (decimal)cell.Row.Cells["TraLai"].Value == 0)
                    cell.Row.Cells["TraLai"].Value = null;
                if ((cell.Row.Cells["TraLai"].Value != null && (decimal)cell.Row.Cells["TraLai"].Value < 0))
                    cell.Row.Cells["TraLai"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("DaThanhToan"))
            {
                if (cell.Row.Cells["DaThanhToan"].Value != null && (decimal)cell.Row.Cells["DaThanhToan"].Value == 0)
                    cell.Row.Cells["DaThanhToan"].Value = null;
                if ((cell.Row.Cells["DaThanhToan"].Value != null && (decimal)cell.Row.Cells["DaThanhToan"].Value < 0))
                    cell.Row.Cells["DaThanhToan"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("ConLai"))
            {
                if (cell.Row.Cells["ConLai"].Value != null && (decimal)cell.Row.Cells["ConLai"].Value == 0)
                    cell.Row.Cells["ConLai"].Value = null;
                if ((cell.Row.Cells["ConLai"].Value != null && (decimal)cell.Row.Cells["ConLai"].Value < 0))
                    cell.Row.Cells["ConLai"].Appearance.ForeColor = Color.Red;
            }

        }

        private void FSOKTCTTIENMATex_SizeChanged(object sender, EventArgs e)
        {
            var state = this.WindowState;
            switch (state)
            {
                case FormWindowState.Maximized:
                    break;
                case FormWindowState.Minimized:
                    break;
                case FormWindowState.Normal:
                    this.Left = (Screen.PrimaryScreen.Bounds.Width - this.Width) / 2;
                    this.Top = (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2;
                    this.Location = new Point((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2, (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
                    break;
                default:
                    break;
            }
        }

        private void ultraGridExcelExporter1_SummaryCellExported(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.SummaryCellExportedEventArgs e)
        {
            Worksheet ws = e.CurrentWorksheet;
            WorksheetRow wsRow = ws.Rows[e.CurrentRowIndex];
            WorksheetCell wsCell = wsRow.Cells[e.CurrentColumnIndex];
            if (!e.Summary.SummaryText.Contains("Số dòng") && !e.Summary.SummaryText.Contains(" "))
            {
                string s = e.Summary.SummaryText.Trim('.');
                if (s.Contains("("))
                    wsCell.Value = -decimal.Parse(s.Trim('(').Trim(')'));
                else
                    wsCell.Value = decimal.Parse(s);

                wsCell.CellFormat.FormatString = "###,###,###,###,##0;(###,###,###,###,##0)";
            }
        }
    }

}
