﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.AppStyling;
using System.IO;
using FX.Core;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinMaskedEdit;
using Infragistics.Win.UltraWinTabs;
using Accounting;
using Accounting.Core.DAO;
using System.Diagnostics;
using Accounting.Core.Domain.obj.Report;
using Infragistics.Documents.Excel;

namespace Accounting.Frm.FReport
{
    public partial class FCHITIETCONGNOPHAITHUKHex : CustormForm
    {
        private string _subSystemCode;
        private List<SA_ReceivableDetail> data = new List<SA_ReceivableDetail>();

        private string CurrencyID;
        private string Acount;
        private string list_acount;
        private DateTime FromDate;
        private DateTime ToDate;
        private string period = "";
        ReportProcedureSDS sp = new ReportProcedureSDS();
        public FCHITIETCONGNOPHAITHUKHex(DateTime FromDate1, DateTime ToDate1, string CurrencyID1, string Acount1, string list_acount1)
        {
            InitializeComponent();
            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\ChiTietCongNoPhaiThuKH.rst", path);

            List<SA_ReceivableDetail> data1 = sp.GetCHICONGNOPHAITHUKH(FromDate1, ToDate1, CurrencyID1, Acount1, list_acount1, false).ToList();

            string txtThoiGian = "Tài khoản: " + Acount1 + "; Loại tiền: " + CurrencyID1;
            period = txtThoiGian + "; " + ReportUtils.GetPeriod(FromDate1, ToDate1);

            data = data1;
            Acount = Acount1;
            CurrencyID = CurrencyID1;
            list_acount = list_acount1;
            FromDate = FromDate1;
            ToDate = ToDate1;
            
            configGrid(data);

            if (Utils.isDemo)
                btnExport.Visible = false;
            else
                btnExport.Visible = true;

        }

        private void configGrid(List<SA_ReceivableDetail> lst)
        {
            uGrid.DataSource = lst;
            Utils.ConfigGrid(uGrid, ConstDatabase.SA_ReceivableDetail_TableName);
            uGrid.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            uGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;

            lst.ForEach(t =>
            {
                if (t.JournalMemo == null)
                    t.JournalMemo = "";
                else
                    t.JournalMemo = t.JournalMemo;
            });

            Utils.AddSumColumn(uGrid, "RefDate", false);
            Utils.AddSumColumn(uGrid, "RefNo", false);
            Utils.AddSumColumn(uGrid, "JournalMemo", false);
            Utils.AddSumColumn(uGrid, "AccountNumber", false);
            Utils.AddSumColumn(uGrid, "CorrespondingAccountNumber", false);

            uGrid.DisplayLayout.Bands[0].Summaries[1].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[2].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[3].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[4].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[5].DisplayFormat = " ";

            if (CurrencyID == "VND")
            {
                uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGrid.DisplayLayout.Bands[0].Columns["ClosingDebitAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGrid.DisplayLayout.Bands[0].Columns["ClosingCreditAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGrid.DisplayLayout.Bands[0].Columns["DebitAmountOC"].Hidden = true;
                uGrid.DisplayLayout.Bands[0].Columns["CreditAmountOC"].Hidden = true;
                uGrid.DisplayLayout.Bands[0].Columns["ClosingDebitAmountOC"].Hidden = true;
                uGrid.DisplayLayout.Bands[0].Columns["ClosingCreditAmountOC"].Hidden = true;

                Utils.AddSumColumn1(lst.Where(n => n.DebitAmount != null && n.JournalMemo == "Cộng nhóm").Sum(n => n.DebitAmount), uGrid, "DebitAmount", false, constDatabaseFormat: 1);
                Utils.AddSumColumn1(lst.Where(n => n.CreditAmount != null && n.JournalMemo == "Cộng nhóm").Sum(n => n.CreditAmount), uGrid, "CreditAmount", false, constDatabaseFormat: 1);
                Utils.AddSumColumn1(lst.Where(n => n.ClosingDebitAmount != null && n.JournalMemo == "Cộng nhóm").Sum(n => n.ClosingDebitAmount), uGrid, "ClosingDebitAmount", false, constDatabaseFormat: 1);
                Utils.AddSumColumn1(lst.Where(n => n.ClosingCreditAmount != null && n.JournalMemo == "Cộng nhóm").Sum(n => n.ClosingCreditAmount), uGrid, "ClosingCreditAmount", false, constDatabaseFormat: 1);

            }
            else
            {
                uGrid.DisplayLayout.Bands[0].Columns["DebitAmountOC"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                uGrid.DisplayLayout.Bands[0].Columns["CreditAmountOC"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                uGrid.DisplayLayout.Bands[0].Columns["ClosingDebitAmountOC"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                uGrid.DisplayLayout.Bands[0].Columns["ClosingCreditAmountOC"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"].Hidden = true;
                uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"].Hidden = true;
                uGrid.DisplayLayout.Bands[0].Columns["ClosingDebitAmount"].Hidden = true;
                uGrid.DisplayLayout.Bands[0].Columns["ClosingCreditAmount"].Hidden = true;

                Utils.AddSumColumn1(lst.Where(n => n.CreditAmountOC != null && n.JournalMemo == "Cộng nhóm").Sum(n => n.CreditAmountOC), uGrid, "CreditAmountOC", false, constDatabaseFormat: 2);
                Utils.AddSumColumn1(lst.Where(n => n.DebitAmountOC != null && n.JournalMemo == "Cộng nhóm").Sum(n => n.DebitAmountOC), uGrid, "DebitAmountOC", false, constDatabaseFormat: 2);
                Utils.AddSumColumn1(lst.Where(n => n.ClosingDebitAmountOC != null && n.JournalMemo == "Cộng nhóm").Sum(n => n.ClosingDebitAmountOC), uGrid, "ClosingDebitAmountOC", false, constDatabaseFormat: 2);
                Utils.AddSumColumn1(lst.Where(n => n.ClosingCreditAmountOC != null && n.JournalMemo == "Cộng nhóm").Sum(n => n.ClosingCreditAmountOC), uGrid, "ClosingCreditAmountOC", false, constDatabaseFormat: 2);

            }

            GridLayout();
           
            uGrid.DisplayLayout.Bands[0].Columns["PostedDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["PostedDate"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["PostedDate"].Width = 120;

            uGrid.DisplayLayout.Bands[0].Columns["RefDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["RefDate"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["RefDate"].Width = 120;

            uGrid.DisplayLayout.Bands[0].Columns["RefNo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["RefNo"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["RefNo"].Width = 120;
            uGrid.DisplayLayout.Bands[0].Columns["RefNo"].CellAppearance.TextHAlign = HAlign.Left;
            uGrid.DisplayLayout.Bands[0].Columns["RefNo"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["JournalMemo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["JournalMemo"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["JournalMemo"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["JournalMemo"].CellAppearance.TextHAlign = HAlign.Left;
            uGrid.DisplayLayout.Bands[0].Columns["JournalMemo"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["AccountNumber"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["AccountNumber"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["AccountNumber"].Width = 80;
            uGrid.DisplayLayout.Bands[0].Columns["AccountNumber"].CellAppearance.TextHAlign = HAlign.Center;
            uGrid.DisplayLayout.Bands[0].Columns["AccountNumber"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["CorrespondingAccountNumber"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["CorrespondingAccountNumber"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["CorrespondingAccountNumber"].Width = 80;
            uGrid.DisplayLayout.Bands[0].Columns["CorrespondingAccountNumber"].CellAppearance.TextHAlign = HAlign.Center;
            uGrid.DisplayLayout.Bands[0].Columns["CorrespondingAccountNumber"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["DebitAmountOC"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["DebitAmountOC"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["DebitAmountOC"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["DebitAmountOC"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["CreditAmountOC"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["CreditAmountOC"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["CreditAmountOC"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["CreditAmountOC"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["ClosingDebitAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["ClosingDebitAmount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["ClosingDebitAmount"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["ClosingDebitAmount"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["ClosingDebitAmountOC"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["ClosingDebitAmountOC"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["ClosingDebitAmountOC"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["ClosingDebitAmountOC"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["ClosingCreditAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["ClosingCreditAmount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["ClosingCreditAmount"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["ClosingCreditAmount"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["ClosingCreditAmountOC"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["ClosingCreditAmountOC"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["ClosingCreditAmountOC"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["ClosingCreditAmountOC"].CellAppearance.TextVAlign = VAlign.Middle;

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            List<SA_ReceivableDetail> data = sp.GetCHICONGNOPHAITHUKH(FromDate, ToDate, CurrencyID, Acount, list_acount, false).ToList();
            if (data.Count == 0)
            {
                MSG.Warning("Không có dữ liệu để lên báo cáo!");
                return;
            }
            else
            {
                data.ForEach(t =>
                {
                    if (t.JournalMemo == null)
                        t.JournalMemo = "";
                    else
                        t.JournalMemo = t.JournalMemo;
                });

                data = data.Where(n => !n.JournalMemo.Contains("Tên khách hàng: ") && n.JournalMemo != "Cộng nhóm" /*&& n.JournalMemo != "TỔNG CỘNG"*/).ToList();
                var data_group = data.Where(t => t.AccountObjectCode != null).ToList();
                var data_group_acount = data.Where(t => t.AccountObjectCode != null).Select(m => m.AccountObjectCode).Distinct();
                foreach (var item in data_group_acount)
                {
                    var list_item = data_group.Where(t => t.AccountObjectCode == item).ToList();
                    //list_item[list_item.Count() - 1].Sum_DuCo = list_item[list_item.Count() - 1].ClosingCreditAmountOC; //comment by cuongpv
                    //list_item[list_item.Count() - 1].Sum_DuNo = list_item[list_item.Count() - 1].ClosingDebitAmountOC; //comment by cuongpv
                    //add by cuongpv
                    if (CurrencyID == "VND")
                    {
                        list_item[list_item.Count() - 1].Sum_DuCo = list_item[list_item.Count() - 1].ClosingCreditAmount;
                        list_item[list_item.Count() - 1].Sum_DuNo = list_item[list_item.Count() - 1].ClosingDebitAmount;
                    }
                    else
                    {
                        list_item[list_item.Count() - 1].Sum_DuCo = list_item[list_item.Count() - 1].ClosingCreditAmountOC;
                        list_item[list_item.Count() - 1].Sum_DuNo = list_item[list_item.Count() - 1].ClosingDebitAmountOC;
                    }
                    //end add by cuongpv
                }
                var rD = new SA_ReceivableDetail_period();
                string txtThoiGian = "Tài khoản: " + Acount + "; Loại tiền: " + CurrencyID;
                rD.Period = txtThoiGian + "; " + ReportUtils.GetPeriod(FromDate, ToDate);
                var f = new ReportForm<SA_ReceivableDetail>(fileReportSlot1, _subSystemCode);
                f.AddDatasource("DBCHITIETNOPT", data, true);
                f.AddDatasource("Detail", rD);
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }


        private void btnExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog
            {
                Title = "Chọn nơi sao lưu ",
                Filter = "File (*.xls)|*.xls",
                InitialDirectory = "@C:\\",
                FileName = "chitietcongnophaithuKH.xls"
            };
            if (save.ShowDialog() == DialogResult.OK)
            {
                string duongdan = save.FileName;
                try
                {
                    ultraGridExcelExporter1.Export(uGrid, duongdan);
                    if (MSG.Question("Kết xuất thành công, Bạn có muốn mở file vừa kết xuất không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(save.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
                catch
                {
                    MSG.Warning("Lỗi khi kết xuất !");
                }
            }
        }
        private void GridLayout()
        {
            UltraGridBand parentBand = this.uGrid.DisplayLayout.Bands[0];
            parentBand.RowLayoutStyle = RowLayoutStyle.GroupLayout;

            UltraGridGroup parentBandGroupPeriod;
            if (parentBand.Groups.Exists("ParentBandGroupPeriod"))
            {
                parentBandGroupPeriod = parentBand.Groups["ParentBandGroupPeriod"];
                parentBandGroupPeriod.Header.Caption = period;
            }
            else
                parentBandGroupPeriod = parentBand.Groups.Add("ParentBandGroupPeriod", period);
            parentBandGroupPeriod.Header.Appearance.FontData.Italic = DefaultableBoolean.True;
            parentBandGroupPeriod.Header.Appearance.FontData.SizeInPoints = 10;
            parentBandGroupPeriod.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupPeriod.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPeriod.Header.Appearance.BorderColor = Color.Black;

            UltraGridGroup parentBandGroupTitle;
            if (parentBand.Groups.Exists("ParentBandGroupTitle"))
                parentBandGroupTitle = parentBand.Groups["ParentBandGroupTitle"];
            else
                parentBandGroupTitle = parentBand.Groups.Add("ParentBandGroupTitle", "SỔ CHI TIẾT CÔNG NỢ PHẢI THU KHÁCH HÀNG");
            parentBandGroupTitle.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroupTitle.Header.Appearance.FontData.SizeInPoints = 12;
            parentBandGroupTitle.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupTitle.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupTitle.Header.Appearance.BorderColor = Color.Black;

            UltraGridGroup parentBandGroupPS;
            if (parentBand.Groups.Exists("ParentBandGroupPS"))
                parentBandGroupPS = parentBand.Groups["ParentBandGroupPS"];
            else
                parentBandGroupPS = parentBand.Groups.Add("ParentBandGroupPS", "Số phát sinh");
            parentBandGroupPS.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            parentBandGroupPS.Header.Appearance.FontData.SizeInPoints = 9;
            parentBandGroupPS.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupPS.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPS.Header.Appearance.BorderColor = Color.Black;
            parentBandGroupPS.Header.Appearance.BackColorAlpha = Alpha.Default;

            UltraGridGroup parentBandGroupSD;
            if (parentBand.Groups.Exists("ParentBandGroupSD"))
                parentBandGroupSD = parentBand.Groups["ParentBandGroupSD"];
            else
                parentBandGroupSD = parentBand.Groups.Add("ParentBandGroupSD", "Số dư");
            parentBandGroupSD.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            parentBandGroupSD.Header.Appearance.FontData.SizeInPoints = 9;
            parentBandGroupSD.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupSD.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupSD.Header.Appearance.BorderColor = Color.Black;
            parentBandGroupSD.Header.Appearance.BackColorAlpha = Alpha.Default;

            parentBandGroupPeriod.RowLayoutGroupInfo.ParentGroup = parentBandGroupTitle;

            if (CurrencyID == "VND")
            {
                parentBand.Columns["DebitAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPS;
                parentBand.Columns["DebitAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["DebitAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["DebitAmount"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["DebitAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["DebitAmount"].Header.Appearance.BorderColor = Color.Black;

                parentBand.Columns["CreditAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPS;
                parentBand.Columns["CreditAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["CreditAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["CreditAmount"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["CreditAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["CreditAmount"].Header.Appearance.BorderColor = Color.Black;

                parentBand.Columns["ClosingDebitAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupSD;
                parentBand.Columns["ClosingDebitAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["ClosingDebitAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["ClosingDebitAmount"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["ClosingDebitAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["ClosingDebitAmount"].Header.Appearance.BorderColor = Color.Black;

                parentBand.Columns["ClosingCreditAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupSD;
                parentBand.Columns["ClosingCreditAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["ClosingCreditAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["ClosingCreditAmount"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["ClosingCreditAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["ClosingCreditAmount"].Header.Appearance.BorderColor = Color.Black;

            }
            else
            {
                parentBand.Columns["DebitAmountOC"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPS;
                parentBand.Columns["DebitAmountOC"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["DebitAmountOC"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["DebitAmountOC"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["DebitAmountOC"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["DebitAmountOC"].Header.Appearance.BorderColor = Color.Black;

                parentBand.Columns["CreditAmountOC"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPS;
                parentBand.Columns["CreditAmountOC"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["CreditAmountOC"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["CreditAmountOC"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["CreditAmountOC"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["CreditAmountOC"].Header.Appearance.BorderColor = Color.Black;

                parentBand.Columns["ClosingDebitAmountOC"].RowLayoutColumnInfo.ParentGroup = parentBandGroupSD;
                parentBand.Columns["ClosingDebitAmountOC"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["ClosingDebitAmountOC"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["ClosingDebitAmountOC"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["ClosingDebitAmountOC"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["ClosingDebitAmountOC"].Header.Appearance.BorderColor = Color.Black;

                parentBand.Columns["ClosingCreditAmountOC"].RowLayoutColumnInfo.ParentGroup = parentBandGroupSD;
                parentBand.Columns["ClosingCreditAmountOC"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["ClosingCreditAmountOC"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["ClosingCreditAmountOC"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["ClosingCreditAmountOC"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["ClosingCreditAmountOC"].Header.Appearance.BorderColor = Color.Black;
            }

            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["PostedDate"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["PostedDate"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["PostedDate"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["PostedDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["PostedDate"].Header.Appearance.BorderColor = Color.Black;


            parentBand.Columns["RefDate"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["RefDate"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["RefDate"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["RefDate"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["RefDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["RefDate"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["RefNo"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["RefNo"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["RefNo"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["RefNo"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["RefNo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["RefNo"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["JournalMemo"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["JournalMemo"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["JournalMemo"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["JournalMemo"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["JournalMemo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["JournalMemo"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["AccountNumber"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["AccountNumber"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["AccountNumber"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["AccountNumber"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["AccountNumber"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["AccountNumber"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["CorrespondingAccountNumber"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["CorrespondingAccountNumber"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["CorrespondingAccountNumber"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["CorrespondingAccountNumber"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["CorrespondingAccountNumber"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["CorrespondingAccountNumber"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.OriginX = 0;
            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.SpanX = 80;
            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["RefDate"].RowLayoutColumnInfo.OriginX = 80;
            parentBand.Columns["RefDate"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["RefDate"].RowLayoutColumnInfo.SpanX = 80;
            parentBand.Columns["RefDate"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["RefNo"].RowLayoutColumnInfo.OriginX = 160;
            parentBand.Columns["RefNo"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["RefNo"].RowLayoutColumnInfo.SpanX = 100;
            parentBand.Columns["RefNo"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["JournalMemo"].RowLayoutColumnInfo.OriginX = 260;
            parentBand.Columns["JournalMemo"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["JournalMemo"].RowLayoutColumnInfo.SpanX = 300;
            parentBand.Columns["JournalMemo"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["AccountNumber"].RowLayoutColumnInfo.OriginX = 560;
            parentBand.Columns["AccountNumber"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["AccountNumber"].RowLayoutColumnInfo.SpanX = 80;
            parentBand.Columns["AccountNumber"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["CorrespondingAccountNumber"].RowLayoutColumnInfo.OriginX = 640;
            parentBand.Columns["CorrespondingAccountNumber"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["CorrespondingAccountNumber"].RowLayoutColumnInfo.SpanX = 80;
            parentBand.Columns["CorrespondingAccountNumber"].RowLayoutColumnInfo.SpanY = 32;

            parentBandGroupPS.RowLayoutGroupInfo.OriginX = 720;
            parentBandGroupPS.RowLayoutGroupInfo.OriginY = 0;
            parentBandGroupPS.RowLayoutGroupInfo.SpanX = 360;
            parentBandGroupPS.RowLayoutGroupInfo.SpanY = 32;

            parentBandGroupSD.RowLayoutGroupInfo.OriginX = 1080;
            parentBandGroupSD.RowLayoutGroupInfo.OriginY = 0;
            parentBandGroupSD.RowLayoutGroupInfo.SpanX = 360;
            parentBandGroupSD.RowLayoutGroupInfo.SpanY = 32;



        }

        private void ultraGridExcelExporter1_InitializeColumn(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.InitializeColumnEventArgs e)
        {
            e.ExcelFormatStr = e.Column.Format;
        }

        private void ultraGridExcelExporter1_BeginExport(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.BeginExportEventArgs e)
        {
            foreach (var row in uGrid.Rows)
            {
                foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
                {
                    e.Rows[row.Index].Cells[column].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                    e.Rows[row.Index].Cells[column].Appearance.BorderColor = Color.Black;
                    e.Rows[row.Index].Cells[column].Column.Width = 200;
                }
            }
            e.Rows.Band.Summaries[0].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[0].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[1].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[1].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[2].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[2].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[3].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[3].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[4].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[4].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[5].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[5].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[6].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[6].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[7].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[7].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[8].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[8].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[9].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[9].Appearance.BorderColor = Color.Black;

        }
        private void reportManager1_GetReportParameter(object sender, PerpetuumSoft.Reporting.Components.GetReportParameterEventArgs e)
        {
            e.Parameters["CurrencyID"].Value = CurrencyID;
        }

        private void uGrid_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            UltraGridCell cell = e.Row.Cells[0];
            cell.Row.Height = 25;
            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("JournalMemo"))
                if (cell.Row.Cells["JournalMemo"].Value != null && (cell.Row.Cells["JournalMemo"].Value.ToString().Contains("Tên khách hàng: ") || cell.Row.Cells["JournalMemo"].Value.ToString() == "Cộng nhóm" 
                    || cell.Row.Cells["JournalMemo"].Value.ToString() == "Số dư đầu kỳ" /*|| cell.Row.Cells["JournalMemo"].Value.ToString() == "TỔNG CỘNG"*/))
                {
                    cell.Row.Cells["PostedDate"].Value = null;
                    cell.Row.Cells["RefDate"].Value = null;
                    cell.Row.Cells["JournalMemo"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    cell.Row.Cells["DebitAmount"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    cell.Row.Cells["DebitAmountOC"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    cell.Row.Cells["CreditAmount"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    cell.Row.Cells["CreditAmountOC"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    cell.Row.Cells["ClosingDebitAmount"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    cell.Row.Cells["ClosingDebitAmountOC"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    cell.Row.Cells["ClosingCreditAmount"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    cell.Row.Cells["ClosingCreditAmountOC"].Appearance.FontData.Bold = DefaultableBoolean.True;
                }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("DebitAmount"))
            {
                if (cell.Row.Cells["DebitAmount"].Value != null && (decimal)cell.Row.Cells["DebitAmount"].Value == 0)
                    cell.Row.Cells["DebitAmount"].Value = null;
                if ((cell.Row.Cells["DebitAmount"].Value != null && (decimal)cell.Row.Cells["DebitAmount"].Value < 0))
                    cell.Row.Cells["DebitAmount"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("DebitAmountOC"))
            {
                if (cell.Row.Cells["DebitAmountOC"].Value != null && (decimal)cell.Row.Cells["DebitAmountOC"].Value == 0)
                    cell.Row.Cells["DebitAmountOC"].Value = null;
                if ((cell.Row.Cells["DebitAmountOC"].Value != null && (decimal)cell.Row.Cells["DebitAmountOC"].Value < 0))
                    cell.Row.Cells["DebitAmountOC"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("CreditAmount"))
            {
                if (cell.Row.Cells["CreditAmount"].Value != null && (decimal)cell.Row.Cells["CreditAmount"].Value == 0)
                    cell.Row.Cells["CreditAmount"].Value = null;
                if ((cell.Row.Cells["CreditAmount"].Value != null && (decimal)cell.Row.Cells["CreditAmount"].Value < 0))
                    cell.Row.Cells["CreditAmount"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("CreditAmountOC"))
            {
                if (cell.Row.Cells["CreditAmountOC"].Value != null && (decimal)cell.Row.Cells["CreditAmountOC"].Value == 0)
                    cell.Row.Cells["CreditAmountOC"].Value = null;
                if ((cell.Row.Cells["CreditAmountOC"].Value != null && (decimal)cell.Row.Cells["CreditAmountOC"].Value < 0))
                    cell.Row.Cells["CreditAmountOC"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("ClosingDebitAmount"))
            {
                if (cell.Row.Cells["ClosingDebitAmount"].Value != null && (decimal)cell.Row.Cells["ClosingDebitAmount"].Value == 0)
                    cell.Row.Cells["ClosingDebitAmount"].Value = null;
                if ((cell.Row.Cells["ClosingDebitAmount"].Value != null && (decimal)cell.Row.Cells["ClosingDebitAmount"].Value < 0))
                    cell.Row.Cells["ClosingDebitAmount"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("ClosingDebitAmountOC"))
            {
                if (cell.Row.Cells["ClosingDebitAmountOC"].Value != null && (decimal)cell.Row.Cells["ClosingDebitAmountOC"].Value == 0)
                    cell.Row.Cells["ClosingDebitAmountOC"].Value = null;
                if ((cell.Row.Cells["ClosingDebitAmountOC"].Value != null && (decimal)cell.Row.Cells["ClosingDebitAmountOC"].Value < 0))
                    cell.Row.Cells["ClosingDebitAmountOC"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("ClosingCreditAmount"))
            {
                if (cell.Row.Cells["ClosingCreditAmount"].Value != null && (decimal)cell.Row.Cells["ClosingCreditAmount"].Value == 0)
                    cell.Row.Cells["ClosingCreditAmount"].Value = null;
                if ((cell.Row.Cells["ClosingCreditAmount"].Value != null && (decimal)cell.Row.Cells["ClosingCreditAmount"].Value < 0))
                    cell.Row.Cells["ClosingCreditAmount"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("ClosingCreditAmountOC"))
            {
                if (cell.Row.Cells["ClosingCreditAmountOC"].Value != null && (decimal)cell.Row.Cells["ClosingCreditAmountOC"].Value == 0)
                    cell.Row.Cells["ClosingCreditAmountOC"].Value = null;
                if ((cell.Row.Cells["ClosingCreditAmountOC"].Value != null && (decimal)cell.Row.Cells["ClosingCreditAmountOC"].Value < 0))
                    cell.Row.Cells["ClosingCreditAmountOC"].Appearance.ForeColor = Color.Red;
            }

        }

        private void FSOKTCTTIENMATex_SizeChanged(object sender, EventArgs e)
        {
            var state = this.WindowState;
            switch (state)
            {
                case FormWindowState.Maximized:
                    break;
                case FormWindowState.Minimized:
                    break;
                case FormWindowState.Normal:
                    this.Left = (Screen.PrimaryScreen.Bounds.Width - this.Width) / 2;
                    this.Top = (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2;
                    this.Location = new Point((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2, (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
                    break;
                default:
                    break;
            }
        }

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                SA_ReceivableDetail temp = (SA_ReceivableDetail)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                if (temp != null)
                {
                    if (temp.HasProperty("RefType"))
                    {
                        if (!temp.GetProperty("RefType").IsNullOrEmpty())
                        {
                            int typeid = temp.GetProperty("RefType").ToInt();

                            if (temp.HasProperty("RefID"))
                            {
                                if (!temp.GetProperty("RefID").IsNullOrEmpty())
                                {
                                    Guid id = (Guid)temp.GetProperty("RefID");
                                    editFuntion(temp);
                                }
                            }
                        }

                    }
                }
                    

            }
        }

        private void editFuntion(SA_ReceivableDetail temp)
        {
            int typeid = temp.GetProperty("RefType").ToInt();
            Guid id = (Guid)temp.GetProperty("RefID");
            var f = Utils.ViewVoucherSelected1(id, typeid);
            //List<SA_ReceivableDetail> data1 = sp.GetCHICONGNOPHAITHUKH(FromDate, ToDate, CurrencyID, Acount, list_acount, false).ToList();
            //configGrid(data1);


        }

        private void FSOKTCTTIENMATex_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }

        private void ultraGridExcelExporter1_SummaryCellExported(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.SummaryCellExportedEventArgs e)
        {
            Worksheet ws = e.CurrentWorksheet;
            WorksheetRow wsRow = ws.Rows[e.CurrentRowIndex];
            WorksheetCell wsCell = wsRow.Cells[e.CurrentColumnIndex];
            if (!e.Summary.SummaryText.Contains("Số dòng") && !e.Summary.SummaryText.Contains(" "))
            {
                string s = e.Summary.SummaryText.Trim('.');
                if (s.Contains("("))
                    wsCell.Value = -decimal.Parse(s.Trim('(').Trim(')'));
                else
                    wsCell.Value = decimal.Parse(s);

                wsCell.CellFormat.FormatString = "###,###,###,###,##0;(###,###,###,###,##0)";
            }
        }
    }

}
