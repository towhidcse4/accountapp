﻿using Accounting.Core.DAO;
using Accounting.Core.Domain.obj.Report;
using Accounting.TextMessage;
using Infragistics.Documents.Excel;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRBAOCAOLUUCHUYENTIENPPTTex : Form
    {
        private string _subSystemCode;
        ReportProcedureSDS sp = new ReportProcedureSDS();
        List<Proc_GetB03_DN_Model> data = new List<Proc_GetB03_DN_Model>();
        string period = "";
        DateTime FromDate;
        DateTime ToDate;
        string thamso = "";
        public FRBAOCAOLUUCHUYENTIENPPTTex(DateTime FromDate1, DateTime ToDate1, string thamso1)
        {
            InitializeComponent();
            ReportUtils.ProcessControls(this);
            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\B03_DNN.rst", path);
            List<Proc_GetB03_DN_Model> data1 = sp.GetProc_GetB03_DN(FromDate1, ToDate1, 0, "");
            period = ReportUtils.GetPeriod(FromDate1, ToDate1);
            data = data1.Where(t => (t.Amount != 0 || t.PrevAmount != 0)).ToList();
            FromDate = FromDate1;
            ToDate = ToDate1;
            thamso = thamso1;
            configGrid(data);

            if (Utils.isDemo)
                btnExport.Visible = false;
            else
                btnExport.Visible = true;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {           
            if (data.Count == 0)
            {
                MSG.Warning("Không có dữ liệu");
            }
            else
            {
                data = data.Where(t => (t.Amount != 0 || t.PrevAmount != 0)).ToList();
                if (data.Count == 0)
                {
                    MSG.Warning("Không có dữ liệu");
                }
                else
                {
                    var rD = new Proc_GetB03_DN_Model_Detail();
                    rD.Period = ReportUtils.GetPeriod(FromDate, ToDate);
                    rD.thamso = thamso;
                    var f = new ReportForm<Proc_GetB03_DN_Model>(fileReportSlot1, _subSystemCode);
                    f.AddDatasource("DB_BO3DNN", data, true);
                    f.AddDatasource("Detail", rD);
                    f.LoadReport();
                    f.WindowState = FormWindowState.Maximized;
                    f.Show();
                }
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog
            {
                Title = "Chọn nơi sao lưu ",
                Filter = "File (*.xls)|*.xls",
                InitialDirectory = "@C:\\",
                FileName = "baocaoluuchuyentienPPTT.xls"
            };
            if (save.ShowDialog() == DialogResult.OK)
            {
                string duongdan = save.FileName;
                try
                {
                    ultraGridExcelExporter1.Export(uGrid, duongdan);
                    if (MSG.Question("Kết xuất thành công, Bạn có muốn mở file vừa kết xuất không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(save.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
                catch
                {
                    MSG.Warning("Lỗi khi kết xuất !");
                }
            }
        }

        private void configGrid(List<Proc_GetB03_DN_Model> lst)
        {
            uGrid.DataSource = lst;
            Utils.ConfigGrid(uGrid, ConstDatabase.GetB01_DN_TableName);
            uGrid.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;

            uGrid.DisplayLayout.Bands[0].Columns["ItemName"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["ItemName"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["ItemName"].Width = 400;
            uGrid.DisplayLayout.Bands[0].Columns["ItemName"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["ItemCode"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["ItemCode"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["ItemCode"].CellAppearance.TextHAlign = HAlign.Center;
            uGrid.DisplayLayout.Bands[0].Columns["ItemCode"].CellAppearance.TextVAlign = VAlign.Middle;
            uGrid.DisplayLayout.Bands[0].Columns["ItemCode"].Width = 120;

            uGrid.DisplayLayout.Bands[0].Columns["Description"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["Description"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["Description"].CellAppearance.TextHAlign = HAlign.Center;
            uGrid.DisplayLayout.Bands[0].Columns["Description"].CellAppearance.TextVAlign = VAlign.Middle;
            uGrid.DisplayLayout.Bands[0].Columns["Description"].Width = 100;

            uGrid.DisplayLayout.Bands[0].Columns["Amount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["Amount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["Amount"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["Amount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["Amount"].Header.Caption = "Năm nay";
            uGrid.DisplayLayout.Bands[0].Columns["Amount"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["PrevAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["PrevAmount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["PrevAmount"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["PrevAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["PrevAmount"].Header.Caption = "Năm trước";
            uGrid.DisplayLayout.Bands[0].Columns["PrevAmount"].CellAppearance.TextVAlign = VAlign.Middle;

            if (thamso.Contains("Năm"))
            {
                uGrid.DisplayLayout.Bands[0].Columns["Amount"].Header.Caption = "Số năm nay";
                uGrid.DisplayLayout.Bands[0].Columns["PrevAmount"].Header.Caption = "Số năm trước";

            }
            else
            {
                uGrid.DisplayLayout.Bands[0].Columns["Amount"].Header.Caption = "Số kỳ này";
                uGrid.DisplayLayout.Bands[0].Columns["PrevAmount"].Header.Caption = "Số kỳ trước";
            }

            GridLayout();
        }

        private void GridLayout()
        {
            UltraGridBand parentBand = this.uGrid.DisplayLayout.Bands[0];
            parentBand.RowLayoutStyle = RowLayoutStyle.GroupLayout;

            UltraGridGroup parentBandGroupPeriod;
            if (parentBand.Groups.Exists("ParentBandGroupPeriod"))
            {
                parentBandGroupPeriod = parentBand.Groups["ParentBandGroupPeriod"];
                parentBandGroupPeriod.Header.Caption = period;
            }
            else
                parentBandGroupPeriod = parentBand.Groups.Add("ParentBandGroupPeriod", period);
            parentBandGroupPeriod.Header.Appearance.FontData.Italic = DefaultableBoolean.True;
            parentBandGroupPeriod.Header.Appearance.FontData.SizeInPoints = 10;
            parentBandGroupPeriod.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupPeriod.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPeriod.Header.Appearance.BorderColor = Color.Black;

            UltraGridGroup parentBandGroupTitle;
            if (parentBand.Groups.Exists("ParentBandGroupTitle"))
                parentBandGroupTitle = parentBand.Groups["ParentBandGroupTitle"];
            else
                parentBandGroupTitle = parentBand.Groups.Add("ParentBandGroupTitle", "BÁO CÁO LƯU CHUYỂN TIỀN TỆ");
            parentBandGroupTitle.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroupTitle.Header.Appearance.FontData.SizeInPoints = 12;
            parentBandGroupTitle.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupTitle.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupTitle.Header.Appearance.BorderColor = Color.Black;

            parentBandGroupPeriod.RowLayoutGroupInfo.ParentGroup = parentBandGroupTitle;

            parentBand.Columns["ItemName"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["ItemName"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ItemName"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["ItemName"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["ItemName"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ItemName"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["ItemCode"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["ItemCode"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ItemCode"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["ItemCode"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["ItemCode"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ItemCode"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["Description"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["Description"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Description"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["Description"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["Description"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Description"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["Amount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["Amount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Amount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["Amount"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["Amount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Amount"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["PrevAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["PrevAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["PrevAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["PrevAmount"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["PrevAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["PrevAmount"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["ItemName"].RowLayoutColumnInfo.OriginX = 0;
            parentBand.Columns["ItemName"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["ItemName"].RowLayoutColumnInfo.SpanX = 400;
            parentBand.Columns["ItemName"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["ItemCode"].RowLayoutColumnInfo.OriginX = 400;
            parentBand.Columns["ItemCode"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["ItemCode"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["ItemCode"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["Description"].RowLayoutColumnInfo.OriginX = 520;
            parentBand.Columns["Description"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["Description"].RowLayoutColumnInfo.SpanX = 100;
            parentBand.Columns["Description"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["Amount"].RowLayoutColumnInfo.OriginX = 620;
            parentBand.Columns["Amount"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["Amount"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["Amount"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["PrevAmount"].RowLayoutColumnInfo.OriginX = 870;
            parentBand.Columns["PrevAmount"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["PrevAmount"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["PrevAmount"].RowLayoutColumnInfo.SpanY = 32;
        }

        private void uGrid_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            UltraGridCell cell = e.Row.Cells[0];
            cell.Row.Height = 25;

            if ((bool)cell.Row.Cells["IsBold"].Value == true)
            {
                cell.Row.Cells["ItemName"].Appearance.FontData.Bold = DefaultableBoolean.True;
                cell.Row.Cells["ItemCode"].Appearance.FontData.Bold = DefaultableBoolean.True;
                cell.Row.Cells["Description"].Appearance.FontData.Bold = DefaultableBoolean.True;
                cell.Row.Cells["Amount"].Appearance.FontData.Bold = DefaultableBoolean.True;
                cell.Row.Cells["PrevAmount"].Appearance.FontData.Bold = DefaultableBoolean.True;
            }

            if ((bool)cell.Row.Cells["IsItalic"].Value == true)
            {
                cell.Row.Cells["ItemName"].Appearance.FontData.Italic = DefaultableBoolean.True;
                cell.Row.Cells["ItemCode"].Appearance.FontData.Italic = DefaultableBoolean.True;
                cell.Row.Cells["Description"].Appearance.FontData.Italic = DefaultableBoolean.True;
                cell.Row.Cells["Amount"].Appearance.FontData.Italic = DefaultableBoolean.True;
                cell.Row.Cells["PrevAmount"].Appearance.FontData.Italic = DefaultableBoolean.True;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("Amount"))
            {
                if (cell.Row.Cells["Amount"].Value != null && (decimal)cell.Row.Cells["Amount"].Value == 0)
                    cell.Row.Cells["Amount"].Value = null;
                if ((cell.Row.Cells["Amount"].Value != null && (decimal)cell.Row.Cells["Amount"].Value < 0))
                    cell.Row.Cells["Amount"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("PrevAmount"))
            {
                if (cell.Row.Cells["PrevAmount"].Value != null && (decimal)cell.Row.Cells["PrevAmount"].Value == 0)
                    cell.Row.Cells["PrevAmount"].Value = null;
                if ((cell.Row.Cells["PrevAmount"].Value != null && (decimal)cell.Row.Cells["PrevAmount"].Value < 0))
                    cell.Row.Cells["PrevAmount"].Appearance.ForeColor = Color.Red;
            }

        }

        private void ultraGridExcelExporter1_InitializeColumn(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.InitializeColumnEventArgs e)
        {
            e.ExcelFormatStr = e.Column.Format;
        }

        private void ultraGridExcelExporter1_BeginExport(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.BeginExportEventArgs e)
        {
            if (e.Rows[0].Cells[0].Value != null && e.Rows[0].Cells[0].Value.ToString() == "0")
                e.Rows[0].Cells[0].Value = null;
        }

        private void FRB01aDNN_SizeChanged(object sender, EventArgs e)
        {
            var state = this.WindowState;
            switch (state)
            {
                case FormWindowState.Maximized:
                    break;
                case FormWindowState.Minimized:
                    break;
                case FormWindowState.Normal:
                    this.Left = (Screen.PrimaryScreen.Bounds.Width - this.Width) / 2;
                    this.Top = (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2;
                    this.Location = new Point((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2, (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
                    break;
                default:
                    break;
            }
        }

        private void ultraGridExcelExporter1_SummaryCellExported(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.SummaryCellExportedEventArgs e)
        {
            Worksheet ws = e.CurrentWorksheet;
            WorksheetRow wsRow = ws.Rows[e.CurrentRowIndex];
            WorksheetCell wsCell = wsRow.Cells[e.CurrentColumnIndex];
            if (!e.Summary.SummaryText.Contains("Số dòng"))
            {
                string s = e.Summary.SummaryText.Trim('.');
                if (s.Contains("("))
                    wsCell.Value = -decimal.Parse(s.Trim('(').Trim(')'));
                else
                    wsCell.Value = decimal.Parse(s);

                wsCell.CellFormat.FormatString = "###,###,###,###,##0;(###,###,###,###,##0)";
            }
        }
    }
}
