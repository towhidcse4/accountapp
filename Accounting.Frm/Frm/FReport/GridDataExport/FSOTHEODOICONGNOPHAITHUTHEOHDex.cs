﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.AppStyling;
using System.IO;
using FX.Core;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinMaskedEdit;
using Infragistics.Win.UltraWinTabs;
using Accounting;
using Accounting.Core.DAO;
using System.Diagnostics;
using Accounting.Core.Domain.obj.Report;
using Infragistics.Documents.Excel;

namespace Accounting.Frm.FReport
{
    public partial class FSOTHEODOICONGNOPHAITHUTHEOHDex : CustormForm
    {
        private string _subSystemCode;
        private List<SA_SoTheoDoiCongNoPhaiThuTheoHoaDon> data = new List<SA_SoTheoDoiCongNoPhaiThuTheoHoaDon>();
        private DateTime FromDate;
        private DateTime ToDate;
        private string lst_account;
        private string period = "";
        ReportProcedureSDS sp = new ReportProcedureSDS();
        public FSOTHEODOICONGNOPHAITHUTHEOHDex(DateTime FromDate1, DateTime ToDate1, string lst_account1)
        {
            InitializeComponent();
            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\SoTheoDoiCongNoPhaiThuTheoHoaDon.rst", path);

            List<SA_SoTheoDoiCongNoPhaiThuTheoHoaDon> data1 = sp.GetSoTheoDoiCongNoPhaiThuTheoHoaDon(FromDate1, ToDate1, lst_account1);
            period = ReportUtils.GetPeriod(FromDate1, ToDate1);

            data = data1;
            FromDate = FromDate1;
            ToDate = ToDate1;
            lst_account = lst_account1;

            configGrid(data1);
            if (Utils.isDemo)
                btnExport.Visible = false;
            else
                btnExport.Visible = true;

        }

        private void configGrid(List<SA_SoTheoDoiCongNoPhaiThuTheoHoaDon> lst)
        {
            uGrid.DataSource = lst;
            Utils.ConfigGrid(uGrid, ConstDatabase.SA_SoTheoDoiCongNoPhaiThuTheoHoaDon_TableName);
            uGrid.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            uGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;

            uGrid.DisplayLayout.Bands[0].Columns["GiaTriHoaDon"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["ChietKhauTT_GiamTruKhac"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["GiamGia"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["TraLai"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["SoDaThu"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["SoConPhaiThu"].FormatNumberic(ConstDatabase.Format_TienVND);

            Utils.AddSumColumn(uGrid, "InvoiceNo", false);
            Utils.AddSumColumn(uGrid, "AccountingObjectName", false);

            uGrid.DisplayLayout.Bands[0].Summaries[1].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[2].DisplayFormat = " ";

            Utils.AddSumColumn1(lst.Where(n => n.GiaTriHoaDon != null).Sum(n => n.GiaTriHoaDon), uGrid, "GiaTriHoaDon", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.ChietKhauTT_GiamTruKhac != null).Sum(n => n.ChietKhauTT_GiamTruKhac), uGrid, "ChietKhauTT_GiamTruKhac", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.GiamGia != null).Sum(n => n.GiamGia), uGrid, "GiamGia", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.TraLai != null).Sum(n => n.TraLai), uGrid, "TraLai", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.SoDaThu != null).Sum(n => n.SoDaThu), uGrid, "SoDaThu", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.SoConPhaiThu != null).Sum(n => n.SoConPhaiThu), uGrid, "SoConPhaiThu", false, constDatabaseFormat: 1);

            uGrid.DisplayLayout.Bands[0].Columns["InvoiceDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["InvoiceDate"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["InvoiceDate"].Width = 120;

            uGrid.DisplayLayout.Bands[0].Columns["InvoiceNo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["InvoiceNo"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["InvoiceNo"].Width = 120;
            uGrid.DisplayLayout.Bands[0].Columns["InvoiceNo"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectName"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectName"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectName"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectName"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["GiaTriHoaDon"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["GiaTriHoaDon"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["GiaTriHoaDon"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["GiaTriHoaDon"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["TraLai"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["TraLai"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["TraLai"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["TraLai"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["GiamGia"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["GiamGia"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["GiamGia"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["GiamGia"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["ChietKhauTT_GiamTruKhac"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["ChietKhauTT_GiamTruKhac"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["ChietKhauTT_GiamTruKhac"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["ChietKhauTT_GiamTruKhac"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["SoDaThu"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["SoDaThu"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["SoDaThu"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["SoDaThu"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["SoConPhaiThu"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["SoConPhaiThu"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["SoConPhaiThu"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["SoConPhaiThu"].CellAppearance.TextVAlign = VAlign.Middle;

            GridLayout();

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (data.Count == 0)
                MSG.Warning("Không có dữ liệu");
            else
            {
                var rD = new SA_SoTheoDoiCongNoPhaiThuTheoHoaDon_period();
                rD.Period = ReportUtils.GetPeriod(FromDate, ToDate);
                var f = new ReportForm<SA_SoTheoDoiCongNoPhaiThuTheoHoaDon>(fileReportSlot1, _subSystemCode);
                f.AddDatasource("DBTHEODOICONGNOHD", data, true);
                f.AddDatasource("Detail", rD);
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }


        private void btnExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog
            {
                Title = "Chọn nơi sao lưu ",
                Filter = "File (*.xls)|*.xls",
                InitialDirectory = "@C:\\",
                FileName = "sotheodoicongnophaithutheohoadon.xls"
            };
            if (save.ShowDialog() == DialogResult.OK)
            {
                string duongdan = save.FileName;
                try
                {
                    ultraGridExcelExporter1.Export(uGrid, duongdan);
                    if (MSG.Question("Kết xuất thành công, Bạn có muốn mở file vừa kết xuất không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(save.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
                catch
                {
                    MSG.Warning("Lỗi khi kết xuất !");
                }
            }
        }
        private void GridLayout()
        {
            UltraGridBand parentBand = this.uGrid.DisplayLayout.Bands[0];
            parentBand.RowLayoutStyle = RowLayoutStyle.GroupLayout;

            UltraGridGroup parentBandGroupPeriod;
            if (parentBand.Groups.Exists("ParentBandGroupPeriod"))
            {
                parentBandGroupPeriod = parentBand.Groups["ParentBandGroupPeriod"];
                parentBandGroupPeriod.Header.Caption = period;
            }
            else
                parentBandGroupPeriod = parentBand.Groups.Add("ParentBandGroupPeriod", period);
            parentBandGroupPeriod.Header.Appearance.FontData.Italic = DefaultableBoolean.True;
            parentBandGroupPeriod.Header.Appearance.FontData.SizeInPoints = 10;
            parentBandGroupPeriod.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupPeriod.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPeriod.Header.Appearance.BorderColor = Color.Black;

            UltraGridGroup parentBandGroupTitle;
            if (parentBand.Groups.Exists("ParentBandGroupTitle"))
                parentBandGroupTitle = parentBand.Groups["ParentBandGroupTitle"];
            else
                parentBandGroupTitle = parentBand.Groups.Add("ParentBandGroupTitle", "SỔ THEO DÕI CÔNG NỢ PHẢI THU THEO HÓA ĐƠN");
            parentBandGroupTitle.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroupTitle.Header.Appearance.FontData.SizeInPoints = 12;
            parentBandGroupTitle.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupTitle.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupTitle.Header.Appearance.BorderColor = Color.Black;

            parentBandGroupPeriod.RowLayoutGroupInfo.ParentGroup = parentBandGroupTitle;

            parentBand.Columns["InvoiceDate"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["InvoiceDate"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["InvoiceDate"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["InvoiceDate"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["InvoiceDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["InvoiceDate"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["InvoiceNo"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["InvoiceNo"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["InvoiceNo"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["InvoiceNo"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["InvoiceNo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["InvoiceNo"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["AccountingObjectName"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["AccountingObjectName"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["AccountingObjectName"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["AccountingObjectName"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["AccountingObjectName"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["AccountingObjectName"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["GiaTriHoaDon"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["GiaTriHoaDon"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GiaTriHoaDon"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["GiaTriHoaDon"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["GiaTriHoaDon"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GiaTriHoaDon"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["TraLai"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["TraLai"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["TraLai"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["TraLai"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["TraLai"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["TraLai"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["GiamGia"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["GiamGia"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GiamGia"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["GiamGia"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["GiamGia"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GiamGia"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["ChietKhauTT_GiamTruKhac"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["ChietKhauTT_GiamTruKhac"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ChietKhauTT_GiamTruKhac"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["ChietKhauTT_GiamTruKhac"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["ChietKhauTT_GiamTruKhac"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ChietKhauTT_GiamTruKhac"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["SoDaThu"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["SoDaThu"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SoDaThu"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["SoDaThu"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["SoDaThu"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SoDaThu"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["SoConPhaiThu"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["SoConPhaiThu"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SoConPhaiThu"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["SoConPhaiThu"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["SoConPhaiThu"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SoConPhaiThu"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["InvoiceDate"].RowLayoutColumnInfo.OriginX = 0;
            parentBand.Columns["InvoiceDate"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["InvoiceDate"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["InvoiceDate"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["InvoiceNo"].RowLayoutColumnInfo.OriginX = 120;
            parentBand.Columns["InvoiceNo"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["InvoiceNo"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["InvoiceNo"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["AccountingObjectName"].RowLayoutColumnInfo.OriginX = 240;
            parentBand.Columns["AccountingObjectName"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["AccountingObjectName"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["AccountingObjectName"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["GiaTriHoaDon"].RowLayoutColumnInfo.OriginX = 490;
            parentBand.Columns["GiaTriHoaDon"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["GiaTriHoaDon"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["GiaTriHoaDon"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["TraLai"].RowLayoutColumnInfo.OriginX = 740;
            parentBand.Columns["TraLai"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["TraLai"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["TraLai"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["GiamGia"].RowLayoutColumnInfo.OriginX = 990;
            parentBand.Columns["GiamGia"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["GiamGia"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["GiamGia"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["ChietKhauTT_GiamTruKhac"].RowLayoutColumnInfo.OriginX = 1240;
            parentBand.Columns["ChietKhauTT_GiamTruKhac"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["ChietKhauTT_GiamTruKhac"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["ChietKhauTT_GiamTruKhac"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["SoDaThu"].RowLayoutColumnInfo.OriginX = 1490;
            parentBand.Columns["SoDaThu"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["SoDaThu"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["SoDaThu"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["SoConPhaiThu"].RowLayoutColumnInfo.OriginX = 1740;
            parentBand.Columns["SoConPhaiThu"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["SoConPhaiThu"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["SoConPhaiThu"].RowLayoutColumnInfo.SpanY = 32;

        }

        private void ultraGridExcelExporter1_InitializeColumn(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.InitializeColumnEventArgs e)
        {
            e.ExcelFormatStr = e.Column.Format;
        }

        private void ultraGridExcelExporter1_BeginExport(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.BeginExportEventArgs e)
        {
            foreach (var row in uGrid.Rows)
            {
                foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
                {
                    e.Rows[row.Index].Cells[column].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                    e.Rows[row.Index].Cells[column].Appearance.BorderColor = Color.Black;
                    e.Rows[row.Index].Cells[column].Column.Width = 200;
                }
            }
            e.Rows.Band.Summaries[0].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[0].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[1].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[1].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[2].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[2].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[3].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[3].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[4].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[4].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[5].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[5].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[6].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[6].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[7].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[7].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[8].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[8].Appearance.BorderColor = Color.Black;

        }

        private void uGrid_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            UltraGridCell cell = e.Row.Cells[0];
            cell.Row.Height = 25;

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("GiaTriHoaDon"))
            {
                if (cell.Row.Cells["GiaTriHoaDon"].Value != null && (decimal)cell.Row.Cells["GiaTriHoaDon"].Value == 0)
                    cell.Row.Cells["GiaTriHoaDon"].Value = null;
                if ((cell.Row.Cells["GiaTriHoaDon"].Value != null && (decimal)cell.Row.Cells["GiaTriHoaDon"].Value < 0))
                    cell.Row.Cells["GiaTriHoaDon"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("TraLai"))
            {
                if (cell.Row.Cells["TraLai"].Value != null && (decimal)cell.Row.Cells["TraLai"].Value == 0)
                    cell.Row.Cells["TraLai"].Value = null;
                if ((cell.Row.Cells["TraLai"].Value != null && (decimal)cell.Row.Cells["TraLai"].Value < 0))
                    cell.Row.Cells["TraLai"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("GiamGia"))
            {
                if (cell.Row.Cells["GiamGia"].Value != null && (decimal)cell.Row.Cells["GiamGia"].Value == 0)
                    cell.Row.Cells["GiamGia"].Value = null;
                if ((cell.Row.Cells["GiamGia"].Value != null && (decimal)cell.Row.Cells["GiamGia"].Value < 0))
                    cell.Row.Cells["GiamGia"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("ChietKhauTT_GiamTruKhac"))
            {
                if (cell.Row.Cells["ChietKhauTT_GiamTruKhac"].Value != null && (decimal)cell.Row.Cells["TraLai"].Value == 0)
                    cell.Row.Cells["ChietKhauTT_GiamTruKhac"].Value = null;
                if ((cell.Row.Cells["ChietKhauTT_GiamTruKhac"].Value != null && (decimal)cell.Row.Cells["ChietKhauTT_GiamTruKhac"].Value < 0))
                    cell.Row.Cells["ChietKhauTT_GiamTruKhac"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("SoDaThu"))
            {
                if (cell.Row.Cells["SoDaThu"].Value != null && (decimal)cell.Row.Cells["SoDaThu"].Value == 0)
                    cell.Row.Cells["SoDaThu"].Value = null;
                if ((cell.Row.Cells["SoDaThu"].Value != null && (decimal)cell.Row.Cells["SoDaThu"].Value < 0))
                    cell.Row.Cells["SoDaThu"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("SoConPhaiThu"))
            {
                if (cell.Row.Cells["SoConPhaiThu"].Value != null && (decimal)cell.Row.Cells["SoConPhaiThu"].Value == 0)
                    cell.Row.Cells["SoConPhaiThu"].Value = null;
                if ((cell.Row.Cells["SoConPhaiThu"].Value != null && (decimal)cell.Row.Cells["SoConPhaiThu"].Value < 0))
                    cell.Row.Cells["SoConPhaiThu"].Appearance.ForeColor = Color.Red;
            }

        }

        private void FSOKTCTTIENMATex_SizeChanged(object sender, EventArgs e)
        {
            var state = this.WindowState;
            switch (state)
            {
                case FormWindowState.Maximized:
                    break;
                case FormWindowState.Minimized:
                    break;
                case FormWindowState.Normal:
                    this.Left = (Screen.PrimaryScreen.Bounds.Width - this.Width) / 2;
                    this.Top = (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2;
                    this.Location = new Point((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2, (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
                    break;
                default:
                    break;
            }
        }

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                SA_SoTheoDoiCongNoPhaiThuTheoHoaDon temp = (SA_SoTheoDoiCongNoPhaiThuTheoHoaDon)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                if (temp != null)
                    editFuntion(temp);

            }
        }

        private void editFuntion(SA_SoTheoDoiCongNoPhaiThuTheoHoaDon temp)
        {

            var f = Utils.ViewVoucherSelected1(temp.RefID, temp.RefType);
            //List<SA_SoTheoDoiCongNoPhaiThuTheoHoaDon> data1 = sp.GetSoTheoDoiCongNoPhaiThuTheoHoaDon(FromDate, ToDate, lst_account);
            //configGrid(data1);

        }

        private void ultraGridExcelExporter1_SummaryCellExported(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.SummaryCellExportedEventArgs e)
        {
            Worksheet ws = e.CurrentWorksheet;
            WorksheetRow wsRow = ws.Rows[e.CurrentRowIndex];
            WorksheetCell wsCell = wsRow.Cells[e.CurrentColumnIndex];
            if (!e.Summary.SummaryText.Contains("Số dòng") && !e.Summary.SummaryText.Contains(" "))
            {
                string s = e.Summary.SummaryText.Trim('.');
                if (s.Contains("("))
                    wsCell.Value = -decimal.Parse(s.Trim('(').Trim(')'));
                else
                    wsCell.Value = decimal.Parse(s);

                wsCell.CellFormat.FormatString = "###,###,###,###,##0;(###,###,###,###,##0)";
            }
        }
    }

}
