﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.AppStyling;
using System.IO;
using FX.Core;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinMaskedEdit;
using Infragistics.Win.UltraWinTabs;
using Accounting;
using Accounting.Core.DAO;
using System.Diagnostics;
using Accounting.Core.Domain.obj.Report;
using Infragistics.Documents.Excel;

namespace Accounting.Frm.FReport
{
    public partial class FSOTHEODOICONGNOPHAITHUTHEOHDBex : CustormForm
    {
        private string _subSystemCode;
        private List<SA_SoTheoDoiCongNoPhaiThuTheoHopDongBan> data = new List<SA_SoTheoDoiCongNoPhaiThuTheoHopDongBan>();
        private DateTime FromDate;
        private DateTime ToDate;
        private string lst_account;
        private string period = "";
        ReportProcedureSDS sp = new ReportProcedureSDS();
        public FSOTHEODOICONGNOPHAITHUTHEOHDBex(DateTime FromDate1, DateTime ToDate1, string lst_account1)
        {
            InitializeComponent();
            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\SoTheoDoiCongNoPhaiThuTheoHopDongBan.rst", path);

            List<SA_SoTheoDoiCongNoPhaiThuTheoHopDongBan> data1 = sp.GetSoTheoDoiCongNoTheoHopDongBan(FromDate1, ToDate1, lst_account1);
            period = ReportUtils.GetPeriod(FromDate1, ToDate1);

            data = data1;
            FromDate = FromDate1;
            ToDate = ToDate1;
            lst_account = lst_account1;

            configGrid(data1);
            if (Utils.isDemo)
                btnExport.Visible = false;
            else
                btnExport.Visible = true;
        }

        private void configGrid(List<SA_SoTheoDoiCongNoPhaiThuTheoHopDongBan> lst)
        {
            uGrid.DataSource = lst;
            Utils.ConfigGrid(uGrid, ConstDatabase.SA_SoTheoDoiCongNoPhaiThuTheoHopDongBan_TableName);
            uGrid.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            uGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;

            uGrid.DisplayLayout.Bands[0].Columns["GiaTriHopDong"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["GiaTriThucTe"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["CacKhoanGiamTru"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["DaThanhToan"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["ConLai"].FormatNumberic(ConstDatabase.Format_TienVND);

            Utils.AddSumColumn(uGrid, "SoHopDong", false);
            Utils.AddSumColumn(uGrid, "DoiTuong", false);
            Utils.AddSumColumn(uGrid, "TrichYeu", false);

            uGrid.DisplayLayout.Bands[0].Summaries[1].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[2].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[3].DisplayFormat = " ";

            Utils.AddSumColumn1(lst.Where(n => n.GiaTriHopDong != null).Sum(n => n.GiaTriHopDong), uGrid, "GiaTriHopDong", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.GiaTriThucTe != null).Sum(n => n.GiaTriThucTe), uGrid, "GiaTriThucTe", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.CacKhoanGiamTru != null).Sum(n => n.CacKhoanGiamTru), uGrid, "CacKhoanGiamTru", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.DaThanhToan != null).Sum(n => n.DaThanhToan), uGrid, "DaThanhToan", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.ConLai != null).Sum(n => n.ConLai), uGrid, "ConLai", false, constDatabaseFormat: 1);

            uGrid.DisplayLayout.Bands[0].Columns["NgayKy"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["NgayKy"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["NgayKy"].Width = 120;

            uGrid.DisplayLayout.Bands[0].Columns["SoHopDong"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["SoHopDong"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["SoHopDong"].Width = 120;
            uGrid.DisplayLayout.Bands[0].Columns["SoHopDong"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["DoiTuong"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["DoiTuong"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["DoiTuong"].Width = 150;
            uGrid.DisplayLayout.Bands[0].Columns["DoiTuong"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["TrichYeu"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["TrichYeu"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["TrichYeu"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["TrichYeu"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["GiaTriHopDong"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["GiaTriHopDong"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["GiaTriHopDong"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["GiaTriHopDong"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["GiaTriThucTe"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["GiaTriThucTe"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["GiaTriThucTe"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["GiaTriThucTe"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["CacKhoanGiamTru"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["CacKhoanGiamTru"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["CacKhoanGiamTru"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["CacKhoanGiamTru"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["DaThanhToan"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["DaThanhToan"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["DaThanhToan"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["DaThanhToan"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["ConLai"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["ConLai"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["ConLai"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["ConLai"].CellAppearance.TextVAlign = VAlign.Middle;

            GridLayout();

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (data.Count == 0)
                MSG.Warning("Không có dữ liệu");
            else
            {
                var rD = new SA_SoTheoDoiCongNoPhaiThuTheoHopDongBan_period();
                rD.Period = ReportUtils.GetPeriod(FromDate, ToDate);
                var f = new ReportForm<SA_SoTheoDoiCongNoPhaiThuTheoHopDongBan>(fileReportSlot1, _subSystemCode);
                f.AddDatasource("DBTHEODOICONGNOHDB", data, true);
                f.AddDatasource("Detail", rD);
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }


        private void btnExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog
            {
                Title = "Chọn nơi sao lưu ",
                Filter = "File (*.xls)|*.xls",
                InitialDirectory = "@C:\\",
                FileName = "sotheodoicongnotheohopdongban.xls"
            };
            if (save.ShowDialog() == DialogResult.OK)
            {
                string duongdan = save.FileName;
                try
                {
                    ultraGridExcelExporter1.Export(uGrid, duongdan);
                    if (MSG.Question("Kết xuất thành công, Bạn có muốn mở file vừa kết xuất không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(save.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
                catch
                {
                    MSG.Warning("Lỗi khi kết xuất !");
                }
            }
        }
        private void GridLayout()
        {
            UltraGridBand parentBand = this.uGrid.DisplayLayout.Bands[0];
            parentBand.RowLayoutStyle = RowLayoutStyle.GroupLayout;

            UltraGridGroup parentBandGroupPeriod;
            if (parentBand.Groups.Exists("ParentBandGroupPeriod"))
            {
                parentBandGroupPeriod = parentBand.Groups["ParentBandGroupPeriod"];
                parentBandGroupPeriod.Header.Caption = period;
            }
            else
                parentBandGroupPeriod = parentBand.Groups.Add("ParentBandGroupPeriod", period);
            parentBandGroupPeriod.Header.Appearance.FontData.Italic = DefaultableBoolean.True;
            parentBandGroupPeriod.Header.Appearance.FontData.SizeInPoints = 10;
            parentBandGroupPeriod.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupPeriod.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPeriod.Header.Appearance.BorderColor = Color.Black;

            UltraGridGroup parentBandGroupTitle;
            if (parentBand.Groups.Exists("ParentBandGroupTitle"))
                parentBandGroupTitle = parentBand.Groups["ParentBandGroupTitle"];
            else
                parentBandGroupTitle = parentBand.Groups.Add("ParentBandGroupTitle", "SỔ THEO DÕI CÔNG NỢ PHẢI THU THEO HỢP ĐỒNG BÁN");
            parentBandGroupTitle.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroupTitle.Header.Appearance.FontData.SizeInPoints = 12;
            parentBandGroupTitle.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupTitle.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupTitle.Header.Appearance.BorderColor = Color.Black;

            parentBandGroupPeriod.RowLayoutGroupInfo.ParentGroup = parentBandGroupTitle;

            parentBand.Columns["NgayKy"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["NgayKy"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["NgayKy"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["NgayKy"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["NgayKy"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["NgayKy"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["SoHopDong"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["SoHopDong"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SoHopDong"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["SoHopDong"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["SoHopDong"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SoHopDong"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["DoiTuong"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["DoiTuong"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["DoiTuong"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["DoiTuong"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["DoiTuong"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["DoiTuong"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["TrichYeu"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["TrichYeu"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["TrichYeu"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["TrichYeu"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["TrichYeu"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["TrichYeu"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["GiaTriHopDong"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["GiaTriHopDong"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GiaTriHopDong"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["GiaTriHopDong"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["GiaTriHopDong"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GiaTriHopDong"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["GiaTriThucTe"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["GiaTriThucTe"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GiaTriThucTe"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["GiaTriThucTe"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["GiaTriThucTe"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GiaTriThucTe"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["CacKhoanGiamTru"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["CacKhoanGiamTru"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["CacKhoanGiamTru"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["CacKhoanGiamTru"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["CacKhoanGiamTru"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["CacKhoanGiamTru"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["DaThanhToan"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["DaThanhToan"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["DaThanhToan"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["DaThanhToan"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["DaThanhToan"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["DaThanhToan"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["ConLai"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["ConLai"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ConLai"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["ConLai"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["ConLai"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ConLai"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["NgayKy"].RowLayoutColumnInfo.OriginX = 0;
            parentBand.Columns["NgayKy"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["NgayKy"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["NgayKy"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["SoHopDong"].RowLayoutColumnInfo.OriginX = 120;
            parentBand.Columns["SoHopDong"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["SoHopDong"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["SoHopDong"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["DoiTuong"].RowLayoutColumnInfo.OriginX = 240;
            parentBand.Columns["DoiTuong"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["DoiTuong"].RowLayoutColumnInfo.SpanX = 150;
            parentBand.Columns["DoiTuong"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["TrichYeu"].RowLayoutColumnInfo.OriginX = 390;
            parentBand.Columns["TrichYeu"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["TrichYeu"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["TrichYeu"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["GiaTriHopDong"].RowLayoutColumnInfo.OriginX = 640;
            parentBand.Columns["GiaTriHopDong"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["GiaTriHopDong"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["GiaTriHopDong"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["GiaTriThucTe"].RowLayoutColumnInfo.OriginX = 890;
            parentBand.Columns["GiaTriThucTe"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["GiaTriThucTe"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["GiaTriThucTe"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["CacKhoanGiamTru"].RowLayoutColumnInfo.OriginX = 1140;
            parentBand.Columns["CacKhoanGiamTru"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["CacKhoanGiamTru"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["CacKhoanGiamTru"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["DaThanhToan"].RowLayoutColumnInfo.OriginX = 1390;
            parentBand.Columns["DaThanhToan"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["DaThanhToan"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["DaThanhToan"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["ConLai"].RowLayoutColumnInfo.OriginX = 1640;
            parentBand.Columns["ConLai"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["ConLai"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["ConLai"].RowLayoutColumnInfo.SpanY = 32;

        }

        private void ultraGridExcelExporter1_InitializeColumn(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.InitializeColumnEventArgs e)
        {
            e.ExcelFormatStr = e.Column.Format;
        }

        private void ultraGridExcelExporter1_BeginExport(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.BeginExportEventArgs e)
        {
            foreach (var row in uGrid.Rows)
            {
                foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
                {
                    e.Rows[row.Index].Cells[column].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                    e.Rows[row.Index].Cells[column].Appearance.BorderColor = Color.Black;
                    e.Rows[row.Index].Cells[column].Column.Width = 200;
                }
            }
            e.Rows.Band.Summaries[0].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[0].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[1].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[1].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[2].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[2].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[3].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[3].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[4].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[4].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[5].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[5].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[6].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[6].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[7].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[7].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[8].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[8].Appearance.BorderColor = Color.Black;
        }

        private void uGrid_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            UltraGridCell cell = e.Row.Cells[0];
            cell.Row.Height = 25;

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("GiaTriHopDong"))
            {
                if (cell.Row.Cells["GiaTriHopDong"].Value != null && (decimal)cell.Row.Cells["GiaTriHopDong"].Value == 0)
                    cell.Row.Cells["GiaTriHopDong"].Value = null;
                if ((cell.Row.Cells["GiaTriHopDong"].Value != null && (decimal)cell.Row.Cells["GiaTriHopDong"].Value < 0))
                    cell.Row.Cells["GiaTriHopDong"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("GiaTriThucTe"))
            {
                if (cell.Row.Cells["GiaTriThucTe"].Value != null && (decimal)cell.Row.Cells["GiaTriThucTe"].Value == 0)
                    cell.Row.Cells["GiaTriThucTe"].Value = null;
                if ((cell.Row.Cells["GiaTriThucTe"].Value != null && (decimal)cell.Row.Cells["GiaTriThucTe"].Value < 0))
                    cell.Row.Cells["GiaTriThucTe"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("CacKhoanGiamTru"))
            {
                if (cell.Row.Cells["CacKhoanGiamTru"].Value != null && (decimal)cell.Row.Cells["CacKhoanGiamTru"].Value == 0)
                    cell.Row.Cells["CacKhoanGiamTru"].Value = null;
                if ((cell.Row.Cells["CacKhoanGiamTru"].Value != null && (decimal)cell.Row.Cells["CacKhoanGiamTru"].Value < 0))
                    cell.Row.Cells["CacKhoanGiamTru"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("DaThanhToan"))
            {
                if (cell.Row.Cells["DaThanhToan"].Value != null && (decimal)cell.Row.Cells["DaThanhToan"].Value == 0)
                    cell.Row.Cells["DaThanhToan"].Value = null;
                if ((cell.Row.Cells["DaThanhToan"].Value != null && (decimal)cell.Row.Cells["DaThanhToan"].Value < 0))
                    cell.Row.Cells["DaThanhToan"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("ConLai"))
            {
                if (cell.Row.Cells["ConLai"].Value != null && (decimal)cell.Row.Cells["ConLai"].Value == 0)
                    cell.Row.Cells["ConLai"].Value = null;
                if ((cell.Row.Cells["ConLai"].Value != null && (decimal)cell.Row.Cells["ConLai"].Value < 0))
                    cell.Row.Cells["ConLai"].Appearance.ForeColor = Color.Red;
            }

        }

        private void FSOKTCTTIENMATex_SizeChanged(object sender, EventArgs e)
        {
            var state = this.WindowState;
            switch (state)
            {
                case FormWindowState.Maximized:
                    break;
                case FormWindowState.Minimized:
                    break;
                case FormWindowState.Normal:
                    this.Left = (Screen.PrimaryScreen.Bounds.Width - this.Width) / 2;
                    this.Top = (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2;
                    this.Location = new Point((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2, (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
                    break;
                default:
                    break;
            }
        }

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                SA_SoTheoDoiCongNoPhaiThuTheoHopDongBan temp = (SA_SoTheoDoiCongNoPhaiThuTheoHopDongBan)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                if (temp != null)
                    editFuntion(temp);

            }
        }

        private void editFuntion(SA_SoTheoDoiCongNoPhaiThuTheoHopDongBan temp)
        {

            var f = Utils.ViewVoucherSelected1(temp.ContractID, temp.RefType);
            //List<SA_SoTheoDoiCongNoPhaiThuTheoHopDongBan> data1 = sp.GetSoTheoDoiCongNoTheoHopDongBan(FromDate, ToDate, lst_account);
            //configGrid(data1);

        }

        private void ultraGridExcelExporter1_SummaryCellExported(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.SummaryCellExportedEventArgs e)
        {
            Worksheet ws = e.CurrentWorksheet;
            WorksheetRow wsRow = ws.Rows[e.CurrentRowIndex];
            WorksheetCell wsCell = wsRow.Cells[e.CurrentColumnIndex];
            if (!e.Summary.SummaryText.Contains("Số dòng") && !e.Summary.SummaryText.Contains(" "))
            {
                string s = e.Summary.SummaryText.Trim('.');
                if (s.Contains("("))
                    wsCell.Value = -decimal.Parse(s.Trim('(').Trim(')'));
                else
                    wsCell.Value = decimal.Parse(s);

                wsCell.CellFormat.FormatString = "###,###,###,###,##0;(###,###,###,###,##0)";
            }
        }
    }

}
