﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.AppStyling;
using System.IO;
using FX.Core;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinMaskedEdit;
using Infragistics.Win.UltraWinTabs;
using Accounting;
using Accounting.Core.DAO;
using System.Diagnostics;
using Infragistics.Documents.Excel;

namespace Accounting.Frm.FReport
{
    public partial class FSOCCDCex : CustormForm
    {
        private string _subSystemCode;
        private List<S11DNN_PBCCDC> data = new List<S11DNN_PBCCDC>();
        private string list_item;
        private DateTime FromDate;
        private DateTime ToDate;
        private string period = "";
        ReportProcedureSDS sp = new ReportProcedureSDS();
        public FSOCCDCex(DateTime FromDate1, DateTime ToDate1, string list_item1)
        {
            InitializeComponent();
            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\BangPhanBoCCDC.rst", path);

            List<S11DNN_PBCCDC> data1 = sp.GetSoCongCuDungCu(FromDate1, ToDate1, list_item1);
            period = ReportUtils.GetPeriod(FromDate1, ToDate1);

            data = data1;
            list_item = list_item1;
            FromDate = FromDate1;
            ToDate = ToDate1;
            configGrid(data1);
            if (Utils.isDemo)
                btnExport.Visible = false;
            else
                btnExport.Visible = true;
        }

        private void configGrid(List<S11DNN_PBCCDC> lst)
        {
            uGrid.DataSource = lst;
            Utils.ConfigGrid(uGrid, ConstDatabase.FRS11DNN_PBCCDC_TableName); uGrid.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            uGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;

            uGrid.DisplayLayout.Bands[0].Columns["Amount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["AllocatedAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["DecrementAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["RemainingAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["AllocationTimes"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["DecrementAllocationTime"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["RemainingAllocationTime"].FormatNumberic(ConstDatabase.Format_TienVND);

            Utils.AddSumColumn(uGrid, "ToolsName", false);
            Utils.AddSumColumn(uGrid, "IncrementDate", false);
            Utils.AddSumColumn(uGrid, "PostedDate", false);
            Utils.AddSumColumn(uGrid, "Amount", false);
            Utils.AddSumColumn(uGrid, "AllocationTimes", false);
            Utils.AddSumColumn(uGrid, "AllocatedAmount", false);
            Utils.AddSumColumn(uGrid, "DecrementAllocationTime", false);
            Utils.AddSumColumn(uGrid, "RemainingAllocationTime", false);

            uGrid.DisplayLayout.Bands[0].Summaries[1].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[2].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[3].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[4].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[5].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[6].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[7].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[8].DisplayFormat = " ";

            Utils.AddSumColumn1(lst.Where(n => n.DecrementAmount != null).Sum(n => n.DecrementAmount), uGrid, "DecrementAmount", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.RemainingAmount != null).Sum(n => n.RemainingAmount), uGrid, "RemainingAmount", false, constDatabaseFormat: 1);

            uGrid.DisplayLayout.Bands[0].Columns["ToolsCode"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["ToolsCode"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["ToolsCode"].Width = 100;
            uGrid.DisplayLayout.Bands[0].Columns["ToolsCode"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["ToolsName"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["ToolsName"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["ToolsName"].Width = 150;
            uGrid.DisplayLayout.Bands[0].Columns["ToolsName"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["IncrementDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["IncrementDate"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["IncrementDate"].Width = 100;

            uGrid.DisplayLayout.Bands[0].Columns["PostedDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["PostedDate"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["PostedDate"].Width = 100;

            uGrid.DisplayLayout.Bands[0].Columns["Amount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["Amount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["Amount"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["Amount"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["AllocationTimes"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["AllocationTimes"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["AllocationTimes"].Width = 100;
            uGrid.DisplayLayout.Bands[0].Columns["AllocationTimes"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["AllocatedAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["AllocatedAmount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["AllocatedAmount"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["AllocatedAmount"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["DecrementAllocationTime"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["DecrementAllocationTime"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["DecrementAllocationTime"].Width = 80;
            uGrid.DisplayLayout.Bands[0].Columns["DecrementAllocationTime"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["RemainingAllocationTime"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["RemainingAllocationTime"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["RemainingAllocationTime"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["RemainingAllocationTime"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["DecrementAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["DecrementAmount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["DecrementAmount"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["DecrementAmount"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["RemainingAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["RemainingAmount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["RemainingAmount"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["RemainingAmount"].CellAppearance.TextVAlign = VAlign.Middle;

            GridLayout();

        }

        private void btnOk_Click(object sender, EventArgs e)
        {

            if (data.Count == 0)
            {
                MSG.Warning("Không có dữ liệu để lên báo cáo!");
                return;
            }
            else
            {
                var rd = new S11DNN_PBCCDC_period();
                rd.Period = ReportUtils.GetPeriod(FromDate, ToDate);
                var f = new ReportForm<S11DNN_PBCCDC>(fileReportSlot1, _subSystemCode);
                f.AddDatasource("PBCCDC", data, true);
                f.AddDatasource("detail", rd);
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }


        private void btnExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog
            {
                Title = "Chọn nơi sao lưu ",
                Filter = "File (*.xls)|*.xls",
                InitialDirectory = "@C:\\",
                FileName = "socongcudungcu.xls"
            };
            if (save.ShowDialog() == DialogResult.OK)
            {
                string duongdan = save.FileName;
                try
                {
                    ultraGridExcelExporter1.Export(uGrid, duongdan);
                    if (MSG.Question("Kết xuất thành công, Bạn có muốn mở file vừa kết xuất không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(save.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
                catch
                {
                    MSG.Warning("Lỗi khi kết xuất !");
                }
            }
        }
        private void GridLayout()
        {
            UltraGridBand parentBand = this.uGrid.DisplayLayout.Bands[0];
            parentBand.RowLayoutStyle = RowLayoutStyle.GroupLayout;

            UltraGridGroup parentBandGroupPeriod;
            if (parentBand.Groups.Exists("ParentBandGroupPeriod"))
            {
                parentBandGroupPeriod = parentBand.Groups["ParentBandGroupPeriod"];
                parentBandGroupPeriod.Header.Caption = period;
            }
            else
                parentBandGroupPeriod = parentBand.Groups.Add("ParentBandGroupPeriod", period);
            parentBandGroupPeriod.Header.Appearance.FontData.Italic = DefaultableBoolean.True;
            parentBandGroupPeriod.Header.Appearance.FontData.SizeInPoints = 10;
            parentBandGroupPeriod.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupPeriod.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPeriod.Header.Appearance.BorderColor = Color.Black;

            UltraGridGroup parentBandGroupTitle;
            if (parentBand.Groups.Exists("ParentBandGroupTitle"))
                parentBandGroupTitle = parentBand.Groups["ParentBandGroupTitle"];
            else
                parentBandGroupTitle = parentBand.Groups.Add("ParentBandGroupTitle", "SỔ CÔNG CỤ DỤNG CỤ");
            parentBandGroupTitle.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroupTitle.Header.Appearance.FontData.SizeInPoints = 12;
            parentBandGroupTitle.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupTitle.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupTitle.Header.Appearance.BorderColor = Color.Black;

            parentBandGroupPeriod.RowLayoutGroupInfo.ParentGroup = parentBandGroupTitle;

            parentBand.Columns["ToolsCode"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["ToolsCode"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ToolsCode"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["ToolsCode"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["ToolsCode"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ToolsCode"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["ToolsName"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["ToolsName"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ToolsName"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["ToolsName"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["ToolsName"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ToolsName"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["IncrementDate"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["IncrementDate"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["IncrementDate"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["IncrementDate"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["IncrementDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["IncrementDate"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["PostedDate"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["PostedDate"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["PostedDate"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["PostedDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["PostedDate"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["Amount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["Amount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Amount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["Amount"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["Amount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Amount"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["AllocationTimes"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["AllocationTimes"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["AllocationTimes"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["AllocationTimes"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["AllocationTimes"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["AllocationTimes"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["AllocatedAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["AllocatedAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["AllocatedAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["AllocatedAmount"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["AllocatedAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["AllocatedAmount"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["DecrementAllocationTime"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["DecrementAllocationTime"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["DecrementAllocationTime"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["DecrementAllocationTime"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["DecrementAllocationTime"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["DecrementAllocationTime"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["RemainingAllocationTime"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["RemainingAllocationTime"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["RemainingAllocationTime"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["RemainingAllocationTime"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["RemainingAllocationTime"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["RemainingAllocationTime"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["DecrementAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["DecrementAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["DecrementAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["DecrementAmount"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["DecrementAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["DecrementAmount"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["RemainingAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["RemainingAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["RemainingAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["RemainingAmount"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["RemainingAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["RemainingAmount"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["ToolsCode"].RowLayoutColumnInfo.OriginX = 0;
            parentBand.Columns["ToolsCode"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["ToolsCode"].RowLayoutColumnInfo.SpanX = 100;
            parentBand.Columns["ToolsCode"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["ToolsName"].RowLayoutColumnInfo.OriginX = 100;
            parentBand.Columns["ToolsName"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["ToolsName"].RowLayoutColumnInfo.SpanX = 150;
            parentBand.Columns["ToolsName"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["IncrementDate"].RowLayoutColumnInfo.OriginX = 250;
            parentBand.Columns["IncrementDate"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["IncrementDate"].RowLayoutColumnInfo.SpanX = 100;
            parentBand.Columns["IncrementDate"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.OriginX = 350;
            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.SpanX = 100;
            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["Amount"].RowLayoutColumnInfo.OriginX = 450;
            parentBand.Columns["Amount"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["Amount"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["Amount"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["AllocationTimes"].RowLayoutColumnInfo.OriginX = 700;
            parentBand.Columns["AllocationTimes"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["AllocationTimes"].RowLayoutColumnInfo.SpanX = 100;
            parentBand.Columns["AllocationTimes"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["AllocatedAmount"].RowLayoutColumnInfo.OriginX = 800;
            parentBand.Columns["AllocatedAmount"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["AllocatedAmount"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["AllocatedAmount"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["DecrementAllocationTime"].RowLayoutColumnInfo.OriginX = 1050;
            parentBand.Columns["DecrementAllocationTime"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["DecrementAllocationTime"].RowLayoutColumnInfo.SpanX = 100;
            parentBand.Columns["DecrementAllocationTime"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["RemainingAllocationTime"].RowLayoutColumnInfo.OriginX = 1150;
            parentBand.Columns["RemainingAllocationTime"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["RemainingAllocationTime"].RowLayoutColumnInfo.SpanX = 100;
            parentBand.Columns["RemainingAllocationTime"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["DecrementAmount"].RowLayoutColumnInfo.OriginX = 1250;
            parentBand.Columns["DecrementAmount"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["DecrementAmount"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["DecrementAmount"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["RemainingAmount"].RowLayoutColumnInfo.OriginX = 1500;
            parentBand.Columns["RemainingAmount"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["RemainingAmount"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["RemainingAmount"].RowLayoutColumnInfo.SpanY = 32;

        }

        private void ultraGridExcelExporter1_InitializeColumn(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.InitializeColumnEventArgs e)
        {
            e.ExcelFormatStr = e.Column.Format;
        }

        private void ultraGridExcelExporter1_BeginExport(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.BeginExportEventArgs e)
        {
            foreach (var row in uGrid.Rows)
            {
                foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
                {
                    e.Rows[row.Index].Cells[column].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                    e.Rows[row.Index].Cells[column].Appearance.BorderColor = Color.Black;
                    e.Rows[row.Index].Cells[column].Column.Width = 200;
                }
            }
            e.Rows.Band.Summaries[0].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[0].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[1].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[1].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[2].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[2].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[3].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[3].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[4].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[4].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[5].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[5].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[6].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[6].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[7].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[7].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[8].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[8].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[9].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[9].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[10].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[10].Appearance.BorderColor = Color.Black;
        }

        private void uGrid_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            UltraGridCell cell = e.Row.Cells[0];
            cell.Row.Height = 25;

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("Amount"))
            {
                if (cell.Row.Cells["Amount"].Value != null && (decimal)cell.Row.Cells["Amount"].Value == 0)
                    cell.Row.Cells["Amount"].Value = null;
                if ((cell.Row.Cells["Amount"].Value != null && (decimal)cell.Row.Cells["Amount"].Value < 0))
                    cell.Row.Cells["Amount"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("AllocationTimes"))
            {
                if (cell.Row.Cells["AllocationTimes"].Value != null && cell.Row.Cells["AllocationTimes"].Value.ToInt() == 0)
                    cell.Row.Cells["AllocationTimes"].Value = null;
                if ((cell.Row.Cells["AllocationTimes"].Value != null && cell.Row.Cells["AllocationTimes"].Value.ToInt() < 0))
                    cell.Row.Cells["AllocationTimes"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("AllocatedAmount"))
            {
                if (cell.Row.Cells["AllocatedAmount"].Value != null && (decimal)cell.Row.Cells["AllocatedAmount"].Value == 0)
                    cell.Row.Cells["AllocatedAmount"].Value = null;
                if ((cell.Row.Cells["AllocatedAmount"].Value != null && (decimal)cell.Row.Cells["AllocatedAmount"].Value < 0))
                    cell.Row.Cells["AllocatedAmount"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("DecrementAllocationTime"))
            {
                if (cell.Row.Cells["DecrementAllocationTime"].Value != null && cell.Row.Cells["DecrementAllocationTime"].Value.ToInt() == 0)
                    cell.Row.Cells["DecrementAllocationTime"].Value = null;
                if ((cell.Row.Cells["DecrementAllocationTime"].Value != null && cell.Row.Cells["DecrementAllocationTime"].Value.ToInt() < 0))
                    cell.Row.Cells["DecrementAllocationTime"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("RemainingAllocationTime"))
            {
                if (cell.Row.Cells["RemainingAllocationTime"].Value != null && cell.Row.Cells["RemainingAllocationTime"].Value.ToInt() == 0)
                    cell.Row.Cells["RemainingAllocationTime"].Value = null;
                if ((cell.Row.Cells["RemainingAllocationTime"].Value != null && cell.Row.Cells["RemainingAllocationTime"].Value.ToInt() < 0))
                    cell.Row.Cells["RemainingAllocationTime"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("DecrementAmount"))
            {
                if (cell.Row.Cells["DecrementAmount"].Value != null && (decimal)cell.Row.Cells["DecrementAmount"].Value == 0)
                    cell.Row.Cells["DecrementAmount"].Value = null;
                if ((cell.Row.Cells["DecrementAmount"].Value != null && (decimal)cell.Row.Cells["DecrementAmount"].Value < 0))
                    cell.Row.Cells["DecrementAmount"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("RemainingAmount"))
            {
                if (cell.Row.Cells["RemainingAmount"].Value != null && (decimal)cell.Row.Cells["RemainingAmount"].Value == 0)
                    cell.Row.Cells["RemainingAmount"].Value = null;
                if ((cell.Row.Cells["RemainingAmount"].Value != null && (decimal)cell.Row.Cells["RemainingAmount"].Value < 0))
                    cell.Row.Cells["RemainingAmount"].Appearance.ForeColor = Color.Red;
            }

        }

        private void FSOKTCTTIENMATex_SizeChanged(object sender, EventArgs e)
        {
            var state = this.WindowState;
            switch (state)
            {
                case FormWindowState.Maximized:
                    break;
                case FormWindowState.Minimized:
                    break;
                case FormWindowState.Normal:
                    this.Left = (Screen.PrimaryScreen.Bounds.Width - this.Width) / 2;
                    this.Top = (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2;
                    this.Location = new Point((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2, (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
                    break;
                default:
                    break;
            }
        }

        private void ultraGridExcelExporter1_SummaryCellExported(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.SummaryCellExportedEventArgs e)
        {
            Worksheet ws = e.CurrentWorksheet;
            WorksheetRow wsRow = ws.Rows[e.CurrentRowIndex];
            WorksheetCell wsCell = wsRow.Cells[e.CurrentColumnIndex];
            if (!e.Summary.SummaryText.Contains("Số dòng") && !e.Summary.SummaryText.Contains(" "))
            {
                string s = e.Summary.SummaryText.Trim('.');
                if (s.Contains("("))
                    wsCell.Value = -decimal.Parse(s.Trim('(').Trim(')'));
                else
                    wsCell.Value = decimal.Parse(s);

                wsCell.CellFormat.FormatString = "###,###,###,###,##0;(###,###,###,###,##0)";
            }
        }
    }

}
