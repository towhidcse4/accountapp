﻿using Accounting.Core.DAO;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.Report;
using Accounting.TextMessage;
using Infragistics.Documents.Excel;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRSONHATKYMUAHANGex : Form
    {
        private string _subSystemCode;
        ReportProcedureSDS sp = new ReportProcedureSDS();
        List<PO_DiaryBook> data = new List<PO_DiaryBook>();
        private string period = "";
        DateTime FromDate;
        DateTime ToDate;
        int check;
        public FRSONHATKYMUAHANGex(DateTime FromDate1, DateTime ToDate1, int check1)
        {
            InitializeComponent();
            ReportUtils.ProcessControls(this);
            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\SoNhatKyMuaHang.rst", path);
            List<PO_DiaryBook> data1 = sp.GetSoNhatKyMuaHang(FromDate1, ToDate1, check1, "", "");
            period = ReportUtils.GetPeriod(FromDate1, ToDate1);
            data = data1;
            FromDate = FromDate1;
            ToDate = ToDate1;
            check = check1;
            configGrid(data);
            if (Utils.isDemo)
                btnExport.Visible = false;
            else
                btnExport.Visible = true;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            
            if (data.Count == 0)
            {
                MSG.Warning("Không có dữ liệu để lên báo cáo");
            }
            else
            {
                foreach (var item in data)
                {
                    if (item.TypeID == 220)
                    {
                        item.GoodsAmount = -item.GoodsAmount;
                        item.EquipmentAmount = -item.EquipmentAmount;
                        item.AnotherAmount = -item.AnotherAmount;
                        item.PaymentableAmount = -item.PaymentableAmount;
                    }
                }
                var rD = new PO_DiaryBook_Detail();
                rD.Period = ReportUtils.GetPeriod(FromDate, ToDate);
                var f = new ReportForm<PO_DiaryBook>(fileReportSlot1, _subSystemCode);
                f.AddDatasource("DBNHATKYMUAHANG", data, true);
                f.AddDatasource("Detail", rD);
                f.SetHyperlinkFunction("Accounting.ReportUtils", "Hyperlink");
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void configGrid(List<PO_DiaryBook> lst)
        {
            uGrid.DataSource = lst;
            Utils.ConfigGrid(uGrid, ConstDatabase.PO_DiaryBook_TableName);

            uGrid.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            uGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;

            uGrid.DisplayLayout.Bands[0].Columns["GoodsAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["EquipmentAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["AnotherAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["PaymentableAmount"].FormatNumberic(ConstDatabase.Format_TienVND);

            Utils.AddSumColumn(uGrid, "RefDate", false);
            Utils.AddSumColumn(uGrid, "RefNo", false);
            Utils.AddSumColumn(uGrid, "InvoiceDate", false);
            Utils.AddSumColumn(uGrid, "InvoiceNo", false);
            Utils.AddSumColumn(uGrid, "Description", false);
            Utils.AddSumColumn(uGrid, "AnotherAccount", false);
            
            uGrid.DisplayLayout.Bands[0].Summaries[1].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[2].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[3].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[4].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[5].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[6].DisplayFormat = " ";

            Utils.AddSumColumn1(lst.Where(n => n.GoodsAmount != null).Sum(n => n.GoodsAmount), uGrid, "GoodsAmount", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.EquipmentAmount != null).Sum(n => n.EquipmentAmount), uGrid, "EquipmentAmount", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.AnotherAmount != null).Sum(n => n.AnotherAmount), uGrid, "AnotherAmount", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.PaymentableAmount != null).Sum(n => n.PaymentableAmount), uGrid, "PaymentableAmount", false, constDatabaseFormat: 1);

            uGrid.DisplayLayout.Bands[0].Columns["PostedDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["PostedDate"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["PostedDate"].Width = 120;

            uGrid.DisplayLayout.Bands[0].Columns["RefDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["RefDate"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["RefDate"].Width = 120;

            uGrid.DisplayLayout.Bands[0].Columns["RefNo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["RefNo"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["RefNo"].Width = 130;
            uGrid.DisplayLayout.Bands[0].Columns["RefNo"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["InvoiceDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["InvoiceDate"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["InvoiceDate"].Width = 120;

            uGrid.DisplayLayout.Bands[0].Columns["InvoiceNo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["InvoiceNo"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["InvoiceNo"].Width = 130;
            uGrid.DisplayLayout.Bands[0].Columns["InvoiceNo"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["Description"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["Description"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["Description"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["Description"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["GoodsAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["GoodsAmount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["GoodsAmount"].Width = 200;
            uGrid.DisplayLayout.Bands[0].Columns["GoodsAmount"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["EquipmentAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["EquipmentAmount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["EquipmentAmount"].Width = 200;
            uGrid.DisplayLayout.Bands[0].Columns["EquipmentAmount"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["AnotherAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["AnotherAmount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["AnotherAmount"].Width = 200;
            uGrid.DisplayLayout.Bands[0].Columns["AnotherAmount"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["AnotherAccount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["AnotherAccount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["AnotherAccount"].Width = 120;
            uGrid.DisplayLayout.Bands[0].Columns["AnotherAccount"].CellAppearance.TextVAlign = VAlign.Middle;
            uGrid.DisplayLayout.Bands[0].Columns["AnotherAccount"].CellAppearance.TextHAlign = HAlign.Center;

            uGrid.DisplayLayout.Bands[0].Columns["PaymentableAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["PaymentableAmount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["PaymentableAmount"].Width = 200;
            uGrid.DisplayLayout.Bands[0].Columns["PaymentableAmount"].CellAppearance.TextVAlign = VAlign.Middle;

            GridLayout();

        }

        private void GridLayout()
        {
            UltraGridBand parentBand = this.uGrid.DisplayLayout.Bands[0];
            parentBand.RowLayoutStyle = RowLayoutStyle.GroupLayout;

            UltraGridGroup parentBandGroupPeriod;
            if (parentBand.Groups.Exists("ParentBandGroupPeriod"))
            {
                parentBandGroupPeriod = parentBand.Groups["ParentBandGroupPeriod"];
                parentBandGroupPeriod.Header.Caption = period;
            }
            else
                parentBandGroupPeriod = parentBand.Groups.Add("ParentBandGroupPeriod", period);
            parentBandGroupPeriod.Header.Appearance.FontData.Italic = DefaultableBoolean.True;
            parentBandGroupPeriod.Header.Appearance.FontData.SizeInPoints = 10;
            parentBandGroupPeriod.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupPeriod.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPeriod.Header.Appearance.BorderColor = Color.Black;

            UltraGridGroup parentBandGroupTitle;
            if (parentBand.Groups.Exists("ParentBandGroupTitle"))
                parentBandGroupTitle = parentBand.Groups["ParentBandGroupTitle"];
            else
                parentBandGroupTitle = parentBand.Groups.Add("ParentBandGroupTitle", "SỔ NHẬT KÝ MUA HÀNG");
            parentBandGroupTitle.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroupTitle.Header.Appearance.FontData.SizeInPoints = 12;
            parentBandGroupTitle.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupTitle.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupTitle.Header.Appearance.BorderColor = Color.Black;

            UltraGridGroup parentBandGroupTKK;
            if (parentBand.Groups.Exists("ParentBandGroupTKK"))
                parentBandGroupTKK = parentBand.Groups["ParentBandGroupTKK"];
            else
                parentBandGroupTKK = parentBand.Groups.Add("ParentBandGroupTKK", "Tài khoản khác");
            parentBand.Columns["AnotherAccount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupTKK;
            parentBand.Columns["AnotherAccount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["AnotherAccount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["AnotherAccount"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["AnotherAccount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["AnotherAccount"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["AnotherAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupTKK;
            parentBand.Columns["AnotherAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["AnotherAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["AnotherAmount"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["AnotherAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["AnotherAmount"].Header.Appearance.BorderColor = Color.Black;

            parentBandGroupPeriod.RowLayoutGroupInfo.ParentGroup = parentBandGroupTitle;
            parentBandGroupTKK.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            parentBandGroupTKK.Header.Appearance.FontData.SizeInPoints = 9;
            parentBandGroupTKK.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupTKK.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupTKK.Header.Appearance.BorderColor = Color.Black;
            parentBandGroupTKK.Header.Appearance.BackColorAlpha = Alpha.Default;

            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["PostedDate"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["PostedDate"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["PostedDate"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["PostedDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["PostedDate"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["RefDate"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["RefDate"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["RefDate"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["RefDate"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["RefDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["RefDate"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["RefNo"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["RefNo"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["RefNo"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["RefNo"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["RefNo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["RefNo"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["InvoiceDate"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["InvoiceDate"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["InvoiceDate"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["InvoiceDate"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["InvoiceDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["InvoiceDate"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["InvoiceNo"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["InvoiceNo"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["InvoiceNo"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["InvoiceNo"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["InvoiceNo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["InvoiceNo"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["Description"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["Description"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Description"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["Description"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["Description"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Description"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["GoodsAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["GoodsAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GoodsAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["GoodsAmount"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["GoodsAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GoodsAmount"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["EquipmentAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["EquipmentAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["EquipmentAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["EquipmentAmount"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["EquipmentAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["EquipmentAmount"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["PaymentableAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["PaymentableAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["PaymentableAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["PaymentableAmount"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["PaymentableAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["PaymentableAmount"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.OriginX = 0;
            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["RefDate"].RowLayoutColumnInfo.OriginX = 120;
            parentBand.Columns["RefDate"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["RefDate"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["RefDate"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["RefNo"].RowLayoutColumnInfo.OriginX = 240;
            parentBand.Columns["RefNo"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["RefNo"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["RefNo"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["InvoiceDate"].RowLayoutColumnInfo.OriginX = 360;
            parentBand.Columns["InvoiceDate"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["InvoiceDate"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["InvoiceDate"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["InvoiceNo"].RowLayoutColumnInfo.OriginX = 480;
            parentBand.Columns["InvoiceNo"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["InvoiceNo"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["InvoiceNo"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["Description"].RowLayoutColumnInfo.OriginX = 600;
            parentBand.Columns["Description"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["Description"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["Description"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["GoodsAmount"].RowLayoutColumnInfo.OriginX = 850;
            parentBand.Columns["GoodsAmount"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["GoodsAmount"].RowLayoutColumnInfo.SpanX = 200;
            parentBand.Columns["GoodsAmount"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["EquipmentAmount"].RowLayoutColumnInfo.OriginX = 1050;
            parentBand.Columns["EquipmentAmount"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["EquipmentAmount"].RowLayoutColumnInfo.SpanX = 200;
            parentBand.Columns["EquipmentAmount"].RowLayoutColumnInfo.SpanY = 32;

            parentBandGroupTKK.RowLayoutGroupInfo.OriginX = 1250;
            parentBandGroupTKK.RowLayoutGroupInfo.OriginY = 0;
            parentBandGroupTKK.RowLayoutGroupInfo.SpanX = 320;
            parentBandGroupTKK.RowLayoutGroupInfo.SpanY = 32;

            parentBand.Columns["PaymentableAmount"].RowLayoutColumnInfo.OriginX = 1570;
            parentBand.Columns["PaymentableAmount"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["PaymentableAmount"].RowLayoutColumnInfo.SpanX = 200;
            parentBand.Columns["PaymentableAmount"].RowLayoutColumnInfo.SpanY = 32;

        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog
            {
                Title = "Chọn nơi sao lưu ",
                Filter = "File (*.xls)|*.xls",
                InitialDirectory = "@C:\\",
                FileName = "sonhatkymuahang.xls"
            };
            if (save.ShowDialog() == DialogResult.OK)
            {
                string duongdan = save.FileName;
                try
                {
                    ultraGridExcelExporter1.Export(uGrid, duongdan);
                    if (MSG.Question("Kết xuất thành công, Bạn có muốn mở file vừa kết xuất không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(save.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
                catch
                {
                    MSG.Warning("Lỗi khi kết xuất !");
                }
            }
        }

        private void uGrid_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            UltraGridCell cell = e.Row.Cells[0];
            cell.Row.Height = 25;

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("GoodsAmount"))
            {
                if (cell.Row.Cells["GoodsAmount"].Value != null && (decimal)cell.Row.Cells["GoodsAmount"].Value == 0)
                    cell.Row.Cells["GoodsAmount"].Value = null;
                if ((cell.Row.Cells["GoodsAmount"].Value != null && (decimal)cell.Row.Cells["GoodsAmount"].Value < 0))
                    cell.Row.Cells["GoodsAmount"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("EquipmentAmount"))
            {
                if (cell.Row.Cells["EquipmentAmount"].Value != null && (decimal)cell.Row.Cells["EquipmentAmount"].Value == 0)
                    cell.Row.Cells["EquipmentAmount"].Value = null;
                if ((cell.Row.Cells["EquipmentAmount"].Value != null && (decimal)cell.Row.Cells["EquipmentAmount"].Value < 0))
                    cell.Row.Cells["EquipmentAmount"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("AnotherAmount"))
            {
                if (cell.Row.Cells["AnotherAmount"].Value != null && (decimal)cell.Row.Cells["AnotherAmount"].Value == 0)
                    cell.Row.Cells["AnotherAmount"].Value = null;
                if ((cell.Row.Cells["AnotherAmount"].Value != null && (decimal)cell.Row.Cells["AnotherAmount"].Value < 0))
                    cell.Row.Cells["AnotherAmount"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("PaymentableAmount"))
            {
                if (cell.Row.Cells["PaymentableAmount"].Value != null && (decimal)cell.Row.Cells["PaymentableAmount"].Value == 0)
                    cell.Row.Cells["PaymentableAmount"].Value = null;
                if ((cell.Row.Cells["PaymentableAmount"].Value != null && (decimal)cell.Row.Cells["PaymentableAmount"].Value < 0))
                    cell.Row.Cells["PaymentableAmount"].Appearance.ForeColor = Color.Red;
            }



        }

        private void ultraGridExcelExporter1_InitializeColumn(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.InitializeColumnEventArgs e)
        {
            e.ExcelFormatStr = e.Column.Format;
        }

        private void ultraGridExcelExporter1_BeginExport(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.BeginExportEventArgs e)
        {
            foreach (var row in uGrid.Rows)
            {
                foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
                {
                    e.Rows[row.Index].Cells[column].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                    e.Rows[row.Index].Cells[column].Appearance.BorderColor = Color.Black;
                    e.Rows[row.Index].Cells[column].Column.Width = 200;
                }
            }
            e.Rows.Band.Summaries[0].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[0].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[1].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[1].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[2].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[2].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[3].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[3].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[4].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[4].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[5].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[5].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[6].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[6].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[7].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[7].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[8].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[8].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[9].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[9].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[10].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[10].Appearance.BorderColor = Color.Black;
        }

        private void FRB01aDNN_SizeChanged(object sender, EventArgs e)
        {
            var state = this.WindowState;
            switch (state)
            {
                case FormWindowState.Maximized:
                    break;
                case FormWindowState.Minimized:
                    break;
                case FormWindowState.Normal:
                    this.Left = (Screen.PrimaryScreen.Bounds.Width - this.Width) / 2;
                    this.Top = (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2;
                    this.Location = new Point((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2, (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
                    break;
                default:
                    break;
            }
        }

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                PO_DiaryBook temp = (PO_DiaryBook)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                if (temp != null)
                    editFuntion(temp);

            }
        }

        private void editFuntion(PO_DiaryBook temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID, temp.TypeID);
            //List<PO_DiaryBook> data1 = sp.GetSoNhatKyMuaHang(FromDate, ToDate, check, "", "");
            //configGrid(data1);
        }

        private void ultraGridExcelExporter1_SummaryCellExported(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.SummaryCellExportedEventArgs e)
        {
            Worksheet ws = e.CurrentWorksheet;
            WorksheetRow wsRow = ws.Rows[e.CurrentRowIndex];
            WorksheetCell wsCell = wsRow.Cells[e.CurrentColumnIndex];
            if (!e.Summary.SummaryText.Contains("Số dòng") && !e.Summary.SummaryText.Contains(" "))
            {
                string s = e.Summary.SummaryText.Trim('.');
                if (s.Contains("("))
                    wsCell.Value = -decimal.Parse(s.Trim('(').Trim(')'));
                else
                    wsCell.Value = decimal.Parse(s);

                wsCell.CellFormat.FormatString = "###,###,###,###,##0;(###,###,###,###,##0)";
            }
        }
    }
}
