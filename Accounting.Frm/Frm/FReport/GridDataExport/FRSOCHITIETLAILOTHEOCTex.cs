﻿using Accounting.Core.DAO;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.Report;
using Accounting.TextMessage;
using Infragistics.Documents.Excel;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRSOCHITIETLAILOTHEOCTex : Form
    {
        private string _subSystemCode;
        ReportProcedureSDS sp = new ReportProcedureSDS();
        List<CTBook> data = new List<CTBook>();
        string period = "";
        DateTime FromDate;
        DateTime ToDate;
        string lstItem = "";
        public FRSOCHITIETLAILOTHEOCTex(DateTime FromDate1, DateTime ToDate1, string lstItem1)
        {
            InitializeComponent();
            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\SoChiTietLaiLoTheoCongTrinh.rst", path);
            ReportUtils.ProcessControls(this);

            List<CTBook> data1 = sp.SOCHITIETCONGTRINH(FromDate1, ToDate1, lstItem1);
            period = ReportUtils.GetPeriod(FromDate1, ToDate1);

            FromDate = FromDate1;
            ToDate = ToDate1;
            data = data1;
            lstItem = lstItem1;

            configGrid(data1);
            if (Utils.isDemo)
                btnExport.Visible = false;
            else
                btnExport.Visible = true;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            List<CTBook> data = sp.SOCHITIETCONGTRINH(FromDate, ToDate, lstItem);            

            if (data.Count == 0)
                MSG.Warning("Không có dữ liệu");
            else
            {
                data.ForEach(t =>
                {
                    if (t.Reason == null)
                        t.Reason = "";
                    else
                        t.Reason = t.Reason;
                });
                data = data.Where(n => !n.Reason.Contains("Mã công trình") && n.Reason != "Cộng nhóm").ToList();
                var rD = new CTBook_Period();
                rD.Period = ReportUtils.GetPeriod(FromDate, ToDate);
                var f = new ReportForm<CTBook>(fileReportSlot1, _subSystemCode);
                f.AddDatasource("CTBOOK", data, true);
                f.AddDatasource("Detail", rD);
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog
            {
                Title = "Chọn nơi sao lưu ",
                Filter = "File (*.xls)|*.xls",
                InitialDirectory = "@C:\\",
                FileName = "sochitietlailotheocongtrinh.xls"
            };
            if (save.ShowDialog() == DialogResult.OK)
            {
                string duongdan = save.FileName;
                try
                {
                    ultraGridExcelExporter1.Export(uGrid, duongdan);
                    if (MSG.Question("Kết xuất thành công, Bạn có muốn mở file vừa kết xuất không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(save.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
                catch
                {
                    MSG.Warning("Lỗi khi kết xuất !");
                }
            }
        }     

        private void configGrid(List<CTBook> lst)
        {
            uGrid.DataSource = lst;
            Utils.ConfigGrid(uGrid, ConstDatabase.SOCHITIETCONGTRINH_TableName);
            uGrid.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;

            lst.ForEach(t =>
            {
                if (t.Reason == null)
                    t.Reason = "";
                else
                    t.Reason = t.Reason;
            });

            Utils.AddSumColumn(uGrid, "Date", false);
            Utils.AddSumColumn(uGrid, "No", false);
            Utils.AddSumColumn(uGrid, "Reason", false);

            uGrid.DisplayLayout.Bands[0].Summaries[1].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[2].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[3].DisplayFormat = " ";

            uGrid.DisplayLayout.Bands[0].Columns["PostedDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["PostedDate"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["PostedDate"].Width = 120;
            uGrid.DisplayLayout.Bands[0].Columns["PostedDate"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["Date"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["Date"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["Date"].CellAppearance.TextVAlign = VAlign.Middle;
            uGrid.DisplayLayout.Bands[0].Columns["Date"].Width = 120;

            uGrid.DisplayLayout.Bands[0].Columns["No"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["No"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["No"].CellAppearance.TextVAlign = VAlign.Middle;
            uGrid.DisplayLayout.Bands[0].Columns["No"].Width = 100;

            uGrid.DisplayLayout.Bands[0].Columns["Reason"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["Reason"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["Reason"].Width = 270;
            uGrid.DisplayLayout.Bands[0].Columns["Reason"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["DoanhThu"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["DoanhThu"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["DoanhThu"].Width = 180;
            uGrid.DisplayLayout.Bands[0].Columns["DoanhThu"].CellAppearance.TextVAlign = VAlign.Middle;
            uGrid.DisplayLayout.Bands[0].Columns["DoanhThu"].FormatNumberic(ConstDatabase.Format_TienVND);

            uGrid.DisplayLayout.Bands[0].Columns["GiamTru"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["GiamTru"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["GiamTru"].Width = 180;
            uGrid.DisplayLayout.Bands[0].Columns["GiamTru"].CellAppearance.TextVAlign = VAlign.Middle;
            uGrid.DisplayLayout.Bands[0].Columns["GiamTru"].FormatNumberic(ConstDatabase.Format_TienVND);

            uGrid.DisplayLayout.Bands[0].Columns["NVLTT"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["NVLTT"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["NVLTT"].CellAppearance.TextVAlign = VAlign.Middle;
            uGrid.DisplayLayout.Bands[0].Columns["NVLTT"].Width = 180;
            uGrid.DisplayLayout.Bands[0].Columns["NVLTT"].FormatNumberic(ConstDatabase.Format_TienVND);

            uGrid.DisplayLayout.Bands[0].Columns["NCTT"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["NCTT"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["NCTT"].CellAppearance.TextVAlign = VAlign.Middle;
            uGrid.DisplayLayout.Bands[0].Columns["NCTT"].Width = 180;
            uGrid.DisplayLayout.Bands[0].Columns["NCTT"].FormatNumberic(ConstDatabase.Format_TienVND);

            uGrid.DisplayLayout.Bands[0].Columns["CPSXC"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["CPSXC"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["CPSXC"].Width = 180;
            uGrid.DisplayLayout.Bands[0].Columns["CPSXC"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["CPSXC"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["Cong"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["Cong"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["Cong"].Width = 180;
            uGrid.DisplayLayout.Bands[0].Columns["Cong"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["Cong"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["GiaVon"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["GiaVon"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["GiaVon"].Width = 180;
            uGrid.DisplayLayout.Bands[0].Columns["GiaVon"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["GiaVon"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["LaiLo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["LaiLo"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["LaiLo"].Width = 180;
            uGrid.DisplayLayout.Bands[0].Columns["LaiLo"].CellAppearance.TextVAlign = VAlign.Middle;
            uGrid.DisplayLayout.Bands[0].Columns["LaiLo"].FormatNumberic(ConstDatabase.Format_TienVND);

            Utils.AddSumColumn1(lst.Where(n => n.DoanhThu != null && n.Reason != "Cộng nhóm").Sum(n => n.DoanhThu), uGrid, "DoanhThu", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.GiamTru != null && n.Reason != "Cộng nhóm").Sum(n => n.GiamTru), uGrid, "GiamTru", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.NVLTT != null && n.Reason != "Cộng nhóm").Sum(n => n.NVLTT), uGrid, "NVLTT", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.NCTT != null && n.Reason != "Cộng nhóm").Sum(n => n.NCTT), uGrid, "NCTT", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.CPSXC != null && n.Reason != "Cộng nhóm").Sum(n => n.CPSXC), uGrid, "CPSXC", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.Cong != null && n.Reason != "Cộng nhóm").Sum(n => n.Cong), uGrid, "Cong", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.GiaVon != null && n.Reason != "Cộng nhóm").Sum(n => n.GiaVon), uGrid, "GiaVon", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.LaiLo != null && n.Reason != "Cộng nhóm").Sum(n => n.LaiLo), uGrid, "LaiLo", false, constDatabaseFormat: 1);

            GridLayout();
        }
        private void GridLayout()
        {
            UltraGridBand parentBand = this.uGrid.DisplayLayout.Bands[0];
            parentBand.RowLayoutStyle = RowLayoutStyle.GroupLayout;

            UltraGridGroup parentBandGroupPeriod;
            if (parentBand.Groups.Exists("ParentBandGroupPeriod"))
            {
                parentBandGroupPeriod = parentBand.Groups["ParentBandGroupPeriod"];
                parentBandGroupPeriod.Header.Caption = period;
            }
            else
                parentBandGroupPeriod = parentBand.Groups.Add("ParentBandGroupPeriod", period);
            parentBandGroupPeriod.Header.Appearance.FontData.Italic = DefaultableBoolean.True;
            parentBandGroupPeriod.Header.Appearance.FontData.SizeInPoints = 10;
            parentBandGroupPeriod.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupPeriod.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPeriod.Header.Appearance.BorderColor = Color.Black;

            UltraGridGroup parentBandGroupPS;
            if (parentBand.Groups.Exists("ParentBandGroupPS"))
            {
                parentBandGroupPS = parentBand.Groups["ParentBandGroupPS"];
            }
            else
                parentBandGroupPS = parentBand.Groups.Add("ParentBandGroupPS", "Chi phí phát sinh");
            parentBandGroupPS.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            parentBandGroupPS.Header.Appearance.FontData.SizeInPoints = 9;
            parentBandGroupPS.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupPS.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPS.Header.Appearance.BorderColor = Color.Black;
            parentBandGroupPS.Header.Appearance.BackColorAlpha = Alpha.Default;

            UltraGridGroup parentBandGroupTitle;
            if (parentBand.Groups.Exists("ParentBandGroupTitle"))
                parentBandGroupTitle = parentBand.Groups["ParentBandGroupTitle"];
            else
                parentBandGroupTitle = parentBand.Groups.Add("ParentBandGroupTitle", "SỔ CHI TIẾT LÃI LỖ THEO CÔNG TRÌNH");
            parentBandGroupTitle.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroupTitle.Header.Appearance.FontData.SizeInPoints = 12;
            parentBandGroupTitle.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupTitle.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupTitle.Header.Appearance.BorderColor = Color.Black;

            parentBandGroupPeriod.RowLayoutGroupInfo.ParentGroup = parentBandGroupTitle;
            parentBandGroupPS.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;

            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["PostedDate"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["PostedDate"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["PostedDate"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["PostedDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["PostedDate"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["Date"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["Date"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Date"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["Date"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["Date"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Date"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["No"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["No"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["No"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["No"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["No"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["No"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["Reason"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["Reason"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Reason"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["Reason"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["Reason"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Reason"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["DoanhThu"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["DoanhThu"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["DoanhThu"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["DoanhThu"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["DoanhThu"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["DoanhThu"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["GiamTru"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["GiamTru"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GiamTru"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["GiamTru"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["GiamTru"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GiamTru"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["NVLTT"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPS;
            parentBand.Columns["NVLTT"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["NVLTT"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["NVLTT"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["NVLTT"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["NVLTT"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["NCTT"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPS;
            parentBand.Columns["NCTT"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["NCTT"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["NCTT"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["NCTT"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["NCTT"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["CPSXC"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPS;
            parentBand.Columns["CPSXC"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["CPSXC"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["CPSXC"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["CPSXC"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["CPSXC"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["Cong"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPS;
            parentBand.Columns["Cong"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Cong"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["Cong"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["Cong"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Cong"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["GiaVon"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["GiaVon"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GiaVon"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["GiaVon"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["GiaVon"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GiaVon"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["LaiLo"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["LaiLo"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["LaiLo"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["LaiLo"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["LaiLo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["LaiLo"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.OriginX = 0;
            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["Date"].RowLayoutColumnInfo.OriginX = 120;
            parentBand.Columns["Date"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["Date"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["Date"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["No"].RowLayoutColumnInfo.OriginX = 240;
            parentBand.Columns["No"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["No"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["No"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["Reason"].RowLayoutColumnInfo.OriginX = 360;
            parentBand.Columns["Reason"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["Reason"].RowLayoutColumnInfo.SpanX = 270;
            parentBand.Columns["Reason"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["DoanhThu"].RowLayoutColumnInfo.OriginX = 630;
            parentBand.Columns["DoanhThu"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["DoanhThu"].RowLayoutColumnInfo.SpanX = 180;
            parentBand.Columns["DoanhThu"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["GiamTru"].RowLayoutColumnInfo.OriginX = 810;
            parentBand.Columns["GiamTru"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["GiamTru"].RowLayoutColumnInfo.SpanX = 180;
            parentBand.Columns["GiamTru"].RowLayoutColumnInfo.SpanY = 32;

            parentBandGroupPS.RowLayoutGroupInfo.OriginX = 990;
            parentBandGroupPS.RowLayoutGroupInfo.OriginY = 0;
            parentBandGroupPS.RowLayoutGroupInfo.SpanX = 720;
            parentBandGroupPS.RowLayoutGroupInfo.SpanY = 32;

            parentBand.Columns["GiaVon"].RowLayoutColumnInfo.OriginX = 1710;
            parentBand.Columns["GiaVon"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["GiaVon"].RowLayoutColumnInfo.SpanX = 180;
            parentBand.Columns["GiaVon"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["LaiLo"].RowLayoutColumnInfo.OriginX = 1890;
            parentBand.Columns["LaiLo"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["LaiLo"].RowLayoutColumnInfo.SpanX = 180;
            parentBand.Columns["LaiLo"].RowLayoutColumnInfo.SpanY = 32;
        }

        private void uGrid_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            UltraGridCell cell = e.Row.Cells[0];
            cell.Row.Height = 25;

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("Reason"))
                if (cell.Row.Cells["Reason"].Value != null && (cell.Row.Cells["Reason"].Value.ToString().Contains("Mã công trình: ") || cell.Row.Cells["Reason"].Value.ToString() == "Cộng nhóm" ))
                {
                    cell.Row.Cells["PostedDate"].Value = null;
                    cell.Row.Cells["Date"].Value = null;
                    cell.Row.Cells["Reason"].Appearance.FontData.Bold = DefaultableBoolean.True;
                }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("DoanhThu"))
            {
                if (cell.Row.Cells["DoanhThu"].Value != null && (decimal)cell.Row.Cells["DoanhThu"].Value == 0)
                    cell.Row.Cells["DoanhThu"].Value = null;
                if ((cell.Row.Cells["DoanhThu"].Value != null && (decimal)cell.Row.Cells["DoanhThu"].Value < 0))
                    cell.Row.Cells["DoanhThu"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("GiamTru"))
            {
                if (cell.Row.Cells["GiamTru"].Value != null && (decimal)cell.Row.Cells["GiamTru"].Value == 0)
                    cell.Row.Cells["GiamTru"].Value = null;
                if ((cell.Row.Cells["GiamTru"].Value != null && (decimal)cell.Row.Cells["GiamTru"].Value < 0))
                    cell.Row.Cells["GiamTru"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("NVLTT"))
            {
                if (cell.Row.Cells["NVLTT"].Value != null && (decimal)cell.Row.Cells["NVLTT"].Value == 0)
                    cell.Row.Cells["NVLTT"].Value = null;
                if ((cell.Row.Cells["NVLTT"].Value != null && (decimal)cell.Row.Cells["NVLTT"].Value < 0))
                    cell.Row.Cells["NVLTT"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("NCTT"))
            {
                if (cell.Row.Cells["NCTT"].Value != null && (decimal)cell.Row.Cells["NCTT"].Value == 0)
                    cell.Row.Cells["NCTT"].Value = null;
                if ((cell.Row.Cells["NCTT"].Value != null && (decimal)cell.Row.Cells["NCTT"].Value < 0))
                    cell.Row.Cells["NCTT"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("CPSXC"))
            {
                if (cell.Row.Cells["CPSXC"].Value != null && (decimal)cell.Row.Cells["CPSXC"].Value == 0)
                    cell.Row.Cells["CPSXC"].Value = null;
                if ((cell.Row.Cells["CPSXC"].Value != null && (decimal)cell.Row.Cells["CPSXC"].Value < 0))
                    cell.Row.Cells["CPSXC"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("Cong"))
            {
                if (cell.Row.Cells["Cong"].Value != null && (decimal)cell.Row.Cells["Cong"].Value == 0)
                    cell.Row.Cells["Cong"].Value = null;
                if ((cell.Row.Cells["Cong"].Value != null && (decimal)cell.Row.Cells["Cong"].Value < 0))
                    cell.Row.Cells["Cong"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("GiaVon"))
            {
                if (cell.Row.Cells["GiaVon"].Value != null && (decimal)cell.Row.Cells["GiaVon"].Value == 0)
                    cell.Row.Cells["GiaVon"].Value = null;
                if ((cell.Row.Cells["GiaVon"].Value != null && (decimal)cell.Row.Cells["GiaVon"].Value < 0))
                    cell.Row.Cells["GiaVon"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("LaiLo"))
            {
                if (cell.Row.Cells["LaiLo"].Value != null && (decimal)cell.Row.Cells["LaiLo"].Value == 0)
                    cell.Row.Cells["LaiLo"].Value = null;
                if ((cell.Row.Cells["LaiLo"].Value != null && (decimal)cell.Row.Cells["LaiLo"].Value < 0))
                    cell.Row.Cells["LaiLo"].Appearance.ForeColor = Color.Red;
            }

        }

        private void ultraGridExcelExporter1_InitializeColumn(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.InitializeColumnEventArgs e)
        {
            e.ExcelFormatStr = e.Column.Format;
        }

        private void ultraGridExcelExporter1_BeginExport(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.BeginExportEventArgs e)
        {
            foreach (var row in uGrid.Rows)
            {
                foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
                {
                    e.Rows[row.Index].Cells[column].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                    e.Rows[row.Index].Cells[column].Appearance.BorderColor = Color.Black;
                    e.Rows[row.Index].Cells[column].Column.Width = 200;
                }
            }
            e.Rows.Band.Summaries[0].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[0].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[1].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[1].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[2].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[2].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[3].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[3].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[4].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[4].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[5].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[5].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[6].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[6].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[7].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[7].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[8].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[8].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[9].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[9].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[10].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[10].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[11].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[11].Appearance.BorderColor = Color.Black;
        }

        private void FRB01aDNN_SizeChanged(object sender, EventArgs e)
        {
            var state = this.WindowState;
            switch (state)
            {
                case FormWindowState.Maximized:
                    break;
                case FormWindowState.Minimized:
                    break;
                case FormWindowState.Normal:
                    this.Left = (Screen.PrimaryScreen.Bounds.Width - this.Width) / 2;
                    this.Top = (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2;
                    this.Location = new Point((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2, (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
                    break;
                default:
                    break;
            }
        }

        private void ultraGridExcelExporter1_SummaryCellExported(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.SummaryCellExportedEventArgs e)
        {
            Worksheet ws = e.CurrentWorksheet;
            WorksheetRow wsRow = ws.Rows[e.CurrentRowIndex];
            WorksheetCell wsCell = wsRow.Cells[e.CurrentColumnIndex];
            if (!e.Summary.SummaryText.Contains("Số dòng") && !e.Summary.SummaryText.Contains(" "))
            {
                string s = e.Summary.SummaryText.Trim('.');
                if (s.Contains("("))
                    wsCell.Value = -decimal.Parse(s.Trim('(').Trim(')'));
                else
                    wsCell.Value = decimal.Parse(s);

                wsCell.CellFormat.FormatString = "###,###,###,###,##0;(###,###,###,###,##0)";
            }
        }

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                CTBook temp = (CTBook)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                if (temp != null)
                    editFuntion(temp);

            }
        }

        private void editFuntion(CTBook temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID, temp.RefType);
            //List<CTBook> data1 = sp.SOCHITIETCONGTRINH(FromDate, ToDate, lstItem);
            //configGrid(data1);
        }
    }
}
