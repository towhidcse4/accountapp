﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.AppStyling;
using System.IO;
using FX.Core;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinMaskedEdit;
using Infragistics.Win.UltraWinTabs;
using Accounting;
using Accounting.Core.DAO;
using System.Diagnostics;
using Infragistics.Documents.Excel;

namespace Accounting.Frm.FReport
{
    public partial class FRBANGCANDOITAIKHOANex : CustormForm
    {
        private string _subSystemCode;
        List<BalanceAccountF01> data = new List<BalanceAccountF01>();
        string period = "";
        DateTime FromDate;
        DateTime ToDate;
        int level;
        bool sodu;
        ReportProcedureSDS sp = new ReportProcedureSDS();
        public FRBANGCANDOITAIKHOANex(DateTime FromDate1, DateTime ToDate1, int level1, bool sodu1)
        {
            InitializeComponent();
            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\BangCDTKReport.rst", path);


            List<BalanceAccountF01> data1 = sp.GetBalanceAccountF01(FromDate1, ToDate1, level1, sodu1);
            data1.ForEach(t =>
            {
                t.OpeningDebitAmount = Covert_Money((decimal)t.OpeningDebitAmount);
                t.OpeningCreditAmount = Covert_Money((decimal)t.OpeningCreditAmount);
                t.DebitAmount = Covert_Money((decimal)t.DebitAmount);
                t.CreditAmount = Covert_Money((decimal)t.CreditAmount);
                t.ClosingDebitAmount = Covert_Money((decimal)t.ClosingDebitAmount);
                t.ClosingCreditAmount = Covert_Money((decimal)t.ClosingCreditAmount);
            });
            period = ReportUtils.GetPeriod(FromDate1, ToDate1);

            data = data1;
            FromDate = FromDate1;
            ToDate = ToDate1;
            level = level1;
            sodu = sodu1;

            configGrid(data1);

            if (Utils.isDemo)
                btnExport.Visible = false;
            else
                btnExport.Visible = true;
        }

        private void configGrid(List<BalanceAccountF01> lst)
        {

            uGrid.DataSource = lst;
            Utils.ConfigGrid(uGrid, ConstDatabase.BalanceAccountF01_TableName);
            uGrid.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;

            GridLayout();

            Utils.AddSumColumn(uGrid, "AccountName", false);
            uGrid.DisplayLayout.Bands[0].Summaries[1].DisplayFormat = " ";

            uGrid.DisplayLayout.Bands[0].Columns["OpeningDebitAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["OpeningCreditAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["ClosingDebitAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["ClosingCreditAmount"].FormatNumberic(ConstDatabase.Format_TienVND);

            uGrid.DisplayLayout.Bands[0].Columns["AccountNumber"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["AccountNumber"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["AccountNumber"].Width = 120;
            uGrid.DisplayLayout.Bands[0].Columns["AccountNumber"].CellAppearance.TextVAlign = VAlign.Middle;
            uGrid.DisplayLayout.Bands[0].Columns["AccountNumber"].CellAppearance.TextHAlign = HAlign.Center;

            uGrid.DisplayLayout.Bands[0].Columns["AccountName"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["AccountName"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["AccountName"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["AccountName"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["OpeningDebitAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["OpeningDebitAmount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["OpeningDebitAmount"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["OpeningDebitAmount"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["OpeningCreditAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["OpeningCreditAmount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["OpeningCreditAmount"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["OpeningCreditAmount"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["ClosingDebitAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["ClosingDebitAmount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["ClosingDebitAmount"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["ClosingDebitAmount"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["ClosingCreditAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["ClosingCreditAmount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["ClosingCreditAmount"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["ClosingCreditAmount"].CellAppearance.TextVAlign = VAlign.Middle;


            Utils.AddSumColumn1(lst.Where(p => p.AccountNumber.Length == 3 && p.OpeningDebitAmount != null).Sum(n => n.OpeningDebitAmount), uGrid, "OpeningDebitAmount", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(p => p.AccountNumber.Length == 3 && p.OpeningCreditAmount != null).Sum(n => n.OpeningCreditAmount), uGrid, "OpeningCreditAmount", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(p => p.AccountNumber.Length == 3 && p.DebitAmount != null).Sum(n => n.DebitAmount), uGrid, "DebitAmount", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(p => p.AccountNumber.Length == 3 && p.CreditAmount != null).Sum(n => n.CreditAmount), uGrid, "CreditAmount", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(p => p.AccountNumber.Length == 3 && p.ClosingDebitAmount != null).Sum(n => n.ClosingDebitAmount), uGrid, "ClosingDebitAmount", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(p => p.AccountNumber.Length == 3 && p.ClosingCreditAmount != null).Sum(n => n.ClosingCreditAmount), uGrid, "ClosingCreditAmount", false, constDatabaseFormat: 1);           
        }
       
        private void btnOk_Click(object sender, EventArgs e)
        {
            if (data.Count == 0)
            {
                MSG.Warning("Không có dữ liệu để lên báo cáo!");
            }
            else
            {
                var rD = new BalanceAccountF01Detail();
                rD.Period = ReportUtils.GetPeriod(FromDate, ToDate);
                var f = new ReportForm<BalanceAccountF01>(fileReportSlot1, _subSystemCode);
                f.AddDatasource("DBCanDoi", data, true);
                f.AddDatasource("Detail", rD);
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }
        private Decimal Covert_Money(decimal gt)
        {
            Decimal ketqua = 0;
            string s = "n";
            if (gt < 0)
            {
                string gt_am = String.Format("{0:" + Utils.GetTypeFormatNumberic(ConstDatabase.Format_TienVND) + ";" + Utils.GetFormatNegative().Replace("n", Utils.GetTypeFormatNumberic(ConstDatabase.Format_TienVND)) + "}", Convert.ToDecimal(gt));
                gt_am = gt_am.Replace('(', '-').Replace(")", string.Empty);
                ketqua = Convert.ToDecimal(gt_am);
            }
            else
            {
                string kq = String.Format("{0:" + Utils.GetTypeFormatNumberic(ConstDatabase.Format_TienVND) + ";" + s.Replace("n", Utils.GetTypeFormatNumberic(ConstDatabase.Format_TienVND)) + "}", Convert.ToDecimal(gt));
                ketqua = Convert.ToDecimal(kq);
            }

            return ketqua;
        }

        private void btnExport_Click(object sender, EventArgs e)
        {

            SaveFileDialog save = new SaveFileDialog
            {
                Title = "Chọn nơi sao lưu ",
                Filter = "File (*.xls)|*.xls",
                InitialDirectory = "@C:\\",
                FileName = "bangcandoitaikhoan.xls"
            };
            if (save.ShowDialog() == DialogResult.OK)
            {
                string duongdan = save.FileName;
                try
                {
                    ultraGridExcelExporter1.Export(uGrid, duongdan);
                    if (MSG.Question("Kết xuất thành công, Bạn có muốn mở file vừa kết xuất không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(save.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
                catch
                {
                    MSG.Warning("Lỗi khi kết xuất !");
                }
            }
        }
        private void GridLayout()
        {
            UltraGridBand parentBand = this.uGrid.DisplayLayout.Bands[0];
            parentBand.RowLayoutStyle = RowLayoutStyle.GroupLayout;

            UltraGridGroup parentBandGroupDK;
            if (parentBand.Groups.Exists("ParentBandGroupDK"))
                parentBandGroupDK = parentBand.Groups["ParentBandGroupDK"];
            else
                parentBandGroupDK = parentBand.Groups.Add("ParentBandGroupDK", "Đầu kỳ");
            parentBand.Columns["OpeningDebitAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupDK;
            parentBand.Columns["OpeningDebitAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["OpeningDebitAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["OpeningDebitAmount"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["OpeningDebitAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["OpeningDebitAmount"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["OpeningCreditAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupDK;
            parentBand.Columns["OpeningCreditAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["OpeningCreditAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["OpeningCreditAmount"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["OpeningCreditAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["OpeningCreditAmount"].Header.Appearance.BorderColor = Color.Black;

            UltraGridGroup parentBandGroupPS;
            if (parentBand.Groups.Exists("ParentBandGroupPS"))
                parentBandGroupPS = parentBand.Groups["ParentBandGroupPS"];
            else
                parentBandGroupPS = parentBand.Groups.Add("ParentBandGroupPS", "Phát sinh");
            parentBand.Columns["DebitAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPS;
            parentBand.Columns["DebitAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["DebitAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["DebitAmount"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["DebitAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["DebitAmount"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["CreditAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPS;
            parentBand.Columns["CreditAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["CreditAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["CreditAmount"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["CreditAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["CreditAmount"].Header.Appearance.BorderColor = Color.Black;

            UltraGridGroup parentBandGroupCK;
            if (parentBand.Groups.Exists("ParentBandGroupCK"))
                parentBandGroupCK = parentBand.Groups["ParentBandGroupCK"];
            else
                parentBandGroupCK = parentBand.Groups.Add("ParentBandGroupCK", "Cuối kỳ");
            parentBand.Columns["ClosingDebitAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupCK;
            parentBand.Columns["ClosingDebitAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ClosingDebitAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["ClosingDebitAmount"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["ClosingDebitAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ClosingDebitAmount"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["ClosingCreditAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupCK;
            parentBand.Columns["ClosingCreditAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ClosingCreditAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["ClosingCreditAmount"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["ClosingCreditAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ClosingCreditAmount"].Header.Appearance.BorderColor = Color.Black;

            UltraGridGroup parentBandGroupPeriod;
            if (parentBand.Groups.Exists("ParentBandGroupPeriod"))
            {
                parentBandGroupPeriod = parentBand.Groups["ParentBandGroupPeriod"];
                parentBandGroupPeriod.Header.Caption = period;
            }
            else
                parentBandGroupPeriod = parentBand.Groups.Add("ParentBandGroupPeriod", period);
            parentBandGroupPeriod.Header.Appearance.FontData.Italic = DefaultableBoolean.True;
            parentBandGroupPeriod.Header.Appearance.FontData.SizeInPoints = 10;
            parentBandGroupPeriod.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupPeriod.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPeriod.Header.Appearance.BorderColor = Color.Black;

            UltraGridGroup parentBandGroupTitle;
            if (parentBand.Groups.Exists("ParentBandGroupTitle"))
                parentBandGroupTitle = parentBand.Groups["ParentBandGroupTitle"];
            else
                parentBandGroupTitle = parentBand.Groups.Add("ParentBandGroupTitle", "BẢNG CÂN ĐỐI TÀI KHOẢN");
            parentBandGroupTitle.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroupTitle.Header.Appearance.FontData.SizeInPoints = 12;
            parentBandGroupTitle.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupTitle.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupTitle.Header.Appearance.BorderColor = Color.Black;


            parentBandGroupDK.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            parentBandGroupDK.Header.Appearance.FontData.SizeInPoints = 9;
            parentBandGroupDK.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupDK.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupDK.Header.Appearance.BorderColor = Color.Black;
            parentBandGroupDK.Header.Appearance.BackColorAlpha = Alpha.Default;

            parentBandGroupPS.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            parentBandGroupPS.Header.Appearance.FontData.SizeInPoints = 9;
            parentBandGroupPS.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupPS.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPS.Header.Appearance.BorderColor = Color.Black;
            parentBandGroupPS.Header.Appearance.BackColorAlpha = Alpha.Default;

            parentBandGroupCK.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            parentBandGroupCK.Header.Appearance.FontData.SizeInPoints = 9;
            parentBandGroupCK.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupCK.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupCK.Header.Appearance.BorderColor = Color.Black;
            parentBandGroupCK.Header.Appearance.BackColorAlpha = Alpha.Default;

            parentBandGroupPeriod.RowLayoutGroupInfo.ParentGroup = parentBandGroupTitle;

            parentBand.Columns["AccountNumber"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["AccountNumber"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["AccountNumber"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["AccountNumber"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["AccountNumber"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["AccountNumber"].Header.Appearance.BorderColor = Color.Black;


            parentBand.Columns["AccountName"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["AccountName"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["AccountName"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["AccountName"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["AccountName"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["AccountName"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["AccountNumber"].RowLayoutColumnInfo.OriginX = 0;
            parentBand.Columns["AccountNumber"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["AccountNumber"].RowLayoutColumnInfo.SpanX = 80;
            parentBand.Columns["AccountNumber"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["AccountName"].RowLayoutColumnInfo.OriginX = 80;
            parentBand.Columns["AccountName"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["AccountName"].RowLayoutColumnInfo.SpanX = 80;
            parentBand.Columns["AccountName"].RowLayoutColumnInfo.SpanY = 32;

            parentBandGroupDK.RowLayoutGroupInfo.OriginX = 160;
            parentBandGroupDK.RowLayoutGroupInfo.OriginY = 0;
            parentBandGroupDK.RowLayoutGroupInfo.SpanX = 320;
            parentBandGroupDK.RowLayoutGroupInfo.SpanY = 32;

            parentBandGroupPS.RowLayoutGroupInfo.OriginX = 480;
            parentBandGroupPS.RowLayoutGroupInfo.OriginY = 0;
            parentBandGroupPS.RowLayoutGroupInfo.SpanX = 320;
            parentBandGroupPS.RowLayoutGroupInfo.SpanY = 32;

            parentBandGroupCK.RowLayoutGroupInfo.OriginX = 800;
            parentBandGroupCK.RowLayoutGroupInfo.OriginY = 0;
            parentBandGroupCK.RowLayoutGroupInfo.SpanX = 320;
            parentBandGroupCK.RowLayoutGroupInfo.SpanY = 32;

        }

        private void ultraGridExcelExporter1_InitializeColumn(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.InitializeColumnEventArgs e)
        {
            e.ExcelFormatStr = e.Column.Format;
        }

        private void ultraGridExcelExporter1_BeginExport(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.BeginExportEventArgs e)
        {
            foreach (var row in uGrid.Rows)
            {
                foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
                {
                    e.Rows[row.Index].Cells[column].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                    e.Rows[row.Index].Cells[column].Appearance.BorderColor = Color.Black;
                    e.Rows[row.Index].Cells[column].Column.Width = 200;
                }
            }
            e.Rows.Band.Summaries[0].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[0].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[1].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[1].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[2].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[2].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[3].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[3].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[4].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[4].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[5].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[5].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[6].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[6].Appearance.BorderColor = Color.Black;
        }

        private void uGrid_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            UltraGridCell cell = e.Row.Cells[0];
            cell.Row.Height = 25;

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("AccountNumber"))
                if(cell.Row.Cells["AccountNumber"].Value != null)
                {
                    cell.Row.Cells["AccountNumber"].Appearance.FontData.Underline = DefaultableBoolean.True;
                    cell.Row.Cells["AccountNumber"].Appearance.Cursor = Cursors.Hand;
                }    
                if (cell.Row.Cells["AccountNumber"].Value != null && cell.Row.Cells["AccountNumber"].Value.ToString().Length == 3)
                {
                    cell.Row.Cells["AccountNumber"].Appearance.FontData.Bold = DefaultableBoolean.True;                   
                    cell.Row.Cells["AccountName"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    cell.Row.Cells["OpeningDebitAmount"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    cell.Row.Cells["OpeningCreditAmount"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    cell.Row.Cells["DebitAmount"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    cell.Row.Cells["CreditAmount"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    cell.Row.Cells["ClosingDebitAmount"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    cell.Row.Cells["ClosingCreditAmount"].Appearance.FontData.Bold = DefaultableBoolean.True;
                }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("OpeningDebitAmount"))
            {
                if (cell.Row.Cells["OpeningDebitAmount"].Value != null && (decimal)cell.Row.Cells["OpeningDebitAmount"].Value == 0)
                    cell.Row.Cells["OpeningDebitAmount"].Value = null;
                if ((cell.Row.Cells["OpeningDebitAmount"].Value != null && (decimal)cell.Row.Cells["OpeningDebitAmount"].Value < 0))
                    cell.Row.Cells["OpeningDebitAmount"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("OpeningCreditAmount"))
            {
                if (cell.Row.Cells["OpeningCreditAmount"].Value != null && (decimal)cell.Row.Cells["OpeningCreditAmount"].Value == 0)
                    cell.Row.Cells["OpeningCreditAmount"].Value = null;
                if ((cell.Row.Cells["OpeningCreditAmount"].Value != null && (decimal)cell.Row.Cells["OpeningCreditAmount"].Value < 0))
                    cell.Row.Cells["OpeningCreditAmount"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("CreditAmount"))
            {
                if (cell.Row.Cells["CreditAmount"].Value != null && (decimal)cell.Row.Cells["CreditAmount"].Value == 0)
                    cell.Row.Cells["CreditAmount"].Value = null;
                if ((cell.Row.Cells["CreditAmount"].Value != null && (decimal)cell.Row.Cells["CreditAmount"].Value < 0))
                    cell.Row.Cells["CreditAmount"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("DebitAmount"))
            {
                if (cell.Row.Cells["DebitAmount"].Value != null && (decimal)cell.Row.Cells["DebitAmount"].Value == 0)
                    cell.Row.Cells["DebitAmount"].Value = null;
                if ((cell.Row.Cells["DebitAmount"].Value != null && (decimal)cell.Row.Cells["DebitAmount"].Value < 0))
                    cell.Row.Cells["DebitAmount"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("ClosingDebitAmount"))
            {
                if (cell.Row.Cells["ClosingDebitAmount"].Value != null && (decimal)cell.Row.Cells["ClosingDebitAmount"].Value == 0)
                    cell.Row.Cells["ClosingDebitAmount"].Value = null;
                if ((cell.Row.Cells["ClosingDebitAmount"].Value != null && (decimal)cell.Row.Cells["ClosingDebitAmount"].Value < 0))
                    cell.Row.Cells["ClosingDebitAmount"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("ClosingCreditAmount"))
            {
                if (cell.Row.Cells["ClosingCreditAmount"].Value != null && (decimal)cell.Row.Cells["ClosingCreditAmount"].Value == 0)
                    cell.Row.Cells["ClosingCreditAmount"].Value = null;
                if ((cell.Row.Cells["ClosingCreditAmount"].Value != null && (decimal)cell.Row.Cells["ClosingCreditAmount"].Value < 0))
                    cell.Row.Cells["ClosingCreditAmount"].Appearance.ForeColor = Color.Red;
            }

        }

        private void FRBANGCANDOITAIKHOAN_SizeChanged(object sender, EventArgs e)
        {
            var state = this.WindowState;
            switch (state)
            {
                case FormWindowState.Maximized:
                    break;
                case FormWindowState.Minimized:
                    break;
                case FormWindowState.Normal:
                    this.Left = (Screen.PrimaryScreen.Bounds.Width - this.Width) / 2;
                    this.Top = (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2;
                    this.Location = new Point((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2, (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
                    break;
                default:
                    break;
            }
        }

        private void ultraGridExcelExporter1_SummaryCellExported(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.SummaryCellExportedEventArgs e)
        {
            Worksheet ws = e.CurrentWorksheet;
            WorksheetRow wsRow = ws.Rows[e.CurrentRowIndex];
            WorksheetCell wsCell = wsRow.Cells[e.CurrentColumnIndex];
            if (!e.Summary.SummaryText.Contains("Số dòng") && !e.Summary.SummaryText.Contains(" "))
            {
                string s = e.Summary.SummaryText.Trim('.');
                if (s.Contains("("))
                    wsCell.Value = -decimal.Parse(s.Trim('(').Trim(')'));
                else
                    wsCell.Value = decimal.Parse(s);

                wsCell.CellFormat.FormatString = "###,###,###,###,##0;(###,###,###,###,##0)";
            }
        }

        private void uGrid_ClickCell(object sender, ClickCellEventArgs e)
        {
            if(e.Cell.Column.ToString()== "AccountNumber")
            {
                ReportUtils.CallFormBC("AccountNumber", e.Cell.Value.ToString(), FromDate, ToDate);
            }    
        }
    }

}
