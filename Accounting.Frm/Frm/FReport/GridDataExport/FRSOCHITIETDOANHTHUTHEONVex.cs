﻿using Accounting.Core.DAO;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.Report;
using Accounting.TextMessage;
using Infragistics.Documents.Excel;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRSOCHITIETDOANHTHUTHEONVex : Form
    {
        private string _subSystemCode;
        ReportProcedureSDS sp = new ReportProcedureSDS();
        List<AccountingObjectEmployees> data = new List<AccountingObjectEmployees>();
        string period = "";
        DateTime FromDate;
        DateTime ToDate;
        string lst_acc = "";
        public FRSOCHITIETDOANHTHUTHEONVex(DateTime FromDate1, DateTime ToDate1, string lst_acc1)
        {
            InitializeComponent();
            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(
                 System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\SoChiTietDoanhThuTheoNV.rst", path);

            List<AccountingObjectEmployees> data1 = sp.GETDOANHTHUNV(FromDate1, ToDate1, lst_acc1);
            period = ReportUtils.GetPeriod(FromDate1, ToDate1);

            FromDate = FromDate1;
            ToDate = ToDate1;
            lst_acc = lst_acc1;
            data = data1;

            configGrid(data1);
            if (Utils.isDemo)
                btnExport.Visible = false;
            else
                btnExport.Visible = true;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            List<AccountingObjectEmployees> data = sp.GETDOANHTHUNV(FromDate, ToDate, lst_acc);
            data.ForEach(t =>
            {
                if (t.DienGiai == null)
                    t.DienGiai = "";
                else
                    t.DienGiai = t.DienGiai;
            });

            data = data.Where(t => t.DienGiai != "Cộng nhóm" && !t.DienGiai.Contains("Mã nhân viên: ")).ToList();
            if (data.Count == 0)
                MSG.Warning("Không có dữ liệu");
            else
            {
                var rD = new AccountingObjectEmployees_Period();
                rD.Period = ReportUtils.GetPeriod(FromDate, ToDate);
                var f = new ReportForm<AccountingObjectEmployees>(fileReportSlot1, _subSystemCode);
                f.AddDatasource("DoanhThuNV", data, true);
                f.AddDatasource("Detail", rD);
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog
            {
                Title = "Chọn nơi sao lưu ",
                Filter = "File (*.xls)|*.xls",
                InitialDirectory = "@C:\\",
                FileName = "sochitietdoanhthutheonv.xls"
            };
            if (save.ShowDialog() == DialogResult.OK)
            {
                string duongdan = save.FileName;
                try
                {
                    ultraGridExcelExporter1.Export(uGrid, duongdan);
                    if (MSG.Question("Kết xuất thành công, Bạn có muốn mở file vừa kết xuất không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(save.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
                catch
                {
                    MSG.Warning("Lỗi khi kết xuất !");
                }
            }
        }     

        private void configGrid(List<AccountingObjectEmployees> lst)
        {
            uGrid.DataSource = lst;
            Utils.ConfigGrid(uGrid, ConstDatabase.Proc_SoChiTietDoanhThuTheoNhanVien_TableName);
            uGrid.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;

            lst.ForEach(t =>
            {
                if (t.DienGiai == null)
                    t.DienGiai = "";
                else
                    t.DienGiai = t.DienGiai;
            });

            Utils.AddSumColumn(uGrid, "NgayHachToan", false);
            Utils.AddSumColumn(uGrid, "SoChungTu", false);
            Utils.AddSumColumn(uGrid, "DienGiai", false);
            Utils.AddSumColumn(uGrid, "GhiChu", false);

            uGrid.DisplayLayout.Bands[0].Summaries[1].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[2].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[3].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[4].DisplayFormat = " ";

            Utils.AddSumColumn1(lst.Where(t => t.DienGiai != "Cộng nhóm" && t.DoanhSo != null).Sum(t => t.DoanhSo), uGrid, "DoanhSo", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(t => t.DienGiai != "Cộng nhóm" && t.GiamDoanhThu != null).Sum(t => t.GiamDoanhThu), uGrid, "GiamDoanhThu", false, constDatabaseFormat: 1);

            uGrid.DisplayLayout.Bands[0].Columns["NgayChungTu"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["NgayChungTu"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["NgayChungTu"].Width = 120;

            uGrid.DisplayLayout.Bands[0].Columns["NgayHachToan"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["NgayHachToan"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["NgayHachToan"].CellAppearance.TextHAlign = HAlign.Center;
            uGrid.DisplayLayout.Bands[0].Columns["NgayHachToan"].Width = 120;

            uGrid.DisplayLayout.Bands[0].Columns["SoChungTu"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["SoChungTu"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["SoChungTu"].Width = 120;
            uGrid.DisplayLayout.Bands[0].Columns["SoChungTu"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["DienGiai"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["DienGiai"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["DienGiai"].Width = 400;
            uGrid.DisplayLayout.Bands[0].Columns["DienGiai"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["DoanhSo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["DoanhSo"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["DoanhSo"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["DoanhSo"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["DoanhSo"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["GiamDoanhThu"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["GiamDoanhThu"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["GiamDoanhThu"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["GiamDoanhThu"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["GiamDoanhThu"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["GhiChu"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["GhiChu"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["GhiChu"].Width = 180;


            GridLayout();
        }
        private void GridLayout()
        {
            UltraGridBand parentBand = this.uGrid.DisplayLayout.Bands[0];
            parentBand.RowLayoutStyle = RowLayoutStyle.GroupLayout;

            UltraGridGroup parentBandGroupPeriod;
            if (parentBand.Groups.Exists("ParentBandGroupPeriod"))
            {
                parentBandGroupPeriod = parentBand.Groups["ParentBandGroupPeriod"];
                parentBandGroupPeriod.Header.Caption = period;
            }
            else
                parentBandGroupPeriod = parentBand.Groups.Add("ParentBandGroupPeriod", period);
            parentBandGroupPeriod.Header.Appearance.FontData.Italic = DefaultableBoolean.True;
            parentBandGroupPeriod.Header.Appearance.FontData.SizeInPoints = 10;
            parentBandGroupPeriod.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupPeriod.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPeriod.Header.Appearance.BorderColor = Color.Black;

            UltraGridGroup parentBandGroupTitle;
            if (parentBand.Groups.Exists("ParentBandGroupTitle"))
                parentBandGroupTitle = parentBand.Groups["ParentBandGroupTitle"];
            else
                parentBandGroupTitle = parentBand.Groups.Add("ParentBandGroupTitle", "SỔ CHI TIẾT DOANH THU THEO NHÂN VIÊN");
            parentBandGroupTitle.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroupTitle.Header.Appearance.FontData.SizeInPoints = 12;
            parentBandGroupTitle.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupTitle.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupTitle.Header.Appearance.BorderColor = Color.Black;

            parentBandGroupPeriod.RowLayoutGroupInfo.ParentGroup = parentBandGroupTitle;

            parentBand.Columns["NgayChungTu"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["NgayChungTu"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["NgayChungTu"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["NgayChungTu"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["NgayChungTu"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["NgayChungTu"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["NgayHachToan"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["NgayHachToan"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["NgayHachToan"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["NgayHachToan"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["NgayHachToan"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["NgayHachToan"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["SoChungTu"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["SoChungTu"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SoChungTu"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["SoChungTu"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["SoChungTu"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SoChungTu"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["DienGiai"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["DienGiai"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["DienGiai"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["DienGiai"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["DienGiai"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["DienGiai"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["DoanhSo"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["DoanhSo"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["DoanhSo"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["DoanhSo"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["DoanhSo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["DoanhSo"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["GiamDoanhThu"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["GiamDoanhThu"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GiamDoanhThu"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["GiamDoanhThu"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["GiamDoanhThu"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GiamDoanhThu"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["GhiChu"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["GhiChu"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GhiChu"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["GhiChu"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["GhiChu"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GhiChu"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["NgayChungTu"].RowLayoutColumnInfo.OriginX = 0;
            parentBand.Columns["NgayChungTu"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["NgayChungTu"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["NgayChungTu"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["NgayHachToan"].RowLayoutColumnInfo.OriginX = 120;
            parentBand.Columns["NgayHachToan"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["NgayHachToan"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["NgayHachToan"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["SoChungTu"].RowLayoutColumnInfo.OriginX = 240;
            parentBand.Columns["SoChungTu"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["SoChungTu"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["SoChungTu"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["DienGiai"].RowLayoutColumnInfo.OriginX = 360;
            parentBand.Columns["DienGiai"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["DienGiai"].RowLayoutColumnInfo.SpanX = 400;
            parentBand.Columns["DienGiai"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["DoanhSo"].RowLayoutColumnInfo.OriginX = 760;
            parentBand.Columns["DoanhSo"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["DoanhSo"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["DoanhSo"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["GiamDoanhThu"].RowLayoutColumnInfo.OriginX = 1010;
            parentBand.Columns["GiamDoanhThu"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["GiamDoanhThu"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["GiamDoanhThu"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["GhiChu"].RowLayoutColumnInfo.OriginX = 1260;
            parentBand.Columns["GhiChu"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["GhiChu"].RowLayoutColumnInfo.SpanX = 150;
            parentBand.Columns["GhiChu"].RowLayoutColumnInfo.SpanY = 32;
        }

        private void uGrid_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            UltraGridCell cell = e.Row.Cells[0];
            cell.Row.Height = 25;

            if(uGrid.DisplayLayout.Bands[0].Columns.Exists("DienGiai"))
            {
                if(cell.Row.Cells["DienGiai"].Value.ToString() == "Cộng nhóm" || cell.Row.Cells["DienGiai"].Value.ToString().Contains("Mã nhân viên: "))
                {
                    cell.Row.Cells["DienGiai"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    cell.Row.Cells["DoanhSo"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    cell.Row.Cells["GiamDoanhThu"].Appearance.FontData.Bold = DefaultableBoolean.True;
                }
            }
            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("DoanhSo"))
            {
                if (cell.Row.Cells["DoanhSo"].Value != null && (decimal)cell.Row.Cells["DoanhSo"].Value == 0)
                    cell.Row.Cells["DoanhSo"].Value = null;
                if ((cell.Row.Cells["DoanhSo"].Value != null && (decimal)cell.Row.Cells["DoanhSo"].Value < 0))
                    cell.Row.Cells["DoanhSo"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("GiamDoanhThu"))
            {
                if (cell.Row.Cells["GiamDoanhThu"].Value != null && (decimal)cell.Row.Cells["GiamDoanhThu"].Value == 0)
                    cell.Row.Cells["GiamDoanhThu"].Value = null;
                if ((cell.Row.Cells["GiamDoanhThu"].Value != null && (decimal)cell.Row.Cells["GiamDoanhThu"].Value < 0))
                    cell.Row.Cells["GiamDoanhThu"].Appearance.ForeColor = Color.Red;
            }

        }

        private void ultraGridExcelExporter1_InitializeColumn(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.InitializeColumnEventArgs e)
        {
            e.ExcelFormatStr = e.Column.Format;
        }

        private void ultraGridExcelExporter1_BeginExport(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.BeginExportEventArgs e)
        {
            foreach (var row in uGrid.Rows)
            {
                foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
                {
                    e.Rows[row.Index].Cells[column].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                    e.Rows[row.Index].Cells[column].Appearance.BorderColor = Color.Black;
                    e.Rows[row.Index].Cells[column].Column.Width = 200;
                }
            }
            e.Rows.Band.Summaries[0].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[0].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[1].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[1].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[2].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[2].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[3].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[3].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[4].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[4].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[5].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[5].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[6].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[6].Appearance.BorderColor = Color.Black;
        }

        private void FRB01aDNN_SizeChanged(object sender, EventArgs e)
        {
            var state = this.WindowState;
            switch (state)
            {
                case FormWindowState.Maximized:
                    break;
                case FormWindowState.Minimized:
                    break;
                case FormWindowState.Normal:
                    this.Left = (Screen.PrimaryScreen.Bounds.Width - this.Width) / 2;
                    this.Top = (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2;
                    this.Location = new Point((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2, (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
                    break;
                default:
                    break;
            }
        }

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                AccountingObjectEmployees temp = (AccountingObjectEmployees)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                if (temp != null)
                    editFuntion(temp);

            }
        }

        private void editFuntion(AccountingObjectEmployees temp)
        {

            var f = Utils.ViewVoucherSelected1(temp.RefID, temp.RefType);
            //List<AccountingObjectEmployees> data1 = sp.GETDOANHTHUNV(FromDate, ToDate, lst_acc);
            //configGrid(data1);

        }

        private void ultraGridExcelExporter1_SummaryCellExported(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.SummaryCellExportedEventArgs e)
        {
            Worksheet ws = e.CurrentWorksheet;
            WorksheetRow wsRow = ws.Rows[e.CurrentRowIndex];
            WorksheetCell wsCell = wsRow.Cells[e.CurrentColumnIndex];
            if (!e.Summary.SummaryText.Contains("Số dòng") && !e.Summary.SummaryText.Contains(" "))
            {
                string s = e.Summary.SummaryText.Trim('.');
                if (s.Contains("("))
                    wsCell.Value = -decimal.Parse(s.Trim('(').Trim(')'));
                else
                    wsCell.Value = decimal.Parse(s);

                wsCell.CellFormat.FormatString = "###,###,###,###,##0;(###,###,###,###,##0)";
            }
        }
    }
}
