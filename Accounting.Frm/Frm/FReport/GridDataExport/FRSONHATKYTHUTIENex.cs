﻿using Accounting.Core.DAO;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.Report;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Documents.Excel;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRSONHATKYTHUTIENex : CustormForm
    {
        private string _subSystemCode;
        private readonly IBankAccountDetailService _IBankAccountDetailService;
        ReportProcedureSDS sp = new ReportProcedureSDS();
        List<GLR_GetS03a1_DN> data = new List<GLR_GetS03a1_DN>();
        string period = "";
        DateTime FromDate;
        DateTime ToDate;
        string account;
        string currency;
        string bankAccount;
        int check;
        public FRSONHATKYTHUTIENex(DateTime FromDate1, DateTime ToDate1, string account1, string currency1, string bankAccount1, int check1)
        {
            InitializeComponent();
            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\SO_NHATKYTHUTIEN.rst", path);
            List<GLR_GetS03a1_DN>  data1 = sp.Get_SoNhatKyThuTien(FromDate1, ToDate1, account1, currency1, bankAccount1, check1);
            period = ReportUtils.GetPeriod(FromDate1, ToDate1);
            data = data1;
            account = account1;
            currency = currency1;
            bankAccount = bankAccount1;
            check = check1;
            FromDate = FromDate1;
            ToDate = ToDate1;
            configGrid(data);
            if (Utils.isDemo)
                btnExport.Visible = false;
            else
                btnExport.Visible = true;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {           
            if (data.Count == 0)
            {
                MSG.Warning("Không có dữ liệu để lên báo cáo");
            }
            else
            {
                var rD = new GLR_GetS03a1_DNDetail();
                rD.Period = ReportUtils.GetPeriod(FromDate, ToDate);
                rD.TenTK = "Ghi nợ TK " + account;
                var f = new ReportForm<SA_GetDetailPayS12>(fileReportSlot1, _subSystemCode);
                f.AddDatasource("DBSONKTHUTIEN", data);
                f.AddDatasource("Detail", rD);
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog
            {
                Title = "Chọn nơi sao lưu ",
                Filter = "File (*.xls)|*.xls",
                InitialDirectory = "@C:\\",
                FileName = "sonhatkythutien.xls"
            };
            if (save.ShowDialog() == DialogResult.OK)
            {
                string duongdan = save.FileName;
                try
                {
                    ultraGridExcelExporter1.Export(uGrid, duongdan);
                    if (MSG.Question("Kết xuất thành công, Bạn có muốn mở file vừa kết xuất không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(save.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
                catch
                {
                    MSG.Warning("Lỗi khi kết xuất !");
                }
            }
        }

        private void configGrid(List<GLR_GetS03a1_DN> lst)
        {
            uGrid.DataSource = lst;
            Utils.ConfigGrid(uGrid, ConstDatabase.GLR_GetS03a1_DN_TableName);
            uGrid.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;

            Utils.AddSumColumn(uGrid, "RefNo", false);
            Utils.AddSumColumn(uGrid, "RefDate", false);
            Utils.AddSumColumn(uGrid, "Description", false);
            Utils.AddSumColumn(uGrid, "ColOtherAccount", false);

            uGrid.DisplayLayout.Bands[0].Summaries[1].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[2].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[3].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[4].DisplayFormat = " ";

            uGrid.DisplayLayout.Bands[0].Columns["PostedDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["PostedDate"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["PostedDate"].Width = 100;

            uGrid.DisplayLayout.Bands[0].Columns["RefNo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["RefNo"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["RefNo"].Width = 100;
            uGrid.DisplayLayout.Bands[0].Columns["RefNo"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["RefDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["RefDate"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["RefDate"].Width = 100;

            uGrid.DisplayLayout.Bands[0].Columns["Description"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["Description"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["Description"].Width = 300;
            uGrid.DisplayLayout.Bands[0].Columns["Description"].CellAppearance.TextVAlign = VAlign.Middle;

            if (currency == "VND")
            {
                uGrid.DisplayLayout.Bands[0].Columns["Amount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                uGrid.DisplayLayout.Bands[0].Columns["Amount"].Header.Appearance.BorderColor = Color.Black;
                uGrid.DisplayLayout.Bands[0].Columns["Amount"].Width = 120;
                uGrid.DisplayLayout.Bands[0].Columns["Amount"].Header.Caption = "Ghi nợ TK " + account;
                uGrid.DisplayLayout.Bands[0].Columns["Amount"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGrid.DisplayLayout.Bands[0].Columns["Amount"].CellAppearance.TextVAlign = VAlign.Middle;

                uGrid.DisplayLayout.Bands[0].Columns["Col2"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                uGrid.DisplayLayout.Bands[0].Columns["Col2"].Header.Appearance.BorderColor = Color.Black;
                uGrid.DisplayLayout.Bands[0].Columns["Col2"].Width = 180;
                uGrid.DisplayLayout.Bands[0].Columns["Col2"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGrid.DisplayLayout.Bands[0].Columns["Col2"].CellAppearance.TextVAlign = VAlign.Middle;

                uGrid.DisplayLayout.Bands[0].Columns["Col3"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                uGrid.DisplayLayout.Bands[0].Columns["Col3"].Header.Appearance.BorderColor = Color.Black;
                uGrid.DisplayLayout.Bands[0].Columns["Col3"].Width = 180;
                uGrid.DisplayLayout.Bands[0].Columns["Col3"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGrid.DisplayLayout.Bands[0].Columns["Col3"].CellAppearance.TextVAlign = VAlign.Middle;

                uGrid.DisplayLayout.Bands[0].Columns["Col4"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                uGrid.DisplayLayout.Bands[0].Columns["Col4"].Header.Appearance.BorderColor = Color.Black;
                uGrid.DisplayLayout.Bands[0].Columns["Col4"].Width = 180;
                uGrid.DisplayLayout.Bands[0].Columns["Col4"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGrid.DisplayLayout.Bands[0].Columns["Col4"].CellAppearance.TextVAlign = VAlign.Middle;

                uGrid.DisplayLayout.Bands[0].Columns["Col5"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                uGrid.DisplayLayout.Bands[0].Columns["Col5"].Header.Appearance.BorderColor = Color.Black;
                uGrid.DisplayLayout.Bands[0].Columns["Col5"].Width = 180;
                uGrid.DisplayLayout.Bands[0].Columns["Col5"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGrid.DisplayLayout.Bands[0].Columns["Col5"].CellAppearance.TextVAlign = VAlign.Middle;

                uGrid.DisplayLayout.Bands[0].Columns["Col7"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                uGrid.DisplayLayout.Bands[0].Columns["Col7"].Header.Appearance.BorderColor = Color.Black;
                uGrid.DisplayLayout.Bands[0].Columns["Col7"].Width = 180;
                uGrid.DisplayLayout.Bands[0].Columns["Col7"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGrid.DisplayLayout.Bands[0].Columns["Col7"].CellAppearance.TextVAlign = VAlign.Middle;

                Utils.AddSumColumn1(lst.Where(n => n.Amount != null).Sum(n => n.Amount), uGrid, "Amount", false, constDatabaseFormat: 1);
                Utils.AddSumColumn1(lst.Where(n => n.Col2 != null).Sum(n => n.Col2), uGrid, "Col2", false, constDatabaseFormat: 1);
                Utils.AddSumColumn1(lst.Where(n => n.Col3 != null).Sum(n => n.Col3), uGrid, "Col3", false, constDatabaseFormat: 1);
                Utils.AddSumColumn1(lst.Where(n => n.Col4 != null).Sum(n => n.Col4), uGrid, "Col4", false, constDatabaseFormat: 1);
                Utils.AddSumColumn1(lst.Where(n => n.Col5 != null).Sum(n => n.Col5), uGrid, "Col5", false, constDatabaseFormat: 1);
                Utils.AddSumColumn1(lst.Where(n => n.Col7 != null).Sum(n => n.Col7), uGrid, "Col7", false, constDatabaseFormat: 1);
            }
            else
            {
                uGrid.DisplayLayout.Bands[0].Columns["Amount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                uGrid.DisplayLayout.Bands[0].Columns["Amount"].Header.Appearance.BorderColor = Color.Black;
                uGrid.DisplayLayout.Bands[0].Columns["Amount"].Width = 120;
                uGrid.DisplayLayout.Bands[0].Columns["Amount"].Header.Caption = "Ghi nợ TK " + account;
                uGrid.DisplayLayout.Bands[0].Columns["Amount"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                uGrid.DisplayLayout.Bands[0].Columns["Amount"].CellAppearance.TextVAlign = VAlign.Middle;

                uGrid.DisplayLayout.Bands[0].Columns["Col2"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                uGrid.DisplayLayout.Bands[0].Columns["Col2"].Header.Appearance.BorderColor = Color.Black;
                uGrid.DisplayLayout.Bands[0].Columns["Col2"].Width = 180;
                uGrid.DisplayLayout.Bands[0].Columns["Col2"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                uGrid.DisplayLayout.Bands[0].Columns["Col2"].CellAppearance.TextVAlign = VAlign.Middle;

                uGrid.DisplayLayout.Bands[0].Columns["Col3"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                uGrid.DisplayLayout.Bands[0].Columns["Col3"].Header.Appearance.BorderColor = Color.Black;
                uGrid.DisplayLayout.Bands[0].Columns["Col3"].Width = 180;
                uGrid.DisplayLayout.Bands[0].Columns["Col3"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                uGrid.DisplayLayout.Bands[0].Columns["Col3"].CellAppearance.TextVAlign = VAlign.Middle;

                uGrid.DisplayLayout.Bands[0].Columns["Col4"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                uGrid.DisplayLayout.Bands[0].Columns["Col4"].Header.Appearance.BorderColor = Color.Black;
                uGrid.DisplayLayout.Bands[0].Columns["Col4"].Width = 180;
                uGrid.DisplayLayout.Bands[0].Columns["Col4"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                uGrid.DisplayLayout.Bands[0].Columns["Col4"].CellAppearance.TextVAlign = VAlign.Middle;

                uGrid.DisplayLayout.Bands[0].Columns["Col5"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                uGrid.DisplayLayout.Bands[0].Columns["Col5"].Header.Appearance.BorderColor = Color.Black;
                uGrid.DisplayLayout.Bands[0].Columns["Col5"].Width = 180;
                uGrid.DisplayLayout.Bands[0].Columns["Col5"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                uGrid.DisplayLayout.Bands[0].Columns["Col5"].CellAppearance.TextVAlign = VAlign.Middle;

                uGrid.DisplayLayout.Bands[0].Columns["Col7"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                uGrid.DisplayLayout.Bands[0].Columns["Col7"].Header.Appearance.BorderColor = Color.Black;
                uGrid.DisplayLayout.Bands[0].Columns["Col7"].Width = 180;
                uGrid.DisplayLayout.Bands[0].Columns["Col7"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                uGrid.DisplayLayout.Bands[0].Columns["Col7"].CellAppearance.TextVAlign = VAlign.Middle;

                Utils.AddSumColumn1(lst.Where(n => n.Amount != null).Sum(n => n.Amount), uGrid, "Amount", false, constDatabaseFormat: 2);
                Utils.AddSumColumn1(lst.Where(n => n.Col2 != null).Sum(n => n.Col2), uGrid, "Col2", false, constDatabaseFormat: 2);
                Utils.AddSumColumn1(lst.Where(n => n.Col3 != null).Sum(n => n.Col3), uGrid, "Col3", false, constDatabaseFormat: 2);
                Utils.AddSumColumn1(lst.Where(n => n.Col4 != null).Sum(n => n.Col4), uGrid, "Col4", false, constDatabaseFormat: 2);
                Utils.AddSumColumn1(lst.Where(n => n.Col5 != null).Sum(n => n.Col5), uGrid, "Col5", false, constDatabaseFormat: 2);
                Utils.AddSumColumn1(lst.Where(n => n.Col7 != null).Sum(n => n.Col7), uGrid, "Col7", false, constDatabaseFormat: 2);
            }

            uGrid.DisplayLayout.Bands[0].Columns["ColOtherAccount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["ColOtherAccount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["ColOtherAccount"].Width = 100;
            uGrid.DisplayLayout.Bands[0].Columns["ColOtherAccount"].CellAppearance.TextVAlign = VAlign.Middle;
            uGrid.DisplayLayout.Bands[0].Columns["ColOtherAccount"].CellAppearance.TextHAlign = HAlign.Center;


            string dstaikhoan = lst.Count != 0 ? lst.FirstOrDefault().AccountNumberList : "";
            var list_tk = dstaikhoan.Split(',');
            if (list_tk.Length > 1)
            {
                uGrid.DisplayLayout.Bands[0].Columns["Col2"].Header.Caption = list_tk[1];
            }
            if (list_tk.Length > 2)
            {
                uGrid.DisplayLayout.Bands[0].Columns["Col3"].Header.Caption = list_tk[2];
            }
            if (list_tk.Length > 3)
            {
                uGrid.DisplayLayout.Bands[0].Columns["Col4"].Header.Caption = list_tk[3];
            }
            if (list_tk.Length > 4)
            {
                uGrid.DisplayLayout.Bands[0].Columns["Col5"].Header.Caption = list_tk[4];
            }

            GridLayout();
        }

        private void GridLayout()
        {
            UltraGridBand parentBand = this.uGrid.DisplayLayout.Bands[0];
            parentBand.RowLayoutStyle = RowLayoutStyle.GroupLayout;

            UltraGridGroup parentBandGroupPeriod;
            if (parentBand.Groups.Exists("ParentBandGroupPeriod"))
            {
                parentBandGroupPeriod = parentBand.Groups["ParentBandGroupPeriod"];
                parentBandGroupPeriod.Header.Caption = period;
            }
            else
                parentBandGroupPeriod = parentBand.Groups.Add("ParentBandGroupPeriod", period);
            parentBandGroupPeriod.Header.Appearance.FontData.Italic = DefaultableBoolean.True;
            parentBandGroupPeriod.Header.Appearance.FontData.SizeInPoints = 10;
            parentBandGroupPeriod.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupPeriod.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPeriod.Header.Appearance.BorderColor = Color.Black;

            UltraGridGroup parentBandGroupTitle;
            if (parentBand.Groups.Exists("ParentBandGroupTitle"))
                parentBandGroupTitle = parentBand.Groups["ParentBandGroupTitle"];
            else
                parentBandGroupTitle = parentBand.Groups.Add("ParentBandGroupTitle", "SỔ NHẬT KÝ THU TIỀN");
            parentBandGroupTitle.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroupTitle.Header.Appearance.FontData.SizeInPoints = 12;
            parentBandGroupTitle.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupTitle.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupTitle.Header.Appearance.BorderColor = Color.Black;

            UltraGridGroup parentBandGroupCT;
            if (parentBand.Groups.Exists("ParentBandGroupCT"))
                parentBandGroupCT = parentBand.Groups["ParentBandGroupCT"];
            else
                parentBandGroupCT = parentBand.Groups.Add("ParentBandGroupCT", "");

            UltraGridGroup parentBandGroupCT1;
            if (parentBand.Groups.Exists("ParentBandGroupCT1"))
                parentBandGroupCT1 = parentBand.Groups["ParentBandGroupCT1"];
            else
                parentBandGroupCT1 = parentBand.Groups.Add("ParentBandGroupCT1", "Chứng từ");

            UltraGridGroup parentBandGroupGCCTK;
            if (parentBand.Groups.Exists("ParentBandGroupGCCTK"))
                parentBandGroupGCCTK = parentBand.Groups["ParentBandGroupGCCTK"];
            else
                parentBandGroupGCCTK = parentBand.Groups.Add("ParentBandGroupGCCTK", "Ghi có các tài khoản");

            UltraGridGroup parentBandGroupGCCTK1;
            if (parentBand.Groups.Exists("ParentBandGroupGCCTK1"))
                parentBandGroupGCCTK1 = parentBand.Groups["ParentBandGroupGCCTK1"];
            else
                parentBandGroupGCCTK1 = parentBand.Groups.Add("ParentBandGroupGCCTK1", "Tài khoản");

            UltraGridGroup parentBandGroupTKK;
            if (parentBand.Groups.Exists("ParentBandGroupTKK"))
                parentBandGroupTKK = parentBand.Groups["ParentBandGroupTKK"];
            else
                parentBandGroupTKK = parentBand.Groups.Add("ParentBandGroupTKK", "Tài khoản khác");

            parentBandGroupPeriod.RowLayoutGroupInfo.ParentGroup = parentBandGroupTitle;

            parentBandGroupCT1.RowLayoutGroupInfo.ParentGroup = parentBandGroupCT;
            parentBandGroupCT1.Header.Appearance.BackColorAlpha = Alpha.Default;
            parentBandGroupCT1.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupCT1.Header.Appearance.BorderColor = Color.Black;
            parentBandGroupCT1.Header.Appearance.TextVAlign = VAlign.Top;

            parentBandGroupCT.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            parentBandGroupCT.Header.Appearance.FontData.SizeInPoints = 9;
            parentBandGroupCT.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupCT.Header.Appearance.BackColorAlpha = Alpha.Default;

            parentBandGroupTKK.RowLayoutGroupInfo.ParentGroup = parentBandGroupGCCTK;
            parentBandGroupTKK.Header.Appearance.FontData.SizeInPoints = 9;
            parentBandGroupTKK.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupTKK.Header.Appearance.BackColorAlpha = Alpha.Default;
            parentBandGroupTKK.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupTKK.Header.Appearance.BorderColor = Color.Black;

            parentBandGroupGCCTK1.RowLayoutGroupInfo.ParentGroup = parentBandGroupGCCTK;
            parentBandGroupGCCTK1.Header.Appearance.BackColorAlpha = Alpha.Default;
            parentBandGroupGCCTK1.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupGCCTK1.Header.Appearance.BorderColor = Color.Black;

            parentBandGroupGCCTK.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            parentBandGroupGCCTK.Header.Appearance.FontData.SizeInPoints = 9;
            parentBandGroupGCCTK.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupGCCTK.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupGCCTK.Header.Appearance.BorderColor = Color.Black;
            parentBandGroupGCCTK.Header.Appearance.BackColorAlpha = Alpha.Default;

            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["PostedDate"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["PostedDate"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["PostedDate"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["PostedDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["PostedDate"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["RefNo"].RowLayoutColumnInfo.ParentGroup = parentBandGroupCT1;
            parentBand.Columns["RefNo"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["RefNo"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["RefNo"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["RefNo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["RefNo"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["RefDate"].RowLayoutColumnInfo.ParentGroup = parentBandGroupCT1;
            parentBand.Columns["RefDate"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["RefDate"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["RefDate"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["RefDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["RefDate"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["Description"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["Description"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Description"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["Description"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["Description"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Description"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["Amount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["Amount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Amount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["Amount"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["Amount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Amount"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["Col2"].RowLayoutColumnInfo.ParentGroup = parentBandGroupGCCTK1;
            parentBand.Columns["Col2"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Col2"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["Col2"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["Col2"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Col2"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["Col3"].RowLayoutColumnInfo.ParentGroup = parentBandGroupGCCTK1;
            parentBand.Columns["Col3"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Col3"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["Col3"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["Col3"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Col3"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["Col4"].RowLayoutColumnInfo.ParentGroup = parentBandGroupGCCTK1;
            parentBand.Columns["Col4"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Col4"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["Col4"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["Col4"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Col4"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["Col5"].RowLayoutColumnInfo.ParentGroup = parentBandGroupGCCTK1;
            parentBand.Columns["Col5"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Col5"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["Col5"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["Col5"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Col5"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["Col7"].RowLayoutColumnInfo.ParentGroup = parentBandGroupTKK;
            parentBand.Columns["Col7"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Col7"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["Col7"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["Col7"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Col7"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["ColOtherAccount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupTKK;
            parentBand.Columns["ColOtherAccount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ColOtherAccount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["ColOtherAccount"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["ColOtherAccount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ColOtherAccount"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.OriginX = 0;
            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.SpanX = 100;
            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.SpanY = 32;

            parentBandGroupCT.RowLayoutGroupInfo.OriginX = 100;
            parentBandGroupCT.RowLayoutGroupInfo.OriginY = 0;
            parentBandGroupCT.RowLayoutGroupInfo.SpanX = 200;
            parentBandGroupCT.RowLayoutGroupInfo.SpanY = 32;

            parentBand.Columns["Description"].RowLayoutColumnInfo.OriginX = 300;
            parentBand.Columns["Description"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["Description"].RowLayoutColumnInfo.SpanX = 300;
            parentBand.Columns["Description"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["Amount"].RowLayoutColumnInfo.OriginX = 600;
            parentBand.Columns["Amount"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["Amount"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["Amount"].RowLayoutColumnInfo.SpanY = 32;

            parentBandGroupGCCTK.RowLayoutGroupInfo.OriginX = 720;
            parentBandGroupGCCTK.RowLayoutGroupInfo.OriginY = 0;
            parentBandGroupGCCTK.RowLayoutGroupInfo.SpanX = 1000;
            parentBandGroupGCCTK.RowLayoutGroupInfo.SpanY = 32;

        }

        private void uGrid_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            UltraGridCell cell = e.Row.Cells[0];
            cell.Row.Height = 25;

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("Amount"))
            {
                if (cell.Row.Cells["Amount"].Value != null && (decimal)cell.Row.Cells["Amount"].Value == 0)
                    cell.Row.Cells["Amount"].Value = null;
                if ((cell.Row.Cells["Amount"].Value != null && (decimal)cell.Row.Cells["Amount"].Value < 0))
                    cell.Row.Cells["Amount"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("Col2"))
            {
                if (cell.Row.Cells["Col2"].Value != null && (decimal)cell.Row.Cells["Col2"].Value == 0)
                    cell.Row.Cells["Col2"].Value = null;
                if ((cell.Row.Cells["Col2"].Value != null && (decimal)cell.Row.Cells["Col2"].Value < 0))
                    cell.Row.Cells["Col2"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("Col3"))
            {
                if (cell.Row.Cells["Col3"].Value != null && (decimal)cell.Row.Cells["Col3"].Value == 0)
                    cell.Row.Cells["Col3"].Value = null;
                if ((cell.Row.Cells["Col3"].Value != null && (decimal)cell.Row.Cells["Col3"].Value < 0))
                    cell.Row.Cells["Col3"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("Col4"))
            {
                if (cell.Row.Cells["Col4"].Value != null && (decimal)cell.Row.Cells["Col4"].Value == 0)
                    cell.Row.Cells["Col4"].Value = null;
                if ((cell.Row.Cells["Col4"].Value != null && (decimal)cell.Row.Cells["Col4"].Value < 0))
                    cell.Row.Cells["Col4"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("Col5"))
            {
                if (cell.Row.Cells["Col5"].Value != null && (decimal)cell.Row.Cells["Col5"].Value == 0)
                    cell.Row.Cells["Col5"].Value = null;
                if ((cell.Row.Cells["Col5"].Value != null && (decimal)cell.Row.Cells["Col5"].Value < 0))
                    cell.Row.Cells["Col5"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("Col7"))
            {
                if (cell.Row.Cells["Col7"].Value != null && (decimal)cell.Row.Cells["Col7"].Value == 0)
                    cell.Row.Cells["Col7"].Value = null;
                if ((cell.Row.Cells["Col7"].Value != null && (decimal)cell.Row.Cells["Col7"].Value < 0))
                    cell.Row.Cells["Col7"].Appearance.ForeColor = Color.Red;
            }


        }

        private void ultraGridExcelExporter1_InitializeColumn(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.InitializeColumnEventArgs e)
        {
            e.ExcelFormatStr = e.Column.Format;
        }

        private void ultraGridExcelExporter1_BeginExport(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.BeginExportEventArgs e)
        {
            foreach (var row in uGrid.Rows)
            {
                foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
                {
                    e.Rows[row.Index].Cells[column].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                    e.Rows[row.Index].Cells[column].Appearance.BorderColor = Color.Black;
                    e.Rows[row.Index].Cells[column].Column.Width = 200;
                }
            }
            e.Rows.Band.Summaries[0].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[0].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[1].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[1].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[2].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[2].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[3].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[3].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[4].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[4].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[5].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[5].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[6].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[6].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[7].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[7].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[8].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[8].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[9].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[9].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[10].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[10].Appearance.BorderColor = Color.Black;
        }

        private void FRB01aDNN_SizeChanged(object sender, EventArgs e)
        {
            var state = this.WindowState;
            switch (state)
            {
                case FormWindowState.Maximized:
                    break;
                case FormWindowState.Minimized:
                    break;
                case FormWindowState.Normal:
                    this.Left = (Screen.PrimaryScreen.Bounds.Width - this.Width) / 2;
                    this.Top = (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2;
                    this.Location = new Point((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2, (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
                    break;
                default:
                    break;
            }
        }

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                GLR_GetS03a1_DN temp = (GLR_GetS03a1_DN)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                if (temp != null)
                    editFuntion(temp);

            }
        }

        private void editFuntion(GLR_GetS03a1_DN temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID, temp.RefType);
            //List<GLR_GetS03a1_DN> data1 = sp.Get_SoNhatKyThuTien(FromDate, ToDate, account, currency, bankAccount, check);
            //configGrid(data);


        }

        private void reportManager1_GetReportParameter(object sender, PerpetuumSoft.Reporting.Components.GetReportParameterEventArgs e)
        {
            e.Parameters["CurrencyID"].Value = currency;
        }

        private void ultraGridExcelExporter1_SummaryCellExported(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.SummaryCellExportedEventArgs e)
        {
            Worksheet ws = e.CurrentWorksheet;
            WorksheetRow wsRow = ws.Rows[e.CurrentRowIndex];
            WorksheetCell wsCell = wsRow.Cells[e.CurrentColumnIndex];
            if (!e.Summary.SummaryText.Contains("Số dòng") && !e.Summary.SummaryText.Contains(" "))
            {
                string s = e.Summary.SummaryText.Trim('.');
                if (s.Contains("("))
                    wsCell.Value = -decimal.Parse(s.Trim('(').Trim(')'));
                else
                    wsCell.Value = decimal.Parse(s);

                wsCell.CellFormat.FormatString = "###,###,###,###,##0;(###,###,###,###,##0)";
            }
        }
    }
}
