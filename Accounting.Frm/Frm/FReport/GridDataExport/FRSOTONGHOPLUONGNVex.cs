﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.AppStyling;
using System.IO;
using FX.Core;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinMaskedEdit;
using Infragistics.Win.UltraWinTabs;
using Accounting;
using Accounting.Core.DAO;
using System.Diagnostics;
using Infragistics.Documents.Excel;

namespace Accounting.Frm.FReport
{
    public partial class FRSOTONGHOPLUONGNVex : CustormForm
    {
        private string _subSystemCode;
        List<AccountingObjectSalary> data = new List<AccountingObjectSalary>();
        string period = "";
        DateTime FromDate;
        DateTime ToDate;
        string lstNV;
        ReportProcedureSDS sp = new ReportProcedureSDS();
        public FRSOTONGHOPLUONGNVex(DateTime FromDate1, DateTime ToDate1, string lstNV1)
        {
            InitializeComponent();
            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\SoTongHopLuongNhanVien.rst", path);

            int BeginMonth = FromDate1.Month;
            int BeginYear = FromDate1.Year;
            int EndMonth = ToDate1.Month;
            int EndYear = ToDate1.Year;

            List<AccountingObjectSalary> data1 = sp.GETSALARYNV(BeginMonth, BeginYear, EndMonth, EndYear, lstNV1);
            period = ReportUtils.GetPeriod(FromDate1, ToDate1);

            int stt = 1;
            data1.ForEach(t => 
            {
                t.STT = stt;
                stt++;
            });
            data = data1;
            FromDate = FromDate1;
            ToDate = ToDate1;
            lstNV = lstNV1;

            configGrid(data1);
            if (Utils.isDemo)
                btnExport.Visible = false;
            else
                btnExport.Visible = true;
        }

        private void configGrid(List<AccountingObjectSalary> lst)
        {
            uGrid.DataSource = lst;
            Utils.ConfigGrid(uGrid, ConstDatabase.TONGHOPLUONGNV_TableName);
            uGrid.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;

            GridLayout();

            Utils.AddSumColumn(uGrid, "AccountingObjectCode", false);
            Utils.AddSumColumn(uGrid, "AccountingObjectName", false);
            Utils.AddSumColumn(uGrid, "TaxCode", false);
            Utils.AddSumColumn(uGrid, "IdentificationNo", false);

            uGrid.DisplayLayout.Bands[0].Summaries[1].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[2].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[3].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[4].DisplayFormat = " ";

            uGrid.DisplayLayout.Bands[0].Columns["TotalPersonalTaxIncomeAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["GiamTruGiaCanh"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["BaoHiemDuocTru"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["IncomeForTaxCalcuation"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["IncomeTaxAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["NetAmount"].FormatNumberic(ConstDatabase.Format_TienVND);

            uGrid.DisplayLayout.Bands[0].Columns["STT"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["STT"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["STT"].Width = 100;
            uGrid.DisplayLayout.Bands[0].Columns["STT"].CellAppearance.TextVAlign = VAlign.Middle;
            uGrid.DisplayLayout.Bands[0].Columns["STT"].CellAppearance.TextHAlign = HAlign.Center;

            uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectCode"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectCode"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectCode"].Width = 180;
            uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectCode"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectName"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectName"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectName"].Width = 200;
            uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectName"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["TaxCode"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["TaxCode"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["TaxCode"].Width = 120;
            uGrid.DisplayLayout.Bands[0].Columns["TaxCode"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["IdentificationNo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["IdentificationNo"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["IdentificationNo"].Width = 120;
            uGrid.DisplayLayout.Bands[0].Columns["IdentificationNo"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["TotalPersonalTaxIncomeAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["TotalPersonalTaxIncomeAmount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["TotalPersonalTaxIncomeAmount"].Width = 200;
            uGrid.DisplayLayout.Bands[0].Columns["TotalPersonalTaxIncomeAmount"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["BaoHiemDuocTru"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["BaoHiemDuocTru"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["BaoHiemDuocTru"].Width = 200;
            uGrid.DisplayLayout.Bands[0].Columns["BaoHiemDuocTru"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["GiamTruGiaCanh"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["GiamTruGiaCanh"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["GiamTruGiaCanh"].Width = 200;
            uGrid.DisplayLayout.Bands[0].Columns["GiamTruGiaCanh"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["IncomeForTaxCalcuation"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["IncomeForTaxCalcuation"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["IncomeForTaxCalcuation"].Width = 200;
            uGrid.DisplayLayout.Bands[0].Columns["IncomeForTaxCalcuation"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["IncomeTaxAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["IncomeTaxAmount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["IncomeTaxAmount"].Width = 200;
            uGrid.DisplayLayout.Bands[0].Columns["IncomeTaxAmount"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["NetAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["NetAmount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["NetAmount"].Width = 200;
            uGrid.DisplayLayout.Bands[0].Columns["NetAmount"].CellAppearance.TextVAlign = VAlign.Middle;


            Utils.AddSumColumn1(lst.Where(p => p.TotalPersonalTaxIncomeAmount != null).Sum(n => n.TotalPersonalTaxIncomeAmount), uGrid, "TotalPersonalTaxIncomeAmount", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(p => p.GiamTruGiaCanh != null).Sum(n => n.GiamTruGiaCanh), uGrid, "GiamTruGiaCanh", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(p => p.BaoHiemDuocTru != null).Sum(n => n.BaoHiemDuocTru), uGrid, "BaoHiemDuocTru", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(p => p.IncomeForTaxCalcuation != null).Sum(n => n.IncomeForTaxCalcuation), uGrid, "IncomeForTaxCalcuation", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(p => p.IncomeTaxAmount != null).Sum(n => n.IncomeTaxAmount), uGrid, "IncomeTaxAmount", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(p => p.NetAmount != null).Sum(n => n.NetAmount), uGrid, "NetAmount", false, constDatabaseFormat: 1);

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (data.Count == 0)
                MSG.Warning("Không có dữ liệu");
            else
            {
                var rD = new AccountingObjectSalary_Period();

                rD.Period = ReportUtils.GetPeriod(FromDate, ToDate);
                var f = new ReportForm<AccountingObjectSalary>(fileReportSlot1, _subSystemCode);
                f.AddDatasource("DoanhThuNV", data, true);
                f.AddDatasource("Detail", rD);
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }
        private Decimal Covert_Money(decimal gt)
        {
            Decimal ketqua = 0;
            string s = "n";
            if (gt < 0)
            {
                string gt_am = String.Format("{0:" + Utils.GetTypeFormatNumberic(ConstDatabase.Format_TienVND) + ";" + Utils.GetFormatNegative().Replace("n", Utils.GetTypeFormatNumberic(ConstDatabase.Format_TienVND)) + "}", Convert.ToDecimal(gt));
                gt_am = gt_am.Replace('(', '-').Replace(")", string.Empty);
                ketqua = Convert.ToDecimal(gt_am);
            }
            else
            {
                string kq = String.Format("{0:" + Utils.GetTypeFormatNumberic(ConstDatabase.Format_TienVND) + ";" + s.Replace("n", Utils.GetTypeFormatNumberic(ConstDatabase.Format_TienVND)) + "}", Convert.ToDecimal(gt));
                ketqua = Convert.ToDecimal(kq);
            }

            return ketqua;
        }

        private void btnExport_Click(object sender, EventArgs e)
        {

            SaveFileDialog save = new SaveFileDialog
            {
                Title = "Chọn nơi sao lưu ",
                Filter = "File (*.xls)|*.xls",
                InitialDirectory = "@C:\\",
                FileName = "sotonghopluongnv.xls"
            };
            if (save.ShowDialog() == DialogResult.OK)
            {
                string duongdan = save.FileName;
                try
                {
                    ultraGridExcelExporter1.Export(uGrid, duongdan);
                    if (MSG.Question("Kết xuất thành công, Bạn có muốn mở file vừa kết xuất không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(save.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
                catch
                {
                    MSG.Warning("Lỗi khi kết xuất !");
                }
            }
        }
        private void GridLayout()
        {
            UltraGridBand parentBand = this.uGrid.DisplayLayout.Bands[0];
            parentBand.RowLayoutStyle = RowLayoutStyle.GroupLayout;

            UltraGridGroup parentBandGroupDK;
            if (parentBand.Groups.Exists("ParentBandGroupDK"))
                parentBandGroupDK = parentBand.Groups["ParentBandGroupDK"];
            else
                parentBandGroupDK = parentBand.Groups.Add("ParentBandGroupDK", "Các khoản giảm trừ");
            parentBand.Columns["BaoHiemDuocTru"].RowLayoutColumnInfo.ParentGroup = parentBandGroupDK;
            parentBand.Columns["BaoHiemDuocTru"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["BaoHiemDuocTru"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["BaoHiemDuocTru"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["BaoHiemDuocTru"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["BaoHiemDuocTru"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["GiamTruGiaCanh"].RowLayoutColumnInfo.ParentGroup = parentBandGroupDK;
            parentBand.Columns["GiamTruGiaCanh"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GiamTruGiaCanh"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["GiamTruGiaCanh"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["GiamTruGiaCanh"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["GiamTruGiaCanh"].Header.Appearance.BorderColor = Color.Black;

            UltraGridGroup parentBandGroupPeriod;
            if (parentBand.Groups.Exists("ParentBandGroupPeriod"))
            {
                parentBandGroupPeriod = parentBand.Groups["ParentBandGroupPeriod"];
                parentBandGroupPeriod.Header.Caption = period;
            }
            else
                parentBandGroupPeriod = parentBand.Groups.Add("ParentBandGroupPeriod", period);
            parentBandGroupPeriod.Header.Appearance.FontData.Italic = DefaultableBoolean.True;
            parentBandGroupPeriod.Header.Appearance.FontData.SizeInPoints = 10;
            parentBandGroupPeriod.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupPeriod.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPeriod.Header.Appearance.BorderColor = Color.Black;

            UltraGridGroup parentBandGroupTitle;
            if (parentBand.Groups.Exists("ParentBandGroupTitle"))
                parentBandGroupTitle = parentBand.Groups["ParentBandGroupTitle"];
            else
                parentBandGroupTitle = parentBand.Groups.Add("ParentBandGroupTitle", "SỔ TỔNG HỢP LƯƠNG NHÂN VIÊN");
            parentBandGroupTitle.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroupTitle.Header.Appearance.FontData.SizeInPoints = 12;
            parentBandGroupTitle.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupTitle.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupTitle.Header.Appearance.BorderColor = Color.Black;


            parentBandGroupDK.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            parentBandGroupDK.Header.Appearance.FontData.SizeInPoints = 9;
            parentBandGroupDK.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupDK.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupDK.Header.Appearance.BorderColor = Color.Black;
            parentBandGroupDK.Header.Appearance.BackColorAlpha = Alpha.Default;

            parentBandGroupPeriod.RowLayoutGroupInfo.ParentGroup = parentBandGroupTitle;

            parentBand.Columns["STT"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["STT"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["STT"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["STT"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["STT"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["STT"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["AccountingObjectCode"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["AccountingObjectCode"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["AccountingObjectCode"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["AccountingObjectCode"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["AccountingObjectCode"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["AccountingObjectCode"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["AccountingObjectName"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["AccountingObjectName"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["AccountingObjectName"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["AccountingObjectName"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["AccountingObjectName"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["AccountingObjectName"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["TaxCode"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["TaxCode"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["TaxCode"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["TaxCode"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["TaxCode"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["TaxCode"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["IdentificationNo"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["IdentificationNo"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["IdentificationNo"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["IdentificationNo"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["IdentificationNo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["IdentificationNo"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["TotalPersonalTaxIncomeAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["TotalPersonalTaxIncomeAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["TotalPersonalTaxIncomeAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["TotalPersonalTaxIncomeAmount"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["TotalPersonalTaxIncomeAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["TotalPersonalTaxIncomeAmount"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["IncomeForTaxCalcuation"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["IncomeForTaxCalcuation"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["IncomeForTaxCalcuation"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["IncomeForTaxCalcuation"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["IncomeForTaxCalcuation"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["IncomeForTaxCalcuation"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["IncomeTaxAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["IncomeTaxAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["IncomeTaxAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["IncomeTaxAmount"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["IncomeTaxAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["IncomeTaxAmount"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["NetAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["NetAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["NetAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["NetAmount"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["NetAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["NetAmount"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["STT"].RowLayoutColumnInfo.OriginX = 0;
            parentBand.Columns["STT"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["STT"].RowLayoutColumnInfo.SpanX = 100;
            parentBand.Columns["STT"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["AccountingObjectCode"].RowLayoutColumnInfo.OriginX = 100;
            parentBand.Columns["AccountingObjectCode"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["AccountingObjectCode"].RowLayoutColumnInfo.SpanX = 180;
            parentBand.Columns["AccountingObjectCode"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["AccountingObjectName"].RowLayoutColumnInfo.OriginX = 280;
            parentBand.Columns["AccountingObjectName"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["AccountingObjectName"].RowLayoutColumnInfo.SpanX = 200;
            parentBand.Columns["AccountingObjectName"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["TaxCode"].RowLayoutColumnInfo.OriginX = 480;
            parentBand.Columns["TaxCode"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["TaxCode"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["TaxCode"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["IdentificationNo"].RowLayoutColumnInfo.OriginX = 600;
            parentBand.Columns["IdentificationNo"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["IdentificationNo"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["IdentificationNo"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["TotalPersonalTaxIncomeAmount"].RowLayoutColumnInfo.OriginX = 720;
            parentBand.Columns["TotalPersonalTaxIncomeAmount"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["TotalPersonalTaxIncomeAmount"].RowLayoutColumnInfo.SpanX = 200;
            parentBand.Columns["TotalPersonalTaxIncomeAmount"].RowLayoutColumnInfo.SpanY = 32;

            parentBandGroupDK.RowLayoutGroupInfo.OriginX = 920;
            parentBandGroupDK.RowLayoutGroupInfo.OriginY = 0;
            parentBandGroupDK.RowLayoutGroupInfo.SpanX = 400;
            parentBandGroupDK.RowLayoutGroupInfo.SpanY = 32;

            parentBand.Columns["IncomeForTaxCalcuation"].RowLayoutColumnInfo.OriginX = 1320;
            parentBand.Columns["IncomeForTaxCalcuation"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["IncomeForTaxCalcuation"].RowLayoutColumnInfo.SpanX = 200;
            parentBand.Columns["IncomeForTaxCalcuation"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["IncomeTaxAmount"].RowLayoutColumnInfo.OriginX = 1520;
            parentBand.Columns["IncomeTaxAmount"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["IncomeTaxAmount"].RowLayoutColumnInfo.SpanX = 200;
            parentBand.Columns["IncomeTaxAmount"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["NetAmount"].RowLayoutColumnInfo.OriginX = 1720;
            parentBand.Columns["NetAmount"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["NetAmount"].RowLayoutColumnInfo.SpanX = 200;
            parentBand.Columns["NetAmount"].RowLayoutColumnInfo.SpanY = 32;

        }

        private void ultraGridExcelExporter1_InitializeColumn(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.InitializeColumnEventArgs e)
        {
            e.ExcelFormatStr = e.Column.Format;
        }

        private void ultraGridExcelExporter1_BeginExport(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.BeginExportEventArgs e)
        {
            foreach (var row in uGrid.Rows)
            {
                foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
                {
                    e.Rows[row.Index].Cells[column].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                    e.Rows[row.Index].Cells[column].Appearance.BorderColor = Color.Black;
                    e.Rows[row.Index].Cells[column].Column.Width = 200;
                }
            }
            e.Rows.Band.Summaries[0].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[0].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[1].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[1].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[2].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[2].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[3].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[3].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[4].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[4].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[5].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[5].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[6].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[6].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[7].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[7].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[8].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[8].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[9].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[9].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[10].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[10].Appearance.BorderColor = Color.Black;
        }

        private void uGrid_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            UltraGridCell cell = e.Row.Cells[0];
            cell.Row.Height = 25;

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("TotalPersonalTaxIncomeAmount"))
            {
                if (cell.Row.Cells["TotalPersonalTaxIncomeAmount"].Value != null && (decimal)cell.Row.Cells["TotalPersonalTaxIncomeAmount"].Value == 0)
                    cell.Row.Cells["TotalPersonalTaxIncomeAmount"].Value = null;
                if ((cell.Row.Cells["TotalPersonalTaxIncomeAmount"].Value != null && (decimal)cell.Row.Cells["TotalPersonalTaxIncomeAmount"].Value < 0))
                    cell.Row.Cells["TotalPersonalTaxIncomeAmount"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("BaoHiemDuocTru"))
            {
                if (cell.Row.Cells["BaoHiemDuocTru"].Value != null && (decimal)cell.Row.Cells["BaoHiemDuocTru"].Value == 0)
                    cell.Row.Cells["BaoHiemDuocTru"].Value = null;
                if ((cell.Row.Cells["BaoHiemDuocTru"].Value != null && (decimal)cell.Row.Cells["BaoHiemDuocTru"].Value < 0))
                    cell.Row.Cells["BaoHiemDuocTru"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("GiamTruGiaCanh"))
            {
                if (cell.Row.Cells["GiamTruGiaCanh"].Value != null && (decimal)cell.Row.Cells["GiamTruGiaCanh"].Value == 0)
                    cell.Row.Cells["GiamTruGiaCanh"].Value = null;
                if ((cell.Row.Cells["GiamTruGiaCanh"].Value != null && (decimal)cell.Row.Cells["GiamTruGiaCanh"].Value < 0))
                    cell.Row.Cells["GiamTruGiaCanh"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("IncomeForTaxCalcuation"))
            {
                if (cell.Row.Cells["IncomeForTaxCalcuation"].Value != null && (decimal)cell.Row.Cells["IncomeForTaxCalcuation"].Value == 0)
                    cell.Row.Cells["IncomeForTaxCalcuation"].Value = null;
                if ((cell.Row.Cells["IncomeForTaxCalcuation"].Value != null && (decimal)cell.Row.Cells["IncomeForTaxCalcuation"].Value < 0))
                    cell.Row.Cells["IncomeForTaxCalcuation"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("IncomeTaxAmount"))
            {
                if (cell.Row.Cells["IncomeTaxAmount"].Value != null && (decimal)cell.Row.Cells["IncomeTaxAmount"].Value == 0)
                    cell.Row.Cells["IncomeTaxAmount"].Value = null;
                if ((cell.Row.Cells["IncomeTaxAmount"].Value != null && (decimal)cell.Row.Cells["IncomeTaxAmount"].Value < 0))
                    cell.Row.Cells["IncomeTaxAmount"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("NetAmount"))
            {
                if (cell.Row.Cells["NetAmount"].Value != null && (decimal)cell.Row.Cells["NetAmount"].Value == 0)
                    cell.Row.Cells["NetAmount"].Value = null;
                if ((cell.Row.Cells["NetAmount"].Value != null && (decimal)cell.Row.Cells["NetAmount"].Value < 0))
                    cell.Row.Cells["NetAmount"].Appearance.ForeColor = Color.Red;
            }

        }

        private void FRBANGCANDOITAIKHOAN_SizeChanged(object sender, EventArgs e)
        {
            var state = this.WindowState;
            switch (state)
            {
                case FormWindowState.Maximized:
                    break;
                case FormWindowState.Minimized:
                    break;
                case FormWindowState.Normal:
                    this.Left = (Screen.PrimaryScreen.Bounds.Width - this.Width) / 2;
                    this.Top = (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2;
                    this.Location = new Point((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2, (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
                    break;
                default:
                    break;
            }
        }

        private void ultraGridExcelExporter1_SummaryCellExported(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.SummaryCellExportedEventArgs e)
        {
            Worksheet ws = e.CurrentWorksheet;
            WorksheetRow wsRow = ws.Rows[e.CurrentRowIndex];
            WorksheetCell wsCell = wsRow.Cells[e.CurrentColumnIndex];
            if (!e.Summary.SummaryText.Contains("Số dòng") && !e.Summary.SummaryText.Contains(" "))
            {
                string s = e.Summary.SummaryText.Trim('.');
                if (s.Contains("("))
                    wsCell.Value = -decimal.Parse(s.Trim('(').Trim(')'));
                else
                    wsCell.Value = decimal.Parse(s);

                wsCell.CellFormat.FormatString = "###,###,###,###,##0;(###,###,###,###,##0)";
            }
        }
    }

}
