﻿using Accounting.Core.DAO;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Documents.Excel;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRSoNhatKyBanHangex : Form
    {
        private string _subSystemCode;
        ReportProcedureSDS sp = new ReportProcedureSDS();
        List<SalesDiaryBook> data = new List<SalesDiaryBook>();
        private string period = "";
        DateTime FromDate;
        DateTime ToDate;
        int check;
        public FRSoNhatKyBanHangex(DateTime FromDate1, DateTime ToDate1, int check1)
        {
            InitializeComponent();
            ReportUtils.ProcessControls(this);
            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\SoNhatKyBanHang.rst", path);
            List<SalesDiaryBook> data1 = sp.GetNhatKyBanHang(FromDate1, ToDate1, check1);
            period = ReportUtils.GetPeriod(FromDate1, ToDate1);
            data = data1;
            FromDate = FromDate1;
            ToDate = ToDate1;
            check = check1;
            configGrid(data);
            if (Utils.isDemo)
                btnExport.Visible = false;
            else
                btnExport.Visible = true;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            
            if (data.Count == 0)
            {
                MSG.Warning("Không có dữ liệu để lên báo cáo");
            }
            else
            {
                var rD = new SalesDiaryBook_Detail();
                rD.Period = ReportUtils.GetPeriod(FromDate, ToDate);
                var f = new ReportForm<SalesDiaryBook>(fileReportSlot1, _subSystemCode);
                f.AddDatasource("DBSoNhatKyBH", data, true);
                f.AddDatasource("Detail", rD);
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void configGrid(List<SalesDiaryBook> lst)
        {
            uGrid.DataSource = lst;
            Utils.ConfigGrid(uGrid, ConstDatabase.SalesDiaryBook_TableName);
            uGrid.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            uGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;

            uGrid.DisplayLayout.Bands[0].Columns["SumTurnOver"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["TurnOverAmountInv"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["TurnOverAmountFinishedInv"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["TurnOverAmountServiceInv"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["TurnOverAmountOther"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["DiscountAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["ReturnAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["ReduceAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["TurnOverPure"].FormatNumberic(ConstDatabase.Format_TienVND);

            Utils.AddSumColumn(uGrid, "Date", false);
            Utils.AddSumColumn(uGrid, "No", false);
            Utils.AddSumColumn(uGrid, "InvoiceDate", false);
            Utils.AddSumColumn(uGrid, "InvoiceNo", false);
            Utils.AddSumColumn(uGrid, "Description", false);
            Utils.AddSumColumn(uGrid, "CustomerName", false);

            uGrid.DisplayLayout.Bands[0].Summaries[1].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[2].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[3].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[4].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[5].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[6].DisplayFormat = " ";

            Utils.AddSumColumn1(lst.Where(n => n.SumTurnOver != null).Sum(n => n.SumTurnOver), uGrid, "SumTurnOver", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.TurnOverAmountInv != null).Sum(n => n.TurnOverAmountInv), uGrid, "TurnOverAmountInv", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.TurnOverAmountFinishedInv != null).Sum(n => n.TurnOverAmountFinishedInv), uGrid, "TurnOverAmountFinishedInv", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.TurnOverAmountServiceInv != null).Sum(n => n.TurnOverAmountServiceInv), uGrid, "TurnOverAmountServiceInv", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.TurnOverAmountOther != null).Sum(n => n.TurnOverAmountOther), uGrid, "TurnOverAmountOther", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.DiscountAmount != null).Sum(n => n.DiscountAmount), uGrid, "DiscountAmount", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.ReturnAmount != null).Sum(n => n.ReturnAmount), uGrid, "ReturnAmount", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.ReduceAmount != null).Sum(n => n.ReduceAmount), uGrid, "ReduceAmount", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.TurnOverPure != null).Sum(n => n.TurnOverPure), uGrid, "TurnOverPure", false, constDatabaseFormat: 1);

            uGrid.DisplayLayout.Bands[0].Columns["PostedDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["PostedDate"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["PostedDate"].Width = 120;

            uGrid.DisplayLayout.Bands[0].Columns["Date"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["Date"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["Date"].Width = 120;

            uGrid.DisplayLayout.Bands[0].Columns["No"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["No"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["No"].Width = 150;
            uGrid.DisplayLayout.Bands[0].Columns["No"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["InvoiceDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["InvoiceDate"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["InvoiceDate"].Width = 120;

            uGrid.DisplayLayout.Bands[0].Columns["InvoiceNo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["InvoiceNo"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["InvoiceNo"].Width = 150;
            uGrid.DisplayLayout.Bands[0].Columns["InvoiceNo"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["Description"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["Description"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["Description"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["Description"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["SumTurnOver"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["SumTurnOver"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["SumTurnOver"].Width = 170;
            uGrid.DisplayLayout.Bands[0].Columns["SumTurnOver"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["TurnOverAmountInv"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["TurnOverAmountInv"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["TurnOverAmountInv"].Width = 170;
            uGrid.DisplayLayout.Bands[0].Columns["TurnOverAmountInv"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["TurnOverAmountFinishedInv"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["TurnOverAmountFinishedInv"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["TurnOverAmountFinishedInv"].Width = 170;
            uGrid.DisplayLayout.Bands[0].Columns["TurnOverAmountFinishedInv"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["TurnOverAmountServiceInv"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["TurnOverAmountServiceInv"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["TurnOverAmountServiceInv"].Width = 170;
            uGrid.DisplayLayout.Bands[0].Columns["TurnOverAmountServiceInv"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["TurnOverAmountOther"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["TurnOverAmountOther"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["TurnOverAmountOther"].Width = 170;
            uGrid.DisplayLayout.Bands[0].Columns["TurnOverAmountOther"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["DiscountAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["DiscountAmount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["DiscountAmount"].Width = 170;
            uGrid.DisplayLayout.Bands[0].Columns["DiscountAmount"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["ReturnAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["ReturnAmount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["ReturnAmount"].Width = 170;
            uGrid.DisplayLayout.Bands[0].Columns["ReturnAmount"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["ReduceAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["ReduceAmount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["ReduceAmount"].Width = 170;
            uGrid.DisplayLayout.Bands[0].Columns["ReduceAmount"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["TurnOverPure"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["TurnOverPure"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["TurnOverPure"].Width = 170;
            uGrid.DisplayLayout.Bands[0].Columns["TurnOverPure"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["CustomerName"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["CustomerName"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["CustomerName"].Width = 200;
            uGrid.DisplayLayout.Bands[0].Columns["CustomerName"].CellAppearance.TextVAlign = VAlign.Middle;

            GridLayout();

        }

        private void GridLayout()
        {
            UltraGridBand parentBand = this.uGrid.DisplayLayout.Bands[0];
            parentBand.RowLayoutStyle = RowLayoutStyle.GroupLayout;

            UltraGridGroup parentBandGroupPeriod;
            if (parentBand.Groups.Exists("ParentBandGroupPeriod"))
            {
                parentBandGroupPeriod = parentBand.Groups["ParentBandGroupPeriod"];
                parentBandGroupPeriod.Header.Caption = period;
            }
            else
                parentBandGroupPeriod = parentBand.Groups.Add("ParentBandGroupPeriod", period);
            parentBandGroupPeriod.Header.Appearance.FontData.Italic = DefaultableBoolean.True;
            parentBandGroupPeriod.Header.Appearance.FontData.SizeInPoints = 10;
            parentBandGroupPeriod.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupPeriod.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPeriod.Header.Appearance.BorderColor = Color.Black;

            UltraGridGroup parentBandGroupTitle;
            if (parentBand.Groups.Exists("ParentBandGroupTitle"))
                parentBandGroupTitle = parentBand.Groups["ParentBandGroupTitle"];
            else
                parentBandGroupTitle = parentBand.Groups.Add("ParentBandGroupTitle", "SỔ NHẬT KÝ BÁN HÀNG");
            parentBandGroupTitle.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroupTitle.Header.Appearance.FontData.SizeInPoints = 12;
            parentBandGroupTitle.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupTitle.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupTitle.Header.Appearance.BorderColor = Color.Black;

            parentBandGroupPeriod.RowLayoutGroupInfo.ParentGroup = parentBandGroupTitle;

            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["PostedDate"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["PostedDate"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["PostedDate"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["PostedDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["PostedDate"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["Date"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["Date"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Date"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["Date"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["Date"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Date"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["No"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["No"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["No"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["No"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["No"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["No"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["InvoiceDate"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["InvoiceDate"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["InvoiceDate"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["InvoiceDate"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["InvoiceDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["InvoiceDate"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["InvoiceNo"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["InvoiceNo"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["InvoiceNo"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["InvoiceNo"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["InvoiceNo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["InvoiceNo"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["Description"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["Description"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Description"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["Description"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["Description"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Description"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["SumTurnOver"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["SumTurnOver"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SumTurnOver"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["SumTurnOver"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["SumTurnOver"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SumTurnOver"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["TurnOverAmountInv"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["TurnOverAmountInv"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["TurnOverAmountInv"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["TurnOverAmountInv"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["TurnOverAmountInv"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["TurnOverAmountInv"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["TurnOverAmountFinishedInv"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["TurnOverAmountFinishedInv"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["TurnOverAmountFinishedInv"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["TurnOverAmountFinishedInv"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["TurnOverAmountFinishedInv"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["TurnOverAmountFinishedInv"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["TurnOverAmountServiceInv"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["TurnOverAmountServiceInv"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["TurnOverAmountServiceInv"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["TurnOverAmountServiceInv"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["TurnOverAmountServiceInv"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["TurnOverAmountServiceInv"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["TurnOverAmountOther"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["TurnOverAmountOther"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["TurnOverAmountOther"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["TurnOverAmountOther"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["TurnOverAmountOther"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["TurnOverAmountOther"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["DiscountAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["DiscountAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["DiscountAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["DiscountAmount"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["DiscountAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["DiscountAmount"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["ReturnAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["ReturnAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ReturnAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["ReturnAmount"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["ReturnAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ReturnAmount"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["ReduceAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["ReduceAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ReduceAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["ReduceAmount"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["ReduceAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ReduceAmount"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["TurnOverPure"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["TurnOverPure"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["TurnOverPure"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["TurnOverPure"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["TurnOverPure"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["TurnOverPure"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["CustomerName"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["CustomerName"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["CustomerName"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["CustomerName"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["CustomerName"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["CustomerName"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.OriginX = 0;
            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["Date"].RowLayoutColumnInfo.OriginX = 120;
            parentBand.Columns["Date"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["Date"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["Date"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["No"].RowLayoutColumnInfo.OriginX = 240;
            parentBand.Columns["No"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["No"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["No"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["InvoiceDate"].RowLayoutColumnInfo.OriginX = 360;
            parentBand.Columns["InvoiceDate"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["InvoiceDate"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["InvoiceDate"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["InvoiceNo"].RowLayoutColumnInfo.OriginX = 480;
            parentBand.Columns["InvoiceNo"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["InvoiceNo"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["InvoiceNo"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["Description"].RowLayoutColumnInfo.OriginX = 600;
            parentBand.Columns["Description"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["Description"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["Description"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["SumTurnOver"].RowLayoutColumnInfo.OriginX = 850;
            parentBand.Columns["SumTurnOver"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["SumTurnOver"].RowLayoutColumnInfo.SpanX = 170;
            parentBand.Columns["SumTurnOver"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["TurnOverAmountInv"].RowLayoutColumnInfo.OriginX = 1020;
            parentBand.Columns["TurnOverAmountInv"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["TurnOverAmountInv"].RowLayoutColumnInfo.SpanX = 170;
            parentBand.Columns["TurnOverAmountInv"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["TurnOverAmountFinishedInv"].RowLayoutColumnInfo.OriginX = 1190;
            parentBand.Columns["TurnOverAmountFinishedInv"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["TurnOverAmountFinishedInv"].RowLayoutColumnInfo.SpanX = 170;
            parentBand.Columns["TurnOverAmountFinishedInv"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["TurnOverAmountServiceInv"].RowLayoutColumnInfo.OriginX = 1360;
            parentBand.Columns["TurnOverAmountServiceInv"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["TurnOverAmountServiceInv"].RowLayoutColumnInfo.SpanX = 170;
            parentBand.Columns["TurnOverAmountServiceInv"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["TurnOverAmountOther"].RowLayoutColumnInfo.OriginX = 1530;
            parentBand.Columns["TurnOverAmountOther"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["TurnOverAmountOther"].RowLayoutColumnInfo.SpanX = 170;
            parentBand.Columns["TurnOverAmountOther"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["DiscountAmount"].RowLayoutColumnInfo.OriginX = 1700;
            parentBand.Columns["DiscountAmount"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["DiscountAmount"].RowLayoutColumnInfo.SpanX = 170;
            parentBand.Columns["DiscountAmount"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["ReturnAmount"].RowLayoutColumnInfo.OriginX = 1870;
            parentBand.Columns["ReturnAmount"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["ReturnAmount"].RowLayoutColumnInfo.SpanX = 170;
            parentBand.Columns["ReturnAmount"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["ReduceAmount"].RowLayoutColumnInfo.OriginX = 2040;
            parentBand.Columns["ReduceAmount"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["ReduceAmount"].RowLayoutColumnInfo.SpanX = 170;
            parentBand.Columns["ReduceAmount"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["TurnOverPure"].RowLayoutColumnInfo.OriginX = 2210;
            parentBand.Columns["TurnOverPure"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["TurnOverPure"].RowLayoutColumnInfo.SpanX = 170;
            parentBand.Columns["TurnOverPure"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["CustomerName"].RowLayoutColumnInfo.OriginX = 2380;
            parentBand.Columns["CustomerName"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["CustomerName"].RowLayoutColumnInfo.SpanX = 200;
            parentBand.Columns["CustomerName"].RowLayoutColumnInfo.SpanY = 32;
           
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog
            {
                Title = "Chọn nơi sao lưu ",
                Filter = "File (*.xls)|*.xls",
                InitialDirectory = "@C:\\",
                FileName = "sonhatkybanhang.xls"
            };
            if (save.ShowDialog() == DialogResult.OK)
            {
                string duongdan = save.FileName;
                try
                {
                    ultraGridExcelExporter1.Export(uGrid, duongdan);
                    if (MSG.Question("Kết xuất thành công, Bạn có muốn mở file vừa kết xuất không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(save.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
                catch
                {
                    MSG.Warning("Lỗi khi kết xuất !");
                }
            }
        }

        private void uGrid_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            UltraGridCell cell = e.Row.Cells[0];
            cell.Row.Height = 25;

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("TurnOverAmountInv"))
            {
                if (cell.Row.Cells["TurnOverAmountInv"].Value != null && (decimal)cell.Row.Cells["TurnOverAmountInv"].Value == 0)
                    cell.Row.Cells["TurnOverAmountInv"].Value = null;
                if ((cell.Row.Cells["TurnOverAmountInv"].Value != null && (decimal)cell.Row.Cells["TurnOverAmountInv"].Value < 0))
                    cell.Row.Cells["TurnOverAmountInv"].Appearance.ForeColor = Color.Red;
            }
         
            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("TurnOverAmountFinishedInv"))
            {
                if (cell.Row.Cells["TurnOverAmountFinishedInv"].Value != null && (decimal)cell.Row.Cells["TurnOverAmountFinishedInv"].Value == 0)
                    cell.Row.Cells["TurnOverAmountFinishedInv"].Value = null;
                if ((cell.Row.Cells["TurnOverAmountFinishedInv"].Value != null && (decimal)cell.Row.Cells["TurnOverAmountFinishedInv"].Value < 0))
                    cell.Row.Cells["TurnOverAmountFinishedInv"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("TurnOverAmountServiceInv"))
            {
                if (cell.Row.Cells["TurnOverAmountServiceInv"].Value != null && (decimal)cell.Row.Cells["TurnOverAmountServiceInv"].Value == 0)
                    cell.Row.Cells["TurnOverAmountServiceInv"].Value = null;
                if ((cell.Row.Cells["TurnOverAmountServiceInv"].Value != null && (decimal)cell.Row.Cells["TurnOverAmountServiceInv"].Value < 0))
                    cell.Row.Cells["TurnOverAmountServiceInv"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("TurnOverAmountOther"))
            {
                if (cell.Row.Cells["TurnOverAmountOther"].Value != null && (decimal)cell.Row.Cells["TurnOverAmountOther"].Value == 0)
                    cell.Row.Cells["TurnOverAmountOther"].Value = null;
                if ((cell.Row.Cells["TurnOverAmountOther"].Value != null && (decimal)cell.Row.Cells["TurnOverAmountOther"].Value < 0))
                    cell.Row.Cells["TurnOverAmountOther"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("DiscountAmount"))
            {
                if (cell.Row.Cells["DiscountAmount"].Value != null && (decimal)cell.Row.Cells["DiscountAmount"].Value == 0)
                    cell.Row.Cells["DiscountAmount"].Value = null;
                if ((cell.Row.Cells["DiscountAmount"].Value != null && (decimal)cell.Row.Cells["DiscountAmount"].Value < 0))
                    cell.Row.Cells["DiscountAmount"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("ReturnAmount"))
            {
                if (cell.Row.Cells["ReturnAmount"].Value != null && (decimal)cell.Row.Cells["ReturnAmount"].Value == 0)
                    cell.Row.Cells["ReturnAmount"].Value = null;
                if ((cell.Row.Cells["ReturnAmount"].Value != null && (decimal)cell.Row.Cells["ReturnAmount"].Value < 0))
                    cell.Row.Cells["ReturnAmount"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("ReduceAmount"))
            {
                if (cell.Row.Cells["ReduceAmount"].Value != null && (decimal)cell.Row.Cells["ReduceAmount"].Value == 0)
                    cell.Row.Cells["ReduceAmount"].Value = null;
                if ((cell.Row.Cells["ReduceAmount"].Value != null && (decimal)cell.Row.Cells["ReduceAmount"].Value < 0))
                    cell.Row.Cells["ReduceAmount"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("SumTurnOver"))
            {
                if (cell.Row.Cells["SumTurnOver"].Value != null && (decimal)cell.Row.Cells["SumTurnOver"].Value == 0)
                    cell.Row.Cells["SumTurnOver"].Value = null;
                if ((cell.Row.Cells["SumTurnOver"].Value != null && (decimal)cell.Row.Cells["SumTurnOver"].Value < 0))
                    cell.Row.Cells["SumTurnOver"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("TurnOverPure"))
            {
                if (cell.Row.Cells["TurnOverPure"].Value != null && (decimal)cell.Row.Cells["TurnOverPure"].Value == 0)
                    cell.Row.Cells["TurnOverPure"].Value = null;
                if ((cell.Row.Cells["TurnOverPure"].Value != null && (decimal)cell.Row.Cells["TurnOverPure"].Value < 0))
                    cell.Row.Cells["TurnOverPure"].Appearance.ForeColor = Color.Red;
            }

        }

        private void ultraGridExcelExporter1_InitializeColumn(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.InitializeColumnEventArgs e)
        {
            e.ExcelFormatStr = e.Column.Format;
        }

        private void ultraGridExcelExporter1_BeginExport(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.BeginExportEventArgs e)
        {
            foreach (var row in uGrid.Rows)
            {
                foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
                {
                    e.Rows[row.Index].Cells[column].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                    e.Rows[row.Index].Cells[column].Appearance.BorderColor = Color.Black;
                    e.Rows[row.Index].Cells[column].Column.Width = 200;
                }
            }
            e.Rows.Band.Summaries[0].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[0].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[1].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[1].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[2].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[2].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[3].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[3].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[4].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[4].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[5].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[5].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[6].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[6].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[7].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[7].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[8].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[8].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[9].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[9].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[10].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[10].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[11].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[11].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[12].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[12].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[13].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[13].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[14].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[14].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[15].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[15].Appearance.BorderColor = Color.Black;
        }

        private void FRB01aDNN_SizeChanged(object sender, EventArgs e)
        {
            var state = this.WindowState;
            switch (state)
            {
                case FormWindowState.Maximized:
                    break;
                case FormWindowState.Minimized:
                    break;
                case FormWindowState.Normal:
                    this.Left = (Screen.PrimaryScreen.Bounds.Width - this.Width) / 2;
                    this.Top = (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2;
                    this.Location = new Point((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2, (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
                    break;
                default:
                    break;
            }
        }

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                SalesDiaryBook temp = (SalesDiaryBook)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                if (temp != null)
                    editFuntion(temp);

            }
        }

        private void editFuntion(SalesDiaryBook temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID, temp.TypeID);
            //List<SalesDiaryBook> data1 = sp.GetNhatKyBanHang(FromDate, ToDate, check);
            //configGrid(data1);
        }

        private void ultraGridExcelExporter1_SummaryCellExported(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.SummaryCellExportedEventArgs e)
        {
            Worksheet ws = e.CurrentWorksheet;
            WorksheetRow wsRow = ws.Rows[e.CurrentRowIndex];
            WorksheetCell wsCell = wsRow.Cells[e.CurrentColumnIndex];
            if (!e.Summary.SummaryText.Contains("Số dòng") && !e.Summary.SummaryText.Contains(" "))
            {
                string s = e.Summary.SummaryText.Trim('.');
                if (s.Contains("("))
                    wsCell.Value = -decimal.Parse(s.Trim('(').Trim(')'));
                else
                    wsCell.Value = decimal.Parse(s);

                wsCell.CellFormat.FormatString = "###,###,###,###,##0;(###,###,###,###,##0)";
            }
        }
    }
}
