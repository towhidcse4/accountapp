﻿using Accounting.Core.DAO;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.Report;
using Accounting.TextMessage;
using Infragistics.Documents.Excel;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRTINHHINHTHUCHIENHDMex : Form
    {
        private string _subSystemCode;
        ReportProcedureSDS sp = new ReportProcedureSDS();
        List<RContract> data = new List<RContract>();
        string period = "";
        DateTime FromDate;
        DateTime ToDate;
        List<Guid?> lstIDContractBuy;
        public FRTINHHINHTHUCHIENHDMex(DateTime FromDate1, DateTime ToDate1, List<Guid?> lstIDContractBuy1)
        {
            InitializeComponent();
            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\HopDongMua.rst", path);
            ReportUtils.ProcessControls(this);

            List<RContract> data1 = sp.GetContract(FromDate1, ToDate1, false);
            period = ReportUtils.GetPeriod(FromDate1, ToDate1);

            FromDate = FromDate1;
            ToDate = ToDate1;
            data = data1;
            lstIDContractBuy = lstIDContractBuy1;

            configGrid(data1);
            if (Utils.isDemo)
                btnExport.Visible = false;
            else
                btnExport.Visible = true;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (data.Count > 0)
            {
                foreach (var item in data)
                {
                    item.FromDate = FromDate.ToString();
                    item.ToDate = ToDate.ToString();
                }

                if (lstIDContractBuy.Count > 0)
                {
                    var query = from g in data
                                where lstIDContractBuy.Contains(g.ContractID)
                                select g;
                    data = query.ToList();
                }

                var rD = new RContractDetail();
                rD.Period = ReportUtils.GetPeriod(FromDate, ToDate);

                if (data.Count > 0)
                {
                    var f = new ReportForm<RContract>(fileReportSlot1, _subSystemCode);
                    f.AddDatasource("ContractBuy", data);
                    f.AddDatasource("Detail", rD);
                    f.SetHyperlinkFunction("Accounting.ReportUtils", "Hyperlink");
                    f.LoadReport();
                    f.WindowState = FormWindowState.Maximized;
                    f.Show();
                }
                else
                {
                    MSG.Warning("Không có dữ liệu để xuất báo cáo");
                }
            }
            else
            {
                MSG.Warning("Không có dữ liệu để xuất báo cáo");
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog
            {
                Title = "Chọn nơi sao lưu ",
                Filter = "File (*.xls)|*.xls",
                InitialDirectory = "@C:\\",
                FileName = "tinhhinhthuchienhopdongmua.xls"
            };
            if (save.ShowDialog() == DialogResult.OK)
            {
                string duongdan = save.FileName;
                try
                {
                    ultraGridExcelExporter1.Export(uGrid, duongdan);
                    if (MSG.Question("Kết xuất thành công, Bạn có muốn mở file vừa kết xuất không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(save.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
                catch
                {
                    MSG.Warning("Lỗi khi kết xuất !");
                }
            }
        }     

        private void configGrid(List<RContract> lst)
        {
            uGrid.DataSource = lst;
            Utils.ConfigGrid(uGrid, ConstDatabase.ContractPurchase_TableName);
            uGrid.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;

            Utils.AddSumColumn(uGrid, "Code", false);
            Utils.AddSumColumn(uGrid, "AccountingObjectName", false);
            Utils.AddSumColumn(uGrid, "MaterialGoodsName", false);
            Utils.AddSumColumn(uGrid, "Unit", false);
            Utils.AddSumColumn(uGrid, "Quantity", false);
            Utils.AddSumColumn(uGrid, "QuantityReceipt", false);
            Utils.AddSumColumn(uGrid, "QuantityLeft", false);

            uGrid.DisplayLayout.Bands[0].Summaries[1].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[2].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[3].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[4].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[5].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[6].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[7].DisplayFormat = " ";

            uGrid.DisplayLayout.Bands[0].Columns["RevenueTypeText"].Hidden = true;

            uGrid.DisplayLayout.Bands[0].Columns["SignedDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["SignedDate"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["SignedDate"].Width = 120;
            uGrid.DisplayLayout.Bands[0].Columns["SignedDate"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["Code"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["Code"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["Code"].CellAppearance.TextVAlign = VAlign.Middle;
            uGrid.DisplayLayout.Bands[0].Columns["Code"].Width = 120;

            uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectName"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectName"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectName"].CellAppearance.TextVAlign = VAlign.Middle;
            uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectName"].Width = 180;

            uGrid.DisplayLayout.Bands[0].Columns["MaterialGoodsName"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["MaterialGoodsName"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["MaterialGoodsName"].Width = 180;
            uGrid.DisplayLayout.Bands[0].Columns["MaterialGoodsName"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["Unit"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["Unit"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["Unit"].Width = 100;
            uGrid.DisplayLayout.Bands[0].Columns["Unit"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["Quantity"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["Quantity"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["Quantity"].Width = 100;
            uGrid.DisplayLayout.Bands[0].Columns["Quantity"].CellAppearance.TextVAlign = VAlign.Middle;
            uGrid.DisplayLayout.Bands[0].Columns["Quantity"].FormatNumberic(ConstDatabase.Format_Quantity);

            uGrid.DisplayLayout.Bands[0].Columns["QuantityReceipt"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["QuantityReceipt"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["QuantityReceipt"].CellAppearance.TextVAlign = VAlign.Middle;
            uGrid.DisplayLayout.Bands[0].Columns["QuantityReceipt"].Width = 100;
            uGrid.DisplayLayout.Bands[0].Columns["QuantityReceipt"].FormatNumberic(ConstDatabase.Format_Quantity);

            uGrid.DisplayLayout.Bands[0].Columns["QuantityLeft"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["QuantityLeft"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["QuantityLeft"].CellAppearance.TextVAlign = VAlign.Middle;
            uGrid.DisplayLayout.Bands[0].Columns["QuantityLeft"].Width = 100;
            uGrid.DisplayLayout.Bands[0].Columns["QuantityLeft"].FormatNumberic(ConstDatabase.Format_Quantity);

            uGrid.DisplayLayout.Bands[0].Columns["ContractAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["ContractAmount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["ContractAmount"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["ContractAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["ContractAmount"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["ActionAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["ActionAmount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["ActionAmount"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["ActionAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["ActionAmount"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["UnActionAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["UnActionAmount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["UnActionAmount"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["UnActionAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["UnActionAmount"].CellAppearance.TextVAlign = VAlign.Middle;

            Utils.AddSumColumn1(lst.Where(n => n.ContractAmount != null).Sum(n => n.ContractAmount), uGrid, "ContractAmount", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.ActionAmount != null).Sum(n => n.ActionAmount), uGrid, "ActionAmount", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(lst.Where(n => n.UnActionAmount != null).Sum(n => n.UnActionAmount), uGrid, "UnActionAmount", false, constDatabaseFormat: 1);

            GridLayout();
        }
        private void GridLayout()
        {
            UltraGridBand parentBand = this.uGrid.DisplayLayout.Bands[0];
            parentBand.RowLayoutStyle = RowLayoutStyle.GroupLayout;

            UltraGridGroup parentBandGroupPeriod;
            if (parentBand.Groups.Exists("ParentBandGroupPeriod"))
            {
                parentBandGroupPeriod = parentBand.Groups["ParentBandGroupPeriod"];
                parentBandGroupPeriod.Header.Caption = period;
            }
            else
                parentBandGroupPeriod = parentBand.Groups.Add("ParentBandGroupPeriod", period);
            parentBandGroupPeriod.Header.Appearance.FontData.Italic = DefaultableBoolean.True;
            parentBandGroupPeriod.Header.Appearance.FontData.SizeInPoints = 10;
            parentBandGroupPeriod.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupPeriod.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPeriod.Header.Appearance.BorderColor = Color.Black;

            UltraGridGroup parentBandGroupTitle;
            if (parentBand.Groups.Exists("ParentBandGroupTitle"))
                parentBandGroupTitle = parentBand.Groups["ParentBandGroupTitle"];
            else
                parentBandGroupTitle = parentBand.Groups.Add("ParentBandGroupTitle", "TÌNH HÌNH THỰC HIỆN HỢP ĐỒNG MUA");
            parentBandGroupTitle.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroupTitle.Header.Appearance.FontData.SizeInPoints = 12;
            parentBandGroupTitle.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupTitle.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupTitle.Header.Appearance.BorderColor = Color.Black;

            parentBandGroupPeriod.RowLayoutGroupInfo.ParentGroup = parentBandGroupTitle;

            parentBand.Columns["SignedDate"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["SignedDate"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SignedDate"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["SignedDate"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["SignedDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SignedDate"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["Code"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["Code"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Code"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["Code"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["Code"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Code"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["AccountingObjectName"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["AccountingObjectName"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["AccountingObjectName"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["AccountingObjectName"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["AccountingObjectName"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["AccountingObjectName"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["MaterialGoodsName"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["MaterialGoodsName"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["MaterialGoodsName"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["MaterialGoodsName"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["MaterialGoodsName"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["MaterialGoodsName"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["Unit"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["Unit"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Unit"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["Unit"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["Unit"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Unit"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["Quantity"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["Quantity"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Quantity"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["Quantity"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["Quantity"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Quantity"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["QuantityReceipt"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["QuantityReceipt"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["QuantityReceipt"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["QuantityReceipt"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["QuantityReceipt"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["QuantityReceipt"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["QuantityLeft"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["QuantityLeft"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["QuantityLeft"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["QuantityLeft"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["QuantityLeft"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["QuantityLeft"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["ContractAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["ContractAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ContractAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["ContractAmount"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["ContractAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ContractAmount"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["ActionAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["ActionAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ActionAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["ActionAmount"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["ActionAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["ActionAmount"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["UnActionAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["UnActionAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["UnActionAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["UnActionAmount"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["UnActionAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["UnActionAmount"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["SignedDate"].RowLayoutColumnInfo.OriginX = 0;
            parentBand.Columns["SignedDate"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["SignedDate"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["SignedDate"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["Code"].RowLayoutColumnInfo.OriginX = 120;
            parentBand.Columns["Code"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["Code"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["Code"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["AccountingObjectName"].RowLayoutColumnInfo.OriginX = 240;
            parentBand.Columns["AccountingObjectName"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["AccountingObjectName"].RowLayoutColumnInfo.SpanX = 180;
            parentBand.Columns["AccountingObjectName"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["MaterialGoodsName"].RowLayoutColumnInfo.OriginX = 420;
            parentBand.Columns["MaterialGoodsName"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["MaterialGoodsName"].RowLayoutColumnInfo.SpanX = 180;
            parentBand.Columns["MaterialGoodsName"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["Unit"].RowLayoutColumnInfo.OriginX = 600;
            parentBand.Columns["Unit"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["Unit"].RowLayoutColumnInfo.SpanX = 100;
            parentBand.Columns["Unit"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["Quantity"].RowLayoutColumnInfo.OriginX = 700;
            parentBand.Columns["Quantity"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["Quantity"].RowLayoutColumnInfo.SpanX = 100;
            parentBand.Columns["Quantity"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["QuantityReceipt"].RowLayoutColumnInfo.OriginX = 800;
            parentBand.Columns["QuantityReceipt"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["QuantityReceipt"].RowLayoutColumnInfo.SpanX = 100;
            parentBand.Columns["QuantityReceipt"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["QuantityLeft"].RowLayoutColumnInfo.OriginX = 900;
            parentBand.Columns["QuantityLeft"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["QuantityLeft"].RowLayoutColumnInfo.SpanX = 100;
            parentBand.Columns["QuantityLeft"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["ContractAmount"].RowLayoutColumnInfo.OriginX = 1000;
            parentBand.Columns["ContractAmount"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["ContractAmount"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["ContractAmount"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["ActionAmount"].RowLayoutColumnInfo.OriginX = 1250;
            parentBand.Columns["ActionAmount"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["ActionAmount"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["ActionAmount"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["UnActionAmount"].RowLayoutColumnInfo.OriginX = 1500;
            parentBand.Columns["UnActionAmount"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["UnActionAmount"].RowLayoutColumnInfo.SpanX = 250;
            parentBand.Columns["UnActionAmount"].RowLayoutColumnInfo.SpanY = 32;
        }

        private void uGrid_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            UltraGridCell cell = e.Row.Cells[0];
            cell.Row.Height = 25;

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("ContractAmount"))
            {
                if (cell.Row.Cells["ContractAmount"].Value != null && (decimal)cell.Row.Cells["ContractAmount"].Value == 0)
                    cell.Row.Cells["ContractAmount"].Value = null;
                if ((cell.Row.Cells["ContractAmount"].Value != null && (decimal)cell.Row.Cells["ContractAmount"].Value < 0))
                    cell.Row.Cells["ContractAmount"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("ActionAmount"))
            {
                if (cell.Row.Cells["ActionAmount"].Value != null && (decimal)cell.Row.Cells["ActionAmount"].Value == 0)
                    cell.Row.Cells["ActionAmount"].Value = null;
                if ((cell.Row.Cells["ActionAmount"].Value != null && (decimal)cell.Row.Cells["ActionAmount"].Value < 0))
                    cell.Row.Cells["ActionAmount"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("UnActionAmount"))
            {
                if (cell.Row.Cells["UnActionAmount"].Value != null && (decimal)cell.Row.Cells["UnActionAmount"].Value == 0)
                    cell.Row.Cells["UnActionAmount"].Value = null;
                if ((cell.Row.Cells["UnActionAmount"].Value != null && (decimal)cell.Row.Cells["UnActionAmount"].Value < 0))
                    cell.Row.Cells["UnActionAmount"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("QuantityLeft"))
            {
                if (cell.Row.Cells["QuantityLeft"].Value != null && (decimal)cell.Row.Cells["QuantityLeft"].Value == 0)
                    cell.Row.Cells["QuantityLeft"].Value = null;
                if ((cell.Row.Cells["QuantityLeft"].Value != null && (decimal)cell.Row.Cells["QuantityLeft"].Value < 0))
                    cell.Row.Cells["QuantityLeft"].Appearance.ForeColor = Color.Red;
            }

        }

        private void ultraGridExcelExporter1_InitializeColumn(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.InitializeColumnEventArgs e)
        {
            e.ExcelFormatStr = e.Column.Format;
        }

        private void ultraGridExcelExporter1_BeginExport(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.BeginExportEventArgs e)
        {
            foreach (var row in uGrid.Rows)
            {
                foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
                {
                    e.Rows[row.Index].Cells[column].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                    e.Rows[row.Index].Cells[column].Appearance.BorderColor = Color.Black;
                    e.Rows[row.Index].Cells[column].Column.Width = 200;
                }
            }
            e.Rows.Band.Summaries[0].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[0].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[1].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[1].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[2].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[2].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[3].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[3].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[4].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[4].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[5].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[5].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[6].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[6].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[7].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[7].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[8].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[8].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[9].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[9].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[10].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[10].Appearance.BorderColor = Color.Black;
        }

        private void FRB01aDNN_SizeChanged(object sender, EventArgs e)
        {
            var state = this.WindowState;
            switch (state)
            {
                case FormWindowState.Maximized:
                    break;
                case FormWindowState.Minimized:
                    break;
                case FormWindowState.Normal:
                    this.Left = (Screen.PrimaryScreen.Bounds.Width - this.Width) / 2;
                    this.Top = (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2;
                    this.Location = new Point((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2, (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
                    break;
                default:
                    break;
            }
        }

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                RContract temp = (RContract)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                if (temp != null)
                    editFuntion(temp);

            }
        }

        private void editFuntion(RContract temp)
        {
            var f = Utils.ViewVoucherSelected1((Guid)temp.ContractID, 850);
            //List<RContract> data1 = sp.GetContract(FromDate, ToDate, false);
            //configGrid(data1);
        }

        private void ultraGridExcelExporter1_SummaryCellExported(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.SummaryCellExportedEventArgs e)
        {
            Worksheet ws = e.CurrentWorksheet;
            WorksheetRow wsRow = ws.Rows[e.CurrentRowIndex];
            WorksheetCell wsCell = wsRow.Cells[e.CurrentColumnIndex];
            if (!e.Summary.SummaryText.Contains("Số dòng") && !e.Summary.SummaryText.Contains(" "))
            {
                string s = e.Summary.SummaryText.Trim('.');
                if (s.Contains("("))
                    wsCell.Value = -decimal.Parse(s.Trim('(').Trim(')'));
                else
                    wsCell.Value = decimal.Parse(s);

                wsCell.CellFormat.FormatString = "###,###,###,###,##0;(###,###,###,###,##0)";
            }
        }
    }
}
