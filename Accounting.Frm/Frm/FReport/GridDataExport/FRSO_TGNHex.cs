﻿using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.DAO;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Documents.Excel;

namespace Accounting.Frm.FReport
{
    public partial class FRSO_TGNHex : Form
    {
        private readonly IAccountService _IAccountService;
        private readonly IBankAccountDetailService _IBankAccountDetailService;
        List<BookDepositListDetail> data = new List<BookDepositListDetail>();
        private string _subSystemCode;
        ReportProcedureSDS sp = new ReportProcedureSDS();
        string period = "";
        DateTime FromDate;
        DateTime ToDate;
        string account;
        string accountBank;
        string CurrencyID;
        public FRSO_TGNHex(DateTime FromDate1, DateTime ToDate1, string account1, string CurrencyID1, string accountBank1)
        {
            InitializeComponent();
            _IAccountService = IoC.Resolve<IAccountService>();
            _IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\SoTienGuiTKNganHang.rst", path);

            if (accountBank1 != "")
            {
                string txtThoiGian = "Tài khoản: " + account1 + "; Loại tiền: " + CurrencyID1 + "; TK Ngân hàng: " + accountBank1;
                period = txtThoiGian + "; " + ReportUtils.GetPeriod(FromDate1, ToDate1);
            }
            else
            {
                string txtThoiGian = "Tài khoản: " + account1 + "; Loại tiền: " + CurrencyID1;
                period = txtThoiGian + "; " + ReportUtils.GetPeriod(FromDate1, ToDate1);
            }            
            List<BookDepositListDetail> data1 = sp.GetBookDepositListDetail("", false, FromDate1, ToDate1, account1, CurrencyID1, accountBank1, false, false);

            data = data1;
            FromDate = FromDate1;
            ToDate = ToDate1;
            account = account1;
            accountBank = accountBank1;
            CurrencyID = CurrencyID1;

            configGrid(data);

            if (Utils.isDemo)
                btnExport.Visible = false;
            else
                btnExport.Visible = true;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            List<BookDepositListDetail> data = sp.GetBookDepositListDetail("", false, FromDate, ToDate, account, CurrencyID, accountBank, false, false);
            if (data.Count == 0)
                MSG.Warning("Không có dữ liệu.");
            else
            {
                data.ForEach(t =>
                {
                    if (t.JournalMemo == null)
                        t.JournalMemo = "";
                    else
                        t.JournalMemo = t.JournalMemo;
                });

                var listCongNhom = new List<BookDepositListDetail>();
                try
                {

                    listCongNhom = data.GroupBy(l => l.BankAccount).Select(t => new BookDepositListDetail
                    {
                        BankAccount = t.Key,
                        DebitAmountOC = t.Where(n => n.DebitAmountOC != null && n.JournalMemo == "Cộng nhóm").Sum(k => k.DebitAmountOC),//add by cuongpv
                        DebitAmount = t.Where(n => n.DebitAmount != null && n.JournalMemo == "Cộng nhóm").Sum(k => k.DebitAmount),
                        CreditAmountOC = t.Where(n => n.CreditAmountOC != null && n.JournalMemo == "Cộng nhóm").Sum(k => k.CreditAmountOC),//add by cuongpv
                        CreditAmount = t.Where(n => n.CreditAmount != null && n.JournalMemo == "Cộng nhóm").Sum(k => k.CreditAmount),
                        ClosingAmountOC = t.Select(n => n.JournalMemo).Contains("Số dư đầu kỳ") ? t.Where(n => n.DebitAmountOC != null && n.JournalMemo == "Cộng nhóm").Sum(k => k.DebitAmountOC) - t.Where(n => n.CreditAmountOC != null && n.JournalMemo == "Cộng nhóm").Sum(k => k.CreditAmountOC) + (t.Where(k => k.ClosingAmountOC != null && k.JournalMemo == "Số dư đầu kỳ").Sum(k => k.ClosingAmountOC)) : t.Last().ClosingAmountOC,//add by cuongpv
                        ClosingAmount = t.Select(n => n.JournalMemo).Contains("Số dư đầu kỳ") ? t.Where(n => n.DebitAmount != null && n.JournalMemo == "Cộng nhóm").Sum(k => k.DebitAmount) - t.Where(n => n.CreditAmount != null && n.JournalMemo == "Cộng nhóm").Sum(k => k.CreditAmount) + (t.Where(k => k.ClosingAmount != null && k.JournalMemo == "Số dư đầu kỳ").Sum(k => k.ClosingAmount)) : t.Last().ClosingAmount,
                    }).ToList();
                }
                catch (Exception ex) { }
                data = data.Where(n => n.JournalMemo != "Cộng nhóm" && !n.JournalMemo.Contains("Tài khoản ngân hàng: ")).ToList();

                var rD = new BookDepositListDetail_period();
                if (accountBank != "")
                {
                    string txtThoiGian = "Tài khoản: " + account + "; Loại tiền: " + CurrencyID + "; TK Ngân hàng: " + accountBank;
                    rD.Period = txtThoiGian + "; " + ReportUtils.GetPeriod(FromDate, ToDate);
                }
                else
                {
                    string txtThoiGian = "Tài khoản: " + account + "; Loại tiền: " + CurrencyID;
                    rD.Period = txtThoiGian + "; " + ReportUtils.GetPeriod(FromDate, ToDate);
                }
                var f = new ReportForm<BookDepositListDetail>(fileReportSlot1, _subSystemCode);
                f.AddDatasource("DBCONGNHOM", listCongNhom, true);
                f.AddDatasource("DBSOTGTKNH", data, true);
                f.AddDatasource("Detail", rD);
                f.SetHyperlinkFunction("Accounting.ReportUtils", "Hyperlink");
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void reportManager1_GetReportParameter(object sender, PerpetuumSoft.Reporting.Components.GetReportParameterEventArgs e)
        {
            e.Parameters["CurrencyID"].Value = CurrencyID;
        }

        private void configGrid(List<BookDepositListDetail> lst)
        {
            uGrid.DataSource = lst;
            Utils.ConfigGrid(uGrid, ConstDatabase.BookDepositListDetail_TableName);
            uGrid.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;

            lst.ForEach(t =>
            {
                if (t.JournalMemo == null)
                    t.JournalMemo = "";
                else
                    t.JournalMemo = t.JournalMemo;
            });

            Utils.AddSumColumn(uGrid, "RefDate", false);
            Utils.AddSumColumn(uGrid, "RefNo", false);
            Utils.AddSumColumn(uGrid, "JournalMemo", false);
            Utils.AddSumColumn(uGrid, "CorrespondingAccountNumber", false);

            uGrid.DisplayLayout.Bands[0].Summaries[1].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[2].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[3].DisplayFormat = " ";
            uGrid.DisplayLayout.Bands[0].Summaries[4].DisplayFormat = " ";

            if (CurrencyID == "VND")
            {
                uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGrid.DisplayLayout.Bands[0].Columns["ClosingAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGrid.DisplayLayout.Bands[0].Columns["DebitAmountOC"].Hidden = true;
                uGrid.DisplayLayout.Bands[0].Columns["CreditAmountOC"].Hidden = true;
                uGrid.DisplayLayout.Bands[0].Columns["ClosingAmountOC"].Hidden = true;

                Utils.AddSumColumn1(lst.Where(n => n.JournalMemo == "Cộng nhóm" && n.DebitAmount != null).Sum(n => n.DebitAmount), uGrid, "DebitAmount", false, constDatabaseFormat: 1);
                Utils.AddSumColumn1(lst.Where(n => n.JournalMemo == "Cộng nhóm" && n.CreditAmount != null).Sum(n => n.CreditAmount), uGrid, "CreditAmount", false, constDatabaseFormat: 1);
                Utils.AddSumColumn1(lst.Where(n => n.JournalMemo == "Cộng nhóm" && n.ClosingAmount != null).Sum(n => n.ClosingAmount), uGrid, "ClosingAmount", false, constDatabaseFormat: 1);

            }
            else
            {
                uGrid.DisplayLayout.Bands[0].Columns["DebitAmountOC"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                uGrid.DisplayLayout.Bands[0].Columns["CreditAmountOC"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                uGrid.DisplayLayout.Bands[0].Columns["ClosingAmountOC"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"].Hidden = true;
                uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"].Hidden = true;
                uGrid.DisplayLayout.Bands[0].Columns["ClosingAmount"].Hidden = true;

                Utils.AddSumColumn1(lst.Where(n => n.JournalMemo == "Cộng nhóm" && n.CreditAmountOC != null).Sum(n => n.CreditAmountOC), uGrid, "CreditAmountOC", false, constDatabaseFormat: 2);
                Utils.AddSumColumn1(lst.Where(n => n.JournalMemo == "Cộng nhóm" && n.DebitAmountOC != null).Sum(n => n.DebitAmountOC), uGrid, "DebitAmountOC", false, constDatabaseFormat: 2);
                Utils.AddSumColumn1(lst.Where(n => n.JournalMemo == "Cộng nhóm" && n.ClosingAmountOC != null).Sum(n => n.ClosingAmountOC), uGrid, "ClosingAmountOC", false, constDatabaseFormat: 2);

            }

            GridLayout();

            uGrid.DisplayLayout.Bands[0].Columns["PostedDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["PostedDate"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["PostedDate"].Width = 120;

            uGrid.DisplayLayout.Bands[0].Columns["RefDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["RefDate"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["RefDate"].Width = 120;

            uGrid.DisplayLayout.Bands[0].Columns["RefNo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["RefNo"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["RefNo"].Width = 120;
            uGrid.DisplayLayout.Bands[0].Columns["RefNo"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["JournalMemo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["JournalMemo"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["JournalMemo"].Width = 500;
            uGrid.DisplayLayout.Bands[0].Columns["JournalMemo"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["CorrespondingAccountNumber"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["CorrespondingAccountNumber"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["CorrespondingAccountNumber"].Width = 100;
            uGrid.DisplayLayout.Bands[0].Columns["CorrespondingAccountNumber"].CellAppearance.TextHAlign = HAlign.Center;
            uGrid.DisplayLayout.Bands[0].Columns["CorrespondingAccountNumber"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["DebitAmount"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["CreditAmount"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["DebitAmountOC"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["DebitAmountOC"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["DebitAmountOC"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["DebitAmountOC"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["CreditAmountOC"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["CreditAmountOC"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["CreditAmountOC"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["CreditAmountOC"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["ClosingAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["ClosingAmount"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["ClosingAmount"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["ClosingAmount"].CellAppearance.TextVAlign = VAlign.Middle;

            uGrid.DisplayLayout.Bands[0].Columns["ClosingAmountOC"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            uGrid.DisplayLayout.Bands[0].Columns["ClosingAmountOC"].Header.Appearance.BorderColor = Color.Black;
            uGrid.DisplayLayout.Bands[0].Columns["ClosingAmountOC"].Width = 250;
            uGrid.DisplayLayout.Bands[0].Columns["ClosingAmountOC"].CellAppearance.TextVAlign = VAlign.Middle;

        }
        private void btnExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog
            {
                Title = "Chọn nơi sao lưu ",
                Filter = "File (*.xls)|*.xls",
                InitialDirectory = "@C:\\",
                FileName = "sotienguinganhang.xls"
            };
            if (save.ShowDialog() == DialogResult.OK)
            {
                string duongdan = save.FileName;
                try
                {
                    ultraGridExcelExporter1.Export(uGrid, duongdan);
                    if (MSG.Question("Kết xuất thành công, Bạn có muốn mở file vừa kết xuất không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(save.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
                catch
                {
                    MSG.Warning("Lỗi khi kết xuất !");
                }
            }
        }


        private void GridLayout()
        {
            UltraGridBand parentBand = this.uGrid.DisplayLayout.Bands[0];
            parentBand.RowLayoutStyle = RowLayoutStyle.GroupLayout;

            UltraGridGroup parentBandGroupPeriod;
            if (parentBand.Groups.Exists("ParentBandGroupPeriod"))
            {
                parentBandGroupPeriod = parentBand.Groups["ParentBandGroupPeriod"];
                parentBandGroupPeriod.Header.Caption = period;
            }
            else
                parentBandGroupPeriod = parentBand.Groups.Add("ParentBandGroupPeriod", period);
            parentBandGroupPeriod.Header.Appearance.FontData.Italic = DefaultableBoolean.True;
            parentBandGroupPeriod.Header.Appearance.FontData.SizeInPoints = 10;
            parentBandGroupPeriod.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupPeriod.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPeriod.Header.Appearance.BorderColor = Color.Black;

            UltraGridGroup parentBandGroupTitle;
            if (parentBand.Groups.Exists("ParentBandGroupTitle"))
                parentBandGroupTitle = parentBand.Groups["ParentBandGroupTitle"];
            else
                parentBandGroupTitle = parentBand.Groups.Add("ParentBandGroupTitle", "SỔ TIỀN GỬI NGÂN HÀNG");
            parentBandGroupTitle.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroupTitle.Header.Appearance.FontData.SizeInPoints = 12;
            parentBandGroupTitle.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupTitle.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupTitle.Header.Appearance.BorderColor = Color.Black;

            parentBandGroupPeriod.RowLayoutGroupInfo.ParentGroup = parentBandGroupTitle;

            if (CurrencyID == "VND")
            {
                parentBand.Columns["DebitAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
                parentBand.Columns["DebitAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["DebitAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["DebitAmount"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["DebitAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["DebitAmount"].Header.Appearance.BorderColor = Color.Black;

                parentBand.Columns["CreditAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
                parentBand.Columns["CreditAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["CreditAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["CreditAmount"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["CreditAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["CreditAmount"].Header.Appearance.BorderColor = Color.Black;

                parentBand.Columns["ClosingAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
                parentBand.Columns["ClosingAmount"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["ClosingAmount"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["ClosingAmount"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["ClosingAmount"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["ClosingAmount"].Header.Appearance.BorderColor = Color.Black;

            }
            else
            {
                parentBand.Columns["DebitAmountOC"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
                parentBand.Columns["DebitAmountOC"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["DebitAmountOC"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["DebitAmountOC"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["DebitAmountOC"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["DebitAmountOC"].Header.Appearance.BorderColor = Color.Black;

                parentBand.Columns["CreditAmountOC"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
                parentBand.Columns["CreditAmountOC"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["CreditAmountOC"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["CreditAmountOC"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["CreditAmountOC"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["CreditAmountOC"].Header.Appearance.BorderColor = Color.Black;

                parentBand.Columns["ClosingAmountOC"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
                parentBand.Columns["ClosingAmountOC"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["ClosingAmountOC"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
                parentBand.Columns["ClosingAmountOC"].Header.Appearance.ForeColor = Color.Black;
                parentBand.Columns["ClosingAmountOC"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                parentBand.Columns["ClosingAmountOC"].Header.Appearance.BorderColor = Color.Black;
            }

            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["PostedDate"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["PostedDate"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["PostedDate"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["PostedDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["PostedDate"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["RefDate"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["RefDate"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["RefDate"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["RefDate"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["RefDate"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["RefDate"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["RefNo"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["RefNo"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["RefNo"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["RefNo"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["RefNo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["RefNo"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["JournalMemo"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["JournalMemo"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["JournalMemo"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["JournalMemo"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["JournalMemo"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["JournalMemo"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["CorrespondingAccountNumber"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["CorrespondingAccountNumber"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["CorrespondingAccountNumber"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["CorrespondingAccountNumber"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["CorrespondingAccountNumber"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["CorrespondingAccountNumber"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.OriginX = 0;
            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["PostedDate"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["RefDate"].RowLayoutColumnInfo.OriginX = 120;
            parentBand.Columns["RefDate"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["RefDate"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["RefDate"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["RefNo"].RowLayoutColumnInfo.OriginX = 240;
            parentBand.Columns["RefNo"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["RefNo"].RowLayoutColumnInfo.SpanX = 120;
            parentBand.Columns["RefNo"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["JournalMemo"].RowLayoutColumnInfo.OriginX = 360;
            parentBand.Columns["JournalMemo"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["JournalMemo"].RowLayoutColumnInfo.SpanX = 500;
            parentBand.Columns["JournalMemo"].RowLayoutColumnInfo.SpanY = 32;

            parentBand.Columns["CorrespondingAccountNumber"].RowLayoutColumnInfo.OriginX = 860;
            parentBand.Columns["CorrespondingAccountNumber"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["CorrespondingAccountNumber"].RowLayoutColumnInfo.SpanX = 100;
            parentBand.Columns["CorrespondingAccountNumber"].RowLayoutColumnInfo.SpanY = 32;

            if (CurrencyID == "VND")
            {
                parentBand.Columns["DebitAmount"].RowLayoutColumnInfo.OriginX = 960;
                parentBand.Columns["DebitAmount"].RowLayoutColumnInfo.OriginY = 0;
                parentBand.Columns["DebitAmount"].RowLayoutColumnInfo.SpanX = 250;
                parentBand.Columns["DebitAmount"].RowLayoutColumnInfo.SpanY = 32;

                parentBand.Columns["CreditAmount"].RowLayoutColumnInfo.OriginX = 1210;
                parentBand.Columns["CreditAmount"].RowLayoutColumnInfo.OriginY = 0;
                parentBand.Columns["CreditAmount"].RowLayoutColumnInfo.SpanX = 250;
                parentBand.Columns["CreditAmount"].RowLayoutColumnInfo.SpanY = 32;

                parentBand.Columns["ClosingAmount"].RowLayoutColumnInfo.OriginX = 1460;
                parentBand.Columns["ClosingAmount"].RowLayoutColumnInfo.OriginY = 0;
                parentBand.Columns["ClosingAmount"].RowLayoutColumnInfo.SpanX = 250;
                parentBand.Columns["ClosingAmount"].RowLayoutColumnInfo.SpanY = 32;
            }
            else
            {
                parentBand.Columns["DebitAmountOC"].RowLayoutColumnInfo.OriginX = 960;
                parentBand.Columns["DebitAmountOC"].RowLayoutColumnInfo.OriginY = 0;
                parentBand.Columns["DebitAmountOC"].RowLayoutColumnInfo.SpanX = 250;
                parentBand.Columns["DebitAmountOC"].RowLayoutColumnInfo.SpanY = 32;

                parentBand.Columns["CreditAmountOC"].RowLayoutColumnInfo.OriginX = 1210;
                parentBand.Columns["CreditAmountOC"].RowLayoutColumnInfo.OriginY = 0;
                parentBand.Columns["CreditAmountOC"].RowLayoutColumnInfo.SpanX = 250;
                parentBand.Columns["CreditAmountOC"].RowLayoutColumnInfo.SpanY = 32;

                parentBand.Columns["ClosingAmountOC"].RowLayoutColumnInfo.OriginX = 1460;
                parentBand.Columns["ClosingAmountOC"].RowLayoutColumnInfo.OriginY = 0;
                parentBand.Columns["ClosingAmountOC"].RowLayoutColumnInfo.SpanX = 250;
                parentBand.Columns["ClosingAmountOC"].RowLayoutColumnInfo.SpanY = 32;
            }
        }

        private void uGrid_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            UltraGridCell cell = e.Row.Cells[0];
            cell.Row.Height = 25;

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("JournalMemo"))
                if (cell.Row.Cells["JournalMemo"].Value != null && (cell.Row.Cells["JournalMemo"].Value.ToString().Contains("Tài khoản ngân hàng: ") || cell.Row.Cells["JournalMemo"].Value.ToString() == "Cộng nhóm" || cell.Row.Cells["JournalMemo"].Value.ToString() == "Số dư đầu kỳ"))
                {
                    cell.Row.Cells["PostedDate"].Value = null;
                    cell.Row.Cells["RefDate"].Value = null;
                    cell.Row.Cells["JournalMemo"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    cell.Row.Cells["DebitAmount"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    cell.Row.Cells["DebitAmountOC"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    cell.Row.Cells["CreditAmount"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    cell.Row.Cells["CreditAmountOC"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    cell.Row.Cells["ClosingAmount"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    cell.Row.Cells["ClosingAmountOC"].Appearance.FontData.Bold = DefaultableBoolean.True;
                }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("DebitAmount"))
            {
                if (cell.Row.Cells["DebitAmount"].Value != null && (decimal)cell.Row.Cells["DebitAmount"].Value == 0)
                    cell.Row.Cells["DebitAmount"].Value = null;
                if ((cell.Row.Cells["DebitAmount"].Value != null && (decimal)cell.Row.Cells["DebitAmount"].Value < 0))
                    cell.Row.Cells["DebitAmount"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("DebitAmountOC"))
            {
                if (cell.Row.Cells["DebitAmountOC"].Value != null && (decimal)cell.Row.Cells["DebitAmountOC"].Value == 0)
                    cell.Row.Cells["DebitAmountOC"].Value = null;
                if ((cell.Row.Cells["DebitAmountOC"].Value != null && (decimal)cell.Row.Cells["DebitAmountOC"].Value < 0))
                    cell.Row.Cells["DebitAmountOC"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("CreditAmount"))
            {
                if (cell.Row.Cells["CreditAmount"].Value != null && (decimal)cell.Row.Cells["CreditAmount"].Value == 0)
                    cell.Row.Cells["CreditAmount"].Value = null;
                if ((cell.Row.Cells["CreditAmount"].Value != null && (decimal)cell.Row.Cells["CreditAmount"].Value < 0))
                    cell.Row.Cells["CreditAmount"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("CreditAmountOC"))
            {
                if (cell.Row.Cells["CreditAmountOC"].Value != null && (decimal)cell.Row.Cells["CreditAmountOC"].Value == 0)
                    cell.Row.Cells["CreditAmountOC"].Value = null;
                if ((cell.Row.Cells["CreditAmountOC"].Value != null && (decimal)cell.Row.Cells["CreditAmountOC"].Value < 0))
                    cell.Row.Cells["CreditAmountOC"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("ClosingAmount"))
            {
                if (cell.Row.Cells["ClosingAmount"].Value != null && (decimal)cell.Row.Cells["ClosingAmount"].Value == 0)
                    cell.Row.Cells["ClosingAmount"].Value = null;
                if ((cell.Row.Cells["ClosingAmount"].Value != null && (decimal)cell.Row.Cells["ClosingAmount"].Value < 0))
                    cell.Row.Cells["ClosingAmount"].Appearance.ForeColor = Color.Red;
            }

            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("ClosingAmountOC"))
            {
                if (cell.Row.Cells["ClosingAmountOC"].Value != null && (decimal)cell.Row.Cells["ClosingAmountOC"].Value == 0)
                    cell.Row.Cells["ClosingAmountOC"].Value = null;
                if ((cell.Row.Cells["ClosingAmountOC"].Value != null && (decimal)cell.Row.Cells["ClosingAmountOC"].Value < 0))
                    cell.Row.Cells["ClosingAmountOC"].Appearance.ForeColor = Color.Red;
            }
        }

        private void ultraGridExcelExporter1_InitializeColumn(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.InitializeColumnEventArgs e)
        {
            e.ExcelFormatStr = e.Column.Format;
        }

        private void ultraGridExcelExporter1_BeginExport(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.BeginExportEventArgs e)
        {
            foreach (var row in uGrid.Rows)
            {
                foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
                {
                    e.Rows[row.Index].Cells[column].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                    e.Rows[row.Index].Cells[column].Appearance.BorderColor = Color.Black;
                    e.Rows[row.Index].Cells[column].Column.Width = 200;
                }
            }
            e.Rows.Band.Summaries[0].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[0].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[1].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[1].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[2].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[2].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[3].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[3].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[4].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[4].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[5].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[5].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[6].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[6].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[7].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[7].Appearance.BorderColor = Color.Black;
        }

        private void FRSO_TGNH_SizeChanged(object sender, EventArgs e)
        {
            var state = this.WindowState;
            switch (state)
            {
                case FormWindowState.Maximized:
                    break;
                case FormWindowState.Minimized:
                    break;
                case FormWindowState.Normal:
                    this.Left = (Screen.PrimaryScreen.Bounds.Width - this.Width) / 2;
                    this.Top = (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2;
                    this.Location = new Point((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2, (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
                    break;
                default:
                    break;
            }
        }

        private void uGrid_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                BookDepositListDetail temp = (BookDepositListDetail)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                if (temp != null)
                    editFuntion(temp);

            }
        }
        private void editFuntion(BookDepositListDetail temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID, temp.RefType);
            //List<BookDepositListDetail> data1 = sp.GetBookDepositListDetail("", false, FromDate, ToDate, account, CurrencyID, accountBank, false, false);
            //configGrid(data1);

        }

        private void ultraGridExcelExporter1_SummaryCellExported(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.SummaryCellExportedEventArgs e)
        {
            Worksheet ws = e.CurrentWorksheet;
            WorksheetRow wsRow = ws.Rows[e.CurrentRowIndex];
            WorksheetCell wsCell = wsRow.Cells[e.CurrentColumnIndex];
            if (!e.Summary.SummaryText.Contains("Số dòng") && !e.Summary.SummaryText.Contains(" "))
            {
                string s = e.Summary.SummaryText.Trim('.');
                if (s.Contains("("))
                    wsCell.Value = -decimal.Parse(s.Trim('(').Trim(')'));
                else
                    wsCell.Value = decimal.Parse(s);

                wsCell.CellFormat.FormatString = "###,###,###,###,##0;(###,###,###,###,##0)";
            }
        }
    }
}
