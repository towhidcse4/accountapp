﻿namespace Accounting.Frm.FReport
{
    partial class FRB09DNNDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FRB09DNNDetail));
            this.Sec_V_5_1 = new System.Windows.Forms.Label();
            this.Sec_V_5_2 = new System.Windows.Forms.Label();
            this.Sec_V_5_3 = new System.Windows.Forms.Label();
            this.Sec_V_5_4 = new System.Windows.Forms.Label();
            this.Sec_V_5_5 = new System.Windows.Forms.Label();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.label108 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.label112 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.label116 = new System.Windows.Forms.Label();
            this.label117 = new System.Windows.Forms.Label();
            this.label118 = new System.Windows.Forms.Label();
            this.label119 = new System.Windows.Forms.Label();
            this.label120 = new System.Windows.Forms.Label();
            this.label121 = new System.Windows.Forms.Label();
            this.label122 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.V_6_a_1_1 = new System.Windows.Forms.Label();
            this.V_6_a_2_1 = new System.Windows.Forms.Label();
            this.V_6_a_3_1 = new System.Windows.Forms.Label();
            this.V_6_a_4_1 = new System.Windows.Forms.Label();
            this.V_6_a_4_2 = new System.Windows.Forms.Label();
            this.V_6_a_4_3 = new System.Windows.Forms.Label();
            this.V_6_b_4_1 = new System.Windows.Forms.Label();
            this.V_6_b_4_2 = new System.Windows.Forms.Label();
            this.V_6_b_4_3 = new System.Windows.Forms.Label();
            this.V_6_b_4_4 = new System.Windows.Forms.Label();
            this.V_6_a_3_2 = new System.Windows.Forms.Label();
            this.V_6_a_3_3 = new System.Windows.Forms.Label();
            this.V_6_b_3_1 = new System.Windows.Forms.Label();
            this.V_6_b_3_2 = new System.Windows.Forms.Label();
            this.V_6_b_3_3 = new System.Windows.Forms.Label();
            this.V_6_b_3_4 = new System.Windows.Forms.Label();
            this.V_6_a_2_2 = new System.Windows.Forms.Label();
            this.V_6_a_2_3 = new System.Windows.Forms.Label();
            this.V_6_b_2_1 = new System.Windows.Forms.Label();
            this.V_6_b_2_2 = new System.Windows.Forms.Label();
            this.V_6_b_2_3 = new System.Windows.Forms.Label();
            this.V_6_b_2_4 = new System.Windows.Forms.Label();
            this.V_6_a_1_2 = new System.Windows.Forms.Label();
            this.V_6_a_1_3 = new System.Windows.Forms.Label();
            this.V_6_b_1_1 = new System.Windows.Forms.Label();
            this.V_6_b_1_2 = new System.Windows.Forms.Label();
            this.V_6_b_1_3 = new System.Windows.Forms.Label();
            this.V_6_b_1_4 = new System.Windows.Forms.Label();
            this.Sec_V_6_1 = new System.Windows.Forms.Label();
            this.Sec_V_6_2 = new System.Windows.Forms.Label();
            this.Sec_V_6_3 = new System.Windows.Forms.Label();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.label126 = new System.Windows.Forms.Label();
            this.label127 = new System.Windows.Forms.Label();
            this.label128 = new System.Windows.Forms.Label();
            this.label129 = new System.Windows.Forms.Label();
            this.label130 = new System.Windows.Forms.Label();
            this.label131 = new System.Windows.Forms.Label();
            this.label132 = new System.Windows.Forms.Label();
            this.V_7_1_1 = new System.Windows.Forms.Label();
            this.V_7_1_2 = new System.Windows.Forms.Label();
            this.V_7_Sum_1 = new System.Windows.Forms.Label();
            this.V_7_2_1 = new System.Windows.Forms.Label();
            this.V_7_2_2 = new System.Windows.Forms.Label();
            this.V_7_2_3 = new System.Windows.Forms.Label();
            this.V_7_Sum_2 = new System.Windows.Forms.Label();
            this.V_7_1_3 = new System.Windows.Forms.Label();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.label133 = new System.Windows.Forms.Label();
            this.label134 = new System.Windows.Forms.Label();
            this.label135 = new System.Windows.Forms.Label();
            this.label136 = new System.Windows.Forms.Label();
            this.label137 = new System.Windows.Forms.Label();
            this.V_8_1_1 = new System.Windows.Forms.Label();
            this.V_8_1_2 = new System.Windows.Forms.Label();
            this.V_8_2_2 = new System.Windows.Forms.Label();
            this.V_8_2_1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.label138 = new System.Windows.Forms.Label();
            this.label139 = new System.Windows.Forms.Label();
            this.label140 = new System.Windows.Forms.Label();
            this.label141 = new System.Windows.Forms.Label();
            this.label142 = new System.Windows.Forms.Label();
            this.label143 = new System.Windows.Forms.Label();
            this.label144 = new System.Windows.Forms.Label();
            this.label145 = new System.Windows.Forms.Label();
            this.V_9_a_1 = new System.Windows.Forms.Label();
            this.label146 = new System.Windows.Forms.Label();
            this.label147 = new System.Windows.Forms.Label();
            this.label148 = new System.Windows.Forms.Label();
            this.label149 = new System.Windows.Forms.Label();
            this.label150 = new System.Windows.Forms.Label();
            this.label151 = new System.Windows.Forms.Label();
            this.label152 = new System.Windows.Forms.Label();
            this.V_9_a_2 = new System.Windows.Forms.Label();
            this.V_9_a_2_1 = new System.Windows.Forms.Label();
            this.V_9_a_1_1 = new System.Windows.Forms.Label();
            this.V_9_b_1 = new System.Windows.Forms.Label();
            this.V_9_b_1_1 = new System.Windows.Forms.Label();
            this.V_9_c_1 = new System.Windows.Forms.Label();
            this.V_9_c_1_1 = new System.Windows.Forms.Label();
            this.V_9_c_1_3 = new System.Windows.Forms.Label();
            this.V_9_c_1_2 = new System.Windows.Forms.Label();
            this.V_9_c_1_3_1 = new System.Windows.Forms.Label();
            this.V_9_c_1_3_2 = new System.Windows.Forms.Label();
            this.V_9_c_1_3_3 = new System.Windows.Forms.Label();
            this.V_9_d_1 = new System.Windows.Forms.Label();
            this.V_9_b_2 = new System.Windows.Forms.Label();
            this.V_9_b_2_1 = new System.Windows.Forms.Label();
            this.V_9_c_2 = new System.Windows.Forms.Label();
            this.V_9_c_2_1 = new System.Windows.Forms.Label();
            this.V_9_c_2_2 = new System.Windows.Forms.Label();
            this.V_9_c_2_3 = new System.Windows.Forms.Label();
            this.V_9_c_2_3_1 = new System.Windows.Forms.Label();
            this.V_9_c_2_3_2 = new System.Windows.Forms.Label();
            this.V_9_c_2_3_3 = new System.Windows.Forms.Label();
            this.V_9_d_2 = new System.Windows.Forms.Label();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.label153 = new System.Windows.Forms.Label();
            this.label154 = new System.Windows.Forms.Label();
            this.label155 = new System.Windows.Forms.Label();
            this.label156 = new System.Windows.Forms.Label();
            this.label157 = new System.Windows.Forms.Label();
            this.label158 = new System.Windows.Forms.Label();
            this.label159 = new System.Windows.Forms.Label();
            this.label160 = new System.Windows.Forms.Label();
            this.label161 = new System.Windows.Forms.Label();
            this.label162 = new System.Windows.Forms.Label();
            this.label163 = new System.Windows.Forms.Label();
            this.label164 = new System.Windows.Forms.Label();
            this.label165 = new System.Windows.Forms.Label();
            this.V_10_1_1 = new System.Windows.Forms.Label();
            this.V_10_1_2 = new System.Windows.Forms.Label();
            this.V_10_1_3 = new System.Windows.Forms.Label();
            this.V_10_1_5 = new System.Windows.Forms.Label();
            this.V_10_1_4 = new System.Windows.Forms.Label();
            this.V_10_1_6 = new System.Windows.Forms.Label();
            this.V_10_1_7 = new System.Windows.Forms.Label();
            this.V_10_1_8 = new System.Windows.Forms.Label();
            this.V_10_1_9 = new System.Windows.Forms.Label();
            this.V_10_Sum_1 = new System.Windows.Forms.Label();
            this.V_10_Sum_2 = new System.Windows.Forms.Label();
            this.V_10_2_9 = new System.Windows.Forms.Label();
            this.V_10_2_8 = new System.Windows.Forms.Label();
            this.V_10_2_7 = new System.Windows.Forms.Label();
            this.V_10_2_6 = new System.Windows.Forms.Label();
            this.V_10_2_5 = new System.Windows.Forms.Label();
            this.V_10_2_4 = new System.Windows.Forms.Label();
            this.V_10_2_3 = new System.Windows.Forms.Label();
            this.V_10_2_2 = new System.Windows.Forms.Label();
            this.V_10_2_1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.label166 = new System.Windows.Forms.Label();
            this.label167 = new System.Windows.Forms.Label();
            this.label168 = new System.Windows.Forms.Label();
            this.label169 = new System.Windows.Forms.Label();
            this.label170 = new System.Windows.Forms.Label();
            this.label171 = new System.Windows.Forms.Label();
            this.label172 = new System.Windows.Forms.Label();
            this.label173 = new System.Windows.Forms.Label();
            this.label174 = new System.Windows.Forms.Label();
            this.label175 = new System.Windows.Forms.Label();
            this.label176 = new System.Windows.Forms.Label();
            this.label177 = new System.Windows.Forms.Label();
            this.V_11_a_1 = new System.Windows.Forms.Label();
            this.V_11_a_2 = new System.Windows.Forms.Label();
            this.V_11_a_3 = new System.Windows.Forms.Label();
            this.V_11_a_4 = new System.Windows.Forms.Label();
            this.V_11_a_4_1 = new System.Windows.Forms.Label();
            this.V_11_b_4 = new System.Windows.Forms.Label();
            this.V_11_b_4_1 = new System.Windows.Forms.Label();
            this.V_11_c_4 = new System.Windows.Forms.Label();
            this.V_11_c_4_1 = new System.Windows.Forms.Label();
            this.V_11_Sum_4 = new System.Windows.Forms.Label();
            this.V_11_a_3_1 = new System.Windows.Forms.Label();
            this.V_11_b_3_1 = new System.Windows.Forms.Label();
            this.V_11_b_3 = new System.Windows.Forms.Label();
            this.V_11_c_3 = new System.Windows.Forms.Label();
            this.V_11_c_3_1 = new System.Windows.Forms.Label();
            this.V_11_Sum_3 = new System.Windows.Forms.Label();
            this.V_11_a_2_1 = new System.Windows.Forms.Label();
            this.V_11_b_2 = new System.Windows.Forms.Label();
            this.V_11_b_2_1 = new System.Windows.Forms.Label();
            this.V_11_c_2 = new System.Windows.Forms.Label();
            this.V_11_c_2_1 = new System.Windows.Forms.Label();
            this.V_11_Sum_2 = new System.Windows.Forms.Label();
            this.V_11_a_1_1 = new System.Windows.Forms.Label();
            this.V_11_b_1 = new System.Windows.Forms.Label();
            this.V_11_b_1_1 = new System.Windows.Forms.Label();
            this.V_11_c_1 = new System.Windows.Forms.Label();
            this.V_11_c_1_1 = new System.Windows.Forms.Label();
            this.V_11_Sum_1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.label178 = new System.Windows.Forms.Label();
            this.label179 = new System.Windows.Forms.Label();
            this.label180 = new System.Windows.Forms.Label();
            this.label181 = new System.Windows.Forms.Label();
            this.label182 = new System.Windows.Forms.Label();
            this.label183 = new System.Windows.Forms.Label();
            this.label184 = new System.Windows.Forms.Label();
            this.V_12_1_1 = new System.Windows.Forms.Label();
            this.V_12_2_1 = new System.Windows.Forms.Label();
            this.V_12_2_2 = new System.Windows.Forms.Label();
            this.V_12_1_2 = new System.Windows.Forms.Label();
            this.V_12_1_3 = new System.Windows.Forms.Label();
            this.V_12_2_3 = new System.Windows.Forms.Label();
            this.V_12_Sum_2 = new System.Windows.Forms.Label();
            this.V_12_Sum_1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.label185 = new System.Windows.Forms.Label();
            this.label186 = new System.Windows.Forms.Label();
            this.label187 = new System.Windows.Forms.Label();
            this.label188 = new System.Windows.Forms.Label();
            this.label189 = new System.Windows.Forms.Label();
            this.label190 = new System.Windows.Forms.Label();
            this.label191 = new System.Windows.Forms.Label();
            this.label192 = new System.Windows.Forms.Label();
            this.label193 = new System.Windows.Forms.Label();
            this.label194 = new System.Windows.Forms.Label();
            this.V_13_1_1 = new System.Windows.Forms.Label();
            this.label195 = new System.Windows.Forms.Label();
            this.label196 = new System.Windows.Forms.Label();
            this.label197 = new System.Windows.Forms.Label();
            this.label198 = new System.Windows.Forms.Label();
            this.label199 = new System.Windows.Forms.Label();
            this.label200 = new System.Windows.Forms.Label();
            this.label201 = new System.Windows.Forms.Label();
            this.label202 = new System.Windows.Forms.Label();
            this.label203 = new System.Windows.Forms.Label();
            this.label204 = new System.Windows.Forms.Label();
            this.label205 = new System.Windows.Forms.Label();
            this.label206 = new System.Windows.Forms.Label();
            this.V_13_2_1 = new System.Windows.Forms.Label();
            this.V_13_3_1 = new System.Windows.Forms.Label();
            this.V_13_4_1 = new System.Windows.Forms.Label();
            this.V_13_5_1 = new System.Windows.Forms.Label();
            this.V_13_6_1 = new System.Windows.Forms.Label();
            this.V_13_7_1 = new System.Windows.Forms.Label();
            this.V_13_7_2 = new System.Windows.Forms.Label();
            this.V_13_7_3 = new System.Windows.Forms.Label();
            this.V_13_7_4 = new System.Windows.Forms.Label();
            this.V_13_6_2 = new System.Windows.Forms.Label();
            this.V_13_6_3 = new System.Windows.Forms.Label();
            this.V_13_6_4 = new System.Windows.Forms.Label();
            this.V_13_5_2 = new System.Windows.Forms.Label();
            this.V_13_5_3 = new System.Windows.Forms.Label();
            this.V_13_5_4 = new System.Windows.Forms.Label();
            this.V_13_4_2 = new System.Windows.Forms.Label();
            this.V_13_4_3 = new System.Windows.Forms.Label();
            this.V_13_4_4 = new System.Windows.Forms.Label();
            this.V_13_3_2 = new System.Windows.Forms.Label();
            this.V_13_3_3 = new System.Windows.Forms.Label();
            this.V_13_3_4 = new System.Windows.Forms.Label();
            this.V_13_2_2 = new System.Windows.Forms.Label();
            this.V_13_2_3 = new System.Windows.Forms.Label();
            this.V_13_2_4 = new System.Windows.Forms.Label();
            this.V_13_1_4 = new System.Windows.Forms.Label();
            this.V_13_1_3 = new System.Windows.Forms.Label();
            this.V_13_1_2 = new System.Windows.Forms.Label();
            this.Sec_V_13_1 = new System.Windows.Forms.Label();
            this.label208 = new System.Windows.Forms.Label();
            this.Sec_V_14_1 = new System.Windows.Forms.Label();
            this.Sec_V_14_2 = new System.Windows.Forms.Label();
            this.Sec_V_14_3 = new System.Windows.Forms.Label();
            this.Sec_V_14_4 = new System.Windows.Forms.Label();
            this.Sec_V_14_5 = new System.Windows.Forms.Label();
            this.Sec_V_14_6 = new System.Windows.Forms.Label();
            this.Sec_V_14_7 = new System.Windows.Forms.Label();
            this.Sec_V_14_8 = new System.Windows.Forms.Label();
            this.label217 = new System.Windows.Forms.Label();
            this.Sec_V_15_1 = new System.Windows.Forms.Label();
            this.label219 = new System.Windows.Forms.Label();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.label221 = new System.Windows.Forms.Label();
            this.label222 = new System.Windows.Forms.Label();
            this.label223 = new System.Windows.Forms.Label();
            this.label224 = new System.Windows.Forms.Label();
            this.label225 = new System.Windows.Forms.Label();
            this.label226 = new System.Windows.Forms.Label();
            this.label227 = new System.Windows.Forms.Label();
            this.label228 = new System.Windows.Forms.Label();
            this.label229 = new System.Windows.Forms.Label();
            this.label230 = new System.Windows.Forms.Label();
            this.label231 = new System.Windows.Forms.Label();
            this.label232 = new System.Windows.Forms.Label();
            this.label233 = new System.Windows.Forms.Label();
            this.label234 = new System.Windows.Forms.Label();
            this.label235 = new System.Windows.Forms.Label();
            this.label236 = new System.Windows.Forms.Label();
            this.label237 = new System.Windows.Forms.Label();
            this.label238 = new System.Windows.Forms.Label();
            this.label239 = new System.Windows.Forms.Label();
            this.label240 = new System.Windows.Forms.Label();
            this.label241 = new System.Windows.Forms.Label();
            this.label242 = new System.Windows.Forms.Label();
            this.label243 = new System.Windows.Forms.Label();
            this.label244 = new System.Windows.Forms.Label();
            this.label245 = new System.Windows.Forms.Label();
            this.label246 = new System.Windows.Forms.Label();
            this.label247 = new System.Windows.Forms.Label();
            this.label248 = new System.Windows.Forms.Label();
            this.label249 = new System.Windows.Forms.Label();
            this.label250 = new System.Windows.Forms.Label();
            this.label251 = new System.Windows.Forms.Label();
            this.label252 = new System.Windows.Forms.Label();
            this.label253 = new System.Windows.Forms.Label();
            this.label254 = new System.Windows.Forms.Label();
            this.label255 = new System.Windows.Forms.Label();
            this.label256 = new System.Windows.Forms.Label();
            this.label257 = new System.Windows.Forms.Label();
            this.label258 = new System.Windows.Forms.Label();
            this.label259 = new System.Windows.Forms.Label();
            this.label260 = new System.Windows.Forms.Label();
            this.label261 = new System.Windows.Forms.Label();
            this.label262 = new System.Windows.Forms.Label();
            this.label263 = new System.Windows.Forms.Label();
            this.label264 = new System.Windows.Forms.Label();
            this.label265 = new System.Windows.Forms.Label();
            this.label266 = new System.Windows.Forms.Label();
            this.label267 = new System.Windows.Forms.Label();
            this.label268 = new System.Windows.Forms.Label();
            this.label269 = new System.Windows.Forms.Label();
            this.label270 = new System.Windows.Forms.Label();
            this.label271 = new System.Windows.Forms.Label();
            this.label272 = new System.Windows.Forms.Label();
            this.label273 = new System.Windows.Forms.Label();
            this.label274 = new System.Windows.Forms.Label();
            this.label275 = new System.Windows.Forms.Label();
            this.label276 = new System.Windows.Forms.Label();
            this.label277 = new System.Windows.Forms.Label();
            this.label278 = new System.Windows.Forms.Label();
            this.label279 = new System.Windows.Forms.Label();
            this.label280 = new System.Windows.Forms.Label();
            this.label281 = new System.Windows.Forms.Label();
            this.label282 = new System.Windows.Forms.Label();
            this.label283 = new System.Windows.Forms.Label();
            this.label292 = new System.Windows.Forms.Label();
            this.label293 = new System.Windows.Forms.Label();
            this.label294 = new System.Windows.Forms.Label();
            this.label295 = new System.Windows.Forms.Label();
            this.label296 = new System.Windows.Forms.Label();
            this.label297 = new System.Windows.Forms.Label();
            this.label298 = new System.Windows.Forms.Label();
            this.label299 = new System.Windows.Forms.Label();
            this.label300 = new System.Windows.Forms.Label();
            this.label301 = new System.Windows.Forms.Label();
            this.label302 = new System.Windows.Forms.Label();
            this.label303 = new System.Windows.Forms.Label();
            this.label304 = new System.Windows.Forms.Label();
            this.label305 = new System.Windows.Forms.Label();
            this.label306 = new System.Windows.Forms.Label();
            this.label307 = new System.Windows.Forms.Label();
            this.VI_1_a_2_1 = new System.Windows.Forms.Label();
            this.VI_1_a_1_1 = new System.Windows.Forms.Label();
            this.VI_1_a_1_2 = new System.Windows.Forms.Label();
            this.VI_1_a_2_2 = new System.Windows.Forms.Label();
            this.VI_1_a_2_3 = new System.Windows.Forms.Label();
            this.VI_1_a_1_3 = new System.Windows.Forms.Label();
            this.VI_1_a_1_4 = new System.Windows.Forms.Label();
            this.VI_1_a_2_4 = new System.Windows.Forms.Label();
            this.VI_1_a_Sum_2 = new System.Windows.Forms.Label();
            this.VI_1_a_Sum_1 = new System.Windows.Forms.Label();
            this.VI_1_b_2 = new System.Windows.Forms.Label();
            this.VI_1_b_1 = new System.Windows.Forms.Label();
            this.VI_1_c_1 = new System.Windows.Forms.Label();
            this.VI_1_c_2 = new System.Windows.Forms.Label();
            this.VI_2_1_1 = new System.Windows.Forms.Label();
            this.VI_2_2_1 = new System.Windows.Forms.Label();
            this.VI_2_2_2 = new System.Windows.Forms.Label();
            this.VI_2_1_2 = new System.Windows.Forms.Label();
            this.VI_2_1_3 = new System.Windows.Forms.Label();
            this.VI_2_2_3 = new System.Windows.Forms.Label();
            this.VI_2_Sum_2 = new System.Windows.Forms.Label();
            this.VI_2_Sum_1 = new System.Windows.Forms.Label();
            this.VI_3_1_1 = new System.Windows.Forms.Label();
            this.VI_3_2_1 = new System.Windows.Forms.Label();
            this.VI_3_2_2 = new System.Windows.Forms.Label();
            this.VI_3_1_2 = new System.Windows.Forms.Label();
            this.VI_3_1_4 = new System.Windows.Forms.Label();
            this.VI_3_1_3 = new System.Windows.Forms.Label();
            this.VI_3_2_3 = new System.Windows.Forms.Label();
            this.VI_3_2_4 = new System.Windows.Forms.Label();
            this.VI_3_2_5 = new System.Windows.Forms.Label();
            this.VI_3_1_5 = new System.Windows.Forms.Label();
            this.VI_3_1_6 = new System.Windows.Forms.Label();
            this.VI_3_2_6 = new System.Windows.Forms.Label();
            this.VI_3_Sum_2 = new System.Windows.Forms.Label();
            this.VI_3_Sum_1 = new System.Windows.Forms.Label();
            this.VI_4_1_1 = new System.Windows.Forms.Label();
            this.VI_4_2_1 = new System.Windows.Forms.Label();
            this.VI_4_2_2 = new System.Windows.Forms.Label();
            this.VI_4_1_2 = new System.Windows.Forms.Label();
            this.VI_4_1_3 = new System.Windows.Forms.Label();
            this.VI_4_2_4 = new System.Windows.Forms.Label();
            this.VI_4_2_3 = new System.Windows.Forms.Label();
            this.VI_4_1_4 = new System.Windows.Forms.Label();
            this.VI_4_1_5 = new System.Windows.Forms.Label();
            this.VI_4_2_5 = new System.Windows.Forms.Label();
            this.VI_4_2_6 = new System.Windows.Forms.Label();
            this.VI_4_1_6 = new System.Windows.Forms.Label();
            this.VI_4_Sum_1 = new System.Windows.Forms.Label();
            this.VI_4_Sum_2 = new System.Windows.Forms.Label();
            this.VI_5_2_1 = new System.Windows.Forms.Label();
            this.VI_5_1_1 = new System.Windows.Forms.Label();
            this.VI_5_1_2 = new System.Windows.Forms.Label();
            this.VI_5_2_2 = new System.Windows.Forms.Label();
            this.VI_5_2_3 = new System.Windows.Forms.Label();
            this.VI_5_1_3 = new System.Windows.Forms.Label();
            this.VI_5_1_4 = new System.Windows.Forms.Label();
            this.VI_5_2_4 = new System.Windows.Forms.Label();
            this.VI_5_2_6 = new System.Windows.Forms.Label();
            this.VI_5_2_5 = new System.Windows.Forms.Label();
            this.VI_5_2_7 = new System.Windows.Forms.Label();
            this.VI_5_1_7 = new System.Windows.Forms.Label();
            this.VI_5_1_6 = new System.Windows.Forms.Label();
            this.VI_5_1_5 = new System.Windows.Forms.Label();
            this.VI_6_a_1 = new System.Windows.Forms.Label();
            this.VI_6_a_2 = new System.Windows.Forms.Label();
            this.VI_6_b_2 = new System.Windows.Forms.Label();
            this.VI_6_b_1 = new System.Windows.Forms.Label();
            this.VI_6_c_1 = new System.Windows.Forms.Label();
            this.VI_6_c_2 = new System.Windows.Forms.Label();
            this.VI_6_c_2_2 = new System.Windows.Forms.Label();
            this.VI_6_c_2_1 = new System.Windows.Forms.Label();
            this.VI_6_c_1_1 = new System.Windows.Forms.Label();
            this.VI_6_c_1_2 = new System.Windows.Forms.Label();
            this.VI_7_1_1 = new System.Windows.Forms.Label();
            this.VI_7_2_1 = new System.Windows.Forms.Label();
            this.VI_7_2_2 = new System.Windows.Forms.Label();
            this.VI_7_1_2 = new System.Windows.Forms.Label();
            this.VI_7_1_3 = new System.Windows.Forms.Label();
            this.VI_7_2_3 = new System.Windows.Forms.Label();
            this.VI_7_2_4 = new System.Windows.Forms.Label();
            this.VI_7_1_4 = new System.Windows.Forms.Label();
            this.VI_7_1_5 = new System.Windows.Forms.Label();
            this.VI_7_2_5 = new System.Windows.Forms.Label();
            this.VI_7_Sum_2 = new System.Windows.Forms.Label();
            this.VI_7_Sum_1 = new System.Windows.Forms.Label();
            this.VI_8_1_1 = new System.Windows.Forms.Label();
            this.VI_8_2_1 = new System.Windows.Forms.Label();
            this.VI_8_2_2 = new System.Windows.Forms.Label();
            this.VI_8_1_2 = new System.Windows.Forms.Label();
            this.VI_8_1_3 = new System.Windows.Forms.Label();
            this.VI_8_2_3 = new System.Windows.Forms.Label();
            this.VI_8_2_4 = new System.Windows.Forms.Label();
            this.VI_8_1_4 = new System.Windows.Forms.Label();
            this.VI_8_Sum_1 = new System.Windows.Forms.Label();
            this.VI_8_Sum_2 = new System.Windows.Forms.Label();
            this.VI_9_2_1 = new System.Windows.Forms.Label();
            this.VI_9_1_1 = new System.Windows.Forms.Label();
            this.VI_9_1_2 = new System.Windows.Forms.Label();
            this.VI_9_2_2 = new System.Windows.Forms.Label();
            this.VI_9_2_3 = new System.Windows.Forms.Label();
            this.VI_9_1_3 = new System.Windows.Forms.Label();
            this.label284 = new System.Windows.Forms.Label();
            this.Sec_VII_1 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnPrint = new System.Windows.Forms.Button();
            this.Sec_VIII_5 = new System.Windows.Forms.Label();
            this.Sec_VIII_4 = new System.Windows.Forms.Label();
            this.Sec_VIII_3 = new System.Windows.Forms.Label();
            this.Sec_VIII_2 = new System.Windows.Forms.Label();
            this.Sec_VIII_1 = new System.Windows.Forms.Label();
            this.label220 = new System.Windows.Forms.Label();
            this.label291 = new System.Windows.Forms.Label();
            this.Sec_V_16_1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.V_5_a_1_1 = new System.Windows.Forms.Label();
            this.V_5_a_2_1 = new System.Windows.Forms.Label();
            this.V_5_a_3_1 = new System.Windows.Forms.Label();
            this.V_5_a_4_1 = new System.Windows.Forms.Label();
            this.V_5_a_4_2 = new System.Windows.Forms.Label();
            this.V_5_a_4_3 = new System.Windows.Forms.Label();
            this.V_5_b_4_1 = new System.Windows.Forms.Label();
            this.V_5_b_4_2 = new System.Windows.Forms.Label();
            this.V_5_b_4_3 = new System.Windows.Forms.Label();
            this.V_5_c_4_1 = new System.Windows.Forms.Label();
            this.V_5_c_4_2 = new System.Windows.Forms.Label();
            this.V_5_c_4_3 = new System.Windows.Forms.Label();
            this.V_5_a_3_2 = new System.Windows.Forms.Label();
            this.V_5_a_3_3 = new System.Windows.Forms.Label();
            this.V_5_b_3_1 = new System.Windows.Forms.Label();
            this.V_5_b_3_2 = new System.Windows.Forms.Label();
            this.V_5_b_3_3 = new System.Windows.Forms.Label();
            this.V_5_c_3_1 = new System.Windows.Forms.Label();
            this.V_5_c_3_2 = new System.Windows.Forms.Label();
            this.V_5_c_3_3 = new System.Windows.Forms.Label();
            this.V_5_a_2_2 = new System.Windows.Forms.Label();
            this.V_5_a_2_3 = new System.Windows.Forms.Label();
            this.V_5_b_2_1 = new System.Windows.Forms.Label();
            this.V_5_b_2_2 = new System.Windows.Forms.Label();
            this.V_5_b_2_3 = new System.Windows.Forms.Label();
            this.V_5_c_2_1 = new System.Windows.Forms.Label();
            this.V_5_c_2_2 = new System.Windows.Forms.Label();
            this.V_5_c_2_3 = new System.Windows.Forms.Label();
            this.V_5_a_1_3 = new System.Windows.Forms.Label();
            this.V_5_a_1_2 = new System.Windows.Forms.Label();
            this.V_5_b_1_1 = new System.Windows.Forms.Label();
            this.V_5_b_1_2 = new System.Windows.Forms.Label();
            this.V_5_b_1_3 = new System.Windows.Forms.Label();
            this.V_5_c_1_1 = new System.Windows.Forms.Label();
            this.V_5_c_1_2 = new System.Windows.Forms.Label();
            this.V_5_c_1_3 = new System.Windows.Forms.Label();
            this.Sec_V_4_4 = new System.Windows.Forms.Label();
            this.Sec_V_4_3 = new System.Windows.Forms.Label();
            this.Sec_V_4_2 = new System.Windows.Forms.Label();
            this.Sec_V_4_1 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.V_4_1_1 = new System.Windows.Forms.Label();
            this.V_4_2_1 = new System.Windows.Forms.Label();
            this.V_4_2_2 = new System.Windows.Forms.Label();
            this.V_4_1_2 = new System.Windows.Forms.Label();
            this.V_4_1_3 = new System.Windows.Forms.Label();
            this.V_4_2_3 = new System.Windows.Forms.Label();
            this.V_4_2_4 = new System.Windows.Forms.Label();
            this.V_4_1_4 = new System.Windows.Forms.Label();
            this.V_4_1_5 = new System.Windows.Forms.Label();
            this.V_4_2_5 = new System.Windows.Forms.Label();
            this.V_4_2_6 = new System.Windows.Forms.Label();
            this.V_4_1_6 = new System.Windows.Forms.Label();
            this.V_4_1_7 = new System.Windows.Forms.Label();
            this.V_4_2_7 = new System.Windows.Forms.Label();
            this.V_4_Sum_2 = new System.Windows.Forms.Label();
            this.V_4_Sum_1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.V_3_a_2_1 = new System.Windows.Forms.Label();
            this.V_3_a_1_1 = new System.Windows.Forms.Label();
            this.V_3_b_1_1 = new System.Windows.Forms.Label();
            this.V_3_b_2_1 = new System.Windows.Forms.Label();
            this.V_3_a_1 = new System.Windows.Forms.Label();
            this.V_3_a_2 = new System.Windows.Forms.Label();
            this.V_3_b_2 = new System.Windows.Forms.Label();
            this.V_3_b_1 = new System.Windows.Forms.Label();
            this.V_3_c_1 = new System.Windows.Forms.Label();
            this.V_3_c_2 = new System.Windows.Forms.Label();
            this.V_3_c_2_1 = new System.Windows.Forms.Label();
            this.V_3_c_1_1 = new System.Windows.Forms.Label();
            this.V_3_c_1_2 = new System.Windows.Forms.Label();
            this.V_3_c_2_2 = new System.Windows.Forms.Label();
            this.V_3_c_2_3 = new System.Windows.Forms.Label();
            this.V_3_c_1_3 = new System.Windows.Forms.Label();
            this.V_3_c_1_4 = new System.Windows.Forms.Label();
            this.V_3_c_2_4 = new System.Windows.Forms.Label();
            this.V_3_d_2 = new System.Windows.Forms.Label();
            this.V_3_d_1 = new System.Windows.Forms.Label();
            this.V_3_d_1_1 = new System.Windows.Forms.Label();
            this.V_3_d_2_1 = new System.Windows.Forms.Label();
            this.V_3_d_2_2 = new System.Windows.Forms.Label();
            this.V_3_d_1_2 = new System.Windows.Forms.Label();
            this.V_3_d_1_3 = new System.Windows.Forms.Label();
            this.V_3_d_2_3 = new System.Windows.Forms.Label();
            this.V_3_d_2_4 = new System.Windows.Forms.Label();
            this.V_3_d_1_4 = new System.Windows.Forms.Label();
            this.V_3_e_1 = new System.Windows.Forms.Label();
            this.V_3_e_2 = new System.Windows.Forms.Label();
            this.V_3_Sum_2 = new System.Windows.Forms.Label();
            this.V_3_Sum_1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.V_2_a_2_1 = new System.Windows.Forms.Label();
            this.V_2_a_1_1 = new System.Windows.Forms.Label();
            this.V_2_a_1_2 = new System.Windows.Forms.Label();
            this.V_2_a_2_2 = new System.Windows.Forms.Label();
            this.V_2_a_1_3 = new System.Windows.Forms.Label();
            this.V_2_a_2_3 = new System.Windows.Forms.Label();
            this.V_2_b_1_1 = new System.Windows.Forms.Label();
            this.V_2_b_2_1 = new System.Windows.Forms.Label();
            this.V_2_b_1_2 = new System.Windows.Forms.Label();
            this.V_2_b_2_2 = new System.Windows.Forms.Label();
            this.V_2_c_2_1 = new System.Windows.Forms.Label();
            this.V_2_c_1_1 = new System.Windows.Forms.Label();
            this.V_2_c_2_2 = new System.Windows.Forms.Label();
            this.V_2_Sum_2 = new System.Windows.Forms.Label();
            this.V_2_Sum_1 = new System.Windows.Forms.Label();
            this.V_2_a_1 = new System.Windows.Forms.Label();
            this.V_2_a_2 = new System.Windows.Forms.Label();
            this.V_2_b_2 = new System.Windows.Forms.Label();
            this.V_2_b_1 = new System.Windows.Forms.Label();
            this.V_2_c_1 = new System.Windows.Forms.Label();
            this.V_2_c_2 = new System.Windows.Forms.Label();
            this.V_2_c_1_2 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.V_1_1_1 = new System.Windows.Forms.Label();
            this.V_1_2_1 = new System.Windows.Forms.Label();
            this.V_1_2_2 = new System.Windows.Forms.Label();
            this.V_1_1_2 = new System.Windows.Forms.Label();
            this.V_1_1_3 = new System.Windows.Forms.Label();
            this.V_1_2_3 = new System.Windows.Forms.Label();
            this.V_1_sum_2 = new System.Windows.Forms.Label();
            this.V_1_sum_1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
            this.label35 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.Sec_IV_12 = new System.Windows.Forms.Label();
            this.Sec_IV_11 = new System.Windows.Forms.Label();
            this.Sec_IV_10 = new System.Windows.Forms.Label();
            this.Sec_IV_9 = new System.Windows.Forms.Label();
            this.Sec_IV_8 = new System.Windows.Forms.Label();
            this.Sec_IV_7 = new System.Windows.Forms.Label();
            this.Sec_IV_6 = new System.Windows.Forms.Label();
            this.Sec_IV_5 = new System.Windows.Forms.Label();
            this.Sec_IV_4 = new System.Windows.Forms.Label();
            this.Sec_IV_3 = new System.Windows.Forms.Label();
            this.Sec_IV_2 = new System.Windows.Forms.Label();
            this.Sec_IV_1 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.Sec_III_1 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.Sec_II_2 = new System.Windows.Forms.Label();
            this.Sec_II_1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.Sec_I_6 = new System.Windows.Forms.Label();
            this.Sec_I_5 = new System.Windows.Forms.Label();
            this.Sec_I_4 = new System.Windows.Forms.Label();
            this.Sec_I_3 = new System.Windows.Forms.Label();
            this.Sec_I_2 = new System.Windows.Forms.Label();
            this.Sec_I_1 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.tableLayoutPanel14.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel15.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // Sec_V_5_1
            // 
            this.Sec_V_5_1.AutoSize = true;
            this.Sec_V_5_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_V_5_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Sec_V_5_1.Location = new System.Drawing.Point(3, 2379);
            this.Sec_V_5_1.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_V_5_1.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_V_5_1.Name = "Sec_V_5_1";
            this.Sec_V_5_1.Size = new System.Drawing.Size(394, 13);
            this.Sec_V_5_1.TabIndex = 24;
            this.Sec_V_5_1.Text = "- Giá trị còn lại cuối kỳ của TSCĐ dùng để thế chấp, cầm cố đảm bảo khoản vay:";
            this.Sec_V_5_1.Click += new System.EventHandler(this.label_edit);
            // 
            // Sec_V_5_2
            // 
            this.Sec_V_5_2.AutoSize = true;
            this.Sec_V_5_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_V_5_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Sec_V_5_2.Location = new System.Drawing.Point(3, 2402);
            this.Sec_V_5_2.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_V_5_2.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_V_5_2.Name = "Sec_V_5_2";
            this.Sec_V_5_2.Size = new System.Drawing.Size(346, 13);
            this.Sec_V_5_2.TabIndex = 24;
            this.Sec_V_5_2.Text = "- Nguyên giá TSCĐ cuối năm đã khấu hao hết nhưng vẫn còn sử dụng:";
            this.Sec_V_5_2.Click += new System.EventHandler(this.label_edit);
            // 
            // Sec_V_5_3
            // 
            this.Sec_V_5_3.AutoSize = true;
            this.Sec_V_5_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_V_5_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Sec_V_5_3.Location = new System.Drawing.Point(3, 2425);
            this.Sec_V_5_3.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_V_5_3.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_V_5_3.Name = "Sec_V_5_3";
            this.Sec_V_5_3.Size = new System.Drawing.Size(209, 13);
            this.Sec_V_5_3.TabIndex = 24;
            this.Sec_V_5_3.Text = "- Nguyên giá TSCĐ cuối năm chờ thanh lý:";
            this.Sec_V_5_3.Click += new System.EventHandler(this.label_edit);
            // 
            // Sec_V_5_4
            // 
            this.Sec_V_5_4.AutoSize = true;
            this.Sec_V_5_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_V_5_4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Sec_V_5_4.Location = new System.Drawing.Point(3, 2448);
            this.Sec_V_5_4.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_V_5_4.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_V_5_4.Name = "Sec_V_5_4";
            this.Sec_V_5_4.Size = new System.Drawing.Size(150, 13);
            this.Sec_V_5_4.TabIndex = 24;
            this.Sec_V_5_4.Text = "- Đối với TSCĐ thuê tài chính:";
            this.Sec_V_5_4.Click += new System.EventHandler(this.label_edit);
            // 
            // Sec_V_5_5
            // 
            this.Sec_V_5_5.AutoSize = true;
            this.Sec_V_5_5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_V_5_5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Sec_V_5_5.Location = new System.Drawing.Point(3, 2471);
            this.Sec_V_5_5.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_V_5_5.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_V_5_5.Name = "Sec_V_5_5";
            this.Sec_V_5_5.Size = new System.Drawing.Size(191, 13);
            this.Sec_V_5_5.TabIndex = 24;
            this.Sec_V_5_5.Text = "- Thuyết minh số liệu và giải trình khác:";
            this.Sec_V_5_5.Click += new System.EventHandler(this.label_edit);
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel6.ColumnCount = 5;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 165F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 135F));
            this.tableLayoutPanel6.Controls.Add(this.label108, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.label109, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.label111, 0, 3);
            this.tableLayoutPanel6.Controls.Add(this.label112, 0, 4);
            this.tableLayoutPanel6.Controls.Add(this.label113, 0, 5);
            this.tableLayoutPanel6.Controls.Add(this.label114, 0, 6);
            this.tableLayoutPanel6.Controls.Add(this.label115, 0, 7);
            this.tableLayoutPanel6.Controls.Add(this.label116, 0, 8);
            this.tableLayoutPanel6.Controls.Add(this.label117, 0, 9);
            this.tableLayoutPanel6.Controls.Add(this.label118, 0, 10);
            this.tableLayoutPanel6.Controls.Add(this.label119, 1, 1);
            this.tableLayoutPanel6.Controls.Add(this.label120, 2, 1);
            this.tableLayoutPanel6.Controls.Add(this.label121, 3, 1);
            this.tableLayoutPanel6.Controls.Add(this.label122, 4, 1);
            this.tableLayoutPanel6.Controls.Add(this.label110, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this.V_6_a_1_1, 1, 3);
            this.tableLayoutPanel6.Controls.Add(this.V_6_a_2_1, 2, 3);
            this.tableLayoutPanel6.Controls.Add(this.V_6_a_3_1, 3, 3);
            this.tableLayoutPanel6.Controls.Add(this.V_6_a_4_1, 4, 3);
            this.tableLayoutPanel6.Controls.Add(this.V_6_a_4_2, 4, 4);
            this.tableLayoutPanel6.Controls.Add(this.V_6_a_4_3, 4, 5);
            this.tableLayoutPanel6.Controls.Add(this.V_6_b_4_1, 4, 7);
            this.tableLayoutPanel6.Controls.Add(this.V_6_b_4_2, 4, 8);
            this.tableLayoutPanel6.Controls.Add(this.V_6_b_4_3, 4, 9);
            this.tableLayoutPanel6.Controls.Add(this.V_6_b_4_4, 4, 10);
            this.tableLayoutPanel6.Controls.Add(this.V_6_a_3_2, 3, 4);
            this.tableLayoutPanel6.Controls.Add(this.V_6_a_3_3, 3, 5);
            this.tableLayoutPanel6.Controls.Add(this.V_6_b_3_1, 3, 7);
            this.tableLayoutPanel6.Controls.Add(this.V_6_b_3_2, 3, 8);
            this.tableLayoutPanel6.Controls.Add(this.V_6_b_3_3, 3, 9);
            this.tableLayoutPanel6.Controls.Add(this.V_6_b_3_4, 3, 10);
            this.tableLayoutPanel6.Controls.Add(this.V_6_a_2_2, 2, 4);
            this.tableLayoutPanel6.Controls.Add(this.V_6_a_2_3, 2, 5);
            this.tableLayoutPanel6.Controls.Add(this.V_6_b_2_1, 2, 7);
            this.tableLayoutPanel6.Controls.Add(this.V_6_b_2_2, 2, 8);
            this.tableLayoutPanel6.Controls.Add(this.V_6_b_2_3, 2, 9);
            this.tableLayoutPanel6.Controls.Add(this.V_6_b_2_4, 2, 10);
            this.tableLayoutPanel6.Controls.Add(this.V_6_a_1_2, 1, 4);
            this.tableLayoutPanel6.Controls.Add(this.V_6_a_1_3, 1, 5);
            this.tableLayoutPanel6.Controls.Add(this.V_6_b_1_1, 1, 7);
            this.tableLayoutPanel6.Controls.Add(this.V_6_b_1_2, 1, 8);
            this.tableLayoutPanel6.Controls.Add(this.V_6_b_1_3, 1, 9);
            this.tableLayoutPanel6.Controls.Add(this.V_6_b_1_4, 1, 10);
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 2494);
            this.tableLayoutPanel6.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 11;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 64F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(630, 327);
            this.tableLayoutPanel6.TabIndex = 25;
            // 
            // label108
            // 
            this.label108.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label108.AutoSize = true;
            this.tableLayoutPanel6.SetColumnSpan(this.label108, 5);
            this.label108.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label108.Location = new System.Drawing.Point(1, 1);
            this.label108.Margin = new System.Windows.Forms.Padding(0, 0, 25, 0);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(624, 25);
            this.label108.TabIndex = 0;
            this.label108.Text = "06. Tăng, giảm Bất động sản đầu tư (chi tiết từng loại tài sản theo yêu cầu quản " +
    "lý của DN)";
            this.label108.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label109
            // 
            this.label109.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label109.AutoSize = true;
            this.label109.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label109.Location = new System.Drawing.Point(4, 27);
            this.label109.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(70, 25);
            this.label109.TabIndex = 0;
            this.label109.Text = "Khoản mục";
            this.label109.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label111
            // 
            this.label111.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(4, 79);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(159, 25);
            this.label111.TabIndex = 0;
            this.label111.Text = "Nguyên giá";
            this.label111.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label112
            // 
            this.label112.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(4, 105);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(159, 25);
            this.label112.TabIndex = 0;
            this.label112.Text = "Giá trị hao mòn luỹ kế";
            this.label112.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label113
            // 
            this.label113.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(4, 131);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(159, 25);
            this.label113.TabIndex = 0;
            this.label113.Text = "Giá trị còn lại";
            this.label113.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label114
            // 
            this.label114.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label114.AutoSize = true;
            this.tableLayoutPanel6.SetColumnSpan(this.label114, 5);
            this.label114.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label114.Location = new System.Drawing.Point(4, 157);
            this.label114.Margin = new System.Windows.Forms.Padding(3, 0, 25, 0);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(621, 25);
            this.label114.TabIndex = 0;
            this.label114.Text = "B. Bất động sản đầu tư nắm giữ chờ tăng giá";
            this.label114.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label115
            // 
            this.label115.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(4, 183);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(159, 25);
            this.label115.TabIndex = 0;
            this.label115.Text = "Nguyên giá";
            this.label115.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label116
            // 
            this.label116.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(4, 209);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(159, 64);
            this.label116.TabIndex = 0;
            this.label116.Text = "Giá trị hao mòn luỹ kế của BĐS đầu tư cho thuê/ TSCĐ chuyển sang BĐS đầu tư nắm g" +
    "iữ chờ tăng giá";
            this.label116.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label117
            // 
            this.label117.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label117.AutoSize = true;
            this.label117.Location = new System.Drawing.Point(4, 274);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(159, 25);
            this.label117.TabIndex = 0;
            this.label117.Text = "Tổn thất do suy giảm giá trị\t\t";
            this.label117.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label118
            // 
            this.label118.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(4, 300);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(159, 26);
            this.label118.TabIndex = 0;
            this.label118.Text = "Giá trị còn lại";
            this.label118.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label119
            // 
            this.label119.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label119.AutoSize = true;
            this.label119.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label119.Location = new System.Drawing.Point(170, 27);
            this.label119.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(93, 25);
            this.label119.TabIndex = 0;
            this.label119.Text = "Số dư đầu năm";
            this.label119.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label120
            // 
            this.label120.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label120.AutoSize = true;
            this.label120.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label120.Location = new System.Drawing.Point(286, 27);
            this.label120.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(96, 25);
            this.label120.TabIndex = 0;
            this.label120.Text = "Tăng trong năm";
            this.label120.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label121
            // 
            this.label121.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label121.AutoSize = true;
            this.label121.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label121.Location = new System.Drawing.Point(402, 27);
            this.label121.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(95, 25);
            this.label121.TabIndex = 0;
            this.label121.Text = "Giảm trong năm";
            this.label121.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label122
            // 
            this.label122.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label122.AutoSize = true;
            this.label122.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label122.Location = new System.Drawing.Point(518, 27);
            this.label122.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(95, 25);
            this.label122.TabIndex = 0;
            this.label122.Text = "Số dư cuối năm";
            this.label122.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label110
            // 
            this.label110.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label110.AutoSize = true;
            this.tableLayoutPanel6.SetColumnSpan(this.label110, 5);
            this.label110.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label110.Location = new System.Drawing.Point(1, 53);
            this.label110.Margin = new System.Windows.Forms.Padding(0, 0, 25, 0);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(624, 25);
            this.label110.TabIndex = 0;
            this.label110.Text = "A. Bất động sản đầu tư cho thuê";
            this.label110.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // V_6_a_1_1
            // 
            this.V_6_a_1_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_6_a_1_1.AutoSize = true;
            this.V_6_a_1_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_6_a_1_1.Location = new System.Drawing.Point(170, 79);
            this.V_6_a_1_1.Name = "V_6_a_1_1";
            this.V_6_a_1_1.Size = new System.Drawing.Size(109, 25);
            this.V_6_a_1_1.TabIndex = 240;
            this.V_6_a_1_1.Text = "0";
            this.V_6_a_1_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_6_a_1_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_6_a_2_1
            // 
            this.V_6_a_2_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_6_a_2_1.AutoSize = true;
            this.V_6_a_2_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_6_a_2_1.Location = new System.Drawing.Point(286, 79);
            this.V_6_a_2_1.Name = "V_6_a_2_1";
            this.V_6_a_2_1.Size = new System.Drawing.Size(109, 25);
            this.V_6_a_2_1.TabIndex = 241;
            this.V_6_a_2_1.Text = "0";
            this.V_6_a_2_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_6_a_2_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_6_a_3_1
            // 
            this.V_6_a_3_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_6_a_3_1.AutoSize = true;
            this.V_6_a_3_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_6_a_3_1.Location = new System.Drawing.Point(402, 79);
            this.V_6_a_3_1.Name = "V_6_a_3_1";
            this.V_6_a_3_1.Size = new System.Drawing.Size(109, 25);
            this.V_6_a_3_1.TabIndex = 242;
            this.V_6_a_3_1.Text = "0";
            this.V_6_a_3_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_6_a_3_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_6_a_4_1
            // 
            this.V_6_a_4_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_6_a_4_1.AutoSize = true;
            this.V_6_a_4_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_6_a_4_1.Location = new System.Drawing.Point(518, 79);
            this.V_6_a_4_1.Margin = new System.Windows.Forms.Padding(3, 0, 25, 0);
            this.V_6_a_4_1.Name = "V_6_a_4_1";
            this.V_6_a_4_1.Size = new System.Drawing.Size(107, 25);
            this.V_6_a_4_1.TabIndex = 243;
            this.V_6_a_4_1.Text = "0";
            this.V_6_a_4_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_6_a_4_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_6_a_4_2
            // 
            this.V_6_a_4_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_6_a_4_2.AutoSize = true;
            this.V_6_a_4_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_6_a_4_2.Location = new System.Drawing.Point(518, 105);
            this.V_6_a_4_2.Margin = new System.Windows.Forms.Padding(3, 0, 25, 0);
            this.V_6_a_4_2.Name = "V_6_a_4_2";
            this.V_6_a_4_2.Size = new System.Drawing.Size(107, 25);
            this.V_6_a_4_2.TabIndex = 247;
            this.V_6_a_4_2.Text = "0";
            this.V_6_a_4_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_6_a_4_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_6_a_4_3
            // 
            this.V_6_a_4_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_6_a_4_3.AutoSize = true;
            this.V_6_a_4_3.Location = new System.Drawing.Point(518, 131);
            this.V_6_a_4_3.Margin = new System.Windows.Forms.Padding(3, 0, 25, 0);
            this.V_6_a_4_3.Name = "V_6_a_4_3";
            this.V_6_a_4_3.Size = new System.Drawing.Size(107, 25);
            this.V_6_a_4_3.TabIndex = 251;
            this.V_6_a_4_3.Text = "0";
            this.V_6_a_4_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_6_b_4_1
            // 
            this.V_6_b_4_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_6_b_4_1.AutoSize = true;
            this.V_6_b_4_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_6_b_4_1.Location = new System.Drawing.Point(518, 183);
            this.V_6_b_4_1.Margin = new System.Windows.Forms.Padding(3, 0, 25, 0);
            this.V_6_b_4_1.Name = "V_6_b_4_1";
            this.V_6_b_4_1.Size = new System.Drawing.Size(107, 25);
            this.V_6_b_4_1.TabIndex = 255;
            this.V_6_b_4_1.Text = "0";
            this.V_6_b_4_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_6_b_4_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_6_b_4_2
            // 
            this.V_6_b_4_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_6_b_4_2.AutoSize = true;
            this.V_6_b_4_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_6_b_4_2.Location = new System.Drawing.Point(518, 209);
            this.V_6_b_4_2.Margin = new System.Windows.Forms.Padding(3, 0, 25, 0);
            this.V_6_b_4_2.Name = "V_6_b_4_2";
            this.V_6_b_4_2.Size = new System.Drawing.Size(107, 64);
            this.V_6_b_4_2.TabIndex = 259;
            this.V_6_b_4_2.Text = "0";
            this.V_6_b_4_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_6_b_4_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_6_b_4_3
            // 
            this.V_6_b_4_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_6_b_4_3.AutoSize = true;
            this.V_6_b_4_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_6_b_4_3.Location = new System.Drawing.Point(518, 274);
            this.V_6_b_4_3.Margin = new System.Windows.Forms.Padding(3, 0, 25, 0);
            this.V_6_b_4_3.Name = "V_6_b_4_3";
            this.V_6_b_4_3.Size = new System.Drawing.Size(107, 25);
            this.V_6_b_4_3.TabIndex = 263;
            this.V_6_b_4_3.Text = "0";
            this.V_6_b_4_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_6_b_4_3.Click += new System.EventHandler(this.number_edit);
            // 
            // V_6_b_4_4
            // 
            this.V_6_b_4_4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_6_b_4_4.AutoSize = true;
            this.V_6_b_4_4.Location = new System.Drawing.Point(518, 300);
            this.V_6_b_4_4.Margin = new System.Windows.Forms.Padding(3, 0, 25, 0);
            this.V_6_b_4_4.Name = "V_6_b_4_4";
            this.V_6_b_4_4.Size = new System.Drawing.Size(107, 26);
            this.V_6_b_4_4.TabIndex = 267;
            this.V_6_b_4_4.Text = "0";
            this.V_6_b_4_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_6_b_4_4.Click += new System.EventHandler(this.number_edit);
            // 
            // V_6_a_3_2
            // 
            this.V_6_a_3_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_6_a_3_2.AutoSize = true;
            this.V_6_a_3_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_6_a_3_2.Location = new System.Drawing.Point(402, 105);
            this.V_6_a_3_2.Name = "V_6_a_3_2";
            this.V_6_a_3_2.Size = new System.Drawing.Size(109, 25);
            this.V_6_a_3_2.TabIndex = 246;
            this.V_6_a_3_2.Text = "0";
            this.V_6_a_3_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_6_a_3_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_6_a_3_3
            // 
            this.V_6_a_3_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_6_a_3_3.AutoSize = true;
            this.V_6_a_3_3.Location = new System.Drawing.Point(402, 131);
            this.V_6_a_3_3.Name = "V_6_a_3_3";
            this.V_6_a_3_3.Size = new System.Drawing.Size(109, 25);
            this.V_6_a_3_3.TabIndex = 250;
            this.V_6_a_3_3.Text = "0";
            this.V_6_a_3_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_6_b_3_1
            // 
            this.V_6_b_3_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_6_b_3_1.AutoSize = true;
            this.V_6_b_3_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_6_b_3_1.Location = new System.Drawing.Point(402, 183);
            this.V_6_b_3_1.Name = "V_6_b_3_1";
            this.V_6_b_3_1.Size = new System.Drawing.Size(109, 25);
            this.V_6_b_3_1.TabIndex = 254;
            this.V_6_b_3_1.Text = "0";
            this.V_6_b_3_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_6_b_3_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_6_b_3_2
            // 
            this.V_6_b_3_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_6_b_3_2.AutoSize = true;
            this.V_6_b_3_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_6_b_3_2.Location = new System.Drawing.Point(402, 209);
            this.V_6_b_3_2.Name = "V_6_b_3_2";
            this.V_6_b_3_2.Size = new System.Drawing.Size(109, 64);
            this.V_6_b_3_2.TabIndex = 258;
            this.V_6_b_3_2.Text = "0";
            this.V_6_b_3_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_6_b_3_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_6_b_3_3
            // 
            this.V_6_b_3_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_6_b_3_3.AutoSize = true;
            this.V_6_b_3_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_6_b_3_3.Location = new System.Drawing.Point(402, 274);
            this.V_6_b_3_3.Name = "V_6_b_3_3";
            this.V_6_b_3_3.Size = new System.Drawing.Size(109, 25);
            this.V_6_b_3_3.TabIndex = 262;
            this.V_6_b_3_3.Text = "0";
            this.V_6_b_3_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_6_b_3_3.Click += new System.EventHandler(this.number_edit);
            // 
            // V_6_b_3_4
            // 
            this.V_6_b_3_4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_6_b_3_4.AutoSize = true;
            this.V_6_b_3_4.Location = new System.Drawing.Point(402, 300);
            this.V_6_b_3_4.Name = "V_6_b_3_4";
            this.V_6_b_3_4.Size = new System.Drawing.Size(109, 26);
            this.V_6_b_3_4.TabIndex = 266;
            this.V_6_b_3_4.Text = "0";
            this.V_6_b_3_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_6_b_3_4.Click += new System.EventHandler(this.number_edit);
            // 
            // V_6_a_2_2
            // 
            this.V_6_a_2_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_6_a_2_2.AutoSize = true;
            this.V_6_a_2_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_6_a_2_2.Location = new System.Drawing.Point(286, 105);
            this.V_6_a_2_2.Name = "V_6_a_2_2";
            this.V_6_a_2_2.Size = new System.Drawing.Size(109, 25);
            this.V_6_a_2_2.TabIndex = 245;
            this.V_6_a_2_2.Text = "0";
            this.V_6_a_2_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_6_a_2_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_6_a_2_3
            // 
            this.V_6_a_2_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_6_a_2_3.AutoSize = true;
            this.V_6_a_2_3.Location = new System.Drawing.Point(286, 131);
            this.V_6_a_2_3.Name = "V_6_a_2_3";
            this.V_6_a_2_3.Size = new System.Drawing.Size(109, 25);
            this.V_6_a_2_3.TabIndex = 249;
            this.V_6_a_2_3.Text = "0";
            this.V_6_a_2_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_6_b_2_1
            // 
            this.V_6_b_2_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_6_b_2_1.AutoSize = true;
            this.V_6_b_2_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_6_b_2_1.Location = new System.Drawing.Point(286, 183);
            this.V_6_b_2_1.Name = "V_6_b_2_1";
            this.V_6_b_2_1.Size = new System.Drawing.Size(109, 25);
            this.V_6_b_2_1.TabIndex = 253;
            this.V_6_b_2_1.Text = "0";
            this.V_6_b_2_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_6_b_2_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_6_b_2_2
            // 
            this.V_6_b_2_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_6_b_2_2.AutoSize = true;
            this.V_6_b_2_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_6_b_2_2.Location = new System.Drawing.Point(286, 209);
            this.V_6_b_2_2.Name = "V_6_b_2_2";
            this.V_6_b_2_2.Size = new System.Drawing.Size(109, 64);
            this.V_6_b_2_2.TabIndex = 257;
            this.V_6_b_2_2.Text = "0";
            this.V_6_b_2_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_6_b_2_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_6_b_2_3
            // 
            this.V_6_b_2_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_6_b_2_3.AutoSize = true;
            this.V_6_b_2_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_6_b_2_3.Location = new System.Drawing.Point(286, 274);
            this.V_6_b_2_3.Name = "V_6_b_2_3";
            this.V_6_b_2_3.Size = new System.Drawing.Size(109, 25);
            this.V_6_b_2_3.TabIndex = 261;
            this.V_6_b_2_3.Text = "0";
            this.V_6_b_2_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_6_b_2_3.Click += new System.EventHandler(this.number_edit);
            // 
            // V_6_b_2_4
            // 
            this.V_6_b_2_4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_6_b_2_4.AutoSize = true;
            this.V_6_b_2_4.Location = new System.Drawing.Point(286, 300);
            this.V_6_b_2_4.Name = "V_6_b_2_4";
            this.V_6_b_2_4.Size = new System.Drawing.Size(109, 26);
            this.V_6_b_2_4.TabIndex = 265;
            this.V_6_b_2_4.Text = "0";
            this.V_6_b_2_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_6_b_2_4.Click += new System.EventHandler(this.number_edit);
            // 
            // V_6_a_1_2
            // 
            this.V_6_a_1_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_6_a_1_2.AutoSize = true;
            this.V_6_a_1_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_6_a_1_2.Location = new System.Drawing.Point(170, 105);
            this.V_6_a_1_2.Name = "V_6_a_1_2";
            this.V_6_a_1_2.Size = new System.Drawing.Size(109, 25);
            this.V_6_a_1_2.TabIndex = 244;
            this.V_6_a_1_2.Text = "0";
            this.V_6_a_1_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_6_a_1_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_6_a_1_3
            // 
            this.V_6_a_1_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_6_a_1_3.AutoSize = true;
            this.V_6_a_1_3.Location = new System.Drawing.Point(170, 131);
            this.V_6_a_1_3.Name = "V_6_a_1_3";
            this.V_6_a_1_3.Size = new System.Drawing.Size(109, 25);
            this.V_6_a_1_3.TabIndex = 248;
            this.V_6_a_1_3.Text = "0";
            this.V_6_a_1_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_6_b_1_1
            // 
            this.V_6_b_1_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_6_b_1_1.AutoSize = true;
            this.V_6_b_1_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_6_b_1_1.Location = new System.Drawing.Point(170, 183);
            this.V_6_b_1_1.Name = "V_6_b_1_1";
            this.V_6_b_1_1.Size = new System.Drawing.Size(109, 25);
            this.V_6_b_1_1.TabIndex = 252;
            this.V_6_b_1_1.Text = "0";
            this.V_6_b_1_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_6_b_1_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_6_b_1_2
            // 
            this.V_6_b_1_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_6_b_1_2.AutoSize = true;
            this.V_6_b_1_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_6_b_1_2.Location = new System.Drawing.Point(170, 209);
            this.V_6_b_1_2.Name = "V_6_b_1_2";
            this.V_6_b_1_2.Size = new System.Drawing.Size(109, 64);
            this.V_6_b_1_2.TabIndex = 256;
            this.V_6_b_1_2.Text = "0";
            this.V_6_b_1_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_6_b_1_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_6_b_1_3
            // 
            this.V_6_b_1_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_6_b_1_3.AutoSize = true;
            this.V_6_b_1_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_6_b_1_3.Location = new System.Drawing.Point(170, 274);
            this.V_6_b_1_3.Name = "V_6_b_1_3";
            this.V_6_b_1_3.Size = new System.Drawing.Size(109, 25);
            this.V_6_b_1_3.TabIndex = 260;
            this.V_6_b_1_3.Text = "0";
            this.V_6_b_1_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_6_b_1_3.Click += new System.EventHandler(this.number_edit);
            // 
            // V_6_b_1_4
            // 
            this.V_6_b_1_4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_6_b_1_4.AutoSize = true;
            this.V_6_b_1_4.Location = new System.Drawing.Point(170, 300);
            this.V_6_b_1_4.Name = "V_6_b_1_4";
            this.V_6_b_1_4.Size = new System.Drawing.Size(109, 26);
            this.V_6_b_1_4.TabIndex = 264;
            this.V_6_b_1_4.Text = "0";
            this.V_6_b_1_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_6_b_1_4.Click += new System.EventHandler(this.number_edit);
            // 
            // Sec_V_6_1
            // 
            this.Sec_V_6_1.AutoSize = true;
            this.Sec_V_6_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_V_6_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Sec_V_6_1.Location = new System.Drawing.Point(3, 2834);
            this.Sec_V_6_1.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_V_6_1.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_V_6_1.Name = "Sec_V_6_1";
            this.Sec_V_6_1.Size = new System.Drawing.Size(424, 13);
            this.Sec_V_6_1.TabIndex = 26;
            this.Sec_V_6_1.Text = "- Giá trị còn lại cuối kỳ của BĐS đầu tư dùng để thế chấp, cầm cố đảm bảo khoản v" +
    "ay: ";
            this.Sec_V_6_1.Click += new System.EventHandler(this.label_edit);
            // 
            // Sec_V_6_2
            // 
            this.Sec_V_6_2.AutoSize = true;
            this.Sec_V_6_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_V_6_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Sec_V_6_2.Location = new System.Drawing.Point(3, 2857);
            this.Sec_V_6_2.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_V_6_2.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_V_6_2.Name = "Sec_V_6_2";
            this.Sec_V_6_2.Size = new System.Drawing.Size(488, 13);
            this.Sec_V_6_2.TabIndex = 26;
            this.Sec_V_6_2.Text = "- Nguyên giá BĐS đầu tư cuối năm đã khấu hao hết nhưng vẫn cho thuê hoặc nắm giữ " +
    "chờ tăng giá: ";
            this.Sec_V_6_2.Click += new System.EventHandler(this.label_edit);
            // 
            // Sec_V_6_3
            // 
            this.Sec_V_6_3.AutoSize = true;
            this.Sec_V_6_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_V_6_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Sec_V_6_3.Location = new System.Drawing.Point(3, 2880);
            this.Sec_V_6_3.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_V_6_3.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_V_6_3.Name = "Sec_V_6_3";
            this.Sec_V_6_3.Size = new System.Drawing.Size(194, 13);
            this.Sec_V_6_3.TabIndex = 26;
            this.Sec_V_6_3.Text = "- Thuyết minh số liệu và giải trình khác: ";
            this.Sec_V_6_3.Click += new System.EventHandler(this.label_edit);
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel7.ColumnCount = 3;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 313F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Controls.Add(this.label126, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.label127, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.label128, 2, 0);
            this.tableLayoutPanel7.Controls.Add(this.label129, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.label130, 0, 2);
            this.tableLayoutPanel7.Controls.Add(this.label131, 0, 3);
            this.tableLayoutPanel7.Controls.Add(this.label132, 0, 4);
            this.tableLayoutPanel7.Controls.Add(this.V_7_1_1, 1, 1);
            this.tableLayoutPanel7.Controls.Add(this.V_7_1_2, 1, 2);
            this.tableLayoutPanel7.Controls.Add(this.V_7_Sum_1, 1, 4);
            this.tableLayoutPanel7.Controls.Add(this.V_7_2_1, 2, 1);
            this.tableLayoutPanel7.Controls.Add(this.V_7_2_2, 2, 2);
            this.tableLayoutPanel7.Controls.Add(this.V_7_2_3, 2, 3);
            this.tableLayoutPanel7.Controls.Add(this.V_7_Sum_2, 2, 4);
            this.tableLayoutPanel7.Controls.Add(this.V_7_1_3, 1, 3);
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 2903);
            this.tableLayoutPanel7.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 5;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(630, 131);
            this.tableLayoutPanel7.TabIndex = 27;
            // 
            // label126
            // 
            this.label126.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label126.AutoSize = true;
            this.label126.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label126.Location = new System.Drawing.Point(4, 1);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(307, 25);
            this.label126.TabIndex = 0;
            this.label126.Text = "07. Xây dựng cơ bản dở dang";
            this.label126.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label127
            // 
            this.label127.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label127.AutoSize = true;
            this.label127.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label127.Location = new System.Drawing.Point(318, 1);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(150, 25);
            this.label127.TabIndex = 0;
            this.label127.Text = "Cuối năm\t";
            this.label127.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label128
            // 
            this.label128.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label128.AutoSize = true;
            this.label128.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label128.Location = new System.Drawing.Point(475, 1);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(151, 25);
            this.label128.TabIndex = 0;
            this.label128.Text = "Đầu năm";
            this.label128.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label129
            // 
            this.label129.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label129.AutoSize = true;
            this.label129.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label129.Location = new System.Drawing.Point(4, 27);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(307, 25);
            this.label129.TabIndex = 0;
            this.label129.Text = "- Mua sắm";
            this.label129.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label130
            // 
            this.label130.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label130.AutoSize = true;
            this.label130.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label130.Location = new System.Drawing.Point(4, 53);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(307, 25);
            this.label130.TabIndex = 0;
            this.label130.Text = "- XDCB";
            this.label130.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label131
            // 
            this.label131.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label131.AutoSize = true;
            this.label131.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label131.Location = new System.Drawing.Point(4, 79);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(307, 25);
            this.label131.TabIndex = 0;
            this.label131.Text = "- Sửa chữa lớn TSCĐ";
            this.label131.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label132
            // 
            this.label132.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label132.AutoSize = true;
            this.label132.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label132.Location = new System.Drawing.Point(51, 105);
            this.label132.Margin = new System.Windows.Forms.Padding(50, 0, 3, 0);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(260, 25);
            this.label132.TabIndex = 0;
            this.label132.Text = "Cộng";
            this.label132.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // V_7_1_1
            // 
            this.V_7_1_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_7_1_1.AutoSize = true;
            this.V_7_1_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_7_1_1.Location = new System.Drawing.Point(318, 27);
            this.V_7_1_1.Name = "V_7_1_1";
            this.V_7_1_1.Size = new System.Drawing.Size(150, 25);
            this.V_7_1_1.TabIndex = 268;
            this.V_7_1_1.Text = "0";
            this.V_7_1_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_7_1_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_7_1_2
            // 
            this.V_7_1_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_7_1_2.AutoSize = true;
            this.V_7_1_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_7_1_2.Location = new System.Drawing.Point(318, 53);
            this.V_7_1_2.Name = "V_7_1_2";
            this.V_7_1_2.Size = new System.Drawing.Size(150, 25);
            this.V_7_1_2.TabIndex = 270;
            this.V_7_1_2.Text = "0";
            this.V_7_1_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_7_1_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_7_Sum_1
            // 
            this.V_7_Sum_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_7_Sum_1.AutoSize = true;
            this.V_7_Sum_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.V_7_Sum_1.Location = new System.Drawing.Point(318, 105);
            this.V_7_Sum_1.Name = "V_7_Sum_1";
            this.V_7_Sum_1.Size = new System.Drawing.Size(150, 25);
            this.V_7_Sum_1.TabIndex = 274;
            this.V_7_Sum_1.Text = "0";
            this.V_7_Sum_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_7_2_1
            // 
            this.V_7_2_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_7_2_1.AutoSize = true;
            this.V_7_2_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_7_2_1.Location = new System.Drawing.Point(475, 27);
            this.V_7_2_1.Name = "V_7_2_1";
            this.V_7_2_1.Size = new System.Drawing.Size(151, 25);
            this.V_7_2_1.TabIndex = 269;
            this.V_7_2_1.Text = "0";
            this.V_7_2_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_7_2_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_7_2_2
            // 
            this.V_7_2_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_7_2_2.AutoSize = true;
            this.V_7_2_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_7_2_2.Location = new System.Drawing.Point(475, 53);
            this.V_7_2_2.Name = "V_7_2_2";
            this.V_7_2_2.Size = new System.Drawing.Size(151, 25);
            this.V_7_2_2.TabIndex = 271;
            this.V_7_2_2.Text = "0";
            this.V_7_2_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_7_2_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_7_2_3
            // 
            this.V_7_2_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_7_2_3.AutoSize = true;
            this.V_7_2_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_7_2_3.Location = new System.Drawing.Point(475, 79);
            this.V_7_2_3.Name = "V_7_2_3";
            this.V_7_2_3.Size = new System.Drawing.Size(151, 25);
            this.V_7_2_3.TabIndex = 273;
            this.V_7_2_3.Text = "0";
            this.V_7_2_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_7_2_3.Click += new System.EventHandler(this.number_edit);
            // 
            // V_7_Sum_2
            // 
            this.V_7_Sum_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_7_Sum_2.AutoSize = true;
            this.V_7_Sum_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.V_7_Sum_2.Location = new System.Drawing.Point(475, 105);
            this.V_7_Sum_2.Name = "V_7_Sum_2";
            this.V_7_Sum_2.Size = new System.Drawing.Size(151, 25);
            this.V_7_Sum_2.TabIndex = 275;
            this.V_7_Sum_2.Text = "0";
            this.V_7_Sum_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_7_1_3
            // 
            this.V_7_1_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_7_1_3.AutoSize = true;
            this.V_7_1_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_7_1_3.Location = new System.Drawing.Point(318, 79);
            this.V_7_1_3.Name = "V_7_1_3";
            this.V_7_1_3.Size = new System.Drawing.Size(150, 25);
            this.V_7_1_3.TabIndex = 272;
            this.V_7_1_3.Text = "0";
            this.V_7_1_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_7_1_3.Click += new System.EventHandler(this.number_edit);
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel8.ColumnCount = 3;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 313F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Controls.Add(this.label133, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.label134, 1, 0);
            this.tableLayoutPanel8.Controls.Add(this.label135, 2, 0);
            this.tableLayoutPanel8.Controls.Add(this.label136, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.label137, 0, 2);
            this.tableLayoutPanel8.Controls.Add(this.V_8_1_1, 1, 1);
            this.tableLayoutPanel8.Controls.Add(this.V_8_1_2, 2, 1);
            this.tableLayoutPanel8.Controls.Add(this.V_8_2_2, 2, 2);
            this.tableLayoutPanel8.Controls.Add(this.V_8_2_1, 1, 2);
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 3054);
            this.tableLayoutPanel8.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 3;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 14F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(630, 93);
            this.tableLayoutPanel8.TabIndex = 28;
            // 
            // label133
            // 
            this.label133.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label133.AutoSize = true;
            this.label133.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label133.Location = new System.Drawing.Point(4, 1);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(307, 25);
            this.label133.TabIndex = 0;
            this.label133.Text = "08. Chi phí trả trước";
            this.label133.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label134
            // 
            this.label134.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label134.AutoSize = true;
            this.label134.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label134.Location = new System.Drawing.Point(318, 1);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(150, 25);
            this.label134.TabIndex = 0;
            this.label134.Text = "Cuối năm";
            this.label134.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label135
            // 
            this.label135.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label135.AutoSize = true;
            this.label135.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label135.Location = new System.Drawing.Point(475, 1);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(151, 25);
            this.label135.TabIndex = 0;
            this.label135.Text = "Đầu năm";
            this.label135.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label136
            // 
            this.label136.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label136.AutoSize = true;
            this.label136.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label136.Location = new System.Drawing.Point(4, 27);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(307, 36);
            this.label136.TabIndex = 0;
            this.label136.Text = "- Chi phí trả trước  (ngắn hạn, dài hạn theo yêu cầu quản lý của DN)";
            this.label136.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label137
            // 
            this.label137.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label137.AutoSize = true;
            this.label137.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label137.Location = new System.Drawing.Point(4, 64);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(307, 28);
            this.label137.TabIndex = 0;
            this.label137.Text = "- Các khoản phải thu Nhà Nước";
            this.label137.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // V_8_1_1
            // 
            this.V_8_1_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_8_1_1.AutoSize = true;
            this.V_8_1_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_8_1_1.Location = new System.Drawing.Point(318, 27);
            this.V_8_1_1.Name = "V_8_1_1";
            this.V_8_1_1.Size = new System.Drawing.Size(150, 36);
            this.V_8_1_1.TabIndex = 276;
            this.V_8_1_1.Text = "0";
            this.V_8_1_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_8_1_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_8_1_2
            // 
            this.V_8_1_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_8_1_2.AutoSize = true;
            this.V_8_1_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_8_1_2.Location = new System.Drawing.Point(475, 27);
            this.V_8_1_2.Name = "V_8_1_2";
            this.V_8_1_2.Size = new System.Drawing.Size(151, 36);
            this.V_8_1_2.TabIndex = 277;
            this.V_8_1_2.Text = "0";
            this.V_8_1_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_8_1_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_8_2_2
            // 
            this.V_8_2_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_8_2_2.AutoSize = true;
            this.V_8_2_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_8_2_2.Location = new System.Drawing.Point(475, 64);
            this.V_8_2_2.Name = "V_8_2_2";
            this.V_8_2_2.Size = new System.Drawing.Size(151, 28);
            this.V_8_2_2.TabIndex = 279;
            this.V_8_2_2.Text = "0";
            this.V_8_2_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_8_2_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_8_2_1
            // 
            this.V_8_2_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_8_2_1.AutoSize = true;
            this.V_8_2_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_8_2_1.Location = new System.Drawing.Point(318, 64);
            this.V_8_2_1.Name = "V_8_2_1";
            this.V_8_2_1.Size = new System.Drawing.Size(150, 28);
            this.V_8_2_1.TabIndex = 278;
            this.V_8_2_1.Text = "0";
            this.V_8_2_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_8_2_1.Click += new System.EventHandler(this.number_edit);
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel9.ColumnCount = 3;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 313F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Controls.Add(this.label138, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.label139, 1, 0);
            this.tableLayoutPanel9.Controls.Add(this.label140, 2, 0);
            this.tableLayoutPanel9.Controls.Add(this.label141, 0, 1);
            this.tableLayoutPanel9.Controls.Add(this.label142, 0, 2);
            this.tableLayoutPanel9.Controls.Add(this.label143, 0, 4);
            this.tableLayoutPanel9.Controls.Add(this.label144, 0, 3);
            this.tableLayoutPanel9.Controls.Add(this.label145, 0, 5);
            this.tableLayoutPanel9.Controls.Add(this.V_9_a_1, 1, 1);
            this.tableLayoutPanel9.Controls.Add(this.label146, 0, 6);
            this.tableLayoutPanel9.Controls.Add(this.label147, 0, 7);
            this.tableLayoutPanel9.Controls.Add(this.label148, 0, 8);
            this.tableLayoutPanel9.Controls.Add(this.label149, 0, 9);
            this.tableLayoutPanel9.Controls.Add(this.label150, 0, 10);
            this.tableLayoutPanel9.Controls.Add(this.label151, 0, 11);
            this.tableLayoutPanel9.Controls.Add(this.label152, 0, 12);
            this.tableLayoutPanel9.Controls.Add(this.V_9_a_2, 2, 1);
            this.tableLayoutPanel9.Controls.Add(this.V_9_a_2_1, 2, 2);
            this.tableLayoutPanel9.Controls.Add(this.V_9_a_1_1, 1, 2);
            this.tableLayoutPanel9.Controls.Add(this.V_9_b_1, 1, 3);
            this.tableLayoutPanel9.Controls.Add(this.V_9_b_1_1, 1, 4);
            this.tableLayoutPanel9.Controls.Add(this.V_9_c_1, 1, 5);
            this.tableLayoutPanel9.Controls.Add(this.V_9_c_1_1, 1, 6);
            this.tableLayoutPanel9.Controls.Add(this.V_9_c_1_3, 1, 8);
            this.tableLayoutPanel9.Controls.Add(this.V_9_c_1_2, 1, 7);
            this.tableLayoutPanel9.Controls.Add(this.V_9_c_1_3_1, 1, 9);
            this.tableLayoutPanel9.Controls.Add(this.V_9_c_1_3_2, 1, 10);
            this.tableLayoutPanel9.Controls.Add(this.V_9_c_1_3_3, 1, 11);
            this.tableLayoutPanel9.Controls.Add(this.V_9_d_1, 1, 12);
            this.tableLayoutPanel9.Controls.Add(this.V_9_b_2, 2, 3);
            this.tableLayoutPanel9.Controls.Add(this.V_9_b_2_1, 2, 4);
            this.tableLayoutPanel9.Controls.Add(this.V_9_c_2, 2, 5);
            this.tableLayoutPanel9.Controls.Add(this.V_9_c_2_1, 2, 6);
            this.tableLayoutPanel9.Controls.Add(this.V_9_c_2_2, 2, 7);
            this.tableLayoutPanel9.Controls.Add(this.V_9_c_2_3, 2, 8);
            this.tableLayoutPanel9.Controls.Add(this.V_9_c_2_3_1, 2, 9);
            this.tableLayoutPanel9.Controls.Add(this.V_9_c_2_3_2, 2, 10);
            this.tableLayoutPanel9.Controls.Add(this.V_9_c_2_3_3, 2, 11);
            this.tableLayoutPanel9.Controls.Add(this.V_9_d_2, 2, 12);
            this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 3167);
            this.tableLayoutPanel9.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 13;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(630, 356);
            this.tableLayoutPanel9.TabIndex = 29;
            // 
            // label138
            // 
            this.label138.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label138.AutoSize = true;
            this.label138.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label138.Location = new System.Drawing.Point(4, 1);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(307, 40);
            this.label138.TabIndex = 0;
            this.label138.Text = "09. Các khoản phải trả  (Tuỳ theo yêu cầu quản lý của DN, có thể thuyết minh chi " +
    "tiết ngắn hạn, dài hạn)";
            this.label138.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label139
            // 
            this.label139.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label139.AutoSize = true;
            this.label139.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label139.Location = new System.Drawing.Point(318, 1);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(150, 40);
            this.label139.TabIndex = 0;
            this.label139.Text = "Cuối năm";
            this.label139.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label140
            // 
            this.label140.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label140.AutoSize = true;
            this.label140.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label140.Location = new System.Drawing.Point(475, 1);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(151, 40);
            this.label140.TabIndex = 0;
            this.label140.Text = "Đầu năm";
            this.label140.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label141
            // 
            this.label141.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label141.AutoSize = true;
            this.label141.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label141.Location = new System.Drawing.Point(4, 42);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(307, 25);
            this.label141.TabIndex = 0;
            this.label141.Text = "a) Phải trả Người bán";
            this.label141.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label142
            // 
            this.label142.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label142.AutoSize = true;
            this.label142.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label142.Location = new System.Drawing.Point(4, 68);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(307, 25);
            this.label142.TabIndex = 0;
            this.label142.Text = "Trong đó: Phải trả các bên liên quan";
            this.label142.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label143
            // 
            this.label143.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label143.AutoSize = true;
            this.label143.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label143.Location = new System.Drawing.Point(4, 120);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(307, 25);
            this.label143.TabIndex = 0;
            this.label143.Text = "Trong đó: Nhận trước tiền của các bên liên quan";
            this.label143.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label144
            // 
            this.label144.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label144.AutoSize = true;
            this.label144.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label144.Location = new System.Drawing.Point(4, 94);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(307, 25);
            this.label144.TabIndex = 0;
            this.label144.Text = "b. Người mua trả trước tiền ";
            this.label144.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label145
            // 
            this.label145.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label145.AutoSize = true;
            this.label145.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label145.Location = new System.Drawing.Point(4, 146);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(307, 25);
            this.label145.TabIndex = 0;
            this.label145.Text = "c) Phải trả khác (Chi tiết theo yêu cầu quản lý)";
            this.label145.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // V_9_a_1
            // 
            this.V_9_a_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_9_a_1.AutoSize = true;
            this.V_9_a_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_9_a_1.Location = new System.Drawing.Point(318, 42);
            this.V_9_a_1.Name = "V_9_a_1";
            this.V_9_a_1.Size = new System.Drawing.Size(150, 25);
            this.V_9_a_1.TabIndex = 280;
            this.V_9_a_1.Text = "0";
            this.V_9_a_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_9_a_1.Click += new System.EventHandler(this.number_edit);
            // 
            // label146
            // 
            this.label146.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label146.AutoSize = true;
            this.label146.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label146.Location = new System.Drawing.Point(4, 172);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(307, 25);
            this.label146.TabIndex = 0;
            this.label146.Text = "- Chi phí phải trả";
            this.label146.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label147
            // 
            this.label147.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label147.AutoSize = true;
            this.label147.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label147.Location = new System.Drawing.Point(4, 198);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(307, 25);
            this.label147.TabIndex = 0;
            this.label147.Text = "- Phải trả nội bộ khác";
            this.label147.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label148
            // 
            this.label148.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label148.AutoSize = true;
            this.label148.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label148.Location = new System.Drawing.Point(4, 224);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(307, 25);
            this.label148.TabIndex = 0;
            this.label148.Text = "- Phải trả, phải nộp khác";
            this.label148.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label149
            // 
            this.label149.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label149.AutoSize = true;
            this.label149.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label149.Location = new System.Drawing.Point(16, 250);
            this.label149.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(295, 25);
            this.label149.TabIndex = 0;
            this.label149.Text = "+ Tài sản thừa chờ xử lý";
            this.label149.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label150
            // 
            this.label150.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label150.AutoSize = true;
            this.label150.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label150.Location = new System.Drawing.Point(16, 276);
            this.label150.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(295, 25);
            this.label150.TabIndex = 0;
            this.label150.Text = "+ Các khoản phải nộp theo lương";
            this.label150.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label151
            // 
            this.label151.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label151.AutoSize = true;
            this.label151.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label151.Location = new System.Drawing.Point(16, 302);
            this.label151.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(295, 25);
            this.label151.TabIndex = 0;
            this.label151.Text = "+ Các khoản khác";
            this.label151.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label152
            // 
            this.label152.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label152.AutoSize = true;
            this.label152.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label152.Location = new System.Drawing.Point(4, 328);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(307, 27);
            this.label152.TabIndex = 0;
            this.label152.Text = "d) Nợ quá hạn chưa thanh toán";
            this.label152.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // V_9_a_2
            // 
            this.V_9_a_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_9_a_2.AutoSize = true;
            this.V_9_a_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_9_a_2.Location = new System.Drawing.Point(475, 42);
            this.V_9_a_2.Name = "V_9_a_2";
            this.V_9_a_2.Size = new System.Drawing.Size(151, 25);
            this.V_9_a_2.TabIndex = 281;
            this.V_9_a_2.Text = "0";
            this.V_9_a_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_9_a_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_9_a_2_1
            // 
            this.V_9_a_2_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_9_a_2_1.AutoSize = true;
            this.V_9_a_2_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_9_a_2_1.Location = new System.Drawing.Point(475, 68);
            this.V_9_a_2_1.Name = "V_9_a_2_1";
            this.V_9_a_2_1.Size = new System.Drawing.Size(151, 25);
            this.V_9_a_2_1.TabIndex = 283;
            this.V_9_a_2_1.Text = "0";
            this.V_9_a_2_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_9_a_2_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_9_a_1_1
            // 
            this.V_9_a_1_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_9_a_1_1.AutoSize = true;
            this.V_9_a_1_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_9_a_1_1.Location = new System.Drawing.Point(318, 68);
            this.V_9_a_1_1.Name = "V_9_a_1_1";
            this.V_9_a_1_1.Size = new System.Drawing.Size(150, 25);
            this.V_9_a_1_1.TabIndex = 282;
            this.V_9_a_1_1.Text = "0";
            this.V_9_a_1_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_9_a_1_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_9_b_1
            // 
            this.V_9_b_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_9_b_1.AutoSize = true;
            this.V_9_b_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_9_b_1.Location = new System.Drawing.Point(318, 94);
            this.V_9_b_1.Name = "V_9_b_1";
            this.V_9_b_1.Size = new System.Drawing.Size(150, 25);
            this.V_9_b_1.TabIndex = 284;
            this.V_9_b_1.Text = "0";
            this.V_9_b_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_9_b_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_9_b_1_1
            // 
            this.V_9_b_1_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_9_b_1_1.AutoSize = true;
            this.V_9_b_1_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_9_b_1_1.Location = new System.Drawing.Point(318, 120);
            this.V_9_b_1_1.Name = "V_9_b_1_1";
            this.V_9_b_1_1.Size = new System.Drawing.Size(150, 25);
            this.V_9_b_1_1.TabIndex = 286;
            this.V_9_b_1_1.Text = "0";
            this.V_9_b_1_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_9_b_1_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_9_c_1
            // 
            this.V_9_c_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_9_c_1.AutoSize = true;
            this.V_9_c_1.Location = new System.Drawing.Point(318, 146);
            this.V_9_c_1.Name = "V_9_c_1";
            this.V_9_c_1.Size = new System.Drawing.Size(150, 25);
            this.V_9_c_1.TabIndex = 288;
            this.V_9_c_1.Text = "0";
            this.V_9_c_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_9_c_1_1
            // 
            this.V_9_c_1_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_9_c_1_1.AutoSize = true;
            this.V_9_c_1_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_9_c_1_1.Location = new System.Drawing.Point(318, 172);
            this.V_9_c_1_1.Name = "V_9_c_1_1";
            this.V_9_c_1_1.Size = new System.Drawing.Size(150, 25);
            this.V_9_c_1_1.TabIndex = 290;
            this.V_9_c_1_1.Text = "0";
            this.V_9_c_1_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_9_c_1_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_9_c_1_3
            // 
            this.V_9_c_1_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_9_c_1_3.AutoSize = true;
            this.V_9_c_1_3.Location = new System.Drawing.Point(318, 224);
            this.V_9_c_1_3.Name = "V_9_c_1_3";
            this.V_9_c_1_3.Size = new System.Drawing.Size(150, 25);
            this.V_9_c_1_3.TabIndex = 294;
            this.V_9_c_1_3.Text = "0";
            this.V_9_c_1_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_9_c_1_3.Click += new System.EventHandler(this.number_edit);
            // 
            // V_9_c_1_2
            // 
            this.V_9_c_1_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_9_c_1_2.AutoSize = true;
            this.V_9_c_1_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_9_c_1_2.Location = new System.Drawing.Point(318, 198);
            this.V_9_c_1_2.Name = "V_9_c_1_2";
            this.V_9_c_1_2.Size = new System.Drawing.Size(150, 25);
            this.V_9_c_1_2.TabIndex = 292;
            this.V_9_c_1_2.Text = "0";
            this.V_9_c_1_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_9_c_1_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_9_c_1_3_1
            // 
            this.V_9_c_1_3_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_9_c_1_3_1.AutoSize = true;
            this.V_9_c_1_3_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_9_c_1_3_1.Location = new System.Drawing.Point(318, 250);
            this.V_9_c_1_3_1.Name = "V_9_c_1_3_1";
            this.V_9_c_1_3_1.Size = new System.Drawing.Size(150, 25);
            this.V_9_c_1_3_1.TabIndex = 296;
            this.V_9_c_1_3_1.Text = "0";
            this.V_9_c_1_3_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_9_c_1_3_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_9_c_1_3_2
            // 
            this.V_9_c_1_3_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_9_c_1_3_2.AutoSize = true;
            this.V_9_c_1_3_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_9_c_1_3_2.Location = new System.Drawing.Point(318, 276);
            this.V_9_c_1_3_2.Name = "V_9_c_1_3_2";
            this.V_9_c_1_3_2.Size = new System.Drawing.Size(150, 25);
            this.V_9_c_1_3_2.TabIndex = 298;
            this.V_9_c_1_3_2.Text = "0";
            this.V_9_c_1_3_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_9_c_1_3_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_9_c_1_3_3
            // 
            this.V_9_c_1_3_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_9_c_1_3_3.AutoSize = true;
            this.V_9_c_1_3_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_9_c_1_3_3.Location = new System.Drawing.Point(318, 302);
            this.V_9_c_1_3_3.Name = "V_9_c_1_3_3";
            this.V_9_c_1_3_3.Size = new System.Drawing.Size(150, 25);
            this.V_9_c_1_3_3.TabIndex = 300;
            this.V_9_c_1_3_3.Text = "0";
            this.V_9_c_1_3_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_9_c_1_3_3.Click += new System.EventHandler(this.number_edit);
            // 
            // V_9_d_1
            // 
            this.V_9_d_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_9_d_1.AutoSize = true;
            this.V_9_d_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_9_d_1.Location = new System.Drawing.Point(318, 328);
            this.V_9_d_1.Name = "V_9_d_1";
            this.V_9_d_1.Size = new System.Drawing.Size(150, 27);
            this.V_9_d_1.TabIndex = 302;
            this.V_9_d_1.Text = "0";
            this.V_9_d_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_9_d_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_9_b_2
            // 
            this.V_9_b_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_9_b_2.AutoSize = true;
            this.V_9_b_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_9_b_2.Location = new System.Drawing.Point(475, 94);
            this.V_9_b_2.Name = "V_9_b_2";
            this.V_9_b_2.Size = new System.Drawing.Size(151, 25);
            this.V_9_b_2.TabIndex = 285;
            this.V_9_b_2.Text = "0";
            this.V_9_b_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_9_b_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_9_b_2_1
            // 
            this.V_9_b_2_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_9_b_2_1.AutoSize = true;
            this.V_9_b_2_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_9_b_2_1.Location = new System.Drawing.Point(475, 120);
            this.V_9_b_2_1.Name = "V_9_b_2_1";
            this.V_9_b_2_1.Size = new System.Drawing.Size(151, 25);
            this.V_9_b_2_1.TabIndex = 287;
            this.V_9_b_2_1.Text = "0";
            this.V_9_b_2_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_9_b_2_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_9_c_2
            // 
            this.V_9_c_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_9_c_2.AutoSize = true;
            this.V_9_c_2.Location = new System.Drawing.Point(475, 146);
            this.V_9_c_2.Name = "V_9_c_2";
            this.V_9_c_2.Size = new System.Drawing.Size(151, 25);
            this.V_9_c_2.TabIndex = 289;
            this.V_9_c_2.Text = "0";
            this.V_9_c_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_9_c_2_1
            // 
            this.V_9_c_2_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_9_c_2_1.AutoSize = true;
            this.V_9_c_2_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_9_c_2_1.Location = new System.Drawing.Point(475, 172);
            this.V_9_c_2_1.Name = "V_9_c_2_1";
            this.V_9_c_2_1.Size = new System.Drawing.Size(151, 25);
            this.V_9_c_2_1.TabIndex = 291;
            this.V_9_c_2_1.Text = "0";
            this.V_9_c_2_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_9_c_2_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_9_c_2_2
            // 
            this.V_9_c_2_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_9_c_2_2.AutoSize = true;
            this.V_9_c_2_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_9_c_2_2.Location = new System.Drawing.Point(475, 198);
            this.V_9_c_2_2.Name = "V_9_c_2_2";
            this.V_9_c_2_2.Size = new System.Drawing.Size(151, 25);
            this.V_9_c_2_2.TabIndex = 293;
            this.V_9_c_2_2.Text = "0";
            this.V_9_c_2_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_9_c_2_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_9_c_2_3
            // 
            this.V_9_c_2_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_9_c_2_3.AutoSize = true;
            this.V_9_c_2_3.Location = new System.Drawing.Point(475, 224);
            this.V_9_c_2_3.Name = "V_9_c_2_3";
            this.V_9_c_2_3.Size = new System.Drawing.Size(151, 25);
            this.V_9_c_2_3.TabIndex = 295;
            this.V_9_c_2_3.Text = "0";
            this.V_9_c_2_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_9_c_2_3.Click += new System.EventHandler(this.number_edit);
            // 
            // V_9_c_2_3_1
            // 
            this.V_9_c_2_3_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_9_c_2_3_1.AutoSize = true;
            this.V_9_c_2_3_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_9_c_2_3_1.Location = new System.Drawing.Point(475, 250);
            this.V_9_c_2_3_1.Name = "V_9_c_2_3_1";
            this.V_9_c_2_3_1.Size = new System.Drawing.Size(151, 25);
            this.V_9_c_2_3_1.TabIndex = 297;
            this.V_9_c_2_3_1.Text = "0";
            this.V_9_c_2_3_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_9_c_2_3_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_9_c_2_3_2
            // 
            this.V_9_c_2_3_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_9_c_2_3_2.AutoSize = true;
            this.V_9_c_2_3_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_9_c_2_3_2.Location = new System.Drawing.Point(475, 276);
            this.V_9_c_2_3_2.Name = "V_9_c_2_3_2";
            this.V_9_c_2_3_2.Size = new System.Drawing.Size(151, 25);
            this.V_9_c_2_3_2.TabIndex = 299;
            this.V_9_c_2_3_2.Text = "0";
            this.V_9_c_2_3_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_9_c_2_3_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_9_c_2_3_3
            // 
            this.V_9_c_2_3_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_9_c_2_3_3.AutoSize = true;
            this.V_9_c_2_3_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_9_c_2_3_3.Location = new System.Drawing.Point(475, 302);
            this.V_9_c_2_3_3.Name = "V_9_c_2_3_3";
            this.V_9_c_2_3_3.Size = new System.Drawing.Size(151, 25);
            this.V_9_c_2_3_3.TabIndex = 301;
            this.V_9_c_2_3_3.Text = "0";
            this.V_9_c_2_3_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_9_c_2_3_3.Click += new System.EventHandler(this.number_edit);
            // 
            // V_9_d_2
            // 
            this.V_9_d_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_9_d_2.AutoSize = true;
            this.V_9_d_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_9_d_2.Location = new System.Drawing.Point(475, 328);
            this.V_9_d_2.Name = "V_9_d_2";
            this.V_9_d_2.Size = new System.Drawing.Size(151, 27);
            this.V_9_d_2.TabIndex = 302;
            this.V_9_d_2.Text = "0";
            this.V_9_d_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_9_d_2.Click += new System.EventHandler(this.number_edit);
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel10.ColumnCount = 3;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 313F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.Controls.Add(this.label153, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.label154, 1, 0);
            this.tableLayoutPanel10.Controls.Add(this.label155, 2, 0);
            this.tableLayoutPanel10.Controls.Add(this.label156, 0, 1);
            this.tableLayoutPanel10.Controls.Add(this.label157, 0, 2);
            this.tableLayoutPanel10.Controls.Add(this.label158, 0, 3);
            this.tableLayoutPanel10.Controls.Add(this.label159, 0, 4);
            this.tableLayoutPanel10.Controls.Add(this.label160, 0, 5);
            this.tableLayoutPanel10.Controls.Add(this.label161, 0, 6);
            this.tableLayoutPanel10.Controls.Add(this.label162, 0, 7);
            this.tableLayoutPanel10.Controls.Add(this.label163, 0, 8);
            this.tableLayoutPanel10.Controls.Add(this.label164, 0, 9);
            this.tableLayoutPanel10.Controls.Add(this.label165, 0, 10);
            this.tableLayoutPanel10.Controls.Add(this.V_10_1_1, 1, 1);
            this.tableLayoutPanel10.Controls.Add(this.V_10_1_2, 1, 2);
            this.tableLayoutPanel10.Controls.Add(this.V_10_1_3, 1, 3);
            this.tableLayoutPanel10.Controls.Add(this.V_10_1_5, 1, 5);
            this.tableLayoutPanel10.Controls.Add(this.V_10_1_4, 1, 4);
            this.tableLayoutPanel10.Controls.Add(this.V_10_1_6, 1, 6);
            this.tableLayoutPanel10.Controls.Add(this.V_10_1_7, 1, 7);
            this.tableLayoutPanel10.Controls.Add(this.V_10_1_8, 1, 8);
            this.tableLayoutPanel10.Controls.Add(this.V_10_1_9, 1, 9);
            this.tableLayoutPanel10.Controls.Add(this.V_10_Sum_1, 1, 10);
            this.tableLayoutPanel10.Controls.Add(this.V_10_Sum_2, 2, 10);
            this.tableLayoutPanel10.Controls.Add(this.V_10_2_9, 2, 9);
            this.tableLayoutPanel10.Controls.Add(this.V_10_2_8, 2, 8);
            this.tableLayoutPanel10.Controls.Add(this.V_10_2_7, 2, 7);
            this.tableLayoutPanel10.Controls.Add(this.V_10_2_6, 2, 6);
            this.tableLayoutPanel10.Controls.Add(this.V_10_2_5, 2, 5);
            this.tableLayoutPanel10.Controls.Add(this.V_10_2_4, 2, 4);
            this.tableLayoutPanel10.Controls.Add(this.V_10_2_3, 2, 3);
            this.tableLayoutPanel10.Controls.Add(this.V_10_2_2, 2, 2);
            this.tableLayoutPanel10.Controls.Add(this.V_10_2_1, 2, 1);
            this.tableLayoutPanel10.Location = new System.Drawing.Point(3, 3543);
            this.tableLayoutPanel10.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 11;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(630, 288);
            this.tableLayoutPanel10.TabIndex = 30;
            // 
            // label153
            // 
            this.label153.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label153.AutoSize = true;
            this.label153.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label153.Location = new System.Drawing.Point(4, 1);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(307, 25);
            this.label153.TabIndex = 0;
            this.label153.Text = "10. Thuế và các khoản phải nộp nhà nước";
            this.label153.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label154
            // 
            this.label154.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label154.AutoSize = true;
            this.label154.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label154.Location = new System.Drawing.Point(318, 1);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(150, 25);
            this.label154.TabIndex = 0;
            this.label154.Text = "Cuối năm";
            this.label154.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label155
            // 
            this.label155.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label155.AutoSize = true;
            this.label155.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label155.Location = new System.Drawing.Point(475, 1);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(151, 25);
            this.label155.TabIndex = 0;
            this.label155.Text = "Đầu năm";
            this.label155.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label156
            // 
            this.label156.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label156.AutoSize = true;
            this.label156.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label156.Location = new System.Drawing.Point(4, 27);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(307, 25);
            this.label156.TabIndex = 0;
            this.label156.Text = "- Thuế GTGT phải nộp";
            this.label156.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label157
            // 
            this.label157.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label157.AutoSize = true;
            this.label157.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label157.Location = new System.Drawing.Point(4, 53);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(307, 25);
            this.label157.TabIndex = 0;
            this.label157.Text = "- Thuế tiêu thụ đặc biệt";
            this.label157.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label158
            // 
            this.label158.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label158.AutoSize = true;
            this.label158.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label158.Location = new System.Drawing.Point(4, 79);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(307, 25);
            this.label158.TabIndex = 0;
            this.label158.Text = "- Thuế xuất khẩu, nhập khẩu.";
            this.label158.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label159
            // 
            this.label159.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label159.AutoSize = true;
            this.label159.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label159.Location = new System.Drawing.Point(4, 105);
            this.label159.Name = "label159";
            this.label159.Size = new System.Drawing.Size(307, 25);
            this.label159.TabIndex = 0;
            this.label159.Text = "- Thuế thu nhập doanh nghiệp";
            this.label159.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label160
            // 
            this.label160.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label160.AutoSize = true;
            this.label160.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label160.Location = new System.Drawing.Point(4, 131);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(307, 25);
            this.label160.TabIndex = 0;
            this.label160.Text = "- Thuế thu nhập cá nhân";
            this.label160.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label161
            // 
            this.label161.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label161.AutoSize = true;
            this.label161.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label161.Location = new System.Drawing.Point(4, 157);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(307, 25);
            this.label161.TabIndex = 0;
            this.label161.Text = "- Thuế tài nguyên.";
            this.label161.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label162
            // 
            this.label162.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label162.AutoSize = true;
            this.label162.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label162.Location = new System.Drawing.Point(4, 183);
            this.label162.Name = "label162";
            this.label162.Size = new System.Drawing.Size(307, 25);
            this.label162.TabIndex = 0;
            this.label162.Text = "- Thuế nhà đất, tiền thuê đất";
            this.label162.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label163
            // 
            this.label163.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label163.AutoSize = true;
            this.label163.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label163.Location = new System.Drawing.Point(4, 209);
            this.label163.Name = "label163";
            this.label163.Size = new System.Drawing.Size(307, 25);
            this.label163.TabIndex = 0;
            this.label163.Text = "- Các loại thuế khác";
            this.label163.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label164
            // 
            this.label164.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label164.AutoSize = true;
            this.label164.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label164.Location = new System.Drawing.Point(4, 235);
            this.label164.Name = "label164";
            this.label164.Size = new System.Drawing.Size(307, 25);
            this.label164.TabIndex = 0;
            this.label164.Text = "- Phí, lệ phí và các khoản phải nộp khác.";
            this.label164.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label165
            // 
            this.label165.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label165.AutoSize = true;
            this.label165.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label165.Location = new System.Drawing.Point(51, 261);
            this.label165.Margin = new System.Windows.Forms.Padding(50, 0, 3, 0);
            this.label165.Name = "label165";
            this.label165.Size = new System.Drawing.Size(260, 26);
            this.label165.TabIndex = 0;
            this.label165.Text = "Cộng";
            this.label165.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // V_10_1_1
            // 
            this.V_10_1_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_10_1_1.AutoSize = true;
            this.V_10_1_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_10_1_1.Location = new System.Drawing.Point(318, 27);
            this.V_10_1_1.Name = "V_10_1_1";
            this.V_10_1_1.Size = new System.Drawing.Size(150, 25);
            this.V_10_1_1.TabIndex = 304;
            this.V_10_1_1.Text = "0";
            this.V_10_1_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_10_1_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_10_1_2
            // 
            this.V_10_1_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_10_1_2.AutoSize = true;
            this.V_10_1_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_10_1_2.Location = new System.Drawing.Point(318, 53);
            this.V_10_1_2.Name = "V_10_1_2";
            this.V_10_1_2.Size = new System.Drawing.Size(150, 25);
            this.V_10_1_2.TabIndex = 306;
            this.V_10_1_2.Text = "0";
            this.V_10_1_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_10_1_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_10_1_3
            // 
            this.V_10_1_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_10_1_3.AutoSize = true;
            this.V_10_1_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_10_1_3.Location = new System.Drawing.Point(318, 79);
            this.V_10_1_3.Name = "V_10_1_3";
            this.V_10_1_3.Size = new System.Drawing.Size(150, 25);
            this.V_10_1_3.TabIndex = 308;
            this.V_10_1_3.Text = "0";
            this.V_10_1_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_10_1_3.Click += new System.EventHandler(this.number_edit);
            // 
            // V_10_1_5
            // 
            this.V_10_1_5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_10_1_5.AutoSize = true;
            this.V_10_1_5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_10_1_5.Location = new System.Drawing.Point(318, 131);
            this.V_10_1_5.Name = "V_10_1_5";
            this.V_10_1_5.Size = new System.Drawing.Size(150, 25);
            this.V_10_1_5.TabIndex = 312;
            this.V_10_1_5.Text = "0";
            this.V_10_1_5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_10_1_5.Click += new System.EventHandler(this.number_edit);
            // 
            // V_10_1_4
            // 
            this.V_10_1_4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_10_1_4.AutoSize = true;
            this.V_10_1_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_10_1_4.Location = new System.Drawing.Point(318, 105);
            this.V_10_1_4.Name = "V_10_1_4";
            this.V_10_1_4.Size = new System.Drawing.Size(150, 25);
            this.V_10_1_4.TabIndex = 310;
            this.V_10_1_4.Text = "0";
            this.V_10_1_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_10_1_4.Click += new System.EventHandler(this.number_edit);
            // 
            // V_10_1_6
            // 
            this.V_10_1_6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_10_1_6.AutoSize = true;
            this.V_10_1_6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_10_1_6.Location = new System.Drawing.Point(318, 157);
            this.V_10_1_6.Name = "V_10_1_6";
            this.V_10_1_6.Size = new System.Drawing.Size(150, 25);
            this.V_10_1_6.TabIndex = 314;
            this.V_10_1_6.Text = "0";
            this.V_10_1_6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_10_1_6.Click += new System.EventHandler(this.number_edit);
            // 
            // V_10_1_7
            // 
            this.V_10_1_7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_10_1_7.AutoSize = true;
            this.V_10_1_7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_10_1_7.Location = new System.Drawing.Point(318, 183);
            this.V_10_1_7.Name = "V_10_1_7";
            this.V_10_1_7.Size = new System.Drawing.Size(150, 25);
            this.V_10_1_7.TabIndex = 316;
            this.V_10_1_7.Text = "0";
            this.V_10_1_7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_10_1_7.Click += new System.EventHandler(this.number_edit);
            // 
            // V_10_1_8
            // 
            this.V_10_1_8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_10_1_8.AutoSize = true;
            this.V_10_1_8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_10_1_8.Location = new System.Drawing.Point(318, 209);
            this.V_10_1_8.Name = "V_10_1_8";
            this.V_10_1_8.Size = new System.Drawing.Size(150, 25);
            this.V_10_1_8.TabIndex = 318;
            this.V_10_1_8.Text = "0";
            this.V_10_1_8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_10_1_8.Click += new System.EventHandler(this.number_edit);
            // 
            // V_10_1_9
            // 
            this.V_10_1_9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_10_1_9.AutoSize = true;
            this.V_10_1_9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_10_1_9.Location = new System.Drawing.Point(318, 235);
            this.V_10_1_9.Name = "V_10_1_9";
            this.V_10_1_9.Size = new System.Drawing.Size(150, 25);
            this.V_10_1_9.TabIndex = 320;
            this.V_10_1_9.Text = "0";
            this.V_10_1_9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_10_1_9.Click += new System.EventHandler(this.number_edit);
            // 
            // V_10_Sum_1
            // 
            this.V_10_Sum_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_10_Sum_1.AutoSize = true;
            this.V_10_Sum_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.V_10_Sum_1.Location = new System.Drawing.Point(318, 261);
            this.V_10_Sum_1.Name = "V_10_Sum_1";
            this.V_10_Sum_1.Size = new System.Drawing.Size(150, 26);
            this.V_10_Sum_1.TabIndex = 322;
            this.V_10_Sum_1.Text = "0";
            this.V_10_Sum_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_10_Sum_2
            // 
            this.V_10_Sum_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_10_Sum_2.AutoSize = true;
            this.V_10_Sum_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.V_10_Sum_2.Location = new System.Drawing.Point(475, 261);
            this.V_10_Sum_2.Name = "V_10_Sum_2";
            this.V_10_Sum_2.Size = new System.Drawing.Size(151, 26);
            this.V_10_Sum_2.TabIndex = 323;
            this.V_10_Sum_2.Text = "0";
            this.V_10_Sum_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_10_2_9
            // 
            this.V_10_2_9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_10_2_9.AutoSize = true;
            this.V_10_2_9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_10_2_9.Location = new System.Drawing.Point(475, 235);
            this.V_10_2_9.Name = "V_10_2_9";
            this.V_10_2_9.Size = new System.Drawing.Size(151, 25);
            this.V_10_2_9.TabIndex = 321;
            this.V_10_2_9.Text = "0";
            this.V_10_2_9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_10_2_9.Click += new System.EventHandler(this.number_edit);
            // 
            // V_10_2_8
            // 
            this.V_10_2_8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_10_2_8.AutoSize = true;
            this.V_10_2_8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_10_2_8.Location = new System.Drawing.Point(475, 209);
            this.V_10_2_8.Name = "V_10_2_8";
            this.V_10_2_8.Size = new System.Drawing.Size(151, 25);
            this.V_10_2_8.TabIndex = 319;
            this.V_10_2_8.Text = "0";
            this.V_10_2_8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_10_2_8.Click += new System.EventHandler(this.number_edit);
            // 
            // V_10_2_7
            // 
            this.V_10_2_7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_10_2_7.AutoSize = true;
            this.V_10_2_7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_10_2_7.Location = new System.Drawing.Point(475, 183);
            this.V_10_2_7.Name = "V_10_2_7";
            this.V_10_2_7.Size = new System.Drawing.Size(151, 25);
            this.V_10_2_7.TabIndex = 317;
            this.V_10_2_7.Text = "0";
            this.V_10_2_7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_10_2_7.Click += new System.EventHandler(this.number_edit);
            // 
            // V_10_2_6
            // 
            this.V_10_2_6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_10_2_6.AutoSize = true;
            this.V_10_2_6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_10_2_6.Location = new System.Drawing.Point(475, 157);
            this.V_10_2_6.Name = "V_10_2_6";
            this.V_10_2_6.Size = new System.Drawing.Size(151, 25);
            this.V_10_2_6.TabIndex = 315;
            this.V_10_2_6.Text = "0";
            this.V_10_2_6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_10_2_6.Click += new System.EventHandler(this.number_edit);
            // 
            // V_10_2_5
            // 
            this.V_10_2_5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_10_2_5.AutoSize = true;
            this.V_10_2_5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_10_2_5.Location = new System.Drawing.Point(475, 131);
            this.V_10_2_5.Name = "V_10_2_5";
            this.V_10_2_5.Size = new System.Drawing.Size(151, 25);
            this.V_10_2_5.TabIndex = 313;
            this.V_10_2_5.Text = "0";
            this.V_10_2_5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_10_2_5.Click += new System.EventHandler(this.number_edit);
            // 
            // V_10_2_4
            // 
            this.V_10_2_4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_10_2_4.AutoSize = true;
            this.V_10_2_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_10_2_4.Location = new System.Drawing.Point(475, 105);
            this.V_10_2_4.Name = "V_10_2_4";
            this.V_10_2_4.Size = new System.Drawing.Size(151, 25);
            this.V_10_2_4.TabIndex = 311;
            this.V_10_2_4.Text = "0";
            this.V_10_2_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_10_2_4.Click += new System.EventHandler(this.number_edit);
            // 
            // V_10_2_3
            // 
            this.V_10_2_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_10_2_3.AutoSize = true;
            this.V_10_2_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_10_2_3.Location = new System.Drawing.Point(475, 79);
            this.V_10_2_3.Name = "V_10_2_3";
            this.V_10_2_3.Size = new System.Drawing.Size(151, 25);
            this.V_10_2_3.TabIndex = 309;
            this.V_10_2_3.Text = "0";
            this.V_10_2_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_10_2_3.Click += new System.EventHandler(this.number_edit);
            // 
            // V_10_2_2
            // 
            this.V_10_2_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_10_2_2.AutoSize = true;
            this.V_10_2_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_10_2_2.Location = new System.Drawing.Point(475, 53);
            this.V_10_2_2.Name = "V_10_2_2";
            this.V_10_2_2.Size = new System.Drawing.Size(151, 25);
            this.V_10_2_2.TabIndex = 307;
            this.V_10_2_2.Text = "0";
            this.V_10_2_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_10_2_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_10_2_1
            // 
            this.V_10_2_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_10_2_1.AutoSize = true;
            this.V_10_2_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_10_2_1.Location = new System.Drawing.Point(475, 27);
            this.V_10_2_1.Name = "V_10_2_1";
            this.V_10_2_1.Size = new System.Drawing.Size(151, 25);
            this.V_10_2_1.TabIndex = 305;
            this.V_10_2_1.Text = "0";
            this.V_10_2_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_10_2_1.Click += new System.EventHandler(this.number_edit);
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel11.ColumnCount = 5;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 165F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 132F));
            this.tableLayoutPanel11.Controls.Add(this.label166, 0, 0);
            this.tableLayoutPanel11.Controls.Add(this.label167, 1, 0);
            this.tableLayoutPanel11.Controls.Add(this.label168, 2, 0);
            this.tableLayoutPanel11.Controls.Add(this.label169, 3, 0);
            this.tableLayoutPanel11.Controls.Add(this.label170, 4, 0);
            this.tableLayoutPanel11.Controls.Add(this.label171, 0, 1);
            this.tableLayoutPanel11.Controls.Add(this.label172, 0, 2);
            this.tableLayoutPanel11.Controls.Add(this.label173, 0, 3);
            this.tableLayoutPanel11.Controls.Add(this.label174, 0, 4);
            this.tableLayoutPanel11.Controls.Add(this.label175, 0, 5);
            this.tableLayoutPanel11.Controls.Add(this.label176, 0, 6);
            this.tableLayoutPanel11.Controls.Add(this.label177, 0, 7);
            this.tableLayoutPanel11.Controls.Add(this.V_11_a_1, 1, 1);
            this.tableLayoutPanel11.Controls.Add(this.V_11_a_2, 2, 1);
            this.tableLayoutPanel11.Controls.Add(this.V_11_a_3, 3, 1);
            this.tableLayoutPanel11.Controls.Add(this.V_11_a_4, 4, 1);
            this.tableLayoutPanel11.Controls.Add(this.V_11_a_4_1, 4, 2);
            this.tableLayoutPanel11.Controls.Add(this.V_11_b_4, 4, 3);
            this.tableLayoutPanel11.Controls.Add(this.V_11_b_4_1, 4, 4);
            this.tableLayoutPanel11.Controls.Add(this.V_11_c_4, 4, 5);
            this.tableLayoutPanel11.Controls.Add(this.V_11_c_4_1, 4, 6);
            this.tableLayoutPanel11.Controls.Add(this.V_11_Sum_4, 4, 7);
            this.tableLayoutPanel11.Controls.Add(this.V_11_a_3_1, 3, 2);
            this.tableLayoutPanel11.Controls.Add(this.V_11_b_3_1, 3, 4);
            this.tableLayoutPanel11.Controls.Add(this.V_11_b_3, 3, 3);
            this.tableLayoutPanel11.Controls.Add(this.V_11_c_3, 3, 5);
            this.tableLayoutPanel11.Controls.Add(this.V_11_c_3_1, 3, 6);
            this.tableLayoutPanel11.Controls.Add(this.V_11_Sum_3, 3, 7);
            this.tableLayoutPanel11.Controls.Add(this.V_11_a_2_1, 2, 2);
            this.tableLayoutPanel11.Controls.Add(this.V_11_b_2, 2, 3);
            this.tableLayoutPanel11.Controls.Add(this.V_11_b_2_1, 2, 4);
            this.tableLayoutPanel11.Controls.Add(this.V_11_c_2, 2, 5);
            this.tableLayoutPanel11.Controls.Add(this.V_11_c_2_1, 2, 6);
            this.tableLayoutPanel11.Controls.Add(this.V_11_Sum_2, 2, 7);
            this.tableLayoutPanel11.Controls.Add(this.V_11_a_1_1, 1, 2);
            this.tableLayoutPanel11.Controls.Add(this.V_11_b_1, 1, 3);
            this.tableLayoutPanel11.Controls.Add(this.V_11_b_1_1, 1, 4);
            this.tableLayoutPanel11.Controls.Add(this.V_11_c_1, 1, 5);
            this.tableLayoutPanel11.Controls.Add(this.V_11_c_1_1, 1, 6);
            this.tableLayoutPanel11.Controls.Add(this.V_11_Sum_1, 1, 7);
            this.tableLayoutPanel11.Location = new System.Drawing.Point(3, 3851);
            this.tableLayoutPanel11.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 8;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(630, 232);
            this.tableLayoutPanel11.TabIndex = 31;
            // 
            // label166
            // 
            this.label166.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label166.AutoSize = true;
            this.label166.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label166.Location = new System.Drawing.Point(4, 1);
            this.label166.Name = "label166";
            this.label166.Size = new System.Drawing.Size(159, 25);
            this.label166.TabIndex = 0;
            this.label166.Text = "11. Vay nợ thuê tài chính";
            this.label166.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label167
            // 
            this.label167.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label167.AutoSize = true;
            this.label167.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label167.Location = new System.Drawing.Point(170, 1);
            this.label167.Name = "label167";
            this.label167.Size = new System.Drawing.Size(109, 25);
            this.label167.TabIndex = 0;
            this.label167.Text = "Cuối năm";
            this.label167.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label168
            // 
            this.label168.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label168.AutoSize = true;
            this.label168.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label168.Location = new System.Drawing.Point(286, 1);
            this.label168.Name = "label168";
            this.label168.Size = new System.Drawing.Size(109, 25);
            this.label168.TabIndex = 0;
            this.label168.Text = "Tăng trong năm";
            this.label168.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label169
            // 
            this.label169.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label169.AutoSize = true;
            this.label169.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label169.Location = new System.Drawing.Point(402, 1);
            this.label169.Name = "label169";
            this.label169.Size = new System.Drawing.Size(109, 25);
            this.label169.TabIndex = 0;
            this.label169.Text = "Giảm trong năm";
            this.label169.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label170
            // 
            this.label170.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label170.AutoSize = true;
            this.label170.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label170.Location = new System.Drawing.Point(518, 1);
            this.label170.Margin = new System.Windows.Forms.Padding(3, 0, 20, 0);
            this.label170.Name = "label170";
            this.label170.Size = new System.Drawing.Size(109, 25);
            this.label170.TabIndex = 0;
            this.label170.Text = "Đầu năm";
            this.label170.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label171
            // 
            this.label171.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label171.AutoSize = true;
            this.label171.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label171.Location = new System.Drawing.Point(4, 27);
            this.label171.Name = "label171";
            this.label171.Size = new System.Drawing.Size(159, 25);
            this.label171.TabIndex = 0;
            this.label171.Text = "a) Vay ngắn hạn";
            this.label171.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label172
            // 
            this.label172.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label172.AutoSize = true;
            this.label172.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label172.Location = new System.Drawing.Point(4, 53);
            this.label172.Name = "label172";
            this.label172.Size = new System.Drawing.Size(159, 30);
            this.label172.TabIndex = 0;
            this.label172.Text = "Trong đó: Vay từ các bên liên quan";
            this.label172.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label173
            // 
            this.label173.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label173.AutoSize = true;
            this.label173.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label173.Location = new System.Drawing.Point(4, 84);
            this.label173.Name = "label173";
            this.label173.Size = new System.Drawing.Size(159, 25);
            this.label173.TabIndex = 0;
            this.label173.Text = "b) Vay dài hạn";
            this.label173.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label174
            // 
            this.label174.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label174.AutoSize = true;
            this.label174.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label174.Location = new System.Drawing.Point(4, 110);
            this.label174.Name = "label174";
            this.label174.Size = new System.Drawing.Size(159, 30);
            this.label174.TabIndex = 0;
            this.label174.Text = "Trong đó: Vay từ các bên liên quan";
            this.label174.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label175
            // 
            this.label175.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label175.AutoSize = true;
            this.label175.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label175.Location = new System.Drawing.Point(4, 141);
            this.label175.Name = "label175";
            this.label175.Size = new System.Drawing.Size(159, 30);
            this.label175.TabIndex = 0;
            this.label175.Text = "c) Các khoản nợ gốc thuê tài chính";
            this.label175.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label176
            // 
            this.label176.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label176.AutoSize = true;
            this.label176.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label176.Location = new System.Drawing.Point(4, 172);
            this.label176.Name = "label176";
            this.label176.Size = new System.Drawing.Size(159, 30);
            this.label176.TabIndex = 0;
            this.label176.Text = "Trong đó: Nợ thuê tài chính từ các bên liên quan";
            this.label176.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label177
            // 
            this.label177.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label177.AutoSize = true;
            this.label177.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label177.Location = new System.Drawing.Point(51, 203);
            this.label177.Margin = new System.Windows.Forms.Padding(50, 0, 3, 0);
            this.label177.Name = "label177";
            this.label177.Size = new System.Drawing.Size(112, 28);
            this.label177.TabIndex = 0;
            this.label177.Text = "Cộng";
            this.label177.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // V_11_a_1
            // 
            this.V_11_a_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_11_a_1.AutoSize = true;
            this.V_11_a_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_11_a_1.Location = new System.Drawing.Point(170, 27);
            this.V_11_a_1.Name = "V_11_a_1";
            this.V_11_a_1.Size = new System.Drawing.Size(109, 25);
            this.V_11_a_1.TabIndex = 324;
            this.V_11_a_1.Text = "0";
            this.V_11_a_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_11_a_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_11_a_2
            // 
            this.V_11_a_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_11_a_2.AutoSize = true;
            this.V_11_a_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_11_a_2.Location = new System.Drawing.Point(286, 27);
            this.V_11_a_2.Name = "V_11_a_2";
            this.V_11_a_2.Size = new System.Drawing.Size(109, 25);
            this.V_11_a_2.TabIndex = 325;
            this.V_11_a_2.Text = "0";
            this.V_11_a_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_11_a_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_11_a_3
            // 
            this.V_11_a_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_11_a_3.AutoSize = true;
            this.V_11_a_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_11_a_3.Location = new System.Drawing.Point(402, 27);
            this.V_11_a_3.Name = "V_11_a_3";
            this.V_11_a_3.Size = new System.Drawing.Size(109, 25);
            this.V_11_a_3.TabIndex = 326;
            this.V_11_a_3.Text = "0";
            this.V_11_a_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_11_a_3.Click += new System.EventHandler(this.number_edit);
            // 
            // V_11_a_4
            // 
            this.V_11_a_4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_11_a_4.AutoSize = true;
            this.V_11_a_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_11_a_4.Location = new System.Drawing.Point(518, 27);
            this.V_11_a_4.Margin = new System.Windows.Forms.Padding(3, 0, 20, 0);
            this.V_11_a_4.Name = "V_11_a_4";
            this.V_11_a_4.Size = new System.Drawing.Size(109, 25);
            this.V_11_a_4.TabIndex = 327;
            this.V_11_a_4.Text = "0";
            this.V_11_a_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_11_a_4.Click += new System.EventHandler(this.number_edit);
            // 
            // V_11_a_4_1
            // 
            this.V_11_a_4_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_11_a_4_1.AutoSize = true;
            this.V_11_a_4_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_11_a_4_1.Location = new System.Drawing.Point(518, 53);
            this.V_11_a_4_1.Margin = new System.Windows.Forms.Padding(3, 0, 20, 0);
            this.V_11_a_4_1.Name = "V_11_a_4_1";
            this.V_11_a_4_1.Size = new System.Drawing.Size(109, 30);
            this.V_11_a_4_1.TabIndex = 331;
            this.V_11_a_4_1.Text = "0";
            this.V_11_a_4_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_11_a_4_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_11_b_4
            // 
            this.V_11_b_4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_11_b_4.AutoSize = true;
            this.V_11_b_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_11_b_4.Location = new System.Drawing.Point(518, 84);
            this.V_11_b_4.Margin = new System.Windows.Forms.Padding(3, 0, 20, 0);
            this.V_11_b_4.Name = "V_11_b_4";
            this.V_11_b_4.Size = new System.Drawing.Size(109, 25);
            this.V_11_b_4.TabIndex = 335;
            this.V_11_b_4.Text = "0";
            this.V_11_b_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_11_b_4.Click += new System.EventHandler(this.number_edit);
            // 
            // V_11_b_4_1
            // 
            this.V_11_b_4_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_11_b_4_1.AutoSize = true;
            this.V_11_b_4_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_11_b_4_1.Location = new System.Drawing.Point(518, 110);
            this.V_11_b_4_1.Margin = new System.Windows.Forms.Padding(3, 0, 20, 0);
            this.V_11_b_4_1.Name = "V_11_b_4_1";
            this.V_11_b_4_1.Size = new System.Drawing.Size(109, 30);
            this.V_11_b_4_1.TabIndex = 339;
            this.V_11_b_4_1.Text = "0";
            this.V_11_b_4_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_11_b_4_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_11_c_4
            // 
            this.V_11_c_4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_11_c_4.AutoSize = true;
            this.V_11_c_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_11_c_4.Location = new System.Drawing.Point(518, 141);
            this.V_11_c_4.Margin = new System.Windows.Forms.Padding(3, 0, 20, 0);
            this.V_11_c_4.Name = "V_11_c_4";
            this.V_11_c_4.Size = new System.Drawing.Size(109, 30);
            this.V_11_c_4.TabIndex = 343;
            this.V_11_c_4.Text = "0";
            this.V_11_c_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_11_c_4.Click += new System.EventHandler(this.number_edit);
            // 
            // V_11_c_4_1
            // 
            this.V_11_c_4_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_11_c_4_1.AutoSize = true;
            this.V_11_c_4_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_11_c_4_1.Location = new System.Drawing.Point(518, 172);
            this.V_11_c_4_1.Margin = new System.Windows.Forms.Padding(3, 0, 20, 0);
            this.V_11_c_4_1.Name = "V_11_c_4_1";
            this.V_11_c_4_1.Size = new System.Drawing.Size(109, 30);
            this.V_11_c_4_1.TabIndex = 347;
            this.V_11_c_4_1.Text = "0";
            this.V_11_c_4_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_11_c_4_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_11_Sum_4
            // 
            this.V_11_Sum_4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_11_Sum_4.AutoSize = true;
            this.V_11_Sum_4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.V_11_Sum_4.Location = new System.Drawing.Point(518, 203);
            this.V_11_Sum_4.Margin = new System.Windows.Forms.Padding(3, 0, 20, 0);
            this.V_11_Sum_4.Name = "V_11_Sum_4";
            this.V_11_Sum_4.Size = new System.Drawing.Size(109, 28);
            this.V_11_Sum_4.TabIndex = 351;
            this.V_11_Sum_4.Text = "0";
            this.V_11_Sum_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_11_a_3_1
            // 
            this.V_11_a_3_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_11_a_3_1.AutoSize = true;
            this.V_11_a_3_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_11_a_3_1.Location = new System.Drawing.Point(402, 53);
            this.V_11_a_3_1.Name = "V_11_a_3_1";
            this.V_11_a_3_1.Size = new System.Drawing.Size(109, 30);
            this.V_11_a_3_1.TabIndex = 330;
            this.V_11_a_3_1.Text = "0";
            this.V_11_a_3_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_11_a_3_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_11_b_3_1
            // 
            this.V_11_b_3_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_11_b_3_1.AutoSize = true;
            this.V_11_b_3_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_11_b_3_1.Location = new System.Drawing.Point(402, 110);
            this.V_11_b_3_1.Name = "V_11_b_3_1";
            this.V_11_b_3_1.Size = new System.Drawing.Size(109, 30);
            this.V_11_b_3_1.TabIndex = 338;
            this.V_11_b_3_1.Text = "0";
            this.V_11_b_3_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_11_b_3_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_11_b_3
            // 
            this.V_11_b_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_11_b_3.AutoSize = true;
            this.V_11_b_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_11_b_3.Location = new System.Drawing.Point(402, 84);
            this.V_11_b_3.Name = "V_11_b_3";
            this.V_11_b_3.Size = new System.Drawing.Size(109, 25);
            this.V_11_b_3.TabIndex = 334;
            this.V_11_b_3.Text = "0";
            this.V_11_b_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_11_b_3.Click += new System.EventHandler(this.number_edit);
            // 
            // V_11_c_3
            // 
            this.V_11_c_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_11_c_3.AutoSize = true;
            this.V_11_c_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_11_c_3.Location = new System.Drawing.Point(402, 141);
            this.V_11_c_3.Name = "V_11_c_3";
            this.V_11_c_3.Size = new System.Drawing.Size(109, 30);
            this.V_11_c_3.TabIndex = 342;
            this.V_11_c_3.Text = "0";
            this.V_11_c_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_11_c_3.Click += new System.EventHandler(this.number_edit);
            // 
            // V_11_c_3_1
            // 
            this.V_11_c_3_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_11_c_3_1.AutoSize = true;
            this.V_11_c_3_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_11_c_3_1.Location = new System.Drawing.Point(402, 172);
            this.V_11_c_3_1.Name = "V_11_c_3_1";
            this.V_11_c_3_1.Size = new System.Drawing.Size(109, 30);
            this.V_11_c_3_1.TabIndex = 346;
            this.V_11_c_3_1.Text = "0";
            this.V_11_c_3_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_11_c_3_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_11_Sum_3
            // 
            this.V_11_Sum_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_11_Sum_3.AutoSize = true;
            this.V_11_Sum_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.V_11_Sum_3.Location = new System.Drawing.Point(402, 203);
            this.V_11_Sum_3.Name = "V_11_Sum_3";
            this.V_11_Sum_3.Size = new System.Drawing.Size(109, 28);
            this.V_11_Sum_3.TabIndex = 350;
            this.V_11_Sum_3.Text = "0";
            this.V_11_Sum_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_11_a_2_1
            // 
            this.V_11_a_2_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_11_a_2_1.AutoSize = true;
            this.V_11_a_2_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_11_a_2_1.Location = new System.Drawing.Point(286, 53);
            this.V_11_a_2_1.Name = "V_11_a_2_1";
            this.V_11_a_2_1.Size = new System.Drawing.Size(109, 30);
            this.V_11_a_2_1.TabIndex = 329;
            this.V_11_a_2_1.Text = "0";
            this.V_11_a_2_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_11_a_2_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_11_b_2
            // 
            this.V_11_b_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_11_b_2.AutoSize = true;
            this.V_11_b_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_11_b_2.Location = new System.Drawing.Point(286, 84);
            this.V_11_b_2.Name = "V_11_b_2";
            this.V_11_b_2.Size = new System.Drawing.Size(109, 25);
            this.V_11_b_2.TabIndex = 333;
            this.V_11_b_2.Text = "0";
            this.V_11_b_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_11_b_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_11_b_2_1
            // 
            this.V_11_b_2_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_11_b_2_1.AutoSize = true;
            this.V_11_b_2_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_11_b_2_1.Location = new System.Drawing.Point(286, 110);
            this.V_11_b_2_1.Name = "V_11_b_2_1";
            this.V_11_b_2_1.Size = new System.Drawing.Size(109, 30);
            this.V_11_b_2_1.TabIndex = 337;
            this.V_11_b_2_1.Text = "0";
            this.V_11_b_2_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_11_b_2_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_11_c_2
            // 
            this.V_11_c_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_11_c_2.AutoSize = true;
            this.V_11_c_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_11_c_2.Location = new System.Drawing.Point(286, 141);
            this.V_11_c_2.Name = "V_11_c_2";
            this.V_11_c_2.Size = new System.Drawing.Size(109, 30);
            this.V_11_c_2.TabIndex = 341;
            this.V_11_c_2.Text = "0";
            this.V_11_c_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_11_c_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_11_c_2_1
            // 
            this.V_11_c_2_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_11_c_2_1.AutoSize = true;
            this.V_11_c_2_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_11_c_2_1.Location = new System.Drawing.Point(286, 172);
            this.V_11_c_2_1.Name = "V_11_c_2_1";
            this.V_11_c_2_1.Size = new System.Drawing.Size(109, 30);
            this.V_11_c_2_1.TabIndex = 345;
            this.V_11_c_2_1.Text = "0";
            this.V_11_c_2_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_11_c_2_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_11_Sum_2
            // 
            this.V_11_Sum_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_11_Sum_2.AutoSize = true;
            this.V_11_Sum_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.V_11_Sum_2.Location = new System.Drawing.Point(286, 203);
            this.V_11_Sum_2.Name = "V_11_Sum_2";
            this.V_11_Sum_2.Size = new System.Drawing.Size(109, 28);
            this.V_11_Sum_2.TabIndex = 349;
            this.V_11_Sum_2.Text = "0";
            this.V_11_Sum_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_11_a_1_1
            // 
            this.V_11_a_1_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_11_a_1_1.AutoSize = true;
            this.V_11_a_1_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_11_a_1_1.Location = new System.Drawing.Point(170, 53);
            this.V_11_a_1_1.Name = "V_11_a_1_1";
            this.V_11_a_1_1.Size = new System.Drawing.Size(109, 30);
            this.V_11_a_1_1.TabIndex = 328;
            this.V_11_a_1_1.Text = "0";
            this.V_11_a_1_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_11_a_1_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_11_b_1
            // 
            this.V_11_b_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_11_b_1.AutoSize = true;
            this.V_11_b_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_11_b_1.Location = new System.Drawing.Point(170, 84);
            this.V_11_b_1.Name = "V_11_b_1";
            this.V_11_b_1.Size = new System.Drawing.Size(109, 25);
            this.V_11_b_1.TabIndex = 332;
            this.V_11_b_1.Text = "0";
            this.V_11_b_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_11_b_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_11_b_1_1
            // 
            this.V_11_b_1_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_11_b_1_1.AutoSize = true;
            this.V_11_b_1_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_11_b_1_1.Location = new System.Drawing.Point(170, 110);
            this.V_11_b_1_1.Name = "V_11_b_1_1";
            this.V_11_b_1_1.Size = new System.Drawing.Size(109, 30);
            this.V_11_b_1_1.TabIndex = 336;
            this.V_11_b_1_1.Text = "0";
            this.V_11_b_1_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_11_b_1_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_11_c_1
            // 
            this.V_11_c_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_11_c_1.AutoSize = true;
            this.V_11_c_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_11_c_1.Location = new System.Drawing.Point(170, 141);
            this.V_11_c_1.Name = "V_11_c_1";
            this.V_11_c_1.Size = new System.Drawing.Size(109, 30);
            this.V_11_c_1.TabIndex = 340;
            this.V_11_c_1.Text = "0";
            this.V_11_c_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_11_c_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_11_c_1_1
            // 
            this.V_11_c_1_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_11_c_1_1.AutoSize = true;
            this.V_11_c_1_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_11_c_1_1.Location = new System.Drawing.Point(170, 172);
            this.V_11_c_1_1.Name = "V_11_c_1_1";
            this.V_11_c_1_1.Size = new System.Drawing.Size(109, 30);
            this.V_11_c_1_1.TabIndex = 344;
            this.V_11_c_1_1.Text = "0";
            this.V_11_c_1_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_11_c_1_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_11_Sum_1
            // 
            this.V_11_Sum_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_11_Sum_1.AutoSize = true;
            this.V_11_Sum_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.V_11_Sum_1.Location = new System.Drawing.Point(170, 203);
            this.V_11_Sum_1.Name = "V_11_Sum_1";
            this.V_11_Sum_1.Size = new System.Drawing.Size(109, 28);
            this.V_11_Sum_1.TabIndex = 348;
            this.V_11_Sum_1.Text = "0";
            this.V_11_Sum_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel12.ColumnCount = 3;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 313F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.Controls.Add(this.label178, 0, 0);
            this.tableLayoutPanel12.Controls.Add(this.label179, 1, 0);
            this.tableLayoutPanel12.Controls.Add(this.label180, 2, 0);
            this.tableLayoutPanel12.Controls.Add(this.label181, 0, 1);
            this.tableLayoutPanel12.Controls.Add(this.label182, 0, 2);
            this.tableLayoutPanel12.Controls.Add(this.label183, 0, 3);
            this.tableLayoutPanel12.Controls.Add(this.label184, 0, 4);
            this.tableLayoutPanel12.Controls.Add(this.V_12_1_1, 1, 1);
            this.tableLayoutPanel12.Controls.Add(this.V_12_2_1, 2, 1);
            this.tableLayoutPanel12.Controls.Add(this.V_12_2_2, 2, 2);
            this.tableLayoutPanel12.Controls.Add(this.V_12_1_2, 1, 2);
            this.tableLayoutPanel12.Controls.Add(this.V_12_1_3, 1, 3);
            this.tableLayoutPanel12.Controls.Add(this.V_12_2_3, 2, 3);
            this.tableLayoutPanel12.Controls.Add(this.V_12_Sum_2, 2, 4);
            this.tableLayoutPanel12.Controls.Add(this.V_12_Sum_1, 1, 4);
            this.tableLayoutPanel12.Location = new System.Drawing.Point(3, 4103);
            this.tableLayoutPanel12.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 5;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(630, 131);
            this.tableLayoutPanel12.TabIndex = 32;
            // 
            // label178
            // 
            this.label178.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label178.AutoSize = true;
            this.label178.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label178.Location = new System.Drawing.Point(4, 1);
            this.label178.Name = "label178";
            this.label178.Size = new System.Drawing.Size(307, 25);
            this.label178.TabIndex = 0;
            this.label178.Text = "12. Dự phòng phải trả";
            this.label178.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label179
            // 
            this.label179.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label179.AutoSize = true;
            this.label179.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label179.Location = new System.Drawing.Point(318, 1);
            this.label179.Name = "label179";
            this.label179.Size = new System.Drawing.Size(150, 25);
            this.label179.TabIndex = 0;
            this.label179.Text = "Cuối năm";
            this.label179.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label180
            // 
            this.label180.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label180.AutoSize = true;
            this.label180.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label180.Location = new System.Drawing.Point(475, 1);
            this.label180.Name = "label180";
            this.label180.Size = new System.Drawing.Size(151, 25);
            this.label180.TabIndex = 0;
            this.label180.Text = "Đầu năm";
            this.label180.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label181
            // 
            this.label181.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label181.AutoSize = true;
            this.label181.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label181.Location = new System.Drawing.Point(4, 27);
            this.label181.Name = "label181";
            this.label181.Size = new System.Drawing.Size(307, 25);
            this.label181.TabIndex = 1;
            this.label181.Text = "- Dự phòng bảo hành sản phẩm hàng hoá";
            this.label181.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label182
            // 
            this.label182.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label182.AutoSize = true;
            this.label182.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label182.Location = new System.Drawing.Point(4, 53);
            this.label182.Name = "label182";
            this.label182.Size = new System.Drawing.Size(307, 25);
            this.label182.TabIndex = 1;
            this.label182.Text = "- Dự phòng bảo hành công trình xây dựng";
            this.label182.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label183
            // 
            this.label183.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label183.AutoSize = true;
            this.label183.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label183.Location = new System.Drawing.Point(4, 79);
            this.label183.Name = "label183";
            this.label183.Size = new System.Drawing.Size(307, 25);
            this.label183.TabIndex = 1;
            this.label183.Text = "- Dự phòng phải trả khác";
            this.label183.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label184
            // 
            this.label184.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label184.AutoSize = true;
            this.label184.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label184.Location = new System.Drawing.Point(51, 105);
            this.label184.Margin = new System.Windows.Forms.Padding(50, 0, 3, 0);
            this.label184.Name = "label184";
            this.label184.Size = new System.Drawing.Size(260, 25);
            this.label184.TabIndex = 1;
            this.label184.Text = "Cộng";
            this.label184.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // V_12_1_1
            // 
            this.V_12_1_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_12_1_1.AutoSize = true;
            this.V_12_1_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_12_1_1.Location = new System.Drawing.Point(318, 27);
            this.V_12_1_1.Name = "V_12_1_1";
            this.V_12_1_1.Size = new System.Drawing.Size(150, 25);
            this.V_12_1_1.TabIndex = 352;
            this.V_12_1_1.Text = "0";
            this.V_12_1_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_12_1_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_12_2_1
            // 
            this.V_12_2_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_12_2_1.AutoSize = true;
            this.V_12_2_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_12_2_1.Location = new System.Drawing.Point(475, 27);
            this.V_12_2_1.Name = "V_12_2_1";
            this.V_12_2_1.Size = new System.Drawing.Size(151, 25);
            this.V_12_2_1.TabIndex = 353;
            this.V_12_2_1.Text = "0";
            this.V_12_2_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_12_2_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_12_2_2
            // 
            this.V_12_2_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_12_2_2.AutoSize = true;
            this.V_12_2_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_12_2_2.Location = new System.Drawing.Point(475, 53);
            this.V_12_2_2.Name = "V_12_2_2";
            this.V_12_2_2.Size = new System.Drawing.Size(151, 25);
            this.V_12_2_2.TabIndex = 355;
            this.V_12_2_2.Text = "0";
            this.V_12_2_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_12_2_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_12_1_2
            // 
            this.V_12_1_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_12_1_2.AutoSize = true;
            this.V_12_1_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_12_1_2.Location = new System.Drawing.Point(318, 53);
            this.V_12_1_2.Name = "V_12_1_2";
            this.V_12_1_2.Size = new System.Drawing.Size(150, 25);
            this.V_12_1_2.TabIndex = 354;
            this.V_12_1_2.Text = "0";
            this.V_12_1_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_12_1_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_12_1_3
            // 
            this.V_12_1_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_12_1_3.AutoSize = true;
            this.V_12_1_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_12_1_3.Location = new System.Drawing.Point(318, 79);
            this.V_12_1_3.Name = "V_12_1_3";
            this.V_12_1_3.Size = new System.Drawing.Size(150, 25);
            this.V_12_1_3.TabIndex = 356;
            this.V_12_1_3.Text = "0";
            this.V_12_1_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_12_1_3.Click += new System.EventHandler(this.number_edit);
            // 
            // V_12_2_3
            // 
            this.V_12_2_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_12_2_3.AutoSize = true;
            this.V_12_2_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_12_2_3.Location = new System.Drawing.Point(475, 79);
            this.V_12_2_3.Name = "V_12_2_3";
            this.V_12_2_3.Size = new System.Drawing.Size(151, 25);
            this.V_12_2_3.TabIndex = 357;
            this.V_12_2_3.Text = "0";
            this.V_12_2_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_12_2_3.Click += new System.EventHandler(this.number_edit);
            // 
            // V_12_Sum_2
            // 
            this.V_12_Sum_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_12_Sum_2.AutoSize = true;
            this.V_12_Sum_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.V_12_Sum_2.Location = new System.Drawing.Point(475, 105);
            this.V_12_Sum_2.Name = "V_12_Sum_2";
            this.V_12_Sum_2.Size = new System.Drawing.Size(151, 25);
            this.V_12_Sum_2.TabIndex = 359;
            this.V_12_Sum_2.Text = "0";
            this.V_12_Sum_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_12_Sum_1
            // 
            this.V_12_Sum_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_12_Sum_1.AutoSize = true;
            this.V_12_Sum_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.V_12_Sum_1.Location = new System.Drawing.Point(318, 105);
            this.V_12_Sum_1.Name = "V_12_Sum_1";
            this.V_12_Sum_1.Size = new System.Drawing.Size(150, 25);
            this.V_12_Sum_1.TabIndex = 358;
            this.V_12_Sum_1.Text = "0";
            this.V_12_Sum_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel13.ColumnCount = 8;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 77F));
            this.tableLayoutPanel13.Controls.Add(this.label185, 0, 1);
            this.tableLayoutPanel13.Controls.Add(this.label186, 1, 1);
            this.tableLayoutPanel13.Controls.Add(this.label187, 1, 2);
            this.tableLayoutPanel13.Controls.Add(this.label188, 2, 2);
            this.tableLayoutPanel13.Controls.Add(this.label189, 3, 2);
            this.tableLayoutPanel13.Controls.Add(this.label190, 4, 2);
            this.tableLayoutPanel13.Controls.Add(this.label191, 5, 2);
            this.tableLayoutPanel13.Controls.Add(this.label192, 6, 2);
            this.tableLayoutPanel13.Controls.Add(this.label193, 7, 2);
            this.tableLayoutPanel13.Controls.Add(this.label194, 0, 0);
            this.tableLayoutPanel13.Controls.Add(this.V_13_1_1, 1, 4);
            this.tableLayoutPanel13.Controls.Add(this.label195, 0, 3);
            this.tableLayoutPanel13.Controls.Add(this.label196, 0, 4);
            this.tableLayoutPanel13.Controls.Add(this.label197, 0, 5);
            this.tableLayoutPanel13.Controls.Add(this.label198, 0, 6);
            this.tableLayoutPanel13.Controls.Add(this.label199, 0, 7);
            this.tableLayoutPanel13.Controls.Add(this.label200, 1, 3);
            this.tableLayoutPanel13.Controls.Add(this.label201, 2, 3);
            this.tableLayoutPanel13.Controls.Add(this.label202, 3, 3);
            this.tableLayoutPanel13.Controls.Add(this.label203, 4, 3);
            this.tableLayoutPanel13.Controls.Add(this.label204, 5, 3);
            this.tableLayoutPanel13.Controls.Add(this.label205, 6, 3);
            this.tableLayoutPanel13.Controls.Add(this.label206, 7, 3);
            this.tableLayoutPanel13.Controls.Add(this.V_13_2_1, 2, 4);
            this.tableLayoutPanel13.Controls.Add(this.V_13_3_1, 3, 4);
            this.tableLayoutPanel13.Controls.Add(this.V_13_4_1, 4, 4);
            this.tableLayoutPanel13.Controls.Add(this.V_13_5_1, 5, 4);
            this.tableLayoutPanel13.Controls.Add(this.V_13_6_1, 6, 4);
            this.tableLayoutPanel13.Controls.Add(this.V_13_7_1, 7, 4);
            this.tableLayoutPanel13.Controls.Add(this.V_13_7_2, 7, 5);
            this.tableLayoutPanel13.Controls.Add(this.V_13_7_3, 7, 6);
            this.tableLayoutPanel13.Controls.Add(this.V_13_7_4, 7, 7);
            this.tableLayoutPanel13.Controls.Add(this.V_13_6_2, 6, 5);
            this.tableLayoutPanel13.Controls.Add(this.V_13_6_3, 6, 6);
            this.tableLayoutPanel13.Controls.Add(this.V_13_6_4, 6, 7);
            this.tableLayoutPanel13.Controls.Add(this.V_13_5_2, 5, 5);
            this.tableLayoutPanel13.Controls.Add(this.V_13_5_3, 5, 6);
            this.tableLayoutPanel13.Controls.Add(this.V_13_5_4, 5, 7);
            this.tableLayoutPanel13.Controls.Add(this.V_13_4_2, 4, 5);
            this.tableLayoutPanel13.Controls.Add(this.V_13_4_3, 4, 6);
            this.tableLayoutPanel13.Controls.Add(this.V_13_4_4, 4, 7);
            this.tableLayoutPanel13.Controls.Add(this.V_13_3_2, 3, 5);
            this.tableLayoutPanel13.Controls.Add(this.V_13_3_3, 3, 6);
            this.tableLayoutPanel13.Controls.Add(this.V_13_3_4, 3, 7);
            this.tableLayoutPanel13.Controls.Add(this.V_13_2_2, 2, 5);
            this.tableLayoutPanel13.Controls.Add(this.V_13_2_3, 2, 6);
            this.tableLayoutPanel13.Controls.Add(this.V_13_2_4, 2, 7);
            this.tableLayoutPanel13.Controls.Add(this.V_13_1_4, 1, 7);
            this.tableLayoutPanel13.Controls.Add(this.V_13_1_3, 1, 6);
            this.tableLayoutPanel13.Controls.Add(this.V_13_1_2, 1, 5);
            this.tableLayoutPanel13.Location = new System.Drawing.Point(3, 4254);
            this.tableLayoutPanel13.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 8;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(630, 300);
            this.tableLayoutPanel13.TabIndex = 33;
            // 
            // label185
            // 
            this.label185.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label185.AutoSize = true;
            this.label185.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label185.Location = new System.Drawing.Point(1, 27);
            this.label185.Margin = new System.Windows.Forms.Padding(0);
            this.label185.Name = "label185";
            this.tableLayoutPanel13.SetRowSpan(this.label185, 2);
            this.label185.Size = new System.Drawing.Size(100, 116);
            this.label185.TabIndex = 0;
            this.label185.Text = "Nội dung\t";
            this.label185.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label186
            // 
            this.label186.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label186.AutoSize = true;
            this.tableLayoutPanel13.SetColumnSpan(this.label186, 7);
            this.label186.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label186.Location = new System.Drawing.Point(105, 27);
            this.label186.Margin = new System.Windows.Forms.Padding(3, 0, 20, 0);
            this.label186.Name = "label186";
            this.label186.Size = new System.Drawing.Size(510, 25);
            this.label186.TabIndex = 0;
            this.label186.Text = "Các khoản mục thuộc vốn chủ sở hữu";
            this.label186.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label187
            // 
            this.label187.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label187.AutoSize = true;
            this.label187.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label187.Location = new System.Drawing.Point(105, 53);
            this.label187.Name = "label187";
            this.label187.Size = new System.Drawing.Size(69, 90);
            this.label187.TabIndex = 0;
            this.label187.Text = "Vốn góp của chủ sở hữu";
            this.label187.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label188
            // 
            this.label188.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label188.AutoSize = true;
            this.label188.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label188.Location = new System.Drawing.Point(181, 53);
            this.label188.Name = "label188";
            this.label188.Size = new System.Drawing.Size(69, 90);
            this.label188.TabIndex = 0;
            this.label188.Text = "Thặng dư vốn cổ phần";
            this.label188.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label189
            // 
            this.label189.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label189.AutoSize = true;
            this.label189.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label189.Location = new System.Drawing.Point(257, 53);
            this.label189.Name = "label189";
            this.label189.Size = new System.Drawing.Size(69, 90);
            this.label189.TabIndex = 0;
            this.label189.Text = "Vốn khác của Chủ sở hữu";
            this.label189.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label190
            // 
            this.label190.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label190.AutoSize = true;
            this.label190.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label190.Location = new System.Drawing.Point(333, 53);
            this.label190.Name = "label190";
            this.label190.Size = new System.Drawing.Size(69, 90);
            this.label190.TabIndex = 0;
            this.label190.Text = "Cổ phiếu quỹ";
            this.label190.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label191
            // 
            this.label191.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label191.AutoSize = true;
            this.label191.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label191.Location = new System.Drawing.Point(409, 53);
            this.label191.Name = "label191";
            this.label191.Size = new System.Drawing.Size(69, 90);
            this.label191.TabIndex = 0;
            this.label191.Text = "Chênh lệch tỷ giá";
            this.label191.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label192
            // 
            this.label192.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label192.AutoSize = true;
            this.label192.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label192.Location = new System.Drawing.Point(485, 53);
            this.label192.Name = "label192";
            this.label192.Size = new System.Drawing.Size(69, 90);
            this.label192.TabIndex = 0;
            this.label192.Text = "Lợi nhuận sau thuế chưa phân phối và các quỹ";
            this.label192.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label193
            // 
            this.label193.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label193.AutoSize = true;
            this.label193.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label193.Location = new System.Drawing.Point(561, 53);
            this.label193.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.label193.Name = "label193";
            this.label193.Size = new System.Drawing.Size(64, 90);
            this.label193.TabIndex = 0;
            this.label193.Text = "Cộng";
            this.label193.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label194
            // 
            this.label194.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label194.AutoSize = true;
            this.tableLayoutPanel13.SetColumnSpan(this.label194, 8);
            this.label194.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label194.Location = new System.Drawing.Point(4, 1);
            this.label194.Margin = new System.Windows.Forms.Padding(3, 0, 20, 0);
            this.label194.Name = "label194";
            this.label194.Size = new System.Drawing.Size(611, 25);
            this.label194.TabIndex = 1;
            this.label194.Text = "13. Vốn chủ sở hữu";
            this.label194.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // V_13_1_1
            // 
            this.V_13_1_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_13_1_1.AutoSize = true;
            this.V_13_1_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_13_1_1.Location = new System.Drawing.Point(105, 175);
            this.V_13_1_1.Name = "V_13_1_1";
            this.V_13_1_1.Size = new System.Drawing.Size(69, 30);
            this.V_13_1_1.TabIndex = 360;
            this.V_13_1_1.Text = "0";
            this.V_13_1_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_13_1_1.Click += new System.EventHandler(this.number_edit);
            // 
            // label195
            // 
            this.label195.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label195.AutoSize = true;
            this.label195.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label195.Location = new System.Drawing.Point(4, 144);
            this.label195.Name = "label195";
            this.label195.Size = new System.Drawing.Size(94, 30);
            this.label195.TabIndex = 0;
            this.label195.Text = "A";
            this.label195.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label196
            // 
            this.label196.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label196.AutoSize = true;
            this.label196.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label196.Location = new System.Drawing.Point(4, 175);
            this.label196.Name = "label196";
            this.label196.Size = new System.Drawing.Size(94, 30);
            this.label196.TabIndex = 0;
            this.label196.Text = "Số dư đầu năm";
            this.label196.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label197
            // 
            this.label197.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label197.AutoSize = true;
            this.label197.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label197.Location = new System.Drawing.Point(4, 206);
            this.label197.Name = "label197";
            this.label197.Size = new System.Drawing.Size(94, 30);
            this.label197.TabIndex = 0;
            this.label197.Text = "Tăng vốn trong năm";
            this.label197.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label198
            // 
            this.label198.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label198.AutoSize = true;
            this.label198.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label198.Location = new System.Drawing.Point(4, 237);
            this.label198.Name = "label198";
            this.label198.Size = new System.Drawing.Size(94, 30);
            this.label198.TabIndex = 0;
            this.label198.Text = "Giảm vốn trong năm";
            this.label198.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label199
            // 
            this.label199.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label199.AutoSize = true;
            this.label199.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label199.Location = new System.Drawing.Point(4, 268);
            this.label199.Name = "label199";
            this.label199.Size = new System.Drawing.Size(94, 31);
            this.label199.TabIndex = 0;
            this.label199.Text = "Số dư cuối năm";
            this.label199.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label200
            // 
            this.label200.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label200.AutoSize = true;
            this.label200.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label200.Location = new System.Drawing.Point(105, 144);
            this.label200.Name = "label200";
            this.label200.Size = new System.Drawing.Size(69, 30);
            this.label200.TabIndex = 0;
            this.label200.Text = "1";
            this.label200.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label201
            // 
            this.label201.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label201.AutoSize = true;
            this.label201.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label201.Location = new System.Drawing.Point(181, 144);
            this.label201.Name = "label201";
            this.label201.Size = new System.Drawing.Size(69, 30);
            this.label201.TabIndex = 0;
            this.label201.Text = "2";
            this.label201.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label202
            // 
            this.label202.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label202.AutoSize = true;
            this.label202.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label202.Location = new System.Drawing.Point(257, 144);
            this.label202.Name = "label202";
            this.label202.Size = new System.Drawing.Size(69, 30);
            this.label202.TabIndex = 0;
            this.label202.Text = "3";
            this.label202.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label203
            // 
            this.label203.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label203.AutoSize = true;
            this.label203.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label203.Location = new System.Drawing.Point(333, 144);
            this.label203.Name = "label203";
            this.label203.Size = new System.Drawing.Size(69, 30);
            this.label203.TabIndex = 0;
            this.label203.Text = "4";
            this.label203.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label204
            // 
            this.label204.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label204.AutoSize = true;
            this.label204.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label204.Location = new System.Drawing.Point(409, 144);
            this.label204.Name = "label204";
            this.label204.Size = new System.Drawing.Size(69, 30);
            this.label204.TabIndex = 0;
            this.label204.Text = "5";
            this.label204.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label205
            // 
            this.label205.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label205.AutoSize = true;
            this.label205.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label205.Location = new System.Drawing.Point(485, 144);
            this.label205.Name = "label205";
            this.label205.Size = new System.Drawing.Size(69, 30);
            this.label205.TabIndex = 0;
            this.label205.Text = "6";
            this.label205.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label206
            // 
            this.label206.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label206.AutoSize = true;
            this.label206.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label206.Location = new System.Drawing.Point(561, 144);
            this.label206.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.label206.Name = "label206";
            this.label206.Size = new System.Drawing.Size(64, 30);
            this.label206.TabIndex = 0;
            this.label206.Text = "7";
            this.label206.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // V_13_2_1
            // 
            this.V_13_2_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_13_2_1.AutoSize = true;
            this.V_13_2_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_13_2_1.Location = new System.Drawing.Point(181, 175);
            this.V_13_2_1.Name = "V_13_2_1";
            this.V_13_2_1.Size = new System.Drawing.Size(69, 30);
            this.V_13_2_1.TabIndex = 361;
            this.V_13_2_1.Text = "0";
            this.V_13_2_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_13_2_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_13_3_1
            // 
            this.V_13_3_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_13_3_1.AutoSize = true;
            this.V_13_3_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_13_3_1.Location = new System.Drawing.Point(257, 175);
            this.V_13_3_1.Name = "V_13_3_1";
            this.V_13_3_1.Size = new System.Drawing.Size(69, 30);
            this.V_13_3_1.TabIndex = 362;
            this.V_13_3_1.Text = "0";
            this.V_13_3_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_13_3_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_13_4_1
            // 
            this.V_13_4_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_13_4_1.AutoSize = true;
            this.V_13_4_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_13_4_1.Location = new System.Drawing.Point(333, 175);
            this.V_13_4_1.Name = "V_13_4_1";
            this.V_13_4_1.Size = new System.Drawing.Size(69, 30);
            this.V_13_4_1.TabIndex = 363;
            this.V_13_4_1.Text = "0";
            this.V_13_4_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_13_4_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_13_5_1
            // 
            this.V_13_5_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_13_5_1.AutoSize = true;
            this.V_13_5_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_13_5_1.Location = new System.Drawing.Point(409, 175);
            this.V_13_5_1.Name = "V_13_5_1";
            this.V_13_5_1.Size = new System.Drawing.Size(69, 30);
            this.V_13_5_1.TabIndex = 364;
            this.V_13_5_1.Text = "0";
            this.V_13_5_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_13_5_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_13_6_1
            // 
            this.V_13_6_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_13_6_1.AutoSize = true;
            this.V_13_6_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_13_6_1.Location = new System.Drawing.Point(485, 175);
            this.V_13_6_1.Name = "V_13_6_1";
            this.V_13_6_1.Size = new System.Drawing.Size(69, 30);
            this.V_13_6_1.TabIndex = 365;
            this.V_13_6_1.Text = "0";
            this.V_13_6_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_13_6_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_13_7_1
            // 
            this.V_13_7_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_13_7_1.AutoSize = true;
            this.V_13_7_1.Location = new System.Drawing.Point(561, 175);
            this.V_13_7_1.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.V_13_7_1.Name = "V_13_7_1";
            this.V_13_7_1.Size = new System.Drawing.Size(64, 30);
            this.V_13_7_1.TabIndex = 366;
            this.V_13_7_1.Text = "0";
            this.V_13_7_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_13_7_2
            // 
            this.V_13_7_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_13_7_2.AutoSize = true;
            this.V_13_7_2.Location = new System.Drawing.Point(561, 206);
            this.V_13_7_2.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.V_13_7_2.Name = "V_13_7_2";
            this.V_13_7_2.Size = new System.Drawing.Size(64, 30);
            this.V_13_7_2.TabIndex = 373;
            this.V_13_7_2.Text = "0";
            this.V_13_7_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_13_7_3
            // 
            this.V_13_7_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_13_7_3.AutoSize = true;
            this.V_13_7_3.Location = new System.Drawing.Point(561, 237);
            this.V_13_7_3.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.V_13_7_3.Name = "V_13_7_3";
            this.V_13_7_3.Size = new System.Drawing.Size(64, 30);
            this.V_13_7_3.TabIndex = 380;
            this.V_13_7_3.Text = "0";
            this.V_13_7_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_13_7_4
            // 
            this.V_13_7_4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_13_7_4.AutoSize = true;
            this.V_13_7_4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.V_13_7_4.Location = new System.Drawing.Point(561, 268);
            this.V_13_7_4.Margin = new System.Windows.Forms.Padding(3, 0, 10, 0);
            this.V_13_7_4.Name = "V_13_7_4";
            this.V_13_7_4.Size = new System.Drawing.Size(64, 31);
            this.V_13_7_4.TabIndex = 387;
            this.V_13_7_4.Text = "0";
            this.V_13_7_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_13_6_2
            // 
            this.V_13_6_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_13_6_2.AutoSize = true;
            this.V_13_6_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_13_6_2.Location = new System.Drawing.Point(485, 206);
            this.V_13_6_2.Name = "V_13_6_2";
            this.V_13_6_2.Size = new System.Drawing.Size(69, 30);
            this.V_13_6_2.TabIndex = 372;
            this.V_13_6_2.Text = "0";
            this.V_13_6_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_13_6_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_13_6_3
            // 
            this.V_13_6_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_13_6_3.AutoSize = true;
            this.V_13_6_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_13_6_3.Location = new System.Drawing.Point(485, 237);
            this.V_13_6_3.Name = "V_13_6_3";
            this.V_13_6_3.Size = new System.Drawing.Size(69, 30);
            this.V_13_6_3.TabIndex = 379;
            this.V_13_6_3.Text = "0";
            this.V_13_6_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_13_6_3.Click += new System.EventHandler(this.number_edit);
            // 
            // V_13_6_4
            // 
            this.V_13_6_4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_13_6_4.AutoSize = true;
            this.V_13_6_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_13_6_4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.V_13_6_4.Location = new System.Drawing.Point(485, 268);
            this.V_13_6_4.Name = "V_13_6_4";
            this.V_13_6_4.Size = new System.Drawing.Size(69, 31);
            this.V_13_6_4.TabIndex = 386;
            this.V_13_6_4.Text = "0";
            this.V_13_6_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_13_6_4.Click += new System.EventHandler(this.number_edit);
            // 
            // V_13_5_2
            // 
            this.V_13_5_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_13_5_2.AutoSize = true;
            this.V_13_5_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_13_5_2.Location = new System.Drawing.Point(409, 206);
            this.V_13_5_2.Name = "V_13_5_2";
            this.V_13_5_2.Size = new System.Drawing.Size(69, 30);
            this.V_13_5_2.TabIndex = 371;
            this.V_13_5_2.Text = "0";
            this.V_13_5_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_13_5_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_13_5_3
            // 
            this.V_13_5_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_13_5_3.AutoSize = true;
            this.V_13_5_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_13_5_3.Location = new System.Drawing.Point(409, 237);
            this.V_13_5_3.Name = "V_13_5_3";
            this.V_13_5_3.Size = new System.Drawing.Size(69, 30);
            this.V_13_5_3.TabIndex = 378;
            this.V_13_5_3.Text = "0";
            this.V_13_5_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_13_5_3.Click += new System.EventHandler(this.number_edit);
            // 
            // V_13_5_4
            // 
            this.V_13_5_4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_13_5_4.AutoSize = true;
            this.V_13_5_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_13_5_4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.V_13_5_4.Location = new System.Drawing.Point(409, 268);
            this.V_13_5_4.Name = "V_13_5_4";
            this.V_13_5_4.Size = new System.Drawing.Size(69, 31);
            this.V_13_5_4.TabIndex = 385;
            this.V_13_5_4.Text = "0";
            this.V_13_5_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_13_5_4.Click += new System.EventHandler(this.number_edit);
            // 
            // V_13_4_2
            // 
            this.V_13_4_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_13_4_2.AutoSize = true;
            this.V_13_4_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_13_4_2.Location = new System.Drawing.Point(333, 206);
            this.V_13_4_2.Name = "V_13_4_2";
            this.V_13_4_2.Size = new System.Drawing.Size(69, 30);
            this.V_13_4_2.TabIndex = 370;
            this.V_13_4_2.Text = "0";
            this.V_13_4_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_13_4_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_13_4_3
            // 
            this.V_13_4_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_13_4_3.AutoSize = true;
            this.V_13_4_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_13_4_3.Location = new System.Drawing.Point(333, 237);
            this.V_13_4_3.Name = "V_13_4_3";
            this.V_13_4_3.Size = new System.Drawing.Size(69, 30);
            this.V_13_4_3.TabIndex = 377;
            this.V_13_4_3.Text = "0";
            this.V_13_4_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_13_4_3.Click += new System.EventHandler(this.number_edit);
            // 
            // V_13_4_4
            // 
            this.V_13_4_4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_13_4_4.AutoSize = true;
            this.V_13_4_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_13_4_4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.V_13_4_4.Location = new System.Drawing.Point(333, 268);
            this.V_13_4_4.Name = "V_13_4_4";
            this.V_13_4_4.Size = new System.Drawing.Size(69, 31);
            this.V_13_4_4.TabIndex = 384;
            this.V_13_4_4.Text = "0";
            this.V_13_4_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_13_4_4.Click += new System.EventHandler(this.number_edit);
            // 
            // V_13_3_2
            // 
            this.V_13_3_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_13_3_2.AutoSize = true;
            this.V_13_3_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_13_3_2.Location = new System.Drawing.Point(257, 206);
            this.V_13_3_2.Name = "V_13_3_2";
            this.V_13_3_2.Size = new System.Drawing.Size(69, 30);
            this.V_13_3_2.TabIndex = 369;
            this.V_13_3_2.Text = "0";
            this.V_13_3_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_13_3_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_13_3_3
            // 
            this.V_13_3_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_13_3_3.AutoSize = true;
            this.V_13_3_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_13_3_3.Location = new System.Drawing.Point(257, 237);
            this.V_13_3_3.Name = "V_13_3_3";
            this.V_13_3_3.Size = new System.Drawing.Size(69, 30);
            this.V_13_3_3.TabIndex = 376;
            this.V_13_3_3.Text = "0";
            this.V_13_3_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_13_3_3.Click += new System.EventHandler(this.number_edit);
            // 
            // V_13_3_4
            // 
            this.V_13_3_4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_13_3_4.AutoSize = true;
            this.V_13_3_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_13_3_4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.V_13_3_4.Location = new System.Drawing.Point(257, 268);
            this.V_13_3_4.Name = "V_13_3_4";
            this.V_13_3_4.Size = new System.Drawing.Size(69, 31);
            this.V_13_3_4.TabIndex = 383;
            this.V_13_3_4.Text = "0";
            this.V_13_3_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_13_3_4.Click += new System.EventHandler(this.number_edit);
            // 
            // V_13_2_2
            // 
            this.V_13_2_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_13_2_2.AutoSize = true;
            this.V_13_2_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_13_2_2.Location = new System.Drawing.Point(181, 206);
            this.V_13_2_2.Name = "V_13_2_2";
            this.V_13_2_2.Size = new System.Drawing.Size(69, 30);
            this.V_13_2_2.TabIndex = 368;
            this.V_13_2_2.Text = "0";
            this.V_13_2_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_13_2_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_13_2_3
            // 
            this.V_13_2_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_13_2_3.AutoSize = true;
            this.V_13_2_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_13_2_3.Location = new System.Drawing.Point(181, 237);
            this.V_13_2_3.Name = "V_13_2_3";
            this.V_13_2_3.Size = new System.Drawing.Size(69, 30);
            this.V_13_2_3.TabIndex = 375;
            this.V_13_2_3.Text = "0";
            this.V_13_2_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_13_2_3.Click += new System.EventHandler(this.number_edit);
            // 
            // V_13_2_4
            // 
            this.V_13_2_4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_13_2_4.AutoSize = true;
            this.V_13_2_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_13_2_4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.V_13_2_4.Location = new System.Drawing.Point(181, 268);
            this.V_13_2_4.Name = "V_13_2_4";
            this.V_13_2_4.Size = new System.Drawing.Size(69, 31);
            this.V_13_2_4.TabIndex = 382;
            this.V_13_2_4.Text = "0";
            this.V_13_2_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_13_2_4.Click += new System.EventHandler(this.number_edit);
            // 
            // V_13_1_4
            // 
            this.V_13_1_4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_13_1_4.AutoSize = true;
            this.V_13_1_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_13_1_4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.V_13_1_4.Location = new System.Drawing.Point(105, 268);
            this.V_13_1_4.Name = "V_13_1_4";
            this.V_13_1_4.Size = new System.Drawing.Size(69, 31);
            this.V_13_1_4.TabIndex = 381;
            this.V_13_1_4.Text = "0";
            this.V_13_1_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_13_1_4.Click += new System.EventHandler(this.number_edit);
            // 
            // V_13_1_3
            // 
            this.V_13_1_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_13_1_3.AutoSize = true;
            this.V_13_1_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_13_1_3.Location = new System.Drawing.Point(105, 237);
            this.V_13_1_3.Name = "V_13_1_3";
            this.V_13_1_3.Size = new System.Drawing.Size(69, 30);
            this.V_13_1_3.TabIndex = 374;
            this.V_13_1_3.Text = "0";
            this.V_13_1_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_13_1_3.Click += new System.EventHandler(this.number_edit);
            // 
            // V_13_1_2
            // 
            this.V_13_1_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_13_1_2.AutoSize = true;
            this.V_13_1_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_13_1_2.Location = new System.Drawing.Point(105, 206);
            this.V_13_1_2.Name = "V_13_1_2";
            this.V_13_1_2.Size = new System.Drawing.Size(69, 30);
            this.V_13_1_2.TabIndex = 367;
            this.V_13_1_2.Text = "0";
            this.V_13_1_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_13_1_2.Click += new System.EventHandler(this.number_edit);
            // 
            // Sec_V_13_1
            // 
            this.Sec_V_13_1.AutoSize = true;
            this.Sec_V_13_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_V_13_1.Location = new System.Drawing.Point(3, 4574);
            this.Sec_V_13_1.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_V_13_1.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_V_13_1.Name = "Sec_V_13_1";
            this.Sec_V_13_1.Size = new System.Drawing.Size(484, 13);
            this.Sec_V_13_1.TabIndex = 34;
            this.Sec_V_13_1.Text = "- Thuyết minh và giải trình khác về vốn chủ sở hữu (Nguyên nhân biến động và các " +
    "thông tin khác): ";
            this.Sec_V_13_1.Click += new System.EventHandler(this.label_edit);
            // 
            // label208
            // 
            this.label208.AutoSize = true;
            this.label208.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label208.Location = new System.Drawing.Point(3, 4597);
            this.label208.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label208.Name = "label208";
            this.label208.Size = new System.Drawing.Size(257, 13);
            this.label208.TabIndex = 35;
            this.label208.Text = "14. Các khoản mục ngoài báo cáo tài chính";
            // 
            // Sec_V_14_1
            // 
            this.Sec_V_14_1.AutoSize = true;
            this.Sec_V_14_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_V_14_1.Location = new System.Drawing.Point(3, 4620);
            this.Sec_V_14_1.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_V_14_1.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_V_14_1.Name = "Sec_V_14_1";
            this.Sec_V_14_1.Size = new System.Drawing.Size(591, 13);
            this.Sec_V_14_1.TabIndex = 36;
            this.Sec_V_14_1.Text = "a) Tài sản thuê ngoài (Chi tiết số lượng, chủng loại và các thông tin quan trọng " +
    "khác đối với các tài sản thuê ngoài chủ yếu)";
            this.Sec_V_14_1.Click += new System.EventHandler(this.label_edit);
            // 
            // Sec_V_14_2
            // 
            this.Sec_V_14_2.AutoSize = true;
            this.Sec_V_14_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_V_14_2.Location = new System.Drawing.Point(3, 4643);
            this.Sec_V_14_2.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_V_14_2.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_V_14_2.Name = "Sec_V_14_2";
            this.Sec_V_14_2.Size = new System.Drawing.Size(620, 26);
            this.Sec_V_14_2.TabIndex = 37;
            this.Sec_V_14_2.Text = "b) Tài sản nhận giữ hộ (Doanh nghiệp phải thuyết minh chi tiết về số lượng, chủng" +
    " loại, quy cách, phẩm chất của từng loại tài sản tại thời điểm cuối kỳ)";
            this.Sec_V_14_2.Click += new System.EventHandler(this.label_edit);
            // 
            // Sec_V_14_3
            // 
            this.Sec_V_14_3.AutoSize = true;
            this.Sec_V_14_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_V_14_3.Location = new System.Drawing.Point(3, 4679);
            this.Sec_V_14_3.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_V_14_3.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_V_14_3.Name = "Sec_V_14_3";
            this.Sec_V_14_3.Size = new System.Drawing.Size(263, 13);
            this.Sec_V_14_3.TabIndex = 38;
            this.Sec_V_14_3.Text = "- Vật tư hàng hoá nhận giữ hộ, gia công, nhận uỷ thác";
            this.Sec_V_14_3.Click += new System.EventHandler(this.label_edit);
            // 
            // Sec_V_14_4
            // 
            this.Sec_V_14_4.AutoSize = true;
            this.Sec_V_14_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_V_14_4.Location = new System.Drawing.Point(3, 4702);
            this.Sec_V_14_4.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_V_14_4.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_V_14_4.Name = "Sec_V_14_4";
            this.Sec_V_14_4.Size = new System.Drawing.Size(300, 13);
            this.Sec_V_14_4.TabIndex = 39;
            this.Sec_V_14_4.Text = "- Hàng hoá nhận bán hộ, nhận ký gửi, nhận cầm cố, thế chấp";
            this.Sec_V_14_4.Click += new System.EventHandler(this.label_edit);
            // 
            // Sec_V_14_5
            // 
            this.Sec_V_14_5.AutoSize = true;
            this.Sec_V_14_5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_V_14_5.Location = new System.Drawing.Point(3, 4725);
            this.Sec_V_14_5.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_V_14_5.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_V_14_5.Name = "Sec_V_14_5";
            this.Sec_V_14_5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Sec_V_14_5.Size = new System.Drawing.Size(339, 13);
            this.Sec_V_14_5.TabIndex = 40;
            this.Sec_V_14_5.Text = "c) Ngoại tệ các loại: (Thuyết minh chi tiết số lượng từng loại nguyên tệ)";
            this.Sec_V_14_5.Click += new System.EventHandler(this.label_edit);
            // 
            // Sec_V_14_6
            // 
            this.Sec_V_14_6.AutoSize = true;
            this.Sec_V_14_6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_V_14_6.Location = new System.Drawing.Point(3, 4748);
            this.Sec_V_14_6.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_V_14_6.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_V_14_6.Name = "Sec_V_14_6";
            this.Sec_V_14_6.Size = new System.Drawing.Size(112, 13);
            this.Sec_V_14_6.TabIndex = 41;
            this.Sec_V_14_6.Text = "d) Nợ khó đòi đã xử lý";
            this.Sec_V_14_6.Click += new System.EventHandler(this.label_edit);
            // 
            // Sec_V_14_7
            // 
            this.Sec_V_14_7.AutoSize = true;
            this.Sec_V_14_7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_V_14_7.Location = new System.Drawing.Point(3, 4771);
            this.Sec_V_14_7.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_V_14_7.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_V_14_7.Name = "Sec_V_14_7";
            this.Sec_V_14_7.Size = new System.Drawing.Size(616, 26);
            this.Sec_V_14_7.TabIndex = 42;
            this.Sec_V_14_7.Text = "đ) Thông tin về các khoản tiền phạt, phải thu về lãi trả chậm,... phát sinh từ cá" +
    "c khoản nợ quá hạn nhưng không được ghi nhận doanh thu";
            this.Sec_V_14_7.Click += new System.EventHandler(this.label_edit);
            // 
            // Sec_V_14_8
            // 
            this.Sec_V_14_8.AutoSize = true;
            this.Sec_V_14_8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_V_14_8.Location = new System.Drawing.Point(3, 4807);
            this.Sec_V_14_8.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_V_14_8.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_V_14_8.Name = "Sec_V_14_8";
            this.Sec_V_14_8.Size = new System.Drawing.Size(361, 13);
            this.Sec_V_14_8.TabIndex = 43;
            this.Sec_V_14_8.Text = "e) Các thông tin khác về các khoản mục ngoài Báo cáo tình hình tài chính";
            this.Sec_V_14_8.Click += new System.EventHandler(this.label_edit);
            // 
            // label217
            // 
            this.label217.AutoSize = true;
            this.label217.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label217.Location = new System.Drawing.Point(3, 4830);
            this.label217.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label217.Name = "label217";
            this.label217.Size = new System.Drawing.Size(226, 13);
            this.label217.TabIndex = 44;
            this.label217.Text = "15. Thuyết minh về các bên liên quan ";
            // 
            // Sec_V_15_1
            // 
            this.Sec_V_15_1.AutoSize = true;
            this.Sec_V_15_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_V_15_1.Location = new System.Drawing.Point(3, 4853);
            this.Sec_V_15_1.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_V_15_1.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_V_15_1.Name = "Sec_V_15_1";
            this.Sec_V_15_1.Size = new System.Drawing.Size(624, 13);
            this.Sec_V_15_1.TabIndex = 45;
            this.Sec_V_15_1.Text = "(danh sách các bên liên quan, giao dịch và các thông tin khác về các bên liên qua" +
    "n chưa được trình bày ở các nội dung nêu trên)";
            this.Sec_V_15_1.Click += new System.EventHandler(this.label_edit);
            // 
            // label219
            // 
            this.label219.AutoSize = true;
            this.label219.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label219.Location = new System.Drawing.Point(3, 4876);
            this.label219.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label219.Name = "label219";
            this.label219.Size = new System.Drawing.Size(637, 26);
            this.label219.TabIndex = 46;
            this.label219.Text = "16. Ngoài các nội dung đã trình bày trên, các DN được giải trình thuyết minh các " +
    "thông tin khác nếu thấy cần thiết";
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel14.ColumnCount = 3;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 313F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel14.Controls.Add(this.label221, 0, 0);
            this.tableLayoutPanel14.Controls.Add(this.label222, 1, 0);
            this.tableLayoutPanel14.Controls.Add(this.label223, 2, 0);
            this.tableLayoutPanel14.Controls.Add(this.label224, 0, 1);
            this.tableLayoutPanel14.Controls.Add(this.label225, 0, 2);
            this.tableLayoutPanel14.Controls.Add(this.label226, 0, 3);
            this.tableLayoutPanel14.Controls.Add(this.label227, 0, 4);
            this.tableLayoutPanel14.Controls.Add(this.label228, 0, 5);
            this.tableLayoutPanel14.Controls.Add(this.label229, 0, 6);
            this.tableLayoutPanel14.Controls.Add(this.label230, 0, 7);
            this.tableLayoutPanel14.Controls.Add(this.label231, 0, 8);
            this.tableLayoutPanel14.Controls.Add(this.label232, 0, 9);
            this.tableLayoutPanel14.Controls.Add(this.label233, 0, 10);
            this.tableLayoutPanel14.Controls.Add(this.label234, 0, 11);
            this.tableLayoutPanel14.Controls.Add(this.label235, 0, 12);
            this.tableLayoutPanel14.Controls.Add(this.label236, 0, 13);
            this.tableLayoutPanel14.Controls.Add(this.label237, 0, 14);
            this.tableLayoutPanel14.Controls.Add(this.label238, 0, 15);
            this.tableLayoutPanel14.Controls.Add(this.label239, 0, 16);
            this.tableLayoutPanel14.Controls.Add(this.label240, 0, 17);
            this.tableLayoutPanel14.Controls.Add(this.label241, 0, 18);
            this.tableLayoutPanel14.Controls.Add(this.label242, 0, 19);
            this.tableLayoutPanel14.Controls.Add(this.label243, 0, 20);
            this.tableLayoutPanel14.Controls.Add(this.label244, 0, 21);
            this.tableLayoutPanel14.Controls.Add(this.label245, 0, 22);
            this.tableLayoutPanel14.Controls.Add(this.label246, 0, 23);
            this.tableLayoutPanel14.Controls.Add(this.label247, 0, 25);
            this.tableLayoutPanel14.Controls.Add(this.label248, 0, 24);
            this.tableLayoutPanel14.Controls.Add(this.label249, 0, 26);
            this.tableLayoutPanel14.Controls.Add(this.label250, 0, 27);
            this.tableLayoutPanel14.Controls.Add(this.label251, 0, 28);
            this.tableLayoutPanel14.Controls.Add(this.label252, 0, 29);
            this.tableLayoutPanel14.Controls.Add(this.label253, 0, 30);
            this.tableLayoutPanel14.Controls.Add(this.label254, 0, 31);
            this.tableLayoutPanel14.Controls.Add(this.label255, 0, 32);
            this.tableLayoutPanel14.Controls.Add(this.label256, 0, 33);
            this.tableLayoutPanel14.Controls.Add(this.label257, 0, 34);
            this.tableLayoutPanel14.Controls.Add(this.label258, 0, 35);
            this.tableLayoutPanel14.Controls.Add(this.label259, 0, 36);
            this.tableLayoutPanel14.Controls.Add(this.label260, 0, 37);
            this.tableLayoutPanel14.Controls.Add(this.label261, 0, 38);
            this.tableLayoutPanel14.Controls.Add(this.label262, 0, 39);
            this.tableLayoutPanel14.Controls.Add(this.label263, 0, 40);
            this.tableLayoutPanel14.Controls.Add(this.label264, 0, 41);
            this.tableLayoutPanel14.Controls.Add(this.label265, 0, 42);
            this.tableLayoutPanel14.Controls.Add(this.label266, 0, 43);
            this.tableLayoutPanel14.Controls.Add(this.label267, 0, 44);
            this.tableLayoutPanel14.Controls.Add(this.label268, 0, 45);
            this.tableLayoutPanel14.Controls.Add(this.label269, 0, 46);
            this.tableLayoutPanel14.Controls.Add(this.label270, 0, 47);
            this.tableLayoutPanel14.Controls.Add(this.label271, 0, 48);
            this.tableLayoutPanel14.Controls.Add(this.label272, 0, 49);
            this.tableLayoutPanel14.Controls.Add(this.label273, 0, 50);
            this.tableLayoutPanel14.Controls.Add(this.label274, 0, 51);
            this.tableLayoutPanel14.Controls.Add(this.label275, 0, 52);
            this.tableLayoutPanel14.Controls.Add(this.label276, 0, 53);
            this.tableLayoutPanel14.Controls.Add(this.label277, 0, 54);
            this.tableLayoutPanel14.Controls.Add(this.label278, 0, 55);
            this.tableLayoutPanel14.Controls.Add(this.label279, 0, 56);
            this.tableLayoutPanel14.Controls.Add(this.label280, 0, 57);
            this.tableLayoutPanel14.Controls.Add(this.label281, 0, 58);
            this.tableLayoutPanel14.Controls.Add(this.label282, 0, 59);
            this.tableLayoutPanel14.Controls.Add(this.label283, 0, 60);
            this.tableLayoutPanel14.Controls.Add(this.label292, 1, 9);
            this.tableLayoutPanel14.Controls.Add(this.label293, 2, 9);
            this.tableLayoutPanel14.Controls.Add(this.label294, 1, 14);
            this.tableLayoutPanel14.Controls.Add(this.label295, 2, 14);
            this.tableLayoutPanel14.Controls.Add(this.label296, 1, 22);
            this.tableLayoutPanel14.Controls.Add(this.label297, 2, 22);
            this.tableLayoutPanel14.Controls.Add(this.label298, 1, 30);
            this.tableLayoutPanel14.Controls.Add(this.label299, 2, 30);
            this.tableLayoutPanel14.Controls.Add(this.label300, 1, 38);
            this.tableLayoutPanel14.Controls.Add(this.label301, 2, 38);
            this.tableLayoutPanel14.Controls.Add(this.label302, 1, 44);
            this.tableLayoutPanel14.Controls.Add(this.label303, 2, 44);
            this.tableLayoutPanel14.Controls.Add(this.label304, 1, 51);
            this.tableLayoutPanel14.Controls.Add(this.label305, 2, 51);
            this.tableLayoutPanel14.Controls.Add(this.label306, 1, 57);
            this.tableLayoutPanel14.Controls.Add(this.label307, 2, 57);
            this.tableLayoutPanel14.Controls.Add(this.VI_1_a_2_1, 2, 2);
            this.tableLayoutPanel14.Controls.Add(this.VI_1_a_1_1, 1, 2);
            this.tableLayoutPanel14.Controls.Add(this.VI_1_a_1_2, 1, 3);
            this.tableLayoutPanel14.Controls.Add(this.VI_1_a_2_2, 2, 3);
            this.tableLayoutPanel14.Controls.Add(this.VI_1_a_2_3, 2, 4);
            this.tableLayoutPanel14.Controls.Add(this.VI_1_a_1_3, 1, 4);
            this.tableLayoutPanel14.Controls.Add(this.VI_1_a_1_4, 1, 5);
            this.tableLayoutPanel14.Controls.Add(this.VI_1_a_2_4, 2, 5);
            this.tableLayoutPanel14.Controls.Add(this.VI_1_a_Sum_2, 2, 6);
            this.tableLayoutPanel14.Controls.Add(this.VI_1_a_Sum_1, 1, 6);
            this.tableLayoutPanel14.Controls.Add(this.VI_1_b_2, 2, 7);
            this.tableLayoutPanel14.Controls.Add(this.VI_1_b_1, 1, 7);
            this.tableLayoutPanel14.Controls.Add(this.VI_1_c_1, 1, 8);
            this.tableLayoutPanel14.Controls.Add(this.VI_1_c_2, 2, 8);
            this.tableLayoutPanel14.Controls.Add(this.VI_2_1_1, 1, 10);
            this.tableLayoutPanel14.Controls.Add(this.VI_2_2_1, 2, 10);
            this.tableLayoutPanel14.Controls.Add(this.VI_2_2_2, 2, 11);
            this.tableLayoutPanel14.Controls.Add(this.VI_2_1_2, 1, 11);
            this.tableLayoutPanel14.Controls.Add(this.VI_2_1_3, 1, 12);
            this.tableLayoutPanel14.Controls.Add(this.VI_2_2_3, 2, 12);
            this.tableLayoutPanel14.Controls.Add(this.VI_2_Sum_2, 2, 13);
            this.tableLayoutPanel14.Controls.Add(this.VI_2_Sum_1, 1, 13);
            this.tableLayoutPanel14.Controls.Add(this.VI_3_1_1, 1, 15);
            this.tableLayoutPanel14.Controls.Add(this.VI_3_2_1, 2, 15);
            this.tableLayoutPanel14.Controls.Add(this.VI_3_2_2, 2, 16);
            this.tableLayoutPanel14.Controls.Add(this.VI_3_1_2, 1, 16);
            this.tableLayoutPanel14.Controls.Add(this.VI_3_1_4, 1, 18);
            this.tableLayoutPanel14.Controls.Add(this.VI_3_1_3, 1, 17);
            this.tableLayoutPanel14.Controls.Add(this.VI_3_2_3, 2, 17);
            this.tableLayoutPanel14.Controls.Add(this.VI_3_2_4, 2, 18);
            this.tableLayoutPanel14.Controls.Add(this.VI_3_2_5, 2, 19);
            this.tableLayoutPanel14.Controls.Add(this.VI_3_1_5, 1, 19);
            this.tableLayoutPanel14.Controls.Add(this.VI_3_1_6, 1, 20);
            this.tableLayoutPanel14.Controls.Add(this.VI_3_2_6, 2, 20);
            this.tableLayoutPanel14.Controls.Add(this.VI_3_Sum_2, 2, 21);
            this.tableLayoutPanel14.Controls.Add(this.VI_3_Sum_1, 1, 21);
            this.tableLayoutPanel14.Controls.Add(this.VI_4_1_1, 1, 23);
            this.tableLayoutPanel14.Controls.Add(this.VI_4_2_1, 2, 23);
            this.tableLayoutPanel14.Controls.Add(this.VI_4_2_2, 2, 24);
            this.tableLayoutPanel14.Controls.Add(this.VI_4_1_2, 1, 24);
            this.tableLayoutPanel14.Controls.Add(this.VI_4_1_3, 1, 25);
            this.tableLayoutPanel14.Controls.Add(this.VI_4_2_4, 2, 26);
            this.tableLayoutPanel14.Controls.Add(this.VI_4_2_3, 2, 25);
            this.tableLayoutPanel14.Controls.Add(this.VI_4_1_4, 1, 26);
            this.tableLayoutPanel14.Controls.Add(this.VI_4_1_5, 1, 27);
            this.tableLayoutPanel14.Controls.Add(this.VI_4_2_5, 2, 27);
            this.tableLayoutPanel14.Controls.Add(this.VI_4_2_6, 2, 28);
            this.tableLayoutPanel14.Controls.Add(this.VI_4_1_6, 1, 28);
            this.tableLayoutPanel14.Controls.Add(this.VI_4_Sum_1, 1, 29);
            this.tableLayoutPanel14.Controls.Add(this.VI_4_Sum_2, 2, 29);
            this.tableLayoutPanel14.Controls.Add(this.VI_5_2_1, 2, 31);
            this.tableLayoutPanel14.Controls.Add(this.VI_5_1_1, 1, 31);
            this.tableLayoutPanel14.Controls.Add(this.VI_5_1_2, 1, 32);
            this.tableLayoutPanel14.Controls.Add(this.VI_5_2_2, 2, 32);
            this.tableLayoutPanel14.Controls.Add(this.VI_5_2_3, 2, 33);
            this.tableLayoutPanel14.Controls.Add(this.VI_5_1_3, 1, 33);
            this.tableLayoutPanel14.Controls.Add(this.VI_5_1_4, 1, 34);
            this.tableLayoutPanel14.Controls.Add(this.VI_5_2_4, 2, 34);
            this.tableLayoutPanel14.Controls.Add(this.VI_5_2_6, 2, 36);
            this.tableLayoutPanel14.Controls.Add(this.VI_5_2_5, 2, 35);
            this.tableLayoutPanel14.Controls.Add(this.VI_5_2_7, 2, 37);
            this.tableLayoutPanel14.Controls.Add(this.VI_5_1_7, 1, 37);
            this.tableLayoutPanel14.Controls.Add(this.VI_5_1_6, 1, 36);
            this.tableLayoutPanel14.Controls.Add(this.VI_5_1_5, 1, 35);
            this.tableLayoutPanel14.Controls.Add(this.VI_6_a_1, 1, 39);
            this.tableLayoutPanel14.Controls.Add(this.VI_6_a_2, 2, 39);
            this.tableLayoutPanel14.Controls.Add(this.VI_6_b_2, 2, 40);
            this.tableLayoutPanel14.Controls.Add(this.VI_6_b_1, 1, 40);
            this.tableLayoutPanel14.Controls.Add(this.VI_6_c_1, 1, 41);
            this.tableLayoutPanel14.Controls.Add(this.VI_6_c_2, 2, 41);
            this.tableLayoutPanel14.Controls.Add(this.VI_6_c_2_2, 2, 43);
            this.tableLayoutPanel14.Controls.Add(this.VI_6_c_2_1, 2, 42);
            this.tableLayoutPanel14.Controls.Add(this.VI_6_c_1_1, 1, 42);
            this.tableLayoutPanel14.Controls.Add(this.VI_6_c_1_2, 1, 43);
            this.tableLayoutPanel14.Controls.Add(this.VI_7_1_1, 1, 45);
            this.tableLayoutPanel14.Controls.Add(this.VI_7_2_1, 2, 45);
            this.tableLayoutPanel14.Controls.Add(this.VI_7_2_2, 2, 46);
            this.tableLayoutPanel14.Controls.Add(this.VI_7_1_2, 1, 46);
            this.tableLayoutPanel14.Controls.Add(this.VI_7_1_3, 1, 47);
            this.tableLayoutPanel14.Controls.Add(this.VI_7_2_3, 2, 47);
            this.tableLayoutPanel14.Controls.Add(this.VI_7_2_4, 2, 48);
            this.tableLayoutPanel14.Controls.Add(this.VI_7_1_4, 1, 48);
            this.tableLayoutPanel14.Controls.Add(this.VI_7_1_5, 1, 49);
            this.tableLayoutPanel14.Controls.Add(this.VI_7_2_5, 2, 49);
            this.tableLayoutPanel14.Controls.Add(this.VI_7_Sum_2, 2, 50);
            this.tableLayoutPanel14.Controls.Add(this.VI_7_Sum_1, 1, 50);
            this.tableLayoutPanel14.Controls.Add(this.VI_8_1_1, 1, 52);
            this.tableLayoutPanel14.Controls.Add(this.VI_8_2_1, 2, 52);
            this.tableLayoutPanel14.Controls.Add(this.VI_8_2_2, 2, 53);
            this.tableLayoutPanel14.Controls.Add(this.VI_8_1_2, 1, 53);
            this.tableLayoutPanel14.Controls.Add(this.VI_8_1_3, 1, 54);
            this.tableLayoutPanel14.Controls.Add(this.VI_8_2_3, 2, 54);
            this.tableLayoutPanel14.Controls.Add(this.VI_8_2_4, 2, 55);
            this.tableLayoutPanel14.Controls.Add(this.VI_8_1_4, 1, 55);
            this.tableLayoutPanel14.Controls.Add(this.VI_8_Sum_1, 1, 56);
            this.tableLayoutPanel14.Controls.Add(this.VI_8_Sum_2, 2, 56);
            this.tableLayoutPanel14.Controls.Add(this.VI_9_2_1, 2, 58);
            this.tableLayoutPanel14.Controls.Add(this.VI_9_1_1, 1, 58);
            this.tableLayoutPanel14.Controls.Add(this.VI_9_1_2, 1, 59);
            this.tableLayoutPanel14.Controls.Add(this.VI_9_2_2, 2, 59);
            this.tableLayoutPanel14.Controls.Add(this.VI_9_2_3, 2, 60);
            this.tableLayoutPanel14.Controls.Add(this.VI_9_1_3, 1, 60);
            this.tableLayoutPanel14.Location = new System.Drawing.Point(3, 4958);
            this.tableLayoutPanel14.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 61;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(630, 1674);
            this.tableLayoutPanel14.TabIndex = 48;
            // 
            // label221
            // 
            this.label221.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label221.AutoSize = true;
            this.label221.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label221.Location = new System.Drawing.Point(4, 1);
            this.label221.Name = "label221";
            this.label221.Size = new System.Drawing.Size(307, 25);
            this.label221.TabIndex = 0;
            this.label221.Text = "1. Tổng Doanh thu bán hàng và cung cấp dịch vụ";
            this.label221.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label222
            // 
            this.label222.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label222.AutoSize = true;
            this.label222.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label222.Location = new System.Drawing.Point(318, 1);
            this.label222.Name = "label222";
            this.label222.Size = new System.Drawing.Size(150, 25);
            this.label222.TabIndex = 0;
            this.label222.Text = "Năm nay";
            this.label222.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label223
            // 
            this.label223.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label223.AutoSize = true;
            this.label223.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label223.Location = new System.Drawing.Point(475, 1);
            this.label223.Name = "label223";
            this.label223.Size = new System.Drawing.Size(151, 25);
            this.label223.TabIndex = 0;
            this.label223.Text = "Năm trước";
            this.label223.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label224
            // 
            this.label224.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label224.AutoSize = true;
            this.label224.Location = new System.Drawing.Point(4, 27);
            this.label224.Name = "label224";
            this.label224.Size = new System.Drawing.Size(307, 25);
            this.label224.TabIndex = 0;
            this.label224.Text = "a) Doanh thu";
            this.label224.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label225
            // 
            this.label225.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label225.AutoSize = true;
            this.label225.Location = new System.Drawing.Point(16, 53);
            this.label225.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label225.Name = "label225";
            this.label225.Size = new System.Drawing.Size(295, 25);
            this.label225.TabIndex = 0;
            this.label225.Text = "- Doanh thu bán hàng hoá";
            this.label225.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label226
            // 
            this.label226.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label226.AutoSize = true;
            this.label226.Location = new System.Drawing.Point(16, 79);
            this.label226.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label226.Name = "label226";
            this.label226.Size = new System.Drawing.Size(295, 25);
            this.label226.TabIndex = 0;
            this.label226.Text = "- Doanh thu bán thành phẩm";
            this.label226.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label227
            // 
            this.label227.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label227.AutoSize = true;
            this.label227.Location = new System.Drawing.Point(16, 105);
            this.label227.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label227.Name = "label227";
            this.label227.Size = new System.Drawing.Size(295, 25);
            this.label227.TabIndex = 0;
            this.label227.Text = "- Doanh thu cung cấp dịch vụ";
            this.label227.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label228
            // 
            this.label228.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label228.AutoSize = true;
            this.label228.Location = new System.Drawing.Point(16, 131);
            this.label228.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label228.Name = "label228";
            this.label228.Size = new System.Drawing.Size(295, 25);
            this.label228.TabIndex = 0;
            this.label228.Text = "- Doanh thu khác";
            this.label228.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label229
            // 
            this.label229.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label229.AutoSize = true;
            this.label229.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label229.Location = new System.Drawing.Point(51, 157);
            this.label229.Margin = new System.Windows.Forms.Padding(50, 0, 3, 0);
            this.label229.Name = "label229";
            this.label229.Size = new System.Drawing.Size(260, 25);
            this.label229.TabIndex = 0;
            this.label229.Text = "Cộng";
            this.label229.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label230
            // 
            this.label230.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label230.AutoSize = true;
            this.label230.Location = new System.Drawing.Point(4, 183);
            this.label230.Name = "label230";
            this.label230.Size = new System.Drawing.Size(307, 25);
            this.label230.TabIndex = 0;
            this.label230.Text = "b) Doanh thu từ các bên liên quan (chi tiết cho từng đối tượng)";
            this.label230.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label231
            // 
            this.label231.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label231.AutoSize = true;
            this.label231.Location = new System.Drawing.Point(4, 209);
            this.label231.Name = "label231";
            this.label231.Size = new System.Drawing.Size(307, 90);
            this.label231.TabIndex = 0;
            this.label231.Text = resources.GetString("label231.Text");
            this.label231.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label232
            // 
            this.label232.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label232.AutoSize = true;
            this.label232.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label232.Location = new System.Drawing.Point(4, 300);
            this.label232.Name = "label232";
            this.label232.Size = new System.Drawing.Size(307, 25);
            this.label232.TabIndex = 0;
            this.label232.Text = "2. Các khoản giảm trừ Doanh thu";
            this.label232.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label233
            // 
            this.label233.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label233.AutoSize = true;
            this.label233.Location = new System.Drawing.Point(16, 326);
            this.label233.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label233.Name = "label233";
            this.label233.Size = new System.Drawing.Size(295, 25);
            this.label233.TabIndex = 0;
            this.label233.Text = "- Chiết khấu thương mại";
            this.label233.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label234
            // 
            this.label234.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label234.AutoSize = true;
            this.label234.Location = new System.Drawing.Point(16, 352);
            this.label234.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label234.Name = "label234";
            this.label234.Size = new System.Drawing.Size(295, 25);
            this.label234.TabIndex = 0;
            this.label234.Text = "- Giảm giá hàng bán";
            this.label234.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label235
            // 
            this.label235.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label235.AutoSize = true;
            this.label235.Location = new System.Drawing.Point(16, 378);
            this.label235.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label235.Name = "label235";
            this.label235.Size = new System.Drawing.Size(295, 25);
            this.label235.TabIndex = 0;
            this.label235.Text = "- Hàng bán bị trả lại";
            this.label235.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label236
            // 
            this.label236.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label236.AutoSize = true;
            this.label236.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label236.Location = new System.Drawing.Point(51, 404);
            this.label236.Margin = new System.Windows.Forms.Padding(50, 0, 3, 0);
            this.label236.Name = "label236";
            this.label236.Size = new System.Drawing.Size(260, 25);
            this.label236.TabIndex = 0;
            this.label236.Text = "Cộng";
            this.label236.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label237
            // 
            this.label237.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label237.AutoSize = true;
            this.label237.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label237.Location = new System.Drawing.Point(4, 430);
            this.label237.Name = "label237";
            this.label237.Size = new System.Drawing.Size(307, 25);
            this.label237.TabIndex = 0;
            this.label237.Text = "3. Giá vốn hàng bán";
            this.label237.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label238
            // 
            this.label238.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label238.AutoSize = true;
            this.label238.Location = new System.Drawing.Point(16, 456);
            this.label238.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label238.Name = "label238";
            this.label238.Size = new System.Drawing.Size(295, 25);
            this.label238.TabIndex = 0;
            this.label238.Text = "- Giá vốn của hàng hoá đã bán";
            this.label238.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label239
            // 
            this.label239.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label239.AutoSize = true;
            this.label239.Location = new System.Drawing.Point(16, 482);
            this.label239.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label239.Name = "label239";
            this.label239.Size = new System.Drawing.Size(295, 25);
            this.label239.TabIndex = 0;
            this.label239.Text = "- Giá vốn của thành phẩm đã bán";
            this.label239.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label240
            // 
            this.label240.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label240.AutoSize = true;
            this.label240.Location = new System.Drawing.Point(16, 508);
            this.label240.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label240.Name = "label240";
            this.label240.Size = new System.Drawing.Size(295, 25);
            this.label240.TabIndex = 0;
            this.label240.Text = "- Giá vốn của dịch vụ đã cung cấp";
            this.label240.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label241
            // 
            this.label241.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label241.AutoSize = true;
            this.label241.Location = new System.Drawing.Point(16, 534);
            this.label241.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label241.Name = "label241";
            this.label241.Size = new System.Drawing.Size(295, 25);
            this.label241.TabIndex = 0;
            this.label241.Text = "- Giá vốn khác";
            this.label241.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label242
            // 
            this.label242.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label242.AutoSize = true;
            this.label242.Location = new System.Drawing.Point(16, 560);
            this.label242.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label242.Name = "label242";
            this.label242.Size = new System.Drawing.Size(295, 25);
            this.label242.TabIndex = 0;
            this.label242.Text = "- Các khoản chi phí khác được tính vào giá vốn";
            this.label242.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label243
            // 
            this.label243.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label243.AutoSize = true;
            this.label243.Location = new System.Drawing.Point(16, 586);
            this.label243.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label243.Name = "label243";
            this.label243.Size = new System.Drawing.Size(295, 25);
            this.label243.TabIndex = 0;
            this.label243.Text = "- Các khoản ghi giảm giá vốn hàng bán";
            this.label243.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label244
            // 
            this.label244.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label244.AutoSize = true;
            this.label244.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label244.Location = new System.Drawing.Point(51, 612);
            this.label244.Margin = new System.Windows.Forms.Padding(50, 0, 3, 0);
            this.label244.Name = "label244";
            this.label244.Size = new System.Drawing.Size(260, 25);
            this.label244.TabIndex = 0;
            this.label244.Text = "Cộng";
            this.label244.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label245
            // 
            this.label245.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label245.AutoSize = true;
            this.label245.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label245.Location = new System.Drawing.Point(4, 638);
            this.label245.Name = "label245";
            this.label245.Size = new System.Drawing.Size(307, 25);
            this.label245.TabIndex = 0;
            this.label245.Text = "4. Doanh thu hoạt động tài chính";
            this.label245.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label246
            // 
            this.label246.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label246.AutoSize = true;
            this.label246.Location = new System.Drawing.Point(16, 664);
            this.label246.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label246.Name = "label246";
            this.label246.Size = new System.Drawing.Size(295, 25);
            this.label246.TabIndex = 0;
            this.label246.Text = "- Lãi tiền gửi, tiền cho vay";
            this.label246.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label247
            // 
            this.label247.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label247.AutoSize = true;
            this.label247.Location = new System.Drawing.Point(16, 716);
            this.label247.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label247.Name = "label247";
            this.label247.Size = new System.Drawing.Size(295, 25);
            this.label247.TabIndex = 0;
            this.label247.Text = "- Cổ tức, lợi nhuận được chia";
            this.label247.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label248
            // 
            this.label248.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label248.AutoSize = true;
            this.label248.Location = new System.Drawing.Point(16, 690);
            this.label248.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label248.Name = "label248";
            this.label248.Size = new System.Drawing.Size(295, 25);
            this.label248.TabIndex = 0;
            this.label248.Text = "- Lãi bán các khoản đầu tư tài chính";
            this.label248.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label249
            // 
            this.label249.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label249.AutoSize = true;
            this.label249.Location = new System.Drawing.Point(16, 742);
            this.label249.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label249.Name = "label249";
            this.label249.Size = new System.Drawing.Size(295, 25);
            this.label249.TabIndex = 0;
            this.label249.Text = "- Lãi chênh lệch tỷ giá";
            this.label249.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label250
            // 
            this.label250.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label250.AutoSize = true;
            this.label250.Location = new System.Drawing.Point(16, 768);
            this.label250.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label250.Name = "label250";
            this.label250.Size = new System.Drawing.Size(295, 25);
            this.label250.TabIndex = 0;
            this.label250.Text = "- Lãi bán hàng trả chậm, chiết khấu thanh toán";
            this.label250.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label251
            // 
            this.label251.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label251.AutoSize = true;
            this.label251.Location = new System.Drawing.Point(16, 794);
            this.label251.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label251.Name = "label251";
            this.label251.Size = new System.Drawing.Size(295, 25);
            this.label251.TabIndex = 0;
            this.label251.Text = "- Doanh thu hoạt động tài chính khác";
            this.label251.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label252
            // 
            this.label252.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label252.AutoSize = true;
            this.label252.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label252.Location = new System.Drawing.Point(51, 820);
            this.label252.Margin = new System.Windows.Forms.Padding(50, 0, 3, 0);
            this.label252.Name = "label252";
            this.label252.Size = new System.Drawing.Size(260, 25);
            this.label252.TabIndex = 0;
            this.label252.Text = "Cộng";
            this.label252.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label253
            // 
            this.label253.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label253.AutoSize = true;
            this.label253.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label253.Location = new System.Drawing.Point(4, 846);
            this.label253.Name = "label253";
            this.label253.Size = new System.Drawing.Size(307, 25);
            this.label253.TabIndex = 0;
            this.label253.Text = "5. Chi phí tài chính";
            this.label253.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label254
            // 
            this.label254.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label254.AutoSize = true;
            this.label254.Location = new System.Drawing.Point(4, 872);
            this.label254.Name = "label254";
            this.label254.Size = new System.Drawing.Size(307, 25);
            this.label254.TabIndex = 0;
            this.label254.Text = "- Lãi tiền vay";
            this.label254.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label255
            // 
            this.label255.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label255.AutoSize = true;
            this.label255.Location = new System.Drawing.Point(4, 898);
            this.label255.Name = "label255";
            this.label255.Size = new System.Drawing.Size(307, 25);
            this.label255.TabIndex = 0;
            this.label255.Text = "- Chiết khấu thanh toán, lãi mua hàng trả chậm";
            this.label255.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label256
            // 
            this.label256.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label256.AutoSize = true;
            this.label256.Location = new System.Drawing.Point(4, 924);
            this.label256.Name = "label256";
            this.label256.Size = new System.Drawing.Size(307, 25);
            this.label256.TabIndex = 0;
            this.label256.Text = "- Lỗ do bán các khoản đầu tư tài chính";
            this.label256.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label257
            // 
            this.label257.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label257.AutoSize = true;
            this.label257.Location = new System.Drawing.Point(4, 950);
            this.label257.Name = "label257";
            this.label257.Size = new System.Drawing.Size(307, 25);
            this.label257.TabIndex = 0;
            this.label257.Text = "- Lỗ chênh lệch tỷ giá";
            this.label257.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label258
            // 
            this.label258.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label258.AutoSize = true;
            this.label258.Location = new System.Drawing.Point(4, 976);
            this.label258.Name = "label258";
            this.label258.Size = new System.Drawing.Size(307, 35);
            this.label258.TabIndex = 0;
            this.label258.Text = "- Dự phòng giảm giá chứng khoán kinh doanh và tổn thất đầu tư vào đơn vị khác";
            this.label258.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label259
            // 
            this.label259.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label259.AutoSize = true;
            this.label259.Location = new System.Drawing.Point(4, 1012);
            this.label259.Name = "label259";
            this.label259.Size = new System.Drawing.Size(307, 25);
            this.label259.TabIndex = 0;
            this.label259.Text = "- Chi phí tài chính khác";
            this.label259.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label260
            // 
            this.label260.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label260.AutoSize = true;
            this.label260.Location = new System.Drawing.Point(4, 1038);
            this.label260.Name = "label260";
            this.label260.Size = new System.Drawing.Size(307, 25);
            this.label260.TabIndex = 0;
            this.label260.Text = "- Các khoản ghi giảm chi phí tài chính";
            this.label260.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label261
            // 
            this.label261.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label261.AutoSize = true;
            this.label261.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label261.Location = new System.Drawing.Point(4, 1064);
            this.label261.Name = "label261";
            this.label261.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label261.Size = new System.Drawing.Size(307, 25);
            this.label261.TabIndex = 0;
            this.label261.Text = "6. Chi phí quản lý kinh doanh";
            this.label261.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label262
            // 
            this.label262.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label262.AutoSize = true;
            this.label262.Location = new System.Drawing.Point(4, 1090);
            this.label262.Name = "label262";
            this.label262.Size = new System.Drawing.Size(307, 25);
            this.label262.TabIndex = 0;
            this.label262.Text = "a. Các khoản chi phí quản lý doanh nghiệp phát sinh trong kỳ";
            this.label262.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label263
            // 
            this.label263.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label263.AutoSize = true;
            this.label263.Location = new System.Drawing.Point(4, 1116);
            this.label263.Name = "label263";
            this.label263.Size = new System.Drawing.Size(307, 25);
            this.label263.TabIndex = 0;
            this.label263.Text = "b. Các khoản chi phí bán hàng phát sinh trong kỳ";
            this.label263.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label264
            // 
            this.label264.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label264.AutoSize = true;
            this.label264.Location = new System.Drawing.Point(4, 1142);
            this.label264.Name = "label264";
            this.label264.Size = new System.Drawing.Size(307, 25);
            this.label264.TabIndex = 0;
            this.label264.Text = "c. Các khoản ghi giảm chi phí quản lý kinh doanh";
            this.label264.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label265
            // 
            this.label265.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label265.AutoSize = true;
            this.label265.Location = new System.Drawing.Point(16, 1168);
            this.label265.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label265.Name = "label265";
            this.label265.Size = new System.Drawing.Size(295, 25);
            this.label265.TabIndex = 0;
            this.label265.Text = "- Hoàn nhập các khoản dự phòng";
            this.label265.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label266
            // 
            this.label266.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label266.AutoSize = true;
            this.label266.Location = new System.Drawing.Point(16, 1194);
            this.label266.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label266.Name = "label266";
            this.label266.Size = new System.Drawing.Size(295, 25);
            this.label266.TabIndex = 0;
            this.label266.Text = "- Các khoản ghi giảm khác";
            this.label266.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label267
            // 
            this.label267.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label267.AutoSize = true;
            this.label267.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label267.Location = new System.Drawing.Point(4, 1220);
            this.label267.Name = "label267";
            this.label267.Size = new System.Drawing.Size(307, 25);
            this.label267.TabIndex = 0;
            this.label267.Text = "7. Thu nhập khác";
            this.label267.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label268
            // 
            this.label268.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label268.AutoSize = true;
            this.label268.Location = new System.Drawing.Point(16, 1246);
            this.label268.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label268.Name = "label268";
            this.label268.Size = new System.Drawing.Size(295, 25);
            this.label268.TabIndex = 0;
            this.label268.Text = "- Lãi Thanh lý/ nhượng bán TSCĐ";
            this.label268.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label269
            // 
            this.label269.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label269.AutoSize = true;
            this.label269.Location = new System.Drawing.Point(16, 1272);
            this.label269.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label269.Name = "label269";
            this.label269.Size = new System.Drawing.Size(295, 25);
            this.label269.TabIndex = 0;
            this.label269.Text = "- Lãi do đánh giá lại TSCĐ";
            this.label269.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label270
            // 
            this.label270.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label270.AutoSize = true;
            this.label270.Location = new System.Drawing.Point(16, 1298);
            this.label270.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label270.Name = "label270";
            this.label270.Size = new System.Drawing.Size(295, 25);
            this.label270.TabIndex = 0;
            this.label270.Text = "- Tiền phạt thu được";
            this.label270.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label271
            // 
            this.label271.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label271.AutoSize = true;
            this.label271.Location = new System.Drawing.Point(16, 1324);
            this.label271.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label271.Name = "label271";
            this.label271.Size = new System.Drawing.Size(295, 25);
            this.label271.TabIndex = 0;
            this.label271.Text = "- Thuế được giảm, được hoàn";
            this.label271.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label272
            // 
            this.label272.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label272.AutoSize = true;
            this.label272.Location = new System.Drawing.Point(16, 1350);
            this.label272.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label272.Name = "label272";
            this.label272.Size = new System.Drawing.Size(295, 25);
            this.label272.TabIndex = 0;
            this.label272.Text = "- Các khoản khác";
            this.label272.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label273
            // 
            this.label273.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label273.AutoSize = true;
            this.label273.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label273.Location = new System.Drawing.Point(51, 1376);
            this.label273.Margin = new System.Windows.Forms.Padding(50, 0, 3, 0);
            this.label273.Name = "label273";
            this.label273.Size = new System.Drawing.Size(260, 25);
            this.label273.TabIndex = 0;
            this.label273.Text = "Cộng";
            this.label273.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label274
            // 
            this.label274.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label274.AutoSize = true;
            this.label274.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label274.Location = new System.Drawing.Point(4, 1402);
            this.label274.Name = "label274";
            this.label274.Size = new System.Drawing.Size(307, 25);
            this.label274.TabIndex = 0;
            this.label274.Text = "8. Chi phí khác";
            this.label274.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label275
            // 
            this.label275.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label275.AutoSize = true;
            this.label275.Location = new System.Drawing.Point(16, 1428);
            this.label275.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label275.Name = "label275";
            this.label275.Size = new System.Drawing.Size(295, 25);
            this.label275.TabIndex = 0;
            this.label275.Text = "- Lỗ Thanh lý/ nhượng bán TSCĐ";
            this.label275.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label276
            // 
            this.label276.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label276.AutoSize = true;
            this.label276.Location = new System.Drawing.Point(16, 1454);
            this.label276.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label276.Name = "label276";
            this.label276.Size = new System.Drawing.Size(295, 25);
            this.label276.TabIndex = 0;
            this.label276.Text = "- Lỗ do đánh giá lại TSCĐ";
            this.label276.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label277
            // 
            this.label277.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label277.AutoSize = true;
            this.label277.Location = new System.Drawing.Point(16, 1480);
            this.label277.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label277.Name = "label277";
            this.label277.Size = new System.Drawing.Size(295, 25);
            this.label277.TabIndex = 0;
            this.label277.Text = "- Các khoản bị phạt";
            this.label277.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label278
            // 
            this.label278.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label278.AutoSize = true;
            this.label278.Location = new System.Drawing.Point(16, 1506);
            this.label278.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label278.Name = "label278";
            this.label278.Size = new System.Drawing.Size(295, 25);
            this.label278.TabIndex = 0;
            this.label278.Text = "- Các khoản khác";
            this.label278.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label279
            // 
            this.label279.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label279.AutoSize = true;
            this.label279.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label279.Location = new System.Drawing.Point(51, 1532);
            this.label279.Margin = new System.Windows.Forms.Padding(50, 0, 3, 0);
            this.label279.Name = "label279";
            this.label279.Size = new System.Drawing.Size(260, 25);
            this.label279.TabIndex = 0;
            this.label279.Text = "Cộng";
            this.label279.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label280
            // 
            this.label280.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label280.AutoSize = true;
            this.label280.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label280.Location = new System.Drawing.Point(4, 1558);
            this.label280.Name = "label280";
            this.label280.Size = new System.Drawing.Size(307, 25);
            this.label280.TabIndex = 0;
            this.label280.Text = "9. Chi phí thuế thu nhập doanh nghiệp hiện hành";
            this.label280.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label281
            // 
            this.label281.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label281.AutoSize = true;
            this.label281.Location = new System.Drawing.Point(16, 1584);
            this.label281.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label281.Name = "label281";
            this.label281.Size = new System.Drawing.Size(295, 25);
            this.label281.TabIndex = 0;
            this.label281.Text = "- Chi phí thuế TNDN tính trên thu nhập chịu thuế hiện hành";
            this.label281.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label282
            // 
            this.label282.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label282.AutoSize = true;
            this.label282.Location = new System.Drawing.Point(16, 1610);
            this.label282.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label282.Name = "label282";
            this.label282.Size = new System.Drawing.Size(295, 35);
            this.label282.TabIndex = 0;
            this.label282.Text = "- Điều chỉnh chi phí thuế TNDN của các năm trước vào chi phí thuế TNDN năm hiện h" +
    "ành";
            this.label282.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label283
            // 
            this.label283.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label283.AutoSize = true;
            this.label283.Location = new System.Drawing.Point(16, 1646);
            this.label283.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label283.Name = "label283";
            this.label283.Size = new System.Drawing.Size(295, 27);
            this.label283.TabIndex = 0;
            this.label283.Text = "- Tổng chi phí thuế TNDN hiện hành";
            this.label283.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label292
            // 
            this.label292.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label292.AutoSize = true;
            this.label292.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label292.Location = new System.Drawing.Point(318, 300);
            this.label292.Name = "label292";
            this.label292.Size = new System.Drawing.Size(150, 25);
            this.label292.TabIndex = 0;
            this.label292.Text = "Năm nay";
            this.label292.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label293
            // 
            this.label293.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label293.AutoSize = true;
            this.label293.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label293.Location = new System.Drawing.Point(475, 300);
            this.label293.Name = "label293";
            this.label293.Size = new System.Drawing.Size(151, 25);
            this.label293.TabIndex = 0;
            this.label293.Text = "Năm trước";
            this.label293.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label294
            // 
            this.label294.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label294.AutoSize = true;
            this.label294.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label294.Location = new System.Drawing.Point(318, 430);
            this.label294.Name = "label294";
            this.label294.Size = new System.Drawing.Size(150, 25);
            this.label294.TabIndex = 0;
            this.label294.Text = "Năm nay";
            this.label294.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label295
            // 
            this.label295.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label295.AutoSize = true;
            this.label295.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label295.Location = new System.Drawing.Point(475, 430);
            this.label295.Name = "label295";
            this.label295.Size = new System.Drawing.Size(151, 25);
            this.label295.TabIndex = 0;
            this.label295.Text = "Năm trước";
            this.label295.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label296
            // 
            this.label296.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label296.AutoSize = true;
            this.label296.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label296.Location = new System.Drawing.Point(318, 638);
            this.label296.Name = "label296";
            this.label296.Size = new System.Drawing.Size(150, 25);
            this.label296.TabIndex = 0;
            this.label296.Text = "Năm nay";
            this.label296.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label297
            // 
            this.label297.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label297.AutoSize = true;
            this.label297.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label297.Location = new System.Drawing.Point(475, 638);
            this.label297.Name = "label297";
            this.label297.Size = new System.Drawing.Size(151, 25);
            this.label297.TabIndex = 0;
            this.label297.Text = "Năm trước";
            this.label297.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label298
            // 
            this.label298.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label298.AutoSize = true;
            this.label298.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label298.Location = new System.Drawing.Point(318, 846);
            this.label298.Name = "label298";
            this.label298.Size = new System.Drawing.Size(150, 25);
            this.label298.TabIndex = 0;
            this.label298.Text = "Năm nay";
            this.label298.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label299
            // 
            this.label299.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label299.AutoSize = true;
            this.label299.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label299.Location = new System.Drawing.Point(475, 846);
            this.label299.Name = "label299";
            this.label299.Size = new System.Drawing.Size(151, 25);
            this.label299.TabIndex = 0;
            this.label299.Text = "Năm trước";
            this.label299.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label300
            // 
            this.label300.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label300.AutoSize = true;
            this.label300.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label300.Location = new System.Drawing.Point(318, 1064);
            this.label300.Name = "label300";
            this.label300.Size = new System.Drawing.Size(150, 25);
            this.label300.TabIndex = 0;
            this.label300.Text = "Năm nay";
            this.label300.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label301
            // 
            this.label301.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label301.AutoSize = true;
            this.label301.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label301.Location = new System.Drawing.Point(475, 1064);
            this.label301.Name = "label301";
            this.label301.Size = new System.Drawing.Size(151, 25);
            this.label301.TabIndex = 0;
            this.label301.Text = "Năm trước";
            this.label301.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label302
            // 
            this.label302.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label302.AutoSize = true;
            this.label302.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label302.Location = new System.Drawing.Point(318, 1220);
            this.label302.Name = "label302";
            this.label302.Size = new System.Drawing.Size(150, 25);
            this.label302.TabIndex = 0;
            this.label302.Text = "Năm nay";
            this.label302.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label303
            // 
            this.label303.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label303.AutoSize = true;
            this.label303.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label303.Location = new System.Drawing.Point(475, 1220);
            this.label303.Name = "label303";
            this.label303.Size = new System.Drawing.Size(151, 25);
            this.label303.TabIndex = 0;
            this.label303.Text = "Năm trước";
            this.label303.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label304
            // 
            this.label304.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label304.AutoSize = true;
            this.label304.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label304.Location = new System.Drawing.Point(318, 1402);
            this.label304.Name = "label304";
            this.label304.Size = new System.Drawing.Size(150, 25);
            this.label304.TabIndex = 0;
            this.label304.Text = "Năm nay";
            this.label304.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label305
            // 
            this.label305.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label305.AutoSize = true;
            this.label305.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label305.Location = new System.Drawing.Point(475, 1402);
            this.label305.Name = "label305";
            this.label305.Size = new System.Drawing.Size(151, 25);
            this.label305.TabIndex = 0;
            this.label305.Text = "Năm trước";
            this.label305.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label306
            // 
            this.label306.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label306.AutoSize = true;
            this.label306.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label306.Location = new System.Drawing.Point(318, 1558);
            this.label306.Name = "label306";
            this.label306.Size = new System.Drawing.Size(150, 25);
            this.label306.TabIndex = 0;
            this.label306.Text = "Năm nay";
            this.label306.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label307
            // 
            this.label307.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label307.AutoSize = true;
            this.label307.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label307.Location = new System.Drawing.Point(475, 1558);
            this.label307.Name = "label307";
            this.label307.Size = new System.Drawing.Size(151, 25);
            this.label307.TabIndex = 0;
            this.label307.Text = "Năm trước";
            this.label307.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // VI_1_a_2_1
            // 
            this.VI_1_a_2_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_1_a_2_1.AutoSize = true;
            this.VI_1_a_2_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_1_a_2_1.Location = new System.Drawing.Point(475, 53);
            this.VI_1_a_2_1.Name = "VI_1_a_2_1";
            this.VI_1_a_2_1.Size = new System.Drawing.Size(151, 25);
            this.VI_1_a_2_1.TabIndex = 391;
            this.VI_1_a_2_1.Text = "0";
            this.VI_1_a_2_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_1_a_2_1.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_1_a_1_1
            // 
            this.VI_1_a_1_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_1_a_1_1.AutoSize = true;
            this.VI_1_a_1_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_1_a_1_1.Location = new System.Drawing.Point(318, 53);
            this.VI_1_a_1_1.Name = "VI_1_a_1_1";
            this.VI_1_a_1_1.Size = new System.Drawing.Size(150, 25);
            this.VI_1_a_1_1.TabIndex = 390;
            this.VI_1_a_1_1.Text = "0";
            this.VI_1_a_1_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_1_a_1_1.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_1_a_1_2
            // 
            this.VI_1_a_1_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_1_a_1_2.AutoSize = true;
            this.VI_1_a_1_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_1_a_1_2.Location = new System.Drawing.Point(318, 79);
            this.VI_1_a_1_2.Name = "VI_1_a_1_2";
            this.VI_1_a_1_2.Size = new System.Drawing.Size(150, 25);
            this.VI_1_a_1_2.TabIndex = 392;
            this.VI_1_a_1_2.Text = "0";
            this.VI_1_a_1_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_1_a_1_2.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_1_a_2_2
            // 
            this.VI_1_a_2_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_1_a_2_2.AutoSize = true;
            this.VI_1_a_2_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_1_a_2_2.Location = new System.Drawing.Point(475, 79);
            this.VI_1_a_2_2.Name = "VI_1_a_2_2";
            this.VI_1_a_2_2.Size = new System.Drawing.Size(151, 25);
            this.VI_1_a_2_2.TabIndex = 393;
            this.VI_1_a_2_2.Text = "0";
            this.VI_1_a_2_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_1_a_2_2.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_1_a_2_3
            // 
            this.VI_1_a_2_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_1_a_2_3.AutoSize = true;
            this.VI_1_a_2_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_1_a_2_3.Location = new System.Drawing.Point(475, 105);
            this.VI_1_a_2_3.Name = "VI_1_a_2_3";
            this.VI_1_a_2_3.Size = new System.Drawing.Size(151, 25);
            this.VI_1_a_2_3.TabIndex = 395;
            this.VI_1_a_2_3.Text = "0";
            this.VI_1_a_2_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_1_a_2_3.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_1_a_1_3
            // 
            this.VI_1_a_1_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_1_a_1_3.AutoSize = true;
            this.VI_1_a_1_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_1_a_1_3.Location = new System.Drawing.Point(318, 105);
            this.VI_1_a_1_3.Name = "VI_1_a_1_3";
            this.VI_1_a_1_3.Size = new System.Drawing.Size(150, 25);
            this.VI_1_a_1_3.TabIndex = 394;
            this.VI_1_a_1_3.Text = "0";
            this.VI_1_a_1_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_1_a_1_3.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_1_a_1_4
            // 
            this.VI_1_a_1_4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_1_a_1_4.AutoSize = true;
            this.VI_1_a_1_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_1_a_1_4.Location = new System.Drawing.Point(318, 131);
            this.VI_1_a_1_4.Name = "VI_1_a_1_4";
            this.VI_1_a_1_4.Size = new System.Drawing.Size(150, 25);
            this.VI_1_a_1_4.TabIndex = 396;
            this.VI_1_a_1_4.Text = "0";
            this.VI_1_a_1_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_1_a_1_4.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_1_a_2_4
            // 
            this.VI_1_a_2_4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_1_a_2_4.AutoSize = true;
            this.VI_1_a_2_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_1_a_2_4.Location = new System.Drawing.Point(475, 131);
            this.VI_1_a_2_4.Name = "VI_1_a_2_4";
            this.VI_1_a_2_4.Size = new System.Drawing.Size(151, 25);
            this.VI_1_a_2_4.TabIndex = 397;
            this.VI_1_a_2_4.Text = "0";
            this.VI_1_a_2_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_1_a_2_4.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_1_a_Sum_2
            // 
            this.VI_1_a_Sum_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_1_a_Sum_2.AutoSize = true;
            this.VI_1_a_Sum_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VI_1_a_Sum_2.Location = new System.Drawing.Point(475, 157);
            this.VI_1_a_Sum_2.Name = "VI_1_a_Sum_2";
            this.VI_1_a_Sum_2.Size = new System.Drawing.Size(151, 25);
            this.VI_1_a_Sum_2.TabIndex = 399;
            this.VI_1_a_Sum_2.Text = "0";
            this.VI_1_a_Sum_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // VI_1_a_Sum_1
            // 
            this.VI_1_a_Sum_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_1_a_Sum_1.AutoSize = true;
            this.VI_1_a_Sum_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VI_1_a_Sum_1.Location = new System.Drawing.Point(318, 157);
            this.VI_1_a_Sum_1.Name = "VI_1_a_Sum_1";
            this.VI_1_a_Sum_1.Size = new System.Drawing.Size(150, 25);
            this.VI_1_a_Sum_1.TabIndex = 398;
            this.VI_1_a_Sum_1.Text = "0";
            this.VI_1_a_Sum_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // VI_1_b_2
            // 
            this.VI_1_b_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_1_b_2.AutoSize = true;
            this.VI_1_b_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_1_b_2.Location = new System.Drawing.Point(475, 183);
            this.VI_1_b_2.Name = "VI_1_b_2";
            this.VI_1_b_2.Size = new System.Drawing.Size(151, 25);
            this.VI_1_b_2.TabIndex = 401;
            this.VI_1_b_2.Text = "0";
            this.VI_1_b_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_1_b_2.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_1_b_1
            // 
            this.VI_1_b_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_1_b_1.AutoSize = true;
            this.VI_1_b_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_1_b_1.Location = new System.Drawing.Point(318, 183);
            this.VI_1_b_1.Name = "VI_1_b_1";
            this.VI_1_b_1.Size = new System.Drawing.Size(150, 25);
            this.VI_1_b_1.TabIndex = 400;
            this.VI_1_b_1.Text = "0";
            this.VI_1_b_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_1_b_1.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_1_c_1
            // 
            this.VI_1_c_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_1_c_1.AutoSize = true;
            this.VI_1_c_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_1_c_1.Location = new System.Drawing.Point(318, 209);
            this.VI_1_c_1.Name = "VI_1_c_1";
            this.VI_1_c_1.Size = new System.Drawing.Size(150, 90);
            this.VI_1_c_1.TabIndex = 402;
            this.VI_1_c_1.Text = "0";
            this.VI_1_c_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_1_c_1.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_1_c_2
            // 
            this.VI_1_c_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_1_c_2.AutoSize = true;
            this.VI_1_c_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_1_c_2.Location = new System.Drawing.Point(475, 209);
            this.VI_1_c_2.Name = "VI_1_c_2";
            this.VI_1_c_2.Size = new System.Drawing.Size(151, 90);
            this.VI_1_c_2.TabIndex = 403;
            this.VI_1_c_2.Text = "0";
            this.VI_1_c_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_1_c_2.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_2_1_1
            // 
            this.VI_2_1_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_2_1_1.AutoSize = true;
            this.VI_2_1_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_2_1_1.Location = new System.Drawing.Point(318, 326);
            this.VI_2_1_1.Name = "VI_2_1_1";
            this.VI_2_1_1.Size = new System.Drawing.Size(150, 25);
            this.VI_2_1_1.TabIndex = 404;
            this.VI_2_1_1.Text = "0";
            this.VI_2_1_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_2_1_1.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_2_2_1
            // 
            this.VI_2_2_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_2_2_1.AutoSize = true;
            this.VI_2_2_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_2_2_1.Location = new System.Drawing.Point(475, 326);
            this.VI_2_2_1.Name = "VI_2_2_1";
            this.VI_2_2_1.Size = new System.Drawing.Size(151, 25);
            this.VI_2_2_1.TabIndex = 405;
            this.VI_2_2_1.Text = "0";
            this.VI_2_2_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_2_2_1.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_2_2_2
            // 
            this.VI_2_2_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_2_2_2.AutoSize = true;
            this.VI_2_2_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_2_2_2.Location = new System.Drawing.Point(475, 352);
            this.VI_2_2_2.Name = "VI_2_2_2";
            this.VI_2_2_2.Size = new System.Drawing.Size(151, 25);
            this.VI_2_2_2.TabIndex = 407;
            this.VI_2_2_2.Text = "0";
            this.VI_2_2_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_2_2_2.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_2_1_2
            // 
            this.VI_2_1_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_2_1_2.AutoSize = true;
            this.VI_2_1_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_2_1_2.Location = new System.Drawing.Point(318, 352);
            this.VI_2_1_2.Name = "VI_2_1_2";
            this.VI_2_1_2.Size = new System.Drawing.Size(150, 25);
            this.VI_2_1_2.TabIndex = 406;
            this.VI_2_1_2.Text = "0";
            this.VI_2_1_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_2_1_2.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_2_1_3
            // 
            this.VI_2_1_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_2_1_3.AutoSize = true;
            this.VI_2_1_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_2_1_3.Location = new System.Drawing.Point(318, 378);
            this.VI_2_1_3.Name = "VI_2_1_3";
            this.VI_2_1_3.Size = new System.Drawing.Size(150, 25);
            this.VI_2_1_3.TabIndex = 408;
            this.VI_2_1_3.Text = "0";
            this.VI_2_1_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_2_1_3.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_2_2_3
            // 
            this.VI_2_2_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_2_2_3.AutoSize = true;
            this.VI_2_2_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_2_2_3.Location = new System.Drawing.Point(475, 378);
            this.VI_2_2_3.Name = "VI_2_2_3";
            this.VI_2_2_3.Size = new System.Drawing.Size(151, 25);
            this.VI_2_2_3.TabIndex = 409;
            this.VI_2_2_3.Text = "0";
            this.VI_2_2_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_2_2_3.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_2_Sum_2
            // 
            this.VI_2_Sum_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_2_Sum_2.AutoSize = true;
            this.VI_2_Sum_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VI_2_Sum_2.Location = new System.Drawing.Point(475, 404);
            this.VI_2_Sum_2.Name = "VI_2_Sum_2";
            this.VI_2_Sum_2.Size = new System.Drawing.Size(151, 25);
            this.VI_2_Sum_2.TabIndex = 411;
            this.VI_2_Sum_2.Text = "0";
            this.VI_2_Sum_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // VI_2_Sum_1
            // 
            this.VI_2_Sum_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_2_Sum_1.AutoSize = true;
            this.VI_2_Sum_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VI_2_Sum_1.Location = new System.Drawing.Point(318, 404);
            this.VI_2_Sum_1.Name = "VI_2_Sum_1";
            this.VI_2_Sum_1.Size = new System.Drawing.Size(150, 25);
            this.VI_2_Sum_1.TabIndex = 410;
            this.VI_2_Sum_1.Text = "0";
            this.VI_2_Sum_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // VI_3_1_1
            // 
            this.VI_3_1_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_3_1_1.AutoSize = true;
            this.VI_3_1_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_3_1_1.Location = new System.Drawing.Point(318, 456);
            this.VI_3_1_1.Name = "VI_3_1_1";
            this.VI_3_1_1.Size = new System.Drawing.Size(150, 25);
            this.VI_3_1_1.TabIndex = 412;
            this.VI_3_1_1.Text = "0";
            this.VI_3_1_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_3_1_1.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_3_2_1
            // 
            this.VI_3_2_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_3_2_1.AutoSize = true;
            this.VI_3_2_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_3_2_1.Location = new System.Drawing.Point(475, 456);
            this.VI_3_2_1.Name = "VI_3_2_1";
            this.VI_3_2_1.Size = new System.Drawing.Size(151, 25);
            this.VI_3_2_1.TabIndex = 413;
            this.VI_3_2_1.Text = "0";
            this.VI_3_2_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_3_2_1.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_3_2_2
            // 
            this.VI_3_2_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_3_2_2.AutoSize = true;
            this.VI_3_2_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_3_2_2.Location = new System.Drawing.Point(475, 482);
            this.VI_3_2_2.Name = "VI_3_2_2";
            this.VI_3_2_2.Size = new System.Drawing.Size(151, 25);
            this.VI_3_2_2.TabIndex = 415;
            this.VI_3_2_2.Text = "0";
            this.VI_3_2_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_3_2_2.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_3_1_2
            // 
            this.VI_3_1_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_3_1_2.AutoSize = true;
            this.VI_3_1_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_3_1_2.Location = new System.Drawing.Point(318, 482);
            this.VI_3_1_2.Name = "VI_3_1_2";
            this.VI_3_1_2.Size = new System.Drawing.Size(150, 25);
            this.VI_3_1_2.TabIndex = 414;
            this.VI_3_1_2.Text = "0";
            this.VI_3_1_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_3_1_2.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_3_1_4
            // 
            this.VI_3_1_4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_3_1_4.AutoSize = true;
            this.VI_3_1_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_3_1_4.Location = new System.Drawing.Point(318, 534);
            this.VI_3_1_4.Name = "VI_3_1_4";
            this.VI_3_1_4.Size = new System.Drawing.Size(150, 25);
            this.VI_3_1_4.TabIndex = 418;
            this.VI_3_1_4.Text = "0";
            this.VI_3_1_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_3_1_4.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_3_1_3
            // 
            this.VI_3_1_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_3_1_3.AutoSize = true;
            this.VI_3_1_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_3_1_3.Location = new System.Drawing.Point(318, 508);
            this.VI_3_1_3.Name = "VI_3_1_3";
            this.VI_3_1_3.Size = new System.Drawing.Size(150, 25);
            this.VI_3_1_3.TabIndex = 416;
            this.VI_3_1_3.Text = "0";
            this.VI_3_1_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_3_1_3.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_3_2_3
            // 
            this.VI_3_2_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_3_2_3.AutoSize = true;
            this.VI_3_2_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_3_2_3.Location = new System.Drawing.Point(475, 508);
            this.VI_3_2_3.Name = "VI_3_2_3";
            this.VI_3_2_3.Size = new System.Drawing.Size(151, 25);
            this.VI_3_2_3.TabIndex = 417;
            this.VI_3_2_3.Text = "0";
            this.VI_3_2_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_3_2_3.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_3_2_4
            // 
            this.VI_3_2_4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_3_2_4.AutoSize = true;
            this.VI_3_2_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_3_2_4.Location = new System.Drawing.Point(475, 534);
            this.VI_3_2_4.Name = "VI_3_2_4";
            this.VI_3_2_4.Size = new System.Drawing.Size(151, 25);
            this.VI_3_2_4.TabIndex = 419;
            this.VI_3_2_4.Text = "0";
            this.VI_3_2_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_3_2_4.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_3_2_5
            // 
            this.VI_3_2_5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_3_2_5.AutoSize = true;
            this.VI_3_2_5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_3_2_5.Location = new System.Drawing.Point(475, 560);
            this.VI_3_2_5.Name = "VI_3_2_5";
            this.VI_3_2_5.Size = new System.Drawing.Size(151, 25);
            this.VI_3_2_5.TabIndex = 421;
            this.VI_3_2_5.Text = "0";
            this.VI_3_2_5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_3_2_5.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_3_1_5
            // 
            this.VI_3_1_5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_3_1_5.AutoSize = true;
            this.VI_3_1_5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_3_1_5.Location = new System.Drawing.Point(318, 560);
            this.VI_3_1_5.Name = "VI_3_1_5";
            this.VI_3_1_5.Size = new System.Drawing.Size(150, 25);
            this.VI_3_1_5.TabIndex = 420;
            this.VI_3_1_5.Text = "0";
            this.VI_3_1_5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_3_1_5.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_3_1_6
            // 
            this.VI_3_1_6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_3_1_6.AutoSize = true;
            this.VI_3_1_6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_3_1_6.Location = new System.Drawing.Point(318, 586);
            this.VI_3_1_6.Name = "VI_3_1_6";
            this.VI_3_1_6.Size = new System.Drawing.Size(150, 25);
            this.VI_3_1_6.TabIndex = 422;
            this.VI_3_1_6.Text = "0";
            this.VI_3_1_6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_3_1_6.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_3_2_6
            // 
            this.VI_3_2_6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_3_2_6.AutoSize = true;
            this.VI_3_2_6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_3_2_6.Location = new System.Drawing.Point(475, 586);
            this.VI_3_2_6.Name = "VI_3_2_6";
            this.VI_3_2_6.Size = new System.Drawing.Size(151, 25);
            this.VI_3_2_6.TabIndex = 423;
            this.VI_3_2_6.Text = "0";
            this.VI_3_2_6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_3_2_6.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_3_Sum_2
            // 
            this.VI_3_Sum_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_3_Sum_2.AutoSize = true;
            this.VI_3_Sum_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VI_3_Sum_2.Location = new System.Drawing.Point(475, 612);
            this.VI_3_Sum_2.Name = "VI_3_Sum_2";
            this.VI_3_Sum_2.Size = new System.Drawing.Size(151, 25);
            this.VI_3_Sum_2.TabIndex = 425;
            this.VI_3_Sum_2.Text = "0";
            this.VI_3_Sum_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // VI_3_Sum_1
            // 
            this.VI_3_Sum_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_3_Sum_1.AutoSize = true;
            this.VI_3_Sum_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VI_3_Sum_1.Location = new System.Drawing.Point(318, 612);
            this.VI_3_Sum_1.Name = "VI_3_Sum_1";
            this.VI_3_Sum_1.Size = new System.Drawing.Size(150, 25);
            this.VI_3_Sum_1.TabIndex = 424;
            this.VI_3_Sum_1.Text = "0";
            this.VI_3_Sum_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // VI_4_1_1
            // 
            this.VI_4_1_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_4_1_1.AutoSize = true;
            this.VI_4_1_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_4_1_1.Location = new System.Drawing.Point(318, 664);
            this.VI_4_1_1.Name = "VI_4_1_1";
            this.VI_4_1_1.Size = new System.Drawing.Size(150, 25);
            this.VI_4_1_1.TabIndex = 426;
            this.VI_4_1_1.Text = "0";
            this.VI_4_1_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_4_1_1.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_4_2_1
            // 
            this.VI_4_2_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_4_2_1.AutoSize = true;
            this.VI_4_2_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_4_2_1.Location = new System.Drawing.Point(475, 664);
            this.VI_4_2_1.Name = "VI_4_2_1";
            this.VI_4_2_1.Size = new System.Drawing.Size(151, 25);
            this.VI_4_2_1.TabIndex = 427;
            this.VI_4_2_1.Text = "0";
            this.VI_4_2_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_4_2_1.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_4_2_2
            // 
            this.VI_4_2_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_4_2_2.AutoSize = true;
            this.VI_4_2_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_4_2_2.Location = new System.Drawing.Point(475, 690);
            this.VI_4_2_2.Name = "VI_4_2_2";
            this.VI_4_2_2.Size = new System.Drawing.Size(151, 25);
            this.VI_4_2_2.TabIndex = 429;
            this.VI_4_2_2.Text = "0";
            this.VI_4_2_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_4_2_2.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_4_1_2
            // 
            this.VI_4_1_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_4_1_2.AutoSize = true;
            this.VI_4_1_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_4_1_2.Location = new System.Drawing.Point(318, 690);
            this.VI_4_1_2.Name = "VI_4_1_2";
            this.VI_4_1_2.Size = new System.Drawing.Size(150, 25);
            this.VI_4_1_2.TabIndex = 428;
            this.VI_4_1_2.Text = "0";
            this.VI_4_1_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_4_1_2.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_4_1_3
            // 
            this.VI_4_1_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_4_1_3.AutoSize = true;
            this.VI_4_1_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_4_1_3.Location = new System.Drawing.Point(318, 716);
            this.VI_4_1_3.Name = "VI_4_1_3";
            this.VI_4_1_3.Size = new System.Drawing.Size(150, 25);
            this.VI_4_1_3.TabIndex = 430;
            this.VI_4_1_3.Text = "0";
            this.VI_4_1_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_4_1_3.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_4_2_4
            // 
            this.VI_4_2_4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_4_2_4.AutoSize = true;
            this.VI_4_2_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_4_2_4.Location = new System.Drawing.Point(475, 742);
            this.VI_4_2_4.Name = "VI_4_2_4";
            this.VI_4_2_4.Size = new System.Drawing.Size(151, 25);
            this.VI_4_2_4.TabIndex = 433;
            this.VI_4_2_4.Text = "0";
            this.VI_4_2_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_4_2_4.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_4_2_3
            // 
            this.VI_4_2_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_4_2_3.AutoSize = true;
            this.VI_4_2_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_4_2_3.Location = new System.Drawing.Point(475, 716);
            this.VI_4_2_3.Name = "VI_4_2_3";
            this.VI_4_2_3.Size = new System.Drawing.Size(151, 25);
            this.VI_4_2_3.TabIndex = 431;
            this.VI_4_2_3.Text = "0";
            this.VI_4_2_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_4_2_3.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_4_1_4
            // 
            this.VI_4_1_4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_4_1_4.AutoSize = true;
            this.VI_4_1_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_4_1_4.Location = new System.Drawing.Point(318, 742);
            this.VI_4_1_4.Name = "VI_4_1_4";
            this.VI_4_1_4.Size = new System.Drawing.Size(150, 25);
            this.VI_4_1_4.TabIndex = 432;
            this.VI_4_1_4.Text = "0";
            this.VI_4_1_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_4_1_4.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_4_1_5
            // 
            this.VI_4_1_5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_4_1_5.AutoSize = true;
            this.VI_4_1_5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_4_1_5.Location = new System.Drawing.Point(318, 768);
            this.VI_4_1_5.Name = "VI_4_1_5";
            this.VI_4_1_5.Size = new System.Drawing.Size(150, 25);
            this.VI_4_1_5.TabIndex = 434;
            this.VI_4_1_5.Text = "0";
            this.VI_4_1_5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_4_1_5.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_4_2_5
            // 
            this.VI_4_2_5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_4_2_5.AutoSize = true;
            this.VI_4_2_5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_4_2_5.Location = new System.Drawing.Point(475, 768);
            this.VI_4_2_5.Name = "VI_4_2_5";
            this.VI_4_2_5.Size = new System.Drawing.Size(151, 25);
            this.VI_4_2_5.TabIndex = 435;
            this.VI_4_2_5.Text = "0";
            this.VI_4_2_5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_4_2_5.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_4_2_6
            // 
            this.VI_4_2_6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_4_2_6.AutoSize = true;
            this.VI_4_2_6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_4_2_6.Location = new System.Drawing.Point(475, 794);
            this.VI_4_2_6.Name = "VI_4_2_6";
            this.VI_4_2_6.Size = new System.Drawing.Size(151, 25);
            this.VI_4_2_6.TabIndex = 437;
            this.VI_4_2_6.Text = "0";
            this.VI_4_2_6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_4_2_6.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_4_1_6
            // 
            this.VI_4_1_6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_4_1_6.AutoSize = true;
            this.VI_4_1_6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_4_1_6.Location = new System.Drawing.Point(318, 794);
            this.VI_4_1_6.Name = "VI_4_1_6";
            this.VI_4_1_6.Size = new System.Drawing.Size(150, 25);
            this.VI_4_1_6.TabIndex = 436;
            this.VI_4_1_6.Text = "0";
            this.VI_4_1_6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_4_1_6.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_4_Sum_1
            // 
            this.VI_4_Sum_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_4_Sum_1.AutoSize = true;
            this.VI_4_Sum_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VI_4_Sum_1.Location = new System.Drawing.Point(318, 820);
            this.VI_4_Sum_1.Name = "VI_4_Sum_1";
            this.VI_4_Sum_1.Size = new System.Drawing.Size(150, 25);
            this.VI_4_Sum_1.TabIndex = 438;
            this.VI_4_Sum_1.Text = "0";
            this.VI_4_Sum_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // VI_4_Sum_2
            // 
            this.VI_4_Sum_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_4_Sum_2.AutoSize = true;
            this.VI_4_Sum_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VI_4_Sum_2.Location = new System.Drawing.Point(475, 820);
            this.VI_4_Sum_2.Name = "VI_4_Sum_2";
            this.VI_4_Sum_2.Size = new System.Drawing.Size(151, 25);
            this.VI_4_Sum_2.TabIndex = 439;
            this.VI_4_Sum_2.Text = "0";
            this.VI_4_Sum_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // VI_5_2_1
            // 
            this.VI_5_2_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_5_2_1.AutoSize = true;
            this.VI_5_2_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_5_2_1.Location = new System.Drawing.Point(475, 872);
            this.VI_5_2_1.Name = "VI_5_2_1";
            this.VI_5_2_1.Size = new System.Drawing.Size(151, 25);
            this.VI_5_2_1.TabIndex = 441;
            this.VI_5_2_1.Text = "0";
            this.VI_5_2_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_5_2_1.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_5_1_1
            // 
            this.VI_5_1_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_5_1_1.AutoSize = true;
            this.VI_5_1_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_5_1_1.Location = new System.Drawing.Point(318, 872);
            this.VI_5_1_1.Name = "VI_5_1_1";
            this.VI_5_1_1.Size = new System.Drawing.Size(150, 25);
            this.VI_5_1_1.TabIndex = 440;
            this.VI_5_1_1.Text = "0";
            this.VI_5_1_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_5_1_1.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_5_1_2
            // 
            this.VI_5_1_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_5_1_2.AutoSize = true;
            this.VI_5_1_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_5_1_2.Location = new System.Drawing.Point(318, 898);
            this.VI_5_1_2.Name = "VI_5_1_2";
            this.VI_5_1_2.Size = new System.Drawing.Size(150, 25);
            this.VI_5_1_2.TabIndex = 442;
            this.VI_5_1_2.Text = "0";
            this.VI_5_1_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_5_1_2.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_5_2_2
            // 
            this.VI_5_2_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_5_2_2.AutoSize = true;
            this.VI_5_2_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_5_2_2.Location = new System.Drawing.Point(475, 898);
            this.VI_5_2_2.Name = "VI_5_2_2";
            this.VI_5_2_2.Size = new System.Drawing.Size(151, 25);
            this.VI_5_2_2.TabIndex = 443;
            this.VI_5_2_2.Text = "0";
            this.VI_5_2_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_5_2_2.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_5_2_3
            // 
            this.VI_5_2_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_5_2_3.AutoSize = true;
            this.VI_5_2_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_5_2_3.Location = new System.Drawing.Point(475, 924);
            this.VI_5_2_3.Name = "VI_5_2_3";
            this.VI_5_2_3.Size = new System.Drawing.Size(151, 25);
            this.VI_5_2_3.TabIndex = 445;
            this.VI_5_2_3.Text = "0";
            this.VI_5_2_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_5_2_3.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_5_1_3
            // 
            this.VI_5_1_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_5_1_3.AutoSize = true;
            this.VI_5_1_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_5_1_3.Location = new System.Drawing.Point(318, 924);
            this.VI_5_1_3.Name = "VI_5_1_3";
            this.VI_5_1_3.Size = new System.Drawing.Size(150, 25);
            this.VI_5_1_3.TabIndex = 444;
            this.VI_5_1_3.Text = "0";
            this.VI_5_1_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_5_1_3.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_5_1_4
            // 
            this.VI_5_1_4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_5_1_4.AutoSize = true;
            this.VI_5_1_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_5_1_4.Location = new System.Drawing.Point(318, 950);
            this.VI_5_1_4.Name = "VI_5_1_4";
            this.VI_5_1_4.Size = new System.Drawing.Size(150, 25);
            this.VI_5_1_4.TabIndex = 446;
            this.VI_5_1_4.Text = "0";
            this.VI_5_1_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_5_1_4.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_5_2_4
            // 
            this.VI_5_2_4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_5_2_4.AutoSize = true;
            this.VI_5_2_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_5_2_4.Location = new System.Drawing.Point(475, 950);
            this.VI_5_2_4.Name = "VI_5_2_4";
            this.VI_5_2_4.Size = new System.Drawing.Size(151, 25);
            this.VI_5_2_4.TabIndex = 447;
            this.VI_5_2_4.Text = "0";
            this.VI_5_2_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_5_2_4.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_5_2_6
            // 
            this.VI_5_2_6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_5_2_6.AutoSize = true;
            this.VI_5_2_6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_5_2_6.Location = new System.Drawing.Point(475, 1012);
            this.VI_5_2_6.Name = "VI_5_2_6";
            this.VI_5_2_6.Size = new System.Drawing.Size(151, 25);
            this.VI_5_2_6.TabIndex = 451;
            this.VI_5_2_6.Text = "0";
            this.VI_5_2_6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_5_2_6.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_5_2_5
            // 
            this.VI_5_2_5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_5_2_5.AutoSize = true;
            this.VI_5_2_5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_5_2_5.Location = new System.Drawing.Point(475, 976);
            this.VI_5_2_5.Name = "VI_5_2_5";
            this.VI_5_2_5.Size = new System.Drawing.Size(151, 35);
            this.VI_5_2_5.TabIndex = 449;
            this.VI_5_2_5.Text = "0";
            this.VI_5_2_5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_5_2_5.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_5_2_7
            // 
            this.VI_5_2_7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_5_2_7.AutoSize = true;
            this.VI_5_2_7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_5_2_7.Location = new System.Drawing.Point(475, 1038);
            this.VI_5_2_7.Name = "VI_5_2_7";
            this.VI_5_2_7.Size = new System.Drawing.Size(151, 25);
            this.VI_5_2_7.TabIndex = 453;
            this.VI_5_2_7.Text = "0";
            this.VI_5_2_7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_5_2_7.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_5_1_7
            // 
            this.VI_5_1_7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_5_1_7.AutoSize = true;
            this.VI_5_1_7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_5_1_7.Location = new System.Drawing.Point(318, 1038);
            this.VI_5_1_7.Name = "VI_5_1_7";
            this.VI_5_1_7.Size = new System.Drawing.Size(150, 25);
            this.VI_5_1_7.TabIndex = 452;
            this.VI_5_1_7.Text = "0";
            this.VI_5_1_7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_5_1_7.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_5_1_6
            // 
            this.VI_5_1_6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_5_1_6.AutoSize = true;
            this.VI_5_1_6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_5_1_6.Location = new System.Drawing.Point(318, 1012);
            this.VI_5_1_6.Name = "VI_5_1_6";
            this.VI_5_1_6.Size = new System.Drawing.Size(150, 25);
            this.VI_5_1_6.TabIndex = 450;
            this.VI_5_1_6.Text = "0";
            this.VI_5_1_6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_5_1_6.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_5_1_5
            // 
            this.VI_5_1_5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_5_1_5.AutoSize = true;
            this.VI_5_1_5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_5_1_5.Location = new System.Drawing.Point(318, 976);
            this.VI_5_1_5.Name = "VI_5_1_5";
            this.VI_5_1_5.Size = new System.Drawing.Size(150, 35);
            this.VI_5_1_5.TabIndex = 448;
            this.VI_5_1_5.Text = "0";
            this.VI_5_1_5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_5_1_5.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_6_a_1
            // 
            this.VI_6_a_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_6_a_1.AutoSize = true;
            this.VI_6_a_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_6_a_1.Location = new System.Drawing.Point(318, 1090);
            this.VI_6_a_1.Name = "VI_6_a_1";
            this.VI_6_a_1.Size = new System.Drawing.Size(150, 25);
            this.VI_6_a_1.TabIndex = 454;
            this.VI_6_a_1.Text = "0";
            this.VI_6_a_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_6_a_1.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_6_a_2
            // 
            this.VI_6_a_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_6_a_2.AutoSize = true;
            this.VI_6_a_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_6_a_2.Location = new System.Drawing.Point(475, 1090);
            this.VI_6_a_2.Name = "VI_6_a_2";
            this.VI_6_a_2.Size = new System.Drawing.Size(151, 25);
            this.VI_6_a_2.TabIndex = 455;
            this.VI_6_a_2.Text = "0";
            this.VI_6_a_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_6_a_2.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_6_b_2
            // 
            this.VI_6_b_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_6_b_2.AutoSize = true;
            this.VI_6_b_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_6_b_2.Location = new System.Drawing.Point(475, 1116);
            this.VI_6_b_2.Name = "VI_6_b_2";
            this.VI_6_b_2.Size = new System.Drawing.Size(151, 25);
            this.VI_6_b_2.TabIndex = 457;
            this.VI_6_b_2.Text = "0";
            this.VI_6_b_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_6_b_2.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_6_b_1
            // 
            this.VI_6_b_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_6_b_1.AutoSize = true;
            this.VI_6_b_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_6_b_1.Location = new System.Drawing.Point(318, 1116);
            this.VI_6_b_1.Name = "VI_6_b_1";
            this.VI_6_b_1.Size = new System.Drawing.Size(150, 25);
            this.VI_6_b_1.TabIndex = 456;
            this.VI_6_b_1.Text = "0";
            this.VI_6_b_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_6_b_1.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_6_c_1
            // 
            this.VI_6_c_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_6_c_1.AutoSize = true;
            this.VI_6_c_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_6_c_1.Location = new System.Drawing.Point(318, 1142);
            this.VI_6_c_1.Name = "VI_6_c_1";
            this.VI_6_c_1.Size = new System.Drawing.Size(150, 25);
            this.VI_6_c_1.TabIndex = 458;
            this.VI_6_c_1.Text = "0";
            this.VI_6_c_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_6_c_1.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_6_c_2
            // 
            this.VI_6_c_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_6_c_2.AutoSize = true;
            this.VI_6_c_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_6_c_2.Location = new System.Drawing.Point(475, 1142);
            this.VI_6_c_2.Name = "VI_6_c_2";
            this.VI_6_c_2.Size = new System.Drawing.Size(151, 25);
            this.VI_6_c_2.TabIndex = 459;
            this.VI_6_c_2.Text = "0";
            this.VI_6_c_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_6_c_2.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_6_c_2_2
            // 
            this.VI_6_c_2_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_6_c_2_2.AutoSize = true;
            this.VI_6_c_2_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_6_c_2_2.Location = new System.Drawing.Point(475, 1194);
            this.VI_6_c_2_2.Name = "VI_6_c_2_2";
            this.VI_6_c_2_2.Size = new System.Drawing.Size(151, 25);
            this.VI_6_c_2_2.TabIndex = 463;
            this.VI_6_c_2_2.Text = "0";
            this.VI_6_c_2_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_6_c_2_2.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_6_c_2_1
            // 
            this.VI_6_c_2_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_6_c_2_1.AutoSize = true;
            this.VI_6_c_2_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_6_c_2_1.Location = new System.Drawing.Point(475, 1168);
            this.VI_6_c_2_1.Name = "VI_6_c_2_1";
            this.VI_6_c_2_1.Size = new System.Drawing.Size(151, 25);
            this.VI_6_c_2_1.TabIndex = 461;
            this.VI_6_c_2_1.Text = "0";
            this.VI_6_c_2_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_6_c_2_1.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_6_c_1_1
            // 
            this.VI_6_c_1_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_6_c_1_1.AutoSize = true;
            this.VI_6_c_1_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_6_c_1_1.Location = new System.Drawing.Point(318, 1168);
            this.VI_6_c_1_1.Name = "VI_6_c_1_1";
            this.VI_6_c_1_1.Size = new System.Drawing.Size(150, 25);
            this.VI_6_c_1_1.TabIndex = 460;
            this.VI_6_c_1_1.Text = "0";
            this.VI_6_c_1_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_6_c_1_1.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_6_c_1_2
            // 
            this.VI_6_c_1_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_6_c_1_2.AutoSize = true;
            this.VI_6_c_1_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_6_c_1_2.Location = new System.Drawing.Point(318, 1194);
            this.VI_6_c_1_2.Name = "VI_6_c_1_2";
            this.VI_6_c_1_2.Size = new System.Drawing.Size(150, 25);
            this.VI_6_c_1_2.TabIndex = 462;
            this.VI_6_c_1_2.Text = "0";
            this.VI_6_c_1_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_6_c_1_2.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_7_1_1
            // 
            this.VI_7_1_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_7_1_1.AutoSize = true;
            this.VI_7_1_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_7_1_1.Location = new System.Drawing.Point(318, 1246);
            this.VI_7_1_1.Name = "VI_7_1_1";
            this.VI_7_1_1.Size = new System.Drawing.Size(150, 25);
            this.VI_7_1_1.TabIndex = 464;
            this.VI_7_1_1.Text = "0";
            this.VI_7_1_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_7_1_1.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_7_2_1
            // 
            this.VI_7_2_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_7_2_1.AutoSize = true;
            this.VI_7_2_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_7_2_1.Location = new System.Drawing.Point(475, 1246);
            this.VI_7_2_1.Name = "VI_7_2_1";
            this.VI_7_2_1.Size = new System.Drawing.Size(151, 25);
            this.VI_7_2_1.TabIndex = 465;
            this.VI_7_2_1.Text = "0";
            this.VI_7_2_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_7_2_1.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_7_2_2
            // 
            this.VI_7_2_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_7_2_2.AutoSize = true;
            this.VI_7_2_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_7_2_2.Location = new System.Drawing.Point(475, 1272);
            this.VI_7_2_2.Name = "VI_7_2_2";
            this.VI_7_2_2.Size = new System.Drawing.Size(151, 25);
            this.VI_7_2_2.TabIndex = 467;
            this.VI_7_2_2.Text = "0";
            this.VI_7_2_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_7_2_2.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_7_1_2
            // 
            this.VI_7_1_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_7_1_2.AutoSize = true;
            this.VI_7_1_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_7_1_2.Location = new System.Drawing.Point(318, 1272);
            this.VI_7_1_2.Name = "VI_7_1_2";
            this.VI_7_1_2.Size = new System.Drawing.Size(150, 25);
            this.VI_7_1_2.TabIndex = 466;
            this.VI_7_1_2.Text = "0";
            this.VI_7_1_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_7_1_2.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_7_1_3
            // 
            this.VI_7_1_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_7_1_3.AutoSize = true;
            this.VI_7_1_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_7_1_3.Location = new System.Drawing.Point(318, 1298);
            this.VI_7_1_3.Name = "VI_7_1_3";
            this.VI_7_1_3.Size = new System.Drawing.Size(150, 25);
            this.VI_7_1_3.TabIndex = 468;
            this.VI_7_1_3.Text = "0";
            this.VI_7_1_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_7_1_3.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_7_2_3
            // 
            this.VI_7_2_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_7_2_3.AutoSize = true;
            this.VI_7_2_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_7_2_3.Location = new System.Drawing.Point(475, 1298);
            this.VI_7_2_3.Name = "VI_7_2_3";
            this.VI_7_2_3.Size = new System.Drawing.Size(151, 25);
            this.VI_7_2_3.TabIndex = 469;
            this.VI_7_2_3.Text = "0";
            this.VI_7_2_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_7_2_3.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_7_2_4
            // 
            this.VI_7_2_4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_7_2_4.AutoSize = true;
            this.VI_7_2_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_7_2_4.Location = new System.Drawing.Point(475, 1324);
            this.VI_7_2_4.Name = "VI_7_2_4";
            this.VI_7_2_4.Size = new System.Drawing.Size(151, 25);
            this.VI_7_2_4.TabIndex = 471;
            this.VI_7_2_4.Text = "0";
            this.VI_7_2_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_7_2_4.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_7_1_4
            // 
            this.VI_7_1_4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_7_1_4.AutoSize = true;
            this.VI_7_1_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_7_1_4.Location = new System.Drawing.Point(318, 1324);
            this.VI_7_1_4.Name = "VI_7_1_4";
            this.VI_7_1_4.Size = new System.Drawing.Size(150, 25);
            this.VI_7_1_4.TabIndex = 470;
            this.VI_7_1_4.Text = "0";
            this.VI_7_1_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_7_1_4.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_7_1_5
            // 
            this.VI_7_1_5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_7_1_5.AutoSize = true;
            this.VI_7_1_5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_7_1_5.Location = new System.Drawing.Point(318, 1350);
            this.VI_7_1_5.Name = "VI_7_1_5";
            this.VI_7_1_5.Size = new System.Drawing.Size(150, 25);
            this.VI_7_1_5.TabIndex = 472;
            this.VI_7_1_5.Text = "0";
            this.VI_7_1_5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_7_1_5.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_7_2_5
            // 
            this.VI_7_2_5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_7_2_5.AutoSize = true;
            this.VI_7_2_5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_7_2_5.Location = new System.Drawing.Point(475, 1350);
            this.VI_7_2_5.Name = "VI_7_2_5";
            this.VI_7_2_5.Size = new System.Drawing.Size(151, 25);
            this.VI_7_2_5.TabIndex = 473;
            this.VI_7_2_5.Text = "0";
            this.VI_7_2_5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_7_2_5.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_7_Sum_2
            // 
            this.VI_7_Sum_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_7_Sum_2.AutoSize = true;
            this.VI_7_Sum_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VI_7_Sum_2.Location = new System.Drawing.Point(475, 1376);
            this.VI_7_Sum_2.Name = "VI_7_Sum_2";
            this.VI_7_Sum_2.Size = new System.Drawing.Size(151, 25);
            this.VI_7_Sum_2.TabIndex = 475;
            this.VI_7_Sum_2.Text = "0";
            this.VI_7_Sum_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // VI_7_Sum_1
            // 
            this.VI_7_Sum_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_7_Sum_1.AutoSize = true;
            this.VI_7_Sum_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VI_7_Sum_1.Location = new System.Drawing.Point(318, 1376);
            this.VI_7_Sum_1.Name = "VI_7_Sum_1";
            this.VI_7_Sum_1.Size = new System.Drawing.Size(150, 25);
            this.VI_7_Sum_1.TabIndex = 474;
            this.VI_7_Sum_1.Text = "0";
            this.VI_7_Sum_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // VI_8_1_1
            // 
            this.VI_8_1_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_8_1_1.AutoSize = true;
            this.VI_8_1_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_8_1_1.Location = new System.Drawing.Point(318, 1428);
            this.VI_8_1_1.Name = "VI_8_1_1";
            this.VI_8_1_1.Size = new System.Drawing.Size(150, 25);
            this.VI_8_1_1.TabIndex = 476;
            this.VI_8_1_1.Text = "0";
            this.VI_8_1_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_8_1_1.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_8_2_1
            // 
            this.VI_8_2_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_8_2_1.AutoSize = true;
            this.VI_8_2_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_8_2_1.Location = new System.Drawing.Point(475, 1428);
            this.VI_8_2_1.Name = "VI_8_2_1";
            this.VI_8_2_1.Size = new System.Drawing.Size(151, 25);
            this.VI_8_2_1.TabIndex = 477;
            this.VI_8_2_1.Text = "0";
            this.VI_8_2_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_8_2_1.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_8_2_2
            // 
            this.VI_8_2_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_8_2_2.AutoSize = true;
            this.VI_8_2_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_8_2_2.Location = new System.Drawing.Point(475, 1454);
            this.VI_8_2_2.Name = "VI_8_2_2";
            this.VI_8_2_2.Size = new System.Drawing.Size(151, 25);
            this.VI_8_2_2.TabIndex = 479;
            this.VI_8_2_2.Text = "0";
            this.VI_8_2_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_8_2_2.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_8_1_2
            // 
            this.VI_8_1_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_8_1_2.AutoSize = true;
            this.VI_8_1_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_8_1_2.Location = new System.Drawing.Point(318, 1454);
            this.VI_8_1_2.Name = "VI_8_1_2";
            this.VI_8_1_2.Size = new System.Drawing.Size(150, 25);
            this.VI_8_1_2.TabIndex = 478;
            this.VI_8_1_2.Text = "0";
            this.VI_8_1_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_8_1_2.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_8_1_3
            // 
            this.VI_8_1_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_8_1_3.AutoSize = true;
            this.VI_8_1_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_8_1_3.Location = new System.Drawing.Point(318, 1480);
            this.VI_8_1_3.Name = "VI_8_1_3";
            this.VI_8_1_3.Size = new System.Drawing.Size(150, 25);
            this.VI_8_1_3.TabIndex = 480;
            this.VI_8_1_3.Text = "0";
            this.VI_8_1_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_8_1_3.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_8_2_3
            // 
            this.VI_8_2_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_8_2_3.AutoSize = true;
            this.VI_8_2_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_8_2_3.Location = new System.Drawing.Point(475, 1480);
            this.VI_8_2_3.Name = "VI_8_2_3";
            this.VI_8_2_3.Size = new System.Drawing.Size(151, 25);
            this.VI_8_2_3.TabIndex = 481;
            this.VI_8_2_3.Text = "0";
            this.VI_8_2_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_8_2_3.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_8_2_4
            // 
            this.VI_8_2_4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_8_2_4.AutoSize = true;
            this.VI_8_2_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_8_2_4.Location = new System.Drawing.Point(475, 1506);
            this.VI_8_2_4.Name = "VI_8_2_4";
            this.VI_8_2_4.Size = new System.Drawing.Size(151, 25);
            this.VI_8_2_4.TabIndex = 483;
            this.VI_8_2_4.Text = "0";
            this.VI_8_2_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_8_2_4.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_8_1_4
            // 
            this.VI_8_1_4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_8_1_4.AutoSize = true;
            this.VI_8_1_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_8_1_4.Location = new System.Drawing.Point(318, 1506);
            this.VI_8_1_4.Name = "VI_8_1_4";
            this.VI_8_1_4.Size = new System.Drawing.Size(150, 25);
            this.VI_8_1_4.TabIndex = 482;
            this.VI_8_1_4.Text = "0";
            this.VI_8_1_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_8_1_4.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_8_Sum_1
            // 
            this.VI_8_Sum_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_8_Sum_1.AutoSize = true;
            this.VI_8_Sum_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VI_8_Sum_1.Location = new System.Drawing.Point(318, 1532);
            this.VI_8_Sum_1.Name = "VI_8_Sum_1";
            this.VI_8_Sum_1.Size = new System.Drawing.Size(150, 25);
            this.VI_8_Sum_1.TabIndex = 484;
            this.VI_8_Sum_1.Text = "0";
            this.VI_8_Sum_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // VI_8_Sum_2
            // 
            this.VI_8_Sum_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_8_Sum_2.AutoSize = true;
            this.VI_8_Sum_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VI_8_Sum_2.Location = new System.Drawing.Point(475, 1532);
            this.VI_8_Sum_2.Name = "VI_8_Sum_2";
            this.VI_8_Sum_2.Size = new System.Drawing.Size(151, 25);
            this.VI_8_Sum_2.TabIndex = 485;
            this.VI_8_Sum_2.Text = "0";
            this.VI_8_Sum_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // VI_9_2_1
            // 
            this.VI_9_2_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_9_2_1.AutoSize = true;
            this.VI_9_2_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_9_2_1.Location = new System.Drawing.Point(475, 1584);
            this.VI_9_2_1.Name = "VI_9_2_1";
            this.VI_9_2_1.Size = new System.Drawing.Size(151, 25);
            this.VI_9_2_1.TabIndex = 487;
            this.VI_9_2_1.Text = "0";
            this.VI_9_2_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_9_2_1.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_9_1_1
            // 
            this.VI_9_1_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_9_1_1.AutoSize = true;
            this.VI_9_1_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_9_1_1.Location = new System.Drawing.Point(318, 1584);
            this.VI_9_1_1.Name = "VI_9_1_1";
            this.VI_9_1_1.Size = new System.Drawing.Size(150, 25);
            this.VI_9_1_1.TabIndex = 486;
            this.VI_9_1_1.Text = "0";
            this.VI_9_1_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_9_1_1.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_9_1_2
            // 
            this.VI_9_1_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_9_1_2.AutoSize = true;
            this.VI_9_1_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_9_1_2.Location = new System.Drawing.Point(318, 1610);
            this.VI_9_1_2.Name = "VI_9_1_2";
            this.VI_9_1_2.Size = new System.Drawing.Size(150, 35);
            this.VI_9_1_2.TabIndex = 488;
            this.VI_9_1_2.Text = "0";
            this.VI_9_1_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_9_1_2.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_9_2_2
            // 
            this.VI_9_2_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_9_2_2.AutoSize = true;
            this.VI_9_2_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_9_2_2.Location = new System.Drawing.Point(475, 1610);
            this.VI_9_2_2.Name = "VI_9_2_2";
            this.VI_9_2_2.Size = new System.Drawing.Size(151, 35);
            this.VI_9_2_2.TabIndex = 489;
            this.VI_9_2_2.Text = "0";
            this.VI_9_2_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_9_2_2.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_9_2_3
            // 
            this.VI_9_2_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_9_2_3.AutoSize = true;
            this.VI_9_2_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_9_2_3.Location = new System.Drawing.Point(475, 1646);
            this.VI_9_2_3.Name = "VI_9_2_3";
            this.VI_9_2_3.Size = new System.Drawing.Size(151, 27);
            this.VI_9_2_3.TabIndex = 491;
            this.VI_9_2_3.Text = "0";
            this.VI_9_2_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_9_2_3.Click += new System.EventHandler(this.number_edit);
            // 
            // VI_9_1_3
            // 
            this.VI_9_1_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VI_9_1_3.AutoSize = true;
            this.VI_9_1_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VI_9_1_3.Location = new System.Drawing.Point(318, 1646);
            this.VI_9_1_3.Name = "VI_9_1_3";
            this.VI_9_1_3.Size = new System.Drawing.Size(150, 27);
            this.VI_9_1_3.TabIndex = 490;
            this.VI_9_1_3.Text = "0";
            this.VI_9_1_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VI_9_1_3.Click += new System.EventHandler(this.number_edit);
            // 
            // label284
            // 
            this.label284.AutoSize = true;
            this.label284.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label284.Location = new System.Drawing.Point(3, 6652);
            this.label284.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label284.Name = "label284";
            this.label284.Size = new System.Drawing.Size(493, 13);
            this.label284.TabIndex = 49;
            this.label284.Text = "VII. Thông tin bổ sung cho các khoản mục trình bày trong Báo cáo lưu chuyển tiền " +
    "tệ";
            // 
            // Sec_VII_1
            // 
            this.Sec_VII_1.AutoSize = true;
            this.Sec_VII_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_VII_1.Location = new System.Drawing.Point(3, 6675);
            this.Sec_VII_1.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_VII_1.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_VII_1.Name = "Sec_VII_1";
            this.Sec_VII_1.Size = new System.Drawing.Size(629, 39);
            this.Sec_VII_1.TabIndex = 492;
            this.Sec_VII_1.Text = resources.GetString("Sec_VII_1.Text");
            this.Sec_VII_1.Click += new System.EventHandler(this.label_edit);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.Controls.Add(this.panel1);
            this.flowLayoutPanel1.Controls.Add(this.Sec_VIII_5);
            this.flowLayoutPanel1.Controls.Add(this.Sec_VIII_4);
            this.flowLayoutPanel1.Controls.Add(this.Sec_VIII_3);
            this.flowLayoutPanel1.Controls.Add(this.Sec_VIII_2);
            this.flowLayoutPanel1.Controls.Add(this.Sec_VIII_1);
            this.flowLayoutPanel1.Controls.Add(this.label220);
            this.flowLayoutPanel1.Controls.Add(this.Sec_VII_1);
            this.flowLayoutPanel1.Controls.Add(this.label284);
            this.flowLayoutPanel1.Controls.Add(this.tableLayoutPanel14);
            this.flowLayoutPanel1.Controls.Add(this.label291);
            this.flowLayoutPanel1.Controls.Add(this.Sec_V_16_1);
            this.flowLayoutPanel1.Controls.Add(this.label219);
            this.flowLayoutPanel1.Controls.Add(this.Sec_V_15_1);
            this.flowLayoutPanel1.Controls.Add(this.label217);
            this.flowLayoutPanel1.Controls.Add(this.Sec_V_14_8);
            this.flowLayoutPanel1.Controls.Add(this.Sec_V_14_7);
            this.flowLayoutPanel1.Controls.Add(this.Sec_V_14_6);
            this.flowLayoutPanel1.Controls.Add(this.Sec_V_14_5);
            this.flowLayoutPanel1.Controls.Add(this.Sec_V_14_4);
            this.flowLayoutPanel1.Controls.Add(this.Sec_V_14_3);
            this.flowLayoutPanel1.Controls.Add(this.Sec_V_14_2);
            this.flowLayoutPanel1.Controls.Add(this.Sec_V_14_1);
            this.flowLayoutPanel1.Controls.Add(this.label208);
            this.flowLayoutPanel1.Controls.Add(this.Sec_V_13_1);
            this.flowLayoutPanel1.Controls.Add(this.tableLayoutPanel13);
            this.flowLayoutPanel1.Controls.Add(this.tableLayoutPanel12);
            this.flowLayoutPanel1.Controls.Add(this.tableLayoutPanel11);
            this.flowLayoutPanel1.Controls.Add(this.tableLayoutPanel10);
            this.flowLayoutPanel1.Controls.Add(this.tableLayoutPanel9);
            this.flowLayoutPanel1.Controls.Add(this.tableLayoutPanel8);
            this.flowLayoutPanel1.Controls.Add(this.tableLayoutPanel7);
            this.flowLayoutPanel1.Controls.Add(this.Sec_V_6_3);
            this.flowLayoutPanel1.Controls.Add(this.Sec_V_6_2);
            this.flowLayoutPanel1.Controls.Add(this.Sec_V_6_1);
            this.flowLayoutPanel1.Controls.Add(this.tableLayoutPanel6);
            this.flowLayoutPanel1.Controls.Add(this.Sec_V_5_5);
            this.flowLayoutPanel1.Controls.Add(this.Sec_V_5_4);
            this.flowLayoutPanel1.Controls.Add(this.Sec_V_5_3);
            this.flowLayoutPanel1.Controls.Add(this.Sec_V_5_2);
            this.flowLayoutPanel1.Controls.Add(this.Sec_V_5_1);
            this.flowLayoutPanel1.Controls.Add(this.tableLayoutPanel5);
            this.flowLayoutPanel1.Controls.Add(this.Sec_V_4_4);
            this.flowLayoutPanel1.Controls.Add(this.Sec_V_4_3);
            this.flowLayoutPanel1.Controls.Add(this.Sec_V_4_2);
            this.flowLayoutPanel1.Controls.Add(this.Sec_V_4_1);
            this.flowLayoutPanel1.Controls.Add(this.label80);
            this.flowLayoutPanel1.Controls.Add(this.tableLayoutPanel4);
            this.flowLayoutPanel1.Controls.Add(this.tableLayoutPanel3);
            this.flowLayoutPanel1.Controls.Add(this.tableLayoutPanel2);
            this.flowLayoutPanel1.Controls.Add(this.tableLayoutPanel1);
            this.flowLayoutPanel1.Controls.Add(this.tableLayoutPanel15);
            this.flowLayoutPanel1.Controls.Add(this.label27);
            this.flowLayoutPanel1.Controls.Add(this.Sec_IV_12);
            this.flowLayoutPanel1.Controls.Add(this.Sec_IV_11);
            this.flowLayoutPanel1.Controls.Add(this.Sec_IV_10);
            this.flowLayoutPanel1.Controls.Add(this.Sec_IV_9);
            this.flowLayoutPanel1.Controls.Add(this.Sec_IV_8);
            this.flowLayoutPanel1.Controls.Add(this.Sec_IV_7);
            this.flowLayoutPanel1.Controls.Add(this.Sec_IV_6);
            this.flowLayoutPanel1.Controls.Add(this.Sec_IV_5);
            this.flowLayoutPanel1.Controls.Add(this.Sec_IV_4);
            this.flowLayoutPanel1.Controls.Add(this.Sec_IV_3);
            this.flowLayoutPanel1.Controls.Add(this.Sec_IV_2);
            this.flowLayoutPanel1.Controls.Add(this.Sec_IV_1);
            this.flowLayoutPanel1.Controls.Add(this.label14);
            this.flowLayoutPanel1.Controls.Add(this.Sec_III_1);
            this.flowLayoutPanel1.Controls.Add(this.label12);
            this.flowLayoutPanel1.Controls.Add(this.Sec_II_2);
            this.flowLayoutPanel1.Controls.Add(this.Sec_II_1);
            this.flowLayoutPanel1.Controls.Add(this.label9);
            this.flowLayoutPanel1.Controls.Add(this.Sec_I_6);
            this.flowLayoutPanel1.Controls.Add(this.Sec_I_5);
            this.flowLayoutPanel1.Controls.Add(this.Sec_I_4);
            this.flowLayoutPanel1.Controls.Add(this.Sec_I_3);
            this.flowLayoutPanel1.Controls.Add(this.Sec_I_2);
            this.flowLayoutPanel1.Controls.Add(this.Sec_I_1);
            this.flowLayoutPanel1.Controls.Add(this.label1);
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.BottomUp;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(13, 43);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 3, 3, 20);
            this.flowLayoutPanel1.MaximumSize = new System.Drawing.Size(647, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.flowLayoutPanel1.Size = new System.Drawing.Size(647, 6962);
            this.flowLayoutPanel1.TabIndex = 1000;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnPrint);
            this.panel1.Location = new System.Drawing.Point(3, 6862);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 10, 3, 50);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(643, 40);
            this.panel1.TabIndex = 1000;
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(555, 17);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 23);
            this.btnPrint.TabIndex = 500;
            this.btnPrint.TabStop = false;
            this.btnPrint.Text = "In Báo Cáo";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_click);
            // 
            // Sec_VIII_5
            // 
            this.Sec_VIII_5.AutoSize = true;
            this.Sec_VIII_5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_VIII_5.Location = new System.Drawing.Point(3, 6839);
            this.Sec_VIII_5.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_VIII_5.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_VIII_5.Name = "Sec_VIII_5";
            this.Sec_VIII_5.Size = new System.Drawing.Size(125, 13);
            this.Sec_VIII_5.TabIndex = 497;
            this.Sec_VIII_5.Text = "5. Những thông tin khác:";
            this.Sec_VIII_5.Click += new System.EventHandler(this.label_edit);
            // 
            // Sec_VIII_4
            // 
            this.Sec_VIII_4.AutoSize = true;
            this.Sec_VIII_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_VIII_4.Location = new System.Drawing.Point(3, 6816);
            this.Sec_VIII_4.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_VIII_4.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_VIII_4.Name = "Sec_VIII_4";
            this.Sec_VIII_4.Size = new System.Drawing.Size(171, 13);
            this.Sec_VIII_4.TabIndex = 496;
            this.Sec_VIII_4.Text = "4. Thông tin về hoạt động liên tục:";
            this.Sec_VIII_4.Click += new System.EventHandler(this.label_edit);
            // 
            // Sec_VIII_3
            // 
            this.Sec_VIII_3.AutoSize = true;
            this.Sec_VIII_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_VIII_3.Location = new System.Drawing.Point(3, 6793);
            this.Sec_VIII_3.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_VIII_3.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_VIII_3.Name = "Sec_VIII_3";
            this.Sec_VIII_3.Size = new System.Drawing.Size(508, 13);
            this.Sec_VIII_3.TabIndex = 495;
            this.Sec_VIII_3.Text = "3. Thông tin so sánh (những thay đổi về thông tin trong Báo cáo tài chính của các" +
    " niên độ kế toán trước):";
            this.Sec_VIII_3.Click += new System.EventHandler(this.label_edit);
            // 
            // Sec_VIII_2
            // 
            this.Sec_VIII_2.AutoSize = true;
            this.Sec_VIII_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_VIII_2.Location = new System.Drawing.Point(3, 6770);
            this.Sec_VIII_2.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_VIII_2.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_VIII_2.Name = "Sec_VIII_2";
            this.Sec_VIII_2.Size = new System.Drawing.Size(301, 13);
            this.Sec_VIII_2.TabIndex = 394;
            this.Sec_VIII_2.Text = "2. Những sự kiện phát sinh sau ngày kết thúc kỳ kế toán năm:";
            this.Sec_VIII_2.Click += new System.EventHandler(this.label_edit);
            // 
            // Sec_VIII_1
            // 
            this.Sec_VIII_1.AutoSize = true;
            this.Sec_VIII_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_VIII_1.Location = new System.Drawing.Point(3, 6747);
            this.Sec_VIII_1.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_VIII_1.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_VIII_1.Name = "Sec_VIII_1";
            this.Sec_VIII_1.Size = new System.Drawing.Size(392, 13);
            this.Sec_VIII_1.TabIndex = 493;
            this.Sec_VIII_1.Text = "1. Những khoản nợ tiềm tàng, khoản cam kết và những thông tin tài chính khác: ";
            this.Sec_VIII_1.Click += new System.EventHandler(this.label_edit);
            // 
            // label220
            // 
            this.label220.AutoSize = true;
            this.label220.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label220.Location = new System.Drawing.Point(3, 6724);
            this.label220.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label220.Name = "label220";
            this.label220.Size = new System.Drawing.Size(158, 13);
            this.label220.TabIndex = 63;
            this.label220.Text = "VIII. Những thông tin khác";
            // 
            // label291
            // 
            this.label291.AutoSize = true;
            this.label291.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label291.Location = new System.Drawing.Point(3, 4935);
            this.label291.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label291.Name = "label291";
            this.label291.Size = new System.Drawing.Size(559, 13);
            this.label291.TabIndex = 69;
            this.label291.Text = "VI. Thông tin bổ sung cho các khoản mục trình bày trong Báo cáo kết quả hoạt động" +
    " kinh doanh";
            // 
            // Sec_V_16_1
            // 
            this.Sec_V_16_1.AutoSize = true;
            this.Sec_V_16_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_V_16_1.Location = new System.Drawing.Point(3, 4912);
            this.Sec_V_16_1.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_V_16_1.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_V_16_1.Name = "Sec_V_16_1";
            this.Sec_V_16_1.Size = new System.Drawing.Size(107, 13);
            this.Sec_V_16_1.TabIndex = 1001;
            this.Sec_V_16_1.Text = "(Nội dung giải trình...)";
            this.Sec_V_16_1.Click += new System.EventHandler(this.label_edit);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel5.ColumnCount = 5;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 165F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 135F));
            this.tableLayoutPanel5.Controls.Add(this.label85, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.label86, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.label87, 2, 1);
            this.tableLayoutPanel5.Controls.Add(this.label88, 3, 1);
            this.tableLayoutPanel5.Controls.Add(this.label89, 4, 1);
            this.tableLayoutPanel5.Controls.Add(this.label90, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.label91, 0, 3);
            this.tableLayoutPanel5.Controls.Add(this.label92, 0, 4);
            this.tableLayoutPanel5.Controls.Add(this.label93, 0, 5);
            this.tableLayoutPanel5.Controls.Add(this.label94, 0, 6);
            this.tableLayoutPanel5.Controls.Add(this.label95, 0, 7);
            this.tableLayoutPanel5.Controls.Add(this.label96, 0, 8);
            this.tableLayoutPanel5.Controls.Add(this.label97, 0, 9);
            this.tableLayoutPanel5.Controls.Add(this.label98, 0, 10);
            this.tableLayoutPanel5.Controls.Add(this.label99, 0, 11);
            this.tableLayoutPanel5.Controls.Add(this.label100, 0, 12);
            this.tableLayoutPanel5.Controls.Add(this.label101, 0, 13);
            this.tableLayoutPanel5.Controls.Add(this.label103, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.V_5_a_1_1, 1, 3);
            this.tableLayoutPanel5.Controls.Add(this.V_5_a_2_1, 2, 3);
            this.tableLayoutPanel5.Controls.Add(this.V_5_a_3_1, 3, 3);
            this.tableLayoutPanel5.Controls.Add(this.V_5_a_4_1, 4, 3);
            this.tableLayoutPanel5.Controls.Add(this.V_5_a_4_2, 4, 4);
            this.tableLayoutPanel5.Controls.Add(this.V_5_a_4_3, 4, 5);
            this.tableLayoutPanel5.Controls.Add(this.V_5_b_4_1, 4, 7);
            this.tableLayoutPanel5.Controls.Add(this.V_5_b_4_2, 4, 8);
            this.tableLayoutPanel5.Controls.Add(this.V_5_b_4_3, 4, 9);
            this.tableLayoutPanel5.Controls.Add(this.V_5_c_4_1, 4, 11);
            this.tableLayoutPanel5.Controls.Add(this.V_5_c_4_2, 4, 12);
            this.tableLayoutPanel5.Controls.Add(this.V_5_c_4_3, 4, 13);
            this.tableLayoutPanel5.Controls.Add(this.V_5_a_3_2, 3, 4);
            this.tableLayoutPanel5.Controls.Add(this.V_5_a_3_3, 3, 5);
            this.tableLayoutPanel5.Controls.Add(this.V_5_b_3_1, 3, 7);
            this.tableLayoutPanel5.Controls.Add(this.V_5_b_3_2, 3, 8);
            this.tableLayoutPanel5.Controls.Add(this.V_5_b_3_3, 3, 9);
            this.tableLayoutPanel5.Controls.Add(this.V_5_c_3_1, 3, 11);
            this.tableLayoutPanel5.Controls.Add(this.V_5_c_3_2, 3, 12);
            this.tableLayoutPanel5.Controls.Add(this.V_5_c_3_3, 3, 13);
            this.tableLayoutPanel5.Controls.Add(this.V_5_a_2_2, 2, 4);
            this.tableLayoutPanel5.Controls.Add(this.V_5_a_2_3, 2, 5);
            this.tableLayoutPanel5.Controls.Add(this.V_5_b_2_1, 2, 7);
            this.tableLayoutPanel5.Controls.Add(this.V_5_b_2_2, 2, 8);
            this.tableLayoutPanel5.Controls.Add(this.V_5_b_2_3, 2, 9);
            this.tableLayoutPanel5.Controls.Add(this.V_5_c_2_1, 2, 11);
            this.tableLayoutPanel5.Controls.Add(this.V_5_c_2_2, 2, 12);
            this.tableLayoutPanel5.Controls.Add(this.V_5_c_2_3, 2, 13);
            this.tableLayoutPanel5.Controls.Add(this.V_5_a_1_3, 1, 5);
            this.tableLayoutPanel5.Controls.Add(this.V_5_a_1_2, 1, 4);
            this.tableLayoutPanel5.Controls.Add(this.V_5_b_1_1, 1, 7);
            this.tableLayoutPanel5.Controls.Add(this.V_5_b_1_2, 1, 8);
            this.tableLayoutPanel5.Controls.Add(this.V_5_b_1_3, 1, 9);
            this.tableLayoutPanel5.Controls.Add(this.V_5_c_1_1, 1, 11);
            this.tableLayoutPanel5.Controls.Add(this.V_5_c_1_2, 1, 12);
            this.tableLayoutPanel5.Controls.Add(this.V_5_c_1_3, 1, 13);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 1997);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 14;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(630, 369);
            this.tableLayoutPanel5.TabIndex = 62;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.Location = new System.Drawing.Point(4, 34);
            this.label85.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(70, 13);
            this.label85.TabIndex = 0;
            this.label85.Text = "Khoản mục";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.Location = new System.Drawing.Point(170, 34);
            this.label86.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(93, 13);
            this.label86.TabIndex = 0;
            this.label86.Text = "Số dư đầu năm";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label87.Location = new System.Drawing.Point(286, 34);
            this.label87.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(96, 13);
            this.label87.TabIndex = 0;
            this.label87.Text = "Tăng trong năm";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label88.Location = new System.Drawing.Point(402, 34);
            this.label88.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(95, 13);
            this.label88.TabIndex = 0;
            this.label88.Text = "Giảm trong năm";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label89.Location = new System.Drawing.Point(518, 34);
            this.label89.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(95, 13);
            this.label89.TabIndex = 0;
            this.label89.Text = "Số dư cuối năm";
            // 
            // label90
            // 
            this.label90.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label90.AutoSize = true;
            this.tableLayoutPanel5.SetColumnSpan(this.label90, 5);
            this.label90.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label90.Location = new System.Drawing.Point(4, 55);
            this.label90.Margin = new System.Windows.Forms.Padding(3, 0, 25, 0);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(621, 25);
            this.label90.TabIndex = 0;
            this.label90.Text = "A. TSCĐ hữu hình";
            this.label90.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(4, 86);
            this.label91.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(61, 13);
            this.label91.TabIndex = 0;
            this.label91.Text = "Nguyên giá";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(4, 112);
            this.label92.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(109, 13);
            this.label92.TabIndex = 0;
            this.label92.Text = "Giá trị hao mòn luỹ kế";
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(4, 138);
            this.label93.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(68, 13);
            this.label93.TabIndex = 0;
            this.label93.Text = "Giá trị còn lại";
            // 
            // label94
            // 
            this.label94.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label94.AutoSize = true;
            this.tableLayoutPanel5.SetColumnSpan(this.label94, 5);
            this.label94.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label94.Location = new System.Drawing.Point(4, 159);
            this.label94.Margin = new System.Windows.Forms.Padding(3, 0, 25, 0);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(621, 25);
            this.label94.TabIndex = 0;
            this.label94.Text = "B. TSCĐ vô hình";
            this.label94.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(4, 190);
            this.label95.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(61, 13);
            this.label95.TabIndex = 0;
            this.label95.Text = "Nguyên giá";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(4, 216);
            this.label96.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(109, 13);
            this.label96.TabIndex = 0;
            this.label96.Text = "Giá trị hao mòn luỹ kế";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(4, 242);
            this.label97.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(68, 13);
            this.label97.TabIndex = 0;
            this.label97.Text = "Giá trị còn lại";
            // 
            // label98
            // 
            this.label98.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label98.AutoSize = true;
            this.tableLayoutPanel5.SetColumnSpan(this.label98, 5);
            this.label98.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label98.Location = new System.Drawing.Point(4, 263);
            this.label98.Margin = new System.Windows.Forms.Padding(3, 0, 25, 0);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(621, 25);
            this.label98.TabIndex = 0;
            this.label98.Text = "C. TSCĐ thuê tài chính";
            this.label98.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(4, 294);
            this.label99.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(61, 13);
            this.label99.TabIndex = 0;
            this.label99.Text = "Nguyên giá";
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(4, 320);
            this.label100.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(109, 13);
            this.label100.TabIndex = 0;
            this.label100.Text = "Giá trị hao mòn luỹ kế";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(4, 346);
            this.label101.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(68, 13);
            this.label101.TabIndex = 0;
            this.label101.Text = "Giá trị còn lại";
            // 
            // label103
            // 
            this.label103.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label103.AutoSize = true;
            this.tableLayoutPanel5.SetColumnSpan(this.label103, 5);
            this.label103.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label103.Location = new System.Drawing.Point(4, 1);
            this.label103.Margin = new System.Windows.Forms.Padding(3, 0, 25, 0);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(621, 27);
            this.label103.TabIndex = 1;
            this.label103.Text = "05. Tăng, giảm TSCĐ (chi tiết từng loại tài sản theo yêu cầu quản lý của DN)     " +
    "                                        ";
            this.label103.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // V_5_a_1_1
            // 
            this.V_5_a_1_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_5_a_1_1.AutoSize = true;
            this.V_5_a_1_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_5_a_1_1.Location = new System.Drawing.Point(170, 81);
            this.V_5_a_1_1.Name = "V_5_a_1_1";
            this.V_5_a_1_1.Size = new System.Drawing.Size(109, 25);
            this.V_5_a_1_1.TabIndex = 104;
            this.V_5_a_1_1.Text = "0";
            this.V_5_a_1_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_5_a_1_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_5_a_2_1
            // 
            this.V_5_a_2_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_5_a_2_1.AutoSize = true;
            this.V_5_a_2_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_5_a_2_1.Location = new System.Drawing.Point(286, 81);
            this.V_5_a_2_1.Name = "V_5_a_2_1";
            this.V_5_a_2_1.Size = new System.Drawing.Size(109, 25);
            this.V_5_a_2_1.TabIndex = 105;
            this.V_5_a_2_1.Text = "0";
            this.V_5_a_2_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_5_a_2_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_5_a_3_1
            // 
            this.V_5_a_3_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_5_a_3_1.AutoSize = true;
            this.V_5_a_3_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_5_a_3_1.Location = new System.Drawing.Point(402, 81);
            this.V_5_a_3_1.Name = "V_5_a_3_1";
            this.V_5_a_3_1.Size = new System.Drawing.Size(109, 25);
            this.V_5_a_3_1.TabIndex = 106;
            this.V_5_a_3_1.Text = "0";
            this.V_5_a_3_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_5_a_3_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_5_a_4_1
            // 
            this.V_5_a_4_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_5_a_4_1.AutoSize = true;
            this.V_5_a_4_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_5_a_4_1.Location = new System.Drawing.Point(518, 81);
            this.V_5_a_4_1.Margin = new System.Windows.Forms.Padding(3, 0, 25, 0);
            this.V_5_a_4_1.Name = "V_5_a_4_1";
            this.V_5_a_4_1.Size = new System.Drawing.Size(107, 25);
            this.V_5_a_4_1.TabIndex = 107;
            this.V_5_a_4_1.Text = "0";
            this.V_5_a_4_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_5_a_4_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_5_a_4_2
            // 
            this.V_5_a_4_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_5_a_4_2.AutoSize = true;
            this.V_5_a_4_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_5_a_4_2.Location = new System.Drawing.Point(518, 107);
            this.V_5_a_4_2.Margin = new System.Windows.Forms.Padding(3, 0, 25, 0);
            this.V_5_a_4_2.Name = "V_5_a_4_2";
            this.V_5_a_4_2.Size = new System.Drawing.Size(107, 25);
            this.V_5_a_4_2.TabIndex = 111;
            this.V_5_a_4_2.Text = "0";
            this.V_5_a_4_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_5_a_4_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_5_a_4_3
            // 
            this.V_5_a_4_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_5_a_4_3.AutoSize = true;
            this.V_5_a_4_3.Cursor = System.Windows.Forms.Cursors.Default;
            this.V_5_a_4_3.Location = new System.Drawing.Point(518, 133);
            this.V_5_a_4_3.Margin = new System.Windows.Forms.Padding(3, 0, 25, 0);
            this.V_5_a_4_3.Name = "V_5_a_4_3";
            this.V_5_a_4_3.Size = new System.Drawing.Size(107, 25);
            this.V_5_a_4_3.TabIndex = 115;
            this.V_5_a_4_3.Text = "0";
            this.V_5_a_4_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_5_b_4_1
            // 
            this.V_5_b_4_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_5_b_4_1.AutoSize = true;
            this.V_5_b_4_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_5_b_4_1.Location = new System.Drawing.Point(518, 185);
            this.V_5_b_4_1.Margin = new System.Windows.Forms.Padding(3, 0, 25, 0);
            this.V_5_b_4_1.Name = "V_5_b_4_1";
            this.V_5_b_4_1.Size = new System.Drawing.Size(107, 25);
            this.V_5_b_4_1.TabIndex = 119;
            this.V_5_b_4_1.Text = "0";
            this.V_5_b_4_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_5_b_4_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_5_b_4_2
            // 
            this.V_5_b_4_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_5_b_4_2.AutoSize = true;
            this.V_5_b_4_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_5_b_4_2.Location = new System.Drawing.Point(518, 211);
            this.V_5_b_4_2.Margin = new System.Windows.Forms.Padding(3, 0, 25, 0);
            this.V_5_b_4_2.Name = "V_5_b_4_2";
            this.V_5_b_4_2.Size = new System.Drawing.Size(107, 25);
            this.V_5_b_4_2.TabIndex = 223;
            this.V_5_b_4_2.Text = "0";
            this.V_5_b_4_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_5_b_4_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_5_b_4_3
            // 
            this.V_5_b_4_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_5_b_4_3.AutoSize = true;
            this.V_5_b_4_3.Location = new System.Drawing.Point(518, 237);
            this.V_5_b_4_3.Margin = new System.Windows.Forms.Padding(3, 0, 25, 0);
            this.V_5_b_4_3.Name = "V_5_b_4_3";
            this.V_5_b_4_3.Size = new System.Drawing.Size(107, 25);
            this.V_5_b_4_3.TabIndex = 227;
            this.V_5_b_4_3.Text = "0";
            this.V_5_b_4_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_5_c_4_1
            // 
            this.V_5_c_4_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_5_c_4_1.AutoSize = true;
            this.V_5_c_4_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_5_c_4_1.Location = new System.Drawing.Point(518, 289);
            this.V_5_c_4_1.Margin = new System.Windows.Forms.Padding(3, 0, 25, 0);
            this.V_5_c_4_1.Name = "V_5_c_4_1";
            this.V_5_c_4_1.Size = new System.Drawing.Size(107, 25);
            this.V_5_c_4_1.TabIndex = 231;
            this.V_5_c_4_1.Text = "0";
            this.V_5_c_4_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_5_c_4_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_5_c_4_2
            // 
            this.V_5_c_4_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_5_c_4_2.AutoSize = true;
            this.V_5_c_4_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_5_c_4_2.Location = new System.Drawing.Point(518, 315);
            this.V_5_c_4_2.Margin = new System.Windows.Forms.Padding(3, 0, 25, 0);
            this.V_5_c_4_2.Name = "V_5_c_4_2";
            this.V_5_c_4_2.Size = new System.Drawing.Size(107, 25);
            this.V_5_c_4_2.TabIndex = 235;
            this.V_5_c_4_2.Text = "0";
            this.V_5_c_4_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_5_c_4_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_5_c_4_3
            // 
            this.V_5_c_4_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_5_c_4_3.AutoSize = true;
            this.V_5_c_4_3.Location = new System.Drawing.Point(518, 341);
            this.V_5_c_4_3.Margin = new System.Windows.Forms.Padding(3, 0, 25, 0);
            this.V_5_c_4_3.Name = "V_5_c_4_3";
            this.V_5_c_4_3.Size = new System.Drawing.Size(107, 27);
            this.V_5_c_4_3.TabIndex = 239;
            this.V_5_c_4_3.Text = "0";
            this.V_5_c_4_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_5_a_3_2
            // 
            this.V_5_a_3_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_5_a_3_2.AutoSize = true;
            this.V_5_a_3_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_5_a_3_2.Location = new System.Drawing.Point(402, 107);
            this.V_5_a_3_2.Name = "V_5_a_3_2";
            this.V_5_a_3_2.Size = new System.Drawing.Size(109, 25);
            this.V_5_a_3_2.TabIndex = 110;
            this.V_5_a_3_2.Text = "0";
            this.V_5_a_3_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_5_a_3_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_5_a_3_3
            // 
            this.V_5_a_3_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_5_a_3_3.AutoSize = true;
            this.V_5_a_3_3.Cursor = System.Windows.Forms.Cursors.Default;
            this.V_5_a_3_3.Location = new System.Drawing.Point(402, 133);
            this.V_5_a_3_3.Name = "V_5_a_3_3";
            this.V_5_a_3_3.Size = new System.Drawing.Size(109, 25);
            this.V_5_a_3_3.TabIndex = 114;
            this.V_5_a_3_3.Text = "0";
            this.V_5_a_3_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_5_b_3_1
            // 
            this.V_5_b_3_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_5_b_3_1.AutoSize = true;
            this.V_5_b_3_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_5_b_3_1.Location = new System.Drawing.Point(402, 185);
            this.V_5_b_3_1.Name = "V_5_b_3_1";
            this.V_5_b_3_1.Size = new System.Drawing.Size(109, 25);
            this.V_5_b_3_1.TabIndex = 118;
            this.V_5_b_3_1.Text = "0";
            this.V_5_b_3_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_5_b_3_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_5_b_3_2
            // 
            this.V_5_b_3_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_5_b_3_2.AutoSize = true;
            this.V_5_b_3_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_5_b_3_2.Location = new System.Drawing.Point(402, 211);
            this.V_5_b_3_2.Name = "V_5_b_3_2";
            this.V_5_b_3_2.Size = new System.Drawing.Size(109, 25);
            this.V_5_b_3_2.TabIndex = 222;
            this.V_5_b_3_2.Text = "0";
            this.V_5_b_3_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_5_b_3_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_5_b_3_3
            // 
            this.V_5_b_3_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_5_b_3_3.AutoSize = true;
            this.V_5_b_3_3.Location = new System.Drawing.Point(402, 237);
            this.V_5_b_3_3.Name = "V_5_b_3_3";
            this.V_5_b_3_3.Size = new System.Drawing.Size(109, 25);
            this.V_5_b_3_3.TabIndex = 226;
            this.V_5_b_3_3.Text = "0";
            this.V_5_b_3_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_5_c_3_1
            // 
            this.V_5_c_3_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_5_c_3_1.AutoSize = true;
            this.V_5_c_3_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_5_c_3_1.Location = new System.Drawing.Point(402, 289);
            this.V_5_c_3_1.Name = "V_5_c_3_1";
            this.V_5_c_3_1.Size = new System.Drawing.Size(109, 25);
            this.V_5_c_3_1.TabIndex = 230;
            this.V_5_c_3_1.Text = "0";
            this.V_5_c_3_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_5_c_3_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_5_c_3_2
            // 
            this.V_5_c_3_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_5_c_3_2.AutoSize = true;
            this.V_5_c_3_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_5_c_3_2.Location = new System.Drawing.Point(402, 315);
            this.V_5_c_3_2.Name = "V_5_c_3_2";
            this.V_5_c_3_2.Size = new System.Drawing.Size(109, 25);
            this.V_5_c_3_2.TabIndex = 234;
            this.V_5_c_3_2.Text = "0";
            this.V_5_c_3_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_5_c_3_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_5_c_3_3
            // 
            this.V_5_c_3_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_5_c_3_3.AutoSize = true;
            this.V_5_c_3_3.Location = new System.Drawing.Point(402, 341);
            this.V_5_c_3_3.Name = "V_5_c_3_3";
            this.V_5_c_3_3.Size = new System.Drawing.Size(109, 27);
            this.V_5_c_3_3.TabIndex = 238;
            this.V_5_c_3_3.Text = "0";
            this.V_5_c_3_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_5_a_2_2
            // 
            this.V_5_a_2_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_5_a_2_2.AutoSize = true;
            this.V_5_a_2_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_5_a_2_2.Location = new System.Drawing.Point(286, 107);
            this.V_5_a_2_2.Name = "V_5_a_2_2";
            this.V_5_a_2_2.Size = new System.Drawing.Size(109, 25);
            this.V_5_a_2_2.TabIndex = 109;
            this.V_5_a_2_2.Text = "0";
            this.V_5_a_2_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_5_a_2_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_5_a_2_3
            // 
            this.V_5_a_2_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_5_a_2_3.AutoSize = true;
            this.V_5_a_2_3.Cursor = System.Windows.Forms.Cursors.Default;
            this.V_5_a_2_3.Location = new System.Drawing.Point(286, 133);
            this.V_5_a_2_3.Name = "V_5_a_2_3";
            this.V_5_a_2_3.Size = new System.Drawing.Size(109, 25);
            this.V_5_a_2_3.TabIndex = 113;
            this.V_5_a_2_3.Text = "0";
            this.V_5_a_2_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_5_b_2_1
            // 
            this.V_5_b_2_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_5_b_2_1.AutoSize = true;
            this.V_5_b_2_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_5_b_2_1.Location = new System.Drawing.Point(286, 185);
            this.V_5_b_2_1.Name = "V_5_b_2_1";
            this.V_5_b_2_1.Size = new System.Drawing.Size(109, 25);
            this.V_5_b_2_1.TabIndex = 117;
            this.V_5_b_2_1.Text = "0";
            this.V_5_b_2_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_5_b_2_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_5_b_2_2
            // 
            this.V_5_b_2_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_5_b_2_2.AutoSize = true;
            this.V_5_b_2_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_5_b_2_2.Location = new System.Drawing.Point(286, 211);
            this.V_5_b_2_2.Name = "V_5_b_2_2";
            this.V_5_b_2_2.Size = new System.Drawing.Size(109, 25);
            this.V_5_b_2_2.TabIndex = 221;
            this.V_5_b_2_2.Text = "0";
            this.V_5_b_2_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_5_b_2_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_5_b_2_3
            // 
            this.V_5_b_2_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_5_b_2_3.AutoSize = true;
            this.V_5_b_2_3.Location = new System.Drawing.Point(286, 237);
            this.V_5_b_2_3.Name = "V_5_b_2_3";
            this.V_5_b_2_3.Size = new System.Drawing.Size(109, 25);
            this.V_5_b_2_3.TabIndex = 225;
            this.V_5_b_2_3.Text = "0";
            this.V_5_b_2_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_5_c_2_1
            // 
            this.V_5_c_2_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_5_c_2_1.AutoSize = true;
            this.V_5_c_2_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_5_c_2_1.Location = new System.Drawing.Point(286, 289);
            this.V_5_c_2_1.Name = "V_5_c_2_1";
            this.V_5_c_2_1.Size = new System.Drawing.Size(109, 25);
            this.V_5_c_2_1.TabIndex = 229;
            this.V_5_c_2_1.Text = "0";
            this.V_5_c_2_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_5_c_2_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_5_c_2_2
            // 
            this.V_5_c_2_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_5_c_2_2.AutoSize = true;
            this.V_5_c_2_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_5_c_2_2.Location = new System.Drawing.Point(286, 315);
            this.V_5_c_2_2.Name = "V_5_c_2_2";
            this.V_5_c_2_2.Size = new System.Drawing.Size(109, 25);
            this.V_5_c_2_2.TabIndex = 233;
            this.V_5_c_2_2.Text = "0";
            this.V_5_c_2_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_5_c_2_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_5_c_2_3
            // 
            this.V_5_c_2_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_5_c_2_3.AutoSize = true;
            this.V_5_c_2_3.Location = new System.Drawing.Point(286, 341);
            this.V_5_c_2_3.Name = "V_5_c_2_3";
            this.V_5_c_2_3.Size = new System.Drawing.Size(109, 27);
            this.V_5_c_2_3.TabIndex = 237;
            this.V_5_c_2_3.Text = "0";
            this.V_5_c_2_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_5_a_1_3
            // 
            this.V_5_a_1_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_5_a_1_3.AutoSize = true;
            this.V_5_a_1_3.Cursor = System.Windows.Forms.Cursors.Default;
            this.V_5_a_1_3.Location = new System.Drawing.Point(170, 133);
            this.V_5_a_1_3.Name = "V_5_a_1_3";
            this.V_5_a_1_3.Size = new System.Drawing.Size(109, 25);
            this.V_5_a_1_3.TabIndex = 112;
            this.V_5_a_1_3.Text = "0";
            this.V_5_a_1_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_5_a_1_2
            // 
            this.V_5_a_1_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_5_a_1_2.AutoSize = true;
            this.V_5_a_1_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_5_a_1_2.Location = new System.Drawing.Point(170, 107);
            this.V_5_a_1_2.Name = "V_5_a_1_2";
            this.V_5_a_1_2.Size = new System.Drawing.Size(109, 25);
            this.V_5_a_1_2.TabIndex = 108;
            this.V_5_a_1_2.Text = "0";
            this.V_5_a_1_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_5_a_1_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_5_b_1_1
            // 
            this.V_5_b_1_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_5_b_1_1.AutoSize = true;
            this.V_5_b_1_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_5_b_1_1.Location = new System.Drawing.Point(170, 185);
            this.V_5_b_1_1.Name = "V_5_b_1_1";
            this.V_5_b_1_1.Size = new System.Drawing.Size(109, 25);
            this.V_5_b_1_1.TabIndex = 116;
            this.V_5_b_1_1.Text = "0";
            this.V_5_b_1_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_5_b_1_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_5_b_1_2
            // 
            this.V_5_b_1_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_5_b_1_2.AutoSize = true;
            this.V_5_b_1_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_5_b_1_2.Location = new System.Drawing.Point(170, 211);
            this.V_5_b_1_2.Name = "V_5_b_1_2";
            this.V_5_b_1_2.Size = new System.Drawing.Size(109, 25);
            this.V_5_b_1_2.TabIndex = 220;
            this.V_5_b_1_2.Text = "0";
            this.V_5_b_1_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_5_b_1_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_5_b_1_3
            // 
            this.V_5_b_1_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_5_b_1_3.AutoSize = true;
            this.V_5_b_1_3.Location = new System.Drawing.Point(170, 237);
            this.V_5_b_1_3.Name = "V_5_b_1_3";
            this.V_5_b_1_3.Size = new System.Drawing.Size(109, 25);
            this.V_5_b_1_3.TabIndex = 224;
            this.V_5_b_1_3.Text = "0";
            this.V_5_b_1_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_5_c_1_1
            // 
            this.V_5_c_1_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_5_c_1_1.AutoSize = true;
            this.V_5_c_1_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_5_c_1_1.Location = new System.Drawing.Point(170, 289);
            this.V_5_c_1_1.Name = "V_5_c_1_1";
            this.V_5_c_1_1.Size = new System.Drawing.Size(109, 25);
            this.V_5_c_1_1.TabIndex = 228;
            this.V_5_c_1_1.Text = "0";
            this.V_5_c_1_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_5_c_1_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_5_c_1_2
            // 
            this.V_5_c_1_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_5_c_1_2.AutoSize = true;
            this.V_5_c_1_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_5_c_1_2.Location = new System.Drawing.Point(170, 315);
            this.V_5_c_1_2.Name = "V_5_c_1_2";
            this.V_5_c_1_2.Size = new System.Drawing.Size(109, 25);
            this.V_5_c_1_2.TabIndex = 232;
            this.V_5_c_1_2.Text = "0";
            this.V_5_c_1_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_5_c_1_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_5_c_1_3
            // 
            this.V_5_c_1_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_5_c_1_3.AutoSize = true;
            this.V_5_c_1_3.Location = new System.Drawing.Point(170, 341);
            this.V_5_c_1_3.Name = "V_5_c_1_3";
            this.V_5_c_1_3.Size = new System.Drawing.Size(109, 27);
            this.V_5_c_1_3.TabIndex = 236;
            this.V_5_c_1_3.Text = "0";
            this.V_5_c_1_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Sec_V_4_4
            // 
            this.Sec_V_4_4.AutoSize = true;
            this.Sec_V_4_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_V_4_4.Location = new System.Drawing.Point(3, 1974);
            this.Sec_V_4_4.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_V_4_4.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_V_4_4.Name = "Sec_V_4_4";
            this.Sec_V_4_4.Size = new System.Drawing.Size(413, 13);
            this.Sec_V_4_4.TabIndex = 59;
            this.Sec_V_4_4.Text = "- Lý do dẫn đến việc trích lập thêm hoặc hoàn nhập dự phòng giảm giá hàng tồn kho" +
    ":";
            this.Sec_V_4_4.Click += new System.EventHandler(this.label_edit);
            // 
            // Sec_V_4_3
            // 
            this.Sec_V_4_3.AutoSize = true;
            this.Sec_V_4_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_V_4_3.Location = new System.Drawing.Point(3, 1951);
            this.Sec_V_4_3.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_V_4_3.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_V_4_3.Name = "Sec_V_4_3";
            this.Sec_V_4_3.Size = new System.Drawing.Size(392, 13);
            this.Sec_V_4_3.TabIndex = 58;
            this.Sec_V_4_3.Text = "- Nguyên nhân và hướng xử lý đối với hàng tồn kho ứ đọng, kém, mất phẩm chất:";
            this.Sec_V_4_3.Click += new System.EventHandler(this.label_edit);
            // 
            // Sec_V_4_2
            // 
            this.Sec_V_4_2.AutoSize = true;
            this.Sec_V_4_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_V_4_2.Location = new System.Drawing.Point(3, 1928);
            this.Sec_V_4_2.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_V_4_2.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_V_4_2.Name = "Sec_V_4_2";
            this.Sec_V_4_2.Size = new System.Drawing.Size(390, 13);
            this.Sec_V_4_2.TabIndex = 57;
            this.Sec_V_4_2.Text = "- Giá trị hàng tồn kho dùng để thế chấp, cầm cố bảo đảm các khoản nợ phải trả:\t";
            this.Sec_V_4_2.Click += new System.EventHandler(this.label_edit);
            // 
            // Sec_V_4_1
            // 
            this.Sec_V_4_1.AutoSize = true;
            this.Sec_V_4_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_V_4_1.Location = new System.Drawing.Point(3, 1905);
            this.Sec_V_4_1.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_V_4_1.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_V_4_1.Name = "Sec_V_4_1";
            this.Sec_V_4_1.Size = new System.Drawing.Size(385, 13);
            this.Sec_V_4_1.TabIndex = 56;
            this.Sec_V_4_1.Text = "- Giá trị hàng tồn kho ứ đọng, kém, mất phẩm chất không có khả năng tiêu thụ: \t";
            this.Sec_V_4_1.Click += new System.EventHandler(this.label_edit);
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(3, 1882);
            this.label80.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(57, 13);
            this.label80.TabIndex = 55;
            this.label80.Text = "Trong đó: ";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel4.ColumnCount = 3;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 313F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.label69, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.label70, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.label71, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.label72, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.label73, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.label74, 0, 3);
            this.tableLayoutPanel4.Controls.Add(this.label75, 0, 4);
            this.tableLayoutPanel4.Controls.Add(this.label76, 0, 5);
            this.tableLayoutPanel4.Controls.Add(this.label77, 0, 6);
            this.tableLayoutPanel4.Controls.Add(this.label78, 0, 7);
            this.tableLayoutPanel4.Controls.Add(this.label79, 0, 8);
            this.tableLayoutPanel4.Controls.Add(this.V_4_1_1, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.V_4_2_1, 2, 1);
            this.tableLayoutPanel4.Controls.Add(this.V_4_2_2, 2, 2);
            this.tableLayoutPanel4.Controls.Add(this.V_4_1_2, 1, 2);
            this.tableLayoutPanel4.Controls.Add(this.V_4_1_3, 1, 3);
            this.tableLayoutPanel4.Controls.Add(this.V_4_2_3, 2, 3);
            this.tableLayoutPanel4.Controls.Add(this.V_4_2_4, 2, 4);
            this.tableLayoutPanel4.Controls.Add(this.V_4_1_4, 1, 4);
            this.tableLayoutPanel4.Controls.Add(this.V_4_1_5, 1, 5);
            this.tableLayoutPanel4.Controls.Add(this.V_4_2_5, 2, 5);
            this.tableLayoutPanel4.Controls.Add(this.V_4_2_6, 2, 6);
            this.tableLayoutPanel4.Controls.Add(this.V_4_1_6, 1, 6);
            this.tableLayoutPanel4.Controls.Add(this.V_4_1_7, 1, 7);
            this.tableLayoutPanel4.Controls.Add(this.V_4_2_7, 2, 7);
            this.tableLayoutPanel4.Controls.Add(this.V_4_Sum_2, 2, 8);
            this.tableLayoutPanel4.Controls.Add(this.V_4_Sum_1, 1, 8);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 1631);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 9;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(630, 238);
            this.tableLayoutPanel4.TabIndex = 54;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.Location = new System.Drawing.Point(4, 6);
            this.label69.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(106, 13);
            this.label69.TabIndex = 0;
            this.label69.Text = "04. Hàng tồn kho";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.Location = new System.Drawing.Point(365, 6);
            this.label70.Margin = new System.Windows.Forms.Padding(50, 5, 3, 0);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(59, 13);
            this.label70.TabIndex = 0;
            this.label70.Text = "Cuối năm";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.Location = new System.Drawing.Point(522, 6);
            this.label71.Margin = new System.Windows.Forms.Padding(50, 5, 3, 0);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(57, 13);
            this.label71.TabIndex = 0;
            this.label71.Text = "Đầu năm";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(4, 32);
            this.label72.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(134, 13);
            this.label72.TabIndex = 0;
            this.label72.Text = "- Hàng đang đi trên đường";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(4, 58);
            this.label73.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(109, 13);
            this.label73.TabIndex = 0;
            this.label73.Text = "- Nguyên liệu, vật liệu";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(4, 84);
            this.label74.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(95, 13);
            this.label74.TabIndex = 0;
            this.label74.Text = "- Công cụ dụng cụ";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(4, 110);
            this.label75.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(121, 13);
            this.label75.TabIndex = 0;
            this.label75.Text = "- Chi phí SXKD dở dang";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(4, 136);
            this.label76.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(73, 13);
            this.label76.TabIndex = 0;
            this.label76.Text = "- Thành phẩm";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(4, 162);
            this.label77.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(66, 13);
            this.label77.TabIndex = 0;
            this.label77.Text = "- Hoàng hoá";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(4, 188);
            this.label78.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(77, 13);
            this.label78.TabIndex = 0;
            this.label78.Text = "- Hàng gửi bán";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.Location = new System.Drawing.Point(51, 214);
            this.label79.Margin = new System.Windows.Forms.Padding(50, 5, 3, 0);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(36, 13);
            this.label79.TabIndex = 0;
            this.label79.Text = "Cộng";
            // 
            // V_4_1_1
            // 
            this.V_4_1_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_4_1_1.AutoSize = true;
            this.V_4_1_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_4_1_1.Location = new System.Drawing.Point(318, 27);
            this.V_4_1_1.Name = "V_4_1_1";
            this.V_4_1_1.Size = new System.Drawing.Size(150, 25);
            this.V_4_1_1.TabIndex = 88;
            this.V_4_1_1.Text = "0";
            this.V_4_1_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_4_1_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_4_2_1
            // 
            this.V_4_2_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_4_2_1.AutoSize = true;
            this.V_4_2_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_4_2_1.Location = new System.Drawing.Point(475, 27);
            this.V_4_2_1.Name = "V_4_2_1";
            this.V_4_2_1.Size = new System.Drawing.Size(151, 25);
            this.V_4_2_1.TabIndex = 89;
            this.V_4_2_1.Text = "0";
            this.V_4_2_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_4_2_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_4_2_2
            // 
            this.V_4_2_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_4_2_2.AutoSize = true;
            this.V_4_2_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_4_2_2.Location = new System.Drawing.Point(475, 53);
            this.V_4_2_2.Name = "V_4_2_2";
            this.V_4_2_2.Size = new System.Drawing.Size(151, 25);
            this.V_4_2_2.TabIndex = 91;
            this.V_4_2_2.Text = "0";
            this.V_4_2_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_4_2_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_4_1_2
            // 
            this.V_4_1_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_4_1_2.AutoSize = true;
            this.V_4_1_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_4_1_2.Location = new System.Drawing.Point(318, 53);
            this.V_4_1_2.Name = "V_4_1_2";
            this.V_4_1_2.Size = new System.Drawing.Size(150, 25);
            this.V_4_1_2.TabIndex = 90;
            this.V_4_1_2.Text = "0";
            this.V_4_1_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_4_1_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_4_1_3
            // 
            this.V_4_1_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_4_1_3.AutoSize = true;
            this.V_4_1_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_4_1_3.Location = new System.Drawing.Point(318, 79);
            this.V_4_1_3.Name = "V_4_1_3";
            this.V_4_1_3.Size = new System.Drawing.Size(150, 25);
            this.V_4_1_3.TabIndex = 92;
            this.V_4_1_3.Text = "0";
            this.V_4_1_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_4_1_3.Click += new System.EventHandler(this.number_edit);
            // 
            // V_4_2_3
            // 
            this.V_4_2_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_4_2_3.AutoSize = true;
            this.V_4_2_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_4_2_3.Location = new System.Drawing.Point(475, 79);
            this.V_4_2_3.Name = "V_4_2_3";
            this.V_4_2_3.Size = new System.Drawing.Size(151, 25);
            this.V_4_2_3.TabIndex = 93;
            this.V_4_2_3.Text = "0";
            this.V_4_2_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_4_2_3.Click += new System.EventHandler(this.number_edit);
            // 
            // V_4_2_4
            // 
            this.V_4_2_4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_4_2_4.AutoSize = true;
            this.V_4_2_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_4_2_4.Location = new System.Drawing.Point(475, 105);
            this.V_4_2_4.Name = "V_4_2_4";
            this.V_4_2_4.Size = new System.Drawing.Size(151, 25);
            this.V_4_2_4.TabIndex = 95;
            this.V_4_2_4.Text = "0";
            this.V_4_2_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_4_2_4.Click += new System.EventHandler(this.number_edit);
            // 
            // V_4_1_4
            // 
            this.V_4_1_4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_4_1_4.AutoSize = true;
            this.V_4_1_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_4_1_4.Location = new System.Drawing.Point(318, 105);
            this.V_4_1_4.Name = "V_4_1_4";
            this.V_4_1_4.Size = new System.Drawing.Size(150, 25);
            this.V_4_1_4.TabIndex = 94;
            this.V_4_1_4.Text = "0";
            this.V_4_1_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_4_1_4.Click += new System.EventHandler(this.number_edit);
            // 
            // V_4_1_5
            // 
            this.V_4_1_5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_4_1_5.AutoSize = true;
            this.V_4_1_5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_4_1_5.Location = new System.Drawing.Point(318, 131);
            this.V_4_1_5.Name = "V_4_1_5";
            this.V_4_1_5.Size = new System.Drawing.Size(150, 25);
            this.V_4_1_5.TabIndex = 96;
            this.V_4_1_5.Text = "0";
            this.V_4_1_5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_4_1_5.Click += new System.EventHandler(this.number_edit);
            // 
            // V_4_2_5
            // 
            this.V_4_2_5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_4_2_5.AutoSize = true;
            this.V_4_2_5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_4_2_5.Location = new System.Drawing.Point(475, 131);
            this.V_4_2_5.Name = "V_4_2_5";
            this.V_4_2_5.Size = new System.Drawing.Size(151, 25);
            this.V_4_2_5.TabIndex = 97;
            this.V_4_2_5.Text = "0";
            this.V_4_2_5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_4_2_5.Click += new System.EventHandler(this.number_edit);
            // 
            // V_4_2_6
            // 
            this.V_4_2_6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_4_2_6.AutoSize = true;
            this.V_4_2_6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_4_2_6.Location = new System.Drawing.Point(475, 157);
            this.V_4_2_6.Name = "V_4_2_6";
            this.V_4_2_6.Size = new System.Drawing.Size(151, 25);
            this.V_4_2_6.TabIndex = 99;
            this.V_4_2_6.Text = "0";
            this.V_4_2_6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_4_2_6.Click += new System.EventHandler(this.number_edit);
            // 
            // V_4_1_6
            // 
            this.V_4_1_6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_4_1_6.AutoSize = true;
            this.V_4_1_6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_4_1_6.Location = new System.Drawing.Point(318, 157);
            this.V_4_1_6.Name = "V_4_1_6";
            this.V_4_1_6.Size = new System.Drawing.Size(150, 25);
            this.V_4_1_6.TabIndex = 98;
            this.V_4_1_6.Text = "0";
            this.V_4_1_6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_4_1_6.Click += new System.EventHandler(this.number_edit);
            // 
            // V_4_1_7
            // 
            this.V_4_1_7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_4_1_7.AutoSize = true;
            this.V_4_1_7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_4_1_7.Location = new System.Drawing.Point(318, 183);
            this.V_4_1_7.Name = "V_4_1_7";
            this.V_4_1_7.Size = new System.Drawing.Size(150, 25);
            this.V_4_1_7.TabIndex = 100;
            this.V_4_1_7.Text = "0";
            this.V_4_1_7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_4_1_7.Click += new System.EventHandler(this.number_edit);
            // 
            // V_4_2_7
            // 
            this.V_4_2_7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_4_2_7.AutoSize = true;
            this.V_4_2_7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_4_2_7.Location = new System.Drawing.Point(475, 183);
            this.V_4_2_7.Name = "V_4_2_7";
            this.V_4_2_7.Size = new System.Drawing.Size(151, 25);
            this.V_4_2_7.TabIndex = 101;
            this.V_4_2_7.Text = "0";
            this.V_4_2_7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_4_2_7.Click += new System.EventHandler(this.number_edit);
            // 
            // V_4_Sum_2
            // 
            this.V_4_Sum_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_4_Sum_2.AutoSize = true;
            this.V_4_Sum_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.V_4_Sum_2.Location = new System.Drawing.Point(475, 209);
            this.V_4_Sum_2.Name = "V_4_Sum_2";
            this.V_4_Sum_2.Size = new System.Drawing.Size(151, 28);
            this.V_4_Sum_2.TabIndex = 103;
            this.V_4_Sum_2.Text = "0";
            this.V_4_Sum_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_4_Sum_1
            // 
            this.V_4_Sum_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_4_Sum_1.AutoSize = true;
            this.V_4_Sum_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.V_4_Sum_1.Location = new System.Drawing.Point(318, 209);
            this.V_4_Sum_1.Name = "V_4_Sum_1";
            this.V_4_Sum_1.Size = new System.Drawing.Size(150, 28);
            this.V_4_Sum_1.TabIndex = 102;
            this.V_4_Sum_1.Text = "0";
            this.V_4_Sum_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel3.ColumnCount = 3;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 312F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.label50, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label51, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.label52, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.label53, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label54, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.label55, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.label56, 0, 4);
            this.tableLayoutPanel3.Controls.Add(this.label57, 0, 5);
            this.tableLayoutPanel3.Controls.Add(this.label58, 0, 6);
            this.tableLayoutPanel3.Controls.Add(this.label59, 0, 7);
            this.tableLayoutPanel3.Controls.Add(this.label60, 0, 8);
            this.tableLayoutPanel3.Controls.Add(this.label61, 0, 9);
            this.tableLayoutPanel3.Controls.Add(this.label62, 0, 10);
            this.tableLayoutPanel3.Controls.Add(this.label63, 0, 11);
            this.tableLayoutPanel3.Controls.Add(this.label64, 0, 12);
            this.tableLayoutPanel3.Controls.Add(this.label65, 0, 13);
            this.tableLayoutPanel3.Controls.Add(this.label66, 0, 14);
            this.tableLayoutPanel3.Controls.Add(this.label67, 0, 15);
            this.tableLayoutPanel3.Controls.Add(this.label68, 0, 16);
            this.tableLayoutPanel3.Controls.Add(this.V_3_a_2_1, 2, 2);
            this.tableLayoutPanel3.Controls.Add(this.V_3_a_1_1, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.V_3_b_1_1, 1, 4);
            this.tableLayoutPanel3.Controls.Add(this.V_3_b_2_1, 2, 4);
            this.tableLayoutPanel3.Controls.Add(this.V_3_a_1, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.V_3_a_2, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.V_3_b_2, 2, 3);
            this.tableLayoutPanel3.Controls.Add(this.V_3_b_1, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.V_3_c_1, 1, 5);
            this.tableLayoutPanel3.Controls.Add(this.V_3_c_2, 2, 5);
            this.tableLayoutPanel3.Controls.Add(this.V_3_c_2_1, 2, 6);
            this.tableLayoutPanel3.Controls.Add(this.V_3_c_1_1, 1, 6);
            this.tableLayoutPanel3.Controls.Add(this.V_3_c_1_2, 1, 7);
            this.tableLayoutPanel3.Controls.Add(this.V_3_c_2_2, 2, 7);
            this.tableLayoutPanel3.Controls.Add(this.V_3_c_2_3, 2, 8);
            this.tableLayoutPanel3.Controls.Add(this.V_3_c_1_3, 1, 8);
            this.tableLayoutPanel3.Controls.Add(this.V_3_c_1_4, 1, 9);
            this.tableLayoutPanel3.Controls.Add(this.V_3_c_2_4, 2, 9);
            this.tableLayoutPanel3.Controls.Add(this.V_3_d_2, 2, 10);
            this.tableLayoutPanel3.Controls.Add(this.V_3_d_1, 1, 10);
            this.tableLayoutPanel3.Controls.Add(this.V_3_d_1_1, 1, 11);
            this.tableLayoutPanel3.Controls.Add(this.V_3_d_2_1, 2, 11);
            this.tableLayoutPanel3.Controls.Add(this.V_3_d_2_2, 2, 12);
            this.tableLayoutPanel3.Controls.Add(this.V_3_d_1_2, 1, 12);
            this.tableLayoutPanel3.Controls.Add(this.V_3_d_1_3, 1, 13);
            this.tableLayoutPanel3.Controls.Add(this.V_3_d_2_3, 2, 13);
            this.tableLayoutPanel3.Controls.Add(this.V_3_d_2_4, 2, 14);
            this.tableLayoutPanel3.Controls.Add(this.V_3_d_1_4, 1, 14);
            this.tableLayoutPanel3.Controls.Add(this.V_3_e_1, 1, 15);
            this.tableLayoutPanel3.Controls.Add(this.V_3_e_2, 2, 15);
            this.tableLayoutPanel3.Controls.Add(this.V_3_Sum_2, 2, 16);
            this.tableLayoutPanel3.Controls.Add(this.V_3_Sum_1, 1, 16);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 1126);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 17;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(630, 485);
            this.tableLayoutPanel3.TabIndex = 53;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(4, 6);
            this.label50.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(296, 39);
            this.label50.TabIndex = 0;
            this.label50.Text = "03. Các khoản phải thu (Tuỳ theo yêu cầu quản lý của DN, có thể thuyết minh chi t" +
    "iết ngắn hạn, dài hạn)\t\t\t\t";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(364, 6);
            this.label51.Margin = new System.Windows.Forms.Padding(50, 5, 3, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(59, 13);
            this.label51.TabIndex = 0;
            this.label51.Text = "Cuối năm";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(522, 6);
            this.label52.Margin = new System.Windows.Forms.Padding(50, 5, 3, 0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(57, 13);
            this.label52.TabIndex = 0;
            this.label52.Text = "Đầu năm";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(16, 61);
            this.label53.Margin = new System.Windows.Forms.Padding(15, 5, 3, 0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(140, 13);
            this.label53.TabIndex = 0;
            this.label53.Text = "a. Phải thu khách hàng";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(4, 86);
            this.label54.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(205, 13);
            this.label54.TabIndex = 0;
            this.label54.Text = "Trong đó: Phải thu của các bên liên quan";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(16, 112);
            this.label55.Margin = new System.Windows.Forms.Padding(15, 5, 3, 0);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(161, 13);
            this.label55.TabIndex = 0;
            this.label55.Text = "b. Trả trước cho Người bán";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(4, 138);
            this.label56.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(209, 13);
            this.label56.TabIndex = 0;
            this.label56.Text = "Trong đó: Trả trước cho các bên liên quan";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(16, 165);
            this.label57.Margin = new System.Windows.Forms.Padding(15, 5, 3, 0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(275, 13);
            this.label57.TabIndex = 0;
            this.label57.Text = "c. Phải thu khác (chi tiết theo yêu cầu quản lý)";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(4, 191);
            this.label58.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(108, 13);
            this.label58.TabIndex = 0;
            this.label58.Text = "- Phải thu về cho vay";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(4, 217);
            this.label59.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(55, 13);
            this.label59.TabIndex = 0;
            this.label59.Text = "- Tạm ứng";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(4, 241);
            this.label60.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(111, 13);
            this.label60.TabIndex = 0;
            this.label60.Text = "- Phải thu nội bộ khác";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(4, 267);
            this.label61.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(79, 13);
            this.label61.TabIndex = 0;
            this.label61.Text = "- Phải thu khác";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.Location = new System.Drawing.Point(16, 293);
            this.label62.Margin = new System.Windows.Forms.Padding(15, 5, 3, 0);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(151, 13);
            this.label62.TabIndex = 0;
            this.label62.Text = "d. Tài sản thiếu chờ xử lý";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(4, 320);
            this.label63.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(34, 13);
            this.label63.TabIndex = 0;
            this.label63.Text = "- Tiền";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(4, 346);
            this.label64.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(78, 13);
            this.label64.TabIndex = 0;
            this.label64.Text = "- Hàng tồn kho";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(4, 370);
            this.label65.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(42, 13);
            this.label65.TabIndex = 0;
            this.label65.Text = "- TSCĐ";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(4, 394);
            this.label66.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(75, 13);
            this.label66.TabIndex = 0;
            this.label66.Text = "- Tài sản khác";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.Location = new System.Drawing.Point(16, 418);
            this.label67.Margin = new System.Windows.Forms.Padding(15, 5, 3, 0);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(283, 39);
            this.label67.TabIndex = 0;
            this.label67.Text = "đ. Nợ xấu (Tổng giá trị các khoản phải thu, cho vay quá hạn thanh  toán hoặc chưa" +
    " quá hạn nhưng khó có khả năng thu hồi)";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.Location = new System.Drawing.Point(51, 468);
            this.label68.Margin = new System.Windows.Forms.Padding(50, 5, 3, 0);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(36, 13);
            this.label68.TabIndex = 0;
            this.label68.Text = "Cộng";
            // 
            // V_3_a_2_1
            // 
            this.V_3_a_2_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_3_a_2_1.AutoSize = true;
            this.V_3_a_2_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_3_a_2_1.Location = new System.Drawing.Point(475, 81);
            this.V_3_a_2_1.Name = "V_3_a_2_1";
            this.V_3_a_2_1.Size = new System.Drawing.Size(151, 25);
            this.V_3_a_2_1.TabIndex = 459;
            this.V_3_a_2_1.Text = "0";
            this.V_3_a_2_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_3_a_2_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_3_a_1_1
            // 
            this.V_3_a_1_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_3_a_1_1.AutoSize = true;
            this.V_3_a_1_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_3_a_1_1.Location = new System.Drawing.Point(317, 81);
            this.V_3_a_1_1.Name = "V_3_a_1_1";
            this.V_3_a_1_1.Size = new System.Drawing.Size(151, 25);
            this.V_3_a_1_1.TabIndex = 58;
            this.V_3_a_1_1.Text = "0";
            this.V_3_a_1_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_3_a_1_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_3_b_1_1
            // 
            this.V_3_b_1_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_3_b_1_1.AutoSize = true;
            this.V_3_b_1_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_3_b_1_1.Location = new System.Drawing.Point(317, 133);
            this.V_3_b_1_1.Name = "V_3_b_1_1";
            this.V_3_b_1_1.Size = new System.Drawing.Size(151, 26);
            this.V_3_b_1_1.TabIndex = 62;
            this.V_3_b_1_1.Text = "0";
            this.V_3_b_1_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_3_b_1_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_3_b_2_1
            // 
            this.V_3_b_2_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_3_b_2_1.AutoSize = true;
            this.V_3_b_2_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_3_b_2_1.Location = new System.Drawing.Point(475, 133);
            this.V_3_b_2_1.Name = "V_3_b_2_1";
            this.V_3_b_2_1.Size = new System.Drawing.Size(151, 26);
            this.V_3_b_2_1.TabIndex = 63;
            this.V_3_b_2_1.Text = "0";
            this.V_3_b_2_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_3_b_2_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_3_a_1
            // 
            this.V_3_a_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_3_a_1.AutoSize = true;
            this.V_3_a_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_3_a_1.Location = new System.Drawing.Point(317, 56);
            this.V_3_a_1.Name = "V_3_a_1";
            this.V_3_a_1.Size = new System.Drawing.Size(151, 24);
            this.V_3_a_1.TabIndex = 56;
            this.V_3_a_1.Text = "0";
            this.V_3_a_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_3_a_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_3_a_2
            // 
            this.V_3_a_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_3_a_2.AutoSize = true;
            this.V_3_a_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_3_a_2.Location = new System.Drawing.Point(475, 56);
            this.V_3_a_2.Name = "V_3_a_2";
            this.V_3_a_2.Size = new System.Drawing.Size(151, 24);
            this.V_3_a_2.TabIndex = 57;
            this.V_3_a_2.Text = "0";
            this.V_3_a_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_3_a_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_3_b_2
            // 
            this.V_3_b_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_3_b_2.AutoSize = true;
            this.V_3_b_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_3_b_2.Location = new System.Drawing.Point(475, 107);
            this.V_3_b_2.Name = "V_3_b_2";
            this.V_3_b_2.Size = new System.Drawing.Size(151, 25);
            this.V_3_b_2.TabIndex = 61;
            this.V_3_b_2.Text = "0";
            this.V_3_b_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_3_b_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_3_b_1
            // 
            this.V_3_b_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_3_b_1.AutoSize = true;
            this.V_3_b_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_3_b_1.Location = new System.Drawing.Point(317, 107);
            this.V_3_b_1.Name = "V_3_b_1";
            this.V_3_b_1.Size = new System.Drawing.Size(151, 25);
            this.V_3_b_1.TabIndex = 60;
            this.V_3_b_1.Text = "0";
            this.V_3_b_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_3_b_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_3_c_1
            // 
            this.V_3_c_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_3_c_1.AutoSize = true;
            this.V_3_c_1.Location = new System.Drawing.Point(317, 160);
            this.V_3_c_1.Name = "V_3_c_1";
            this.V_3_c_1.Size = new System.Drawing.Size(151, 25);
            this.V_3_c_1.TabIndex = 64;
            this.V_3_c_1.Text = "0";
            this.V_3_c_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_3_c_2
            // 
            this.V_3_c_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_3_c_2.AutoSize = true;
            this.V_3_c_2.Location = new System.Drawing.Point(475, 160);
            this.V_3_c_2.Name = "V_3_c_2";
            this.V_3_c_2.Size = new System.Drawing.Size(151, 25);
            this.V_3_c_2.TabIndex = 65;
            this.V_3_c_2.Text = "0";
            this.V_3_c_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_3_c_2_1
            // 
            this.V_3_c_2_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_3_c_2_1.AutoSize = true;
            this.V_3_c_2_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_3_c_2_1.Location = new System.Drawing.Point(475, 186);
            this.V_3_c_2_1.Name = "V_3_c_2_1";
            this.V_3_c_2_1.Size = new System.Drawing.Size(151, 25);
            this.V_3_c_2_1.TabIndex = 67;
            this.V_3_c_2_1.Text = "0";
            this.V_3_c_2_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_3_c_2_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_3_c_1_1
            // 
            this.V_3_c_1_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_3_c_1_1.AutoSize = true;
            this.V_3_c_1_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_3_c_1_1.Location = new System.Drawing.Point(317, 186);
            this.V_3_c_1_1.Name = "V_3_c_1_1";
            this.V_3_c_1_1.Size = new System.Drawing.Size(151, 25);
            this.V_3_c_1_1.TabIndex = 66;
            this.V_3_c_1_1.Text = "0";
            this.V_3_c_1_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_3_c_1_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_3_c_1_2
            // 
            this.V_3_c_1_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_3_c_1_2.AutoSize = true;
            this.V_3_c_1_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_3_c_1_2.Location = new System.Drawing.Point(317, 212);
            this.V_3_c_1_2.Name = "V_3_c_1_2";
            this.V_3_c_1_2.Size = new System.Drawing.Size(151, 23);
            this.V_3_c_1_2.TabIndex = 68;
            this.V_3_c_1_2.Text = "0";
            this.V_3_c_1_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_3_c_1_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_3_c_2_2
            // 
            this.V_3_c_2_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_3_c_2_2.AutoSize = true;
            this.V_3_c_2_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_3_c_2_2.Location = new System.Drawing.Point(475, 212);
            this.V_3_c_2_2.Name = "V_3_c_2_2";
            this.V_3_c_2_2.Size = new System.Drawing.Size(151, 23);
            this.V_3_c_2_2.TabIndex = 69;
            this.V_3_c_2_2.Text = "0";
            this.V_3_c_2_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_3_c_2_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_3_c_2_3
            // 
            this.V_3_c_2_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_3_c_2_3.AutoSize = true;
            this.V_3_c_2_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_3_c_2_3.Location = new System.Drawing.Point(475, 236);
            this.V_3_c_2_3.Name = "V_3_c_2_3";
            this.V_3_c_2_3.Size = new System.Drawing.Size(151, 25);
            this.V_3_c_2_3.TabIndex = 71;
            this.V_3_c_2_3.Text = "0";
            this.V_3_c_2_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_3_c_2_3.Click += new System.EventHandler(this.number_edit);
            // 
            // V_3_c_1_3
            // 
            this.V_3_c_1_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_3_c_1_3.AutoSize = true;
            this.V_3_c_1_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_3_c_1_3.Location = new System.Drawing.Point(317, 236);
            this.V_3_c_1_3.Name = "V_3_c_1_3";
            this.V_3_c_1_3.Size = new System.Drawing.Size(151, 25);
            this.V_3_c_1_3.TabIndex = 70;
            this.V_3_c_1_3.Text = "0";
            this.V_3_c_1_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_3_c_1_3.Click += new System.EventHandler(this.number_edit);
            // 
            // V_3_c_1_4
            // 
            this.V_3_c_1_4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_3_c_1_4.AutoSize = true;
            this.V_3_c_1_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_3_c_1_4.Location = new System.Drawing.Point(317, 262);
            this.V_3_c_1_4.Name = "V_3_c_1_4";
            this.V_3_c_1_4.Size = new System.Drawing.Size(151, 25);
            this.V_3_c_1_4.TabIndex = 72;
            this.V_3_c_1_4.Text = "0";
            this.V_3_c_1_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_3_c_1_4.Click += new System.EventHandler(this.number_edit);
            // 
            // V_3_c_2_4
            // 
            this.V_3_c_2_4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_3_c_2_4.AutoSize = true;
            this.V_3_c_2_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_3_c_2_4.Location = new System.Drawing.Point(475, 262);
            this.V_3_c_2_4.Name = "V_3_c_2_4";
            this.V_3_c_2_4.Size = new System.Drawing.Size(151, 25);
            this.V_3_c_2_4.TabIndex = 73;
            this.V_3_c_2_4.Text = "0";
            this.V_3_c_2_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_3_c_2_4.Click += new System.EventHandler(this.number_edit);
            // 
            // V_3_d_2
            // 
            this.V_3_d_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_3_d_2.AutoSize = true;
            this.V_3_d_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_3_d_2.Location = new System.Drawing.Point(475, 288);
            this.V_3_d_2.Name = "V_3_d_2";
            this.V_3_d_2.Size = new System.Drawing.Size(151, 26);
            this.V_3_d_2.TabIndex = 75;
            this.V_3_d_2.Text = "0";
            this.V_3_d_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_3_d_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_3_d_1
            // 
            this.V_3_d_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_3_d_1.AutoSize = true;
            this.V_3_d_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_3_d_1.Location = new System.Drawing.Point(317, 288);
            this.V_3_d_1.Name = "V_3_d_1";
            this.V_3_d_1.Size = new System.Drawing.Size(151, 26);
            this.V_3_d_1.TabIndex = 74;
            this.V_3_d_1.Text = "0";
            this.V_3_d_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_3_d_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_3_d_1_1
            // 
            this.V_3_d_1_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_3_d_1_1.AutoSize = true;
            this.V_3_d_1_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_3_d_1_1.Location = new System.Drawing.Point(317, 315);
            this.V_3_d_1_1.Name = "V_3_d_1_1";
            this.V_3_d_1_1.Size = new System.Drawing.Size(151, 25);
            this.V_3_d_1_1.TabIndex = 76;
            this.V_3_d_1_1.Text = "0";
            this.V_3_d_1_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_3_d_1_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_3_d_2_1
            // 
            this.V_3_d_2_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_3_d_2_1.AutoSize = true;
            this.V_3_d_2_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_3_d_2_1.Location = new System.Drawing.Point(475, 315);
            this.V_3_d_2_1.Name = "V_3_d_2_1";
            this.V_3_d_2_1.Size = new System.Drawing.Size(151, 25);
            this.V_3_d_2_1.TabIndex = 77;
            this.V_3_d_2_1.Text = "0";
            this.V_3_d_2_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_3_d_2_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_3_d_2_2
            // 
            this.V_3_d_2_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_3_d_2_2.AutoSize = true;
            this.V_3_d_2_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_3_d_2_2.Location = new System.Drawing.Point(475, 341);
            this.V_3_d_2_2.Name = "V_3_d_2_2";
            this.V_3_d_2_2.Size = new System.Drawing.Size(151, 23);
            this.V_3_d_2_2.TabIndex = 79;
            this.V_3_d_2_2.Text = "0";
            this.V_3_d_2_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_3_d_2_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_3_d_1_2
            // 
            this.V_3_d_1_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_3_d_1_2.AutoSize = true;
            this.V_3_d_1_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_3_d_1_2.Location = new System.Drawing.Point(317, 341);
            this.V_3_d_1_2.Name = "V_3_d_1_2";
            this.V_3_d_1_2.Size = new System.Drawing.Size(151, 23);
            this.V_3_d_1_2.TabIndex = 78;
            this.V_3_d_1_2.Text = "0";
            this.V_3_d_1_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_3_d_1_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_3_d_1_3
            // 
            this.V_3_d_1_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_3_d_1_3.AutoSize = true;
            this.V_3_d_1_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_3_d_1_3.Location = new System.Drawing.Point(317, 365);
            this.V_3_d_1_3.Name = "V_3_d_1_3";
            this.V_3_d_1_3.Size = new System.Drawing.Size(151, 23);
            this.V_3_d_1_3.TabIndex = 80;
            this.V_3_d_1_3.Text = "0";
            this.V_3_d_1_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_3_d_1_3.Click += new System.EventHandler(this.number_edit);
            // 
            // V_3_d_2_3
            // 
            this.V_3_d_2_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_3_d_2_3.AutoSize = true;
            this.V_3_d_2_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_3_d_2_3.Location = new System.Drawing.Point(475, 365);
            this.V_3_d_2_3.Name = "V_3_d_2_3";
            this.V_3_d_2_3.Size = new System.Drawing.Size(151, 23);
            this.V_3_d_2_3.TabIndex = 81;
            this.V_3_d_2_3.Text = "0";
            this.V_3_d_2_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_3_d_2_3.Click += new System.EventHandler(this.number_edit);
            // 
            // V_3_d_2_4
            // 
            this.V_3_d_2_4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_3_d_2_4.AutoSize = true;
            this.V_3_d_2_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_3_d_2_4.Location = new System.Drawing.Point(475, 389);
            this.V_3_d_2_4.Name = "V_3_d_2_4";
            this.V_3_d_2_4.Size = new System.Drawing.Size(151, 23);
            this.V_3_d_2_4.TabIndex = 83;
            this.V_3_d_2_4.Text = "0";
            this.V_3_d_2_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_3_d_2_4.Click += new System.EventHandler(this.number_edit);
            // 
            // V_3_d_1_4
            // 
            this.V_3_d_1_4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_3_d_1_4.AutoSize = true;
            this.V_3_d_1_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_3_d_1_4.Location = new System.Drawing.Point(317, 389);
            this.V_3_d_1_4.Name = "V_3_d_1_4";
            this.V_3_d_1_4.Size = new System.Drawing.Size(151, 23);
            this.V_3_d_1_4.TabIndex = 82;
            this.V_3_d_1_4.Text = "0";
            this.V_3_d_1_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_3_d_1_4.Click += new System.EventHandler(this.number_edit);
            // 
            // V_3_e_1
            // 
            this.V_3_e_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_3_e_1.AutoSize = true;
            this.V_3_e_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_3_e_1.Location = new System.Drawing.Point(317, 413);
            this.V_3_e_1.Name = "V_3_e_1";
            this.V_3_e_1.Size = new System.Drawing.Size(151, 49);
            this.V_3_e_1.TabIndex = 84;
            this.V_3_e_1.Text = "0";
            this.V_3_e_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_3_e_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_3_e_2
            // 
            this.V_3_e_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_3_e_2.AutoSize = true;
            this.V_3_e_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_3_e_2.Location = new System.Drawing.Point(475, 413);
            this.V_3_e_2.Name = "V_3_e_2";
            this.V_3_e_2.Size = new System.Drawing.Size(151, 49);
            this.V_3_e_2.TabIndex = 85;
            this.V_3_e_2.Text = "0";
            this.V_3_e_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_3_e_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_3_Sum_2
            // 
            this.V_3_Sum_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_3_Sum_2.AutoSize = true;
            this.V_3_Sum_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.V_3_Sum_2.Location = new System.Drawing.Point(475, 463);
            this.V_3_Sum_2.Name = "V_3_Sum_2";
            this.V_3_Sum_2.Size = new System.Drawing.Size(151, 21);
            this.V_3_Sum_2.TabIndex = 87;
            this.V_3_Sum_2.Text = "0";
            this.V_3_Sum_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_3_Sum_1
            // 
            this.V_3_Sum_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_3_Sum_1.AutoSize = true;
            this.V_3_Sum_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.V_3_Sum_1.Location = new System.Drawing.Point(317, 463);
            this.V_3_Sum_1.Name = "V_3_Sum_1";
            this.V_3_Sum_1.Size = new System.Drawing.Size(151, 21);
            this.V_3_Sum_1.TabIndex = 86;
            this.V_3_Sum_1.Text = "0";
            this.V_3_Sum_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 312F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.label36, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label37, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label38, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.label39, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label40, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.label41, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.label42, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.label43, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.label44, 0, 6);
            this.tableLayoutPanel2.Controls.Add(this.label45, 0, 7);
            this.tableLayoutPanel2.Controls.Add(this.label46, 0, 8);
            this.tableLayoutPanel2.Controls.Add(this.label47, 0, 9);
            this.tableLayoutPanel2.Controls.Add(this.label48, 0, 10);
            this.tableLayoutPanel2.Controls.Add(this.label49, 0, 11);
            this.tableLayoutPanel2.Controls.Add(this.V_2_a_2_1, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.V_2_a_1_1, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.V_2_a_1_2, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.V_2_a_2_2, 2, 3);
            this.tableLayoutPanel2.Controls.Add(this.V_2_a_1_3, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.V_2_a_2_3, 2, 4);
            this.tableLayoutPanel2.Controls.Add(this.V_2_b_1_1, 1, 6);
            this.tableLayoutPanel2.Controls.Add(this.V_2_b_2_1, 2, 6);
            this.tableLayoutPanel2.Controls.Add(this.V_2_b_1_2, 1, 7);
            this.tableLayoutPanel2.Controls.Add(this.V_2_b_2_2, 2, 7);
            this.tableLayoutPanel2.Controls.Add(this.V_2_c_2_1, 2, 9);
            this.tableLayoutPanel2.Controls.Add(this.V_2_c_1_1, 1, 9);
            this.tableLayoutPanel2.Controls.Add(this.V_2_c_2_2, 2, 10);
            this.tableLayoutPanel2.Controls.Add(this.V_2_Sum_2, 2, 11);
            this.tableLayoutPanel2.Controls.Add(this.V_2_Sum_1, 1, 11);
            this.tableLayoutPanel2.Controls.Add(this.V_2_a_1, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.V_2_a_2, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.V_2_b_2, 2, 5);
            this.tableLayoutPanel2.Controls.Add(this.V_2_b_1, 1, 5);
            this.tableLayoutPanel2.Controls.Add(this.V_2_c_1, 1, 8);
            this.tableLayoutPanel2.Controls.Add(this.V_2_c_2, 2, 8);
            this.tableLayoutPanel2.Controls.Add(this.V_2_c_1_2, 1, 10);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 809);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 12;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 54.16667F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 45.83333F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(630, 297);
            this.tableLayoutPanel2.TabIndex = 51;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(4, 6);
            this.label36.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(186, 13);
            this.label36.TabIndex = 0;
            this.label36.Text = "02. Các khoản đầu tư tài chính";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(364, 6);
            this.label37.Margin = new System.Windows.Forms.Padding(50, 5, 3, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(59, 13);
            this.label37.TabIndex = 1;
            this.label37.Text = "Cuối năm\t";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(522, 6);
            this.label38.Margin = new System.Windows.Forms.Padding(50, 5, 3, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(57, 13);
            this.label38.TabIndex = 2;
            this.label38.Text = "Đầu năm";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(16, 35);
            this.label39.Margin = new System.Windows.Forms.Padding(15, 5, 3, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(164, 13);
            this.label39.TabIndex = 3;
            this.label39.Text = "a) Chứng khoán kinh doanh";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(4, 59);
            this.label40.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(110, 13);
            this.label40.TabIndex = 4;
            this.label40.Text = "- Tổng giá trị cổ phiểu";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(4, 82);
            this.label41.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(112, 13);
            this.label41.TabIndex = 5;
            this.label41.Text = "- Tổng giá trị trái phiếu";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(4, 105);
            this.label42.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(144, 13);
            this.label42.TabIndex = 6;
            this.label42.Text = "- Các loại chứng khoán khác";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(16, 129);
            this.label43.Margin = new System.Windows.Forms.Padding(15, 5, 3, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(216, 13);
            this.label43.TabIndex = 7;
            this.label43.Text = "b) Đầu tư nắm giữ đến ngày đáo hạn";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(4, 154);
            this.label44.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(101, 13);
            this.label44.TabIndex = 8;
            this.label44.Text = "- Tiền gửi có kỳ hạn";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(4, 179);
            this.label45.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(257, 13);
            this.label45.TabIndex = 9;
            this.label45.Text = "- Các khoản đầu tư khác nắm giữ đến ngày đáo hạn";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(16, 204);
            this.label46.Margin = new System.Windows.Forms.Padding(15, 5, 3, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(219, 13);
            this.label46.TabIndex = 10;
            this.label46.Text = "c) Dự phòng tổn thất đầu tư tài chinh";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(4, 229);
            this.label47.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(224, 13);
            this.label47.TabIndex = 11;
            this.label47.Text = "- Dự phòng giảm giá chứng khoán kinh doanh";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(4, 254);
            this.label48.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(214, 13);
            this.label48.TabIndex = 12;
            this.label48.Text = "- Dự phòng tổn thất đầu tư vào đơn vị khác";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(51, 279);
            this.label49.Margin = new System.Windows.Forms.Padding(50, 5, 3, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(36, 13);
            this.label49.TabIndex = 13;
            this.label49.Text = "Cộng";
            // 
            // V_2_a_2_1
            // 
            this.V_2_a_2_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_2_a_2_1.AutoSize = true;
            this.V_2_a_2_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_2_a_2_1.Location = new System.Drawing.Point(475, 54);
            this.V_2_a_2_1.Name = "V_2_a_2_1";
            this.V_2_a_2_1.Size = new System.Drawing.Size(151, 22);
            this.V_2_a_2_1.TabIndex = 37;
            this.V_2_a_2_1.Text = "0";
            this.V_2_a_2_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_2_a_2_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_2_a_1_1
            // 
            this.V_2_a_1_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_2_a_1_1.AutoSize = true;
            this.V_2_a_1_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_2_a_1_1.Location = new System.Drawing.Point(317, 54);
            this.V_2_a_1_1.Name = "V_2_a_1_1";
            this.V_2_a_1_1.Size = new System.Drawing.Size(151, 22);
            this.V_2_a_1_1.TabIndex = 36;
            this.V_2_a_1_1.Text = "0";
            this.V_2_a_1_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_2_a_1_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_2_a_1_2
            // 
            this.V_2_a_1_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_2_a_1_2.AutoSize = true;
            this.V_2_a_1_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_2_a_1_2.Location = new System.Drawing.Point(317, 77);
            this.V_2_a_1_2.Name = "V_2_a_1_2";
            this.V_2_a_1_2.Size = new System.Drawing.Size(151, 22);
            this.V_2_a_1_2.TabIndex = 38;
            this.V_2_a_1_2.Text = "0";
            this.V_2_a_1_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_2_a_1_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_2_a_2_2
            // 
            this.V_2_a_2_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_2_a_2_2.AutoSize = true;
            this.V_2_a_2_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_2_a_2_2.Location = new System.Drawing.Point(475, 77);
            this.V_2_a_2_2.Name = "V_2_a_2_2";
            this.V_2_a_2_2.Size = new System.Drawing.Size(151, 22);
            this.V_2_a_2_2.TabIndex = 39;
            this.V_2_a_2_2.Text = "0";
            this.V_2_a_2_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_2_a_2_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_2_a_1_3
            // 
            this.V_2_a_1_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_2_a_1_3.AutoSize = true;
            this.V_2_a_1_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_2_a_1_3.Location = new System.Drawing.Point(317, 100);
            this.V_2_a_1_3.Name = "V_2_a_1_3";
            this.V_2_a_1_3.Size = new System.Drawing.Size(151, 23);
            this.V_2_a_1_3.TabIndex = 40;
            this.V_2_a_1_3.Text = "0";
            this.V_2_a_1_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_2_a_1_3.Click += new System.EventHandler(this.number_edit);
            // 
            // V_2_a_2_3
            // 
            this.V_2_a_2_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_2_a_2_3.AutoSize = true;
            this.V_2_a_2_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_2_a_2_3.Location = new System.Drawing.Point(475, 100);
            this.V_2_a_2_3.Name = "V_2_a_2_3";
            this.V_2_a_2_3.Size = new System.Drawing.Size(151, 23);
            this.V_2_a_2_3.TabIndex = 41;
            this.V_2_a_2_3.Text = "0";
            this.V_2_a_2_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_2_a_2_3.Click += new System.EventHandler(this.number_edit);
            // 
            // V_2_b_1_1
            // 
            this.V_2_b_1_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_2_b_1_1.AutoSize = true;
            this.V_2_b_1_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_2_b_1_1.Location = new System.Drawing.Point(317, 149);
            this.V_2_b_1_1.Name = "V_2_b_1_1";
            this.V_2_b_1_1.Size = new System.Drawing.Size(151, 24);
            this.V_2_b_1_1.TabIndex = 44;
            this.V_2_b_1_1.Text = "0";
            this.V_2_b_1_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_2_b_1_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_2_b_2_1
            // 
            this.V_2_b_2_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_2_b_2_1.AutoSize = true;
            this.V_2_b_2_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_2_b_2_1.Location = new System.Drawing.Point(475, 149);
            this.V_2_b_2_1.Name = "V_2_b_2_1";
            this.V_2_b_2_1.Size = new System.Drawing.Size(151, 24);
            this.V_2_b_2_1.TabIndex = 45;
            this.V_2_b_2_1.Text = "0";
            this.V_2_b_2_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_2_b_2_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_2_b_1_2
            // 
            this.V_2_b_1_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_2_b_1_2.AutoSize = true;
            this.V_2_b_1_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_2_b_1_2.Location = new System.Drawing.Point(317, 174);
            this.V_2_b_1_2.Name = "V_2_b_1_2";
            this.V_2_b_1_2.Size = new System.Drawing.Size(151, 24);
            this.V_2_b_1_2.TabIndex = 46;
            this.V_2_b_1_2.Text = "0";
            this.V_2_b_1_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_2_b_1_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_2_b_2_2
            // 
            this.V_2_b_2_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_2_b_2_2.AutoSize = true;
            this.V_2_b_2_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_2_b_2_2.Location = new System.Drawing.Point(475, 174);
            this.V_2_b_2_2.Name = "V_2_b_2_2";
            this.V_2_b_2_2.Size = new System.Drawing.Size(151, 24);
            this.V_2_b_2_2.TabIndex = 47;
            this.V_2_b_2_2.Text = "0";
            this.V_2_b_2_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_2_b_2_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_2_c_2_1
            // 
            this.V_2_c_2_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_2_c_2_1.AutoSize = true;
            this.V_2_c_2_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_2_c_2_1.Location = new System.Drawing.Point(475, 224);
            this.V_2_c_2_1.Name = "V_2_c_2_1";
            this.V_2_c_2_1.Size = new System.Drawing.Size(151, 24);
            this.V_2_c_2_1.TabIndex = 51;
            this.V_2_c_2_1.Text = "0";
            this.V_2_c_2_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_2_c_2_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_2_c_1_1
            // 
            this.V_2_c_1_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_2_c_1_1.AutoSize = true;
            this.V_2_c_1_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_2_c_1_1.Location = new System.Drawing.Point(317, 224);
            this.V_2_c_1_1.Name = "V_2_c_1_1";
            this.V_2_c_1_1.Size = new System.Drawing.Size(151, 24);
            this.V_2_c_1_1.TabIndex = 50;
            this.V_2_c_1_1.Text = "0";
            this.V_2_c_1_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_2_c_1_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_2_c_2_2
            // 
            this.V_2_c_2_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_2_c_2_2.AutoSize = true;
            this.V_2_c_2_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_2_c_2_2.Location = new System.Drawing.Point(475, 249);
            this.V_2_c_2_2.Name = "V_2_c_2_2";
            this.V_2_c_2_2.Size = new System.Drawing.Size(151, 24);
            this.V_2_c_2_2.TabIndex = 53;
            this.V_2_c_2_2.Text = "0";
            this.V_2_c_2_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_2_c_2_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_2_Sum_2
            // 
            this.V_2_Sum_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_2_Sum_2.AutoSize = true;
            this.V_2_Sum_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.V_2_Sum_2.Location = new System.Drawing.Point(475, 274);
            this.V_2_Sum_2.Name = "V_2_Sum_2";
            this.V_2_Sum_2.Size = new System.Drawing.Size(151, 22);
            this.V_2_Sum_2.TabIndex = 55;
            this.V_2_Sum_2.Text = "0";
            this.V_2_Sum_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_2_Sum_1
            // 
            this.V_2_Sum_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_2_Sum_1.AutoSize = true;
            this.V_2_Sum_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.V_2_Sum_1.Location = new System.Drawing.Point(317, 274);
            this.V_2_Sum_1.Name = "V_2_Sum_1";
            this.V_2_Sum_1.Size = new System.Drawing.Size(151, 22);
            this.V_2_Sum_1.TabIndex = 54;
            this.V_2_Sum_1.Text = "0";
            this.V_2_Sum_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_2_a_1
            // 
            this.V_2_a_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_2_a_1.AutoSize = true;
            this.V_2_a_1.Cursor = System.Windows.Forms.Cursors.Default;
            this.V_2_a_1.Location = new System.Drawing.Point(317, 30);
            this.V_2_a_1.Name = "V_2_a_1";
            this.V_2_a_1.Size = new System.Drawing.Size(151, 23);
            this.V_2_a_1.TabIndex = 34;
            this.V_2_a_1.Text = "0";
            this.V_2_a_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_2_a_2
            // 
            this.V_2_a_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_2_a_2.AutoSize = true;
            this.V_2_a_2.Cursor = System.Windows.Forms.Cursors.Default;
            this.V_2_a_2.Location = new System.Drawing.Point(475, 30);
            this.V_2_a_2.Name = "V_2_a_2";
            this.V_2_a_2.Size = new System.Drawing.Size(151, 23);
            this.V_2_a_2.TabIndex = 35;
            this.V_2_a_2.Text = "0";
            this.V_2_a_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_2_b_2
            // 
            this.V_2_b_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_2_b_2.AutoSize = true;
            this.V_2_b_2.Location = new System.Drawing.Point(475, 124);
            this.V_2_b_2.Name = "V_2_b_2";
            this.V_2_b_2.Size = new System.Drawing.Size(151, 24);
            this.V_2_b_2.TabIndex = 43;
            this.V_2_b_2.Text = "0";
            this.V_2_b_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_2_b_1
            // 
            this.V_2_b_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_2_b_1.AutoSize = true;
            this.V_2_b_1.Cursor = System.Windows.Forms.Cursors.Default;
            this.V_2_b_1.Location = new System.Drawing.Point(317, 124);
            this.V_2_b_1.Name = "V_2_b_1";
            this.V_2_b_1.Size = new System.Drawing.Size(151, 24);
            this.V_2_b_1.TabIndex = 42;
            this.V_2_b_1.Text = "0";
            this.V_2_b_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_2_c_1
            // 
            this.V_2_c_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_2_c_1.AutoSize = true;
            this.V_2_c_1.Location = new System.Drawing.Point(317, 199);
            this.V_2_c_1.Name = "V_2_c_1";
            this.V_2_c_1.Size = new System.Drawing.Size(151, 24);
            this.V_2_c_1.TabIndex = 48;
            this.V_2_c_1.Text = "0";
            this.V_2_c_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_2_c_2
            // 
            this.V_2_c_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_2_c_2.AutoSize = true;
            this.V_2_c_2.Location = new System.Drawing.Point(475, 199);
            this.V_2_c_2.Name = "V_2_c_2";
            this.V_2_c_2.Size = new System.Drawing.Size(151, 24);
            this.V_2_c_2.TabIndex = 49;
            this.V_2_c_2.Text = "0";
            this.V_2_c_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_2_c_1_2
            // 
            this.V_2_c_1_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_2_c_1_2.AutoSize = true;
            this.V_2_c_1_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_2_c_1_2.Location = new System.Drawing.Point(317, 249);
            this.V_2_c_1_2.Name = "V_2_c_1_2";
            this.V_2_c_1_2.Size = new System.Drawing.Size(151, 24);
            this.V_2_c_1_2.TabIndex = 52;
            this.V_2_c_1_2.Text = "0";
            this.V_2_c_1_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_2_c_1_2.Click += new System.EventHandler(this.number_edit);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 312F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.label29, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label30, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label28, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label31, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label32, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label33, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label34, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.V_1_1_1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.V_1_2_1, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.V_1_2_2, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.V_1_1_2, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.V_1_1_3, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.V_1_2_3, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.V_1_sum_2, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.V_1_sum_1, 1, 4);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 658);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 21F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(630, 131);
            this.tableLayoutPanel1.TabIndex = 50;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(364, 6);
            this.label29.Margin = new System.Windows.Forms.Padding(50, 5, 3, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(59, 13);
            this.label29.TabIndex = 1;
            this.label29.Text = "Cuối năm\t";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(522, 6);
            this.label30.Margin = new System.Windows.Forms.Padding(50, 5, 3, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(57, 13);
            this.label30.TabIndex = 2;
            this.label30.Text = "Đầu năm";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(4, 6);
            this.label28.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(237, 13);
            this.label28.TabIndex = 0;
            this.label28.Text = "01. Tiền và các khoản tương đương tiền";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(4, 33);
            this.label31.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(57, 13);
            this.label31.TabIndex = 3;
            this.label31.Text = "- Tiền mặt:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(4, 60);
            this.label32.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(105, 13);
            this.label32.TabIndex = 3;
            this.label32.Text = "- Tiền gửi ngân hàng";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(4, 88);
            this.label33.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(101, 13);
            this.label33.TabIndex = 3;
            this.label33.Text = "- Tương đương tiền:";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(51, 113);
            this.label34.Margin = new System.Windows.Forms.Padding(50, 5, 3, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(36, 13);
            this.label34.TabIndex = 3;
            this.label34.Text = "Cộng";
            // 
            // V_1_1_1
            // 
            this.V_1_1_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_1_1_1.AutoSize = true;
            this.V_1_1_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_1_1_1.Location = new System.Drawing.Point(317, 28);
            this.V_1_1_1.Name = "V_1_1_1";
            this.V_1_1_1.Size = new System.Drawing.Size(151, 26);
            this.V_1_1_1.TabIndex = 26;
            this.V_1_1_1.Text = "0";
            this.V_1_1_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_1_1_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_1_2_1
            // 
            this.V_1_2_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_1_2_1.AutoSize = true;
            this.V_1_2_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_1_2_1.Location = new System.Drawing.Point(475, 28);
            this.V_1_2_1.Name = "V_1_2_1";
            this.V_1_2_1.Size = new System.Drawing.Size(151, 26);
            this.V_1_2_1.TabIndex = 27;
            this.V_1_2_1.Text = "0";
            this.V_1_2_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_1_2_1.Click += new System.EventHandler(this.number_edit);
            // 
            // V_1_2_2
            // 
            this.V_1_2_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_1_2_2.AutoSize = true;
            this.V_1_2_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_1_2_2.Location = new System.Drawing.Point(475, 55);
            this.V_1_2_2.Name = "V_1_2_2";
            this.V_1_2_2.Size = new System.Drawing.Size(151, 27);
            this.V_1_2_2.TabIndex = 29;
            this.V_1_2_2.Text = "0";
            this.V_1_2_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_1_2_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_1_1_2
            // 
            this.V_1_1_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_1_1_2.AutoSize = true;
            this.V_1_1_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_1_1_2.Location = new System.Drawing.Point(317, 55);
            this.V_1_1_2.Name = "V_1_1_2";
            this.V_1_1_2.Size = new System.Drawing.Size(151, 27);
            this.V_1_1_2.TabIndex = 28;
            this.V_1_1_2.Text = "0";
            this.V_1_1_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_1_1_2.Click += new System.EventHandler(this.number_edit);
            // 
            // V_1_1_3
            // 
            this.V_1_1_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_1_1_3.AutoSize = true;
            this.V_1_1_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_1_1_3.Location = new System.Drawing.Point(317, 83);
            this.V_1_1_3.Name = "V_1_1_3";
            this.V_1_1_3.Size = new System.Drawing.Size(151, 24);
            this.V_1_1_3.TabIndex = 30;
            this.V_1_1_3.Text = "0";
            this.V_1_1_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_1_1_3.Click += new System.EventHandler(this.number_edit);
            // 
            // V_1_2_3
            // 
            this.V_1_2_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_1_2_3.AutoSize = true;
            this.V_1_2_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.V_1_2_3.Location = new System.Drawing.Point(475, 83);
            this.V_1_2_3.Name = "V_1_2_3";
            this.V_1_2_3.Size = new System.Drawing.Size(151, 24);
            this.V_1_2_3.TabIndex = 31;
            this.V_1_2_3.Text = "0";
            this.V_1_2_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.V_1_2_3.Click += new System.EventHandler(this.number_edit);
            // 
            // V_1_sum_2
            // 
            this.V_1_sum_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_1_sum_2.AutoSize = true;
            this.V_1_sum_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.V_1_sum_2.Location = new System.Drawing.Point(475, 108);
            this.V_1_sum_2.Name = "V_1_sum_2";
            this.V_1_sum_2.Size = new System.Drawing.Size(151, 22);
            this.V_1_sum_2.TabIndex = 33;
            this.V_1_sum_2.Text = "0";
            this.V_1_sum_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // V_1_sum_1
            // 
            this.V_1_sum_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.V_1_sum_1.AutoSize = true;
            this.V_1_sum_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.V_1_sum_1.Location = new System.Drawing.Point(317, 108);
            this.V_1_sum_1.Name = "V_1_sum_1";
            this.V_1_sum_1.Size = new System.Drawing.Size(151, 22);
            this.V_1_sum_1.TabIndex = 32;
            this.V_1_sum_1.Text = "0";
            this.V_1_sum_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tableLayoutPanel15
            // 
            this.tableLayoutPanel15.ColumnCount = 1;
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.Controls.Add(this.label35, 0, 0);
            this.tableLayoutPanel15.Location = new System.Drawing.Point(3, 627);
            this.tableLayoutPanel15.Name = "tableLayoutPanel15";
            this.tableLayoutPanel15.RowCount = 1;
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.Size = new System.Drawing.Size(630, 25);
            this.tableLayoutPanel15.TabIndex = 61;
            // 
            // label35
            // 
            this.label35.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(0, 0);
            this.label35.Margin = new System.Windows.Forms.Padding(0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(630, 25);
            this.label35.TabIndex = 60;
            this.label35.Text = "Đơn vị tính: Đồng";
            this.label35.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(3, 611);
            this.label27.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(491, 13);
            this.label27.TabIndex = 49;
            this.label27.Text = "V. Thông tin bổ sung cho các khoản mục trình bày trong Báo cáo tình hình tài chín" +
    "h:";
            // 
            // Sec_IV_12
            // 
            this.Sec_IV_12.AutoSize = true;
            this.Sec_IV_12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_IV_12.Location = new System.Drawing.Point(3, 588);
            this.Sec_IV_12.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_IV_12.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_IV_12.Name = "Sec_IV_12";
            this.Sec_IV_12.Size = new System.Drawing.Size(146, 13);
            this.Sec_IV_12.TabIndex = 25;
            this.Sec_IV_12.Text = "- Nguyên tắc kế toán chi phí:";
            this.Sec_IV_12.Click += new System.EventHandler(this.label_edit);
            // 
            // Sec_IV_11
            // 
            this.Sec_IV_11.AutoSize = true;
            this.Sec_IV_11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_IV_11.Location = new System.Drawing.Point(3, 565);
            this.Sec_IV_11.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_IV_11.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_IV_11.Name = "Sec_IV_11";
            this.Sec_IV_11.Size = new System.Drawing.Size(247, 13);
            this.Sec_IV_11.TabIndex = 24;
            this.Sec_IV_11.Text = "- Nguyên tắc và phương pháp ghi nhận doanh thu:";
            this.Sec_IV_11.Click += new System.EventHandler(this.label_edit);
            // 
            // Sec_IV_10
            // 
            this.Sec_IV_10.AutoSize = true;
            this.Sec_IV_10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_IV_10.Location = new System.Drawing.Point(3, 542);
            this.Sec_IV_10.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_IV_10.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_IV_10.Name = "Sec_IV_10";
            this.Sec_IV_10.Size = new System.Drawing.Size(192, 13);
            this.Sec_IV_10.TabIndex = 23;
            this.Sec_IV_10.Text = "- Nguyên tắc ghi nhận vốn chủ sở hữu:";
            this.Sec_IV_10.Click += new System.EventHandler(this.label_edit);
            // 
            // Sec_IV_9
            // 
            this.Sec_IV_9.AutoSize = true;
            this.Sec_IV_9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_IV_9.Location = new System.Drawing.Point(3, 519);
            this.Sec_IV_9.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_IV_9.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_IV_9.Name = "Sec_IV_9";
            this.Sec_IV_9.Size = new System.Drawing.Size(294, 13);
            this.Sec_IV_9.TabIndex = 22;
            this.Sec_IV_9.Text = "- Nguyên tắc ghi nhận và vốn hoá các khoản chi phí đi vay:";
            this.Sec_IV_9.Click += new System.EventHandler(this.label_edit);
            // 
            // Sec_IV_8
            // 
            this.Sec_IV_8.AutoSize = true;
            this.Sec_IV_8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_IV_8.Location = new System.Drawing.Point(3, 496);
            this.Sec_IV_8.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_IV_8.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_IV_8.Name = "Sec_IV_8";
            this.Sec_IV_8.Size = new System.Drawing.Size(165, 13);
            this.Sec_IV_8.TabIndex = 21;
            this.Sec_IV_8.Text = "- Nguyên tắc kế toán Nợ phải trả:";
            this.Sec_IV_8.Click += new System.EventHandler(this.label_edit);
            // 
            // Sec_IV_7
            // 
            this.Sec_IV_7.AutoSize = true;
            this.Sec_IV_7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_IV_7.Location = new System.Drawing.Point(3, 473);
            this.Sec_IV_7.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_IV_7.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_IV_7.Name = "Sec_IV_7";
            this.Sec_IV_7.Size = new System.Drawing.Size(526, 13);
            this.Sec_IV_7.TabIndex = 20;
            this.Sec_IV_7.Text = "- Nguyên tắc ghi nhận và các phương pháp tính khấu hao TSCĐ, TSCĐ thuê tài chính," +
    " bất động sản đầu tư:";
            this.Sec_IV_7.Click += new System.EventHandler(this.label_edit);
            // 
            // Sec_IV_6
            // 
            this.Sec_IV_6.AutoSize = true;
            this.Sec_IV_6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_IV_6.Location = new System.Drawing.Point(3, 450);
            this.Sec_IV_6.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_IV_6.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_IV_6.Name = "Sec_IV_6";
            this.Sec_IV_6.Size = new System.Drawing.Size(181, 13);
            this.Sec_IV_6.TabIndex = 19;
            this.Sec_IV_6.Text = "- Nguyên tắc ghi nhận hàng tồn kho:";
            this.Sec_IV_6.Click += new System.EventHandler(this.label_edit);
            // 
            // Sec_IV_5
            // 
            this.Sec_IV_5.AutoSize = true;
            this.Sec_IV_5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_IV_5.Location = new System.Drawing.Point(3, 427);
            this.Sec_IV_5.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_IV_5.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_IV_5.Name = "Sec_IV_5";
            this.Sec_IV_5.Size = new System.Drawing.Size(166, 13);
            this.Sec_IV_5.TabIndex = 18;
            this.Sec_IV_5.Text = "- Nguyên tắc kế toán nợ phải thu:";
            this.Sec_IV_5.Click += new System.EventHandler(this.label_edit);
            // 
            // Sec_IV_4
            // 
            this.Sec_IV_4.AutoSize = true;
            this.Sec_IV_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_IV_4.Location = new System.Drawing.Point(3, 404);
            this.Sec_IV_4.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_IV_4.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_IV_4.Name = "Sec_IV_4";
            this.Sec_IV_4.Size = new System.Drawing.Size(243, 13);
            this.Sec_IV_4.TabIndex = 17;
            this.Sec_IV_4.Text = "- Nguyên tắc kế toán các khoản đầu tư tài chính:";
            this.Sec_IV_4.Click += new System.EventHandler(this.label_edit);
            // 
            // Sec_IV_3
            // 
            this.Sec_IV_3.AutoSize = true;
            this.Sec_IV_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_IV_3.Location = new System.Drawing.Point(3, 381);
            this.Sec_IV_3.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_IV_3.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_IV_3.Name = "Sec_IV_3";
            this.Sec_IV_3.Size = new System.Drawing.Size(342, 13);
            this.Sec_IV_3.TabIndex = 16;
            this.Sec_IV_3.Text = "- Nguyên tắc ghi nhận các khoản tiền và các khoản tương đương tiền:";
            this.Sec_IV_3.Click += new System.EventHandler(this.label_edit);
            // 
            // Sec_IV_2
            // 
            this.Sec_IV_2.AutoSize = true;
            this.Sec_IV_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_IV_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Sec_IV_2.Location = new System.Drawing.Point(3, 358);
            this.Sec_IV_2.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_IV_2.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_IV_2.Name = "Sec_IV_2";
            this.Sec_IV_2.Size = new System.Drawing.Size(344, 13);
            this.Sec_IV_2.TabIndex = 15;
            this.Sec_IV_2.Text = "- Nguyên tắc chuyển đổi BCTC lập bằng ngoại tệ sang Việt Nam Đồng:";
            this.Sec_IV_2.Click += new System.EventHandler(this.label_edit);
            // 
            // Sec_IV_1
            // 
            this.Sec_IV_1.AutoSize = true;
            this.Sec_IV_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_IV_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Sec_IV_1.Location = new System.Drawing.Point(3, 335);
            this.Sec_IV_1.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_IV_1.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_IV_1.Name = "Sec_IV_1";
            this.Sec_IV_1.Size = new System.Drawing.Size(194, 13);
            this.Sec_IV_1.TabIndex = 14;
            this.Sec_IV_1.Text = "- Tỷ giá hối đoái áp dụng trong kế toán:";
            this.Sec_IV_1.Click += new System.EventHandler(this.label_edit);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(3, 312);
            this.label14.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(214, 13);
            this.label14.TabIndex = 13;
            this.label14.Text = "IV. Các chính sách kế toán áp dụng";
            // 
            // Sec_III_1
            // 
            this.Sec_III_1.AutoSize = true;
            this.Sec_III_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_III_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Sec_III_1.Location = new System.Drawing.Point(3, 289);
            this.Sec_III_1.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_III_1.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_III_1.Name = "Sec_III_1";
            this.Sec_III_1.Size = new System.Drawing.Size(593, 13);
            this.Sec_III_1.TabIndex = 12;
            this.Sec_III_1.Text = "Báo cáo tài chính được lập và trình bày phù hợp, tuân thủ chuẩn mực kế toán và ch" +
    "ế độ kế toán doanh nghiệp vừa và nhỏ";
            this.Sec_III_1.Click += new System.EventHandler(this.label_edit);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(3, 266);
            this.label12.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(249, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "III. Chuẩn mực và chế độ kế toán áp dụng";
            // 
            // Sec_II_2
            // 
            this.Sec_II_2.AutoSize = true;
            this.Sec_II_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Sec_II_2.Location = new System.Drawing.Point(3, 243);
            this.Sec_II_2.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_II_2.Name = "Sec_II_2";
            this.Sec_II_2.Size = new System.Drawing.Size(270, 13);
            this.Sec_II_2.TabIndex = 10;
            this.Sec_II_2.Text = "2. Đơn vị tiền tệ sử dụng trong kế toán:  Việt Nam Đồng";
            // 
            // Sec_II_1
            // 
            this.Sec_II_1.AutoSize = true;
            this.Sec_II_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Sec_II_1.Location = new System.Drawing.Point(3, 220);
            this.Sec_II_1.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_II_1.Name = "Sec_II_1";
            this.Sec_II_1.Size = new System.Drawing.Size(96, 13);
            this.Sec_II_1.TabIndex = 9;
            this.Sec_II_1.Text = "1. Kỳ kế toán năm:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(3, 197);
            this.label9.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(298, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "II. Kỳ kế toán, Đơn vị tiền tệ sử dụng trong kế toán";
            // 
            // Sec_I_6
            // 
            this.Sec_I_6.AutoSize = true;
            this.Sec_I_6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_I_6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Sec_I_6.Location = new System.Drawing.Point(3, 148);
            this.Sec_I_6.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_I_6.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_I_6.Name = "Sec_I_6";
            this.Sec_I_6.Size = new System.Drawing.Size(628, 39);
            this.Sec_I_6.TabIndex = 7;
            this.Sec_I_6.Text = resources.GetString("Sec_I_6.Text");
            this.Sec_I_6.Click += new System.EventHandler(this.label_edit);
            // 
            // Sec_I_5
            // 
            this.Sec_I_5.AutoSize = true;
            this.Sec_I_5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_I_5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Sec_I_5.Location = new System.Drawing.Point(3, 125);
            this.Sec_I_5.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_I_5.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_I_5.Name = "Sec_I_5";
            this.Sec_I_5.Size = new System.Drawing.Size(485, 13);
            this.Sec_I_5.TabIndex = 6;
            this.Sec_I_5.Text = "5. Đặc điểm hoạt động của Doanh nghiệp trong năm tài chính có ảnh hưởng đến Báo c" +
    "áo tài chính:";
            this.Sec_I_5.Click += new System.EventHandler(this.label_edit);
            // 
            // Sec_I_4
            // 
            this.Sec_I_4.AutoSize = true;
            this.Sec_I_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_I_4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Sec_I_4.Location = new System.Drawing.Point(3, 102);
            this.Sec_I_4.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_I_4.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_I_4.Name = "Sec_I_4";
            this.Sec_I_4.Size = new System.Drawing.Size(220, 13);
            this.Sec_I_4.TabIndex = 5;
            this.Sec_I_4.Text = "4. Chu kỳ sản xuất kinh doanh thông thường:";
            this.Sec_I_4.Click += new System.EventHandler(this.label_edit);
            // 
            // Sec_I_3
            // 
            this.Sec_I_3.AutoSize = true;
            this.Sec_I_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_I_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Sec_I_3.Location = new System.Drawing.Point(3, 79);
            this.Sec_I_3.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_I_3.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_I_3.Name = "Sec_I_3";
            this.Sec_I_3.Size = new System.Drawing.Size(137, 13);
            this.Sec_I_3.TabIndex = 4;
            this.Sec_I_3.Text = "3. Ngành nghề kinh doanh:";
            this.Sec_I_3.Click += new System.EventHandler(this.label_edit);
            // 
            // Sec_I_2
            // 
            this.Sec_I_2.AutoSize = true;
            this.Sec_I_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_I_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Sec_I_2.Location = new System.Drawing.Point(3, 56);
            this.Sec_I_2.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_I_2.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_I_2.Name = "Sec_I_2";
            this.Sec_I_2.Size = new System.Drawing.Size(125, 13);
            this.Sec_I_2.TabIndex = 3;
            this.Sec_I_2.Text = "2. Lĩnh vực kinh doanh: ";
            this.Sec_I_2.Click += new System.EventHandler(this.label_edit);
            // 
            // Sec_I_1
            // 
            this.Sec_I_1.AutoSize = true;
            this.Sec_I_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sec_I_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Sec_I_1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Sec_I_1.Location = new System.Drawing.Point(3, 33);
            this.Sec_I_1.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.Sec_I_1.MaximumSize = new System.Drawing.Size(630, 0);
            this.Sec_I_1.Name = "Sec_I_1";
            this.Sec_I_1.Size = new System.Drawing.Size(127, 13);
            this.Sec_I_1.TabIndex = 2;
            this.Sec_I_1.Text = "1. Hình thức sở hữu vốn: ";
            this.Sec_I_1.Click += new System.EventHandler(this.label_edit);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 10);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(243, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "I. Đặc điểm hoạt động của Doanh nghiệp";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label8);
            this.panel2.Location = new System.Drawing.Point(13, 13);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(647, 24);
            this.panel2.TabIndex = 0;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(3, 3);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(255, 17);
            this.label8.TabIndex = 0;
            this.label8.Text = "Bản thuyết minh báo cáo tài chính";
            // 
            // FRB09DNNDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(894, 636);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FRB09DNNDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dữ liệu thuyết minh báo cáo tài chính";
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel11.PerformLayout();
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel12.PerformLayout();
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel13.PerformLayout();
            this.tableLayoutPanel14.ResumeLayout(false);
            this.tableLayoutPanel14.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel15.ResumeLayout(false);
            this.tableLayoutPanel15.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label Sec_V_5_1;
        private System.Windows.Forms.Label Sec_V_5_2;
        private System.Windows.Forms.Label Sec_V_5_3;
        private System.Windows.Forms.Label Sec_V_5_4;
        private System.Windows.Forms.Label Sec_V_5_5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.Label Sec_V_6_1;
        private System.Windows.Forms.Label Sec_V_6_2;
        private System.Windows.Forms.Label Sec_V_6_3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.Label label129;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.Label label132;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Label label133;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.Label label135;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.Label label139;
        private System.Windows.Forms.Label label140;
        private System.Windows.Forms.Label label141;
        private System.Windows.Forms.Label label142;
        private System.Windows.Forms.Label label143;
        private System.Windows.Forms.Label label144;
        private System.Windows.Forms.Label label145;
        private System.Windows.Forms.Label label146;
        private System.Windows.Forms.Label label147;
        private System.Windows.Forms.Label label148;
        private System.Windows.Forms.Label label149;
        private System.Windows.Forms.Label label150;
        private System.Windows.Forms.Label label151;
        private System.Windows.Forms.Label label152;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.Label label153;
        private System.Windows.Forms.Label label154;
        private System.Windows.Forms.Label label155;
        private System.Windows.Forms.Label label156;
        private System.Windows.Forms.Label label157;
        private System.Windows.Forms.Label label158;
        private System.Windows.Forms.Label label159;
        private System.Windows.Forms.Label label160;
        private System.Windows.Forms.Label label161;
        private System.Windows.Forms.Label label162;
        private System.Windows.Forms.Label label163;
        private System.Windows.Forms.Label label164;
        private System.Windows.Forms.Label label165;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.Label label166;
        private System.Windows.Forms.Label label167;
        private System.Windows.Forms.Label label168;
        private System.Windows.Forms.Label label169;
        private System.Windows.Forms.Label label170;
        private System.Windows.Forms.Label label171;
        private System.Windows.Forms.Label label172;
        private System.Windows.Forms.Label label173;
        private System.Windows.Forms.Label label174;
        private System.Windows.Forms.Label label175;
        private System.Windows.Forms.Label label176;
        private System.Windows.Forms.Label label177;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.Label label178;
        private System.Windows.Forms.Label label179;
        private System.Windows.Forms.Label label180;
        private System.Windows.Forms.Label label181;
        private System.Windows.Forms.Label label182;
        private System.Windows.Forms.Label label183;
        private System.Windows.Forms.Label label184;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.Label label185;
        private System.Windows.Forms.Label label186;
        private System.Windows.Forms.Label label187;
        private System.Windows.Forms.Label label188;
        private System.Windows.Forms.Label label189;
        private System.Windows.Forms.Label label190;
        private System.Windows.Forms.Label label191;
        private System.Windows.Forms.Label label192;
        private System.Windows.Forms.Label label193;
        private System.Windows.Forms.Label label194;
        private System.Windows.Forms.Label label195;
        private System.Windows.Forms.Label label196;
        private System.Windows.Forms.Label label197;
        private System.Windows.Forms.Label label198;
        private System.Windows.Forms.Label label199;
        private System.Windows.Forms.Label label200;
        private System.Windows.Forms.Label label201;
        private System.Windows.Forms.Label label202;
        private System.Windows.Forms.Label label203;
        private System.Windows.Forms.Label label204;
        private System.Windows.Forms.Label label205;
        private System.Windows.Forms.Label label206;
        private System.Windows.Forms.Label Sec_V_13_1;
        private System.Windows.Forms.Label label208;
        private System.Windows.Forms.Label Sec_V_14_1;
        private System.Windows.Forms.Label Sec_V_14_2;
        private System.Windows.Forms.Label Sec_V_14_3;
        private System.Windows.Forms.Label Sec_V_14_4;
        private System.Windows.Forms.Label Sec_V_14_5;
        private System.Windows.Forms.Label Sec_V_14_6;
        private System.Windows.Forms.Label Sec_V_14_7;
        private System.Windows.Forms.Label Sec_V_14_8;
        private System.Windows.Forms.Label label217;
        private System.Windows.Forms.Label Sec_V_15_1;
        private System.Windows.Forms.Label label219;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        private System.Windows.Forms.Label label221;
        private System.Windows.Forms.Label label222;
        private System.Windows.Forms.Label label223;
        private System.Windows.Forms.Label label224;
        private System.Windows.Forms.Label label225;
        private System.Windows.Forms.Label label226;
        private System.Windows.Forms.Label label227;
        private System.Windows.Forms.Label label228;
        private System.Windows.Forms.Label label229;
        private System.Windows.Forms.Label label230;
        private System.Windows.Forms.Label label231;
        private System.Windows.Forms.Label label232;
        private System.Windows.Forms.Label label233;
        private System.Windows.Forms.Label label234;
        private System.Windows.Forms.Label label235;
        private System.Windows.Forms.Label label236;
        private System.Windows.Forms.Label label237;
        private System.Windows.Forms.Label label238;
        private System.Windows.Forms.Label label239;
        private System.Windows.Forms.Label label240;
        private System.Windows.Forms.Label label241;
        private System.Windows.Forms.Label label242;
        private System.Windows.Forms.Label label243;
        private System.Windows.Forms.Label label244;
        private System.Windows.Forms.Label label245;
        private System.Windows.Forms.Label label246;
        private System.Windows.Forms.Label label247;
        private System.Windows.Forms.Label label248;
        private System.Windows.Forms.Label label249;
        private System.Windows.Forms.Label label250;
        private System.Windows.Forms.Label label251;
        private System.Windows.Forms.Label label252;
        private System.Windows.Forms.Label label253;
        private System.Windows.Forms.Label label254;
        private System.Windows.Forms.Label label255;
        private System.Windows.Forms.Label label256;
        private System.Windows.Forms.Label label257;
        private System.Windows.Forms.Label label258;
        private System.Windows.Forms.Label label259;
        private System.Windows.Forms.Label label260;
        private System.Windows.Forms.Label label261;
        private System.Windows.Forms.Label label262;
        private System.Windows.Forms.Label label263;
        private System.Windows.Forms.Label label264;
        private System.Windows.Forms.Label label265;
        private System.Windows.Forms.Label label266;
        private System.Windows.Forms.Label label267;
        private System.Windows.Forms.Label label268;
        private System.Windows.Forms.Label label269;
        private System.Windows.Forms.Label label270;
        private System.Windows.Forms.Label label271;
        private System.Windows.Forms.Label label272;
        private System.Windows.Forms.Label label273;
        private System.Windows.Forms.Label label274;
        private System.Windows.Forms.Label label275;
        private System.Windows.Forms.Label label276;
        private System.Windows.Forms.Label label277;
        private System.Windows.Forms.Label label278;
        private System.Windows.Forms.Label label279;
        private System.Windows.Forms.Label label280;
        private System.Windows.Forms.Label label281;
        private System.Windows.Forms.Label label282;
        private System.Windows.Forms.Label label283;
        private System.Windows.Forms.Label label292;
        private System.Windows.Forms.Label label293;
        private System.Windows.Forms.Label label294;
        private System.Windows.Forms.Label label295;
        private System.Windows.Forms.Label label296;
        private System.Windows.Forms.Label label297;
        private System.Windows.Forms.Label label298;
        private System.Windows.Forms.Label label299;
        private System.Windows.Forms.Label label300;
        private System.Windows.Forms.Label label301;
        private System.Windows.Forms.Label label302;
        private System.Windows.Forms.Label label303;
        private System.Windows.Forms.Label label304;
        private System.Windows.Forms.Label label305;
        private System.Windows.Forms.Label label306;
        private System.Windows.Forms.Label label307;
        private System.Windows.Forms.Label label284;
        private System.Windows.Forms.Label Sec_VII_1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label Sec_VIII_5;
        private System.Windows.Forms.Label Sec_VIII_4;
        private System.Windows.Forms.Label Sec_VIII_3;
        private System.Windows.Forms.Label Sec_VIII_2;
        private System.Windows.Forms.Label Sec_VIII_1;
        private System.Windows.Forms.Label label220;
        private System.Windows.Forms.Label label291;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label Sec_V_4_4;
        private System.Windows.Forms.Label Sec_V_4_3;
        private System.Windows.Forms.Label Sec_V_4_2;
        private System.Windows.Forms.Label Sec_V_4_1;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel15;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label Sec_IV_12;
        private System.Windows.Forms.Label Sec_IV_11;
        private System.Windows.Forms.Label Sec_IV_10;
        private System.Windows.Forms.Label Sec_IV_9;
        private System.Windows.Forms.Label Sec_IV_8;
        private System.Windows.Forms.Label Sec_IV_7;
        private System.Windows.Forms.Label Sec_IV_6;
        private System.Windows.Forms.Label Sec_IV_5;
        private System.Windows.Forms.Label Sec_IV_4;
        private System.Windows.Forms.Label Sec_IV_3;
        private System.Windows.Forms.Label Sec_IV_2;
        private System.Windows.Forms.Label Sec_IV_1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label Sec_III_1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label Sec_II_2;
        private System.Windows.Forms.Label Sec_II_1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label Sec_I_6;
        private System.Windows.Forms.Label Sec_I_5;
        private System.Windows.Forms.Label Sec_I_4;
        private System.Windows.Forms.Label Sec_I_3;
        private System.Windows.Forms.Label Sec_I_2;
        private System.Windows.Forms.Label Sec_I_1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label V_3_a_2_1;
        private System.Windows.Forms.Label V_3_a_1_1;
        private System.Windows.Forms.Label V_3_b_1_1;
        private System.Windows.Forms.Label V_3_b_2_1;
        private System.Windows.Forms.Label V_2_a_2_1;
        private System.Windows.Forms.Label V_2_a_1_1;
        private System.Windows.Forms.Label V_2_a_1_2;
        private System.Windows.Forms.Label V_2_a_2_2;
        private System.Windows.Forms.Label V_2_a_1_3;
        private System.Windows.Forms.Label V_2_a_2_3;
        private System.Windows.Forms.Label V_2_b_1_1;
        private System.Windows.Forms.Label V_2_b_2_1;
        private System.Windows.Forms.Label V_2_b_1_2;
        private System.Windows.Forms.Label V_2_b_2_2;
        private System.Windows.Forms.Label V_2_c_2_1;
        private System.Windows.Forms.Label V_2_c_1_1;
        private System.Windows.Forms.Label V_2_c_2_2;
        private System.Windows.Forms.Label V_2_Sum_2;
        private System.Windows.Forms.Label V_2_Sum_1;
        private System.Windows.Forms.Label V_2_a_1;
        private System.Windows.Forms.Label V_2_a_2;
        private System.Windows.Forms.Label V_2_b_2;
        private System.Windows.Forms.Label V_2_b_1;
        private System.Windows.Forms.Label V_2_c_1;
        private System.Windows.Forms.Label V_2_c_2;
        private System.Windows.Forms.Label V_2_c_1_2;
        private System.Windows.Forms.Label V_1_1_1;
        private System.Windows.Forms.Label V_1_2_1;
        private System.Windows.Forms.Label V_1_2_2;
        private System.Windows.Forms.Label V_1_1_2;
        private System.Windows.Forms.Label V_1_1_3;
        private System.Windows.Forms.Label V_1_2_3;
        private System.Windows.Forms.Label V_1_sum_2;
        private System.Windows.Forms.Label V_1_sum_1;
        private System.Windows.Forms.Label V_6_a_1_1;
        private System.Windows.Forms.Label V_6_a_2_1;
        private System.Windows.Forms.Label V_6_a_3_1;
        private System.Windows.Forms.Label V_6_a_4_1;
        private System.Windows.Forms.Label V_6_a_4_2;
        private System.Windows.Forms.Label V_6_a_4_3;
        private System.Windows.Forms.Label V_6_b_4_1;
        private System.Windows.Forms.Label V_6_b_4_2;
        private System.Windows.Forms.Label V_6_b_4_3;
        private System.Windows.Forms.Label V_6_b_4_4;
        private System.Windows.Forms.Label V_6_a_3_2;
        private System.Windows.Forms.Label V_6_a_3_3;
        private System.Windows.Forms.Label V_6_b_3_1;
        private System.Windows.Forms.Label V_6_b_3_2;
        private System.Windows.Forms.Label V_6_b_3_3;
        private System.Windows.Forms.Label V_6_b_3_4;
        private System.Windows.Forms.Label V_5_a_1_1;
        private System.Windows.Forms.Label V_5_a_2_1;
        private System.Windows.Forms.Label V_5_a_3_1;
        private System.Windows.Forms.Label V_5_a_4_1;
        private System.Windows.Forms.Label V_5_a_4_2;
        private System.Windows.Forms.Label V_5_a_4_3;
        private System.Windows.Forms.Label V_5_b_4_1;
        private System.Windows.Forms.Label V_5_b_4_2;
        private System.Windows.Forms.Label V_5_b_4_3;
        private System.Windows.Forms.Label V_5_c_4_1;
        private System.Windows.Forms.Label V_5_c_4_2;
        private System.Windows.Forms.Label V_5_c_4_3;
        private System.Windows.Forms.Label V_5_a_3_2;
        private System.Windows.Forms.Label V_5_a_3_3;
        private System.Windows.Forms.Label V_5_b_3_1;
        private System.Windows.Forms.Label V_5_b_3_2;
        private System.Windows.Forms.Label V_5_b_3_3;
        private System.Windows.Forms.Label V_5_c_3_1;
        private System.Windows.Forms.Label V_5_c_3_2;
        private System.Windows.Forms.Label V_5_c_3_3;
        private System.Windows.Forms.Label V_5_a_2_2;
        private System.Windows.Forms.Label V_5_a_2_3;
        private System.Windows.Forms.Label V_5_b_2_1;
        private System.Windows.Forms.Label V_5_b_2_2;
        private System.Windows.Forms.Label V_5_b_2_3;
        private System.Windows.Forms.Label V_5_c_2_1;
        private System.Windows.Forms.Label V_5_c_2_2;
        private System.Windows.Forms.Label V_5_c_2_3;
        private System.Windows.Forms.Label V_5_a_1_3;
        private System.Windows.Forms.Label V_5_a_1_2;
        private System.Windows.Forms.Label V_5_b_1_1;
        private System.Windows.Forms.Label V_5_b_1_2;
        private System.Windows.Forms.Label V_5_b_1_3;
        private System.Windows.Forms.Label V_5_c_1_1;
        private System.Windows.Forms.Label V_5_c_1_2;
        private System.Windows.Forms.Label V_5_c_1_3;
        private System.Windows.Forms.Label V_4_1_1;
        private System.Windows.Forms.Label V_4_2_1;
        private System.Windows.Forms.Label V_4_2_2;
        private System.Windows.Forms.Label V_4_1_2;
        private System.Windows.Forms.Label V_4_1_3;
        private System.Windows.Forms.Label V_4_2_3;
        private System.Windows.Forms.Label V_4_2_4;
        private System.Windows.Forms.Label V_4_1_4;
        private System.Windows.Forms.Label V_4_1_5;
        private System.Windows.Forms.Label V_4_2_5;
        private System.Windows.Forms.Label V_4_2_6;
        private System.Windows.Forms.Label V_4_1_6;
        private System.Windows.Forms.Label V_4_1_7;
        private System.Windows.Forms.Label V_4_2_7;
        private System.Windows.Forms.Label V_4_Sum_2;
        private System.Windows.Forms.Label V_4_Sum_1;
        private System.Windows.Forms.Label V_3_a_1;
        private System.Windows.Forms.Label V_3_a_2;
        private System.Windows.Forms.Label V_3_b_2;
        private System.Windows.Forms.Label V_3_b_1;
        private System.Windows.Forms.Label V_3_c_1;
        private System.Windows.Forms.Label V_3_c_2;
        private System.Windows.Forms.Label V_3_c_2_1;
        private System.Windows.Forms.Label V_3_c_1_1;
        private System.Windows.Forms.Label V_3_c_1_2;
        private System.Windows.Forms.Label V_3_c_2_2;
        private System.Windows.Forms.Label V_3_c_2_3;
        private System.Windows.Forms.Label V_3_c_1_3;
        private System.Windows.Forms.Label V_3_c_1_4;
        private System.Windows.Forms.Label V_3_c_2_4;
        private System.Windows.Forms.Label V_3_d_2;
        private System.Windows.Forms.Label V_3_d_1;
        private System.Windows.Forms.Label V_3_d_1_1;
        private System.Windows.Forms.Label V_3_d_2_1;
        private System.Windows.Forms.Label V_3_d_2_2;
        private System.Windows.Forms.Label V_3_d_1_2;
        private System.Windows.Forms.Label V_3_d_1_3;
        private System.Windows.Forms.Label V_3_d_2_3;
        private System.Windows.Forms.Label V_3_d_2_4;
        private System.Windows.Forms.Label V_3_d_1_4;
        private System.Windows.Forms.Label V_3_e_1;
        private System.Windows.Forms.Label V_3_e_2;
        private System.Windows.Forms.Label V_3_Sum_2;
        private System.Windows.Forms.Label V_3_Sum_1;
        private System.Windows.Forms.Label V_6_a_2_2;
        private System.Windows.Forms.Label V_6_a_2_3;
        private System.Windows.Forms.Label V_6_b_2_1;
        private System.Windows.Forms.Label V_6_b_2_2;
        private System.Windows.Forms.Label V_6_b_2_3;
        private System.Windows.Forms.Label V_6_b_2_4;
        private System.Windows.Forms.Label V_6_a_1_2;
        private System.Windows.Forms.Label V_6_a_1_3;
        private System.Windows.Forms.Label V_6_b_1_1;
        private System.Windows.Forms.Label V_6_b_1_2;
        private System.Windows.Forms.Label V_6_b_1_3;
        private System.Windows.Forms.Label V_6_b_1_4;
        private System.Windows.Forms.Label V_7_1_1;
        private System.Windows.Forms.Label V_7_1_2;
        private System.Windows.Forms.Label V_7_Sum_1;
        private System.Windows.Forms.Label V_7_2_1;
        private System.Windows.Forms.Label V_7_2_2;
        private System.Windows.Forms.Label V_7_2_3;
        private System.Windows.Forms.Label V_7_Sum_2;
        private System.Windows.Forms.Label V_7_1_3;
        private System.Windows.Forms.Label V_8_1_1;
        private System.Windows.Forms.Label V_8_1_2;
        private System.Windows.Forms.Label V_8_2_2;
        private System.Windows.Forms.Label V_8_2_1;
        private System.Windows.Forms.Label V_9_a_1;
        private System.Windows.Forms.Label V_9_a_2;
        private System.Windows.Forms.Label V_9_a_2_1;
        private System.Windows.Forms.Label V_9_a_1_1;
        private System.Windows.Forms.Label V_9_b_1;
        private System.Windows.Forms.Label V_9_b_1_1;
        private System.Windows.Forms.Label V_9_c_1;
        private System.Windows.Forms.Label V_9_c_1_1;
        private System.Windows.Forms.Label V_9_c_1_3;
        private System.Windows.Forms.Label V_9_c_1_2;
        private System.Windows.Forms.Label V_9_c_1_3_1;
        private System.Windows.Forms.Label V_9_c_1_3_2;
        private System.Windows.Forms.Label V_9_c_1_3_3;
        private System.Windows.Forms.Label V_9_d_1;
        private System.Windows.Forms.Label V_9_b_2;
        private System.Windows.Forms.Label V_9_b_2_1;
        private System.Windows.Forms.Label V_9_c_2;
        private System.Windows.Forms.Label V_9_c_2_1;
        private System.Windows.Forms.Label V_9_c_2_2;
        private System.Windows.Forms.Label V_9_c_2_3;
        private System.Windows.Forms.Label V_9_c_2_3_1;
        private System.Windows.Forms.Label V_9_c_2_3_2;
        private System.Windows.Forms.Label V_9_c_2_3_3;
        private System.Windows.Forms.Label V_9_d_2;
        private System.Windows.Forms.Label V_10_1_1;
        private System.Windows.Forms.Label V_10_1_2;
        private System.Windows.Forms.Label V_10_1_3;
        private System.Windows.Forms.Label V_10_1_5;
        private System.Windows.Forms.Label V_10_1_4;
        private System.Windows.Forms.Label V_10_1_6;
        private System.Windows.Forms.Label V_10_1_7;
        private System.Windows.Forms.Label V_10_1_8;
        private System.Windows.Forms.Label V_10_1_9;
        private System.Windows.Forms.Label V_10_Sum_1;
        private System.Windows.Forms.Label V_10_Sum_2;
        private System.Windows.Forms.Label V_10_2_9;
        private System.Windows.Forms.Label V_10_2_8;
        private System.Windows.Forms.Label V_10_2_7;
        private System.Windows.Forms.Label V_10_2_6;
        private System.Windows.Forms.Label V_10_2_5;
        private System.Windows.Forms.Label V_10_2_4;
        private System.Windows.Forms.Label V_10_2_3;
        private System.Windows.Forms.Label V_10_2_2;
        private System.Windows.Forms.Label V_10_2_1;
        private System.Windows.Forms.Label V_11_a_1;
        private System.Windows.Forms.Label V_11_a_2;
        private System.Windows.Forms.Label V_11_a_3;
        private System.Windows.Forms.Label V_11_a_4;
        private System.Windows.Forms.Label V_11_a_4_1;
        private System.Windows.Forms.Label V_11_b_4;
        private System.Windows.Forms.Label V_11_b_4_1;
        private System.Windows.Forms.Label V_11_c_4;
        private System.Windows.Forms.Label V_11_c_4_1;
        private System.Windows.Forms.Label V_11_Sum_4;
        private System.Windows.Forms.Label V_11_a_3_1;
        private System.Windows.Forms.Label V_11_b_3_1;
        private System.Windows.Forms.Label V_11_b_3;
        private System.Windows.Forms.Label V_11_c_3;
        private System.Windows.Forms.Label V_11_c_3_1;
        private System.Windows.Forms.Label V_11_Sum_3;
        private System.Windows.Forms.Label V_11_a_2_1;
        private System.Windows.Forms.Label V_11_b_2;
        private System.Windows.Forms.Label V_11_b_2_1;
        private System.Windows.Forms.Label V_11_c_2;
        private System.Windows.Forms.Label V_11_c_2_1;
        private System.Windows.Forms.Label V_11_Sum_2;
        private System.Windows.Forms.Label V_11_a_1_1;
        private System.Windows.Forms.Label V_11_b_1;
        private System.Windows.Forms.Label V_11_b_1_1;
        private System.Windows.Forms.Label V_11_c_1;
        private System.Windows.Forms.Label V_11_c_1_1;
        private System.Windows.Forms.Label V_11_Sum_1;
        private System.Windows.Forms.Label V_12_1_1;
        private System.Windows.Forms.Label V_12_2_1;
        private System.Windows.Forms.Label V_12_1_2;
        private System.Windows.Forms.Label V_12_1_3;
        private System.Windows.Forms.Label V_12_2_3;
        private System.Windows.Forms.Label V_12_Sum_2;
        private System.Windows.Forms.Label V_12_Sum_1;
        private System.Windows.Forms.Label V_13_1_1;
        private System.Windows.Forms.Label V_13_2_1;
        private System.Windows.Forms.Label V_13_3_1;
        private System.Windows.Forms.Label V_13_4_1;
        private System.Windows.Forms.Label V_13_5_1;
        private System.Windows.Forms.Label V_13_6_1;
        private System.Windows.Forms.Label V_13_7_1;
        private System.Windows.Forms.Label V_13_7_2;
        private System.Windows.Forms.Label V_13_7_3;
        private System.Windows.Forms.Label V_13_7_4;
        private System.Windows.Forms.Label V_13_6_2;
        private System.Windows.Forms.Label V_13_6_3;
        private System.Windows.Forms.Label V_13_6_4;
        private System.Windows.Forms.Label V_13_5_2;
        private System.Windows.Forms.Label V_13_5_3;
        private System.Windows.Forms.Label V_13_5_4;
        private System.Windows.Forms.Label V_13_4_2;
        private System.Windows.Forms.Label V_13_4_3;
        private System.Windows.Forms.Label V_13_4_4;
        private System.Windows.Forms.Label V_13_3_2;
        private System.Windows.Forms.Label V_13_3_3;
        private System.Windows.Forms.Label V_13_3_4;
        private System.Windows.Forms.Label V_13_2_2;
        private System.Windows.Forms.Label V_13_2_3;
        private System.Windows.Forms.Label V_13_2_4;
        private System.Windows.Forms.Label V_13_1_4;
        private System.Windows.Forms.Label V_13_1_3;
        private System.Windows.Forms.Label V_13_1_2;
        private System.Windows.Forms.Label VI_1_a_2_1;
        private System.Windows.Forms.Label VI_1_a_1_1;
        private System.Windows.Forms.Label VI_1_a_1_2;
        private System.Windows.Forms.Label VI_1_a_2_2;
        private System.Windows.Forms.Label VI_1_a_2_3;
        private System.Windows.Forms.Label VI_1_a_1_3;
        private System.Windows.Forms.Label VI_1_a_1_4;
        private System.Windows.Forms.Label VI_1_a_2_4;
        private System.Windows.Forms.Label VI_1_a_Sum_2;
        private System.Windows.Forms.Label VI_1_a_Sum_1;
        private System.Windows.Forms.Label VI_1_b_2;
        private System.Windows.Forms.Label VI_1_b_1;
        private System.Windows.Forms.Label VI_1_c_1;
        private System.Windows.Forms.Label VI_1_c_2;
        private System.Windows.Forms.Label VI_2_1_1;
        private System.Windows.Forms.Label VI_2_2_1;
        private System.Windows.Forms.Label VI_2_2_2;
        private System.Windows.Forms.Label VI_2_1_2;
        private System.Windows.Forms.Label VI_2_1_3;
        private System.Windows.Forms.Label VI_2_2_3;
        private System.Windows.Forms.Label VI_2_Sum_2;
        private System.Windows.Forms.Label VI_2_Sum_1;
        private System.Windows.Forms.Label VI_3_1_1;
        private System.Windows.Forms.Label VI_3_2_1;
        private System.Windows.Forms.Label VI_3_2_2;
        private System.Windows.Forms.Label VI_3_1_2;
        private System.Windows.Forms.Label VI_3_1_4;
        private System.Windows.Forms.Label VI_3_1_3;
        private System.Windows.Forms.Label VI_3_2_3;
        private System.Windows.Forms.Label VI_3_2_4;
        private System.Windows.Forms.Label VI_3_2_5;
        private System.Windows.Forms.Label VI_3_1_5;
        private System.Windows.Forms.Label VI_3_1_6;
        private System.Windows.Forms.Label VI_3_2_6;
        private System.Windows.Forms.Label VI_3_Sum_2;
        private System.Windows.Forms.Label VI_3_Sum_1;
        private System.Windows.Forms.Label VI_4_1_1;
        private System.Windows.Forms.Label VI_4_2_1;
        private System.Windows.Forms.Label VI_4_2_2;
        private System.Windows.Forms.Label VI_4_1_2;
        private System.Windows.Forms.Label VI_4_1_3;
        private System.Windows.Forms.Label VI_4_2_4;
        private System.Windows.Forms.Label VI_4_2_3;
        private System.Windows.Forms.Label VI_4_1_4;
        private System.Windows.Forms.Label VI_4_1_5;
        private System.Windows.Forms.Label VI_4_2_5;
        private System.Windows.Forms.Label VI_4_2_6;
        private System.Windows.Forms.Label VI_4_1_6;
        private System.Windows.Forms.Label VI_4_Sum_1;
        private System.Windows.Forms.Label VI_4_Sum_2;
        private System.Windows.Forms.Label VI_5_2_1;
        private System.Windows.Forms.Label VI_5_1_1;
        private System.Windows.Forms.Label VI_5_1_2;
        private System.Windows.Forms.Label VI_5_2_2;
        private System.Windows.Forms.Label VI_5_2_3;
        private System.Windows.Forms.Label VI_5_1_3;
        private System.Windows.Forms.Label VI_5_1_4;
        private System.Windows.Forms.Label VI_5_2_4;
        private System.Windows.Forms.Label VI_5_2_6;
        private System.Windows.Forms.Label VI_5_2_5;
        private System.Windows.Forms.Label VI_5_2_7;
        private System.Windows.Forms.Label VI_5_1_7;
        private System.Windows.Forms.Label VI_5_1_6;
        private System.Windows.Forms.Label VI_5_1_5;
        private System.Windows.Forms.Label VI_6_a_1;
        private System.Windows.Forms.Label VI_6_a_2;
        private System.Windows.Forms.Label VI_6_b_2;
        private System.Windows.Forms.Label VI_6_b_1;
        private System.Windows.Forms.Label VI_6_c_1;
        private System.Windows.Forms.Label VI_6_c_2;
        private System.Windows.Forms.Label VI_6_c_2_2;
        private System.Windows.Forms.Label VI_6_c_2_1;
        private System.Windows.Forms.Label VI_6_c_1_1;
        private System.Windows.Forms.Label VI_6_c_1_2;
        private System.Windows.Forms.Label VI_7_1_1;
        private System.Windows.Forms.Label VI_7_2_1;
        private System.Windows.Forms.Label VI_7_2_2;
        private System.Windows.Forms.Label VI_7_1_2;
        private System.Windows.Forms.Label VI_7_1_3;
        private System.Windows.Forms.Label VI_7_2_3;
        private System.Windows.Forms.Label VI_7_2_4;
        private System.Windows.Forms.Label VI_7_1_4;
        private System.Windows.Forms.Label VI_7_1_5;
        private System.Windows.Forms.Label VI_7_2_5;
        private System.Windows.Forms.Label VI_7_Sum_2;
        private System.Windows.Forms.Label VI_7_Sum_1;
        private System.Windows.Forms.Label VI_8_1_1;
        private System.Windows.Forms.Label VI_8_2_1;
        private System.Windows.Forms.Label VI_8_2_2;
        private System.Windows.Forms.Label VI_8_1_2;
        private System.Windows.Forms.Label VI_8_1_3;
        private System.Windows.Forms.Label VI_8_2_3;
        private System.Windows.Forms.Label VI_8_2_4;
        private System.Windows.Forms.Label VI_8_1_4;
        private System.Windows.Forms.Label VI_8_Sum_1;
        private System.Windows.Forms.Label VI_8_Sum_2;
        private System.Windows.Forms.Label VI_9_2_1;
        private System.Windows.Forms.Label VI_9_1_1;
        private System.Windows.Forms.Label VI_9_1_2;
        private System.Windows.Forms.Label VI_9_2_2;
        private System.Windows.Forms.Label VI_9_2_3;
        private System.Windows.Forms.Label VI_9_1_3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Label Sec_V_16_1;
        private System.Windows.Forms.Label V_12_2_2;
    }
}