﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm.FReport.Tools
{
    public partial class UEditBox : Form
    {
        public string Output;
        public decimal OutputNum;
        public UEditBox(string content, decimal number, bool isNumber)
        {
            InitializeComponent();
            Output = content;
            OutputNum = number;
            contentText.Text = Output;
            contentNumber.Value = number;
            if(isNumber)
            {
                contentText.Visible = false;
            }
            else
            {
                contentNumber.Visible = false;
            }
        }

        private void btnSave_click(object sender, EventArgs e)
        {
            Output = contentText.Text;
            OutputNum = (decimal)contentNumber.Value;
            this.Close();
        }

        private void btnCancel_click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
