﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;

namespace Accounting
{
    public partial class FRBankBalance : CustormForm
    {
        readonly List<AccountReport> _lstAccountBankReport = new List<AccountReport>();
        private string _subSystemCode;
        public FRBankBalance(string subSystemCode)
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();
            _subSystemCode = subSystemCode;
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\BankBalance.rst", path);
            ReportUtils.LstAccountBankReport.Clear();
            _lstAccountBankReport = ReportUtils.LstAccountBankReport;
            ugridAccount.SetDataBinding(_lstAccountBankReport.ToList(), "");
            ReportUtils.ProcessControls(this);
            btnExit.Click += btnExit_Click;
            btnOk.Click += btnOk_Click;
            WaitingFrm.StopWaiting();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }
            var generalLedgerService = IoC.Resolve<IGeneralLedgerService>();
            List<string> lstAccountNumber = _lstAccountBankReport.Where(c => c.Check).Select(c => c.AccountNumber).ToList();
            if (lstAccountNumber.Count == 0)
            {
                MSG.Warning(resSystem.Report_01);
                return;
            }
            List<BankBalance> lstBankBalances = new List<BankBalance>();
            if (lstAccountNumber.Count > 0)
            {
                lstBankBalances =
                    generalLedgerService.RBankBlance((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, lstAccountNumber, ((Currency)cbbCurrency.SelectedRow.ListObject).ID).ToList();
            }
            var rD = new BankBalanceDetail();
            rD.Period = ReportUtils.GetPeriod(DateTime.Now, DateTime.Now.AddMonths(30));
            var f = new ReportForm<BankBalance>(fileReportSlot1, _subSystemCode);
            f.AddDatasource("BankBalance", lstBankBalances, true);
            f.AddDatasource("Detail", rD);
            f.LoadReport();
            f.WindowState = FormWindowState.Maximized;
            f.Show();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
