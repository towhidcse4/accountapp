﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;

namespace Accounting
{
    public partial class FRS03bDNN : CustormForm
    {
        string _subSystemCode;
        public FRS03bDNN(string subSystemCode)
        {
            InitializeComponent();
            _subSystemCode = subSystemCode;
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\S03b-DNN.rst", path);
        }

        private void ultraButton1_Click(object sender, EventArgs e)
        {
            List<S03BDNN> data = new List<S03BDNN>();
            for (int i = 0; i < 200; i++)
            {
                data.Add(new S03BDNN(i));
            }
            var rD = new S03BDNNDetail();
            rD.Period = ReportUtils.GetPeriod(DateTime.Now.AddDays(-99), DateTime.Now);
            var f = new ReportForm<S03BDNN>(fileReportSlot1, _subSystemCode);
            f.AddDatasource("S03bDNN", data, true);
            f.AddDatasource("Detail", rD);
            f.LoadReport();
            f.WindowState = FormWindowState.Maximized;
            f.Show();
        }
    }
}
