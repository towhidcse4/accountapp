﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;

namespace Accounting
{
    public partial class FRF01DNN : CustormForm
    {
        string _subSystemCode;
        public FRF01DNN(string subSystemCode)
        {
            InitializeComponent();
            _subSystemCode = subSystemCode;
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\F01-DNN.rst", path);
        }

        private void ultraButton1_Click(object sender, EventArgs e)
        {

            List<F01DNN> data = new List<F01DNN>();
            for (int i = 0; i < 200; i++)
            {
                data.Add(new F01DNN(i));
            }
            var rD = new F01DNNDetail();
            rD.Period = ReportUtils.GetPeriod(DateTime.Now.AddDays(-1000), DateTime.Now);

            var f = new ReportForm<F01DNN>(fileReportSlot1, _subSystemCode);
            f.AddDatasource("F01DNN", data, true);
            f.AddDatasource("Detail", rD);
            f.LoadReport();
            f.WindowState = FormWindowState.Maximized;
            f.Show();
        }
    }
}
