﻿using System;
using System.Windows.Forms;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinEditors;
using PerpetuumSoft.Reporting.DOM;
using System.Xml.Serialization;
using System.IO;
using Accounting.Core.Domain;

namespace Accounting
{
    public partial class FReportSettingFont : DialogForm
    {
        //  private ReportForm reportForm;
        ReportStyle _reportStyle;
        private StyleCollection StyleCollection { get; set; }
        public FReportSettingFont()
        {
            InitializeComponent();
            // read xml file
            _reportStyle = new ReportStyle();
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            var file = new StreamReader(string.Format("{0}\\Frm\\FReport\\Template\\ReportStyle.xml", path));
            var reader = new XmlSerializer(typeof(ReportStyle));
            _reportStyle = (ReportStyle)reader.Deserialize(file);
            file.Close();
            LoadView();
            numAlignBottom.ValueChanged += numAlignBottom_ValueChanged;
            numAlignLeft.ValueChanged += numAlignLeft_ValueChanged;
            numAlignRight.ValueChanged += numAlignRight_ValueChanged;
            numAlignTop.ValueChanged += numAlignTop_ValueChanged;

            //btnCancel.Click += btnCancel_Click;
            //btnDefault.Click += btnDefault_Click;
            //btnDetailNumber.Click += btnDetailNumber_Click;
            //btnDetailSum.Click += btnDetailSum_Click;
            //btnDetailText.Click += btnDetailText_Click;
            //btnOk.Click += btnOk_Click;
            //btnRowTitle.Click += btnRowTitle_Click;
            //btnSignerDate.Click += btnSignerDate_Click;
            //btnSignerName.Click += btnSignerName_Click;
            //btnSignerSubTitle.Click += btnSignerSubTitle_Click;
            //btnSignerTitle.Click += btnSignerTitle_Click;
            //btnSubTitle.Click += btnSubTitle_Click;
            //btnSummaryFooterNumber.Click += btnSummaryFooterNumber_Click;
            //btnSummaryFooterText.Click += btnSummaryFooterText_Click;
            //btnTitle.Click += btnTitle_Click;
        }
        private void LoadView()
        {
            StyleCollection = _reportStyle.styleCollection;
            SetTextBox(txtTitle, StyleCollection["ReportTitle"]);
            SetTextBox(txtRowTitle, StyleCollection["DetailHeader"]);
            SetTextBox(txtSubTitle, StyleCollection["ReportSubTitle"]);
            SetTextBox(txtDetailText, StyleCollection["DetailText"]);
            SetTextBox(txtDetailNumber, StyleCollection["DetailNumber"]);
            SetTextBox(txtSummaryDetailNumber, StyleCollection["SummaryDetailNumber"]);
            SetTextBox(txtSummaryFooterText, StyleCollection["SummaryFooterText"]);
            SetTextBox(txtSummaryFooterNumber, StyleCollection["SummaryFooterNumber"]);
            SetTextBox(txtSignerTitle, StyleCollection["SignerTitle"]);
            SetTextBox(txtSignerSubTitle, StyleCollection["SignerSubTitle"]);
            SetTextBox(txtSignerDate, StyleCollection["SignerDate"]);
            SetTextBox(txtSignerName, StyleCollection["SignerName"]);
            numAlignTop.Value = (decimal)_reportStyle.AlignTop;
            numAlignBottom.Value = (decimal)_reportStyle.AlignBottom;
            numAlignLeft.Value = (decimal)_reportStyle.AlignLeft;
            numAlignRight.Value = (decimal)_reportStyle.AlignRight;
            chkShowProductInfo.Checked = _reportStyle.ShowProductInfo;
        }
        private void SetTextBox(UltraTextEditor textBox, Style style)
        {
            if (style == null) return;
            textBox.Font = style.Font.GetFont();
            textBox.Text = style.Font.FamilyName + style.Font.Size;
        }
        private void btnOk_Click(object sender, EventArgs e)
        {
            //XmlSerializer writer = new XmlSerializer(typeof(StyleCollection));
            //StreamWriter file = new StreamWriter("./StyleCollection.xml");
            //writer.Serialize(file, styleCollection);
            //file.Close();
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            var file = new StreamWriter(string.Format("{0}\\Frm\\FReport\\Template\\ReportStyle.xml", path));
            var writer = new XmlSerializer(typeof(ReportStyle));
            writer.Serialize(file, _reportStyle);
            file.Close();
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void SelectFont(string styleName, UltraButton btn, UltraTextEditor txt)
        {
            SelectFontDialog.Font = StyleCollection[styleName].Font.GetFont();
            if (SelectFontDialog.ShowDialog(this) == DialogResult.OK)
            {
                var style = StyleCollection[styleName];
                if (style == null)
                {
                    style = new Style { Font = new PerpetuumSoft.Framework.Drawing.FontDescriptor(SelectFontDialog.Font) };
                    StyleCollection.Add(style);
                }
                else
                    StyleCollection[styleName].Font = new PerpetuumSoft.Framework.Drawing.FontDescriptor(SelectFontDialog.Font);
                SetTextBox(txt, StyleCollection[styleName]);
            }
        }


        private void btnTitle_Click(object sender, EventArgs e)
        {
            SelectFont("ReportTitle", btnTitle, txtTitle);
        }

        private void btnSubTitle_Click(object sender, EventArgs e)
        {
            SelectFont("ReportSubTitle", btnSubTitle, txtSubTitle);
        }

        private void btnRowTitle_Click(object sender, EventArgs e)
        {
            SelectFont("DetailHeader", btnRowTitle, txtRowTitle);
        }

        private void btnDetailText_Click(object sender, EventArgs e)
        {
            SelectFont("DetailText", btnDetailText, txtDetailText);
        }

        private void btnDetailNumber_Click(object sender, EventArgs e)
        {
            SelectFont("DetailNumber", btnDetailNumber, txtDetailNumber);
        }

        private void btnDetailSum_Click(object sender, EventArgs e)
        {
            SelectFont("SummaryDetailNumber", btnDetailSum, txtSummaryDetailNumber);
        }

        private void btnSummaryFooterText_Click(object sender, EventArgs e)
        {
            SelectFont("SummaryFooterText", btnSummaryFooterText, txtSummaryFooterText);
        }

        private void btnSummaryFooterNumber_Click(object sender, EventArgs e)
        {
            SelectFont("SummaryFooterNumber", btnSummaryFooterNumber, txtSummaryFooterNumber);
        }

        private void btnSignerDate_Click(object sender, EventArgs e)
        {
            SelectFont("SignerDate", btnSignerDate, txtSignerDate);
        }

        private void btnSignerTitle_Click(object sender, EventArgs e)
        {
            SelectFont("SignerTitle", btnSignerTitle, txtSignerTitle);
        }

        private void btnSignerSubTitle_Click(object sender, EventArgs e)
        {
            SelectFont("SignerSubTitle", btnSignerSubTitle, txtSignerSubTitle);
        }

        private void btnSignerName_Click(object sender, EventArgs e)
        {
            SelectFont("SignerName", btnSignerName, txtSignerName);
        }

        private void btnDefault_Click(object sender, EventArgs e)
        {
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            var file = new StreamReader(string.Format("{0}\\Frm\\FReport\\Template\\ReportStyleDefault.xml", path));
            var reader = new XmlSerializer(typeof(ReportStyle));
            _reportStyle = (ReportStyle)reader.Deserialize(file);
            file.Close();
            LoadView();
        }


        private void numAlignTop_ValueChanged(object sender, EventArgs e)
        {
            _reportStyle.AlignTop = (double)numAlignTop.Value;
        }

        private void numAlignBottom_ValueChanged(object sender, EventArgs e)
        {
            _reportStyle.AlignBottom = (double)numAlignBottom.Value;
        }

        private void numAlignLeft_ValueChanged(object sender, EventArgs e)
        {
            _reportStyle.AlignLeft = (double)numAlignLeft.Value;
        }

        private void numAlignRight_ValueChanged(object sender, EventArgs e)
        {
            _reportStyle.AlignRight = (double)numAlignRight.Value;
        }

        private void chkShowProductInfo_CheckedChanged(object sender, EventArgs e)
        {
            _reportStyle.ShowProductInfo = chkShowProductInfo.Checked;
        }
    }
}
