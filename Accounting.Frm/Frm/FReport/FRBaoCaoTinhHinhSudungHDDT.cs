﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Accounting.Core;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win.UltraWinGrid;

namespace Accounting.Frm.FReport
{
    public partial class FRBaoCaoTinhHinhSudungHDDT : Form
    {
        private readonly List<Item> _lstItems = Utils.ObjConstValue.SelectTimes;
        private readonly ISystemOptionService _ISystemOptionService;
        private readonly ISupplierServiceService _ISupplierServiceService;
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        public FRBaoCaoTinhHinhSudungHDDT()
        {
            _ISystemOptionService = IoC.Resolve<ISystemOptionService>();
            _ISupplierServiceService = IoC.Resolve<ISupplierServiceService>();
            InitializeComponent();
            InitializeGUI();
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.MonthOrPrecious != null && dateTimeCacheHistory.Year != null)
                {
                    if(dateTimeCacheHistory.MonthOrPrecious!=0)
                    {
                        ultraOptionSet1.Value = dateTimeCacheHistory.MonthOrPrecious - 1;
                        if(dateTimeCacheHistory.MonthOrPrecious==1)
                        {
                            cbbMonth.SelectedRow = cbbMonth.Rows[dateTimeCacheHistory.Month ?? 0];
                            txtYear.Value = dateTimeCacheHistory.Year;
                        }
                        else
                        {
                            cbbMonth.SelectedRow = cbbMonth.Rows[dateTimeCacheHistory.Precious ?? 0];
                            txtYear.Value = dateTimeCacheHistory.Year;
                        }
                        
                    }
                    
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.MonthOrPrecious = ultraOptionSet1.Value.ToInt() + 1;
                if(ultraOptionSet1.Value.ToInt()==0)
                {
                    dateTimeCacheHistory.Month = cbbMonth.SelectedRow.Index;
                }
                else
                {
                    dateTimeCacheHistory.Precious = cbbMonth.SelectedRow.Index;
                }
                dateTimeCacheHistory.Year = txtYear.Value.ToInt();
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }

        }
        private void InitializeGUI()
        {
            txtYear.Value = DateTime.Now.Year;
            cbbMonth.ConfigComboSelectTime(_lstItems.Where(n => int.Parse(n.Value) < 21 && int.Parse(n.Value) > 8).ToList(), "Name");
            cbbMonth.DropDownStyle = UltraComboStyle.DropDownList;
            foreach (var item in cbbMonth.Rows)
            {
                if (int.Parse((item.ListObject as Item).Value) == DateTime.Now.Month + 8) cbbMonth.SelectedRow = item;
            }
        }
        private void cbbMonth_ValueChanged(object sender, EventArgs e)
        {
            if (cbbMonth.SelectedRow != null)
            {
                var model = cbbMonth.SelectedRow.ListObject as Item;
                DateTime dtBegin;
                DateTime dtEnd;
                Utils.GetDateBeginDateEnd((int)txtYear.Value,DateTime.Now, model, out dtBegin, out dtEnd);
                //dteDateFrom.Value = new DateTime((int)txtYear.Value, dtBegin.Month, dtBegin.Day);
                //dteDateTo.Value = new DateTime((int)txtYear.Value, dtEnd.Month, dtEnd.Day);
            }
        }

        private void ultraOptionSet1_ValueChanged(object sender, EventArgs e)
        {
            if (ultraOptionSet1.CheckedIndex == 0)
            {
                ultraLabel1.Text = "Tháng";
                cbbMonth.ConfigComboSelectTime(_lstItems.Where(n => int.Parse(n.Value) < 21 && int.Parse(n.Value) > 8).ToList(), "Name");
                foreach (var item in cbbMonth.Rows)
                {
                    if (int.Parse((item.ListObject as Item).Value) == DateTime.Now.Month + 8) cbbMonth.SelectedRow = item;
                }
            }
            else
            {
                ultraLabel1.Text = "Quý";
                cbbMonth.ConfigComboSelectTime(_lstItems.Where(n => int.Parse(n.Value) < 25 && int.Parse(n.Value) > 20).ToList(), "Name");
                float now = float.Parse(DateTime.Now.Month.ToString()) / 3;
                foreach (var item in cbbMonth.Rows)
                {
                    if (now <= 1)
                    {
                        if (int.Parse((item.ListObject as Item).Value) == 21) cbbMonth.SelectedRow = item;
                    }
                    if (now <= 2 && now > 1)
                    {
                        if (int.Parse((item.ListObject as Item).Value) == 22) cbbMonth.SelectedRow = item;
                    }
                    if (now <= 3 && now > 2)
                    {
                        if (int.Parse((item.ListObject as Item).Value) == 23) cbbMonth.SelectedRow = item;
                    }
                    if (now <= 4 && now > 3)
                    {
                        if (int.Parse((item.ListObject as Item).Value) == 24) cbbMonth.SelectedRow = item;
                    }

                }
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "HDDT").Data == "1")
            {


                DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
                if (dateTimeCacheHistory != null)
                {
                    dateTimeCacheHistory.MonthOrPrecious = ultraOptionSet1.Value.ToInt() + 1;
                    if (ultraOptionSet1.Value.ToInt() == 0)
                    {
                        dateTimeCacheHistory.Month = cbbMonth.SelectedRow.Index;
                    }
                    else
                    {
                        dateTimeCacheHistory.Precious = cbbMonth.SelectedRow.Index;
                    }
                    dateTimeCacheHistory.Year = txtYear.Value.ToInt();
                    _IDateTimeCacheHistoryService.BeginTran();
                    _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                    _IDateTimeCacheHistoryService.CommitTran();
                }
                else
                {
                    dateTimeCacheHistory = new DateTimeCacheHistory();
                    dateTimeCacheHistory.ID = Guid.NewGuid();
                    dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                    
                    dateTimeCacheHistory.SubSystemCode = this.Name;
                    dateTimeCacheHistory.MonthOrPrecious = ultraOptionSet1.Value.ToInt() + 1;
                    if (ultraOptionSet1.Value.ToInt() == 0)
                    {
                        dateTimeCacheHistory.Month = cbbMonth.SelectedRow.Index;
                    }
                    else
                    {
                        dateTimeCacheHistory.Precious = cbbMonth.SelectedRow.Index;
                    }
                    dateTimeCacheHistory.Year = txtYear.Value.ToInt();
                    _IDateTimeCacheHistoryService.BeginTran();
                    _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                    _IDateTimeCacheHistoryService.CommitTran();
                }
                SystemOption systemOption = Utils.ListSystemOption.FirstOrDefault(n => n.Code == "EI_IDNhaCungCapDichVu");
                if (!string.IsNullOrEmpty(systemOption.Data))
                {
                    SupplierService supplierService = _ISupplierServiceService.Getbykey(Guid.Parse(systemOption.Data));
                    if (supplierService.SupplierServiceCode == "SDS")
                    {
                        RestApiService client = new RestApiService(Utils.GetPathAccess(), Utils.GetUserName(), Core.ITripleDes.TripleDesDefaultDecryption(Utils.GetPassWords()));

                        try
                        {
                            var request = new Request();
                            var model = cbbMonth.SelectedRow.ListObject as Item;
                            DateTime dtBegin;
                            DateTime dtEnd;
                            Utils.GetDateBeginDateEnd((int)txtYear.Value,DateTime.Now, model, out dtBegin, out dtEnd);
                            request.FromDate = new DateTime((int)txtYear.Value, dtBegin.Month, dtBegin.Day).ToString("dd/MM/yyyy");
                            request.ToDate = new DateTime((int)txtYear.Value, dtEnd.Month, dtEnd.Day).ToString("dd/MM/yyyy");
                            request.Option = 0;
                            WaitingFrm.StartWaiting();
                            var response = client.Post(supplierService.ApiService.FirstOrDefault(n => n.ApiName == "BaoCaoTinhHinhSDHD").ApiPath, request);
                            WaitingFrm.StopWaiting();
                            Regex tagRegex = new Regex(@"<\s*([^ >]+)[^>]*>.*?<\s*/\s*\1\s*>");
                            if (tagRegex.IsMatch(response.Message))
                            {
                                ViewRP ViewRP = new ViewRP(response.Message, request, 0);
                                ViewRP.Text = "TÌNH HÌNH SỬ DỤNG HÓA ĐƠN";
                                ViewRP.View();
                            }
                            else
                            {
                                MSG.Error("Lỗi xem báo cáo");
                            }

                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }
                    }
                }
            }
            else
                MSG.Warning("Không thể xem báo cáo khi chưa tích hợp hóa đơn điện tử");
        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
