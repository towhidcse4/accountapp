﻿using System;
using System.Collections.Generic;
using System.Linq;
using FX.Core;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using Accounting.Core.IService;

namespace Accounting
{
    public partial class FRS10DNN : CustormForm
    {
        readonly List<FixedAssetReport> _lstFixedAssetReport = (Utils.IFixedAssetService.GetAll().Select(c => new FixedAssetReport()
                                                                        {
                                                                            ID = c.ID,
                                                                            FixedAssetCode = c.FixedAssetCode,
                                                                            FixedAssetName = c.FixedAssetName,
                                                                            FixedAssetCategoryID = c.FixedAssetCategoryID
                                                                        })).ToList();
        string _subSystemCode;
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        public FRS10DNN(string subSystemCode)
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();
            _subSystemCode = subSystemCode;
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\S10-DNN.rst", path);
            ugridFixedAsset.SetDataBinding(_lstFixedAssetReport.OrderBy(n => n.FixedAssetCode).ToList(), "");
            ReportUtils.ProcessControls(this);
            btnExit.Click += btnExit_Click;
            btnOk.Click += btnOk_Click;
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dtBeginDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }

            WaitingFrm.StopWaiting();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }
            var fixAssetLedgerSrv = IoC.Resolve<IFixedAssetLedgerService>();
            List<S10DNN> data = new List<S10DNN>();
            List<Guid> lstGuidFixAsset = new List<Guid>();
            if (_lstFixedAssetReport.Count > 0)
            {
                foreach (var fixAsset in _lstFixedAssetReport.Where(p => p.Check))
                {
                    lstGuidFixAsset.Add(fixAsset.ID);
                }
            }
            if (lstGuidFixAsset.Count == 0)
            {
                MSG.Warning("Bạn chưa chọn tài sản để lập báo cáo");
                return;
            }
            if (dtEndDate.Value == null || dtBeginDate.Value == null)
            {
                MSG.Warning("Bạn chưa chọn ngày để lập báo cáo"); 
                return;
            }
            DateTime from = new DateTime(((DateTime)dtBeginDate.Value).Year, ((DateTime)dtBeginDate.Value).Month, ((DateTime)dtBeginDate.Value).Day);
            DateTime to = new DateTime(((DateTime)dtEndDate.Value).Year, ((DateTime)dtEndDate.Value).Month, ((DateTime)dtEndDate.Value).Day);
            if (from > to) 
            {
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            data = fixAssetLedgerSrv.RS10DNN((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, lstGuidFixAsset);
            if (data == null || data.Count == 0)
            {
                MSG.Warning("Không có dữ liệu để xuất báo cáo");
                return;
            }
            var rd = new S10DNNDetail();
            rd.Period = ReportUtils.GetPeriod((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value);
            var f = new ReportForm<S10DNN>(fileReportSlot1, _subSystemCode);
            f.AddDatasource("S10DNN", data, true);
            f.AddDatasource("detail", rd);
            f.LoadReport();
            f.WindowState = FormWindowState.Maximized;
            ReportUtils.ClearCheckBox(ugridFixedAsset);
            ugridFixedAsset.SetDataBinding(_lstFixedAssetReport.ToList(), "");
            f.Show();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            ReportUtils.ClearCheckBox(ugridFixedAsset);
            Close();
        }
    }
}
