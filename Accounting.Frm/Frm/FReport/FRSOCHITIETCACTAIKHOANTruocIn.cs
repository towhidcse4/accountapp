﻿using Accounting.Core.DAO;
using Accounting.Core.Domain.obj.Report;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRSOCHITIETCACTAIKHOANTruocIn : Form
    {
        List<GL_GetBookDetailPaymentByAccountNumber> data1 = new List<GL_GetBookDetailPaymentByAccountNumber>();
        DateTime fromDate;
        DateTime toDate;
        int check=0;
        string listaccount = "";
        string path = System.IO.Path.GetDirectoryName(
               System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
       
        public FRSOCHITIETCACTAIKHOANTruocIn(List<GL_GetBookDetailPaymentByAccountNumber> data, DateTime fromDate1, DateTime toDate1, int check1, string listaccount1)
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();
            fromDate = fromDate1;
            toDate = toDate1;
            check = check1;
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\SoChiTietCacTaiKhoan.rst", path);
            listaccount = listaccount1;
            //TinhTongDuNoDuCo(data);
            ReportProcedureSDS sp = new ReportProcedureSDS();
            data1 = sp.GetSoChiTietCacTaiKhoan1("", "", fromDate, toDate, listaccount, check);
            GanTongDuNoDuCo(data1);
            uGridDuLieu.DataSource = data1;
            //data1 = data;
            Utils.ConfigGrid(uGridDuLieu, ConstDatabase.GL_GetBookDetailPaymentByAccountNumber1_TableName);
            
            Utils.AddSumColumn(uGridDuLieu, "RefDate", false);
            //uGridDuLieu.Rows.FilterRow.Hidden = false;
            Utils.AddSumColumn(uGridDuLieu, "RefNo", false);
            Utils.AddSumColumn(uGridDuLieu, "JournalMemo", false);
            //uGridDuLieu.DisplayLayout.Bands[0].Columns["JournalMemo"].AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            uGridDuLieu.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            Utils.AddSumColumn(uGridDuLieu, "AccountNumber", false);
            Utils.AddSumColumn(uGridDuLieu, "CorrespondingAccountNumber", false);
            Utils.AddSumColumn(uGridDuLieu, "DebitAmount", false);
            Utils.AddSumColumn(uGridDuLieu, "CreditAmount", false);
            Utils.AddSumColumn(uGridDuLieu, "ClosingDebitAmount", false);
            Utils.AddSumColumn(uGridDuLieu, "ClosingCreditAmount", false);

            uGridDuLieu.DisplayLayout.Bands[0].Summaries[1].DisplayFormat = " ";
            uGridDuLieu.DisplayLayout.Bands[0].Summaries[2].DisplayFormat = " ";
            uGridDuLieu.DisplayLayout.Bands[0].Summaries[3].DisplayFormat = " ";
            uGridDuLieu.DisplayLayout.Bands[0].Summaries[4].DisplayFormat = " ";
            uGridDuLieu.DisplayLayout.Bands[0].Summaries[5].DisplayFormat = " ";
            uGridDuLieu.DisplayLayout.Bands[0].Summaries[6].DisplayFormat = " ";
            uGridDuLieu.DisplayLayout.Bands[0].Summaries[7].DisplayFormat = " ";
            uGridDuLieu.DisplayLayout.Bands[0].Summaries[8].DisplayFormat = " ";
            uGridDuLieu.DisplayLayout.Bands[0].Summaries[9].DisplayFormat = " ";

            uGridDuLieu.DisplayLayout.Bands[0].Columns["PostedDate"].Width = 60;
            uGridDuLieu.DisplayLayout.Bands[0].Columns["RefDate"].Width =60;
            uGridDuLieu.DisplayLayout.Bands[0].Columns["RefNo"].Width = 60;
            uGridDuLieu.DisplayLayout.Bands[0].Columns["DebitAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGridDuLieu.DisplayLayout.Bands[0].Columns["CreditAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGridDuLieu.DisplayLayout.Bands[0].Columns["ClosingDebitAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGridDuLieu.DisplayLayout.Bands[0].Columns["ClosingCreditAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGridDuLieu.DisplayLayout.Bands[0].Columns["DebitAmount"].CellAppearance.TextHAlign = HAlign.Right;
            uGridDuLieu.DisplayLayout.Bands[0].Columns["CreditAmount"].CellAppearance.TextHAlign = HAlign.Right;
            uGridDuLieu.DisplayLayout.Bands[0].Columns["ClosingDebitAmount"].CellAppearance.TextHAlign = HAlign.Right;
            uGridDuLieu.DisplayLayout.Bands[0].Columns["ClosingCreditAmount"].CellAppearance.TextHAlign = HAlign.Right;
            uGridDuLieu.DisplayLayout.Bands[0].Columns["AccountNumber"].CellAppearance.TextHAlign = HAlign.Center;
            uGridDuLieu.DisplayLayout.Bands[0].Columns["CorrespondingAccountNumber"].CellAppearance.TextHAlign = HAlign.Center;
            InDamGridVaGanGiaTriCongNhom(uGridDuLieu, data1);
            UltraGridBand parentBand = this.uGridDuLieu.DisplayLayout.Bands[0];
            parentBand.RowLayoutStyle = RowLayoutStyle.GroupLayout;
            UltraGridGroup parentBandGroup1 = new UltraGridGroup();
            if (!parentBand.Groups.Exists("ParentBandGroup1"))
            {
                parentBandGroup1 = parentBand.Groups.Add("ParentBandGroup1", "SỔ CHI TIẾTCÁC TÀI KHOẢN");
            }
            else
            {
                parentBandGroup1 = parentBand.Groups["ParentBandGroup1"];
            }
            parentBandGroup1.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroup1.Header.Appearance.TextHAlign = HAlign.Center;
            string tieude = string.Format("Từ ngày {0}/{1}/{2} đến ngày {3}/{4}/{5}", fromDate.Date.Day, fromDate.Date.Month, fromDate.Date.Year, toDate.Date.Day, toDate.Date.Month, toDate.Date.Year);

            UltraGridGroup parentBandGroup = new UltraGridGroup();
            if (!parentBand.Groups.Exists("ParentBandGroup"))
            {
                parentBandGroup = parentBand.Groups.Add("ParentBandGroup", tieude);
            }
            else
            {
                parentBandGroup = parentBand.Groups["ParentBandGroup"];
                parentBandGroup.Header.Caption = tieude;
            }
            parentBandGroup.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroup.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroup.RowLayoutGroupInfo.ParentGroup = parentBandGroup1;
            parentBand.Columns[0].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[1].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[2].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[3].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[4].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[5].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[6].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[7].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[8].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[9].RowLayoutColumnInfo.ParentGroup = parentBandGroup;

            parentBandGroup.Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBandGroup.Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBandGroup.Header.Appearance.ForeColor = Color.Black;
            parentBandGroup.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroup.Header.Appearance.BorderColor = Color.Black;

            parentBandGroup1.Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBandGroup1.Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBandGroup1.Header.Appearance.ForeColor = Color.Black;
            parentBandGroup1.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroup1.Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[0].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[0].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[0].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[0].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[0].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[1].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[1].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[1].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[1].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[1].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[2].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[2].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[2].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[2].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[2].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[3].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[3].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[3].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[3].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[3].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[4].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[4].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[4].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[4].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[4].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[5].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[5].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[5].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[5].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[5].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[6].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[6].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[6].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[6].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[6].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[7].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[7].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[7].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[7].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[7].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[8].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[8].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[8].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[8].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[8].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[9].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[9].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[9].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[9].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[9].Header.Appearance.BorderColor = Color.Black;

            this.WindowState = FormWindowState.Maximized;
            if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "IsMinimized").Data == "1")
            {
                this.WindowState = FormWindowState.Normal;


                this.Width = Screen.PrimaryScreen.WorkingArea.Width - 200;
                this.Height = Screen.PrimaryScreen.WorkingArea.Height - 50;
                this.Location = new System.Drawing.Point(((Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2), ((Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2));

            }
            if (Utils.isDemo)
            {
                ultraButton2.Visible = false;
            }
            SetDoSoAm(uGridDuLieu);
            WaitingFrm.StopWaiting();
        }
        private void SetDoSoAm(UltraGrid ultraGrid)
        {
            foreach (var item in ultraGrid.Rows)
            {

                if ((decimal)item.Cells["DebitAmount"].Value < 0)
                    item.Cells["DebitAmount"].Appearance.ForeColor = Color.Red;


                if ((decimal)item.Cells["CreditAmount"].Value < 0)
                    item.Cells["CreditAmount"].Appearance.ForeColor = Color.Red;
                if ((decimal)item.Cells["ClosingDebitAmount"].Value < 0)
                    item.Cells["ClosingDebitAmount"].Appearance.ForeColor = Color.Red;


                if ((decimal)item.Cells["ClosingCreditAmount"].Value < 0)
                    item.Cells["ClosingCreditAmount"].Appearance.ForeColor = Color.Red;

            }
        }
        private void TinhTongDuNoDuCo(List<GL_GetBookDetailPaymentByAccountNumber> data)
        {
            decimal TongDuNo = 0;
            decimal TongDuCo = 0;
            decimal PhatSinhNo = 0;
            decimal PhatSinhCo = 0;
            string strTongDuNo = "";
            string strTongDuCo = "";
            foreach (var item in data)
            {
                if (!item.Sum_DuCo.ToString().IsNullOrEmpty())
                {

                    TongDuCo += item.Sum_DuCo;
                }
                if (!item.Sum_DuNo.ToString().IsNullOrEmpty())
                {

                    TongDuNo += item.Sum_DuNo;

                }
                if (!item.DebitAmount.ToString().IsNullOrEmpty())
                {

                    PhatSinhNo += item.DebitAmount;
                }
                if (!item.CreditAmount.ToString().IsNullOrEmpty())
                {

                    PhatSinhCo += item.CreditAmount;

                }
            }
            PhatSinhCo = PhatSinhCo / 2;
            PhatSinhNo = PhatSinhNo / 2;
            GL_GetBookDetailPaymentByAccountNumber pO_PayDetailNCC = new GL_GetBookDetailPaymentByAccountNumber();
            pO_PayDetailNCC.JournalMemo = "Tổng cộng";
            pO_PayDetailNCC.DebitAmount =  PhatSinhNo;
            pO_PayDetailNCC.CreditAmount =  PhatSinhCo;
            pO_PayDetailNCC.ClosingCreditAmount =  TongDuCo;
            pO_PayDetailNCC.ClosingDebitAmount =  TongDuNo;
            data.Add(pO_PayDetailNCC);
            strTongDuCo = Accounting.ReportUtils.FormatTiente(TongDuCo, "VND");
            strTongDuNo = Accounting.ReportUtils.FormatTiente(TongDuNo, "VND");


        }
        private void GanTongDuNoDuCo(List<GL_GetBookDetailPaymentByAccountNumber> list)
        {
            foreach(var item in list)
            {
                if (item.AccountGroupKind=="0")
                {
                    if(item.ClosingCreditAmount>0)
                    { item.ClosingDebitAmount = -item.ClosingCreditAmount;
                        item.ClosingCreditAmount = 0;
                    }
                }
                if (item.AccountGroupKind == "1")
                {
                    if (item.ClosingDebitAmount > 0)
                    {
                        item.ClosingCreditAmount = -item.ClosingDebitAmount;
                        item.ClosingDebitAmount = 0;
                    }
                }
                if (item.AccountGroupKind == "3")
                {
                    if (item.AccountNumber[0].ToInt() ==  5)
                    {
                        if (item.ClosingDebitAmount > 0)
                        {
                            item.ClosingCreditAmount = -item.ClosingDebitAmount;
                            item.ClosingDebitAmount = 0;
                        }
                    }
                    if (item.AccountNumber[0].ToInt() == 7)
                    {
                        if (item.ClosingDebitAmount > 0)
                        {
                            item.ClosingCreditAmount = -item.ClosingDebitAmount;
                            item.ClosingDebitAmount = 0;
                        }
                    }
                    if (item.AccountNumber[0].ToInt() == 6)
                    {
                        if (item.ClosingCreditAmount > 0)
                        {
                            item.ClosingDebitAmount = -item.ClosingCreditAmount;
                            item.ClosingCreditAmount = 0;
                        }
                    }
                    if (item.AccountNumber[0].ToInt() == 8)
                    {
                        if (item.ClosingCreditAmount > 0)
                        {
                            item.ClosingDebitAmount = -item.ClosingCreditAmount;
                            item.ClosingCreditAmount = 0;
                        }
                    }
                }

            }
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].JournalMemo == "Số dư cuối kỳ")
                {
                   
                        list[i].ClosingCreditAmount = list[i - 2].ClosingCreditAmount;
                        list[i].ClosingDebitAmount = list[i - 2].ClosingDebitAmount;
                   
                }
            }
        }
        private void InDamGridVaGanGiaTriCongNhom(UltraGrid data, List<GL_GetBookDetailPaymentByAccountNumber> list)
        {
            UltraGridRow ultraGridRow = data.DisplayLayout.Rows[0];
            for (int i = 0; i < list.Count; i++)
            {
                if (data.DisplayLayout.Rows[i].Cells[3].Value != null)
                {
                    if (data.DisplayLayout.Rows[i].Cells[3].Value.ToString() == "Cộng")
                    {
                        data.DisplayLayout.Rows[i].Cells[3].Appearance.FontData.Bold = DefaultableBoolean.True;
                        data.DisplayLayout.Rows[i].Cells[4].Appearance.FontData.Bold = DefaultableBoolean.True;
                        data.DisplayLayout.Rows[i].Cells[7].Appearance.FontData.Bold = DefaultableBoolean.True;
                        data.DisplayLayout.Rows[i].Cells[6].Appearance.FontData.Bold = DefaultableBoolean.True;
                        data.DisplayLayout.Rows[i].Cells[8].Appearance.FontData.Bold = DefaultableBoolean.True;
                        data.DisplayLayout.Rows[i].Cells[9].Appearance.FontData.Bold = DefaultableBoolean.True;
                       


                    }
                    if (data.DisplayLayout.Rows[i].Cells[3].Value.ToString() == "Số dư đầu kỳ")
                    {
                        data.DisplayLayout.Rows[i].Cells[3].Appearance.FontData.Bold = DefaultableBoolean.True;
                        data.DisplayLayout.Rows[i].Cells[4].Appearance.FontData.Bold = DefaultableBoolean.True;
                        data.DisplayLayout.Rows[i].Cells[7].Appearance.FontData.Bold = DefaultableBoolean.True;
                        data.DisplayLayout.Rows[i].Cells[6].Appearance.FontData.Bold = DefaultableBoolean.True;
                        data.DisplayLayout.Rows[i].Cells[8].Appearance.FontData.Bold = DefaultableBoolean.True;
                        data.DisplayLayout.Rows[i].Cells[9].Appearance.FontData.Bold = DefaultableBoolean.True;

                    }
                    //if (data.DisplayLayout.Rows[i].Cells[3].Value.ToString() == "Tổng cộng")
                    //{
                    //    data.DisplayLayout.Rows[i].Cells[3].Appearance.FontData.Bold = DefaultableBoolean.True;
                    //    data.DisplayLayout.Rows[i].Cells[11].Appearance.FontData.Bold = DefaultableBoolean.True;
                    //    data.DisplayLayout.Rows[i].Cells[12].Appearance.FontData.Bold = DefaultableBoolean.True;
                    //    data.DisplayLayout.Rows[i].Cells[13].Appearance.FontData.Bold = DefaultableBoolean.True;
                    //    data.DisplayLayout.Rows[i].Cells[14].Appearance.FontData.Bold = DefaultableBoolean.True;


                    //}
                    if (data.DisplayLayout.Rows[i].Cells[3].Value.ToString() == "Số dư cuối kỳ")
                    {
                        data.DisplayLayout.Rows[i].Cells[3].Appearance.FontData.Bold = DefaultableBoolean.True;
                        data.DisplayLayout.Rows[i].Cells[4].Appearance.FontData.Bold = DefaultableBoolean.True;
                        data.DisplayLayout.Rows[i].Cells[7].Appearance.FontData.Bold = DefaultableBoolean.True;
                        data.DisplayLayout.Rows[i].Cells[6].Appearance.FontData.Bold = DefaultableBoolean.True;
                        data.DisplayLayout.Rows[i].Cells[8].Appearance.FontData.Bold = DefaultableBoolean.True;
                        data.DisplayLayout.Rows[i].Cells[9].Appearance.FontData.Bold = DefaultableBoolean.True;


                    }
                }

            }
        }

        private void uGridDuLieu_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (e.Row.ListObject.HasProperty("RefType"))
            {
                if (!e.Row.ListObject.GetProperty("RefType").IsNullOrEmpty())
                {
                    int typeid = e.Row.ListObject.GetProperty("RefType").ToInt();

                    if (e.Row.ListObject.HasProperty("RefID"))
                    {
                        if (!e.Row.ListObject.GetProperty("RefID").IsNullOrEmpty())
                        {
                            Guid id = (Guid)e.Row.ListObject.GetProperty("RefID");
                            var f = Utils.ViewVoucherSelected1(id, typeid);
                            //this.Close();
                            //if (f.IsDisposed)
                            //{
                            //    new FRSOCHITIETCACTAIKHOANTruocIn(data1,fromDate, toDate,check,listaccount).Show();
                            //}
                        }
                    }
                }

            }
        }

        private void ultraButton2_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog
            {
                Title = "Chọn nơi sao lưu ",
                Filter = "File (*.xls)|*.xls",
                InitialDirectory = "@C:\\",
                FileName = "SoChiTietCacTaiKhoan.xls"
            };
            if (save.ShowDialog() == DialogResult.OK)
            {
                string duongdan = save.FileName;
                try
                {
                    ultraGridExcelExporter1.Export(uGridDuLieu, duongdan);
                    if (MSG.Question("Kết xuất thành công, Bạn có muốn mở file vừa kết xuất không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(save.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
                catch
                {
                    MSG.Warning("Lỗi khi kết xuất !");
                }
            }
        }

        private void ultraButton3_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ultraButton1_Click(object sender, EventArgs e)
        {
            if (fromDate == null || toDate == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }

            if (fromDate > toDate)
            {
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }

            //int check1 = check == false ? 0 : 1;
            //string listaccount = "";
            //string loaitien = cbbCurrency.Value.ToString();//comment by cuongpv
            //if (!_lstAccountReport.Any(c => c.Check))
            //{
            //    MSG.Warning(resSystem.Report_01);
            //    return;
            //}
            //var dsaccount = _lstAccountReport.Where(t => t.Check).Select(c => c.AccountNumber.ToString());
            //listaccount = string.Join(",", dsaccount.ToArray());
            ReportProcedureSDS sp = new ReportProcedureSDS();
            List<GL_GetBookDetailPaymentByAccountNumber> data = sp.GetSoChiTietCacTaiKhoan("", "", fromDate, toDate, listaccount, check);
            if (data.Count == 0)
                MSG.Warning("Không có dữ liệu");
            else
            {
                foreach (var item in data)
                {
                    if (item.AccountGroupKind == "0")
                    {
                        if (item.ClosingCreditAmount > 0)
                        {
                            item.ClosingDebitAmount = -item.ClosingCreditAmount;
                            item.ClosingCreditAmount = 0;
                        }
                    }
                    if (item.AccountGroupKind == "1")
                    {
                        if (item.ClosingDebitAmount > 0)
                        {
                            item.ClosingCreditAmount = -item.ClosingDebitAmount;
                            item.ClosingDebitAmount = 0;
                        }
                    }
                    if (item.AccountGroupKind == "3")
                    {
                        if (item.AccountNumber[0] == 5)
                        {
                            if (item.ClosingDebitAmount > 0)
                            {
                                item.ClosingCreditAmount = -item.ClosingDebitAmount;
                                item.ClosingDebitAmount = 0;
                            }
                        }
                        if (item.AccountNumber[0] == 7)
                        {
                            if (item.ClosingDebitAmount > 0)
                            {
                                item.ClosingCreditAmount = -item.ClosingDebitAmount;
                                item.ClosingDebitAmount = 0;
                            }
                        }
                        if (item.AccountNumber[0] == 6)
                        {
                            if (item.ClosingCreditAmount > 0)
                            {
                                item.ClosingDebitAmount = -item.ClosingCreditAmount;
                                item.ClosingCreditAmount = 0;
                            }
                        }
                        if (item.AccountNumber[0] == 8)
                        {
                            if (item.ClosingCreditAmount > 0)
                            {
                                item.ClosingDebitAmount = -item.ClosingCreditAmount;
                                item.ClosingCreditAmount = 0;
                            }
                        }
                    }

                }
                var list_accountnumber = data.Select(t => t.AccountNumber).Distinct().ToList();
                foreach (var item in list_accountnumber)
                {
                    var list1 = data.Where(t => t.AccountNumber == item).ToList();
                    list1[list1.Count() - 1].Sum_DuNo = list1[list1.Count() - 1].ClosingDebitAmount;
                    list1[list1.Count() - 1].Sum_DuCo = list1[list1.Count() - 1].ClosingCreditAmount;
                }
                var rD = new GL_GetBookDetailPaymentByAccountNumber_Detail();
                rD.Period = ReportUtils.GetPeriod(fromDate, toDate);
                //if (dsaccount.Count() > 1) comment by cuongpv
                //    rD.LoaiTien = "Loại tiền: " + loaitien;
                //else
                //    rD.LoaiTien = "Loại tiền: " + loaitien + "; Tài khoản: " + listaccount;
                string _subSystemCode = "";
                var f = new ReportForm<GL_GetBookDetailPaymentByAccountNumber>(fileReportSlot1, _subSystemCode);
                f.AddDatasource("DBSOCHITIETCACTK", data);
                f.AddDatasource("Detail", rD);
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
        }

        private void ultraGridExcelExporter1_InitializeColumn(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.InitializeColumnEventArgs e)

        {
            e.Column.Width = 200;
            if (!string.IsNullOrEmpty(e.Column.Format))
            {
                e.ExcelFormatStr = e.Column.Format;
                e.Column.Width = 200;
            }
        }

        private void uGridDuLieu_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            UltraGridCell cell = e.Row.Cells[0];
            cell.Row.Height = 25;
        }

        private void ultraGridExcelExporter1_BeginExport(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.BeginExportEventArgs e)
        {
            e.Rows[e.CurrentRowIndex].Cells[e.CurrentColumnIndex].Column.Width=200;
            foreach (var row in uGridDuLieu.Rows)
            {
                foreach (var column in uGridDuLieu.DisplayLayout.Bands[0].Columns)
                {
                    e.Rows[row.Index].Cells[column].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                    e.Rows[row.Index].Cells[column].Appearance.BorderColor = Color.Black;
                    e.Rows[row.Index].Cells[column].Column.Width = 200;
                }
            }
            e.Rows.Band.Summaries[0].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[0].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[1].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[1].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[2].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[2].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[3].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[3].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[4].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[4].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[5].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[5].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[6].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[6].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[7].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[7].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[8].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[8].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[9].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[9].Appearance.BorderColor = Color.Black;

        }
    }
}
