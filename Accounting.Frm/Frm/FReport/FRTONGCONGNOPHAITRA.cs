﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Accounting.Core.DAO;
using Accounting.Core.Domain.obj.Report;

namespace Accounting.Frm.FReport
{
    public partial class FRTONGCONGNOPHAITRA : Form
    {
        readonly List<AccountReport> _lstAccountReceivablesReport = ReportUtils.LstAccountReceivablesReportCongNo.Where(t=>t.AccountNumber=="331" || t.AccountNumber=="3388").ToList();
        private readonly List<AccountingObjectReport> _lstnew = ReportUtils.LstProvider;
        List<AccountingObjectReport> _lstAccountingObjectReport = new List<AccountingObjectReport>();
        string _subSystemCode;
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        public FRTONGCONGNOPHAITRA()
        {
            InitializeComponent();
            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\TongHopCongNoPhaiTra.rst", path);
            _lstAccountingObjectReport = _lstnew;
            ugridAccountingObject.SetDataBinding(_lstAccountingObjectReport.ToList(), "");
            
            cbTaiKhoanCNPT.DataSource = _lstAccountReceivablesReport;
            cbTaiKhoanCNPT.DisplayMember = "AccountNumber";            
            Utils.ConfigGrid(cbTaiKhoanCNPT, ConstDatabase.Account_TableName);
            cbTaiKhoanCNPT.SelectedText = "331";
            ReportUtils.ProcessControls(this);
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dtBeginDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }

            if((DateTime)dtBeginDate.Value > (DateTime)dtEndDate.Value)
            {
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            string listncc = "";
            if (!_lstAccountingObjectReport.Any(c => c.Check))
            {
                MSG.Warning(resSystem.Report_02);
                return;
            }
            var dsnhacc = _lstAccountingObjectReport.Where(t => t.Check).Select(c => c.ID.ToString());
            string acount = cbTaiKhoanCNPT.Value != null ? cbTaiKhoanCNPT.Value.ToString() : string.Empty;
            if ( String.IsNullOrEmpty(acount))
            {
                acount = null;
            }
            listncc = string.Join(",", dsnhacc.ToArray());
            ReportProcedureSDS sp = new ReportProcedureSDS();
            List<PO_PayableDetail> data = sp.GetTongHopCongNoPhaiTraNCC((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, cbbCurrency.Value.ToString(), acount, listncc);
            if (data.Count == 0)
                MSG.Warning("Không có dữ liệu");
            else
            {
                var rD = new PO_PayableDetail_SP();
                rD.Period = ReportUtils.GetPeriod((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value);
                var f = new FRTONGCONGNOPHAITRATruocIn((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, cbbCurrency.Value.ToString(), acount, listncc, rD.Period);
                f.Show();
                //var f = new ReportForm<PO_PayableDetail>(fileReportSlot1, _subSystemCode);
                //f.AddDatasource("DBTONGCONGNOPT", data, true);
                //f.AddDatasource("Detail", rD);
                //f.LoadReport();
                //f.WindowState = FormWindowState.Maximized;
                //f.Show();
            }

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            foreach(var item in ugridAccountingObject.Rows)
            {
                item.Cells["check"].Value = false;
            }
            Close();
        }

        private void reportManager1_GetReportParameter(object sender, PerpetuumSoft.Reporting.Components.GetReportParameterEventArgs e)
        {
            string typeCurrency = cbbCurrency.Value != null ? cbbCurrency.Value.ToString() : string.Empty;
            e.Parameters["CurrencyID"].Value = typeCurrency;
        }
    }
}
