﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting
{
    public partial class MenuTest : Form
    {
        public MenuTest()
        {
            InitializeComponent();
        }

        private void ultraButton1_Click(object sender, EventArgs e)
        {
            new FRS05aDNN().Show();
        }

        private void ultraButton2_Click(object sender, EventArgs e)
        {
            new FRPayableSummary().Show();
        }

        private void ultraButton3_Click(object sender, EventArgs e)
        {
            new FRReceiptableSummary().Show();
        }

        private void ultraButton4_Click(object sender, EventArgs e)
        {
            new FRPurchaseDetailSupplerInventoryItem().Show();
        }

        private void ultraButton5_Click(object sender, EventArgs e)
        {
            new FRS17DNN().Show();
        }

        private void ultraButton6_Click(object sender, EventArgs e)
        {
            new FRS08DNN().Show();
        }

        private void ultraButton7_Click(object sender, EventArgs e)
        {
            new FRS09DNN().Show();
        }

        private void ultraButton8_Click(object sender, EventArgs e)
        {
            new FRS12SDNN().Show();
        }

        private void ultraButton9_Click(object sender, EventArgs e)
        {
            new FRContractStatusPurchase().Show();
        }

        private void ultraButton14_Click(object sender, EventArgs e)
        {
            new FRContractStatusSale().Show();
        }

        private void ultraButton13_Click(object sender, EventArgs e)
        {
            new FRDividendPayable().Show();
        }

        private void ultraButton11_Click(object sender, EventArgs e)
        {
            new FRBUAllocationAndUse().Show();
        }

        private void ultraButton10_Click(object sender, EventArgs e)
        {
            new FRS03a1DNN().Show();
        }

        private void ultraButton12_Click(object sender, EventArgs e)
        {
            new FRS03a2DNN().Show();
        }

        private void ultraButton18_Click(object sender, EventArgs e)
        {
            new FRS03a3DNN().Show();
        }

        private void ultraButton16_Click(object sender, EventArgs e)
        {
            new FRS03a4DNN().Show();
        }

        private void ultraButton22_Click(object sender, EventArgs e)
        {
            new FRBankCompare().Show();
        }

        private void ultraButton20_Click(object sender, EventArgs e)
        {
            new FRBankBalance().Show();
        }

        private void ultraButton19_Click(object sender, EventArgs e)
        {
            new FRBankForeignCurrency().Show();
        }

        private void ultraButton21_Click(object sender, EventArgs e)
        {
            new FRS07DNN().Show();
        }

        private void ultraButton31_Click(object sender, EventArgs e)
        {
            new FRS10DNN().Show();
        }

        private void ultraButton30_Click(object sender, EventArgs e)
        {
            new FRS11DNN().Show();
        }

        private void ultraButton28_Click(object sender, EventArgs e)
        {
            new FRS03aDNN().Show();
        }

        private void ultraButton27_Click(object sender, EventArgs e)
        {
            new FRS03bDNN().Show();
        }

        private void ultraButton15_Click(object sender, EventArgs e)
        {
            new FRB01DNN().Show();
        }

        private void ultraButton17_Click(object sender, EventArgs e)
        {
            new FRB02DNN().Show();
        }

        private void ultraButton26_Click(object sender, EventArgs e)
        {
            new FRB03DNN().Show();
        }

        private void ultraButton24_Click(object sender, EventArgs e)
        {
            new FRF01DNN().Show();
        }
    }
}
