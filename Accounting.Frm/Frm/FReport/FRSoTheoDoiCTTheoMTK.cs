﻿using Accounting.Core.DAO;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.Report;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinTree;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRSoTheoDoiCTTheoMTK : Form
    {
        private readonly List<AccountTongHopCP> _lstnew2 = ReportUtils.LstAccount;
        private readonly List<StatisticsCodeCT> _lstnew = ReportUtils.LstStatisticsCode;
        private List<AccountTongHopCP> _lstAccountReport = new List<AccountTongHopCP>();
        private List<StatisticsCodeCT> dsStatisticsCode = new List<StatisticsCodeCT>();
        private IStatisticsCodeService _IStatisticsCodeService
        {
            get { return IoC.Resolve<IStatisticsCodeService>(); }
        }
        private string _subSystemCode;
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        public FRSoTheoDoiCTTheoMTK()
        {
            InitializeComponent();
            this.uTreeMTK.InitializeDataNode += new Infragistics.Win.UltraWinTree.InitializeDataNodeEventHandler(this.uTree_InitializeDataNode);
            this.uTreeMTK.AfterDataNodesCollectionPopulated += new Infragistics.Win.UltraWinTree.AfterDataNodesCollectionPopulatedEventHandler(Utils.uTree_AfterDataNodesCollectionPopulated);
            this.uTreeMTK.ColumnSetGenerated += new Infragistics.Win.UltraWinTree.ColumnSetGeneratedEventHandler(this.uTree_ColumnSetGenerated);

            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\TongHopSoTheoDoiCTMaThongKe.rst", path);
            _lstAccountReport = _lstnew2;
            ugridAccountCP.SetDataBinding(_lstAccountReport.OrderBy(n => n.AccountNumber).ToList(), "");
            ReportUtils.ProcessControls(this);

            dsStatisticsCode = _lstnew;
            DataSet ds = Utils.ToDataSet<StatisticsCodeCT>(dsStatisticsCode, ConstDatabase.StatisticsCodeCT_TableName);
            uTreeMTK.SetDataBinding(ds, ConstDatabase.StatisticsCodeCT_TableName);
            
            ConfigTree(uTreeMTK);
         
            foreach (var node in uTreeMTK.Nodes)
            {
                node.Cells["Check"].Value = false;
            }
            foreach (var row in ugridAccountCP.Rows)
            {
                row.Cells["Check"].Value = false;
            }

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dtBeginDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }

        }
        private void uTree_ColumnSetGenerated(object sender, ColumnSetGeneratedEventArgs e)
        {
            Utils.uTree_ColumnSetGenerated(sender, e, ConstDatabase.StatisticsCodeCT_TableName);
        }
        private void uTree_InitializeDataNode(object sender, InitializeDataNodeEventArgs e)
        {
            Utils.uTree_InitializeDataNode(sender, e, ConstDatabase.StatisticsCodeCT_TableName);
        }
        void ConfigTree(Infragistics.Win.UltraWinTree.UltraTree ultraTree)
        {//hàm chung
            Utils.ConfigTree(ultraTree, TextMessage.ConstDatabase.StatisticsCodeCT_TableName);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
           
        }

        private void uTreeMTK_Click(object sender, EventArgs e)
        {
            if (uTreeMTK.SelectedNodes.Count == 0 && uTreeMTK.ActiveNode != null)
            {
                uTreeMTK.ActiveNode.Selected = true;
            }
            if (uTreeMTK.SelectedNodes.Count > 0)
            {
                if ((bool)uTreeMTK.SelectedNodes[0].Cells["Check"].Value == false)
                {
                    uTreeMTK.SelectedNodes[0].Cells["Check"].Value = true;
                }
                else
                {
                    uTreeMTK.SelectedNodes[0].Cells["Check"].Value = false;
                }
                var id = (Guid)uTreeMTK.SelectedNodes[0].Cells["ID"].Value;
                _lstnew.FirstOrDefault(x => x.ID == id).Check = (bool)uTreeMTK.SelectedNodes[0].Cells["Check"].Value;
                var lstID = new List<Guid>();
                getListID(lstID, id);
                foreach (var node in uTreeMTK.Nodes)
                {
                    if (lstID.Any(d => d == (Guid)node.Cells["ID"].Value))
                    {
                        _lstnew.FirstOrDefault(x => x.ID == (Guid)node.Cells["ID"].Value).Check = (bool)uTreeMTK.SelectedNodes[0].Cells["Check"].Value;
                        node.Cells["Check"].Value = uTreeMTK.SelectedNodes[0].Cells["Check"].Value;
                    }
                }
            }
        }
        void getListID(List<Guid> lstID, Guid parent)
        {
            foreach (var x in _lstnew)
            {
                if (x.ParentID == parent)
                {
                    lstID.Add(x.ID);
                    if (x.IsParentNode) getListID(lstID, x.ID);
                }
            }
        }

    

     

        private void btnExit_Click_2(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống!");
            }
            if (dtBeginDate.DateTime.Date > dtEndDate.DateTime.Date)
            {
                //MessageBox.Show("Ngày bắt đầu không thể lớn hơn ngày kết thúc", "Cảnh báo");
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            var lstAccount = _lstnew2.Where(x => x.Check).ToList();
            if (lstAccount.Count == 0)
            {
                MSG.Warning("Bạn chưa chọn tài khoản!");
                return;
            }
            var lst = _lstnew.Where(x => x.Check).ToList();
            if (lst.Count == 0)
            {
                MSG.Warning("Bạn chưa chọn Mã thống kê!");
                return;
            }
            string list_item2 = "";
            try
            {
                var dsitemChoose = lstAccount != null ? lstAccount.Select(c => c.AccountNumber.ToString()) : new List<string>();
                list_item2 = string.Join(",", dsitemChoose.ToArray());
                list_item2 = "," + list_item2 + ",";
            }
            catch (Exception ex)
            {
                list_item2 = "";
            }


            ReportProcedureSDS sp = new ReportProcedureSDS();
            string list_item = "";

            try
            {
                var dsitemChoose = lst != null ? lst.Select(c => c.ID.ToString()) : new List<string>();
                list_item = string.Join(",", dsitemChoose.ToArray());
                list_item = "," + list_item + ",";
            }
            catch (Exception ex)
            {
                list_item = "";
            }
            List<StatisticsCodeBook> data = sp.GetCHITIETMATHONGKE((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, list_item, list_item2);
            if (data.Count == 0)
                MSG.Warning("Không có dữ liệu");
            else
            {
                var rD = new StatisticsCode_Period();
                rD.Period = ReportUtils.GetPeriod((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value);
                var f = new ReportForm<StatisticsCodeBook>(fileReportSlot1, _subSystemCode);
                f.AddDatasource("CTMTK", data, true);
                f.AddDatasource("Detail", rD);
                f.SetHyperlinkFunction("Accounting.ReportUtils", "Hyperlink");
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
        }

        private void btnOk_Click_1(object sender, EventArgs e)
        {
            Close();
        }
    }
}
