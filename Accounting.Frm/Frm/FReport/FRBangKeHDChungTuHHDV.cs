﻿using Accounting.Core;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using Castle.MicroKernel.Registration;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Xsl;

namespace Accounting.Frm.FReport
{
    public partial class FRBangKeHDChungTuHHDV : Form
    {
        private readonly List<Item> _lstItems = Utils.ObjConstValue.SelectTimes;
        private readonly ISystemOptionService _ISystemOptionService;
        private readonly ISupplierServiceService _ISupplierServiceService;
        DateTime dt;
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        public FRBangKeHDChungTuHHDV()
        {
            _ISystemOptionService = IoC.Resolve<ISystemOptionService>();
            _ISupplierServiceService = IoC.Resolve<ISupplierServiceService>();
            InitializeComponent();
            InitializeGUI();
            txtYear.Value = DateTime.Now.Year;

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.MonthOrPrecious != null && dateTimeCacheHistory.Year != null)
                {
                    if (dateTimeCacheHistory.MonthOrPrecious != 0)
                    {
                        ultraOptionSet1.Value = dateTimeCacheHistory.MonthOrPrecious - 1;
                        if (dateTimeCacheHistory.MonthOrPrecious == 1)
                        {
                            cbbMonth.SelectedRow = cbbMonth.Rows[dateTimeCacheHistory.Month ?? 0];
                            txtYear.Value = dateTimeCacheHistory.Year;
                        }
                        else
                        {
                            cbbMonth.SelectedRow = cbbMonth.Rows[dateTimeCacheHistory.Precious ?? 0];
                            txtYear.Value = dateTimeCacheHistory.Year;
                        }

                    }

                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.MonthOrPrecious = ultraOptionSet1.Value.ToInt() + 1;
                if (ultraOptionSet1.Value.ToInt() == 0)
                {
                    dateTimeCacheHistory.Month = cbbMonth.SelectedRow.Index;
                }
                else
                {
                    dateTimeCacheHistory.Precious = cbbMonth.SelectedRow.Index;
                }
                dateTimeCacheHistory.Year = txtYear.Value.ToInt();
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }
        }
        private void InitializeGUI()
        {
            cbbMonth.ConfigComboSelectTime(_lstItems.Where(n => int.Parse(n.Value) < 21 && int.Parse(n.Value) > 8).ToList(), "Name");
            cbbMonth.DropDownStyle = UltraComboStyle.DropDownList;
            foreach (var item in cbbMonth.Rows)
            {
                if (int.Parse((item.ListObject as Item).Value) == DateTime.Now.Month + 8) cbbMonth.SelectedRow = item;
            }
        }

        private void cbbMonth_ValueChanged(object sender, EventArgs e)
        {
            if (cbbMonth.SelectedRow != null)
            {
                var model = cbbMonth.SelectedRow.ListObject as Item;
                DateTime dtBegin;
                DateTime dtEnd;
                Utils.GetDateBeginDateEnd((int)txtYear.Value,DateTime.Now, model, out dtBegin, out dtEnd);
                dteDateFrom.Value = new DateTime((int)txtYear.Value, dtBegin.Month, dtBegin.Day);
                dteToDate.Value = new DateTime((int)txtYear.Value, dtEnd.Month, dtEnd.Day);
            }
        }

        private void ultraOptionSet1_ValueChanged(object sender, EventArgs e)
        {
            if (ultraOptionSet1.CheckedIndex == 0)
            {
                ultraLabel1.Text = "Tháng";
                cbbMonth.ConfigComboSelectTime(_lstItems.Where(n => int.Parse(n.Value) < 21 && int.Parse(n.Value) > 8).ToList(), "Name");
                foreach (var item in cbbMonth.Rows)
                {
                    if (int.Parse((item.ListObject as Item).Value) == DateTime.Now.Month + 8) cbbMonth.SelectedRow = item;
                }
            }
            else
            {
                ultraLabel1.Text = "Quý";
                cbbMonth.ConfigComboSelectTime(_lstItems.Where(n => int.Parse(n.Value) < 25 && int.Parse(n.Value) > 20).ToList(), "Name");
                float now = float.Parse(DateTime.Now.Month.ToString()) / 3;
                foreach (var item in cbbMonth.Rows)
                {
                    if (now <= 1)
                    {
                        if (int.Parse((item.ListObject as Item).Value) == 21) cbbMonth.SelectedRow = item;
                    }
                    if (now <= 2 && now > 1)
                    {
                        if (int.Parse((item.ListObject as Item).Value) == 22) cbbMonth.SelectedRow = item;
                    }
                    if (now <= 3 && now > 2)
                    {
                        if (int.Parse((item.ListObject as Item).Value) == 23) cbbMonth.SelectedRow = item;
                    }
                    if (now <= 4 && now > 3)
                    {
                        if (int.Parse((item.ListObject as Item).Value) == 24) cbbMonth.SelectedRow = item;
                    }

                }

            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "HDDT").Data == "1")
            {

                DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
                if (dateTimeCacheHistory != null)
                {
                    dateTimeCacheHistory.MonthOrPrecious = ultraOptionSet1.Value.ToInt() + 1;
                    if (ultraOptionSet1.Value.ToInt() == 0)
                    {
                        dateTimeCacheHistory.Month = cbbMonth.SelectedRow.Index;
                    }
                    else
                    {
                        dateTimeCacheHistory.Precious = cbbMonth.SelectedRow.Index;
                    }
                    dateTimeCacheHistory.Year = txtYear.Value.ToInt();
                    _IDateTimeCacheHistoryService.BeginTran();
                    _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                    _IDateTimeCacheHistoryService.CommitTran();
                }
                else
                {
                    dateTimeCacheHistory = new DateTimeCacheHistory();
                    dateTimeCacheHistory.ID = Guid.NewGuid();
                    dateTimeCacheHistory.IDUser = Authenticate.User.userid;

                    dateTimeCacheHistory.SubSystemCode = this.Name;
                    dateTimeCacheHistory.MonthOrPrecious = ultraOptionSet1.Value.ToInt() + 1;
                    if (ultraOptionSet1.Value.ToInt() == 0)
                    {
                        dateTimeCacheHistory.Month = cbbMonth.SelectedRow.Index;
                    }
                    else
                    {
                        dateTimeCacheHistory.Precious = cbbMonth.SelectedRow.Index;
                    }
                    dateTimeCacheHistory.Year = txtYear.Value.ToInt();
                    _IDateTimeCacheHistoryService.BeginTran();
                    _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                    _IDateTimeCacheHistoryService.CommitTran();
                }
                SystemOption systemOption = Utils.ListSystemOption.FirstOrDefault(n => n.Code == "EI_IDNhaCungCapDichVu");
                if (!string.IsNullOrEmpty(systemOption.Data))
                {
                    SupplierService supplierService = _ISupplierServiceService.Getbykey(Guid.Parse(systemOption.Data));
                    if (supplierService.SupplierServiceCode == "SDS")
                    {
                        RestApiService client = new RestApiService(Utils.GetPathAccess(), Utils.GetUserName(), Core.ITripleDes.TripleDesDefaultDecryption(Utils.GetPassWords()));

                        try
                        {
                            var request = new Request();
                            var model = cbbMonth.SelectedRow.ListObject as Item;
                            DateTime dtBegin;
                            DateTime dtEnd;
                            Utils.GetDateBeginDateEnd(DateTime.Now.Year, DateTime.Now, model, out dtBegin, out dtEnd);
                            request.FromDate = dteDateFrom.DateTime.ToString("dd/MM/yyyy");
                            request.ToDate = dteToDate.DateTime.ToString("dd/MM/yyyy");
                            request.Option = 0;
                            var response = client.Download(supplierService.ApiService.FirstOrDefault(n => n.ApiName == "BangKeHDChungTuHHDV").ApiPath, request);
                            Regex tagRegex = new Regex(@"<\s*([^ >]+)[^>]*>.*?<\s*/\s*\1\s*>");
                            string t1 = "<html>";

                            ViewRP ViewRP = new ViewRP(t1 + response.Content + "</html>", request, 1);
                            ViewRP.Text = "BẢNG KÊ HOÁ ĐƠN, CHỨNG TỪ HÀNG HOÁ, DỊCH VỤ BÁN RA";
                            ViewRP.View();
                            //if (tagRegex.IsMatch(response.Message))
                            //{
                            //    ViewRP ViewRP = new ViewRP(response.Message, request);
                            //    ViewRP.View();
                            //}else if (IsValidXml(response.Message))
                            //{
                            //    ViewRP ViewRP = new ViewRP(TransformXMLToHTML(response.Message,""), request);
                            //    ViewRP.View();
                            //}
                            //else
                            //{
                            //    MSG.Error("Lỗi xem báo cáo : "+ response.Message);
                            //}

                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }
                    }
                }
            }
            else
                MSG.Warning("Không thể xem hóa đơn khi chưa tích hợp hóa đơn điện tử");
        }
        public string GenerateHtml()
        {
            var doc = new XmlDocument();
            var htmlRoot = doc.CreateElement("html");
            doc.AppendChild(htmlRoot);
            var body = doc.CreateElement("body");
            body.AppendChild(doc.CreateTextNode("Hello world"));
            htmlRoot.AppendChild(body);
            return doc.OuterXml;
        }
        public bool IsValidXml(string xml)
        {
            try
            {
                XDocument.Parse(xml);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public string TransformXMLToHTML(string inputXml, string xsltString)
        {
            XslCompiledTransform transform = new XslCompiledTransform();
            using (XmlReader reader = XmlReader.Create(new StringReader(xsltString)))
            {
                transform.Load(reader);
            }
            StringWriter results = new StringWriter();
            using (XmlReader reader = XmlReader.Create(new StringReader(inputXml)))
            {
                transform.Transform(reader, null, results);
            }
            return results.ToString();
        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtYear_ValueChanged(object sender, EventArgs e)
        {
            DateTime t1 = dteDateFrom.DateTime;
            DateTime t2 = dteToDate.DateTime;
            if (cbbMonth.Text == "Tháng 2")
            {
                var model = cbbMonth.SelectedRow.ListObject as Item;
                DateTime dtBegin;
                DateTime dtEnd;
                Utils.GetDateBeginDateEnd((int)txtYear.Value, DateTime.Now, model, out dtBegin, out dtEnd);
                dteDateFrom.Value = new DateTime((int)txtYear.Value, t1.Month, t1.Day);
                dteToDate.Value = new DateTime((int)txtYear.Value, t2.Month, dtEnd.Day);
            }
            else
            {
                dteDateFrom.Value = new DateTime((int)txtYear.Value, t1.Month, t1.Day);
                dteToDate.Value = new DateTime((int)txtYear.Value, t2.Month, t2.Day);
            }
        }
    }
}
