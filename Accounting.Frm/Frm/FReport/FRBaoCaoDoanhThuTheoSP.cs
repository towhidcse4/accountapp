﻿using Accounting.Core;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRBaoCaoDoanhThuTheoSP : Form
    {
        private readonly List<Item> _lstItems = Utils.ObjConstValue.SelectTimes;
        private readonly ISystemOptionService _ISystemOptionService;
        private readonly ISupplierServiceService _ISupplierServiceService;
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        public FRBaoCaoDoanhThuTheoSP()
        {
            _ISystemOptionService = IoC.Resolve<ISystemOptionService>();
            _ISupplierServiceService = IoC.Resolve<ISupplierServiceService>();
            InitializeComponent();

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    
                    dteFromDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dteToDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dteFromDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dteToDate.DateTime;
                
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "HDDT").Data == "1")
            {

                DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
                if (dateTimeCacheHistory != null)
                {
                    dateTimeCacheHistory.DtBeginDate = dteFromDate.DateTime;
                    dateTimeCacheHistory.DtEndDate = dteToDate.DateTime;
                   
                    _IDateTimeCacheHistoryService.BeginTran();
                    _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                    _IDateTimeCacheHistoryService.CommitTran();
                }
                else
                {
                    dateTimeCacheHistory = new DateTimeCacheHistory();
                    dateTimeCacheHistory.ID = Guid.NewGuid();
                    dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                   
                    dateTimeCacheHistory.SubSystemCode = this.Name;
                    dateTimeCacheHistory.DtBeginDate = dteFromDate.DateTime;
                    dateTimeCacheHistory.DtEndDate = dteToDate.DateTime;
                    _IDateTimeCacheHistoryService.BeginTran();
                    _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                    _IDateTimeCacheHistoryService.CommitTran();
                }
                SystemOption systemOption = Utils.ListSystemOption.FirstOrDefault(n => n.Code == "EI_IDNhaCungCapDichVu");
                if (!string.IsNullOrEmpty(systemOption.Data))
                {
                    SupplierService supplierService = _ISupplierServiceService.Getbykey(Guid.Parse(systemOption.Data));
                    if (supplierService.SupplierServiceCode == "SDS")
                    {
                        RestApiService client = new RestApiService(Utils.GetPathAccess(), Utils.GetUserName(), Core.ITripleDes.TripleDesDefaultDecryption(Utils.GetPassWords()));

                        try
                        {
                            var request = new Request();
                            //var model = cbbMonth.SelectedRow.ListObject as Item;
                            //Utils.GetDateBeginDateEnd(DateTime.Now, model, out dtBegin, out dtEnd);
                            request.FromDate = dteFromDate.DateTime.ToString("dd/MM/yyyy");
                            request.ToDate = dteToDate.DateTime.ToString("dd/MM/yyyy");
                            request.Option = 0;
                            var response = client.Download(supplierService.ApiService.FirstOrDefault(n => n.ApiName == "BaoCaoDoanhThuTheoSP").ApiPath, request);
                            //Regex tagRegex = new Regex(@"<\s*([^ >]+)[^>]*>.*?<\s*/\s*\1\s*>");
                            //if (tagRegex.IsMatch(response.Message))
                            //{
                            //    ViewRP ViewRP = new ViewRP(response.Message, request,2);
                            //    ViewRP.View();
                            //}
                            //else
                            //{
                            //    MSG.Error("Lỗi xem báo cáo");
                            //}
                            string t1 = "<html>";
                            ViewRP ViewRP = new ViewRP(t1 + response.Content + "</html>", request, 2);
                            ViewRP.Text = "BÁO CÁO DOANH THU THEO SẢN PHẨM";
                            ViewRP.View();

                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }
                    }
                }
            }
            else
                MSG.Warning("Không thể xem báo cáo khi chưa tích hợp hóa đơn điện tử");
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
