﻿using Accounting.Core.Domain;
using Accounting.TextMessage;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRS07DNNTruocIn : Form
    {
        public FRS07DNNTruocIn(List<S07DNN> s07DNNs )
        {
            InitializeComponent();
            uGridDuLieu.DataSource = s07DNNs;
            Utils.ConfigGrid(uGridDuLieu, ConstDatabase.S07DNN_TableName);
            //uGridDuLieu.DataSource = s07DNNs;
        }
    }
}
