﻿using Accounting.Core.DAO;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.Report;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
//using Infragistics.Win.UltraWinTree;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRSoTongHopDoanhThuTheoNhanVien : Form
    {
        private readonly IMaterialGoodsService _IMaterialGoodsService;
        private List<MaterialGoodsReport> _lstMaterialGoods = new List<MaterialGoodsReport>();       
        List<AccountingObjectReport> _lstAccountingObjectReport = new List<AccountingObjectReport>();      
        private readonly List<MaterialGoodsReport> _lstgoods = ReportUtils.LstMaterialGoodsReport;
        private readonly List<AccountingObjectReport> _lstNV = ReportUtils.LstEmployee;

        private readonly ICurrencyService _ICurrencyService;
        private string _subSystemCode;
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        public FRSoTongHopDoanhThuTheoNhanVien()
        {
            InitializeComponent();
            _subSystemCode = "";
            _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();

            //Grid nhân viên
            foreach (var item in _lstNV) { item.Check = false; }
            //lấy list nhân viên
            _lstAccountingObjectReport = _lstNV.OrderBy(n => n.AccountingObjectCode).OrderBy(n => n.DepartmentName).ToList();
            //binding ugridEmployee
            ugridEmployee.SetDataBinding(_lstAccountingObjectReport.ToList(), "");

            //Gridivew vật tư           
            foreach (var item in _lstgoods) { item.Check = false; }
            //lấy list vat tu
            _lstMaterialGoods = _lstgoods.OrderBy(n => n.MaterialGoodsCode).ToList();
            //binding ugridMaterialGoodsCategory
            ugridMaterialGoodsCategory.SetDataBinding(_lstMaterialGoods.ToList(), "");
            ReportUtils.ProcessControls(this);

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dtBeginDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }
        }
       
        private void btnExit_Click(object sender, EventArgs e)
        {
            foreach (var item in ugridEmployee.Rows)
            {
                item.Cells["Check"].Value = false;
            }
            foreach (var item in ugridMaterialGoodsCategory.Rows)
            {
                item.Cells["Check"].Value = false;
            }
            Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }

            if (dtBeginDate.DateTime.Date > dtEndDate.DateTime.Date)
            {
                //MessageBox.Show("Ngày bắt đầu không thể lớn hơn ngày kết thúc", "Cảnh báo");
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            var lstAccountingObjectReport = _lstAccountingObjectReport.Where(x => x.Check).ToList();
            if (lstAccountingObjectReport.Count == 0)
            {
                MSG.Warning("Bạn chưa chọn mã nhân viên!");
                return;
            }
            //ham
            string GetStringByListString(List<string> lstStr)
            {
                var str = "";
                foreach (var x in lstStr)
                {
                    str = str + "," + x;
                }
                return str + ",";
            }
            ReportProcedureSDS sp = new ReportProcedureSDS();
            //list Id nhân viên
            var lstNV_id = ((List<AccountingObjectReport>)ugridEmployee.DataSource).Where(c => c.Check).Select(x => x.ID.ToString()).ToList();
            string lstNV_id_tostring = GetStringByListString(lstNV_id);
            //list Id vật tư
            var lstVattu_id = ((List<MaterialGoodsReport>)ugridMaterialGoodsCategory.DataSource).Where(c => c.Check).Select(x => x.ID.ToString()).ToList();
            var lstVattu_idAll = _lstgoods.Select(n => n.ID.ToString()).ToList();//list all mã vật tư ID

            string strlstVattu_idSelect;
            //nếu ko chọn mã hàng thì lấy toàn bộ mã hàng
            if (lstVattu_id.Count==0)
            {
                strlstVattu_idSelect = GetStringByListString(lstVattu_idAll);
            }
            else
            {
                strlstVattu_idSelect = GetStringByListString(lstVattu_id);
            }
            FRSOTONGHOPDOANHTHUTHEONVex fm = new FRSOTONGHOPDOANHTHUTHEONVex((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, lstNV_id_tostring, strlstVattu_idSelect);
            fm.Show(this);
            
        }
    }
}
