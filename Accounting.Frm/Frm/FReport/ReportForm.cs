﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PerpetuumSoft.Reporting.DOM;
using PerpetuumSoft.Framework.Serialization;
using System.Linq.Dynamic;
using System.IO;
using Accounting.Core.Domain;
using PerpetuumSoft.Reporting.View;
using Accounting.TextMessage;
using PerpetuumSoft.Reporting.Components;
using Accounting.Core.IService;
using Accounting.Core.Domain.obj;
using Newtonsoft.Json;
using Accounting.Core;
using PerpetuumSoft.Reporting.Export;
using PerpetuumSoft.Reporting.Export.Excel;
using Excel = Microsoft.Office.Interop.Excel;       //microsoft Excel 14 object in references-> COM tab
using System.Runtime.InteropServices;
using Infragistics.Documents.Excel;
using ClosedXML.Excel;
using System.Reflection;


namespace Accounting
{
    public partial class ReportForm<T> : PerpetuumSoft.Reporting.View.PreviewForm
    {
        public int DefaultPaperSize = ConstFrm.Paper.PAPER_SIZE_A4;
        private string _filSourceName = "";
        private List<T> _filDatasource;
        private IList<ReportFilter> _listFilter;
        ReportStyle _reportStyle;
        readonly PerpetuumSoft.Reporting.Components.ReportSlot rSlot;
        private string _hyperlinkClickFunctionClass = "Accounting.ReportUtils";
        private string HyperlinkClickFunctionName = "Hyperlink";
        private string _subSystemCode;
        private Image Image = Properties.Resources.PhienBanDungThu;
        public ReportForm(PerpetuumSoft.Reporting.Components.ReportSlot slot, string subSystemCode)
            : base(slot)
        {
            InitializeComponent();
            rSlot = slot;
            _subSystemCode = subSystemCode;
            _listFilter = LoadFilter(typeof(T).Name);
            // ẩn btn chỉnh sửa dữ liệu
            ReportViewer.ShowDesigner = false;
            //   ReportViewer.ShowExport = false;
            ReportViewer.ShowSave = false;
            ReportViewer.ShowOpen = false;
            ReportViewer.ShowContent = false;
            ReportViewer.SetNewZoom(new PerpetuumSoft.Reporting.View.Zoom(1));
            ReportViewer.Actions["Print"].Executing += ReportForm_Executing;
            ////thay thế menu about
            var mainMenu = MainMenuStrip;
            mainMenu.Items.RemoveAt(4);
            var helpTSMI = new ToolStripMenuItem("Trợ giúp");
            var aboutMenu = new ToolStripMenuItem("Thông tin", null, new EventHandler(About_Event));
            helpTSMI.DropDownItems.Add(aboutMenu);
            mainMenu.Items.Add(helpTSMI);
            toolStripButton.Visible = false;
            
            toolStrip1.Location = toolStripButton.Location;
            //toolStrip1.Visible = false;
        }
        //Image to byte[]   

        void aboutclick(object sender, PerpetuumSoft.Reporting.Windows.Forms.ExecutingEventArgs e)
        {
            e.Handled = true;
        }

        void About_Event(object sender, EventArgs e)
        {
            var f = new Form { Text = "Thông tin chương trình" };
            f.ShowDialog(this);
        }

        public void AddDatasource(string sourceName, object data)
        {
            rSlot.Manager.DataSources.Remove(sourceName);
            rSlot.Manager.DataSources.Add(sourceName, data);
        }
        #region Add watermark for client demo byHautv
        private bool checkIsDemo()
        {
            SystemOption ProductID = Utils.ListSystemOption.FirstOrDefault(o => o.Code == "ProductID");
            Guid pid = new Guid();
            Registration product = new Registration();
            Parameters param = Utils.IParametersService.GetByCode("Registration");
            if (ProductID == null)
            {
                MSG.Error("Phần mềm chưa được đăng ký sử dụng với công ty phát hành.");
                return true;
            }
            else
            {
                Guid.TryParse(ProductID.Data, out pid);

                if (pid == Guid.Empty)
                {
                    MSG.Error("Phần mềm chưa được đăng ký sử dụng với công ty phát hành.");
                    return true;
                }
            }
            if (param != null && !param.Data.IsNullOrEmpty())
            {
                string descrypt = ITripleDes.TripleDesDefaultDecryption(param.Data);
                product = JsonConvert.DeserializeObject<Registration>(descrypt);
                if (product.ServicePackage != null)
                {
                    if (product.ServicePackage.Equals("DEMO"))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                    return true;
            }
            return false;
        }
        public void AddWatermark(Document document)
        {
            if (Utils.isDemo)
            {
                var t = document.Pages;
                foreach (Page item in t)
                {
                    //PerpetuumSoft.Reporting.DOM.TextBox textBox = new PerpetuumSoft.Reporting.DOM.TextBox();
                    //textBox.Font = new PerpetuumSoft.Framework.Drawing.FontDescriptor(font);
                    //textBox.Location = new PerpetuumSoft.Framework.Drawing.Vector(2000, 1500);
                    //textBox.Text = "PHIÊN BẢN DÙNG THỬ";
                    //textBox.Font = new PerpetuumSoft.Framework.Drawing.FontDescriptor("Times New Roman", 30, PerpetuumSoft.Framework.Drawing.FontStyleMode.On, PerpetuumSoft.Framework.Drawing.FontStyleMode.On, PerpetuumSoft.Framework.Drawing.FontStyleMode.Off);
                    //textBox.CanGrow = true;
                    //textBox.Visible = true;
                    //textBox.Size = new PerpetuumSoft.Framework.Drawing.Vector(1500, 500);
                    //item.Controls.Add(textBox);
                    PerpetuumSoft.Reporting.DOM.Picture picture = new Picture();
                    picture.Image = Image;
                    picture.Size = picture.Image.Size;
                    picture.Visible = true;
                    var X = item.Location.X + (item.Size.Width / 2) - picture.Size.Width / 2;
                    var Y = item.Location.Y + item.Size.Height / 2 - picture.Size.Height / 2;
                    picture.Location = new PerpetuumSoft.Framework.Drawing.Vector(X, Y);
                    item.Controls.Add(picture);
                }
                //btnExport.Visible = false;
                if (MainMenuStrip.Items[0].Name == "fileToolStripMenuItem")
                {
                    MainMenuStrip.Items.RemoveAt(0);
                    toolStripButton.Location = new Point(toolStripButton.Location.X - 60, toolStripButton.Location.Y);
                }
                    
                //ReportViewer.ShowToolBar. = false;
                
                ((System.Windows.Forms.ToolStrip)ReportViewer.Controls[2]).Items.RemoveByKey("exportToolStripButton");
                toolStrip1.Visible = false;
            }
            //trungnq thêm để ẩn button kết xuất excel khi đã có nút excel ở form trc
            string tenFile = this.Text.Replace(" - Preview report", "");
            tenFile = tenFile.Replace(" ", "");
            List<string> listAnNutInOFormIn = new List<string> {"BảngCânĐốiTàiKhoản","BáoCáoTìnhHìnhTàiChính","BáoCáoTìnhHìnhTàiChính","Báocáokếtquảhoạtđộngkinhdoanh","Báocáolưuchuyểntiềntệ","Sổkếtoánchitiếtquỹtiềnmặt","Sổquỹtiềnmặt","Sốtiềngửitàikhoảnngânhàng","Bảngkêsốdưngânhàng","Sổnhậtkýbánhàng","Tổnghợpcôngnợphảithu","Chitiếtcôngnợphảithu","Tổnghợpcôngnợphảithutheonhómkháchhàng","Sổtheodõilãilỗtheomặthàng","Sổtheodõicôngnợphảithutheomặthàng","Sổtheodõilãilỗtheohóađơn","Sổtheodõicôngnợphảithutheohóađơn","Sổtheodõilãilỗtheohợpđồngbán","Sổtheodõicôngnợphảithutheohợpđồngbán","Sổchitiếtdoanhthutheonhânviên","Sổtổnghợpdoanhthutheonhânviên","Tổnghợpcôngnợphảitrả","Sổnhậtkýmuahàng","Sổchitiếtmuahàng","Chitiếtcôngnợphảitrảnhàcungcấp","Tổnghợptồnkho","Sổcôngcụdụngcụ","Sổchitiếtcáctàikhoản","Nhậtkýchung","Sổtổnghợpdoanhthutheonhânviên","Sổnhậtkýthutiền","Sổnhậtkýchitiền","Tổnghợpcôngnợnhânviên","Chitiếtcôngnợnhânviên","Bảngphânbổchiphítrảtrước","TongHopCPTheoKMCP","Sổtổnghợplươngnhânviên","Sổchitiếtlãilỗtheocôngtrìnhvụviệc","Sổtổnghợplãilỗtheocôngtrình", "Bảngkêhóađơn,chứngtừhànghóamuavào", "Bảngkêhóađơn,chứngtừhànghóabánra", "TìnhHìnhThựcHiệnHợpĐồngMua", "TìnhHìnhThựcHiệnHợpĐồngBán" };

            List<string> listHienNutInOFormIn = new List<string> { "Phiếuxuấtkhobánhàng", "Phiếuxuấtkho", "Sổchitiếtbánhàng", "Sổchitiếtphảithukháchhàngtheomặthàng", "Sổchitiếtphảithukháchhàngtheomặthàng", "Thẻkho(Sổkho)", "Sổchitiếtvậtliệu,dụngcụ(sảnphẩm,hànghóa)", "Bảngtổnghợpchitiếtvậtliệu,dụngcụ(sảnphẩm,hànghóa)", "Sổtheodõicôngcụdụngcụtạinơisửdụng", "Thẻtàisảncốđịnh", "Sổtàisảncốđịnh", "Sổtheodõitàisảncốđịnhtạinơisửdụng", "Sổcái(DùngchohìnhthứckếtoánNhậtkýchung)", "Nhậtkýchung", "Sổcái(DùngchohìnhthứckếtoánChứngtừghisổ)", "Sổchitiếttiềnvay", "SoChitietThanhToan_S12", "Sốtheodõithanhtoánbằngngoạitệ", "Sổtheodõichitiếttheomãthốngkê", "Sổtheodõichitiếttheođốitượngtậphợpchiphí", "SoTheoDoiDoiTuongTHCP", "TheTinhGiaThanhSanPhamDichVu", "TÌNHHÌNHSỬDỤNGHÓAĐƠN" };
            if(listHienNutInOFormIn.Contains(tenFile))
            {
                toolStrip1.Visible = true;
            }
            if (listAnNutInOFormIn.Contains(tenFile))
            {
                MainMenuStrip.Items.RemoveAt(0);
                toolStripButton.Location = new Point(toolStripButton.Location.X - 60, toolStripButton.Location.Y);
                //((System.Windows.Forms.ToolStrip)ReportViewer.Controls[2]).Items.RemoveByKey("exportToolStripButton");
                toolStrip1.Visible = false;
            }//trungnq
        }
        #endregion
        public void AddDatasource(string sourceName1, string sourceName2, object data1, object data2)
        {
            rSlot.Manager.DataSources.Remove(sourceName1);
            rSlot.Manager.DataSources.Remove(sourceName2);
            rSlot.Manager.DataSources.Add(sourceName1, data1);
            rSlot.Manager.DataSources.Add(sourceName2, data2);
        }

        public void AddDatasource(string sourceName, object data, bool isListData)
        {
            _filSourceName = sourceName;
            _filDatasource = ((System.Collections.IEnumerable)data).Cast<T>().ToList();
            if (_filDatasource.Count == 0) _filDatasource.Add((T)Activator.CreateInstance(typeof(T)));
            //  listFilter = lstfil;
            rSlot.Manager.DataSources.Remove(sourceName);
            rSlot.Manager.DataSources.Add(sourceName, FillData(_filDatasource, _listFilter));
        }

        public void LoadReport()
        {
            try
            {
                rSlot.RenderDocument();                
                ReportViewer.DocumentLoaded += new EventHandler(SetStyle);
                ReportViewer.Source.HyperlinkClick += new PerpetuumSoft.Reporting.Components.HyperlinkEventHandler(HyperlinkClick);
            }
            catch (Exception ex)
            {
                MSG.Warning(ex.ToString());
            }
        }
        // ReportViewer.Source.OnHyperlinkClick
        void HyperlinkClick(object sender, PerpetuumSoft.Reporting.Components.HyperlinkEventArgs e)
        {
            e.Handled = true;
            string hyperlink = e.Hyperlink;
            if (string.IsNullOrEmpty(hyperlink) || string.IsNullOrEmpty(_hyperlinkClickFunctionClass)
                                        || string.IsNullOrEmpty(HyperlinkClickFunctionName))
                return;
            var type = System.Type.GetType(_hyperlinkClickFunctionClass);
            if (type == null) return;
            object instance = new object();
            try { instance = Activator.CreateInstance(type); }
            catch { }
            System.Reflection.MethodInfo methodinfo = type.GetMethod(HyperlinkClickFunctionName);
            if (methodinfo == null)
                return;
            methodinfo.Invoke(instance, new object[] { hyperlink });
        }
        private void SetStyle(object sender, EventArgs e)
        {
            _reportStyle = new ReportStyle();
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            var file = new System.IO.StreamReader(string.Format("{0}\\Frm\\FReport\\Template\\ReportStyle.xml", path));
            var reader = new System.Xml.Serialization.XmlSerializer(typeof(ReportStyle));
            _reportStyle = (ReportStyle)reader.Deserialize(file);
            file.Close();
            // load style
            StyleCollection styleCollection = _reportStyle.styleCollection;
            Document document = ReportViewer.Document;
            // Document document = rSlot.Document;
            if (document == null) return;
            SetFontStyle(document.StyleSheet.Styles, styleCollection, "ReportTitle");
            SetFontStyle(document.StyleSheet.Styles, styleCollection, "DetailHeader");
            SetFontStyle(document.StyleSheet.Styles, styleCollection, "ReportSubTitle");
            SetFontStyle(document.StyleSheet.Styles, styleCollection, "DetailText");
            SetFontStyle(document.StyleSheet.Styles, styleCollection, "DetailNumber");
            SetFontStyle(document.StyleSheet.Styles, styleCollection, "SummaryDetailText");
            SetFontStyle(document.StyleSheet.Styles, styleCollection, "SummaryDetailNumber");
            SetFontStyle(document.StyleSheet.Styles, styleCollection, "SummaryFooterText");
            SetFontStyle(document.StyleSheet.Styles, styleCollection, "SummaryFooterNumber");
            SetFontStyle(document.StyleSheet.Styles, styleCollection, "SignerSubTitle");
            SetFontStyle(document.StyleSheet.Styles, styleCollection, "SignerDate");
            SetFontStyle(document.StyleSheet.Styles, styleCollection, "SignerName");
            SetFontStyle(document.StyleSheet.Styles, styleCollection, "CompanyName");
            SetFontStyle(document.StyleSheet.Styles, styleCollection, "CompanyInformation");
            SetFontStyle(document.StyleSheet.Styles, styleCollection, "ProductName");
            SetFontStyle(document.StyleSheet.Styles, styleCollection, "ReportSubTitleItalicBold");
            //Check phien ban dung thu
            AddWatermark(document);
            ReportViewer.Refresh();
            ReportViewer.Zoom = new Zoom(ZoomMode.Scale, 1.0);

        }

        private void SetFontStyle(StyleCollection styleCollectionOut, StyleCollection styleCollectionIn, string styleName)
        {
            if (styleCollectionIn[styleName] == null)
                return;
            if (styleCollectionOut[styleName] == null)
                styleCollectionOut.Add(styleCollectionIn[styleName]);
            else styleCollectionOut[styleName].Font = styleCollectionIn[styleName].Font;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                Form f = new FReportSettingFont();
                f.ShowDialog(this);
                LoadReport();
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                return;
            }
        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            //var obj = rSlot.Manager.DataSources[filSourceName];
            //var list = ((System.Collections.IEnumerable)obj).Cast<object>().ToList();
            //if (this.listFilter == null) return;
            try
            {
                var f = new FReportFilter(_listFilter);
                f.ShowDialog(this);
                if (f.DialogResult == DialogResult.Cancel)
                    return;
                _listFilter = f.LstFilter;
                var ddd = FillData(_filDatasource, _listFilter);
                AddDatasource(_filSourceName, ddd);
                LoadReport();
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }

        public void SetHyperlinkFunction(string className, string functionName)
        {
            _hyperlinkClickFunctionClass = className;
            HyperlinkClickFunctionName = functionName;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if(Utils.isDemo)
            {
                MSG.Warning("Phiên bản dùng thử không có quyền sử dụng chức năng này");
                return;
            }
            ReportViewer.ExportReport();
        }

        private List<T> FillData(List<T> data, IList<ReportFilter> lstRF)
        {
            if (lstRF == null || lstRF.Count == 0)
                return data;
            List<T> inputData = data;
            List<T> outputData = new List<T>();
            foreach (ReportFilter rf in lstRF)
            {
                List<T> temp = new List<T>();
                var property = typeof(T).GetProperty(rf.Name);
                if (property != null)
                {
                    rf.FieldType = property.PropertyType;

                    temp = FillData(inputData, rf);
                }
                else
                {
                    rf.FieldType = typeof(String);
                    temp = inputData;
                }
                if (rf.Condition == "or")
                {
                    outputData = outputData.Union(temp).ToList();
                }
                else
                {
                    outputData = temp;
                    inputData = temp;
                }
            }
            return outputData;
        }
        private List<T> FillData(List<T> data, ReportFilter filter)
        {
            List<T> outputData;
            string predicate = GetPredicate(filter);
            if (string.IsNullOrEmpty(predicate))
                outputData = data;
            else try
                {
                    outputData = data.AsQueryable().Where(predicate).ToList();
                    var property = typeof(T).GetProperty("OrderType");
                    if (property != null)  // add thêm các dòng có ordertype != 0
                    {
                        var sumData = data.AsQueryable().Where("OrderType != 0").ToList();
                        outputData.AddRange(sumData);
                    }
                }
                catch { outputData = data; }
            outputData = SortData(outputData, filter);
            return outputData;
        }
        private string GetPredicate(ReportFilter filter)
        {
            if (string.IsNullOrEmpty(filter.Operator)) return "";
            #region// edit value by type
            string tValue1 = filter.Value1;
            string tValue2 = filter.Value2;
            switch (filter.FieldType.Name)
            {
                case "DateTime":
                    if (!String.IsNullOrEmpty(filter.Value1))
                    {
                        DateTime d;
                        //DateTime.TryParseExact(filter.Value1, "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out d);
                        DateTime.TryParse(filter.Value1, out d);
                        tValue1 = String.Format("DateTime({0}, {1}, {2})", d.Year, d.Month, d.Day);
                    }
                    if (!String.IsNullOrEmpty(filter.Value2))
                    {
                        DateTime d;
                        // DateTime.TryParseExact(filter.Value2, "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out d);
                        DateTime.TryParse(filter.Value2, out d);
                        tValue2 = String.Format("DateTime({0}, {1}, {2})", d.Year, d.Month, d.Day);
                    }
                    // trường hợp datetime, chỉ so sánh Date không so sánh Time nên phải thay đổi tên trường
                    switch (filter.Operator)
                    {
                        case "": return "";
                        case "betwen":
                            if (string.IsNullOrEmpty(tValue1) || string.IsNullOrEmpty(tValue2)) return "";
                            return filter.Name + ".Date >" + tValue1 + " And " + filter.Name + ".Date <" + tValue2;
                        case ">":
                        case "<":
                        case "==":
                            if (string.IsNullOrEmpty(tValue1)) return "";
                            return filter.Name + ".Date " + filter.Operator + tValue1;
                    }
                    break;
                case "String":
                    if (!String.IsNullOrEmpty(filter.Value1))
                    {
                        tValue1 = String.Format("\"{0}\"", filter.Value1);
                    }
                    if (!String.IsNullOrEmpty(filter.Value2))
                    {
                        tValue2 = String.Format("\"{0}\"", filter.Value2);
                    }
                    break;
                default: break;
            }
            #endregion
            if (string.IsNullOrEmpty(filter.Operator)) return "";
            switch (filter.Operator)
            {
                case "": return "";
                case "true":
                case "false":
                    return filter.Name + " = " + filter.Operator;
                case "betwen":
                    if (string.IsNullOrEmpty(tValue1) || string.IsNullOrEmpty(tValue2)) return "";
                    return filter.Name + ">" + tValue1 + " And " + filter.Name + "<" + tValue2;
                case "contain":
                    if (string.IsNullOrEmpty(tValue1)) return "";
                    return filter.Name + ".Contains(" + tValue1 + ")";
                default:
                    if (string.IsNullOrEmpty(tValue1)) return "";
                    return filter.Name + filter.Operator + tValue1;
            }
        }
        private List<T> SortData(List<T> source, ReportFilter filter)
        {
            if (source == null || source.Count == 0) return source;
            if (string.IsNullOrEmpty(filter.Sort)) return source;
            var orderAscPr = source.FirstOrDefault().GetType().GetProperty(filter.Name);
            if (orderAscPr == null) return source;
            List<T> outputData = new List<T>();
            if (filter.Sort.Contains("des"))
            {
                outputData = (from s in source
                              orderby orderAscPr.GetValue(s, null) descending
                              select s).ToList();
            }
            else if (filter.Sort.Contains("asc"))
            {
                outputData = (from s in source
                              orderby orderAscPr.GetValue(s, null) ascending
                              select s).ToList();
            }
            return outputData;
        }

        private IList<ReportFilter> LoadFilter(string fName)
        {
            FilterCollection fc = new FilterCollection();
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            System.IO.StreamReader file = new System.IO.StreamReader(string.Format("{0}\\Frm\\FReport\\Template\\ReportFilter.xml", path));
            System.Xml.Serialization.XmlSerializer reader = new System.Xml.Serialization.XmlSerializer(typeof(FilterCollection));
            fc = (FilterCollection)reader.Deserialize(file);
            file.Close();
            var fff = fc[fName];
            if (fff == null)
            {
                fc.Add(new Filter(fName, ReportUtils.CreateListRFilter(fName)));
                StreamWriter wfile = new StreamWriter(string.Format("{0}\\Frm\\FReport\\Template\\ReportFilter.xml", path));
                System.Xml.Serialization.XmlSerializer writer = new System.Xml.Serialization.XmlSerializer(typeof(FilterCollection));
                writer.Serialize(wfile, fc);
                wfile.Close();
            }
            IList<ReportFilter> outList = new List<ReportFilter>();
            foreach (ReportFilter rf in fc[fName].ListReportFilter)
            {
                var property = typeof(T).GetProperty(rf.Name);
                if (property != null)
                {
                    rf.FieldType = property.PropertyType;
                    //MSG.Warning(rf.FieldType.FullName);
                    outList.Add(rf);
                }
            }
            return outList;
        }
        /// <summary>
        /// Đăng ký Shortcut Keys cho Form
        /// </summary>
        /// <param name="message"></param>
        /// <param name="keys"></param>
        /// <returns></returns>
        protected override bool ProcessCmdKey(ref Message message, Keys keys)
        {
            if (Utils.isDemo) return true;
            switch (keys)
            {
                case Keys.Control | Keys.P:
                    {//In

                        PrintReport();
                        return true;
                    }
            }
            return base.ProcessCmdKey(ref message, keys);
        }

        void ReportForm_Executing(object sender, PerpetuumSoft.Reporting.Windows.Forms.ExecutingEventArgs e)
        {
            e.Handled = true;
            // my print handler 
            PrintReport();
        }
        public void PrintReport()
        {
            if (!Authenticate.Permissions("PR", _subSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            System.Drawing.Printing.PrinterSettings printerSettings = new System.Drawing.Printing.PrinterSettings();
            printerSettings.DefaultPageSettings.PaperSize = ConstFrm.Paper.PaperSize(DefaultPaperSize);

            using (PrintDialog printDialog = new PrintDialog())
            {
                printDialog.UseEXDialog = true;
                printDialog.PrinterSettings = printerSettings;
                if (printDialog.ShowDialog() == DialogResult.OK)
                {
                    using (PerpetuumSoft.Reporting.Components.ReportPrintDocument printDoc = new PerpetuumSoft.Reporting.Components.ReportPrintDocument())
                    {
                        printDoc.Source = ReportViewer.Source;
                        printDoc.PrinterSettings = printDialog.PrinterSettings;
                        printDoc.Print();
                    }
                }
            }
        }

        private void ReportForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            ReportViewer.Source.HyperlinkClick -= new PerpetuumSoft.Reporting.Components.HyperlinkEventHandler(HyperlinkClick);
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }

        private void ultraButton1_Click(object sender, EventArgs e)//trungnq  luồng code : xuất ra file excel, đọc lại file excel, remove hyperlink, duyệt các cell , lưu lại vị trí các cell phải format có tên trong list, foreach trong list lưu lại vị trí cột, set format định dạng của cả cột trong excel, nhưng khi xuất excel bị gộp cột, nên phải gom nhóm thành các range và set cả format cho range đã gom nhóm
        {// bất cứ thay đổi vị trí nào ở tem in sharpshooter đều ảnh hưởng đến hàm này
            
            string tenFile = this.Text.Replace(" - Preview report", "");
            tenFile = tenFile.Replace(" ", "");
            List<string> lstCheckTenCotFormatTien = new List<string> { "Nợ", "Có", "Số đầu kỳ", "Số cuối kỳ", "Số kỳ này", "Số kỳ trước", "Số tồn, Thu", "Chi", "Tồn", "Số dư đầu kỳ", "Phát sinh nợ", "Phát sinh có", "Số dư cuối kỳ", "Thành tiền", "Thuế", "Khác", "Tổng doanh thu", "Doanh thu hàng hóa", "Doanh thu thành phẩm", "Doanh thu dịch vụ", "Doanh thu khác", "Chiết khấu", "Giá trị trả lại", "Giá trị giảm giá", "Doanh thu thuần", "Số tiền", "Tiền thuế GTGT", "Trả Lại/giảm giá", "Số đã thu", "Số còn phải thu", "Doanh số bán", "Giảm giá", "Trả lại", "Giá vốn", "Lãi - Lỗ", "Còn lại", "Đã thanh toán", "Giá trị hàng", "Giá trị HHDV", "Tồn đầu kỳ", "Nhập trong kỳ", "Xuất trong kỳ", "Tồn cuối kỳ", "Nguyên giá", "Giá trị hao mòn", "Cộng dồn", "Nguyên giá TSCĐ", "Mức khấu hao", "Khấu hao lũy kế", "Quy đổi", "Phát sinh Nợ", "Phát sinh Có", "Số phát sinh", "Lũy kế cuối kỳ", "Thu nhập chịu thuế", "Bảo hiểm được trừ", "Giảm trừ gia cảnh", "Thu nhập tính thuế TNCN", "Thuế TNCN đã khấu trừ", "Thực lĩnh", "Tổng số tiền", "Nguyên vật liệu trực tiếp", "Nhân công trực tiếp", "Chi phí sản xuất chung", "Doanh thu", "Giảm trừ", "NVL trực tiếp", "NC trực tiếp", "CPSX chung", "Cộng", "Giá vốn", "Lãi_Lỗ", "Dở dang", "Chi phí phát sinh", "Giảm trừ doanh thu", "Doanh số đã thực hiện", "Doanh số chưa thực hiện", "Giá trị đặt mua theo hợp đồng (chưa VAT)","Tỷ giá hối đoái" };
            List<string> lstCheckTenCotFormatSoLuong = new List<string> { "Số lượng", "Đơn giá", "Số lượng trả lại", "Nhập", "Xuất", "Tồn", "Số lượng đã nhận", "Số lượng còn lại", "Số lượng đã sử dụng","Tổng số", "Yêu cầu", "Thực xuất" };
            List<string> lstCheckTenCotHyperLink = new List<string> { "Số hiệu", "Số chứng từ", "Số CT", "Nhập", "Xuất", "Số hiệu chứng từ" };
            List<string> lstCheckTenCotSoChungTu = new List<string> { "Từ số", "Đến số", "Số" };
            SaveFileDialog save = new SaveFileDialog
            {
                Title = "Chọn nơi sao lưu ",
                Filter = "File (*.xls)|*.xls",
                InitialDirectory = "@C:\\",
                FileName = tenFile + ".xls"
            };
            if (save.ShowDialog() == DialogResult.OK)
            {
                string duongdan = save.FileName;
                try
                {
                    WaitingFrm.StartWaiting();
                    ExportFilter filter = new ExcelExportFilter();

                    //XLWorkbook wk = new XLWorkbook(a);
                    ////wk.Save();
                    ////if (File.Exists(duongdan))
                    ////{
                    ////    File.Delete(duongdan);
                    ////}
                    //IXLWorksheet xLWorksheet;

                    //IXLCell xLCell;
                    ////  wk.SaveAs("C:\\Users\\Admin\\Desktop\\hfghgfhgfhghgfhgf.xlsx");
                    //xLWorksheet = wk.Worksheet("Sheet1");
                    //IXLColumn xLColumn=xLWorksheet.Column(1);
                    //xLColumn.Style.NumberFormat.Format = "";
                    //xLColumn.Hyperlinks.Delete(allof);
                    //foreach( var uk in xLWorksheet.Hyperlinks)
                    //{
                    //    uk.Delete();
                    //}
                    
                        filter.Export(rSlot.RenderDocument(), duongdan, false);
                    

                    
                    Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
                    Microsoft.Office.Interop.Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(duongdan, System.Type.Missing, false, System.Type.Missing, "", "", true, System.Type.Missing, System.Type.Missing, true, false, System.Type.Missing, System.Type.Missing, System.Type.Missing, System.Type.Missing);
                    Microsoft.Office.Interop.Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[1];
                    xlWorksheet.Name = "sheet1";
                    Microsoft.Office.Interop.Excel.Range xlRange1 =null;
                    Microsoft.Office.Interop.Excel.Range xlRange2=null;
                    Microsoft.Office.Interop.Excel.Range xlRange3=null;
                    Microsoft.Office.Interop.Excel.Range xlRange4 = null;
                    Microsoft.Office.Interop.Excel.Range xlRange5 = null;
                    Microsoft.Office.Interop.Excel.Range xlRange6 = null;
                    Microsoft.Office.Interop.Excel.Range xlRange7 = null;
                    Microsoft.Office.Interop.Excel.Range xlRange8 = null;
                    Microsoft.Office.Interop.Excel.Range xlRange9 = null;
                    Microsoft.Office.Interop.Excel.Range xlRange10 = null;
                    Microsoft.Office.Interop.Excel.Range xlRange11 = null;
                    Microsoft.Office.Interop.Excel.Range xlRange = xlWorksheet.UsedRange;
                    xlWorksheet.Hyperlinks.Delete();
                    //Microsoft.Office.Interop.Excel.WorksheetCell wsCell = null;
                    //WorksheetRow wsRow = null;
                    decimal CheckValue = 0;
                    int rowCount = xlRange.Rows.Count;
                    int colCount = xlRange.Columns.Count;
                    List<int> listFormatSoLuong = new List<int> { };
                    List<int> listFormatTien = new List<int> { };
                    List<int> listXoaHyperLink = new List<int> { };
                    List<int> listFormatSoChungTu = new List<int> { };
                    List<int> listHopNhat = new List<int> { };
                    int checkRowStop = 0;
                    int checkStop = 0;
                    string str = "";
                    //int checkStop = 0;
                    for (int i = 1; i <= rowCount; i++)
                    {
                        for (int j = 1; j <= colCount; j++)
                        {

                            
                            if ((xlRange.Cells[i, j] as Microsoft.Office.Interop.Excel.Range).Text!=null)
                            {
                                if ((xlRange.Cells[i, j] as Microsoft.Office.Interop.Excel.Range).Text != "")
                                {
                                    if (lstCheckTenCotFormatSoLuong.Contains((xlRange.Cells[i, j] as Microsoft.Office.Interop.Excel.Range).Text))
                                    {
                                        //(xlRange.Cells[i, j] as Microsoft.Office.Interop.Excel.Range).EntireColumn["j:j+1"].NumberFormat = "###,###,###,###,###.#0;(###,###,###,###,###.#0)";
                                        if (checkRowStop == 0)
                                        {
                                            checkRowStop = i;
                                        }
                                        listFormatSoLuong.Add(j);
                                    }
                                    else if (lstCheckTenCotFormatTien.Contains((xlRange.Cells[i, j] as Microsoft.Office.Interop.Excel.Range).Text))
                                    {
                                        //(xlRange.Cells[i, j] as Microsoft.Office.Interop.Excel.Range).EntireColumn.NumberFormat = "###,###,###,###,##0;(###,###,###,###,##0)";
                                        //(xlRange.Cells[i, j] as Microsoft.Office.Interop.Excel.Range).Columns.NumberFormat = "###,###,###,###,##0;(###,###,###,###,##0)";
                                        
                                        if (checkRowStop == 0)
                                        {
                                            checkRowStop = i;
                                        }
                                        listFormatTien.Add(j);
                                    }
                                    else if (lstCheckTenCotSoChungTu.Contains((xlRange.Cells[i, j] as Microsoft.Office.Interop.Excel.Range).Text))
                                    {
                                        //(xlRange.Cells[i, j] as Microsoft.Office.Interop.Excel.Range).EntireColumn.NumberFormat = "###,###,###,###,##0;(###,###,###,###,##0)";
                                        //(xlRange.Cells[i, j] as Microsoft.Office.Interop.Excel.Range).Columns.NumberFormat = "###,###,###,###,##0;(###,###,###,###,##0)";

                                        if (checkRowStop == 0)
                                        {
                                            checkRowStop = i;
                                        }
                                        listFormatSoChungTu.Add(j);
                                    }
                                    
                                }
                            }
                            if (tenFile == "TÌNHHÌNHSỬDỤNGHÓAĐƠN")
                            {
                                if (i == checkRowStop + 3 && checkRowStop != 0)
                                {
                                    checkStop = 1;
                                }
                            }
                            else
                            {
                                if (i == checkRowStop + 1 && checkRowStop != 0)
                                {
                                    checkStop = 1;
                                }
                            }
                           


                        }
                        if (checkStop == 1)
                        {
                            listHopNhat = HopNhat3List(listFormatSoLuong, listFormatTien, listFormatSoChungTu);
                            if (listHopNhat.Count != 0)
                            {

                                foreach (var item in listHopNhat)
                                {
                                    if (listFormatSoLuong.Contains(item))
                                    {
                                        if (tenFile == "TÌNHHÌNHSỬDỤNGHÓAĐƠN")
                                        {
                                            //xlRange1 = xlWorksheet.Columns[item];
                                            //xlRange2 = xlWorksheet.Columns[item + 1];
                                            //xlRange3 = xlWorksheet.get_Range(xlRange1, xlRange2);

                                            //xlRange3.NumberFormat = "###.###.###.###.###.###.###.##0;(###.###.###.###.###.###.###.##0)";
                                        }else
                                        {
                                            xlRange1 = xlWorksheet.Columns[item];
                                            xlRange2 = xlWorksheet.Columns[item + 1];
                                            xlRange3 = xlWorksheet.get_Range(xlRange1, xlRange2);

                                            xlRange3.NumberFormat = "###,###,###,###,###,###,###,###.#0;(###,###,###,###,###,###,###,###.#0)";
                                        }
                                    }
                                    else if (listFormatTien.Contains(item))
                                    {
                                        if (tenFile == "TÌNHHÌNHSỬDỤNGHÓAĐƠN")
                                        {
                                            //xlRange1 = xlWorksheet.Columns[item];
                                            //xlRange2 = xlWorksheet.Columns[item + 1];
                                            //xlRange3 = xlWorksheet.get_Range(xlRange1, xlRange2);

                                            //xlRange3.NumberFormat = "###.###.###.###.###.###.###.##0;(###.###.###.###.###.###.###.##0)";
                                        }
                                        else
                                        {
                                            xlRange1 = xlWorksheet.Columns[item];
                                            xlRange2 = xlWorksheet.Columns[item + 1];
                                            xlRange3 = xlWorksheet.get_Range(xlRange1, xlRange2);

                                            xlRange3.NumberFormat = "###,###,###,###,###,###,###,##0;(###,###,###,###,###,###,###,##0)";
                                        }
                                    }
                                    else if (listFormatSoChungTu.Contains(item))
                                    {
                                        if (tenFile == "TÌNHHÌNHSỬDỤNGHÓAĐƠN")
                                        {
                                            xlRange1 = xlWorksheet.Columns[item];


                                            xlRange1.NumberFormat = "0000000";
                                        }
                                    }
                                    if (tenFile == "Sổchitiếtbánhàng")
                                    {
                                        if (item == 15)
                                        {
                                            xlRange1 = xlWorksheet.Columns[item];
                                            xlRange2 = xlWorksheet.Columns[item + 1];
                                            xlRange3 = xlWorksheet.Columns[item + 2];
                                            xlRange4 = xlWorksheet.get_Range(xlRange1, xlRange2);
                                            xlRange5 = xlWorksheet.get_Range(xlRange3, xlRange4);
                                            xlRange5.NumberFormat = "###,###,###,###,###,###,###,###.#0;(###,###,###,###,###,###,###,###.#0)";
                                        }
                                        else if (item == 19)
                                        {
                                            xlRange1 = xlWorksheet.Columns[item];
                                            xlRange2 = xlWorksheet.Columns[item + 1];
                                            xlRange3 = xlWorksheet.Columns[item + 2];
                                            xlRange4 = xlWorksheet.Columns[item + 3];
                                            xlRange5 = xlWorksheet.Columns[item + 4];
                                            xlRange6 = xlWorksheet.Columns[item + 5];
                                            xlRange7 = xlWorksheet.get_Range(xlRange1, xlRange2);
                                            xlRange8 = xlWorksheet.get_Range(xlRange3, xlRange4);
                                            xlRange9 = xlWorksheet.get_Range(xlRange5, xlRange6);
                                            xlRange10 = xlWorksheet.get_Range(xlRange8, xlRange7);
                                            xlRange11 = xlWorksheet.get_Range(xlRange9, xlRange10);
                                            xlRange11.NumberFormat = "###,###,###,###,###,###,###,##0;(###,###,###,###,###,###,###,##0)";
                                        }else if ( item == 25)
                                        {
                                            xlRange1 = xlWorksheet.Columns[item];
                                            xlRange2 = xlWorksheet.Columns[item + 1];
                                            xlRange3 = xlWorksheet.Columns[item + 2];
                                            xlRange4 = xlWorksheet.get_Range(xlRange1, xlRange2);
                                            xlRange5 = xlWorksheet.get_Range(xlRange3, xlRange4);
                                            xlRange5.NumberFormat = "###,###,###,###,###,###,###,##0;(###,###,###,###,###,###,###,##0)";
                                        }
                                    }
                                    else if (tenFile == "Sổchitiếtphảithukháchhàngtheomặthàng")
                                    {
                                        if (item == 15||item==18||item==23)
                                        {
                                            xlRange1 = xlWorksheet.Columns[item];
                                            xlRange2 = xlWorksheet.Columns[item + 1];
                                            xlRange3 = xlWorksheet.Columns[item + 2];
                                            xlRange4 = xlWorksheet.Columns[item + 3];
                                            xlRange5 = xlWorksheet.Columns[item + 4];
                                            xlRange6 = xlWorksheet.Columns[item + 5];
                                            xlRange7 = xlWorksheet.get_Range(xlRange1, xlRange2);
                                            xlRange8 = xlWorksheet.get_Range(xlRange3, xlRange4);
                                            xlRange9 = xlWorksheet.get_Range(xlRange5, xlRange6);
                                            xlRange10 = xlWorksheet.get_Range(xlRange8, xlRange7);
                                            xlRange11 = xlWorksheet.get_Range(xlRange9, xlRange10);
                                            xlRange11.NumberFormat = "###,###,###,###,###,###,###,##0;(###,###,###,###,###,###,###,##0)";
                                        }

                                    }
                                    else if (tenFile == "Sổchitiếtvậtliệu,dụngcụ(sảnphẩm,hànghóa)")
                                    {
                                        if (item == 17)
                                        {
                                            xlRange1 = xlWorksheet.Columns[item];
                                            xlRange2 = xlWorksheet.Columns[item + 1];
                                            xlRange3 = xlWorksheet.Columns[item + 2];
                                            xlRange4 = xlWorksheet.Columns[item + 3];

                                            xlRange5 = xlWorksheet.get_Range(xlRange1, xlRange2);
                                            xlRange6 = xlWorksheet.get_Range(xlRange3, xlRange4);
                                            xlRange7 = xlWorksheet.get_Range(xlRange6, xlRange5);

                                            xlRange7.NumberFormat = "###,###,###,###,###,###,###,##0;(###,###,###,###,###,###,###,##0)";
                                        }
                                    }
                                    else if (tenFile == "Sổtàisảncốđịnh")
                                    {
                                        if (item == 21)
                                        {
                                            xlRange1 = xlWorksheet.Columns[item];
                                            xlRange2 = xlWorksheet.Columns[item + 1];
                                            xlRange3 = xlWorksheet.Columns[item + 2];
                                            xlRange4 = xlWorksheet.Columns[item + 3];

                                            xlRange7 = xlWorksheet.get_Range(xlRange1, xlRange2);
                                            xlRange8 = xlWorksheet.get_Range(xlRange3, xlRange4);
                                            xlRange9 = xlWorksheet.get_Range(xlRange7, xlRange8);

                                            xlRange9.NumberFormat = "###,###,###,###,###,###,###,##0;(###,###,###,###,###,###,###,##0)";
                                        }
                                        else if (item == 18)
                                        {
                                            xlRange1 = xlWorksheet.Columns[item];
                                            xlRange2 = xlWorksheet.Columns[item + 1];
                                            xlRange3 = xlWorksheet.Columns[item + 2];
                                            xlRange4 = xlWorksheet.Columns[item + 3];

                                            xlRange7 = xlWorksheet.get_Range(xlRange1, xlRange2);
                                            xlRange8 = xlWorksheet.get_Range(xlRange3, xlRange4);
                                            xlRange9 = xlWorksheet.get_Range(xlRange7, xlRange8);

                                            xlRange9.NumberFormat = "###,###,###,###,###,###,###,##0;(###,###,###,###,###,###,###,##0)";
                                        }

                                    }
                                    else if (tenFile == "Sổcái(DùngchohìnhthứckếtoánChứngtừghisổ)")
                                    {
                                        if (item == 9)
                                        {
                                            xlRange1 = xlWorksheet.Columns[item];
                                            xlRange2 = xlWorksheet.Columns[item + 1];
                                            xlRange3 = xlWorksheet.Columns[item + 2];
                                            xlRange4 = xlWorksheet.get_Range(xlRange1, xlRange2);
                                            xlRange5 = xlWorksheet.get_Range(xlRange3, xlRange4);
                                            xlRange5.NumberFormat = "###,###,###,###,###,###,###,##0;(###,###,###,###,###,###,###,##0)";
                                        }
                                    }
                                    else if (tenFile == "Sổchitiếttiềnvay")
                                    {
                                        if (item == 14)
                                        {
                                            xlRange1 = xlWorksheet.Columns[item];
                                            xlRange2 = xlWorksheet.Columns[item + 1];
                                            xlRange3 = xlWorksheet.Columns[item + 2];
                                            xlRange4 = xlWorksheet.get_Range(xlRange1, xlRange2);
                                            xlRange5 = xlWorksheet.get_Range(xlRange3, xlRange4);
                                            xlRange5.NumberFormat = "###,###,###,###,###,###,###,##0;(###,###,###,###,###,###,###,##0)";
                                        }
                                    }
                                    else if (tenFile == "SoChitietThanhToan_S12")
                                    {
                                        if (item == 14)
                                        {
                                            xlRange1 = xlWorksheet.Columns[item];
                                            xlRange2 = xlWorksheet.Columns[item + 1];
                                            xlRange3 = xlWorksheet.Columns[item + 2];
                                            xlRange4 = xlWorksheet.Columns[item + 3];
                                            xlRange5 = xlWorksheet.Columns[item + 4];
                                            xlRange6 = xlWorksheet.Columns[item + 5];
                                            xlRange7 = xlWorksheet.get_Range(xlRange1, xlRange2);
                                            xlRange8 = xlWorksheet.get_Range(xlRange3, xlRange4);
                                            xlRange9 = xlWorksheet.get_Range(xlRange5, xlRange6);
                                            xlRange10 = xlWorksheet.get_Range(xlRange8, xlRange7);
                                            xlRange11 = xlWorksheet.get_Range(xlRange9, xlRange10);
                                            xlRange11.NumberFormat = "###,###,###,###,###,###,###,##0;(###,###,###,###,###,###,###,##0)";
                                        }
                                    }
                                    else if (tenFile == "Sốtheodõithanhtoánbằngngoạitệ")
                                    {
                                        if (item == 21)
                                        {
                                            xlRange1 = xlWorksheet.Columns[item];
                                            xlRange2 = xlWorksheet.Columns[item + 1];
                                            xlRange3 = xlWorksheet.Columns[item + 2];
                                            xlRange4 = xlWorksheet.get_Range(xlRange1, xlRange2);
                                            xlRange5 = xlWorksheet.get_Range(xlRange3, xlRange4);
                                            xlRange5.NumberFormat = "###,###,###,###,###,###,###,##0;(###,###,###,###,###,###,###,##0)";
                                        }
                                    }
                                    else if (tenFile == "Sổtheodõichitiếttheomãthốngkê")
                                    {
                                        if (item == 19)
                                        {
                                            xlRange1 = xlWorksheet.Columns[item];
                                            xlRange2 = xlWorksheet.Columns[item + 1];
                                            xlRange3 = xlWorksheet.Columns[item + 2];
                                            xlRange4 = xlWorksheet.get_Range(xlRange1, xlRange2);
                                            xlRange5 = xlWorksheet.get_Range(xlRange3, xlRange4);
                                            xlRange5.NumberFormat = "###,###,###,###,###,###,###,##0;(###,###,###,###,###,###,###,##0)";
                                        }
                                    }
                                    else if (tenFile == "Sổtheodõichitiếttheođốitượngtậphợpchiphí")
                                    {
                                        if (item == 20)
                                        {
                                            xlRange1 = xlWorksheet.Columns[item];
                                            xlRange2 = xlWorksheet.Columns[item + 1];
                                            xlRange3 = xlWorksheet.Columns[item + 2];
                                            xlRange4 = xlWorksheet.get_Range(xlRange1, xlRange2);
                                            xlRange5 = xlWorksheet.get_Range(xlRange3, xlRange4);
                                            xlRange5.NumberFormat = "###,###,###,###,###,###,###,##0;(###,###,###,###,###,###,###,##0)";
                                        }
                                    }
                                    else if (tenFile == "SoTheoDoiDoiTuongTHCP")
                                    {
                                        if (item == 12)
                                        {
                                            xlRange1 = xlWorksheet.Columns[item];
                                            xlRange2 = xlWorksheet.Columns[item + 1];
                                            xlRange3 = xlWorksheet.Columns[item + 2];
                                            xlRange4 = xlWorksheet.Columns[item + 3];
                                            xlRange5 = xlWorksheet.Columns[item + 4];
                                            xlRange6 = xlWorksheet.Columns[item + 5];
                                            xlRange7 = xlWorksheet.get_Range(xlRange1, xlRange2);
                                            xlRange8 = xlWorksheet.get_Range(xlRange3, xlRange4);
                                            xlRange9 = xlWorksheet.get_Range(xlRange5, xlRange6);
                                            xlRange10 = xlWorksheet.get_Range(xlRange8, xlRange7);
                                            xlRange11 = xlWorksheet.get_Range(xlRange9, xlRange10);
                                            xlRange11.NumberFormat = "###,###,###,###,###,###,###,##0;(###,###,###,###,###,###,###,##0)";
                                        }
                                    }
                                    else if (tenFile == "TheTinhGiaThanhSanPhamDichVu")
                                    {
                                        if (item == 8||item==12||item==18||item==21)
                                        {
                                            xlRange1 = xlWorksheet.Columns[item];
                                            xlRange2 = xlWorksheet.Columns[item + 1];
                                            xlRange3 = xlWorksheet.Columns[item + 2];
                                            xlRange4 = xlWorksheet.Columns[item + 3];
                                            xlRange5 = xlWorksheet.Columns[item + 4];
                                            xlRange6 = xlWorksheet.Columns[item + 5];
                                            xlRange7 = xlWorksheet.get_Range(xlRange1, xlRange2);
                                            xlRange8 = xlWorksheet.get_Range(xlRange3, xlRange4);
                                            xlRange9 = xlWorksheet.get_Range(xlRange5, xlRange6);
                                            xlRange10 = xlWorksheet.get_Range(xlRange8, xlRange7);
                                            xlRange11 = xlWorksheet.get_Range(xlRange9, xlRange10);
                                            xlRange11.NumberFormat = "###,###,###,###,###,###,###,##0;(###,###,###,###,###,###,###,##0)";
                                        }
                                    }
                                    else if (tenFile == "TÌNHHÌNHSỬDỤNGHÓAĐƠN")
                                    {
                                        if (item == 25)
                                        {
                                            //xlRange1 = xlWorksheet.Columns[item];
                                            //xlRange2 = xlWorksheet.Columns[item + 1];
                                            //xlRange3 = xlWorksheet.Columns[item + 2];
                                            //xlRange4 = xlWorksheet.get_Range(xlRange1, xlRange2);
                                            //xlRange5 = xlWorksheet.get_Range(xlRange3, xlRange4);
                                            //xlRange5.NumberFormat = "###.###.###.###.###.###.###.##0;(###.###.###.###.###.###.###.##0)";
                                        }else if (item == 23)
                                        {
                                            xlRange1 = xlWorksheet.Columns[item];
                                            
                                            xlRange1.NumberFormat = "0000000";
                                        }
                                    }
                                    else if (tenFile == "Phiếuxuấtkho")
                                    {
                                        if (item == 19)
                                        {
                                            xlRange1 = xlWorksheet.Columns[item];
                                            xlRange2 = xlWorksheet.Columns[item + 1];
                                            xlRange3 = xlWorksheet.Columns[item + 2];
                                            xlRange3.NumberFormat = "###,###,###,###,###,###,###,###.#0;(###,###,###,###,###,###,###,###.#0)";
                                        }
                                        else if (item == 21)
                                        {
                                            xlRange1 = xlWorksheet.Columns[item];
                                            xlRange2 = xlWorksheet.Columns[item + 1];
                                            xlRange3 = xlWorksheet.Columns[item + 2];
                                            xlRange4 = xlWorksheet.Columns[item + 3];
                                            xlRange5 = xlWorksheet.Columns[item + 4];
                                            xlRange6 = xlWorksheet.Columns[item + 5];
                                            xlRange7 = xlWorksheet.get_Range(xlRange1, xlRange2);
                                            xlRange8 = xlWorksheet.get_Range(xlRange3, xlRange4);
                                            xlRange9 = xlWorksheet.get_Range(xlRange5, xlRange6);
                                            xlRange10 = xlWorksheet.get_Range(xlRange8, xlRange7);
                                            xlRange11 = xlWorksheet.get_Range(xlRange9, xlRange10);
                                            xlRange11.NumberFormat = "###,###,###,###,###,###,###,###.#0;(###,###,###,###,###,###,###,###.#0)";
                                        }
                                        else if (item == 27)
                                        {
                                            xlRange1 = xlWorksheet.Columns[item];
                                            xlRange2 = xlWorksheet.Columns[item + 1];
                                            xlRange3 = xlWorksheet.Columns[item + 2];
                                            xlRange4 = xlWorksheet.Columns[item + 3];
                                            xlRange5 = xlWorksheet.Columns[item + 4];
                                            
                                            xlRange7 = xlWorksheet.get_Range(xlRange1, xlRange2);
                                            xlRange8 = xlWorksheet.get_Range(xlRange3, xlRange4);
                                            
                                            xlRange10 = xlWorksheet.get_Range(xlRange8, xlRange7);
                                            xlRange11 = xlWorksheet.get_Range(xlRange5, xlRange10);
                                            xlRange11.NumberFormat = "###,###,###,###,###,###,###,###.#0;(###,###,###,###,###,###,###,###.#0)";
                                        }
                                        else if (item == 32)
                                        {
                                            xlRange1 = xlWorksheet.Columns[item];
                                            xlRange2 = xlWorksheet.Columns[item + 1];
                                            xlRange3 = xlWorksheet.Columns[item + 2];
                                            xlRange7 = xlWorksheet.get_Range(xlRange1, xlRange2);
                                            xlRange8 = xlWorksheet.get_Range(xlRange3, xlRange7);
                                            xlRange8.NumberFormat = "###,###,###,###,###,###,###,##0;(###,###,###,###,###,###,###,##0)";
                                        }
                                    }
                                    else if (tenFile == "Phiếuxuấtkhobánhàng")
                                    {
                                        if (item == 21)
                                        {
                                            xlRange1 = xlWorksheet.Columns[item];
                                            xlRange2 = xlWorksheet.Columns[item + 1];
                                            xlRange3 = xlWorksheet.Columns[item + 2];
                                            xlRange3.NumberFormat = "###,###,###,###,###,###,###,###.#0;(###,###,###,###,###,###,###,###.#0)";
                                        }
                                        else if (item == 23)
                                        {
                                            xlRange1 = xlWorksheet.Columns[item];
                                            xlRange2 = xlWorksheet.Columns[item + 1];
                                            xlRange3 = xlWorksheet.Columns[item + 2];
                                            xlRange4 = xlWorksheet.Columns[item + 3];
                                           
                                            xlRange7 = xlWorksheet.get_Range(xlRange1, xlRange2);
                                            xlRange8 = xlWorksheet.get_Range(xlRange3, xlRange4);
                                            
                                            xlRange10 = xlWorksheet.get_Range(xlRange8, xlRange7);
                                          
                                            xlRange10.NumberFormat = "###,###,###,###,###,###,###,###.#0;(###,###,###,###,###,###,###,###.#0)";
                                        }
                                        else if (item == 27)
                                        {
                                            xlRange1 = xlWorksheet.Columns[item];
                                            xlRange2 = xlWorksheet.Columns[item + 1];
                                            xlRange3 = xlWorksheet.Columns[item + 2];
                                            xlRange4 = xlWorksheet.Columns[item + 3];
                                            xlRange5 = xlWorksheet.Columns[item + 4];

                                            xlRange7 = xlWorksheet.get_Range(xlRange1, xlRange2);
                                            xlRange8 = xlWorksheet.get_Range(xlRange3, xlRange4);

                                            xlRange10 = xlWorksheet.get_Range(xlRange8, xlRange7);
                                            xlRange11 = xlWorksheet.get_Range(xlRange5, xlRange10);
                                            xlRange11.NumberFormat = "###,###,###,###,###,###,###,###.#0;(###,###,###,###,###,###,###,###.#0)";
                                        }
                                        else if (item == 32)
                                        {
                                            xlRange1 = xlWorksheet.Columns[item -1];
                                            xlRange5 = xlWorksheet.Columns[item - 2];
                                            xlRange4 = xlWorksheet.Columns[item ];
                                            xlRange2 = xlWorksheet.Columns[item + 1];
                                            xlRange3 = xlWorksheet.Columns[item + 2];
                                            xlRange6 = xlWorksheet.Columns[item + 3];
                                            xlRange7 = xlWorksheet.get_Range(xlRange1, xlRange2);
                                            xlRange8 = xlWorksheet.get_Range(xlRange3, xlRange4);
                                            xlRange9 = xlWorksheet.get_Range(xlRange5, xlRange6);
                                            xlRange10 = xlWorksheet.get_Range(xlRange8, xlRange7);
                                            xlRange11 = xlWorksheet.get_Range(xlRange9, xlRange10);
                                            xlRange11.NumberFormat = "###,###,###,###,###,###,###,##0;(###,###,###,###,###,###,###,##0)";
                                        }
                                    }
                                    

                                }

                                

                            }
                        }
                        if (checkStop == 1)
                        {
                            break;
                        }
                    }
                    xlWorkbook.Save();
                    //xlWorkbook.Save();//Close(1,duongdan,Missing.Value);//khó


                    GC.Collect();
                    GC.WaitForPendingFinalizers();

                    //rule of thumb for releasing com objects:
                    //  never use two dots, all COM objects must be referenced and released individually
                    //  ex: [somthing].[something].[something] is bad

                    //release com objects to fully kill excel process from running in the background
                    Marshal.ReleaseComObject(xlRange);
                    if (xlRange1 != null)
                    {
                        Marshal.ReleaseComObject(xlRange1);
                    }
                    if (xlRange2 != null)
                    {
                        Marshal.ReleaseComObject(xlRange2);
                    }
                    if (xlRange3 != null)
                    {
                        Marshal.ReleaseComObject(xlRange3);
                    }
                    if (xlRange4 != null)
                    {
                        Marshal.ReleaseComObject(xlRange4);
                    }
                    if (xlRange5 != null)
                    {
                        Marshal.ReleaseComObject(xlRange5);
                    }
                    if (xlRange6 != null)
                    {
                        Marshal.ReleaseComObject(xlRange6);
                    }
                    if (xlRange7 != null)
                    {
                        Marshal.ReleaseComObject(xlRange7);
                    }
                    if (xlRange8 != null)
                    {
                        Marshal.ReleaseComObject(xlRange8);
                    }
                    if (xlRange9 != null)
                    {
                        Marshal.ReleaseComObject(xlRange9);
                    }
                    if (xlRange10 != null)
                    {
                        Marshal.ReleaseComObject(xlRange10);
                    }
                    if (xlRange11 != null)
                    {
                        Marshal.ReleaseComObject(xlRange11);
                    }
                    
                        Marshal.ReleaseComObject(xlWorksheet);
                    
                    //close and release
                    xlWorkbook.Close();
                    Marshal.ReleaseComObject(xlWorkbook);

                    //quit and release
                    xlApp.Quit();
                    Marshal.ReleaseComObject(xlApp);
                    WaitingFrm.StopWaiting();
                    if (MSG.Question("Kết xuất thành công, Bạn có muốn mở file vừa kết xuất không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(save.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
                catch (Exception es)
                {
                    MSG.Warning(es.Message+es.Source+es.HResult.ToString());
                    WaitingFrm.StopWaiting();
                    
                }
            }


        }
        
        private List<int> HopNhat3List(List<int> lista, List<int> listb, List<int> listc)
        {
            List<int> listhopnhat = new List<int> { };
            foreach (var item in lista)
            {
                if (!listhopnhat.Contains(item))
                {
                    listhopnhat.Add(item);
                }
            }
            foreach (var item in listb)
            {
                if (!listhopnhat.Contains(item))
                {
                    listhopnhat.Add(item);
                }
            }
            foreach (var item in listc)
            {
                if (!listhopnhat.Contains(item))
                {
                    listhopnhat.Add(item);
                }
            }
            return listhopnhat;
        }
        private void toolStrip1_Click(object sender, EventArgs e)
        {
            
        }

        private void ReportForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            rSlot.Manager.DataSources = new ObjectPointerCollection();
        }
    }
}
