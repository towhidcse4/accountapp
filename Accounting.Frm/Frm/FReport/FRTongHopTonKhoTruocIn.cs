﻿using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRTongHopTonKhoTruocIn : Form
    {
        List<TongHopTonKho> data = new List<TongHopTonKho>();
        List<TongHopTonKhoDetail> dataDetail = new List<TongHopTonKhoDetail>();
        string tieude1;
        string _subSystemCode1;
        public FRTongHopTonKhoTruocIn(List<TongHopTonKho> data1, string tieude, string _subSystemCode)
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();
            data = data1;
            XuLiListLenGrid(data1, dataDetail);
            dataDetail = dataDetail.OrderBy(n => n.RepositoryCode).ThenBy(n => n.Ordercode).ThenBy(n=>n.MaterialGoodCode).ToList();
            tieude1 = tieude;
            _subSystemCode1 = _subSystemCode;
           
            //  data1.OrderBy(n => n.TongHopTonKhoDetails.OrderBy(t=>t.MaterialGoodName));
            //uGridDuLieu.DisplayLayout.Override.TipStyleCell = TipStyle.Show;
            //uGridDuLieu.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            //uGridDuLieu.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.ExtendFirstCell;
            //uGridDuLieu.DisplayLayout.Override.ActiveRowAppearance.ForeColor = Color.Black;
            //uGridDuLieu.DisplayLayout.Override.SelectedRowAppearance.ForeColor = Color.Black;
            //uGridDuLieu.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.False;
            //uGridDuLieu.DisplayLayout.Override.FilterUIType = FilterUIType.Default;
            ////tự thay đổi kích thước cột
            ////if (!isStandard) ultraGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            ////select cả hàng hay ko?
            //uGridDuLieu.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            //uGridDuLieu.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            ////Hiển thị SupportDataErrorInfo
            //uGridDuLieu.DisplayLayout.Override.SupportDataErrorInfo = SupportDataErrorInfo.CellsOnly;
            //uGridDuLieu.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            //uGridDuLieu.DisplayLayout.UseFixedHeaders = false;
            //uGridDuLieu.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            ////UltraGridBand band = uGridDuLieu.DisplayLayout.Bands[0];
            //uGridDuLieu.DisplayLayout.Bands[0].Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            

            //foreach (var item in data1)
            //{
            //    item.RepositoryName = string.Format("Kho: {0}",item.RepositoryName);
            //  item.TongHopTonKhoDetails=  item.TongHopTonKhoDetails.OrderBy(n => n.MaterialGoodCode).ToList();
                
            //}
            //data1 = data1.OrderBy(n => n.RepositoryCode).ToList();
            uGridDuLieu.DataSource = dataDetail;
            Utils.ConfigGrid(uGridDuLieu, ConstDatabase.TongHopTonKho_TableName);
            uGridDuLieu.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            //foreach (var item in uGridDuLieu.DisplayLayout.Rows)
            //{
            //    item.Expanded = true;

            //}
            //uGridDuLieu.DisplayLayout.Bands[1].Columns["MaterialGoodID"].Hidden = true;
            //uGridDuLieu.DisplayLayout.Bands[0].Columns["RepositoryCode"].Hidden = true;

            //uGridDuLieu.DisplayLayout.Bands[0].Columns["RepositoryName"].Header.Caption = "Mã hàng";
            //uGridDuLieu.DisplayLayout.Bands[0].Columns["SumSLDK"].Header.Caption = "Số lượng";
            //uGridDuLieu.DisplayLayout.Bands[0].Columns["SumSLIWGK"].Header.Caption = "Số lượng";
            //uGridDuLieu.DisplayLayout.Bands[0].Columns["SumSLOWGK"].Header.Caption = "Số lượng";
            //uGridDuLieu.DisplayLayout.Bands[0].Columns["SumSLCK"].Header.Caption = "Số lượng";
            //uGridDuLieu.DisplayLayout.Bands[0].Columns["SumDK"].Header.Caption = "Giá trị";
            //uGridDuLieu.DisplayLayout.Bands[0].Columns["SumIWGK"].Header.Caption = "Giá trị";
            //uGridDuLieu.DisplayLayout.Bands[0].Columns["SumOWGK"].Header.Caption = "Giá trị";
            //uGridDuLieu.DisplayLayout.Bands[0].Columns["SumCK"].Header.Caption = "Giá trị";

            //uGridDuLieu.DisplayLayout.Bands[1].Columns["MaterialGoodCode"].Header.Caption = "Mã hàng";
            //uGridDuLieu.DisplayLayout.Bands[1].Columns["MaterialGoodName"].Header.Caption = "Tên hàng";
            //uGridDuLieu.DisplayLayout.Bands[1].Columns["Unit"].Header.Caption = "ĐVT";
            //uGridDuLieu.DisplayLayout.Bands[1].Columns["SLDK"].Header.Caption = "Số lượng";
            //uGridDuLieu.DisplayLayout.Bands[1].Columns["SLIWGK"].Header.Caption = "Số lượng";
            //uGridDuLieu.DisplayLayout.Bands[1].Columns["SLOWGK"].Header.Caption = "Số lượng";
            //uGridDuLieu.DisplayLayout.Bands[1].Columns["SLCK"].Header.Caption = "Số lượng";
            //uGridDuLieu.DisplayLayout.Bands[1].Columns["DK"].Header.Caption = "Giá trị";
            //uGridDuLieu.DisplayLayout.Bands[1].Columns["IWGK"].Header.Caption = "Giá trị";
            //uGridDuLieu.DisplayLayout.Bands[1].Columns["OWGK"].Header.Caption = "Giá trị";
            //uGridDuLieu.DisplayLayout.Bands[1].Columns["CK"].Header.Caption = "Giá trị";





            uGridDuLieu.DisplayLayout.Bands[0].Columns["MaterialGoodCode"].Width = 81;
            uGridDuLieu.DisplayLayout.Bands[0].Columns["MaterialGoodName"].Width=68;
            uGridDuLieu.DisplayLayout.Bands[0].Columns["Unit"].Width=44;


            //uGridDuLieu.DisplayLayout.Bands[0].Columns["SumSLDK"].Format = "###,###,###,###,##0.00;(###,###,###,###,##0.00)";
            //uGridDuLieu.DisplayLayout.Bands[0].Columns["SumSLIWGK"].Format = "###,###,###,###,##0.00;(###,###,###,###,##0.00)";
            //uGridDuLieu.DisplayLayout.Bands[0].Columns["SumSLOWGK"].Format = "###,###,###,###,##0.00;(###,###,###,###,##0.00)";
            //uGridDuLieu.DisplayLayout.Bands[0].Columns["SumSLCK"].Format = "###,###,###,###,##0.00;(###,###,###,###,##0.00)";
            //uGridDuLieu.DisplayLayout.Bands[0].Columns["SumDK"].Format = "##,#";
            //uGridDuLieu.DisplayLayout.Bands[0].Columns["SumIWGK"].Format = "##,#";
            //uGridDuLieu.DisplayLayout.Bands[0].Columns["SumOWGK"].Format = "##,#";
            //uGridDuLieu.DisplayLayout.Bands[0].Columns["SumCK"].Format = "##,#";

            uGridDuLieu.DisplayLayout.Bands[0].Columns["SLDK"].Format = "###,###,###,###,##0.00;(###,###,###,###,##0.00)";
            uGridDuLieu.DisplayLayout.Bands[0].Columns["SLIWGK"].Format = "###,###,###,###,##0.00;(###,###,###,###,##0.00)";
            uGridDuLieu.DisplayLayout.Bands[0].Columns["SLOWGK"].Format = "###,###,###,###,##0.00;(###,###,###,###,##0.00)";
            uGridDuLieu.DisplayLayout.Bands[0].Columns["SLCK"].Format = "###,###,###,###,##0.00;(###,###,###,###,##0.00)";
            uGridDuLieu.DisplayLayout.Bands[0].Columns["DK"].Format = "##,#";
            uGridDuLieu.DisplayLayout.Bands[0].Columns["IWGK"].Format = "##,#";
            uGridDuLieu.DisplayLayout.Bands[0].Columns["OWGK"].Format = "##,#";
            uGridDuLieu.DisplayLayout.Bands[0].Columns["CK"].Format = "##,#";

            UltraGridBand parentBand = this.uGridDuLieu.DisplayLayout.Bands[0];
            parentBand.RowLayoutStyle = RowLayoutStyle.GroupLayout;
            parentBand.Override.HeaderPlacement = HeaderPlacement.FixedOnTop;

            UltraGridGroup parentBandGroupPeriod;
            if (parentBand.Groups.Exists("ParentBandGroupPeriod"))
            {
                parentBandGroupPeriod = parentBand.Groups["ParentBandGroupPeriod"];
                parentBandGroupPeriod.Header.Caption = tieude;
            }
            else
                parentBandGroupPeriod = parentBand.Groups.Add("ParentBandGroupPeriod", tieude);
            parentBandGroupPeriod.Header.Appearance.FontData.Italic = DefaultableBoolean.True;
            parentBandGroupPeriod.Header.Appearance.FontData.SizeInPoints = 10;
            parentBandGroupPeriod.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupPeriod.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPeriod.Header.Appearance.BorderColor = Color.Black;

            UltraGridGroup parentBandGroupTitle;
            if (parentBand.Groups.Exists("ParentBandGroupTitle"))
                parentBandGroupTitle = parentBand.Groups["ParentBandGroupTitle"];
            else
                parentBandGroupTitle = parentBand.Groups.Add("ParentBandGroupTitle", "TỔNG HỢP TỒN KHO");
            parentBandGroupTitle.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroupTitle.Header.Appearance.FontData.SizeInPoints = 12;
            parentBandGroupTitle.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupTitle.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupTitle.Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["MaterialGoodCode"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["MaterialGoodCode"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["MaterialGoodCode"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["MaterialGoodCode"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["MaterialGoodCode"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["MaterialGoodCode"].Header.Appearance.BorderColor = Color.Black;
            //uGridDuLieu.DisplayLayout.Bands[0].Columns["MaterialGoodCode"].RowLayoutColumnInfo.SpanX = 40;
            uGridDuLieu.DisplayLayout.Bands[0].Columns["MaterialGoodCode"].RowLayoutColumnInfo.SpanY = 64;
            uGridDuLieu.DisplayLayout.Bands[0].Columns["MaterialGoodCode"].RowLayoutColumnInfo.OriginX = 0;
            uGridDuLieu.DisplayLayout.Bands[0].Columns["MaterialGoodCode"].RowLayoutColumnInfo.OriginY = 0;

            parentBand.Columns["MaterialGoodName"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["MaterialGoodName"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["MaterialGoodName"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["MaterialGoodName"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["MaterialGoodName"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["MaterialGoodName"].Header.Appearance.BorderColor = Color.Black;
            //uGridDuLieu.DisplayLayout.Bands[0].Columns["MaterialGoodCode"].RowLayoutColumnInfo.SpanX = 40;
            uGridDuLieu.DisplayLayout.Bands[0].Columns["MaterialGoodName"].RowLayoutColumnInfo.SpanY = 64;
            uGridDuLieu.DisplayLayout.Bands[0].Columns["MaterialGoodName"].RowLayoutColumnInfo.OriginX = uGridDuLieu.DisplayLayout.Bands[0].Columns["MaterialGoodCode"].RowLayoutColumnInfo.OriginX + uGridDuLieu.DisplayLayout.Bands[0].Columns["MaterialGoodName"].RowLayoutColumnInfo.SpanX;
            uGridDuLieu.DisplayLayout.Bands[0].Columns["MaterialGoodName"].RowLayoutColumnInfo.OriginY = 0;

            parentBand.Columns["Unit"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["Unit"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Unit"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["Unit"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["Unit"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["Unit"].Header.Appearance.BorderColor = Color.Black;
            //uGridDuLieu.DisplayLayout.Bands[0].Columns["Unit"].RowLayoutColumnInfo.SpanX = 40;
            uGridDuLieu.DisplayLayout.Bands[0].Columns["Unit"].RowLayoutColumnInfo.SpanY = 64;
            uGridDuLieu.DisplayLayout.Bands[0].Columns["Unit"].RowLayoutColumnInfo.OriginX = uGridDuLieu.DisplayLayout.Bands[0].Columns["MaterialGoodName"].RowLayoutColumnInfo.OriginX + uGridDuLieu.DisplayLayout.Bands[0].Columns["Unit"].RowLayoutColumnInfo.SpanX;
            uGridDuLieu.DisplayLayout.Bands[0].Columns["Unit"].RowLayoutColumnInfo.OriginY = 0;


            //uGridDuLieu.DisplayLayout.Bands[0].Columns["RepositoryName"].RowLayoutColumnInfo.SpanX = 40;
            //uGridDuLieu.DisplayLayout.Bands[0].Columns["RepositoryName"].RowLayoutColumnInfo.SpanY = 32;
            //uGridDuLieu.DisplayLayout.Bands[0].Columns["RepositoryName"].RowLayoutColumnInfo.OriginX = 0;
            //uGridDuLieu.DisplayLayout.Bands[0].Columns["RepositoryName"].RowLayoutColumnInfo.OriginY = 0;

            //parentBand.Columns[11].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            //parentBand.Columns[11].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            //parentBand.Columns[11].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            //parentBand.Columns[11].Header.Appearance.ForeColor = Color.Black;
            //parentBand.Columns[11].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;     mới bỏ
            //parentBand.Columns[11].Header.Appearance.BorderColor = Color.Black;
            //parentBand.Columns[11].Width = 72;  
            //uGridDuLieu.DisplayLayout.Bands[0].Columns[11].RowLayoutColumnInfo.SpanX = 40;
            //uGridDuLieu.DisplayLayout.Bands[0].Columns[11].RowLayoutColumnInfo.SpanY = 32;
            //uGridDuLieu.DisplayLayout.Bands[0].Columns[11].RowLayoutColumnInfo.OriginX = 40;
            //uGridDuLieu.DisplayLayout.Bands[0].Columns[11].RowLayoutColumnInfo.OriginY = 0;

            //parentBand.Columns["RepositoryName"].RowLayoutColumnInfo.ParentGroup = parentBand.Columns[11].;
            //UltraGridGroup parentBandGroupTenHang;
            //if (parentBand.Groups.Exists("ParentBandGroupTenHang"))
            //    parentBandGroupTenHang = parentBand.Groups["ParentBandGroupTenHang"];
            //else
            //    parentBandGroupTenHang = parentBand.Groups.Add("ParentBandGroupTenHang", "Tên hàng  |\n        |");
            //parentBandGroupTenHang.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            //parentBandGroupTenHang.Header.Appearance.FontData.SizeInPoints = 9;
            //parentBandGroupTenHang.Header.Appearance.TextHAlign = HAlign.Center;
            ////parentBandGroupTenHang.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            ////parentBandGroupTenHang.Header.Appearance.BorderColor = Color.Black;
            //parentBandGroupTenHang.Header.Appearance.BackColorAlpha = Alpha.Default;
            //parentBandGroupTenHang.RowLayoutGroupInfo.SpanX = 40;
            //parentBandGroupTenHang.RowLayoutGroupInfo.SpanY = 100;
            //parentBandGroupTenHang.RowLayoutGroupInfo.OriginX = 40;
            //parentBandGroupTenHang.RowLayoutGroupInfo.OriginY = 0;
            //parentBandGroupTenHang.CellAppearance.BorderAlpha = Alpha.Transparent;
            //parentBandGroupTenHang.CellAppearance.BorderColor = Color.White;

            //UltraGridGroup parentBandGroupTrenTenHang;
            //if (parentBand.Groups.Exists("ParentBandGroupTrenTenHang"))
            //    parentBandGroupTrenTenHang = parentBand.Groups["ParentBandGroupTrenTenHang"];
            //else
            //    parentBandGroupTrenTenHang = parentBand.Groups.Add("ParentBandGroupTrenTenHang", "");
            //parentBandGroupTrenTenHang.RowLayoutGroupInfo.ParentGroup = parentBandGroupTenHang;
            //parentBandGroupTrenTenHang.Header.Appearance.FontData.SizeInPoints = 9;
            //parentBandGroupTrenTenHang.Header.Appearance.TextHAlign = HAlign.Center;
            //parentBandGroupTrenTenHang.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            //parentBandGroupTrenTenHang.Header.Appearance.BorderColor = Color.Black;
            //parentBandGroupTrenTenHang.Header.Appearance.BackColorAlpha = Alpha.Default;
            //parentBandGroupTrenTenHang.RowLayoutGroupInfo.SpanX = 40;
            //parentBandGroupTrenTenHang.RowLayoutGroupInfo.SpanY = 100;
            //parentBandGroupTrenTenHang.RowLayoutGroupInfo.OriginX = 40;
            //parentBandGroupTrenTenHang.RowLayoutGroupInfo.OriginY = 32;



            //UltraGridGroup parentBandGroupDVT;
            //if (parentBand.Groups.Exists("ParentBandGroupDVT"))
            //    parentBandGroupDVT = parentBand.Groups["ParentBandGroupDVT"];
            //else
            //    parentBandGroupDVT = parentBand.Groups.Add("ParentBandGroupDVT", "ĐVT");
            //parentBandGroupDVT.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            //parentBandGroupDVT.Header.Appearance.FontData.SizeInPoints = 9;
            //parentBandGroupDVT.Header.Appearance.TextHAlign = HAlign.Center;
            //parentBandGroupDVT.Header.Appearance.TextVAlign = VAlign.Bottom;
            ////parentBandGroupDVT.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            ////parentBandGroupDVT.Header.Appearance.BorderColor = Color.Black;
            //parentBandGroupDVT.Header.Appearance.BackColorAlpha = Alpha.Default;
            //parentBandGroupDVT.RowLayoutGroupInfo.SpanX = 20;
            //parentBandGroupDVT.RowLayoutGroupInfo.SpanY = 100;
            //parentBandGroupDVT.RowLayoutGroupInfo.OriginX = 80;
            //parentBandGroupDVT.RowLayoutGroupInfo.OriginY = 0;
            //parentBandGroupDVT.CellAppearance.BorderAlpha = Alpha.Transparent;
            //parentBandGroupDVT.CellAppearance.BorderColor = Color.White;

            //UltraGridGroup parentBandGroupDuoiDVT;
            //if (parentBand.Groups.Exists("ParentBandGroupDuoiDVT"))
            //    parentBandGroupDuoiDVT = parentBand.Groups["ParentBandGroupDuoiDVT"];
            //else
            //    parentBandGroupDuoiDVT = parentBand.Groups.Add("ParentBandGroupDuoiDVT", "ĐVT");
            //parentBandGroupDuoiDVT.RowLayoutGroupInfo.ParentGroup = parentBandGroupDVT;
            //parentBandGroupDuoiDVT.Header.Appearance.FontData.SizeInPoints = 9;
            //parentBandGroupDuoiDVT.Header.Appearance.TextHAlign = HAlign.Center;
            //parentBandGroupDuoiDVT.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            //parentBandGroupDuoiDVT.Header.Appearance.BorderColor = Color.Black;
            //parentBandGroupDuoiDVT.Header.Appearance.BackColorAlpha = Alpha.Default;
            //parentBandGroupDuoiDVT.RowLayoutGroupInfo.SpanX = 20;
            //parentBandGroupDuoiDVT.RowLayoutGroupInfo.SpanY = 100;
            //parentBandGroupDuoiDVT.RowLayoutGroupInfo.OriginX = 80;
            //parentBandGroupDuoiDVT.RowLayoutGroupInfo.OriginY = 32;

            //uGridDuLieu.DisplayLayout.Bands[0].Columns["RepositoryName"].RowLayoutColumnInfo.ParentGroup = parentBandGroupTenHang;
            //uGridDuLieu.DisplayLayout.Bands[0].Columns["RepositoryName"].RowLayoutColumnInfo.ParentGroup = parentBandGroupDVT;
            //uGridDuLieu.DisplayLayout.Bands[1].Columns["MaterialGoodName"].RowLayoutColumnInfo.ParentGroup= parentBandGroupTenHang;
            //uGridDuLieu.DisplayLayout.Bands[0].Columns["Unit"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;

            UltraGridGroup parentBandGroupDK;
            if (parentBand.Groups.Exists("ParentBandGroupDK"))
                parentBandGroupDK = parentBand.Groups["ParentBandGroupDK"];
            else
                parentBandGroupDK = parentBand.Groups.Add("ParentBandGroupDK", "Đầu kỳ");
            parentBandGroupDK.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            parentBandGroupDK.Header.Appearance.FontData.SizeInPoints = 9;
            parentBandGroupDK.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupDK.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupDK.Header.Appearance.BorderColor = Color.Black;
            parentBandGroupDK.Header.Appearance.BackColorAlpha = Alpha.Default;
            parentBandGroupDK.RowLayoutGroupInfo.OriginX= uGridDuLieu.DisplayLayout.Bands[0].Columns["Unit"].RowLayoutColumnInfo.OriginX+ parentBandGroupDK.RowLayoutGroupInfo.SpanX;
            parentBandGroupDK.RowLayoutGroupInfo.OriginY = 0;



            UltraGridGroup parentBandGroupNK;
            if (parentBand.Groups.Exists("ParentBandGroupNK"))
                parentBandGroupNK = parentBand.Groups["ParentBandGroupNK"];
            else
                parentBandGroupNK = parentBand.Groups.Add("ParentBandGroupNK", "Nhập kho");
            parentBandGroupNK.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            parentBandGroupNK.Header.Appearance.FontData.SizeInPoints = 9;
            parentBandGroupNK.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupNK.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupNK.Header.Appearance.BorderColor = Color.Black;
            parentBandGroupNK.Header.Appearance.BackColorAlpha = Alpha.Default;

            UltraGridGroup parentBandGroupXK;
            if (parentBand.Groups.Exists("ParentBandGroupXK"))
                parentBandGroupXK = parentBand.Groups["ParentBandGroupXK"];
            else
                parentBandGroupXK = parentBand.Groups.Add("ParentBandGroupXK", "Xuất kho");
            parentBandGroupXK.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            parentBandGroupXK.Header.Appearance.FontData.SizeInPoints = 9;
            parentBandGroupXK.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupXK.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupXK.Header.Appearance.BorderColor = Color.Black;
            parentBandGroupXK.Header.Appearance.BackColorAlpha = Alpha.Default;

            UltraGridGroup parentBandGroupCK;
            if (parentBand.Groups.Exists("ParentBandGroupCK"))
                parentBandGroupCK = parentBand.Groups["ParentBandGroupCK"];
            else
                parentBandGroupCK = parentBand.Groups.Add("ParentBandGroupCK", "Cuối kỳ");
            parentBandGroupCK.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            parentBandGroupCK.Header.Appearance.FontData.SizeInPoints = 9;
            parentBandGroupCK.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupCK.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupCK.Header.Appearance.BorderColor = Color.Black;
            parentBandGroupCK.Header.Appearance.BackColorAlpha = Alpha.Default;
           

            parentBandGroupPeriod.RowLayoutGroupInfo.ParentGroup = parentBandGroupTitle;
            parentBandGroupDK.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            parentBandGroupNK.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            parentBandGroupXK.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            parentBandGroupCK.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;

            //parentBandGroupDK.RowLayoutGroupInfo.OriginY = 0;
            //parentBandGroupDK.RowLayoutGroupInfo.OriginX = 100;
            //parentBandGroupDK.RowLayoutGroupInfo.SpanX = 300;
            //parentBandGroupDK.RowLayoutGroupInfo.SpanY = 32;


            parentBand.Columns["SLDK"].RowLayoutColumnInfo.ParentGroup = parentBandGroupDK;
            parentBand.Columns["SLDK"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SLDK"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["SLDK"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["SLDK"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SLDK"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["DK"].RowLayoutColumnInfo.ParentGroup = parentBandGroupDK;
            parentBand.Columns["DK"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["DK"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["DK"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["DK"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["DK"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["SLIWGK"].RowLayoutColumnInfo.ParentGroup = parentBandGroupNK;
            parentBand.Columns["SLIWGK"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SLIWGK"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["SLIWGK"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["SLIWGK"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SLIWGK"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["IWGK"].RowLayoutColumnInfo.ParentGroup = parentBandGroupNK;
            parentBand.Columns["IWGK"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["IWGK"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["IWGK"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["IWGK"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["IWGK"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["SLOWGK"].RowLayoutColumnInfo.ParentGroup = parentBandGroupXK;
            parentBand.Columns["SLOWGK"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SLOWGK"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["SLOWGK"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["SLOWGK"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SLOWGK"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["OWGK"].RowLayoutColumnInfo.ParentGroup = parentBandGroupXK;
            parentBand.Columns["OWGK"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["OWGK"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["OWGK"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["OWGK"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["OWGK"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["SLCK"].RowLayoutColumnInfo.ParentGroup = parentBandGroupCK;
            parentBand.Columns["SLCK"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SLCK"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["SLCK"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["SLCK"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SLCK"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["CK"].RowLayoutColumnInfo.ParentGroup = parentBandGroupCK;
            parentBand.Columns["CK"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["CK"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["CK"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["CK"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["CK"].Header.Appearance.BorderColor = Color.Black;

           // uGridDuLieu.DisplayLayout.Bands[1].Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
           // uGridDuLieu.DisplayLayout.Bands[0].Columns["RepositoryName"].Header.VisiblePosition = 0;
           //// uGridDuLieu.DisplayLayout.Bands[0].Columns[11].Header.VisiblePosition = 1;     mới bỏ
           // uGridDuLieu.DisplayLayout.Bands[0].Columns["SumSLDK"].Header.VisiblePosition = 2;
           // uGridDuLieu.DisplayLayout.Bands[0].Columns["SumDK"].Header.VisiblePosition = 3;
           // uGridDuLieu.DisplayLayout.Bands[0].Columns["SumSLIWGK"].Header.VisiblePosition = 4;
           // uGridDuLieu.DisplayLayout.Bands[0].Columns["SumIWGK"].Header.VisiblePosition = 5;
           // uGridDuLieu.DisplayLayout.Bands[0].Columns["SumSLOWGK"].Header.VisiblePosition = 6;
           // uGridDuLieu.DisplayLayout.Bands[0].Columns["SumOWGK"].Header.VisiblePosition = 7;
           // uGridDuLieu.DisplayLayout.Bands[0].Columns["SumSLCK"].Header.VisiblePosition = 8;
           // uGridDuLieu.DisplayLayout.Bands[0].Columns["SumCK"].Header.VisiblePosition = 9;

            
           // uGridDuLieu.DisplayLayout.Bands[1].ColHeadersVisible = false;

            
            //uGridDuLieu.DisplayLayout.Bands[0].Columns["SumSLDK"].FormatNumberic(ConstDatabase.Format_TienVND);
            //uGridDuLieu.DisplayLayout.Bands[0].Columns["SumSLIWGK"].FormatNumberic(ConstDatabase.Format_TienVND);
            //uGridDuLieu.DisplayLayout.Bands[0].Columns["SumSLOWGK"].FormatNumberic(ConstDatabase.Format_TienVND);
            //uGridDuLieu.DisplayLayout.Bands[0].Columns["SumSLCK"].FormatNumberic(ConstDatabase.Format_TienVND);
            //uGridDuLieu.DisplayLayout.Bands[0].Columns["SumDK"].FormatNumberic(ConstDatabase.Format_TienVND);
            //uGridDuLieu.DisplayLayout.Bands[0].Columns["SumIWGK"].FormatNumberic(ConstDatabase.Format_TienVND);
            //uGridDuLieu.DisplayLayout.Bands[0].Columns["SumOWGK"].FormatNumberic(ConstDatabase.Format_TienVND);
            //uGridDuLieu.DisplayLayout.Bands[0].Columns["SumCK"].FormatNumberic(ConstDatabase.Format_TienVND);

            //uGridDuLieu.DisplayLayout.Bands[1].Columns["SLDK"].FormatNumberic(ConstDatabase.Format_TienVND);
            //uGridDuLieu.DisplayLayout.Bands[1].Columns["SLIWGK"].FormatNumberic(ConstDatabase.Format_TienVND);
            //uGridDuLieu.DisplayLayout.Bands[1].Columns["SLOWGK"].FormatNumberic(ConstDatabase.Format_TienVND);
            //uGridDuLieu.DisplayLayout.Bands[1].Columns["SLCK"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGridDuLieu.DisplayLayout.Bands[0].Columns["DK"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGridDuLieu.DisplayLayout.Bands[0].Columns["IWGK"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGridDuLieu.DisplayLayout.Bands[0].Columns["OWGK"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGridDuLieu.DisplayLayout.Bands[0].Columns["CK"].FormatNumberic(ConstDatabase.Format_TienVND);
            //           uGridDuLieu.DisplayLayout.Bands[0].Columns["RepositoryName"].Header.VisiblePosition = 1;

            SetDoSoAm(uGridDuLieu);
            uGridDuLieu.DisplayLayout.Bands[0].Columns["MaterialGoodCode"].Header.VisiblePosition = 0;
            uGridDuLieu.DisplayLayout.Bands[0].Columns["MaterialGoodName"].Header.VisiblePosition = 1;
            uGridDuLieu.DisplayLayout.Bands[0].Columns["Unit"].Header.VisiblePosition = 2;
            uGridDuLieu.DisplayLayout.Bands[0].Columns["SLDK"].Header.VisiblePosition = 3;
            uGridDuLieu.DisplayLayout.Bands[0].Columns["DK"].Header.VisiblePosition = 4;
            uGridDuLieu.DisplayLayout.Bands[0].Columns["SLIWGK"].Header.VisiblePosition = 5;
            uGridDuLieu.DisplayLayout.Bands[0].Columns["IWGK"].Header.VisiblePosition = 6;
            uGridDuLieu.DisplayLayout.Bands[0].Columns["SLOWGK"].Header.VisiblePosition = 7;
            uGridDuLieu.DisplayLayout.Bands[0].Columns["OWGK"].Header.VisiblePosition = 8;
            uGridDuLieu.DisplayLayout.Bands[0].Columns["SLCK"].Header.VisiblePosition = 9;
            uGridDuLieu.DisplayLayout.Bands[0].Columns["CK"].Header.VisiblePosition = 10;
            //parentBand.Columns["SLCK"].RowLayoutColumnInfo.ParentGroup = parentBandGroupCK;
            //parentBand.Columns["SLCK"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            //parentBand.Columns["SLCK"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            //parentBand.Columns["SLCK"].Header.Appearance.ForeColor = Color.Black;
            //parentBand.Columns["SLCK"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            //parentBand.Columns["SLCK"].Header.Appearance.BorderColor = Color.Black;

            //parentBand.Columns["CK"].RowLayoutColumnInfo.ParentGroup = parentBandGroupCK;
            //parentBand.Columns["CK"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            //parentBand.Columns["CK"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            //parentBand.Columns["CK"].Header.Appearance.ForeColor = Color.Black;
            //parentBand.Columns["CK"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            //parentBand.Columns["CK"].Header.Appearance.BorderColor = Color.Black;
            if (Utils.isDemo)
            {
                ultraButton2.Visible = false;
            }
            WaitingFrm.StopWaiting();
        }
        private void SetDoSoAm(UltraGrid ultraGrid)
        {
            foreach (var item in ultraGrid.Rows)
            {
                if ((decimal)item.Cells["DK"].Value < 0)
                {
                    item.Cells["DK"].Appearance.ForeColor = Color.Red;

                }
                if ((decimal)item.Cells["IWGK"].Value < 0)
                {
                    item.Cells["IWGK"].Appearance.ForeColor = Color.Red;

                }
                if ((decimal)item.Cells["OWGK"].Value < 0)
                {
                    item.Cells["OWGK"].Appearance.ForeColor = Color.Red;

                }
                if ((decimal)item.Cells["CK"].Value < 0)
                {
                    item.Cells["CK"].Appearance.ForeColor = Color.Red;

                }
                if ((decimal)item.Cells["SLDK"].Value < 0)
                {
                    item.Cells["SLDK"].Appearance.ForeColor = Color.Red;

                }
                if ((decimal)item.Cells["SLIWGK"].Value < 0)
                {
                    item.Cells["SLIWGK"].Appearance.ForeColor = Color.Red;

                }
                if ((decimal)item.Cells["SLOWGK"].Value < 0)
                {
                    item.Cells["SLOWGK"].Appearance.ForeColor = Color.Red;

                }
                if ((decimal)item.Cells["SLCK"].Value < 0)
                {
                    item.Cells["SLCK"].Appearance.ForeColor = Color.Red;

                }
                if(item.Cells["MaterialGoodCode"].Value.ToString()=="Kho hàng :")
                {
                    item.Cells["MaterialGoodCode"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    item.Cells["MaterialGoodName"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    item.Cells["Unit"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    item.Cells["SLDK"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    item.Cells["SLIWGK"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    item.Cells["SLOWGK"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    item.Cells["SLCK"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    item.Cells["DK"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    item.Cells["IWGK"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    item.Cells["OWGK"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    item.Cells["CK"].Appearance.FontData.Bold = DefaultableBoolean.True;
                }
                
            }
            //foreach (var item in ultraGrid.Rows)
            //{
            //    foreach(var child in item.ChildBands[0].Rows)
            //    {
            //        if ((decimal)child.Cells["SLDK"].Value < 0)
            //            child.Cells["SLDK"].Appearance.ForeColor = Color.Red;
            //    }
            //    foreach (var child in item.ChildBands[0].Rows)
            //    {
            //        if ((decimal)child.Cells["SLCK"].Value < 0)
            //            child.Cells["SLCK"].Appearance.ForeColor = Color.Red;
            //    }
            //    foreach (var child in item.ChildBands[0].Rows)
            //    {
            //        if ((decimal)child.Cells["DK"].Value < 0)
            //            child.Cells["DK"].Appearance.ForeColor = Color.Red;
            //    }
            //    foreach (var child in item.ChildBands[0].Rows)
            //    {
            //        if ((decimal)child.Cells["CK"].Value < 0)
            //            child.Cells["CK"].Appearance.ForeColor = Color.Red;
            //    }
            //    foreach (var child in item.ChildBands[0].Rows)
            //    {
            //        if ((decimal)child.Cells["SLIWGK"].Value < 0)
            //            child.Cells["SLIWGK"].Appearance.ForeColor = Color.Red;
            //    }
            //    foreach (var child in item.ChildBands[0].Rows)
            //    {
            //        if ((decimal)child.Cells["IWGK"].Value < 0)
            //            child.Cells["IWGK"].Appearance.ForeColor = Color.Red;
            //    }
            //    foreach (var child in item.ChildBands[0].Rows)
            //    {
            //        if ((decimal)child.Cells["SLOWGK"].Value < 0)
            //            child.Cells["SLOWGK"].Appearance.ForeColor = Color.Red;
            //    }
            //    foreach (var child in item.ChildBands[0].Rows)
            //    {
            //        if ((decimal)child.Cells["OWGK"].Value < 0)
            //            child.Cells["OWGK"].Appearance.ForeColor = Color.Red;
            //    }
            //}

        }
        private void uGridDuLieu_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {

        }

        private void uGridDuLieu_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            UltraGridCell cell = e.Row.Cells[0];
            cell.Row.Height = 25;
            //UltraGridCell cell = e.Row.ChildBands[0].Rows[0].Cells["DK"];
            //cell.Row.Height = 20;

            //if (uGridDuLieu.DisplayLayout.Bands[0].Columns.Exists("SumDK"))
            //{
            //    if (cell.Row.Cells["SumDK"].Value != null && (decimal)cell.Row.Cells["SumDK"].Value == 0)
            //        cell.Row.Cells["SumDK"].Value = null;
            //    if ((cell.Row.Cells["SumDK"].Value != null && (decimal)cell.Row.Cells["SumDK"].Value < 0))
            //        cell.Row.Cells["SumDK"].Appearance.ForeColor = Color.Red;
            //}
            //if (uGridDuLieu.DisplayLayout.Bands[0].Columns.Exists("SumIWGK"))
            //{
            //    if (cell.Row.Cells["SumIWGK"].Value != null && (decimal)cell.Row.Cells["SumIWGK"].Value == 0)
            //        cell.Row.Cells["SumIWGK"].Value = null;
            //    if ((cell.Row.Cells["SumIWGK"].Value != null && (decimal)cell.Row.Cells["SumIWGK"].Value < 0))
            //        cell.Row.Cells["SumIWGK"].Appearance.ForeColor = Color.Red;
            //}
            //if (uGridDuLieu.DisplayLayout.Bands[0].Columns.Exists("SumOWGK"))
            //{
            //    if (cell.Row.Cells["SumOWGK"].Value != null && (decimal)cell.Row.Cells["SumOWGK"].Value == 0)
            //        cell.Row.Cells["SumOWGK"].Value = null;
            //    if ((cell.Row.Cells["SumOWGK"].Value != null && (decimal)cell.Row.Cells["SumOWGK"].Value < 0))
            //        cell.Row.Cells["SumOWGK"].Appearance.ForeColor = Color.Red;
            //}
            //if (uGridDuLieu.DisplayLayout.Bands[0].Columns.Exists("SumCK"))
            //{
            //    if (cell.Row.Cells["SumCK"].Value != null && (decimal)cell.Row.Cells["SumCK"].Value == 0)
            //        cell.Row.Cells["SumCK"].Value = null;
            //    if ((cell.Row.Cells["SumCK"].Value != null && (decimal)cell.Row.Cells["SumCK"].Value < 0))
            //        cell.Row.Cells["SumCK"].Appearance.ForeColor = Color.Red;
            //}
            //if (uGridDuLieu.DisplayLayout.Bands[1].Columns.Exists("DK"))
            //{
            //    if (cell.Row.Cells["DK"].Value != null && (decimal)cell.Row.Cells["DK"].Value == 0)
            //        cell.Row.Cells["DK"].Value = null;
            //    if ((cell.Row.Cells["DK"].Value != null && (decimal)cell.Row.Cells["DK"].Value < 0))
            //        cell.Row.Cells["DK"].Appearance.ForeColor = Color.Red;
            //}
            //if (uGridDuLieu.DisplayLayout.Bands[1].Columns.Exists("IWGK"))
            //{
            //    if (cell.Row.Cells["IWGK"].Value != null && (decimal)cell.Row.Cells["IWGK"].Value == 0)
            //        cell.Row.Cells["IWGK"].Value = null;
            //    if ((cell.Row.Cells["IWGK"].Value != null && (decimal)cell.Row.Cells["IWGK"].Value < 0))
            //        cell.Row.Cells["IWGK"].Appearance.ForeColor = Color.Red;
            //}
            //if (uGridDuLieu.DisplayLayout.Bands[1].Columns.Exists("OWGK"))
            //{
            //    if (cell.Row.Cells["OWGK"].Value != null && (decimal)cell.Row.Cells["OWGK"].Value == 0)
            //        cell.Row.Cells["OWGK"].Value = null;
            //    if ((cell.Row.Cells["OWGK"].Value != null && (decimal)cell.Row.Cells["OWGK"].Value < 0))
            //        cell.Row.Cells["OWGK"].Appearance.ForeColor = Color.Red;
            //}
            //if (uGridDuLieu.DisplayLayout.Bands[1].Columns.Exists("CK"))
            //{
            //    if (cell.Row.Cells["CK"].Value != null && (decimal)cell.Row.Cells["CK"].Value == 0)
            //        cell.Row.Cells["CK"].Value = null;
            //    if ((cell.Row.Cells["CK"].Value != null && (decimal)cell.Row.Cells["CK"].Value < 0))
            //        cell.Row.Cells["CK"].Appearance.ForeColor = Color.Red;
            //}

        }

        private void ultraButton3_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void XuLiListLenGrid(List<TongHopTonKho> data ,List<TongHopTonKhoDetail> dataDetail )
        {
            foreach(var item in data)
            {
                TongHopTonKhoDetail tongHopTonKhoDetail = new TongHopTonKhoDetail();
                tongHopTonKhoDetail.MaterialGoodCode = "Kho hàng :";
                tongHopTonKhoDetail.MaterialGoodName = item.RepositoryName;
                tongHopTonKhoDetail.SLDK = item.SumSLDK;
                tongHopTonKhoDetail.DK = item.SumDK;
                tongHopTonKhoDetail.SLCK = item.SumSLCK;
                tongHopTonKhoDetail.CK = item.SumCK;
                tongHopTonKhoDetail.IWGK = item.SumIWGK;
                tongHopTonKhoDetail.SLIWGK = item.SumSLIWGK;
                tongHopTonKhoDetail.SLOWGK = item.SumSLOWGK;
                tongHopTonKhoDetail.OWGK = item.SumOWGK;
                tongHopTonKhoDetail.Ordercode = 0;
                tongHopTonKhoDetail.RepositoryCode = item.RepositoryCode;
                dataDetail.Add(tongHopTonKhoDetail);
                foreach(var child in item.TongHopTonKhoDetails)
                {
                    TongHopTonKhoDetail tongHopTonKhoDetail1 = new TongHopTonKhoDetail();
                    tongHopTonKhoDetail1 = child;
                    tongHopTonKhoDetail1.Ordercode = 1;
                    tongHopTonKhoDetail1.RepositoryCode = item.RepositoryCode;
                    dataDetail.Add(tongHopTonKhoDetail1);
                }
            }
        }
        private void ultraButton2_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog
            {
                Title = "Chọn nơi sao lưu ",
                Filter = "File (*.xls)|*.xls",
                InitialDirectory = "@C:\\",
                FileName = "TongHopTonKho.xls"
            };
            if (save.ShowDialog() == DialogResult.OK)
            {
                string duongdan = save.FileName;
                try
                {
                    ultraGridExcelExporter1.Export(uGridDuLieu, duongdan);
                    if (MSG.Question("Kết xuất thành công, Bạn có muốn mở file vừa kết xuất không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(save.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
                catch
                {
                    MSG.Warning("Lỗi khi kết xuất !");
                }
            }
        }

        private void ultraGridExcelExporter1_InitializeColumn(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.InitializeColumnEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Column.Format))
            {
                e.ExcelFormatStr = e.Column.Format;
            }
        }

        private void ultraButton1_Click(object sender, EventArgs e)
        {
            if (radInA3.Checked == true)
            {
                string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
                fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\TongHopTonKho.rst", path);
            }
            else if (radInA4.Checked == true)
            {
                string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
                fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\TongHopTonKhoA4.rst", path);
            }
            else
            {
                MSG.Warning("Chọn mẫu in báo cáo trước !");
            }
            //if (dtBeginDate.Value == null || dtEndDate.Value == null)
            //{
            //    MSG.Warning("Ngày không được để trống");
            //    return;
            //}

            //if (dtBeginDate.DateTime.Date > dtEndDate.DateTime.Date)
            //{
            //    //MessageBox.Show("Ngày bắt đầu không thể lớn hơn ngày kết thúc", "Cảnh báo");
            //    MSG.Warning("Từ ngày không được lớn hơn đến ngày");
            //    return;
            //}
            //if (_lstRepositoryReport == null || !_lstRepositoryReport.Any(x => x.Check))
            //{
            //    MessageBox.Show("Chưa chọn kho để xem", "Cảnh báo");
            //    return;
            //}
            var rD = new Detail_Period
            {
                Period = tieude1
            };
            List<TongHopTonKho> data1 = new List<TongHopTonKho>();
            //var lstRepos = Utils.ListRepository.Where(c => (_lstRepositoryReport.Where(a => a.Check).ToList()).Any(d => d.ID == c.ID)).ToList();
            //if (_check1) data1 = _IMaterialGoodsService.getMaterialGood(dtBeginDate.DateTime, dtEndDate.DateTime, lstRepos, (MaterialGoodsCategory)cbbMaterialGoodsCategory1.SelectedRow.ListObject);
            //else data1 = _IMaterialGoodsService.getMaterialGood(dtBeginDate.DateTime, dtEndDate.DateTime, lstRepos);
            var f = new ReportForm<TongHopTonKho>(fileReportSlot1, _subSystemCode1);
            f.AddDatasource("TongHopTonKho", data, true);
            f.AddDatasource("Detail", rD);
            f.SetHyperlinkFunction("Accounting.ReportUtils", "Hyperlink");
            f.LoadReport();
            f.WindowState = FormWindowState.Maximized;
            f.Show();
            {
                //foreach (var item in ugridRepository.Rows)
                //{
                //    item.Cells["Check"].Value = false;
                //}
            }
        }

        private void ultraGridExcelExporter1_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.ExcelExportInitializeRowEventArgs e)
        {
           
        }

        private void ultraGridExcelExporter1_RowExporting(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.RowExportingEventArgs e)
        {
           
            
            
        }

        private void ultraGridExcelExporter1_RowExported(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.RowExportedEventArgs e)
        {
          //  e.GridRow.ChildBands.All.
        }

        private void ultraGridExcelExporter1_BeginExport(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.BeginExportEventArgs e)
        {
            foreach (var row in uGridDuLieu.Rows)
            {
                foreach (var column in uGridDuLieu.DisplayLayout.Bands[0].Columns)
                {
                    e.Rows[row.Index].Cells[column].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                    e.Rows[row.Index].Cells[column].Appearance.BorderColor = Color.Black;
                    e.Rows[row.Index].Cells[column].Column.Width = 200;
                }
            }
        }
    }
}
