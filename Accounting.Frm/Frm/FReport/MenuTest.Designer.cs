﻿namespace Accounting
{
    partial class MenuTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ultraButton1 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton2 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton3 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton4 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton5 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton6 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton7 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton8 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton9 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton10 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton11 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton12 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton13 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton14 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton15 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton16 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton17 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton18 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton19 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton20 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton21 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton22 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton23 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton24 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton25 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton26 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton27 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton28 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton30 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton31 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton29 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton32 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton33 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton34 = new Infragistics.Win.Misc.UltraButton();
            this.SuspendLayout();
            // 
            // ultraButton1
            // 
            this.ultraButton1.Location = new System.Drawing.Point(12, 12);
            this.ultraButton1.Name = "ultraButton1";
            this.ultraButton1.Size = new System.Drawing.Size(284, 23);
            this.ultraButton1.TabIndex = 0;
            this.ultraButton1.Text = "S05a-DNN Sổ quỹ tiền mặt";
            this.ultraButton1.Click += new System.EventHandler(this.ultraButton1_Click);
            // 
            // ultraButton2
            // 
            this.ultraButton2.Location = new System.Drawing.Point(12, 41);
            this.ultraButton2.Name = "ultraButton2";
            this.ultraButton2.Size = new System.Drawing.Size(284, 23);
            this.ultraButton2.TabIndex = 1;
            this.ultraButton2.Text = "Tổng hợp công nợ phải trả PayableSumary";
            this.ultraButton2.Click += new System.EventHandler(this.ultraButton2_Click);
            // 
            // ultraButton3
            // 
            this.ultraButton3.Location = new System.Drawing.Point(12, 128);
            this.ultraButton3.Name = "ultraButton3";
            this.ultraButton3.Size = new System.Drawing.Size(284, 23);
            this.ultraButton3.TabIndex = 2;
            this.ultraButton3.Text = "Tổng hợp công nợ phải thu ReceiveableSumary";
            this.ultraButton3.Click += new System.EventHandler(this.ultraButton3_Click);
            // 
            // ultraButton4
            // 
            this.ultraButton4.Location = new System.Drawing.Point(12, 70);
            this.ultraButton4.Name = "ultraButton4";
            this.ultraButton4.Size = new System.Drawing.Size(284, 23);
            this.ultraButton4.TabIndex = 3;
            this.ultraButton4.Text = "Số chi tiết mua hàng -PurchaseDetails";
            this.ultraButton4.Click += new System.EventHandler(this.ultraButton4_Click);
            // 
            // ultraButton5
            // 
            this.ultraButton5.Location = new System.Drawing.Point(12, 99);
            this.ultraButton5.Name = "ultraButton5";
            this.ultraButton5.Size = new System.Drawing.Size(284, 23);
            this.ultraButton5.TabIndex = 4;
            this.ultraButton5.Text = "S17DNN Sổ chi tiết bán hàng";
            this.ultraButton5.Click += new System.EventHandler(this.ultraButton5_Click);
            // 
            // ultraButton6
            // 
            this.ultraButton6.Location = new System.Drawing.Point(338, 12);
            this.ultraButton6.Name = "ultraButton6";
            this.ultraButton6.Size = new System.Drawing.Size(284, 23);
            this.ultraButton6.TabIndex = 5;
            this.ultraButton6.Text = "S06DNN Sổ tiền gửi ngân hàng";
            this.ultraButton6.Click += new System.EventHandler(this.ultraButton6_Click);
            // 
            // ultraButton7
            // 
            this.ultraButton7.Location = new System.Drawing.Point(338, 41);
            this.ultraButton7.Name = "ultraButton7";
            this.ultraButton7.Size = new System.Drawing.Size(284, 23);
            this.ultraButton7.TabIndex = 6;
            this.ultraButton7.Text = "S09DNN Thẻ kho";
            this.ultraButton7.Click += new System.EventHandler(this.ultraButton7_Click);
            // 
            // ultraButton8
            // 
            this.ultraButton8.Location = new System.Drawing.Point(338, 70);
            this.ultraButton8.Name = "ultraButton8";
            this.ultraButton8.Size = new System.Drawing.Size(284, 23);
            this.ultraButton8.TabIndex = 7;
            this.ultraButton8.Text = "S12DNN Thẻ tài sản cố định ";
            this.ultraButton8.Click += new System.EventHandler(this.ultraButton8_Click);
            // 
            // ultraButton9
            // 
            this.ultraButton9.Location = new System.Drawing.Point(338, 99);
            this.ultraButton9.Name = "ultraButton9";
            this.ultraButton9.Size = new System.Drawing.Size(284, 23);
            this.ultraButton9.TabIndex = 8;
            this.ultraButton9.Text = "Tình hình thực hiện HĐ bán ContractStatusPurchase";
            this.ultraButton9.Click += new System.EventHandler(this.ultraButton9_Click);
            // 
            // ultraButton10
            // 
            this.ultraButton10.Location = new System.Drawing.Point(338, 215);
            this.ultraButton10.Name = "ultraButton10";
            this.ultraButton10.Size = new System.Drawing.Size(284, 23);
            this.ultraButton10.TabIndex = 13;
            this.ultraButton10.Text = "S03a1DNN Sổ Nhật ký thu tiền ";
            this.ultraButton10.Click += new System.EventHandler(this.ultraButton10_Click);
            // 
            // ultraButton11
            // 
            this.ultraButton11.Location = new System.Drawing.Point(338, 186);
            this.ultraButton11.Name = "ultraButton11";
            this.ultraButton11.Size = new System.Drawing.Size(284, 23);
            this.ultraButton11.TabIndex = 12;
            this.ultraButton11.Text = " TH cấp phát SD ngân sách BUAllocationAndUse";
            this.ultraButton11.Click += new System.EventHandler(this.ultraButton11_Click);
            // 
            // ultraButton12
            // 
            this.ultraButton12.Location = new System.Drawing.Point(338, 244);
            this.ultraButton12.Name = "ultraButton12";
            this.ultraButton12.Size = new System.Drawing.Size(284, 23);
            this.ultraButton12.TabIndex = 11;
            this.ultraButton12.Text = "S03a2DNN  Sổ nhật ký chi tiền ";
            this.ultraButton12.Click += new System.EventHandler(this.ultraButton12_Click);
            // 
            // ultraButton13
            // 
            this.ultraButton13.Location = new System.Drawing.Point(338, 157);
            this.ultraButton13.Name = "ultraButton13";
            this.ultraButton13.Size = new System.Drawing.Size(284, 23);
            this.ultraButton13.TabIndex = 10;
            this.ultraButton13.Text = " Báo cáo cổ tức phải trả DividendPayable";
            this.ultraButton13.Click += new System.EventHandler(this.ultraButton13_Click);
            // 
            // ultraButton14
            // 
            this.ultraButton14.Location = new System.Drawing.Point(338, 128);
            this.ultraButton14.Name = "ultraButton14";
            this.ultraButton14.Size = new System.Drawing.Size(284, 23);
            this.ultraButton14.TabIndex = 9;
            this.ultraButton14.Text = " Tình hình thực hiện HĐ mua ContractStatusSale";
            this.ultraButton14.Click += new System.EventHandler(this.ultraButton14_Click);
            // 
            // ultraButton15
            // 
            this.ultraButton15.Location = new System.Drawing.Point(12, 215);
            this.ultraButton15.Name = "ultraButton15";
            this.ultraButton15.Size = new System.Drawing.Size(284, 23);
            this.ultraButton15.TabIndex = 17;
            this.ultraButton15.Text = " B01DNN";
            this.ultraButton15.Click += new System.EventHandler(this.ultraButton15_Click);
            // 
            // ultraButton16
            // 
            this.ultraButton16.Location = new System.Drawing.Point(12, 186);
            this.ultraButton16.Name = "ultraButton16";
            this.ultraButton16.Size = new System.Drawing.Size(284, 23);
            this.ultraButton16.TabIndex = 16;
            this.ultraButton16.Text = "S03a4-DNN Sổ nhật ký bán hàng ";
            this.ultraButton16.Click += new System.EventHandler(this.ultraButton16_Click);
            // 
            // ultraButton17
            // 
            this.ultraButton17.Location = new System.Drawing.Point(12, 244);
            this.ultraButton17.Name = "ultraButton17";
            this.ultraButton17.Size = new System.Drawing.Size(284, 23);
            this.ultraButton17.TabIndex = 15;
            this.ultraButton17.Text = " B02DNN";
            this.ultraButton17.Click += new System.EventHandler(this.ultraButton17_Click);
            // 
            // ultraButton18
            // 
            this.ultraButton18.Location = new System.Drawing.Point(12, 157);
            this.ultraButton18.Name = "ultraButton18";
            this.ultraButton18.Size = new System.Drawing.Size(284, 23);
            this.ultraButton18.TabIndex = 14;
            this.ultraButton18.Text = "S03a3-DNN  Sổ nhật ký mua hàng";
            this.ultraButton18.Click += new System.EventHandler(this.ultraButton18_Click);
            // 
            // ultraButton19
            // 
            this.ultraButton19.Location = new System.Drawing.Point(338, 331);
            this.ultraButton19.Name = "ultraButton19";
            this.ultraButton19.Size = new System.Drawing.Size(284, 23);
            this.ultraButton19.TabIndex = 21;
            this.ultraButton19.Text = " Sổ tiền gửi Ngoại tệ BankForeignCurrency";
            this.ultraButton19.Click += new System.EventHandler(this.ultraButton19_Click);
            // 
            // ultraButton20
            // 
            this.ultraButton20.Location = new System.Drawing.Point(338, 302);
            this.ultraButton20.Name = "ultraButton20";
            this.ultraButton20.Size = new System.Drawing.Size(284, 23);
            this.ultraButton20.TabIndex = 20;
            this.ultraButton20.Text = " Bảng kê số dư ngân hàng BankBalance";
            this.ultraButton20.Click += new System.EventHandler(this.ultraButton20_Click);
            // 
            // ultraButton21
            // 
            this.ultraButton21.Location = new System.Drawing.Point(338, 360);
            this.ultraButton21.Name = "ultraButton21";
            this.ultraButton21.Size = new System.Drawing.Size(284, 23);
            this.ultraButton21.TabIndex = 19;
            this.ultraButton21.Text = "S07DNN Sổ chi tiết vật liệu, dụng cụ";
            this.ultraButton21.Click += new System.EventHandler(this.ultraButton21_Click);
            // 
            // ultraButton22
            // 
            this.ultraButton22.Location = new System.Drawing.Point(338, 273);
            this.ultraButton22.Name = "ultraButton22";
            this.ultraButton22.Size = new System.Drawing.Size(284, 23);
            this.ultraButton22.TabIndex = 18;
            this.ultraButton22.Text = " Bảng đối chiếu ngân hàng BankCompare";
            this.ultraButton22.Click += new System.EventHandler(this.ultraButton22_Click);
            // 
            // ultraButton23
            // 
            this.ultraButton23.Location = new System.Drawing.Point(12, 331);
            this.ultraButton23.Name = "ultraButton23";
            this.ultraButton23.Size = new System.Drawing.Size(284, 23);
            this.ultraButton23.TabIndex = 25;
            this.ultraButton23.Text = " ";
            // 
            // ultraButton24
            // 
            this.ultraButton24.Location = new System.Drawing.Point(12, 302);
            this.ultraButton24.Name = "ultraButton24";
            this.ultraButton24.Size = new System.Drawing.Size(284, 23);
            this.ultraButton24.TabIndex = 24;
            this.ultraButton24.Text = " F01DNN";
            this.ultraButton24.Click += new System.EventHandler(this.ultraButton24_Click);
            // 
            // ultraButton25
            // 
            this.ultraButton25.Location = new System.Drawing.Point(12, 360);
            this.ultraButton25.Name = "ultraButton25";
            this.ultraButton25.Size = new System.Drawing.Size(284, 23);
            this.ultraButton25.TabIndex = 23;
            this.ultraButton25.Text = " ";
            // 
            // ultraButton26
            // 
            this.ultraButton26.Location = new System.Drawing.Point(12, 273);
            this.ultraButton26.Name = "ultraButton26";
            this.ultraButton26.Size = new System.Drawing.Size(284, 23);
            this.ultraButton26.TabIndex = 22;
            this.ultraButton26.Text = " B03DNN";
            this.ultraButton26.Click += new System.EventHandler(this.ultraButton26_Click);
            // 
            // ultraButton27
            // 
            this.ultraButton27.Location = new System.Drawing.Point(338, 476);
            this.ultraButton27.Name = "ultraButton27";
            this.ultraButton27.Size = new System.Drawing.Size(284, 23);
            this.ultraButton27.TabIndex = 30;
            this.ultraButton27.Text = " S03bDNN";
            this.ultraButton27.Click += new System.EventHandler(this.ultraButton27_Click);
            // 
            // ultraButton28
            // 
            this.ultraButton28.Location = new System.Drawing.Point(338, 447);
            this.ultraButton28.Name = "ultraButton28";
            this.ultraButton28.Size = new System.Drawing.Size(284, 23);
            this.ultraButton28.TabIndex = 29;
            this.ultraButton28.Text = " S03aDNN";
            this.ultraButton28.Click += new System.EventHandler(this.ultraButton28_Click);
            // 
            // ultraButton30
            // 
            this.ultraButton30.Location = new System.Drawing.Point(338, 418);
            this.ultraButton30.Name = "ultraButton30";
            this.ultraButton30.Size = new System.Drawing.Size(284, 23);
            this.ultraButton30.TabIndex = 27;
            this.ultraButton30.Text = "S11DNN  Sổ theo dõi TSCĐ tại nơi sử dụng";
            this.ultraButton30.Click += new System.EventHandler(this.ultraButton30_Click);
            // 
            // ultraButton31
            // 
            this.ultraButton31.Location = new System.Drawing.Point(338, 389);
            this.ultraButton31.Name = "ultraButton31";
            this.ultraButton31.Size = new System.Drawing.Size(284, 23);
            this.ultraButton31.TabIndex = 26;
            this.ultraButton31.Text = "S10DNN Sổ TSCĐ";
            this.ultraButton31.Click += new System.EventHandler(this.ultraButton31_Click);
            // 
            // ultraButton29
            // 
            this.ultraButton29.Location = new System.Drawing.Point(12, 389);
            this.ultraButton29.Name = "ultraButton29";
            this.ultraButton29.Size = new System.Drawing.Size(284, 23);
            this.ultraButton29.TabIndex = 31;
            this.ultraButton29.Text = " ";
            // 
            // ultraButton32
            // 
            this.ultraButton32.Location = new System.Drawing.Point(12, 418);
            this.ultraButton32.Name = "ultraButton32";
            this.ultraButton32.Size = new System.Drawing.Size(284, 23);
            this.ultraButton32.TabIndex = 32;
            this.ultraButton32.Text = " ";
            // 
            // ultraButton33
            // 
            this.ultraButton33.Location = new System.Drawing.Point(12, 447);
            this.ultraButton33.Name = "ultraButton33";
            this.ultraButton33.Size = new System.Drawing.Size(284, 23);
            this.ultraButton33.TabIndex = 33;
            this.ultraButton33.Text = " ";
            // 
            // ultraButton34
            // 
            this.ultraButton34.Location = new System.Drawing.Point(12, 476);
            this.ultraButton34.Name = "ultraButton34";
            this.ultraButton34.Size = new System.Drawing.Size(284, 23);
            this.ultraButton34.TabIndex = 34;
            this.ultraButton34.Text = " ";
            // 
            // MenuTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 510);
            this.Controls.Add(this.ultraButton34);
            this.Controls.Add(this.ultraButton33);
            this.Controls.Add(this.ultraButton32);
            this.Controls.Add(this.ultraButton29);
            this.Controls.Add(this.ultraButton27);
            this.Controls.Add(this.ultraButton28);
            this.Controls.Add(this.ultraButton30);
            this.Controls.Add(this.ultraButton31);
            this.Controls.Add(this.ultraButton23);
            this.Controls.Add(this.ultraButton24);
            this.Controls.Add(this.ultraButton25);
            this.Controls.Add(this.ultraButton26);
            this.Controls.Add(this.ultraButton19);
            this.Controls.Add(this.ultraButton20);
            this.Controls.Add(this.ultraButton21);
            this.Controls.Add(this.ultraButton22);
            this.Controls.Add(this.ultraButton15);
            this.Controls.Add(this.ultraButton16);
            this.Controls.Add(this.ultraButton17);
            this.Controls.Add(this.ultraButton18);
            this.Controls.Add(this.ultraButton10);
            this.Controls.Add(this.ultraButton11);
            this.Controls.Add(this.ultraButton12);
            this.Controls.Add(this.ultraButton13);
            this.Controls.Add(this.ultraButton14);
            this.Controls.Add(this.ultraButton9);
            this.Controls.Add(this.ultraButton8);
            this.Controls.Add(this.ultraButton7);
            this.Controls.Add(this.ultraButton6);
            this.Controls.Add(this.ultraButton5);
            this.Controls.Add(this.ultraButton4);
            this.Controls.Add(this.ultraButton3);
            this.Controls.Add(this.ultraButton2);
            this.Controls.Add(this.ultraButton1);
            this.Name = "MenuTest";
            this.Text = "MenuTest";
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton ultraButton1;
        private Infragistics.Win.Misc.UltraButton ultraButton2;
        private Infragistics.Win.Misc.UltraButton ultraButton3;
        private Infragistics.Win.Misc.UltraButton ultraButton4;
        private Infragistics.Win.Misc.UltraButton ultraButton5;
        private Infragistics.Win.Misc.UltraButton ultraButton6;
        private Infragistics.Win.Misc.UltraButton ultraButton7;
        private Infragistics.Win.Misc.UltraButton ultraButton8;
        private Infragistics.Win.Misc.UltraButton ultraButton9;
        private Infragistics.Win.Misc.UltraButton ultraButton10;
        private Infragistics.Win.Misc.UltraButton ultraButton11;
        private Infragistics.Win.Misc.UltraButton ultraButton12;
        private Infragistics.Win.Misc.UltraButton ultraButton13;
        private Infragistics.Win.Misc.UltraButton ultraButton14;
        private Infragistics.Win.Misc.UltraButton ultraButton15;
        private Infragistics.Win.Misc.UltraButton ultraButton16;
        private Infragistics.Win.Misc.UltraButton ultraButton17;
        private Infragistics.Win.Misc.UltraButton ultraButton18;
        private Infragistics.Win.Misc.UltraButton ultraButton19;
        private Infragistics.Win.Misc.UltraButton ultraButton20;
        private Infragistics.Win.Misc.UltraButton ultraButton21;
        private Infragistics.Win.Misc.UltraButton ultraButton22;
        private Infragistics.Win.Misc.UltraButton ultraButton23;
        private Infragistics.Win.Misc.UltraButton ultraButton24;
        private Infragistics.Win.Misc.UltraButton ultraButton25;
        private Infragistics.Win.Misc.UltraButton ultraButton26;
        private Infragistics.Win.Misc.UltraButton ultraButton27;
        private Infragistics.Win.Misc.UltraButton ultraButton28;
        private Infragistics.Win.Misc.UltraButton ultraButton30;
        private Infragistics.Win.Misc.UltraButton ultraButton31;
        private Infragistics.Win.Misc.UltraButton ultraButton29;
        private Infragistics.Win.Misc.UltraButton ultraButton32;
        private Infragistics.Win.Misc.UltraButton ultraButton33;
        private Infragistics.Win.Misc.UltraButton ultraButton34;
    }
}