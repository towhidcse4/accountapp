﻿using Accounting.Core.DAO;
using Accounting.Core.Domain.obj.Report;
using Accounting.TextMessage;
using Infragistics.Documents.Excel;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRBaoCaoBangKeBanraTruocIn : Form
    {
        List<BangKeBanRa_Model> data1 = new List<BangKeBanRa_Model>();
        DateTime begin1;
        DateTime end1;
        string tieude1;
        int check1;
        public FRBaoCaoBangKeBanraTruocIn(List<BangKeBanRa_Model> data, string tieude, int check, DateTime begin, DateTime end)
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\BangKeHoaDonThueBanRa.rst", path);
            ReportUtils.ProcessControls(this);
            tieude1 = tieude;
            begin1 = begin;
            end1 = end;
            check1 = check;
            ReportProcedureSDS sp = new ReportProcedureSDS();
            
            
            data1 = sp.GetProc_GetBkeInv_Sale_QT(begin, end, check, 0, "");
            data1.ForEach(
                t =>
                {
                    t.VATRATENAME = GetNameGroup(t.VATRATE);
                    if (t.FLAG == -1)
                    {
                        t.DOANH_SO = -t.DOANH_SO;
                        t.THUE_GTGT = -t.THUE_GTGT;
                    }
                });
            List<BangKeBanRa_Model> listGroup = (from a in data1
                                                  group a by a.VATRATENAME into g
                                                  select new BangKeBanRa_Model
                                                  {
                                                      Ordercode = 1,
                                                      VATRATENAME = g.Key,

                                                      DOANH_SO = g.Sum(t => t.DOANH_SO),
                                                      THUE_GTGT = g.Sum(t => t.THUE_GTGT)
                                                  }).ToList();
            decimal sumDOANH_SO = 0;
            decimal sumTHUE_GTGT = 0;
            foreach (var item in listGroup)
            {
                data1.Add(item);
                sumDOANH_SO += item.DOANH_SO;
                sumTHUE_GTGT += item.THUE_GTGT;
            }
            List<string> code = new List<string>();
            for (int i = 0; i < data1.Count; i++)
            {
                if (!code.Contains(data1[i].VATRATENAME))
                {
                    code.Add(data1[i].VATRATENAME);
                    BangKeBanRa_Model bangKeBanRa_Model = new BangKeBanRa_Model();
                    bangKeBanRa_Model.VATRATENAME = data1[i].VATRATENAME;
                    data1.FirstOrDefault(n => n.VATRATENAME == data1[i].VATRATENAME && n.Ordercode == 1).VATRATE = data1[i].VATRATE;
                    data1.FirstOrDefault(n => n.VATRATENAME == data1[i].VATRATENAME && n.Ordercode == 1).SO_HD= data1[i].VATRATENAME;
                    bangKeBanRa_Model.VATRATE = data1[i].VATRATE;
                    //bangKeBanRa_Model.NGUOI_MUA = data1[i].VATRATENAME;
                    bangKeBanRa_Model.SO_HD= data1[i].VATRATENAME;
                    bangKeBanRa_Model.Ordercode = -1;
                    data1.Add(bangKeBanRa_Model);

                }

                //if (item.Ordercode != 0)
                //{
                //    item.Ordercode = 1;
                //}
                //if (data[i].THUE_SUAT_DETAIL == -1)
                //{
                //    data[i].THUE_SUAT_STRING = "Không chịu thuế";
                //}
                //if (data[i].THUE_SUAT_DETAIL == -2)
                //{
                //    data[i].THUE_SUAT_STRING = "Không tính thuế";
                //}
                //if (data[i].THUE_SUAT_DETAIL != -2 && data[i].THUE_SUAT_DETAIL != -1 && data[i].THUE_SUAT_DETAIL != 0)
                //{
                //    data[i].THUE_SUAT_STRING = data[i].THUE_SUAT_DETAIL.ToString() + "%";
                //}
            }
            data1 = data1.OrderByDescending(n => n.VATRATE).ThenBy(n => n.Ordercode).ThenBy(n => n.NGAY_HD).ThenBy(n => n.SO_HD).ToList();
            uGridDuLieu.DataSource = data1;
            Utils.ConfigGrid(uGridDuLieu, ConstDatabase.BangKeBanRa_Model_TableName);
            uGridDuLieu.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            Utils.AddSumColumn(uGridDuLieu, "NGAY_HD", false);
            Utils.AddSumColumn(uGridDuLieu, "NGUOI_MUA", false);
            Utils.AddSumColumn(uGridDuLieu, "MST", false);
            Utils.AddSumColumn(uGridDuLieu, "MAT_HANG", false);
            Utils.AddSumColumn(uGridDuLieu, "TKTHUE", false);

            uGridDuLieu.DisplayLayout.Bands[0].Summaries[1].DisplayFormat = " ";
            uGridDuLieu.DisplayLayout.Bands[0].Summaries[2].DisplayFormat = " ";
            uGridDuLieu.DisplayLayout.Bands[0].Summaries[3].DisplayFormat = " ";
            uGridDuLieu.DisplayLayout.Bands[0].Summaries[4].DisplayFormat = " ";
            uGridDuLieu.DisplayLayout.Bands[0].Summaries[5].DisplayFormat = " ";

            Utils.AddSumColumn1(sumDOANH_SO, uGridDuLieu, "DOANH_SO", false, constDatabaseFormat: 1);
            Utils.AddSumColumn1(sumTHUE_GTGT, uGridDuLieu, "THUE_GTGT", false, constDatabaseFormat: 1);
            uGridDuLieu.DisplayLayout.Bands[0].Columns["DOANH_SO"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGridDuLieu.DisplayLayout.Bands[0].Columns["THUE_GTGT"].FormatNumberic(ConstDatabase.Format_TienVND);
            SetDoSoAm(uGridDuLieu);
            UltraGridBand parentBand = this.uGridDuLieu.DisplayLayout.Bands[0];
            parentBand.RowLayoutStyle = RowLayoutStyle.GroupLayout;
            parentBand.Override.HeaderPlacement = HeaderPlacement.FixedOnTop;

            UltraGridGroup parentBandGroupPeriod;
            if (parentBand.Groups.Exists("ParentBandGroupPeriod"))
            {
                parentBandGroupPeriod = parentBand.Groups["ParentBandGroupPeriod"];
                parentBandGroupPeriod.Header.Caption = tieude;
            }
            else
                parentBandGroupPeriod = parentBand.Groups.Add("ParentBandGroupPeriod", tieude);
            parentBandGroupPeriod.Header.Appearance.FontData.Italic = DefaultableBoolean.True;
            parentBandGroupPeriod.Header.Appearance.FontData.SizeInPoints = 10;
            parentBandGroupPeriod.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupPeriod.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPeriod.Header.Appearance.BorderColor = Color.Black;
            UltraGridGroup parentBandGroupTitle;
            if (parentBand.Groups.Exists("ParentBandGroupTitle"))
                parentBandGroupTitle = parentBand.Groups["ParentBandGroupTitle"];
            else
                parentBandGroupTitle = parentBand.Groups.Add("ParentBandGroupTitle", "BẢNG KÊ HOÁ ĐƠN, CHỨNG TỪ HÀNG HOÁ BÁN RA (MẪU QUẢN TRỊ)");
            parentBandGroupTitle.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroupTitle.Header.Appearance.FontData.SizeInPoints = 12;
            parentBandGroupTitle.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupTitle.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupTitle.Header.Appearance.BorderColor = Color.Black;
            parentBandGroupPeriod.RowLayoutGroupInfo.ParentGroup = parentBandGroupTitle;

            parentBand.Columns["VATRATE"].Hidden = true;
            parentBand.Columns["FLAG"].Hidden = true;
            parentBand.Columns["Note"].Hidden = true;
            parentBand.Columns["Ordercode"].Hidden = true;
            parentBand.Columns["RefID"].Hidden = true;
            parentBand.Columns["RefType"].Hidden = true;
            parentBand.Columns["VATRATENAME"].Hidden = true;


            //UltraGridGroup parentBandGroupSO_HD;
            //if (parentBand.Groups.Exists("ParentBandGroupSO_HD"))
            //    parentBandGroupSO_HD = parentBand.Groups["ParentBandGroupSO_HD"];
            //else
            //    parentBandGroupSO_HD = parentBand.Groups.Add("ParentBandGroupSO_HD", "");
            //parentBandGroupSO_HD.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            //parentBandGroupSO_HD.Header.Appearance.FontData.SizeInPoints = 2;
            //parentBandGroupSO_HD.Header.Appearance.TextHAlign = HAlign.Center;
            //parentBandGroupSO_HD.Header.Appearance.BorderAlpha = Alpha.Transparent;

            ////parentBandGroupSO_HD.Header.Appearance.BorderColor = Color.Black;
            //parentBandGroupSO_HD.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            //parentBandGroupSO_HD.RowLayoutGroupInfo.SpanY = 2;


            //UltraGridGroup parentBandGroupNGAY_HD;
            //if (parentBand.Groups.Exists("ParentBandGroupNGAY_HD"))
            //    parentBandGroupNGAY_HD = parentBand.Groups["ParentBandGroupNGAY_HD"];
            //else
            //    parentBandGroupNGAY_HD = parentBand.Groups.Add("ParentBandGroupNGAY_HD", "");
            //parentBandGroupNGAY_HD.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            //parentBandGroupNGAY_HD.Header.Appearance.FontData.SizeInPoints = 2;
            //parentBandGroupNGAY_HD.Header.Appearance.TextHAlign = HAlign.Center;
            //parentBandGroupNGAY_HD.Header.Appearance.BorderAlpha = Alpha.Transparent;
            ////parentBandGroupNGAY_HD.Header.Appearance.BorderColor = Color.Black;
            //parentBandGroupNGAY_HD.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            //parentBandGroupNGAY_HD.RowLayoutGroupInfo.SpanY = 2;

            //UltraGridGroup parentBandGroupTEN_NBAN;
            //if (parentBand.Groups.Exists("ParentBandGroupTEN_NBAN"))
            //    parentBandGroupTEN_NBAN = parentBand.Groups["ParentBandGroupTEN_NBAN"];
            //else
            //    parentBandGroupTEN_NBAN = parentBand.Groups.Add("ParentBandGroupTEN_NBAN", "");
            //parentBandGroupTEN_NBAN.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            //parentBandGroupTEN_NBAN.Header.Appearance.FontData.SizeInPoints = 2;
            //parentBandGroupTEN_NBAN.Header.Appearance.TextHAlign = HAlign.Center;
            //parentBandGroupTEN_NBAN.Header.Appearance.BorderAlpha = Alpha.Transparent;
            ////parentBandGroupTEN_NBAN.Header.Appearance.BorderColor = Color.Black;
            //parentBandGroupTEN_NBAN.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            //parentBandGroupTEN_NBAN.RowLayoutGroupInfo.SpanY = 2;

            //UltraGridGroup parentBandGroupMST;
            //if (parentBand.Groups.Exists("ParentBandGroupMST"))
            //    parentBandGroupMST = parentBand.Groups["ParentBandGroupMST"];
            //else
            //    parentBandGroupMST = parentBand.Groups.Add("ParentBandGroupMST", "");
            //parentBandGroupMST.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            //parentBandGroupMST.Header.Appearance.FontData.SizeInPoints = 2;
            //parentBandGroupMST.Header.Appearance.TextHAlign = HAlign.Center;
            //parentBandGroupMST.Header.Appearance.BorderAlpha = Alpha.Transparent;
            ////parentBandGroupMST.Header.Appearance.BorderColor = Color.Black;
            //parentBandGroupMST.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            //parentBandGroupMST.RowLayoutGroupInfo.SpanY = 2;


            //UltraGridGroup parentBandGroupMAT_HANG;
            //if (parentBand.Groups.Exists("ParentBandGroupMAT_HANG"))
            //    parentBandGroupMAT_HANG = parentBand.Groups["ParentBandGroupMAT_HANG"];
            //else
            //    parentBandGroupMAT_HANG = parentBand.Groups.Add("ParentBandGroupMAT_HANG", "");
            //parentBandGroupMAT_HANG.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            //parentBandGroupMAT_HANG.Header.Appearance.FontData.SizeInPoints = 2;
            //parentBandGroupMAT_HANG.Header.Appearance.TextHAlign = HAlign.Center;
            //parentBandGroupMAT_HANG.Header.Appearance.BorderAlpha = Alpha.Transparent;
            ////parentBandGroupMAT_HANG.Header.Appearance.BorderColor = Color.Black;
            //parentBandGroupMAT_HANG.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            //parentBandGroupMAT_HANG.RowLayoutGroupInfo.SpanY = 2;


            //UltraGridGroup parentBandGroupGT_CHUATHUE;
            //if (parentBand.Groups.Exists("ParentBandGroupGT_CHUATHUE"))
            //    parentBandGroupGT_CHUATHUE = parentBand.Groups["ParentBandGroupGT_CHUATHUE"];
            //else
            //    parentBandGroupGT_CHUATHUE = parentBand.Groups.Add("ParentBandGroupGT_CHUATHUE", "");
            //parentBandGroupGT_CHUATHUE.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            //parentBandGroupGT_CHUATHUE.Header.Appearance.FontData.SizeInPoints = 2;
            //parentBandGroupGT_CHUATHUE.Header.Appearance.TextHAlign = HAlign.Center;
            //parentBandGroupGT_CHUATHUE.Header.Appearance.BorderAlpha = Alpha.Transparent;
            ////parentBandGroupGT_CHUATHUE.Header.Appearance.BorderColor = Color.Black;
            //parentBandGroupGT_CHUATHUE.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            //parentBandGroupGT_CHUATHUE.RowLayoutGroupInfo.SpanY = 2;


            ////UltraGridGroup parentBandGroupTHUE_SUAT_STRING;
            ////if (parentBand.Groups.Exists("ParentBandGroupTHUE_SUAT_STRING"))
            ////    parentBandGroupTHUE_SUAT_STRING = parentBand.Groups["ParentBandGroupTHUE_SUAT_STRING"];
            ////else
            ////    parentBandGroupTHUE_SUAT_STRING = parentBand.Groups.Add("ParentBandGroupTHUE_SUAT_STRING", "");
            ////parentBandGroupTHUE_SUAT_STRING.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            ////parentBandGroupTHUE_SUAT_STRING.Header.Appearance.FontData.SizeInPoints = 2;
            ////parentBandGroupTHUE_SUAT_STRING.Header.Appearance.TextHAlign = HAlign.Center;
            ////parentBandGroupTHUE_SUAT_STRING.Header.Appearance.BorderAlpha = Alpha.Transparent;
            //////parentBandGroupTHUE_SUAT_STRING.Header.Appearance.BorderColor = Color.Black;
            ////parentBandGroupTHUE_SUAT_STRING.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            ////parentBandGroupTHUE_SUAT_STRING.RowLayoutGroupInfo.SpanY = 2;


            //UltraGridGroup parentBandGroupTHUE_GTGT;
            //if (parentBand.Groups.Exists("ParentBandGroupTHUE_GTGT"))
            //    parentBandGroupTHUE_GTGT = parentBand.Groups["ParentBandGroupTHUE_GTGT"];
            //else
            //    parentBandGroupTHUE_GTGT = parentBand.Groups.Add("ParentBandGroupTHUE_GTGT", "");
            //parentBandGroupTHUE_GTGT.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            //parentBandGroupTHUE_GTGT.Header.Appearance.FontData.SizeInPoints = 2;
            //parentBandGroupTHUE_GTGT.Header.Appearance.TextHAlign = HAlign.Center;
            //parentBandGroupTHUE_GTGT.Header.Appearance.BorderAlpha = Alpha.Transparent;
            ////parentBandGroupTHUE_GTGT.Header.Appearance.BorderColor = Color.Black;
            //parentBandGroupTHUE_GTGT.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            //parentBandGroupTHUE_GTGT.RowLayoutGroupInfo.SpanY = 2;

            //UltraGridGroup parentBandGroupTK_THUE;
            //if (parentBand.Groups.Exists("ParentBandGroupTK_THUE"))
            //    parentBandGroupTK_THUE = parentBand.Groups["ParentBandGroupTK_THUE"];
            //else
            //    parentBandGroupTK_THUE = parentBand.Groups.Add("ParentBandGroupTK_THUE", "");
            //parentBandGroupTK_THUE.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            //parentBandGroupTK_THUE.Header.Appearance.FontData.SizeInPoints = 2;
            //parentBandGroupTK_THUE.Header.Appearance.TextHAlign = HAlign.Center;
            //parentBandGroupTK_THUE.Header.Appearance.BorderAlpha = Alpha.Transparent;
            ////parentBandGroupTK_THUE.Header.Appearance.BorderColor = Color.Black;
            //parentBandGroupTK_THUE.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            //parentBandGroupTK_THUE.RowLayoutGroupInfo.SpanY = 2;


            //parentBand.Columns["VATRATENAME"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            //parentBand.Columns["VATRATENAME"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            //parentBand.Columns["VATRATENAME"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            //parentBand.Columns["VATRATENAME"].Header.Appearance.ForeColor = Color.Black;
            //parentBand.Columns["VATRATENAME"].Header.Appearance.BorderAlpha = Alpha.Transparent;
            //parentBand.Columns["VATRATENAME"].Header.Appearance.BorderColor = Color.Black;
            //parentBand.Columns["GOODSSERVICEPURCHASENAME"].Header.
            //parentBand.Columns["VATRATENAME"].col


            parentBand.Columns["SO_HD"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["SO_HD"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SO_HD"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["SO_HD"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["SO_HD"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["SO_HD"].Header.Appearance.BorderColor = Color.Black;
            //parentBand.Columns["SO_HD"].Header.Appearance.BorderColor2 = Color.Red;
            //parentBand.Columns["SO_HD"].Header.Appearance.BorderColor3DBase = Color.Orange;
            //parentBand.Columns["SO_HD"].
            parentBand.Columns["SO_HD"].RowLayoutColumnInfo.OriginX = 0;
            parentBand.Columns["SO_HD"].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns["SO_HD"].Width = 170;
            //parentBand.Columns["SO_HD"].RowLayoutColumnInfo.SpanX = parentBand.Columns["TK_THUE"].RowLayoutColumnInfo.SpanX + parentBand.Columns["THUE_GTGT"].RowLayoutColumnInfo.SpanX + parentBand.Columns["THUE_SUAT_STRING"].RowLayoutColumnInfo.SpanX + parentBand.Columns["GT_CHUATHUE"].RowLayoutColumnInfo.SpanX + parentBand.Columns["MAT_HANG"].RowLayoutColumnInfo.SpanX + parentBand.Columns["MST"].RowLayoutColumnInfo.SpanX + parentBand.Columns["NGAY_HD"].RowLayoutColumnInfo.SpanX + parentBand.Columns["SO_HD"].RowLayoutColumnInfo.SpanX;
            //parentBand.Columns["SO_HD"].RowLayoutColumnInfo.SpanY = parentBand.Columns["TK_THUE"].RowLayoutColumnInfo.SpanY;


            parentBand.Columns["NGAY_HD"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["NGAY_HD"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["NGAY_HD"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);

            parentBand.Columns["NGAY_HD"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["NGAY_HD"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["NGAY_HD"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["NGUOI_MUA"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["NGUOI_MUA"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["NGUOI_MUA"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["NGUOI_MUA"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["NGUOI_MUA"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["NGUOI_MUA"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["MST"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["MST"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["MST"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["MST"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["MST"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["MST"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["MAT_HANG"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["MAT_HANG"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["MAT_HANG"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["MAT_HANG"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["MAT_HANG"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["MAT_HANG"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["DOANH_SO"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["DOANH_SO"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["DOANH_SO"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["DOANH_SO"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["DOANH_SO"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["DOANH_SO"].Header.Appearance.BorderColor = Color.Black;

            //parentBand.Columns["THUE_SUAT_STRING"].RowLayoutColumnInfo.ParentGroup = parentBandGroupTHUE_SUAT_STRING;
            //parentBand.Columns["THUE_SUAT_STRING"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            //parentBand.Columns["THUE_SUAT_STRING"].Header.Appearance.BackColor = Color.FromArgb(200, 210, 250);
            //parentBand.Columns["THUE_SUAT_STRING"].Header.Appearance.ForeColor = Color.Black;
            //parentBand.Columns["THUE_SUAT_STRING"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            //parentBand.Columns["THUE_SUAT_STRING"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["THUE_GTGT"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["THUE_GTGT"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["THUE_GTGT"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["THUE_GTGT"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["THUE_GTGT"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["THUE_GTGT"].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns["TKTHUE"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPeriod;
            parentBand.Columns["TKTHUE"].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["TKTHUE"].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns["TKTHUE"].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns["TKTHUE"].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns["TKTHUE"].Header.Appearance.BorderColor = Color.Black;


           // parentBand.Columns["VATRATENAME"].Header.Appearance.BackColorAlpha = Alpha.Transparent;

            // parentBand.Columns["GOODSSERVICEPURCHASENAME"].

            //parentBand.Columns["GOODSSERVICEPURCHASENAME"].Header.VisiblePosition = 1;
            //parentBand.Columns["SO_HD"].Header.VisiblePosition = 0;
            //parentBand.Columns["NGAY_HD"].Header.VisiblePosition = 2;

            //parentBand.Columns["TEN_NBAN"].Header.VisiblePosition = 3;
            //parentBand.Columns["MST"].Header.VisiblePosition = 4;
            //parentBand.Columns["MAT_HANG"].Header.VisiblePosition = 5;
            //parentBand.Columns["GT_CHUATHUE"].Header.VisiblePosition = 6;
            //parentBand.Columns["THUE_SUAT_STRING"].Header.VisiblePosition = 7;
            //parentBand.Columns["THUE_GTGT"].Header.VisiblePosition = 8;
            //parentBand.Columns["TK_THUE"].Header.VisiblePosition = 9;

            //parentBand.Columns["VATRATENAME"].RowLayoutColumnInfo.OriginX = 0;
            //parentBand.Columns["VATRATENAME"].RowLayoutColumnInfo.OriginY = 0;
            //parentBand.Columns["VATRATENAME"].RowLayoutColumnInfo.SpanX = parentBand.Columns["MAT_HANG"].RowLayoutColumnInfo.SpanX + parentBand.Columns["MST"].RowLayoutColumnInfo.SpanX + parentBand.Columns["NGAY_HD"].RowLayoutColumnInfo.SpanX + parentBand.Columns["SO_HD"].RowLayoutColumnInfo.SpanX + parentBand.Columns["NGUOI_MUA"].RowLayoutColumnInfo.SpanX;
            //parentBand.Columns["VATRATENAME"].RowLayoutColumnInfo.SpanY = 2;//parentBand.Columns["TK_THUE"].RowLayoutColumnInfo.SpanY;
            //parentBand.Columns["VATRATENAME"].Header.Appearance.FontData.SizeInPoints = 2;
            //parentBand.Columns["VATRATENAME"].Header.Caption = "";

            parentBand.Columns["Ngay_HD"].Width = 100;
            //UltraGridGroup parentBandGroupKhoangTrong;

            //if (parentBand.Groups.Exists("ParentBandGroupKhoangTrong"))
            //    parentBandGroupKhoangTrong = parentBand.Groups["ParentBandGroupKhoangTrong"];
            //else
            //    parentBandGroupKhoangTrong = parentBand.Groups.Add("ParentBandGroupKhoangTrong", "");
            //parentBandGroupKhoangTrong.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            //parentBandGroupKhoangTrong.Header.Appearance.FontData.SizeInPoints = 2;
            //parentBandGroupKhoangTrong.Header.Appearance.TextHAlign = HAlign.Center;
            ////parentBandGroupKhoangTrong.Header.Appearance.BorderAlpha = Alpha.Transparent;

            ////parentBandGroupSO_HD.Header.Appearance.BorderColor = Color.Black;
            //parentBandGroupKhoangTrong.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            //parentBandGroupKhoangTrong.RowLayoutGroupInfo.SpanY = 2;

            //parentBandGroupKhoangTrong.RowLayoutGroupInfo.ParentGroup = parentBandGroupPeriod;
            //parentBandGroupKhoangTrong.Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            //parentBandGroupKhoangTrong.Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            //parentBandGroupKhoangTrong.Header.Appearance.ForeColor = Color.Black;
            ////parentBandGroupKhoangTrong.Header.Appearance.BorderAlpha = Alpha.Transparent;
            //parentBandGroupKhoangTrong.Header.Appearance.BorderColor = Color.Black;

            //parentBandGroupKhoangTrong.RowLayoutGroupInfo.OriginX = 0;
            //parentBandGroupKhoangTrong.RowLayoutGroupInfo.OriginY = 0;
            //parentBandGroupKhoangTrong.RowLayoutGroupInfo.SpanX = parentBand.Columns["VATRATENAME"].RowLayoutColumnInfo.SpanX + parentBand.Columns["TKTHUE"].RowLayoutColumnInfo.SpanX + parentBand.Columns["DOANH_SO"].RowLayoutColumnInfo.SpanX + parentBand.Columns["THUE_GTGT"].RowLayoutColumnInfo.SpanX ;
            //parentBandGroupKhoangTrong.RowLayoutGroupInfo.SpanY = 2;//parentBand.Columns["TK_THUE"].RowLayoutColumnInfo.SpanY;
            //parentBandGroupKhoangTrong.Header.Appearance.FontData.SizeInPoints = 2;
            //parentBandGroupKhoangTrong.Header.Caption = "";

            this.WindowState = FormWindowState.Maximized;
            if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "IsMinimized").Data == "1")
            {
                this.WindowState = FormWindowState.Normal;


                this.Width = Screen.PrimaryScreen.WorkingArea.Width - 200;
                this.Height = Screen.PrimaryScreen.WorkingArea.Height - 50;
                this.Location = new System.Drawing.Point(((Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2), ((Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2));

            }
            if (Utils.isDemo)
            {
                ultraButton2.Visible = false;
            }
            WaitingFrm.StopWaiting();
        }
        private string GetNameGroup(decimal ma)
        {
            string ten = "";
            switch (ma)
            {
                case -2:
                    ten = "Hàng hoá, dịch vụ không tính thuế GTGT";
                    break;
                case -1:
                    ten = "Hàng hoá, dịch vụ không chịu thuế GTGT";
                    break;
                case 0:
                    ten = "Hàng hoá, dịch vụ chịu thuế GTGT 0%";
                    break;
                case 5:
                    ten = "Hàng hoá, dịch vụ chịu thuế GTGT 5%";
                    break;
                case 10:
                    ten = "Hàng hoá, dịch vụ chịu thuế GTGT 10%";
                    break;
                default:
                    break;
            }
            return ten;
        }

        private void uGridDuLieu_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            if (check1 == 0)
            {
                if (e.Row.ListObject != null)
                {
                    if (e.Row.ListObject.HasProperty("RefType"))
                    {
                        if (!e.Row.ListObject.GetProperty("RefType").IsNullOrEmpty())
                        {
                            int typeid = e.Row.ListObject.GetProperty("RefType").ToInt();

                            if (e.Row.ListObject.HasProperty("RefID"))
                            {
                                if (!e.Row.ListObject.GetProperty("RefID").IsNullOrEmpty())
                                {
                                    Guid id = (Guid)e.Row.ListObject.GetProperty("RefID");
                                    var f = Utils.ViewVoucherSelected1(id, typeid);
                                    //this.Close();
                                    //if (f.IsDisposed)
                                    //{
                                    //    new FRBaoCaoBangKeBanraTruocIn(data1, tieude1, check1, begin1, end1).Show();
                                    //}
                                }
                            }
                        }

                    }
                }
            }
            else
            {
                List<BangKeBanRa_Model> list = new List<BangKeBanRa_Model>();
                ReportProcedureSDS sp = new ReportProcedureSDS();
                list = sp.GetProc_GetBkeInv_Sale_QT(begin1, end1, 0, 0, "");
                //data = sp.GetProc_GetBkeInv_Sale((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, check, 0, "");
                list.ForEach(
                t =>
                {
                    t.VATRATENAME = GetNameGroup(t.VATRATE);
                    if (t.FLAG == -1)
                    {
                        t.DOANH_SO = -t.DOANH_SO;
                        t.THUE_GTGT = -t.THUE_GTGT;
                    }
                });
                decimal? vatrate = null;
               // string GoodsServicePurchaseName = null;
                string sohd = null;
                DateTime? NgayHD = null; string nguoimua = null; string mst = null; string mathang = null; string tkthue = null; int? flag = null;
                if (e.Row.ListObject.HasProperty("VATRATE"))
                {

                    if (e.Row.ListObject.GetProperty("VATRATE") == null)
                    {
                        vatrate = null;
                    }
                    else
                    {
                        vatrate =(decimal)e.Row.ListObject.GetProperty("VATRATE");
                    }
                }
                //if (e.Row.ListObject.HasProperty("GOODSSERVICEPURCHASENAME"))
                //{

                //    if (e.Row.ListObject.GetProperty("GOODSSERVICEPURCHASENAME") == null)
                //    {
                //        GoodsServicePurchaseName = null;
                //    }
                //    else
                //    {
                //        GoodsServicePurchaseName = e.Row.ListObject.GetProperty("GOODSSERVICEPURCHASENAME").ToString();
                //    }
                //}
                if (e.Row.ListObject.HasProperty("SO_HD"))
                {

                    if (e.Row.ListObject.GetProperty("SO_HD") == null)
                    {
                        sohd = null;
                    }
                    else
                    {
                        sohd = e.Row.ListObject.GetProperty("SO_HD").ToString();
                    }
                }
                if (e.Row.ListObject.HasProperty("NGAY_HD"))
                {
                    if (e.Row.ListObject.GetProperty("NGAY_HD") == null)
                    {
                        NgayHD = null;
                    }
                    else
                    {
                        NgayHD = (DateTime)e.Row.ListObject.GetProperty("NGAY_HD");
                    }

                }

                if (e.Row.ListObject.HasProperty("NGUOI_MUA"))
                {

                    if (e.Row.ListObject.GetProperty("NGUOI_MUA") == null)
                    {
                        nguoimua = null;
                    }
                    else
                    {
                        nguoimua = e.Row.ListObject.GetProperty("NGUOI_MUA").ToString();
                    }

                }
                if (e.Row.ListObject.HasProperty("MST"))
                {

                    if (e.Row.ListObject.GetProperty("MST") == null)
                    {
                        mst = null;
                    }
                    else
                    {
                        mst = e.Row.ListObject.GetProperty("MST").ToString();
                    }

                }
                if (e.Row.ListObject.HasProperty("MAT_HANG"))
                {

                    if (e.Row.ListObject.GetProperty("MAT_HANG") == null)
                    {
                        mathang = null;
                    }
                    else
                    {
                        mathang = e.Row.ListObject.GetProperty("MAT_HANG").ToString();
                    }

                }
                if (e.Row.ListObject.HasProperty("TKTHUE"))
                {

                    if (e.Row.ListObject.GetProperty("TKTHUE") == null)
                    {
                        tkthue = null;
                    }
                    else
                    {
                        tkthue = e.Row.ListObject.GetProperty("TKTHUE").ToString();
                    }

                }
                if (e.Row.ListObject.HasProperty("FLAG"))
                {

                    if (e.Row.ListObject.GetProperty("FLAG") == null)
                    {
                        flag = null;
                    }
                    else
                    {
                        flag = (int)e.Row.ListObject.GetProperty("FLAG");
                    }

                }
                //if (e.Row.ListObject.HasProperty("THUE_SUAT"))
                //{

                //    if (e.Row.ListObject.GetProperty("THUE_SUAT") == null)
                //    {
                //        thuesuat = null;
                //    }
                //    else
                //    {
                //        thuesuat = (decimal)e.Row.ListObject.GetProperty("THUE_SUAT");
                //    }

                //}
                List<BangKeBanRa_Model> list1 = new List<BangKeBanRa_Model>();
                //list1.ForEach(t =>
                //{
                //    t.THUE_SUAT_DETAIL = Convert.ToInt32(t.THUE_SUAT);
                //    if (t.FLAG == -1)
                //    {
                //        t.GT_CHUATHUE = -t.GT_CHUATHUE;
                //        t.THUE_GTGT = -t.THUE_GTGT;
                //    }
                //});
                list1 = list.Where(n => n.VATRATE == vatrate && n.FLAG == flag && n.TKTHUE == tkthue && n.MST == mst && n.NGUOI_MUA == nguoimua && n.NGAY_HD == NgayHD && n.SO_HD == sohd ).ToList();
                if (list1.Count == 0)
                {
                    MSG.Information("Không có chứng từ");
                }
                else
                {
                    if (list1.Count == 1)
                    {


                        if (!list1[0].RefType.IsNullOrEmpty())
                        {
                            int typeid = list1[0].RefType;



                            if (!list1[0].RefID.IsNullOrEmpty())
                            {
                                string refid = list1[0].RefID.ToString();
                                Guid id = Guid.Parse(refid);
                                var f = Utils.ViewVoucherSelected1(id, typeid);
                                //this.Close();
                                ////this.Close();
                                //if (f.IsDisposed)
                                //{
                                //    new FRBaoCaoBangKeBanraTruocIn(data1, tieude1, check1, begin1, end1).Show();
                                //}
                            }

                        }


                    }
                    else
                    {
                        var f = new DanhSachChungTuBanRa(list1);
                        f.Show();
                    }
                }


            }
        }

        private void ultraGridExcelExporter1_InitializeColumn(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.InitializeColumnEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Column.Format))
            {
                e.ExcelFormatStr = e.Column.Format;

            }
            //if (e.Column.Key == "VATRATENAME")
            //{
            //    e.Column.Header.Appearance.BackColorAlpha = Alpha.Transparent;
            //}

        }
        private void SetDoSoAm(UltraGrid ultraGrid)
        {
            foreach (var item in ultraGrid.Rows)
            {
                if ((int)item.Cells["Ordercode"].Value == -1)
                {
                    
                    item.Cells["SO_HD"].Appearance.FontData.Bold = DefaultableBoolean.True;
                    
                    //  item.Cells["Ngay_HD"].Hidden = true;

                }
                else
                {
                    if ((int)item.Cells["Ordercode"].Value == 0)
                    {
                        
                    }
                    if ((int)item.Cells["Ordercode"].Value == 1)
                    {
                       
                        item.Cells["SO_HD"].Appearance.FontData.Bold = DefaultableBoolean.True;
                        item.Cells["THUE_GTGT"].Appearance.FontData.Bold = DefaultableBoolean.True;
                        item.Cells["DOANH_SO"].Appearance.FontData.Bold = DefaultableBoolean.True;
                        
                    }
                }
                if ((decimal)item.Cells["DOANH_SO"].Value < 0)
                    item.Cells["DOANH_SO"].Appearance.ForeColor = Color.Red;


                if ((decimal)item.Cells["THUE_GTGT"].Value < 0)
                    item.Cells["THUE_GTGT"].Appearance.ForeColor = Color.Red;

            }
        }

        private void ultraButton2_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog
            {
                Title = "Chọn nơi sao lưu ",
                Filter = "File (*.xls)|*.xls",
                InitialDirectory = "@C:\\",
                FileName = "BaoCaoBangKeBanRa.xls"
            };
            if (save.ShowDialog() == DialogResult.OK)
            {
                string duongdan = save.FileName;
                try
                {
                    ultraGridExcelExporter1.Export(uGridDuLieu, duongdan);
                    if (MSG.Question("Kết xuất thành công, Bạn có muốn mở file vừa kết xuất không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(save.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
                catch
                {
                    MSG.Warning("Lỗi khi kết xuất !");
                }
            }
        }

        private void ultraButton3_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ultraButton1_Click(object sender, EventArgs e)
        {
            
            ReportProcedureSDS sp = new ReportProcedureSDS();
            List<BangKeBanRa_Model> data = new List<BangKeBanRa_Model>();
            
            data = sp.GetProc_GetBkeInv_Sale_QT(begin1, end1, check1, 0, "");
            data.ForEach(
                t =>
                {
                    t.VATRATENAME = GetNameGroup(t.VATRATE);
                    if (t.FLAG == -1)
                    {
                        t.DOANH_SO = -t.DOANH_SO;
                        t.THUE_GTGT = -t.THUE_GTGT;
                    }
                });
            if (data.Count == 0)
            {
                MSG.Warning("Không có dữ liệu để lên báo cáo!");
            }
            else
            {
                var rD = new BangKeBanRa_ModelDetail();
                rD.Period = ReportUtils.GetPeriod(begin1, end1);
                var f = new ReportForm<BangKeBanRa_Model>(fileReportSlot1, "");
                f.AddDatasource("DBBANGKEBANRA", data.OrderByDescending(n => n.VATRATE).ThenBy(n => n.NGAY_HD).ThenBy(n => n.SO_HD).ToList());
                f.AddDatasource("Detail", rD);
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show();
            }
        }

        private void uGridDuLieu_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            UltraGridCell cell = e.Row.Cells[0];
            cell.Row.Height = 25;
        }

        private void ultraGridExcelExporter1_SummaryCellExported(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.SummaryCellExportedEventArgs e)
        {
            Worksheet ws = e.CurrentWorksheet;
            WorksheetRow wsRow = ws.Rows[e.CurrentRowIndex];
            WorksheetCell wsCell = wsRow.Cells[e.CurrentColumnIndex];
            if (!e.Summary.SummaryText.Contains("Số dòng") && !e.Summary.SummaryText.Contains(" "))
            {
                string s = e.Summary.SummaryText.Trim('.');
                if (s.Contains("("))
                    wsCell.Value = -decimal.Parse(s.Trim('(').Trim(')'));
                else
                    wsCell.Value = decimal.Parse(s);

                wsCell.CellFormat.FormatString = "###,###,###,###,##0;(###,###,###,###,##0)";
            }
        }

        private void ultraGridExcelExporter1_BeginExport(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.BeginExportEventArgs e)
        {
            foreach (var row in uGridDuLieu.Rows)
            {
                foreach (var column in uGridDuLieu.DisplayLayout.Bands[0].Columns)
                {
                    e.Rows[row.Index].Cells[column].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                    e.Rows[row.Index].Cells[column].Appearance.BorderColor = Color.Black;
                    e.Rows[row.Index].Cells[column].Column.Width = 200;
                }
            }
            e.Rows.Band.Summaries[0].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[0].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[1].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[1].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[2].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[2].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[3].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[3].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[4].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[4].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[5].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[5].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[6].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[6].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[7].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[7].Appearance.BorderColor = Color.Black;

        }
    }
}
