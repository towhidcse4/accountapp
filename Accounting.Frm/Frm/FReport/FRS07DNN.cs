﻿using System;
using System.Collections.Generic;
using System.Linq;
using FX.Core;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using Accounting.Frm.FReport;

namespace Accounting
{
    public partial class FRS07DNN : CustormForm
    {
        readonly List<MaterialGoodsReport> _lstnew = ReportUtils.LstMaterialGoodsReport;
        private List<MaterialGoodsReport> _lstMaterialGoodsReports = new List<MaterialGoodsReport>();
        //readonly List<MaterialGoodsReport> _lstMaterialGoods = ReportUtils.LstMaterialGoodsReport;
        string _subSystemCode;
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        public FRS07DNN(string subSystemCode)
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();
            _subSystemCode = subSystemCode;
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\S07-DNN.rst", path);
            ugridMaterialGoodsCategory.SetDataBinding(_lstMaterialGoodsReports.ToList(), "");
            ReportUtils.ProcessControls(this);
            btnExit.Click += btnExit_Click;
            btnOk.Click += btnOk_Click;
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dtBeginDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }

            WaitingFrm.StopWaiting();
        }

        #region lọc nhóm nhà cung cấp
        private bool _check1 = false;
        private bool _check2 = false;
        private void cbbMaterialGoodsCategory1_ValueChanged(object sender, EventArgs e)
        {
            if (cbbMaterialGoodsCategory1.Text != null && !string.IsNullOrEmpty(cbbMaterialGoodsCategory1.Text))
            {
                _check1 = ((MaterialGoodsCategory)cbbMaterialGoodsCategory1.SelectedRow.ListObject).MaterialGoodsCategoryCode != "Tất cả";//edit by cuongpv 20190419 TC->Tất cả
                Filter();
            }
        }

        private void cbbRepository_ValueChanged(object sender, EventArgs e)
        {
            if (cbbRepository.Text != null && !string.IsNullOrEmpty(cbbRepository.Text))
            {
                _check2 = ((Repository)cbbRepository.SelectedRow.ListObject).RepositoryCode != "Tất cả";//edit by cuongpv 20190419 TC->Tất cả
                Filter();
            }
        }

        private void Filter()
        {
            _lstMaterialGoodsReports = _lstnew;
            MaterialGoodsCategory materialGoodsCategory;
            Repository repository;
            List<Guid> lstID;
            if (_check1 && !_check2)
            {
                materialGoodsCategory = (MaterialGoodsCategory)cbbMaterialGoodsCategory1.SelectedRow.ListObject;
                _lstMaterialGoodsReports = _lstMaterialGoodsReports.Where(c => c.MaterialGoodsCategoryID == materialGoodsCategory.ID).ToList();
            }
            else if (_check2 && !_check1)
            {
                repository = (Repository)cbbRepository.SelectedRow.ListObject;
                lstID = Utils.ListRepositoryLedger.Where(x => x.RepositoryID == repository.ID).Select(x => x.MaterialGoodsID).ToList();
                _lstMaterialGoodsReports = _lstMaterialGoodsReports.Where(c => lstID.Any(d => d == c.ID)).ToList();
            }
            else if (_check1 && _check2)
            {
                repository = (Repository)cbbRepository.SelectedRow.ListObject;
                lstID = Utils.ListRepositoryLedger.Where(x => x.RepositoryID == repository.ID).Select(x => x.MaterialGoodsID).ToList();
                materialGoodsCategory = (MaterialGoodsCategory)cbbMaterialGoodsCategory1.SelectedRow.ListObject;
                _lstMaterialGoodsReports = _lstMaterialGoodsReports.Where(c => c.MaterialGoodsCategoryID == materialGoodsCategory.ID && lstID.Any(d => d == c.ID)).ToList();
            }
            ugridMaterialGoodsCategory.SetDataBinding(_lstMaterialGoodsReports.ToList(), "");
        }
        #endregion

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }

            if ((DateTime)dtBeginDate.Value > (DateTime)dtEndDate.Value)
            {
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            WaitingFrm.StartWaiting();
            List<S07DNN> data = new List<S07DNN>();
            var repositoryLedger = IoC.Resolve<IRepositoryLedgerService>();
            List<Guid> lstGuidMaterialGoodsCategory = new List<Guid>();
            if (((MaterialGoodsCategory)cbbMaterialGoodsCategory1.SelectedRow.ListObject).MaterialGoodsCategoryCode == "Tất cả")//edit by cuongpv 20190419 TC->Tất cả
            {
                foreach (var item in cbbMaterialGoodsCategory1.Rows)
                {
                    if (((MaterialGoodsCategory)item.ListObject).ID != Guid.Empty)
                        lstGuidMaterialGoodsCategory.Add(((MaterialGoodsCategory)item.ListObject).ID);
                }
            }
            lstGuidMaterialGoodsCategory.Add(((MaterialGoodsCategory)cbbMaterialGoodsCategory1.SelectedRow.ListObject).ID);
            data = repositoryLedger.RS07DNN((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value,
                ((Repository)cbbRepository.SelectedRow.ListObject).ID, _lstMaterialGoodsReports.Where(c => c.Check).Select(c => c.ID).ToList(), chk.Checked);
            WaitingFrm.StopWaiting();
            //for (int i = 0; i < 200; i++)
            //{
            //    data.Add(new S07DNN(i));
            //}
            var rd = new S07DNNDetail();
            rd.Period = ReportUtils.GetPeriod(dtBeginDate.DateTime, dtEndDate.DateTime);
            var f = new ReportForm<S07DNN>(fileReportSlot1, _subSystemCode);
            f.AddDatasource("S07DNN", data, true);
            f.AddDatasource("detail", rd);
            f.SetHyperlinkFunction("Accounting.ReportUtils", "Hyperlink");
            f.LoadReport();
            f.WindowState = FormWindowState.Maximized;
            f.Show();
            {
                foreach (var item in ugridMaterialGoodsCategory.Rows)
                {
                    item.Cells["Check"].Value = false;
                }
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            foreach (var item in ugridMaterialGoodsCategory.Rows)
            {
                item.Cells["Check"].Value = false;
            }
            Close();
        }

        private void ultraButton2_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }

            if ((DateTime)dtBeginDate.Value > (DateTime)dtEndDate.Value)
            {
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }
            WaitingFrm.StartWaiting();
            List<S07DNN> data = new List<S07DNN>();
            var repositoryLedger = IoC.Resolve<IRepositoryLedgerService>();
            List<Guid> lstGuidMaterialGoodsCategory = new List<Guid>();
            if (((MaterialGoodsCategory)cbbMaterialGoodsCategory1.SelectedRow.ListObject).MaterialGoodsCategoryCode == "Tất cả")//edit by cuongpv 20190419 TC->Tất cả
            {
                foreach (var item in cbbMaterialGoodsCategory1.Rows)
                {
                    if (((MaterialGoodsCategory)item.ListObject).ID != Guid.Empty)
                        lstGuidMaterialGoodsCategory.Add(((MaterialGoodsCategory)item.ListObject).ID);
                }
            }
            lstGuidMaterialGoodsCategory.Add(((MaterialGoodsCategory)cbbMaterialGoodsCategory1.SelectedRow.ListObject).ID);
            data = repositoryLedger.RS07DNN((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value,
                ((Repository)cbbRepository.SelectedRow.ListObject).ID, _lstMaterialGoodsReports.Where(c => c.Check).Select(c => c.ID).ToList(), chk.Checked);
            WaitingFrm.StopWaiting();
            //for (int i = 0; i < 200; i++)
            //{
            //    data.Add(new S07DNN(i));
            //}
            var rd = new S07DNNDetail();
            rd.Period = ReportUtils.GetPeriod(dtBeginDate.DateTime, dtEndDate.DateTime);
            new FRS07DNNTruocIn(data).Show();
        }
    }
}
