﻿using Accounting.Core.DAO;
using Accounting.Core.Domain.obj.Report;
using Accounting.TextMessage;
using Infragistics.Documents.Excel;
using Infragistics.Win;
using Infragistics.Win.FormattedLinkLabel;
using Infragistics.Win.UltraWinGrid;
using PerpetuumSoft.Reporting.Components;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FChiTietCongNoPhaiTraNhaCCTruocIn : Form
    {
        List<PO_PayDetailNCC> data1 = new List<PO_PayDetailNCC>();
        string tai_khoan;
        string loai_tien;
        DateTime begin1;
        DateTime end1;
        string ds_ncc;
        List<PO_PayDetailNCC1> data2 = new List<PO_PayDetailNCC1>();
        string _subSystemCode = "";
        string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
        
        public FChiTietCongNoPhaiTraNhaCCTruocIn(List<PO_PayDetailNCC> data, string taikhoan, string loaitien, DateTime begin, DateTime end, string ds_ncc )
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();
            //data.ForEach(t =>
            //{
            //    t.OpeningDebitAmount = Covert_Money(t.OpeningDebitAmount);
            //    t.OpeningCreditAmount = Covert_Money(t.OpeningCreditAmount);
            //    t.DebitAmount = Covert_Money(t.DebitAmount);
            //    t.CreditAmount = Covert_Money(t.CreditAmount);
            //    t.ClosingDebitAmount = Covert_Money(t.ClosingDebitAmount);
            //    t.ClosingCreditAmount = Covert_Money(t.ClosingCreditAmount);
            //});
            ReportProcedureSDS sp = new ReportProcedureSDS();
            loai_tien = loaitien;
            this.ds_ncc = ds_ncc;
            data2 = sp.Get_SoChitietPhaTra_NCC1(begin, end, taikhoan, loaitien, ds_ncc);
            
            GanTongDuNoDuCo(data2, loaitien);
            ugridDuLieu.DataSource = data2.ToList();
            Utils.ConfigGrid(ugridDuLieu, ConstDatabase.PO_PayDetailNCC1_TableName);
            ugridDuLieu.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            Utils.AddSumColumn(ugridDuLieu, "SoCtu", false);
            Utils.AddSumColumn(ugridDuLieu, "AccountObjectName", false);
            Utils.AddSumColumn(ugridDuLieu, "ngayCtu", false);
            Utils.AddSumColumn(ugridDuLieu, "DienGiai", false);
            Utils.AddSumColumn(ugridDuLieu, "TK_CONGNO", false);
            Utils.AddSumColumn(ugridDuLieu, "TkDoiUng", false);

            ugridDuLieu.DisplayLayout.Bands[0].Summaries[1].DisplayFormat = " ";
            ugridDuLieu.DisplayLayout.Bands[0].Summaries[2].DisplayFormat = " ";
            ugridDuLieu.DisplayLayout.Bands[0].Summaries[3].DisplayFormat = " ";
            ugridDuLieu.DisplayLayout.Bands[0].Summaries[4].DisplayFormat = " ";
            ugridDuLieu.DisplayLayout.Bands[0].Summaries[5].DisplayFormat = " ";
            ugridDuLieu.DisplayLayout.Bands[0].Summaries[6].DisplayFormat = " ";
            TinhTongDuNoDuCo(data);
            // ugridDuLieu.DisplayLayout.Override.AllowRowSummaries = AllowRowSummaries.False;
            if (loaitien == "VND")
            {
                ugridDuLieu.DisplayLayout.Bands[0].Columns["SoCtu"].Width = 135;
                ugridDuLieu.DisplayLayout.Bands[0].Columns["AccountObjectName"].Width = 208;
                ugridDuLieu.DisplayLayout.Bands[0].Columns["DebitAmount"].Width = 108;
                ugridDuLieu.DisplayLayout.Bands[0].Columns["CreditAmount"].Width = 108;
                ugridDuLieu.DisplayLayout.Bands[0].Columns["ClosingDebitAmount"].Width = 108;
                ugridDuLieu.DisplayLayout.Bands[0].Columns["ClosingCreditAmount"].Width = 108;
                ugridDuLieu.DisplayLayout.Bands[0].Columns["DebitAmount"].Format= "##,#"; 
                ugridDuLieu.DisplayLayout.Bands[0].Columns["CreditAmount"].Format = "##,#";
                ugridDuLieu.DisplayLayout.Bands[0].Columns["ClosingDebitAmount"].Format = "##,#";
                ugridDuLieu.DisplayLayout.Bands[0].Columns["ClosingCreditAmount"].Format = "##,#";
                ugridDuLieu.DisplayLayout.Bands[0].Columns["DebitAmount"].CellAppearance.TextHAlign=HAlign.Right;
                ugridDuLieu.DisplayLayout.Bands[0].Columns["CreditAmount"].CellAppearance.TextHAlign = HAlign.Right;
                ugridDuLieu.DisplayLayout.Bands[0].Columns["ClosingDebitAmount"].CellAppearance.TextHAlign = HAlign.Right;
                ugridDuLieu.DisplayLayout.Bands[0].Columns["ClosingCreditAmount"].CellAppearance.TextHAlign = HAlign.Right;
                ugridDuLieu.DisplayLayout.Bands[0].Columns["DebitAmountOC"].Hidden = true;
                ugridDuLieu.DisplayLayout.Bands[0].Columns["CreditAmountOC"].Hidden = true;
                ugridDuLieu.DisplayLayout.Bands[0].Columns["ClosingDebitAmountOC"].Hidden = true;
                ugridDuLieu.DisplayLayout.Bands[0].Columns["ClosingCreditAmountOC"].Hidden = true;
                //Utils.AddSumColumn(ugridDuLieu, "ClosingCreditAmount", false);
                //Utils.AddSumColumn(ugridDuLieu, "ClosingDebitAmount", false);
               
                
                //AddSumColumn(ugridDuLieu, "abc");
                //ugridDuLieu.DisplayLayout.Bands[0].Summaries[3].DisplayFormat = "trungnq";
                //ugridDuLieu.DisplayLayout.Bands[0].Summaries.Add("a", "123456789");
                
                ugridDuLieu.DisplayLayout.Bands[0].Columns["Sum_DuCo"].Hidden = true;
                ugridDuLieu.DisplayLayout.Bands[0].Columns["Sum_DuNo"].Hidden = true;
            }
            else
            {
                ugridDuLieu.DisplayLayout.Bands[0].Columns["SoCtu"].Width = 135;
                ugridDuLieu.DisplayLayout.Bands[0].Columns["AccountObjectName"].Width = 208;
                ugridDuLieu.DisplayLayout.Bands[0].Columns["DebitAmountOC"].Width = 108;
                ugridDuLieu.DisplayLayout.Bands[0].Columns["CreditAmountOC"].Width = 108;
                ugridDuLieu.DisplayLayout.Bands[0].Columns["ClosingDebitAmountOC"].Width = 108;
                ugridDuLieu.DisplayLayout.Bands[0].Columns["ClosingCreditAmountOC"].Width = 108;
                ugridDuLieu.DisplayLayout.Bands[0].Columns["DebitAmountOC"].Format = "##,#";
                ugridDuLieu.DisplayLayout.Bands[0].Columns["CreditAmountOC"].Format = "##,#";
                ugridDuLieu.DisplayLayout.Bands[0].Columns["ClosingDebitAmountOC"].Format = "##,#";
                ugridDuLieu.DisplayLayout.Bands[0].Columns["ClosingCreditAmountOC"].Format = "##,#";
                ugridDuLieu.DisplayLayout.Bands[0].Columns["DebitAmountOC"].CellAppearance.TextHAlign = HAlign.Right;
                ugridDuLieu.DisplayLayout.Bands[0].Columns["CreditAmountOC"].CellAppearance.TextHAlign = HAlign.Right;
                ugridDuLieu.DisplayLayout.Bands[0].Columns["ClosingDebitAmountOC"].CellAppearance.TextHAlign = HAlign.Right;
                ugridDuLieu.DisplayLayout.Bands[0].Columns["ClosingCreditAmountOC"].CellAppearance.TextHAlign = HAlign.Right;
                ugridDuLieu.DisplayLayout.Bands[0].Columns["DebitAmount"].Hidden = true;
                ugridDuLieu.DisplayLayout.Bands[0].Columns["CreditAmount"].Hidden = true;
                ugridDuLieu.DisplayLayout.Bands[0].Columns["ClosingDebitAmount"].Hidden = true;
                ugridDuLieu.DisplayLayout.Bands[0].Columns["ClosingCreditAmount"].Hidden = true;
                //Utils.AddSumColumn(ugridDuLieu, "ClosingCreditAmountOC", false);
                //Utils.AddSumColumn(ugridDuLieu, "ClosingDebitAmountOC", false);
             //   SummarySettings summarySettings = ugridDuLieu.DisplayLayout.Bands[0].Summaries[0];
                
                
                //ugridDuLieu.DisplayLayout.Bands[0].Summaries["SumClosingCreditAmountOC"].GroupBySummaryValueAppearance.
                //Utils.AddSumColumn(ugridDuLieu, "Sum_DuCo", false);
                //Utils.AddSumColumn(ugridDuLieu, "Sum_DuNo", false);
                ugridDuLieu.DisplayLayout.Bands[0].Columns["Sum_DuCo"].Hidden = true;
                ugridDuLieu.DisplayLayout.Bands[0].Columns["Sum_DuNo"].Hidden = true;
            }
            //ugridDuLieu.DisplayLayout.Bands[0].Summaries.RemoveAt(0);
            ugridDuLieu.DisplayLayout.Bands[0].Override.GroupByRowDescriptionMask = "[caption] :[value] ([count] Dòng dữ liệu)";
            // ugridDuLieu.DisplayLayout.Bands[0].SummaryFooterCaption = "fuck";
            
      //      TinhTongDuNoDuCo(data);
            InDamGridVaGanGiaTriCongNhom(ugridDuLieu,data2);
           
             data1 = data;
            tai_khoan = taikhoan;
            
            begin1 = begin;
            end1 = end;
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\ChiTietCongNoPhaiTraNCC.rst", path);
            UltraGridBand parentBand = this.ugridDuLieu.DisplayLayout.Bands[0];
            parentBand.RowLayoutStyle = RowLayoutStyle.GroupLayout;
            UltraGridGroup parentBandGroup1 = parentBand.Groups.Add("ParentBandGroup1", "CHI TIẾT CÔNG NỢ PHẢI TRẢ");
            parentBandGroup1.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            string tieude = string.Format("Tài khoản: {0}; Loại tiền: {1}; Từ ngày {2}/{3}/{4} đến ngày {5}/{6}/{7}",taikhoan, loaitien,begin.Day.ToString(),begin.Month.ToString(),begin.Year.ToString(),end.Day.ToString(),end.Month.ToString(),end.Year.ToString());
            UltraGridGroup parentBandGroup = parentBand.Groups.Add("ParentBandGroup", tieude);
            parentBandGroup.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroup.RowLayoutGroupInfo.ParentGroup = parentBandGroup1;

            UltraGridGroup parentBandGroupPS = parentBand.Groups.Add("ParentBandGroupPS", "Phát sinh");
            parentBandGroupPS.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroupPS.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupPS.RowLayoutGroupInfo.ParentGroup = parentBandGroup;

            UltraGridGroup parentBandGroupSD = parentBand.Groups.Add("ParentBandGroupCK", "Số dư");
            parentBandGroupSD.Header.Appearance.FontData.Bold = DefaultableBoolean.True;
            parentBandGroupSD.Header.Appearance.TextHAlign = HAlign.Center;
            parentBandGroupSD.RowLayoutGroupInfo.ParentGroup = parentBandGroup;

            parentBandGroupSD.Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBandGroupSD.Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBandGroupSD.Header.Appearance.ForeColor = Color.Black;
            parentBandGroupSD.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupSD.Header.Appearance.BorderColor = Color.Black;

            parentBandGroupPS.Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPS.Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBandGroupPS.Header.Appearance.ForeColor = Color.Black;
            parentBandGroupPS.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroupPS.Header.Appearance.BorderColor = Color.Black;

            parentBandGroup.Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBandGroup.Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBandGroup.Header.Appearance.ForeColor = Color.Black;
            parentBandGroup.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroup.Header.Appearance.BorderColor = Color.Black;

            parentBandGroup1.Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBandGroup1.Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBandGroup1.Header.Appearance.ForeColor = Color.Black;
            parentBandGroup1.Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBandGroup1.Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[0].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[0].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[0].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[0].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[0].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[1].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[1].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[1].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[1].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[1].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[2].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[2].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[2].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[2].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[2].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[3].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[3].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[3].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[3].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[3].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[4].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[4].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[4].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[4].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[4].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[5].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[5].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[5].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[5].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[5].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[6].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[6].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[6].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[6].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[6].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[7].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[7].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[7].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[7].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[7].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[8].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[8].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[8].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[8].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[8].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[9].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[9].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[9].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[9].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[9].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[10].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[10].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[10].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[10].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[10].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[11].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[11].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[11].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[11].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[11].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[12].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[12].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[12].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[12].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[12].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[13].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[13].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[13].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[13].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[13].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[14].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[14].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[14].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[14].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[14].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[15].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[15].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[15].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[15].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[15].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[16].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[16].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[16].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[16].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[16].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[17].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[17].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[17].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[17].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[17].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[18].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[18].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[18].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[18].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[18].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[19].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[19].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[19].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[19].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[19].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[20].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[20].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[20].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[20].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[20].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[21].Header.Appearance.BackColorAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[21].Header.Appearance.BackColor = Color.FromArgb(219, 229, 241);
            parentBand.Columns[21].Header.Appearance.ForeColor = Color.Black;
            parentBand.Columns[21].Header.Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            parentBand.Columns[21].Header.Appearance.BorderColor = Color.Black;

            parentBand.Columns[0].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[1].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[2].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[3].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[4].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[5].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[6].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[7].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[8].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[9].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[10].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[11].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[12].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[13].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[14].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[15].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[16].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[17].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[18].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[19].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            parentBand.Columns[21].RowLayoutColumnInfo.ParentGroup = parentBandGroup;

            ugridDuLieu.DisplayLayout.Bands[0].Columns["DebitAmountOC"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPS;
            ugridDuLieu.DisplayLayout.Bands[0].Columns["CreditAmountOC"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPS;
            ugridDuLieu.DisplayLayout.Bands[0].Columns["ClosingDebitAmountOC"].RowLayoutColumnInfo.ParentGroup = parentBandGroupSD;
            ugridDuLieu.DisplayLayout.Bands[0].Columns["ClosingCreditAmountOC"].RowLayoutColumnInfo.ParentGroup = parentBandGroupSD;
            ugridDuLieu.DisplayLayout.Bands[0].Columns["DebitAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPS;
            ugridDuLieu.DisplayLayout.Bands[0].Columns["CreditAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupPS;
            ugridDuLieu.DisplayLayout.Bands[0].Columns["ClosingDebitAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupSD;
            ugridDuLieu.DisplayLayout.Bands[0].Columns["ClosingCreditAmount"].RowLayoutColumnInfo.ParentGroup = parentBandGroupSD;

            //"Ngay_HT", "ngayCtu", "SoCtu","DienGiai", "TK_CONGNO", "TkDoiUng", "AccountObjectCode", "AccountObjectName", "AccountNumber"
            parentBand.Columns[1].RowLayoutColumnInfo.SpanY = parentBand.Columns[1].RowLayoutColumnInfo.SpanY + parentBandGroupPS.RowLayoutGroupInfo.SpanY;
            parentBand.Columns[0].RowLayoutColumnInfo.SpanY = parentBand.Columns[0].RowLayoutColumnInfo.SpanY + parentBandGroupPS.RowLayoutGroupInfo.SpanY;
            parentBand.Columns[2].RowLayoutColumnInfo.SpanY = parentBand.Columns[2].RowLayoutColumnInfo.SpanY + parentBandGroupPS.RowLayoutGroupInfo.SpanY;
            parentBand.Columns[3].RowLayoutColumnInfo.SpanY = parentBand.Columns[3].RowLayoutColumnInfo.SpanY + parentBandGroupPS.RowLayoutGroupInfo.SpanY;
            parentBand.Columns[4].RowLayoutColumnInfo.SpanY = parentBand.Columns[4].RowLayoutColumnInfo.SpanY + parentBandGroupPS.RowLayoutGroupInfo.SpanY;
            parentBand.Columns[5].RowLayoutColumnInfo.SpanY = parentBand.Columns[5].RowLayoutColumnInfo.SpanY + parentBandGroupPS.RowLayoutGroupInfo.SpanY;
            parentBand.Columns[6].RowLayoutColumnInfo.SpanY = parentBand.Columns[6].RowLayoutColumnInfo.SpanY + parentBandGroupPS.RowLayoutGroupInfo.SpanY;
            parentBand.Columns[7].RowLayoutColumnInfo.SpanY = parentBand.Columns[7].RowLayoutColumnInfo.SpanY + parentBandGroupPS.RowLayoutGroupInfo.SpanY;
            parentBand.Columns[8].RowLayoutColumnInfo.SpanY = parentBand.Columns[8].RowLayoutColumnInfo.SpanY + parentBandGroupPS.RowLayoutGroupInfo.SpanY;

            parentBand.Columns[0].RowLayoutColumnInfo.OriginX = 0;
            parentBand.Columns[0].RowLayoutColumnInfo.OriginY = 0;
            //parentBand.Columns[0].RowLayoutColumnInfo.SpanX = 40;
            parentBand.Columns[1].RowLayoutColumnInfo.OriginX = parentBand.Columns[0].RowLayoutColumnInfo.SpanX;
            parentBand.Columns[1].RowLayoutColumnInfo.OriginY = 0;
            //parentBand.Columns[1].RowLayoutColumnInfo.SpanX = 40;
            parentBand.Columns[2].RowLayoutColumnInfo.OriginX = parentBand.Columns[1].RowLayoutColumnInfo.OriginX + parentBand.Columns[1].RowLayoutColumnInfo.SpanX;
            parentBand.Columns[2].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns[3].RowLayoutColumnInfo.OriginX = parentBand.Columns[2].RowLayoutColumnInfo.OriginX + parentBand.Columns[2].RowLayoutColumnInfo.SpanX;
            parentBand.Columns[3].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns[4].RowLayoutColumnInfo.OriginX = parentBand.Columns[3].RowLayoutColumnInfo.OriginX + parentBand.Columns[3].RowLayoutColumnInfo.SpanX;
            parentBand.Columns[4].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns[5].RowLayoutColumnInfo.OriginX = parentBand.Columns[4].RowLayoutColumnInfo.OriginX + parentBand.Columns[4].RowLayoutColumnInfo.SpanX;
            parentBand.Columns[5].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns[6].RowLayoutColumnInfo.OriginX = parentBand.Columns[5].RowLayoutColumnInfo.OriginX + parentBand.Columns[5].RowLayoutColumnInfo.SpanX;
            parentBand.Columns[6].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns[7].RowLayoutColumnInfo.OriginX = parentBand.Columns[6].RowLayoutColumnInfo.OriginX + parentBand.Columns[6].RowLayoutColumnInfo.SpanX;
            parentBand.Columns[7].RowLayoutColumnInfo.OriginY = 0;
            parentBand.Columns[8].RowLayoutColumnInfo.OriginX = parentBand.Columns[7].RowLayoutColumnInfo.OriginX + parentBand.Columns[7].RowLayoutColumnInfo.SpanX;
            parentBand.Columns[8].RowLayoutColumnInfo.OriginY = 0;
            //parentBand.Columns[2].RowLayoutColumnInfo.SpanX = 40;
            
            parentBandGroupPS.RowLayoutGroupInfo.OriginX = parentBand.Columns[8].RowLayoutColumnInfo.OriginX + parentBand.Columns[8].RowLayoutColumnInfo.SpanX;
            parentBandGroupPS.RowLayoutGroupInfo.OriginY = 0;
            parentBandGroupPS.RowLayoutGroupInfo.SpanX = 300;
            parentBandGroupSD.RowLayoutGroupInfo.OriginX = parentBandGroupPS.RowLayoutGroupInfo.OriginX + parentBandGroupPS.RowLayoutGroupInfo.SpanX;
            parentBandGroupSD.RowLayoutGroupInfo.OriginY = 0;
            parentBandGroupSD.RowLayoutGroupInfo.SpanX = 300;

            this.WindowState = FormWindowState.Maximized;
            if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "IsMinimized").Data == "1")
            {
                this.WindowState = FormWindowState.Normal;


                this.Width = Screen.PrimaryScreen.WorkingArea.Width - 200;
                this.Height = Screen.PrimaryScreen.WorkingArea.Height - 50;
                this.Location = new System.Drawing.Point(((Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2), ((Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2));

            }
            //if(Utils.isDemo)
            //{
            //    ultraButton2.Visible = false;
            //}
            WaitingFrm.StopWaiting();
        }
        private void GanTongDuNoDuCo(List<PO_PayDetailNCC1> list, string loaitien)
        {
            for(int i=0; i< list.Count;i++)
            {
                if(list[i].SoCtu=="Cộng nhóm")
                {
                    if(loaitien=="VND")
                    {
                        list[i].ClosingCreditAmount = list[i - 1].ClosingCreditAmount;
                        list[i].ClosingDebitAmount = list[i - 1].ClosingDebitAmount;
                    }
                    else
                    {
                        list[i].ClosingCreditAmountOC = list[i - 1].ClosingCreditAmountOC;
                        list[i].ClosingDebitAmountOC = list[i - 1].ClosingDebitAmountOC;
                    }
                }
            }
        }

        private void ultraButton1_Click(object sender, EventArgs e)
        {
            var rD = new PO_PayDetailNCC_Detail();
            rD.Period = "Tài khoản: " + tai_khoan + "; Loại tiền: " + loai_tien + "; " + ReportUtils.GetPeriod(begin1, end1);
            var f = new ReportForm<PO_PayDetailNCC>(fileReportSlot1, _subSystemCode);
            f.AddDatasource("DBCHITIETNOPTRA", data1, true);
            f.AddDatasource("Detail", rD);
            f.LoadReport();
            f.WindowState = FormWindowState.Maximized;
            f.Show();
        }

        private void ultraButton2_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog
            {
                Title = "Chọn nơi sao lưu ",
                Filter = "File (*.xls)|*.xls",
                InitialDirectory = "@C:\\",
                FileName = "chitietcongnophaitranhacungcap.xls"
            };
            if (save.ShowDialog() == DialogResult.OK)
            {
                string duongdan = save.FileName;
                try
                {
                    ultraGridExcelExporter1.Export(ugridDuLieu, duongdan);
                    if (MSG.Question("Kết xuất thành công, Bạn có muốn mở file vừa kết xuất không?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start(save.FileName);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                        }

                    }
                }
                catch
                {
                    MSG.Warning("Lỗi khi kết xuất !");
                }
            }
        }
        private void InDamGridVaGanGiaTriCongNhom(UltraGrid data, List<PO_PayDetailNCC1> list)
        {
            UltraGridRow ultraGridRow = data.DisplayLayout.Rows[0];
            for (int i = 0; i < list.Count; i++)
            {
                if (data.DisplayLayout.Rows[i].Cells[2].Value != null)
                {
                    if (data.DisplayLayout.Rows[i].Cells[2].Value.ToString() == "Cộng nhóm")
                    {
                        data.DisplayLayout.Rows[i].Cells[2].Appearance.FontData.Bold=DefaultableBoolean.True;
                        data.DisplayLayout.Rows[i].Cells[7].Appearance.FontData.Bold = DefaultableBoolean.True;
                        data.DisplayLayout.Rows[i].Cells[11].Appearance.FontData.Bold = DefaultableBoolean.True;
                        data.DisplayLayout.Rows[i].Cells[12].Appearance.FontData.Bold = DefaultableBoolean.True;
                        data.DisplayLayout.Rows[i].Cells[13].Appearance.FontData.Bold = DefaultableBoolean.True;
                        data.DisplayLayout.Rows[i].Cells[14].Appearance.FontData.Bold = DefaultableBoolean.True;


                    }
                    if (data.DisplayLayout.Rows[i].Cells[2].Value.ToString() == "Tên nhà cung cấp :")
                    {
                        data.DisplayLayout.Rows[i].Cells[2].Appearance.FontData.Bold = DefaultableBoolean.True;
                        data.DisplayLayout.Rows[i].Cells[7].Appearance.FontData.Bold = DefaultableBoolean.True;

                    }
                    if (data.DisplayLayout.Rows[i].Cells[2].Value.ToString() == "Tổng cộng")
                    {
                        data.DisplayLayout.Rows[i].Cells[2].Appearance.FontData.Bold = DefaultableBoolean.True;
                        data.DisplayLayout.Rows[i].Cells[11].Appearance.FontData.Bold = DefaultableBoolean.True;
                        data.DisplayLayout.Rows[i].Cells[12].Appearance.FontData.Bold = DefaultableBoolean.True;
                        data.DisplayLayout.Rows[i].Cells[13].Appearance.FontData.Bold = DefaultableBoolean.True;
                        data.DisplayLayout.Rows[i].Cells[14].Appearance.FontData.Bold = DefaultableBoolean.True;


                    }
                }
            
            }
        }
        private void TinhTongDuNoDuCo(List<PO_PayDetailNCC> data)
        {
            decimal TongDuNo = 0;
            decimal TongDuCo = 0;
            decimal PhatSinhNo = 0;
            decimal PhatSinhCo = 0;
            string strTongDuNo = "";
            string strTongDuCo = "";
            foreach (var item in data)
            {
                if(!item.Sum_DuCo.ToString().IsNullOrEmpty())
                {
                    
                    TongDuCo += item.Sum_DuCo;
                }
                if (!item.Sum_DuNo.ToString().IsNullOrEmpty())
                {

                    TongDuNo += item.Sum_DuNo;

                }
            }
            if(loai_tien=="VND")
            {
                foreach (var item in data)
                {
                    if (!item.DebitAmount.ToString().IsNullOrEmpty())
                    {

                        PhatSinhNo += item.DebitAmount;
                    }
                    if (!item.CreditAmount.ToString().IsNullOrEmpty())
                    {

                        PhatSinhCo += item.CreditAmount;

                    }
                }
            }
            else
            {
                foreach (var item in data)
                {
                    if (!item.DebitAmountOC.ToString().IsNullOrEmpty())
                    {

                        PhatSinhNo += item.DebitAmountOC;
                    }
                    if (!item.CreditAmountOC.ToString().IsNullOrEmpty())
                    {

                        PhatSinhCo += item.CreditAmountOC;

                    }
                }
            }
            PO_PayDetailNCC1 pO_PayDetailNCC = new PO_PayDetailNCC1();
            pO_PayDetailNCC.SoCtu = "Tổng cộng";
            pO_PayDetailNCC.DebitAmount = pO_PayDetailNCC.DebitAmountOC = PhatSinhNo;
            pO_PayDetailNCC.CreditAmount = pO_PayDetailNCC.CreditAmountOC = PhatSinhCo;
            pO_PayDetailNCC.ClosingCreditAmount = pO_PayDetailNCC.ClosingCreditAmountOC = TongDuCo;
            pO_PayDetailNCC.ClosingDebitAmount = pO_PayDetailNCC.ClosingDebitAmountOC = TongDuNo;
            if (loai_tien == "VND")
            {
                Utils.AddSumColumn1(PhatSinhNo, ugridDuLieu, "DebitAmount", false, constDatabaseFormat: 1);
                Utils.AddSumColumn1(PhatSinhCo, ugridDuLieu, "CreditAmount", false, constDatabaseFormat: 1);
                Utils.AddSumColumn1(TongDuCo, ugridDuLieu, "ClosingCreditAmount", false, constDatabaseFormat: 1);
                Utils.AddSumColumn1(TongDuNo, ugridDuLieu, "ClosingDebitAmount", false, constDatabaseFormat: 1);
            }
            else
            {
                Utils.AddSumColumn1(PhatSinhNo, ugridDuLieu, "DebitAmountOC", false, constDatabaseFormat: 0);
                Utils.AddSumColumn1(PhatSinhCo, ugridDuLieu, "CreditAmountOC", false, constDatabaseFormat: 0);
                Utils.AddSumColumn1(TongDuCo, ugridDuLieu, "ClosingCreditAmountOC", false, constDatabaseFormat: 0);
                Utils.AddSumColumn1(TongDuNo, ugridDuLieu, "ClosingDebitAmountOC", false, constDatabaseFormat: 0);
            }
            //data2.Add(pO_PayDetailNCC);
            strTongDuCo = Accounting.ReportUtils.FormatTiente(TongDuCo, loai_tien);
            strTongDuNo = Accounting.ReportUtils.FormatTiente(TongDuNo, loai_tien);
           

        }
        private void sethyperlinktogrid(List<PO_PayDetailNCC> data12)
        {
            for(int i =0;i<data12.Count;i++)
            {
                var ultraFormattedLinkLabel = new UltraFormattedLinkLabel { Name = "ultraFormattedLinkLabel" + i };
                ultraFormattedLinkLabel.Value = data12[i].SoCtu;
                
            }
        }
        private Decimal Covert_Money(decimal gt)
        {
            Decimal ketqua = 0;
            string s = "n";
            if (gt < 0)
            {
                string gt_am = String.Format("{0:" + Utils.GetTypeFormatNumberic(ConstDatabase.Format_TienVND) + ";" + Utils.GetFormatNegative().Replace("n", Utils.GetTypeFormatNumberic(ConstDatabase.Format_TienVND)) + "}", Convert.ToDecimal(gt));
                gt_am = gt_am.Replace('(', '-').Replace(")", string.Empty);
                ketqua = Convert.ToDecimal(gt_am);
            }
            else
            {
                string kq = String.Format("{0:" + Utils.GetTypeFormatNumberic(ConstDatabase.Format_TienVND) + ";" + s.Replace("n", Utils.GetTypeFormatNumberic(ConstDatabase.Format_TienVND)) + "}", Convert.ToDecimal(gt));
                ketqua = Convert.ToDecimal(kq);
            }

            return ketqua;
        }
        
        private void ugridDuLieu_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {

            //UltraGridBand parentBand = this.ugridDuLieu.DisplayLayout.Bands[0];
            //parentBand.RowLayoutStyle = RowLayoutStyle.GroupLayout;
            //UltraGridGroup parentBandGroup1 = parentBand.Groups.Add("ParentBandGroup1", "CHI TIẾT CÔNG NỢ PHẢI TRẢ");
            
            //UltraGridGroup parentBandGroup = parentBand.Groups.Add("ParentBandGroup", "Loaji tieefn vnd");
            //parentBandGroup.RowLayoutGroupInfo.ParentGroup = parentBandGroup1;
            //parentBand.Columns[0].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            //parentBand.Columns[1].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            //parentBand.Columns[2].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            //parentBand.Columns[3].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            //parentBand.Columns[4].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            //parentBand.Columns[5].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            //parentBand.Columns[6].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            //parentBand.Columns[7].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            //parentBand.Columns[8].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            //parentBand.Columns[9].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            //parentBand.Columns[10].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            //parentBand.Columns[11].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            //parentBand.Columns[12].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            //parentBand.Columns[13].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            //parentBand.Columns[14].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            //parentBand.Columns[15].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            //parentBand.Columns[16].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            //parentBand.Columns[17].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            //parentBand.Columns[18].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            //parentBand.Columns[19].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            //parentBand.Columns[21].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            //parentBand.Columns.Add("B1.8", "B1.8");
            //parentBand.Columns["B1.8"].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            //parentBand.Columns.Add("B1.9", "B1.9");
            //parentBand.Columns["B1.9"].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            //parentBand.Columns.Add("B1.10", "B1.10");
            //parentBand.Columns["B1.10"].RowLayoutColumnInfo.ParentGroup = parentBandGroup;
            //parentBand.Columns.Add("B1.11", "B1.11");
            //parentBand.Columns["B1.11"].RowLayoutColumnInfo.ParentGroup = parentBandGroup;

            //parentBand.Columns.Add("B1.1", "B1.1");
            //parentBand.Columns["B1.1"].RowLayoutColumnInfo.OriginX = 0;
            //parentBand.Columns["B1.1"].RowLayoutColumnInfo.OriginY = 0;
            //parentBand.Columns["B1.1"].RowLayoutColumnInfo.SpanX = 2;
            //parentBand.Columns["B1.1"].RowLayoutColumnInfo.SpanY = 4;

            //parentBand.Columns.Add("B1.2", "B1.2");
            //parentBand.Columns["B1.2"].RowLayoutColumnInfo.OriginX = 2;
            //parentBand.Columns["B1.2"].RowLayoutColumnInfo.OriginY = 0;
            //parentBand.Columns["B1.2"].RowLayoutColumnInfo.SpanX = 2;
            //parentBand.Columns["B1.2"].RowLayoutColumnInfo.SpanY = 4;

            //parentBand.Columns.Add("B1.3", "B1.3");
            //parentBand.Columns["B1.3"].RowLayoutColumnInfo.OriginX = 4;
            //parentBand.Columns["B1.3"].RowLayoutColumnInfo.OriginY = 0;
            //parentBand.Columns["B1.3"].RowLayoutColumnInfo.SpanX = 2;
            //parentBand.Columns["B1.3"].RowLayoutColumnInfo.SpanY = 4;

            //parentBand.Columns.Add("B1.4", "B1.4");
            //parentBand.Columns["B1.4"].RowLayoutColumnInfo.OriginX = 6;
            //parentBand.Columns["B1.4"].RowLayoutColumnInfo.OriginY = 0;
            //parentBand.Columns["B1.4"].RowLayoutColumnInfo.SpanX = 2;
            //parentBand.Columns["B1.4"].RowLayoutColumnInfo.SpanY = 4;

            //parentBand.Columns.Add("B1.5", "B1.5");
            //parentBand.Columns["B1.5"].RowLayoutColumnInfo.OriginX = 8;
            //parentBand.Columns["B1.5"].RowLayoutColumnInfo.OriginY = 0;
            //parentBand.Columns["B1.5"].RowLayoutColumnInfo.SpanX = 2;
            //parentBand.Columns["B1.5"].RowLayoutColumnInfo.SpanY = 4;

            //parentBand.Columns.Add("B1.6", "B1.6");
            //parentBand.Columns["B1.6"].RowLayoutColumnInfo.OriginX = 10;
            //parentBand.Columns["B1.6"].RowLayoutColumnInfo.OriginY = 0;
            //parentBand.Columns["B1.6"].RowLayoutColumnInfo.SpanX = 2;
            //parentBand.Columns["B1.6"].RowLayoutColumnInfo.SpanY = 4;

            //parentBand.Columns.Add("B1.7", "B1.7");
            //parentBand.Columns["B1.7"].RowLayoutColumnInfo.OriginX = 12;
            //parentBand.Columns["B1.7"].RowLayoutColumnInfo.OriginY = 0;
            //parentBand.Columns["B1.7"].RowLayoutColumnInfo.SpanX = 2;
            //parentBand.Columns["B1.7"].RowLayoutColumnInfo.SpanY = 4;

            //parentBandGroup.RowLayoutGroupInfo.OriginX = 14;
            //parentBandGroup.RowLayoutGroupInfo.OriginY = 0;
            //parentBandGroup.RowLayoutGroupInfo.SpanX = 8;
            //parentBandGroup.RowLayoutGroupInfo.SpanY = 4;
            //string DoiTuongHienTai = "";
            //string DoiTuongCu = "";
            //UltraGridRow DongCu= null;
            //foreach (var row in ugridDuLieu.Rows)
            //{

            //    if (row.Index != 0)
            //    {
            //         DongCu = ugridDuLieu.Rows[row.Index - 1];
            //        DoiTuongCu = DongCu.Cells[6].Text;
            //    }
            //    DoiTuongHienTai = row.Cells[6].Text;

            //    if(DoiTuongHienTai!=DoiTuongCu)
            //    { ugridDuLieu.DisplayLayout.Bands[0].AddNew(); }
            //}
        }

        private void ugridDuLieu_InitializeGroupByRow(object sender, InitializeGroupByRowEventArgs e)
        {
            //if (e.ReInitialize)
            //    Debug.WriteLine("A group-by row is being re-initilaized.");
            //else
            //    Debug.WriteLine("A group-by row is being initilaized.");

            //// Set the description to desired value. This is the text that shows up on
            //// the group-by row.
            //e.Row.Description = e.Row.Value.ToString() + ", " + e.Row.Rows.Count + " Items.";

            //// We only want to do this if we are grouping by the Country column.
            //if (e.Row.Column.Key == "AccountObjectName")
            //{
            //    // If the group has more than 5 items, then make the background color 
            //    // of the row to red
            //    if (e.Row.Rows.Count > 5)
            //    {
            //        e.Row.Appearance.BackColor = Color.Red;
            //    }
            //    else
            //    {
            //        e.Row.Appearance.ResetBackColor();
            //    }
            //}
        }

        private void ugridDuLieu_SummaryValueChanged(object sender, SummaryValueChangedEventArgs e)
        {
            
        }
        public static void AddSumColumn(UltraGrid uGrid, string keyColumn, bool left = true, string displayFormat = "", int constDatabaseFormat = 0, bool isMinus = true)
        {
            string keySum = keyColumn;
            do
            {
                keySum += "Sum";
            } while (uGrid.DisplayLayout.Bands[0].Summaries.Exists(keySum));
            if (uGrid.DisplayLayout.Bands[0].Columns.Exists(keyColumn))
            {
                SummarySettings summary = uGrid.DisplayLayout.Bands[0].Summaries.Add(keySum, SummaryType.Maximum, uGrid.DisplayLayout.Bands[0].Columns[keyColumn]);
                summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;


                if (!string.IsNullOrEmpty(displayFormat))
                {
                    summary.DisplayFormat = displayFormat;
                }
                else if (constDatabaseFormat == 0)
                {
                    summary.DisplayFormat = "{0:N0}";
                }
                else if (constDatabaseFormat != 0)
                {
                    string format = Utils.GetTypeFormatNumberic(constDatabaseFormat);
                    string s = "{0:" + format;
                    if (!isMinus) s += ";(" + format + ")}";
                    else s += "}";
                    summary.DisplayFormat = s;
                }

                summary.Appearance.FontData.Bold = DefaultableBoolean.True;//trungnq
                summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed | SummaryDisplayAreas.GroupByRowsFooter;
                summary.Appearance.TextHAlign = left ? HAlign.Left : HAlign.Right;
                
            }
            else
            {
                
            }
        }

        private void ugridDuLieu_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if(e.Row.ListObject.HasProperty("TypeID"))
            {
                if (!e.Row.ListObject.GetProperty("TypeID").IsNullOrEmpty())
                {
                    int typeid = e.Row.ListObject.GetProperty("TypeID").ToInt();
                    
                    if (e.Row.ListObject.HasProperty("ID"))
                    {
                        if (!e.Row.ListObject.GetProperty("ID").IsNullOrEmpty())
                        {
                            Guid id = (Guid)e.Row.ListObject.GetProperty("ID");
                            var f =Utils.ViewVoucherSelected1(id, typeid);
                            //this.Close();
                            //if (f.IsDisposed)
                            //{
                            //    new FChiTietCongNoPhaiTraNhaCCTruocIn(data1, tai_khoan, loai_tien, begin1, end1, this.ds_ncc).ShowDialog();
                            //}
                        }
                    }
                }
                
            }
        }

        private void ultraGridExcelExporter1_InitializeColumn(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.InitializeColumnEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Column.Format))
            {
                e.ExcelFormatStr = e.Column.Format;
            }
        }

        private void reportManager1_GetReportParameter(object sender, GetReportParameterEventArgs e)
        {
            string typeCurrency = loai_tien != null ? loai_tien : string.Empty;
            e.Parameters["CurrencyID"].Value = typeCurrency;
        }

        private void FChiTietCongNoPhaiTraNhaCCTruocIn_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }

        private void ultraButton3_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ugridDuLieu_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            UltraGridCell cell = e.Row.Cells[0];
            cell.Row.Height = 25;
        }

        private void ultraGridExcelExporter1_BeginExport(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.BeginExportEventArgs e)
        {
            foreach (var row in ugridDuLieu.Rows)
            {
                foreach (var column in ugridDuLieu.DisplayLayout.Bands[0].Columns)
                {
                    e.Rows[row.Index].Cells[column].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
                    e.Rows[row.Index].Cells[column].Appearance.BorderColor = Color.Black;
                    e.Rows[row.Index].Cells[column].Column.Width = 200;
                }
            }
            e.Rows.Band.Summaries[0].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[0].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[1].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[1].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[2].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[2].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[3].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[3].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[4].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[4].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[5].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[5].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[6].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[6].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[7].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[7].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[8].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[8].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[9].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[9].Appearance.BorderColor = Color.Black;
            e.Rows.Band.Summaries[10].Appearance.BorderAlpha = Alpha.UseAlphaLevel;
            e.Rows.Band.Summaries[10].Appearance.BorderColor = Color.Black;

        }

        private void ultraGridExcelExporter1_SummaryCellExported(object sender, Infragistics.Win.UltraWinGrid.ExcelExport.SummaryCellExportedEventArgs e)
        {
            Worksheet ws = e.CurrentWorksheet;
            WorksheetRow wsRow = ws.Rows[e.CurrentRowIndex];
            WorksheetCell wsCell = wsRow.Cells[e.CurrentColumnIndex];
            if (!e.Summary.SummaryText.Contains("Số dòng") && !e.Summary.SummaryText.Contains(" "))
            {
                string s = e.Summary.SummaryText.Trim('.');
                if (s.Contains("("))
                    wsCell.Value = -decimal.Parse(s.Trim('(').Trim(')'));
                else
                    wsCell.Value = decimal.Parse(s);

                wsCell.CellFormat.FormatString = "###,###,###,###,##0;(###,###,###,###,##0)";
            }
        }
    }
    
}
