﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Accounting.Core.Domain;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Accounting.Core.DAO;
using System.Globalization;
using Accounting.Frm.FReport;

namespace Accounting
{
    public partial class FRContractStatusSale : CustormForm
    {
        #region các hàm service, list sử dụng
        #region commnet by cuongpv
        //private static IAccountService AccountService
        //{
        //    get { return IoC.Resolve<IAccountService>(); }
        //}
        //private static IAccountingObjectService AccountingObjectService
        //{
        //    get { return IoC.Resolve<IAccountingObjectService>(); }
        //}
        //private static IAccountingObjectGroupService AccountingObjectGroupService
        //{
        //    get { return IoC.Resolve<IAccountingObjectGroupService>(); }
        //}
        //private static IMaterialGoodsService MaterialGoodsService
        //{
        //    get { return IoC.Resolve<IMaterialGoodsService>(); }
        //}
        //private static IMaterialGoodsCategoryService MaterialGoodsCategoryService
        //{
        //    get { return IoC.Resolve<IMaterialGoodsCategoryService>(); }
        //}
        //private static IDepartmentService DepartmentService
        //{
        //    get { return IoC.Resolve<IDepartmentService>(); }
        //}
        //private static ICurrencyService CurrencyService
        //{
        //    get { return IoC.Resolve<ICurrencyService>(); }
        //}
        //private static IEMContractDetailMGService EMContractDetailMGService
        //{
        //    get { return IoC.Resolve<IEMContractDetailMGService>(); }
        //}

        //readonly List<MaterialGoodsReport> _lstMaterialGoodsReport = ReportUtils.LstMaterialGoodsReport;
        //readonly List<AccountingObjectReport> _lstCustomerReport = ReportUtils.LstProviderKH;
        //readonly List<Department> _lstDepartment = DepartmentService.GetAll_ByIsActive_OrderByDepartmentCode(true);
        //readonly List<AccountingObject> _lstEmployee = AccountingObjectService.GetAccountingObjects(2, true);
        //readonly List<MaterialGoodsCategory> _lstMaterialGoodsCategory = MaterialGoodsCategoryService.GetAll_OrderBy();
        //readonly List<AccountingObjectGroup> _lstAccountingObjectGroup = AccountingObjectGroupService.GetListByIsActiveOrderCode(true);
        //List<string> ghidoanhso = new List<string> { "Đã ghi doanh số", "Chưa ghi doanh số"};
        //readonly List<Currency> _lstCurrency = CurrencyService.GetIsActive(true);
        #endregion
        string _subSystemCode;
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        //List<MaterialGoods> lstMaterial = new List<MaterialGoods>(); //comment by cuongpv
        //List<AccountingObject> lstAccountingObject = new List<AccountingObject>(); //comment by cuongpv
        List<EMContractReport> lstContractReportSale = ReportUtils.lstEMContractReportSale;//add by cuongpv
        #endregion
        public FRContractStatusSale(string subSystemCode)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            _subSystemCode = subSystemCode;
            ReportUtils.ProcessControls(this);
            #region comment code cu by cuongpv
            //foreach(var item in _lstCustomerReport)
            //{
            //    item.Check = false;
            //}
            //ugridAccountingObject.SetDataBinding(_lstCustomerReport.ToList(), "");
            //ugridAccountingObject.DisplayLayout.Bands[0].Columns["ID"].Hidden = true;
            //ugridAccountingObject.DisplayLayout.Bands[0].Columns["ObjectType"].Hidden = true;
            //ugridAccountingObject.DisplayLayout.Bands[0].Columns["AccountingObjectCategory"].Hidden = true;
            //ugridAccountingObject.DisplayLayout.Bands[0].Columns["AccountObjectGroupID"].Hidden = true;
            //ugridAccountingObject.DisplayLayout.Bands[0].Columns["AccountingObjectCode"].Header.Caption = "Mã đối tượng";
            //ugridAccountingObject.DisplayLayout.Bands[0].Columns["AccountingObjectName"].Header.Caption = "Tên đối tượng";
            //ugridAccountingObject.DisplayLayout.Bands[0].Columns["Address"].Header.Caption = "Địa chỉ";
            //ugridAccountingObject.DisplayLayout.Bands[0].Columns["Check"].Header.CheckBoxVisibility = HeaderCheckBoxVisibility.Always;
            //ugridAccountingObject.DisplayLayout.Bands[0].Columns["Check"].Header.Caption = "";
            //ugridAccountingObject.DisplayLayout.Override.AllowAddNew = AllowAddNew.No;


            //foreach(var item in _lstMaterialGoodsReport)
            //{
            //    item.Check = false;
            //}
            //ugridEMContractSale.SetDataBinding(_lstMaterialGoodsReport.ToList(), "");
            //ugridEMContractSale.DisplayLayout.Bands[0].Columns["ID"].Hidden = true;
            //ugridEMContractSale.DisplayLayout.Bands[0].Columns["MaterialGoodsGSTID"].Hidden = true;
            //ugridEMContractSale.DisplayLayout.Bands[0].Columns["MaterialGoodsCategoryID"].Hidden = true;
            //ugridEMContractSale.DisplayLayout.Bands[0].Columns["RepositoryID"].Hidden = true;
            //ugridEMContractSale.DisplayLayout.Bands[0].Columns["MaterialGoodsCode"].Header.Caption = "Mã VTHH";
            //ugridEMContractSale.DisplayLayout.Bands[0].Columns["MaterialGoodsName"].Header.Caption = "Tên VTHH";
            //ugridEMContractSale.DisplayLayout.Bands[0].Columns["Check"].Header.CheckBoxVisibility = HeaderCheckBoxVisibility.Always;
            //ugridEMContractSale.DisplayLayout.Bands[0].Columns["Check"].Header.Caption = "";
            //ugridEMContractSale.DisplayLayout.Override.AllowAddNew = AllowAddNew.No;
            #endregion
            //add by cuongpv
            Utils.ConfigGrid(ugridEMContractSale, ConstDatabase.FEMContractSale_TableName);

            foreach (var item in lstContractReportSale)
            {
                item.Check = false;
            }
            ugridEMContractSale.SetDataBinding(lstContractReportSale.ToList(), "");
            UltraGridBand band = ugridEMContractSale.DisplayLayout.Bands[0];
            band.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.Default;
            band.Columns["ID"].Hidden = true;
            band.Columns["ContractCode"].Header.Caption = "Mã hợp đồng";
            band.Columns["ContractName"].Header.Caption = "Tên hợp đồng";
            UltraGridColumn ugc = band.Columns["Check"];
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            band.Columns["Check"].CellActivation = Activation.AllowEdit;
            band.Columns["Check"].Header.Fixed = true;
            band.Columns["Check"].Header.Caption = "";
            ugridEMContractSale.DisplayLayout.Override.AllowAddNew = AllowAddNew.No;
            foreach (var c in band.Columns)
            {
                if (c.Key != "Check") c.CellActivation = Activation.NoEdit;
            }
            //end add by cuonpgv
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dtBeginDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }

        }

        private void btnExit_Click_1(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOk_Click_1(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }

            if ((DateTime)dtBeginDate.Value > (DateTime)dtEndDate.Value)
            {
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            #region comment by cuongpv
            //List<Guid?> lstA = new List<Guid?>();
            //List<Guid?> lstM = new List<Guid?>();
            //for (int i = 0; i < ugridAccountingObject.Rows.Count; i++)
            //{
            //    if (bool.Parse(ugridAccountingObject.Rows[i].Cells["Check"].Value.ToString()) == true)
            //    {
            //        lstA.Add((Guid?)ugridAccountingObject.Rows[i].Cells["ID"].Value);
            //    }
            //}

            //if (lstA.Count == 0)
            //{
            //    MSG.Warning("Bạn chưa chọn đối tượng");
            //    return;
            //}

            //for (int i = 0; i < ugridEMContractSale.Rows.Count; i++)
            //{
            //    if (bool.Parse(ugridEMContractSale.Rows[i].Cells["Check"].Value.ToString()) == true)
            //    {
            //        lstM.Add((Guid?)ugridEMContractSale.Rows[i].Cells["ID"].Value);
            //    }
            //}

            //if (lstM.Count == 0)
            //{
            //    MSG.Warning("Bạn chưa chọn vật tư hàng hóa");
            //    return;
            //}
            #endregion
            //add by cuongpv
            List<Guid?> lstIDContract = new List<Guid?>();
            for (int i = 0; i < ugridEMContractSale.Rows.Count; i++)
            {
                if(bool.Parse(ugridEMContractSale.Rows[i].Cells["Check"].Value.ToString()) == true)
                {
                    lstIDContract.Add((Guid?)ugridEMContractSale.Rows[i].Cells["ID"].Value);
                }
            }

            if(lstIDContract.Count == 0)
            {
                MSG.Warning("Bạn chưa chọn hợp đồng");
                return;
            }
            //end add by cuongpv
            FRTINHHINHTHUCHIENHDBex fm = new FRTINHHINHTHUCHIENHDBex((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, lstIDContract);
            fm.Show(this);
            
        }        
    }
}