﻿using Accounting.Core.DAO;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.Report;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRCHITIETCONGNOPHAITRANCC : Form
    {

        readonly List<AccountReport> _lstAccountReceivablesReport = ReportUtils.LstAccountReceivablesReportCongNo.Where(t => t.AccountNumber == "331" || t.AccountNumber == "3388").ToList();
        private readonly List<AccountingObjectReport> _lstnew = ReportUtils.LstProvider;
        List<AccountingObjectReport> _lstAccountingObjectReport = new List<AccountingObjectReport>();
        string _subSystemCode = "";
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        public FRCHITIETCONGNOPHAITRANCC()
        {

            InitializeComponent();
            Utils.ClearCacheByType<AccountingObject>();
            Utils.ClearCacheByType<AccountingObjectReport>();
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\ChiTietCongNoPhaiTraNCC.rst", path);

            _lstAccountingObjectReport = _lstnew;
            ugridAccountingObject.SetDataBinding(_lstAccountingObjectReport.ToList(), "");
            cbTaiKhoanCNPT.DataSource = _lstAccountReceivablesReport;
            cbTaiKhoanCNPT.DisplayMember = "AccountNumber";
            cbTaiKhoanCNPT.SelectedRow = cbTaiKhoanCNPT.Rows[0];
            Utils.ConfigGrid(cbTaiKhoanCNPT, ConstDatabase.Account_TableName);
            ReportUtils.ProcessControls(this);
            UltraGridBand band = ugridAccountingObject.DisplayLayout.Bands[0];
            foreach (var item in band.Columns)
            {
                if (item.Header.Caption == "Mã đối tượng")
                {
                    item.Header.Caption = "Mã NCC";
                }
                if (item.Header.Caption == "Tên đối tượng")
                {
                    item.Header.Caption = "Tên NCC";
                }
            }
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dtBeginDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
         
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
           
        }

        private void Form_FormClosed(object sender, FormClosedEventArgs e)
        {
            foreach (var item in ugridAccountingObject.Rows)
            {
                item.Cells["check"].Value = false;
            }
        }

        private void reportManager1_GetReportParameter(object sender, PerpetuumSoft.Reporting.Components.GetReportParameterEventArgs e)
        {
            string typeCurrency = cbbCurrency.Value != null ? cbbCurrency.Value.ToString() : string.Empty;
            e.Parameters["CurrencyID"].Value = typeCurrency;
        }

        private void btnOk_Click_1(object sender, EventArgs e)
        {

        }

        private void btnExit_Click_1(object sender, EventArgs e)
        {
            foreach (var item in ugridAccountingObject.Rows)
            {
                item.Cells["check"].Value = false;
            }
            Close();
        }

        private void ultraButton1_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }

            if ((DateTime)dtBeginDate.Value > (DateTime)dtEndDate.Value)
            {
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            string tai_khoan = cbTaiKhoanCNPT.Value != null ? cbTaiKhoanCNPT.Value.ToString() : string.Empty;
            string loai_tien = cbbCurrency.Value != null ? cbbCurrency.Value.ToString() : string.Empty;

            ReportProcedureSDS sp = new ReportProcedureSDS();
            List<PO_PayDetailNCC> data = new List<PO_PayDetailNCC>();
            var list_ncc = _lstAccountingObjectReport.Where(t => t.Check).Select(v => v.ID).ToList();
            string ds_ncc = "," + string.Join(",", list_ncc) + ",";
            data = sp.Get_SoChitietPhaTra_NCC((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, tai_khoan, loai_tien, ds_ncc);
            if (data.Count == 0)
            {
                MSG.Warning("Không có dữ liệu để lên báo cáo");
            }
            else
            {
                var data_group = data.Where(t => t.AccountObjectCode != null).ToList();
                var data_group_acount = data.Where(t => t.AccountObjectCode != null).Select(m => m.AccountObjectCode).Distinct();
                foreach (var item in data_group_acount)
                {
                    var list_item = data_group.Where(t => t.AccountObjectCode == item).ToList();
                    //list_item[list_item.Count() - 1].Sum_DuCo = list_item[list_item.Count() - 1].ClosingCreditAmount; //comment by cuongpv
                    //list_item[list_item.Count() - 1].Sum_DuNo = list_item[list_item.Count() - 1].ClosingDebitAmount; //comment by cuongpv
                    //add by cuongpv
                    if (loai_tien == "VND")
                    {
                        list_item[list_item.Count() - 1].Sum_DuCo = list_item[list_item.Count() - 1].ClosingCreditAmount;
                        list_item[list_item.Count() - 1].Sum_DuNo = list_item[list_item.Count() - 1].ClosingDebitAmount;
                    }
                    else
                    {
                        list_item[list_item.Count() - 1].Sum_DuCo = list_item[list_item.Count() - 1].ClosingCreditAmountOC;
                        list_item[list_item.Count() - 1].Sum_DuNo = list_item[list_item.Count() - 1].ClosingDebitAmountOC;
                    }
                    //end add by cuongpv
                }


                var rD = new PO_PayDetailNCC_Detail();
                rD.Period = "Tài khoản: " + tai_khoan + "; Loại tiền: " + loai_tien + "; " + ReportUtils.GetPeriod((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value);
                var f = new FChiTietCongNoPhaiTraNhaCCTruocIn(data,tai_khoan,loai_tien, (DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, ds_ncc);
                
                //f.WindowState = FormWindowState.Normal;
                f.Show();
            }
        }

    }
}
