﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Accounting.Core.Domain;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Accounting.Core.DAO;

namespace Accounting
{
    public partial class FRTinhHinhSuDungHD : CustormForm
    {
        #region các hàm service, list sử dụng
        private readonly List<Item> _lstItems = Utils.ObjConstValue.SelectTimes;
        public readonly ITT153ReportService _ITT153ReportService;
        public readonly ITT153PublishInvoiceDetailService _ITT153PublishInvoiceDetailService;
        public readonly ITT153DestructionInvoiceDetailService _ITT153DestructionInvoiceDetailService;
        public readonly ITT153DeletedInvoiceService _ITT153DeletedInvoiceService;
        public readonly ITT153LostInvoiceDetailService _ITT153LostInvoiceDetailService;
        public readonly ISAInvoiceService _ISAInvoiceService;
        public readonly ITT153LostInvoiceService _ITT153LostInvoiceService;
        public readonly ITT153DestructionInvoiceService _ITT153DestructionInvoiceService;
        List<TT153UseOfInvoice> data;
        List<int> lstDelete = new List<int>();
        List<int> lstLost = new List<int>();
        List<TT153DestructionInvoiceDetail> lstDestruction = new List<TT153DestructionInvoiceDetail>();
        List<int> lstSaInvoiceUseInPeriod = new List<int>();
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        #endregion
        public FRTinhHinhSuDungHD(string subSystemCode)
        {
            _ITT153ReportService = IoC.Resolve<ITT153ReportService>();
            _ITT153PublishInvoiceDetailService = IoC.Resolve<ITT153PublishInvoiceDetailService>();
            _ITT153DestructionInvoiceDetailService = IoC.Resolve<ITT153DestructionInvoiceDetailService>();
            _ITT153DeletedInvoiceService = IoC.Resolve<ITT153DeletedInvoiceService>();
            _ITT153LostInvoiceDetailService = IoC.Resolve<ITT153LostInvoiceDetailService>();
            _ITT153LostInvoiceService = IoC.Resolve<ITT153LostInvoiceService>();
            _ISAInvoiceService = IoC.Resolve<ISAInvoiceService>();
            _ITT153DestructionInvoiceService = IoC.Resolve<ITT153DestructionInvoiceService>();
            InitializeComponent();
            txtYear.Value = DateTime.Now.Year;
            InitializeGUI();
            

            #region getlink file
            IVoucherPatternsReportService vprSrv = IoC.Resolve<IVoucherPatternsReportService>();
            VoucherPatternsReport vpr = vprSrv.Query.ToList().FirstOrDefault(v => v.VoucherPatternsCode == "TinhHinhSuDungHD");
            if (vpr == null) return;
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            string filePath = string.Format("{0}\\Frm\\FReport\\Template\\{1}", path, vpr.FilePath);
            fileReportSlot1.FilePath = filePath;
            ReportUtils.ProcessControls(this);

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.MonthOrPrecious != null && dateTimeCacheHistory.Year != null)
                {
                    if (dateTimeCacheHistory.MonthOrPrecious != 0)
                    {
                        ultraOptionSet1.Value = dateTimeCacheHistory.MonthOrPrecious - 1;
                        if (dateTimeCacheHistory.MonthOrPrecious == 1)
                        {
                            cbbMonth.SelectedRow = cbbMonth.Rows[dateTimeCacheHistory.Month ?? 0];
                            txtYear.Value = dateTimeCacheHistory.Year;
                        }
                        else
                        {
                            cbbMonth.SelectedRow = cbbMonth.Rows[dateTimeCacheHistory.Precious ?? 0];
                            txtYear.Value = dateTimeCacheHistory.Year;
                        }

                    }

                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.MonthOrPrecious = ultraOptionSet1.Value.ToInt() + 1;
                if (ultraOptionSet1.Value.ToInt() == 0)
                {
                    dateTimeCacheHistory.Month = cbbMonth.SelectedRow.Index;
                }
                else
                {
                    dateTimeCacheHistory.Precious = cbbMonth.SelectedRow.Index;
                }
                dateTimeCacheHistory.Year = txtYear.Value.ToInt();
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }

            #endregion
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void InitializeGUI()
        {
            cbbMonth.ConfigComboSelectTime(_lstItems.Where(n => int.Parse(n.Value) < 21 && int.Parse(n.Value) > 8).ToList(), "Name");
            cbbMonth.DropDownStyle = UltraComboStyle.DropDownList;
            foreach (var item in cbbMonth.Rows)
            {
                if (int.Parse((item.ListObject as Item).Value) == DateTime.Now.Month + 8) cbbMonth.SelectedRow = item;
            }
        }


        public bool CheckError()
        {
            bool kq = true;
            if (cbbMonth.Value == null)
            {
            }
            return kq;
        }
        public List<TT153PublishInvoiceDetail> GetlstPublish(DateTime Time)
        {
            List<TT153PublishInvoiceDetail> lst = new List<TT153PublishInvoiceDetail>();
            foreach (TT153PublishInvoiceDetail item in _ITT153PublishInvoiceDetailService.GetAll().Where(n => n.StartUsing <= Time))
            {
                if (lst.Count(n => n.TT153ReportID == item.TT153ReportID) == 0)
                    lst.Add(item);
            }
            return lst;
        }
        List<TT153PublishInvoiceDetail> lstPI = new List<TT153PublishInvoiceDetail>();
        List<SAInvoice> lstSA = new List<SAInvoice>();
        List<SABill> lstSB = new List<SABill>();
        List<SAReturn> lstSR = new List<SAReturn>();
        List<PPDiscountReturn> lstPP = new List<PPDiscountReturn>();
        public string GetTonDauKi_TuSo(TT153PublishInvoiceDetail tT153PublishInvoiceDetail)
        {
            lstPI = new List<TT153PublishInvoiceDetail>();
            lstSA = new List<SAInvoice>();
            lstSB = new List<SABill>();
            lstSR = new List<SAReturn>();
            lstPP = new List<PPDiscountReturn>();
            int DestructionInvoiceMax = 0;
            List<TT153DestructionInvoiceDetail> lstTT153DestructionInvoice = _ITT153DestructionInvoiceService.GetAllToDate(dteDateFrom.DateTime, tT153PublishInvoiceDetail.TT153ReportID);
            if (lstTT153DestructionInvoice.Count > 0)
            {
                DestructionInvoiceMax = lstTT153DestructionInvoice.Select(n => int.Parse(n.ToNo)).Max();
            }
            lstPI = _ITT153PublishInvoiceDetailService.GetAllToDate(dteDateFrom.DateTime, tT153PublishInvoiceDetail.TT153ReportID);

            lstSA = _ISAInvoiceService.GetListSAInVoiceTT153(tT153PublishInvoiceDetail.InvoiceTypeID, tT153PublishInvoiceDetail.InvoiceForm, tT153PublishInvoiceDetail.InvoiceTemplate, tT153PublishInvoiceDetail.InvoiceSeries);
            lstSB = Utils.ISABillService.GetListSABillTT153(tT153PublishInvoiceDetail.InvoiceTypeID, tT153PublishInvoiceDetail.InvoiceForm, tT153PublishInvoiceDetail.InvoiceTemplate, tT153PublishInvoiceDetail.InvoiceSeries);
            lstSR = Utils.ISAReturnService.GetListSAReturnTT153(tT153PublishInvoiceDetail.InvoiceTypeID, tT153PublishInvoiceDetail.InvoiceForm, tT153PublishInvoiceDetail.InvoiceTemplate, tT153PublishInvoiceDetail.InvoiceSeries);
            lstPP = Utils.IPPDiscountReturnService.GetListPPDiscountReturnTT153(tT153PublishInvoiceDetail.InvoiceTypeID, tT153PublishInvoiceDetail.InvoiceForm, tT153PublishInvoiceDetail.InvoiceTemplate, tT153PublishInvoiceDetail.InvoiceSeries);
            if (lstPI.Count != 0)
                if (!string.IsNullOrEmpty(tT153PublishInvoiceDetail.InvoiceTypeID.ToString()) && !string.IsNullOrEmpty(tT153PublishInvoiceDetail.InvoiceForm.ToString())
                    || !string.IsNullOrEmpty(tT153PublishInvoiceDetail.InvoiceTemplate))

                {
                    List<int> ls = new List<int>();
                    List<SAInvoice> lst = lstSA.Where(n => n.InvoiceDate < dteDateFrom.DateTime).ToList();
                    if (lst.Count > 0) ls.Add(lst.Select(n => int.Parse(n.InvoiceNo)).Max());
                    List<SABill> lst1 = lstSB.Where(n => n.InvoiceDate < dteDateFrom.DateTime).ToList();
                    if (lst1.Count > 0) ls.Add(lst1.Select(n => int.Parse(n.InvoiceNo)).Max());
                    List<SAReturn> lst2 = lstSR.Where(n => n.TypeID == 340 && n.InvoiceDate < dteDateFrom.DateTime).ToList();
                    if (lst2.Count > 0) ls.Add(lst2.Select(n => int.Parse(n.InvoiceNo)).Max());
                    List<PPDiscountReturn> lst3 = lstPP.Where(n => n.TypeID == 220 && n.InvoiceDate < dteDateFrom.DateTime).ToList();
                    if (lst3.Count > 0) ls.Add(lst3.Select(n => int.Parse(n.InvoiceNo)).Max());
                    if (ls.Count > 0)
                    {
                        int T = ls.Max() + 1;
                        if (DestructionInvoiceMax > T)
                            return (DestructionInvoiceMax + 1).ToString().PadLeft(7, '0');
                        return T.ToString().PadLeft(7, '0');
                    }
                    else
                    {
                        //List<TT153PublishInvoiceDetail> lstP = _ITT153PublishInvoiceDetailService.GetAllToDate(dteDateFrom.DateTime, tT153PublishInvoiceDetail.TT153ReportID);
                        if (lstPI != null)
                        {
                            return lstPI.Select(n => int.Parse(n.FromNo)).Min().ToString().PadLeft(7, '0');
                        }
                        else
                        {
                            return "";
                        }

                    }
                }
                else
                    return "";
            return "";
        }

        public string GetTonDauKi_DenSo(TT153PublishInvoiceDetail tT153PublishInvoiceDetail)
        {
            //List<TT153PublishInvoiceDetail> lst = _ITT153PublishInvoiceDetailService.GetAllToDate(dteDateFrom.DateTime, tT153PublishInvoiceDetail.TT153ReportID);
            if (lstPI.Count > 0)
                return lstPI.Select(n => int.Parse(n.ToNo)).Max().ToString().PadLeft(7, '0');
            else return "";
        }
        public string GetTongSoTon(TT153PublishInvoiceDetail tT153PublishInvoiceDetail)
        {
            int Quantity = 0;
            foreach (TT153PublishInvoiceDetail item_ in _ITT153PublishInvoiceDetailService.GetAllToDate(dteDateTo.DateTime.Date.AddDays(1), tT153PublishInvoiceDetail.TT153ReportID))
            {
                Quantity += Convert.ToInt32(item_.Quantity);
            }
            return "";
        }
        List<TT153PublishInvoiceDetail> lstMuaPH = new List<TT153PublishInvoiceDetail>();
        public string GetMuaPhatHanhtrongKi_TuSo(TT153PublishInvoiceDetail tT153PublishInvoiceDetail)
        {
            lstMuaPH = _ITT153PublishInvoiceDetailService.GetAllFromDateToDate(dteDateFrom.DateTime, dteDateTo.DateTime.Date.AddDays(1), tT153PublishInvoiceDetail.TT153ReportID);
            if (lstMuaPH.Count > 0)
            {
                return lstMuaPH.Select(n => int.Parse(n.FromNo)).Min().ToString().PadLeft(7, '0');
            }
            else
            {
                return "";
            }

        }
        public string GetMuaPhatHanhtrongKi_DenSo(TT153PublishInvoiceDetail tT153PublishInvoiceDetail)
        {
            //List<TT153PublishInvoiceDetail> lst = _ITT153PublishInvoiceDetailService.GetAllFromDateToDate(dteDateFrom.DateTime, dteDateTo.DateTime.Date.AddDays(1), tT153PublishInvoiceDetail.TT153ReportID);
            if (lstMuaPH.Count > 0)
            {
                return lstMuaPH.Select(n => int.Parse(n.ToNo)).Max().ToString().PadLeft(7, '0');
            }
            else
            {
                return "";
            }
        }

        public string GetSoLuongXoa(TT153PublishInvoiceDetail tT153PublishInvoiceDetail)
        {
            lstDelete = new List<int>();
            lstDelete = _ITT153DeletedInvoiceService.GetAllFromDateToDate(dteDateFrom.DateTime, dteDateTo.DateTime.Date.AddDays(1), tT153PublishInvoiceDetail.InvoiceTypeID,
                 tT153PublishInvoiceDetail.InvoiceForm, tT153PublishInvoiceDetail.InvoiceTemplate, tT153PublishInvoiceDetail.InvoiceSeries);
            foreach (var item in Utils.ListSAInvoice.Where(n => n.InvoiceDate >= dteDateFrom.DateTime && n.InvoiceDate <= dteDateTo.DateTime.Date.AddDays(1) && n.InvoiceForm == 2 && n.InvoiceTemplate == tT153PublishInvoiceDetail.InvoiceTemplate && n.InvoiceTypeID == tT153PublishInvoiceDetail.InvoiceTypeID && n.InvoiceSeries == tT153PublishInvoiceDetail.InvoiceSeries).Where(n => n.InvoiceForm == 2 && n.StatusInvoice == 5 || n.InvoiceForm == 2 && n.StatusInvoice == 3))
            {
                if (!string.IsNullOrEmpty(item.InvoiceNo))
                {
                    lstDelete.Add(int.Parse(item.InvoiceNo));
                }
            }
            foreach (var item in Utils.ListSAReturn.Where(n => n.InvoiceDate >= dteDateFrom.DateTime && n.InvoiceDate <= dteDateTo.DateTime.Date.AddDays(1) && n.InvoiceForm == 2 && n.InvoiceTemplate == tT153PublishInvoiceDetail.InvoiceTemplate && n.InvoiceTypeID == tT153PublishInvoiceDetail.InvoiceTypeID && n.InvoiceSeries == tT153PublishInvoiceDetail.InvoiceSeries).Where(n => n.StatusInvoice == 5 || n.StatusInvoice == 3))
            {
                if (!string.IsNullOrEmpty(item.InvoiceNo))
                {
                    lstDelete.Add(int.Parse(item.InvoiceNo));
                }
            }
            foreach (var item in Utils.ListSABill.Where(n => n.InvoiceDate >= dteDateFrom.DateTime && n.InvoiceDate <= dteDateTo.DateTime.Date.AddDays(1) && n.InvoiceForm == 2 && n.InvoiceTemplate == tT153PublishInvoiceDetail.InvoiceTemplate && n.InvoiceTypeID == tT153PublishInvoiceDetail.InvoiceTypeID && n.InvoiceSeries == tT153PublishInvoiceDetail.InvoiceSeries).Where(n => n.InvoiceForm == 2 && n.StatusInvoice == 5 || n.InvoiceForm == 2 && n.StatusInvoice == 3))
            {
                if (!string.IsNullOrEmpty(item.InvoiceNo))
                {
                    lstDelete.Add(int.Parse(item.InvoiceNo));
                }
            }
            foreach (var item in Utils.ListPPDiscountReturn.Where(n => n.InvoiceDate >= dteDateFrom.DateTime && n.InvoiceDate <= dteDateTo.DateTime.Date.AddDays(1) && n.InvoiceForm == 2 && n.InvoiceTemplate == tT153PublishInvoiceDetail.InvoiceTemplate && n.InvoiceTypeID == tT153PublishInvoiceDetail.InvoiceTypeID && n.InvoiceSeries == tT153PublishInvoiceDetail.InvoiceSeries).Where(n => n.InvoiceForm == 2 && n.StatusInvoice == 5 || n.InvoiceForm == 2 && n.StatusInvoice == 3))
            {
                if (!string.IsNullOrEmpty(item.InvoiceNo))
                {
                    lstDelete.Add(int.Parse(item.InvoiceNo));
                }
            }
            lstDelete.GroupBy(n => n).Select(n => n.First()).ToList();
            if (lstDelete != null && lstDelete.Count > 0)
            {
                return lstDelete.Count.ToString();
            }
            return "";
        }
        public string GetSoLuongMat(TT153PublishInvoiceDetail tT153PublishInvoiceDetail)
        {
            lstLost = new List<int>();
            lstLost = _ITT153LostInvoiceService.GetAllFromDateToDate(dteDateFrom.DateTime, dteDateTo.DateTime.Date.AddDays(1), tT153PublishInvoiceDetail.TT153ReportID);
            if (lstLost != null && lstLost.Count > 0)
            {
                //return lstLost.Select(n => Convert.ToInt32(n.Quantity)).Sum().ToString();
                return lstLost./*Select(n => Convert.ToInt32(n.Quantity)).*/Sum().ToString();
            }
            return "";
        }
        public string GetSoXoa(TT153PublishInvoiceDetail tT153PublishInvoiceDetail)
        {
            string NoDelete = "";
            if (lstDelete.Count > 0)
            {
                List<int> lst = lstDelete.OrderBy(n => n).ToList();
                for (int i = 0; i < lst.Count; i++)
                {
                    if (string.IsNullOrEmpty(NoDelete))
                        NoDelete += lst[i].ToString().PadLeft(7, '0');
                    else
                    {
                        if (lst[i - 1] + 1 == lst[i])
                        {
                            for (; i < lst.Count - 1; i++)
                            {
                                if (lst[i] + 1 != lst[i + 1])
                                {
                                    break;
                                }
                            }
                            NoDelete += "- " + lst[i].ToString().PadLeft(7, '0');
                        }
                        else
                        {
                            NoDelete += "; " + lst[i].ToString().PadLeft(7, '0');//trungnq sửa lỗi hóa đơm mất, hủy, cháy , => ;
                        }
                    }
                }
            }
            return NoDelete;
        }
        public string GetSoMat(TT153PublishInvoiceDetail tT153PublishInvoiceDetail)
        {
            string NoLost = "";
            if (lstLost.Count > 0)
            {
                List<int> lst = lstLost.OrderBy(n => n).ToList();
                for (int i = 0; i < lst.Count; i++)
                {
                    if (string.IsNullOrEmpty(NoLost))
                        NoLost += lst[i].ToString().PadLeft(7, '0');
                    else
                    {
                        if (lst[i - 1] + 1 == lst[i])
                        {
                            for (; i < lst.Count - 1; i++)
                            {
                                if (lst[i] + 1 != lst[i + 1])
                                {
                                    break;
                                }
                            }
                            NoLost += "- " + lst[i].ToString().PadLeft(7, '0');
                        }
                        else
                        {
                            NoLost += "; " + lst[i].ToString().PadLeft(7, '0');//trungnq sửa lỗi hóa đơm mất, hủy, cháy , => ;
                        }
                    }
                }
            }
            return NoLost;
        }
        public string GetSoLuongHuy(TT153PublishInvoiceDetail tT153PublishInvoiceDetail)
        {
            lstDestruction = new List<TT153DestructionInvoiceDetail>();
            lstDestruction = _ITT153DestructionInvoiceService.GetAllFromDateToDate(dteDateFrom.DateTime, dteDateTo.DateTime.Date.AddDays(1), tT153PublishInvoiceDetail.TT153ReportID);
            if (lstDestruction != null && lstDestruction.Count > 0)
            {
                return lstDestruction.Select(n => Convert.ToInt32(n.Quantity)).Sum().ToString();
            }
            return "";
        }
        //public string GetSoMat(TT153PublishInvoiceDetail tT153PublishInvoiceDetail)
        //{
        //    string number = "";
        //    List<string> lst = new List<string>();
        //    lstLost = lstLost.OrderBy(n => int.Parse(n.FromNo)).ToList();
        //    foreach (TT153LostInvoiceDetail item in lstLost)
        //    {
        //        if (item.FromNo == item.ToNo)
        //        {
        //            lst.Add(item.FromNo.PadLeft(7, '0'));
        //        }
        //        else
        //        {
        //            lst.Add(item.FromNo.PadLeft(7, '0') + "- " + item.ToNo.PadLeft(7, '0'));
        //        }
        //    }
        //    foreach (string item in lst)
        //    {
        //        number += item;
        //        if (lst.IndexOf(item) != lst.Count - 1)
        //        {
        //            number += ", ";
        //        }
        //    }
        //    return number;
        //}

        public string GetSoHuy(TT153PublishInvoiceDetail tT153PublishInvoiceDetail)
        {
            string number = "";
            List<string> lst = new List<string>();
            lstDestruction = lstDestruction.OrderBy(n => int.Parse(n.FromNo)).ToList();
            foreach (TT153DestructionInvoiceDetail item in lstDestruction)
            {
                if (item.FromNo == item.ToNo)
                {
                    lst.Add(item.FromNo.PadLeft(7, '0'));
                }
                else
                {
                    lst.Add(item.FromNo.PadLeft(7, '0') + "- " + item.ToNo.PadLeft(7, '0'));
                }
            }
            foreach (string item in lst)
            {
                number += item;
                if (lst.IndexOf(item) != lst.Count - 1)
                {
                    number += "; ";//trungnq sửa lỗi hóa đơm mất, hủy, cháy , => ;
                }
            }
            return number;
        }
        List<SAInvoice> lstSD;
        List<SABill> lstSD1;
        List<SAReturn> lstSD2;
        List<PPDiscountReturn> lstSD3;
        public string GetTuSoSuDungTrongKi(TT153PublishInvoiceDetail tT153PublishInvoiceDetail)
        {
            //lstSaInvoiceUseInPeriod= _ISAInvoiceService.GetListSAInVoiceTT153inPeriod(dteDateFrom.DateTime, dteDateTo.DateTime.Date.AddDays(1), tT153PublishInvoiceDetail.InvoiceTypeID,
            //        tT153PublishInvoiceDetail.InvoiceForm, tT153PublishInvoiceDetail.InvoiceTemplate);
            lstSaInvoiceUseInPeriod = new List<int>();
            lstSD = new List<SAInvoice>();
            lstSD1 = new List<SABill>();
            lstSD2 = new List<SAReturn>();
            lstSD3 = new List<PPDiscountReturn>();
            lstSD = lstSA.Where(n => n.InvoiceDate >= dteDateFrom.DateTime && n.InvoiceDate <= dteDateTo.DateTime.Date.AddDays(1)).ToList();
            if (lstSD.Count > 0) lstSaInvoiceUseInPeriod.Add(lstSD.Select(n => int.Parse(n.InvoiceNo)).Min());
            lstSD1 = lstSB.Where(n => n.InvoiceDate >= dteDateFrom.DateTime && n.InvoiceDate <= dteDateTo.DateTime.Date.AddDays(1)).ToList();
            if (lstSD1.Count > 0) lstSaInvoiceUseInPeriod.Add(lstSD1.Select(n => int.Parse(n.InvoiceNo)).Min());
            lstSD2 = lstSR.Where(n => n.TypeID == 340 && n.InvoiceDate >= dteDateFrom.DateTime && n.InvoiceDate <= dteDateTo.DateTime.Date.AddDays(1)).ToList();
            if (lstSD2.Count > 0) lstSaInvoiceUseInPeriod.Add(lstSD2.Select(n => int.Parse(n.InvoiceNo)).Min());
            lstSD3 = lstPP.Where(n => n.TypeID == 220 && n.InvoiceDate >= dteDateFrom.DateTime && n.InvoiceDate <= dteDateTo.DateTime.Date.AddDays(1)).ToList();
            if (lstSD3.Count > 0) lstSaInvoiceUseInPeriod.Add(lstSD3.Select(n => int.Parse(n.InvoiceNo)).Min());
            if (lstSaInvoiceUseInPeriod.Count > 0)
            {
                if (lstSaInvoiceUseInPeriod.Min() == 0) return "1".PadLeft(7, '0');
                return lstSaInvoiceUseInPeriod.Min().ToString().PadLeft(7, '0');
            }
            return "";
        }
        List<int> lstSaInvoiceUseInPeriod2 = new List<int>();
        public string GetDenSoSuDungTrongKi(TT153PublishInvoiceDetail tT153PublishInvoiceDetail)
        {
            //lstSaInvoiceUseInPeriod = _ISAInvoiceService.GetListSAInVoiceTT153inPeriod(dteDateFrom.DateTime, dteDateTo.DateTime.Date.AddDays(1), tT153PublishInvoiceDetail.InvoiceTypeID,
            //        tT153PublishInvoiceDetail.InvoiceForm, tT153PublishInvoiceDetail.InvoiceTemplate);
            lstSaInvoiceUseInPeriod2 = new List<int>();

            if (lstSD.Count > 0) lstSaInvoiceUseInPeriod2.Add(lstSD.Select(n => int.Parse(n.InvoiceNo)).Max());
            if (lstSD1.Count > 0) lstSaInvoiceUseInPeriod2.Add(lstSD1.Select(n => int.Parse(n.InvoiceNo)).Max());
            if (lstSD2.Count > 0) lstSaInvoiceUseInPeriod2.Add(lstSD2.Select(n => int.Parse(n.InvoiceNo)).Max());
            if (lstSD3.Count > 0) lstSaInvoiceUseInPeriod2.Add(lstSD3.Select(n => int.Parse(n.InvoiceNo)).Max());

            if (lstSaInvoiceUseInPeriod2.Count > 0)
            {
                return lstSaInvoiceUseInPeriod2.Max().ToString().PadLeft(7, '0');
            }
            return "";
        }
        public string GetSelectMonthquarter()
        {
            return cbbMonth.Text + " Năm " + " " + txtYear.Value;
        }
        public void LoadList()
        {
            #region Fill lst
            data = new List<TT153UseOfInvoice>();
            List<TT153PublishInvoiceDetail> lst = Utils.ListTT153PublishInvoiceDetail.GroupBy(n => n.TT153ReportID).Select(n => n.First()).Where(n => n.StartUsing <= dteDateTo.DateTime.Date.AddDays(1)).ToList();
            if (lst.Count == 0)
                data.Add(new TT153UseOfInvoice
                { SelectMonthquarter = GetSelectMonthquarter() });
            else
                foreach (TT153PublishInvoiceDetail item in lst)
                {
                    string soluongxoa = GetSoLuongXoa(item);
                    string soluongmat = GetSoLuongMat(item);

                    TT153UseOfInvoice t153UseOfInvoice = new TT153UseOfInvoice();
                    t153UseOfInvoice.SelectMonthquarter = GetSelectMonthquarter();
                    t153UseOfInvoice.TT153ReportID = item.TT153ReportID;
                    t153UseOfInvoice.ReportName = item.ReportName;
                    t153UseOfInvoice.InvoiceTemplate = item.InvoiceTemplate;
                    t153UseOfInvoice.InvoiceSeries = item.InvoiceSeries;
                    t153UseOfInvoice.FromNoEarlyPeriod = GetTonDauKi_TuSo(item);
                    t153UseOfInvoice.ToNoEarlyPeriod = GetTonDauKi_DenSo(item);
                    //t153UseOfInvoice.Total = GetTongSoTon(item);
                    t153UseOfInvoice.FromNoPurchase = GetMuaPhatHanhtrongKi_TuSo(item);
                    t153UseOfInvoice.ToNoPurchase = GetMuaPhatHanhtrongKi_DenSo(item);
                    t153UseOfInvoice.QuantityDeleted = soluongxoa;
                    t153UseOfInvoice.QuantityLost = soluongmat;
                    t153UseOfInvoice.QuantityDestruction = GetSoLuongHuy(item);
                    t153UseOfInvoice.NoDeleted = GetSoXoa(item);
                    t153UseOfInvoice.NoLost = GetSoMat(item);
                    t153UseOfInvoice.NoDestruction = GetSoHuy(item);
                    t153UseOfInvoice.FromNoUse = GetTuSoSuDungTrongKi(item);
                    t153UseOfInvoice.ToNoUse = GetDenSoSuDungTrongKi(item);

                    //số lượng đã sử dụng
                    string soluongsudung = "";
                    if (lstSaInvoiceUseInPeriod.Count > 0)
                    {
                        int soluongsudung_ = 0;
                        if (!string.IsNullOrEmpty(soluongxoa) && string.IsNullOrEmpty(soluongmat))
                        {
                            soluongsudung_ = lstSaInvoiceUseInPeriod2.Max() - lstSaInvoiceUseInPeriod.Min() + 1 - int.Parse(soluongxoa);
                        }
                        else if (string.IsNullOrEmpty(soluongxoa) && !string.IsNullOrEmpty(soluongmat))
                        {
                            soluongsudung_ = lstSaInvoiceUseInPeriod2.Max() - lstSaInvoiceUseInPeriod.Min() + 1 - int.Parse(soluongmat);
                        }
                        else if (!string.IsNullOrEmpty(soluongxoa) && !string.IsNullOrEmpty(soluongmat))
                        {
                            soluongsudung_ = lstSaInvoiceUseInPeriod2.Max() - lstSaInvoiceUseInPeriod.Min() + 1 - int.Parse(soluongmat) - int.Parse(soluongxoa);
                        }
                        else
                        {
                            soluongsudung_ = lstSaInvoiceUseInPeriod2.Max() - lstSaInvoiceUseInPeriod.Min() + 1;
                        }
                        soluongsudung = soluongsudung_.ToString();
                    }
                    t153UseOfInvoice.QuantityUsed = soluongsudung;

                    //số lượng sử dụng bao gồm xóa mất hủy
                    if (!string.IsNullOrEmpty(t153UseOfInvoice.FromNoUse))
                    {
                        if (!string.IsNullOrEmpty(t153UseOfInvoice.NoDestruction))
                        {
                            if (int.Parse(t153UseOfInvoice.FromNoUse) > int.Parse(t153UseOfInvoice.NoDestruction.Split(';', ' ', '-')[0]))
                            {
                                t153UseOfInvoice.FromNoUse = t153UseOfInvoice.NoDestruction;
                            }
                            if (int.Parse(t153UseOfInvoice.ToNoUse) < int.Parse(t153UseOfInvoice.NoDestruction.Substring(t153UseOfInvoice.NoDestruction.Length - 7, 7)))
                            {
                                t153UseOfInvoice.ToNoUse = t153UseOfInvoice.NoDestruction.Substring(t153UseOfInvoice.NoDestruction.Length - 7, 7);
                            }
                            t153UseOfInvoice.Sum = (int.Parse(t153UseOfInvoice.ToNoUse) - int.Parse(t153UseOfInvoice.FromNoUse) + 1).ToString();
                        }
                        else
                        {
                            t153UseOfInvoice.Sum = (int.Parse(t153UseOfInvoice.ToNoUse) - int.Parse(t153UseOfInvoice.FromNoUse) + 1).ToString();
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(t153UseOfInvoice.NoDestruction))
                        {
                            t153UseOfInvoice.Sum = "";
                        }
                        else
                        {
                            t153UseOfInvoice.FromNoUse = lstDestruction.Select(n => int.Parse(n.FromNo)).Min().ToString().PadLeft(7, '0');
                            t153UseOfInvoice.ToNoUse = lstDestruction.Select(n => int.Parse(n.ToNo)).Max().ToString().PadLeft(7, '0');
                            t153UseOfInvoice.Sum = (int.Parse(t153UseOfInvoice.ToNoUse) - int.Parse(t153UseOfInvoice.FromNoUse) + 1).ToString();
                        }
                    }


                    //if (!string.IsNullOrEmpty(t153UseOfInvoice.FromNoEarlyPeriod))
                    //{
                    //    if (int.Parse(t153UseOfInvoice.FromNoEarlyPeriod) > int.Parse(t153UseOfInvoice.ToNoEarlyPeriod))
                    //    {
                    //        t153UseOfInvoice.FromNoEarlyPeriod = "";
                    //        t153UseOfInvoice.ToNoEarlyPeriod = "";
                    //    }
                    //}

                    //Tổng số
                    if (string.IsNullOrEmpty(t153UseOfInvoice.FromNoEarlyPeriod) && !string.IsNullOrEmpty(t153UseOfInvoice.FromNoPurchase))
                    {
                        t153UseOfInvoice.Total = (int.Parse(t153UseOfInvoice.ToNoPurchase) - int.Parse(t153UseOfInvoice.FromNoPurchase) + 1).ToString();

                    }
                    else if (!string.IsNullOrEmpty(t153UseOfInvoice.FromNoEarlyPeriod) && string.IsNullOrEmpty(t153UseOfInvoice.FromNoPurchase))
                    {
                        t153UseOfInvoice.Total = (int.Parse(t153UseOfInvoice.ToNoEarlyPeriod) - int.Parse(t153UseOfInvoice.FromNoEarlyPeriod) + 1).ToString();
                    }
                    else if (!string.IsNullOrEmpty(t153UseOfInvoice.FromNoEarlyPeriod) && !string.IsNullOrEmpty(t153UseOfInvoice.FromNoPurchase))
                    {
                        t153UseOfInvoice.Total = (int.Parse(t153UseOfInvoice.ToNoPurchase) - int.Parse(t153UseOfInvoice.FromNoPurchase) + 1 + int.Parse(t153UseOfInvoice.ToNoEarlyPeriod) - int.Parse(t153UseOfInvoice.FromNoEarlyPeriod) + 1).ToString();
                    }
                    else
                    {
                        t153UseOfInvoice.Total = "";
                    }

                    // Fill tồn cuối kì
                    if (string.IsNullOrEmpty(t153UseOfInvoice.Sum))
                    {
                        if (!string.IsNullOrEmpty(t153UseOfInvoice.FromNoEarlyPeriod))
                        {
                            if (!string.IsNullOrEmpty(t153UseOfInvoice.ToNoPurchase))
                            {
                                t153UseOfInvoice.FromNoEndPeriod = t153UseOfInvoice.FromNoEarlyPeriod;
                                t153UseOfInvoice.ToNoEndPeriod = t153UseOfInvoice.ToNoPurchase;
                                t153UseOfInvoice.Quantity = t153UseOfInvoice.Total;
                            }
                            else
                            {
                                t153UseOfInvoice.FromNoEndPeriod = t153UseOfInvoice.FromNoEarlyPeriod;
                                t153UseOfInvoice.ToNoEndPeriod = t153UseOfInvoice.ToNoEarlyPeriod;
                                t153UseOfInvoice.Quantity = t153UseOfInvoice.Total;
                            }

                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(t153UseOfInvoice.ToNoPurchase))
                            {
                                t153UseOfInvoice.FromNoEndPeriod = t153UseOfInvoice.FromNoPurchase;
                                t153UseOfInvoice.ToNoEndPeriod = t153UseOfInvoice.ToNoPurchase;
                                t153UseOfInvoice.Quantity = t153UseOfInvoice.Total;
                            }
                            else
                            {
                                t153UseOfInvoice.FromNoEndPeriod = "";
                                t153UseOfInvoice.ToNoEndPeriod = "";
                                t153UseOfInvoice.Quantity = "";
                            }
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(t153UseOfInvoice.ToNoPurchase))
                        {
                            if (t153UseOfInvoice.ToNoPurchase == t153UseOfInvoice.ToNoUse)
                            {
                                t153UseOfInvoice.FromNoEndPeriod = "";
                                t153UseOfInvoice.ToNoEndPeriod = "";
                                t153UseOfInvoice.Quantity = "";
                            }
                            else
                            {
                                t153UseOfInvoice.FromNoEndPeriod = (int.Parse(t153UseOfInvoice.ToNoUse) + 1).ToString().PadLeft(7, '0');
                                t153UseOfInvoice.ToNoEndPeriod = t153UseOfInvoice.ToNoPurchase;
                                t153UseOfInvoice.Quantity = (int.Parse(t153UseOfInvoice.ToNoEndPeriod) - int.Parse(t153UseOfInvoice.FromNoEndPeriod) + 1).ToString();
                            }
                        }
                        else
                        {
                            if (t153UseOfInvoice.ToNoEarlyPeriod == t153UseOfInvoice.ToNoUse)
                            {
                                t153UseOfInvoice.FromNoEndPeriod = "";
                                t153UseOfInvoice.ToNoEndPeriod = "";
                                t153UseOfInvoice.Quantity = "";
                            }
                            else
                            {
                                t153UseOfInvoice.FromNoEndPeriod = (int.Parse(t153UseOfInvoice.ToNoUse) + 1).ToString().PadLeft(7, '0');
                                t153UseOfInvoice.ToNoEndPeriod = t153UseOfInvoice.ToNoEarlyPeriod;
                                t153UseOfInvoice.Quantity = (int.Parse(t153UseOfInvoice.ToNoEndPeriod) - int.Parse(t153UseOfInvoice.FromNoEndPeriod) + 1).ToString();
                            }
                        }
                    }
                    data.Add(t153UseOfInvoice);
                }
            #endregion
        }
        private void btnOk_Click(object sender, EventArgs e)
        {
            WaitingFrm.StartWaiting();
            #region CheckError
            #endregion
            if (!CheckError()) return;

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.MonthOrPrecious = ultraOptionSet1.Value.ToInt() + 1;
                if (ultraOptionSet1.Value.ToInt() == 0)
                {
                    dateTimeCacheHistory.Month = cbbMonth.SelectedRow.Index;
                }
                else
                {
                    dateTimeCacheHistory.Precious = cbbMonth.SelectedRow.Index;
                }
                dateTimeCacheHistory.Year = txtYear.Value.ToInt();
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;

                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.MonthOrPrecious = ultraOptionSet1.Value.ToInt() + 1;
                if (ultraOptionSet1.Value.ToInt() == 0)
                {
                    dateTimeCacheHistory.Month = cbbMonth.SelectedRow.Index;
                }
                else
                {
                    dateTimeCacheHistory.Precious = cbbMonth.SelectedRow.Index;
                }
                dateTimeCacheHistory.Year = txtYear.Value.ToInt();
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            LoadList();
            #region Fill All
            #endregion
            WaitingFrm.StopWaiting();
            #region Show report
            var f = new ReportForm<TT153UseOfInvoice>(fileReportSlot1, "");
            f.AddDatasource("Data", data, true);
            //f.SetHyperlinkFunction("Accounting.ReportUtils", "Hyperlink");
            f.LoadReport();
            f.WindowState = FormWindowState.Maximized;
            f.Show();
            #endregion

        }


        private void txtYear_ValueChanged(object sender, EventArgs e)
        {
            DateTime t1 = dteDateFrom.DateTime;
            DateTime t2 = dteDateTo.DateTime;//.Date.AddDays(1);
            if(cbbMonth.Text=="Tháng 2")
            {
                var model = cbbMonth.SelectedRow.ListObject as Item;
                DateTime dtBegin;
                DateTime dtEnd;
                Utils.GetDateBeginDateEnd((int)txtYear.Value, DateTime.Now, model, out dtBegin, out dtEnd);
                dteDateFrom.Value = new DateTime((int)txtYear.Value, t1.Month, t1.Day);
                dteDateTo.Value = new DateTime((int)txtYear.Value, t2.Month, dtEnd.Day);
            }
            else
            {
                dteDateFrom.Value = new DateTime((int)txtYear.Value, t1.Month, t1.Day);
                dteDateTo.Value = new DateTime((int)txtYear.Value, t2.Month, t2.Day);
            }
            
        }

        private void cbbMonth_ValueChanged(object sender, EventArgs e)
        {
            if (cbbMonth.SelectedRow != null)
            {
                var model = cbbMonth.SelectedRow.ListObject as Item;
                DateTime dtBegin;
                DateTime dtEnd;
                Utils.GetDateBeginDateEnd((int)txtYear.Value, DateTime.Now, model, out dtBegin, out dtEnd);
                dteDateFrom.Value = new DateTime((int)txtYear.Value, dtBegin.Month, dtBegin.Day);
                dteDateTo.Value = new DateTime((int)txtYear.Value, dtEnd.Month, dtEnd.Day);
            }
        }

        private void ultraOptionSet_ValueChanged(object sender, EventArgs e)
        {
            if (ultraOptionSet1.CheckedIndex == 0)
            {
                ultraLabel1.Text = "Tháng";
                cbbMonth.ConfigComboSelectTime(_lstItems.Where(n => int.Parse(n.Value) < 21 && int.Parse(n.Value) > 8).ToList(), "Name");
                foreach (var item in cbbMonth.Rows)
                {
                    if (int.Parse((item.ListObject as Item).Value) == DateTime.Now.Month + 8) cbbMonth.SelectedRow = item;
                }
            }
            else
            {
                ultraLabel1.Text = "Quý";
                cbbMonth.ConfigComboSelectTime(_lstItems.Where(n => int.Parse(n.Value) < 25 && int.Parse(n.Value) > 20).ToList(), "Name");
                float now = float.Parse(DateTime.Now.Month.ToString()) / 3;
                foreach (var item in cbbMonth.Rows)
                {
                    if (now <= 1)
                    {
                        if (int.Parse((item.ListObject as Item).Value) == 21) cbbMonth.SelectedRow = item;
                    }
                    if (now <= 2 && now > 1)
                    {
                        if (int.Parse((item.ListObject as Item).Value) == 22) cbbMonth.SelectedRow = item;
                    }
                    if (now <= 3 && now > 2)
                    {
                        if (int.Parse((item.ListObject as Item).Value) == 23) cbbMonth.SelectedRow = item;
                    }
                    if (now <= 4 && now > 3)
                    {
                        if (int.Parse((item.ListObject as Item).Value) == 24) cbbMonth.SelectedRow = item;
                    }

                }

            }
        }
    }
}