﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FTest0 : Form
    {
        #region khai báo
        public ITemplateService ItemplateService { get { return IoC.Resolve<ITemplateService>(); } }
        public ITemplateDetailService ItemplateDetailService { get { return IoC.Resolve<ITemplateDetailService>(); } }
        public ITemplateColumnService ItemplateColumnService { get { return IoC.Resolve<ITemplateColumnService>(); } }

        #region Danh mục
        public IAccountService IaccountService { get { return IoC.Resolve<IAccountService>(); } }
        public IAccountGroupService IaccountGroupService { get { return IoC.Resolve<IAccountGroupService>(); } }
        public IAccountTransferService IaccountTransferService { get { return IoC.Resolve<IAccountTransferService>(); } }
        public IAccountDefaultService IaccountDefaultService { get { return IoC.Resolve<IAccountDefaultService>(); } }
        public IAutoPrincipleService IautoPrincipleService { get { return IoC.Resolve<IAutoPrincipleService>(); } }
        public IDepartmentService IdepartmentService { get { return IoC.Resolve<IDepartmentService>(); } }
        public IAccountingObjectCategoryService IaccountingObjectCategoryService { get { return IoC.Resolve<IAccountingObjectCategoryService>(); } }
        public IAccountingObjectGroupService IaccountingObjectGroupService { get { return IoC.Resolve<IAccountingObjectGroupService>(); } }
        public IAccountingObjectService IaccountingObjectService { get { return IoC.Resolve<IAccountingObjectService>(); } }
        public IPersonalSalaryTaxService IpersonalSalaryTaxService { get { return IoC.Resolve<IPersonalSalaryTaxService>(); } }
        public ITimeSheetSymbolsService ItimeSheetSymbolsService { get { return IoC.Resolve<ITimeSheetSymbolsService>(); } }
        public IRepositoryService IrepositoryService { get { return IoC.Resolve<IRepositoryService>(); } }
        public IMaterialGoodsCategoryService ImaterialGoodsCategoryService { get { return IoC.Resolve<IMaterialGoodsCategoryService>(); } }
        public IMaterialGoodsService ImaterialGoodsService { get { return IoC.Resolve<IMaterialGoodsService>(); } }
        public IMaterialQuantumService ImaterialQuantumService { get { return IoC.Resolve<IMaterialQuantumService>(); } }
        public IMaterialQuantumDetailService ImaterialQuantumDetailService { get { return IoC.Resolve<IMaterialQuantumDetailService>(); } }
        public IFixedAssetCategoryService IfixedAssetCategoryService { get { return IoC.Resolve<IFixedAssetCategoryService>(); } }
        public IFixedAssetService IfixedAssetService { get { return IoC.Resolve<IFixedAssetService>(); } }
        public IFixedAssetDetailService IfixedAssetDetailService { get { return IoC.Resolve<IFixedAssetDetailService>(); } }
        public IExpenseItemService IexpenseItemService { get { return IoC.Resolve<IExpenseItemService>(); } }
        //public ICostFactorCategoryService IcostFactorCategoryService { get { return IoC.Resolve<ICostFactorCategoryService>(); } }
        public ICostSetService IcostSetService { get { return IoC.Resolve<ICostSetService>(); } }
        public IStockCategoryService IstockCategoryService { get { return IoC.Resolve<IStockCategoryService>(); } }
        public IInvestorGroupService IinvestorGroupService { get { return IoC.Resolve<IInvestorGroupService>(); } }
        public IInvestorService IinvestorService { get { return IoC.Resolve<IInvestorService>(); } }
        public IRegistrationGroupService IregistrationGroupService { get { return IoC.Resolve<IRegistrationGroupService>(); } }
        public IShareHolderGroupService IshareHolderGroupService { get { return IoC.Resolve<IShareHolderGroupService>(); } }
        public IBankService IbankService { get { return IoC.Resolve<IBankService>(); } }
        public IBankAccountDetailService IbankAccountDetailService { get { return IoC.Resolve<IBankAccountDetailService>(); } }
        public ICreditCardService IcreditCardService { get { return IoC.Resolve<ICreditCardService>(); } }
        public IStatisticsCodeService IstatisticsCodeService { get { return IoC.Resolve<IStatisticsCodeService>(); } }
        public IPaymentClauseService IpaymentClauseService { get { return IoC.Resolve<IPaymentClauseService>(); } }
        public ITransportMethodService ItransportMethodService { get { return IoC.Resolve<ITransportMethodService>(); } }
        public IBudgetItemService IbudgetItemService { get { return IoC.Resolve<IBudgetItemService>(); } }
        //public ISalePriceGroupService IsalePriceGroup { get { return IoC.Resolve<ISalePriceGroup>(); } }
        //public IGoodsServicePurchaseService IgoodsServicePurchase { get { return IoC.Resolve<IGoodsServicePurchase>(); } }
        //public IMaterialGoodsSpecialTaxGroupService ImaterialGoodsSpecialTaxGroup { get { return IoC.Resolve<IMaterialGoodsSpecialTaxGroup>(); } }
        public ITypeService ItypeService { get { return IoC.Resolve<ITypeService>(); } }
        public ITypeGroupService ItypeGroupService { get { return IoC.Resolve<ITypeGroupService>(); } }
        public ICurrencyService IcurrencyService { get { return IoC.Resolve<ICurrencyService>(); } }
        public IContractStateService IcontractStateService { get { return IoC.Resolve<IContractStateService>(); } }
        #endregion  //danh mục
        #endregion

        BindingList<TemplateColumn> _dsTemplateColumn = new BindingList<TemplateColumn>();

        private readonly IAccountGroupService _iAccountGroupService;
        private readonly IMCReceiptService _mcReceiptService;
        private readonly IAccountGroupService _IAccountGroupService;

        public FTest0()
        {
            InitializeComponent();

            ConfigGridAutoGenConstDatabase();

            #region tab 1

            //_IAccountGroupService = IoC.Resolve<IAccountGroupService>();
            //List<AccountGroup> ltemp = _IAccountGroupService.GetAll();

            //ultraCombo1.DataSource = ltemp;
            //ultraCombo1.DisplayMember = "AccountGroupName";
            //ultraCombo1.ValueMember = "ID";

            //Utils.ConfigGrid(ultraCombo1, "AccountGroup");

            #endregion

            #region tab 2

            ////Test tìm vị trí của một obj trong một list obj
            //_mcReceiptService = IoC.Resolve<IMCReceiptService>();

            //List<MCReceipt> dsMCReceipts = _mcReceiptService.GetAll();
            //MCReceipt mcReceipt = _mcReceiptService.getMCReceiptbyNo("PT000009");
            //int index = Utils.GetIndexOfList(mcReceipt, dsMCReceipts);

            #endregion

            #region tab 3
            //_IAccountGroupService = IoC.Resolve<IAccountGroupService>();

            //BindingList<AccountGroup> bdl = new BindingList<AccountGroup>(_IAccountGroupService.GetAll());
            //foreach (AccountGroup accountGroup in bdl)
            //    accountGroup.AccountGroupKindView = accountGroup.AccountGroupKind.ToString();
            //ultraGrid1.DataSource = bdl;

            //ultraGrid1.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
            //ultraGrid1.DisplayLayout.Override.TemplateAddRowPrompt = "Bấm vào đây để thêm mới....";
            //ultraGrid1.DisplayLayout.Override.SpecialRowSeparator = Infragistics.Win.UltraWinGrid.SpecialRowSeparator.None;

            //string nameTable = ConstDatabase.AccountGroup_TableName;
            //Utils.ConfigGrid(ultraGrid1, nameTable, new List<TemplateColumn>(), 0);
            ////hiển thị 1 band
            //ultraGrid1.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            ////tắt lọc cột
            //ultraGrid1.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.False;
            //ultraGrid1.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.Default;
            ////tự thay đổi kích thước cột
            //ultraGrid1.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            ////tắt tiêu đề
            //ultraGrid1.DisplayLayout.CaptionVisible = DefaultableBoolean.False;
            ////select cả hàng hay ko?
            //ultraGrid1.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            //ultraGrid1.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            ////Hiện những dòng trống?
            //ultraGrid1.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            //ultraGrid1.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.ExtendFirstCell;
            ////Fix Header
            //ultraGrid1.DisplayLayout.UseFixedHeaders = true;

            ////tao style cho colum
            //foreach (var item in ultraGrid1.DisplayLayout.Bands[0].Columns)
            //{
            //    if (item.Key.Equals("AccountGroupKindView"))
            //    {
            //        //EditorWithCombo editer = new EditorWithCombo();
            //        ValueList valueList = new ValueList();
            //        valueList.ValueListItems.Add(1, "Theo %");
            //        valueList.ValueListItems.Add(2, "Theo số tiền");
            //        valueList.ValueListItems.Add(3, "Theo đơn giá (số tiền CK/1 đơn vị SL)");
            //        //editer = valueList;
            //        //item.Editor = editer;
            //        item.Style = ColumnStyle.DropDownList;
            //        item.ValueList = valueList;
            //    }
            //}
            List<TemplateColumn> temp = ItemplateColumnService.Query.Where(p => p.ColumnName.Contains("CustomField")).ToList();
            uGridTemplateColumnProperty.DataSource = temp;
            ConfigGrid(uGridTemplateColumnProperty);
            #endregion

            #region tab template editor
            //loại chứng từ
            cbbTypeGroup.DataSource = ItypeGroupService.GetAll();
            cbbTypeGroup.DisplayMember = "TypeGroupName";
            ConfigGrid(cbbTypeGroup, ConstDatabase.TypeGroup_TableName, new List<TemplateColumn>());
            if (cbbTypeGroup.Rows.Count > 0) cbbTypeGroup.Rows[0].Selected = true;
            uGridTemplate.DataSource = new BindingList<TemplateColumn>();
            ConfigGrid(uGridTemplate);
            #endregion

            IViewVouchersCloseBookService serv = IoC.Resolve<IViewVouchersCloseBookService>();
            var lst = serv.GetAll();
            uGridTestView.DataSource = lst;
        }

        #region Tab 1
        void test()
        {
            DataSet ds = new DataSet();
            DataTable dtCountries = ds.Tables.Add("Countries");
            dtCountries.Columns.Add("PK", typeof(int));
            dtCountries.Columns.Add("Name", typeof(string));
            DataTable dtCities = ds.Tables.Add("Cities");
            dtCities.Columns.Add("PK", typeof(int));
            dtCities.Columns.Add("CountryId", typeof(int));
            dtCities.Columns.Add("Name", typeof(string));

            dtCountries.Rows.Add(new object[] { 0, "Country A" });
            dtCountries.Rows.Add(new object[] { 1, "Country B" });
            dtCountries.Rows.Add(new object[] { 2, "Country C" });

            dtCities.Rows.Add(new object[] { 0, 0, "City A1" });
            dtCities.Rows.Add(new object[] { 1, 0, "City A2" });
            dtCities.Rows.Add(new object[] { 2, 1, "City B1" });
            dtCities.Rows.Add(new object[] { 3, 1, "City B2" });
            dtCities.Rows.Add(new object[] { 4, 2, "City C1" });
            dtCities.Rows.Add(new object[] { 5, 2, "City C2" });

            ds.Relations.Add("Country_City", dtCountries.Columns["PK"], dtCities.Columns["CountryId"]);

            this.ultraCombo2.SyncWithCurrencyManager = true;

            this.ultraCombo2.SetDataBinding(ds, "Countries");
            this.ultraCombo3.SetDataBinding(ds, "Countries.Country_City");
        }

        private void ultraButton1_Click(object sender, EventArgs e)
        {
            test();
        }

        private void ultraButton2_Click(object sender, EventArgs e)
        {
            var temp = getSelectCbbItem(ultraCombo1);
        }

        object getSelectCbbItem(Infragistics.Win.UltraWinGrid.UltraCombo cbb)
        {
            return cbb.SelectedRow == null ? cbb.SelectedRow : cbb.SelectedRow.ListObject;
        }

        private void ultraButton3_Click(object sender, EventArgs e)
        {
            new FTest1().ShowDialog(this);
        }

        private void ultraButton4_Click(object sender, EventArgs e)
        {
            decimal? test = null;
            test = test ?? 10;
        }

        private void ultraButton5_Click(object sender, EventArgs e)
        {
            new FTest2().ShowDialog(this);
        }
        #endregion

        private void ultraButton7_Click(object sender, EventArgs e)
        {
            txtKq.SelectAll();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //tab 4
            int bientam = TestTab4();       //10
            bientam = TestTab4(2);          //11
            bientam = TestTab4(2, 1);       //10
            bientam = TestTab4(2, 1, 1);    //8
            bientam = TestTab4(2, 1, 1, 1); //5
            bientam = TestTab4(c: 9);       //16
        }

        static int TestTab4(int a = 1, int b = 2, int c = 3, int d = 4)
        {
            int kq = 0;
            kq = a + b + c + d;
            return kq;
        }

        private void BtnGetDataClick(object sender, EventArgs e)
        {
            //get dữ liệu
            Core.Domain.Type select = (Core.Domain.Type)Utils.getSelectCbbItem(cbbType);
            //CBB TEMPLATE
            cbbTemplate.DataSource = ItemplateService.Query.Where(k => k.TypeID == select.ID).ToList();
            cbbTemplate.DisplayMember = "TemplateName";
            ConfigGrid(cbbTemplate, "Template", GetTemplateColums("Template"));
            if (cbbTemplate.Rows.Count > 0) cbbTemplate.Rows[0].Selected = true;
        }

        private void CbbTypeGroupRowSelected(object sender, RowSelectedEventArgs e)
        {
            TypeGroup select = (TypeGroup)Utils.getSelectCbbItem(cbbTypeGroup);
            //chứng từ
            cbbType.DataSource = ItypeService.Query.Where(k => k.TypeGroupID == select.ID).ToList();
            Utils.ConfigGrid(cbbType, ConstDatabase.Type_TableName);
            if (cbbType.Rows.Count > 0) cbbType.Rows[0].Selected = true;
        }

        private void CbbTemplateRowSelected(object sender, RowSelectedEventArgs e)
        {//khi chọn dữ liệu trong cbb Template thì tự fill dữ liệu sang cbb TemplateDetails
            Template select = (Template)Utils.getSelectCbbItem(cbbTemplate);
            if (select == null) return;
            cbbTemplateDetail.DataSource = ItemplateDetailService.Query.Where(k => k.TemplateID == select.ID).ToList();
            cbbTemplateDetail.DisplayMember = "TabCaption";
            cbbTemplateDetail.ValueMember = "ID";
            ConfigGrid(cbbTemplateDetail, "TemplateDetail", GetTemplateColums("TemplateDetail"));
            if (cbbTemplateDetail.Rows.Count > 0) cbbTemplateDetail.Rows[0].Selected = true;
        }

        private void CbbTemplateDetailRowSelected(object sender, RowSelectedEventArgs e)
        {
            LoadTemplate();
        }
        private void LoadTemplate()
        {
            Core.Domain.Type selectType = (Core.Domain.Type)Utils.getSelectCbbItem(cbbType);
            TemplateDetail select = (TemplateDetail)Utils.getSelectCbbItem(cbbTemplateDetail);
            if (select == null) return;
            List<TemplateColumn> dsTemp = ItemplateColumnService.Query.Where(k => k.TemplateDetailID == select.ID).ToList();
            _dsTemplateColumn = new BindingList<TemplateColumn>(dsTemp);
            uGridTemplate.DataSource = _dsTemplateColumn;
            //_dsTemplateColumn = new BindingList<TemplateColumn>();

            Dictionary<int, Dictionary<System.Type, System.Collections.IList>> dicObject = ConstDatabase.DicObjectForTemplete();
            Dictionary<System.Type, System.Collections.IList> @DicObject = new Dictionary<System.Type, System.Collections.IList>();
            List<System.Collections.IList> dsObject = new List<System.Collections.IList>();
            List<TemplateColumnsWrong> listWrong = new List<TemplateColumnsWrong>();
            foreach (KeyValuePair<int, Dictionary<System.Type, System.Collections.IList>> item in dicObject)
            {
                if (item.Key.Equals(selectType.ID))
                {
                    @DicObject = item.Value;
                    break;
                }
            }
            dsObject = DicObject.Values.ToList();
            foreach (object temp in dsObject)
            {
                UltraGrid grid = new UltraGrid();
                this.Controls.Add(grid);
                List<string> dsColumn = new List<string>();
                grid.DataSource = temp;
                foreach (UltraGridColumn column in grid.DisplayLayout.Bands[0].Columns)
                {
                    dsColumn.Add(column.Key);
                }
                //IList<TemplateColumn> configGui = mauGiaoDien;
                List<string> dsColumnTemplete = new List<string>();
                if (dsTemp.Count > 0) foreach (var item in dsTemp) dsColumnTemplete.Add(item.ColumnName);
                //bool check = true;
                foreach (string item in dsColumnTemplete)
                {
                    if (!item.StartsWith("Panel") && !item.Contains("Width") && !item.Contains("Height"))
                        if (dsColumn.Where(p => p.Equals(item)).Count() == 0)
                        {
                            TemplateColumnsWrong tmp = new TemplateColumnsWrong { ObjectName = temp.GetType().FullName.Split(new string[] { "Accounting.Core.Domain.", ", Accounting.Core" }, StringSplitOptions.RemoveEmptyEntries)[1], ColumnWrong = item };
                            listWrong.Add(tmp);
                        }
                }
                this.Controls.Remove(grid);
            }

            //lọc listWrong
            listWrong = select.TabIndex == 100 ? listWrong.Where(k => !k.ObjectName.Contains("Detail")).ToList() : listWrong.Where(k => k.ObjectName.Contains("Detail")).ToList();

            uGridTemplateColumnWrong.DataSource = listWrong;
            uGridTemplateColumnWrong.DisplayLayout.Bands[0].Columns["ColumnWrong"].Header.Caption = "Column sai";
            uGridTemplateColumnWrong.DisplayLayout.Bands[0].Columns["ObjectName"].Header.Caption = "Đối tượng";
            uGridTemplateColumnWrong.DisplayLayout.Bands[0].Columns["ObjectName"].CellActivation = Activation.NoEdit;
            //uGridTemplateColumnWrong.DisplayLayout.Bands[0].Columns["ColumnWrong"].CellActivation = Activation.NoEdit;
        }

        private void ConfigGrid(UltraGrid uGridTemplate)
        {
            //hiển thị 1 band
            uGridTemplate.DisplayLayout.ViewStyle = ViewStyle.SingleBand;
            //tắt lọc cột
            uGridTemplate.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.True;
            uGridTemplate.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            //tự thay đổi kích thước cột
            uGridTemplate.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            //tắt tiêu đề
            uGridTemplate.DisplayLayout.CaptionVisible = DefaultableBoolean.False;
            //select cả hàng hay ko?
            //uGridTemplate.DisplayLayout.Override.RowSelectors = DefaultableBoolean.True;
            uGridTemplate.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            //Hiển thị SupportDataErrorInfo
            uGridTemplate.DisplayLayout.Override.SupportDataErrorInfo = SupportDataErrorInfo.CellsOnly;
            //Hiện những dòng trống?
            uGridTemplate.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            uGridTemplate.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.ExtendFirstCell;
            //Sort
            uGridTemplate.DisplayLayout.Override.HeaderClickAction = HeaderClickAction.SortSingle;

            UltraGridBand band = uGridTemplate.DisplayLayout.Bands[0];
            band.Summaries.Clear();

            uGridTemplate.DisplayLayout.Override.AllowAddNew = AllowAddNew.TemplateOnBottom;
            uGridTemplate.DisplayLayout.Override.TemplateAddRowPrompt = resSystem.MSG_System_07;
            uGridTemplate.DisplayLayout.Override.SpecialRowSeparator = SpecialRowSeparator.None;
            //Fix Header
            uGridTemplate.DisplayLayout.UseFixedHeaders = true;
            // Turn on all of the Cut, Copy, and Paste functionality. 
            uGridTemplate.DisplayLayout.Override.AllowMultiCellOperations = AllowMultiCellOperation.All;

            Utils.ConfigGrid(uGridTemplate, "TemplateColumn", GetTemplateColums("TemplateColumn"), true);
        }

        #region Utils
        List<TemplateColumn> GetTemplateColums(string select)
        {
            Dictionary<string, Dictionary<string, TemplateColumn>> dicTemp = new Dictionary<string, Dictionary<string, TemplateColumn>>();
            string nameTable = string.Empty;
            List<string> strColumnName, strColumnCaption, strColumnToolTip = new List<string>();
            List<bool> bolIsReadOnly, bolIsVisible, bolIsVisibleCbb = new List<bool>();
            List<int> intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition = new List<int>();
            List<int> VTbolIsVisible, VTbolIsVisibleCbb = new List<int>();

            #region TemplateColumn
            nameTable = "TemplateColumn";
            strColumnName = new List<string> { "IsVisibleCbb", "ID", "TemplateDetailID", "ColumnCaption", "IsReadOnly", "ColumnWidth", "ColumnMaxWidth", "ColumnMinWidth", "IsVisible", "VisiblePosition", "ColumnToolTip", "ColumnName", "IsColumnHeader" };
            strColumnCaption = strColumnToolTip = new List<string> { "IsVisibleCbb", "ID", "TemplateDetailID", "ColumnCaption", "IsReadOnly", "ColumnWidth", "ColumnMaxWidth", "ColumnMinWidth", "IsVisible", "VisiblePosition", "ColumnToolTip", "ColumnName", "Header Fixed" };
            bolIsReadOnly = new List<bool>();
            bolIsVisible = new List<bool>();
            bolIsVisibleCbb = new List<bool>();
            intColumnWidth = new List<int>();
            intColumnMaxWidth = new List<int>();
            intColumnMinWidth = new List<int>();
            intVisiblePosition = new List<int> { 9, 10, 11, 1, 3, 4, 5, 6, 7, 8, 2, 0, 12 };
            VTbolIsVisible = new List<int> { 3, 4, 5, 6, 7, 8, 9, 10, 12, 11 };    //vị trí có giá trị bằng true
            VTbolIsVisibleCbb = new List<int> { 1, 2 };
            for (int i = 0; i < 13; i++)
            {
                bolIsVisible.Add(VTbolIsVisible.Contains(i));
                bolIsVisibleCbb.Add(VTbolIsVisibleCbb.Contains(i));
                bolIsReadOnly.Add(false);
                intColumnWidth.Add(-1);
                intColumnMaxWidth.Add(-1);
                intColumnMinWidth.Add(-1);
                //intVisiblePosition.Add(-1);
            }
            List<TemplateColumn> dsTemplateColumn = ConstDatabase.CreateData(strColumnName, strColumnCaption, strColumnToolTip, bolIsReadOnly, bolIsVisible, bolIsVisibleCbb, intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition).Values.ToList();
            #endregion

            #region TemplateDetail
            nameTable = "TemplateDetail";
            strColumnName = new List<string> { "TemplateColumns", "ID", "TemplateID", "GridType", "TabIndex", "TabCaption", };
            strColumnCaption = strColumnToolTip = new List<string> { "TemplateColumns", "ID", "TemplateID", "GridType", "TabIndex", "TabCaption", };
            bolIsReadOnly = new List<bool>();
            bolIsVisible = new List<bool>();
            bolIsVisibleCbb = new List<bool>();
            intColumnWidth = new List<int>();
            intColumnMaxWidth = new List<int>();
            intColumnMinWidth = new List<int>();
            intVisiblePosition = new List<int>();
            VTbolIsVisible = new List<int> { 1, 2 };    //vị trí có giá trị bằng true
            VTbolIsVisibleCbb = new List<int> { 5, 4 };
            for (int i = 0; i < 6; i++)
            {
                bolIsVisible.Add(VTbolIsVisible.Contains(i));
                bolIsVisibleCbb.Add(VTbolIsVisibleCbb.Contains(i));
                bolIsReadOnly.Add(false);
                intColumnWidth.Add(-1);
                intColumnMaxWidth.Add(-1);
                intColumnMinWidth.Add(-1);
                intVisiblePosition.Add(-1);
            }
            List<TemplateColumn> dsTemplateDetail = ConstDatabase.CreateData(strColumnName, strColumnCaption, strColumnToolTip, bolIsReadOnly, bolIsVisible, bolIsVisibleCbb, intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition).Values.ToList();
            #endregion

            #region Template
            nameTable = "Template";
            strColumnName = new List<string> { "TemplateDetails", "ID", "TemplateName", "TypeID", "IsActive", "IsSecurity", "OrderPriority", "IsDefault", };
            strColumnCaption = strColumnToolTip = new List<string> { "TemplateDetails", "ID", "TemplateName", "TypeID", "IsActive", "IsSecurity", "OrderPriority", "IsDefault", };
            bolIsReadOnly = new List<bool>();
            bolIsVisible = new List<bool>();
            bolIsVisibleCbb = new List<bool>();
            intColumnWidth = new List<int>();
            intColumnMaxWidth = new List<int>();
            intColumnMinWidth = new List<int>();
            intVisiblePosition = new List<int>();
            VTbolIsVisible = new List<int> { 1, 2 };    //vị trí có giá trị bằng true
            VTbolIsVisibleCbb = new List<int> { 2 };
            for (int i = 0; i < 8; i++)
            {
                bolIsVisible.Add(VTbolIsVisible.Contains(i));
                bolIsVisibleCbb.Add(VTbolIsVisibleCbb.Contains(i));
                bolIsReadOnly.Add(false);
                intColumnWidth.Add(-1);
                intColumnMaxWidth.Add(-1);
                intColumnMinWidth.Add(-1);
                intVisiblePosition.Add(-1);
            }
            List<TemplateColumn> dsTemplate = ConstDatabase.CreateData(strColumnName, strColumnCaption, strColumnToolTip, bolIsReadOnly, bolIsVisible, bolIsVisibleCbb, intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition).Values.ToList();
            #endregion

            if (select.Equals("Template")) return dsTemplate;
            return @select.Equals("TemplateDetail") ? dsTemplateDetail : dsTemplateColumn;
        }

        public static void ConfigGrid(UltraCombo utralCombo, string nameTable, IList<TemplateColumn> configGUI)
        {
            Dictionary<string, TemplateColumn> dicconfigGUI = new Dictionary<string, TemplateColumn>();
            if (configGUI.Count > 0) foreach (var item in configGUI) dicconfigGUI.Add(item.ColumnName, item);

            UltraGridBand band = utralCombo.DisplayLayout.Bands[0];
            band.Override.CellClickAction = CellClickAction.RowSelect;
            band.Override.SelectedRowAppearance.BackColor = Color.FromArgb(255, 192, 128);

            //Tiêu đề cột
            string headerCaptionFirst = string.Empty;
            foreach (var item in band.Columns)
            {
                if (dicconfigGUI.Count > 0 ? dicconfigGUI.ContainsKey(item.Key) : ConstDatabase.TablesInDatabase[nameTable].ContainsKey(item.Key))
                {
                    TemplateColumn temp = dicconfigGUI.Count > 0 ? dicconfigGUI[item.Key] : ConstDatabase.TablesInDatabase[nameTable][item.Key];
                    if (string.IsNullOrEmpty(headerCaptionFirst) && temp.IsVisible) headerCaptionFirst = item.Key;   //Lấy dòng Header Caption đầu tiên
                    item.Header.Caption = temp.ColumnCaption;
                    item.Header.ToolTipText = temp.ColumnToolTip;
                    item.Header.Enabled = !temp.IsReadOnly;
                    item.Width = temp.ColumnWidth == -1 ? item.Width : temp.ColumnWidth;
                    item.MaxWidth = temp.ColumnMaxWidth == -1 ? item.MaxWidth : temp.ColumnMaxWidth;
                    item.MinWidth = temp.ColumnMinWidth == -1 ? item.MinWidth : temp.ColumnMinWidth;
                    item.Hidden = !temp.IsVisibleCbb;
                }
                else { if (dicconfigGUI.Count > 0) item.Hidden = true; }
            }
        }
        #endregion

        private void BtnUpdateDataClick(object sender, EventArgs e)
        {
            //Update dữ liệu template colum
            bool isError = true;
            TemplateDetail select = (TemplateDetail)Utils.getSelectCbbItem(cbbTemplateDetail);
            if (select == null) return;
            List<TemplateColumn> dsTemp = ItemplateColumnService.Query.Where(k => k.TemplateDetailID == select.ID).ToList();

            TemplateDetail objTemp =
                    ItemplateDetailService.Query.Where(a => a.ID == (Guid)cbbTemplateDetail.Value).FirstOrDefault();
            if (objTemp != null)
            {
                objTemp.TemplateColumns = _dsTemplateColumn;
                if (_dsTemplateColumn.Count > dsTemp.Count)
                {
                    for (int i = dsTemp.Count; i < _dsTemplateColumn.Count; i++)
                    {
                        objTemp.TemplateColumns[i].ID = Guid.NewGuid();
                        objTemp.TemplateColumns[i].TemplateDetailID = objTemp.ID;
                        //objTemp.TemplateColumns.Add(_dsTemplateColumn[i]);
                    }
                }
                //foreach (TemplateColumn templateColumn in _dsTemplateColumn)
                //{
                try
                {
                    ItemplateDetailService.BeginTran();
                    //ItemplateColumnService.BeginTran();
                    ItemplateDetailService.Update(objTemp);
                    //ItemplateColumnService.Update(templateColumn);
                    ItemplateDetailService.CommitTran();
                    //ItemplateColumnService.CommitTran();
                    isError = false;
                }
                catch
                {
                    ItemplateDetailService.RolbackTran();
                    //ItemplateColumnService.RolbackTran();
                    isError = true;
                }
                //}
            }
            if (isError)
                MSG.MessageBoxStand("Có lỗi xáy ra", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
                MessageBox.Show("Cập nhật xong!");
            //Load lại dữ liệu
            LoadTemplate();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //Uncheck
            foreach (TemplateColumn templateColum in _dsTemplateColumn) templateColum.IsVisible = false;
        }

        private void BtnShowTemplateEditorFormClick(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtTypeID.Text))
            {
                MSG.Error("Bạn chưa chọn loại chứng từ!");
                return;
            }
            int typeID;
            if (!int.TryParse(txtTypeID.Text, out typeID))
            {
                MSG.Error("Loại chứng từ phải là số!");
                return;
            }
            new FTemplate(typeID).Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            new FTest2().Show();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (
                MessageBox.Show("Bạn muốn xóa dòng TemplateColumn này không?", "Phần mềm kế toán",
                                MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1) ==
                DialogResult.Yes)
            {
                if (uGridTemplate.ActiveRow == null) return;

                TemplateColumn temp = uGridTemplate.ActiveRow.ListObject as TemplateColumn;
                //TemplateColumn templateColumn = ItemplateColumnService.Getbykey(temp.ID);
                TemplateDetail objTemp =
                    ItemplateDetailService.Query.Where(a => a.TemplateColumns.Contains(temp)).FirstOrDefault();
                objTemp.TemplateColumns.Remove(temp);

                try
                {
                    ItemplateDetailService.Update(objTemp);
                    ItemplateColumnService.Delete(temp.ID);
                    ItemplateColumnService.CommitChanges();
                    MSG.MessageBoxStand("Xóa thành công", MessageBoxButtons.OK, MessageBoxIcon.None);
                    LoadTemplate();
                }
                catch
                {
                    ItemplateColumnService.RolbackTran();
                    MSG.MessageBoxStand("Có lỗi xáy ra", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            List<TemplateColumn> temp = (List<TemplateColumn>)uGridTemplateColumnProperty.DataSource;
            try
            {
                ItemplateColumnService.BeginTran();
                foreach (TemplateColumn templateColumn in temp)
                {
                    string str =
                        templateColumn.ColumnName.Split(new string[] { "CustomField" }, StringSplitOptions.RemoveEmptyEntries)
                            [0];
                    templateColumn.ColumnName = "CustomProperty" + str;
                    ItemplateColumnService.Update(templateColumn);
                }
                ItemplateColumnService.CommitTran();
                MSG.MessageBoxStand("Chuyển đổi thành công", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                List<TemplateColumn> TemplateColumn = ItemplateColumnService.Query.Where(p => p.ColumnName.Contains("CustomField")).ToList();
                uGridTemplateColumnProperty.DataSource = TemplateColumn;
            }
            catch
            {
                ItemplateColumnService.RolbackTran();
            }
        }

        private void btnMoveUp_Click(object sender, EventArgs e)
        {
            FTemplateDetail.ChangeIndexTemplateColum(uGridTemplate.ActiveRow.Index, uGridTemplate.ActiveRow.Index - (((UltraButton)sender).Name.Equals("btnMoveUp") ? 1 : -1), (BindingList<TemplateColumn>)uGridTemplate.DataSource, uGridTemplate);
        }

        #region AutoGen ConstDatabase
        void ConfigGridAutoGenConstDatabase()
        {
            //
        }

        private void ultraButton6_Click(object sender, EventArgs e)
        {
            string VTbolIsVisible = string.IsNullOrEmpty(txtVTbolIsVisible.Text) ? "1,2" : txtVTbolIsVisible.Text;
            string VTintVisiblePosition = string.IsNullOrEmpty(txtVTintVisiblePosition.Text) ? "" : txtVTintVisiblePosition.Text;
            string VTbolIsReadOnly = string.IsNullOrEmpty(txtVTbolIsReadOnly.Text) ? "" : txtVTbolIsReadOnly.Text;
            string strColumnCaption = string.IsNullOrEmpty(txtstrColumnCaption.Text) ? "" : txtstrColumnCaption.Text;
            txtKq.Text = CreateStrConstDatabase(txtNameObj.Text, VTbolIsVisible, VTintVisiblePosition, VTbolIsReadOnly, strColumnCaption);
        }

        static string CreateStrConstDatabase(string nameTable, string VTbolIsVisible = "1,2", string VTintVisiblePosition = "", string VTbolIsReadOnly = "", string strColumnCaption = "")
        {
            //đọc đối tượng và tạo chuỗi
            //const string patternStrBAK = "#region {0}\r\n            nameTable = {0}_TableName;\r\n            strColumnName = new List<string> { {1} };\r\n //{3} \r\n            strColumnCaption = strColumnToolTip = new List<string> { {1} };\r\n            bolIsReadOnly = new List<bool>();\r\n            bolIsVisible = new List<bool>();\r\n            bolIsVisibleCbb = new List<bool>();\r\n            intColumnWidth = new List<int>();\r\n            intColumnMaxWidth = new List<int>();\r\n            intColumnMinWidth = new List<int>();\r\n            intVisiblePosition = new List<int>();\r\n            VTbolIsVisible = new List<int> { 1, 2 };    //vị trí có giá trị bằng true\r\n            VTbolIsVisibleCbb = new List<int> { 1, 2 };\r\n            for (int i = 0; i < {2}; i++)\r\n            {\r\n                bolIsVisible.Add(VTbolIsVisible.Contains(i));\r\n                bolIsVisibleCbb.Add(VTbolIsVisibleCbb.Contains(i));\r\n                bolIsReadOnly.Add(false);\r\n                intColumnWidth.Add(-1);\r\n                intColumnMaxWidth.Add(-1);\r\n                intColumnMinWidth.Add(-1);\r\n                intVisiblePosition.Add(-1);\r\n            }\r\n            dicTemp.Add(nameTable, CreateData(strColumnName, strColumnCaption, strColumnToolTip, bolIsReadOnly, bolIsVisible, bolIsVisibleCbb, intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition));\r\n            #endregion";
            //const string patternStr = "#region {0}\r\n            nameTable = {0}_TableName;\r\n            strColumnName = new List<string> { {1} };\r\n //{3} \r\n            strColumnCaption = strColumnToolTip = new List<string> { {1} };\r\n            bolIsReadOnly = new List<bool>();\r\n            bolIsVisible = new List<bool>();\r\n            bolIsVisibleCbb = new List<bool>();\r\n            intColumnWidth = new List<int>();\r\n            intColumnMaxWidth = new List<int>();\r\n            intColumnMinWidth = new List<int>();\r\n            intVisiblePosition = new List<int>();\r\n            VTbolIsVisible = new List<int> { 1, 2 };    //vị trí có giá trị bằng true\r\n            VTbolIsVisibleCbb = new List<int> { 1, 2 };\r\n            VTintVisiblePosition = new List<int>() {  };\r\n            VTbolIsReadOnly = new List<int>() {  };\r\n \r\n            dicVisiblePosition = new Dictionary<int, int>();   //bolIsVisible/ intVisiblePosition \r\n            if (VTintVisiblePosition.Count > 0) { for (int j = 0; j < VTbolIsVisible.Count; j++) dicVisiblePosition.Add(VTbolIsVisible[j], VTintVisiblePosition[j]); } \r\n \r\n           for (int i = 0; i < strColumnName.Count; i++)\r\n            {\r\n                bool temp = VTbolIsVisible.Contains(i); \r\n bolIsVisible.Add(temp);\r\n                bolIsVisibleCbb.Add(VTbolIsVisibleCbb.Contains(i));\r\n                bolIsReadOnly.Add(VTbolIsReadOnly.Contains(i));\r\n                intColumnWidth.Add(-1);\r\n                intColumnMaxWidth.Add(-1);\r\n                intColumnMinWidth.Add(-1);\r\n                intVisiblePosition.Add(dicVisiblePosition.Count > 0 ? (temp ? dicVisiblePosition[i] : strColumnName.Count - 1) : -1);\r\n            }\r\n            dicTemp.Add(nameTable, CreateData(strColumnName, strColumnCaption, strColumnToolTip, bolIsReadOnly, bolIsVisible, bolIsVisibleCbb, intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition));\r\n            #endregion";
            const string patternStr = "#region {0}\r\n            nameTable = {0}_TableName;\r\n            strColumnName = new List<string> { {1} };\r\n //{2} \r\n            strColumnCaption = strColumnToolTip = new List<string> { {3} };\r\n            bolIsReadOnly = new List<bool>();\r\n            bolIsVisible = new List<bool>();\r\n            bolIsVisibleCbb = new List<bool>();\r\n            intColumnWidth = new List<int>();\r\n            intColumnMaxWidth = new List<int>();\r\n            intColumnMinWidth = new List<int>();\r\n            intVisiblePosition = new List<int>();\r\n            VTbolIsVisible = new List<int> { {4} };    //vị trí có giá trị bằng true\r\n            VTbolIsVisibleCbb = new List<int> { 1, 2 };\r\n            VTintVisiblePosition = new List<int>() { {5} };\r\n            VTbolIsReadOnly = new List<int>() { {6} };\r\n \r\n            dicVisiblePosition = new Dictionary<int, int>();   //bolIsVisible/ intVisiblePosition \r\n            if (VTintVisiblePosition.Count > 0) { for (int j = 0; j < VTbolIsVisible.Count; j++) dicVisiblePosition.Add(VTbolIsVisible[j], VTintVisiblePosition[j]); } \r\n \r\n           for (int i = 0; i < strColumnName.Count; i++)\r\n            {\r\n                bool temp = VTbolIsVisible.Contains(i); \r\n bolIsVisible.Add(temp);\r\n                bolIsVisibleCbb.Add(VTbolIsVisibleCbb.Contains(i));\r\n                bolIsReadOnly.Add(VTbolIsReadOnly.Contains(i));\r\n                intColumnWidth.Add(-1);\r\n                intColumnMaxWidth.Add(-1);\r\n                intColumnMinWidth.Add(-1);\r\n                intVisiblePosition.Add(dicVisiblePosition.Count > 0 ? (temp ? dicVisiblePosition[i] : strColumnName.Count - 1) : -1);\r\n            }\r\n            dicTemp.Add(nameTable, CreateData(strColumnName, strColumnCaption, strColumnToolTip, bolIsReadOnly, bolIsVisible, bolIsVisibleCbb, intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition));\r\n            #endregion";
            string strColumnName = string.Empty;
            string strColumnNameWithIndex;
            string countItem;
            if (Core.Utils.getType(string.Format("Accounting.Core.Domain.{0}", nameTable)) == null) return "";
            GetStrProObj(Activator.CreateInstance(Core.Utils.getType(string.Format("Accounting.Core.Domain.{0}", nameTable))), out strColumnName, out countItem, out strColumnNameWithIndex);
            strColumnCaption = string.IsNullOrEmpty(strColumnCaption) ? strColumnName : strColumnCaption;
            //return string.Format(patternStr, nameTable, strColumnName, strColumnNameWithIndex, strColumnCaption, VTbolIsVisible, VTintVisiblePosition, VTbolIsReadOnly);
            return patternStr.Replace("{0}", nameTable).Replace("{1}", strColumnName).Replace("{2}", strColumnNameWithIndex).Replace("{3}", strColumnCaption).Replace("{4}", VTbolIsVisible).Replace("{5}", VTintVisiblePosition).Replace("{6}", VTbolIsReadOnly);
        }

        static void GetStrProObj(object obj, out string strColumnName, out string countItem, out string strColumnNameWithIndex)
        {
            PropertyInfo[] dsPropertyInfo = obj.GetType().GetProperties();
            strColumnName = strColumnNameWithIndex = string.Empty;
            for (int i = 0; i < dsPropertyInfo.Length; i++)
            {
                strColumnName += string.Format("\"{0}\", ", dsPropertyInfo[i].Name);
                strColumnNameWithIndex += string.Format("\"{1}-{0}\", ", dsPropertyInfo[i].Name, i);
            }
            //strColumnName = dsPropertyInfo.Aggregate(string.Empty, (current, propertyInfo) => current + string.Format("\"{0}\", ", propertyInfo.Name));
            countItem = dsPropertyInfo.Count().ToString();
        }

        private void btnUp_Click(object sender, EventArgs e)
        {
            FTemplateDetail.ChangeIndexTemplateColum(ultraGrid1.ActiveRow.Index, ultraGrid1.ActiveRow.Index - (((UltraButton)sender).Name.Equals("btnUp") ? 1 : -1), (BindingList<TemplateColumn>)ultraGrid1.DataSource, ultraGrid1);
        }

        Dictionary<string, int> dicIndexOfPropertyInfo = new Dictionary<string, int>();
        BindingList<TemplateColumn> lstTemplateColumn = new BindingList<TemplateColumn>();
        private void ultraButton8_Click(object sender, EventArgs e)
        {
            //1. bấm cái này đầu tiên
            object obj = Activator.CreateInstance(Core.Utils.getType(string.Format("Accounting.Core.Domain.{0}", txtNameObj.Text)));

            //tạo dicIndexOfPropertyInfo
            PropertyInfo[] dsPropertyInfo = obj.GetType().GetProperties();
            dicIndexOfPropertyInfo.Clear();
            for (int ii = 0; ii < dsPropertyInfo.Length; ii++) dicIndexOfPropertyInfo.Add(dsPropertyInfo[ii].Name, ii);

            string s = obj.GetType().GetProperties().Aggregate("", (current, a) => current + (a.Name + ","));
            List<string> lst = s.Split(',').Where(k => !string.IsNullOrEmpty(k)).ToList();
            int i = 0;
            lstTemplateColumn =
                new BindingList<TemplateColumn>(
                    lst.Select(
                        s1 =>
                        new TemplateColumn
                            {
                                IsVisible = false,
                                ColumnName = s1,
                                VisiblePosition = i++,
                                ColumnCaption = s1,
                                ColumnToolTip = s1
                            }).ToList());
            ultraGrid1.DataSource = lstTemplateColumn;
            Utils.ConfigGrid(ultraGrid1, string.Empty, genTemplateColum(), isConfigGuiManually: true);
            ultraGrid1.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            ultraGrid1.DisplayLayout.ViewStyleBand = ViewStyleBand.Horizontal;
            //hiển thị 1 band
            ultraGrid1.DisplayLayout.ViewStyle = ViewStyle.SingleBand;
            //tắt lọc cột
            ultraGrid1.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.True;
            ultraGrid1.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
        }

        private void ultraButton9_Click(object sender, EventArgs e)
        {
            ultraGrid1.UpdateData();
            //2. bấm cái này thứ 2 (lấy các chuỗi config)
            object obj = Activator.CreateInstance(Core.Utils.getType(string.Format("Accounting.Core.Domain.{0}", txtNameObj.Text)));

            //vị trí visible
            List<string> vitriVisible = new List<string>();
            List<string> vitriVisiblePosition = new List<string>();
            List<string> vitriReadOnly = new List<string>();
            List<string> strColumnCaption = new List<string>();
            foreach (TemplateColumn templateColumn in lstTemplateColumn)
            {
                if (templateColumn.IsVisible && dicIndexOfPropertyInfo.ContainsKey(templateColumn.ColumnName))
                {
                    vitriVisible.Add(dicIndexOfPropertyInfo[templateColumn.ColumnName].ToString());
                    vitriVisiblePosition.Add(templateColumn.VisiblePosition.ToString());
                }
                if (templateColumn.IsReadOnly) vitriReadOnly.Add(dicIndexOfPropertyInfo[templateColumn.ColumnName].ToString());
                strColumnCaption.Add(string.Format("\"{0}\"", templateColumn.ColumnCaption));
            }
            txtVTbolIsVisible.Text = string.Join(",", vitriVisible.ToArray());
            txtVTintVisiblePosition.Text = string.Join(",", vitriVisiblePosition.ToArray());
            txtVTbolIsReadOnly.Text = string.Join(",", vitriReadOnly.ToArray());
            txtstrColumnCaption.Text = string.Join(",", strColumnCaption.ToArray());
        }

        private void ultraButton7_Click_1(object sender, EventArgs e)
        {
            Clipboard.SetText(txtKq.Text);
            txtKq.SelectAll();
        }

        List<TemplateColumn> genTemplateColum()
        {
            Dictionary<string, Dictionary<string, TemplateColumn>> dicTemp = new Dictionary<string, Dictionary<string, TemplateColumn>>();
            Dictionary<int, int> dicVisiblePosition;
            List<string> strColumnName, strColumnCaption, strColumnToolTip;
            List<bool> bolIsReadOnly, bolIsVisible, bolIsVisibleCbb;
            List<int> intColumnWidth,
                      intColumnMaxWidth,
                      intColumnMinWidth,
                      intVisiblePosition,
                      VTbolIsVisible,
                      VTbolIsVisibleCbb,
                      VTintVisiblePosition,
                      VTbolIsReadOnly;
            #region TemplateColumn
            strColumnName = new List<string> { "ID", "TemplateDetailID", "ColumnCaption", "IsReadOnly", "ColumnWidth", "ColumnMaxWidth", "ColumnMinWidth", "IsVisible", "VisiblePosition", "ColumnToolTip", "ColumnName", "IsColumnHeader", "IsVisibleCbb", };
            //"0-ID", "1-TemplateDetailID", "2-ColumnCaption", "3-IsReadOnly", "4-ColumnWidth", "5-ColumnMaxWidth", "6-ColumnMinWidth", "7-IsVisible", "8-VisiblePosition", "9-ColumnToolTip", "10-ColumnName", "11-IsColumnHeader", "12-IsVisibleCbb",  
            strColumnCaption = strColumnToolTip = new List<string> { "ID", "TemplateDetailID", "ColumnCaption", "IsReadOnly", "ColumnWidth", "ColumnMaxWidth", "ColumnMinWidth", "IsVisible", "VisiblePosition", "ColumnToolTip", "ColumnName", "IsColumnHeader", "IsVisibleCbb", };
            bolIsReadOnly = new List<bool>();
            bolIsVisible = new List<bool>();
            bolIsVisibleCbb = new List<bool>();
            intColumnWidth = new List<int>();
            intColumnMaxWidth = new List<int>();
            intColumnMinWidth = new List<int>();
            intVisiblePosition = new List<int>();
            VTbolIsVisible = new List<int> { 10, 2, 7, 8, 3 };    //vị trí có giá trị bằng true
            VTbolIsVisibleCbb = new List<int> { 1, 2 };
            VTintVisiblePosition = new List<int>() { 0, 1, 2, 3, 4 };
            VTbolIsReadOnly = new List<int>() { };

            dicVisiblePosition = new Dictionary<int, int>();   //bolIsVisible/ intVisiblePosition 
            if (VTintVisiblePosition.Count > 0) { for (int j = 0; j < VTbolIsVisible.Count; j++) dicVisiblePosition.Add(VTbolIsVisible[j], VTintVisiblePosition[j]); }

            for (int i = 0; i < strColumnName.Count; i++)
            {
                bool temp = VTbolIsVisible.Contains(i);
                bolIsVisible.Add(temp);
                bolIsVisibleCbb.Add(VTbolIsVisibleCbb.Contains(i));
                bolIsReadOnly.Add(VTbolIsReadOnly.Contains(i));
                intColumnWidth.Add(-1);
                intColumnMaxWidth.Add(-1);
                intColumnMinWidth.Add(-1);
                intVisiblePosition.Add(dicVisiblePosition.Count > 0 ? (temp ? dicVisiblePosition[i] : strColumnName.Count - 1) : -1);
            }
            return ConstDatabase.CreateData(strColumnName, strColumnCaption, strColumnToolTip, bolIsReadOnly, bolIsVisible, bolIsVisibleCbb, intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition).Values.ToList();
            #endregion
        }
        #endregion

        private void btnDeleteTemplate_Click(object sender, EventArgs e)
        {
            if (cbbTemplate.SelectedRow == null) return;
            var model = cbbTemplate.SelectedRow.ListObject as Template;
            try
            {
                ItemplateService.BeginTran();
                ItemplateService.Delete(model);
                ItemplateService.CommitTran();
                MSG.MessageBoxStand("Xóa Template thành công");
                btnGetData.PerformClick();
                cbbTemplate.SelectedRow = cbbTemplate.Rows.FirstOrDefault();
            }
            catch (Exception)
            {
                ItemplateService.RolbackTran();
                MSG.Warning("Xóa Template không thành công");
            }
        }
    }
    public class TemplateColumnsWrong
    {
        public virtual string ObjectName { get; set; }
        public virtual string ColumnWrong { get; set; }
    }
}
