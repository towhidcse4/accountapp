﻿using System.Collections.Generic;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;

namespace Accounting
{
    public partial class FTest1 : Form
    {
        private readonly IMCReceiptService _IMCReceiptService;

        public FTest1()
        {
            InitializeComponent();
            _IMCReceiptService = IoC.Resolve<IMCReceiptService>();
            List<MCReceipt> list = _IMCReceiptService.GetAll();
            if (list.Count == 0) list.Add(new MCReceipt());
            uGrid.DataSource = list.ToArray();

            this.uGrid.DisplayLayout.Override.ColumnSizingArea = Infragistics.Win.UltraWinGrid.ColumnSizingArea.EntireColumn;
            this.uGrid.DisplayLayout.UseFixedHeaders = true;
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGrid.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.Default;
            this.uGrid.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;

            this.uGrid.DisplayLayout.Override.RowSelectorStyle = Infragistics.Win.HeaderStyle.WindowsVista;

            //this.uGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            //this.uGrid.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;

            this.uGrid.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            this.uGrid.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.ExtendFirstCell;

            this.uGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;

            ConfigGrid(uGrid, TextMessage.ConstDatabase.MCReceipt_TableName, new List<TemplateColumn>());

            Infragistics.Win.UltraWinGrid.UltraGridBand band = uGrid.DisplayLayout.Bands[0];
            band.Summaries.Clear();
            band.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.Default;
        }

        public static void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid, string nameTable, List<TemplateColumn> configGUI)
        {
            //Convert configGUI to dic
            Dictionary<string, TemplateColumn> dicconfigGUI = new Dictionary<string, TemplateColumn>();
            if (configGUI.Count > 0) foreach (var item in configGUI) dicconfigGUI.Add(item.ColumnName, item);

            Infragistics.Shared.ResourceCustomizer rc = Infragistics.Win.UltraWinGrid.Resources.Customizer;
            rc.SetCustomizedString("RowFilterDropDownCustomItem", "(Tùy chọn)");
            rc.SetCustomizedString("RowFilterDropDownBlanksItem", "(Trống)");
            rc.SetCustomizedString("RowFilterDropDownNonBlanksItem", "(Không trống)");
            rc.SetCustomizedString("RowFilterDropDownEquals", "Bằng");
            rc.SetCustomizedString("RowFilterDropDownNotEquals", "Khác");
            rc.SetCustomizedString("RowFilterDropDownLessThan", "Nhỏ hơn");
            rc.SetCustomizedString("RowFilterDropDownLessThanOrEqualTo", "Nhỏ hơn hoặc bằng");
            rc.SetCustomizedString("RowFilterDropDownGreaterThan", "Lớn hơn");
            rc.SetCustomizedString("RowFilterDropDownGreaterThanOrEqualTo", "Lớn hơn hoặc bằng");
            rc.SetCustomizedString("RowFilterDropDownMatch", "Thỏa mãn biểu thức");
            rc.SetCustomizedString("RowFilterDropDownLike", "Giống");
            rc.SetCustomizedString("RowFilterDropDown_Operator_StartsWith", "Bắt đầu với");
            rc.SetCustomizedString("RowFilterDropDown_Operator_Contains", "Chứa");
            rc.SetCustomizedString("RowFilterDropDown_Operator_EndsWith", "Kết thúc với");
            rc.SetCustomizedString("RowFilterDropDown_Operator_DoesNotStartWith", "Không bắt đầu với");
            rc.SetCustomizedString("RowFilterDropDown_Operator_DoesNotContain", "Không chứa");
            rc.SetCustomizedString("RowFilterDropDown_Operator_DoesNotEndWith", "Không kết thúc với");
            rc.SetCustomizedString("RowFilterDropDown_Operator_DoesNotMatch", "Không chứa(chuỗi)");
            rc.SetCustomizedString("RowFilterDropDown_Operator_NotLike", "Không giống");

            //Tiêu đề
            utralGrid.Text = nameTable;

            Infragistics.Win.UltraWinGrid.UltraGridBand band = utralGrid.DisplayLayout.Bands[0];
            band.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            band.Override.SelectedRowAppearance.BackColor = System.Drawing.Color.FromArgb(255, 192, 128);

            //Tiêu đề cột
            string Header_Caption_first = string.Empty;
            foreach (var item in band.Columns)
            {
                if (Accounting.TextMessage.ConstDatabase.TablesInDatabase[nameTable].ContainsKey(item.Key))
                {
                    //nếu có các thiết lập từ CSDL thì lấy các thiết lập đó, còn ngược lại thì ko? (trường nào cần thiết lập thì thiết lập trường nào ko cần thiết lập thì thôi)
                    TemplateColumn temp = dicconfigGUI.ContainsKey(item.Key) ? dicconfigGUI[item.Key] : Accounting.TextMessage.ConstDatabase.TablesInDatabase[nameTable][item.Key];
                    if (string.IsNullOrEmpty(Header_Caption_first) && temp.IsVisible == true) Header_Caption_first = item.Key;   //Lấy dòng Header Caption của cột đầu tiên trong các cột hiển thị
                    item.Header.Caption = temp.ColumnCaption;
                    item.Header.ToolTipText = temp.ColumnToolTip;
                    item.Header.Enabled = !temp.IsReadOnly;
                    item.Width = temp.ColumnWidth == -1 ? item.Width : temp.ColumnWidth;
                    item.MaxWidth = temp.ColumnMaxWidth == -1 ? item.MaxWidth : temp.ColumnMaxWidth;
                    item.MinWidth = temp.ColumnMinWidth == -1 ? item.MinWidth : temp.ColumnMinWidth;
                    item.Hidden = !temp.IsVisible;
                    item.Header.VisiblePosition = temp.VisiblePosition == -1 ? item.Header.VisiblePosition : temp.VisiblePosition;
                }
            }

            if (!string.IsNullOrEmpty(Header_Caption_first))
            {
                //Tính tổng hàng
                utralGrid.DisplayLayout.Override.AllowRowSummaries = Infragistics.Win.UltraWinGrid.AllowRowSummaries.Default; //Ẩn ký hiệu tổng trên Header Caption
                if (band.Summaries.Count != 0) band.Summaries.Clear();
                Infragistics.Win.UltraWinGrid.SummarySettings summary = band.Summaries.Add("Count", Infragistics.Win.UltraWinGrid.SummaryType.Count, band.Columns[Header_Caption_first]);
                summary.DisplayFormat = "Number of Rows: {0:N0}";
                summary.SummaryPosition = Infragistics.Win.UltraWinGrid.SummaryPosition.Left;
                utralGrid.DisplayLayout.Override.SummaryFooterCaptionVisible = Infragistics.Win.DefaultableBoolean.False;   //ẩn 

                ////utralGrid.DisplayLayout.Override.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
                ////utralGrid.DisplayLayout.Override.SummaryDisplayArea |= SummaryDisplayAreas.GroupByRowsFooter;
                ////utralGrid.DisplayLayout.Override.SummaryDisplayArea |= SummaryDisplayAreas.InGroupByRows;

                summary.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.BottomFixed | Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.GroupByRowsFooter;
                utralGrid.DisplayLayout.Override.GroupBySummaryDisplayStyle = Infragistics.Win.UltraWinGrid.GroupBySummaryDisplayStyle.SummaryCells;
                utralGrid.DisplayLayout.Bands[0].SummaryFooterCaption = "Số dòng dữ liệu: ";
                utralGrid.DisplayLayout.Override.SummaryFooterAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
            }
        }

        private void FTest1_Load(object sender, System.EventArgs e)
        {

        }
    }
}
