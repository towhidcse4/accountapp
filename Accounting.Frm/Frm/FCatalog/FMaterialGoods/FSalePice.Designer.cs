﻿namespace Accounting
{
    partial class FSalePice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtSalePriceAfterTax3 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtSalePriceAfterTax2 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtSalePriceAfterTax = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtSalePrice = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.chkActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.txtFixedSalePrice = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtSalePrice3 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtSalePrice2 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.lblBankCode = new Infragistics.Win.Misc.UltraLabel();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkActive)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox1.Controls.Add(this.txtSalePriceAfterTax3);
            this.ultraGroupBox1.Controls.Add(this.txtSalePriceAfterTax2);
            this.ultraGroupBox1.Controls.Add(this.txtSalePriceAfterTax);
            this.ultraGroupBox1.Controls.Add(this.txtSalePrice);
            this.ultraGroupBox1.Controls.Add(this.chkActive);
            this.ultraGroupBox1.Controls.Add(this.txtFixedSalePrice);
            this.ultraGroupBox1.Controls.Add(this.txtSalePrice3);
            this.ultraGroupBox1.Controls.Add(this.txtSalePrice2);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel9);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel10);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel11);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel3);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox1.Controls.Add(this.lblBankCode);
            this.ultraGroupBox1.Controls.Add(this.btnClose);
            this.ultraGroupBox1.Controls.Add(this.btnSave);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(471, 165);
            this.ultraGroupBox1.TabIndex = 27;
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtSalePriceAfterTax3
            // 
            appearance1.TextHAlignAsString = "Right";
            this.txtSalePriceAfterTax3.Appearance = appearance1;
            this.txtSalePriceAfterTax3.AutoSize = false;
            this.txtSalePriceAfterTax3.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtSalePriceAfterTax3.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.txtSalePriceAfterTax3.InputMask = "{double:10.2}";
            this.txtSalePriceAfterTax3.Location = new System.Drawing.Point(305, 63);
            this.txtSalePriceAfterTax3.Name = "txtSalePriceAfterTax3";
            this.txtSalePriceAfterTax3.PromptChar = ' ';
            this.txtSalePriceAfterTax3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtSalePriceAfterTax3.Size = new System.Drawing.Size(139, 22);
            this.txtSalePriceAfterTax3.TabIndex = 100;
            this.txtSalePriceAfterTax3.Text = "000";
            this.txtSalePriceAfterTax3.ValueChanged += new System.EventHandler(this.txtSalePriceAfterTax3_ValueChanged);
            // 
            // txtSalePriceAfterTax2
            // 
            appearance2.TextHAlignAsString = "Right";
            this.txtSalePriceAfterTax2.Appearance = appearance2;
            this.txtSalePriceAfterTax2.AutoSize = false;
            this.txtSalePriceAfterTax2.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtSalePriceAfterTax2.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.txtSalePriceAfterTax2.InputMask = "{double:10.2}";
            this.txtSalePriceAfterTax2.Location = new System.Drawing.Point(305, 40);
            this.txtSalePriceAfterTax2.Name = "txtSalePriceAfterTax2";
            this.txtSalePriceAfterTax2.PromptChar = ' ';
            this.txtSalePriceAfterTax2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtSalePriceAfterTax2.Size = new System.Drawing.Size(139, 22);
            this.txtSalePriceAfterTax2.TabIndex = 99;
            this.txtSalePriceAfterTax2.Text = "000";
            this.txtSalePriceAfterTax2.ValueChanged += new System.EventHandler(this.txtSalePriceAfterTax2_ValueChanged);
            // 
            // txtSalePriceAfterTax
            // 
            appearance3.TextHAlignAsString = "Right";
            this.txtSalePriceAfterTax.Appearance = appearance3;
            this.txtSalePriceAfterTax.AutoSize = false;
            this.txtSalePriceAfterTax.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtSalePriceAfterTax.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.txtSalePriceAfterTax.InputMask = "{double:10.2}";
            this.txtSalePriceAfterTax.Location = new System.Drawing.Point(305, 16);
            this.txtSalePriceAfterTax.Name = "txtSalePriceAfterTax";
            this.txtSalePriceAfterTax.PromptChar = ' ';
            this.txtSalePriceAfterTax.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtSalePriceAfterTax.Size = new System.Drawing.Size(139, 22);
            this.txtSalePriceAfterTax.TabIndex = 98;
            this.txtSalePriceAfterTax.Text = "000";
            this.txtSalePriceAfterTax.ValueChanged += new System.EventHandler(this.txtSalePriceAfterTax_ValueChanged);
            // 
            // txtSalePrice
            // 
            appearance4.TextHAlignAsString = "Right";
            this.txtSalePrice.Appearance = appearance4;
            this.txtSalePrice.AutoSize = false;
            this.txtSalePrice.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtSalePrice.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.txtSalePrice.InputMask = "{double:10.2}";
            this.txtSalePrice.Location = new System.Drawing.Point(72, 16);
            this.txtSalePrice.Name = "txtSalePrice";
            this.txtSalePrice.PromptChar = ' ';
            this.txtSalePrice.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtSalePrice.Size = new System.Drawing.Size(139, 22);
            this.txtSalePrice.TabIndex = 95;
            this.txtSalePrice.Text = "000";
            // 
            // chkActive
            // 
            appearance5.TextVAlignAsString = "Middle";
            this.chkActive.Appearance = appearance5;
            this.chkActive.BackColor = System.Drawing.Color.Transparent;
            this.chkActive.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkActive.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkActive.Location = new System.Drawing.Point(6, 127);
            this.chkActive.Name = "chkActive";
            this.chkActive.Size = new System.Drawing.Size(191, 22);
            this.chkActive.TabIndex = 92;
            this.chkActive.Text = "Lấy đơn giá bán sau thuế";
            this.chkActive.CheckedChanged += new System.EventHandler(this.chkActive_CheckedChanged);
            this.chkActive.CheckedValueChanged += new System.EventHandler(this.chkActive_CheckedValueChanged);
            this.chkActive.CheckStateChanged += new System.EventHandler(this.chkActive_CheckStateChanged);
            // 
            // txtFixedSalePrice
            // 
            appearance6.TextHAlignAsString = "Right";
            this.txtFixedSalePrice.Appearance = appearance6;
            this.txtFixedSalePrice.AutoSize = false;
            this.txtFixedSalePrice.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtFixedSalePrice.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.txtFixedSalePrice.InputMask = "{double:10.2}";
            this.txtFixedSalePrice.Location = new System.Drawing.Point(72, 86);
            this.txtFixedSalePrice.Name = "txtFixedSalePrice";
            this.txtFixedSalePrice.PromptChar = ' ';
            this.txtFixedSalePrice.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtFixedSalePrice.Size = new System.Drawing.Size(139, 22);
            this.txtFixedSalePrice.TabIndex = 88;
            this.txtFixedSalePrice.Text = "000";
            // 
            // txtSalePrice3
            // 
            appearance7.TextHAlignAsString = "Right";
            this.txtSalePrice3.Appearance = appearance7;
            this.txtSalePrice3.AutoSize = false;
            this.txtSalePrice3.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtSalePrice3.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.txtSalePrice3.InputMask = "{double:10.2}";
            this.txtSalePrice3.Location = new System.Drawing.Point(72, 63);
            this.txtSalePrice3.Name = "txtSalePrice3";
            this.txtSalePrice3.PromptChar = ' ';
            this.txtSalePrice3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtSalePrice3.Size = new System.Drawing.Size(139, 22);
            this.txtSalePrice3.TabIndex = 87;
            this.txtSalePrice3.Text = "000";
            // 
            // txtSalePrice2
            // 
            appearance8.TextHAlignAsString = "Right";
            this.txtSalePrice2.Appearance = appearance8;
            this.txtSalePrice2.AutoSize = false;
            this.txtSalePrice2.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtSalePrice2.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.txtSalePrice2.InputMask = "{double:10.2}";
            this.txtSalePrice2.Location = new System.Drawing.Point(72, 40);
            this.txtSalePrice2.Name = "txtSalePrice2";
            this.txtSalePrice2.PromptChar = ' ';
            this.txtSalePrice2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtSalePrice2.Size = new System.Drawing.Size(139, 22);
            this.txtSalePrice2.TabIndex = 86;
            this.txtSalePrice2.Text = "000";
            // 
            // ultraLabel9
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.TextVAlignAsString = "Middle";
            this.ultraLabel9.Appearance = appearance9;
            this.ultraLabel9.Location = new System.Drawing.Point(222, 63);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(77, 22);
            this.ultraLabel9.TabIndex = 77;
            this.ultraLabel9.Text = "ĐG sau thuế 3";
            // 
            // ultraLabel10
            // 
            appearance10.BackColor = System.Drawing.Color.Transparent;
            appearance10.TextVAlignAsString = "Middle";
            this.ultraLabel10.Appearance = appearance10;
            this.ultraLabel10.Location = new System.Drawing.Point(221, 40);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(77, 22);
            this.ultraLabel10.TabIndex = 76;
            this.ultraLabel10.Text = "ĐG sau thuế 2";
            // 
            // ultraLabel11
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            appearance11.TextVAlignAsString = "Middle";
            this.ultraLabel11.Appearance = appearance11;
            this.ultraLabel11.Location = new System.Drawing.Point(222, 16);
            this.ultraLabel11.Name = "ultraLabel11";
            this.ultraLabel11.Size = new System.Drawing.Size(77, 22);
            this.ultraLabel11.TabIndex = 75;
            this.ultraLabel11.Text = "ĐG sau thuế 1";
            // 
            // ultraLabel3
            // 
            appearance12.BackColor = System.Drawing.Color.Transparent;
            appearance12.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance12;
            this.ultraLabel3.Location = new System.Drawing.Point(6, 85);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(61, 22);
            this.ultraLabel3.TabIndex = 70;
            this.ultraLabel3.Text = "ĐG cố định";
            // 
            // ultraLabel2
            // 
            appearance13.BackColor = System.Drawing.Color.Transparent;
            appearance13.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance13;
            this.ultraLabel2.Location = new System.Drawing.Point(6, 62);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(52, 22);
            this.ultraLabel2.TabIndex = 69;
            this.ultraLabel2.Text = "ĐG bán 3";
            // 
            // ultraLabel1
            // 
            appearance14.BackColor = System.Drawing.Color.Transparent;
            appearance14.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance14;
            this.ultraLabel1.Location = new System.Drawing.Point(6, 40);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(52, 22);
            this.ultraLabel1.TabIndex = 68;
            this.ultraLabel1.Text = "ĐG bán 2";
            // 
            // lblBankCode
            // 
            appearance15.BackColor = System.Drawing.Color.Transparent;
            appearance15.TextVAlignAsString = "Middle";
            this.lblBankCode.Appearance = appearance15;
            this.lblBankCode.Location = new System.Drawing.Point(6, 17);
            this.lblBankCode.Name = "lblBankCode";
            this.lblBankCode.Size = new System.Drawing.Size(52, 22);
            this.lblBankCode.TabIndex = 67;
            this.lblBankCode.Text = "ĐG bán 1";
            // 
            // btnClose
            // 
            appearance16.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance16;
            this.btnClose.Location = new System.Drawing.Point(372, 123);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 66;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            appearance17.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance17;
            this.btnSave.Location = new System.Drawing.Point(291, 123);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 65;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // FSalePice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(471, 165);
            this.Controls.Add(this.ultraGroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FSalePice";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Nhập đơn giá";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FSalePice_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkActive)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel lblBankCode;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtFixedSalePrice;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtSalePrice3;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtSalePrice2;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkActive;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtSalePrice;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtSalePriceAfterTax3;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtSalePriceAfterTax2;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtSalePriceAfterTax;
    }
}