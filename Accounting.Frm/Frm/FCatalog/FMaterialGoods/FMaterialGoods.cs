﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using Accounting.TextMessage;
using System.Linq;
using Accounting.Core;

namespace Accounting
{
    public partial class FMaterialGoods : CatalogBase
    {
        #region khai báo
        ////private readonly ISaleDiscountPolicyService _ISaleDiscountPolicyService;
        private readonly IMaterialGoodsService _IMaterialGoodsService;
        private readonly IMaterialGoodsCategoryService _IIMaterialGoodsCategoryService;
        private readonly ISaleDiscountPolicyService _ISaleDiscountPolicyService;
        private readonly IMaterialGoodsAssemblyService _IMaterialGoodsAssemblyService;
        List<VoucherMaterialGoods> listVoucherMaterialGoods = new List<VoucherMaterialGoods>();
        List<MaterialGoods> dsMaterialGoods = new List<MaterialGoods>();
        //add by namnh
        static Dictionary<int, string> DicMaterialGoodsType;
        public static Dictionary<int, string> dicMaterialGoodsType
        {
            get { DicMaterialGoodsType = DicMaterialGoodsType ?? (Dictionary<int, string>)BuildConfig(1); return DicMaterialGoodsType; }
        }

        #endregion
        private Guid selectedGuid;
        private DateTime ngayHoachToan;
        private DateTime dtBegin;
        private DateTime dtEnd;
        private readonly List<Item> _lstItems = Utils.ObjConstValue.SelectTimes;

        protected override void AddFunction()
        {

            new FMaterialGoodsDetail().ShowDialog(this);
            if (!FMaterialGoodsDetail.isClose) { Utils.ClearCacheByType<MaterialGoods>(); Utils.ClearCacheByType<MaterialGoodsCustom>(); LoadDuLieu(); }
        }
        protected override void EditFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                
                MaterialGoods temp = uGrid.Selected.Rows[0].ListObject as MaterialGoods;
                MaterialGoods tempp = _IMaterialGoodsService.Getbykey(temp.ID);
                //Investor Node = _IInvestorService.Getbykey((Guid)temp.ID);
                //ultraLabel1.Text = "Nhà đầu tư\t " + Node.InvestorCode;

                new FMaterialGoodsDetail(tempp).ShowDialog(this);
                if (!FMaterialGoodsDetail.isClose) { Utils.ClearCacheByType<MaterialGoods>(); Utils.ClearCacheByType<MaterialGoodsCustom>(); LoadDuLieu(); }
            }
            else
                MSG.Error(string.Format(resSystem.MSG_Catalog3, "một Vật tư/Hàng hóa"));
        }
        public FMaterialGoods()
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            ToolTip ToolTip1 = new ToolTip();
                ToolTip1.SetToolTip(btnAdd, "Thêm (Ctrl + N)");
            ToolTip ToolTip2 = new ToolTip();
            ToolTip2.SetToolTip(btnEdit, "Xem (Ctrl + E)");
            ToolTip ToolTip3 = new ToolTip();
            ToolTip3.SetToolTip(btnDelete, "Xóa (Ctrl + D)");
            if (uGrid.Rows.Count > 0) uGrid.Rows[0].Selected = true;
            ngayHoachToan = (DateTime)Utils.StringToDateTime(ConstFrm.DbStartDate);
            //dtBegin = ngayHoachToan.AddDays(1 - ngayHoachToan.Day);
            //dtEnd = ngayHoachToan;
            //dteToDate.DateTime = dtEnd;
            //dteFromDate.DateTime = dtBegin;
            cbbAboutTime.DataSource = _lstItems;
            cbbAboutTime.DisplayMember = "Name";
            Utils.ConfigGrid(cbbAboutTime, ConstDatabase.ConfigXML_TableName);
            cbbAboutTime.SelectedRow = cbbAboutTime.Rows[7];
            cbbAboutTime.DisplayLayout.AutoFitStyle = AutoFitStyle.ExtendLastColumn;
            #endregion

            #region Thiết lập ban đầu cho Form
            _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
            _IIMaterialGoodsCategoryService = IoC.Resolve<IMaterialGoodsCategoryService>();
            _ISaleDiscountPolicyService = IoC.Resolve<ISaleDiscountPolicyService>();
            _IMaterialGoodsAssemblyService = IoC.Resolve<IMaterialGoodsAssemblyService>();
            this.uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            //this.uGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ExtendLastColumn;
            #endregion

            #region Tạo dữ liệu ban đầu
            LoadDuLieu();
            if (uGrid.Rows.Count > 0) uGrid.Rows[0].Selected = true;

           
            #endregion
        }
        //add by namnh
        static object BuildConfig(int obj)
        {
            Dictionary<int, string> temp = new Dictionary<int, string>();
            switch (obj)
            {
                case 1:
                    {
                        temp.Add(0, "Vật tư hàng hóa");
                        temp.Add(1, "VTHH lắp ráp, tháo dỡ");
                        temp.Add(2, "Dịch vụ");
                        temp.Add(3, "Thành phẩm");
                        temp.Add(4, "Chỉ là diễn giải");
                        temp.Add(5, "Khác");
                    }
                    break;

            }
            return temp;
        }
        //end namnh
        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }
        private void LoadDuLieu(bool configGrid)
        {
            WaitingFrm.StartWaiting();
            #region Lấy dữ liệu từ CSDL
            //List<MaterialGoods> list1 = _IMaterialGoodsService.GetAll().Where(p => p.MaterialToolType !=1 ).ToList();
            int materialToolType = 1;

            List<MaterialGoodsCustom> list1 = _IMaterialGoodsService.GetMateriaGoodByMaterialToolTypeAndRepositoryLedger(materialToolType);
            _IMaterialGoodsService.UnbindSession(list1);
            list1 = _IMaterialGoodsService.GetMateriaGoodByMaterialToolTypeAndRepositoryLedger(materialToolType);
            //add by namnh
            foreach (var item in list1)
            {
                item.MaterialGoodsTypeView = dicMaterialGoodsType[item.MaterialGoodsType];
            }
            uGrid.DataSource = list1.ToArray();
           
            #endregion
            //foreach (var item in list1)
            //{
            //    MaterialGoodsCategory temp = listMaterialCategory.Where(p => p.ID == item.MaterialGoodsCategoryID).SingleOrDefault();
            //    item.MaterialGoodsCategoryIDVIEW = temp != null ? temp.MaterialGoodsName : string.Empty;
            //}
            if ((uGrid.Selected.Rows.Count == 0) && (list1.Count > 0))
            {
                uGrid.Rows[0].Selected = true;
            }
            if (configGrid) ConfigGrid(uGrid);
            listVoucherMaterialGoods = _IMaterialGoodsService.GetlistVoucherMaterialGoodsView();
            uGrid.DisplayLayout.Bands[0].Columns["SumIWQuantity"].FormatNumberic(ConstDatabase.Format_Quantity);
            Utils.AddSumColumn(uGrid, "SumIWQuantity", false);
            uGrid.DisplayLayout.Bands[0].Columns["SumIWAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGrid, "SumIWAmount", false);
            uGrid.DisplayLayout.Bands[0].Columns["DiffSumIWQuantity"].FormatNumberic(ConstDatabase.Format_Quantity);
            Utils.AddSumColumn(uGrid, "DiffSumIWQuantity", false);
            uGrid.DisplayLayout.Bands[0].Columns["DiffSumIWAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGrid, "DiffSumIWAmount", false);

            uGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            uGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Extended;
            //foreach (var item in listVoucherMaterialGoods)
            //{
            //    item.MaterialGoodsId = dicCostSetType[item.CostSetType];
            //}
            WaitingFrm.StopWaiting();
        }
        private void ChinhSuaDuLieuTruocKhiExport(List<MaterialGoodsCustom> list)
        {
            foreach(var item in list)
            {
                if(item.MaterialGoodsGSTID!=null&&item.MaterialGoodsGSTID!=Guid.Empty)
                {
                    item.NhomHHDVchiuthueTTĐB = Utils.IMaterialGoodsSpecialTaxGroupService.GetMaterialGoodsSpecialTaxGroupCodeByGuid(item.MaterialGoodsGSTID);
                }
                if(item.RepositoryID!=null&&item.RepositoryID!=Guid.Empty)
                {
                    item.KhoNgamDinh = Utils.IRepositoryService.GetRespositoryCodeByGuid(item.RepositoryID);
                }
                if (item.MaterialGoodsCategoryID != null && item.MaterialGoodsCategoryID != Guid.Empty)
                {
                    item.LoaiVTHH = Utils.IMaterialGoodsCategoryService.GetMaterialGoodsCategoryCodeByGuid(item.MaterialGoodsCategoryID);
                }
                
            }
        }
        private void LoadDuLieuGridExport( UltraGrid ultraGrid)
        {
            WaitingFrm.StartWaiting();
            #region Lấy dữ liệu từ CSDL
            //List<MaterialGoods> list1 = _IMaterialGoodsService.GetAll().Where(p => p.MaterialToolType !=1 ).ToList();
            int materialToolType = 1;

            List<MaterialGoodsCustom> list1 = _IMaterialGoodsService.GetMateriaGoodByMaterialToolTypeAndRepositoryLedger(materialToolType);
            _IMaterialGoodsService.UnbindSession(list1);
            list1 = _IMaterialGoodsService.GetMateriaGoodByMaterialToolTypeAndRepositoryLedger(materialToolType);
            //add by namnh
            foreach (var item in list1)
            {
                item.MaterialGoodsTypeView = dicMaterialGoodsType[item.MaterialGoodsType];
            }
            ChinhSuaDuLieuTruocKhiExport(list1);
            ultraGrid.DataSource = list1.ToArray();

            #endregion
            //foreach (var item in list1)
            //{
            //    MaterialGoodsCategory temp = listMaterialCategory.Where(p => p.ID == item.MaterialGoodsCategoryID).SingleOrDefault();
            //    item.MaterialGoodsCategoryIDVIEW = temp != null ? temp.MaterialGoodsName : string.Empty;
            //}
            //if ((ultraGrid.Selected.Rows.Count == 0) && (list1.Count > 0))
            //{
            //    ultraGrid.Rows[0].Selected = true;
            //}
            //if (configGrid) ConfigGrid(ultraGrid);
            listVoucherMaterialGoods = _IMaterialGoodsService.GetlistVoucherMaterialGoodsView();
            ultraGrid.DisplayLayout.Bands[0].Columns["SumIWQuantity"].FormatNumberic(ConstDatabase.Format_Quantity);
            Utils.AddSumColumn(ultraGrid, "SumIWQuantity", false);
            ultraGrid.DisplayLayout.Bands[0].Columns["SumIWAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(ultraGrid, "SumIWAmount", false);
            ultraGrid.DisplayLayout.Bands[0].Columns["DiffSumIWQuantity"].FormatNumberic(ConstDatabase.Format_Quantity);
            Utils.AddSumColumn(ultraGrid, "DiffSumIWQuantity", false);
            ultraGrid.DisplayLayout.Bands[0].Columns["DiffSumIWAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(ultraGrid, "DiffSumIWAmount", false);

            ultraGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            ultraGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Extended;
            //foreach (var item in listVoucherMaterialGoods)
            //{
            //    item.MaterialGoodsId = dicCostSetType[item.CostSetType];
            //}
            WaitingFrm.StopWaiting();
        }

        #region Utils
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(uGrid, TextMessage.ConstDatabase.MaterialGoods_TableName);
        }
        #endregion

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void uGrid_DoubleClick(object sender, EventArgs e)
        {
            EditFunction();
        }

        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        protected override void DeleteFunction()
        {
            bool a = uGrid.Selected.Rows.Count > 0;
            bool b = uGrid.ActiveRow != null;
            if (a || b)
            {
                if (!(DialogResult.Yes == MessageBox.Show("Bạn có muốn xóa những dữ liệu này không ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question)))
                    return;
                SelectedRowsCollection lst = uGrid.Selected.Rows;
                var strResult = new List<Result>();
                foreach (UltraGridRow ultraGridRow in lst)
                {
                    var result = new Result();
                    MaterialGoods temp = (MaterialGoods)ultraGridRow.ListObject;
                    MaterialGoods data = Utils.IMaterialGoodsService.Getbykey(temp.ID);
                    try
                    {                       
                        if (!Utils.checkRelationVoucher(data) && !Utils.IOPMaterialGoodsService.Query.Any(o => o.MaterialGoodsID == data.ID))
                        {

                            Utils.IMaterialGoodsService.BeginTran();
                            Utils.IMaterialGoodsService.Delete(data);
                            Utils.IMaterialGoodsService.CommitTran();
                            Utils.ClearCacheByType<MaterialGoods>();
                            Utils.ClearCacheByType<MaterialGoodsCustom>();
                            result.Reason = "Xóa thành công";
                            result.Code = data.MaterialGoodsCode;
                            result.IsSucceed = true;
                            strResult.Add(result);


                        }
                        else
                        {
                            result.Reason = "Không thể xóa do dữ liệu đã phát sinh chứng từ liên quan";
                            result.Code = data.MaterialGoodsCode;
                            result.IsSucceed = false;
                            strResult.Add(result);
                        }
                    }
                    catch (Exception ex)
                    {
                        Utils.IMaterialGoodsService.RolbackTran();
                        MSG.Error("Xóa chứng từ thất bại");
                    }
                }
                if (lst.Count > 1 && strResult.Count > 0) new MsgResult(strResult, false).ShowDialog(this);
                else MSG.Information(strResult.First().Reason);
                LoadDuLieu();
            }
            
            //if (uGrid.Selected.Rows.Count > 0)
            //{
            //    MaterialGoods temp = uGrid.Selected.Rows[0].ListObject as MaterialGoods;
            //    // Để xóa danh mục CCDC --> CCDC phải chưa ghi tăng, hoặc không phải là khai báo
            //    if (Utils.ITIIncrementDetailService.CheckDeleteTools(temp.ID))
            //    {
            //        MSG.Warning("Không thể xóa CCDC này vì tài sản này đã có phát sinh chứng từ liên quan!");
            //        return;
            //    }
            //    if (MSG.Question(string.Format(resSystem.MSG_System_05, temp.MaterialGoodsCode)) == System.Windows.Forms.DialogResult.Yes)
            //    {
            //        if (!Utils.checkRelationVoucher(temp) && !Utils.IOPMaterialGoodsService.Query.Any(o => o.MaterialGoodsID == temp.ID))
            //        {
            //            _IMaterialGoodsService.BeginTran();
            //            try
            //            {
            //                var item = Utils.ListMaterialGoods.FirstOrDefault(o => o.ID == temp.ID);
            //                _IMaterialGoodsService.BeginTran();
            //                _IMaterialGoodsService.Delete(item);
            //                _IMaterialGoodsService.CommitTran();
            //                Utils.ClearCacheByType<MaterialGoods>();
            //                Utils.ClearCacheByType<MaterialGoodsCustom>();
            //                LoadDuLieu();
            //            }
            //            catch (Exception ex)
            //            {
            //                _IMaterialGoodsService.RolbackTran();
            //                MSG.Warning("Có lỗi xảy ra khi xóa vật tư hàng hóa");
            //            }
            //        }
            //        else
            //        {
            //            MSG.Error("Cần xóa chứng từ kế toán có liên quan trước khi xóa dữ liệu danh mục");
            //        }
            //    }
            //}
            //else
            //    MSG.Error(string.Format(resSystem.MSG_Catalog2, "một Vật tư/Hàng hóa"));
        }

        protected override void ResetFunction()
        {
            LoadDuLieu(false);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void uGrid_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                MaterialGoodsCustom Node = uGrid.Selected.Rows[0].ListObject as MaterialGoodsCustom;
                txtCode.Text = Node.MaterialGoodsCode;
                txtName.Text = Node.MaterialGoodsName;
                txtUnit.Text = Node.Unit;
                txtQuantity.Text = Node.SumIWQuantity.ToStringNumbericFormat(ConstDatabase.Format_Quantity);
                txtAmount.Text = Node.SumIWAmount.ToStringNumbericFormat(ConstDatabase.Format_TienVND);
            }
        }

        private void uGrid_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }

        private void tsmAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void tsmEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            ResetFunction();
        }

        private void uGrid_BeforeRowActivate(object sender, RowEventArgs e)
        {
            if (e.Row == null) return;
            var temp = e.Row.ListObject as MaterialGoodsCustom;
            if (temp == null) return;
            txtCode.Text = temp.MaterialGoodsCode;
            txtName.Text = temp.MaterialGoodsName;
            txtUnit.Text = temp.Unit;
            txtQuantity.Text = temp.SumIWQuantity.ToStringNumbericFormat(ConstDatabase.Format_Quantity);
            txtAmount.Text = temp.SumIWAmount.ToStringNumbericFormat(ConstDatabase.Format_TienVND);
            List<VoucherMaterialGoods> voucher = listVoucherMaterialGoods.Where(p => p.MaterialGoodsId == temp.ID).OrderByDescending(p => p.Date).ToList();
            uGridTax.DataSource = voucher;
            uGridTax.ConfigGrid(ConstDatabase.VoucherMaterialGoods_KeyName);
            uGridTax.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            selectedGuid = temp.ID;
            foreach (var column in uGridTax.DisplayLayout.Bands[0].Columns)
            {
                this.ConfigEachColumn4Grid(0, column, uGridTax);
            }
          
        }

        private void btnViewVoucher_Click(object sender, EventArgs e)
        {
            if (uGridTax.ActiveRow == null) return;
            var voucher = uGridTax.ActiveRow.ListObject as VoucherMaterialGoods;
            if (voucher == null) return;
            Utils.ViewVoucherSelected(voucher.ID, voucher.TypeID);
        }

        private void cbbAboutTime_ValueChanged(object sender, EventArgs e)
        {
            if (cbbAboutTime.SelectedRow != null)
            {
                var model = cbbAboutTime.SelectedRow.ListObject as Item;

                Utils.GetDateBeginDateEnd(ngayHoachToan.Year,ngayHoachToan, model, out dtBegin, out dtEnd);
                dteFromDate.DateTime = dtBegin;
                dteToDate.DateTime = dtEnd;
                List<VoucherMaterialGoods> voucher = listVoucherMaterialGoods.
    Where(p => p.MaterialGoodsId == selectedGuid && p.Date >= dtBegin && p.Date <= dtEnd).OrderByDescending(u => u.Date).ToList();
                foreach (var item in voucher)
                {
                    var firstOrDefault = Utils.ListType.FirstOrDefault(x => x.ID == item.TypeID);
                    if (firstOrDefault != null)
                        item.No = firstOrDefault.TypeName;
                }

                uGridTax.DataSource = voucher;
                uGridTax.ConfigGrid(ConstDatabase.VoucherAccountingObject_KeyName);
                uGridTax.DisplayLayout.Bands[0].Columns["TotalAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            }
        }

        private void uGridTax_DoubleClick(object sender, EventArgs e)
        {
            btnViewVoucher_Click(sender,e);
        }

        private void dteToDate_ValueChanged(object sender, EventArgs e)
        {
            if (dteFromDate.DateTime <= dteToDate.DateTime)
            {
                List<VoucherMaterialGoods> voucher = listVoucherMaterialGoods.
    Where(p => p.MaterialGoodsId == selectedGuid && p.Date >= dteFromDate.DateTime && p.Date <= dteToDate.DateTime).ToList();
                foreach (var item in voucher)
                {
                    var firstOrDefault = Utils.ListType.FirstOrDefault(x => x.ID == item.TypeID);
                    if (firstOrDefault != null)
                        item.No = firstOrDefault.TypeName;
                }

                uGridTax.DataSource = voucher;
                uGridTax.ConfigGrid(ConstDatabase.VoucherMaterialGoods_KeyName);
                uGridTax.DisplayLayout.Bands[0].Columns["TotalAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            }
            else
            {
              //  MSG.Warning("Ngày đến phải lớn hơn ngày bắt đầu !");
            }
        }

        private void dteFromDate_ValueChanged(object sender, EventArgs e)
        {
            if (dteFromDate.DateTime <= dteToDate.DateTime)
            {
                List<VoucherMaterialGoods> voucher = listVoucherMaterialGoods.
    Where(p => p.MaterialGoodsId == selectedGuid && p.Date >= dteFromDate.DateTime && p.Date <= dteToDate.DateTime).OrderByDescending(u => u.Date).ToList();
                foreach (var item in voucher)
                {
                    var firstOrDefault = Utils.ListType.FirstOrDefault(x => x.ID == item.TypeID);
                    if (firstOrDefault != null)
                        item.No = firstOrDefault.TypeName;
                }

                uGridTax.DataSource = voucher;
                uGridTax.ConfigGrid(ConstDatabase.VoucherAccountingObject_KeyName);
                uGridTax.DisplayLayout.Bands[0].Columns["TotalAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            }
            else
            {
               // MSG.Warning("Ngày bắt đầu phải nhỏ hơn ngày đến !");
            }
        }

        private void FMaterialGoods_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGrid.ResetText();
            uGrid.ResetUpdateMode();
            uGrid.ResetExitEditModeOnLeave();
            uGrid.ResetRowUpdateCancelAction();
            uGrid.DataSource = null;
            uGrid.Layouts.Clear();
            uGrid.ResetLayouts();
            uGrid.ResetDisplayLayout();
            uGrid.Refresh();
            uGrid.ClearUndoHistory();
            uGrid.ClearXsdConstraints();
            
            uGridTax.ResetText();
            uGridTax.ResetUpdateMode();
            uGridTax.ResetExitEditModeOnLeave();
            uGridTax.ResetRowUpdateCancelAction();
            uGridTax.DataSource = null;
            uGridTax.Layouts.Clear();
            uGridTax.ResetLayouts();
            uGridTax.ResetDisplayLayout();
            uGridTax.Refresh();
            uGridTax.ClearUndoHistory();
            uGridTax.ClearXsdConstraints();
        }

        private void FMaterialGoods_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            UltraGrid ultraGridExport=uGridExport;
            ultraGridExport.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            LoadDuLieuGridExport(ultraGridExport);
            Utils.ConfigGrid(ultraGridExport, TextMessage.ConstDatabase.MaterialGoods_TableNameExportExcel);
            Utils.ExportExcel(ultraGridExport, "Export_DMVatTuHangHoa");
        }
    }
}
