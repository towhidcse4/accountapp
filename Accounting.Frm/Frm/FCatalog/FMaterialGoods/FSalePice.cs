﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using System.Drawing;

namespace Accounting
{
    public partial class FSalePice : DialogForm
    {
        private readonly IMaterialGoodsService _IMaterialGoodsService;
        public static decimal salePrice;
        public static decimal salePrice3;
        public static decimal salePrice2;
        public static decimal fixedSalePrice;
        public static decimal salePriceAfterTax;
        public static decimal salePriceAfterTax2;
        public static decimal salePriceAfterTax3;
        public static bool CheckActive;
        public static List<decimal> aa = new List<decimal>();
        public static bool isClose = true;
        private decimal taxRateInput = 0;
        private MaterialGoods _outputMaterialGoods;

        public FSalePice(decimal taxRate, MaterialGoods materialGoods)
        {
            InitializeComponent();
            _outputMaterialGoods = materialGoods;

            txtSalePriceAfterTax.Enabled = false;
            txtSalePriceAfterTax2.Enabled = false;
            txtSalePriceAfterTax3.Enabled = false;
            if (chkActive.Checked == false)
            {
                txtSalePrice.Text = txtSalePriceAfterTax.Text;
            }
            txtSalePrice.FormatNumberic(ConstDatabase.Format_DonGiaQuyDoi);
            txtSalePrice2.FormatNumberic(ConstDatabase.Format_DonGiaQuyDoi);
            txtSalePrice3.FormatNumberic(ConstDatabase.Format_DonGiaQuyDoi);
            txtSalePriceAfterTax.FormatNumberic(ConstDatabase.Format_DonGiaQuyDoi);
            txtSalePriceAfterTax2.FormatNumberic(ConstDatabase.Format_DonGiaQuyDoi);
            txtSalePriceAfterTax3.FormatNumberic(ConstDatabase.Format_DonGiaQuyDoi);
            txtFixedSalePrice.FormatNumberic(ConstDatabase.Format_DonGiaQuyDoi);
            taxRateInput = taxRate;
            
        }

        public FSalePice(decimal a, decimal taxRate, MaterialGoods materialGoods)
        {
            InitializeComponent();
            _outputMaterialGoods = materialGoods;

            chkActive.Checked = CheckActive;


            if (chkActive.Checked == true)
            {
                txtSalePriceAfterTax.Enabled = true;
                txtSalePriceAfterTax2.Enabled = true;
                txtSalePriceAfterTax3.Enabled = true;
                txtSalePrice2.Enabled = false;
                txtSalePrice3.Enabled = false;
                txtSalePrice.Enabled = false;
            }
            else
            {
                txtSalePriceAfterTax.Enabled = false;
                txtSalePriceAfterTax2.Enabled = false;
                txtSalePriceAfterTax3.Enabled = false;
                txtSalePrice2.Enabled = true;
                txtSalePrice3.Enabled = true;
                txtSalePrice.Enabled = true;
            }
            txtSalePrice.Text = materialGoods.SalePrice.ToString();
            txtSalePrice2.Text = materialGoods.SalePrice2.ToString();
            txtSalePrice3.Text = materialGoods.SalePrice3.ToString();
            txtFixedSalePrice.Text = materialGoods.FixedSalePrice.ToString();
            txtSalePriceAfterTax.Text = materialGoods.SalePriceAfterTax.ToString();
            txtSalePriceAfterTax2.Text = materialGoods.SalePriceAfterTax2.ToString();
            txtSalePriceAfterTax3.Text = materialGoods.SalePriceAfterTax3.ToString();

            txtSalePrice.FormatNumberic(ConstDatabase.Format_DonGiaQuyDoi);
            txtSalePrice2.FormatNumberic(ConstDatabase.Format_DonGiaQuyDoi);
            txtSalePrice3.FormatNumberic(ConstDatabase.Format_DonGiaQuyDoi);
            txtSalePriceAfterTax.FormatNumberic(ConstDatabase.Format_DonGiaQuyDoi);
            txtSalePriceAfterTax2.FormatNumberic(ConstDatabase.Format_DonGiaQuyDoi);
            txtSalePriceAfterTax3.FormatNumberic(ConstDatabase.Format_DonGiaQuyDoi);
            txtFixedSalePrice.FormatNumberic(ConstDatabase.Format_DonGiaQuyDoi);
            taxRateInput = taxRate;

        }

        private void chkActive_CheckedChanged(object sender, EventArgs e)
        {
            if (chkActive.CheckState == CheckState.Checked)
            {
                txtSalePriceAfterTax.Enabled = true;
                txtSalePriceAfterTax2.Enabled = true;
                txtSalePriceAfterTax3.Enabled = true;
                txtSalePrice2.Enabled = false;
                txtSalePrice3.Enabled = false;
                txtSalePrice.Enabled = false;
                //salePrice = 0;
                //salePrice2 = 0;
                //salePrice3 = 0;
                //txtSalePrice.Text = "0.00";
                //txtSalePrice2.Text = "0.00";
                //txtSalePrice3.Text = "0.00";
            }
            else
            {
                txtSalePriceAfterTax.Enabled = false;
                txtSalePriceAfterTax2.Enabled = false;
                txtSalePriceAfterTax3.Enabled = false;
                txtSalePrice2.Enabled = true;
                txtSalePrice3.Enabled = true;
                txtSalePrice.Enabled = true;
                salePriceAfterTax = 0;
                salePriceAfterTax2 = 0;
                salePriceAfterTax3 = 0;
                txtSalePriceAfterTax.Text = "0,00";
                txtSalePriceAfterTax2.Text = "0,00";
                txtSalePriceAfterTax3.Text = "0,00";
            }
        }



        private void btnSave_Click(object sender, EventArgs e)
        {
            isClose = false;
            decimal.TryParse(txtSalePrice.Text, out salePrice);
            _outputMaterialGoods.SalePrice = salePrice;
            decimal.TryParse(txtSalePrice2.Text, out salePrice2);
            _outputMaterialGoods.SalePrice2 = salePrice2;
            decimal.TryParse(txtSalePrice3.Text, out salePrice3);
            _outputMaterialGoods.SalePrice3 = salePrice3;
            decimal.TryParse(txtFixedSalePrice.Text, out fixedSalePrice);
            _outputMaterialGoods.FixedSalePrice = fixedSalePrice;
            decimal.TryParse(txtSalePriceAfterTax.Text, out salePriceAfterTax);
            _outputMaterialGoods.SalePriceAfterTax = salePriceAfterTax;
            decimal.TryParse(txtSalePriceAfterTax2.Text, out salePriceAfterTax2);
            _outputMaterialGoods.SalePriceAfterTax2 = salePriceAfterTax2;
            decimal.TryParse(txtSalePriceAfterTax3.Text, out salePriceAfterTax3);
            _outputMaterialGoods.SalePriceAfterTax3 = salePriceAfterTax3;
            CheckActive = chkActive.Checked;

            this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            isClose = true;
            this.Close();
        }

        private void txtSalePriceAfterTax_ValueChanged(object sender, EventArgs e)
        {
            decimal.TryParse(txtSalePriceAfterTax.Text, out salePriceAfterTax);
            if (salePriceAfterTax != 0)
            {
                salePrice = (salePriceAfterTax / (1 + taxRateInput));
                txtSalePrice.Text = salePrice.ToString();
            }
        }

        private void txtSalePriceAfterTax2_ValueChanged(object sender, EventArgs e)
        {
            decimal.TryParse(txtSalePriceAfterTax2.Text, out salePriceAfterTax2);
            if (salePriceAfterTax2 != 0)
            {
                salePrice2 = (salePriceAfterTax2 / (1 + taxRateInput));
                txtSalePrice2.Text = salePrice2.ToString();
            }
        }

        private void txtSalePriceAfterTax3_ValueChanged(object sender, EventArgs e)
        {
            decimal.TryParse(txtSalePriceAfterTax3.Text, out salePriceAfterTax3);
            if (salePriceAfterTax3 != 0)
            {
                salePrice3 = (salePriceAfterTax3 / (1 + taxRateInput));
                txtSalePrice3.Text = salePrice3.ToString();
            }
        }

        private void chkActive_CheckStateChanged(object sender, EventArgs e)
        {

        }

        private void chkActive_CheckedValueChanged(object sender, EventArgs e)
        {

        }

        private void FSalePice_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }

        //private void txtSalePriceAfterTax_AfterExitEditMode(object sender, EventArgs e)
        //{
        //    salePriceAfterTax = Convert.ToDecimal(txtSalePriceAfterTax.Text);
        //    salePrice = (salePriceAfterTax / (1 + taxRateInput));
        //    txtSalePrice.Text = salePrice.ToString();
        //}

        //private void txtSalePriceAfterTax2_AfterExitEditMode(object sender, EventArgs e)
        //{
        //    salePriceAfterTax2 = Convert.ToDecimal(txtSalePriceAfterTax2.Text);
        //    salePrice2 = (salePriceAfterTax2 / (1 + taxRateInput));
        //    txtSalePrice2.Text = salePrice2.ToString();
        //}

        //private void txtSalePriceAfterTax3_AfterExitEditMode(object sender, EventArgs e)
        //{
        //    salePriceAfterTax3 = Convert.ToDecimal(txtSalePriceAfterTax3.Text);
        //    salePrice3 = (salePriceAfterTax3 / (1 + taxRateInput));
        //    txtSalePrice3.Text = salePrice3.ToString();
        //}
    }
}
