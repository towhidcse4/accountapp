﻿namespace Accounting
{
    partial class FMaterialGoods
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.txtAmount = new Infragistics.Win.Misc.UltraLabel();
            this.txtQuantity = new Infragistics.Win.Misc.UltraLabel();
            this.txtUnit = new Infragistics.Win.Misc.UltraLabel();
            this.txtName = new Infragistics.Win.Misc.UltraLabel();
            this.txtCode = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.lbl1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridTax = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraPanel2 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.btnViewVoucher = new Infragistics.Win.Misc.UltraButton();
            this.dteToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbAboutTime = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.dteFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.panel1 = new System.Windows.Forms.Panel();
            this.uGridExport = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.cms4Grid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.tmsReLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.btnExportExcel = new Infragistics.Win.Misc.UltraButton();
            this.btnAdd = new Infragistics.Win.Misc.UltraButton();
            this.btnDelete = new Infragistics.Win.Misc.UltraButton();
            this.btnEdit = new Infragistics.Win.Misc.UltraButton();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraSplitter1 = new Infragistics.Win.Misc.UltraSplitter();
            this.ultraTabPageControl1.SuspendLayout();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridTax)).BeginInit();
            this.ultraPanel2.ClientArea.SuspendLayout();
            this.ultraPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dteToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAboutTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFromDate)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridExport)).BeginInit();
            this.cms4Grid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).BeginInit();
            this.ultraTabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.txtAmount);
            this.ultraTabPageControl1.Controls.Add(this.txtQuantity);
            this.ultraTabPageControl1.Controls.Add(this.txtUnit);
            this.ultraTabPageControl1.Controls.Add(this.txtName);
            this.ultraTabPageControl1.Controls.Add(this.txtCode);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel5);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel4);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel3);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel2);
            this.ultraTabPageControl1.Controls.Add(this.lbl1);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 22);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(766, 190);
            // 
            // txtAmount
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            this.txtAmount.Appearance = appearance1;
            this.txtAmount.Location = new System.Drawing.Point(152, 93);
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(300, 22);
            this.txtAmount.TabIndex = 9;
            // 
            // txtQuantity
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            this.txtQuantity.Appearance = appearance2;
            this.txtQuantity.Location = new System.Drawing.Point(152, 71);
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.Size = new System.Drawing.Size(300, 22);
            this.txtQuantity.TabIndex = 8;
            // 
            // txtUnit
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextVAlignAsString = "Middle";
            this.txtUnit.Appearance = appearance3;
            this.txtUnit.Location = new System.Drawing.Point(152, 49);
            this.txtUnit.Name = "txtUnit";
            this.txtUnit.Size = new System.Drawing.Size(300, 22);
            this.txtUnit.TabIndex = 7;
            // 
            // txtName
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.txtName.Appearance = appearance4;
            this.txtName.Location = new System.Drawing.Point(152, 27);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(300, 22);
            this.txtName.TabIndex = 6;
            // 
            // txtCode
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.TextVAlignAsString = "Middle";
            this.txtCode.Appearance = appearance5;
            this.txtCode.Location = new System.Drawing.Point(152, 5);
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(300, 22);
            this.txtCode.TabIndex = 5;
            // 
            // ultraLabel5
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance6;
            this.ultraLabel5.Location = new System.Drawing.Point(3, 93);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel5.TabIndex = 4;
            this.ultraLabel5.Text = "Giá trị tồn";
            // 
            // ultraLabel4
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance7;
            this.ultraLabel4.Location = new System.Drawing.Point(3, 71);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel4.TabIndex = 3;
            this.ultraLabel4.Text = "Số lượng tồn";
            // 
            // ultraLabel3
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance8;
            this.ultraLabel3.Location = new System.Drawing.Point(3, 49);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel3.TabIndex = 2;
            this.ultraLabel3.Text = "Đơn vị tính";
            // 
            // ultraLabel2
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance9;
            this.ultraLabel2.Location = new System.Drawing.Point(3, 27);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(120, 22);
            this.ultraLabel2.TabIndex = 1;
            this.ultraLabel2.Text = "Tên vật tư, hàng hoá";
            // 
            // lbl1
            // 
            appearance10.TextVAlignAsString = "Middle";
            this.lbl1.Appearance = appearance10;
            this.lbl1.Location = new System.Drawing.Point(3, 5);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(100, 22);
            this.lbl1.TabIndex = 0;
            this.lbl1.Text = "Mã hàng";
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.ultraGroupBox1);
            this.ultraTabPageControl2.Controls.Add(this.ultraPanel2);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(766, 190);
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.uGridTax);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(766, 152);
            this.ultraGroupBox1.TabIndex = 3;
            // 
            // uGridTax
            // 
            this.uGridTax.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridTax.Location = new System.Drawing.Point(3, 0);
            this.uGridTax.Name = "uGridTax";
            this.uGridTax.Size = new System.Drawing.Size(760, 149);
            this.uGridTax.TabIndex = 0;
            this.uGridTax.DoubleClick += new System.EventHandler(this.uGridTax_DoubleClick);
            // 
            // ultraPanel2
            // 
            // 
            // ultraPanel2.ClientArea
            // 
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel13);
            this.ultraPanel2.ClientArea.Controls.Add(this.btnViewVoucher);
            this.ultraPanel2.ClientArea.Controls.Add(this.dteToDate);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel12);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel11);
            this.ultraPanel2.ClientArea.Controls.Add(this.cbbAboutTime);
            this.ultraPanel2.ClientArea.Controls.Add(this.dteFromDate);
            this.ultraPanel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraPanel2.Location = new System.Drawing.Point(0, 152);
            this.ultraPanel2.Name = "ultraPanel2";
            this.ultraPanel2.Size = new System.Drawing.Size(766, 38);
            this.ultraPanel2.TabIndex = 2;
            // 
            // ultraLabel13
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel13.Appearance = appearance11;
            this.ultraLabel13.Location = new System.Drawing.Point(503, 10);
            this.ultraLabel13.Name = "ultraLabel13";
            this.ultraLabel13.Size = new System.Drawing.Size(25, 19);
            this.ultraLabel13.TabIndex = 81;
            this.ultraLabel13.Text = "Đến";
            // 
            // btnViewVoucher
            // 
            this.btnViewVoucher.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnViewVoucher.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2010Button;
            this.btnViewVoucher.Location = new System.Drawing.Point(680, 6);
            this.btnViewVoucher.Name = "btnViewVoucher";
            this.btnViewVoucher.Size = new System.Drawing.Size(75, 23);
            this.btnViewVoucher.TabIndex = 1;
            this.btnViewVoucher.Text = "&Xem ";
            this.btnViewVoucher.Click += new System.EventHandler(this.btnViewVoucher_Click);
            // 
            // dteToDate
            // 
            appearance12.TextHAlignAsString = "Center";
            appearance12.TextVAlignAsString = "Middle";
            this.dteToDate.Appearance = appearance12;
            this.dteToDate.AutoSize = false;
            this.dteToDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteToDate.Location = new System.Drawing.Point(534, 9);
            this.dteToDate.MaskInput = "dd/mm/yyyy";
            this.dteToDate.Name = "dteToDate";
            this.dteToDate.Size = new System.Drawing.Size(85, 22);
            this.dteToDate.TabIndex = 79;
            this.dteToDate.Value = null;
            this.dteToDate.ValueChanged += new System.EventHandler(this.dteToDate_ValueChanged);
            // 
            // ultraLabel12
            // 
            appearance13.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel12.Appearance = appearance13;
            this.ultraLabel12.Location = new System.Drawing.Point(371, 10);
            this.ultraLabel12.Name = "ultraLabel12";
            this.ultraLabel12.Size = new System.Drawing.Size(17, 19);
            this.ultraLabel12.TabIndex = 80;
            this.ultraLabel12.Text = "Từ";
            // 
            // ultraLabel11
            // 
            appearance14.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel11.Appearance = appearance14;
            this.ultraLabel11.Location = new System.Drawing.Point(1, 10);
            this.ultraLabel11.Name = "ultraLabel11";
            this.ultraLabel11.Size = new System.Drawing.Size(89, 19);
            this.ultraLabel11.TabIndex = 76;
            this.ultraLabel11.Text = "Khoảng thời gian";
            // 
            // cbbAboutTime
            // 
            this.cbbAboutTime.AutoSize = false;
            this.cbbAboutTime.Location = new System.Drawing.Point(93, 8);
            this.cbbAboutTime.Name = "cbbAboutTime";
            this.cbbAboutTime.Size = new System.Drawing.Size(222, 23);
            this.cbbAboutTime.TabIndex = 77;
            this.cbbAboutTime.ValueChanged += new System.EventHandler(this.cbbAboutTime_ValueChanged);
            // 
            // dteFromDate
            // 
            appearance15.TextHAlignAsString = "Center";
            appearance15.TextVAlignAsString = "Middle";
            this.dteFromDate.Appearance = appearance15;
            this.dteFromDate.AutoSize = false;
            this.dteFromDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteFromDate.Location = new System.Drawing.Point(394, 8);
            this.dteFromDate.MaskInput = "dd/mm/yyyy";
            this.dteFromDate.Name = "dteFromDate";
            this.dteFromDate.Size = new System.Drawing.Size(93, 23);
            this.dteFromDate.TabIndex = 78;
            this.dteFromDate.Value = null;
            this.dteFromDate.ValueChanged += new System.EventHandler(this.dteFromDate_ValueChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.uGridExport);
            this.panel1.Controls.Add(this.btnExportExcel);
            this.panel1.Controls.Add(this.btnAdd);
            this.panel1.Controls.Add(this.btnDelete);
            this.panel1.Controls.Add(this.btnEdit);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(768, 52);
            this.panel1.TabIndex = 3;
            // 
            // uGridExport
            // 
            this.uGridExport.ContextMenuStrip = this.cms4Grid;
            this.uGridExport.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGridExport.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGridExport.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGridExport.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGridExport.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.uGridExport.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGridExport.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGridExport.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGridExport.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGridExport.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGridExport.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGridExport.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGridExport.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.uGridExport.Location = new System.Drawing.Point(388, 12);
            this.uGridExport.Name = "uGridExport";
            this.uGridExport.Size = new System.Drawing.Size(368, 297);
            this.uGridExport.TabIndex = 64;
            this.uGridExport.Text = "ultraGrid1";
            this.uGridExport.Visible = false;
            // 
            // cms4Grid
            // 
            this.cms4Grid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.tsmAdd,
            this.tsmEdit,
            this.tsmDelete,
            this.tmsReLoad});
            this.cms4Grid.Name = "cms4Grid";
            this.cms4Grid.Size = new System.Drawing.Size(148, 98);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(144, 6);
            // 
            // tsmAdd
            // 
            this.tsmAdd.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.tsmAdd.Name = "tsmAdd";
            this.tsmAdd.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.tsmAdd.Size = new System.Drawing.Size(147, 22);
            this.tsmAdd.Text = "Thêm";
            this.tsmAdd.Click += new System.EventHandler(this.tsmAdd_Click);
            // 
            // tsmEdit
            // 
            this.tsmEdit.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            this.tsmEdit.Name = "tsmEdit";
            this.tsmEdit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.tsmEdit.Size = new System.Drawing.Size(147, 22);
            this.tsmEdit.Text = "Sửa";
            this.tsmEdit.Click += new System.EventHandler(this.tsmEdit_Click);
            // 
            // tsmDelete
            // 
            this.tsmDelete.Image = global::Accounting.Properties.Resources.xoa1_xoa_du_lieu_ke_toan_khoi_database_;
            this.tsmDelete.Name = "tsmDelete";
            this.tsmDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.tsmDelete.Size = new System.Drawing.Size(147, 22);
            this.tsmDelete.Text = "Xóa";
            this.tsmDelete.Click += new System.EventHandler(this.tsmDelete_Click);
            // 
            // tmsReLoad
            // 
            this.tmsReLoad.Image = global::Accounting.Properties.Resources.ubtnReset;
            this.tmsReLoad.Name = "tmsReLoad";
            this.tmsReLoad.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.tmsReLoad.Size = new System.Drawing.Size(147, 22);
            this.tmsReLoad.Text = "Tải Lại";
            this.tmsReLoad.Click += new System.EventHandler(this.tmsReLoad_Click);
            // 
            // btnExportExcel
            // 
            appearance16.BackColor = System.Drawing.Color.DarkRed;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance16.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance16.Image = global::Accounting.Properties.Resources.Excel1;
            this.btnExportExcel.Appearance = appearance16;
            this.btnExportExcel.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance17.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance17.BorderAlpha = Infragistics.Win.Alpha.Opaque;
            this.btnExportExcel.HotTrackAppearance = appearance17;
            this.btnExportExcel.Location = new System.Drawing.Point(253, 12);
            this.btnExportExcel.Name = "btnExportExcel";
            appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.btnExportExcel.PressedAppearance = appearance18;
            this.btnExportExcel.Size = new System.Drawing.Size(75, 30);
            this.btnExportExcel.TabIndex = 63;
            this.btnExportExcel.Text = "Kết xuất";
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // btnAdd
            // 
            appearance19.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.btnAdd.Appearance = appearance19;
            this.btnAdd.Location = new System.Drawing.Point(10, 12);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 30);
            this.btnAdd.TabIndex = 2;
            this.btnAdd.Text = "Thêm";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnDelete
            // 
            appearance20.Image = global::Accounting.Properties.Resources.xoa1_xoa_du_lieu_ke_toan_khoi_database_;
            this.btnDelete.Appearance = appearance20;
            this.btnDelete.Location = new System.Drawing.Point(172, 12);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 30);
            this.btnDelete.TabIndex = 1;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnEdit
            // 
            appearance21.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            this.btnEdit.Appearance = appearance21;
            this.btnEdit.Location = new System.Drawing.Point(91, 12);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(75, 30);
            this.btnEdit.TabIndex = 0;
            this.btnEdit.Text = "Sửa";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // uGrid
            // 
            this.uGrid.ContextMenuStrip = this.cms4Grid;
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.uGrid.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGrid.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGrid.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGrid.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.uGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid.Location = new System.Drawing.Point(0, 52);
            this.uGrid.Name = "uGrid";
            this.uGrid.Size = new System.Drawing.Size(768, 297);
            this.uGrid.TabIndex = 5;
            this.uGrid.Text = "uGrid";
            this.uGrid.BeforeRowActivate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.uGrid_BeforeRowActivate);
            this.uGrid.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.uGrid_AfterSelectChange);
            this.uGrid.DoubleClick += new System.EventHandler(this.uGrid_DoubleClick);
            this.uGrid.MouseDown += new System.Windows.Forms.MouseEventHandler(this.uGrid_MouseDown);
            // 
            // ultraTabControl1
            // 
            this.ultraTabControl1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl2);
            this.ultraTabControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraTabControl1.Location = new System.Drawing.Point(0, 359);
            this.ultraTabControl1.Name = "ultraTabControl1";
            this.ultraTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.ultraTabControl1.Size = new System.Drawing.Size(768, 213);
            this.ultraTabControl1.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Office2007Ribbon;
            this.ultraTabControl1.TabIndex = 6;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "1. Thông tin chung";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "2. Chứng từ";
            this.ultraTabControl1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            this.ultraTabControl1.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007;
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(766, 190);
            // 
            // ultraSplitter1
            // 
            this.ultraSplitter1.BackColor = System.Drawing.SystemColors.Control;
            this.ultraSplitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraSplitter1.Location = new System.Drawing.Point(0, 349);
            this.ultraSplitter1.Name = "ultraSplitter1";
            this.ultraSplitter1.RestoreExtent = 239;
            this.ultraSplitter1.Size = new System.Drawing.Size(768, 10);
            this.ultraSplitter1.TabIndex = 37;
            // 
            // FMaterialGoods
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(768, 572);
            this.Controls.Add(this.uGrid);
            this.Controls.Add(this.ultraSplitter1);
            this.Controls.Add(this.ultraTabControl1);
            this.Controls.Add(this.panel1);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FMaterialGoods";
            this.Text = "Công cụ, dụng cụ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FMaterialGoods_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FMaterialGoods_FormClosed);
            this.ultraTabPageControl1.ResumeLayout(false);
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridTax)).EndInit();
            this.ultraPanel2.ClientArea.ResumeLayout(false);
            this.ultraPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dteToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAboutTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFromDate)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridExport)).EndInit();
            this.cms4Grid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).EndInit();
            this.ultraTabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private Infragistics.Win.Misc.UltraButton btnAdd;
        private Infragistics.Win.Misc.UltraButton btnDelete;
        private Infragistics.Win.Misc.UltraButton btnEdit;
        private System.Windows.Forms.ContextMenuStrip cms4Grid;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripMenuItem tsmEdit;
        private System.Windows.Forms.ToolStripMenuItem tsmDelete;
        private System.Windows.Forms.ToolStripMenuItem tmsReLoad;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridTax;
        private Infragistics.Win.Misc.UltraLabel lbl1;
        private Infragistics.Win.Misc.UltraLabel txtUnit;
        private Infragistics.Win.Misc.UltraLabel txtName;
        private Infragistics.Win.Misc.UltraLabel txtCode;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel txtAmount;
        private Infragistics.Win.Misc.UltraLabel txtQuantity;
        private Infragistics.Win.Misc.UltraSplitter ultraSplitter1;
        private Infragistics.Win.Misc.UltraPanel ultraPanel2;
        private Infragistics.Win.Misc.UltraButton btnViewVoucher;
        private Infragistics.Win.Misc.UltraLabel ultraLabel13;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteToDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel12;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAboutTime;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteFromDate;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        public Infragistics.Win.Misc.UltraButton btnExportExcel;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridExport;
    }
}