﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using System.Transactions;
using Accounting.Core;

//using System.Linq;

namespace Accounting
{
    public partial class FMaterialGoodsDetail : CustormForm
    {
        #region khai báo
        private readonly IMaterialGoodsService _IMaterialGoodsService;
        private readonly IMaterialGoodsAssemblyService _IMaterialGoodsAssemblyService;
        private readonly IMaterialGoodsCategoryService _IMaterialGoodsCategoryService;
        private readonly IAccountingObjectService _IAccountingObjectService;
        private readonly IRepositoryService _IRepositoryService;
        private readonly ISaleDiscountPolicyService _ISaleDiscountPolicyService;
        private readonly IMaterialGoodsSpecialTaxGroupService _IMaterialGoodsSpecialTaxGroupService;
        private readonly IWarrantyService _IWarrantyService;
        BindingList<SaleDiscountPolicy> saleDiscountPolicies = new BindingList<SaleDiscountPolicy>();
        List<Warranty> lstWarranty = new List<Warranty>();
        MaterialGoods _Select;
        static MaterialGoods input;
        public decimal mTaxrate = 0;
        public Guid Id { get; private set; }
        //biến này không được sử dụng
        private int _statusForm = ConstFrm.optStatusForm.View;
        // 3 tai` khoản kho , chi phi , doanh thu
        List<string> accNumber2 = new List<string>() { "241", "154", "642", "811", "631", "632" };
        List<string> accNumber1 = new List<string>() { "152", "153", "154", "155", "156", "157" };
        List<string> accNumber3 = new List<string>() { "5111", "5112", "5113", "5118", ",711", "515" };

        /// <summary>
        /// ///////////////
        /// </summary>
        bool Them = true;
        public static string MaterialGoodsCode = null;
        public static bool isClose = true;
        public static bool _forceReload = false;
        #endregion

        public FMaterialGoodsDetail()
        {//Thêm

            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            //Khai báo các webservices
            _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();

            _IMaterialGoodsAssemblyService = IoC.Resolve<IMaterialGoodsAssemblyService>();
            _IMaterialGoodsCategoryService = IoC.Resolve<IMaterialGoodsCategoryService>();
            _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();
            _IRepositoryService = IoC.Resolve<IRepositoryService>();
            _ISaleDiscountPolicyService = IoC.Resolve<ISaleDiscountPolicyService>();
            _IMaterialGoodsSpecialTaxGroupService = IoC.Resolve<IMaterialGoodsSpecialTaxGroupService>();
            _IWarrantyService = IoC.Resolve<IWarrantyService>();

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            _Select = new MaterialGoods();
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            cbbMaterialGoodsType.SelectedIndex = 0;
            #endregion
            this.Text = "Thêm mới Vật tư, Hàng hoá";
            //đóng Isactive
            chkActive.Visible = false;
            if (Utils.ListSystemOption.FirstOrDefault(x => x.Code == "PPTTGTGT").Data == "Phương pháp trực tiếp trên doanh thu")
            {
                cbbTaxRate.Enabled = false;
            }
            ultraTabControl1.Tabs[1].Visible = false;
        }

        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }

        private void LoadDuLieu(bool configGrid)
        {
            #region Lấy dữ liệu từ CSDL

            List<MaterialGoodsAssembly> list1 = new List<MaterialGoodsAssembly>();
            _IMaterialGoodsAssemblyService.UnbindSession(list1);
            BindingList<MaterialGoodsAssembly> bdMaterialGoods = new BindingList<MaterialGoodsAssembly>(list1);
            #endregion

            #region Xử lý dữ liệu
            #endregion

            #region hiển thị và xử lý hiển thị
            uGrid.DataSource = bdMaterialGoods;
            uGrid.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
            uGrid.DisplayLayout.Override.TemplateAddRowPrompt = "Bấm vào đây để thêm mới....";
            uGrid.DisplayLayout.Override.SpecialRowSeparator = Infragistics.Win.UltraWinGrid.SpecialRowSeparator.None;
            //Hiện những dòng trống?
            uGrid.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            uGrid.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.ExtendFirstCell;
            //tắt lọc cột
            uGrid.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.Default;
            //tắt tiêu đề
            uGrid.DisplayLayout.CaptionVisible = DefaultableBoolean.False;



            CreaterColumsStyle(uGrid);
            if (configGrid) ConfigGrid(uGrid);
            #endregion

        }

        public FMaterialGoodsDetail(MaterialGoods temp)
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            //temp.CopyTo(ref _Select);
            Them = false;
            //txtMaterialGoodsCode.Enabled = false;

            //Khai báo các webservices
            _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
            _IMaterialGoodsCategoryService = IoC.Resolve<IMaterialGoodsCategoryService>();
            _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();
            _IRepositoryService = IoC.Resolve<IRepositoryService>();
            _IMaterialGoodsAssemblyService = IoC.Resolve<IMaterialGoodsAssemblyService>();
            _ISaleDiscountPolicyService = IoC.Resolve<ISaleDiscountPolicyService>();
            _IMaterialGoodsSpecialTaxGroupService = IoC.Resolve<IMaterialGoodsSpecialTaxGroupService>();
            _IWarrantyService = IoC.Resolve<IWarrantyService>();
            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            _Select = _IMaterialGoodsService.Getbykey(temp.ID);
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion

            #region Fill dữ liệu Obj vào control
            ObjandGUI(temp, false);
            saleDiscountPolicies = new BindingList<SaleDiscountPolicy>(temp.SaleDiscountPolicys);
            #endregion
            this.Text = "Sửa Vật tư, Hàng hoá";
            chkActive.Visible = true;
            if (Utils.ListSystemOption.FirstOrDefault(x => x.Code == "PPTTGTGT").Data == "Phương pháp trực tiếp trên doanh thu")
            {
                cbbTaxRate.Enabled = false;
            }
        }

        void loadNCC()
        {
            //cbbAccountingObjectID.DataSource = _IAccountingObjectService.getAccountingObjects(1).Where(p => p.IsActive == true).ToList();
            //cbbAccountingObjectID.DataSource = _IAccountingObjectService.GetAccountingObjects(1, true); //thành duy đã sửa chỗ này
            //cbbAccountingObjectID.DisplayMember = "AccountingObjectCode";
            //Utils.ConfigGrid(cbbAccountingObjectID, ConstDatabase.AccountingObject_TableName);
            //this.ConfigCombo(Utils.ListAccountingObject, cbbAccountingObjectID, "AccountingObjectCode", "ID", accountingObjectType: 1);
        }
        void loadKho()
        {
            //kho
            //cbbRepository.DataSource = _IRepositoryService.Query.Where(p => p.IsActive == true).ToList();
            //cbbRepository.DataSource = _IRepositoryService.GetAll_ByIsActive(true);
            //cbbRepository.DisplayMember = "RepositoryCode";
            //Utils.ConfigGrid(cbbRepository, ConstDatabase.Repository_TableName);
            this.ConfigCombo(Utils.ListRepository, cbbRepository, "RepositoryCode", "ID");
        }
        #region xử lý các combo trong Form Detail
        private void InitializeGUI()
        {
            btnDiscount.Enabled = false;

            //ultraCombo6.DataSource = _IMaterialGoodsSpecialTaxGroupService.Query.Where(p => p.IsActive == true).ToList();
            ultraCombo6.DataSource = _IMaterialGoodsSpecialTaxGroupService.GetAll_ByIsActive(true).OrderBy(p => p.MaterialGoodsSpecialTaxGroupCode).ToList();//thành duy đã sửa chỗ này
            ultraCombo6.DisplayMember = "MaterialGoodsSpecialTaxGroupCode";
            Utils.ConfigGrid(ultraCombo6, ConstDatabase.MaterialGoodsSpecialTaxGroup_TableName);
            //khởi tạo cbb
            //cbbMaterialGoodCategory.DataSource = _IMaterialGoodsCategoryService.Query.Where(p => p.IsActive == true).ToList();
            cbbMaterialGoodCategory.DataSource = _IMaterialGoodsCategoryService.GetAll_ByIsActive(true);//thành duy đã sửa chỗ này
            cbbMaterialGoodCategory.DisplayMember = "MaterialGoodsCategoryCode";
            cbbMaterialGoodCategory.ValueMember = "ID";
            Utils.ConfigGrid(cbbMaterialGoodCategory, ConstDatabase.MaterialGoodsCategory_TableName);
            lstWarranty = _IWarrantyService.getActive();
            cbbWarrantyTime.DataSource = lstWarranty;
            //cbbWarrantyTime.DataBindings.Add("Value", _Select, "WarrantyTime", true, DataSourceUpdateMode.OnPropertyChanged);
            cbbWarrantyTime.DisplayMember = "WarrantyName";
            cbbWarrantyTime.ValueMember = "WarrantyName";
            Utils.ConfigGrid(cbbWarrantyTime, ConstDatabase.Warranty_TableName);

            loadNCC();


            loadKho();


            //1

            List<Account> list = new List<Account>();
            foreach (var item in accNumber1)
            {
                foreach (Account acc in Utils.GetChildrenAccount(new Account(item)))
                    list.Add(acc);
            }
            cbbReponsitoryAccount.DataSource = list.ToList().OrderBy(p => p.AccountNumber).Where(p => p.IsActive == true).ToList();
            cbbReponsitoryAccount.DisplayMember = "AccountNumber";
            Utils.ConfigGrid(cbbReponsitoryAccount, ConstDatabase.Account_TableName);

            //2

            List<Account> list2 = new List<Account>();
            foreach (var item in accNumber2)
            {
                foreach (Account acc in Utils.GetChildrenAccount(new Account(item)))
                    list2.Add(acc);
            }
            cbbExpenseAccount.DataSource = list2.ToList().OrderBy(p => p.AccountNumber).Where(p => p.IsActive == true).ToList();
            cbbExpenseAccount.DisplayMember = "AccountNumber";
            Utils.ConfigGrid(cbbExpenseAccount, ConstDatabase.Account_TableName);


            List<Account> list3 = new List<Account>();
            foreach (var item in accNumber3)
            {
                foreach (Account acc in Utils.GetChildrenAccount(new Account(item)))
                    list3.Add(acc);
            }
            cbbRevenueAccount.DataSource = list3.ToList().OrderBy(p => p.AccountNumber).Where(p => p.IsActive == true).ToList();
            cbbRevenueAccount.DisplayMember = "AccountNumber";
            Utils.ConfigGrid(cbbRevenueAccount, ConstDatabase.Account_TableName);

            if (Utils.ListSystemOption.FirstOrDefault(x => x.Code == "PPTTGTGT").Data == "Phương pháp trực tiếp trên doanh thu")
            {
                ultraLabel5.Visible = true;
                cbbCareerGroup.Visible = true;
                cbbCareerGroup.DataSource = Utils.ListCareerGroup;
                cbbCareerGroup.DisplayMember = "CareerGroupName";
                cbbCareerGroup.ValueMember = "ID";
                Utils.ConfigGrid(cbbCareerGroup, ConstDatabase.CareerGroup_TableName);
                foreach (var item in cbbCareerGroup.Rows)
                {
                    if ((item.ListObject as CareerGroup).CareerGroupCode == (Utils.ListSystemOption.FirstOrDefault(x => x.Code == "VTHH_NhomNNMD").Data)) cbbCareerGroup.SelectedRow = item;
                }
            }
            //List<string> Taxrate = new List<string> { "0%", "5%", "10%", "KCT" };
            //List<Rate> Taxrate = new List<Rate>() { new Rate() { Description = "0%", Value = 0 }, new Rate() { Description = "5%", Value = 5 }, new Rate() { Description = "10%", Value = 10 }, new Rate() { Description = "KCT", Value = 0 } };
            //cbbTaxRate.DataSource = Taxrate.ToList();
            ValueListItem[] Taxrate = new[] { new ValueListItem(0, "0%"), new ValueListItem(5, "5%"), new ValueListItem(10, "10%"), new ValueListItem(-1, "Không chịu thuế"), new ValueListItem(-2, "Không tính thuế") };
            cbbTaxRate.Items.AddRange(Taxrate);
            txtConvertRate.FormatNumberic(ConstDatabase.Format_Coefficient);
            txtMinimumStock.FormatNumberic(ConstDatabase.Format_Quantity);
            txtSalePrice.FormatNumberic(ConstDatabase.Format_DonGiaQuyDoi);
            txtPuchasePrice.FormatNumberic(ConstDatabase.Format_DonGiaQuyDoi);
            txtPuchaseDiscountRate.FormatNumberic(ConstDatabase.Format_Coefficient);
            txtSaleDiscountRate.FormatNumberic(ConstDatabase.Format_Coefficient);
        }
        #endregion

        MaterialGoods ObjandGUI(MaterialGoods input, bool isGet)
        {
            if (isGet)
            {
                //if (input == null) input = new MaterialGoods();
                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();  //Thêm mới
                //MaterialGoods uControlItem = uGrid.Rows[0].ListObject as MaterialGoods;

                input.MaterialGoodsCode = txtMaterialGoodsCode.Text;
                input.MaterialGoodsName = txtMaterialGoodsName.Text;
                input.MaterialGoodsType = cbbMaterialGoodsType.SelectedIndex;
                //Nhóm Vật tư
                MaterialGoodsCategory temp1 = (MaterialGoodsCategory)Utils.getSelectCbbItem(cbbMaterialGoodCategory);
                if (temp1 == null) input.MaterialGoodsCategoryID = null;
                else input.MaterialGoodsCategoryID = temp1.ID;

                Account temp = (Account)Utils.getSelectCbbItem(cbbExpenseAccount);
                if (temp == null) input.ExpenseAccount = string.Empty;
                else input.ExpenseAccount = temp.AccountNumber;

                Account temp2 = (Account)Utils.getSelectCbbItem(cbbRevenueAccount);
                if (temp2 == null) input.RevenueAccount = string.Empty;
                else input.RevenueAccount = temp2.AccountNumber;

                Account temp3 = (Account)Utils.getSelectCbbItem(cbbReponsitoryAccount);
                if (temp3 == null) input.ReponsitoryAccount = string.Empty;
                else input.ReponsitoryAccount = temp3.AccountNumber;

                Repository temp7 = (Repository)Utils.getSelectCbbItem(cbbRepository);
                if (temp7 == null) input.RepositoryID = null;
                else input.RepositoryID = temp7.ID;
                input.WarrantyTime = cbbWarrantyTime.Text;
                //AccountingObject temp4 = (AccountingObject)Utils.getSelectCbbItem(cbbAccountingObjectID);
                //if (temp4 == null) input.AccountingObjectID = null;
                //else input.AccountingObjectID = temp4.ID;
                if (Utils.ListSystemOption.FirstOrDefault(x => x.Code == "PPTTGTGT").Data == "Phương pháp trực tiếp trên doanh thu")
                {
                    input.CareerGroupID = (Guid?)cbbCareerGroup.Value;
                }
                input.MaterialToolType = chkMaterialToolType.CheckState == CheckState.Checked ? 2 : 0;
                input.IsActive = chkActive.Checked;
                input.IsSaleDiscountPolicy = chkIsSaleDiscountPolicy.CheckState == CheckState.Checked ? true : false;
                input.IsSecurity = false;
                if (!txtConvertRate.Text.Equals(""))
                {
                    input.ConvertRate = Utils.decimalTryParse(txtConvertRate.Text);
                }
                else
                {
                    input.ConvertRate = 0;
                }
                input.Unit = txtUnit.Text;
                input.ConvertUnit = txtConvertUnit.Text;
                input.PurchasePrice = txtPuchasePrice.Value == null ? 0 : Utils.decimalTryParse(txtPuchasePrice.Text);
                if (Them)
                {
                    input.MaterialGoodsAssemblys.Clear();
                    if (cbbMaterialGoodsType.SelectedIndex == 1)
                    {
                        #region MCReceiptDetailCustomer
                        List<MaterialGoodsAssembly> uMaterialGoodsAssembly = ((BindingList<MaterialGoodsAssembly>)uGrid.DataSource).ToList();
                        foreach (MaterialGoodsAssembly item in uMaterialGoodsAssembly.GroupBy(x => x.MaterialAssemblyID).Select(x => x.First()))
                        {
                            item.ID = Guid.NewGuid();
                            item.MaterialGoodsID = input.ID;
                            input.MaterialGoodsAssemblys.Add(item);

                        }
                        #endregion
                    }
                }
                else
                {
                    input.MaterialGoodsAssemblys.Clear();
                    if (cbbMaterialGoodsType.SelectedIndex == 1)
                    {
                        List<MaterialGoodsAssembly> uMaterialGoodsAssembly = ((BindingList<MaterialGoodsAssembly>)uGrid.DataSource).ToList();
                        foreach (MaterialGoodsAssembly item in uMaterialGoodsAssembly.GroupBy(x => x.MaterialAssemblyID).Select(x => x.First()))
                        {
                            input.MaterialGoodsAssemblys.Add(new MaterialGoodsAssembly
                            {
                                ID = Guid.NewGuid(),
                                MaterialGoodsID = input.ID,
                                MaterialAssemblyID = item.MaterialAssemblyID,
                                MaterialAssemblyDescription = item.MaterialAssemblyDescription,
                                Quantity = item.Quantity,
                                UnitPrice = item.UnitPrice,
                                Unit = item.Unit,
                                UnitConvert = item.UnitConvert,
                                TotalMoney = item.TotalMoney
                            });
                        }
                    }

                }
                //else
                //{
                //    List<MaterialGoodsAssembly> uMaterialGoodsAssembly = ((BindingList<MaterialGoodsAssembly>)uGrid.DataSource).ToList();
                //    if (input.MaterialGoodsAssemblys.Count() == 0)
                //    {   //cac phan tử được chọn giống nhau sẽ chỉ lấy 1 phần tử
                //        foreach (MaterialGoodsAssembly item in uMaterialGoodsAssembly.GroupBy(x => x.MaterialAssemblyID).Select(x => x.First()))
                //        {
                //            item.ID = Guid.NewGuid();
                //            item.MaterialGoodsID = input.ID;
                //            input.MaterialGoodsAssemblys.Add(item);
                //        }
                //    }
                //else
                //{
                //    ////Kiem tra va xoa phan tu o list cu neu list sau khi sua khong co
                //    for (int j = 0; j < input.MaterialGoodsAssemblys.Count(); j++)
                //    {
                //        Boolean T = false;
                //        foreach (MaterialGoodsAssembly gt in uMaterialGoodsAssembly)
                //        {
                //            //MaterialGoods mg = _IMaterialGoodsService.Getbykey(gt.MaterialGoodsID);
                //            //if (input.MaterialGoodsAssemblys[j].MaterialGoodsID.Equals(mg.ID))
                //            if (input.MaterialGoodsAssemblys[j].MaterialAssemblyID.Equals(gt.MaterialAssemblyID))
                //                T = true;
                //            if (T) break;
                //        }
                //        if (!T)
                //        {
                //            //_IMaterialGoodsAssemblyService.Delete(input.MaterialGoodsAssemblys[j]);
                //            input.MaterialGoodsAssemblys.RemoveAt(j);
                //        }
                //    }

                //    foreach (MaterialGoodsAssembly item in uMaterialGoodsAssembly.GroupBy(x => x.MaterialAssemblyID).Select(x => x.First()))
                //    {
                //        Boolean T = false;
                //        //MaterialGoods mg = _IMaterialGoodsService.GetAll_ByMaterialGoodsCode(item.MaterialGoodsCode);
                //        for (int i = 0; i < input.MaterialGoodsAssemblys.Count(); i++)
                //        {
                //            //MaterialGoods mtg = _IMaterialGoodsService.Getbykey(test1s[i].MaterialGoodsID);
                //            if (item.MaterialAssemblyID.Equals(input.MaterialGoodsAssemblys[i].MaterialAssemblyID))
                //                T = true;
                //            if (T) break;
                //        }
                //        if (!T)
                //        {
                //            item.ID = Guid.NewGuid();
                //            item.MaterialGoodsID = input.ID;
                //            input.MaterialGoodsAssemblys.Add(item);
                //        }
                //    }
                //}
                //}
                input.TaxRate = cbbTaxRate.Value != null ? Utils.decimalTryParse(cbbTaxRate.Value != null ? cbbTaxRate.Value.ToString() : "") : (decimal?)null;
                //input.SaleDiscountPolicys.Clear();

                if (_Select != null && _Select.ID != null)
                {
                    foreach (var item in saleDiscountPolicies)
                    {
                        //var t = item.CloneObject();
                        //t.ID = Guid.Empty;
                        //if(t.MaterialGoodsID == Guid.Empty)
                        //    t.MaterialGoodsID = input.ID;
                        //input.SaleDiscountPolicys.Add(t);
                        if (item.ID == Guid.Empty)
                        {
                            item.ID = Guid.NewGuid();
                            item.MaterialGoodsID = input.ID;
                        }
                    }
                }

                input.ItemSource = txtItemSource.Text;
                //input.PurchaseDescription = txtPurchaseDescription.Text;
                //input.SaleDescription = txtSaleDescription.Text;
                input.SaleDiscountRate = !string.IsNullOrEmpty(txtSaleDiscountRate.Text) ? Utils.decimalTryParse(txtSaleDiscountRate.Text) : 0;
                input.PurchaseDiscountRate = !string.IsNullOrEmpty(txtPuchaseDiscountRate.Text) ? Utils.decimalTryParse(txtPuchaseDiscountRate.Text) : 0;
                input.SalePrice = !string.IsNullOrEmpty(txtSalePrice.Text) ? Utils.decimalTryParse(txtSalePrice.Text) : 0;
                input.PurchasePrice = !string.IsNullOrEmpty(txtPuchasePrice.Text) ? Utils.decimalTryParse(txtPuchasePrice.Text) : 0;
                input.MinimumStock = !string.IsNullOrEmpty(txtMinimumStock.Text) ? Utils.decimalTryParse(txtMinimumStock.Text) : 0;
                input.SalePrice2 = FSalePice.salePrice2;
                input.SalePrice3 = FSalePice.salePrice3;
                input.FixedSalePrice = FSalePice.fixedSalePrice;
                input.SalePriceAfterTax = FSalePice.salePriceAfterTax;
                input.SalePriceAfterTax2 = FSalePice.salePriceAfterTax2;
                input.SalePriceAfterTax3 = FSalePice.salePriceAfterTax3;
                MaterialGoodsSpecialTaxGroup temp5 = (MaterialGoodsSpecialTaxGroup)Utils.getSelectCbbItem(ultraCombo6);
                if (temp5 == null) input.MaterialGoodsGSTID = null;
                else input.MaterialGoodsGSTID = temp5.ID;
                //if (input.MaterialGoodsAssemblys.Count > 0)
                //{
                //    foreach (MaterialGoodsAssembly mGAssembly in input.MaterialGoodsAssemblys)
                //    {
                //        if (mGAssembly.MaterialGoodsID == null || mGAssembly.ID == Guid.Empty || mGAssembly.MaterialGoodsID == null || mGAssembly.MaterialGoodsID == Guid.Empty)
                //            mGAssembly.ID = Guid.NewGuid();
                //        mGAssembly.MaterialGoodsID = input.ID;
                //        
                //    }
                //}
                input.PrintMetarial = chkPrintMaterial.Checked;
            }
            else
            {
                txtMaterialGoodsCode.Text = input.MaterialGoodsCode;
                txtMaterialGoodsCode.Enabled = false;
                txtMaterialGoodsName.Text = input.MaterialGoodsName;
                cbbMaterialGoodsType.SelectedIndex = input.MaterialGoodsType;
                //tài khoản tổng hợp
                foreach (var item in cbbMaterialGoodCategory.Rows)
                {
                    if ((item.ListObject as MaterialGoodsCategory).ID == input.MaterialGoodsCategoryID) cbbMaterialGoodCategory.SelectedRow = item;
                }
                //Kho
                foreach (var item in cbbRepository.Rows)
                {
                    if ((item.ListObject as Repository).ID == input.RepositoryID) cbbRepository.SelectedRow = item;
                }
                //nhóm tài khoản
                //tài khoản Kho
                foreach (var item in cbbReponsitoryAccount.Rows)
                {
                    if ((item.ListObject as Account).AccountNumber == input.ReponsitoryAccount) 
                        cbbReponsitoryAccount.SelectedRow = item;
                    var gt = cbbReponsitoryAccount.SelectedRow;
                }

                //tài khoản doanh thu
                foreach (var item in cbbRevenueAccount.Rows)
                {
                    if ((item.ListObject as Account).AccountNumber == input.RevenueAccount) cbbRevenueAccount.SelectedRow = item;
                }
                //nhóm tài khoản
                //tài khoản chi phí
                foreach (var item in cbbExpenseAccount.Rows)
                {
                    if ((item.ListObject as Account).AccountNumber == input.ExpenseAccount) cbbExpenseAccount.SelectedRow = item;
                }
                
                if (!input.WarrantyTime.IsNullOrEmpty())
                {
                    cbbWarrantyTime.Text = input.WarrantyTime;
                    if (lstWarranty.Any(n => n.WarrantyTime.ToString() == input.WarrantyTime))
                    {
                        cbbWarrantyTime.Text = lstWarranty.FirstOrDefault(n => n.WarrantyTime.ToString() == input.WarrantyTime).WarrantyName;
                    }
                }
                if (Utils.ListSystemOption.FirstOrDefault(x => x.Code == "PPTTGTGT").Data == "Phương pháp trực tiếp trên doanh thu")
                {
                    foreach (var item in cbbCareerGroup.Rows)
                    {
                        if ((item.ListObject as CareerGroup).ID == (input.CareerGroupID)) cbbCareerGroup.SelectedRow = item;
                    }
                }


                foreach (var item in cbbTaxRate.Items)
                {
                    if (Convert.ToDecimal(item.DataValue) == input.TaxRate) cbbTaxRate.SelectedItem = item;
                }

                if (input.MaterialToolType == 0)
                {
                    chkMaterialToolType.Checked = false;
                }
                else
                {
                    chkMaterialToolType.Checked = true;
                }
                chkActive.Checked = input.IsActive;
                chkIsSaleDiscountPolicy.CheckState = input.IsSaleDiscountPolicy ? CheckState.Checked : CheckState.Unchecked;
                txtConvertRate.Value = input.ConvertRate;
                txtUnit.Text = input.Unit;
                txtConvertUnit.Text = input.ConvertUnit;
                txtPuchasePrice.Value = input.PurchasePrice;
                if (input.MaterialGoodsType == 1)
                {
                    //BindingList<MaterialGoodsAssembly> bdMaterialGoods = new BindingList<MaterialGoodsAssembly>(_IMaterialGoodsAssemblyService.Query.Where(p => p.MaterialGoodsID == input.ID).ToList());
                    BindingList<MaterialGoodsAssembly> bdMaterialGoods = new BindingList<MaterialGoodsAssembly>(input.MaterialGoodsAssemblys); //thành duy đã sửa chỗ này
                    var source = (BindingList<MaterialGoodsAssembly>)uGrid.DataSource;
                    foreach (var item in bdMaterialGoods)
                    {
                        source.Add(new MaterialGoodsAssembly
                        {
                            ID = item.ID,
                            MaterialAssemblyID = item.MaterialAssemblyID,
                            MaterialAssemblyDescription = item.MaterialAssemblyDescription,
                            Quantity = item.Quantity,
                            UnitPrice = item.UnitPrice,
                            Unit = item.Unit,
                            UnitConvert = item.UnitConvert,
                            TotalMoney = item.TotalMoney

                        });
                    }
                    ConfigGrid(uGrid);
                }
                txtItemSource.Text = input.ItemSource;
                //txtPurchaseDescription.Text = input.PurchaseDescription;
                //txtSaleDescription.Text = input.SaleDescription;
                txtPuchaseDiscountRate.Value = input.PurchaseDiscountRate;
                txtSaleDiscountRate.Value = input.SaleDiscountRate;
                txtSalePrice.Value = input.SalePrice;
                txtMinimumStock.Value = input.MinimumStock;
                txtSalePrice.Value = input.SalePrice;
                chkPrintMaterial.Checked = input.PrintMetarial;

                foreach (var item in ultraCombo6.Rows)
                {
                    if ((item.ListObject as MaterialGoodsSpecialTaxGroup).ID == input.MaterialGoodsGSTID) ultraCombo6.SelectedRow = item;
                }

            }
            return input;
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion
            if (lsRowHasNotificationCell.Count() > 0)
            {
                MSG.Warning("Có lỗi xảy ra khi lưu Vật tư, hàng hóa.");
                return;
            }
            if (ultraTabControl1.Tabs[1].Visible == true)
                if (uGrid.DataSource != null)
                    if (((BindingList<MaterialGoodsAssembly>)uGrid.DataSource).ToList().Count == 0)
                    {
                        MSG.Warning("Bạn chưa nhập chi tiết VTHH");
                        return;
                    }

            //Gui to object
            #region Fill dữ liệu control vào obj
            MaterialGoods temp = Them ? new MaterialGoods() : _IMaterialGoodsService.Getbykey(_Select.ID);
            temp = ObjandGUI(temp, true);
            #endregion

            #region Thao tác CSDL          
            try
            {
                var session = _IMaterialGoodsService.GetSessionState();
                session.Clear();
                session = _IMaterialGoodsService.GetSessionState();
                using (var trans = session.BeginTransaction())
                {
                    //_IMaterialGoodsService.BeginTran();

                    if (Them)
                    {
                        if (!CheckCode()) return;
                        if (cbbMaterialGoodsType.SelectedIndex != 1)
                            temp.MaterialGoodsAssemblys = new List<MaterialGoodsAssembly>();
                        session.Save(temp);
                        //_IMaterialGoodsService.CreateNew(temp);

                        //if (cbbMaterialGoodsType.SelectedIndex == 1)
                        //{
                        //    //List<MaterialGoodsAssembly> MaterialGoodsAssemblyss = _IMaterialGoodsAssemblyService.Query.Where(k => k.MaterialGoodsID == temp.ID).ToList();
                        //    List<MaterialGoodsAssembly> MaterialGoodsAssemblyss = _IMaterialGoodsAssemblyService.GetAll_ByMaterialGoodsID(temp.ID); //thành duy đã sửa chỗ này
                        //    foreach (MaterialGoodsAssembly item in MaterialGoodsAssemblyss) _IMaterialGoodsAssemblyService.Delete(item);
                        //    foreach (MaterialGoodsAssembly item in temp.MaterialGoodsAssemblys) _IMaterialGoodsAssemblyService.Save(item);
                        //}

                        //List<SaleDiscountPolicy> SaleDiscountPolicy = _ISaleDiscountPolicyService.Query.Where(k => k.MaterialGoodsID == temp.ID).ToList();
                        //List<SaleDiscountPolicy> SaleDiscountPolicy = _ISaleDiscountPolicyService.GetAll_ByMaterialGoodsID(temp.ID); //thành duy đã sửa chỗ này
                        //foreach (SaleDiscountPolicy item in SaleDiscountPolicy) _ISaleDiscountPolicyService.Delete(item);
                        //foreach (SaleDiscountPolicy item in temp.SaleDiscountPolicys) _ISaleDiscountPolicyService.Save(item);

                    }
                    else
                    {
                        if (cbbMaterialGoodsType.SelectedIndex != 1)
                            temp.MaterialGoodsAssemblys.Clear();
                        //_IMaterialGoodsService.Update(temp);
                        session.Update(temp);
                        //if (cbbMaterialGoodsType.SelectedIndex == 1) //cùng một hàm được sử dụng 2 lần
                        //{
                        //    //List<MaterialGoodsAssembly> MaterialGoodsAssemblyss = _IMaterialGoodsAssemblyService.Query.Where(k => k.MaterialGoodsID == temp.ID).ToList();
                        //    List<MaterialGoodsAssembly> MaterialGoodsAssemblyss = _IMaterialGoodsAssemblyService.GetAll_ByMaterialGoodsID(temp.ID); //thành duy đã sửa chỗ này
                        //    //foreach (MaterialGoodsAssembly item in MaterialGoodsAssemblyss) _IMaterialGoodsAssemblyService.Delete(item);
                        //    foreach (MaterialGoodsAssembly item in temp.MaterialGoodsAssemblys) _IMaterialGoodsAssemblyService.Save(item);
                        //}
                        //else
                        //{
                        //    //List<MaterialGoodsAssembly> MaterialGoodsAssemblyss = _IMaterialGoodsAssemblyService.Query.Where(k => k.MaterialGoodsID == temp.ID).ToList();
                        //    List<MaterialGoodsAssembly> MaterialGoodsAssemblyss = _IMaterialGoodsAssemblyService.GetAll_ByMaterialGoodsID(temp.ID); //thành duy đã sửa chỗ này
                        //    foreach (MaterialGoodsAssembly item in MaterialGoodsAssemblyss) _IMaterialGoodsAssemblyService.Delete(item);
                        //}
                        ////List<SaleDiscountPolicy> SaleDiscountPolicy = _ISaleDiscountPolicyService.Query.Where(k => k.MaterialGoodsID == temp.ID).ToList();
                        //// cùng một hàm được sử dụng 2 lần, đúng sai đều được sử dụng. Sử dụng trước câu lệnh if.
                        //if (chkIsSaleDiscountPolicy.CheckState == CheckState.Checked)
                        //{
                        //    //List<SaleDiscountPolicy> SaleDiscountPolicy = _ISaleDiscountPolicyService.Query.Where(k => k.MaterialGoodsID == temp.ID).ToList();
                        //    List<SaleDiscountPolicy> SaleDiscountPolicy = _ISaleDiscountPolicyService.GetAll_ByMaterialGoodsID(temp.ID);//thành duy đã sửa chỗ này
                        //    //foreach (SaleDiscountPolicy item in SaleDiscountPolicy) _ISaleDiscountPolicyService.Delete(item);
                        //    foreach (SaleDiscountPolicy item in temp.SaleDiscountPolicys) _ISaleDiscountPolicyService.Save(item);
                        //}
                        //else
                        //{
                        //    //List<SaleDiscountPolicy> SaleDiscountPolicy = _ISaleDiscountPolicyService.Query.Where(k => k.MaterialGoodsID == temp.ID).ToList();
                        //    List<SaleDiscountPolicy> SaleDiscountPolicy = _ISaleDiscountPolicyService.GetAll_ByMaterialGoodsID(temp.ID);//thành duy đã sửa chỗ này
                        //    foreach (SaleDiscountPolicy item in SaleDiscountPolicy) _ISaleDiscountPolicyService.Delete(item);
                        //}

                    }
                    trans.Commit();
                    Utils.ClearCacheByType<MaterialGoods>();
                    BindingList<MaterialGoods> lst = Utils.ListMaterialGoods;
                    Post_Product(temp); //add by Hautv
                }

            }
            catch (Exception ex)
            {
                _IMaterialGoodsService.RolbackTran();
                MSG.Warning("Có lỗi xảy ra khi lưu Vật tư, hàng hóa.");
                return;
            }

            #endregion
            MaterialGoodsCode = txtMaterialGoodsCode.Text;

            #region xử lý form, kết thúc form
            Id = temp.ID;

            //Utils.ClearCacheByType<MaterialGoods>();
            //BindingList<MaterialGoods> list = Utils.ListMaterialGoods;
            //_IMaterialGoodsService.UnbindSession(list);
            //Utils.ClearCacheByType<MaterialGoodsCustom>();
            //BindingList<MaterialGoodsCustom> list1 = Utils.ListMaterialGoodsCustom;
            this.Close();
            isClose = false;
            #endregion
        }
        private void btnSaveContinue_Click(object sender, EventArgs e)
        {
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion
            if (lsRowHasNotificationCell.Count() > 0)
            {
                MSG.Warning("Có lỗi xảy ra khi lưu Vật tư, hàng hóa.");
                return;
            }
            if (ultraTabControl1.Tabs[1].Visible == true)
                if (uGrid.DataSource != null)
                    if (((BindingList<MaterialGoodsAssembly>)uGrid.DataSource).ToList().Count == 0)
                    {
                        MSG.Warning("Bạn chưa nhập chi tiết VTHH");
                        return;
                    }
            //Gui to object
            #region Fill dữ liệu control vào obj
            MaterialGoods temp = Them ? new MaterialGoods() : _IMaterialGoodsService.Getbykey(_Select.ID);
            temp = ObjandGUI(temp, true);
            #endregion

            #region Thao tác CSDL
            try
            {
                //_IMaterialGoodsService.BeginTran();
                //if (Them)
                //{
                //    if (!CheckCode()) return;
                //    if (cbbMaterialGoodsType.SelectedIndex != 1)
                //        temp.MaterialGoodsAssemblys = new List<MaterialGoodsAssembly>();
                //    _IMaterialGoodsService.CreateNew(temp);
                //}
                //else
                //{
                //    if (cbbMaterialGoodsType.SelectedIndex != 1)
                //        temp.MaterialGoodsAssemblys.Clear();
                //    _IMaterialGoodsService.Update(temp);
                //}
                //_IMaterialGoodsService.CommitTran();
                var session = _IMaterialGoodsService.GetSessionState();
                session.Clear();
                session = _IMaterialGoodsService.GetSessionState();
                using (var trans = session.BeginTransaction())
                {
                    if (Them)
                    {
                        if (!CheckCode()) return;
                        if (cbbMaterialGoodsType.SelectedIndex != 1)
                            temp.MaterialGoodsAssemblys = new List<MaterialGoodsAssembly>();
                        session.Save(temp);
                    }
                    else
                    {
                        if (cbbMaterialGoodsType.SelectedIndex != 1)
                            temp.MaterialGoodsAssemblys.Clear();
                        session.Update(temp);
                    }
                    trans.Commit();
                }
                _forceReload = true;
                Post_Product(temp); //add by Hautv
            }
            catch (Exception)
            {
                //_IMaterialGoodsService.RolbackTran();
                MSG.Warning("Có lỗi xảy ra khi lưu Vật tư, hàng hóa.");
                return;
            }
            #endregion
            MaterialGoodsCode = txtMaterialGoodsCode.Text;

            #region xử lý form, reset form
            Id = temp.ID;

            Utils.ClearCacheByType<MaterialGoods>();
            BindingList<MaterialGoods> list = Utils.ListMaterialGoods;
            Utils.ClearCacheByType<MaterialGoodsCustom>();
            BindingList<MaterialGoodsCustom> list1 = Utils.ListMaterialGoodsCustom;
            isClose = false;

            // reset form
            txtMaterialGoodsCode.Focus();
            _Select = new MaterialGoods();
            ObjandGUI(_Select, false);
            SetDefaultValueControls();
            Them = true;
            #endregion
            
        }

        private void SetDefaultValueControls()
        {
            chkActive.Checked = true;
            txtMaterialGoodsCode.Enabled = true;
            //cbbMaterialGoodsType.Text = cbbMaterialGoodsType.NullText;comment by cuongpv
            cbbMaterialGoodsType.SelectedIndex = 0;//add by cuongpv
            cbbMaterialGoodCategory.Text = cbbMaterialGoodCategory.NullText;
            cbbWarrantyTime.Text = cbbWarrantyTime.NullText;
            // cbbAccountingObjectID.Text = cbbAccountingObjectID.NullText;
            cbbRepository.Text = cbbRepository.NullText;
            cbbExpenseAccount.Text = cbbExpenseAccount.NullText;
            ultraCombo6.Text = ultraCombo6.NullText;
            cbbTaxRate.Text = cbbTaxRate.NullText;
            cbbReponsitoryAccount.Text = cbbReponsitoryAccount.NullText;
            cbbRevenueAccount.Text = cbbRevenueAccount.NullText;
        }

        #region xử lý khi chọn Chính sách CK
        private void ultraCheckEditor2_CheckedChanged(object sender, EventArgs e)
        {
            if (chkIsSaleDiscountPolicy.CheckState == CheckState.Checked)
            {
                btnDiscount.Enabled = true;
            }
            else
            {
                btnDiscount.Enabled = false;
            }
        }
        private void btnDiscount_Click(object sender, EventArgs e)
        {
            //if (Them)
            //{
            //    var f = new FSaleDiscountPolicy();
            //    if (f.ShowDialog(this) == DialogResult.OK)
            //    {
            //        List<SaleDiscountPolicy> uSale = f.lstSaleDiscountPolicy.ToList();
            //        foreach (SaleDiscountPolicy item in uSale)
            //        {
            //            item.MaterialGoodsIDVIEW = "";
            //            item.MaterialGoodsID = _Select.ID;
            //        }
            //        _Select.SaleDiscountPolicys.Clear();
            //        _Select.SaleDiscountPolicys = uSale;
            //    }
            //    //new FSaleDiscountPolicy(_Select.SaleDiscountPolicys).ShowDialog(this);
            //    //_Select.SaleDiscountPolicys = (IList<SaleDiscountPolicy>)Utils.CloneObject(FSaleDiscountPolicy.dsSaleDiscount);
            //}
            //else
            //{

            var f = new FSaleDiscountPolicy(saleDiscountPolicies);

            if (f.ShowDialog(this) == DialogResult.OK)
            {
                //List<SaleDiscountPolicy> uSale = f.lstSaleDiscountPolicy.ToList();
                //foreach (SaleDiscountPolicy item in uSale)
                //{
                //    item.MaterialGoodsIDVIEW = "";
                //    item.MaterialGoodsID = _Select.ID;
                //    item.ID = Guid.Empty;
                //}
                //_Select.SaleDiscountPolicys = uSale;
            }

            foreach (var item in saleDiscountPolicies.Where(n => n.DiscountResult == null && n.DiscountType == null && n.MaterialGoodsIDVIEW == null && n.QuantityFrom == null && n.QuantityTo == null).ToList())
            {
                saleDiscountPolicies.Remove(item);
            }

            //new FSaleDiscountPolicy(_ISaleDiscountPolicyService.GetAll_ByMaterialGoodsID(_Select.ID)).ShowDialog(this); //thành duy đã sửa chỗ này
            //_Select.SaleDiscountPolicys = (IList<SaleDiscountPolicy>)Utils.CloneObject(FSaleDiscountPolicy.dsSaleDiscount);
            //}
        }
        #endregion

        #region form load động khi thay đổi item trong combo
        private void cbbMaterialGoodsType_ValueChanged(object sender, EventArgs e)
        {
            #region Code cũ
            //if (cbbMaterialGoodsType.SelectedIndex == 0 || cbbMaterialGoodsType.SelectedIndex == 2 || cbbMaterialGoodsType.SelectedIndex == 3 || cbbMaterialGoodsType.SelectedIndex == 4)
            //{
            //    this.Size = new Size(691, 534);
            //    ultraGroupBox3.Hide();
            //    ultraGroupBox2.Location = new Point(12, 179);
            //    chkActive.Location = new Point(18, 462);
            //    btnSaveContinue.Location = new Point(376, 457);
            //    btnSave.Location = new Point(517, 457);
            //    btnClose.Location = new Point(598, 457);
            //}
            //if (cbbMaterialGoodsType.SelectedIndex == 1)
            //{
            //    this.Size = new Size(691, 695);
            //    ultraGroupBox3.Show();
            //    ultraGroupBox2.Location = new Point(12, 346);
            //    ultraGroupBox3.Location = new Point(12, 179);
            //    chkActive.Location = new Point(18, 629);
            //    btnSaveContinue.Location = new Point(376, 624);
            //    btnSave.Location = new Point(517, 624);
            //    btnClose.Location = new Point(598, 624);
            //    LoadDuLieu();
            //}
            //else
            //{
            //    this.Size = new Size(691, 534);
            //    ultraGroupBox3.Hide();
            //    ultraGroupBox2.Location = new Point(12, 179);
            //    chkActive.Location = new Point(18, 462);
            //    btnSaveContinue.Location = new Point(376, 457);
            //    btnSave.Location = new Point(517, 457);
            //    btnClose.Location = new Point(598, 457);
            //}
            #endregion
            #region Code mới
            if (cbbMaterialGoodsType.SelectedIndex == 1)
            {
                ultraTabControl1.Tabs[1].Visible = true;
                //ultraGroupBox3.Show();
                //ultraGroupBox4.Show();
                //ultraGroupBox2.Location = new Point(ultraGroupBox3.Location.X, ultraGroupBox3.Location.Y + ultraGroupBox3.Height + 6);
                //ultraGroupBox4.Location = new Point(ultraGroupBox2.Location.X, ultraGroupBox2.Location.Y + ultraGroupBox2.Height + 6);
                //this.Size = new Size(40 + ultraGroupBox1.Width, ultraGroupBox1.Height + ultraGroupBox2.Height + ultraGroupBox3.Height + ultraGroupBox4.Height);
                LoadDuLieu();
            }
            else
            {
                ultraTabControl1.Tabs[1].Visible = false;
                //ultraGroupBox3.Hide();
                //ultraGroupBox4.Show();
                //ultraGroupBox2.Location = new Point(ultraGroupBox1.Location.X, ultraGroupBox1.Location.Y + ultraGroupBox1.Height + 6);
                //ultraGroupBox4.Location = new Point(ultraGroupBox2.Location.X, ultraGroupBox2.Location.Y + ultraGroupBox2.Height + 6);
                //this.Size = new Size(40 + ultraGroupBox1.Width, ultraGroupBox1.Height + ultraGroupBox2.Height + ultraGroupBox4.Height);
            }
            #endregion

            if (cbbMaterialGoodsType.SelectedIndex == 2 || cbbMaterialGoodsType.SelectedIndex == 4)
            {
                chkIsSecurity.Enabled = false;

            }
            else
            {
                chkIsSecurity.Enabled = true;
                txtMinimumStock.AllowDrop = false;
            }
        }
        #endregion
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams handleParam = base.CreateParams;
                handleParam.ExStyle |= 0x02000000;   // WS_EX_COMPOSITED       
                return handleParam;
            }
        }
        void CreaterColumsStyle(Infragistics.Win.UltraWinGrid.UltraGrid uGrid)
        {
            #region Grid động
            Infragistics.Win.UltraWinGrid.UltraGridBand band = uGrid.DisplayLayout.Bands[0];


            foreach (var item in band.Columns)
            {
                this.ConfigEachColumn4Grid(0, item, uGrid);
                //if (item.Key.Equals("MaterialAssemblyID"))
                //{
                //    //item.Editor = GetEmbeddableEditorBase(Controls, _IMaterialGoodsService.GetAll().Where(p => p.ID != _Select.ID).ToList(),
                //    //                                                           "ID", "MaterialGoodsCode",
                //    //                                                           ConstDatabase.MaterialGoods_TableName);
                //    item.Editor = GetEmbeddableEditorBase(Controls, _IMaterialGoodsService.GetAll_NotValueByID(_Select.ID),
                //                                                               "ID", "MaterialGoodsCode",
                //                                                               ConstDatabase.MaterialGoods_TableName); //Thành Duy đã sửa chỗ này
                //}
                //else
                //    if (item.Key.Equals("MaterialAssemblyDescription"))
                //    {
                //        item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Edit;
                //    }
                //else 
                //if (item.Key.Equals("ConvertRate"))
                //{
                //    //item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
                //    //item.MaskInput = " -nn,nnn,nnn,nnn,nnn.nn";
                //    item.DefaultCellValue = "1";
                //    //item.CellAppearance.TextHAlign = HAlign.Right;
                //}
                //else if (item.Key.Equals("UnitPrice"))
                //{
                //    //item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
                //    //item.MaskInput = " -nn,nnn,nnn,nnn,nnn.nn";
                //    item.DefaultCellValue = "0";
                //    //item.CellAppearance.TextHAlign = HAlign.Right;
                //}
                //else if (item.Key.Equals("Quantity"))
                //{
                //    //item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
                //    //item.MaskInput = " -nn,nnn,nnn,nnn,nnn.nn";
                //    item.DefaultCellValue = "1";
                //    //item.CellAppearance.TextHAlign = HAlign.Right;
                //}
                //    else if (item.Key.Equals("QuantityConvert"))
                //    {
                //        item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
                //        item.MaskInput = " -nn,nnn,nnn,nnn,nnn.nn";
                //        item.CellAppearance.TextHAlign = HAlign.Right;
                //    }
                //    else if (item.Key.Equals("UnitPriceConvert"))
                //    {
                //        item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
                //        item.MaskInput = " -nn,nnn,nnn,nnn,nnn.nn";
                //        item.CellAppearance.TextHAlign = HAlign.Right;
                //    }
                //    else if (item.Key.Equals("TotalMoney"))
                //    {
                //        item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
                //        item.MaskInput = " -nn,nnn,nnn,nnn,nnn,nnn,nnn,nnn,nnn";
                //        item.CellAppearance.TextHAlign = HAlign.Right;
                //    }
                //    else if (item.Key.Equals("Unit"))
                //    {
                //        item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Edit;

                //    }
                //else if (item.Key.Equals("ConvertUnit"))
                //{
                //    item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Edit;

                //}
            }
            #endregion
        }

        /// <summary>
        /// Tạo một Editor
        /// </summary>
        /// <typeparam name="T">Kiểu dữ liệu</typeparam>
        /// <param name="_this">Form</param>
        /// <param name="inputItem">Danh sách dữ liệu</param>
        /// <param name="valueMember">valueMember</param>
        /// <param name="displayMember">displayMember</param>
        /// <param name="nameTable">Tên bảng csdl</param>
        /// <returns></returns>
        private static EmbeddableEditorBase GetEmbeddableEditorBase<T>(Control.ControlCollection _this, List<T> inputItem, string valueMember, string displayMember, string nameTable)
        {

            Infragistics.Win.UltraWinGrid.UltraDropDown dropDown = new Infragistics.Win.UltraWinGrid.UltraDropDown
            {
                Name = "dropDown1",
                Visible = false,
                DataSource = inputItem,
                ValueMember = valueMember,
                DisplayMember = displayMember
            };
            _this.Add(dropDown);
            Utils.ConfigGrid(dropDown, nameTable);
            EmbeddableEditorBase editor = new EditorWithCombo(new DefaultEditorOwner(new DefaultEditorOwnerSettings { ValueList = dropDown, DataType = typeof(int) }));
            return editor;
        }

        bool thaotac = true;
        List<UltraGridRow> lsRowHasNotificationCell = new List<UltraGridRow>();
        UltraGridRow indexRow;
        #region sự kiện khi thay đổi dữ liệu trên Grid con
        private void uGrid_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            UltraGridCell cell = uGrid.ActiveCell;
            if (cell == null) return;
            if (cell.Value == null) return;
            //if (!thaotac) return;
            if (e.Cell.Column.Key.Equals("MaterialAssemblyID"))
            {
                //Infragistics.Win.UltraWinGrid.UltraDropDown test = (Infragistics.Win.UltraWinGrid.UltraDropDown)Controls["dropDown1"];
                var mGId = (Guid?)e.Cell.Value;
                if (mGId.HasValue)
                {
                    List<MaterialGoodsAssembly> uMaterialGoodsAssembly = ((BindingList<MaterialGoodsAssembly>)uGrid.DataSource).ToList();
                    MaterialGoods select = Utils.ListMaterialGoodsCustom.FirstOrDefault(m => m.ID == mGId.Value);
                    int count_ = uMaterialGoodsAssembly.Count(x => x.MaterialAssemblyID == select.ID);
                    int count = Utils.ListMaterialGoods.Count(x => x.MaterialGoodsCode == cell.Text);
                    bool T = true;
                    if (count == 0)
                    {
                        MSG.Error("Dữ liệu không có trong danh mục");

                        if (lsRowHasNotificationCell.Count() > 0)
                        {
                            foreach (UltraGridRow row in lsRowHasNotificationCell)
                            {
                                if (row.Equals(cell.Row)) T = false;
                            }
                            if (T) lsRowHasNotificationCell.Add(cell.Row);
                        }
                        else
                        {
                            lsRowHasNotificationCell.Add(cell.Row);
                        }
                        return;
                    }
                    if (count_ > 1)
                    {
                        MSG.Error("Vật tư, hàng hóa đã được chọn vui lòng chọn lại.");
                        Utils.NotificationCell(uGrid, cell, "Vật tư, hàng hóa đã được chọn vui lòng chọn lại.");

                        if (lsRowHasNotificationCell.Count() > 0)
                        {
                            foreach (UltraGridRow row in lsRowHasNotificationCell)
                            {
                                if (row.Equals(cell.Row)) T = false;
                            }
                            if (T) lsRowHasNotificationCell.Add(cell.Row);
                        }
                        else
                        {
                            lsRowHasNotificationCell.Add(cell.Row);
                        }
                        return;
                    }
                    foreach (UltraGridRow row in lsRowHasNotificationCell)
                    {
                        if (row.Equals(cell.Row))
                        {
                            T = false;
                            indexRow = row;
                        }
                    }
                    if (!T) lsRowHasNotificationCell.Remove(indexRow);
                    thaotac = false;
                    Utils.RemoveNotificationCell(uGrid, cell);

                    e.Cell.Row.Cells["MaterialAssemblyDescription"].Value = select.MaterialGoodsName;
                    e.Cell.Row.Cells["Unit"].Value = select.Unit;
                    e.Cell.Row.Cells["UnitConvert"].Value = select.ConvertUnit;
                    e.Cell.Row.Cells["UnitPrice"].Value = select.PurchasePrice;
                    if (select.ConvertRate == 0)
                    {
                        e.Cell.Row.Cells["ConvertRate"].Value = (decimal)1;
                    }
                    else
                    {
                        e.Cell.Row.Cells["ConvertRate"].Value = select.ConvertRate;
                    }
                    thaotac = true;
                }
                ////decimal str1 = select.ConvertRate;
                //MessageBox.Show(decimal.Parse(e.Cell.Row.Cells["Quantity"].Text) + "");
                ////
            }

            if (e.Cell.Column.Key.Equals("ConvertRate") || e.Cell.Column.Key.Equals("Quantity") || e.Cell.Column.Key.Equals("UnitPrice"))
            {
                thaotac = false;
                //Infragistics.Win.UltraWinGrid.UltraDropDown test = (Infragistics.Win.UltraWinGrid.UltraDropDown)Controls["dropDown1"];
                //MaterialGoods select = (MaterialGoods)(test.SelectedRow == null ? test.SelectedRow : test.SelectedRow.ListObject);
                MaterialGoodsAssembly temp = e.Cell.Row.ListObject as MaterialGoodsAssembly;
                //e.Cell.Row.Cells["Slchuyendoi"].Value = (decimal.Parse(e.Cell.Row.Cells["ConvertRate"].Text)) * (decimal.Parse(e.Cell.Row.Cells["Quantity"].Text));
                //e.Cell.Row.Cells["QuantityConvert"].Value = temp.Quantity * temp.ConvertRate;
                e.Cell.Row.Cells["QuantityConvert"].Value = ((decimal)e.Cell.Row.Cells["Quantity"].Value) * (e.Cell.Row.Cells["ConvertRate"].Value != null ? (decimal)e.Cell.Row.Cells["ConvertRate"].Value : (decimal)0);
                e.Cell.Row.Cells["UnitPriceConvert"].Value = (e.Cell.Row.Cells["UnitPrice"].Value == null ? (decimal)0 : (decimal)e.Cell.Row.Cells["UnitPrice"].Value) / (e.Cell.Row.Cells["ConvertRate"].Value == null ? (decimal)1 : (decimal)e.Cell.Row.Cells["ConvertRate"].Value);
                e.Cell.Row.Cells["TotalMoney"].Value = (e.Cell.Row.Cells["UnitPrice"].Value == null ? (decimal)0 : (decimal)e.Cell.Row.Cells["UnitPrice"].Value) * ((decimal)e.Cell.Row.Cells["Quantity"].Value);
                thaotac = true;
            }
            if (e.Cell.Column.Key.Equals("TotalMoney"))
            {
                if ((decimal)e.Cell.Row.Cells["Quantity"].Value != 0)
                    e.Cell.Row.Cells["UnitPrice"].Value = (e.Cell.Row.Cells["TotalMoney"].Value == null ? (decimal)0 : (decimal)e.Cell.Row.Cells["TotalMoney"].Value) / ((decimal)e.Cell.Row.Cells["Quantity"].Value);
            }
        }

        #endregion

        #region cấu hình column Grid trong Detail
        List<TemplateColumn> getTemplates()
        {
            List<string> strColumnName, strColumnCaption, strColumnToolTip = new List<string>();
            List<bool> bolIsReadOnly, bolIsVisible, bolIsVisibleCbb = new List<bool>();
            List<int> intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition = new List<int>();
            List<int> VTbolIsVisible, VTbolIsVisibleCbb = new List<int>();

            #region MaterialGoods
            strColumnName = new List<string>() { "ID", "MaterialGoodsCategoryID", "MaterialGoodsCode", "MaterialGoodsName", "MaterialGoodsCategoryIDVIEW", "SltonView", "GttonView", "MaterialGoodsType", "MaterialToolType", "Unit", "ConvertUnit", "Quantity", "Slchuyendoi", "Dgchuyendoi", "ConvertRate", "PurchasePrice", "SalePrice", "FixedSalePrice", "RepositoryID", "ReponsitoryAccount", "ExpenseAccount", "RevenueAccount", "MinimumStock", "AccountingObjectID", "TaxRate", "SystemMaterialGoodsType", "SaleDescription", "PurchaseDescription", "ItemSource", "MaterialGoodsGSTID", "SaleDiscountRate", "PurchaseDiscountRate", "IsSaleDiscountPolicy", "GuarantyPeriod", "CostMethod", "IsActive", "IsSecurity", "PrintMetarial" };
            strColumnCaption = strColumnToolTip = new List<string>() { "ID", "MaterialGoodsCategoryID", "Mã VTHH", "Diễn giải", "Loại vật tư ,hàng hoá", "Số lượng tồn", "Giá trị tồn", "MaterialGoodsType", "MaterialToolType", "Đơn vị tính", "Đơn vị chuyển đổi", "Số lượng", "SL chuyển đổi", "Đơn giá chuyển đổi", "Tỉ lệ chuyển đổi", "Đơn giá", "SalePrice", "FixedSalePrice", "RepositoryID", "RepositoryAccount", "ExpenseAccount", "RevenueAccount", "MinimumStock", "AccountingObjectID", "TaxRate", "SystemMaterialGoodsType", "SaleDescription", "PurchaseDescription", "ItemSource", "MaterialGoodsGSTID", "SaleDiscountRate", "PurchaseDiscountRate", "IsSaleDiscountPolicy", "GuarantyPeriod", "CostMethod", "Ngừng theo dõi", "IsSecurity", "PrintMetarial" };
            bolIsReadOnly = new List<bool>();
            bolIsVisible = new List<bool>();
            bolIsVisibleCbb = new List<bool>();
            intColumnWidth = new List<int>();
            intColumnMaxWidth = new List<int>();
            intColumnMinWidth = new List<int>();
            intVisiblePosition = new List<int>();
            VTbolIsVisible = new List<int>() { 2, 3, 9, 10, 11, 12, 13, 14, 15 };    //vị trí có giá trị bằng true
            VTbolIsVisibleCbb = new List<int>() { 2, 3 };
            for (int i = 0; i < 38; i++)
            {
                if (VTbolIsVisible.Contains(i)) bolIsVisible.Add(true); else bolIsVisible.Add(false);
                if (VTbolIsVisibleCbb.Contains(i)) bolIsVisibleCbb.Add(true); else bolIsVisibleCbb.Add(false);
                bolIsReadOnly.Add(false);
                intColumnWidth.Add(-1);
                intColumnMaxWidth.Add(-1);
                intColumnMinWidth.Add(-1);
                intVisiblePosition.Add(-1);
            }
            return ConstDatabase.CreateData(strColumnName, strColumnCaption, strColumnToolTip, bolIsReadOnly, bolIsVisible, bolIsVisibleCbb, intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition).Values.ToList();
            #endregion

        }
        #endregion

        #region các hàm check lỗi
        bool CheckError()
        {
            bool kq = true;
            //Check Error Chung

            //Check Error Riêng từng Form
            if (string.IsNullOrEmpty(txtMaterialGoodsCode.Text) || string.IsNullOrEmpty(txtMaterialGoodsName.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }
            return kq;
        }

        bool CheckCode()
        {
            bool kq = true;
            //Check Error Chung
            //List<string> hh = _IMaterialGoodsService.Query.Select(a => a.MaterialGoodsCode).ToList();
            List<string> hh = _IMaterialGoodsService.GetMaterialGoodsCode(); //thành duy đã sửa chỗ này
            foreach (var item in hh)
            {
                if (item.ToUpper().Equals(txtMaterialGoodsCode.Text.ToUpper()))
                {
                    MSG.Warning(string.Format(resSystem.MSG_Catalog_Account6, "Vật tư/Hàng hóa"));
                    return false;
                }
            }

            return kq;
        }
        #endregion

        #region Utils
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, TextMessage.ConstDatabase.MaterialGoodsAssembly_TableName, new List<TemplateColumn>(), true);
            //uGridInput.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            //uGridInput.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.ExtendFirstCell;
            ////Fix Header
            //uGridInput.DisplayLayout.UseFixedHeaders = true;
            //// Turn on all of the Cut, Copy, and Paste functionality. 
            //uGridInput.DisplayLayout.Override.AllowMultiCellOperations = Infragistics.Win.UltraWinGrid.AllowMultiCellOperation.All;


            //Utils.ConfigGrid(uGridInput, tableName, mauGiaoDien.TemplateDetails[0].TemplateColumns, 0);

            //Thêm tổng số ở cột "Số tiền"
            Infragistics.Win.UltraWinGrid.UltraGridBand band = utralGrid.DisplayLayout.Bands[0];
            Infragistics.Win.UltraWinGrid.SummarySettings summary = band.Summaries.Add("TotalMoney", Infragistics.Win.UltraWinGrid.SummaryType.Sum, band.Columns["TotalMoney"]);
            summary.SummaryPosition = Infragistics.Win.UltraWinGrid.SummaryPosition.UseSummaryPositionColumn;
            summary.DisplayFormat = "{0:N0}";
            summary.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.BottomFixed;

        }
        #endregion

        private void cbbAccountingObjectID_EditorButtonClick(object sender, EditorButtonEventArgs e)
        {
            //new FAccountingObjectCustomersDetail().ShowDialog(this);
            ////if(this.cbbAccountingObjectID.Text == null)
            //{
            //    Utils.ClearCacheByType<AccountingObject>();
            //    loadNCC();
            //}
            //cbbAccountingObjectID.SelectedText = FAccountingObjectCustomersDetail.AccounttingObjectCode;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Utils.ClearCacheByType<MaterialGoods>();
            isClose = _forceReload ? false : true;
            DialogResult = DialogResult.Ignore;
            MaterialGoodsCode = null;
            this.Close();
            
        }

        private void cbbRepository_EditorButtonClick(object sender, EditorButtonEventArgs e)
        {
            new FRepositoryDetail().ShowDialog(this);
            loadKho();
            //cbbRepository.SelectedText = FRepositoryDetail.MaKho;
            Utils.ClearCacheByType<Repository>();
            if (FRepositoryDetail.MaKho != null && FRepositoryDetail.MaKho != "")
                cbbRepository.SelectedText = FRepositoryDetail.MaKho;
        }

        private void btnUnitOSales_Click(object sender, EventArgs e)
        {
            if (txtSalePrice.Text == "0")
            {
                new FSalePice(mTaxrate, _Select).ShowDialog(this);
            }
            else
            {
                new FSalePice(decimal.Parse(txtSalePrice.Text), mTaxrate, _Select).ShowDialog(this);
            }
            if (!FSalePice.isClose)
                txtSalePrice.Value = FSalePice.salePrice;
        }

        private void cbbRepository_ItemNotInList(object sender, ValidationErrorEventArgs e)
        {
            Repository b = new Repository();
            if (cbbRepository.Text != b.RepositoryCode && cbbRepository.Text != "")
            {
                MSG.Warning(string.Format(resSystem.MSG_Catalog_FMaterialGoodsDetail5, cbbRepository.Text));
                cbbRepository.Focus();
                return;
            }
        }

        private void cbbAccountingObjectID_ItemNotInList(object sender, ValidationErrorEventArgs e)
        {
            //AccountingObject a = new AccountingObject();

            //if (cbbAccountingObjectID.Text != a.AccountingObjectCode && cbbAccountingObjectID.Text != "")
            //{
            //    MSG.Warning(string.Format(resSystem.MSG_Catalog_FMaterialGoodsDetail, cbbAccountingObjectID.Text));
            //    cbbAccountingObjectID.Focus();
            //    return;
            //}
        }

        private void cbbExpenseAccount_ItemNotInList(object sender, ValidationErrorEventArgs e)
        {
            foreach (string item in accNumber2)
            {
                if (cbbExpenseAccount.Text != item && cbbExpenseAccount.Text != "")
                {
                    MSG.Warning(string.Format(resSystem.MSG_Catalog_FMaterialGoodsDetail1, cbbExpenseAccount.Text));
                    cbbExpenseAccount.Focus();
                    return;
                }
            }

        }

        private void ultraCombo6_ItemNotInList(object sender, ValidationErrorEventArgs e)
        {
            MaterialGoodsSpecialTaxGroup a = new MaterialGoodsSpecialTaxGroup();
            if (ultraCombo6.Text != a.MaterialGoodsSpecialTaxGroupCode && ultraCombo6.Text != "")
            {
                MSG.Warning(string.Format(resSystem.MSG_Catalog_FMaterialGoodsDetail2, ultraCombo6.Text));
                ultraCombo6.Focus();
                return;
            }
        }

        private void cbbReponsitoryAccount_ItemNotInList(object sender, ValidationErrorEventArgs e)
        {
            foreach (string item in accNumber1)
            {
                if (cbbReponsitoryAccount.Text != item && cbbReponsitoryAccount.Text != "")
                {
                    MSG.Warning(string.Format(resSystem.MSG_Catalog_FMaterialGoodsDetail3, cbbReponsitoryAccount.Text));
                    cbbReponsitoryAccount.Focus();
                    return;
                }
            }
        }

        private void cbbRevenueAccount_ItemNotInList(object sender, ValidationErrorEventArgs e)
        {
            foreach (string item in accNumber3)
            {
                if (cbbRevenueAccount.Text != item && cbbRevenueAccount.Text != "")
                {
                    MSG.Warning(string.Format(resSystem.MSG_Catalog_FMaterialGoodsDetail4, cbbRevenueAccount.Text));
                    cbbRevenueAccount.Focus();
                    return;
                }
            }
        }

        private void cbbTaxRate_Leave(object sender, EventArgs e)
        {
            decimal taxrate = 0;

            if (cbbTaxRate.Value != null)
            {
                decimal.TryParse(cbbTaxRate.Value.ToString(), out taxrate);
            }
            mTaxrate = taxrate / 100;
        }

        private void uGrid_BeforeRowActivate(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
            foreach (var item in e.Row.Cells)
            {
                if (item.Column.Key.Equals("ConvertRate"))
                {
                    if (item.Value == null)
                        item.Value = (decimal)1;
                }
                else if (item.Column.Key.Equals("UnitPrice"))
                {
                    if (item.Value == null)
                        item.Value = (decimal)0;
                }
                else if (item.Column.Key.Equals("Quantity"))
                {
                    if (item.Value == null)
                        item.Value = (decimal)1;
                }
                indexRow = e.Row;
            }
        }
        private void uGrid1_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {
            UltraGridCell cell = uGrid.ActiveCell;
            indexRow = cell.Row;
        }
        private void uGrid_Error(object sender, Infragistics.Win.UltraWinGrid.ErrorEventArgs e)
        {
            UltraGridCell cell = uGrid.ActiveCell;
            if (uGrid.ActiveCell == null) return;
            if (cell.Column.Key.Equals("MaterialAssemblyID"))
            {
                int count = Utils.ListMaterialGoods.Count(x => x.MaterialGoodsCode == cell.Text);
                if (count == 0)
                {
                    MSG.Error("Dữ liệu không có trong danh mục");
                }
            }
            e.Cancel = true;
        }
        private void tsmDelete_Click(object sender, EventArgs e)
        {
            bool T = true;
            UltraGridCell cell = uGrid.ActiveCell;
            foreach (UltraGridRow row in lsRowHasNotificationCell)
            {
                if (row.Equals(cell.Row))
                {
                    T = false;
                    indexRow = row;
                }
            }
            if (!T) lsRowHasNotificationCell.Remove(indexRow);
            if (uGrid.ActiveCell != null)
            {
                UltraGridRow row = null;
                if (uGrid.ActiveCell != null || uGrid.ActiveRow != null)
                {

                    row = uGrid.ActiveCell != null ? uGrid.ActiveCell.Row : uGrid.ActiveRow;

                }
                else
                {
                    if (uGrid.Rows.Count > 0) row = uGrid.Rows[uGrid.Rows.Count - 1];
                }
                if (row != null) row.Delete(false);

                uGrid.Update();
            }
        }

        private void tsmAdd_Click(object sender, EventArgs e)
        {
            uGrid.AddNewRow4Grid();
        }

        private void txtMinimumStock_Validated(object sender, EventArgs e)
        {

        }

        private void cbbRepository_RowSelected(object sender, RowSelectedEventArgs e)
        {
            cbbReponsitoryAccount.Value = Utils.GetAccount(cbbRepository.Text);
        }

        private void cbbMaterialGoodCategory_ItemNotInList(object sender, ValidationErrorEventArgs e)
        {

            MSG.Warning("Loại " + cbbMaterialGoodsType.SelectedItem.ToString() + " " + cbbMaterialGoodCategory.Value.ToString() + " không tồn tại! Xin kiểm tra lại!");
            cbbMaterialGoodCategory.Focus();

        }

        private void cbbWarrantyTime_ItemNotInList(object sender, ValidationErrorEventArgs e)
        {

            //MSG.Warning("Thời hạn bảo hành " + cbbWarrantyTime.Value.ToString() + " không tồn tại! Xin kiểm tra lại!");
            //cbbWarrantyTime.Focus();
        }

        private void FMaterialGoodsDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }

        #region Add thông tin sản phẩm lên web hóa đơn điện tử SDS add by Hautv
        private void Post_Product(MaterialGoods materialGoods)
        {
            if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "HDDT").Data != "1") return;
            SystemOption systemOption = Utils.ListSystemOption.FirstOrDefault(n => n.Code == "EI_IDNhaCungCapDichVu");
            if (string.IsNullOrEmpty(systemOption.Data))
            {
                //MSG.Error("Chưa kết nối hóa đơn điện tử");
                return;
            }
            SupplierService supplierService = Utils.ISupplierServiceService.Getbykey(Guid.Parse(systemOption.Data));
            if (supplierService == null) return;
            if (supplierService.SupplierServiceCode == "SDS")
            {
                Products products = new Products();
                Product_If product = new Product_If();
                product.Code = materialGoods.MaterialGoodsCode;
                product.Name = materialGoods.MaterialGoodsName;
                product.Price = materialGoods.SalePrice.ToString().Replace(',', '.');
                product.Unit = materialGoods.Unit;
                product.Des = "";
                product.VATRate = (materialGoods.TaxRate ?? 0).ToString().Replace(',', '.');
                products.Product.Add(product);
                var xmldata = Utils.Serialize<Products>(products);
                var request = new Request();
                request.XmlData = xmldata;
                RestApiService client = new RestApiService(Utils.GetPathAccess(), Utils.GetUserName(), Core.ITripleDes.TripleDesDefaultDecryption(Utils.GetPassWords()));
                Response response = client.Post(supplierService.ApiService.FirstOrDefault(n => n.ApiName == "ThemSuaSP").ApiPath, request);
                if (response.Status != 2)
                {
                    MSG.Error("Lỗi khi cập nhật lên web hóa đơn điện tử: \n" + response.Message);
                }
            }
        }
        #endregion
    }

}

