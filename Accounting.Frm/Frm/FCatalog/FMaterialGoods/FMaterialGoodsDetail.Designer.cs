﻿
namespace Accounting
{
    partial class FMaterialGoodsDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem4 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem5 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem6 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtMaterialGoodsName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtMinimumStock = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtConvertRate = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.cbbMaterialGoodsType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbWarrantyTime = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbbMaterialGoodCategory = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtConvertUnit = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.lblMaterialGoodsName = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.chkMaterialToolType = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.txtUnit = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtMaterialGoodsCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblRegistrationGroupName = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.lblParentID = new Infragistics.Win.Misc.UltraLabel();
            this.lblRegistrationGroupCode = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbCareerGroup = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.btnDiscount = new Infragistics.Win.Misc.UltraButton();
            this.btnUnitOSales = new Infragistics.Win.Misc.UltraButton();
            this.txtSalePrice = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.cbbTaxRate = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.txtSaleDiscountRate = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtPuchaseDiscountRate = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtPuchasePrice = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.cbbRevenueAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbbExpenseAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.chkIsSecurity = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.chkIsSaleDiscountPolicy = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel18 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel19 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbReponsitoryAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel20 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel21 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraCombo6 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtItemSource = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbRepository = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.chkPrintMaterial = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.ultraTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.chkActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.btnSaveContinue = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.warrantyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaterialGoodsName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMaterialGoodsType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbWarrantyTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMaterialGoodCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtConvertUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMaterialToolType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaterialGoodsCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCareerGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbTaxRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbRevenueAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbExpenseAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsSecurity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsSaleDiscountPolicy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbReponsitoryAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCombo6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItemSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbRepository)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintMaterial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).BeginInit();
            this.ultraTabControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).BeginInit();
            this.ultraGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkActive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.warrantyBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.ultraGroupBox1);
            this.ultraTabPageControl1.Controls.Add(this.ultraGroupBox2);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(735, 391);
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox1.Controls.Add(this.txtMaterialGoodsName);
            this.ultraGroupBox1.Controls.Add(this.txtMinimumStock);
            this.ultraGroupBox1.Controls.Add(this.txtConvertRate);
            this.ultraGroupBox1.Controls.Add(this.cbbMaterialGoodsType);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel10);
            this.ultraGroupBox1.Controls.Add(this.cbbWarrantyTime);
            this.ultraGroupBox1.Controls.Add(this.cbbMaterialGoodCategory);
            this.ultraGroupBox1.Controls.Add(this.txtConvertUnit);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel6);
            this.ultraGroupBox1.Controls.Add(this.lblMaterialGoodsName);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel8);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel9);
            this.ultraGroupBox1.Controls.Add(this.chkMaterialToolType);
            this.ultraGroupBox1.Controls.Add(this.txtUnit);
            this.ultraGroupBox1.Controls.Add(this.txtMaterialGoodsCode);
            this.ultraGroupBox1.Controls.Add(this.lblRegistrationGroupName);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox1.Controls.Add(this.lblParentID);
            this.ultraGroupBox1.Controls.Add(this.lblRegistrationGroupCode);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            appearance12.FontData.BoldAsString = "True";
            this.ultraGroupBox1.HeaderAppearance = appearance12;
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(735, 138);
            this.ultraGroupBox1.TabIndex = 100;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtMaterialGoodsName
            // 
            this.txtMaterialGoodsName.AutoSize = false;
            this.txtMaterialGoodsName.Location = new System.Drawing.Point(523, 37);
            this.txtMaterialGoodsName.Name = "txtMaterialGoodsName";
            this.txtMaterialGoodsName.Size = new System.Drawing.Size(205, 22);
            this.txtMaterialGoodsName.TabIndex = 111;
            // 
            // txtMinimumStock
            // 
            appearance1.TextHAlignAsString = "Right";
            this.txtMinimumStock.Appearance = appearance1;
            this.txtMinimumStock.AutoSize = false;
            this.txtMinimumStock.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtMinimumStock.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.txtMinimumStock.InputMask = "{double:10.2}";
            this.txtMinimumStock.Location = new System.Drawing.Point(524, 109);
            this.txtMinimumStock.Name = "txtMinimumStock";
            this.txtMinimumStock.PromptChar = ' ';
            this.txtMinimumStock.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtMinimumStock.Size = new System.Drawing.Size(205, 22);
            this.txtMinimumStock.TabIndex = 110;
            this.txtMinimumStock.Text = "000";
            this.txtMinimumStock.Validated += new System.EventHandler(this.txtMinimumStock_Validated);
            // 
            // txtConvertRate
            // 
            appearance2.TextHAlignAsString = "Right";
            this.txtConvertRate.Appearance = appearance2;
            this.txtConvertRate.AutoSize = false;
            this.txtConvertRate.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludePromptChars;
            this.txtConvertRate.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.txtConvertRate.InputMask = "{LOC}nn,nnn,nnn.nn";
            this.txtConvertRate.Location = new System.Drawing.Point(160, 109);
            this.txtConvertRate.Name = "txtConvertRate";
            this.txtConvertRate.PromptChar = ' ';
            this.txtConvertRate.Size = new System.Drawing.Size(198, 22);
            this.txtConvertRate.TabIndex = 107;
            this.txtConvertRate.Visible = false;
            // 
            // cbbMaterialGoodsType
            // 
            this.cbbMaterialGoodsType.AutoSize = false;
            this.cbbMaterialGoodsType.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbMaterialGoodsType.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            valueListItem1.DataValue = "ValueListItem0";
            valueListItem1.DisplayText = "Vật tư hàng hoá";
            valueListItem2.DataValue = "ValueListItem1";
            valueListItem2.DisplayText = "VTHH lắp ráp/tháo dỡ";
            valueListItem4.DataValue = "ValueListItem2";
            valueListItem4.DisplayText = "Dịch vụ";
            valueListItem5.DataValue = "ValueListItem3";
            valueListItem5.DisplayText = "Thành phẩm";
            valueListItem6.DataValue = "ValueListItem4";
            valueListItem6.DisplayText = "Chỉ là diễn giải";
            valueListItem3.DataValue = "Khác";
            valueListItem3.DisplayText = "Khác";
            this.cbbMaterialGoodsType.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2,
            valueListItem4,
            valueListItem5,
            valueListItem6,
            valueListItem3});
            this.cbbMaterialGoodsType.LimitToList = true;
            this.cbbMaterialGoodsType.Location = new System.Drawing.Point(160, 61);
            this.cbbMaterialGoodsType.Name = "cbbMaterialGoodsType";
            this.cbbMaterialGoodsType.Size = new System.Drawing.Size(198, 22);
            this.cbbMaterialGoodsType.TabIndex = 103;
            this.cbbMaterialGoodsType.ValueChanged += new System.EventHandler(this.cbbMaterialGoodsType_ValueChanged);
            // 
            // ultraLabel10
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextVAlignAsString = "Middle";
            this.ultraLabel10.Appearance = appearance3;
            this.ultraLabel10.Location = new System.Drawing.Point(410, 109);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(111, 22);
            this.ultraLabel10.TabIndex = 67;
            this.ultraLabel10.Text = "Số lượng tồn tối thiểu";
            // 
            // cbbWarrantyTime
            // 
            this.cbbWarrantyTime.AllowNull = Infragistics.Win.DefaultableBoolean.True;
            this.cbbWarrantyTime.AutoSize = false;
            this.cbbWarrantyTime.DisplayLayout.DefaultSelectedForeColor = System.Drawing.Color.Black;
            this.cbbWarrantyTime.Location = new System.Drawing.Point(524, 85);
            this.cbbWarrantyTime.Name = "cbbWarrantyTime";
            this.cbbWarrantyTime.Size = new System.Drawing.Size(205, 22);
            this.cbbWarrantyTime.TabIndex = 104;
            // 
            // cbbMaterialGoodCategory
            // 
            this.cbbMaterialGoodCategory.AllowNull = Infragistics.Win.DefaultableBoolean.True;
            this.cbbMaterialGoodCategory.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.SuggestAppend;
            this.cbbMaterialGoodCategory.AutoSize = false;
            this.cbbMaterialGoodCategory.DisplayLayout.DefaultSelectedForeColor = System.Drawing.Color.Black;
            this.cbbMaterialGoodCategory.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbMaterialGoodCategory.LimitToList = true;
            this.cbbMaterialGoodCategory.Location = new System.Drawing.Point(524, 61);
            this.cbbMaterialGoodCategory.Name = "cbbMaterialGoodCategory";
            this.cbbMaterialGoodCategory.Size = new System.Drawing.Size(205, 22);
            this.cbbMaterialGoodCategory.TabIndex = 104;
            this.cbbMaterialGoodCategory.ItemNotInList += new Infragistics.Win.UltraWinGrid.ItemNotInListEventHandler(this.cbbMaterialGoodCategory_ItemNotInList);
            // 
            // txtConvertUnit
            // 
            this.txtConvertUnit.AutoSize = false;
            this.txtConvertUnit.Location = new System.Drawing.Point(523, 133);
            this.txtConvertUnit.MaxLength = 25;
            this.txtConvertUnit.Name = "txtConvertUnit";
            this.txtConvertUnit.Size = new System.Drawing.Size(205, 22);
            this.txtConvertUnit.TabIndex = 106;
            this.txtConvertUnit.Visible = false;
            // 
            // ultraLabel6
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.ultraLabel6.Appearance = appearance4;
            this.ultraLabel6.Location = new System.Drawing.Point(410, 85);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(111, 22);
            this.ultraLabel6.TabIndex = 61;
            this.ultraLabel6.Text = "Thời hạn BH";
            // 
            // lblMaterialGoodsName
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.TextVAlignAsString = "Middle";
            this.lblMaterialGoodsName.Appearance = appearance5;
            this.lblMaterialGoodsName.Location = new System.Drawing.Point(409, 37);
            this.lblMaterialGoodsName.Name = "lblMaterialGoodsName";
            this.lblMaterialGoodsName.Size = new System.Drawing.Size(77, 22);
            this.lblMaterialGoodsName.TabIndex = 60;
            this.lblMaterialGoodsName.Text = "Tên (*)";
            // 
            // ultraLabel8
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextVAlignAsString = "Middle";
            this.ultraLabel8.Appearance = appearance6;
            this.ultraLabel8.Location = new System.Drawing.Point(410, 133);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(101, 22);
            this.ultraLabel8.TabIndex = 59;
            this.ultraLabel8.Text = "Đơn vị chuyển đổi";
            this.ultraLabel8.Visible = false;
            // 
            // ultraLabel9
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.TextVAlignAsString = "Middle";
            this.ultraLabel9.Appearance = appearance7;
            this.ultraLabel9.Location = new System.Drawing.Point(410, 61);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(77, 22);
            this.ultraLabel9.TabIndex = 62;
            this.ultraLabel9.Text = "Loại";
            // 
            // chkMaterialToolType
            // 
            this.chkMaterialToolType.BackColor = System.Drawing.Color.Transparent;
            this.chkMaterialToolType.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkMaterialToolType.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkMaterialToolType.Location = new System.Drawing.Point(98, 134);
            this.chkMaterialToolType.Name = "chkMaterialToolType";
            this.chkMaterialToolType.Size = new System.Drawing.Size(103, 22);
            this.chkMaterialToolType.TabIndex = 109;
            this.chkMaterialToolType.Text = "Là CCDC";
            this.chkMaterialToolType.Visible = false;
            // 
            // txtUnit
            // 
            this.txtUnit.AutoSize = false;
            this.txtUnit.Location = new System.Drawing.Point(160, 85);
            this.txtUnit.MaxLength = 25;
            this.txtUnit.Name = "txtUnit";
            this.txtUnit.Size = new System.Drawing.Size(198, 22);
            this.txtUnit.TabIndex = 105;
            // 
            // txtMaterialGoodsCode
            // 
            this.txtMaterialGoodsCode.AutoSize = false;
            this.txtMaterialGoodsCode.Location = new System.Drawing.Point(160, 37);
            this.txtMaterialGoodsCode.MaxLength = 25;
            this.txtMaterialGoodsCode.Name = "txtMaterialGoodsCode";
            this.txtMaterialGoodsCode.Size = new System.Drawing.Size(198, 22);
            this.txtMaterialGoodsCode.TabIndex = 101;
            // 
            // lblRegistrationGroupName
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.TextVAlignAsString = "Middle";
            this.lblRegistrationGroupName.Appearance = appearance8;
            this.lblRegistrationGroupName.Location = new System.Drawing.Point(9, 109);
            this.lblRegistrationGroupName.Name = "lblRegistrationGroupName";
            this.lblRegistrationGroupName.Size = new System.Drawing.Size(92, 22);
            this.lblRegistrationGroupName.TabIndex = 28;
            this.lblRegistrationGroupName.Text = "Tỷ lệ chuyển đổi";
            this.lblRegistrationGroupName.Visible = false;
            // 
            // ultraLabel1
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance9;
            this.ultraLabel1.Location = new System.Drawing.Point(9, 37);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(77, 22);
            this.ultraLabel1.TabIndex = 24;
            this.ultraLabel1.Text = "Mã  (*)";
            // 
            // lblParentID
            // 
            appearance10.BackColor = System.Drawing.Color.Transparent;
            appearance10.TextVAlignAsString = "Middle";
            this.lblParentID.Appearance = appearance10;
            this.lblParentID.Location = new System.Drawing.Point(9, 85);
            this.lblParentID.Name = "lblParentID";
            this.lblParentID.Size = new System.Drawing.Size(77, 22);
            this.lblParentID.TabIndex = 16;
            this.lblParentID.Text = "Đơn vị tính";
            // 
            // lblRegistrationGroupCode
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            appearance11.TextVAlignAsString = "Middle";
            this.lblRegistrationGroupCode.Appearance = appearance11;
            this.lblRegistrationGroupCode.Location = new System.Drawing.Point(9, 61);
            this.lblRegistrationGroupCode.Name = "lblRegistrationGroupCode";
            this.lblRegistrationGroupCode.Size = new System.Drawing.Size(77, 22);
            this.lblRegistrationGroupCode.TabIndex = 50;
            this.lblRegistrationGroupCode.Text = "Tính chất (*)";
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox2.Controls.Add(this.cbbCareerGroup);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel5);
            this.ultraGroupBox2.Controls.Add(this.btnDiscount);
            this.ultraGroupBox2.Controls.Add(this.btnUnitOSales);
            this.ultraGroupBox2.Controls.Add(this.txtSalePrice);
            this.ultraGroupBox2.Controls.Add(this.cbbTaxRate);
            this.ultraGroupBox2.Controls.Add(this.txtSaleDiscountRate);
            this.ultraGroupBox2.Controls.Add(this.txtPuchaseDiscountRate);
            this.ultraGroupBox2.Controls.Add(this.txtPuchasePrice);
            this.ultraGroupBox2.Controls.Add(this.cbbRevenueAccount);
            this.ultraGroupBox2.Controls.Add(this.cbbExpenseAccount);
            this.ultraGroupBox2.Controls.Add(this.chkIsSecurity);
            this.ultraGroupBox2.Controls.Add(this.chkIsSaleDiscountPolicy);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel16);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel18);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel19);
            this.ultraGroupBox2.Controls.Add(this.cbbReponsitoryAccount);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel20);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel21);
            this.ultraGroupBox2.Controls.Add(this.ultraCombo6);
            this.ultraGroupBox2.Controls.Add(this.txtItemSource);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel15);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel12);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel14);
            this.ultraGroupBox2.Controls.Add(this.cbbRepository);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel3);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel4);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            appearance33.FontData.BoldAsString = "True";
            this.ultraGroupBox2.HeaderAppearance = appearance33;
            this.ultraGroupBox2.Location = new System.Drawing.Point(0, 137);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(735, 254);
            this.ultraGroupBox2.TabIndex = 200;
            this.ultraGroupBox2.Text = "Thông tin ngầm định";
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbCareerGroup
            // 
            this.cbbCareerGroup.AllowNull = Infragistics.Win.DefaultableBoolean.True;
            this.cbbCareerGroup.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.SuggestAppend;
            this.cbbCareerGroup.AutoSize = false;
            this.cbbCareerGroup.DisplayLayout.DefaultSelectedForeColor = System.Drawing.Color.Black;
            this.cbbCareerGroup.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbCareerGroup.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbCareerGroup.LimitToList = true;
            this.cbbCareerGroup.Location = new System.Drawing.Point(202, 207);
            this.cbbCareerGroup.Name = "cbbCareerGroup";
            this.cbbCareerGroup.Size = new System.Drawing.Size(526, 22);
            this.cbbCareerGroup.TabIndex = 222;
            this.cbbCareerGroup.Visible = false;
            // 
            // ultraLabel5
            // 
            appearance13.BackColor = System.Drawing.Color.Transparent;
            appearance13.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance13;
            this.ultraLabel5.Location = new System.Drawing.Point(9, 207);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(198, 22);
            this.ultraLabel5.TabIndex = 221;
            this.ultraLabel5.Text = "Nhóm ngành nghề tính thuế GTGT";
            this.ultraLabel5.Visible = false;
            // 
            // btnDiscount
            // 
            this.btnDiscount.Location = new System.Drawing.Point(697, 151);
            this.btnDiscount.Name = "btnDiscount";
            this.btnDiscount.Size = new System.Drawing.Size(31, 23);
            this.btnDiscount.TabIndex = 220;
            this.btnDiscount.Text = ". . . ";
            this.btnDiscount.Click += new System.EventHandler(this.btnDiscount_Click);
            // 
            // btnUnitOSales
            // 
            this.btnUnitOSales.Location = new System.Drawing.Point(697, 31);
            this.btnUnitOSales.Name = "btnUnitOSales";
            this.btnUnitOSales.Size = new System.Drawing.Size(31, 23);
            this.btnUnitOSales.TabIndex = 219;
            this.btnUnitOSales.Text = ". . . ";
            this.btnUnitOSales.Click += new System.EventHandler(this.btnUnitOSales_Click);
            // 
            // txtSalePrice
            // 
            appearance14.TextHAlignAsString = "Right";
            this.txtSalePrice.Appearance = appearance14;
            this.txtSalePrice.AutoSize = false;
            this.txtSalePrice.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtSalePrice.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.txtSalePrice.InputMask = "{double:14.4}";
            this.txtSalePrice.Location = new System.Drawing.Point(523, 32);
            this.txtSalePrice.Name = "txtSalePrice";
            this.txtSalePrice.NullText = "0";
            this.txtSalePrice.PromptChar = ' ';
            this.txtSalePrice.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtSalePrice.Size = new System.Drawing.Size(168, 22);
            this.txtSalePrice.TabIndex = 202;
            this.txtSalePrice.Text = "00.000";
            // 
            // cbbTaxRate
            // 
            this.cbbTaxRate.AutoSize = false;
            this.cbbTaxRate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbTaxRate.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbbTaxRate.Location = new System.Drawing.Point(523, 56);
            this.cbbTaxRate.Name = "cbbTaxRate";
            this.cbbTaxRate.Size = new System.Drawing.Size(205, 22);
            this.cbbTaxRate.TabIndex = 205;
            this.cbbTaxRate.Leave += new System.EventHandler(this.cbbTaxRate_Leave);
            // 
            // txtSaleDiscountRate
            // 
            appearance15.TextHAlignAsString = "Right";
            this.txtSaleDiscountRate.Appearance = appearance15;
            this.txtSaleDiscountRate.AutoSize = false;
            this.txtSaleDiscountRate.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtSaleDiscountRate.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.txtSaleDiscountRate.InputMask = "{double:2.4}";
            this.txtSaleDiscountRate.Location = new System.Drawing.Point(523, 128);
            this.txtSaleDiscountRate.Name = "txtSaleDiscountRate";
            this.txtSaleDiscountRate.NullText = "0";
            this.txtSaleDiscountRate.PromptChar = ' ';
            this.txtSaleDiscountRate.Size = new System.Drawing.Size(205, 22);
            this.txtSaleDiscountRate.TabIndex = 211;
            this.txtSaleDiscountRate.Text = "00";
            // 
            // txtPuchaseDiscountRate
            // 
            appearance16.TextHAlignAsString = "Right";
            this.txtPuchaseDiscountRate.Appearance = appearance16;
            this.txtPuchaseDiscountRate.AutoSize = false;
            this.txtPuchaseDiscountRate.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtPuchaseDiscountRate.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.txtPuchaseDiscountRate.InputMask = "{double:2.4}";
            this.txtPuchaseDiscountRate.Location = new System.Drawing.Point(160, 104);
            this.txtPuchaseDiscountRate.Name = "txtPuchaseDiscountRate";
            this.txtPuchaseDiscountRate.NullText = "0";
            this.txtPuchaseDiscountRate.PromptChar = ' ';
            this.txtPuchaseDiscountRate.Size = new System.Drawing.Size(198, 22);
            this.txtPuchaseDiscountRate.TabIndex = 210;
            this.txtPuchaseDiscountRate.Text = "00";
            // 
            // txtPuchasePrice
            // 
            appearance17.TextHAlignAsString = "Right";
            this.txtPuchasePrice.Appearance = appearance17;
            this.txtPuchasePrice.AutoSize = false;
            this.txtPuchasePrice.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtPuchasePrice.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.txtPuchasePrice.InputMask = "{double:14.4}";
            this.txtPuchasePrice.Location = new System.Drawing.Point(160, 31);
            this.txtPuchasePrice.Name = "txtPuchasePrice";
            this.txtPuchasePrice.NullText = "0";
            this.txtPuchasePrice.PromptChar = ' ';
            this.txtPuchasePrice.Size = new System.Drawing.Size(198, 22);
            this.txtPuchasePrice.TabIndex = 201;
            this.txtPuchasePrice.Text = "00.000";
            // 
            // cbbRevenueAccount
            // 
            this.cbbRevenueAccount.AllowNull = Infragistics.Win.DefaultableBoolean.True;
            this.cbbRevenueAccount.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.SuggestAppend;
            this.cbbRevenueAccount.AutoSize = false;
            this.cbbRevenueAccount.DisplayLayout.DefaultSelectedForeColor = System.Drawing.Color.Black;
            this.cbbRevenueAccount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbRevenueAccount.LimitToList = true;
            this.cbbRevenueAccount.Location = new System.Drawing.Point(523, 104);
            this.cbbRevenueAccount.Name = "cbbRevenueAccount";
            this.cbbRevenueAccount.Size = new System.Drawing.Size(205, 22);
            this.cbbRevenueAccount.TabIndex = 209;
            this.cbbRevenueAccount.ItemNotInList += new Infragistics.Win.UltraWinGrid.ItemNotInListEventHandler(this.cbbRevenueAccount_ItemNotInList);
            // 
            // cbbExpenseAccount
            // 
            this.cbbExpenseAccount.AllowNull = Infragistics.Win.DefaultableBoolean.True;
            this.cbbExpenseAccount.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.SuggestAppend;
            this.cbbExpenseAccount.AutoSize = false;
            this.cbbExpenseAccount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbExpenseAccount.LimitToList = true;
            this.cbbExpenseAccount.Location = new System.Drawing.Point(160, 80);
            this.cbbExpenseAccount.Name = "cbbExpenseAccount";
            this.cbbExpenseAccount.Size = new System.Drawing.Size(198, 22);
            this.cbbExpenseAccount.TabIndex = 208;
            this.cbbExpenseAccount.ItemNotInList += new Infragistics.Win.UltraWinGrid.ItemNotInListEventHandler(this.cbbExpenseAccount_ItemNotInList);
            // 
            // chkIsSecurity
            // 
            appearance18.TextVAlignAsString = "Middle";
            this.chkIsSecurity.Appearance = appearance18;
            this.chkIsSecurity.BackColor = System.Drawing.Color.Transparent;
            this.chkIsSecurity.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsSecurity.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkIsSecurity.Location = new System.Drawing.Point(160, 248);
            this.chkIsSecurity.Name = "chkIsSecurity";
            this.chkIsSecurity.Size = new System.Drawing.Size(481, 22);
            this.chkIsSecurity.TabIndex = 218;
            this.chkIsSecurity.Text = "Cảnh báo khi nhập mã không đúng quy cách";
            this.chkIsSecurity.Visible = false;
            // 
            // chkIsSaleDiscountPolicy
            // 
            appearance19.TextVAlignAsString = "Middle";
            this.chkIsSaleDiscountPolicy.Appearance = appearance19;
            this.chkIsSaleDiscountPolicy.BackColor = System.Drawing.Color.Transparent;
            this.chkIsSaleDiscountPolicy.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsSaleDiscountPolicy.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkIsSaleDiscountPolicy.Location = new System.Drawing.Point(523, 151);
            this.chkIsSaleDiscountPolicy.Name = "chkIsSaleDiscountPolicy";
            this.chkIsSaleDiscountPolicy.Size = new System.Drawing.Size(151, 22);
            this.chkIsSaleDiscountPolicy.TabIndex = 213;
            this.chkIsSaleDiscountPolicy.Text = "Chính sách CK bán hàng";
            this.chkIsSaleDiscountPolicy.CheckedChanged += new System.EventHandler(this.ultraCheckEditor2_CheckedChanged);
            // 
            // ultraLabel16
            // 
            appearance20.BackColor = System.Drawing.Color.Transparent;
            appearance20.TextVAlignAsString = "Middle";
            this.ultraLabel16.Appearance = appearance20;
            this.ultraLabel16.Location = new System.Drawing.Point(416, 128);
            this.ultraLabel16.Name = "ultraLabel16";
            this.ultraLabel16.Size = new System.Drawing.Size(126, 22);
            this.ultraLabel16.TabIndex = 74;
            this.ultraLabel16.Text = "Tỷ lệ CKBH (%)";
            // 
            // ultraLabel18
            // 
            appearance21.BackColor = System.Drawing.Color.Transparent;
            appearance21.TextVAlignAsString = "Middle";
            this.ultraLabel18.Appearance = appearance21;
            this.ultraLabel18.Location = new System.Drawing.Point(416, 104);
            this.ultraLabel18.Name = "ultraLabel18";
            this.ultraLabel18.Size = new System.Drawing.Size(126, 22);
            this.ultraLabel18.TabIndex = 68;
            this.ultraLabel18.Text = "TK doanh thu";
            // 
            // ultraLabel19
            // 
            appearance22.BackColor = System.Drawing.Color.Transparent;
            appearance22.TextVAlignAsString = "Middle";
            this.ultraLabel19.Appearance = appearance22;
            this.ultraLabel19.Location = new System.Drawing.Point(416, 31);
            this.ultraLabel19.Name = "ultraLabel19";
            this.ultraLabel19.Size = new System.Drawing.Size(126, 22);
            this.ultraLabel19.TabIndex = 67;
            this.ultraLabel19.Text = "Đơn giá bán";
            // 
            // cbbReponsitoryAccount
            // 
            this.cbbReponsitoryAccount.AllowNull = Infragistics.Win.DefaultableBoolean.True;
            this.cbbReponsitoryAccount.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.SuggestAppend;
            this.cbbReponsitoryAccount.AutoSize = false;
            this.cbbReponsitoryAccount.DisplayLayout.DefaultSelectedForeColor = System.Drawing.Color.Black;
            this.cbbReponsitoryAccount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbReponsitoryAccount.LimitToList = true;
            this.cbbReponsitoryAccount.Location = new System.Drawing.Point(523, 80);
            this.cbbReponsitoryAccount.Name = "cbbReponsitoryAccount";
            this.cbbReponsitoryAccount.Size = new System.Drawing.Size(205, 22);
            this.cbbReponsitoryAccount.TabIndex = 207;
            this.cbbReponsitoryAccount.ItemNotInList += new Infragistics.Win.UltraWinGrid.ItemNotInListEventHandler(this.cbbReponsitoryAccount_ItemNotInList);
            // 
            // ultraLabel20
            // 
            appearance23.BackColor = System.Drawing.Color.Transparent;
            appearance23.TextVAlignAsString = "Middle";
            this.ultraLabel20.Appearance = appearance23;
            this.ultraLabel20.Location = new System.Drawing.Point(416, 80);
            this.ultraLabel20.Name = "ultraLabel20";
            this.ultraLabel20.Size = new System.Drawing.Size(92, 22);
            this.ultraLabel20.TabIndex = 66;
            this.ultraLabel20.Text = "Tài khoản kho";
            // 
            // ultraLabel21
            // 
            appearance24.BackColor = System.Drawing.Color.Transparent;
            appearance24.TextVAlignAsString = "Middle";
            this.ultraLabel21.Appearance = appearance24;
            this.ultraLabel21.Location = new System.Drawing.Point(416, 56);
            this.ultraLabel21.Name = "ultraLabel21";
            this.ultraLabel21.Size = new System.Drawing.Size(126, 22);
            this.ultraLabel21.TabIndex = 70;
            this.ultraLabel21.Text = "Thuế suất (%)";
            // 
            // ultraCombo6
            // 
            this.ultraCombo6.AllowNull = Infragistics.Win.DefaultableBoolean.True;
            this.ultraCombo6.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.SuggestAppend;
            this.ultraCombo6.AutoSize = false;
            this.ultraCombo6.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.ultraCombo6.LimitToList = true;
            this.ultraCombo6.Location = new System.Drawing.Point(160, 128);
            this.ultraCombo6.Name = "ultraCombo6";
            this.ultraCombo6.Size = new System.Drawing.Size(198, 22);
            this.ultraCombo6.TabIndex = 212;
            this.ultraCombo6.ItemNotInList += new Infragistics.Win.UltraWinGrid.ItemNotInListEventHandler(this.ultraCombo6_ItemNotInList);
            // 
            // txtItemSource
            // 
            this.txtItemSource.AutoSize = false;
            this.txtItemSource.Location = new System.Drawing.Point(160, 179);
            this.txtItemSource.MaxLength = 250;
            this.txtItemSource.Name = "txtItemSource";
            this.txtItemSource.Size = new System.Drawing.Size(568, 22);
            this.txtItemSource.TabIndex = 217;
            // 
            // ultraLabel15
            // 
            appearance25.BackColor = System.Drawing.Color.Transparent;
            appearance25.TextVAlignAsString = "Middle";
            this.ultraLabel15.Appearance = appearance25;
            this.ultraLabel15.Location = new System.Drawing.Point(9, 179);
            this.ultraLabel15.Name = "ultraLabel15";
            this.ultraLabel15.Size = new System.Drawing.Size(126, 22);
            this.ultraLabel15.TabIndex = 63;
            this.ultraLabel15.Text = "Nguồn gốc";
            // 
            // ultraLabel12
            // 
            appearance26.BackColor = System.Drawing.Color.Transparent;
            appearance26.TextVAlignAsString = "Middle";
            this.ultraLabel12.Appearance = appearance26;
            this.ultraLabel12.Location = new System.Drawing.Point(9, 104);
            this.ultraLabel12.Name = "ultraLabel12";
            this.ultraLabel12.Size = new System.Drawing.Size(126, 22);
            this.ultraLabel12.TabIndex = 56;
            this.ultraLabel12.Text = "Tỷ lệ CKMH (%)";
            // 
            // ultraLabel14
            // 
            appearance27.BackColor = System.Drawing.Color.Transparent;
            appearance27.TextVAlignAsString = "Middle";
            this.ultraLabel14.Appearance = appearance27;
            this.ultraLabel14.Location = new System.Drawing.Point(9, 128);
            this.ultraLabel14.Name = "ultraLabel14";
            this.ultraLabel14.Size = new System.Drawing.Size(172, 22);
            this.ultraLabel14.TabIndex = 58;
            this.ultraLabel14.Text = "Nhóm HHDV chịu thuế TTĐB";
            // 
            // cbbRepository
            // 
            this.cbbRepository.AllowNull = Infragistics.Win.DefaultableBoolean.True;
            this.cbbRepository.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.SuggestAppend;
            this.cbbRepository.AutoSize = false;
            appearance28.Image = global::Accounting.Properties.Resources.plus;
            editorButton1.Appearance = appearance28;
            appearance29.Image = global::Accounting.Properties.Resources.plus_press;
            editorButton1.PressedAppearance = appearance29;
            this.cbbRepository.ButtonsRight.Add(editorButton1);
            this.cbbRepository.DisplayLayout.DefaultSelectedForeColor = System.Drawing.Color.Black;
            this.cbbRepository.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbRepository.LimitToList = true;
            this.cbbRepository.Location = new System.Drawing.Point(160, 56);
            this.cbbRepository.Name = "cbbRepository";
            this.cbbRepository.Size = new System.Drawing.Size(198, 22);
            this.cbbRepository.TabIndex = 206;
            this.cbbRepository.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbRepository_RowSelected);
            this.cbbRepository.ItemNotInList += new Infragistics.Win.UltraWinGrid.ItemNotInListEventHandler(this.cbbRepository_ItemNotInList);
            this.cbbRepository.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.cbbRepository_EditorButtonClick);
            // 
            // ultraLabel2
            // 
            appearance30.BackColor = System.Drawing.Color.Transparent;
            appearance30.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance30;
            this.ultraLabel2.Location = new System.Drawing.Point(9, 80);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(126, 22);
            this.ultraLabel2.TabIndex = 28;
            this.ultraLabel2.Text = "TK chi phí";
            // 
            // ultraLabel3
            // 
            appearance31.BackColor = System.Drawing.Color.Transparent;
            appearance31.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance31;
            this.ultraLabel3.Location = new System.Drawing.Point(9, 31);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(126, 22);
            this.ultraLabel3.TabIndex = 24;
            this.ultraLabel3.Text = "Đơn giá mua";
            // 
            // ultraLabel4
            // 
            appearance32.BackColor = System.Drawing.Color.Transparent;
            appearance32.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance32;
            this.ultraLabel4.Location = new System.Drawing.Point(9, 56);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(92, 22);
            this.ultraLabel4.TabIndex = 16;
            this.ultraLabel4.Text = "Kho ngầm định";
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.ultraGroupBox3);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(735, 391);
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox3.Controls.Add(this.chkPrintMaterial);
            this.ultraGroupBox3.Controls.Add(this.uGrid);
            this.ultraGroupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance34.FontData.BoldAsString = "True";
            this.ultraGroupBox3.HeaderAppearance = appearance34;
            this.ultraGroupBox3.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox3.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(735, 391);
            this.ultraGroupBox3.TabIndex = 111;
            this.ultraGroupBox3.Text = "Danh mục lắp ráp";
            this.ultraGroupBox3.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // chkPrintMaterial
            // 
            this.chkPrintMaterial.BackColor = System.Drawing.Color.Transparent;
            this.chkPrintMaterial.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkPrintMaterial.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkPrintMaterial.Location = new System.Drawing.Point(11, 364);
            this.chkPrintMaterial.Name = "chkPrintMaterial";
            this.chkPrintMaterial.Size = new System.Drawing.Size(204, 20);
            this.chkPrintMaterial.TabIndex = 112;
            this.chkPrintMaterial.Text = "In các bộ phận lên hoá đơn";
            this.chkPrintMaterial.Visible = false;
            // 
            // uGrid
            // 
            this.uGrid.ContextMenuStrip = this.contextMenuStrip1;
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.uGrid.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGrid.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGrid.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGrid.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.uGrid.Location = new System.Drawing.Point(9, 25);
            this.uGrid.Name = "uGrid";
            this.uGrid.Size = new System.Drawing.Size(712, 334);
            this.uGrid.TabIndex = 6;
            this.uGrid.Text = "uGrid";
            this.uGrid.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid_AfterCellUpdate);
            this.uGrid.BeforeRowActivate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.uGrid_BeforeRowActivate);
            this.uGrid.BeforeExitEditMode += new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventHandler(this.uGrid1_BeforeExitEditMode);
            this.uGrid.Error += new Infragistics.Win.UltraWinGrid.ErrorEventHandler(this.uGrid_Error);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmAdd,
            this.tsmDelete});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(205, 48);
            // 
            // tsmAdd
            // 
            this.tsmAdd.Name = "tsmAdd";
            this.tsmAdd.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.tsmAdd.Size = new System.Drawing.Size(204, 22);
            this.tsmAdd.Text = "Thêm một dòng";
            this.tsmAdd.Click += new System.EventHandler(this.tsmAdd_Click);
            // 
            // tsmDelete
            // 
            this.tsmDelete.Name = "tsmDelete";
            this.tsmDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.tsmDelete.Size = new System.Drawing.Size(204, 22);
            this.tsmDelete.Text = "Xóa một dòng";
            this.tsmDelete.Click += new System.EventHandler(this.tsmDelete_Click);
            // 
            // ultraTabControl1
            // 
            this.ultraTabControl1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl2);
            this.ultraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.ultraTabControl1.Name = "ultraTabControl1";
            this.ultraTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.ultraTabControl1.Size = new System.Drawing.Size(739, 417);
            this.ultraTabControl1.TabIndex = 113;
            ultraTab2.TabPage = this.ultraTabPageControl1;
            ultraTab2.Text = "Thông tin VTHH";
            ultraTab1.TabPage = this.ultraTabPageControl2;
            ultraTab1.Text = "Chi tiết VTHH lắp ráp/tháo dỡ";
            this.ultraTabControl1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab2,
            ultraTab1});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(735, 391);
            // 
            // ultraGroupBox4
            // 
            this.ultraGroupBox4.Controls.Add(this.chkActive);
            this.ultraGroupBox4.Controls.Add(this.btnSaveContinue);
            this.ultraGroupBox4.Controls.Add(this.btnSave);
            this.ultraGroupBox4.Controls.Add(this.btnClose);
            this.ultraGroupBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraGroupBox4.Location = new System.Drawing.Point(0, 417);
            this.ultraGroupBox4.Name = "ultraGroupBox4";
            this.ultraGroupBox4.Size = new System.Drawing.Size(739, 36);
            this.ultraGroupBox4.TabIndex = 223;
            // 
            // chkActive
            // 
            appearance35.TextVAlignAsString = "Middle";
            this.chkActive.Appearance = appearance35;
            this.chkActive.BackColor = System.Drawing.Color.Transparent;
            this.chkActive.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkActive.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkActive.Checked = true;
            this.chkActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkActive.Location = new System.Drawing.Point(21, 9);
            this.chkActive.Name = "chkActive";
            this.chkActive.Size = new System.Drawing.Size(103, 22);
            this.chkActive.TabIndex = 219;
            this.chkActive.Text = "Hoạt động";
            // 
            // btnSaveContinue
            // 
            this.btnSaveContinue.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance36.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSaveContinue.Appearance = appearance36;
            this.btnSaveContinue.Location = new System.Drawing.Point(430, 1);
            this.btnSaveContinue.Name = "btnSaveContinue";
            this.btnSaveContinue.Size = new System.Drawing.Size(130, 30);
            this.btnSaveContinue.TabIndex = 222;
            this.btnSaveContinue.Text = "Lưu và Thêm mới";
            this.btnSaveContinue.Click += new System.EventHandler(this.btnSaveContinue_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance37.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance37;
            this.btnSave.Location = new System.Drawing.Point(566, 1);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 220;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance38.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance38;
            this.btnClose.Location = new System.Drawing.Point(647, 1);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 221;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // warrantyBindingSource
            // 
            this.warrantyBindingSource.DataSource = typeof(Accounting.Core.Domain.Warranty);
            // 
            // FMaterialGoodsDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(739, 453);
            this.Controls.Add(this.ultraTabControl1);
            this.Controls.Add(this.ultraGroupBox4);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FMaterialGoodsDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FMaterialGoodsDetail_FormClosed);
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtMaterialGoodsName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMaterialGoodsType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbWarrantyTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMaterialGoodCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtConvertUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMaterialToolType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaterialGoodsCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbCareerGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbTaxRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbRevenueAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbExpenseAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsSecurity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsSaleDiscountPolicy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbReponsitoryAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCombo6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItemSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbRepository)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintMaterial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).EndInit();
            this.ultraTabControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).EndInit();
            this.ultraGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkActive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.warrantyBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraLabel lblRegistrationGroupName;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel lblParentID;
        private Infragistics.Win.Misc.UltraLabel lblRegistrationGroupCode;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbMaterialGoodCategory;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtConvertUnit;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraLabel lblMaterialGoodsName;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkMaterialToolType;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtUnit;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMaterialGoodsCode;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsSaleDiscountPolicy;
        private Infragistics.Win.Misc.UltraLabel ultraLabel16;
        private Infragistics.Win.Misc.UltraLabel ultraLabel18;
        private Infragistics.Win.Misc.UltraLabel ultraLabel19;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbReponsitoryAccount;
        private Infragistics.Win.Misc.UltraLabel ultraLabel20;
        private Infragistics.Win.Misc.UltraLabel ultraLabel21;
        private Infragistics.Win.UltraWinGrid.UltraCombo ultraCombo6;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtItemSource;
        private Infragistics.Win.Misc.UltraLabel ultraLabel15;
        private Infragistics.Win.Misc.UltraLabel ultraLabel12;
        private Infragistics.Win.Misc.UltraLabel ultraLabel14;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbRepository;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkActive;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbMaterialGoodsType;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsSecurity;
       
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbRevenueAccount;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbExpenseAccount;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtMinimumStock;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtPuchasePrice;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtSaleDiscountRate;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtPuchaseDiscountRate;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkPrintMaterial;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtConvertRate;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbTaxRate;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtSalePrice;
        private Infragistics.Win.Misc.UltraButton btnUnitOSales;
        private Infragistics.Win.Misc.UltraButton btnDiscount;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbWarrantyTime;
        private System.Windows.Forms.BindingSource warrantyBindingSource;
        private Infragistics.Win.Misc.UltraButton btnSaveContinue;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripMenuItem tsmDelete;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbCareerGroup;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox4;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMaterialGoodsName;
    }
}