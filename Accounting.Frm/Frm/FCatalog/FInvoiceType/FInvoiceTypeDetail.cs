﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;

namespace Accounting
{
    public partial class FInvoiceTypeDetail : CustormForm
    {
        #region khai báo
        private readonly IInvoiceTypeService _IInvoiceTypeService;

        InvoiceType _Select = new InvoiceType();

        bool Them = true;
        public static bool isClose = true;
        #endregion

        #region khởi tạo
        public FInvoiceTypeDetail()
        {//Thêm
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            this.Text = "Thêm mới Mẫu số hóa đơn";
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.CenterToParent();
            chkIsActive.Visible = false;
            #endregion

            #region Thiết lập ban đầu cho Form
            //Khai báo các webservices
            _IInvoiceTypeService = IoC.Resolve<IInvoiceTypeService>();

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion
        }

        public FInvoiceTypeDetail(InvoiceType temp)
        {//Sửa
            #region Khởi tạo giá trị mặc định của Form

            InitializeComponent();
            //khai báo các webservice
            _IInvoiceTypeService = IoC.Resolve<IInvoiceTypeService>();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.CenterToParent();
            #endregion
            this.Text = "Sửa Mẫu số hóa đơn";
            #region Thiết lập ban đầu cho Form
            _Select = temp;
            Them = false;
            txtInvoiceTypeCode.Enabled = false;


            //Khai báo các webservices


            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion

            #region Fill dữ liệu Obj vào control
            ObjandGUI(temp, false);
            #endregion
        }
        private void InitializeGUI()
        {
            //khởi tạo cbb loại kết chuyển
            cbb_InvoiceType.DataSource = FInvoiceType.dic_InvoiceType.Values.ToList();
            cbb_InvoiceType.SelectedIndex = 0;
            txtFromNo.FormatNumberic(ConstDatabase.Format_Quantity);
            txtToNo.FormatNumberic(ConstDatabase.Format_Quantity);
        }
        #endregion

        #region Button Event
        /// Sự kiện nút Lưu lại
        private void btnSave_Click_1(object sender, EventArgs e)
        {
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion

            //Gui to object
            #region Fill dữ liệu control vào obj
            InvoiceType temp = Them ? new InvoiceType() : _IInvoiceTypeService.Getbykey(_Select.ID);
            temp = ObjandGUI(temp, true);
            #endregion

            #region Thao tác CSDL

            if (Them) _IInvoiceTypeService.CreateNew(temp);
            else _IInvoiceTypeService.Update(temp);
            _IInvoiceTypeService.CommitChanges();//ko dùng comitran.
            #endregion

            #region xử lý form, kết thúc form
            this.Close();
            isClose = false;
            #endregion
        }


        /// Sự kiện nút đóng


        private void btnClose_Click_1(object sender, EventArgs e)
        {
            isClose = true;
            DialogResult = DialogResult.Cancel;
            this.Close();
        }
        #endregion

        #region Utils

        /// get hoặc set một đối tượng vào giao diện xử lý

        InvoiceType ObjandGUI(InvoiceType input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();  //Thêm mới

                input.InvoiceTypeCode = txtInvoiceTypeCode.Text;
                input.InvoiceTypeName = txtInvoiceTypeName.Text;
                input._InvoiceType = cbb_InvoiceType.SelectedIndex;

                if (!string.IsNullOrEmpty(txtFromNo.Value.ToString()))
                {
                    input.FromNo = decimal.Parse(txtFromNo.Value.ToString());
                }
                else
                {
                    input.FromNo = null;
                }

                if (!string.IsNullOrEmpty(txtToNo.Value.ToString()))
                {
                    input.ToNo = decimal.Parse(txtToNo.Value.ToString());
                }
                else
                {
                    input.ToNo = null;
                }

                input.IsActive = chkIsActive.CheckState == CheckState.Checked ? true : false;
            }
            else
            {
                txtInvoiceTypeCode.Text = input.InvoiceTypeCode;
                txtInvoiceTypeName.Text = input.InvoiceTypeName;
                cbb_InvoiceType.SelectedIndex = input._InvoiceType;
                if (input.FromNo > 0)
                {
                    txtFromNo.Value = input.FromNo.ToString();
                }

                if (input.ToNo > 0 && input.ToNo >= input.FromNo)
                {
                    txtToNo.Value = input.ToNo.ToString();
                }

                chkIsActive.CheckState = input.IsActive ? CheckState.Checked : CheckState.Unchecked;
            }
            return input;
        }

        bool CheckError()
        {
            bool kq = true;
            //Check Error Chung

            //Check Error Riêng từng Form
            if (string.IsNullOrEmpty(txtInvoiceTypeCode.Text) || string.IsNullOrEmpty(txtInvoiceTypeName.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }
            List<string> list = (from p in _IInvoiceTypeService.Query where p.InvoiceTypeCode == txtInvoiceTypeCode.Text select p.InvoiceTypeCode).ToList();

            foreach (var item in list)
            {
                if (item == txtInvoiceTypeCode.Text && Them)
                {
                    MSG.Error("Mẫu số : <<" + item + ">> đã bị trùng trong danh sách nhập.Xin vui lòng kiểm tra lại! ");
                    return false;
                }
            }

            if (!string.IsNullOrEmpty(txtToNo.Value.ToString()) && !string.IsNullOrEmpty(txtFromNo.Value.ToString()))
            {
                if (int.Parse(txtToNo.Value.ToString()) < int.Parse(txtFromNo.Value.ToString()))
                {
                    MSG.Error("Dữ liệu nhập không hợp lệ. Giá trị nhập <<Từ Số>> phải nhỏ hơn hoặc bằng <<Đến Số>>");
                    txtToNo.Focus();
                    return false;
                }
                else
                {
                    return true;
                }

            }
            return kq;
        }
        #endregion

        private void FInvoiceTypeDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
