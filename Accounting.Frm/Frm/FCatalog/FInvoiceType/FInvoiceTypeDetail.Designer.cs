﻿namespace Accounting
{
    partial class FInvoiceTypeDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtToNo = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtFromNo = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.cbb_InvoiceType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.txtInvoiceTypeCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblDescription = new Infragistics.Win.Misc.UltraLabel();
            this.lblBankCode = new Infragistics.Win.Misc.UltraLabel();
            this.lblBankName = new Infragistics.Win.Misc.UltraLabel();
            this.txtInvoiceTypeName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.chkIsActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbb_InvoiceType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceTypeCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceTypeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            appearance1.FontData.BoldAsString = "True";
            this.ultraGroupBox1.Appearance = appearance1;
            this.ultraGroupBox1.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox1.Controls.Add(this.txtToNo);
            this.ultraGroupBox1.Controls.Add(this.txtFromNo);
            this.ultraGroupBox1.Controls.Add(this.cbb_InvoiceType);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox1.Controls.Add(this.txtInvoiceTypeCode);
            this.ultraGroupBox1.Controls.Add(this.lblDescription);
            this.ultraGroupBox1.Controls.Add(this.lblBankCode);
            this.ultraGroupBox1.Controls.Add(this.lblBankName);
            this.ultraGroupBox1.Controls.Add(this.txtInvoiceTypeName);
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(1, 1);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(458, 195);
            this.ultraGroupBox1.TabIndex = 67;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtToNo
            // 
            appearance2.TextHAlignAsString = "Right";
            this.txtToNo.Appearance = appearance2;
            this.txtToNo.AutoSize = false;
            this.txtToNo.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.txtToNo.InputMask = "nnnnnnnnn";
            this.txtToNo.Location = new System.Drawing.Point(130, 166);
            this.txtToNo.Name = "txtToNo";
            this.txtToNo.PromptChar = ' ';
            this.txtToNo.Size = new System.Drawing.Size(317, 22);
            this.txtToNo.TabIndex = 41;
            // 
            // txtFromNo
            // 
            appearance3.TextHAlignAsString = "Right";
            this.txtFromNo.Appearance = appearance3;
            this.txtFromNo.AutoSize = false;
            this.txtFromNo.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.txtFromNo.InputMask = "nnnnnnnnn";
            this.txtFromNo.Location = new System.Drawing.Point(130, 139);
            this.txtFromNo.Name = "txtFromNo";
            this.txtFromNo.PromptChar = ' ';
            this.txtFromNo.Size = new System.Drawing.Size(317, 22);
            this.txtFromNo.TabIndex = 40;
            // 
            // cbb_InvoiceType
            // 
            this.cbb_InvoiceType.AutoSize = false;
            this.cbb_InvoiceType.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbb_InvoiceType.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbb_InvoiceType.Location = new System.Drawing.Point(130, 49);
            this.cbb_InvoiceType.MaxLength = 512;
            this.cbb_InvoiceType.Name = "cbb_InvoiceType";
            this.cbb_InvoiceType.Size = new System.Drawing.Size(317, 22);
            this.cbb_InvoiceType.TabIndex = 39;
            // 
            // ultraLabel2
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance4;
            this.ultraLabel2.Location = new System.Drawing.Point(12, 166);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(90, 22);
            this.ultraLabel2.TabIndex = 38;
            this.ultraLabel2.Text = "Đến số";
            // 
            // ultraLabel1
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance5;
            this.ultraLabel1.Location = new System.Drawing.Point(12, 139);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(90, 22);
            this.ultraLabel1.TabIndex = 37;
            this.ultraLabel1.Text = "Từ số";
            // 
            // txtInvoiceTypeCode
            // 
            this.txtInvoiceTypeCode.AutoSize = false;
            this.txtInvoiceTypeCode.Location = new System.Drawing.Point(130, 22);
            this.txtInvoiceTypeCode.MaxLength = 25;
            this.txtInvoiceTypeCode.Name = "txtInvoiceTypeCode";
            this.txtInvoiceTypeCode.Size = new System.Drawing.Size(317, 22);
            this.txtInvoiceTypeCode.TabIndex = 35;
            // 
            // lblDescription
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextVAlignAsString = "Middle";
            this.lblDescription.Appearance = appearance6;
            this.lblDescription.Location = new System.Drawing.Point(12, 76);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(90, 22);
            this.lblDescription.TabIndex = 31;
            this.lblDescription.Text = "Tên mẫu số (*)";
            // 
            // lblBankCode
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.TextVAlignAsString = "Middle";
            this.lblBankCode.Appearance = appearance7;
            this.lblBankCode.Location = new System.Drawing.Point(12, 22);
            this.lblBankCode.Name = "lblBankCode";
            this.lblBankCode.Size = new System.Drawing.Size(60, 22);
            this.lblBankCode.TabIndex = 32;
            this.lblBankCode.Text = "Mẫu số  (*)";
            // 
            // lblBankName
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.TextVAlignAsString = "Middle";
            this.lblBankName.Appearance = appearance8;
            this.lblBankName.Location = new System.Drawing.Point(12, 49);
            this.lblBankName.Name = "lblBankName";
            this.lblBankName.Size = new System.Drawing.Size(91, 22);
            this.lblBankName.TabIndex = 33;
            this.lblBankName.Text = "Loại hóa đơn (*)";
            // 
            // txtInvoiceTypeName
            // 
            this.txtInvoiceTypeName.AutoSize = false;
            this.txtInvoiceTypeName.Location = new System.Drawing.Point(130, 76);
            this.txtInvoiceTypeName.MaxLength = 512;
            this.txtInvoiceTypeName.Multiline = true;
            this.txtInvoiceTypeName.Name = "txtInvoiceTypeName";
            this.txtInvoiceTypeName.Size = new System.Drawing.Size(317, 58);
            this.txtInvoiceTypeName.TabIndex = 30;
            // 
            // btnSave
            // 
            appearance9.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance9;
            this.btnSave.Location = new System.Drawing.Point(292, 202);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 68;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click_1);
            // 
            // btnClose
            // 
            appearance10.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance10;
            this.btnClose.Location = new System.Drawing.Point(373, 202);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 69;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click_1);
            // 
            // chkIsActive
            // 
            appearance11.TextVAlignAsString = "Middle";
            this.chkIsActive.Appearance = appearance11;
            this.chkIsActive.BackColor = System.Drawing.Color.Transparent;
            this.chkIsActive.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsActive.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkIsActive.Checked = true;
            this.chkIsActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIsActive.Location = new System.Drawing.Point(13, 206);
            this.chkIsActive.Name = "chkIsActive";
            this.chkIsActive.Size = new System.Drawing.Size(103, 22);
            this.chkIsActive.TabIndex = 70;
            this.chkIsActive.Text = "Hoạt động";
            // 
            // FInvoiceTypeDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(460, 239);
            this.Controls.Add(this.chkIsActive);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.ultraGroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FInvoiceTypeDetail";
            this.Text = "Mẫu Số Hóa Đơn";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FInvoiceTypeDetail_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbb_InvoiceType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceTypeCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceTypeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtInvoiceTypeCode;
        private Infragistics.Win.Misc.UltraLabel lblDescription;
        private Infragistics.Win.Misc.UltraLabel lblBankCode;
        private Infragistics.Win.Misc.UltraLabel lblBankName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtInvoiceTypeName;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsActive;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbb_InvoiceType;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtFromNo;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtToNo;
    }
}