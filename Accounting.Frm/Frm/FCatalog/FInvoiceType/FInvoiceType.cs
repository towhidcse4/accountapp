﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using Accounting.TextMessage;


namespace Accounting
{
    public partial class FInvoiceType : CatalogBase
    {
        #region khai báo
        private readonly IInvoiceTypeService _IInvoiceTypeService;

        static Dictionary<int, string> Dic_InvoiceType;
        public static Dictionary<int, string> dic_InvoiceType
        {
            get { Dic_InvoiceType = Dic_InvoiceType ?? (Dictionary<int, string>)BuildConfig(1); return Dic_InvoiceType; }
        }

        #endregion

        #region khởi tạo
        public FInvoiceType()
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            #endregion

            #region Thiết lập ban đầu cho Form
            _IInvoiceTypeService = IoC.Resolve<IInvoiceTypeService>();
            this.uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            #endregion

            #region Tạo dữ liệu ban đầu
            LoadDuLieu();
            if (uGrid.Rows.Count > 0)
            {
                uGrid.Rows[0].Selected = true;
            }
            #endregion
        }

        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }
        private void LoadDuLieu(bool configGrid)
        {
            WaitingFrm.StartWaiting();
            #region Lấy dữ liệu từ CSDL
            List<InvoiceType> list = _IInvoiceTypeService.GetAll_OrderBy();
            _IInvoiceTypeService.UnbindSession(list);
            list = _IInvoiceTypeService.GetAll_OrderBy();
            #endregion

            #region Xử lý dữ liệu
            //thiết lập tính chất
            foreach (var item in list)
            {
                item._InvoiceTypeView = dic_InvoiceType[item._InvoiceType];
            }
            #endregion

            #region hiển thị và xử lý hiển thị
            uGrid.DataSource = list.ToArray();
            if (configGrid) ConfigGrid(uGrid);
            #endregion
            WaitingFrm.StopWaiting();
        }
        #endregion

        #region Nghiệp vụ
        #region ContentMenu
        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            ResetFunction();
        }
        protected override void ResetFunction()
        {
            LoadDuLieu(false);
        }

        #endregion

        #region Button

        private void btnAdd_Click_1(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void btnEdit_Click_1(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void btnDelete_Click_1(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        #endregion

        private void uGrid_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (e.Row.Index == -1) return;
            EditFunction();
        }

        /// <summary>
        /// Nghiệp vụ Add
        /// </summary>
        protected override void AddFunction()
        {
            new FInvoiceTypeDetail().ShowDialog(this);
            if (!FInvoiceTypeDetail.isClose) LoadDuLieu();
        }

        /// <summary>
        /// Nghiệp vụ Edit
        /// </summary>
        protected override void EditFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                InvoiceType temp = uGrid.Selected.Rows[0].ListObject as InvoiceType;
                new FInvoiceTypeDetail(temp).ShowDialog(this);
                if (!FInvoiceTypeDetail.isClose) LoadDuLieu();
            }
            else
                Accounting.TextMessage.MSG.Error(Accounting.TextMessage.resSystem.MSG_System_04);
        }

        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        protected override void DeleteFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                InvoiceType temp = uGrid.Selected.Rows[0].ListObject as InvoiceType;
                if (MSG.Question(string.Format(resSystem.MSG_System_05, temp.InvoiceTypeCode)) == System.Windows.Forms.DialogResult.Yes)
                {
                    _IInvoiceTypeService.BeginTran();
                    _IInvoiceTypeService.Delete(temp);
                    _IInvoiceTypeService.CommitTran();
                    Utils.ClearCacheByType<InvoiceType>();
                    LoadDuLieu();
                }
            }
            else
                MSG.Error(resSystem.MSG_System_06);
        }
        #endregion

        #region Utils
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, TextMessage.ConstDatabase.InvoiceType_TableName);
        }

        static object BuildConfig(int obj)
        {
            Dictionary<int, string> temp = new Dictionary<int, string>();
            switch (obj)
            {
                case 1:
                    {
                        temp.Add(0, "Hóa đơn giá trị gia tăng");
                        temp.Add(1, "Hóa đơn bán hàng");
                        temp.Add(2, "Phiếu xuất kho kiêm vận chuyển hàng hóa nội bộ");
                        temp.Add(3, "Phiếu xuất kho gửi bán hàng đại lý");
                        temp.Add(4, "Hóa đơn xuất khẩu");
                        temp.Add(5, "Hóa đơn bán hàng (dành cho tổ chức,cá nhân trong khu phi thuế quan)");
                    }
                    break;

            }
            return temp;
        }
        #endregion

        private void FInvoiceType_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGrid.ResetText();
            uGrid.ResetUpdateMode();
            uGrid.ResetExitEditModeOnLeave();
            uGrid.ResetRowUpdateCancelAction();
            uGrid.DataSource = null;
            uGrid.Layouts.Clear();
            uGrid.ResetLayouts();
            uGrid.ResetDisplayLayout();
            uGrid.Refresh();
            uGrid.ClearUndoHistory();
            uGrid.ClearXsdConstraints();
        }

        private void FInvoiceType_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
