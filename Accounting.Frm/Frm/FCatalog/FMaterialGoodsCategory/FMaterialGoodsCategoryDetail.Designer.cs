﻿namespace Accounting
{
    partial class FMaterialGoodsCategoryDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtMaterialGoodsName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblRegistrationGroupName = new Infragistics.Win.Misc.UltraLabel();
            this.cbbParentID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.lblParentID = new Infragistics.Win.Misc.UltraLabel();
            this.txtMaterialGoodsCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblRegistrationGroupCode = new Infragistics.Win.Misc.UltraLabel();
            this.chkIsActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaterialGoodsName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbParentID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaterialGoodsCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            appearance1.FontData.BoldAsString = "True";
            this.ultraGroupBox1.Appearance = appearance1;
            this.ultraGroupBox1.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox1.Controls.Add(this.txtMaterialGoodsName);
            this.ultraGroupBox1.Controls.Add(this.lblRegistrationGroupName);
            this.ultraGroupBox1.Controls.Add(this.cbbParentID);
            this.ultraGroupBox1.Controls.Add(this.lblParentID);
            this.ultraGroupBox1.Controls.Add(this.txtMaterialGoodsCode);
            this.ultraGroupBox1.Controls.Add(this.lblRegistrationGroupCode);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(382, 112);
            this.ultraGroupBox1.TabIndex = 30;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtMaterialGoodsName
            // 
            this.txtMaterialGoodsName.AutoSize = false;
            this.txtMaterialGoodsName.Location = new System.Drawing.Point(155, 54);
            this.txtMaterialGoodsName.MaxLength = 512;
            this.txtMaterialGoodsName.Name = "txtMaterialGoodsName";
            this.txtMaterialGoodsName.Size = new System.Drawing.Size(216, 22);
            this.txtMaterialGoodsName.TabIndex = 6;
            // 
            // lblRegistrationGroupName
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.TextVAlignAsString = "Middle";
            this.lblRegistrationGroupName.Appearance = appearance2;
            this.lblRegistrationGroupName.Location = new System.Drawing.Point(9, 54);
            this.lblRegistrationGroupName.Name = "lblRegistrationGroupName";
            this.lblRegistrationGroupName.Size = new System.Drawing.Size(141, 22);
            this.lblRegistrationGroupName.TabIndex = 22;
            this.lblRegistrationGroupName.Text = "Tên loại VTHH (*)";
            // 
            // cbbParentID
            // 
            this.cbbParentID.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.SuggestAppend;
            this.cbbParentID.AutoSize = false;
            this.cbbParentID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbParentID.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbParentID.Location = new System.Drawing.Point(155, 81);
            this.cbbParentID.Name = "cbbParentID";
            this.cbbParentID.NullText = "<Xin Chọn>";
            this.cbbParentID.Size = new System.Drawing.Size(216, 22);
            this.cbbParentID.TabIndex = 7;
            // 
            // lblParentID
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextVAlignAsString = "Middle";
            this.lblParentID.Appearance = appearance3;
            this.lblParentID.Location = new System.Drawing.Point(9, 81);
            this.lblParentID.Name = "lblParentID";
            this.lblParentID.Size = new System.Drawing.Size(92, 22);
            this.lblParentID.TabIndex = 16;
            this.lblParentID.Text = "Thuộc loại";
            // 
            // txtMaterialGoodsCode
            // 
            this.txtMaterialGoodsCode.AutoSize = false;
            this.txtMaterialGoodsCode.Location = new System.Drawing.Point(155, 27);
            this.txtMaterialGoodsCode.MaxLength = 25;
            this.txtMaterialGoodsCode.Name = "txtMaterialGoodsCode";
            this.txtMaterialGoodsCode.Size = new System.Drawing.Size(216, 22);
            this.txtMaterialGoodsCode.TabIndex = 5;
            // 
            // lblRegistrationGroupCode
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.lblRegistrationGroupCode.Appearance = appearance4;
            this.lblRegistrationGroupCode.Location = new System.Drawing.Point(9, 27);
            this.lblRegistrationGroupCode.Name = "lblRegistrationGroupCode";
            this.lblRegistrationGroupCode.Size = new System.Drawing.Size(140, 22);
            this.lblRegistrationGroupCode.TabIndex = 0;
            this.lblRegistrationGroupCode.Text = "Mã loại VTHH (*)";
            // 
            // chkIsActive
            // 
            appearance5.TextVAlignAsString = "Middle";
            this.chkIsActive.Appearance = appearance5;
            this.chkIsActive.BackColor = System.Drawing.Color.Transparent;
            this.chkIsActive.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsActive.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkIsActive.Checked = true;
            this.chkIsActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIsActive.Location = new System.Drawing.Point(8, 122);
            this.chkIsActive.Name = "chkIsActive";
            this.chkIsActive.Size = new System.Drawing.Size(119, 22);
            this.chkIsActive.TabIndex = 8;
            this.chkIsActive.Text = "Hoạt động";
            this.chkIsActive.ValidateCheckState += new Infragistics.Win.CheckEditor.ValidateCheckStateHandler(this.chkIsActive_ValidateCheckState);
            // 
            // btnClose
            // 
            appearance6.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance6;
            this.btnClose.Location = new System.Drawing.Point(296, 118);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 10;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            appearance7.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance7;
            this.btnSave.Location = new System.Drawing.Point(215, 118);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // FMaterialGoodsCategoryDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(382, 152);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.ultraGroupBox1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.chkIsActive);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FMaterialGoodsCategoryDetail";
            this.Text = "Thêm mới";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FMaterialGoodsCategoryDetail_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtMaterialGoodsName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbParentID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaterialGoodsCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMaterialGoodsName;
        private Infragistics.Win.Misc.UltraLabel lblRegistrationGroupName;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbParentID;
        private Infragistics.Win.Misc.UltraLabel lblParentID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMaterialGoodsCode;
        private Infragistics.Win.Misc.UltraLabel lblRegistrationGroupCode;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsActive;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnSave;
    }
}