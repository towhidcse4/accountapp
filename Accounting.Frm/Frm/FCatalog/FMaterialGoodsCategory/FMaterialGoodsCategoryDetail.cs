﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Accounting.TextMessage;


namespace Accounting
{
    public partial class FMaterialGoodsCategoryDetail : CustormForm
    {
        #region khai báo
        private readonly IMaterialGoodsCategoryService _IMaterialGoodsCategoryService;
        MaterialGoodsCategory _Select = new MaterialGoodsCategory();
        bool IsAdd = true;
        public static bool isClose = true;
        bool checkActive = true;
        List<MaterialGoodsCategory> dsMaterialGoodsCategory = new List<MaterialGoodsCategory>();
        #endregion

        #region khởi tạo
        public FMaterialGoodsCategoryDetail()
        {
            //Thêm
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            this.Text = "Thêm mới Loại vật tư hàng hóa ";
            this.CenterToParent();
            //  this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            // Set the MaximizeBox to false to remove the maximize box.
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            // Set the MinimizeBox to false to remove the minimize box.

            chkIsActive.Visible = false;
            #endregion

            #region Thiết lập ban đầu cho Form
            //Khai báo các webservices
            _IMaterialGoodsCategoryService = IoC.Resolve<IMaterialGoodsCategoryService>();

            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion
            WaitingFrm.StopWaiting();
        }
        public FMaterialGoodsCategoryDetail(MaterialGoodsCategory temp)
        {//Sửa
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            this.Text = "Sửa Loại vật tư hàng hóa, công cụ dụng cụ ";
            this.CenterToParent();
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            #endregion

            #region Thiết lập ban đầu cho Form
            _Select = temp;
            IsAdd = false;
            txtMaterialGoodsCode.Enabled = false;
           
            //Khai báo các webservices
            _IMaterialGoodsCategoryService = IoC.Resolve<IMaterialGoodsCategoryService>();

            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion

            #region Fill dữ liệu Obj vào control
            ObjandGUI(temp, false);
            #endregion
            WaitingFrm.StopWaiting();
        }

        private void InitializeGUI()
        {
            //khởi tạo cbb
            dsMaterialGoodsCategory = _IMaterialGoodsCategoryService.GetAll();
            cbbParentID.DataSource = dsMaterialGoodsCategory;
            cbbParentID.DisplayMember = "MaterialGoodsCategoryCode";
            Utils.ConfigGrid(cbbParentID, ConstDatabase.MaterialGoodsCategory_TableName);
        }
        #endregion

        #region Button Event
        private void btnSave_Click(object sender, EventArgs e)
        {
          //  MaterialGoodsCategory temp1 = dsMaterialGoodsCategory.Single(k => k.ID == _Select.ID);
            if (Utils.checkRelationVoucher_MaterialGoodsCategory(_Select))
            {
                MSG.Error("không thể sửa loại vthh/ccdc vì có phát sinh chứng từ liên quan");
                return;
            }
          
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion

            //Gui to object
            #region Fill dữ liệu control vào obj

            // thuc hien trong tung thao tac (them moi/ cap nhat)
            #endregion

            #region Thao tác CSDL

            try
            {
                _IMaterialGoodsCategoryService.BeginTran();
                if (IsAdd) // them moi
                {
                    MaterialGoodsCategory temp = IsAdd ? new MaterialGoodsCategory() : _IMaterialGoodsCategoryService.Getbykey(_Select.ID);
                    temp = ObjandGUI(temp, true);
                    // set orderfixcode
                    MaterialGoodsCategory temp0 = (MaterialGoodsCategory)Utils.getSelectCbbItem(cbbParentID);
                    //List<string> lstOrderFixCodeChild = _IMaterialGoodsCategoryService.Query.Where(a => a.ParentID == temp.ParentID).Select(a => a.OrderFixCode).ToList();
                    List<string> lstOrderFixCodeChild = _IMaterialGoodsCategoryService.GetOrderFixCode_ByParentID(temp.ParentID);//thành duy đã sửa chỗ này
                    string orderFixCodeParent = temp0 == null ? "" : temp0.OrderFixCode;
                    temp.OrderFixCode = Utils.GetOrderFixCode(lstOrderFixCodeChild, orderFixCodeParent);
                    temp.IsParentNode = false; // mac dinh cho insert
                    temp.IsSecurity = false; // mac dinh cho insert
                    temp.IsTool = false; // mac dinh cho insert
                    temp.Grade = temp0 == null ? 1 : temp0.Grade + 1; // quy tac lay gia tri cho grade
                    //========> input.OrderFixCode chưa xét

                    _IMaterialGoodsCategoryService.CreateNew(temp);
                    // thuc hien cap nhat lai isParentNode cho cha neu co
                    if (temp.ParentID != null)
                    {
                        MaterialGoodsCategory parent = _IMaterialGoodsCategoryService.Getbykey((Guid)temp.ParentID);
                        parent.IsParentNode = true;
                        _IMaterialGoodsCategoryService.Update(parent);
                    }

                }
                else // cap nhat
                {
                    //Lấy về đối tượng cần sửa
                    MaterialGoodsCategory temp = _IMaterialGoodsCategoryService.Getbykey(_Select.ID);
                    #region Thiết lập trạng thái hoạt động cho cha và con
                    //var results = from c in _IMaterialGoodsCategoryService.Query.Where(p => p.MaterialGoodsCode.StartsWith(temp.MaterialGoodsCode))
                    //              select c;

                    //if (!chkIsActive.Checked)
                    //{
                    //    // kiểm tra xem bó có phải là cha ko
                    //    if (_Select.IsParentNode == true)
                    //    {
                    //        // kiểm tra xem  tài khoản có đang hoạt động hay ko
                    //        if (_Select.IsActive == true)
                    //        {
                    //            foreach (var item in results)
                    //            {
                    //                List<string> temp1 = new List<string>() { item.ToString() };
                    //                foreach (string itemString in temp1)
                    //                {
                    //                    item.IsActive = chkIsActive.CheckState == CheckState.Checked ? false : true;
                    //                    _IMaterialGoodsCategoryService.Update(item);
                    //                }
                    //            }
                    //        }
                    //        else
                    //        {
                    //            if (MessageBox.Show("Bạn có muốn thiết lập tất cả các danh mục con của danh mục này sang trạng thái 'Theo dõi' không  ?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    //            {
                    //                foreach (var item in results)
                    //                {
                    //                    List<string> temp1 = new List<string>() { item.ToString() };
                    //                    foreach (string itemString in temp1)
                    //                    {
                    //                        item.IsActive = chkIsActive.CheckState == CheckState.Checked ? false : true;
                    //                        _IMaterialGoodsCategoryService.Update(item);
                    //                    }
                    //                }
                    //            }
                    //        }
                    //    }
                    //    else
                    //    {

                    //        foreach (var item in results)
                    //        {
                    //            List<string> temp1 = new List<string>() { item.ToString() };
                    //            foreach (string itemString in temp1)
                    //            {
                    //                item.IsActive = chkIsActive.CheckState == CheckState.Checked ? false : true;
                    //                _IMaterialGoodsCategoryService.Update(item);
                    //            }
                    //        }
                    //    }
                    //}
                    //else
                    //{
                    //    foreach (var item in results)
                    //    {
                    //        item.IsActive = chkIsActive.CheckState == CheckState.Checked ? false : true;
                    //        _IMaterialGoodsCategoryService.Update(item);
                    //    }
                    //}
                    #endregion

                    //Lưu đối tượng trước khi sửa

                    MaterialGoodsCategory tempOriginal = (MaterialGoodsCategory)Utils.CloneObject(temp);
                    //Lấy về đối tượng chọn trong combobox
                    MaterialGoodsCategory objParentCombox = (MaterialGoodsCategory)Utils.getSelectCbbItem(cbbParentID);
                    //Không thay đổi đối tượng cha trong combobox
                    if (((temp.ParentID == null) && (objParentCombox == null)) || ((temp.ParentID != null) && (objParentCombox != null) && (temp.ParentID == objParentCombox.ID)))
                    {
                        temp = ObjandGUI(temp, true);
                        _IMaterialGoodsCategoryService.Update(temp);
                    }
                    //Thay đổi đối tượng trong combobox
                    else
                    {
                        //Không cho phép chuyển ngược
                        if (objParentCombox != null)
                        {
                            //List<MaterialGoodsCategory> lstChildCheck =_IMaterialGoodsCategoryService.Query.Where(p => p.OrderFixCode.StartsWith(tempOriginal.OrderFixCode)).ToList();
                            List<MaterialGoodsCategory> lstChildCheck = _IMaterialGoodsCategoryService.GetStartsWith_OrderFixCode(tempOriginal.OrderFixCode);//thành duy đã sửa chỗ này
                            foreach (var item in lstChildCheck)
                            {
                                if (objParentCombox.ID == item.ID)
                                {
                                    MSG.Warning(string.Format(resSystem.MSG_Catalog6, temp.MaterialGoodsCategoryCode));
                                    return;
                                }
                            }
                        }
                        //Trường hợp có cha cũ
                        //Kiểm tra cha cũ có còn con ngoài con chuyển đi hay không
                        //Chuyển trạng thái IsParentNode Cha cũ nếu hết con
                        if (temp.ParentID != null)
                        {
                            MaterialGoodsCategory oldParent = _IMaterialGoodsCategoryService.Getbykey((Guid)temp.ParentID);
                            //int checkChildOldParent = _IMaterialGoodsCategoryService.Query.Count(p => p.ParentID == temp.ParentID);
                            int checkChildOldParent = _IMaterialGoodsCategoryService.CountByParentID(temp.ParentID); //thành duy đã sửa chỗ này
                            if (checkChildOldParent <= 1)
                            {
                                oldParent.IsParentNode = false;
                            }
                            _IMaterialGoodsCategoryService.Update(oldParent);
                        }
                        //Trường hợp có cha mới
                        //Chuyển trạng IsParentNode cha mới
                        if (objParentCombox != null)
                        {
                            MaterialGoodsCategory newParent = _IMaterialGoodsCategoryService.Getbykey(objParentCombox.ID);
                            newParent.IsParentNode = true;
                            _IMaterialGoodsCategoryService.Update(newParent);

                            //List<MaterialGoodsCategory> listChildNewParent = _IMaterialGoodsCategoryService.Query.Where(a => a.ParentID == objParentCombox.ID).ToList();
                            List<MaterialGoodsCategory> listChildNewParent = _IMaterialGoodsCategoryService.GetAll_ByParentID(objParentCombox.ID);
                            //Tính lại OrderFixCode cho đối tượng chính
                            List<string> lstOfcChildNewParent = new List<string>();
                            foreach (var item in listChildNewParent)
                            {
                                lstOfcChildNewParent.Add(item.OrderFixCode);
                            }
                            string orderFixCodeParent = newParent.OrderFixCode;
                            temp.OrderFixCode = Utils.GetOrderFixCode(lstOfcChildNewParent, orderFixCodeParent);
                            //Tính lại bậc cho đối tượng chính
                            temp.Grade = newParent.Grade + 1;
                        }
                        //Trường hợp không có cha mới tức là chuyển đối tượng lên bậc 1
                        if (objParentCombox == null)
                        {
                            //Lấy về tất cả đối tượng bậc 1
                            List<int> lstOfcChildGradeNo1 = new List<int>();
                            //List<MaterialGoodsCategory> listChildGradeNo1 = _IMaterialGoodsCategoryService.Query.Where(a => a.Grade == 1).ToList();
                            List<MaterialGoodsCategory> listChildGradeNo1 = _IMaterialGoodsCategoryService.GetAll_ByGrade(1);//thành duy đã sửa chỗ này
                            foreach (var item in listChildGradeNo1)
                            {
                                lstOfcChildGradeNo1.Add(Convert.ToInt32(item.OrderFixCode));
                            }
                            //Tính lại OrderFixCode cho đối tượng chính
                            temp.OrderFixCode = (lstOfcChildGradeNo1.Max() + 1).ToString(CultureInfo.InvariantCulture);
                            //Tính lại bậc cho đối tượng chính
                            temp.Grade = 1;
                            //temp.IsParentNode = false;//set mac dinh
                            //temp.IsSecurity = false;
                            //temp.IsTool = false;
                            temp = ObjandGUI(temp, true);
                            _IMaterialGoodsCategoryService.Update(temp);
                        }
                        //Xử lý các con của nó 
                        //Lấy về các con
                        //List<MaterialGoodsCategory> lstChild =_IMaterialGoodsCategoryService.Query.Where(p => p.OrderFixCode.StartsWith(tempOriginal.OrderFixCode)).ToList();
                        List<MaterialGoodsCategory> lstChild = _IMaterialGoodsCategoryService.GetStartsWith_OrderFixCode(tempOriginal.OrderFixCode);//thành duy đã sửa chỗ này
                        foreach (var item in lstChild)
                        {
                            string tempOldFixCode = tempOriginal.OrderFixCode;
                            item.OrderFixCode = temp.OrderFixCode + item.OrderFixCode.Substring(tempOldFixCode.Length);
                            item.Grade = temp.Grade - tempOriginal.Grade + item.Grade;
                            _IMaterialGoodsCategoryService.Update(item);

                        }
                        temp = ObjandGUI(temp, true);
                        _IMaterialGoodsCategoryService.Update(temp);
                    }
                    //nếu cha ngừng theo dõi thì con cũng ngừng theo dõi
                    if (!checkActive)
                    {
                        List<MaterialGoodsCategory> lstChild2 = _IMaterialGoodsCategoryService.GetStartsWith_OrderFixCode(tempOriginal.OrderFixCode);//thành duy đã sửa chỗ này
                                                                                                                                                     //List<MaterialGoodsCategory> lstChild2 =_IMaterialGoodsCategoryService.Query.Where(p => p.OrderFixCode.StartsWith(tempOriginal.OrderFixCode)).ToList();
                        if (!chkIsActive.Checked)
                        {
                            foreach (var item2 in lstChild2)
                            {
                                item2.IsActive = false;
                                _IMaterialGoodsCategoryService.Update(item2);
                            }

                        }
                        else
                        {
                            if (temp.IsParentNode)
                            {
                                if (MSG.Question(string.Format(resSystem.MSG_Catalog_Tree3, "loại VTHH/CCDC")) == System.Windows.Forms.DialogResult.Yes)
                                {
                                    foreach (var item2 in lstChild2)
                                    {
                                        item2.IsActive = true;
                                        _IMaterialGoodsCategoryService.Update(item2);
                                    }
                                }
                            }

                        }

                        //nếu cha isactive = false thì con ko được isactive = true,

                        if (temp.IsParentNode == false)
                        {
                            //lấy cha

                            if (temp.ParentID == null)
                            {
                                if (!CheckError()) return;
                                //if (!chkIsActive.Checked) temp.IsActive = false;
                                //else temp.IsActive = true;
                                //_IMaterialGoodsCategoryService.Update(temp);
                            }

                            else
                            {
                                MaterialGoodsCategory Parent = _IMaterialGoodsCategoryService.Getbykey(objParentCombox.ID);
                                if (Parent.IsActive == true)
                                {
                                    if (!CheckError()) return;
                                    //if (!chkIsActive.Checked) temp.IsActive = false;
                                    //else temp.IsActive = true;
                                    //_IMaterialGoodsCategoryService.Update(temp);
                                }
                                else
                                {
                                    if (!chkIsActive.Checked)
                                        MSG.Warning(string.Format(resSystem.MSG_Catalog_Tree2, "loại VTHH/CCDC"));
                                    _IMaterialGoodsCategoryService.RolbackTran();
                                }
                            }

                        }

                        else //isParentNode = true
                        {
                            if (temp.ParentID != null) // kiem tra xem có cha không
                            {
                                MaterialGoodsCategory Parent = _IMaterialGoodsCategoryService.Getbykey(objParentCombox.ID);
                                if (Parent.IsActive == true)
                                {
                                    if (!CheckError()) return;
                                    //if (chkIsActive.Checked) temp.IsActive = false;
                                    //else temp.IsActive = true;
                                    //_IMaterialGoodsCategoryService.Update(temp);
                                }
                                else
                                {
                                    if (!chkIsActive.Checked)
                                        MSG.Error(string.Format(resSystem.MSG_Catalog_Tree2, "loại VTHH/CCDC"));
                                    _IMaterialGoodsCategoryService.RolbackTran();
                                }
                            }
                            else
                            {
                                if (!CheckError()) return;
                                //if (!chkIsActive.Checked) temp.IsActive = false;
                                //else temp.IsActive = true;
                                //_IMaterialGoodsCategoryService.Update(temp);
                            }
                        }
                    }
                }

                _IMaterialGoodsCategoryService.CommitTran();
            }
            catch (Exception ex)
            {
                _IMaterialGoodsCategoryService.RolbackTran();
            }
            #endregion

            #region xử lý form, kết thúc form
            this.Close();
            isClose = false;
            #endregion
        }

        private void btnClose_Click(object sender, EventArgs e)
        {

            isClose = true;
            this.Close();
            
        }
        #endregion

        #region Utils
        /// <summary>
        /// get hoặc set một đối tượng vào giao diện xử lý
        /// </summary>
        /// <param name="input">đối tượng</param>
        /// <param name="isGet">True: get giá trị từ control gán vào đối tượng| False: set giá trị đối tượng vào control</param>
        /// <returns></returns>
        MaterialGoodsCategory ObjandGUI(MaterialGoodsCategory input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();  //Thêm mới

                input.MaterialGoodsCategoryCode = txtMaterialGoodsCode.Text;
                input.MaterialGoodsCategoryName = txtMaterialGoodsName.Text;
                MaterialGoodsCategory temp0 = (MaterialGoodsCategory)Utils.getSelectCbbItem(cbbParentID);
                //Utils.CloneObject(temp0);
                if (temp0 == null) input.ParentID = null;
                else input.ParentID = temp0.ID;
                input.IsTool = false;
                input.IsActive = chkIsActive.Checked;
                input.IsSecurity = false;
            }
            else
            {

                txtMaterialGoodsCode.Text = input.MaterialGoodsCategoryCode;
                txtMaterialGoodsName.Text = input.MaterialGoodsCategoryName;

                foreach (var item in cbbParentID.Rows)
                {
                    if ((item.ListObject as MaterialGoodsCategory).ID == input.ParentID) cbbParentID.SelectedRow = item;
                }

                chkIsActive.Checked = input.IsActive;
            }
            return input;
        }

        bool CheckError()
        {
            bool kq = true;
            //Check Error Chung

            //Check Error Riêng từng Form
            if (string.IsNullOrEmpty(txtMaterialGoodsCode.Text) || string.IsNullOrEmpty(txtMaterialGoodsName.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }

            //Kiểm tra mã đã tồn tại
            //if (dsMaterialGoodsCategory.Select(k => k.MaterialGoodsCode).Contains(txtMaterialGoodsCode.Text) && IsAdd)
            //{
            //    MSG.Error("Dữ liệu đã có trong CSDL");
            //    return false;
            //}
            // List<string> list = _IMaterialGoodsCategoryService.Query.Select(p => p.MaterialGoodsCode).ToList();
            List<string> list = _IMaterialGoodsCategoryService.GetMaterialGoodsCode();
            foreach (var item in list)
            {
                if (item == txtMaterialGoodsCode.Text && IsAdd)
                {
                    MSG.Warning(string.Format(resSystem.MSG_Catalog, "Mã", item));
                    return false;
                }
            }
            return kq;
        }
        #endregion

        private void chkIsActive_ValidateCheckState(object sender, Infragistics.Win.ValidateCheckStateEventArgs e)
        {
            checkActive = false;
        }

        private void FMaterialGoodsCategoryDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
