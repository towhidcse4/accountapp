﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using Accounting.TextMessage;
using System.Data;
using Infragistics.Win.UltraWinTree;

namespace Accounting
{
    public partial class FMaterialGoodsCategory : CatalogBase //UserControl
    {
        #region khai báo
        private readonly IMaterialGoodsCategoryService _IMaterialGoodsCategoryService;
        List<MaterialGoodsCategory> dsMaterialGoodsCategory = new List<MaterialGoodsCategory>();
        #endregion

        #region khởi tạo
        public FMaterialGoodsCategory()
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            ToolTip ToolTip1 = new ToolTip();
            ToolTip1.SetToolTip(btnAdd, "Thêm (Ctrl + N)");
            ToolTip ToolTip2 = new ToolTip();
            ToolTip2.SetToolTip(btnEdit, "Sửa (Ctrl + E)");
            ToolTip ToolTip3 = new ToolTip();
            ToolTip3.SetToolTip(btnDelete, "Xóa (Ctrl + D)");
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            #endregion

            #region Thiết lập ban đầu cho Form
            _IMaterialGoodsCategoryService = IoC.Resolve<IMaterialGoodsCategoryService>();

            this.uTree.InitializeDataNode += new Infragistics.Win.UltraWinTree.InitializeDataNodeEventHandler(this.uTree_InitializeDataNode);
            this.uTree.AfterDataNodesCollectionPopulated += new Infragistics.Win.UltraWinTree.AfterDataNodesCollectionPopulatedEventHandler(Utils.uTree_AfterDataNodesCollectionPopulated);
            this.uTree.ColumnSetGenerated += new Infragistics.Win.UltraWinTree.ColumnSetGeneratedEventHandler(this.uTree_ColumnSetGenerated);
            this.uTree.MouseDown += new System.Windows.Forms.MouseEventHandler(this.uTree_MouseDown);
            this.uTree.DoubleClick += new System.EventHandler(this.uTree_DoubleClick);
            #endregion

            #region Tạo dữ liệu ban đầu
            LoadDuLieu();
            #endregion
        }

        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }
        private void LoadDuLieu(bool configTree)
        {
            WaitingFrm.StartWaiting();
            #region Lấy dữ liệu từ CSDL
            dsMaterialGoodsCategory = _IMaterialGoodsCategoryService.GetAll_OrderBy();
            _IMaterialGoodsCategoryService.UnbindSession(dsMaterialGoodsCategory);
            dsMaterialGoodsCategory = _IMaterialGoodsCategoryService.GetAll_OrderBy();
            #endregion

            #region Xử lý dữ liệu
            #endregion

            #region hiển thị và xử lý hiển thị
            DataSet ds = Utils.ToDataSet<MaterialGoodsCategory>(dsMaterialGoodsCategory, ConstDatabase.MaterialGoodsCategory_TableName);
  
           
            uTree.SetDataBinding(ds, ConstDatabase.MaterialGoodsCategory_TableName);
            
            if (configTree) ConfigTree(uTree);
            //uTree.SelectedNodes[0].Selected = true ;
            uTree.ColumnSettings.ColumnSets[0].Columns["Unit"].Visible = false;


            #endregion
            WaitingFrm.StopWaiting();
        }
        #endregion

        #region Nghiệp vụ
        #region ContentMenu
        private void tsmAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void tsmEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            ResetFunction();
        }
        protected override void ResetFunction()
        {
            LoadDuLieu(false);
        }
        #endregion

        #region Button
        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }
        #endregion

        /// <summary>
        /// Nghiệp vụ Add
        /// </summary>
        protected override void AddFunction()
        {
            WaitingFrm.StartWaiting();
            new FMaterialGoodsCategoryDetail().ShowDialog(this);
            if (!FMaterialGoodsCategoryDetail.isClose)
            {
                Utils.ClearCacheByType<MaterialGoodsCategory>();
                LoadDuLieu();
            }
        }

        /// <summary>
        /// Nghiệp vụ Edit
        /// </summary>
        protected override void EditFunction()
        {
            if (uTree.SelectedNodes.Count == 0 && uTree.ActiveNode != null)
            {
                uTree.ActiveNode.Selected = true;
            }
            if (uTree.SelectedNodes.Count > 0)
            {
                //MaterialGoodsCategory temp = uGrid.Selected.Rows[0].ListObject as MaterialGoodsCategory;
                MaterialGoodsCategory temp = dsMaterialGoodsCategory.Single(k => k.ID == (new Guid(uTree.SelectedNodes[0].Text)));
                new FMaterialGoodsCategoryDetail(temp).ShowDialog(this);
                //if (Utils.checkRelationVoucher_MaterialGoodsCategory(temp))
                //{
                //    MSG.Error("Không thể sửa Loại VTHH/CCDC vì có phát sinh chứng từ liên quan");
                //    return;
                //}
                //else
                //{
                //    new FMaterialGoodsCategoryDetail(temp).ShowDialog(this);
                if (!FMaterialGoodsCategoryDetail.isClose)
                {
                    Utils.ClearCacheByType<MaterialGoodsCategory>();
                    LoadDuLieu();
                }
                //}
            }
            else
                Accounting.TextMessage.MSG.Error(string.Format(Accounting.TextMessage.resSystem.MSG_Catalog3,"một loại VTHH/CCDC"));
        }

        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        protected override void DeleteFunction()
        {
            if (uTree.SelectedNodes.Count == 0 && uTree.ActiveNode != null)
            {
                uTree.ActiveNode.Selected = true;
            }
            if (uTree.SelectedNodes.Count > 0)
            {
                MaterialGoodsCategory temp = dsMaterialGoodsCategory.Single(k => k.ID == (new Guid(uTree.SelectedNodes[0].Text)));
                if (MSG.Question(string.Format(resSystem.MSG_System_05, temp.MaterialGoodsCategoryCode)) == System.Windows.Forms.DialogResult.Yes)
                {
                    if (Utils.checkRelationVoucher_MaterialGoodsCategory(temp))
                    {
                        MSG.Error("Không thể xóa Loại VTHH/CCDC vì có phát sinh chứng từ liên quan");
                        return;
                    }
                    WaitingFrm.StartWaiting();
                    _IMaterialGoodsCategoryService.BeginTran();
                    //Kiểm tra xem đối tượng có con không nếu có con thì không được phép xóa
                    //List<MaterialGoodsCategory> lstChild = _IMaterialGoodsCategoryService.Query.Where(p => p.ParentID == temp.ID).ToList();
                    List<MaterialGoodsCategory> lstChild = _IMaterialGoodsCategoryService.GetAll_ByParentID(temp.ID);
                    if (lstChild.Count > 0)
                    {
                        WaitingFrm.StopWaiting();
                        MSG.Warning(string.Format(resSystem.MSG_Catalog_Tree, "VTHH/CCDC"));
                        return;
                    }
                    //Check xem cha còn có con khác không nếu không thì chuyển trạng thái IsParentNode = false
                    if (temp.ParentID != null)
                    {
                        MaterialGoodsCategory parent = _IMaterialGoodsCategoryService.Getbykey((Guid)temp.ParentID);
                        int checkChildOldParent = _IMaterialGoodsCategoryService.Query.Count(p => p.ParentID == temp.ParentID);
                        if (checkChildOldParent <= 1)
                        {
                            parent.IsParentNode = false;
                        }
                        _IMaterialGoodsCategoryService.Update(parent);
                        _IMaterialGoodsCategoryService.Delete(temp);
                    }

                    if (temp.ParentID == null)
                    {
                        _IMaterialGoodsCategoryService.Delete(temp);
                    }
                    _IMaterialGoodsCategoryService.CommitTran();
                    WaitingFrm.StopWaiting();
                    Utils.ClearCacheByType<MaterialGoodsCategory>();
                    LoadDuLieu();
                }
            }
            else
                MSG.Error(string.Format(resSystem.MSG_Catalog2,"một loại VTHH/CCDC"));
            
        }
        #endregion

        #region Event
        private void uTree_MouseDown(object sender, MouseEventArgs e)
        {//khi click chuột phải hiện content menu
            Utils.uGrid_MouseDown(sender, e, cms4Grid);
        }

        private void uTree_DoubleClick(object sender, EventArgs e)
        {//click đúp chuột để sửa
            EditFunction();
        }

        public void uTree_InitializeDataNode(object sender, InitializeDataNodeEventArgs e)
        {
            Utils.uTree_InitializeDataNode(sender, e, ConstDatabase.MaterialGoodsCategory_TableName);
        }

        private void uTree_ColumnSetGenerated(object sender, ColumnSetGeneratedEventArgs e)
        {
            Utils.uTree_ColumnSetGenerated(sender, e, ConstDatabase.MaterialGoodsCategory_TableName);
        }
        #endregion

        #region Utils
        void ConfigTree(Infragistics.Win.UltraWinTree.UltraTree ultraTree)
        {//hàm chung
            Utils.ConfigTree(ultraTree, TextMessage.ConstDatabase.MaterialGoodsCategory_TableName);
        }
        #endregion

        private void FMaterialGoodsCategory_FormClosing(object sender, FormClosingEventArgs e)
        {
            uTree = null;
        }

        private void FMaterialGoodsCategory_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
