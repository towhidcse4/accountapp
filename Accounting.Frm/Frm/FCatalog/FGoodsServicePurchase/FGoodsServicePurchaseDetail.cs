﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;

namespace Accounting
{
    public partial class FGoodsServicePurchaseDetail : CustormForm
    {
        #region khai báo
        private readonly IGoodsServicePurchaseService _IGoodsServicePurchaseService;

        GoodsServicePurchase _Select = new GoodsServicePurchase();
        private Guid _id;
        public Guid Id
        {
            get { return _id; }
            set { _id = value; }
        }
        bool Them = true;
        public static bool isClose = true;
        #endregion

        #region khởi tạo
        public FGoodsServicePurchaseDetail()
        {//Thêm
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            this.Text = "Thêm mới Nhóm hàng hóa,dịch vụ mua vào";
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.CenterToParent();
            chkIsActive.Visible = false;
            #endregion

            #region Thiết lập ban đầu cho Form
            //Khai báo các webservices
            _IGoodsServicePurchaseService = IoC.Resolve<IGoodsServicePurchaseService>();

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion
        }

        public FGoodsServicePurchaseDetail(GoodsServicePurchase temp)
        {//Sửa
            #region Khởi tạo giá trị mặc định của Form

            InitializeComponent();
            //khai báo các webservice
            _IGoodsServicePurchaseService = IoC.Resolve<IGoodsServicePurchaseService>();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.CenterToParent();
            #endregion
            this.Text = "Sửa Nhóm hàng hóa,dịch vụ mua vào";
            #region Thiết lập ban đầu cho Form
            _Select = temp;
            Them = false;
            txtGoodsServicePurchaseCode.Enabled = false;

            //Khai báo các webservices


            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion

            #region Fill dữ liệu Obj vào control
            ObjandGUI(temp, false);
            #endregion
        }
        private void InitializeGUI()
        {
            //
        }
        #endregion

        #region Button Event
        /// Sự kiện nút Lưu lại
        private void btnSave_Click(object sender, EventArgs e)
        {
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion

            //Gui to object
            #region Fill dữ liệu control vào obj
            GoodsServicePurchase temp = Them ? new GoodsServicePurchase() : _IGoodsServicePurchaseService.Getbykey(_Select.ID);
            temp = ObjandGUI(temp, true);
            #endregion

            #region Thao tác CSDL
            //_IGoodsServicePurchaseService.BeginTran();
            if (Them) _IGoodsServicePurchaseService.CreateNew(temp);
            else _IGoodsServicePurchaseService.Update(temp);
            _IGoodsServicePurchaseService.CommitChanges();
            _id = temp.ID;
            #endregion

            #region xử lý form, kết thúc form
            Utils.ListGoodsServicePurchase.Clear();
            //Utils.AddToBindingList(Utils.ListGoodsServicePurchase, _IGoodsServicePurchaseService.GetAll());
            this.Close();
            isClose = false;
            #endregion
        }


        /// Sự kiện nút đóng

        private void btnClose_Click(object sender, EventArgs e)
        {
            isClose = true;
            DialogResult = DialogResult.Cancel;
            this.Close();
        }
        #endregion

        #region Utils

        /// get hoặc set một đối tượng vào giao diện xử lý

        GoodsServicePurchase ObjandGUI(GoodsServicePurchase input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();  //Thêm mới

                input.GoodsServicePurchaseCode = txtGoodsServicePurchaseCode.Text;
                input.GoodsServicePurchaseName = txtGoodsServicePurchaseName.Text;
                input.Description = txtDescription.Text;
                input.IsActive = chkIsActive.Checked;
            }
            else
            {
                txtGoodsServicePurchaseCode.Text = input.GoodsServicePurchaseCode;
                txtGoodsServicePurchaseName.Text = input.GoodsServicePurchaseName;
                txtDescription.Text = input.Description;
                chkIsActive.Checked = input.IsActive;
            }
            return input;
        }

        bool CheckError()
        {
            bool kq = true;
            //Check Error Chung

            //Check Error Riêng từng Form
            if (string.IsNullOrEmpty(txtGoodsServicePurchaseCode.Text) || string.IsNullOrEmpty(txtGoodsServicePurchaseName.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }
            //List<string> list = _IGoodsServicePurchaseService.Query.Select(p => p.GoodsServicePurchaseCode).ToList();
            List<string> list = _IGoodsServicePurchaseService.GetListGoodServicePurchaseCode();

            foreach (var item in list)
            {
                if (item == txtGoodsServicePurchaseCode.Text && Them)
                {
                    MSG.Error(string.Format(resSystem.MSG_Catalog_Account6,"nhóm HHDV mua vào"));
                    return false;
                }
            }

            return kq;
        }
        #endregion

        private void FGoodsServicePurchaseDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
