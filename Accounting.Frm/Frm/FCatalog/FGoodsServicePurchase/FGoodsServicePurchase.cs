﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Accounting.TextMessage;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FGoodsServicePurchase : CatalogBase
    {
        #region khai báo
        private readonly IGoodsServicePurchaseService _IGoodsServicePurchaseService;
        #endregion

        #region khởi tạo
        public FGoodsServicePurchase()
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            #endregion

            #region Thiết lập ban đầu cho Form
            _IGoodsServicePurchaseService = IoC.Resolve<IGoodsServicePurchaseService>();
            this.uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            #endregion

            #region Tạo dữ liệu ban đầu
            LoadDuLieu();
            #endregion
            ToolTip ToolTip1 = new ToolTip();
            ToolTip1.SetToolTip(btnAdd, "Thêm (Ctrl + N)");
            ToolTip ToolTip2 = new ToolTip();
            ToolTip2.SetToolTip(btnEdit, "Xem (Ctrl + E)");
            ToolTip ToolTip3 = new ToolTip();
            ToolTip3.SetToolTip(btnDelete, "Xóa (Ctrl + D)");
            if (uGrid.Rows.Count > 0) uGrid.Rows[0].Selected = true;
        }

        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }
        private void LoadDuLieu(bool configGrid)
        {
            WaitingFrm.StartWaiting();
            #region Lấy dữ liệu từ CSDL
            List<GoodsServicePurchase> list = _IGoodsServicePurchaseService.GetAll_OrderBy();
            _IGoodsServicePurchaseService.UnbindSession(list);
            list = _IGoodsServicePurchaseService.GetAll_OrderBy();
            #endregion

            #region Xử lý dữ liệu
            #endregion

            #region hiển thị và xử lý hiển thị
            uGrid.DataSource = list.ToArray();
            if (uGrid.Rows.Count > 0)
            {
                uGrid.Rows[0].Selected = true;
            }
            if (configGrid) ConfigGrid(uGrid);

            #endregion
            WaitingFrm.StopWaiting();
        }
        #endregion

        #region Nghiệp vụ
        #region ContentMenu
        private void tmsReLoad_Click_1(object sender, EventArgs e)
        {
            ResetFunction();
        }
        protected override void ResetFunction()
        {
            LoadDuLieu();
        }

        #endregion

        #region Button

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        #endregion


        /// <summary>
        /// Nghiệp vụ Add
        /// </summary>
        protected override void AddFunction()
        {
            new FGoodsServicePurchaseDetail().ShowDialog(this);
            if (!FGoodsServicePurchaseDetail.isClose) LoadDuLieu();
        }

        /// <summary>
        /// Nghiệp vụ Edit
        /// </summary>
        protected override void EditFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                GoodsServicePurchase temp = uGrid.Selected.Rows[0].ListObject as GoodsServicePurchase;
                new FGoodsServicePurchaseDetail(temp).ShowDialog(this);
                if (!FGoodsServicePurchaseDetail.isClose) LoadDuLieu();
            }
            else
                Accounting.TextMessage.MSG.Error(string.Format(Accounting.TextMessage.resSystem.MSG_Catalog3,"một nhóm HHDV mua vào"));
        }

        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        protected override void DeleteFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                GoodsServicePurchase temp = uGrid.Selected.Rows[0].ListObject as GoodsServicePurchase;
                if (MSG.Question(string.Format(resSystem.MSG_System_05, temp.GoodsServicePurchaseCode)) == System.Windows.Forms.DialogResult.Yes)
                {
                    _IGoodsServicePurchaseService.BeginTran();
                    _IGoodsServicePurchaseService.Delete(temp);
                    _IGoodsServicePurchaseService.CommitTran();
                    Utils.ClearCacheByType<GoodsServicePurchase>();
                    LoadDuLieu();
                }
            }
            else
                MSG.Error(string.Format(resSystem.MSG_Catalog2,"một nhóm HHDV mua vào"));
        }
        #endregion

        #region Utils
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, TextMessage.ConstDatabase.GoodsServicePurchase_TableName);
        }
        #endregion

        private void uGrid_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (e.Row.Index == -1) return;
            EditFunction();
        }

        private void FGoodsServicePurchase_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGrid.ResetText();
            uGrid.ResetUpdateMode();
            uGrid.ResetExitEditModeOnLeave();
            uGrid.ResetRowUpdateCancelAction();
            uGrid.DataSource = null;
            uGrid.Layouts.Clear();
            uGrid.ResetLayouts();
            uGrid.ResetDisplayLayout();
            uGrid.Refresh();
            uGrid.ClearUndoHistory();
            uGrid.ClearXsdConstraints();
        }

        private void FGoodsServicePurchase_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
