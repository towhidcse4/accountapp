﻿using System;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;

namespace Accounting
{
    public partial class FAccountGroupDetail : DialogForm //UserControl
    {
        #region khai báo
        private readonly IAccountGroupService _IAccountGroupService;

        AccountGroup _Select = new AccountGroup();

        bool Them = true;
        public static bool isClose = true;
        #endregion

        #region Khởi tạo
        public FAccountGroupDetail()
        {//Thêm
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            //Khai báo các webservices
            _IAccountGroupService = IoC.Resolve<IAccountGroupService>();

            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion
            cbbDetailType.Enabled = false;
            //Chỉnh sửa dữ liệu
            //----
            this.Text = "Thêm Nhóm Tài Khoản";
            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion
        }

        public FAccountGroupDetail(AccountGroup temp)
        {//Sửa
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _Select = temp;
            Them = false;
            txtID.Enabled = false;

            //Khai báo các webservices
            _IAccountGroupService = IoC.Resolve<IAccountGroupService>();

            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion

            #region Fill dữ liệu Obj vào control
            ObjandGUI(temp, false);
            this.Text = "Sửa nhóm tài khoản";
            #endregion
        }

        /// <summary>
        /// Khởi tạo giá trị ban đầu cho control
        /// </summary>
        private void InitializeGUI()
        {
            // cbbDetailType.Enabled = false;

            // //khởi tạo cbb tính chất

            cbbAccountGroupKind.DataSource = Utils.DicAccountKind.Values.Where(k => k.Visible == "1").ToList();
            cbbAccountGroupKind.DisplayMember = "Name";
            cbbAccountGroupKind.ValueMember = "Value";
            cbbAccountGroupKind.SelectedIndex = 0;
            // //khởi tạo cbb chi tiết theo
            // ultraOptionSet1.DataSource = Utils.DicDetailType.Values.Where(k => k.Visible == "1" && TextMessage.ConstFrm.DetailType_of_FAccountGroup.Contains(k.Value)).ToList();
            //// cbbDetailType.DataSource = Utils.DicDetailType.Values.Where(k => k.Visible == "1" && TextMessage.ConstFrm.DetailType_of_FAccount.Contains(k.Value)).ToList();
            ////cbbDetailType.DisplayMember = "Name";
            ////cbbDetailType.ValueMember = "Value";
            ////cbbDetailType.SelectedIndex = 0;
            // //cbbDetailType.DisplayMember = "Name";
            // //cbbDetailType.ValueMember = "Value";
            // //this.ultraOptionSet1.Items.Add("Value", "Name");
            // ultraOptionSet1.DisplayMember = "Name";
            // ultraOptionSet1.ValueMember = "Value";

            // //MessageBox.Show(ultraOptionSet1.Items[1].ToString());

            // //ultraOptionSet1.Enabled = false;

            // //this.ultraOptionSet1.Items.Add(2, "Two");
            // //this.ultraOptionSet1.Items.Add(3, "Three");
            // //this.ultraOptionSet1.Items.Add(4, "Four");
        }
        #endregion

        #region Button Event
        /// <summary>
        /// Sự kiện nút Lưu lại
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {

            #region Check Lỗi
            if (!CheckError()) return;
            #endregion

            //Gui to object
            #region Fill dữ liệu control vào obj
            AccountGroup temp = Them ? new AccountGroup() : _IAccountGroupService.Getbykey(_Select.ID);
            temp = ObjandGUI(temp, true);
            #endregion

            #region Thao tác CSDL
            _IAccountGroupService.BeginTran();
            if (Them) _IAccountGroupService.CreateNew(temp);
            else _IAccountGroupService.Update(temp);
            _IAccountGroupService.CommitTran();
            #endregion

            #region xử lý form, kết thúc form
            this.Close();
            isClose = false;
            #endregion
        }

        /// <summary>
        /// Sự kiện nút đóng
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            isClose = true;
            this.Close();
        }
        #endregion

        #region Utils
        /// <summary>
        /// get hoặc set một đối tượng vào giao diện xử lý
        /// </summary>
        /// <param name="input">đối tượng</param>
        /// <param name="isGet">True: get giá trị từ control gán vào đối tượng| False: set giá trị đối tượng vào control</param>
        /// <returns></returns>
        AccountGroup ObjandGUI(AccountGroup input, bool isGet)
        {
            if (isGet)
            {

                input.ID = txtID.Text;
                input.AccountGroupName = txtAccountGroupName.Text;
                input.AccountGroupKind = cbbAccountGroupKind.SelectedIndex;

                if (chkDetailType.CheckState == CheckState.Unchecked)
                {
                    input.DetailType = "-1";
                }
                else
                {
                    if (ultraOptionSet1.CheckedIndex != 0)
                    {
                        input.DetailType = (chkDetailType.CheckState == CheckState.Checked) ? ((ultraOptionSet1.CheckedIndex + 2).ToString()) : "-1";
                    }
                    else
                    {
                        input.DetailType = cbbDetailType.SelectedIndex.ToString();
                    }
                }
                //input.DetailType = cbbDetailType.SelectedIndex.ToString();

            }
            else
            {

                txtID.Text = input.ID;
                txtAccountGroupName.Text = input.AccountGroupName;
                cbbAccountGroupKind.SelectedIndex = input.AccountGroupKind;

                if (input.DetailType == "-1")
                {
                    chkDetailType.CheckState = CheckState.Unchecked;
                    //ultraOptionSet1.Items[ultraOptionSet1.CheckedIndex].CheckState = CheckState.Unchecked;
                    cbbDetailType.Enabled = false;
                    ultraOptionSet1.Enabled = false;
                }
                else
                {
                    chkDetailType.CheckState = CheckState.Checked;
                    if (int.Parse(input.DetailType) >= 3)
                    {
                        ultraOptionSet1.CheckedIndex = (int.Parse(input.DetailType)) - 2;
                        cbbDetailType.Enabled = false;
                    }
                    else
                    {
                        ultraOptionSet1.CheckedIndex = 0;
                        cbbDetailType.SelectedIndex = int.Parse(input.DetailType);
                    }
                }
                if (ultraOptionSet1.CheckedIndex > 0)
                {
                    cbbDetailType.Enabled = false;
                }
            }
            return input;
        }

        bool CheckError()
        {
            bool kq = true;
            //Check Error Chung

            //Check Error Riêng từng Form
            if (string.IsNullOrEmpty(txtID.Text) || string.IsNullOrEmpty(txtAccountGroupName.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }
            return kq;
        }
        #endregion

        private void chkDetailType_CheckedChanged(object sender, EventArgs e)
        {
            if (!chkDetailType.Checked)
            {

                ultraOptionSet1.Enabled = false;
                cbbDetailType.Enabled = false;
            }
            else
            {

                ultraOptionSet1.Enabled = true;
                ultraOptionSet1.CheckedIndex = 0;
                cbbDetailType.Enabled = true;
            }
        }

        private void ultraOptionSet1_ValueChanged(object sender, EventArgs e)
        {
            if (ultraOptionSet1.CheckedIndex == 0)
            {
                cbbDetailType.Enabled = true;
                cbbDetailType.SelectedIndex = 0;
            }
            else
            {

                cbbDetailType.Enabled = false;
                cbbDetailType.SelectedIndex = -1;
            }
        }
    }
}
