﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FAccountGroup : CatalogBase //UserControl
    {
        #region khai báo
        private readonly IAccountGroupService _IAccountGroupService;
        #endregion

        #region khởi tạo
        public FAccountGroup()
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _IAccountGroupService = IoC.Resolve<IAccountGroupService>();
            this.uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            #endregion

            #region Tạo dữ liệu ban đầu
            LoadDuLieu();
            System.Windows.Forms.ToolTip ToolTip2 = new System.Windows.Forms.ToolTip();
            ToolTip2.SetToolTip(btnEdit, "Xem (Ctrl + E)");
            if (uGrid.Rows.Count > 0) uGrid.Rows[0].Selected = true;
            #endregion
        }

        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }
        private void LoadDuLieu(bool configGrid)
        {
            WaitingFrm.StartWaiting();
            #region Lấy dữ liệu từ CSDL
            List<AccountGroup> list = _IAccountGroupService.GetAll_Orderby();
            _IAccountGroupService.UnbindSession(list);
            list = _IAccountGroupService.GetAll_Orderby();
            #endregion
            //BindingList<SaleDiscountPolicy> bdSaleDiscountPoolicy = new BindingList<SaleDiscountPolicy>(_ISaleDiscountPolicyService.GetAll());
            #region Xử lý dữ liệu
            //thiết lập tính chất
            foreach (var item in list) item.AccountGroupKindView = Utils.DicAccountKind[item.AccountGroupKind.ToString(CultureInfo.InvariantCulture)].Name;
            #endregion

            #region hiển thị và xử lý hiển thị
            uGrid.DataSource = list.ToArray();

            if (configGrid) ConfigGrid(uGrid);
            #endregion
            WaitingFrm.StopWaiting();
        }
        #endregion

        #region edit
        protected override void EditFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                AccountGroup temp = uGrid.Selected.Rows[0].ListObject as AccountGroup;
                new FAccountGroupDetail(temp).ShowDialog(this);
                if (!FAccountGroupDetail.isClose) LoadDuLieu();
            }
            else
                MSG.Error(string.Format(resSystem.MSG_Catalog3, "Nhóm tài khoản"));
        }
        #endregion
        ////#region Add
        ////void AddFunction()
        ////{
        ////    new FAccountGroupDetail().ShowDialog(this);
        ////    if (!FAccountGroupDetail.isClose) LoadDuLieu();
        ////}
        ////#endregion
        ////#region Delete
        ////void deleteFunction()
        ////{
        ////    if (uGrid.Selected.Rows.Count > 0)
        ////    {
        ////        AccountGroup temp = uGrid.Selected.Rows[0].ListObject as AccountGroup;
        ////        if (MSG.Question(string.Format(resSystem.MSG_HeThong_05, temp.AccountGroupName)) == System.Windows.Forms.DialogResult.Yes)
        ////        {
        ////            _IAccountGroupService.BeginTran();
        ////            _IAccountGroupService.Delete(temp);
        ////            _IAccountGroupService.CommitTran();
        ////            LoadDuLieu();
        ////        }
        ////    }
        ////    else
        ////        MSG.Error(resSystem.MSG_HeThong_06);
        ////}
        ////#endregion
        #region nghiệp vụ
        #region ContentMenu

        #endregion

        private void uGrid_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (e.Row.Index >= 0)
            {
                AccountGroup temp = e.Row.ListObject as AccountGroup;
                new FAccountGroupDetail(temp).ShowDialog(this);
                if (!FAccountGroupDetail.isClose) LoadDuLieu();
            }
        }
        #endregion

        #region Utils
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            string nameTable = TextMessage.ConstDatabase.AccountGroup_TableName;
            Utils.ConfigGrid(utralGrid, nameTable);
        }
        #endregion

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tsmEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            ResetFunction();
        }
        protected override void ResetFunction()
        {
            LoadDuLieu();
        }

        private void FAccountGroup_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGrid.ResetText();
            uGrid.ResetUpdateMode();
            uGrid.ResetExitEditModeOnLeave();
            uGrid.ResetRowUpdateCancelAction();
            uGrid.DataSource = null;
            uGrid.Layouts.Clear();
            uGrid.ResetLayouts();
            uGrid.ResetDisplayLayout();
            uGrid.Refresh();
            uGrid.ClearUndoHistory();
            uGrid.ClearXsdConstraints();
        }

        private void FAccountGroup_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
