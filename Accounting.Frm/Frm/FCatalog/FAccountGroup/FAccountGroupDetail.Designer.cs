﻿namespace Accounting
{
    partial class FAccountGroupDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem9 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem10 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem11 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem12 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem13 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbAccountGroupKind = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.txtAccountGroupName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblTinhChat = new Infragistics.Win.Misc.UltraLabel();
            this.lblTen = new Infragistics.Win.Misc.UltraLabel();
            this.lblMa = new Infragistics.Win.Misc.UltraLabel();
            this.cbbDetailType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.chkDetailType = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.ultraOptionSet1 = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountGroupKind)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountGroupName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDetailType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDetailType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraOptionSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            appearance1.BorderColor3DBase = System.Drawing.Color.MintCream;
            appearance1.FontData.BoldAsString = "True";
            this.ultraGroupBox1.Appearance = appearance1;
            this.ultraGroupBox1.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox1.Controls.Add(this.cbbAccountGroupKind);
            this.ultraGroupBox1.Controls.Add(this.txtAccountGroupName);
            this.ultraGroupBox1.Controls.Add(this.txtID);
            this.ultraGroupBox1.Controls.Add(this.lblTinhChat);
            this.ultraGroupBox1.Controls.Add(this.lblTen);
            this.ultraGroupBox1.Controls.Add(this.lblMa);
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(3, 3);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(313, 244);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbAccountGroupKind
            // 
            this.cbbAccountGroupKind.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountGroupKind.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbbAccountGroupKind.Location = new System.Drawing.Point(130, 81);
            this.cbbAccountGroupKind.Name = "cbbAccountGroupKind";
            this.cbbAccountGroupKind.Size = new System.Drawing.Size(170, 21);
            this.cbbAccountGroupKind.TabIndex = 9;
            // 
            // txtAccountGroupName
            // 
            this.txtAccountGroupName.Location = new System.Drawing.Point(130, 54);
            this.txtAccountGroupName.MaxLength = 512;
            this.txtAccountGroupName.Name = "txtAccountGroupName";
            this.txtAccountGroupName.Size = new System.Drawing.Size(169, 21);
            this.txtAccountGroupName.TabIndex = 6;
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(130, 27);
            this.txtID.MaxLength = 25;
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(169, 21);
            this.txtID.TabIndex = 5;
            // 
            // lblTinhChat
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            this.lblTinhChat.Appearance = appearance2;
            this.lblTinhChat.Location = new System.Drawing.Point(9, 85);
            this.lblTinhChat.Name = "lblTinhChat";
            this.lblTinhChat.Size = new System.Drawing.Size(105, 19);
            this.lblTinhChat.TabIndex = 2;
            this.lblTinhChat.Text = "Tính chất";
            // 
            // lblTen
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            this.lblTen.Appearance = appearance3;
            this.lblTen.Location = new System.Drawing.Point(8, 56);
            this.lblTen.Name = "lblTen";
            this.lblTen.Size = new System.Drawing.Size(128, 19);
            this.lblTen.TabIndex = 1;
            this.lblTen.Text = "Tên nhóm tài khoản (*)";
            // 
            // lblMa
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            this.lblMa.Appearance = appearance4;
            this.lblMa.Location = new System.Drawing.Point(8, 29);
            this.lblMa.Name = "lblMa";
            this.lblMa.Size = new System.Drawing.Size(117, 19);
            this.lblMa.TabIndex = 0;
            this.lblMa.Text = "Mã nhóm tài khoản (*)";
            // 
            // cbbDetailType
            // 
            this.cbbDetailType.AutoSize = false;
            this.cbbDetailType.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbDetailType.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            valueListItem1.DataValue = "ValueListItem0";
            valueListItem1.DisplayText = "Nhà Cung Cấp";
            valueListItem2.DataValue = "ValueListItem1";
            valueListItem2.DisplayText = "Khách Hàng";
            valueListItem3.DataValue = "ValueListItem2";
            valueListItem3.DisplayText = "Nhân Viên";
            this.cbbDetailType.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2,
            valueListItem3});
            this.cbbDetailType.Location = new System.Drawing.Point(137, 52);
            this.cbbDetailType.Name = "cbbDetailType";
            this.cbbDetailType.Size = new System.Drawing.Size(115, 21);
            this.cbbDetailType.TabIndex = 10;
            // 
            // chkDetailType
            // 
            this.chkDetailType.BackColor = System.Drawing.Color.Transparent;
            this.chkDetailType.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkDetailType.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkDetailType.Location = new System.Drawing.Point(6, 23);
            this.chkDetailType.Name = "chkDetailType";
            this.chkDetailType.Size = new System.Drawing.Size(115, 22);
            this.chkDetailType.TabIndex = 11;
            this.chkDetailType.Text = "Chi tiết theo";
            this.chkDetailType.CheckedChanged += new System.EventHandler(this.chkDetailType_CheckedChanged);
            // 
            // btnSave
            // 
            appearance5.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance5;
            this.btnSave.Location = new System.Drawing.Point(418, 253);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 11;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            appearance6.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance6;
            this.btnClose.Location = new System.Drawing.Point(499, 253);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 12;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // ultraOptionSet1
            // 
            this.ultraOptionSet1.BackColor = System.Drawing.Color.Transparent;
            this.ultraOptionSet1.BackColorInternal = System.Drawing.Color.Transparent;
            this.ultraOptionSet1.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraOptionSet1.Enabled = false;
            valueListItem9.DataValue = "ValueListItem0";
            valueListItem9.DisplayText = "Đối Tượng";
            valueListItem10.DataValue = "ValueListItem1";
            valueListItem10.DisplayText = "ĐT tập hợp CP";
            valueListItem11.DataValue = "ValueListItem2";
            valueListItem11.DisplayText = "Hợp Đồng";
            valueListItem12.DataValue = "ValueListItem3";
            valueListItem12.DisplayText = "Vật Tư Hàng Hoá";
            valueListItem13.DataValue = "ValueListItem4";
            valueListItem13.DisplayText = "TK Ngân Hàng";
            this.ultraOptionSet1.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem9,
            valueListItem10,
            valueListItem11,
            valueListItem12,
            valueListItem13});
            this.ultraOptionSet1.ItemSpacingHorizontal = 5;
            this.ultraOptionSet1.ItemSpacingVertical = 5;
            this.ultraOptionSet1.Location = new System.Drawing.Point(6, 54);
            this.ultraOptionSet1.Margin = new System.Windows.Forms.Padding(0);
            this.ultraOptionSet1.Name = "ultraOptionSet1";
            this.ultraOptionSet1.Size = new System.Drawing.Size(115, 105);
            this.ultraOptionSet1.TabIndex = 13;
            this.ultraOptionSet1.ValueChanged += new System.EventHandler(this.ultraOptionSet1_ValueChanged);
            // 
            // ultraGroupBox2
            // 
            appearance7.FontData.BoldAsString = "True";
            this.ultraGroupBox2.Appearance = appearance7;
            this.ultraGroupBox2.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox2.Controls.Add(this.ultraOptionSet1);
            this.ultraGroupBox2.Controls.Add(this.chkDetailType);
            this.ultraGroupBox2.Controls.Add(this.cbbDetailType);
            this.ultraGroupBox2.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox2.Location = new System.Drawing.Point(322, 3);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(261, 244);
            this.ultraGroupBox2.TabIndex = 15;
            this.ultraGroupBox2.Text = "Chi tiết theo";
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // FAccountGroupDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(585, 290);
            this.Controls.Add(this.ultraGroupBox2);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.ultraGroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FAccountGroupDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountGroupKind)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountGroupName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDetailType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDetailType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraOptionSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraLabel lblMa;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountGroupName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtID;
        private Infragistics.Win.Misc.UltraLabel lblTen;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraLabel lblTinhChat;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkDetailType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbDetailType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbAccountGroupKind;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet ultraOptionSet1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
    }
}
