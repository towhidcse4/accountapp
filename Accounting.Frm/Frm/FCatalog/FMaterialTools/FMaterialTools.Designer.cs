﻿namespace Accounting
{
    partial class FMaterialTools
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.txtAmount = new Infragistics.Win.Misc.UltraLabel();
            this.txtQuantity = new Infragistics.Win.Misc.UltraLabel();
            this.txtUnit = new Infragistics.Win.Misc.UltraLabel();
            this.txtMaterialName = new Infragistics.Win.Misc.UltraLabel();
            this.txtMaterialcode = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.lbl1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl3 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraPanel2 = new Infragistics.Win.Misc.UltraPanel();
            this.cbbAboutTime = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.btnViewVoucher = new Infragistics.Win.Misc.UltraButton();
            this.ultraLabel22 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel21 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.dteToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.dteFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uGridDetail = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnAdd = new Infragistics.Win.Misc.UltraButton();
            this.btnDelete = new Infragistics.Win.Misc.UltraButton();
            this.btnEdit = new Infragistics.Win.Misc.UltraButton();
            this.ultraTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.cms4Grid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.tmsReLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraSplitter1 = new Infragistics.Win.Misc.UltraSplitter();
            this.ultraTabSharedControlsPage2 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.lblDepartmentID = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel20 = new Infragistics.Win.Misc.UltraLabel();
            this.lblPurchasePrice = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.lblPeriodDepreciationAmount = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.lblOriginalPrice = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.lblProductionYear = new Infragistics.Win.Misc.UltraLabel();
            this.lblMadeIn = new Infragistics.Win.Misc.UltraLabel();
            this.lblSerialNumber = new Infragistics.Win.Misc.UltraLabel();
            this.lblFixedAssetCategory = new Infragistics.Win.Misc.UltraLabel();
            this.lblFixedAssetName = new Infragistics.Win.Misc.UltraLabel();
            this.lblFixedAssetCode = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel19 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel17 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.btnExportExcel = new Infragistics.Win.Misc.UltraButton();
            this.uGridExport = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl1.SuspendLayout();
            this.ultraTabPageControl3.SuspendLayout();
            this.ultraPanel2.ClientArea.SuspendLayout();
            this.ultraPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAboutTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDetail)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).BeginInit();
            this.ultraTabControl1.SuspendLayout();
            this.cms4Grid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl2)).BeginInit();
            this.ultraTabControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridExport)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.txtAmount);
            this.ultraTabPageControl1.Controls.Add(this.txtQuantity);
            this.ultraTabPageControl1.Controls.Add(this.txtUnit);
            this.ultraTabPageControl1.Controls.Add(this.txtMaterialName);
            this.ultraTabPageControl1.Controls.Add(this.txtMaterialcode);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel5);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel4);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel3);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel2);
            this.ultraTabPageControl1.Controls.Add(this.lbl1);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 22);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(772, 192);
            // 
            // txtAmount
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.TextVAlignAsString = "Middle";
            this.txtAmount.Appearance = appearance1;
            this.txtAmount.Location = new System.Drawing.Point(171, 128);
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(162, 23);
            this.txtAmount.TabIndex = 5;
            // 
            // txtQuantity
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.TextVAlignAsString = "Middle";
            this.txtQuantity.Appearance = appearance2;
            this.txtQuantity.Location = new System.Drawing.Point(171, 99);
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.Size = new System.Drawing.Size(162, 23);
            this.txtQuantity.TabIndex = 5;
            // 
            // txtUnit
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextVAlignAsString = "Middle";
            this.txtUnit.Appearance = appearance3;
            this.txtUnit.Location = new System.Drawing.Point(171, 70);
            this.txtUnit.Name = "txtUnit";
            this.txtUnit.Size = new System.Drawing.Size(162, 23);
            this.txtUnit.TabIndex = 5;
            // 
            // txtMaterialName
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.txtMaterialName.Appearance = appearance4;
            this.txtMaterialName.Location = new System.Drawing.Point(171, 41);
            this.txtMaterialName.Name = "txtMaterialName";
            this.txtMaterialName.Size = new System.Drawing.Size(211, 23);
            this.txtMaterialName.TabIndex = 5;
            // 
            // txtMaterialcode
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.TextVAlignAsString = "Middle";
            this.txtMaterialcode.Appearance = appearance5;
            this.txtMaterialcode.Location = new System.Drawing.Point(171, 12);
            this.txtMaterialcode.Name = "txtMaterialcode";
            this.txtMaterialcode.Size = new System.Drawing.Size(162, 23);
            this.txtMaterialcode.TabIndex = 5;
            // 
            // ultraLabel5
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance6;
            this.ultraLabel5.Location = new System.Drawing.Point(10, 128);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel5.TabIndex = 4;
            this.ultraLabel5.Text = "Tổng giá trị";
            // 
            // ultraLabel4
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance7;
            this.ultraLabel4.Location = new System.Drawing.Point(10, 99);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel4.TabIndex = 3;
            this.ultraLabel4.Text = "Số lượng";
            // 
            // ultraLabel3
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance8;
            this.ultraLabel3.Location = new System.Drawing.Point(10, 70);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel3.TabIndex = 2;
            this.ultraLabel3.Text = "Đơn giá";
            // 
            // ultraLabel2
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance9;
            this.ultraLabel2.Location = new System.Drawing.Point(10, 41);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(120, 22);
            this.ultraLabel2.TabIndex = 1;
            this.ultraLabel2.Text = "Tên công cụ, dụng cụ";
            // 
            // lbl1
            // 
            appearance10.TextVAlignAsString = "Middle";
            this.lbl1.Appearance = appearance10;
            this.lbl1.Location = new System.Drawing.Point(10, 12);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(111, 22);
            this.lbl1.TabIndex = 0;
            this.lbl1.Text = "Mã công cụ, dụng cụ";
            // 
            // ultraTabPageControl3
            // 
            this.ultraTabPageControl3.Controls.Add(this.ultraPanel2);
            this.ultraTabPageControl3.Controls.Add(this.uGridDetail);
            this.ultraTabPageControl3.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl3.Name = "ultraTabPageControl3";
            this.ultraTabPageControl3.Size = new System.Drawing.Size(772, 192);
            // 
            // ultraPanel2
            // 
            // 
            // ultraPanel2.ClientArea
            // 
            this.ultraPanel2.ClientArea.Controls.Add(this.cbbAboutTime);
            this.ultraPanel2.ClientArea.Controls.Add(this.btnViewVoucher);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel22);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel21);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel7);
            this.ultraPanel2.ClientArea.Controls.Add(this.dteToDate);
            this.ultraPanel2.ClientArea.Controls.Add(this.dteFromDate);
            this.ultraPanel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraPanel2.Location = new System.Drawing.Point(0, 157);
            this.ultraPanel2.Name = "ultraPanel2";
            this.ultraPanel2.Size = new System.Drawing.Size(772, 35);
            this.ultraPanel2.TabIndex = 8;
            // 
            // cbbAboutTime
            // 
            this.cbbAboutTime.AutoSize = false;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbAboutTime.DisplayLayout.Appearance = appearance11;
            this.cbbAboutTime.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbAboutTime.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance12.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance12.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance12.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAboutTime.DisplayLayout.GroupByBox.Appearance = appearance12;
            appearance13.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAboutTime.DisplayLayout.GroupByBox.BandLabelAppearance = appearance13;
            this.cbbAboutTime.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance14.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance14.BackColor2 = System.Drawing.SystemColors.Control;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance14.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAboutTime.DisplayLayout.GroupByBox.PromptAppearance = appearance14;
            this.cbbAboutTime.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbAboutTime.DisplayLayout.MaxRowScrollRegions = 1;
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            appearance15.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbAboutTime.DisplayLayout.Override.ActiveCellAppearance = appearance15;
            appearance16.BackColor = System.Drawing.SystemColors.Highlight;
            appearance16.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbAboutTime.DisplayLayout.Override.ActiveRowAppearance = appearance16;
            this.cbbAboutTime.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbAboutTime.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            this.cbbAboutTime.DisplayLayout.Override.CardAreaAppearance = appearance17;
            appearance18.BorderColor = System.Drawing.Color.Silver;
            appearance18.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbAboutTime.DisplayLayout.Override.CellAppearance = appearance18;
            this.cbbAboutTime.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbAboutTime.DisplayLayout.Override.CellPadding = 0;
            appearance19.BackColor = System.Drawing.SystemColors.Control;
            appearance19.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance19.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance19.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance19.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAboutTime.DisplayLayout.Override.GroupByRowAppearance = appearance19;
            appearance20.TextHAlignAsString = "Left";
            this.cbbAboutTime.DisplayLayout.Override.HeaderAppearance = appearance20;
            this.cbbAboutTime.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbAboutTime.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance21.BackColor = System.Drawing.SystemColors.Window;
            appearance21.BorderColor = System.Drawing.Color.Silver;
            this.cbbAboutTime.DisplayLayout.Override.RowAppearance = appearance21;
            this.cbbAboutTime.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance22.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbAboutTime.DisplayLayout.Override.TemplateAddRowAppearance = appearance22;
            this.cbbAboutTime.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAboutTime.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbAboutTime.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbAboutTime.Location = new System.Drawing.Point(107, 6);
            this.cbbAboutTime.Name = "cbbAboutTime";
            this.cbbAboutTime.Size = new System.Drawing.Size(204, 22);
            this.cbbAboutTime.TabIndex = 7;
            this.cbbAboutTime.ValueChanged += new System.EventHandler(this.cbbAboutTime_ValueChanged);
            // 
            // btnViewVoucher
            // 
            this.btnViewVoucher.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnViewVoucher.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2010Button;
            this.btnViewVoucher.Location = new System.Drawing.Point(686, 6);
            this.btnViewVoucher.Name = "btnViewVoucher";
            this.btnViewVoucher.Size = new System.Drawing.Size(75, 23);
            this.btnViewVoucher.TabIndex = 6;
            this.btnViewVoucher.Text = "&Xem ";
            this.btnViewVoucher.Click += new System.EventHandler(this.btnViewVoucher_Click);
            // 
            // ultraLabel22
            // 
            appearance23.BackColor = System.Drawing.Color.Transparent;
            appearance23.TextVAlignAsString = "Middle";
            this.ultraLabel22.Appearance = appearance23;
            this.ultraLabel22.Location = new System.Drawing.Point(520, 6);
            this.ultraLabel22.Name = "ultraLabel22";
            this.ultraLabel22.Size = new System.Drawing.Size(33, 22);
            this.ultraLabel22.TabIndex = 5;
            this.ultraLabel22.Text = "Đến";
            // 
            // ultraLabel21
            // 
            appearance24.BackColor = System.Drawing.Color.Transparent;
            appearance24.TextVAlignAsString = "Middle";
            this.ultraLabel21.Appearance = appearance24;
            this.ultraLabel21.Location = new System.Drawing.Point(317, 6);
            this.ultraLabel21.Name = "ultraLabel21";
            this.ultraLabel21.Size = new System.Drawing.Size(27, 22);
            this.ultraLabel21.TabIndex = 5;
            this.ultraLabel21.Text = "Từ";
            // 
            // ultraLabel7
            // 
            appearance25.BackColor = System.Drawing.Color.Transparent;
            appearance25.TextVAlignAsString = "Middle";
            this.ultraLabel7.Appearance = appearance25;
            this.ultraLabel7.Location = new System.Drawing.Point(9, 6);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(102, 22);
            this.ultraLabel7.TabIndex = 5;
            this.ultraLabel7.Text = "Khoảng thời gian";
            // 
            // dteToDate
            // 
            this.dteToDate.AutoSize = false;
            this.dteToDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteToDate.Location = new System.Drawing.Point(559, 6);
            this.dteToDate.Name = "dteToDate";
            this.dteToDate.Size = new System.Drawing.Size(144, 22);
            this.dteToDate.TabIndex = 4;
            this.dteToDate.ValueChanged += new System.EventHandler(this.dteToDate_ValueChanged);
            // 
            // dteFromDate
            // 
            this.dteFromDate.AutoSize = false;
            this.dteFromDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteFromDate.Location = new System.Drawing.Point(350, 6);
            this.dteFromDate.Name = "dteFromDate";
            this.dteFromDate.Size = new System.Drawing.Size(144, 22);
            this.dteFromDate.TabIndex = 3;
            this.dteFromDate.ValueChanged += new System.EventHandler(this.dteFromDate_ValueChanged);
            // 
            // uGridDetail
            // 
            this.uGridDetail.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGridDetail.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGridDetail.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGridDetail.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGridDetail.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            this.uGridDetail.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.AlignWithDataRows;
            this.uGridDetail.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            this.uGridDetail.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGridDetail.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGridDetail.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGridDetail.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGridDetail.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGridDetail.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGridDetail.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGridDetail.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.uGridDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridDetail.Location = new System.Drawing.Point(0, 0);
            this.uGridDetail.Name = "uGridDetail";
            this.uGridDetail.Size = new System.Drawing.Size(772, 192);
            this.uGridDetail.TabIndex = 7;
            this.uGridDetail.Text = "ultraGrid1";
            this.uGridDetail.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridDetail_DoubleClickRow);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnExportExcel);
            this.panel1.Controls.Add(this.btnAdd);
            this.panel1.Controls.Add(this.btnDelete);
            this.panel1.Controls.Add(this.btnEdit);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(774, 52);
            this.panel1.TabIndex = 4;
            // 
            // btnAdd
            // 
            appearance29.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.btnAdd.Appearance = appearance29;
            this.btnAdd.Location = new System.Drawing.Point(10, 12);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 30);
            this.btnAdd.TabIndex = 2;
            this.btnAdd.Text = "Thêm";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnDelete
            // 
            appearance30.Image = global::Accounting.Properties.Resources.xoa1_xoa_du_lieu_ke_toan_khoi_database_;
            this.btnDelete.Appearance = appearance30;
            this.btnDelete.Location = new System.Drawing.Point(172, 12);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 30);
            this.btnDelete.TabIndex = 1;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnEdit
            // 
            appearance31.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            this.btnEdit.Appearance = appearance31;
            this.btnEdit.Location = new System.Drawing.Point(91, 12);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(75, 30);
            this.btnEdit.TabIndex = 0;
            this.btnEdit.Text = "Sửa";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // ultraTabControl1
            // 
            this.ultraTabControl1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl3);
            this.ultraTabControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraTabControl1.Location = new System.Drawing.Point(0, 343);
            this.ultraTabControl1.Name = "ultraTabControl1";
            this.ultraTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.ultraTabControl1.Size = new System.Drawing.Size(774, 215);
            this.ultraTabControl1.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Office2007Ribbon;
            this.ultraTabControl1.TabIndex = 7;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "1. Thông tin chung";
            ultraTab2.TabPage = this.ultraTabPageControl3;
            ultraTab2.Text = "&2. Chứng từ";
            this.ultraTabControl1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            this.ultraTabControl1.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007;
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(772, 192);
            // 
            // cms4Grid
            // 
            this.cms4Grid.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.cms4Grid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.tsmAdd,
            this.tsmEdit,
            this.tsmDelete,
            this.tmsReLoad});
            this.cms4Grid.Name = "cms4Grid";
            this.cms4Grid.Size = new System.Drawing.Size(152, 114);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(148, 6);
            // 
            // tsmAdd
            // 
            this.tsmAdd.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.tsmAdd.Name = "tsmAdd";
            this.tsmAdd.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.tsmAdd.Size = new System.Drawing.Size(151, 26);
            this.tsmAdd.Text = "Thêm";
            this.tsmAdd.Click += new System.EventHandler(this.tsmAdd_Click);
            // 
            // tsmEdit
            // 
            this.tsmEdit.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            this.tsmEdit.Name = "tsmEdit";
            this.tsmEdit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.tsmEdit.Size = new System.Drawing.Size(151, 26);
            this.tsmEdit.Text = "Sửa";
            this.tsmEdit.Click += new System.EventHandler(this.tsmEdit_Click);
            // 
            // tsmDelete
            // 
            this.tsmDelete.Image = global::Accounting.Properties.Resources.xoa1_xoa_du_lieu_ke_toan_khoi_database_;
            this.tsmDelete.Name = "tsmDelete";
            this.tsmDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.tsmDelete.Size = new System.Drawing.Size(151, 26);
            this.tsmDelete.Text = "Xóa";
            this.tsmDelete.Click += new System.EventHandler(this.tsmDelete_Click);
            // 
            // tmsReLoad
            // 
            this.tmsReLoad.Image = global::Accounting.Properties.Resources.ubtnReset;
            this.tmsReLoad.Name = "tmsReLoad";
            this.tmsReLoad.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.tmsReLoad.Size = new System.Drawing.Size(151, 26);
            this.tmsReLoad.Text = "Tải Lại";
            this.tmsReLoad.Click += new System.EventHandler(this.tmsReLoad_Click);
            // 
            // uGrid
            // 
            this.uGrid.ContextMenuStrip = this.cms4Grid;
            appearance32.TextHAlignAsString = "Left";
            this.uGrid.DisplayLayout.Appearance = appearance32;
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.uGrid.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGrid.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGrid.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGrid.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.uGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid.Location = new System.Drawing.Point(0, 52);
            this.uGrid.Name = "uGrid";
            this.uGrid.Size = new System.Drawing.Size(774, 281);
            this.uGrid.TabIndex = 6;
            this.uGrid.Text = "uGrid";
            this.uGrid.BeforeRowActivate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.uGrid_BeforeRowActivate);
            this.uGrid.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.uGrid_AfterSelectChange);
            this.uGrid.DoubleClick += new System.EventHandler(this.uGrid_DoubleClick);
            this.uGrid.MouseDown += new System.Windows.Forms.MouseEventHandler(this.uGrid_MouseDown);
            // 
            // ultraSplitter1
            // 
            this.ultraSplitter1.BackColor = System.Drawing.SystemColors.Control;
            this.ultraSplitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraSplitter1.Location = new System.Drawing.Point(0, 333);
            this.ultraSplitter1.Name = "ultraSplitter1";
            this.ultraSplitter1.RestoreExtent = 158;
            this.ultraSplitter1.Size = new System.Drawing.Size(774, 10);
            this.ultraSplitter1.TabIndex = 37;
            // 
            // ultraTabSharedControlsPage2
            // 
            this.ultraTabSharedControlsPage2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage2.Name = "ultraTabSharedControlsPage2";
            this.ultraTabSharedControlsPage2.Size = new System.Drawing.Size(787, 209);
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.lblDepartmentID);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel20);
            this.ultraTabPageControl2.Controls.Add(this.lblPurchasePrice);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel8);
            this.ultraTabPageControl2.Controls.Add(this.lblPeriodDepreciationAmount);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel6);
            this.ultraTabPageControl2.Controls.Add(this.lblOriginalPrice);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel1);
            this.ultraTabPageControl2.Controls.Add(this.lblProductionYear);
            this.ultraTabPageControl2.Controls.Add(this.lblMadeIn);
            this.ultraTabPageControl2.Controls.Add(this.lblSerialNumber);
            this.ultraTabPageControl2.Controls.Add(this.lblFixedAssetCategory);
            this.ultraTabPageControl2.Controls.Add(this.lblFixedAssetName);
            this.ultraTabPageControl2.Controls.Add(this.lblFixedAssetCode);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel19);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel13);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel14);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel15);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel16);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel17);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(787, 209);
            // 
            // lblDepartmentID
            // 
            appearance33.BackColor = System.Drawing.Color.Transparent;
            appearance33.TextVAlignAsString = "Middle";
            this.lblDepartmentID.Appearance = appearance33;
            this.lblDepartmentID.Location = new System.Drawing.Point(104, 94);
            this.lblDepartmentID.Name = "lblDepartmentID";
            this.lblDepartmentID.Size = new System.Drawing.Size(205, 21);
            this.lblDepartmentID.TabIndex = 33;
            // 
            // ultraLabel20
            // 
            appearance34.BackColor = System.Drawing.Color.Transparent;
            appearance34.TextVAlignAsString = "Middle";
            this.ultraLabel20.Appearance = appearance34;
            this.ultraLabel20.Location = new System.Drawing.Point(10, 94);
            this.ultraLabel20.Name = "ultraLabel20";
            this.ultraLabel20.Size = new System.Drawing.Size(88, 21);
            this.ultraLabel20.TabIndex = 32;
            this.ultraLabel20.Text = "Phòng ban";
            // 
            // lblPurchasePrice
            // 
            appearance35.BackColor = System.Drawing.Color.Transparent;
            appearance35.TextVAlignAsString = "Middle";
            this.lblPurchasePrice.Appearance = appearance35;
            this.lblPurchasePrice.Location = new System.Drawing.Point(522, 67);
            this.lblPurchasePrice.Name = "lblPurchasePrice";
            this.lblPurchasePrice.Size = new System.Drawing.Size(205, 21);
            this.lblPurchasePrice.TabIndex = 27;
            // 
            // ultraLabel8
            // 
            appearance36.BackColor = System.Drawing.Color.Transparent;
            appearance36.TextVAlignAsString = "Middle";
            this.ultraLabel8.Appearance = appearance36;
            this.ultraLabel8.Location = new System.Drawing.Point(428, 67);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(88, 21);
            this.ultraLabel8.TabIndex = 26;
            this.ultraLabel8.Text = "Giá trị KH tháng";
            // 
            // lblPeriodDepreciationAmount
            // 
            appearance37.BackColor = System.Drawing.Color.Transparent;
            appearance37.TextVAlignAsString = "Middle";
            this.lblPeriodDepreciationAmount.Appearance = appearance37;
            this.lblPeriodDepreciationAmount.Location = new System.Drawing.Point(522, 40);
            this.lblPeriodDepreciationAmount.Name = "lblPeriodDepreciationAmount";
            this.lblPeriodDepreciationAmount.Size = new System.Drawing.Size(205, 21);
            this.lblPeriodDepreciationAmount.TabIndex = 25;
            // 
            // ultraLabel6
            // 
            appearance38.BackColor = System.Drawing.Color.Transparent;
            appearance38.TextVAlignAsString = "Middle";
            this.ultraLabel6.Appearance = appearance38;
            this.ultraLabel6.Location = new System.Drawing.Point(428, 40);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(88, 21);
            this.ultraLabel6.TabIndex = 24;
            this.ultraLabel6.Text = "Giá trị KH năm";
            // 
            // lblOriginalPrice
            // 
            appearance39.BackColor = System.Drawing.Color.Transparent;
            appearance39.TextVAlignAsString = "Middle";
            this.lblOriginalPrice.Appearance = appearance39;
            this.lblOriginalPrice.Location = new System.Drawing.Point(522, 13);
            this.lblOriginalPrice.Name = "lblOriginalPrice";
            this.lblOriginalPrice.Size = new System.Drawing.Size(205, 21);
            this.lblOriginalPrice.TabIndex = 23;
            // 
            // ultraLabel1
            // 
            appearance40.BackColor = System.Drawing.Color.Transparent;
            appearance40.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance40;
            this.ultraLabel1.Location = new System.Drawing.Point(428, 13);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(88, 21);
            this.ultraLabel1.TabIndex = 22;
            this.ultraLabel1.Text = "Nguyên giá";
            // 
            // lblProductionYear
            // 
            appearance41.BackColor = System.Drawing.Color.Transparent;
            appearance41.TextVAlignAsString = "Middle";
            this.lblProductionYear.Appearance = appearance41;
            this.lblProductionYear.Location = new System.Drawing.Point(104, 175);
            this.lblProductionYear.Name = "lblProductionYear";
            this.lblProductionYear.Size = new System.Drawing.Size(205, 21);
            this.lblProductionYear.TabIndex = 19;
            // 
            // lblMadeIn
            // 
            appearance42.BackColor = System.Drawing.Color.Transparent;
            appearance42.TextVAlignAsString = "Middle";
            this.lblMadeIn.Appearance = appearance42;
            this.lblMadeIn.Location = new System.Drawing.Point(104, 148);
            this.lblMadeIn.Name = "lblMadeIn";
            this.lblMadeIn.Size = new System.Drawing.Size(205, 21);
            this.lblMadeIn.TabIndex = 17;
            // 
            // lblSerialNumber
            // 
            appearance43.BackColor = System.Drawing.Color.Transparent;
            appearance43.TextVAlignAsString = "Middle";
            this.lblSerialNumber.Appearance = appearance43;
            this.lblSerialNumber.Location = new System.Drawing.Point(104, 121);
            this.lblSerialNumber.Name = "lblSerialNumber";
            this.lblSerialNumber.Size = new System.Drawing.Size(205, 21);
            this.lblSerialNumber.TabIndex = 16;
            // 
            // lblFixedAssetCategory
            // 
            appearance44.BackColor = System.Drawing.Color.Transparent;
            appearance44.TextVAlignAsString = "Middle";
            this.lblFixedAssetCategory.Appearance = appearance44;
            this.lblFixedAssetCategory.Location = new System.Drawing.Point(104, 67);
            this.lblFixedAssetCategory.Name = "lblFixedAssetCategory";
            this.lblFixedAssetCategory.Size = new System.Drawing.Size(205, 21);
            this.lblFixedAssetCategory.TabIndex = 15;
            // 
            // lblFixedAssetName
            // 
            appearance45.BackColor = System.Drawing.Color.Transparent;
            appearance45.TextVAlignAsString = "Middle";
            this.lblFixedAssetName.Appearance = appearance45;
            this.lblFixedAssetName.Location = new System.Drawing.Point(104, 40);
            this.lblFixedAssetName.Name = "lblFixedAssetName";
            this.lblFixedAssetName.Size = new System.Drawing.Size(205, 21);
            this.lblFixedAssetName.TabIndex = 14;
            this.lblFixedAssetName.Text = " ";
            // 
            // lblFixedAssetCode
            // 
            appearance46.BackColor = System.Drawing.Color.Transparent;
            appearance46.TextVAlignAsString = "Middle";
            this.lblFixedAssetCode.Appearance = appearance46;
            this.lblFixedAssetCode.Location = new System.Drawing.Point(104, 13);
            this.lblFixedAssetCode.Name = "lblFixedAssetCode";
            this.lblFixedAssetCode.Size = new System.Drawing.Size(205, 21);
            this.lblFixedAssetCode.TabIndex = 13;
            // 
            // ultraLabel19
            // 
            appearance47.BackColor = System.Drawing.Color.Transparent;
            appearance47.TextVAlignAsString = "Middle";
            this.ultraLabel19.Appearance = appearance47;
            this.ultraLabel19.Location = new System.Drawing.Point(10, 175);
            this.ultraLabel19.Name = "ultraLabel19";
            this.ultraLabel19.Size = new System.Drawing.Size(88, 21);
            this.ultraLabel19.TabIndex = 11;
            this.ultraLabel19.Text = "Năm sản xuất";
            // 
            // ultraLabel13
            // 
            appearance48.BackColor = System.Drawing.Color.Transparent;
            appearance48.TextVAlignAsString = "Middle";
            this.ultraLabel13.Appearance = appearance48;
            this.ultraLabel13.Location = new System.Drawing.Point(10, 148);
            this.ultraLabel13.Name = "ultraLabel13";
            this.ultraLabel13.Size = new System.Drawing.Size(88, 21);
            this.ultraLabel13.TabIndex = 9;
            this.ultraLabel13.Text = "Nước sản xuất";
            // 
            // ultraLabel14
            // 
            appearance49.BackColor = System.Drawing.Color.Transparent;
            appearance49.TextVAlignAsString = "Middle";
            this.ultraLabel14.Appearance = appearance49;
            this.ultraLabel14.Location = new System.Drawing.Point(10, 121);
            this.ultraLabel14.Name = "ultraLabel14";
            this.ultraLabel14.Size = new System.Drawing.Size(88, 21);
            this.ultraLabel14.TabIndex = 8;
            this.ultraLabel14.Text = "Số hiệu";
            // 
            // ultraLabel15
            // 
            appearance50.BackColor = System.Drawing.Color.Transparent;
            appearance50.TextVAlignAsString = "Middle";
            this.ultraLabel15.Appearance = appearance50;
            this.ultraLabel15.Location = new System.Drawing.Point(10, 67);
            this.ultraLabel15.Name = "ultraLabel15";
            this.ultraLabel15.Size = new System.Drawing.Size(88, 21);
            this.ultraLabel15.TabIndex = 7;
            this.ultraLabel15.Text = "Loại TSCĐ";
            // 
            // ultraLabel16
            // 
            appearance51.BackColor = System.Drawing.Color.Transparent;
            appearance51.TextVAlignAsString = "Middle";
            this.ultraLabel16.Appearance = appearance51;
            this.ultraLabel16.Location = new System.Drawing.Point(10, 40);
            this.ultraLabel16.Name = "ultraLabel16";
            this.ultraLabel16.Size = new System.Drawing.Size(88, 21);
            this.ultraLabel16.TabIndex = 6;
            this.ultraLabel16.Text = "Tên TSCĐ";
            // 
            // ultraLabel17
            // 
            appearance52.BackColor = System.Drawing.Color.Transparent;
            appearance52.TextVAlignAsString = "Middle";
            this.ultraLabel17.Appearance = appearance52;
            this.ultraLabel17.Location = new System.Drawing.Point(10, 13);
            this.ultraLabel17.Name = "ultraLabel17";
            this.ultraLabel17.Size = new System.Drawing.Size(88, 21);
            this.ultraLabel17.TabIndex = 5;
            this.ultraLabel17.Text = "Mã TSCĐ";
            // 
            // ultraTabControl2
            // 
            this.ultraTabControl2.Controls.Add(this.ultraTabPageControl2);
            this.ultraTabControl2.Location = new System.Drawing.Point(0, 0);
            this.ultraTabControl2.Name = "ultraTabControl2";
            this.ultraTabControl2.SharedControlsPage = this.ultraTabSharedControlsPage2;
            this.ultraTabControl2.Size = new System.Drawing.Size(200, 100);
            this.ultraTabControl2.TabIndex = 0;
            // 
            // btnExportExcel
            // 
            appearance26.BackColor = System.Drawing.Color.DarkRed;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance26.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance26.Image = global::Accounting.Properties.Resources.Excel1;
            this.btnExportExcel.Appearance = appearance26;
            this.btnExportExcel.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance27.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance27.BorderAlpha = Infragistics.Win.Alpha.Opaque;
            this.btnExportExcel.HotTrackAppearance = appearance27;
            this.btnExportExcel.Location = new System.Drawing.Point(253, 12);
            this.btnExportExcel.Name = "btnExportExcel";
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.btnExportExcel.PressedAppearance = appearance28;
            this.btnExportExcel.Size = new System.Drawing.Size(75, 30);
            this.btnExportExcel.TabIndex = 65;
            this.btnExportExcel.Text = "Kết xuất";
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // uGridExport
            // 
            this.uGridExport.ContextMenuStrip = this.cms4Grid;
            this.uGridExport.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGridExport.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGridExport.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGridExport.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGridExport.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.uGridExport.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGridExport.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGridExport.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGridExport.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGridExport.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGridExport.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGridExport.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGridExport.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.uGridExport.Location = new System.Drawing.Point(394, 12);
            this.uGridExport.Name = "uGridExport";
            this.uGridExport.Size = new System.Drawing.Size(368, 297);
            this.uGridExport.TabIndex = 66;
            this.uGridExport.Text = "ultraGrid1";
            this.uGridExport.Visible = false;
            // 
            // FMaterialTools
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(774, 558);
            this.Controls.Add(this.uGridExport);
            this.Controls.Add(this.uGrid);
            this.Controls.Add(this.ultraSplitter1);
            this.Controls.Add(this.ultraTabControl1);
            this.Controls.Add(this.panel1);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FMaterialTools";
            this.Text = "Công cụ, dụng cụ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FMaterialTools_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FMaterialTools_FormClosed);
            this.Load += new System.EventHandler(this.FMaterialTools_Load);
            this.ultraTabPageControl1.ResumeLayout(false);
            this.ultraTabPageControl3.ResumeLayout(false);
            this.ultraPanel2.ClientArea.ResumeLayout(false);
            this.ultraPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbAboutTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDetail)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).EndInit();
            this.ultraTabControl1.ResumeLayout(false);
            this.cms4Grid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl2)).EndInit();
            this.ultraTabControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridExport)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private Infragistics.Win.Misc.UltraButton btnAdd;
        private Infragistics.Win.Misc.UltraButton btnDelete;
        private Infragistics.Win.Misc.UltraButton btnEdit;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel lbl1;
        private System.Windows.Forms.ContextMenuStrip cms4Grid;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripMenuItem tsmEdit;
        private System.Windows.Forms.ToolStripMenuItem tsmDelete;
        private System.Windows.Forms.ToolStripMenuItem tmsReLoad;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
        private Infragistics.Win.Misc.UltraSplitter ultraSplitter1;
        private Infragistics.Win.Misc.UltraLabel txtAmount;
        private Infragistics.Win.Misc.UltraLabel txtQuantity;
        private Infragistics.Win.Misc.UltraLabel txtUnit;
        private Infragistics.Win.Misc.UltraLabel txtMaterialName;
        private Infragistics.Win.Misc.UltraLabel txtMaterialcode;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage2;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.Misc.UltraLabel lblDepartmentID;
        private Infragistics.Win.Misc.UltraLabel ultraLabel20;
        private Infragistics.Win.Misc.UltraLabel lblPurchasePrice;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.Misc.UltraLabel lblPeriodDepreciationAmount;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraLabel lblOriginalPrice;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel lblProductionYear;
        private Infragistics.Win.Misc.UltraLabel lblMadeIn;
        private Infragistics.Win.Misc.UltraLabel lblSerialNumber;
        private Infragistics.Win.Misc.UltraLabel lblFixedAssetCategory;
        private Infragistics.Win.Misc.UltraLabel lblFixedAssetName;
        private Infragistics.Win.Misc.UltraLabel lblFixedAssetCode;
        private Infragistics.Win.Misc.UltraLabel ultraLabel19;
        private Infragistics.Win.Misc.UltraLabel ultraLabel13;
        private Infragistics.Win.Misc.UltraLabel ultraLabel14;
        private Infragistics.Win.Misc.UltraLabel ultraLabel15;
        private Infragistics.Win.Misc.UltraLabel ultraLabel16;
        private Infragistics.Win.Misc.UltraLabel ultraLabel17;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl2;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl3;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDetail;
        private Infragistics.Win.Misc.UltraPanel ultraPanel2;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAboutTime;
        private Infragistics.Win.Misc.UltraButton btnViewVoucher;
        private Infragistics.Win.Misc.UltraLabel ultraLabel22;
        private Infragistics.Win.Misc.UltraLabel ultraLabel21;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteToDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteFromDate;
        public Infragistics.Win.Misc.UltraButton btnExportExcel;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridExport;
    }
}