﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Accounting.TextMessage;
using System.Linq;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System.ComponentModel;
using System.Drawing;

namespace Accounting
{
    public partial class FMaterialToolsDetail : DialogForm
    {
        #region khai báo
        public static bool isClose = true;
        TIInit _Select = new TIInit();
        bool _Them = false;
        bool _IsEdit;
        private readonly IMaterialGoodsService _IMaterialGoodsService = Utils.IMaterialGoodsService;
        private readonly ITIInitService _ITIInitService = Utils.ITIInitService;
        private readonly ISystemOptionService _ISystemOptionService = Utils.ISystemOptionService;
        private readonly ITemplateService _ITemplateService = Utils.ITemplateService;
        private readonly IToolLedgerService _IToolLedgerService = Utils.IToolLedgerService;
        private readonly ISystemOptionService _SystemOptionService = Utils.ISystemOptionService;
        BindingList<TIInitDetail> TIInitDetails = new BindingList<TIInitDetail>();
        bool OldIsActive = false;
        public Guid ID { get; set; }
        #endregion

        public FMaterialToolsDetail(bool Visible = true)
        {
            InitializeComponent();
            _Them = true;
            _Select.TypeID = 436;
            _Select.DeclareType = 0;

            this.Text = "Thêm mới công cụ dụng cụ";
            InitializeGUI();
            btnSaveContinue.Visible = Visible;
            //add by namnh
            txtQuantity.Location = new Point(cbbToolsCategory.Location.X, txtUnit.Location.Y);
            ultraLabel9.Location = new Point(ultraLabel2.Location.X, ultraLabel4.Location.Y);
            txtUnitPrice.Location = new Point(txtQuantity.Location.X, cbbAllocationType.Location.Y);
            ultraLabel3.Location = new Point(ultraLabel9.Location.X, ultraLabel8.Location.Y);
            txtAmount.Location = new Point(txtUnitPrice.Location.X, txtAllocationTimes.Location.Y);
            lblParentID.Location = new Point(ultraLabel3.Location.X, lblRegistrationGroupName.Location.Y);
            cbbAllocationAwaitAccount.Location = new Point(txtAmount.Location.X, txtAllocatedAmount.Location.Y);
            ultraLabel24.Location = new Point(lblParentID.Location.X, ultraLabel7.Location.Y);
            cbbToolsCategory.Visible = false;
            ultraLabel2.Visible = false;


        }

        public FMaterialToolsDetail(TIInit temp)
        {
            InitializeComponent();
            _Select = temp;
            OldIsActive = temp.CloneObject().IsActive;
            this.Text = "Sửa công cụ dụng cụ";
            TIInitDetails = new BindingList<TIInitDetail>(temp.TIInitDetails);
            btnSaveContinue.Visible = false;
            InitializeGUI();

            BindingData(_Select);
        }
        #region Utils
        private void InitializeGUI()
        {

            // tài khoản
            cbbAllocationAwaitAccount.DataSource = Utils.IAccountDefaultService.GetAccountDefaultByTypeId(430, "cbbAllocationAwaitAccount");
            cbbAllocationAwaitAccount.DisplayMember = "AccountNumber";
            Utils.ConfigGrid(cbbAllocationAwaitAccount, ConstDatabase.Account_TableName);

            // loại CCDC

            cbbToolsCategory.DataSource = Utils.IMaterialGoodsCategoryService.GetAll_ByIsParentNode(false);
            cbbToolsCategory.DisplayMember = "MaterialGoodsCategoryName";
            Utils.ConfigGrid(cbbToolsCategory, ConstDatabase.MaterialGoodsCategory_TableName);
            //Utils.ConfigGrid<CostSet>(this, cbbToolsCategory, ConstDatabase.MaterialGoodsCategory_TableName, "ParentID", "IsParentNode");

            cbbAllocationType.FormatNumberic(ConstDatabase.Format_TienVND);
            txtQuantity.FormatNumberic(ConstDatabase.Format_Quantity);
            txtAmount.FormatNumberic(ConstDatabase.Format_TienVND);
            txtUnitPrice.FormatNumberic(ConstDatabase.Format_DonGiaQuyDoi);
            txtAllocatedAmount.FormatNumberic(ConstDatabase.Format_TienVND);
            //txtAllocationTimes.FormatNumberic(ConstDatabase.Format_TienVND);

            uGrid.DataSource = TIInitDetails;
            Utils.ConfigGrid(uGrid, ConstDatabase.TIInitDetail_TableName, new List<TemplateColumn>(), isBusiness: true);
            //uGrid.CellChange += new CellEventHandler(Utils.uGrid_CellChange<TIInit>);
            foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
            {
                this.ConfigEachColumn4Grid(0, column, uGrid);
            }

        }

        private void BindingData(TIInit material, bool isCreate = false)
        {
            if (isCreate)
            {
                if (material.ID == null || material.ID == Guid.Empty) material.ID = Guid.NewGuid();
                material.ToolsCode = txtToolsCode.Text;
                material.ToolsName = txtToolsName.Text;
                if (!string.IsNullOrEmpty(cbbToolsCategory.Text))
                {
                    material.MaterialGoodsCategoryID = ((MaterialGoodsCategory)Utils.getSelectCbbItem(cbbToolsCategory)).ID;
                }
                material.Unit = txtUnit.Text;
                if (!txtQuantity.Text.IsNullOrEmpty())
                    material.Quantity = Decimal.Parse(txtQuantity.Text);
                material.AllocationType = cbbAllocationType.SelectedIndex;
                if (!txtUnitPrice.Text.IsNullOrEmpty())
                    material.UnitPrice = Decimal.Parse(txtUnitPrice.Text);
                if (!txtAllocationTimes.Text.IsNullOrEmpty())
                    material.AllocationTimes = txtAllocationTimes.Text.ToInt();
                if (!txtAmount.Text.IsNullOrEmpty())
                    material.Amount = Decimal.Parse(txtAmount.Text);
                if (!txtAllocatedAmount.Text.IsNullOrEmpty())
                    material.AllocatedAmount = Decimal.Parse(txtAllocatedAmount.Text);
                if (!string.IsNullOrEmpty(cbbAllocationAwaitAccount.Text))
                {
                    material.AllocationAwaitAccount = ((Account)Utils.getSelectCbbItem(cbbAllocationAwaitAccount)).AccountNumber;
                }
                material.IsActive = chkActive.Checked;
                List<TIInitDetail> TIInitDetails = ((BindingList<TIInitDetail>)uGrid.DataSource).ToList();
                material.TIInitDetails.Clear();
                foreach (TIInitDetail item in TIInitDetails)
                {
                    item.TIInitID = material.ID;
                    material.TIInitDetails.Add(item);
                }
            }
            else
            {
                txtToolsCode.Text = material.ToolsCode;
                txtToolsCode.Enabled = false;
                txtToolsName.Text = material.ToolsName;

                if (material.MaterialGoodsCategoryID != null)
                {
                    foreach (var item in cbbToolsCategory.Rows)
                    {

                        if (((item.ListObject as MaterialGoodsCategory).ID == material.MaterialGoodsCategoryID))
                        {
                            cbbToolsCategory.SelectedRow = item;
                        }
                    }
                }

                txtUnit.Text = material.Unit;
                txtQuantity.Text = material.Quantity.ToString();
                cbbAllocationType.SelectedIndex = material.AllocationType ?? 0;

                txtUnitPrice.Text = material.UnitPrice.ToString();

                txtAllocationTimes.Text = material.AllocationTimes.ToString(); ;

                txtAmount.Text = material.Amount.ToString();
                txtAllocatedAmount.Text = material.AllocatedAmount.ToString();

                if (material.AllocationAwaitAccount != null)
                {
                    foreach (var item in cbbAllocationAwaitAccount.Rows)
                    {

                        if (((item.ListObject as Account).AccountNumber == material.AllocationAwaitAccount))
                        {
                            cbbAllocationAwaitAccount.SelectedRow = item;
                        }
                    }
                }
                chkActive.Checked = material.IsActive;
                BindingList<TIInitDetail> TIInitDetail = new BindingList<TIInitDetail>(material.TIInitDetails);
                uGrid.DataSource = TIInitDetail;
            }
        }
        #endregion

        #region event
        private void btnSaveContinue_Click(object sender, EventArgs e)
        {
            if (Save())
            {
                _Select = new TIInit();
                _Select.TypeID = 436;
                txtToolsCode.Text = null;
                txtToolsName.Text = null;
                cbbToolsCategory.SelectedRow = null;


                txtUnit.Text = null;
                txtQuantity.Text = null;
                cbbAllocationType.SelectedIndex = 0;
                txtUnitPrice.Text = null;
                txtAllocationTimes.Text = null;

                txtAmount.Text = null;
                txtAllocatedAmount.Text = null;
                cbbAllocationAwaitAccount.SelectedRow = null;
                TIInitDetails = new BindingList<TIInitDetail>();
                uGrid.DataSource = TIInitDetails;
                MSG.Information("Thêm mới CCDC thành công");
                isClose = false;
                ID = _Select.ID;
            }
            
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (Save())
            {
                isClose = false;
                this.Close();
                ID = _Select.ID;
            }
        }
        private bool Save()
        {
            if (!_Them)
            {
                if (_ITIInitService.CheckDeleteTIInit(_Select.ID))
                {
                    if (OldIsActive != chkActive.Checked)
                    {

                        var model = _ITIInitService.Getbykey(_Select.ID);
                        model.IsActive = chkActive.Checked;

                        _ITIInitService.BeginTran();
                        try
                        {
                            _ITIInitService.Update(model);
                            _ITIInitService.CommitTran();
                        }
                        catch (Exception ex)
                        {
                            _ITIInitService.RolbackTran();
                        }
                        return true;
                    }
                    else
                    {
                        MSG.Warning("Không thể sửa thông tin của CCDC này tại đây vì đã phát sinh chứng từ liên quan. Vui lòng thực hiện việc sửa đổi tại nghiệp vụ Điều chỉnh CCDC!");
                        return false;
                    }
                }
            }
            BindingData(_Select, true);
            if (CheckError())
            {
                //_ITIInitService.BeginTran();
                //if (_Them)
                //{
                //    _ITIInitService.CreateNew(_Select);
                //    _ITIInitService.CommitTran();
                //}
                //else
                //{
                //    _ITIInitService.Update(_Select);
                //    _ITIInitService.CommitTran();
                //}
                var session = _ITIInitService.GetSession();
                session.Clear();
                session = _ITIInitService.GetSession();
                using (var trans = session.BeginTransaction())
                {
                    try
                    {
                        if (_Them)
                        {
                            session.Save(_Select);
                        }
                        else
                        {
                            session.Update(_Select);
                        }
                        trans.Commit();
                    }
                    catch (Exception ex)
                    {
                        //_ITIInitService.RolbackTran();
                        MSG.Warning("Có lỗi xảy ra khi lưu công cụ dụng cụ." + ex.Message);
                        return false;
                    }
                    Utils.ClearCacheByType<TIInit>();
                    BindingList<TIInit> lst = Utils.ListTIInit;
                    return true;
                }

            }
            return false;
        }

        private MaterialGoods CreateMaterialGoods(MaterialGoods materialGoods)
        {
            materialGoods.ID = materialGoods.ID == Guid.Empty ? Guid.NewGuid() : materialGoods.ID;

            materialGoods.MaterialGoodsCode = _Select.ToolsCode;
            materialGoods.MaterialGoodsName = _Select.ToolsName;
            materialGoods.MaterialGoodsCategoryID = _Select.MaterialGoodsCategoryID;
            materialGoods.Quantity = _Select.Quantity ?? 0;
            materialGoods.AllocationType = _Select.AllocationType ?? 0;
            materialGoods.UnitPrice = _Select.UnitPrice;
            materialGoods.AllocationTimes = _Select.AllocationTimes ?? 0;
            materialGoods.Amount = _Select.Amount;
            materialGoods.AllocatedAmount = _Select.AllocatedAmount ?? 0;
            materialGoods.CostSetID = _Select.CostSetID;
            materialGoods.AllocationAwaitAccount = _Select.AllocationAwaitAccount;
            materialGoods.MaterialGoodsType = 1;
            materialGoods.IsActive = true;

            return materialGoods;
        }

        //ToolLedger CreateToolLedger(Guid id, Guid refID, Guid? Department, decimal quantity)
        //{
        //    ToolLedger toolLedger = new ToolLedger();
        //    toolLedger.ID = toolLedger.ID == Guid.Empty ? Guid.NewGuid() : toolLedger.ID;
        //    toolLedger.ToolsID = id;
        //    toolLedger.No = "OPN";
        //    toolLedger.TypeID = 703;
        //    toolLedger.Date = _Select.PostedDate;
        //    toolLedger.PostedDate = _Select.PostedDate;
        //    toolLedger.IncrementAllocationTime = _Select.AllocationTimes;
        //    toolLedger.DecrementAllocationTime = _Select.AllocationTimes - _Select.RemainAllocationTimes;
        //    toolLedger.UnitPrice = _Select.UnitPrice;
        //    toolLedger.IncrementQuantity = quantity;
        //    toolLedger.IncrementAmount = _Select.Quantity != 0 ? _Select.Amount * quantity / _Select.Quantity : 0;
        //    toolLedger.AllocationAmount = _Select.Quantity != 0 ? _Select.AllocationAmount * quantity / _Select.Quantity : 0;
        //    toolLedger.AllocatedAmount = _Select.Quantity != 0 ? _Select.AllocatedAmount * quantity / _Select.Quantity : 0;
        //    toolLedger.ReferenceID = refID;
        //    toolLedger.RemainingAllocaitonTimes = _Select.RemainAllocationTimes ?? 0;
        //    toolLedger.RemainingAmount = _Select.RemainAmount;
        //    toolLedger.DepartmentID = Department;
        //    toolLedger.RemainingQuantity = _Select.Quantity ?? 0;
        //    return toolLedger;
        //}

        private void btnClose_Click(object sender, EventArgs e)
        {
            TIInitDetails = new BindingList<TIInitDetail>();
            isClose = false;
            this.Close();
        }
        private void Control_ValueChanged(object sender, EventArgs e)
        {
            _IsEdit = true;
        }

        private bool CheckError()
        {
            // Kiểm tra null code và tên, và xem code đã trùng trong db chưa
            if (txtToolsCode.Text.IsNullOrEmpty() || txtToolsName.Text.IsNullOrEmpty())
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }
            else if (_Them && _ITIInitService.ExistsToolsCode(txtToolsCode.Text.Trim()))
            {
                MSG.Error(resSystem.MSG_Error_32);
                txtToolsCode.Focus();
                return false;

            }
            if (txtAllocationTimes.Text.IsNullOrEmpty())
            {
                MSG.Error("Số kỳ phân bổ bắt buộc nhập");
                txtAllocationTimes.Focus();
                return false;
            }
            if (TIInitDetails.Count > 0)
            {
                var sum = TIInitDetails.Sum(a => a.Quantity);
                var rate = Math.Round(TIInitDetails.Sum(a => a.Rate) ?? 0, 10, MidpointRounding.AwayFromZero);
                if (sum != _Select.Quantity)
                {
                    MSG.Warning(string.Format("Tổng số lượng nhập ở tab thiết lập phân bổ phải bằng số lượng ở thông tin chi tiết!"));
                    return false;
                }
                if (rate != 100)
                {
                    MSG.Warning(string.Format("Tổng tỷ lệ phân bổ ở tab thiết lập phân bổ phải bằng 100%!"));
                    return false;
                }
                foreach (TIInitDetail detail in TIInitDetails)
                {
                    if (detail.ObjectID == null)
                    {
                        MSG.Warning("Bạn chưa chọn đối tượng phân bổ");
                        return false;
                    }
                    if (detail.ObjectType == 1 && (detail.Quantity == 0 || detail.Quantity == null))
                    {
                        MSG.Warning("Phân bổ đối tượng phòng ban số lượng phải > 0");
                        return false;
                    }
                }
            }
            else
            {
                MSG.Warning(string.Format("Bạn chưa thiết lập phân bổ!"));
                return false;
            }
            return true;
        }



        private void FMaterialToolsDetail_Shown(object sender, EventArgs e)
        {
            _IsEdit = false;

        }

        private void txtQuantity_Validated(object sender, EventArgs e)
        {
            CalculateAmountAndAllocateAmount();
            if (!txtQuantity.Value.IsNullOrEmpty())
            {
                decimal totalquantity = Convert.ToDecimal(txtQuantity.Value.ToString());
                foreach (UltraGridRow row in uGrid.Rows)
                {
                    if (row.Cells["Quantity"].Value != null && row.Cells["Quantity"].Value.ToString() != "")
                    {
                        var quantity = Convert.ToDecimal(row.Cells["Quantity"].Value.ToString());
                        row.Cells["Rate"].Value = quantity / totalquantity * 100;
                    }

                }
            }
        }
        private void CalculateUnitPrice()
        {
            decimal Declare1 = 0, Declare2 = 0;
            if (!txtQuantity.Value.IsNullOrEmpty() && !txtAmount.Value.IsNullOrEmpty()
                                            && Decimal.TryParse(txtQuantity.Value.ToString(), out Declare1) && Decimal.TryParse(txtAmount.Value.ToString(), out Declare2))
            {
                if (Declare1 != 0)
                    txtUnitPrice.Value = Declare2 / Declare1;
            }
        }
        private void CalculateAmountAndAllocateAmount()
        {
            decimal Declare1 = 0, Declare2 = 0;
            if (!txtQuantity.Value.IsNullOrEmpty() && !txtUnitPrice.Value.IsNullOrEmpty()
                                            && Decimal.TryParse(txtQuantity.Value.ToString(), out Declare1) && Decimal.TryParse(txtUnitPrice.Value.ToString(), out Declare2))
            {
                txtAmount.Value = Declare1 * Declare2;
                if (!txtAllocationTimes.Text.IsNullOrEmpty() && !txtAmount.Value.IsNullOrEmpty() && txtAllocationTimes.Value.ToInt() != 0)
                {
                    txtAllocatedAmount.Value = Declare1 * Declare2 / txtAllocationTimes.Value.ToInt();
                }
            }
            if (txtUnitPrice.Text.IsNullOrEmpty())
            {
                CalculateUnitPrice();
            }
        }

        private void txtUnitPrice_Validated(object sender, EventArgs e)
        {
            CalculateAmountAndAllocateAmount();
        }

        private void cbbAllocationType_Validated(object sender, EventArgs e)
        {
            if (cbbAllocationType.SelectedIndex == 1)
            {
                txtAllocationTimes.Value = 1;
                txtAllocationTimes.Enabled = false;
            }
            else if (cbbAllocationType.SelectedIndex == 2)
            {
                txtAllocationTimes.Value = 2;
                txtAllocationTimes.Enabled = false;
            }
            else
            {
                txtAllocationTimes.Value = null;
                txtAllocationTimes.Enabled = true;
            }
            if (!txtAllocationTimes.Text.IsNullOrEmpty() && !txtAmount.Value.IsNullOrEmpty() && txtAllocationTimes.Value.ToInt() != 0)
            {
                decimal amount = Decimal.Parse(txtAmount.Value.ToString());
                txtAllocatedAmount.Value = amount / txtAllocationTimes.Value.ToInt();
            }
        }

        private void txtAllocationTimes_Validated(object sender, EventArgs e)
        {
            if (!txtAllocationTimes.Text.IsNullOrEmpty() && !txtAmount.Value.IsNullOrEmpty() && txtAllocationTimes.Value.ToInt() != 0)
            {
                decimal amount = Decimal.Parse(txtAmount.Value.ToString());
                txtAllocatedAmount.Value = amount / txtAllocationTimes.Value.ToInt();
            }
        }

        private void cbbToolsCategory_Validated(object sender, EventArgs e)
        {
            bool isContain = false;
            if (cbbToolsCategory.Value == null) return;
            foreach (var item in cbbToolsCategory.Rows)
            {
                if ((item.ListObject as MaterialGoodsCategory).MaterialGoodsCategoryCode.ToLower() == cbbToolsCategory.Value.ToString().ToLower()
                    || (item.ListObject as MaterialGoodsCategory).MaterialGoodsCategoryName.ToLower() == cbbToolsCategory.Value.ToString().ToLower())
                {
                    cbbToolsCategory.SelectedRow = item;
                    isContain = true;
                    break;
                }
            }
            if (!isContain)
            {
                cbbToolsCategory.Value = "";
                cbbToolsCategory.Focus();
                MSG.Warning("Giá trị không tồn tại");
            }
        }

        private void cbbAllocationAwaitAccount_Validated(object sender, EventArgs e)
        {
            bool isContain = false;
            if (cbbAllocationAwaitAccount.Value == null) return;
            foreach (var item in cbbAllocationAwaitAccount.Rows)
            {
                if ((item.ListObject as Account).AccountNumber == cbbAllocationAwaitAccount.Value.ToString())
                {
                    cbbAllocationAwaitAccount.SelectedRow = item;
                    isContain = true;
                    break;
                }
            }
            if (!isContain)
            {
                cbbAllocationAwaitAccount.Value = "";
                cbbAllocationAwaitAccount.Focus();
                MSG.Warning("Giá trị không tồn tại");
            }
        }

        private void uGrid_Error(object sender, ErrorEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            UltraGridCell activeCell = grid.ActiveCell;
            if (activeCell.Column.Key == "ExpenseItemID" || activeCell.Column.Key == "ObjectID" || activeCell.Column.Key == "CostAccount")
            {
                UltraCombo combo = activeCell.ValueListResolved as UltraCombo;
                if (false == combo.IsItemInList() && combo != null)
                {
                    MSG.Warning("Dữ liệu không có trong danh mục");
                    activeCell.Value = null;
                }
            }
            if (e != null)
            {
                e.Cancel = true;
            }
        }
        private void uGrid_AfterCellUpdate(object sender, CellEventArgs e)
        {

            UltraGridCell cell = ((UltraGrid)sender).ActiveCell;
            if (cell == null) return;
            if (cell.Column.Key.Contains("CostAccount") && cell.Value != null && cell.Value.ToString() != "")
            {
                List<Account> listAccounts = new List<Account>();
                listAccounts = ((List<Account>)((UltraCombo)cell.Column.ValueList).DataSource).Where(
                             p => p.AccountNumber == cell.Text.Trim() && p.GetProperty<Account, bool>("IsActive")).ToList();
                if (listAccounts.Count == 0)
                {
                    MSG.Warning(resSystem.MSG_System_30);
                    cell.Value = null;
                }
            }
            else if (cell.Column.Key == "ExpenseItemID" && e.Cell.ValueListResolved != null)
            {
                var combo = (UltraCombo)e.Cell.ValueListResolved;
                if (combo.SelectedRow != null)
                {
                    var ObjectIDs = (ExpenseItem)combo.SelectedRow.ListObject;

                    if (ObjectIDs.IsParentNode)
                    {
                        cell.Value = null;
                        MSG.Warning(resSystem.MSG_System_29);
                    }
                }
            }
            else if (cell.Column.Key.Contains("Quantity") && cell.Value != null && cell.Value.ToString() != "" && txtQuantity.Value != null && txtQuantity.Value.ToString() != "")
            {
                var quantity = Convert.ToDecimal(cell.Value.ToString());
                var totalQuantity = Convert.ToDecimal(txtQuantity.Value.ToString());
                UltraGridBand band = uGrid.DisplayLayout.Bands[0];
                if (band.Columns.Exists("Rate"))
                    cell.Row.Cells["Rate"].Value = totalQuantity != 0 ? quantity / totalQuantity * 100 : 0;
            }
            if (cell.Column.Key.Contains("ObjectID") && e.Cell.ValueListResolved != null)
            {
                var combo = (UltraCombo)e.Cell.ValueListResolved;
                if (combo.SelectedRow != null)
                {
                    var ObjectIDs = (Objects)combo.SelectedRow.ListObject;
                    if (ObjectIDs.IsParentNode)
                    {
                        combo.ToggleDropdown();
                        MSG.Warning(resSystem.MSG_System_29);
                        cell.Value = null;
                        return;
                    }
                    int i = TIInitDetails.Count(x => x.ObjectID == (Guid)cell.Row.Cells["ObjectID"].Value);
                    if (i > 1)
                    {
                        MSG.Warning("Đối tượng này đã được thiết lập phân bổ");
                        cell.Value = null;
                        return;
                    }
                }
            }
        }

        private void uGrid_CellChange(object sender, CellEventArgs e)
        {
            var cell = ((UltraGrid)sender).ActiveCell;
            if (cell == null) return;
            if (cell.Column.Key.Contains("ObjectID") && e.Cell.ValueListResolved != null)
            {
                var combo = (UltraCombo)e.Cell.ValueListResolved;
                if (combo.SelectedRow != null)
                {
                    var ObjectIDs = (Objects)combo.SelectedRow.ListObject;

                    UltraGridBand band = uGrid.DisplayLayout.Bands[0];
                    if (band.Columns.Exists("ObjectType"))
                        cell.Row.Cells["ObjectType"].Value = ObjectIDs.ObjectType;
                    if (ObjectIDs.ObjectType == 1)
                    {
                        cell.Row.Cells["CostAccount"].Value = ObjectIDs.CostAccount;
                        cell.Row.Cells["Rate"].Activation = Activation.AllowEdit;
                        cell.Row.Cells["Quantity"].Activation = Activation.AllowEdit;
                    }
                    else
                    {
                        cell.Row.Cells["Rate"].Activation = Activation.AllowEdit;
                        cell.Row.Cells["Quantity"].Activation = Activation.NoEdit;
                        cell.Row.Cells["Quantity"].Value = null;
                    }
                }
            }
        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            if (uGrid.ActiveCell != null)
            {
                this.RemoveRow4Grid<FEMContractSaleDetail>(uGrid);
                uGrid.Update();
            }
        }

        private void tsmAdd_Click(object sender, EventArgs e)
        {
            uGrid.AddNewRow4Grid();
        }
        #endregion

        private void txtAmount_Validated(object sender, EventArgs e)
        {
            CalculateUnitPrice();
        }

        private void FMaterialToolsDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
