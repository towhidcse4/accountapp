﻿namespace Accounting
{
    partial class FMaterialToolsDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem4 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem5 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem6 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraPanel3 = new Infragistics.Win.Misc.UltraPanel();
            this.txtUnitPrice = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtUnit = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbAllocationType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.txtToolsCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblMaterialGoodsName = new Infragistics.Win.Misc.UltraLabel();
            this.txtToolsName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.txtQuantity = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbToolsCategory = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.lblParentID = new Infragistics.Win.Misc.UltraLabel();
            this.txtAmount = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.lblRegistrationGroupName = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel24 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbAllocationAwaitAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAllocatedAmount = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtAllocationTimes = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraPanel4 = new Infragistics.Win.Misc.UltraPanel();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.btnSaveContinue = new Infragistics.Win.Misc.UltraButton();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.chkActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraPanel2 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraTabPageControl1.SuspendLayout();
            this.ultraPanel3.ClientArea.SuspendLayout();
            this.ultraPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAllocationType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToolsCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToolsName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbToolsCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAllocationAwaitAccount)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            this.ultraPanel4.ClientArea.SuspendLayout();
            this.ultraPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkActive)).BeginInit();
            this.ultraPanel2.ClientArea.SuspendLayout();
            this.ultraPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).BeginInit();
            this.ultraTabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel3);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(749, 220);
            // 
            // ultraPanel3
            // 
            // 
            // ultraPanel3.ClientArea
            // 
            this.ultraPanel3.ClientArea.Controls.Add(this.txtUnitPrice);
            this.ultraPanel3.ClientArea.Controls.Add(this.txtUnit);
            this.ultraPanel3.ClientArea.Controls.Add(this.cbbAllocationType);
            this.ultraPanel3.ClientArea.Controls.Add(this.ultraLabel1);
            this.ultraPanel3.ClientArea.Controls.Add(this.txtToolsCode);
            this.ultraPanel3.ClientArea.Controls.Add(this.lblMaterialGoodsName);
            this.ultraPanel3.ClientArea.Controls.Add(this.txtToolsName);
            this.ultraPanel3.ClientArea.Controls.Add(this.ultraLabel9);
            this.ultraPanel3.ClientArea.Controls.Add(this.txtQuantity);
            this.ultraPanel3.ClientArea.Controls.Add(this.ultraLabel2);
            this.ultraPanel3.ClientArea.Controls.Add(this.cbbToolsCategory);
            this.ultraPanel3.ClientArea.Controls.Add(this.ultraLabel4);
            this.ultraPanel3.ClientArea.Controls.Add(this.lblParentID);
            this.ultraPanel3.ClientArea.Controls.Add(this.txtAmount);
            this.ultraPanel3.ClientArea.Controls.Add(this.ultraLabel3);
            this.ultraPanel3.ClientArea.Controls.Add(this.ultraLabel8);
            this.ultraPanel3.ClientArea.Controls.Add(this.lblRegistrationGroupName);
            this.ultraPanel3.ClientArea.Controls.Add(this.ultraLabel24);
            this.ultraPanel3.ClientArea.Controls.Add(this.cbbAllocationAwaitAccount);
            this.ultraPanel3.ClientArea.Controls.Add(this.ultraLabel7);
            this.ultraPanel3.ClientArea.Controls.Add(this.txtAllocatedAmount);
            this.ultraPanel3.ClientArea.Controls.Add(this.txtAllocationTimes);
            this.ultraPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel3.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel3.Name = "ultraPanel3";
            this.ultraPanel3.Size = new System.Drawing.Size(749, 220);
            this.ultraPanel3.TabIndex = 0;
            // 
            // txtUnitPrice
            // 
            appearance1.TextHAlignAsString = "Right";
            this.txtUnitPrice.Appearance = appearance1;
            this.txtUnitPrice.AutoSize = false;
            this.txtUnitPrice.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtUnitPrice.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.txtUnitPrice.Location = new System.Drawing.Point(109, 119);
            this.txtUnitPrice.Name = "txtUnitPrice";
            this.txtUnitPrice.PromptChar = ' ';
            this.txtUnitPrice.Size = new System.Drawing.Size(250, 22);
            this.txtUnitPrice.TabIndex = 257;
            this.txtUnitPrice.Validated += new System.EventHandler(this.txtUnitPrice_Validated);
            // 
            // txtUnit
            // 
            this.txtUnit.AutoSize = false;
            this.txtUnit.Location = new System.Drawing.Point(482, 48);
            this.txtUnit.MaxLength = 25;
            this.txtUnit.Name = "txtUnit";
            this.txtUnit.Size = new System.Drawing.Size(250, 22);
            this.txtUnit.TabIndex = 254;
            // 
            // cbbAllocationType
            // 
            this.cbbAllocationType.AutoSize = false;
            this.cbbAllocationType.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAllocationType.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            valueListItem3.DataValue = "";
            valueListItem3.DisplayText = " ";
            valueListItem4.DataValue = "0";
            valueListItem4.DisplayText = "Phân bổ 1 lần";
            valueListItem5.DataValue = "1";
            valueListItem5.DisplayText = "Phân bổ 2 lần";
            valueListItem6.DataValue = "2";
            valueListItem6.DisplayText = "Phân bổ nhiều lần";
            this.cbbAllocationType.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem3,
            valueListItem4,
            valueListItem5,
            valueListItem6});
            this.cbbAllocationType.Location = new System.Drawing.Point(482, 83);
            this.cbbAllocationType.Name = "cbbAllocationType";
            this.cbbAllocationType.Size = new System.Drawing.Size(250, 22);
            this.cbbAllocationType.TabIndex = 256;
            this.cbbAllocationType.Validated += new System.EventHandler(this.cbbAllocationType_Validated);
            // 
            // ultraLabel1
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance2;
            this.ultraLabel1.Location = new System.Drawing.Point(13, 14);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(90, 22);
            this.ultraLabel1.TabIndex = 264;
            this.ultraLabel1.Text = "Mã CCDC (*)";
            // 
            // txtToolsCode
            // 
            this.txtToolsCode.AutoSize = false;
            this.txtToolsCode.Location = new System.Drawing.Point(109, 13);
            this.txtToolsCode.MaxLength = 25;
            this.txtToolsCode.Name = "txtToolsCode";
            this.txtToolsCode.Size = new System.Drawing.Size(250, 22);
            this.txtToolsCode.TabIndex = 251;
            // 
            // lblMaterialGoodsName
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextVAlignAsString = "Middle";
            this.lblMaterialGoodsName.Appearance = appearance3;
            this.lblMaterialGoodsName.Location = new System.Drawing.Point(384, 15);
            this.lblMaterialGoodsName.Name = "lblMaterialGoodsName";
            this.lblMaterialGoodsName.Size = new System.Drawing.Size(90, 22);
            this.lblMaterialGoodsName.TabIndex = 267;
            this.lblMaterialGoodsName.Text = "Tên CCDC (*)";
            // 
            // txtToolsName
            // 
            this.txtToolsName.AutoSize = false;
            this.txtToolsName.Location = new System.Drawing.Point(482, 15);
            this.txtToolsName.MaxLength = 255;
            this.txtToolsName.Name = "txtToolsName";
            this.txtToolsName.Size = new System.Drawing.Size(250, 22);
            this.txtToolsName.TabIndex = 252;
            // 
            // ultraLabel9
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.ultraLabel9.Appearance = appearance4;
            this.ultraLabel9.Location = new System.Drawing.Point(13, 84);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(90, 22);
            this.ultraLabel9.TabIndex = 268;
            this.ultraLabel9.Text = "Số lượng";
            // 
            // txtQuantity
            // 
            appearance5.TextHAlignAsString = "Right";
            this.txtQuantity.Appearance = appearance5;
            this.txtQuantity.AutoSize = false;
            this.txtQuantity.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtQuantity.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.txtQuantity.Location = new System.Drawing.Point(109, 83);
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.PromptChar = ' ';
            this.txtQuantity.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtQuantity.Size = new System.Drawing.Size(250, 22);
            this.txtQuantity.TabIndex = 255;
            this.txtQuantity.Validated += new System.EventHandler(this.txtQuantity_Validated);
            // 
            // ultraLabel2
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance6;
            this.ultraLabel2.Location = new System.Drawing.Point(13, 49);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(90, 22);
            this.ultraLabel2.TabIndex = 273;
            this.ultraLabel2.Text = "Loại";
            // 
            // cbbToolsCategory
            // 
            this.cbbToolsCategory.AutoSize = false;
            this.cbbToolsCategory.DisplayLayout.DefaultSelectedForeColor = System.Drawing.Color.Black;
            this.cbbToolsCategory.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbToolsCategory.Location = new System.Drawing.Point(109, 48);
            this.cbbToolsCategory.Name = "cbbToolsCategory";
            this.cbbToolsCategory.Size = new System.Drawing.Size(250, 22);
            this.cbbToolsCategory.TabIndex = 253;
            this.cbbToolsCategory.Validated += new System.EventHandler(this.cbbToolsCategory_Validated);
            // 
            // ultraLabel4
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance7;
            this.ultraLabel4.Location = new System.Drawing.Point(384, 50);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(90, 22);
            this.ultraLabel4.TabIndex = 274;
            this.ultraLabel4.Text = "Đơn vị tính";
            // 
            // lblParentID
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.TextVAlignAsString = "Middle";
            this.lblParentID.Appearance = appearance8;
            this.lblParentID.Location = new System.Drawing.Point(13, 154);
            this.lblParentID.Name = "lblParentID";
            this.lblParentID.Size = new System.Drawing.Size(90, 22);
            this.lblParentID.TabIndex = 263;
            this.lblParentID.Text = "Tổng giá trị";
            // 
            // txtAmount
            // 
            appearance9.TextHAlignAsString = "Right";
            this.txtAmount.Appearance = appearance9;
            this.txtAmount.AutoSize = false;
            this.txtAmount.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtAmount.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.txtAmount.Location = new System.Drawing.Point(109, 153);
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.NullText = "";
            this.txtAmount.PromptChar = ' ';
            this.txtAmount.Size = new System.Drawing.Size(250, 22);
            this.txtAmount.TabIndex = 259;
            this.txtAmount.Validated += new System.EventHandler(this.txtAmount_Validated);
            // 
            // ultraLabel3
            // 
            appearance10.BackColor = System.Drawing.Color.Transparent;
            appearance10.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance10;
            this.ultraLabel3.Location = new System.Drawing.Point(13, 119);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(90, 22);
            this.ultraLabel3.TabIndex = 272;
            this.ultraLabel3.Text = "Đơn giá";
            // 
            // ultraLabel8
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            appearance11.TextVAlignAsString = "Middle";
            this.ultraLabel8.Appearance = appearance11;
            this.ultraLabel8.Location = new System.Drawing.Point(384, 85);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(90, 22);
            this.ultraLabel8.TabIndex = 266;
            this.ultraLabel8.Text = "Kiểu phân bổ";
            // 
            // lblRegistrationGroupName
            // 
            appearance12.BackColor = System.Drawing.Color.Transparent;
            appearance12.TextVAlignAsString = "Middle";
            this.lblRegistrationGroupName.Appearance = appearance12;
            this.lblRegistrationGroupName.Location = new System.Drawing.Point(384, 120);
            this.lblRegistrationGroupName.Name = "lblRegistrationGroupName";
            this.lblRegistrationGroupName.Size = new System.Drawing.Size(98, 22);
            this.lblRegistrationGroupName.TabIndex = 265;
            this.lblRegistrationGroupName.Text = "Số kỳ phân bổ (*)";
            // 
            // ultraLabel24
            // 
            appearance13.BackColor = System.Drawing.Color.Transparent;
            appearance13.TextVAlignAsString = "Middle";
            this.ultraLabel24.Appearance = appearance13;
            this.ultraLabel24.Location = new System.Drawing.Point(11, 191);
            this.ultraLabel24.Name = "ultraLabel24";
            this.ultraLabel24.Size = new System.Drawing.Size(90, 22);
            this.ultraLabel24.TabIndex = 271;
            this.ultraLabel24.Text = "TK chờ phân bổ";
            // 
            // cbbAllocationAwaitAccount
            // 
            this.cbbAllocationAwaitAccount.AutoSize = false;
            this.cbbAllocationAwaitAccount.DisplayLayout.DefaultSelectedForeColor = System.Drawing.Color.Black;
            this.cbbAllocationAwaitAccount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAllocationAwaitAccount.Location = new System.Drawing.Point(109, 191);
            this.cbbAllocationAwaitAccount.Name = "cbbAllocationAwaitAccount";
            this.cbbAllocationAwaitAccount.Size = new System.Drawing.Size(250, 22);
            this.cbbAllocationAwaitAccount.TabIndex = 262;
            this.cbbAllocationAwaitAccount.Validated += new System.EventHandler(this.cbbAllocationAwaitAccount_Validated);
            // 
            // ultraLabel7
            // 
            appearance14.BackColor = System.Drawing.Color.Transparent;
            appearance14.TextVAlignAsString = "Middle";
            this.ultraLabel7.Appearance = appearance14;
            this.ultraLabel7.Location = new System.Drawing.Point(384, 155);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(90, 22);
            this.ultraLabel7.TabIndex = 269;
            this.ultraLabel7.Text = "Giá trị PB / Kỳ";
            // 
            // txtAllocatedAmount
            // 
            appearance15.TextHAlignAsString = "Right";
            this.txtAllocatedAmount.Appearance = appearance15;
            this.txtAllocatedAmount.AutoSize = false;
            this.txtAllocatedAmount.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtAllocatedAmount.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.txtAllocatedAmount.Location = new System.Drawing.Point(482, 155);
            this.txtAllocatedAmount.Name = "txtAllocatedAmount";
            this.txtAllocatedAmount.NullText = "";
            this.txtAllocatedAmount.PromptChar = ' ';
            this.txtAllocatedAmount.Size = new System.Drawing.Size(250, 22);
            this.txtAllocatedAmount.TabIndex = 260;
            // 
            // txtAllocationTimes
            // 
            appearance16.TextHAlignAsString = "Right";
            this.txtAllocationTimes.Appearance = appearance16;
            this.txtAllocationTimes.AutoSize = false;
            this.txtAllocationTimes.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtAllocationTimes.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Integer;
            this.txtAllocationTimes.Location = new System.Drawing.Point(482, 120);
            this.txtAllocationTimes.Name = "txtAllocationTimes";
            this.txtAllocationTimes.NullText = "";
            this.txtAllocationTimes.PromptChar = ' ';
            this.txtAllocationTimes.Size = new System.Drawing.Size(250, 22);
            this.txtAllocationTimes.TabIndex = 258;
            this.txtAllocationTimes.Validated += new System.EventHandler(this.txtAllocationTimes_Validated);
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.ultraPanel4);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(749, 220);
            // 
            // ultraPanel4
            // 
            // 
            // ultraPanel4.ClientArea
            // 
            this.ultraPanel4.ClientArea.Controls.Add(this.uGrid);
            this.ultraPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel4.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel4.Name = "ultraPanel4";
            this.ultraPanel4.Size = new System.Drawing.Size(749, 220);
            this.ultraPanel4.TabIndex = 0;
            // 
            // uGrid
            // 
            this.uGrid.ContextMenuStrip = this.contextMenuStrip1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid.DisplayLayout.Appearance = appearance17;
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance18.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance18.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance18.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid.DisplayLayout.GroupByBox.Appearance = appearance18;
            appearance19.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid.DisplayLayout.GroupByBox.BandLabelAppearance = appearance19;
            this.uGrid.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance20.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance20.BackColor2 = System.Drawing.SystemColors.Control;
            appearance20.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance20.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid.DisplayLayout.GroupByBox.PromptAppearance = appearance20;
            appearance21.BackColor = System.Drawing.SystemColors.Window;
            appearance21.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid.DisplayLayout.Override.ActiveCellAppearance = appearance21;
            appearance22.BackColor = System.Drawing.SystemColors.Highlight;
            appearance22.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid.DisplayLayout.Override.ActiveRowAppearance = appearance22;
            this.uGrid.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid.DisplayLayout.Override.CardAreaAppearance = appearance23;
            appearance24.BorderColor = System.Drawing.Color.Silver;
            appearance24.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid.DisplayLayout.Override.CellAppearance = appearance24;
            this.uGrid.DisplayLayout.Override.CellPadding = 0;
            appearance25.BackColor = System.Drawing.SystemColors.Control;
            appearance25.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance25.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance25.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance25.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid.DisplayLayout.Override.GroupByRowAppearance = appearance25;
            appearance26.TextHAlignAsString = "Left";
            this.uGrid.DisplayLayout.Override.HeaderAppearance = appearance26;
            this.uGrid.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance27.BackColor = System.Drawing.SystemColors.Window;
            appearance27.BorderColor = System.Drawing.Color.Silver;
            this.uGrid.DisplayLayout.Override.RowAppearance = appearance27;
            this.uGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid.DisplayLayout.Override.TemplateAddRowAppearance = appearance28;
            this.uGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid.Location = new System.Drawing.Point(0, 0);
            this.uGrid.Name = "uGrid";
            this.uGrid.Size = new System.Drawing.Size(749, 220);
            this.uGrid.TabIndex = 1;
            this.uGrid.Text = "uGrid";
            this.uGrid.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid_AfterCellUpdate);
            this.uGrid.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid_CellChange);
            this.uGrid.Error += new Infragistics.Win.UltraWinGrid.ErrorEventHandler(this.uGrid_Error);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmAdd,
            this.tsmDelete});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(205, 48);
            // 
            // tsmAdd
            // 
            this.tsmAdd.Name = "tsmAdd";
            this.tsmAdd.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.tsmAdd.Size = new System.Drawing.Size(204, 22);
            this.tsmAdd.Text = "Thêm một dòng";
            this.tsmAdd.Click += new System.EventHandler(this.tsmAdd_Click);
            // 
            // tsmDelete
            // 
            this.tsmDelete.Name = "tsmDelete";
            this.tsmDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.tsmDelete.Size = new System.Drawing.Size(204, 22);
            this.tsmDelete.Text = "Xóa một dòng";
            this.tsmDelete.Click += new System.EventHandler(this.tsmDelete_Click);
            // 
            // btnClose
            // 
            appearance29.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance29;
            this.btnClose.Location = new System.Drawing.Point(658, 15);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 16;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            appearance30.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance30;
            this.btnSave.Location = new System.Drawing.Point(577, 15);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 14;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnSaveContinue
            // 
            appearance31.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSaveContinue.Appearance = appearance31;
            this.btnSaveContinue.Location = new System.Drawing.Point(441, 16);
            this.btnSaveContinue.Name = "btnSaveContinue";
            this.btnSaveContinue.Size = new System.Drawing.Size(130, 30);
            this.btnSaveContinue.TabIndex = 15;
            this.btnSaveContinue.Text = "Lưu và Thêm mới";
            this.btnSaveContinue.Click += new System.EventHandler(this.btnSaveContinue_Click);
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.btnClose);
            this.ultraPanel1.ClientArea.Controls.Add(this.btnSaveContinue);
            this.ultraPanel1.ClientArea.Controls.Add(this.btnSave);
            this.ultraPanel1.ClientArea.Controls.Add(this.chkActive);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 298);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(753, 57);
            this.ultraPanel1.TabIndex = 218;
            // 
            // chkActive
            // 
            appearance32.TextVAlignAsString = "Middle";
            this.chkActive.Appearance = appearance32;
            this.chkActive.BackColor = System.Drawing.Color.Transparent;
            this.chkActive.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkActive.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkActive.Location = new System.Drawing.Point(14, 15);
            this.chkActive.Name = "chkActive";
            this.chkActive.Size = new System.Drawing.Size(250, 29);
            this.chkActive.TabIndex = 12;
            this.chkActive.Text = "Ngừng phân bổ";
            // 
            // ultraPanel2
            // 
            // 
            // ultraPanel2.ClientArea
            // 
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel5);
            this.ultraPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraPanel2.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel2.Name = "ultraPanel2";
            this.ultraPanel2.Size = new System.Drawing.Size(753, 52);
            this.ultraPanel2.TabIndex = 219;
            // 
            // ultraLabel5
            // 
            appearance33.BackColor = System.Drawing.Color.Transparent;
            appearance33.TextHAlignAsString = "Center";
            appearance33.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance33;
            this.ultraLabel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel5.Location = new System.Drawing.Point(0, 0);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(753, 52);
            this.ultraLabel5.TabIndex = 227;
            this.ultraLabel5.Text = "Danh mục công cụ dụng cụ";
            // 
            // ultraTabControl1
            // 
            this.ultraTabControl1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl2);
            this.ultraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraTabControl1.Location = new System.Drawing.Point(0, 52);
            this.ultraTabControl1.Name = "ultraTabControl1";
            this.ultraTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.ultraTabControl1.Size = new System.Drawing.Size(753, 246);
            this.ultraTabControl1.TabIndex = 220;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "Thông tin chi tiết";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "Thiết lập phân bổ";
            this.ultraTabControl1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(749, 220);
            // 
            // FMaterialToolsDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(753, 355);
            this.Controls.Add(this.ultraTabControl1);
            this.Controls.Add(this.ultraPanel2);
            this.Controls.Add(this.ultraPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FMaterialToolsDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Danh mục công cụ dụng cụ";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FMaterialToolsDetail_FormClosed);
            this.Shown += new System.EventHandler(this.FMaterialToolsDetail_Shown);
            this.ultraTabPageControl1.ResumeLayout(false);
            this.ultraPanel3.ClientArea.ResumeLayout(false);
            this.ultraPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAllocationType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToolsCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToolsName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbToolsCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAllocationAwaitAccount)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            this.ultraPanel4.ClientArea.ResumeLayout(false);
            this.ultraPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkActive)).EndInit();
            this.ultraPanel2.ClientArea.ResumeLayout(false);
            this.ultraPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).EndInit();
            this.ultraTabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraButton btnSaveContinue;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraPanel ultraPanel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkActive;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.Misc.UltraPanel ultraPanel3;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtUnitPrice;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtUnit;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbAllocationType;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtToolsCode;
        private Infragistics.Win.Misc.UltraLabel lblMaterialGoodsName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtToolsName;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtQuantity;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbToolsCategory;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel lblParentID;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtAmount;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.Misc.UltraLabel lblRegistrationGroupName;
        private Infragistics.Win.Misc.UltraLabel ultraLabel24;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAllocationAwaitAccount;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtAllocatedAmount;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtAllocationTimes;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.Misc.UltraPanel ultraPanel4;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripMenuItem tsmDelete;
    }
}