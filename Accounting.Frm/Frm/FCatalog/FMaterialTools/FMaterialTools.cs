﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using Accounting.TextMessage;
using System.Linq;
using Accounting.Core;

namespace Accounting
{

    public partial class FMaterialTools : CatalogBase
    {
        #region khai báo
        private readonly IMaterialGoodsService _IMaterialGoodsService = Utils.IMaterialGoodsService;
        private readonly ITIInitService _ITIInitService = Utils.ITIInitService;
        private Guid selectedGuid;
        private DateTime ngayHoachToan;
        private DateTime dtBegin;
        private DateTime dtEnd;
        private readonly List<Item> _lstItems = Utils.ObjConstValue.SelectTimes;
        List<VoucherTools> listVoucherTools = new List<VoucherTools>();

        #endregion

        public FMaterialTools()
        {
            InitializeComponent();
            LoadDuLieu();
            ToolTip ToolTip1 = new ToolTip();
            ToolTip1.SetToolTip(btnAdd, "Thêm (Ctrl + N)");
            ToolTip ToolTip2 = new ToolTip();
            ToolTip2.SetToolTip(btnEdit, "Xem (Ctrl + E)");
            ToolTip ToolTip3 = new ToolTip();
            ToolTip3.SetToolTip(btnDelete, "Xóa (Ctrl + D)");
            if (uGrid.Rows.Count > 0) uGrid.Rows[0].Selected = true;
            ngayHoachToan = (DateTime)Utils.StringToDateTime(ConstFrm.DbStartDate);
            //dtBegin = ngayHoachToan.AddDays(1 - ngayHoachToan.Day);
            //dtEnd = ngayHoachToan;
            //dteToDate.DateTime = dtEnd;
            //dteFromDate.DateTime = dtBegin;
            cbbAboutTime.DataSource = _lstItems;
            cbbAboutTime.DisplayMember = "Name";
            Utils.ConfigGrid(cbbAboutTime, ConstDatabase.ConfigXML_TableName);
            cbbAboutTime.SelectedRow = cbbAboutTime.Rows[7];
            cbbAboutTime.DisplayLayout.AutoFitStyle = AutoFitStyle.ExtendLastColumn;
        }

        protected override void AddFunction()
        {
            new FMaterialToolsDetail().ShowDialog(this);
            if (!FMaterialToolsDetail.isClose)
            {
                Utils.ClearCacheByType<TIInit>();
                LoadDuLieu();
            }
        }
        protected override void EditFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                TIInit temp = uGrid.Selected.Rows[0].ListObject as TIInit;
                new FMaterialToolsDetail(temp).ShowDialog(this);
                if (!FMaterialToolsDetail.isClose)
                {
                    Utils.ClearCacheByType<TIInit>();
                    LoadDuLieu();
                }
            }
            else Accounting.TextMessage.MSG.Error(Accounting.TextMessage.resSystem.MSG_System_04);
        }
        protected override void DeleteFunction()
        {
            bool a = uGrid.Selected.Rows.Count > 0;
            bool b = uGrid.ActiveRow != null;
            if (a || b)
            {
                if (!(DialogResult.Yes == MessageBox.Show("Bạn có muốn xóa những dữ liệu này không ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question)))
                    return;
                SelectedRowsCollection lst = uGrid.Selected.Rows;
                var strResult = new List<Result>();
                foreach (UltraGridRow ultraGridRow in lst)
                {
                    var result = new Result();
                    TIInit temp = (TIInit)ultraGridRow.ListObject;
                    TIInit data = Utils.ITIInitService.Getbykey(temp.ID);
                    try
                    {
                        if (!_ITIInitService.CheckDeleteTIInit(data.ID))
                        {

                            Utils.ITIInitService.BeginTran();
                            Utils.ITIInitService.Delete(data);
                            Utils.ITIInitService.CommitTran();
                            Utils.ClearCacheByType<TIInit>();
                            result.Reason = "Xóa thành công";
                            result.Code = data.ToolsCode;
                            result.IsSucceed = true;
                            strResult.Add(result);


                        }
                        else
                        {
                            result.Reason = "Không thể xóa do dữ liệu đã phát sinh chứng từ liên quan";
                            result.Code = data.ToolsCode;
                            result.IsSucceed = false;
                            strResult.Add(result);
                        }
                    }
                    catch (Exception ex)
                    {
                        Utils.ITIInitService.RolbackTran();
                        MSG.Error("Xóa chứng từ thất bại");
                    }
                }
                if (lst.Count > 1 && strResult.Count > 0) new MsgResult(strResult, false).ShowDialog(this);
                else MSG.Information(strResult.First().Reason);
                LoadDuLieu();
            }
            //if (uGrid.Selected.Rows.Count > 0)
            //{
            //    TIInit temp = uGrid.Selected.Rows[0].ListObject as TIInit;
            //    // Để xóa danh mục TSCĐ --> TSCĐ phải chưa ghi tăng, hoặc không phải là khai báo
            //    if (_ITIInitService.CheckDeleteTIInit(temp.ID))
            //    {
            //        MSG.Warning("Không thể xóa CCDC này vì CCDC này đã có phát sinh chứng từ liên quan!");
            //        return;
            //    }

            //    if (MSG.Question(string.Format(resSystem.MSG_System_05, temp.ToolsCode)) == System.Windows.Forms.DialogResult.Yes)
            //    {
            //        try
            //        {
            //            _ITIInitService.BeginTran();
            //            _ITIInitService.Delete(temp);
            //            _ITIInitService.CommitTran();
            //            Utils.ClearCacheByType<TIInit>();
            //            LoadDuLieu();
            //        }
            //        catch (Exception ex)
            //        {
            //            _ITIInitService.RolbackTran();
            //            throw ex;
            //        }
            //    }
            //}
            //else
            //    MSG.Error(resSystem.MSG_System_06);
        }

        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }
        private void LoadDuLieu(bool configGrid)
        {
            WaitingFrm.StartWaiting();
            List<TIInit> lstMaterialGoods = _ITIInitService.FindByDeclareType(0);
            _ITIInitService.UnbindSession(lstMaterialGoods);
            lstMaterialGoods = _ITIInitService.FindByDeclareType(0);
            uGrid.DataSource = lstMaterialGoods.ToArray();           
            if (configGrid) ConfigGrid(uGrid);           
            WaitingFrm.StopWaiting();
        }
        private void LoadDuLieuExport(bool configGrid)
        {
            WaitingFrm.StartWaiting();
            List<TIInit> lstMaterialGoods = _ITIInitService.FindByDeclareType(0);
            _ITIInitService.UnbindSession(lstMaterialGoods);
            lstMaterialGoods = _ITIInitService.FindByDeclareType(0);
            if (lstMaterialGoods != null)
            {
                if (lstMaterialGoods.Count != 0)
                { int count = lstMaterialGoods.Count;
                    for(int k =0; k<count;k++)
                    {
                        if (lstMaterialGoods[k].TIInitDetails != null) {
                            if (lstMaterialGoods[k].TIInitDetails.Count != 0)
                            {

                                for (int i = 0; i < lstMaterialGoods[k].TIInitDetails.Count; i++)
                                {
                                    if (i == 0)
                                    {
                                        if (lstMaterialGoods[k].TIInitDetails[i].ObjectID != null)
                                        {
                                            string code1 = Utils.IDepartmentService.GetDepartmentCodeByGuid(lstMaterialGoods[k].TIInitDetails[i].ObjectID);
                                            string code2 = Utils.IExpenseItemService.GetExpenseItemCodeByGuid(lstMaterialGoods[k].TIInitDetails[i].ObjectID);
                                            if (!code1.IsNullOrEmpty())
                                            {
                                                lstMaterialGoods[k].DoiTuongPhanBo = code1;
                                            }
                                            else if (!code2.IsNullOrEmpty())
                                            {
                                                lstMaterialGoods[k].DoiTuongPhanBo = code2;
                                            }
                                        }
                                        lstMaterialGoods[k].SoLuongPhanBo = lstMaterialGoods[k].TIInitDetails[i].Quantity;
                                        lstMaterialGoods[k].TyLePhanBo = lstMaterialGoods[k].TIInitDetails[i].Rate;
                                        lstMaterialGoods[k].TaiKhoanChiPhi = lstMaterialGoods[k].TIInitDetails[i].CostAccount;
                                        if (lstMaterialGoods[k].TIInitDetails[i].ExpenseItemID != null)
                                        {
                                            string code1 = Utils.IExpenseItemService.GetExpenseItemCodeByGuid(lstMaterialGoods[k].TIInitDetails[i].ExpenseItemID);
                                            if (!code1.IsNullOrEmpty())
                                            {
                                                lstMaterialGoods[k].KhoanMucChiPhi = code1;
                                            }
                                        }

                                    }
                                    else
                                    {
                                        TIInit tIInit = new TIInit();
                                        tIInit.ToolsCode = lstMaterialGoods[k].ToolsCode;
                                        if (lstMaterialGoods[k].TIInitDetails[i].ObjectID != null)
                                        {
                                            string code1 = Utils.IDepartmentService.GetDepartmentCodeByGuid(lstMaterialGoods[k].TIInitDetails[i].ObjectID);
                                            string code2 = Utils.IExpenseItemService.GetExpenseItemCodeByGuid(lstMaterialGoods[k].TIInitDetails[i].ObjectID);
                                            if (!code1.IsNullOrEmpty())
                                            {
                                                tIInit.DoiTuongPhanBo = code1;
                                            }
                                            else if (!code2.IsNullOrEmpty())
                                            {
                                                tIInit.DoiTuongPhanBo = code2;
                                            }
                                        }
                                        tIInit.SoLuongPhanBo = lstMaterialGoods[k].TIInitDetails[i].Quantity;
                                        tIInit.TyLePhanBo = lstMaterialGoods[k].TIInitDetails[i].Rate;
                                        tIInit.TaiKhoanChiPhi = lstMaterialGoods[k].TIInitDetails[i].CostAccount;
                                        if (lstMaterialGoods[k].TIInitDetails[i].ExpenseItemID != null)
                                        {
                                            string code1 = Utils.IExpenseItemService.GetExpenseItemCodeByGuid(lstMaterialGoods[k].TIInitDetails[i].ExpenseItemID);
                                            if (!code1.IsNullOrEmpty())
                                            {
                                                tIInit.KhoanMucChiPhi = code1;
                                            }
                                        }
                                        lstMaterialGoods.Add(tIInit);
                                    }
                                }
                            }
                        }

                    }
                }
            }
            uGridExport.DataSource = lstMaterialGoods.OrderBy(n=>n.ToolsCode).ToArray();
            if (configGrid) { Utils.ConfigGrid(uGridExport,ConstDatabase.Tiinit_TableNameExport); }
            WaitingFrm.StopWaiting();
        }

        #region Utils
        void ConfigGrid(UltraGrid utralGrid)
        {
            utralGrid.ConfigGrid(ConstDatabase.MaterialTools_TableName);
            this.ConfigEachColumn4Grid(0, uGrid.DisplayLayout.Bands[0].Columns["MaterialGoodsCategoryID"], utralGrid);
            uGrid.DisplayLayout.Bands[0].Columns["Quantity"].FormatNumberic(ConstDatabase.Format_Quantity);
            uGrid.DisplayLayout.Bands[0].Columns["Amount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["AllocationTimes"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["AllocatedAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["UnitPrice"].FormatNumberic(ConstDatabase.Format_DonGiaQuyDoi);
            uGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            uGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Extended;
        }
        #endregion


        private void uGrid_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                TIInit Node = uGrid.Selected.Rows[0].ListObject as TIInit;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void uGrid_DoubleClick(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tsmAdd_Click(object sender, EventArgs e)
        {
            AddFunction();

        }

        private void tsmEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            LoadDuLieu(false);
        }

        private void uGrid_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }

        private void FMaterialTools_Load(object sender, EventArgs e)
        {

        }

        private void uGrid_BeforeRowActivate(object sender, RowEventArgs e)
        {
            if (e.Row == null) return;
            var temp = e.Row.ListObject as TIInit;
            if (temp == null) return;
            txtMaterialcode.Text = ":   " + temp.ToolsCode;
            txtMaterialName.Text = ":   " + temp.ToolsName;
            txtUnit.Text = ":   " + temp.UnitPrice.ToStringNumbericFormat(ConstDatabase.Format_TienVND);
            txtQuantity.Text = ":   " + temp.Quantity.ToStringNumbericFormat(ConstDatabase.Format_Quantity);
            txtAmount.Text = ":   " + temp.Amount.ToStringNumbericFormat(ConstDatabase.Format_TienVND);
            var voucher = _IMaterialGoodsService.GetListVouchers(temp.ID).OrderByDescending(n => n.Date).ToList();
            foreach (var item in voucher)
            {
                var firstOrDefault = Utils.ListType.FirstOrDefault(x => x.ID == item.TypeID);
                if (firstOrDefault != null)
                    item.TypeName = firstOrDefault.TypeName;
            }
            uGridDetail.DataSource = voucher;
            uGridDetail.ConfigGrid(ConstDatabase.VoucherFixedAsset_KeyName);
            selectedGuid = temp.ID;
            foreach (var column in uGridDetail.DisplayLayout.Bands[0].Columns)
            {
                this.ConfigEachColumn4Grid(0, column, uGridDetail);
            }
        }

        private void uGridDetail_BeforeRowActivate(object sender, RowEventArgs e)
        {

        }

        private void btnViewVoucher_Click(object sender, EventArgs e)
        {
            if (uGridDetail.ActiveRow == null) return;
            var voucher = uGridDetail.ActiveRow.ListObject as VoucherTools;
            if (voucher == null) return;
            Utils.ViewVoucherSelected(voucher.ID, voucher.TypeID);
        }

        private void uGridDetail_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (uGridDetail.ActiveRow == null) return;
            var voucher = uGridDetail.ActiveRow.ListObject as VoucherTools;
            if (voucher == null) return;
            Utils.ViewVoucherSelected(voucher.ID, voucher.TypeID);
        }

        private void cbbAboutTime_ValueChanged(object sender, EventArgs e)
        {
            if (cbbAboutTime.SelectedRow != null)
            {
                var model = cbbAboutTime.SelectedRow.ListObject as Item;

                Utils.GetDateBeginDateEnd(ngayHoachToan.Year,ngayHoachToan, model, out dtBegin, out dtEnd);
                dteFromDate.DateTime = dtBegin;
                dteToDate.DateTime = dtEnd;
                var voucher = _IMaterialGoodsService.GetListVouchers(selectedGuid).Where(p => p.Date >= dteFromDate.DateTime && p.Date <= dteToDate.DateTime).ToList();
                foreach (var item in voucher)
                {
                    var firstOrDefault = Utils.ListType.FirstOrDefault(x => x.ID == item.TypeID);
                    if (firstOrDefault != null)
                        item.TypeName = firstOrDefault.TypeName;
                }

                uGridDetail.DataSource = voucher;
                uGridDetail.ConfigGrid(ConstDatabase.VoucherAccountingObject_KeyName);
               // uGridDetail.DisplayLayout.Bands[0].Columns["TotalAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            }
        }

        private void dteFromDate_ValueChanged(object sender, EventArgs e)
        {
            if (dteFromDate.DateTime <= dteToDate.DateTime)
            {
                var voucher = _IMaterialGoodsService.GetListVouchers(selectedGuid).Where(p => p.Date >= dteFromDate.DateTime && p.Date <= dteToDate.DateTime).ToList();
                foreach (var item in voucher)
                {
                    var firstOrDefault = Utils.ListType.FirstOrDefault(x => x.ID == item.TypeID);
                    if (firstOrDefault != null)
                        item.TypeName = firstOrDefault.TypeName;
                }
                uGridDetail.DataSource = voucher;
                uGridDetail.ConfigGrid(ConstDatabase.VoucherAccountingObject_KeyName);
                uGridDetail.DisplayLayout.Bands[0].Columns["TotalAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            }
            else
            {
                // MSG.Warning("Ngày đến phải lớn hơn ngày bắt đầu !");
            }
        }

        private void dteToDate_ValueChanged(object sender, EventArgs e)
        {
            if (dteFromDate.DateTime <= dteToDate.DateTime)
            {
                var voucher = _IMaterialGoodsService.GetListVouchers(selectedGuid).Where(p => p.Date >= dteFromDate.DateTime && p.Date <= dteToDate.DateTime).ToList();
                foreach (var item in voucher)
                {
                    var firstOrDefault = Utils.ListType.FirstOrDefault(x => x.ID == item.TypeID);
                    if (firstOrDefault != null)
                        item.TypeName = firstOrDefault.TypeName;
                }
                uGridDetail.DataSource = voucher;
                uGridDetail.ConfigGrid(ConstDatabase.VoucherAccountingObject_KeyName);
                uGridDetail.DisplayLayout.Bands[0].Columns["TotalAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            }
            else
            {
                // MSG.Warning("Ngày đến phải lớn hơn ngày bắt đầu !");
            }
        }

        private void FMaterialTools_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGrid.ResetText();
            uGrid.ResetUpdateMode();
            uGrid.ResetExitEditModeOnLeave();
            uGrid.ResetRowUpdateCancelAction();
            uGrid.DataSource = null;
            uGrid.Layouts.Clear();
            uGrid.ResetLayouts();
            uGrid.ResetDisplayLayout();
            uGrid.Refresh();
            uGrid.ClearUndoHistory();
            uGrid.ClearXsdConstraints();
            
            uGridDetail.ResetText();
            uGridDetail.ResetUpdateMode();
            uGridDetail.ResetExitEditModeOnLeave();
            uGridDetail.ResetRowUpdateCancelAction();
            uGridDetail.DataSource = null;
            uGridDetail.Layouts.Clear();
            uGridDetail.ResetLayouts();
            uGridDetail.ResetDisplayLayout();
            uGridDetail.Refresh();
            uGridDetail.ClearUndoHistory();
            uGridDetail.ClearXsdConstraints();
        }

        private void FMaterialTools_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            UltraGrid ultraGridExport = uGridExport;
            ultraGridExport.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            LoadDuLieuExport(true);
            //Utils.ConfigGrid(ultraGridExport, TextMessage.ConstDatabase.MaterialGoods_TableNameExportExcel);
            Utils.ExportExcel(ultraGridExport, "Export_DMCongCuDungCu");
        }
    }
}
