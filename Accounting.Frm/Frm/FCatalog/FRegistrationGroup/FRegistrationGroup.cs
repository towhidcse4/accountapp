﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinTree;
using System.Data;

namespace Accounting
{
    public partial class FRegistrationGroup : CatalogBase //UserControl
    {
        #region khai báo
        private readonly IRegistrationGroupService _IRegistrationGroupService;
        List<RegistrationGroup> dsRegistrationGroup = new List<RegistrationGroup>();
        #endregion

        #region khởi tạo
        public FRegistrationGroup()
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _IRegistrationGroupService = IoC.Resolve<IRegistrationGroupService>();

            this.uTree.InitializeDataNode += new Infragistics.Win.UltraWinTree.InitializeDataNodeEventHandler(Utils.uTree_InitializeDataNode);
            this.uTree.AfterDataNodesCollectionPopulated += new Infragistics.Win.UltraWinTree.AfterDataNodesCollectionPopulatedEventHandler(Utils.uTree_AfterDataNodesCollectionPopulated);
            this.uTree.ColumnSetGenerated += new Infragistics.Win.UltraWinTree.ColumnSetGeneratedEventHandler(this.uTree_ColumnSetGenerated);
            this.uTree.MouseDown += new System.Windows.Forms.MouseEventHandler(this.uTree_MouseDown);
            this.uTree.DoubleClick += new System.EventHandler(this.uTree_DoubleClick);
            #endregion

            #region Tạo dữ liệu ban đầu
            LoadDuLieu();
            #endregion
        }

        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }
        private void LoadDuLieu(bool configTree)
        {
            WaitingFrm.StartWaiting();
            #region Lấy dữ liệu từ CSDL
            dsRegistrationGroup = _IRegistrationGroupService.GetAll_OrderBy();
            _IRegistrationGroupService.UnbindSession(dsRegistrationGroup);
            dsRegistrationGroup = _IRegistrationGroupService.GetAll_OrderBy();
            //foreach (var item in dsRegistrationGroup)
            //{
            //    if (item.IsActive)
            //    {
            //        item.IsActiveString = "Đang hoạt động";
            //    }
            //    else
            //    {
            //        item.IsActiveString = "Chưa hoạt động";
            //    }
            //}

            #endregion

            #region Xử lý dữ liệu
            #endregion

            #region hiển thị và xử lý hiển thị
            DataSet ds = Utils.ToDataSet<RegistrationGroup>(dsRegistrationGroup, ConstDatabase.RegistrationGroup_TableName);
            uTree.SetDataBinding(ds, ConstDatabase.RegistrationGroup_TableName);

            if (configTree) ConfigTree(uTree);
            #endregion
            WaitingFrm.StopWaiting();
        }
        #endregion

        #region Nghiệp vụ
        #region ContentMenu
        private void tsmAdd_Click_1(object sender, EventArgs e)
        {
            AddFunction();
        }
        private void tsmEdit_Click_1(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tsmDelete_Click_1(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void tmsReLoad_Click_1(object sender, EventArgs e)
        {
            ResetFunction();
        }
        protected override void ResetFunction()
        {
            LoadDuLieu(false);
        }
        #endregion

        #region Button
        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void btnDelete_Click_1(object sender, EventArgs e)
        {
            DeleteFunction();
        }
        #endregion

        /// <summary>
        /// Nghiệp vụ Add
        /// </summary>
        protected override void AddFunction()
        {
            new FRegistrationGroupDetail().ShowDialog(this);
            if (!FRegistrationGroupDetail.isClose) LoadDuLieu();
        }

        /// <summary>
        /// Nghiệp vụ Edit
        /// </summary>
        protected override void EditFunction()
        {
            if (uTree.SelectedNodes.Count > 0)
            {
                RegistrationGroup temp = dsRegistrationGroup.Single(k => k.ID == (new Guid(uTree.SelectedNodes[0].Text)));
                if (Utils.checkRelationVoucher_RegistrationGroup(temp))
                {
                    MSG.Error("Không thể sửa Nhóm đăng ký vì có phát sinh chứng từ liên quan");
                    return;
                }
                else
                {
                    new FRegistrationGroupDetail(temp).ShowDialog(this);
                    if (!FRegistrationGroupDetail.isClose) LoadDuLieu();
                }
            }
            else
                MSG.Warning(string.Format(resSystem.MSG_Catalog3, "một Nhóm đăng ký"));
        }

        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        protected override void DeleteFunction()
        {
            if (uTree.SelectedNodes.Count > 0)
            {
                RegistrationGroup temp = dsRegistrationGroup.Single(k => k.ID == (new Guid(uTree.SelectedNodes[0].Text)));
                if (MSG.Question(string.Format(resSystem.MSG_System_05, temp.RegistrationGroupCode)) == System.Windows.Forms.DialogResult.Yes)
                {
                    if (Utils.checkRelationVoucher_RegistrationGroup(temp))
                    {
                        MSG.Error("Không thể xóa Nhóm đăng ký vì có phát sinh chứng từ liên quan");
                        return;
                    }
                    _IRegistrationGroupService.BeginTran();
                    //Kiểm tra xem đối tượng có con không nếu có con thì không được phép xóa
                    //   List<RegistrationGroup> lstChild = _IRegistrationGroupService.Query.Where(p => p.ParentID == temp.ID).ToList();
                    List<RegistrationGroup> lstChild = _IRegistrationGroupService.GetParentId(temp.ID);
                    if (lstChild.Count > 0)
                    {
                        MSG.Warning(string.Format(resSystem.MSG_Catalog_Tree, "Nhóm đăng ký"));
                        return;
                    }
                    //Check xem cha còn có con khác không nếu không thì chuyển trạng thái IsParentNode = false
                    if (temp.ParentID != null)
                    {
                        RegistrationGroup parent = _IRegistrationGroupService.Getbykey((Guid)temp.ParentID);
                        //int checkChildOldParent = _IRegistrationGroupService.Query.Count(p => p.ParentID == temp.ParentID);
                        int checkChildOldParent = _IRegistrationGroupService.GetCountByParenId(temp.ID);
                        if (checkChildOldParent <= 1)
                        {
                            parent.IsParentNode = false;
                        }
                        _IRegistrationGroupService.Update(parent);
                        _IRegistrationGroupService.Delete(temp);
                    }
                    if (temp.ParentID == null)
                    {
                        _IRegistrationGroupService.Delete(temp);
                    }
                    _IRegistrationGroupService.CommitTran();
                    Utils.ClearCacheByType<RegistrationGroup>();
                    LoadDuLieu();
                }
            }
            else
                MSG.Error(string.Format(resSystem.MSG_Catalog2,"một Nhóm đăng ký"));
            
        }
        #endregion

        #region Event
        private void uTree_MouseDown(object sender, MouseEventArgs e)
        {//khi click chuột phải hiện content menu
            Utils.uGrid_MouseDown(sender, e, cms4Grid);
        }

        private void uTree_DoubleClick(object sender, EventArgs e)
        {//click đúp chuột để sửa
            EditFunction();
        }

        private void uTree_ColumnSetGenerated(object sender, ColumnSetGeneratedEventArgs e)
        {
            Utils.uTree_ColumnSetGenerated(sender, e, ConstDatabase.RegistrationGroup_TableName);
        }
        #endregion

        #region Utils
        void ConfigTree(Infragistics.Win.UltraWinTree.UltraTree ultraTree)
        {//hàm chung
            Utils.ConfigTree(ultraTree, TextMessage.ConstDatabase.RegistrationGroup_TableName);
        }

        #endregion

        private void FRegistrationGroup_FormClosing(object sender, FormClosingEventArgs e)
        {
            uTree = null;
        }

        private void FRegistrationGroup_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
