﻿namespace Accounting
{
    partial class FRegistrationGroupDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            this.chkIsActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtRegistrationGroupName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblRegistrationGroupName = new Infragistics.Win.Misc.UltraLabel();
            this.cbbParentID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.lblParentID = new Infragistics.Win.Misc.UltraLabel();
            this.txtRegistrationGroupCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblRegistrationGroupCode = new Infragistics.Win.Misc.UltraLabel();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegistrationGroupName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbParentID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegistrationGroupCode)).BeginInit();
            this.SuspendLayout();
            // 
            // chkIsActive
            // 
            appearance1.TextVAlignAsString = "Middle";
            this.chkIsActive.Appearance = appearance1;
            this.chkIsActive.BackColor = System.Drawing.Color.Transparent;
            this.chkIsActive.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsActive.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkIsActive.Checked = true;
            this.chkIsActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIsActive.Location = new System.Drawing.Point(9, 128);
            this.chkIsActive.Name = "chkIsActive";
            this.chkIsActive.Size = new System.Drawing.Size(103, 22);
            this.chkIsActive.TabIndex = 13;
            this.chkIsActive.Text = "Hoạt động";
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox1.Controls.Add(this.txtRegistrationGroupName);
            this.ultraGroupBox1.Controls.Add(this.lblRegistrationGroupName);
            this.ultraGroupBox1.Controls.Add(this.cbbParentID);
            this.ultraGroupBox1.Controls.Add(this.lblParentID);
            this.ultraGroupBox1.Controls.Add(this.txtRegistrationGroupCode);
            this.ultraGroupBox1.Controls.Add(this.lblRegistrationGroupCode);
            appearance5.FontData.BoldAsString = "True";
            this.ultraGroupBox1.HeaderAppearance = appearance5;
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(3, 6);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(377, 112);
            this.ultraGroupBox1.TabIndex = 26;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtRegistrationGroupName
            // 
            this.txtRegistrationGroupName.AutoSize = false;
            this.txtRegistrationGroupName.Location = new System.Drawing.Point(134, 54);
            this.txtRegistrationGroupName.Name = "txtRegistrationGroupName";
            this.txtRegistrationGroupName.Size = new System.Drawing.Size(237, 22);
            this.txtRegistrationGroupName.TabIndex = 23;
            // 
            // lblRegistrationGroupName
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.TextVAlignAsString = "Middle";
            this.lblRegistrationGroupName.Appearance = appearance2;
            this.lblRegistrationGroupName.Location = new System.Drawing.Point(9, 54);
            this.lblRegistrationGroupName.Name = "lblRegistrationGroupName";
            this.lblRegistrationGroupName.Size = new System.Drawing.Size(126, 22);
            this.lblRegistrationGroupName.TabIndex = 22;
            this.lblRegistrationGroupName.Text = "Tên nhóm đăng ký (*) :";
            // 
            // cbbParentID
            // 
            this.cbbParentID.AllowNull = Infragistics.Win.DefaultableBoolean.True;
            this.cbbParentID.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.SuggestAppend;
            this.cbbParentID.AutoSize = false;
            this.cbbParentID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbParentID.LimitToList = true;
            this.cbbParentID.Location = new System.Drawing.Point(134, 81);
            this.cbbParentID.Name = "cbbParentID";
            this.cbbParentID.Size = new System.Drawing.Size(237, 22);
            this.cbbParentID.TabIndex = 21;
            this.cbbParentID.ItemNotInList += new Infragistics.Win.UltraWinGrid.ItemNotInListEventHandler(this.cbbParentID_ItemNotInList);
            // 
            // lblParentID
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextVAlignAsString = "Middle";
            this.lblParentID.Appearance = appearance3;
            this.lblParentID.Location = new System.Drawing.Point(9, 81);
            this.lblParentID.Name = "lblParentID";
            this.lblParentID.Size = new System.Drawing.Size(92, 22);
            this.lblParentID.TabIndex = 16;
            this.lblParentID.Text = "Thuộc nhóm :";
            // 
            // txtRegistrationGroupCode
            // 
            this.txtRegistrationGroupCode.AutoSize = false;
            this.txtRegistrationGroupCode.Location = new System.Drawing.Point(134, 27);
            this.txtRegistrationGroupCode.Name = "txtRegistrationGroupCode";
            this.txtRegistrationGroupCode.Size = new System.Drawing.Size(237, 22);
            this.txtRegistrationGroupCode.TabIndex = 5;
            // 
            // lblRegistrationGroupCode
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.lblRegistrationGroupCode.Appearance = appearance4;
            this.lblRegistrationGroupCode.Location = new System.Drawing.Point(9, 27);
            this.lblRegistrationGroupCode.Name = "lblRegistrationGroupCode";
            this.lblRegistrationGroupCode.Size = new System.Drawing.Size(126, 22);
            this.lblRegistrationGroupCode.TabIndex = 0;
            this.lblRegistrationGroupCode.Text = "Mã nhóm đăng ký (*) :";
            // 
            // btnSave
            // 
            appearance6.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance6;
            this.btnSave.Location = new System.Drawing.Point(212, 123);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 27;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click_1);
            // 
            // btnClose
            // 
            appearance7.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance7;
            this.btnClose.Location = new System.Drawing.Point(296, 123);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 28;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click_1);
            // 
            // FRegistrationGroupDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(383, 156);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.ultraGroupBox1);
            this.Controls.Add(this.chkIsActive);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FRegistrationGroupDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FRegistrationGroupDetail_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtRegistrationGroupName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbParentID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegistrationGroupCode)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsActive;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraLabel lblParentID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtRegistrationGroupCode;
        private Infragistics.Win.Misc.UltraLabel lblRegistrationGroupCode;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbParentID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtRegistrationGroupName;
        private Infragistics.Win.Misc.UltraLabel lblRegistrationGroupName;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraButton btnClose;
    }
}
