﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;

namespace Accounting
{
    public partial class FRegistrationGroupDetail : DialogForm //UserControl
    {
        #region khai báo
        private readonly IRegistrationGroupService _IRegistrationGroupService;

        RegistrationGroup _Select = new RegistrationGroup();

        bool IsAdd = true;
        public static bool isClose = true;
        #endregion

        #region khởi tạo
        public FRegistrationGroupDetail()
        {//Thêm
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            this.Text = "Thêm mới Nhóm đăng ký";
            #endregion

            #region Thiết lập ban đầu cho Form
            //Khai báo các webservices
            _IRegistrationGroupService = IoC.Resolve<IRegistrationGroupService>();

            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion
            this.chkIsActive.Visible = false;
        }

        public FRegistrationGroupDetail(RegistrationGroup temp)
        {//Sửa
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _Select = temp;
            IsAdd = false;
            txtRegistrationGroupCode.Enabled = false;

            //Khai báo các webservices
            _IRegistrationGroupService = IoC.Resolve<IRegistrationGroupService>();

            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            this.Text = "Sửa Nhóm đăng ký";
            #endregion

            #region Fill dữ liệu Obj vào control
            ObjandGUI(temp, false);
            #endregion
        }

        private void InitializeGUI()
        {
            //khởi tạo cbb
            cbbParentID.DataSource = _IRegistrationGroupService.GetAll();
            cbbParentID.DisplayMember = "RegistrationGroupName";
            Utils.ConfigGrid(cbbParentID, ConstDatabase.RegistrationGroup_TableName);
        }
        #endregion

        #region Button Event
        /// <summary>
        /// Sự kiện nút Lưu lại
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click_1(object sender, EventArgs e)
        {
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion

            //Gui to object
            #region Fill dữ liệu control vào obj

            // thuc hien trong tung thao tac (them moi/ cap nhat)
            #endregion

            #region Thao tác CSDL
            try
            {
                _IRegistrationGroupService.BeginTran();
                if (IsAdd) // them moi
                {
                    RegistrationGroup temp = IsAdd ? new RegistrationGroup() : _IRegistrationGroupService.Getbykey(_Select.ID);
                    ////////////
                    temp = ObjandGUI(temp, true);

                    // set orderfixcode
                    RegistrationGroup temp0 = (RegistrationGroup)Utils.getSelectCbbItem(cbbParentID);
                    // List<string> lstOrderFixCodeChild = _IRegistrationGroupService.Query.Where(a => a.ParentID == temp.ParentID) .Select(a => a.OrderFixCode).ToList();
                    List<string> lstOrderFixCodeChild = _IRegistrationGroupService.GetParenIdOrderFixbyParenID(temp.ParentID);
                    string orderFixCodeParent = temp0 == null ? "" : temp0.OrderFixCode;
                    temp.OrderFixCode = Utils.GetOrderFixCode(lstOrderFixCodeChild, orderFixCodeParent);
                    temp.IsParentNode = false; // mac dinh cho insert
                    temp.Grade = temp0 == null ? 1 : temp0.Grade + 1; // quy tac lay gia tri cho grade
                    //////////////

                    //========> input.OrderFixCode chưa xét

                    _IRegistrationGroupService.CreateNew(temp);
                    // thuc hien cap nhat lai isParentNode cho cha neu co
                    if (temp.ParentID != null)
                    {
                        RegistrationGroup parent = _IRegistrationGroupService.Getbykey((Guid)temp.ParentID);
                        parent.IsParentNode = true;
                        _IRegistrationGroupService.Update(parent);
                    }

                }
                else // cap nhat
                {
                    //Lấy về đối tượng cần sửa
                    RegistrationGroup temp = _IRegistrationGroupService.Getbykey(_Select.ID);
                    //Lưu đối tượng trước khi sửa
                    RegistrationGroup tempOriginal = (RegistrationGroup)Utils.CloneObject(temp);
                    //Lấy về đối tượng chọn trong combobox
                    RegistrationGroup objParentCombox = (RegistrationGroup)Utils.getSelectCbbItem(cbbParentID);
                    //Không thay đổi đối tượng cha trong combobox
                    if (((temp.ParentID == null) && (objParentCombox == null)) || ((temp.ParentID != null) && (objParentCombox != null) && (temp.ParentID == objParentCombox.ID)))
                    {
                        temp = ObjandGUI(temp, true);
                        _IRegistrationGroupService.Update(temp);
                    }
                    //Thay đổi đối tượng trong combobox
                    else
                    {
                        //Không cho phép chuyển ngược
                        if (objParentCombox != null)
                        {
                            //List<RegistrationGroup> lstChildCheck = _IRegistrationGroupService.Query.Where(p => p.OrderFixCode.StartsWith(tempOriginal.OrderFixCode)).ToList();
                            List<RegistrationGroup> lstChildCheck = _IRegistrationGroupService.GetOrderChild(tempOriginal.OrderFixCode);
                            foreach (var item in lstChildCheck)
                            {
                                if (objParentCombox.ID == item.ID)
                                {
                                    MSG.Warning(string.Format(resSystem.MSG_Catalog6, temp.RegistrationGroupCode));
                                    return;
                                }
                            }
                        }
                        //Trường hợp có cha cũ
                        //Kiểm tra cha cũ có còn con ngoài con chuyển đi hay không
                        //Chuyển trạng thái IsParentNode Cha cũ nếu hết con
                        if (temp.ParentID != null)
                        {
                            RegistrationGroup oldParent = _IRegistrationGroupService.Getbykey((Guid)temp.ParentID);
                            // int checkChildOldParent = _IRegistrationGroupService.Query.Count(p => p.ParentID == temp.ParentID);
                            int checkChildOldParent = _IRegistrationGroupService.GetCountByParenId(temp.ParentID);
                            if (checkChildOldParent <= 1)
                            {
                                oldParent.IsParentNode = false;
                            }
                            _IRegistrationGroupService.Update(oldParent);
                        }
                        //Trường hợp có cha mới
                        //Chuyển trạng IsParentNode cha mới
                        if (objParentCombox != null)
                        {
                            RegistrationGroup newParent = _IRegistrationGroupService.Getbykey(objParentCombox.ID);
                            newParent.IsParentNode = true;
                            _IRegistrationGroupService.Update(newParent);

                            // List<RegistrationGroup> listChildNewParent = _IRegistrationGroupService.Query.Where(a => a.ParentID == objParentCombox.ID).ToList();
                            List<RegistrationGroup> listChildNewParent = _IRegistrationGroupService.GetNewParent(objParentCombox.ID);
                            //Tính lại OrderFixCode cho đối tượng chính
                            List<string> lstOfcChildNewParent = new List<string>();
                            foreach (var item in listChildNewParent)
                            {
                                lstOfcChildNewParent.Add(item.OrderFixCode);
                            }
                            string orderFixCodeParent = newParent.OrderFixCode;
                            temp.OrderFixCode = Utils.GetOrderFixCode(lstOfcChildNewParent, orderFixCodeParent);
                            //Tính lại bậc cho đối tượng chính
                            temp.Grade = newParent.Grade + 1;
                        }
                        //Trường hợp không có cha mới tức là chuyển đối tượng lên bậc 1
                        if (objParentCombox == null)
                        {
                            //Lấy về tất cả đối tượng bậc 1
                            int intGrade = 1;
                            List<int> lstOfcChildGradeNo1 = new List<int>();
                            //List<RegistrationGroup> listChildGradeNo1 = _IRegistrationGroupService.Query.Where (a => a.Grade == 1).ToList();
                            List<RegistrationGroup> listChildGradeNo1 = _IRegistrationGroupService.GetGarde(intGrade);
                            foreach (var item in listChildGradeNo1)
                            {
                                lstOfcChildGradeNo1.Add(Convert.ToInt32(item.OrderFixCode));
                            }
                            //Tính lại OrderFixCode cho đối tượng chính
                            temp.OrderFixCode = (lstOfcChildGradeNo1.Max() + 1).ToString(CultureInfo.InvariantCulture);
                            //Tính lại bậc cho đối tượng chính
                            temp.Grade = 1;
                            temp = ObjandGUI(temp, true);
                            _IRegistrationGroupService.Update(temp);
                        }
                        //Xử lý các con của nó 
                        //Lấy về các con
                        //List<RegistrationGroup> lstChild =_IRegistrationGroupService.Query.Where(p => p.OrderFixCode.StartsWith(tempOriginal.OrderFixCode)).ToList();
                        List<RegistrationGroup> lstChild = _IRegistrationGroupService.GetOrderChild(tempOriginal.OrderFixCode);
                        foreach (var item in lstChild)
                        {
                            string tempOldFixCode = tempOriginal.OrderFixCode;
                            item.OrderFixCode = temp.OrderFixCode + item.OrderFixCode.Substring(tempOldFixCode.Length);
                            item.Grade = temp.Grade - tempOriginal.Grade + item.Grade;
                            _IRegistrationGroupService.Update(item);
                        }
                        temp = ObjandGUI(temp, true);
                        _IRegistrationGroupService.Update(temp);
                    }
                }

                _IRegistrationGroupService.CommitTran();
            }
            catch (Exception ex)
            {
                _IRegistrationGroupService.RolbackTran();
            }
            #endregion

            #region xử lý form, kết thúc form
            this.Close();
            isClose = false;
            #endregion
        }

        /// <summary>
        /// Sự kiện nút đóng
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click_1(object sender, EventArgs e)
        {
            isClose = true;
            this.Close();
            
        }
        #endregion

        #region Utils
        /// <summary>
        /// get hoặc set một đối tượng vào giao diện xử lý
        /// </summary>
        /// <param name="input">đối tượng</param>
        /// <param name="isGet">True: get giá trị từ control gán vào đối tượng| False: set giá trị đối tượng vào control</param>
        /// <returns></returns>
        /// 

        RegistrationGroup ObjandGUI(RegistrationGroup input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();  //Thêm mới

                input.RegistrationGroupCode = txtRegistrationGroupCode.Text;
                input.RegistrationGroupName = txtRegistrationGroupName.Text;

                RegistrationGroup temp0 = (RegistrationGroup)Utils.getSelectCbbItem(cbbParentID);
                //Utils.CloneObject(temp0);
                if (temp0 == null) input.ParentID = null;
                else input.ParentID = temp0.ID;
                //Utils.CloneObject(temp0);

                input.IsActive = chkIsActive.Checked;
            }
            else
            {
                txtRegistrationGroupCode.Text = input.RegistrationGroupCode;
                txtRegistrationGroupName.Text = input.RegistrationGroupName;
                foreach (var item in cbbParentID.Rows)
                {
                    if ((item.ListObject as RegistrationGroup).ID == input.ParentID) cbbParentID.SelectedRow = item;
                }
                //chkIsActive.CheckState = input.IsActive ? CheckState.Unchecked : CheckState.Checked;
                chkIsActive.Checked = input.IsActive;
            }
            return input;
        }

        bool CheckError()
        {
            bool kq = true;
            //Check Error Chung

            //Check Error Riêng từng Form
            if (string.IsNullOrEmpty(txtRegistrationGroupCode.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                txtRegistrationGroupCode.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtRegistrationGroupName.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                txtRegistrationGroupName.Focus();
                return false;
            }
            // List<RegistrationGroup> lstRegistrationGroups =_IRegistrationGroupService.Query.Where( c => c.RegistrationGroupCode.Equals(txtRegistrationGroupCode.Text)).ToList();
            List<RegistrationGroup> lstRegistrationGroupsCode = _IRegistrationGroupService.GetByRegistrationGroupsCode(txtRegistrationGroupCode.Text);
            if (IsAdd && lstRegistrationGroupsCode.Count > 0)
            {
                MSG.Warning(string.Format(resSystem.MSG_Catalog_Account6, "Nhóm đăng ký"));
                txtRegistrationGroupCode.Focus();
                return false;
            }
            return kq;
        }
        #endregion

        private void cbbParentID_ItemNotInList(object sender, Infragistics.Win.UltraWinEditors.ValidationErrorEventArgs e)
        {
            RegistrationGroup b = new RegistrationGroup();
            if (cbbParentID.Text != b.RegistrationGroupCode && cbbParentID.Text != "")
            {
                MSG.Warning(string.Format("Nhóm đăng ký không tồn tại.", cbbParentID.Text));
                cbbParentID.Focus();
                return;
            }
        }

        private void FRegistrationGroupDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
