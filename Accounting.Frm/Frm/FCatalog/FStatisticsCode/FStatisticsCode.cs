﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using System.Data;

namespace Accounting
{
    public partial class FStatisticsCode : CatalogBase //UserControl
    {
        #region khai báo
        private readonly IStatisticsCodeService _IStatisticsCodeService;
        List<StatisticsCode> dsStatisticsCode = new List<StatisticsCode>();
        #endregion

        #region khởi tạo
        public FStatisticsCode()
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            ToolTip ToolTip1 = new ToolTip();
            ToolTip1.SetToolTip(btnAdd, "Thêm (Ctrl + N)");
            ToolTip ToolTip2 = new ToolTip();
            ToolTip2.SetToolTip(btnEdit, "Sửa (Ctrl + E)");
            ToolTip ToolTip3 = new ToolTip();
            ToolTip3.SetToolTip(btnDelete, "Xóa (Ctrl + D)");
            #endregion

            #region Thiết lập ban đầu cho Form
            _IStatisticsCodeService = IoC.Resolve<IStatisticsCodeService>();

            this.uTree.InitializeDataNode += new Infragistics.Win.UltraWinTree.InitializeDataNodeEventHandler(Utils.uTree_InitializeDataNode);
            this.uTree.AfterDataNodesCollectionPopulated += new Infragistics.Win.UltraWinTree.AfterDataNodesCollectionPopulatedEventHandler(Utils.uTree_AfterDataNodesCollectionPopulated);
            this.uTree.ColumnSetGenerated += new Infragistics.Win.UltraWinTree.ColumnSetGeneratedEventHandler(this.uTree_ColumnSetGenerated);
            this.uTree.MouseDown += new System.Windows.Forms.MouseEventHandler(this.uTree_MouseDown);
            this.uTree.DoubleClick += new System.EventHandler(this.uTree_DoubleClick);
            #endregion

            #region Tạo dữ liệu ban đầu
            LoadDuLieu();
            #endregion
        }

        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }
        private void LoadDuLieu(bool configTree)
        {
            WaitingFrm.StartWaiting();
            #region Lấy dữ liệu từ CSDL
            dsStatisticsCode = _IStatisticsCodeService.GetAll().OrderByDescending(c => c.StatisticsCode_).Reverse().ToList();
            _IStatisticsCodeService.UnbindSession(dsStatisticsCode);
            dsStatisticsCode = _IStatisticsCodeService.GetAll().OrderByDescending(c => c.StatisticsCode_).Reverse().ToList();
            #endregion

            #region Xử lý dữ liệu
            #endregion

            #region hiển thị và xử lý hiển thị
            DataSet ds = Utils.ToDataSet<StatisticsCode>(dsStatisticsCode, ConstDatabase.StatisticsCode_TableName);
            ds.Tables[0].Columns["Description"].SetOrdinal(3);
            uTree.SetDataBinding(ds, ConstDatabase.StatisticsCode_TableName);
            
            if (configTree) ConfigTree(uTree);
            #endregion
            WaitingFrm.StopWaiting();
        }
        #endregion

        #region Nghiệp vụ
        #region ContentMenu
        private void tsmAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void tsmEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            ResetFunction();
        }
        protected override void ResetFunction()
        {
            LoadDuLieu(false);
        }
        #endregion

        #region Button
        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }
        #endregion

        /// <summary>
        /// Nghiệp vụ Add
        /// </summary>
        protected override void AddFunction()
        {
            if (uTree.SelectedNodes.Count > 0)
            {
                StatisticsCode temp = dsStatisticsCode.Single(k => k.ID == (new Guid(uTree.SelectedNodes[0].Text)));
                new FStatisticsCodeDetail(temp, true).ShowDialog(this);
                if (!FStatisticsCodeDetail.isClose) LoadDuLieu();
            }
            else
            {
                new FStatisticsCodeDetail().ShowDialog(this);
                if (!FStatisticsCodeDetail.isClose) LoadDuLieu();
            }
        }

        /// <summary>
        /// Nghiệp vụ Edit
        /// </summary>
        protected override void EditFunction()
        {
            if (uTree.SelectedNodes.Count == 0 && uTree.ActiveNode != null)
            {
                uTree.ActiveNode.Selected = true;
            }
            if (uTree.SelectedNodes.Count > 0)
            {
                StatisticsCode temp = dsStatisticsCode.Single(k => k.ID == (new Guid(uTree.SelectedNodes[0].Text)));
                new FStatisticsCodeDetail(temp).ShowDialog(this);
                if (!FStatisticsCodeDetail.isClose) LoadDuLieu();
            }
            else
                Accounting.TextMessage.MSG.Error(string.Format(Accounting.TextMessage.resSystem.MSG_Catalog3,"một Mã thống kê"));
        }

        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        protected override void DeleteFunction()
        {
            if (uTree.SelectedNodes.Count == 0 && uTree.ActiveNode != null)
            {
                uTree.ActiveNode.Selected = true;
            }
            if (uTree.SelectedNodes.Count > 0)
            {
                StatisticsCode temp = dsStatisticsCode.Single(k => k.ID == (new Guid(uTree.SelectedNodes[0].Text)));
                if (MSG.Question(string.Format(resSystem.MSG_System_05, "<" + temp.StatisticsCode_ + ">")) == System.Windows.Forms.DialogResult.Yes)
                {
                    _IStatisticsCodeService.BeginTran();
                    //Kiểm tra xem đối tượng có con không nếu có con thì không được phép xóa
                    List<StatisticsCode> lstChild =
                            _IStatisticsCodeService.Query.Where(p => p.ParentID == temp.ID).ToList();
                    if (lstChild.Count > 0)
                    {
                        MSG.Warning(string.Format(resSystem.MSG_Catalog_Tree, "Mã thống kê"));
                        return;
                    }
                    //Check xem cha còn có con khác không nếu không thì chuyển trạng thái IsParentNode = false
                    if (temp.ParentID != null)
                    {
                        StatisticsCode parent = _IStatisticsCodeService.Getbykey((Guid)temp.ParentID);
                        int checkChildOldParent = _IStatisticsCodeService.Query.Count(p => p.ParentID == temp.ParentID);
                        if (checkChildOldParent <= 1)
                        {
                            parent.IsParentNode = false;
                        }
                        _IStatisticsCodeService.Update(parent);
                        _IStatisticsCodeService.Delete(temp);
                    }
                    if (temp.ParentID == null)
                    {
                        _IStatisticsCodeService.Delete(temp);
                    }
                    _IStatisticsCodeService.CommitTran();
                    Utils.ClearCacheByType<StatisticsCode>();
                    LoadDuLieu();
                }
            }
            else
                MSG.Error(string.Format(resSystem.MSG_Catalog2,"một Mã thống kê"));
            
        }

        private void uTree_DoubleClick(object sender, EventArgs e)
        {
            if (uTree.SelectedNodes.Count > 0)
                EditFunction();
        }

        private void uTree_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, cms4Grid);
        }

        private void uTree_ColumnSetGenerated(object sender, Infragistics.Win.UltraWinTree.ColumnSetGeneratedEventArgs e)
        {
            Utils.uTree_ColumnSetGenerated(sender, e, ConstDatabase.StatisticsCode_TableName);
        }
        #endregion

        #region Utils
        void ConfigTree(Infragistics.Win.UltraWinTree.UltraTree ultraTree)
        {//hàm chung
            Utils.ConfigTree(ultraTree, TextMessage.ConstDatabase.StatisticsCode_TableName);
        }

        #endregion

        private void FStatisticsCode_FormClosing(object sender, FormClosingEventArgs e)
        {
            uTree = null;
        }

        private void FStatisticsCode_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
