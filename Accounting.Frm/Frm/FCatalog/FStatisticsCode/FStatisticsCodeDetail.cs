﻿using System;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using System.Collections.Generic;
using System.Globalization;

namespace Accounting
{
    public partial class FStatisticsCodeDetail : DialogForm //UserControl
    {
        #region khai báo
        private IStatisticsCodeService _IStatisticsCodeService;
        bool checkActive = true;
        StatisticsCode _Select = new StatisticsCode();
        private Guid _id;
        public Guid Id
        {
            get { return _id; }
            set { _id = value; }
        }
        bool Them = true;
        public static bool isClose = true;
        #endregion

        #region khởi tạo
        private void Add()
        {
            #region Thiết lập ban đầu cho Form
            //Khai báo các webservices
            _IStatisticsCodeService = IoC.Resolve<IStatisticsCodeService>();
            chkIsActive.Visible = false;
            this.Text = "Thêm mã thống kê";
            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion
        }

        private void Edit(StatisticsCode temp)
        {
            #region Thiết lập ban đầu cho Form
            _Select = temp;
            Them = false;
            txtStatisticsCode.Enabled = false;
            this.Text = "Sửa mã thống kê";
            //Khai báo các webservices
            _IStatisticsCodeService = IoC.Resolve<IStatisticsCodeService>();

            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion

            #region Fill dữ liệu Obj vào control
            ObjandGUI(temp, false);
            #endregion
        }

        public FStatisticsCodeDetail()
        {//Thêm
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            Add();
        }

        public FStatisticsCodeDetail(StatisticsCode temp)
        {//Sửa
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            Edit(temp);
        }

        public FStatisticsCodeDetail(StatisticsCode temp, bool Them)
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            if (Them)
            {
                Add();
                foreach (var item in cbbParentID.Rows)
                {
                    if ((item.ListObject as StatisticsCode).ID == temp.ID) cbbParentID.SelectedRow = item;
                }
            }
            else
            {
                Edit(temp);
            }
        }

        private void InitializeGUI()
        {
            //khởi tạo cbb
            //cbbParentID.DataSource = _IStatisticsCodeService.GetAll().OrderByDescending(c => c.StatisticsCodeName).Reverse().ToList();
            cbbParentID.DataSource = _IStatisticsCodeService.OrderByCode();
            cbbParentID.DisplayMember = "StatisticsCodeName";
            Utils.ConfigGrid(cbbParentID, ConstDatabase.StatisticsCode_TableName);
        }
        #endregion

        #region Button Event
        /// <summary>
        /// Sự kiện nút Lưu lại
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion

            #region Thao tác CSDL
            try
            {
                _IStatisticsCodeService.BeginTran();
                StatisticsCode temp;
                if (Them)//them moi
                {
                    temp = Them ? new StatisticsCode() : _IStatisticsCodeService.Getbykey(_Select.ID);
                    temp = ObjandGUI(temp, true);

                    //set order fixcode
                    StatisticsCode temp0 = (StatisticsCode)Utils.getSelectCbbItem(cbbParentID);
                    //List<string> lstOrderFixCodeChild = _IStatisticsCodeService.Query.Where(a => a.ParentID == temp.ParentID)
                    //    .Select(a => a.OrderFixCode).ToList();
                    List<string> lstOrderFixCodeChild = _IStatisticsCodeService.GListOrderFixCodeByParentID(temp.ParentID);
                    string orderFixCodeParent = temp0 == null ? "" : temp0.OrderFixCode;
                    temp.OrderFixCode = Utils.GetOrderFixCode(lstOrderFixCodeChild, orderFixCodeParent);
                    temp.IsParentNode = false; // mac dinh cho insert
                    temp.Grade = temp0 == null ? 1 : temp0.Grade + 1; // quy tac lay gia tri cho grade

                    //nếu cha isactive =false thì khi thêm mới con cũng là false

                    if (cbbParentID.Text.Equals(""))
                    {
                        if (!CheckCode()) return;
                        temp.IsActive = true;
                        _IStatisticsCodeService.CreateNew(temp);
                    }
                    else
                    {
                        StatisticsCode objParentCombox = (StatisticsCode)Utils.getSelectCbbItem(cbbParentID);
                        //lay cha
                        StatisticsCode Parent = _IStatisticsCodeService.Getbykey(objParentCombox.ID);
                        if (Parent.IsActive == false)
                        {
                            temp.IsActive = false;
                        }
                        else
                            temp.IsActive = true;

                        //check loi
                        if (!CheckCode()) return;
                        //temp.IsActive = true;
                        _IStatisticsCodeService.CreateNew(temp);
                    }

                    //update lai isparentnode neu co
                    if (temp.ParentID != null)
                    {
                        StatisticsCode parent = _IStatisticsCodeService.Getbykey((Guid)temp.ParentID);
                        parent.IsParentNode = true;
                        _IStatisticsCodeService.Update(parent);
                    }
                }
                else //cap nhat
                {
                    temp = _IStatisticsCodeService.Getbykey(_Select.ID);
                    //Lưu đối tượng trước khi sửa
                    StatisticsCode tempOriginal = (StatisticsCode)Utils.CloneObject(temp);
                    //Lấy về đối tượng chọn trong combobox
                    StatisticsCode objParentCombox = (StatisticsCode)Utils.getSelectCbbItem(cbbParentID);
                    //Không thay đổi đối tượng cha trong combobox
                    if (((temp.ParentID == null) && (objParentCombox == null)) || ((temp.ParentID != null) && (objParentCombox != null) && (temp.ParentID == objParentCombox.ID)))
                    {
                        temp = ObjandGUI(temp, true);
                        _IStatisticsCodeService.Update(temp);
                    }
                    else
                    {
                        //không cho phép chuyển ngược
                        if (objParentCombox != null)
                        {
                            //List<StatisticsCode> lstChildCheck =
                            //_IStatisticsCodeService.Query.Where(p => p.OrderFixCode.StartsWith(tempOriginal.OrderFixCode)).ToList();
                            List<StatisticsCode> lstChildCheck =
                                _IStatisticsCodeService.GListByOrderFixCode(tempOriginal.OrderFixCode);
                            foreach (var item in lstChildCheck)
                            {
                                if (objParentCombox.ID == item.ID)
                                {
                                    MSG.Warning(string.Format(resSystem.MSG_Catalog6, temp.StatisticsCode_));
                                    return;
                                }
                            }
                        }
                        //Trường hợp có cha cũ
                        //Kiểm tra cha cũ có còn con ngoài con chuyển đi hay không
                        //Chuyển trạng thái IsParentNode Cha cũ nếu hết con
                        if (temp.ParentID != null)
                        {
                            StatisticsCode oldParent = _IStatisticsCodeService.Getbykey((Guid)temp.ParentID);
                            //int checkChildOldParent = _IStatisticsCodeService.Query.Count(p => p.ParentID == temp.ParentID);
                            int checkChildOldParent = temp.ParentID.HasValue
                                                          ? _IStatisticsCodeService.GListByParrentID(temp.ParentID.Value).Count
                                                          : 0;
                            if (checkChildOldParent <= 1)
                            {
                                oldParent.IsParentNode = false;
                            }
                            _IStatisticsCodeService.Update(oldParent);
                        }
                        //Trường hợp có cha mới
                        //Chuyển trạng IsParentNode cha mới
                        if (objParentCombox != null)
                        {
                            StatisticsCode newParent = _IStatisticsCodeService.Getbykey(objParentCombox.ID);
                            newParent.IsParentNode = true;
                            _IStatisticsCodeService.Update(newParent);

                            //List<StatisticsCode> listChildNewParent = _IStatisticsCodeService.Query.Where(
                            //    a => a.ParentID == objParentCombox.ID).ToList();
                            List<StatisticsCode> listChildNewParent =
                                _IStatisticsCodeService.GListByParrentID(objParentCombox.ID);
                            //Tính lại OrderFixCode cho đối tượng chính
                            List<string> lstOfcChildNewParent = new List<string>();
                            foreach (var item in listChildNewParent)
                            {
                                lstOfcChildNewParent.Add(item.OrderFixCode);
                            }
                            string orderFixCodeParent = newParent.OrderFixCode;
                            temp.OrderFixCode = Utils.GetOrderFixCode(lstOfcChildNewParent, orderFixCodeParent);
                            //Tính lại bậc cho đối tượng chính
                            temp.Grade = newParent.Grade + 1;
                        }
                        //Trường hợp không có cha mới tức là chuyển đối tượng lên bậc 1
                        if (objParentCombox == null)
                        {
                            //Lấy về tất cả đối tượng bậc 1
                            List<int> lstOfcChildGradeNo1 = new List<int>();
                            //List<StatisticsCode> listChildGradeNo1 = _IStatisticsCodeService.Query.Where(
                            //    a => a.Grade == 1).ToList();
                            List<StatisticsCode> listChildGradeNo1 = _IStatisticsCodeService.GListByGrade(1);
                            foreach (var item in listChildGradeNo1)
                            {
                                lstOfcChildGradeNo1.Add(Convert.ToInt32(item.OrderFixCode));
                            }
                            //Tính lại OrderFixCode cho đối tượng chính
                            temp.OrderFixCode = (lstOfcChildGradeNo1.Max() + 1).ToString(CultureInfo.InvariantCulture);
                            //Tính lại bậc cho đối tượng chính
                            temp.Grade = 1;
                            temp = ObjandGUI(temp, true);
                            _IStatisticsCodeService.Update(temp);
                        }
                        //Xử lý các con của nó 
                        //Lấy về các con
                        //List<StatisticsCode> lstChild =
                        //    _IStatisticsCodeService.Query.Where(p => p.OrderFixCode.StartsWith(tempOriginal.OrderFixCode)).ToList();
                        List<StatisticsCode> lstChild =
                            _IStatisticsCodeService.GListByOrderFixCode(tempOriginal.OrderFixCode);
                        foreach (var item in lstChild)
                        {
                            string tempOldFixCode = tempOriginal.OrderFixCode;
                            item.OrderFixCode = temp.OrderFixCode + item.OrderFixCode.Substring(tempOldFixCode.Length);
                            item.Grade = temp.Grade - tempOriginal.Grade + item.Grade;
                            _IStatisticsCodeService.Update(item);
                        }
                        temp = ObjandGUI(temp, true);
                    }
                    //nếu cha ngừng theo dõi thì con cũng ngừng theo dõi
                    //Lấy về danh sách các con
                    //List<StatisticsCode> lstChild2 =
                    //        _IStatisticsCodeService.Query.Where(p => p.OrderFixCode.StartsWith(tempOriginal.OrderFixCode) && p.ID != tempOriginal.ID).ToList();
                    if (!checkActive)
                    {
                        List<StatisticsCode> lstChild2 =
                        _IStatisticsCodeService.GListChilds(tempOriginal.OrderFixCode, tempOriginal.ID);
                        //Chuyen True -> False
                        if (chkIsActive.CheckState == CheckState.Unchecked && CheckError())
                        {
                            temp.IsActive = false;
                            //neu la cha
                            if (lstChild2.Count > 0)
                            {
                                foreach (var itemChild in lstChild2)
                                {
                                    itemChild.IsActive = false;
                                    _IStatisticsCodeService.Update(itemChild);
                                }
                            }
                            //neu ko phai la cha
                            _IStatisticsCodeService.Update(temp);
                        }

                        //Chuyen False -> True
                        else if (chkIsActive.CheckState == CheckState.Checked && CheckError())
                        {
                            //Co Cha
                            if (temp.ParentID != null)
                            {
                                StatisticsCode Parent = _IStatisticsCodeService.Getbykey(objParentCombox.ID);
                                //Cha la false khong cho chuyen
                                if (Parent.IsActive == false)
                                {
                                    MSG.Warning(string.Format(resSystem.MSG_Catalog_Tree2, "Mã thống kê"));
                                    return;
                                }
                                //Cha la True duoc phep chuyen
                                temp.IsActive = true;
                                if (lstChild2.Count > 0)
                                {

                                    if (MSG.Question(resSystem.MSG_Catalog8) == System.Windows.Forms.DialogResult.Yes)
                                    {
                                        foreach (var item2 in lstChild2)
                                        {
                                            item2.IsActive = true;
                                            _IStatisticsCodeService.Update(item2);
                                        }
                                    }

                                }
                                _IStatisticsCodeService.Update(temp);
                            }
                            //Khong Co cha
                            else
                            {
                                temp.IsActive = true;
                                if (lstChild2.Count > 0)
                                {

                                    if (MSG.Question(resSystem.MSG_Catalog8) == System.Windows.Forms.DialogResult.Yes)
                                    {
                                        foreach (var item2 in lstChild2)
                                        {
                                            item2.IsActive = true;
                                            _IStatisticsCodeService.Update(item2);
                                        }
                                    }

                                }

                                _IStatisticsCodeService.Update(temp);
                            }
                        }
                    }
                    
                }
                _IStatisticsCodeService.CommitTran();
                _id = temp.ID;
            }
            catch
            {
                _IStatisticsCodeService.RolbackTran();
            }

            #endregion

            #region xử lý form, kết thúc form
            Utils.ListStatisticsCode.Clear();
            //Utils.AddToBindingList(Utils.ListStatisticsCode, _IStatisticsCodeService.GetByActive_OrderByTreeIsParentNode(true));
            this.Close();
            isClose = false;
            #endregion
        }

        /// <summary>
        /// Sự kiện nút đóng
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            isClose = true;
            DialogResult = DialogResult.Cancel;
            this.Close();
            
        }
        #endregion

        #region Utils
        /// <summary>
        /// get hoặc set một đối tượng vào giao diện xử lý
        /// </summary>
        /// <param name="input">đối tượng</param>
        /// <param name="isGet">True: get giá trị từ control gán vào đối tượng| False: set giá trị đối tượng vào control</param>
        /// <returns></returns>
        StatisticsCode ObjandGUI(StatisticsCode input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();  //Thêm mới

                input.StatisticsCode_ = txtStatisticsCode.Text;
                input.StatisticsCodeName = txtStatisticsCodeName.Text;
                input.Description = txtDescription.Text;
                StatisticsCode temp0 = (StatisticsCode)Utils.getSelectCbbItem(cbbParentID);
                if (temp0 == null) input.ParentID = null;
                else input.ParentID = temp0.ID;
                //========> input.OrderFixCode chưa xét
                input.IsActive = chkIsActive.Checked;
            }
            else
            {
                txtStatisticsCode.Text = input.StatisticsCode_;
                txtStatisticsCodeName.Text = input.StatisticsCodeName;
                txtDescription.Text = input.Description;
                foreach (var item in cbbParentID.Rows)
                {
                    if ((item.ListObject as StatisticsCode).ID == input.ParentID) cbbParentID.SelectedRow = item;
                }
                chkIsActive.Checked = input.IsActive;
            }
            return input;
        }

        bool CheckError()
        {
            bool kq = true;
            //Check Error Chung

            //Check Error Riêng từng Form
            if (string.IsNullOrEmpty(txtStatisticsCode.Text) || string.IsNullOrEmpty(txtStatisticsCodeName.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }
            return kq;
        }

        bool CheckCode()
        {
            bool kq = true;
            if (string.IsNullOrEmpty(txtStatisticsCode.Text) || string.IsNullOrEmpty(txtStatisticsCodeName.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }

            //check trùng code
            //List<string> lst = _IStatisticsCodeService.Query.Select(c => c.StatisticsCode_).ToList();
            List<string> lst = _IStatisticsCodeService.GListStatisticsCode_();
            foreach (var x in lst)
            {
                if (txtStatisticsCode.Text.Equals(x))
                {
                    MSG.Warning(string.Format(resSystem.MSG_Catalog, "Mã", "thống kê"));
                    return false;
                }
            }
            return kq;
        }
        #endregion

        private void chkIsActive_ValidateCheckState(object sender, Infragistics.Win.ValidateCheckStateEventArgs e)
        {
            checkActive = false;
        }

        private void FStatisticsCodeDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
