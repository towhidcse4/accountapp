﻿namespace Accounting
{
    partial class FStatisticsCodeDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.chkIsActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbParentID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtDescription = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblDescription = new Infragistics.Win.Misc.UltraLabel();
            this.txtStatisticsCodeName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblStatisticsCodeName = new Infragistics.Win.Misc.UltraLabel();
            this.lblParentID = new Infragistics.Win.Misc.UltraLabel();
            this.txtStatisticsCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblStatisticsCode = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbParentID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatisticsCodeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatisticsCode)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            appearance1.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance1;
            this.btnSave.Location = new System.Drawing.Point(216, 200);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            appearance2.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance2;
            this.btnClose.Location = new System.Drawing.Point(297, 200);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 7;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // chkIsActive
            // 
            appearance3.TextVAlignAsString = "Middle";
            this.chkIsActive.Appearance = appearance3;
            this.chkIsActive.BackColor = System.Drawing.Color.Transparent;
            this.chkIsActive.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsActive.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkIsActive.Checked = true;
            this.chkIsActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIsActive.Location = new System.Drawing.Point(9, 204);
            this.chkIsActive.Name = "chkIsActive";
            this.chkIsActive.Size = new System.Drawing.Size(116, 22);
            this.chkIsActive.TabIndex = 5;
            this.chkIsActive.Text = "Hoạt động";
            this.chkIsActive.ValidateCheckState += new Infragistics.Win.CheckEditor.ValidateCheckStateHandler(this.chkIsActive_ValidateCheckState);
            // 
            // ultraGroupBox1
            // 
            appearance4.FontData.BoldAsString = "True";
            this.ultraGroupBox1.Appearance = appearance4;
            this.ultraGroupBox1.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox1.Controls.Add(this.cbbParentID);
            this.ultraGroupBox1.Controls.Add(this.txtDescription);
            this.ultraGroupBox1.Controls.Add(this.lblDescription);
            this.ultraGroupBox1.Controls.Add(this.txtStatisticsCodeName);
            this.ultraGroupBox1.Controls.Add(this.lblStatisticsCodeName);
            this.ultraGroupBox1.Controls.Add(this.lblParentID);
            this.ultraGroupBox1.Controls.Add(this.txtStatisticsCode);
            this.ultraGroupBox1.Controls.Add(this.lblStatisticsCode);
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(3, 6);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(377, 189);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbParentID
            // 
            this.cbbParentID.AllowNull = Infragistics.Win.DefaultableBoolean.True;
            this.cbbParentID.AutoSize = false;
            this.cbbParentID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbParentID.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbParentID.LimitToList = true;
            this.cbbParentID.Location = new System.Drawing.Point(130, 77);
            this.cbbParentID.MaxLength = 25;
            this.cbbParentID.Name = "cbbParentID";
            this.cbbParentID.NullText = "<Chọn dữ liệu>";
            this.cbbParentID.Size = new System.Drawing.Size(239, 22);
            this.cbbParentID.TabIndex = 3;
            // 
            // txtDescription
            // 
            this.txtDescription.AutoSize = false;
            this.txtDescription.Location = new System.Drawing.Point(130, 103);
            this.txtDescription.MaxLength = 512;
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(239, 75);
            this.txtDescription.TabIndex = 4;
            // 
            // lblDescription
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.TextVAlignAsString = "Middle";
            this.lblDescription.Appearance = appearance5;
            this.lblDescription.Location = new System.Drawing.Point(9, 103);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(112, 22);
            this.lblDescription.TabIndex = 25;
            this.lblDescription.Text = "Mô tả chi tiết";
            // 
            // txtStatisticsCodeName
            // 
            this.txtStatisticsCodeName.AutoSize = false;
            this.txtStatisticsCodeName.Location = new System.Drawing.Point(130, 51);
            this.txtStatisticsCodeName.MaxLength = 512;
            this.txtStatisticsCodeName.Name = "txtStatisticsCodeName";
            this.txtStatisticsCodeName.Size = new System.Drawing.Size(239, 22);
            this.txtStatisticsCodeName.TabIndex = 2;
            // 
            // lblStatisticsCodeName
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextVAlignAsString = "Middle";
            this.lblStatisticsCodeName.Appearance = appearance6;
            this.lblStatisticsCodeName.Location = new System.Drawing.Point(9, 51);
            this.lblStatisticsCodeName.Name = "lblStatisticsCodeName";
            this.lblStatisticsCodeName.Size = new System.Drawing.Size(113, 22);
            this.lblStatisticsCodeName.TabIndex = 22;
            this.lblStatisticsCodeName.Text = "Tên thống kê (*)";
            // 
            // lblParentID
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.TextVAlignAsString = "Middle";
            this.lblParentID.Appearance = appearance7;
            this.lblParentID.Location = new System.Drawing.Point(9, 77);
            this.lblParentID.Name = "lblParentID";
            this.lblParentID.Size = new System.Drawing.Size(112, 22);
            this.lblParentID.TabIndex = 16;
            this.lblParentID.Text = "Thuộc nhóm";
            // 
            // txtStatisticsCode
            // 
            this.txtStatisticsCode.AutoSize = false;
            this.txtStatisticsCode.Location = new System.Drawing.Point(130, 25);
            this.txtStatisticsCode.MaxLength = 25;
            this.txtStatisticsCode.Name = "txtStatisticsCode";
            this.txtStatisticsCode.Size = new System.Drawing.Size(239, 22);
            this.txtStatisticsCode.TabIndex = 1;
            // 
            // lblStatisticsCode
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.TextVAlignAsString = "Middle";
            this.lblStatisticsCode.Appearance = appearance8;
            this.lblStatisticsCode.Location = new System.Drawing.Point(9, 25);
            this.lblStatisticsCode.Name = "lblStatisticsCode";
            this.lblStatisticsCode.Size = new System.Drawing.Size(113, 22);
            this.lblStatisticsCode.TabIndex = 0;
            this.lblStatisticsCode.Text = "Mã thống kê (*)";
            // 
            // FStatisticsCodeDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(383, 236);
            this.Controls.Add(this.ultraGroupBox1);
            this.Controls.Add(this.chkIsActive);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FStatisticsCodeDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FStatisticsCodeDetail_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbParentID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatisticsCodeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatisticsCode)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsActive;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraLabel lblParentID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtStatisticsCode;
        private Infragistics.Win.Misc.UltraLabel lblStatisticsCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtStatisticsCodeName;
        private Infragistics.Win.Misc.UltraLabel lblStatisticsCodeName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDescription;
        private Infragistics.Win.Misc.UltraLabel lblDescription;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbParentID;
    }
}
