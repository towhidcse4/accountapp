﻿namespace Accounting
{
    partial class FCreditCardDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            this.chkIsActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbExFromMonth = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.cbbExToMonth = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.cbbBankNameIDIssueCard = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtCreditCardNumber = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.txtExToYear = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.lblTime = new Infragistics.Win.Misc.UltraLabel();
            this.txtExFromYear = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.cbbCreditCardType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.txtDescription = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblDescription = new Infragistics.Win.Misc.UltraLabel();
            this.lblBankNameIssueCard = new Infragistics.Win.Misc.UltraLabel();
            this.txtOwnerCard = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblOwnerCard = new Infragistics.Win.Misc.UltraLabel();
            this.lblCreditCardType = new Infragistics.Win.Misc.UltraLabel();
            this.lblCreditCardNumber = new Infragistics.Win.Misc.UltraLabel();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbExFromMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbExToMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankNameIDIssueCard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExToYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExFromYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCreditCardType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOwnerCard)).BeginInit();
            this.SuspendLayout();
            // 
            // chkIsActive
            // 
            appearance1.TextVAlignAsString = "Middle";
            this.chkIsActive.Appearance = appearance1;
            this.chkIsActive.BackColor = System.Drawing.Color.Transparent;
            this.chkIsActive.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsActive.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkIsActive.Checked = true;
            this.chkIsActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIsActive.Location = new System.Drawing.Point(5, 298);
            this.chkIsActive.Name = "chkIsActive";
            this.chkIsActive.Size = new System.Drawing.Size(121, 22);
            this.chkIsActive.TabIndex = 10;
            this.chkIsActive.Text = "Hoạt động";
            // 
            // ultraGroupBox1
            // 
            appearance2.FontData.BoldAsString = "True";
            this.ultraGroupBox1.Appearance = appearance2;
            this.ultraGroupBox1.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox1.Controls.Add(this.cbbExFromMonth);
            this.ultraGroupBox1.Controls.Add(this.cbbExToMonth);
            this.ultraGroupBox1.Controls.Add(this.cbbBankNameIDIssueCard);
            this.ultraGroupBox1.Controls.Add(this.txtCreditCardNumber);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel5);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel4);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox1.Controls.Add(this.txtExToYear);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox1.Controls.Add(this.lblTime);
            this.ultraGroupBox1.Controls.Add(this.txtExFromYear);
            this.ultraGroupBox1.Controls.Add(this.cbbCreditCardType);
            this.ultraGroupBox1.Controls.Add(this.txtDescription);
            this.ultraGroupBox1.Controls.Add(this.lblDescription);
            this.ultraGroupBox1.Controls.Add(this.lblBankNameIssueCard);
            this.ultraGroupBox1.Controls.Add(this.txtOwnerCard);
            this.ultraGroupBox1.Controls.Add(this.lblOwnerCard);
            this.ultraGroupBox1.Controls.Add(this.lblCreditCardType);
            this.ultraGroupBox1.Controls.Add(this.lblCreditCardNumber);
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(5, 6);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(494, 282);
            this.ultraGroupBox1.TabIndex = 26;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbExFromMonth
            // 
            this.cbbExFromMonth.AutoSize = false;
            this.cbbExFromMonth.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbExFromMonth.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbbExFromMonth.Location = new System.Drawing.Point(196, 137);
            this.cbbExFromMonth.MaxLength = 512;
            this.cbbExFromMonth.Name = "cbbExFromMonth";
            this.cbbExFromMonth.Size = new System.Drawing.Size(84, 22);
            this.cbbExFromMonth.TabIndex = 49;
            // 
            // cbbExToMonth
            // 
            this.cbbExToMonth.AutoSize = false;
            this.cbbExToMonth.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbExToMonth.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbbExToMonth.Location = new System.Drawing.Point(196, 165);
            this.cbbExToMonth.MaxLength = 512;
            this.cbbExToMonth.Name = "cbbExToMonth";
            this.cbbExToMonth.Size = new System.Drawing.Size(84, 22);
            this.cbbExToMonth.TabIndex = 48;
            // 
            // cbbBankNameIDIssueCard
            // 
            this.cbbBankNameIDIssueCard.AllowNull = Infragistics.Win.DefaultableBoolean.True;
            this.cbbBankNameIDIssueCard.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.SuggestAppend;
            this.cbbBankNameIDIssueCard.AutoSize = false;
            appearance3.Image = global::Accounting.Properties.Resources.plus;
            editorButton1.Appearance = appearance3;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            appearance4.Image = global::Accounting.Properties.Resources.plus_press;
            editorButton1.PressedAppearance = appearance4;
            this.cbbBankNameIDIssueCard.ButtonsRight.Add(editorButton1);
            this.cbbBankNameIDIssueCard.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbBankNameIDIssueCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbBankNameIDIssueCard.Location = new System.Drawing.Point(136, 110);
            this.cbbBankNameIDIssueCard.Name = "cbbBankNameIDIssueCard";
            this.cbbBankNameIDIssueCard.Size = new System.Drawing.Size(348, 22);
            this.cbbBankNameIDIssueCard.TabIndex = 45;
            // 
            // txtCreditCardNumber
            // 
            appearance5.ImageHAlign = Infragistics.Win.HAlign.Left;
            appearance5.TextHAlignAsString = "Left";
            appearance5.TextVAlignAsString = "Middle";
            this.txtCreditCardNumber.Appearance = appearance5;
            this.txtCreditCardNumber.AutoSize = false;
            this.txtCreditCardNumber.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.txtCreditCardNumber.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.String;
            this.txtCreditCardNumber.InputMask = "nnnn nnnn nnnn nnnn";
            this.txtCreditCardNumber.Location = new System.Drawing.Point(136, 29);
            this.txtCreditCardNumber.Name = "txtCreditCardNumber";
            this.txtCreditCardNumber.NullText = "";
            this.txtCreditCardNumber.PromptChar = ' ';
            this.txtCreditCardNumber.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtCreditCardNumber.SelectAllBehavior = Infragistics.Win.UltraWinMaskedEdit.MaskSelectAllBehavior.SelectEnteredCharacters;
            this.txtCreditCardNumber.Size = new System.Drawing.Size(348, 22);
            this.txtCreditCardNumber.TabIndex = 44;
            this.txtCreditCardNumber.Text = "   ";
            // 
            // ultraLabel5
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance6;
            this.ultraLabel5.AutoSize = true;
            this.ultraLabel5.Location = new System.Drawing.Point(136, 168);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(56, 14);
            this.ultraLabel5.TabIndex = 43;
            this.ultraLabel5.Text = "Đến tháng";
            // 
            // ultraLabel4
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance7;
            this.ultraLabel4.AutoSize = true;
            this.ultraLabel4.Location = new System.Drawing.Point(360, 168);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(28, 14);
            this.ultraLabel4.TabIndex = 42;
            this.ultraLabel4.Text = "Năm";
            // 
            // ultraLabel1
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance8;
            this.ultraLabel1.AutoSize = true;
            this.ultraLabel1.Location = new System.Drawing.Point(360, 141);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(28, 14);
            this.ultraLabel1.TabIndex = 41;
            this.ultraLabel1.Text = "Năm";
            // 
            // txtExToYear
            // 
            this.txtExToYear.AutoSize = false;
            this.txtExToYear.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.txtExToYear.Location = new System.Drawing.Point(400, 164);
            this.txtExToYear.MaskInput = "nnnn";
            this.txtExToYear.MaxValue = 9999;
            this.txtExToYear.MinValue = 0;
            this.txtExToYear.Name = "txtExToYear";
            this.txtExToYear.Nullable = true;
            this.txtExToYear.Size = new System.Drawing.Size(84, 22);
            this.txtExToYear.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.OnMouseEnter;
            this.txtExToYear.TabIndex = 8;
            this.txtExToYear.Value = null;
            // 
            // ultraLabel2
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance9;
            this.ultraLabel2.AutoSize = true;
            this.ultraLabel2.Location = new System.Drawing.Point(135, 141);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(49, 14);
            this.ultraLabel2.TabIndex = 36;
            this.ultraLabel2.Text = "Từ tháng";
            // 
            // lblTime
            // 
            appearance10.BackColor = System.Drawing.Color.Transparent;
            appearance10.TextVAlignAsString = "Middle";
            this.lblTime.Appearance = appearance10;
            this.lblTime.Location = new System.Drawing.Point(9, 137);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(121, 22);
            this.lblTime.TabIndex = 35;
            this.lblTime.Text = "Thời gian hiệu lực";
            // 
            // txtExFromYear
            // 
            this.txtExFromYear.AutoSize = false;
            this.txtExFromYear.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.txtExFromYear.Location = new System.Drawing.Point(400, 137);
            this.txtExFromYear.MaskInput = "nnnn";
            this.txtExFromYear.MaxValue = 9999;
            this.txtExFromYear.MinValue = 0;
            this.txtExFromYear.Name = "txtExFromYear";
            this.txtExFromYear.Nullable = true;
            this.txtExFromYear.Size = new System.Drawing.Size(84, 22);
            this.txtExFromYear.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.OnMouseEnter;
            this.txtExFromYear.TabIndex = 6;
            this.txtExFromYear.Value = null;
            // 
            // cbbCreditCardType
            // 
            this.cbbCreditCardType.AutoSize = false;
            this.cbbCreditCardType.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbCreditCardType.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbbCreditCardType.Location = new System.Drawing.Point(136, 56);
            this.cbbCreditCardType.MaxLength = 512;
            this.cbbCreditCardType.Name = "cbbCreditCardType";
            this.cbbCreditCardType.Size = new System.Drawing.Size(348, 22);
            this.cbbCreditCardType.TabIndex = 2;
            // 
            // txtDescription
            // 
            this.txtDescription.AutoSize = false;
            this.txtDescription.Location = new System.Drawing.Point(136, 196);
            this.txtDescription.MaxLength = 512;
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(348, 71);
            this.txtDescription.TabIndex = 9;
            // 
            // lblDescription
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            appearance11.TextVAlignAsString = "Middle";
            this.lblDescription.Appearance = appearance11;
            this.lblDescription.Location = new System.Drawing.Point(9, 196);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(121, 22);
            this.lblDescription.TabIndex = 28;
            this.lblDescription.Text = "Diễn giải";
            // 
            // lblBankNameIssueCard
            // 
            appearance12.BackColor = System.Drawing.Color.Transparent;
            appearance12.TextVAlignAsString = "Middle";
            this.lblBankNameIssueCard.Appearance = appearance12;
            this.lblBankNameIssueCard.Location = new System.Drawing.Point(9, 110);
            this.lblBankNameIssueCard.Name = "lblBankNameIssueCard";
            this.lblBankNameIssueCard.Size = new System.Drawing.Size(121, 22);
            this.lblBankNameIssueCard.TabIndex = 26;
            this.lblBankNameIssueCard.Text = "Ngân hàng phát hành";
            // 
            // txtOwnerCard
            // 
            this.txtOwnerCard.AutoSize = false;
            this.txtOwnerCard.Location = new System.Drawing.Point(136, 83);
            this.txtOwnerCard.Name = "txtOwnerCard";
            this.txtOwnerCard.Size = new System.Drawing.Size(348, 22);
            this.txtOwnerCard.TabIndex = 3;
            // 
            // lblOwnerCard
            // 
            appearance13.BackColor = System.Drawing.Color.Transparent;
            appearance13.TextVAlignAsString = "Middle";
            this.lblOwnerCard.Appearance = appearance13;
            this.lblOwnerCard.Location = new System.Drawing.Point(9, 83);
            this.lblOwnerCard.Name = "lblOwnerCard";
            this.lblOwnerCard.Size = new System.Drawing.Size(121, 22);
            this.lblOwnerCard.TabIndex = 24;
            this.lblOwnerCard.Text = "Chủ thẻ (*)";
            // 
            // lblCreditCardType
            // 
            appearance14.BackColor = System.Drawing.Color.Transparent;
            appearance14.TextVAlignAsString = "Middle";
            this.lblCreditCardType.Appearance = appearance14;
            this.lblCreditCardType.Location = new System.Drawing.Point(9, 56);
            this.lblCreditCardType.Name = "lblCreditCardType";
            this.lblCreditCardType.Size = new System.Drawing.Size(121, 22);
            this.lblCreditCardType.TabIndex = 22;
            this.lblCreditCardType.Text = "Loại thẻ (*)";
            // 
            // lblCreditCardNumber
            // 
            appearance15.BackColor = System.Drawing.Color.Transparent;
            appearance15.TextVAlignAsString = "Middle";
            this.lblCreditCardNumber.Appearance = appearance15;
            this.lblCreditCardNumber.Location = new System.Drawing.Point(9, 29);
            this.lblCreditCardNumber.Name = "lblCreditCardNumber";
            this.lblCreditCardNumber.Size = new System.Drawing.Size(121, 22);
            this.lblCreditCardNumber.TabIndex = 0;
            this.lblCreditCardNumber.Text = "Số thẻ (*)";
            // 
            // btnSave
            // 
            appearance16.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance16;
            this.btnSave.Location = new System.Drawing.Point(334, 294);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 11;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click_1);
            // 
            // btnClose
            // 
            appearance17.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance17;
            this.btnClose.Location = new System.Drawing.Point(415, 294);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 12;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click_1);
            // 
            // FCreditCardDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(502, 327);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.ultraGroupBox1);
            this.Controls.Add(this.chkIsActive);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FCreditCardDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thẻ tín dụng";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FCreditCardDetail_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbExFromMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbExToMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankNameIDIssueCard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExToYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExFromYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCreditCardType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOwnerCard)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsActive;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraLabel lblCreditCardNumber;
        private Infragistics.Win.Misc.UltraLabel lblCreditCardType;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDescription;
        private Infragistics.Win.Misc.UltraLabel lblDescription;
        private Infragistics.Win.Misc.UltraLabel lblBankNameIssueCard;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtOwnerCard;
        private Infragistics.Win.Misc.UltraLabel lblOwnerCard;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbCreditCardType;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor txtExToYear;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel lblTime;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor txtExFromYear;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtCreditCardNumber;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbBankNameIDIssueCard;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbExFromMonth;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbExToMonth;
    }
}
