﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FCreditCard : CatalogBase //UserControl
    {
        #region khai báo
        private readonly ICreditCardService _ICreditCardService;
        private readonly IBankService _IBankService;
        #endregion

        #region khởi tạo
        public FCreditCard()
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _ICreditCardService = IoC.Resolve<ICreditCardService>();
            _IBankService = IoC.Resolve<IBankService>();
            this.uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            #endregion

            #region Tạo dữ liệu ban đầu
            LoadDuLieu();
            if (uGrid.Rows.Count > 0)
            {
                uGrid.Rows[0].Selected = true;
            }
            #endregion
            ToolTip ToolTip1 = new ToolTip();
            ToolTip1.SetToolTip(btnAdd, "Thêm (Ctrl + N)");
            ToolTip ToolTip2 = new ToolTip();
            ToolTip2.SetToolTip(btnEdit, "Xem (Ctrl + E)");
            ToolTip ToolTip3 = new ToolTip();
            ToolTip3.SetToolTip(btnDelete, "Xóa (Ctrl + D)");
            if (uGrid.Rows.Count > 0) uGrid.Rows[0].Selected = true;
        }

        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }
        private void LoadDuLieu(bool configGrid)
        {
            WaitingFrm.StartWaiting();
            #region Lấy dữ liệu từ CSDL
            List<CreditCard> list = _ICreditCardService.GetAll_OrderBy();
            _ICreditCardService.UnbindSession(list);
            list = _ICreditCardService.GetAll_OrderBy();
            List<Bank> ls2 = _IBankService.GetAll();
            foreach (var item in list)
            {
                foreach (var it2 in ls2)
                {
                    if (item.BankIDIssueCard == it2.ID)
                    {
                        item.BankName = it2.BankName;
                        break;
                    }
                }
            }
            #endregion

            #region Xử lý dữ liệu
            #endregion

            #region hiển thị và xử lý hiển thị
            uGrid.DataSource = list.ToArray();

            if (configGrid) ConfigGrid(uGrid);
            uGrid.DisplayLayout.Bands[0].Columns["CreditCardNumber"].Width = 125;
            uGrid.DisplayLayout.Bands[0].Columns["OwnerCard"].Width = 125;
            uGrid.DisplayLayout.Bands[0].Columns["BankName"].Width = 125;
            uGrid.DisplayLayout.Bands[0].Columns["Description"].Width = 175;
            uGrid.DisplayLayout.Bands[0].Columns["IsActive"].Width = 120;
            #endregion
            WaitingFrm.StopWaiting();
        }
        #endregion

        #region Nghiệp vụ
        #region ContentMenu
        private void tsmAdd_Click_1(object sender, EventArgs e)
        {
            AddFunction();
        }


        private void tsmEdit_Click_1(object sender, EventArgs e)
        {
            EditFunction();
        }
        private void tsmDelete_Click_1(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void tmsReLoad_Click_1(object sender, EventArgs e)
        {
            ResetFunction();
        }
        protected override void ResetFunction()
        {
            LoadDuLieu(false);
        }

        #endregion

        #region Button
        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void btnDelete_Click_1(object sender, EventArgs e)
        {
            DeleteFunction();
        }
        #endregion

        private void uGrid_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            EditFunction();
        }

        /// <summary>
        /// Nghiệp vụ Add
        /// </summary>
        protected override void AddFunction()
        {
            new FCreditCardDetail().ShowDialog(this);
            if (!FCreditCardDetail.IsClose)
            {
                LoadDuLieu();
                foreach (UltraGridRow row in uGrid.Rows)
                {
                    var creditCard = row.ListObject as CreditCard;
                    if (creditCard != null && creditCard.CreditCardNumber == FCreditCardDetail.CreditCardNumber)
                    {
                        uGrid.Rows[row.Index].Selected = true;
                        uGrid.ActiveRow = row;
                    }
                }
            }
        }

        /// <summary>
        /// Nghiệp vụ Edit
        /// </summary>
        protected override void EditFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                CreditCard temp = uGrid.Selected.Rows[0].ListObject as CreditCard;
                new FCreditCardDetail(temp).ShowDialog(this);
                if (!FCreditCardDetail.IsClose)
                {
                    LoadDuLieu();
                    foreach (UltraGridRow row in uGrid.Rows)
                    {
                        var creditCard = row.ListObject as CreditCard;
                        if (creditCard != null && creditCard.CreditCardNumber == FCreditCardDetail.CreditCardNumber)
                        {
                            uGrid.Rows[row.Index].Selected = true;
                            uGrid.ActiveRow = row;
                        }
                    }
                }
            }
            else
                Accounting.TextMessage.MSG.Error(string.Format(Accounting.TextMessage.resSystem.MSG_Catalog3,"một Thẻ tín dụng"));
        }

        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        protected override void DeleteFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                CreditCard temp = uGrid.Selected.Rows[0].ListObject as CreditCard;
                if (MSG.Question(string.Format(resSystem.MSG_System_05, temp.CreditCardNumber)) == System.Windows.Forms.DialogResult.Yes)
                {
                    _ICreditCardService.BeginTran();
                    _ICreditCardService.Delete(temp);
                    _ICreditCardService.CommitTran();
                    Utils.ClearCacheByType<CreditCard>();
                    LoadDuLieu();
                }
            }
            else
                MSG.Error(string.Format(resSystem.MSG_Catalog2,"một Thẻ tín dụng"));
        }
        #endregion

        #region Utils
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, TextMessage.ConstDatabase.CreditCard_TableName);
        }
        #endregion

        private void FCreditCard_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGrid.ResetText();
            uGrid.ResetUpdateMode();
            uGrid.ResetExitEditModeOnLeave();
            uGrid.ResetRowUpdateCancelAction();
            uGrid.DataSource = null;
            uGrid.Layouts.Clear();
            uGrid.ResetLayouts();
            uGrid.ResetDisplayLayout();
            uGrid.Refresh();
            uGrid.ClearUndoHistory();
            uGrid.ClearXsdConstraints();
        }

        private void FCreditCard_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
