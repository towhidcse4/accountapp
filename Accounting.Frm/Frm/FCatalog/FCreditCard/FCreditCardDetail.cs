﻿#region

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win.UltraWinEditors;

#endregion

namespace Accounting
{
    public partial class FCreditCardDetail : DialogForm //UserControl
    {
        #region khai báo

        public static bool IsClose = true;
        private readonly bool _them = true;
        private readonly IBankService _iBankService;
        private readonly ICreditCardService _iCreditCardService;

        private readonly CreditCard _select = new CreditCard();
        private static string _creditCardNumber;

        public static string CreditCardNumber
        {
            get { return _creditCardNumber; }
            set { _creditCardNumber = value; }
        }

        #endregion

        #region khởi tạo

        public FCreditCardDetail()
        {
            //Thêm
            #region Khởi tạo giá trị mặc định của Form

            InitializeComponent();
            Text = "Thêm mới Thẻ tín dụng";
            Utils.SortFormControls(this);

            #endregion

            #region Thiết lập ban đầu cho Form

            //Khai báo các webservices
            _iCreditCardService = IoC.Resolve<ICreditCardService>();
            _iBankService = IoC.Resolve<IBankService>();

            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database

            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)

            //

            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            DateTime dt = Utils.StringToDateTime(ConstFrm.DbStartDate) ?? DateTime.Now;                       
            #endregion

            chkIsActive.Visible = false;
        }

        public FCreditCardDetail(CreditCard temp)
        {
            //Sửa

            #region Khởi tạo giá trị mặc định của Form

            InitializeComponent();
            Utils.SortFormControls(this);

            #endregion

            #region Thiết lập ban đầu cho Form

            _select = temp;
            _them = false;
            txtCreditCardNumber.Enabled = false;

            //Khai báo các webservices
            _iCreditCardService = IoC.Resolve<ICreditCardService>();
            _iBankService = IoC.Resolve<IBankService>();


            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database

            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)

            //

            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            Text = "Sửa Thẻ tín dụng";

            #endregion

            #region Fill dữ liệu Obj vào control

            ObjandGUI(temp, false);

            #endregion
        }


        private void InitializeGUI()
        {

            Loadcbb();
            ////khởi tạo cbb
            //List<string> lItem = new List<string>() { "VisaCard", "MasterCard", "Discover", "Diners Club", "American Express" };
            //cbbCreditCardType.DataSource = lItem;
            //cbbCreditCardType.SelectedIndex = 0;
            ////khởi tạo cbb
            //cbbBankNameIDIssueCard.DataSource = _IBankService.Query.Where(c => c.IsActive).ToList();
            //cbbBankNameIDIssueCard.DisplayMember = "BankCode";
            //Utils.ConfigGrid(cbbBankNameIDIssueCard, ConstDatabase.Bank_TableName);
            cbbBankNameIDIssueCard.EditorButtonClick += cbbBankNameIDIssueCard_EdittorButtonClick;
        }

        private void Loadcbb()
        {
            List<int> lstMonth = new List<int>();
            lstMonth.Clear();
            for (int i = 1; i < 13; i++)
            {
                lstMonth.Add(i);
            }
            cbbExFromMonth.DataSource = lstMonth;
            cbbExFromMonth.Value = "Value";
            cbbExToMonth.DataSource = lstMonth;
            cbbExToMonth.Value = "Value";
            List<string> lItem = new List<string>
            {
                "VisaCard",
                "MasterCard",
                "Discover",
                "Diners Club",
                "American Express"
            };
            cbbCreditCardType.DataSource = lItem;
            cbbCreditCardType.SelectedIndex = 0;
            //khởi tạo cbb
            //cbbBankNameIDIssueCard.DataSource = _IBankService.Query.Where(c => c.IsActive).ToList();
            //cbbBankNameIDIssueCard.DataSource = _iBankService.GetListBankIsActive(true);
            //cbbBankNameIDIssueCard.DisplayMember = "BankCode";
            //Utils.ConfigGrid(cbbBankNameIDIssueCard, ConstDatabase.Bank_TableName);
            this.ConfigCombo(Utils.ListBank, cbbBankNameIDIssueCard, "BankCode", "ID");
        }
        #endregion

        #region Event

        /// <summary>
        ///     Sự kiện nút Lưu lại
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click_1(object sender, EventArgs e)
        {
            #region Check Lỗi

            if (!CheckError()) return;

            #endregion

            //Gui to object

            #region Fill dữ liệu control vào obj

            CreditCard temp = _them ? new CreditCard() : _iCreditCardService.Getbykey(_select.ID);
            temp = ObjandGUI(temp, true);

            #endregion

            #region Thao tác CSDL

            _iCreditCardService.BeginTran();
            if (_them) _iCreditCardService.CreateNew(temp);
            else _iCreditCardService.Update(temp);
            _iCreditCardService.CommitTran();
            _creditCardNumber = temp.CreditCardNumber;

            #endregion

            #region xử lý form, kết thúc form

            Utils.ListCreditCard.Clear();
            Close();
            IsClose = false;

            #endregion
        }


        /// <summary>
        ///     Sự kiện nút đóng
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click_1(object sender, EventArgs e)
        {
            IsClose = true;
            DialogResult = DialogResult.Cancel;
            Dispose(true);
            Close();
        }

        private void cbbBankNameIDIssueCard_EdittorButtonClick(object sender, EditorButtonEventArgs e)
        {
            new FBankDetail().ShowDialog(this);
            loadBank();
            Utils.ClearCacheByType<Bank>();
            cbbBankNameIDIssueCard.SelectedText = FBankDetail.bankcode;
        }
        void loadBank()
        {
            this.ConfigCombo(Utils.ListBank, cbbBankNameIDIssueCard, "BankCode", "ID");
        }

        #endregion

        #region Utils

        /// <summary>
        ///     get hoặc set một đối tượng vào giao diện xử lý
        /// </summary>
        /// <param name="input">đối tượng</param>
        /// <param name="isGet">True: get giá trị từ control gán vào đối tượng| False: set giá trị đối tượng vào control</param>
        /// <returns></returns>
        private CreditCard ObjandGUI(CreditCard input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid(); //Thêm mới

                input.CreditCardNumber = txtCreditCardNumber.Text;
                input.CreditCardType = cbbCreditCardType.Value.ToString();
                input.OwnerCard = txtOwnerCard.Text;
                //Lấy cbb ID ngân hàng
                Bank temp0 = (Bank)Utils.getSelectCbbItem(cbbBankNameIDIssueCard);
                input.BankIDIssueCard = (temp0 != null) ? temp0.ID : (Guid?)null;
                input.ExFromMonth = (int?)(cbbExFromMonth.Value);
                input.ExFromYear = (int?)txtExFromYear.Value;
                input.ExToMonth = (int?)cbbExToMonth.Value;
                input.ExToYear = (int?)txtExToYear.Value;
                input.Description = txtDescription.Text;
                input.IsActive = chkIsActive.Checked;
            }
            else
            {
                txtCreditCardNumber.Text = input.CreditCardNumber;
                foreach (var item in cbbCreditCardType.Items)
                {
                    var s = item.ListObject as string;
                    if (s != null && s.Equals(input.CreditCardType)) cbbCreditCardType.SelectedItem = item;
                }
                txtOwnerCard.Text = input.OwnerCard;
                foreach (var item in cbbBankNameIDIssueCard.Rows)
                {
                    var bank = item.ListObject as Bank;
                    if (bank != null && bank.ID == input.BankIDIssueCard)
                        cbbBankNameIDIssueCard.SelectedRow = item;
                }
                if (input.ExFromMonth > 0 && input.ExFromMonth <= 12)
                {
                    cbbExFromMonth.Value = input.ExFromMonth.ToString();
                }
                if (input.ExToMonth > 0 && input.ExToMonth <= 12)
                {
                    cbbExToMonth.Value = input.ExToMonth.ToString();
                }
                if (input.ExFromYear > 0)
                {
                    txtExFromYear.Value = input.ExFromYear.ToString();
                }
                if (input.ExToYear > 0)
                {
                    txtExToYear.Value = input.ExToYear.ToString();
                }
                txtDescription.Text = input.Description;
                chkIsActive.Checked = input.IsActive;
            }
            return input;
        }

        private bool CheckError()
        {
            //Check Error Chung

            //Check Error Riêng từng Form
            const string noPattern = @"^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$";
            System.Text.RegularExpressions.Regex myRegex = new System.Text.RegularExpressions.Regex(noPattern);
            System.Text.RegularExpressions.Match m = myRegex.Match(txtCreditCardNumber.Text.Replace(" ",""));
            if (!m.Success)
            {
                MSG.Warning("Số thẻ không hợp lệ");
                txtCreditCardNumber.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtOwnerCard.Text))
            {
                MSG.Warning(resSystem.MSG_System_03);
                txtOwnerCard.Focus();
                return false;
            }
            int dt = Convert.ToInt32(txtExFromYear.Value);
            if ((dt < 1990)|| (dt > 3000))
            {
                MSG.Warning("Năm nhập vào sai định dạng");
                txtExFromYear.Focus();
                return false;
            }
            int dt2 = Convert.ToInt32(txtExToYear.Value);
            if ((dt < 1990) || (dt > 3000))
            {
                MSG.Warning("Năm nhập vào sai định dạng");
                txtExToYear.Focus();
                return false;
            }
            
            if (string.IsNullOrEmpty(txtCreditCardNumber.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                txtCreditCardNumber.Focus();
                return false;
            }
            //if (Them && _ICreditCardService.Query.Where(c => c.CreditCardNumber.Equals(txtCreditCardNumber.Text)).ToList().Count > 0)
            if (_them && _iCreditCardService.CountListCreditCardCreditCardNumber(txtCreditCardNumber.Text) > 0)
            {
                MSG.Error(string.Format(resSystem.MSG_Catalog_Account6,"thẻ tín dụng"));
                txtCreditCardNumber.Focus();
                return false;
            }
            if (cbbCreditCardType.SelectedIndex < 0)
            {
                MSG.Error(resSystem.MSG_System_03);
                cbbCreditCardType.Focus();
                return false;
            }
            //try
            //{
            //    double.Parse(txtCreditCardNumber.Text.Trim());
            //}
            //catch (Exception)
            //{

            //    MSG.Error("Số thẻ nên để giá trị số!");
            //    txtCreditCardNumber.Focus();
            //    return false;
            //}
            if (!string.IsNullOrEmpty(txtExToYear.Text) &&
                !string.IsNullOrEmpty(txtExFromYear.Text))
            {
                if (int.Parse(txtExToYear.Text) < int.Parse(txtExFromYear.Value.ToString()))
                {
                    MSG.Error(resSystem.MSG_Catalog_FCreditCardDetail);
                    txtExToYear.Focus();
                    return false;
                }
                if (int.Parse(txtExToYear.Text) == int.Parse(txtExFromYear.Value.ToString()))
                {
                    if (cbbExToMonth.Value == null || cbbExFromMonth.Value == null || (int.Parse(cbbExToMonth.Value.ToString()) < int.Parse(cbbExFromMonth.Value.ToString())))
                    {
                        MSG.Error("Thời gian hiệu lực không đúng! Xin bạn nhập lại Thời gian đến > Thời gian bắt đầu");
                        cbbExFromMonth.Focus();
                        return false;
                    }
                }
            }
            return true;
        }


        #endregion

        private void FCreditCardDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}