﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using Castle.Facilities.TypedFactory.Internal;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;
using Infragistics.Win.UltraWinEditors;
namespace Accounting
{
    public partial class FSaleDiscountPolicy : DialogForm
    {
        #region khai báo
        public readonly ISaleDiscountPolicyService _ISaleDiscountPolicyService;
        //public static List<SaleDiscountPolicy> dsSaleDiscount = new List<SaleDiscountPolicy>();
        public BindingList<SaleDiscountPolicy> lstSaleDiscountPolicy = new BindingList<SaleDiscountPolicy>();
        UltraComboEditor cbbDiscountType = new UltraComboEditor();
        MaterialGoods _select = new MaterialGoods();
        int _statusForm;
        bool them;
        #endregion
        public FSaleDiscountPolicy()
        {
            InitializeComponent();
            ConfigGrid(uGridDiscountPolicy);
        }
        public FSaleDiscountPolicy(BindingList<SaleDiscountPolicy> temp)
        {
            InitializeComponent();
            lstSaleDiscountPolicy = temp;
            ConfigGrid(uGridDiscountPolicy);
        }
        #region Utils
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            uGridDiscountPolicy.DataSource = lstSaleDiscountPolicy;
            Utils.ConfigGrid(uGridDiscountPolicy, ConstDatabase.SaleDiscountPolicy_TableName, new List<TemplateColumn>(), isBusiness: true);
            foreach (var column in uGridDiscountPolicy.DisplayLayout.Bands[0].Columns)
            {
                this.ConfigEachColumn4Grid(0, column, uGridDiscountPolicy);
            }
            uGridDiscountPolicy.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            cbbDiscountType.Items.Add(0, "Theo %");
            cbbDiscountType.Items.Add(1, "Theo số tiền");
            cbbDiscountType.Items.Add(2, "Theo đơn giá (số tiền CK/1 đơn vị SL)");
            uGridDiscountPolicy.DisplayLayout.Bands[0].Columns["DiscountType"].EditorComponent = cbbDiscountType;
            uGridDiscountPolicy.DisplayLayout.Bands[0].Columns["DiscountType"].Style = ColumnStyle.DropDownList;
            //if (uGridDiscountPolicy.Selected.Equals("0"))
            //{
            //    uGridDiscountPolicy.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            //    uGridDiscountPolicy.DisplayLayout.Bands[0].Columns["DiscountResult"].FormatNumberic(ConstDatabase.Format_Quantity);
            //}
            //else


            foreach (UltraGridRow row in uGridDiscountPolicy.Rows)
            {
                if (row.Cells["DiscountType"].Value != null)
                    if (int.Parse(row.Cells["DiscountType"].Value.ToString()) == 0)
                    {
                        //uGridDiscountPolicy.DisplayLayout.Bands[0].Columns["DiscountResult"].FormatNumberic(ConstDatabase.Format_Quantity);
                        //row.Cells["DiscountResult"].FormatNumberic(ConstDatabase.Format_Quantity);
                        Utils.FormatNumberic(row.Cells["DiscountResult"], ConstDatabase.Format_Quantity);
                    }
                    else if (int.Parse(row.Cells["DiscountType"].Value.ToString()) == 1)
                    {
                        //uGridDiscountPolicy.DisplayLayout.Bands[0].Columns["DiscountResult"].FormatNumberic(ConstDatabase.Format_TienVND);
                        row.Cells["DiscountResult"].FormatNumberic(ConstDatabase.Format_Quantity);
                    }
                    else if (int.Parse(row.Cells["DiscountType"].Value.ToString()) == 2)
                    {
                        // uGridDiscountPolicy.DisplayLayout.Bands[0].Columns["DiscountResult"].FormatNumberic(ConstDatabase.Format_TienVND);
                        row.Cells["DiscountResult"].FormatNumberic(ConstDatabase.Format_Quantity);
                    }
            }



            uGridDiscountPolicy.DisplayLayout.Bands[0].Columns["QuantityFrom"].FormatNumberic(ConstDatabase.Format_Quantity);
            uGridDiscountPolicy.DisplayLayout.Bands[0].Columns["QuantityTo"].FormatNumberic(ConstDatabase.Format_Quantity);
        }
        #endregion

        //private void CreaterColumsStyle(Infragistics.Win.UltraWinGrid.UltraGrid uGrid)
        //{

        //    #region Grid động
        //    Infragistics.Win.UltraWinGrid.UltraGridBand band = uGrid.DisplayLayout.Bands[0];
        //    //List<string> strType = new List<string> { "Theo %", "Theo số tiền", "Theo đơn giá (số tiền CK/1 đơn vị SL)" };
        //    ////Dictionary<int, string> strType = new Dictionary<int, string>();
        //    //////strType.Add(1, "fdsfds");
        //    ////List<int> strType = new List<int> { 1, 2, 3, 4 };
        //    foreach (var item in band.Columns)
        //    {
        //        if (item.Key.Equals("QuantityFrom"))
        //        {
        //            item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
        //            item.MaskInput = " -nn,nnn,nnn,nnn,nnn.nn";
        //        }
        //        else if (item.Key.Equals("QuantityTo"))
        //        {
        //            item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
        //            item.MaskInput = " -nn,nnn,nnn,nnn,nnn.nn";
        //        }
        //        else if (item.Key.Equals("DiscountType"))
        //        {
        //            DefaultEditorOwnerSettings editorSettings = new DefaultEditorOwnerSettings { DataType = typeof(double) };
        //            ValueList valueList = new ValueList();
        //            Name = "cbbTypeSale";
        //            valueList.ValueListItems.Add(0, "Theo %");
        //            valueList.ValueListItems.Add(1, "Theo số tiền");
        //            valueList.ValueListItems.Add(2, "Theo đơn giá (số tiền CK/1 đơn vị SL)");
        //            editorSettings.ValueList = valueList;
        //            //item.Editor = new EditorWithCombo(new DefaultEditorOwner(editorSettings));



        //            if (valueList.Equals("0") && item.Key.Equals("DiscountResult"))
        //                {

        //                // Utils.FormatNumberic(item.MaskInput, ConstDatabase.Format_Quantity);
        //                item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
        //                item.MaskInput = " -nn,nnn,nnn,nnn,nnn.nn";

        //            }

        //            else if (valueList.Equals("1") && item.Key.Equals("DiscountResult"))
        //            {
        //                //Utils.FormatNumberic(item.MaskInput, ConstDatabase.Format_TienVND);
        //                item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Integer;
        //                item.MaskInput = " -nn,nnn,nnn,nnn,nnn";

        //            }
        //            else if (valueList.Equals("2") && item.Key.Equals("DiscountResult"))
        //            {
        //                //Utils.FormatNumberic(item.MaskInput, ConstDatabase.Format_TienVND);
        //                item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Integer;
        //                item.MaskInput = " -nn,nnn,nnn,nnn,nnn";

        //            }

        //        }
        //        else if (item.Key.Equals("DiscountResult"))
        //        {
        //            if((item.Key.Equals("DiscountType")).Equals("0"))
        //            {
        //                item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
        //                item.MaskInput = " -nn,nnn,nnn,nnn,nnn.nn";
        //            }
        //        }
        //        #endregion
        //    }
        //}

        private void btnSave_Click(object sender, EventArgs e)
        {
            uGridDiscountPolicy.UpdateData();
            DialogResult = DialogResult.OK;
            this.Close();
        }

        //private void ugridposted_aftercellupdate(object sender, celleventargs e)
        //{
        // if (e.Cell.Column.Key.Equals("DiscountType"))
        //            {
        //                Infragistics.Win.UltraWinGrid.UltraCombo test = (Infragistics.Win.UltraWinGrid.UltraCombo)Controls["cbbtypesale"];
        //        SaleDiscountPolicy select = (SaleDiscountPolicy)(test.SelectedRow == null ? test.SelectedRow : test.SelectedRow.ListObject);

        //                if (e.Cell.Row.Cells["unit"].Value == (object) select.DiscountType)
        //        {
        //            e.Cell.Row.Cells["discountresult"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
        //        }
        //                else
        //                {
        //                    e.Cell.Row.Cells["discountresult"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Integer;
        //                }

        //}
        //}

        private void btnClose_Click(object sender, EventArgs e)
        {
            uGridDiscountPolicy.UpdateData();
            lstSaleDiscountPolicy = new BindingList<SaleDiscountPolicy>(_select.SaleDiscountPolicys.CloneObject());
            DialogResult = DialogResult.Cancel;
            this.Close();
            
        }

        private void uGrid_AfterCellUpdate(object sender, CellEventArgs e)
        {
            if (e.Cell.Column.Key.Equals("DiscountType"))
            {
                if (int.Parse(e.Cell.Row.Cells["DiscountType"].Value.ToString()) == 0)
                {
                    //e.Cell.Row.Cells["DiscountResult"].FormatNumberic(ConstDatabase.Format_Quantity);
                    Utils.FormatNumberic(e.Cell.Row.Cells["DiscountResult"], ConstDatabase.Format_Quantity);
                }
                if (int.Parse(e.Cell.Row.Cells["DiscountType"].Value.ToString()) == 1)
                {
                    //e.Cell.Row.Cells["DiscountResult"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Integer;
                    //e.Cell.Row.Cells["DiscountResult"].FormatNumberic(ConstDatabase.Format_TienVND);
                    Utils.FormatNumberic(e.Cell.Row.Cells["DiscountResult"], ConstDatabase.Format_Quantity);
                }
                if (int.Parse(e.Cell.Row.Cells["DiscountType"].Value.ToString()) == 2)
                {
                    //e.Cell.Row.Cells["DiscountResult"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Integer;
                    //e.Cell.Row.Cells["DiscountResult"].FormatNumberic(ConstDatabase.Format_TienVND);
                    Utils.FormatNumberic(e.Cell.Row.Cells["DiscountResult"], ConstDatabase.Format_Quantity);
                }
            }
        }



        private void tsmDelete_Click(object sender, EventArgs e)
        {
            if (uGridDiscountPolicy.ActiveCell != null)
            {
                UltraGridRow row = null;
                if (uGridDiscountPolicy.ActiveCell != null || uGridDiscountPolicy.ActiveRow != null)
                {

                    row = uGridDiscountPolicy.ActiveCell != null ? uGridDiscountPolicy.ActiveCell.Row : uGridDiscountPolicy.ActiveRow;

                }
                else
                {
                    if (uGridDiscountPolicy.Rows.Count > 0) row = uGridDiscountPolicy.Rows[uGridDiscountPolicy.Rows.Count - 1];
                }
                if (row != null) row.Delete(false);

                uGridDiscountPolicy.Update();
            }
        }

        private void tsmAdd_Click(object sender, EventArgs e)
        {
            uGridDiscountPolicy.AddNewRow4Grid();
        }

        private void FSaleDiscountPolicy_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
