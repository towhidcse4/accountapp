﻿using System;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;

namespace Accounting
{
    public partial class FStaffDetail : DialogForm //UserControl
    {
        #region khai báo
        private readonly IAccountGroupService _IAccountGroupService;
        AccountGroup _Select = new AccountGroup();
        bool Them = true;
        public static bool isClose = true;
        #endregion

        #region Khởi tạo
        public FStaffDetail()
        {
            InitializeComponent();
            _IAccountGroupService = IoC.Resolve<IAccountGroupService>();
            InitializeGUI();
        }

        public FStaffDetail(AccountGroup temp)
        {
            InitializeComponent();
            _Select = temp;
            _IAccountGroupService = IoC.Resolve<IAccountGroupService>();
            InitializeGUI();

            ObjandGUI(temp, false);
            Them = false;
            txtDepartmentCode.Enabled = false;
        }

        /// <summary>
        /// Khởi tạo giá trị ban đầu cho control
        /// </summary>
        private void InitializeGUI()
        {
        }
        #endregion

        #region Button Event
        /// <summary>
        /// Sự kiện nút Lưu lại
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            //Gui to object
            AccountGroup temp = Them ? new AccountGroup() : _IAccountGroupService.Getbykey(_Select.ID);
            temp = ObjandGUI(temp, true);
            if (Them) _IAccountGroupService.CreateNew(temp);
            else _IAccountGroupService.Update(temp);
            _IAccountGroupService.CommitChanges();
            this.Close();
            isClose = false;
        }

        /// <summary>
        /// Sự kiện nút đóng
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            isClose = true;
            this.Close();
        }
        #endregion

        #region Utils
        /// <summary>
        /// get hoặc set một đối tượng vào giao diện xử lý
        /// </summary>
        /// <param name="input">đối tượng AccountGroup</param>
        /// <param name="isGet">True: get giá trị từ control gán vào đối tượng| False: set giá trị đối tượng vào control</param>
        /// <returns></returns>
        AccountGroup ObjandGUI(AccountGroup input, bool isGet)
        {
            if (isGet)
            {
                input.ID = txtDepartmentCode.Text;
                input.AccountGroupName = txtDepartmentName.Text;
                input.AccountGroupKind = (byte)cbbParentID.SelectedIndex;
            }
            else
            {
                txtDepartmentCode.Text = input.ID;
                txtDepartmentName.Text = input.AccountGroupName;
                cbbParentID.SelectedIndex = input.AccountGroupKind;
            }
            return input;
        }
        #endregion
    }
}
