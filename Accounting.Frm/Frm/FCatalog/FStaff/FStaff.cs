﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FStaff : CustormForm //UserControl
    {
        #region khai báo
        public static Dictionary<int, string> dicTinhChat = new Dictionary<int, string>();
        public static Dictionary<int, string> dicChiTietTheo = new Dictionary<int, string>();

        private readonly IAccountGroupService _IAccountGroupService;

        #endregion

        #region khởi tạo
        public FStaff()
        {
            InitializeComponent();
            _IAccountGroupService = IoC.Resolve<IAccountGroupService>();

            this.uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
        }

        private void uAccountGroup_Load(object sender, EventArgs e)
        {
            if (dicTinhChat.Count == 0)
            {
                dicTinhChat.Add(0, "Dư nợ");
                dicTinhChat.Add(1, "Dư có");
                dicTinhChat.Add(2, "Lưỡng tính");
                dicTinhChat.Add(3, "Không có số dư");
            }
            if (dicChiTietTheo.Count == 0)
            {
                dicChiTietTheo.Add(1, "chi tiết theo Nhà Cung Cấp");
                dicChiTietTheo.Add(2, "chi tiết theo Khách Hàng");
                dicChiTietTheo.Add(3, "chi tiết theo Nhân Viên");
                dicChiTietTheo.Add(4, "chi tiết theo Đối tượng tập hợp chi phí");
                dicChiTietTheo.Add(5, "chi tiết theo Hợp đồng");
                dicChiTietTheo.Add(6, "chi tiết theo VTHH, CCDC");
                dicChiTietTheo.Add(7, "chi tiết theo TK Ngân hàng");
            }

            LoadDuLieu();
            System.Windows.Forms.ToolTip ToolTip1 = new System.Windows.Forms.ToolTip();
            ToolTip1.SetToolTip(btnAdd, "Thêm (Ctrl + N)");
            System.Windows.Forms.ToolTip ToolTip2 = new System.Windows.Forms.ToolTip();
            ToolTip2.SetToolTip(btnEdit, "Xem (Ctrl + E)");
            System.Windows.Forms.ToolTip ToolTip3 = new System.Windows.Forms.ToolTip();
            ToolTip3.SetToolTip(btnDelete, "Xóa (Ctrl + D)");
            if (uGrid.Rows.Count > 0) uGrid.Rows[0].Selected = true;
        }

        private void LoadDuLieu()
        {
            //Load dữ liệu
            List<AccountGroup> list = _IAccountGroupService.GetAll_Orderby();
            _IAccountGroupService.UnbindSession(list);
            list = _IAccountGroupService.GetAll_Orderby();

            //thiết lập tính chất
            foreach (var item in list) item.AccountGroupKindView = dicTinhChat[item.AccountGroupKind];

            uGrid.DataSource = list.ToArray();
            if (FDepartmentDetail.isClose) ConfigGrid(uGrid);
        }
        #endregion

        #region nghiệp vụ
        #endregion

        #region Utils
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {
            //Tiêu đề
            utralGrid.Text = string.Empty;

            UltraGridBand band = utralGrid.DisplayLayout.Bands[0];
            utralGrid.DisplayLayout.Bands[0].Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            utralGrid.DisplayLayout.Bands[0].Override.SelectedRowAppearance.BackColor = Color.FromArgb(255, 192, 128);

            //Tiêu đề cột
            foreach (var item in band.Columns)
            {
                item.Hidden = true;
                if (item.Header.Caption.Equals("ID"))
                {
                    item.Header.Caption = "Mã nhóm";
                    item.Hidden = false;
                }
                else if (item.Header.Caption.Equals("AccountGroupName"))
                {
                    item.Header.Caption = "Tên nhóm";
                    item.Hidden = false;
                }
                else if (item.Header.Caption.Equals("AccountGroupKindView"))
                {
                    item.Header.Caption = "Tính chất";
                    item.Hidden = false;
                }
            }
        }
        #endregion
    }
}
