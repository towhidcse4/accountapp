﻿namespace Accounting
{
    partial class FStaffDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbCostAccount = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.lblTaiKhoanChiPhiLuong = new Infragistics.Win.Misc.UltraLabel();
            this.txtDescription = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbParentID = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.txtDepartmentName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtDepartmentCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblThuoc = new Infragistics.Win.Misc.UltraLabel();
            this.lblTen = new Infragistics.Win.Misc.UltraLabel();
            this.lblMa = new Infragistics.Win.Misc.UltraLabel();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.chkIsActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCostAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbParentID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDepartmentName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDepartmentCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            appearance1.FontData.BoldAsString = "True";
            this.ultraGroupBox1.Appearance = appearance1;
            this.ultraGroupBox1.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox1.Controls.Add(this.cbbCostAccount);
            this.ultraGroupBox1.Controls.Add(this.lblTaiKhoanChiPhiLuong);
            this.ultraGroupBox1.Controls.Add(this.txtDescription);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox1.Controls.Add(this.cbbParentID);
            this.ultraGroupBox1.Controls.Add(this.txtDepartmentName);
            this.ultraGroupBox1.Controls.Add(this.txtDepartmentCode);
            this.ultraGroupBox1.Controls.Add(this.lblThuoc);
            this.ultraGroupBox1.Controls.Add(this.lblTen);
            this.ultraGroupBox1.Controls.Add(this.lblMa);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(322, 217);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbCostAccount
            // 
            this.cbbCostAccount.AutoSize = false;
            this.cbbCostAccount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbCostAccount.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbbCostAccount.Location = new System.Drawing.Point(119, 171);
            this.cbbCostAccount.Name = "cbbCostAccount";
            this.cbbCostAccount.Size = new System.Drawing.Size(184, 22);
            this.cbbCostAccount.TabIndex = 13;
            // 
            // lblTaiKhoanChiPhiLuong
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.TextVAlignAsString = "Middle";
            this.lblTaiKhoanChiPhiLuong.Appearance = appearance2;
            this.lblTaiKhoanChiPhiLuong.Location = new System.Drawing.Point(8, 171);
            this.lblTaiKhoanChiPhiLuong.Name = "lblTaiKhoanChiPhiLuong";
            this.lblTaiKhoanChiPhiLuong.Size = new System.Drawing.Size(105, 22);
            this.lblTaiKhoanChiPhiLuong.TabIndex = 12;
            this.lblTaiKhoanChiPhiLuong.Text = "Tài khoản chi phí lương";
            // 
            // txtDescription
            // 
            this.txtDescription.AutoSize = false;
            this.txtDescription.Location = new System.Drawing.Point(119, 111);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(184, 54);
            this.txtDescription.TabIndex = 11;
            // 
            // ultraLabel1
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance3;
            this.ultraLabel1.Location = new System.Drawing.Point(8, 111);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(107, 22);
            this.ultraLabel1.TabIndex = 10;
            this.ultraLabel1.Text = "Diễn giải";
            // 
            // cbbParentID
            // 
            this.cbbParentID.AutoSize = false;
            this.cbbParentID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbParentID.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbbParentID.Location = new System.Drawing.Point(119, 83);
            this.cbbParentID.Name = "cbbParentID";
            this.cbbParentID.Size = new System.Drawing.Size(184, 22);
            this.cbbParentID.TabIndex = 9;
            // 
            // txtDepartmentName
            // 
            this.txtDepartmentName.AutoSize = false;
            this.txtDepartmentName.Location = new System.Drawing.Point(119, 55);
            this.txtDepartmentName.Name = "txtDepartmentName";
            this.txtDepartmentName.Size = new System.Drawing.Size(184, 22);
            this.txtDepartmentName.TabIndex = 6;
            // 
            // txtDepartmentCode
            // 
            this.txtDepartmentCode.AutoSize = false;
            this.txtDepartmentCode.Location = new System.Drawing.Point(119, 27);
            this.txtDepartmentCode.Name = "txtDepartmentCode";
            this.txtDepartmentCode.Size = new System.Drawing.Size(184, 22);
            this.txtDepartmentCode.TabIndex = 5;
            // 
            // lblThuoc
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.lblThuoc.Appearance = appearance4;
            this.lblThuoc.Location = new System.Drawing.Point(8, 83);
            this.lblThuoc.Name = "lblThuoc";
            this.lblThuoc.Size = new System.Drawing.Size(105, 22);
            this.lblThuoc.TabIndex = 2;
            this.lblThuoc.Text = "Thuộc";
            // 
            // lblTen
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.TextVAlignAsString = "Middle";
            this.lblTen.Appearance = appearance5;
            this.lblTen.Location = new System.Drawing.Point(8, 55);
            this.lblTen.Name = "lblTen";
            this.lblTen.Size = new System.Drawing.Size(105, 22);
            this.lblTen.TabIndex = 1;
            this.lblTen.Text = "Tên phòng ban (*)";
            // 
            // lblMa
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextVAlignAsString = "Middle";
            this.lblMa.Appearance = appearance6;
            this.lblMa.Location = new System.Drawing.Point(8, 27);
            this.lblMa.Name = "lblMa";
            this.lblMa.Size = new System.Drawing.Size(105, 22);
            this.lblMa.TabIndex = 0;
            this.lblMa.Text = "Mã phòng ban (*)";
            // 
            // btnSave
            // 
            appearance7.Image = global::Accounting.Properties.Resources.btnSave;
            this.btnSave.Appearance = appearance7;
            this.btnSave.Location = new System.Drawing.Point(147, 235);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 11;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            appearance8.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance8;
            this.btnClose.Location = new System.Drawing.Point(228, 235);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 12;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // chkIsActive
            // 
            appearance9.TextVAlignAsString = "Middle";
            this.chkIsActive.Appearance = appearance9;
            this.chkIsActive.BackColor = System.Drawing.Color.Transparent;
            this.chkIsActive.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsActive.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkIsActive.Checked = true;
            this.chkIsActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIsActive.Location = new System.Drawing.Point(8, 239);
            this.chkIsActive.Name = "chkIsActive";
            this.chkIsActive.Size = new System.Drawing.Size(103, 22);
            this.chkIsActive.TabIndex = 14;
            this.chkIsActive.Text = "Hoạt động";
            // 
            // FStaffDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(322, 272);
            this.Controls.Add(this.chkIsActive);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.ultraGroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FStaffDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbCostAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbParentID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDepartmentName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDepartmentCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraLabel lblMa;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDepartmentName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDepartmentCode;
        private Infragistics.Win.Misc.UltraLabel lblTen;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraLabel lblThuoc;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbParentID;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsActive;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDescription;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbCostAccount;
        private Infragistics.Win.Misc.UltraLabel lblTaiKhoanChiPhiLuong;
    }
}
