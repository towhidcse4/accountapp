﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using Accounting.TextMessage;

namespace Accounting
{
    public partial class FAccountTransfer : CatalogBase //UserControl
    {
        #region khai báo
        private readonly IAccountTransferService _IAccountTransferService;

        static Dictionary<int, string> dicAccountTransferType; public static Dictionary<int, string> DicAccountTransferType { get { dicAccountTransferType = dicAccountTransferType ?? (Dictionary<int, string>)BuildConfig(1); return dicAccountTransferType; } }
        static Dictionary<int, string> dicTransferSide; public static Dictionary<int, string> DicTransferSide { get { dicTransferSide = dicTransferSide ?? (Dictionary<int, string>)BuildConfig(2); return dicTransferSide; } }
        #endregion

        #region khởi tạo
        public FAccountTransfer()
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _IAccountTransferService = IoC.Resolve<IAccountTransferService>();
            this.uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            #endregion

            #region Tạo dữ liệu ban đầu
            LoadDuLieu();
            if (uGrid.Rows.Count > 0)
            {
                uGrid.Rows[0].Selected = true;
            }
            #endregion
        }

        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }
        private void LoadDuLieu(bool configGrid)
        {
            WaitingFrm.StartWaiting();
            #region Lấy dữ liệu từ CSDL
            List<AccountTransfer> list = _IAccountTransferService.GetAll_OrderBy();
            _IAccountTransferService.UnbindSession(list);
            list = _IAccountTransferService.GetAll_OrderBy() ;
            #endregion

            #region Xử lý dữ liệu
            //thiết lập tính chất
            foreach (var item in list)
            {
                item.AccountTransferTypeView = DicAccountTransferType[item.AccountTransferType];
                item.TransferSideView = DicTransferSide[item.TransferSide];
            }
            #endregion

            #region hiển thị và xử lý hiển thị
            uGrid.DataSource = list.ToArray();

            if (configGrid) ConfigGrid(uGrid);
            #endregion
            WaitingFrm.StopWaiting();
        }
        #endregion

        #region Nghiệp vụ
        #region ContentMenu
        private void tsmAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void tsmEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            ResetFunction();
        }
        protected override void ResetFunction()
        {
            LoadDuLieu(false);
        }
        #endregion

        #region Button
        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }
        #endregion

        private void uGrid_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (e.Row.Index == -1) return;
            EditFunction();
        }

        /// <summary>
        /// Nghiệp vụ Add
        /// </summary>
        protected override void AddFunction()
        {
            new FAccountTransferDetail().ShowDialog(this);
            if (!FAccountTransferDetail.isClose) LoadDuLieu();
        }

        /// <summary>
        /// Nghiệp vụ Edit
        /// </summary>
        protected override void EditFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                AccountTransfer temp = uGrid.Selected.Rows[0].ListObject as AccountTransfer;
                new FAccountTransferDetail(temp).ShowDialog(this);
                if (!FAccountTransferDetail.isClose) LoadDuLieu();
            }
            else
                MSG.Error(string.Format(resSystem.MSG_Catalog3,"một Tài khoản"));
        }

        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        protected override void DeleteFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                AccountTransfer temp = uGrid.Selected.Rows[0].ListObject as AccountTransfer;
                if (MSG.Question(string.Format(resSystem.MSG_System_05, temp.AccountTransferCode)) == System.Windows.Forms.DialogResult.Yes)
                {
                    _IAccountTransferService.BeginTran();
                    _IAccountTransferService.Delete(temp);
                    _IAccountTransferService.CommitTran();
                    Utils.ClearCacheByType<AccountTransfer>();
                    LoadDuLieu();
                }
            }
            else
                MSG.Error(string.Format(resSystem.MSG_Catalog2,"Tài khoản"));
        }
        #endregion

        #region Utils
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, TextMessage.ConstDatabase.AccountTransfer_TableName);
        }

        //xử lý ConstValue Frm (thường các trường này là XML tuy nhiên do các const này chỉ có ở Frm này nên để luôn ở đây)
        static object BuildConfig(int obj)
        {
            Dictionary<int, string> temp = new Dictionary<int, string>();
            switch (obj)
            {
                case 1:
                    {//AccountTransferType
                        temp.Add(0, "Kết chuyển chi phí sản xuất");
                        temp.Add(1, "Kết chuyển xác định kết quả kinh doanh");
                    }
                    break;
                case 2:
                    {//TransferSide
                        temp.Add(0, "Nợ");
                        temp.Add(1, "Có");
                        temp.Add(2, "Hai bên");
                    }
                    break;
            }
            return temp;
        }
        #endregion

        private void FAccountTransfer_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }

        private void FAccountTransfer_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGrid.ResetText();
            uGrid.ResetUpdateMode();
            uGrid.ResetExitEditModeOnLeave();
            uGrid.ResetRowUpdateCancelAction();
            uGrid.DataSource = null;
            uGrid.Layouts.Clear();
            uGrid.ResetLayouts();
            uGrid.ResetDisplayLayout();
            uGrid.Refresh();
            uGrid.ClearUndoHistory();
            uGrid.ClearXsdConstraints();
        }
    }
}
