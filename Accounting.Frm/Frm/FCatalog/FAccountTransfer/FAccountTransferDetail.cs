﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Accounting.TextMessage;

namespace Accounting
{
    public partial class FAccountTransferDetail : DialogForm //UserControl
    {
        #region khai báo
        private readonly IAccountTransferService _IAccountTransferService;
        private readonly IAccountService _IAccountService;

        AccountTransfer _Select = new AccountTransfer();

        bool Them = true;
        public static bool isClose = true;
        #endregion

        #region khởi tạo
        public FAccountTransferDetail()
        {//Thêm
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion
            this.Text = "Thêm tài khoản kết chuyển";
            #region Thiết lập ban đầu cho Form
            //Khai báo các webservices
            chkIsActive.Visible = false;
            _IAccountTransferService = IoC.Resolve<IAccountTransferService>();
            _IAccountService = IoC.Resolve<IAccountService>();

            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion
        }

        public FAccountTransferDetail(AccountTransfer temp)
        {//Sửa
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            this.Text = "Sửa tài khoản kết chuyển";
            #endregion

            #region Thiết lập ban đầu cho Form
            _Select = temp;
            Them = false;
            txtAccountTransferCode.Enabled = false;

            //Khai báo các webservices
            _IAccountTransferService = IoC.Resolve<IAccountTransferService>();
            _IAccountService = IoC.Resolve<IAccountService>();

            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion

            #region Fill dữ liệu Obj vào control
            ObjandGUI(temp, false);
            #endregion
        }

        private void InitializeGUI()
        {
            //khởi tạo cbb loại kết chuyển
            cbbAccountTransferType.DataSource = FAccountTransfer.DicAccountTransferType.Values.ToList();
            cbbAccountTransferType.SelectedIndex = 0;
            //khởi tạo cbb bên kết chuyển
            cbbTransferSide.DataSource = FAccountTransfer.DicTransferSide.Values.ToList();
            cbbTransferSide.SelectedIndex = 0;
            //khởi tạo cbbFromAccount
            cbbFromAccount.DataSource = _IAccountService.GetAll().Where(p => p.IsActive == true).OrderBy(p=>p.AccountNumber).ToList();
            cbbFromAccount.DisplayMember = "AccountNumber";
            Utils.ConfigGrid(cbbFromAccount, ConstDatabase.Account_TableName);
            //khởi tạo cbbToAccount
            cbbToAccount.DataSource = _IAccountService.GetAll().Where(p => p.IsActive == true).OrderBy(p=>p.AccountNumber).ToList();
            cbbToAccount.DisplayMember = "AccountNumber";
            Utils.ConfigGrid(cbbToAccount, ConstDatabase.Account_TableName);
        }
        #endregion

        #region Button Event
        /// <summary>
        /// Sự kiện nút Lưu lại
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion

            //Gui to object
            #region Fill dữ liệu control vào obj
            AccountTransfer temp = Them ? new AccountTransfer() : _IAccountTransferService.Getbykey(_Select.ID);
            temp = ObjandGUI(temp, true);
            #endregion

            #region Thao tác CSDL
            _IAccountTransferService.BeginTran();
            if (Them) _IAccountTransferService.CreateNew(temp);
            else _IAccountTransferService.Update(temp);
            _IAccountTransferService.CommitTran();
            #endregion

            #region xử lý form, kết thúc form
            this.Close();
            isClose = false;
            #endregion
        }

        /// <summary>
        /// Sự kiện nút đóng
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            isClose = true;
            this.Close();
        }
        #endregion

        #region Utils
        /// <summary>
        /// get hoặc set một đối tượng vào giao diện xử lý
        /// </summary>
        /// <param name="input">đối tượng</param>
        /// <param name="isGet">True: get giá trị từ control gán vào đối tượng| False: set giá trị đối tượng vào control</param>
        /// <returns></returns>
        AccountTransfer ObjandGUI(AccountTransfer input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();  //Thêm mới

                input.AccountTransferCode = txtAccountTransferCode.Text;
                input.AccountTransferOrder = int.Parse(txtAccountTransferOrder.Value.ToString());
                input.AccountTransferType = cbbAccountTransferType.SelectedIndex;
                //từ tài khoản
                Account temp0 = (Account)Utils.getSelectCbbItem(cbbFromAccount);
                if (temp0 == null) input.FromAccount = string.Empty;
                else input.FromAccount = temp0.AccountNumber;
                //đến tài khoản
                Account temp1 = (Account)Utils.getSelectCbbItem(cbbToAccount);
                if (temp1 == null) input.ToAccount = string.Empty;
                else input.ToAccount = temp1.AccountNumber;

                input.TransferSide = cbbTransferSide.SelectedIndex;
                input.Description = txtDescription.Text;
                input.IsActive = chkIsActive.Checked;
                input.IsSecurity = false;
            }
            else
            {
                txtAccountTransferCode.Text = input.AccountTransferCode;
                cbbAccountTransferType.SelectedIndex = input.AccountTransferType;
                txtAccountTransferOrder.Value = input.AccountTransferOrder.ToString();
                cbbTransferSide.SelectedIndex = input.TransferSide;
                //từ tài khoản
                foreach (var item in cbbFromAccount.Rows)
                {
                    if ((item.ListObject as Account).AccountNumber == input.FromAccount) cbbFromAccount.SelectedRow = item;
                }
                //đến tài khoản
                foreach (var item in cbbToAccount.Rows)
                {
                    if ((item.ListObject as Account).AccountNumber == input.ToAccount) cbbToAccount.SelectedRow = item;
                }
                txtDescription.Text = input.Description;
                chkIsActive.Checked = input.IsActive;
            }
            return input;
        }

        bool CheckError()
        {
            bool kq = true;
            //Check Error Chung

            //Check Error Riêng từng Form
            if (string.IsNullOrEmpty(txtAccountTransferCode.Text) || string.IsNullOrEmpty(txtAccountTransferCode.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }
            List<string> ListTran = _IAccountTransferService.Query.Select(p => p.AccountTransferCode).ToList();
            foreach (var item in ListTran)
            {
                if (item == txtAccountTransferCode.Text && Them)
                {
                    MSG.Warning(string.Format(resSystem.MSG_Catalog_Account6, "kết chuyển"));
                    return false;
                }
            }

            if (cbbFromAccount.Value == null)
            {
                MSG.Warning(resSystem.MSG_Catalog_AccountTransfer);
                cbbFromAccount.Focus();
                return false;
            }

            if (cbbToAccount.Value == null)
            {
                MSG.Warning(resSystem.MSG_Catalog_AccountTransfer1);
                cbbToAccount.Focus();
                return false;
            }
            return kq;
        }
        #endregion

        private void FAccountTransferDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
