﻿namespace Accounting
{
    partial class FAccountTransferDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            this.chkIsActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbToAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbbFromAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtAccountTransferOrder = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.lblToAccount = new Infragistics.Win.Misc.UltraLabel();
            this.lblFromAccount = new Infragistics.Win.Misc.UltraLabel();
            this.lblThuTuKC = new Infragistics.Win.Misc.UltraLabel();
            this.cbbAccountTransferType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.lblLoaiKC = new Infragistics.Win.Misc.UltraLabel();
            this.cbbTransferSide = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.txtDescription = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtAccountTransferCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblBenKC = new Infragistics.Win.Misc.UltraLabel();
            this.lblDescription = new Infragistics.Win.Misc.UltraLabel();
            this.lblMa = new Infragistics.Win.Misc.UltraLabel();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbToAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbFromAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountTransferOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountTransferType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbTransferSide)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountTransferCode)).BeginInit();
            this.SuspendLayout();
            // 
            // chkIsActive
            // 
            appearance1.TextVAlignAsString = "Middle";
            this.chkIsActive.Appearance = appearance1;
            this.chkIsActive.BackColor = System.Drawing.Color.Transparent;
            this.chkIsActive.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsActive.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkIsActive.Checked = true;
            this.chkIsActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIsActive.Location = new System.Drawing.Point(9, 240);
            this.chkIsActive.Name = "chkIsActive";
            this.chkIsActive.Size = new System.Drawing.Size(103, 22);
            this.chkIsActive.TabIndex = 27;
            this.chkIsActive.Text = "Hoạt động";
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox1.Controls.Add(this.cbbToAccount);
            this.ultraGroupBox1.Controls.Add(this.cbbFromAccount);
            this.ultraGroupBox1.Controls.Add(this.txtAccountTransferOrder);
            this.ultraGroupBox1.Controls.Add(this.lblToAccount);
            this.ultraGroupBox1.Controls.Add(this.lblFromAccount);
            this.ultraGroupBox1.Controls.Add(this.lblThuTuKC);
            this.ultraGroupBox1.Controls.Add(this.cbbAccountTransferType);
            this.ultraGroupBox1.Controls.Add(this.lblLoaiKC);
            this.ultraGroupBox1.Controls.Add(this.cbbTransferSide);
            this.ultraGroupBox1.Controls.Add(this.txtDescription);
            this.ultraGroupBox1.Controls.Add(this.txtAccountTransferCode);
            this.ultraGroupBox1.Controls.Add(this.lblBenKC);
            this.ultraGroupBox1.Controls.Add(this.lblDescription);
            this.ultraGroupBox1.Controls.Add(this.lblMa);
            appearance9.FontData.BoldAsString = "True";
            this.ultraGroupBox1.HeaderAppearance = appearance9;
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(3, 6);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(377, 224);
            this.ultraGroupBox1.TabIndex = 26;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbToAccount
            // 
            this.cbbToAccount.AutoSize = false;
            this.cbbToAccount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbToAccount.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbToAccount.Location = new System.Drawing.Point(303, 131);
            this.cbbToAccount.Name = "cbbToAccount";
            this.cbbToAccount.Size = new System.Drawing.Size(68, 22);
            this.cbbToAccount.TabIndex = 25;
            // 
            // cbbFromAccount
            // 
            this.cbbFromAccount.AutoSize = false;
            this.cbbFromAccount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbFromAccount.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbFromAccount.Location = new System.Drawing.Point(113, 131);
            this.cbbFromAccount.Name = "cbbFromAccount";
            this.cbbFromAccount.Size = new System.Drawing.Size(68, 22);
            this.cbbFromAccount.TabIndex = 24;
            // 
            // txtAccountTransferOrder
            // 
            this.txtAccountTransferOrder.AutoSize = false;
            this.txtAccountTransferOrder.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.txtAccountTransferOrder.Location = new System.Drawing.Point(113, 79);
            this.txtAccountTransferOrder.MaxValue = 99999;
            this.txtAccountTransferOrder.MinValue = 0;
            this.txtAccountTransferOrder.Name = "txtAccountTransferOrder";
            this.txtAccountTransferOrder.Size = new System.Drawing.Size(258, 22);
            this.txtAccountTransferOrder.SpinButtonAlignment = Infragistics.Win.ButtonAlignment.Left;
            this.txtAccountTransferOrder.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.OnMouseEnter;
            this.txtAccountTransferOrder.TabIndex = 22;
            // 
            // lblToAccount
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            this.lblToAccount.Appearance = appearance2;
            this.lblToAccount.Location = new System.Drawing.Point(199, 136);
            this.lblToAccount.Name = "lblToAccount";
            this.lblToAccount.Size = new System.Drawing.Size(101, 19);
            this.lblToAccount.TabIndex = 18;
            this.lblToAccount.Text = "Kết chuyển đến (*)";
            // 
            // lblFromAccount
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextVAlignAsString = "Middle";
            this.lblFromAccount.Appearance = appearance3;
            this.lblFromAccount.Location = new System.Drawing.Point(9, 131);
            this.lblFromAccount.Name = "lblFromAccount";
            this.lblFromAccount.Size = new System.Drawing.Size(92, 22);
            this.lblFromAccount.TabIndex = 16;
            this.lblFromAccount.Text = "Kết chuyển từ (*)";
            // 
            // lblThuTuKC
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.lblThuTuKC.Appearance = appearance4;
            this.lblThuTuKC.Location = new System.Drawing.Point(9, 79);
            this.lblThuTuKC.Name = "lblThuTuKC";
            this.lblThuTuKC.Size = new System.Drawing.Size(101, 22);
            this.lblThuTuKC.TabIndex = 14;
            this.lblThuTuKC.Text = "Thứ tự kết chuyển";
            // 
            // cbbAccountTransferType
            // 
            this.cbbAccountTransferType.AutoSize = false;
            this.cbbAccountTransferType.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountTransferType.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbbAccountTransferType.Location = new System.Drawing.Point(113, 53);
            this.cbbAccountTransferType.MaxLength = 512;
            this.cbbAccountTransferType.Name = "cbbAccountTransferType";
            this.cbbAccountTransferType.Size = new System.Drawing.Size(258, 22);
            this.cbbAccountTransferType.TabIndex = 21;
            // 
            // lblLoaiKC
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.TextVAlignAsString = "Middle";
            this.lblLoaiKC.Appearance = appearance5;
            this.lblLoaiKC.Location = new System.Drawing.Point(9, 53);
            this.lblLoaiKC.Name = "lblLoaiKC";
            this.lblLoaiKC.Size = new System.Drawing.Size(99, 22);
            this.lblLoaiKC.TabIndex = 12;
            this.lblLoaiKC.Text = "Loại kết chuyển (*)";
            // 
            // cbbTransferSide
            // 
            this.cbbTransferSide.AutoSize = false;
            this.cbbTransferSide.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbTransferSide.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbbTransferSide.Location = new System.Drawing.Point(113, 105);
            this.cbbTransferSide.MaxLength = 512;
            this.cbbTransferSide.Name = "cbbTransferSide";
            this.cbbTransferSide.Size = new System.Drawing.Size(258, 22);
            this.cbbTransferSide.TabIndex = 23;
            // 
            // txtDescription
            // 
            this.txtDescription.AutoSize = false;
            this.txtDescription.Location = new System.Drawing.Point(113, 157);
            this.txtDescription.MaxLength = 512;
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(258, 58);
            this.txtDescription.TabIndex = 26;
            // 
            // txtAccountTransferCode
            // 
            this.txtAccountTransferCode.AutoSize = false;
            this.txtAccountTransferCode.Location = new System.Drawing.Point(113, 27);
            this.txtAccountTransferCode.MaxLength = 25;
            this.txtAccountTransferCode.Name = "txtAccountTransferCode";
            this.txtAccountTransferCode.Size = new System.Drawing.Size(258, 22);
            this.txtAccountTransferCode.TabIndex = 20;
            // 
            // lblBenKC
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextVAlignAsString = "Middle";
            this.lblBenKC.Appearance = appearance6;
            this.lblBenKC.Location = new System.Drawing.Point(9, 105);
            this.lblBenKC.Name = "lblBenKC";
            this.lblBenKC.Size = new System.Drawing.Size(99, 22);
            this.lblBenKC.TabIndex = 2;
            this.lblBenKC.Text = "Bên kết chuyển";
            // 
            // lblDescription
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.TextVAlignAsString = "Middle";
            this.lblDescription.Appearance = appearance7;
            this.lblDescription.Location = new System.Drawing.Point(9, 157);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(92, 22);
            this.lblDescription.TabIndex = 1;
            this.lblDescription.Text = "Diễn giải";
            // 
            // lblMa
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.TextVAlignAsString = "Middle";
            this.lblMa.Appearance = appearance8;
            this.lblMa.Location = new System.Drawing.Point(9, 27);
            this.lblMa.Name = "lblMa";
            this.lblMa.Size = new System.Drawing.Size(98, 22);
            this.lblMa.TabIndex = 0;
            this.lblMa.Text = "Mã kết chuyển (*)";
            // 
            // btnClose
            // 
            appearance10.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance10;
            this.btnClose.Location = new System.Drawing.Point(300, 236);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 29;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            appearance11.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance11;
            this.btnSave.Location = new System.Drawing.Point(213, 236);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 28;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // FAccountTransferDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(383, 272);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.ultraGroupBox1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.chkIsActive);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FAccountTransferDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FAccountTransferDetail_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbToAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbFromAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountTransferOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountTransferType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbTransferSide)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountTransferCode)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsActive;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraLabel lblToAccount;
        private Infragistics.Win.Misc.UltraLabel lblFromAccount;
        private Infragistics.Win.Misc.UltraLabel lblThuTuKC;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbAccountTransferType;
        private Infragistics.Win.Misc.UltraLabel lblLoaiKC;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbTransferSide;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDescription;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountTransferCode;
        private Infragistics.Win.Misc.UltraLabel lblBenKC;
        private Infragistics.Win.Misc.UltraLabel lblDescription;
        private Infragistics.Win.Misc.UltraLabel lblMa;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor txtAccountTransferOrder;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbToAccount;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbFromAccount;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnSave;
    }
}
