﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinTree;
using System.Data;

namespace Accounting
{
    public partial class FBudgetItem : CatalogBase
    {
        #region khai báo
        private readonly IBudgetItemService _IBudgetItemService;
        List<BudgetItem> dsBudgetItem = new List<BudgetItem>();
        #endregion
        public FBudgetItem()
        {
            InitializeComponent();
            ToolTip ToolTip1 = new ToolTip();
            ToolTip1.SetToolTip(btnAdd, "Thêm (Ctrl + N)");
            ToolTip ToolTip2 = new ToolTip();
            ToolTip2.SetToolTip(btnEdit, "Sửa (Ctrl + E)");
            ToolTip ToolTip3 = new ToolTip();
            ToolTip3.SetToolTip(btnDelete, "Xóa (Ctrl + D)");
            #region Thiết lập ban đầu cho Form
            _IBudgetItemService = IoC.Resolve<IBudgetItemService>();

            this.uTree.InitializeDataNode += new Infragistics.Win.UltraWinTree.InitializeDataNodeEventHandler(Utils.uTree_InitializeDataNode);
            this.uTree.AfterDataNodesCollectionPopulated += new Infragistics.Win.UltraWinTree.AfterDataNodesCollectionPopulatedEventHandler(Utils.uTree_AfterDataNodesCollectionPopulated);
            this.uTree.ColumnSetGenerated += new Infragistics.Win.UltraWinTree.ColumnSetGeneratedEventHandler(this.uTree_ColumnSetGenerated);
            this.uTree.MouseDown += new System.Windows.Forms.MouseEventHandler(this.uTree_MouseDown);
            this.uTree.DoubleClick += new System.EventHandler(this.uTree_DoubleClick);
            #endregion

            #region Tạo dữ liệu ban đầu
            LoadDuLieu();
            #endregion
        }
        protected override void DeleteFunction()
        {
            if (uTree.SelectedNodes.Count == 0 && uTree.ActiveNode != null)
            {
                uTree.ActiveNode.Selected = true;
            }
            if (uTree.SelectedNodes.Count > 0)
            {
                BudgetItem temp = dsBudgetItem.Single(k => k.ID == (new Guid(uTree.SelectedNodes[0].Text)));
                if (MSG.Question(string.Format(resSystem.MSG_System_05, temp.BudgetItemCode)) == System.Windows.Forms.DialogResult.Yes)
                {
                    if (Utils.checkRelationVoucher_BudgetItem(temp))
                    {
                        MSG.Error("Không thể xóa Mục Thu/Chi vì có phát sinh chứng từ liên quan");
                        return;
                    }
                    _IBudgetItemService.BeginTran();
                    //Kiểm tra xem đối tượng có con không nếu có con thì không được phép xóa
                    //List<BudgetItem> lstChild =
                    //        _IBudgetItemService.Query.Where(p => p.ParentID == temp.ID).ToList();
                    List<BudgetItem> lstChild =
                            _IBudgetItemService.GetListBudgetItemParentID(temp.ID);
                    if (lstChild.Count > 0)
                    {
                        MessageBox.Show(string.Format(resSystem.MSG_Catalog_Tree,"mục thu chi"));
                        return;
                    }
                    //Check xem cha còn có con khác không nếu không thì chuyển trạng thái IsParentNode = false
                    if (temp.ParentID != null)
                    {
                        BudgetItem parent = _IBudgetItemService.Getbykey((Guid)temp.ParentID);
                        //int checkChildOldParent = _IBudgetItemService.Query.Count(p => p.ParentID == temp.ParentID);
                        int checkChildOldParent = _IBudgetItemService.CountBudgetItemParentID(temp.ParentID);
                        if (checkChildOldParent <= 1)
                        {
                            parent.IsParentNode = false;
                        }
                        _IBudgetItemService.Update(parent);
                        _IBudgetItemService.Delete(temp);
                    }
                    if (temp.ParentID == null)
                    {
                        _IBudgetItemService.Delete(temp);
                    }
                    _IBudgetItemService.CommitTran();
                    Utils.ClearCacheByType<BudgetItem>();
                    LoadDuLieu();
                }
            }
            else
                MSG.Error(resSystem.MSG_System_06);
        }
        protected override void AddFunction()
        {
            if (uTree.SelectedNodes.Count > 0)
            {
                new FBudgetItemDetail(dsBudgetItem.Single(k => k.ID == (new Guid(uTree.SelectedNodes[0].Text))), true).ShowDialog(this);
                if (!FBudgetItemDetail.isClose) LoadDuLieu();
            }
            else
            {

                new FBudgetItemDetail().ShowDialog(this);
                if (!FBudgetItemDetail.isClose) LoadDuLieu();
            }
        }
        protected override void EditFunction()
        {
            if (uTree.SelectedNodes.Count == 0 && uTree.ActiveNode != null)
            {
                uTree.ActiveNode.Selected = true;
            }
            if (uTree.SelectedNodes.Count > 0)
            {
                var temp = dsBudgetItem.Single(k => k.ID == (new Guid(uTree.SelectedNodes[0].Text)));
                if (Utils.checkRelationVoucher_BudgetItem(temp))
                {
                    MSG.Error("Không thể sửa Mục Thu/Chi vì có phát sinh chứng từ liên quan");
                    return;
                }
                else
                {
                    new FBudgetItemDetail(temp, false).ShowDialog(this);
                    if (!FBudgetItemDetail.isClose) LoadDuLieu();
                }
            }
            else
                Accounting.TextMessage.MSG.Error(string.Format(Accounting.TextMessage.resSystem.MSG_Catalog3,"một Mục thu chi"));
        }
        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }
        private void LoadDuLieu(bool configTree)
        {
            //Dictionary<int, string> tpye = new Dictionary<int, string>();
            //tpye.Add(0, "Chi");
            //tpye.Add(1, "Thu");
            WaitingFrm.StartWaiting();
            #region Lấy dữ liệu từ CSDL
            dsBudgetItem = _IBudgetItemService.GetAll_OrderBy();
            _IBudgetItemService.UnbindSession(dsBudgetItem);
            dsBudgetItem = _IBudgetItemService.GetAll_OrderBy();
            #endregion

            #region Xử lý dữ liệu
            foreach (BudgetItem item in dsBudgetItem)
            {
                item.BudgetItemTypeVIEW = item.BudgetItemType == 0 ? "Chi" : "Thu";
                //item.BudgetItemTypeVIEW = tpye[item.BudgetItemType];
                //item.BudgetItemTypeVIEW = Utils.DicBudgetItemType[item.BudgetItemType.ToString()].Name;
            }
            #endregion

            #region hiển thị và xử lý hiển thị
            DataSet ds = Utils.ToDataSet<BudgetItem>(dsBudgetItem, ConstDatabase.BudgetItem_TableName);
            uTree.SetDataBinding(ds, ConstDatabase.BudgetItem_TableName);

            if (configTree) ConfigTree(uTree);
            #endregion
            WaitingFrm.StopWaiting();
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }
        #region Event
        private void uTree_MouseDown(object sender, MouseEventArgs e)
        {//khi click chuột phải hiện content menu
            Utils.uGrid_MouseDown(sender, e, cms4Grid);
        }

        private void uTree_DoubleClick(object sender, EventArgs e)
        {//click đúp chuột để sửa
            //editFunction();
        }

        private void uTree_ColumnSetGenerated(object sender, ColumnSetGeneratedEventArgs e)
        {
            Utils.uTree_ColumnSetGenerated(sender, e, ConstDatabase.BudgetItem_TableName);
        }
        #endregion
        #region Utils
        void ConfigTree(Infragistics.Win.UltraWinTree.UltraTree ultraTree)
        {//hàm chung
            Utils.ConfigTree(ultraTree, TextMessage.ConstDatabase.BudgetItem_TableName);
        }
        #endregion

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tsmAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void tsmEdit_Click(object sender, EventArgs e)
        {
            EditFunction();

        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void uTree_DoubleClick_1(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            ResetFunction();
        }
        protected override void ResetFunction()
        {
            LoadDuLieu(false);
        }

        private void FBudgetItem_FormClosing(object sender, FormClosingEventArgs e)
        {
            uTree = null;
        }

        private void FBudgetItem_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
