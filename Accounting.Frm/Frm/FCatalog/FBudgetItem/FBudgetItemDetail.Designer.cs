﻿namespace Accounting
{
    partial class FBudgetItemDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraComboEditor1 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.txtDescrip = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblRegistrationGroupName = new Infragistics.Win.Misc.UltraLabel();
            this.txtBudgetItemCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbParentID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.lblParentID = new Infragistics.Win.Misc.UltraLabel();
            this.txtBudgetName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblRegistrationGroupCode = new Infragistics.Win.Misc.UltraLabel();
            this.chkIsActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescrip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBudgetItemCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbParentID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBudgetName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox1.Controls.Add(this.ultraComboEditor1);
            this.ultraGroupBox1.Controls.Add(this.txtDescrip);
            this.ultraGroupBox1.Controls.Add(this.lblRegistrationGroupName);
            this.ultraGroupBox1.Controls.Add(this.txtBudgetItemCode);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox1.Controls.Add(this.cbbParentID);
            this.ultraGroupBox1.Controls.Add(this.lblParentID);
            this.ultraGroupBox1.Controls.Add(this.txtBudgetName);
            this.ultraGroupBox1.Controls.Add(this.lblRegistrationGroupCode);
            appearance6.FontData.BoldAsString = "True";
            this.ultraGroupBox1.HeaderAppearance = appearance6;
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(12, 12);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(377, 180);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraComboEditor1
            // 
            this.ultraComboEditor1.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.SuggestAppend;
            this.ultraComboEditor1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.ultraComboEditor1.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.ultraComboEditor1.Location = new System.Drawing.Point(134, 34);
            this.ultraComboEditor1.Name = "ultraComboEditor1";
            this.ultraComboEditor1.Size = new System.Drawing.Size(237, 21);
            this.ultraComboEditor1.TabIndex = 0;
            this.ultraComboEditor1.ValueChanged += new System.EventHandler(this.ultraComboEditor1_ValueChanged);
            // 
            // txtDescrip
            // 
            this.txtDescrip.Location = new System.Drawing.Point(134, 135);
            this.txtDescrip.MaxLength = 512;
            this.txtDescrip.Multiline = true;
            this.txtDescrip.Name = "txtDescrip";
            this.txtDescrip.Size = new System.Drawing.Size(237, 37);
            this.txtDescrip.TabIndex = 4;
            // 
            // lblRegistrationGroupName
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.TextVAlignAsString = "Middle";
            this.lblRegistrationGroupName.Appearance = appearance1;
            this.lblRegistrationGroupName.Location = new System.Drawing.Point(9, 135);
            this.lblRegistrationGroupName.Name = "lblRegistrationGroupName";
            this.lblRegistrationGroupName.Size = new System.Drawing.Size(126, 22);
            this.lblRegistrationGroupName.TabIndex = 28;
            this.lblRegistrationGroupName.Text = "Diễn Giải";
            // 
            // txtBudgetItemCode
            // 
            this.txtBudgetItemCode.Location = new System.Drawing.Point(134, 59);
            this.txtBudgetItemCode.MaxLength = 25;
            this.txtBudgetItemCode.Name = "txtBudgetItemCode";
            this.txtBudgetItemCode.Size = new System.Drawing.Size(237, 21);
            this.txtBudgetItemCode.TabIndex = 1;
            // 
            // ultraLabel2
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance2;
            this.ultraLabel2.Location = new System.Drawing.Point(9, 33);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(126, 22);
            this.ultraLabel2.TabIndex = 25;
            this.ultraLabel2.Text = "Loại (*)";
            // 
            // ultraLabel1
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance3;
            this.ultraLabel1.Location = new System.Drawing.Point(9, 58);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(126, 22);
            this.ultraLabel1.TabIndex = 24;
            this.ultraLabel1.Text = "Mã  (*)";
            // 
            // cbbParentID
            // 
            this.cbbParentID.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.SuggestAppend;
            this.cbbParentID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbParentID.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbParentID.Location = new System.Drawing.Point(134, 109);
            this.cbbParentID.Name = "cbbParentID";
            this.cbbParentID.NullText = "<Xin Chọn>";
            this.cbbParentID.Size = new System.Drawing.Size(237, 22);
            this.cbbParentID.TabIndex = 3;
            // 
            // lblParentID
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.lblParentID.Appearance = appearance4;
            this.lblParentID.Location = new System.Drawing.Point(9, 109);
            this.lblParentID.Name = "lblParentID";
            this.lblParentID.Size = new System.Drawing.Size(92, 22);
            this.lblParentID.TabIndex = 16;
            this.lblParentID.Text = "Thuộc nhóm";
            // 
            // txtBudgetName
            // 
            this.txtBudgetName.Location = new System.Drawing.Point(134, 84);
            this.txtBudgetName.MaxLength = 512;
            this.txtBudgetName.Name = "txtBudgetName";
            this.txtBudgetName.Size = new System.Drawing.Size(237, 21);
            this.txtBudgetName.TabIndex = 2;
            // 
            // lblRegistrationGroupCode
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.TextVAlignAsString = "Middle";
            this.lblRegistrationGroupCode.Appearance = appearance5;
            this.lblRegistrationGroupCode.Location = new System.Drawing.Point(9, 83);
            this.lblRegistrationGroupCode.Name = "lblRegistrationGroupCode";
            this.lblRegistrationGroupCode.Size = new System.Drawing.Size(126, 22);
            this.lblRegistrationGroupCode.TabIndex = 0;
            this.lblRegistrationGroupCode.Text = "Tên (*)";
            // 
            // chkIsActive
            // 
            appearance7.TextHAlignAsString = "Left";
            appearance7.TextVAlignAsString = "Middle";
            this.chkIsActive.Appearance = appearance7;
            this.chkIsActive.BackColor = System.Drawing.Color.Transparent;
            this.chkIsActive.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsActive.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkIsActive.Checked = true;
            this.chkIsActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIsActive.Location = new System.Drawing.Point(21, 201);
            this.chkIsActive.Name = "chkIsActive";
            this.chkIsActive.Size = new System.Drawing.Size(103, 22);
            this.chkIsActive.TabIndex = 5;
            this.chkIsActive.Text = "Hoạt động";
            this.chkIsActive.ValidateCheckState += new Infragistics.Win.CheckEditor.ValidateCheckStateHandler(this.chkIsActive_ValidateCheckState);
            // 
            // btnClose
            // 
            appearance8.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance8;
            this.btnClose.Location = new System.Drawing.Point(312, 197);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 7;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            appearance9.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance9;
            this.btnSave.Location = new System.Drawing.Point(231, 197);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // FBudgetItemDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 243);
            this.Controls.Add(this.chkIsActive);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.ultraGroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FBudgetItemDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mục Thu/Chi";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FBudgetItemDetail_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescrip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBudgetItemCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbParentID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBudgetName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbParentID;
        private Infragistics.Win.Misc.UltraLabel lblParentID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtBudgetName;
        private Infragistics.Win.Misc.UltraLabel lblRegistrationGroupCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDescrip;
        private Infragistics.Win.Misc.UltraLabel lblRegistrationGroupName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtBudgetItemCode;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsActive;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor ultraComboEditor1;
    }
}