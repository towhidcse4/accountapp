﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
namespace Accounting
{
    public partial class FBudgetItemDetail : DialogForm
    {
        #region khai báo
        private readonly IBudgetItemService _IBudgetItemService;
        bool checkActive = true;
        BudgetItem _Select = new BudgetItem();
        //List<string> getBudItemType = new List<string>();
        private Guid _id;
        public Guid Id
        {
            get { return _id; }
            set { _id = value; }
        }
        bool IsAdd = true;
        public static bool isClose = true;
        int type;
        #endregion
        public FBudgetItemDetail()
        {
            InitializeComponent();
            _IBudgetItemService = IoC.Resolve<IBudgetItemService>();
            InitializeGUI();
            chkIsActive.Visible = false;
            this.Text = "Thêm Mục Thu/Chi";
        }
        public FBudgetItemDetail(BudgetItem temp, bool isAdd)
        {//Sửa
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _Select = isAdd ? new BudgetItem() : temp;
            IsAdd = isAdd;

            //Khai báo các webservices
            _IBudgetItemService = IoC.Resolve<IBudgetItemService>();

            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion

            if (!isAdd)
            {
                this.Text = "Sửa Mục Thu/Chi";
                #region Fill dữ liệu Obj vào control
                ObjandGUI(temp, false);
                #endregion

                txtBudgetItemCode.Enabled = false;
                //ultraComboEditor1.Enabled = false;

            }
            else
            {
                this.Text = "Thêm Mục Thu/Chi";
                ultraComboEditor1.SelectedIndex = temp.BudgetItemType;
                chkIsActive.Visible = false;
                //Hỗ trợ chọn ID cha
                foreach (var item in cbbParentID.Rows)
                {
                    if ((item.ListObject as BudgetItem).ID == temp.ID) cbbParentID.SelectedRow = item;
                }
            }
        }

        BudgetItem ObjandGUI(BudgetItem input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();  //Thêm mới
                //input.BudgetItemType = cbbBudgetItemType.SelectedRow
                input.BudgetItemCode = txtBudgetItemCode.Text;
                input.BudgetItemName = txtBudgetName.Text;
                input.Description = txtDescrip.Text;
                input.BudgetItemType = ultraComboEditor1.SelectedIndex;
                BudgetItem temp0 = (BudgetItem)Utils.getSelectCbbItem(cbbParentID);
                if (temp0 == null) input.ParentID = null;
                else input.ParentID = temp0.ID;
                input.IsActive = chkIsActive.Checked;
            }
            else
            {
                txtBudgetName.Text = input.BudgetItemName;
                txtBudgetItemCode.Text = input.BudgetItemCode;
                txtDescrip.Text = input.Description;
                ultraComboEditor1.SelectedIndex = input.BudgetItemType;
                type = input.BudgetItemType;

                foreach (var item in cbbParentID.Rows)
                {
                    if ((item.ListObject as BudgetItem).ID == input.ParentID) cbbParentID.SelectedRow = item;
                }
                chkIsActive.Checked = input.IsActive;
            }
            return input;
        }

        private void InitializeGUI()
        {

            List<string> ltemp = new List<string>() { "Chi", "Thu" };
            ultraComboEditor1.DataSource = ltemp;
            ultraComboEditor1.SelectedIndex = 0;

            //List<BudgetItem> getParent = _IBudgetItemService.Query.Where(
            //                   a => a.BudgetItemType == ultraComboEditor1.SelectedIndex).ToList();
            List<BudgetItem> getParent = _IBudgetItemService.GetListBudgetItemBudgetItemType(ultraComboEditor1.SelectedIndex);
            cbbParentID.DataSource = getParent;
            cbbParentID.DisplayMember = "BudgetItemCode";
            Utils.ConfigGrid(cbbParentID, ConstDatabase.BudgetItem_TableName);


            //Utils.ConfigGrid(cbbParentID, ConstDatabase.BudgetItem_TableName);
        }
        private void btnSave_Click(object sender, EventArgs e)
        {

            #region Check Lỗi
            if (!CheckError()) return;
            #endregion

            //Gui to object
            #region Fill dữ liệu control vào obj

            // thuc hien trong tung thao tac (them moi/ cap nhat)
            #endregion

            #region Thao tác CSDL
            try
            {
                _IBudgetItemService.BeginTran();
                BudgetItem temp;
                if (IsAdd) // them moi
                {
                    temp = IsAdd ? new BudgetItem() : _IBudgetItemService.Getbykey(_Select.ID);
                    temp = ObjandGUI(temp, true);

                    //set order fixcode
                    BudgetItem temp0 = (BudgetItem)Utils.getSelectCbbItem(cbbParentID);
                    //List<string> lstOrderFixCodeChild = _IBudgetItemService.Query.Where(a => a.ParentID == temp.ParentID)
                    //    .Select(a => a.OrderFixCode).ToList();
                    List<string> lstOrderFixCodeChild = _IBudgetItemService.GetListOrderFixCodeBudgetItemChild(temp.ParentID);
                    string orderFixCodeParent = temp0 == null ? "" : temp0.OrderFixCode;
                    temp.OrderFixCode = Utils.GetOrderFixCode(lstOrderFixCodeChild, orderFixCodeParent);
                    temp.IsParentNode = false; // mac dinh cho insert
                    temp.Grade = temp0 == null ? 1 : temp0.Grade + 1; // quy tac lay gia tri cho grade

                    //nếu cha isactive =false thì khi thêm mới con cũng là false

                    if (cbbParentID.Text.Equals(""))
                    {
                        if (!CheckCode()) return;
                        temp.IsActive = true;
                        _IBudgetItemService.CreateNew(temp);
                    }
                    else
                    {
                        BudgetItem objParentCombox = (BudgetItem)Utils.getSelectCbbItem(cbbParentID);
                        //lay cha
                        BudgetItem Parent = _IBudgetItemService.Getbykey(objParentCombox.ID);
                        if (Parent.IsActive == false)
                        {
                            temp.IsActive = false;
                        }
                        else
                            temp.IsActive = true;

                        //check loi
                        if (!CheckCode()) return;
                        //temp.IsActive = true;
                        _IBudgetItemService.CreateNew(temp);
                    }

                    //update lai isparentnode neu co
                    if (temp.ParentID != null)
                    {
                        BudgetItem parent = _IBudgetItemService.Getbykey((Guid)temp.ParentID);
                        parent.IsParentNode = true;
                        _IBudgetItemService.Update(parent);
                    }

                }
                else // cap nhat
                {
                    //if (ultraComboEditor1.SelectedIndex != type)
                    //{

                    //    MessageBox.Show("Không được phép chuyển như vậy !!!");
                    //    return;
                    //}
                    temp = _IBudgetItemService.Getbykey(_Select.ID);
                    if (temp.IsParentNode == true && ultraComboEditor1.SelectedIndex != type)
                    {
                        MSG.Error(string.Format(resSystem.MSG_Catalog6,temp.BudgetItemCode));
                        return;
                    }
                    //Lưu đối tượng trước khi sửa
                    BudgetItem tempOriginal = (BudgetItem)Utils.CloneObject(temp);
                    //Lấy về đối tượng chọn trong combobox
                    BudgetItem objParentCombox = (BudgetItem)Utils.getSelectCbbItem(cbbParentID);
                    //Không thay đổi đối tượng cha trong combobox
                    if (((temp.ParentID == null) && (objParentCombox == null)) || ((temp.ParentID != null) && (objParentCombox != null) && (temp.ParentID == objParentCombox.ID)))
                    {
                        temp = ObjandGUI(temp, true);
                        _IBudgetItemService.Update(temp);
                    }
                    else
                    {
                        //không cho phép chuyển ngược
                        if (objParentCombox != null)
                        {
                            //List<BudgetItem> lstChildCheck =
                            //    _IBudgetItemService.Query.Where(p => p.OrderFixCode.StartsWith(tempOriginal.OrderFixCode)).ToList();
                            List<BudgetItem> lstChildCheck = _IBudgetItemService.GetListBudgetItemOrderFixCode(tempOriginal.OrderFixCode);
                            foreach (var item in lstChildCheck)
                            {
                                if (objParentCombox.ID == item.ID)
                                {
                                    MSG.Error(string.Format(resSystem.MSG_Catalog6, temp.BudgetItemName));
                                    //MessageBox.Show("Không được phép chuyển như vậy");
                                    return;
                                }
                            }
                        }
                        //Trường hợp có cha cũ
                        //Kiểm tra cha cũ có còn con ngoài con chuyển đi hay không
                        //Chuyển trạng thái IsParentNode Cha cũ nếu hết con
                        if (temp.ParentID != null)
                        {
                            BudgetItem oldParent = _IBudgetItemService.Getbykey((Guid)temp.ParentID);
                            //int checkChildOldParent = _IBudgetItemService.Query.Count(p => p.ParentID == temp.ParentID);
                            int checkChildOldParent = _IBudgetItemService.GetListBudgetItemParentID(temp.ParentID).Count;
                            if (checkChildOldParent <= 1)
                            {
                                oldParent.IsParentNode = false;
                            }
                            _IBudgetItemService.Update(oldParent);
                        }
                        //Trường hợp có cha mới
                        //Chuyển trạng IsParentNode cha mới
                        if (objParentCombox != null)
                        {

                            BudgetItem newParent = _IBudgetItemService.Getbykey(objParentCombox.ID);
                            newParent.IsParentNode = true;
                            //if (true)
                            //    {

                            //    }

                            _IBudgetItemService.Update(newParent);

                            //List<BudgetItem> listChildNewParent = _IBudgetItemService.Query.Where(
                            //    a => a.ParentID == objParentCombox.ID).ToList();
                            List<BudgetItem> listChildNewParent = _IBudgetItemService.GetListBudgetItemParentID(objParentCombox.ID);
                            //Tính lại OrderFixCode cho đối tượng chính
                            List<string> lstOfcChildNewParent = new List<string>();
                            foreach (var item in listChildNewParent)
                            {
                                lstOfcChildNewParent.Add(item.OrderFixCode);
                            }
                            string orderFixCodeParent = newParent.OrderFixCode;
                            temp.OrderFixCode = Utils.GetOrderFixCode(lstOfcChildNewParent, orderFixCodeParent);
                            //Tính lại bậc cho đối tượng chính
                            temp.Grade = newParent.Grade + 1;
                        }
                        //Trường hợp không có cha mới tức là chuyển đối tượng lên bậc 1
                        if (objParentCombox == null)
                        {
                            //Lấy về tất cả đối tượng bậc 1
                            List<int> lstOfcChildGradeNo1 = new List<int>();
                            //List<BudgetItem> listChildGradeNo1 = _IBudgetItemService.Query.Where(
                            //    a => a.Grade == 1).ToList();
                            List<BudgetItem> listChildGradeNo1 = _IBudgetItemService.GetListBudgetItemGrade(1);
                            foreach (var item in listChildGradeNo1)
                            {
                                lstOfcChildGradeNo1.Add(Convert.ToInt32(item.OrderFixCode));
                            }
                            //Tính lại OrderFixCode cho đối tượng chính
                            temp.OrderFixCode = (lstOfcChildGradeNo1.Max() + 1).ToString(CultureInfo.InvariantCulture);
                            //Tính lại bậc cho đối tượng chính
                            temp.Grade = 1;
                            temp = ObjandGUI(temp, true);
                            _IBudgetItemService.Update(temp);
                        }
                        //Xử lý các con của nó 
                        //Lấy về các con
                        //List<BudgetItem> lstChild =
                        //       _IBudgetItemService.Query.Where(p => p.OrderFixCode.StartsWith(tempOriginal.OrderFixCode)).ToList();
                        List<BudgetItem> lstChild = _IBudgetItemService.GetListBudgetItemOrderFixCode(tempOriginal.OrderFixCode);
                        foreach (var item in lstChild)
                        {
                            string tempOldFixCode = tempOriginal.OrderFixCode;
                            item.OrderFixCode = temp.OrderFixCode + item.OrderFixCode.Substring(tempOldFixCode.Length);
                            item.Grade = temp.Grade - tempOriginal.Grade + item.Grade;
                            _IBudgetItemService.Update(item);
                        }
                        temp = ObjandGUI(temp, true);
                    }
                    //nếu cha ngừng theo dõi thì con cũng ngừng theo dõi
                    //List<BudgetItem> lstChild2 =
                    //        _IBudgetItemService.Query.Where(p => p.OrderFixCode.StartsWith(tempOriginal.OrderFixCode)).ToList();
                    if (!checkActive)
                    {
                        List<BudgetItem> lstChild2 = _IBudgetItemService.GetListBudgetItemOrderFixCode(tempOriginal.OrderFixCode);
                        if (!chkIsActive.Checked)
                        {
                            foreach (var item2 in lstChild2)
                            {
                                item2.IsActive = false;
                                _IBudgetItemService.Update(item2);
                            }

                        }
                        else
                        {
                            //if (temp.IsParentNode && temp.ParentID == null)
                            if (temp.IsParentNode)
                            {
                                if (MSG.Question("Bạn có muốn thiết lập cho tất cả các danh mục con của danh mục này sang trạng thái <<Hoạt đông>> không?") == System.Windows.Forms.DialogResult.Yes)
                                {
                                    foreach (var item2 in lstChild2)
                                    {
                                        item2.IsActive = true;
                                        _IBudgetItemService.Update(item2);
                                    }
                                }
                            }
                        }

                        //nếu cha isactive = false thì con ko được isactive = true,

                        if (temp.IsParentNode == false)
                        {
                            //lấy cha

                            if (temp.ParentID == null)
                            {
                                if (!CheckError()) return;
                                if (!chkIsActive.Checked) temp.IsActive = false;
                                else temp.IsActive = true;
                                _IBudgetItemService.Update(temp);
                            }

                            else
                            {
                                BudgetItem Parent = _IBudgetItemService.Getbykey(objParentCombox.ID);
                                if (Parent.IsActive == true)
                                {
                                    if (!CheckError()) return;
                                    if (!chkIsActive.Checked) temp.IsActive = false;
                                    else temp.IsActive = true;
                                    _IBudgetItemService.Update(temp);
                                }
                                else
                                {
                                    if (chkIsActive.Checked)
                                        MSG.Warning(string.Format(resSystem.MSG_Catalog_Tree2, "mục thu chi"));
                                    _IBudgetItemService.RolbackTran();
                                }
                            }

                        }
                        else
                        {
                            if (temp.ParentID != null)
                            {
                                BudgetItem Parent = _IBudgetItemService.Getbykey(objParentCombox.ID);
                                if (Parent.IsActive == true)
                                {
                                    if (!CheckError()) return;
                                    if (!chkIsActive.Checked) temp.IsActive = false;
                                    else temp.IsActive = true;
                                    _IBudgetItemService.Update(temp);
                                }
                                else
                                {
                                    if (chkIsActive.Checked)
                                        MSG.Warning(string.Format(resSystem.MSG_Catalog_Tree2, "mục thu chi"));
                                    _IBudgetItemService.RolbackTran();
                                }
                            }
                            else
                            {
                                if (!CheckError()) return;
                                if (!chkIsActive.Checked) temp.IsActive = false;
                                else temp.IsActive = true;
                                _IBudgetItemService.Update(temp);
                            }
                        }
                    }
                }
                _IBudgetItemService.CommitTran();
                _id = temp.ID;
            }
            catch (Exception ex)
            {
                _IBudgetItemService.RolbackTran();
            }
            #endregion

            Utils.ListBudgetItem.Clear();
            //Utils.AddToBindingList(Utils.ListBudgetItem, _IBudgetItemService.GetByActive_OrderByTreeIsParentNode(true));
            #region xử lý form, kết thúc form
            this.Close();
            isClose = false;
            #endregion
        }
        bool CheckError()
        {
            bool kq = true;
            //Check Error Chung

            //Check Error Riêng từng Form
            if (string.IsNullOrEmpty(txtBudgetItemCode.Text) || string.IsNullOrEmpty(txtBudgetName.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }
            return kq;
        }
        bool CheckCode()
        {
            bool kq = true;
            //Check Error Chung
            //List<string> hh = _IBudgetItemService.Query.Select(a => a.BudgetItemCode).ToList();
            List<string> hh = _IBudgetItemService.GetListBudgetItemCode();
            foreach (var item in hh)
            {
                if (item.Equals(txtBudgetItemCode.Text))
                {
                    MSG.Warning(string.Format(resSystem.MSG_Catalog_Account6, "mục thu chi"));
                    return false;
                }
            }

            return kq;
        }
        private void ultraComboEditor1_ValueChanged(object sender, EventArgs e)
        {
            //List<BudgetItem> getParent = _IBudgetItemService.Query.Where(
            //                   a => a.BudgetItemType == ultraComboEditor1.SelectedIndex).ToList();
            List<BudgetItem> getParent = _IBudgetItemService.GetListBudgetItemBudgetItemType(ultraComboEditor1.SelectedIndex);
            cbbParentID.DataSource = getParent;
            cbbParentID.Text = "";

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            isClose = true;
            DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void chkIsActive_ValidateCheckState(object sender, Infragistics.Win.ValidateCheckStateEventArgs e)
        {
            checkActive = false;
        }

        private void FBudgetItemDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
