﻿using System;
using System.Windows.Forms;
using Accounting.TextMessage;
using System.Drawing;

namespace Accounting
{
    public partial class TestLoadImg : CustormForm
    {
        public TestLoadImg()
        {
            InitializeComponent();
        }

        private void ultraButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ultraGroupBox1_Click(object sender, EventArgs e)
        {
            var openFile = new OpenFileDialog
            {
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                Filter = "file hinh|*.jpg|all file|*.*"

            };

      
            var result = openFile.ShowDialog(this);
            if (result == DialogResult.OK || result == DialogResult.Yes)
            {
                //giới hạn dung lượng ảnh cho phép là 50 kb
                double fileSize = (double)new System.IO.FileInfo(openFile.FileName).Length / 1024;
                if (fileSize > 1000)
                {
                    MSG.Error(resSystem.MSG_Catalog_FBankAccountDetail);
                    return;
                }
                //Kích thước ảnh cho phép không lớn hơn 128x128
                try
                {
                    Image temp = System.Drawing.Image.FromFile(openFile.FileName);
                    if (temp.Width > 1280 || temp.Height > 1280)
                    {
                        MSG.Error(resSystem.MSG_Catalog_FBankAccountDetail1);
                        return;
                    }
                    imgIcon.ContentAreaAppearance.ImageBackground = System.Drawing.Image.FromFile(openFile.FileName);
                }
                catch (Exception)
                {
                    MSG.Error(resSystem.MSG_Catalog_FBank);
                    return;
                }
            }
            
        }
    }
}
