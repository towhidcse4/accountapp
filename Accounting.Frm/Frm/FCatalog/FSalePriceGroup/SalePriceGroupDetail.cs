﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;


namespace Accounting
{
    public partial class SalePriceGroupDetail : DialogForm
    {
        #region khai báo
        private readonly ISalePriceGroupService _ISalePriceGroupService;

        SalePriceGroup _Select = new SalePriceGroup();

        bool Them = true;
        public static bool isClose = true;
        #endregion

        #region khởi tạo
        public SalePriceGroupDetail()
        {//Thêm
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            this.Text = "Thêm mới Nhóm giá bán";
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.CenterToParent();
            chkIsActive.Visible = false;
            #endregion

            #region Thiết lập ban đầu cho Form
            //Khai báo các webservices
            _ISalePriceGroupService = IoC.Resolve<ISalePriceGroupService>();

            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion
        }

        public SalePriceGroupDetail(SalePriceGroup temp)
        {//Sửa
            #region Khởi tạo giá trị mặc định của Form

            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.CenterToParent();
            #endregion
            this.Text = "Sửa Nhóm giá bán";
            #region Thiết lập ban đầu cho Form
            _Select = temp;
            Them = false;
            txtSalePriceGroupCode.Enabled = false;

            //Khai báo các webservices
            _ISalePriceGroupService = IoC.Resolve<ISalePriceGroupService>();

            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion

            #region Fill dữ liệu Obj vào control
            ObjandGUI(temp, false);
            #endregion
        }

        private void InitializeGUI()
        {
            //
        }
        #endregion

        #region Button Event
        /// <summary>
        /// Sự kiện nút Lưu lại
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click_1(object sender, EventArgs e)
        {
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion

            //Gui to object
            #region Fill dữ liệu control vào obj
            SalePriceGroup temp = Them ? new SalePriceGroup() : _ISalePriceGroupService.Getbykey(_Select.ID);
            temp = ObjandGUI(temp, true);
            #endregion

            #region Thao tác CSDL
            _ISalePriceGroupService.BeginTran();
            // bắt đầu thực hiên 1 transaction
            if (Them) _ISalePriceGroupService.CreateNew(temp);
            else _ISalePriceGroupService.Update(temp);
            // loc điều kiện
            _ISalePriceGroupService.CommitTran();
            // thực thi transaction 
            #endregion

            #region xử lý form, kết thúc form
            this.Close();
            isClose = false;
            #endregion
        }
        /// <summary>
        /// Sự kiện nút đóng
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose1_Click(object sender, EventArgs e)
        {
            isClose = true;
            this.Close();
            
        }
        #endregion

        #region Utils
        /// <summary>
        /// get hoặc set một đối tượng vào giao diện xử lý
        /// </summary>
        /// <param name="input">đối tượng</param>
        /// <param name="isGet">True: get giá trị từ control gán vào đối tượng| False: set giá trị đối tượng vào control</param>
        /// <returns></returns>
        SalePriceGroup ObjandGUI(SalePriceGroup input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();  //Thêm mới

                input.SalePriceGroupCode = txtSalePriceGroupCode.Text;
                input.SalePriceGroupName = txtSalePriceGroupName.Text;
                input.Description = txtDescription.Text;
                input.IsActive = chkIsActive.Checked;
            }
            else
            {
                txtSalePriceGroupCode.Value = input.SalePriceGroupCode;
                txtSalePriceGroupName.Value = input.SalePriceGroupName;
                txtDescription.Value = input.Description;
                chkIsActive.Checked = input.IsActive;
            }
            return input;
        }
        // hàm check 
        bool CheckError()
        {
            bool kq = true;
            //Check Error Chung

            //Check Error Riêng từng Form
            if (string.IsNullOrEmpty(txtSalePriceGroupCode.Text) || string.IsNullOrEmpty(txtSalePriceGroupName.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }

            //List<string> list = _ISalePriceGroupService.Query.Select(p => p.SalePriceGroupCode).ToList();
            List<string> list = _ISalePriceGroupService.GetSalePriceGroupCode();    //Thành Duy đã sửa chỗ này
            foreach (var item in list)
            {
                if (item == txtSalePriceGroupCode.Text && Them)
                {
                    MSG.Warning(string.Format(resSystem.MSG_Catalog, "Mã", item));
                    return false;
                }
            }

            return kq;
        }
        #endregion

        private void SalePriceGroupDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
