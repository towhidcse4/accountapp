﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using Accounting.TextMessage;

namespace Accounting
{
    public partial class FSalePriceGroup : CatalogBase //UserControl
    {
        #region khai báo
        private readonly ISalePriceGroupService _ISalePriceGroupService;
       // protected UltraGridRow currentActiveRow;
        #endregion

        #region khởi tạo
        public FSalePriceGroup()
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            #endregion

            #region Thiết lập ban đầu cho Form
            _ISalePriceGroupService = IoC.Resolve<ISalePriceGroupService>();
            this.uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
           
            #endregion

            #region Tạo dữ liệu ban đầu
            LoadDuLieu();
            System.Windows.Forms.ToolTip ToolTip1 = new System.Windows.Forms.ToolTip();
            ToolTip1.SetToolTip(btnAdd, "Thêm (Ctrl + N)");
            System.Windows.Forms.ToolTip ToolTip2 = new System.Windows.Forms.ToolTip();
            ToolTip2.SetToolTip(btnEdit, "Xem (Ctrl + E)");
            System.Windows.Forms.ToolTip ToolTip3 = new System.Windows.Forms.ToolTip();
            ToolTip3.SetToolTip(btnDelete, "Xóa (Ctrl + D)");
            if (uGrid.Rows.Count > 0) uGrid.Rows[0].Selected = true;
            #endregion
        }

        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }
        private void LoadDuLieu(bool configGrid)
        {
            WaitingFrm.StartWaiting();
            #region Lấy dữ liệu từ CSDL
            List<SalePriceGroup> listSP = _ISalePriceGroupService.GetAll_OrderBy();
            _ISalePriceGroupService.UnbindSession(listSP);
            listSP = _ISalePriceGroupService.GetAll_OrderBy();
            #endregion

            #region Xử lý dữ liệu
            #endregion

            #region hiển thị và xử lý hiển thị
           
            uGrid.DataSource = listSP.ToArray();
            if ((uGrid.Selected.Rows.Count == 0) && (listSP.Count > 0))
            {
                uGrid.Rows[0].Selected = true;
            }
            if (configGrid) ConfigGrid(uGrid);
            #endregion
            WaitingFrm.StopWaiting();
        }
        #endregion

        #region Nghiệp vụ
        #region ContentMenu

        #endregion

        #region Button
        
        private void btnDelete_Click_1(object sender, EventArgs e)
        {
            deleteFunction();
        }

        private void btnEdit_Click_1(object sender, EventArgs e)
        {

            EditFunction();
        }

        private void btnAdd_Click_1(object sender, EventArgs e)
        {
            AddFunction();
        }
     
        #endregion

        private void tsmAdd_Click_1(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void tsmEdit_Click_1(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tsmDelete_Click_1(object sender, EventArgs e)
        {
            deleteFunction();
        }

        private void tmsReLoad_Click_1(object sender, EventArgs e)
        {
            LoadDuLieu();
        }

        private void uGrid_DoubleClick(object sender, EventArgs e)
        {
          
            EditFunction();
        }

        private void uGrid_MouseDown_1(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }
        /// <summary>
        /// Nghiệp vụ Add
        /// </summary>
        protected override void AddFunction()
        {
           new SalePriceGroupDetail().ShowDialog(this);
           if (!SalePriceGroupDetail.isClose)
           LoadDuLieu();
        }

        /// <summary>
        /// Nghiệp vụ Edit
        /// </summary>
        protected override void EditFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                SalePriceGroup temp = uGrid.Selected.Rows[0].ListObject as SalePriceGroup;
                new SalePriceGroupDetail(temp).ShowDialog(this);
                if (!SalePriceGroupDetail.isClose) 
                LoadDuLieu();
            }
            else
                Accounting.TextMessage.MSG.Error(string.Format(Accounting.TextMessage.resSystem.MSG_Catalog3,"một Nhóm giá bán"));
          }

        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        void deleteFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                SalePriceGroup temp = uGrid.Selected.Rows[0].ListObject as SalePriceGroup;
                if (MSG.Question(string.Format(resSystem.MSG_System_05, temp.SalePriceGroupCode)) == System.Windows.Forms.DialogResult.Yes)
                {
                    _ISalePriceGroupService.BeginTran();
                    _ISalePriceGroupService.Delete(temp);
                    _ISalePriceGroupService.CommitTran();
                    LoadDuLieu();
                }
            }
            else
                MSG.Error(string.Format(resSystem.MSG_Catalog2,"một Nhóm giá bán"));
            
        }
        #endregion

        #region Utils
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, TextMessage.ConstDatabase.SalePriceGroup_TableName);
        }
        #endregion

        private void ultraButton1_Click(object sender, EventArgs e)
        {
            new TestLoadImg().ShowDialog(this);
        }

        private void FSalePriceGroup_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGrid.ResetText();
            uGrid.ResetUpdateMode();
            uGrid.ResetExitEditModeOnLeave();
            uGrid.ResetRowUpdateCancelAction();
            uGrid.DataSource = null;
            uGrid.Layouts.Clear();
            uGrid.ResetLayouts();
            uGrid.ResetDisplayLayout();
            uGrid.Refresh();
            uGrid.ClearUndoHistory();
            uGrid.ClearXsdConstraints();
        }

        private void FSalePriceGroup_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
