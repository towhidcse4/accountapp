﻿namespace Accounting
{
    partial class SalePriceGroupDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            this.txtSalePriceGroupName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblSalePriceGroupName = new Infragistics.Win.Misc.UltraLabel();
            this.lblDescription = new Infragistics.Win.Misc.UltraLabel();
            this.txtSalePriceGroupCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtDescription = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblSalePriceGroupCode = new Infragistics.Win.Misc.UltraLabel();
            this.chkIsActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.btnClose1 = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.txtSalePriceGroupName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSalePriceGroupCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).BeginInit();
            this.SuspendLayout();
            // 
            // txtSalePriceGroupName
            // 
            this.txtSalePriceGroupName.AutoSize = false;
            this.txtSalePriceGroupName.Location = new System.Drawing.Point(134, 55);
            this.txtSalePriceGroupName.Name = "txtSalePriceGroupName";
            this.txtSalePriceGroupName.Size = new System.Drawing.Size(237, 22);
            this.txtSalePriceGroupName.TabIndex = 23;
            // 
            // lblSalePriceGroupName
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.TextVAlignAsString = "Middle";
            this.lblSalePriceGroupName.Appearance = appearance1;
            this.lblSalePriceGroupName.Location = new System.Drawing.Point(9, 55);
            this.lblSalePriceGroupName.Name = "lblSalePriceGroupName";
            this.lblSalePriceGroupName.Size = new System.Drawing.Size(126, 22);
            this.lblSalePriceGroupName.TabIndex = 22;
            this.lblSalePriceGroupName.Text = "Tên nhóm giá bán(*)";
            // 
            // lblDescription
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.TextVAlignAsString = "Middle";
            this.lblDescription.Appearance = appearance2;
            this.lblDescription.Location = new System.Drawing.Point(9, 83);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(92, 22);
            this.lblDescription.TabIndex = 16;
            this.lblDescription.Text = "Diễn giải";
            // 
            // txtSalePriceGroupCode
            // 
            this.txtSalePriceGroupCode.AutoSize = false;
            this.txtSalePriceGroupCode.Location = new System.Drawing.Point(134, 27);
            this.txtSalePriceGroupCode.Name = "txtSalePriceGroupCode";
            this.txtSalePriceGroupCode.Size = new System.Drawing.Size(237, 22);
            this.txtSalePriceGroupCode.TabIndex = 5;
            // 
            // ultraGroupBox1
            // 
            appearance3.FontData.BoldAsString = "True";
            this.ultraGroupBox1.Appearance = appearance3;
            this.ultraGroupBox1.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox1.Controls.Add(this.txtDescription);
            this.ultraGroupBox1.Controls.Add(this.txtSalePriceGroupName);
            this.ultraGroupBox1.Controls.Add(this.lblSalePriceGroupName);
            this.ultraGroupBox1.Controls.Add(this.lblDescription);
            this.ultraGroupBox1.Controls.Add(this.txtSalePriceGroupCode);
            this.ultraGroupBox1.Controls.Add(this.lblSalePriceGroupCode);
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(-1, 3);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(377, 168);
            this.ultraGroupBox1.TabIndex = 30;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtDescription
            // 
            this.txtDescription.AutoSize = false;
            this.txtDescription.Location = new System.Drawing.Point(134, 83);
            this.txtDescription.MaxLength = 512;
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(237, 71);
            this.txtDescription.TabIndex = 30;
            // 
            // lblSalePriceGroupCode
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.lblSalePriceGroupCode.Appearance = appearance4;
            this.lblSalePriceGroupCode.Location = new System.Drawing.Point(9, 27);
            this.lblSalePriceGroupCode.Name = "lblSalePriceGroupCode";
            this.lblSalePriceGroupCode.Size = new System.Drawing.Size(126, 22);
            this.lblSalePriceGroupCode.TabIndex = 0;
            this.lblSalePriceGroupCode.Text = "Mã nhóm giá bán (*)";
            // 
            // chkIsActive
            // 
            appearance5.TextVAlignAsString = "Middle";
            this.chkIsActive.Appearance = appearance5;
            this.chkIsActive.BackColor = System.Drawing.Color.Transparent;
            this.chkIsActive.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsActive.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkIsActive.Checked = true;
            this.chkIsActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIsActive.Location = new System.Drawing.Point(9, 181);
            this.chkIsActive.Name = "chkIsActive";
            this.chkIsActive.Size = new System.Drawing.Size(103, 22);
            this.chkIsActive.TabIndex = 29;
            this.chkIsActive.Text = "Hoạt động";
            // 
            // btnSave
            // 
            appearance6.Image = global::Accounting.Properties.Resources.apply_32;
            this.btnSave.Appearance = appearance6;
            this.btnSave.Location = new System.Drawing.Point(207, 177);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 27;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click_1);
            // 
            // btnClose1
            // 
            appearance7.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose1.Appearance = appearance7;
            this.btnClose1.Location = new System.Drawing.Point(296, 177);
            this.btnClose1.Name = "btnClose1";
            this.btnClose1.Size = new System.Drawing.Size(75, 30);
            this.btnClose1.TabIndex = 28;
            this.btnClose1.Text = "Đóng";
            this.btnClose1.Click += new System.EventHandler(this.btnClose1_Click);
            // 
            // SalePriceGroupDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(376, 219);
            this.Controls.Add(this.ultraGroupBox1);
            this.Controls.Add(this.chkIsActive);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnClose1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SalePriceGroupDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SalePriceGroupDetail";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.SalePriceGroupDetail_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.txtSalePriceGroupName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSalePriceGroupCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSalePriceGroupName;
        private Infragistics.Win.Misc.UltraLabel lblSalePriceGroupName;
        private Infragistics.Win.Misc.UltraLabel lblDescription;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSalePriceGroupCode;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraLabel lblSalePriceGroupCode;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsActive;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraButton btnClose1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDescription;
    }
}