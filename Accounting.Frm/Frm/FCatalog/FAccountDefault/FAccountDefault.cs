﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using Accounting.TextMessage;

namespace Accounting
{
    public partial class FAccountDefault : CatalogBase //UserControl
    {
        #region khai báo
        private readonly IAccountDefaultService _IAccountDefaultService;
        #endregion

        #region khởi tạo
        public FAccountDefault()
        {
            InitializeComponent();
            _IAccountDefaultService = IoC.Resolve<IAccountDefaultService>();
            this.uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            LoadDuLieu();
            System.Windows.Forms.ToolTip ToolTip2 = new System.Windows.Forms.ToolTip();
            ToolTip2.SetToolTip(btnEdit, "Xem (Ctrl + E)");
            if (uGrid.Rows.Count > 0) uGrid.Rows[0].Selected = true;
        }
        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }
        private void LoadDuLieu(bool configGrid)
        {
            WaitingFrm.StartWaiting();
            //Load dữ liệu
            List<AccountDefault_Object> list = _IAccountDefaultService.GetAll_Sort();
            _IAccountDefaultService.UnbindSession(list);
            list = _IAccountDefaultService.GetAll_Sort();
            //thiết lập tính chất
            uGrid.DataSource = list;
            if (configGrid) {
                ConfigGrid(uGrid);
                this.ConfigCbbToGrid(uGrid, "TypeID", Utils.ListType, "ID", "TypeName", ConstDatabase.Type_TableName);
                uGrid.DisplayLayout.Bands[0].Columns["TypeID"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownValidate;
                uGrid.DisplayLayout.Bands[0].Columns["TypeID"].CellActivation = Activation.NoEdit;
            }
            WaitingFrm.StopWaiting();
        }
        #endregion

        #region nghiệp vụ
        private void gridAccountGroups_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            
            AccountDefault_Object temp = new AccountDefault_Object();
            temp = e.Row.ListObject as AccountDefault_Object;
            new FAccountDefaultDetail(temp).ShowDialog(this);
            if (!FAccountDefaultDetail.isClose) LoadDuLieu();
        }
        #endregion

        #region Utils
        void ConfigGrid(UltraGrid utralGrid)
        {
            utralGrid.Text = string.Empty;
            utralGrid.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.RowSelect;
            utralGrid.DisplayLayout.Bands[0].Override.SelectedRowAppearance.BackColor = Color.FromArgb(255, 192, 128);
            utralGrid.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            Utils.ConfigGrid(uGrid, TextMessage.ConstDatabase.AccountDefault_TableName, false);
            utralGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
        }
        #endregion

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }
        protected override void EditFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                AccountDefault_Object temp = uGrid.Selected.Rows[0].ListObject as AccountDefault_Object;
                //Investor Node = _IInvestorService.Getbykey((Guid)temp.ID);
                //ultraLabel1.Text = "Nhà đầu tư\t " + Node.InvestorCode;

                new FAccountDefaultDetail(temp).ShowDialog(this);
                if (!FAccountDefaultDetail.isClose) LoadDuLieu();
            }
            else
                Accounting.TextMessage.MSG.Error(string.Format(resSystem.MSG_Catalog3, "một Tài khoản ngầm định"));
        }
        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            ResetFunction();
        }
        protected override void ResetFunction()
        {
            LoadDuLieu(false);
        }

        private void tsmEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tmsReLoad_Click_1(object sender, EventArgs e)
        {
            LoadDuLieu();
        }

        private void FAccountDefault_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGrid.ResetText();
            uGrid.ResetUpdateMode();
            uGrid.ResetExitEditModeOnLeave();
            uGrid.ResetRowUpdateCancelAction();
            uGrid.DataSource = null;
            uGrid.Layouts.Clear();
            uGrid.ResetLayouts();
            uGrid.ResetDisplayLayout();
            uGrid.Refresh();
            uGrid.ClearUndoHistory();
            uGrid.ClearXsdConstraints();
        }

        private void FAccountDefault_FormClosed(object sender, System.Windows.Forms.FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
