﻿using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FAccountDefault_AccountDetail : DialogForm
    {
        #region khai báo
        private readonly IAccountService _IAccountService;
        List<AccountDefault_Account_Object> listAccountCheck = new List<AccountDefault_Account_Object>();
        public AccountDefault_Object _select = new AccountDefault_Object();
        public bool isClose = true;
        #endregion
        #region khởi tạo
        public FAccountDefault_AccountDetail()
        {
            InitializeComponent();
            _IAccountService = IoC.Resolve<IAccountService>();
            LoadDuLieu();
        }
        public FAccountDefault_AccountDetail(AccountDefault_Object obj, string temp)
        {
            _select = obj;
            //vitriRow1 = vitriRow;
            InitializeComponent();
            _IAccountService = IoC.Resolve<IAccountService>();
            listAccountCheck = _IAccountService.GetChildrenAccountWithCheck(temp);
            LoadDuLieu();
        }
        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }
        private void LoadDuLieu(bool configGrid)
        {
            #region Xử lý dữ liệu
            //thiết lập tính chấtp
            foreach (var item in listAccountCheck) item.AccountGroupKindView = Utils.DicAccountKind[item.AccountGroupKind.ToString()].Name;
            #endregion

            #region hiển thị và xử lý hiển thị
            gridAccountDetail.DataSource = listAccountCheck;
            if (configGrid) ConfigGrid(gridAccountDetail);
            #endregion
        }
        #endregion
        #region Utils
        void ConfigGrid(UltraGrid grid)
        {
            //Utils.ConfigGrid(gridAccountDetail, TextMessage.ConstDatabase.Account_AccountDefault_TableName,false);
            Utils.ConfigGrid(gridAccountDetail, "", DanhSach());
            grid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            grid.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
            grid.DisplayLayout.Bands[0].Columns["Check"].CellActivation = Activation.AllowEdit;
            grid.DisplayLayout.Bands[0].Columns["Check"].Style = ColumnStyle.CheckBox;
            grid.DisplayLayout.Bands[0].Columns["Check"].Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            grid.DisplayLayout.Bands[0].Override.HeaderAppearance.TextHAlign = Infragistics.Win.HAlign.Center;
            UltraGridBand band = grid.DisplayLayout.Bands[0];
            foreach (UltraGridColumn column in band.Columns)
            {
                if (column.Key == "AccountNumber")
                {
                    column.CellActivation = Activation.NoEdit;
                }
                if (column.Key == "AccountName")
                {
                    column.CellActivation = Activation.NoEdit;
                }
                if (column.Key == "Grade")
                {
                    column.CellActivation = Activation.NoEdit;
                }
            }
        }
        private List<TemplateColumn> DanhSach()
        {
            return new List<TemplateColumn>{
                new TemplateColumn{
                    ColumnName = "Check",
                    ColumnWidth = 50,
                    IsVisible = true,
                    IsVisibleCbb = true,
                    VisiblePosition = 1
                },
                new TemplateColumn{
                    ColumnName = "AccountNumber",
                    ColumnCaption = "Số tài khoản",
                    ColumnWidth = 100,
                    IsVisible = true,
                    IsVisibleCbb = true,
                    VisiblePosition =2
                },
                 new TemplateColumn{
                    ColumnName = "AccountName",
                    ColumnCaption = "Tên tài khoản",
                    ColumnWidth = 250,
                    IsVisible = true,
                    IsVisibleCbb = true,
                    VisiblePosition =3
                },
                 new TemplateColumn{
                    ColumnName = "Grade",
                    ColumnCaption = "Bậc tài khoản",
                    ColumnWidth = 80,
                    IsVisible = true,
                    IsVisibleCbb = true,
                    VisiblePosition =4
                },
            };
        }
        public string ObjAndGUI()
        {
            string temp = "";
            StringBuilder temp1 = new StringBuilder();
            string kytu = "";
            foreach (UltraGridRow row in gridAccountDetail.Rows)
            {
                if ((bool)row.Cells["Check"].Value == true)
                {
                    temp1.Append(kytu).Append(row.Cells["AccountNumber"].Value).ToString();
                    kytu = ";";
                }
            }
            temp = temp1.ToString();
            return temp;
        }
        #endregion
        #region button event
        private void btnClose_Click(object sender, EventArgs e)
        {
            isClose = true;
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            isClose = false;
            this.Close();
        }
        #endregion
    }
}
