﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;

namespace Accounting
{
    public partial class FAccountDefaultDetail : DialogForm //UserControl
    {
        #region khai báo
        private readonly IAccountDefaultService _IAccountDefaultService;
        private readonly IAccountService _IAccountService;
        AccountDefault_Object _Select = new AccountDefault_Object();
        public static bool isClose = true;
        UltraCombo ulc = new UltraCombo();
        bool CheckErr = false;
        bool CheckErr1 = false;
        int vitriRow;
        private static readonly int[] TypeTSCDNotBuy = { 510, 520, 540 };
        #endregion

        #region Khởi tạo
        public FAccountDefaultDetail()
        {

            InitializeComponent();
            _IAccountDefaultService = IoC.Resolve<IAccountDefaultService>();
            InitializeGUI();
        }

        public FAccountDefaultDetail(AccountDefault_Object temp)
        {

            InitializeComponent();
            _Select = Utils.CloneObject(temp);
            _IAccountDefaultService = IoC.Resolve<IAccountDefaultService>();
            _IAccountService = IoC.Resolve<IAccountService>();
            LoadDuLieu(true, _Select);
        }

        private void LoadDuLieu(bool configGrid, AccountDefault_Object accountDefault_Object)
        {
            gridChungTu.DataSource = accountDefault_Object.AccountDefaults;

            txtChungTu.Text = accountDefault_Object.TypeName;
            if ((txtChungTu.Text.Trim().Contains("TSCĐ") || txtChungTu.Text.Trim().ToLower().Contains("mua hàng")) && (!TypeTSCDNotBuy.Any(p => p.Equals(accountDefault_Object.TypeID))))
            {
                if (!gridChungTu.DisplayLayout.Bands[0].Columns.Exists("Buy"))
                {
                    gridChungTu.DisplayLayout.Bands[0].Columns.Add("Buy");
                }
            }
            if (configGrid) ConfigGrid(gridChungTu);
            InitializeGUI();

        }
        /// <summary>
        /// Khởi tạo giá trị ban đầu cho control
        /// </summary>
        private void InitializeGUI()
        {
            foreach (var column in gridChungTu.DisplayLayout.Bands[0].Columns)
            {
                if ((column.Key.HasProperty("Buy")) && (column.Key.Equals("Buy") && (txtChungTu.Text.Trim().Contains("TSCĐ") || txtChungTu.Text.Trim().ToLower().Contains("mua hàng"))))
                {
                    UltraComboEditor ulcTSCD = new UltraComboEditor();
                    //String[] ChucNang = { "Mua trong nước", "Mua nhập khẩu" };
                    ulcTSCD.Items.Add("Mua trong nước");// ChucNang;
                    ulcTSCD.Items.Add("Mua nhập khẩu");
                    column.EditorComponent = ulcTSCD;
                    column.Style = ColumnStyle.DropDownList;
                }
            }
            foreach (UltraGridRow row in gridChungTu.Rows.GetRowEnumerator(GridRowType.DataRow, null, null))
            {
                ulc = new UltraCombo();
                string temp = row.Cells["FilterAccount"].Value.ToString();
                List<Account> listAcc = _IAccountService.GetChildrenAccount(temp);
                ulc.DataSource = listAcc;
                row.Cells["DefaultAccount"].ValueList = ulc;
                ulc.ValueMember = "AccountNumber";
                Utils.ConfigGrid(ulc, TextMessage.ConstDatabase.Account_AccountDefault_TableName);
                //ulc.DropDownWidth = 305;
                UltraGridBand band = ulc.DisplayLayout.Bands[0];
                foreach (UltraGridColumn column in band.Columns)
                {
                    if (column.Key == "AccountNumber")
                    {
                        column.Width = 80;
                        column.MaxWidth = 80;
                    }
                    if (column.Key == "AccountName")
                    {
                        column.Width = 220;
                        column.MaxWidth = 220;
                    }
                }
                if ((!TypeTSCDNotBuy.Any(p => p.Equals(_Select.TypeID))) && (txtChungTu.Text.Trim().Contains("TSCĐ") || txtChungTu.Text.Trim().ToLower().Contains("mua hàng")))
                {
                    foreach (var item in _Select.AccountDefaults)
                    {
                        if (item.PPType == false && item.ID == (Guid)row.Cells["ID"].Value)
                        {
                            row.Cells["Buy"].Value = "Mua trong nước";
                            break;
                        }
                        else if (item.PPType == true && item.ID == (Guid)row.Cells["ID"].Value)
                        {
                            row.Cells["Buy"].Value = "Mua nhập khẩu";
                            break;
                        }
                    }
                    //UltraComboEditor ulcTSCD = new UltraComboEditor();
                    //String[] ChucNang = { "Mua trong nước", "Mua nhập khẩu" };
                    //ulcTSCD.DataSource = ChucNang;
                    //row.Cells["Buy"].EditorComponent = ulcTSCD;
                    //row.Cells["Buy"].
                    //@column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
                    //ulcTSCD.DropDownWidth = 0;
                    //ulcTSCD.DisplayLayout.AutoFitStyle = AutoFitStyle.ExtendLastColumn;
                    //ulcTSCD.DisplayLayout.Bands[0].ColHeadersVisible = false;
                }


            }

        }
        #endregion

        #region Button Event
        /// <summary>
        /// Sự kiện nút Lưu lại
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {

            foreach (UltraGridRow row in gridChungTu.Rows.GetRowEnumerator(GridRowType.DataRow, null, null))
            {
                if (txtChungTu.Text.Trim().Contains("TSCĐ") || txtChungTu.Text.Trim().ToLower().Contains("mua hàng"))
                {
                    if (row.Cells["Buy"].Value.ToString().Equals("Mua trong nước"))
                    {
                        _Select.AccountDefaults.ElementAt(row.Index).PPType = false;
                    }
                    else
                    {
                        _Select.AccountDefaults.ElementAt(row.Index).PPType = true;
                    }
                }
                if (CheckErr || CheckErr1)
                {
                    TextMessage.MSG.MessageBoxStand(string.Format(resSystem.MSG_Catalog4, "Tài khoản"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                }
                else
                {
                    AccountDefault acc = new AccountDefault();
                    acc = _IAccountDefaultService.Getbykey(_Select.AccountDefaults.ElementAt(row.Index).ID);
                    acc.FilterAccount = gridChungTu.DisplayLayout.Rows[row.Index].Cells["FilterAccount"].Value.ToString();
                    if (gridChungTu.DisplayLayout.Rows[row.Index].Cells["DefaultAccount"].Value != null)
                    {
                        acc.DefaultAccount = gridChungTu.DisplayLayout.Rows[row.Index].Cells["DefaultAccount"].Value.ToString();
                    }
                    else
                    {
                        acc.DefaultAccount = null;
                    }
                    acc.PPType = _Select.AccountDefaults.ElementAt(row.Index).PPType;
                    _IAccountDefaultService.BeginTran();
                    _IAccountDefaultService.Update(acc);
                    _IAccountDefaultService.CommitTran();
                    Utils.ClearCacheByType<AccountDefault>();
                    isClose = false;
                    this.Close();
                }
            }
        }

        /// <summary>
        /// Sự kiện nút đóng
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            isClose = true;
            DialogResult = DialogResult.Cancel;
            this.Close();
        }
        /// <summary>
        /// Sự kiện nút trong cell
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void btn_EditorButtonClick(object sender, EditorButtonEventArgs e)
        {
            vitriRow = gridChungTu.ActiveRow.Index;
            string temp = gridChungTu.Rows[vitriRow].Cells["FilterAccount"].Value.ToString();
            FAccountDefault_AccountDetail frm = new FAccountDefault_AccountDetail(_Select, temp);
            frm.ShowDialog(this);
            if (!frm.isClose)
            {
                _Select = frm._select;
                gridChungTu.Rows[vitriRow].Cells["FilterAccount"].Value = frm.ObjAndGUI();
                LoadDuLieu(true, _Select);
            }
        }
        #endregion

        #region Utils
        //<summary>
        //get hoặc set một đối tượng vào giao diện xử lý
        //</summary>
        //<param name="input">đối tượng AccountGroup</param>
        //<param name="isGet">True: get giá trị từ control gán vào đối tượng| False: set giá trị đối tượng vào control</param>
        //<returns></returns>
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid grid)
        {
            grid.DisplayLayout.Override.ActiveRowAppearance.ForeColor = Utils.DefaultRowSelectedColor;
            grid.DisplayLayout.Override.SelectedRowAppearance.ForeColor = Utils.DefaultRowSelectedColor;
            UltraTextEditor txtEditor = new UltraTextEditor();
            EditorButton btn = new EditorButton();
            btn.Appearance.Image = Properties.Resources.edit_validated_icon;
            txtEditor.ButtonsRight.Add(btn);
            txtEditor.EditorButtonClick += btn_EditorButtonClick;
            grid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            grid.DisplayLayout.Bands[0].Override.HeaderAppearance.TextHAlign = Infragistics.Win.HAlign.Center;
            grid.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            grid.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.ExtendFirstCell;
            UltraGridBand band = grid.DisplayLayout.Bands[0];
            foreach (UltraGridColumn column in band.Columns)
            {
                if (column.Key == "ID")
                {
                    column.Hidden = true;
                }
                if (column.Key == "TypeID")
                {
                    column.Hidden = true;
                }
                if (column.Key == "ColumnName")
                {
                    column.Header.Caption = "Tên cột";
                    column.CellActivation = Activation.NoEdit;
                }
                if (column.Key == "ReduceAccount")
                {
                    column.Hidden = true;
                }
                if (column.Key == "PPType")
                {
                    column.Hidden = true;
                }
                if (column.Key == "OrderPriority")
                {
                    column.Hidden = true;
                }
                if (column.Key == "ColumnCaption")
                {
                    column.Hidden = true;
                }
                if (column.Key == "FilterAccount")
                {
                    column.Header.Caption = "Lọc tài khoản";

                    column.CellActivation = Activation.AllowEdit;

                    column.EditorComponent = txtEditor;
                }
                if (column.Key == "DefaultAccount")
                {
                    column.Header.Caption = "Tài khoản mặc định";
                    column.CellActivation = Activation.AllowEdit;
                }
                if (column.Key == "Buy")
                {
                    column.Header.Caption = "Mua hàng";
                    column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
                }
            }
            foreach (UltraGridRow row in gridChungTu.Rows.GetRowEnumerator(GridRowType.DataRow, null, null))
            {
                string Name = row.Cells["ColumnName"].Value.ToString();
                switch (Name)
                {
                    case "CreditAccount":
                        row.Cells["ColumnName"].Value = "TK Có";
                        break;
                    case "COGSAccount":
                        row.Cells["ColumnName"].Value = "TK Giá vốn";
                        break;
                    case "DebitAccount":
                        row.Cells["ColumnName"].Value = "TK Nợ";
                        break;
                    case "DeductionDebitAccount":
                        row.Cells["ColumnName"].Value = "TK đối ứng thuế GTGT";
                        break;
                    case "DiscountAccount":
                        row.Cells["ColumnName"].Value = "TK Chiết khấu";
                        break;
                    case "ImportTaxAccount":
                        row.Cells["ColumnName"].Value = "TK thuế nhập khẩu";
                        break;
                    case "InventoryAccount":
                        row.Cells["ColumnName"].Value = "TK Kho";
                        break;
                    case "SpecialConsumeTaxAccount":
                        row.Cells["ColumnName"].Value = "TK thuế TTĐB";
                        break;
                    case "VATAccount":
                        row.Cells["ColumnName"].Value = "TK thuế GTGT";
                        break;
                    case "CostAccount":
                        row.Cells["ColumnName"].Value = "TK giá vốn";
                        break;
                    case "ExportTaxAccount":
                        row.Cells["ColumnName"].Value = "TK thuế XK";
                        break;
                    case "RepositoryAccount":
                        row.Cells["ColumnName"].Value = "TK kho";
                        break;
                }
            }
        }

        //private void ValidateDataRow(DataRow row, DataColumn column, object value)
        private void ValidateDataRow(UltraGridCell cell, String value)
        {
            switch (cell.Column.Key)
            //switch (column.ColumnName)
            {
                case "DefaultAccount":
                    {
                        //row.SetColumnError(column, "");
                        Utils.RemoveNotificationCell(gridChungTu, cell);
                        string temp = gridChungTu.Rows[gridChungTu.ActiveRow.Index].Cells["FilterAccount"].Value.ToString();
                        string[] temp1 = value.ToString().Split(';');
                        List<Account> listAcc = _IAccountService.GetChildrenAccount(temp);
                        if (value.ToString().Equals(""))
                        {
                            Utils.NotificationCell(gridChungTu, cell, "Bị trống");
                            CheckErr = false;
                        }
                        else if (!temp1.All(c => listAcc.Any(d => d.AccountNumber == c)))
                        {
                            Utils.NotificationCell(gridChungTu, cell, "Không có trong danh sách");
                            CheckErr = true;
                            break;
                        }
                        else
                        {
                            CheckErr = false;
                        }
                        break;
                    }
                case "FilterAccount":
                    {
                        Utils.RemoveNotificationCell(gridChungTu, cell);
                        string[] temp1 = value.Split(';');
                        List<Account> listAcc = _IAccountService.GetAll();

                        foreach (var item in temp1)
                        {
                            if (value.Equals(""))
                            {
                                Utils.NotificationCell(gridChungTu, cell, "Bị trống");
                                CheckErr1 = false;
                            }
                            else if (!listAcc.Any(d => d.AccountNumber == item))
                            {
                                Utils.NotificationCell(gridChungTu, cell, "Không có trong danh sách");
                                CheckErr1 = true;
                                break;
                            }
                            else
                            {
                                CheckErr1 = false;
                            }
                        }
                        break;
                    }
            }
        }


        private void gridChungTu_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            e.Layout.Override.SupportDataErrorInfo = SupportDataErrorInfo.RowsAndCells;
            e.Layout.Override.DataErrorCellAppearance.ForeColor = Color.Red;
            e.Layout.Override.DataErrorRowAppearance.BackColor = Color.LightYellow;
            e.Layout.Override.DataErrorRowSelectorAppearance.BackColor = Color.Green;
            e.Layout.Override.RowSelectorWidth = 32;
        }
        #endregion


        private void gridChungTu_AfterCellUpdate(object sender, CellEventArgs e)
        {
            ValidateDataRow(e.Cell, e.Cell.Value.ToString());
            foreach (UltraGridRow row in gridChungTu.Rows.GetRowEnumerator(GridRowType.DataRow, null, null))
            {
                ulc = new UltraCombo();
                string temp = row.Cells["FilterAccount"].Value.ToString();
                List<Account> listAcc = _IAccountService.GetChildrenAccount(temp);
                ulc.DataSource = listAcc;
                row.Cells["DefaultAccount"].ValueList = ulc;
                ulc.ValueMember = "AccountNumber";
                Utils.ConfigGrid(ulc, TextMessage.ConstDatabase.Account_AccountDefault_TableName);
                ulc.DropDownWidth = 305;
                ulc.DisplayLayout.AutoFitStyle = AutoFitStyle.ExtendLastColumn;
                UltraGridBand band = ulc.DisplayLayout.Bands[0];
                foreach (UltraGridColumn column in band.Columns)
                {
                    if (column.Key == "AccountNumber")
                    {
                        column.Width = 80;
                        column.MaxWidth = 80;
                    }
                    if (column.Key == "AccountName")
                    {
                        column.Width = 220;
                        column.MaxWidth = 220;
                    }
                }
            }
        }
    }

}
