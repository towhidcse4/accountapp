﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using Accounting.TextMessage;

namespace Accounting
{
    public partial class FFAInit : CatalogBase
    {
        #region khai báo

        private readonly IFAInitService _IFAInitService = Utils.IFAInitService;
        private readonly IOPFixedAssetService _IOPFixedAssetService = Utils.IOPFixedAssetService;
        private readonly IFixedAssetLedgerService _IFixedAssetLedgerService = Utils.IFixedAssetLedgerService;
        private readonly IFixedAssetService _IFixedAssetService = Utils.IFixedAssetService;
        private readonly IFADepreciationDetailService _IFADepreciationDetailService = Utils.IFADepreciationDetailService;
        private readonly IOPAccountService _IOPAccountService = Utils.IOPAccountService;
        private readonly IGeneralLedgerService _IGeneralLedgerService = Utils.IGeneralLedgerService;
        #endregion

        #region khởi tạo

        public FFAInit()
        {
            #region Khởi tạo giá trị mặc định của Form

            InitializeComponent();

            #endregion

            #region Thiết lập ban đầu cho Form

            uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;

            #endregion

            #region Tạo dữ liệu ban đầu
            
            LoadDuLieu();
            if (uGrid.Rows.Count > 0)
            {
                uGrid.Rows[0].Selected = true;
            }
            #endregion
            ToolTip ToolTip1 = new ToolTip();
            ToolTip1.SetToolTip(btnAdd, "Thêm (Ctrl + N)");
            ToolTip ToolTip2 = new ToolTip();
            ToolTip2.SetToolTip(btnEdit, "Xem (Ctrl + E)");
            ToolTip ToolTip3 = new ToolTip();
            ToolTip3.SetToolTip(btnDelete, "Xóa (Ctrl + D)");
            if (uGrid.Rows.Count > 0) uGrid.Rows[0].Selected = true;
        }

        private void LoadDuLieu()
        {
            LoadDuLieu(true);
        }

        private void LoadDuLieu(bool configGrid)
        {
            WaitingFrm.StartWaiting();
            #region Lấy dữ liệu từ CSDL

            List<FixedAsset> list = _IFixedAssetService.FindByNoIsNull(false);
            _IFixedAssetService.UnbindSession(list);
            list = _IFixedAssetService.FindByNoIsNull(false);
            #endregion

            #region hiển thị và xử lý hiển thị
            foreach(FixedAsset fixedAsset in list)
            {
                fixedAsset.UsedTime2 = fixedAsset.IsDisplayMonth ? fixedAsset.UsedTime : fixedAsset.UsedTime * 12;
                fixedAsset.UsedTimeRemain2 = fixedAsset.IsDisplayMonthRemain ? fixedAsset.UsedTimeRemain : fixedAsset.UsedTimeRemain * 12;
                FAInit fAInit = _IFAInitService.FindByFixedAssetID(fixedAsset.ID);
                if (fAInit != null)
                {
                    fixedAsset.AcDepreciationAmount = fAInit.AcDepreciationAmount;
                    fixedAsset.RemainingAmount = fAInit.RemainingAmount;
                }
            }
            uGrid.DataSource = list.OrderBy(x => x.FixedAssetCode).ToArray();

            if (configGrid) ConfigGrid(uGrid);
            uGrid.DisplayLayout.Bands[0].Columns["OriginalPrice"].FormatNumberic(ConstDatabase.Format_TienVND);
            #endregion
            WaitingFrm.StopWaiting();
        }

        #endregion

        #region Nghiệp vụ

        #region ContentMenu

        private void tsmAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void tsmEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            ResetFunction();
        }
        protected override void ResetFunction()
        {
            LoadDuLieu(false);
        }

        #endregion

        #region Button

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        #endregion

        private void uGrid_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (e.Row.Index >= 0)
            {
                FixedAsset temp = e.Row.ListObject as FixedAsset;
                //if (_IFAInitService.CheckDeleteFAInit(temp.ID))
                //{
                //    MSG.Warning("Không thể sửa thông tin của tài sản này tại đây vì đã phát sinh chứng từ liên quan. Vui lòng thực hiện việc sửa đổi tại nghiệp vụ Điều chỉnh Tài sản cố định");
                //    return;
                //}
                //new FFAInitDetail(temp).ShowDialog(this);
                //if (!FFAInitDetail.isClose) { Utils.ClearCacheByType<FixedAsset>(); LoadDuLieu(); }
                //HUYPD Edit Show Form
                FFAInitDetail frm = new FFAInitDetail(temp);
                frm.FormClosed += new FormClosedEventHandler(FFAInitDetail_FormClosed);
                frm.ShowDialogHD(this);
            }
        }

        /// <summary>
        /// Nghiệp vụ Add
        /// </summary>
        protected override void AddFunction()
        {
            //new FFAInitDetail().ShowDialog(this);
            //if (!FFAInitDetail.isClose) { Utils.ClearCacheByType<FixedAsset>(); LoadDuLieu(); }
            //HUYPD Edit Show Form
            FFAInitDetail frm = new FFAInitDetail();
            frm.FormClosed += new FormClosedEventHandler(FFAInitDetail_FormClosed);
            frm.ShowDialogHD(this);
        }

        /// <summary>
        /// Nghiệp vụ Edit
        /// </summary>
        protected override void EditFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                FixedAsset temp = uGrid.Selected.Rows[0].ListObject as FixedAsset;
                //if (_IFAInitService.CheckDeleteFAInit(temp.ID))
                //{
                //    MSG.Warning("Không thể sửa thông tin của tài sản này tại đây vì đã phát sinh chứng từ liên quan. Vui lòng thực hiện việc sửa đổi tại nghiệp vụ Điều chỉnh Tài sản cố định");
                //    return;
                //}

                //new FFAInitDetail(temp).ShowDialog(this);
                //if (!FFAInitDetail.isClose) { Utils.ClearCacheByType<FixedAsset>(); LoadDuLieu(); }
                //HUYPD Edit Show Form
                FFAInitDetail frm = new FFAInitDetail(temp);
                frm.FormClosed += new FormClosedEventHandler(FFAInitDetail_FormClosed);
                frm.ShowDialogHD(this);
            }
            else
                Accounting.TextMessage.MSG.Error(string.Format(Accounting.TextMessage.resSystem.MSG_Catalog3, "một Tài sản cố định"));
        }
        private void FFAInitDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            Utils.ClearCacheByType<FixedAsset>();
            LoadDuLieu();
        }
        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        protected override void DeleteFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                FixedAsset temp = uGrid.Selected.Rows[0].ListObject as FixedAsset;
                if (_IFAInitService.CheckDeleteFAInit(temp.ID))
                {
                    MSG.Warning("Không thể xóa tài sản này vì tài sản này đã có phát sinh chứng từ liên quan!");
                    return;
                }
                if (MSG.Question(string.Format(resSystem.MSG_System_05, temp.FixedAssetName)) ==
                    System.Windows.Forms.DialogResult.Yes)
                {
                    try
                    {
                        // xóa 4 bảng
                        _IFixedAssetService.BeginTran();
                        _IFixedAssetService.Delete(temp);

                        FAInit tem2 = _IFAInitService.FindByFixedAssetID(temp.ID);
                        _IFAInitService.Delete(tem2);

                        OPFixedAsset tem3 = _IOPFixedAssetService.FindByFixedAssetID(temp.ID);
                        _IOPFixedAssetService.Delete(tem3);

                        FixedAssetLedger temp4 = _IFixedAssetLedgerService.FindByFixedAssetID(temp.ID);
                        _IFixedAssetLedgerService.Delete(temp4);
                        

                        //OPAccount OPAccount = _IOPAccountService.FindByAccountNumber(temp.OriginalPriceAccount);
                        //OPAccount.DebitAmount -= temp.PurchasePrice ?? 0;
                        //OPAccount.DebitAmountOriginal -= temp.PurchasePrice ?? 0;

                        //OPAccount OPAccount2 = _IOPAccountService.FindByAccountNumber(temp.DepreciationAccount);
                        //OPAccount2.CreditAmount -= temp.AcDepreciationAmount ?? 0;
                        //OPAccount2.CreditAmountOriginal -= temp.AcDepreciationAmount ?? 0;

                        //_IOPAccountService.Update(OPAccount);
                        //_IOPAccountService.Update(OPAccount2);

                        //GeneralLedger GeneralLedger = _IGeneralLedgerService.FindByAccountNumber(temp.OriginalPriceAccount);
                        //GeneralLedger.DebitAmount -= temp.PurchasePrice ?? 0;
                        //GeneralLedger.DebitAmountOriginal -= temp.PurchasePrice ?? 0;

                        //GeneralLedger GeneralLedger2 = _IGeneralLedgerService.FindByAccountNumber(temp.DepreciationAccount);
                        //GeneralLedger2.CreditAmount -= temp.AcDepreciationAmount ?? 0;
                        //GeneralLedger2.CreditAmountOriginal -= temp.AcDepreciationAmount ?? 0;

                        //_IGeneralLedgerService.Update(GeneralLedger);
                        //_IGeneralLedgerService.Update(GeneralLedger2);


                        Utils.ClearCacheByType<FixedAsset>();
                        Utils.ClearCacheByType<FAInit>();
                        Utils.ClearCacheByType<OPFixedAsset>();
                        Utils.ClearCacheByType<FixedAssetLedger>();
                        _IFixedAssetService.CommitTran();
                        LoadDuLieu();
                        uGridDetail.DataSource = null;
                    } catch (Exception ex)
                    {
                        _IFixedAssetService.RolbackTran();
                        MSG.Warning("Có lỗi xảy ra, không thể xóa TSCĐ này");
                    } 
                }
            }
            else
                MSG.Error(string.Format(resSystem.MSG_Catalog2, "một Tài sản cố định"));
        }

        #endregion

        #region Utils

        private void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {
            utralGrid.ConfigGrid(ConstDatabase.FAInit_TableName);
            var DepartmentID = utralGrid.DisplayLayout.Bands[0].Columns["DepartmentID"];
            this.ConfigCbbToGrid(utralGrid, DepartmentID.Key, Utils.ListDepartment, "ID", "DepartmentName",
                                ConstDatabase.Department_TableName, "ParentID", "IsParentNode");
            DepartmentID.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownValidate;
            DepartmentID.CellActivation = Activation.NoEdit;
            var band = utralGrid.DisplayLayout.Bands[0];
            var columnAmout = new List<string>();
            foreach (var column in band.Columns)
            {
                column.ConfigColumnByNumberic();
            }
        }

        #endregion

        private void uGrid_BeforeRowActivate(object sender, RowEventArgs e)
        {

            if (e.Row == null) return;
            var fixedAsset = e.Row.ListObject as FixedAsset;
            if (fixedAsset == null) return;
            lblFixedAssetCode.Text = fixedAsset.FixedAssetCode;
            lblFixedAssetName.Text = fixedAsset.FixedAssetName;
            if (fixedAsset.FixedAssetCategory != null)
            {
                lblFixedAssetCategory.Text = fixedAsset.FixedAssetCategoryView;
            }
            try
            {
                if (fixedAsset.Department != null && fixedAsset.DepartmentID != Guid.Empty)
                {
                    lblDepartmentID.Text = fixedAsset.Department.DepartmentName;
                }
            }
            catch (Exception)
            {
                lblDepartmentID.Text = "";
            }
            
            lblOriginalPrice.Text = fixedAsset.OriginalPrice.ToStringNumbericFormat(ConstDatabase.Format_TienVND);
            lblPurchasePrice.Text = fixedAsset.MonthPeriodDepreciationAmount.ToStringNumbericFormat(ConstDatabase.Format_TienVND);
            lblAcDepreciationAmount.Text =
                fixedAsset.AcDepreciationAmount.ToStringNumbericFormat(ConstDatabase.Format_TienVND);
            lblRemainingAmount.Text = fixedAsset.RemainingAmount.ToStringNumbericFormat(ConstDatabase.Format_TienVND);
            //Hiển thị phần chứng từ liên quan
            var voucher = _IFixedAssetService.GetListVoucherFixedAssets(fixedAsset.ID);
            foreach (var item in voucher)
            {
                var firstOrDefault = Utils.ListType.FirstOrDefault(x => x.ID == item.TypeID);
                if (firstOrDefault != null)
                    item.TypeName = firstOrDefault.TypeName;
            }
            uGridDetail.DataSource = voucher.OrderByDescending(c=>c.Date).ToList();
            uGridDetail.ConfigGrid(ConstDatabase.VoucherFixedAsset_KeyName);
            foreach (var column in uGridDetail.DisplayLayout.Bands[0].Columns)
            {
                this.ConfigEachColumn4Grid(0, column, uGridDetail);
            }
        }

        private void btnViewVoucher_Click(object sender, EventArgs e)
        {
            if (uGridDetail.ActiveRow == null) return;
            var voucher = uGridDetail.ActiveRow.ListObject as VoucherFixedAsset;
            if (voucher == null) return;
            switch (voucher.TypeID)
            {
                case 510:
                    {
                        var fAIncrement = Utils.IFAIncrementService.Getbykey(voucher.ID);
                        new FFAIncrementDetail(fAIncrement, new List<FAIncrement> { fAIncrement },
                                               ConstFrm.optStatusForm.View).ShowDialog(this);
                    }
                    break;
                case 520:
                    {
                        var fADecrement = Utils.IFADecrementService.Getbykey(voucher.ID);
                        new FFADecrementDetail(fADecrement, new List<FADecrement> { fADecrement }, ConstFrm.optStatusForm.View).
                            ShowDialog(this);
                    }
                    break;
                case 530:
                    {
                        var fAAdjustment = Utils.IFAAdjustmentService.Getbykey(voucher.ID);
                        new FFAAdjustmentDetail(fAAdjustment, new List<FAAdjustment> { fAAdjustment }, ConstFrm.optStatusForm.View).
                            ShowDialog(this);
                    }
                    break;
                case 540:
                    {
                        var fAIncrement = Utils.IFADepreciationService.Getbykey(voucher.ID);
                        new FFADepreciationDetail(fAIncrement, new List<FADepreciation> { fAIncrement },
                                                  ConstFrm.optStatusForm.View).ShowDialog(this);
                    }
                    break;
                case 550:
                    {
                        var fAIncrement = Utils.IFATransferService.Getbykey(voucher.ID);
                        new FFATransferDetail(fAIncrement, new List<FATransfer> { fAIncrement },
                                                  ConstFrm.optStatusForm.View).ShowDialog(this);
                    }
                    break;
                case 560:
                    {
                        var fAIncrement = Utils.IFAAuditService.Getbykey(voucher.ID);
                        new FFAAuditDetail(fAIncrement, new List<FAAudit> { fAIncrement },
                                                  ConstFrm.optStatusForm.View).ShowDialog(this);
                    }
                    break;
            }
        }

        private void uGridDetail_BeforeRowActivate(object sender, RowEventArgs e)
        {

        }

        private void btnIncrement_Click(object sender, EventArgs e)
        {
            if (uGridDetail.ActiveRow == null) return;
            var voucher = uGridDetail.ActiveRow.ListObject as VoucherFixedAsset;
            if (voucher == null) return;
            var fAIncrement = Utils.IFAIncrementService.Getbykey(voucher.ID);
            new FFAIncrementDetail(fAIncrement, new List<FAIncrement> { fAIncrement },
                                   ConstFrm.optStatusForm.View).ShowDialog(this);
        }

        private void btnAdjustment_Click(object sender, EventArgs e)
        {
            if (uGridDetail.ActiveRow == null) return;
            var voucher = uGridDetail.ActiveRow.ListObject as VoucherFixedAsset;
            if (voucher == null) return;
            var fAAdjustment = Utils.IFAAdjustmentService.Getbykey(voucher.ID);
            new FFAAdjustmentDetail(fAAdjustment, new List<FAAdjustment> { fAAdjustment }, ConstFrm.optStatusForm.View).
                ShowDialog(this);
        }

        private void btnDecrement_Click(object sender, EventArgs e)
        {
            if (uGridDetail.ActiveRow == null) return;
            var voucher = uGridDetail.ActiveRow.ListObject as VoucherFixedAsset;
            if (voucher == null) return;
            var fADecrement = Utils.IFADecrementService.Getbykey(voucher.ID);
            new FFADecrementDetail(fADecrement, new List<FADecrement> { fADecrement }, ConstFrm.optStatusForm.View).
                ShowDialog(this);
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            Utils.ExportExcel(uGrid, "Khai báo TSCD đầu kỳ");
        }

        private void FFAInit_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGrid.ResetText();
            uGrid.ResetUpdateMode();
            uGrid.ResetExitEditModeOnLeave();
            uGrid.ResetRowUpdateCancelAction();
            uGrid.DataSource = null;
            uGrid.Layouts.Clear();
            uGrid.ResetLayouts();
            uGrid.ResetDisplayLayout();
            uGrid.Refresh();
            uGrid.ClearUndoHistory();
            uGrid.ClearXsdConstraints();
            
            uGridDetail.ResetText();
            uGridDetail.ResetUpdateMode();
            uGridDetail.ResetExitEditModeOnLeave();
            uGridDetail.ResetRowUpdateCancelAction();
            uGridDetail.DataSource = null;
            uGridDetail.Layouts.Clear();
            uGridDetail.ResetLayouts();
            uGridDetail.ResetDisplayLayout();
            uGridDetail.Refresh();
            uGridDetail.ClearUndoHistory();
            uGridDetail.ClearXsdConstraints();
        }

        private void FFAInit_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
