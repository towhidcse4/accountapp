﻿namespace Accounting
{
    partial class FFAInitDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem4 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem7 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem8 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance89 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance90 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance91 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance92 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance93 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance94 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance95 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance96 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance97 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance98 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance99 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance100 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance101 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance102 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance103 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance104 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance105 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance106 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance107 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance108 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance109 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance110 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance111 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance112 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance113 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance114 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance115 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance116 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance117 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance118 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance119 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance120 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance121 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance122 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance123 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance124 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance125 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance126 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance127 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance128 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance129 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance130 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance131 = new Infragistics.Win.Appearance();
            this.tsmFixedAssetDetail = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmAdd1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDelete1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmFixedAssetAccessories = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.cbbFixedAssetCategoryID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.txtFixedAssetName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbDepartmentID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.cldIncrementDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel22 = new Infragistics.Win.Misc.UltraLabel();
            this.cldDepreciationDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.nmrUsedTime = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.CbbYearMonth = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel32 = new Infragistics.Win.Misc.UltraLabel();
            this.nmrUsedTimeRemain = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.cbbYearMonth2 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.nmrOriginalPrice = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel29 = new Infragistics.Win.Misc.UltraLabel();
            this.nmrPurchasePrice = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel30 = new Infragistics.Win.Misc.UltraLabel();
            this.nmrAcDepreciationAmount = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel38 = new Infragistics.Win.Misc.UltraLabel();
            this.nmrRemainingAmount = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel37 = new Infragistics.Win.Misc.UltraLabel();
            this.nmrMonthPeriodDepreciationAmount = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel36 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbOriginalPriceAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel25 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbExpenditureAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel27 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbDepreciationAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel26 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbBudgetItem = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbCostSet = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel28 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbStatisticsCodeID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.txtFixedAssetCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.chkActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.tsmFixedAssetDetail.SuspendLayout();
            this.tsmFixedAssetAccessories.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbFixedAssetCategoryID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFixedAssetName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDepartmentID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cldIncrementDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cldDepreciationDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbbYearMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbYearMonth2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbOriginalPriceAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbExpenditureAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDepreciationAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBudgetItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCostSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbStatisticsCodeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFixedAssetCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkActive)).BeginInit();
            this.SuspendLayout();
            // 
            // tsmFixedAssetDetail
            // 
            this.tsmFixedAssetDetail.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmAdd1,
            this.tsmDelete1});
            this.tsmFixedAssetDetail.Name = "tsmFixedAssetDetail";
            this.tsmFixedAssetDetail.Size = new System.Drawing.Size(68, 48);
            // 
            // tsmAdd1
            // 
            this.tsmAdd1.Name = "tsmAdd1";
            this.tsmAdd1.Size = new System.Drawing.Size(67, 22);
            // 
            // tsmDelete1
            // 
            this.tsmDelete1.Name = "tsmDelete1";
            this.tsmDelete1.Size = new System.Drawing.Size(67, 22);
            // 
            // tsmFixedAssetAccessories
            // 
            this.tsmFixedAssetAccessories.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmAdd,
            this.tsmDelete});
            this.tsmFixedAssetAccessories.Name = "contextMenuStrip1";
            this.tsmFixedAssetAccessories.Size = new System.Drawing.Size(68, 48);
            // 
            // tsmAdd
            // 
            this.tsmAdd.Name = "tsmAdd";
            this.tsmAdd.Size = new System.Drawing.Size(67, 22);
            // 
            // tsmDelete
            // 
            this.tsmDelete.Name = "tsmDelete";
            this.tsmDelete.Size = new System.Drawing.Size(67, 22);
            // 
            // btnClose
            // 
            appearance1.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance1;
            this.btnClose.Location = new System.Drawing.Point(578, 435);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 55;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            appearance2.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance2;
            this.btnSave.Location = new System.Drawing.Point(497, 435);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 54;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cbbFixedAssetCategoryID
            // 
            this.cbbFixedAssetCategoryID.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.SuggestAppend;
            this.cbbFixedAssetCategoryID.AutoSize = false;
            appearance3.Image = global::Accounting.Properties.Resources.plus;
            editorButton1.Appearance = appearance3;
            appearance4.Image = global::Accounting.Properties.Resources.plus;
            editorButton1.PressedAppearance = appearance4;
            this.cbbFixedAssetCategoryID.ButtonsRight.Add(editorButton1);
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbFixedAssetCategoryID.DisplayLayout.Appearance = appearance5;
            this.cbbFixedAssetCategoryID.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbFixedAssetCategoryID.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.cbbFixedAssetCategoryID.DisplayLayout.DefaultSelectedForeColor = System.Drawing.Color.Black;
            appearance6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance6.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance6.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbFixedAssetCategoryID.DisplayLayout.GroupByBox.Appearance = appearance6;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbFixedAssetCategoryID.DisplayLayout.GroupByBox.BandLabelAppearance = appearance7;
            this.cbbFixedAssetCategoryID.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance8.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance8.BackColor2 = System.Drawing.SystemColors.Control;
            appearance8.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance8.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbFixedAssetCategoryID.DisplayLayout.GroupByBox.PromptAppearance = appearance8;
            this.cbbFixedAssetCategoryID.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbFixedAssetCategoryID.DisplayLayout.MaxRowScrollRegions = 1;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            appearance9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbFixedAssetCategoryID.DisplayLayout.Override.ActiveCellAppearance = appearance9;
            appearance10.BackColor = System.Drawing.SystemColors.Highlight;
            appearance10.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbFixedAssetCategoryID.DisplayLayout.Override.ActiveRowAppearance = appearance10;
            this.cbbFixedAssetCategoryID.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbFixedAssetCategoryID.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            this.cbbFixedAssetCategoryID.DisplayLayout.Override.CardAreaAppearance = appearance11;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            appearance12.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbFixedAssetCategoryID.DisplayLayout.Override.CellAppearance = appearance12;
            this.cbbFixedAssetCategoryID.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbFixedAssetCategoryID.DisplayLayout.Override.CellPadding = 0;
            appearance13.BackColor = System.Drawing.SystemColors.Control;
            appearance13.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance13.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance13.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbFixedAssetCategoryID.DisplayLayout.Override.GroupByRowAppearance = appearance13;
            appearance14.TextHAlignAsString = "Left";
            this.cbbFixedAssetCategoryID.DisplayLayout.Override.HeaderAppearance = appearance14;
            this.cbbFixedAssetCategoryID.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbFixedAssetCategoryID.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            appearance15.BorderColor = System.Drawing.Color.Silver;
            this.cbbFixedAssetCategoryID.DisplayLayout.Override.RowAppearance = appearance15;
            this.cbbFixedAssetCategoryID.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbFixedAssetCategoryID.DisplayLayout.Override.TemplateAddRowAppearance = appearance16;
            this.cbbFixedAssetCategoryID.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbFixedAssetCategoryID.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbFixedAssetCategoryID.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbFixedAssetCategoryID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbFixedAssetCategoryID.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbFixedAssetCategoryID.Location = new System.Drawing.Point(116, 44);
            this.cbbFixedAssetCategoryID.Name = "cbbFixedAssetCategoryID";
            this.cbbFixedAssetCategoryID.Size = new System.Drawing.Size(537, 22);
            this.cbbFixedAssetCategoryID.TabIndex = 3009;
            this.cbbFixedAssetCategoryID.ValueChanged += new System.EventHandler(this.cbbFixedAssetCategoryID_ValueChanged);
            this.cbbFixedAssetCategoryID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.cbbFixedAssetCategoryID_EditorButtonClick);
            // 
            // ultraLabel1
            // 
            appearance17.BackColor = System.Drawing.Color.Transparent;
            appearance17.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance17;
            this.ultraLabel1.Location = new System.Drawing.Point(10, 11);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel1.TabIndex = 3006;
            this.ultraLabel1.Text = "Mã (*)";
            // 
            // txtFixedAssetName
            // 
            this.txtFixedAssetName.AutoSize = false;
            this.txtFixedAssetName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.txtFixedAssetName.Location = new System.Drawing.Point(359, 11);
            this.txtFixedAssetName.MaxLength = 512;
            this.txtFixedAssetName.Name = "txtFixedAssetName";
            this.txtFixedAssetName.Size = new System.Drawing.Size(294, 22);
            this.txtFixedAssetName.TabIndex = 3008;
            // 
            // ultraLabel2
            // 
            appearance18.BackColor = System.Drawing.Color.Transparent;
            appearance18.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance18;
            this.ultraLabel2.Location = new System.Drawing.Point(316, 12);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(37, 22);
            this.ultraLabel2.TabIndex = 3004;
            this.ultraLabel2.Text = "Tên(*)";
            // 
            // ultraLabel3
            // 
            appearance19.BackColor = System.Drawing.Color.Transparent;
            appearance19.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance19;
            this.ultraLabel3.Location = new System.Drawing.Point(10, 44);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel3.TabIndex = 3005;
            this.ultraLabel3.Text = "Loại (*)";
            // 
            // cbbDepartmentID
            // 
            this.cbbDepartmentID.AutoSize = false;
            appearance20.Image = global::Accounting.Properties.Resources.plus;
            editorButton2.Appearance = appearance20;
            appearance21.Image = global::Accounting.Properties.Resources.plus;
            editorButton2.PressedAppearance = appearance21;
            this.cbbDepartmentID.ButtonsRight.Add(editorButton2);
            appearance22.BackColor = System.Drawing.SystemColors.Window;
            appearance22.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbDepartmentID.DisplayLayout.Appearance = appearance22;
            this.cbbDepartmentID.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbDepartmentID.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.cbbDepartmentID.DisplayLayout.DefaultSelectedForeColor = System.Drawing.Color.Black;
            appearance23.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance23.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance23.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbDepartmentID.DisplayLayout.GroupByBox.Appearance = appearance23;
            appearance24.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbDepartmentID.DisplayLayout.GroupByBox.BandLabelAppearance = appearance24;
            this.cbbDepartmentID.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance25.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance25.BackColor2 = System.Drawing.SystemColors.Control;
            appearance25.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance25.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbDepartmentID.DisplayLayout.GroupByBox.PromptAppearance = appearance25;
            this.cbbDepartmentID.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbDepartmentID.DisplayLayout.MaxRowScrollRegions = 1;
            appearance26.BackColor = System.Drawing.SystemColors.Window;
            appearance26.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbDepartmentID.DisplayLayout.Override.ActiveCellAppearance = appearance26;
            appearance27.BackColor = System.Drawing.SystemColors.Highlight;
            appearance27.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbDepartmentID.DisplayLayout.Override.ActiveRowAppearance = appearance27;
            this.cbbDepartmentID.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbDepartmentID.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance28.BackColor = System.Drawing.SystemColors.Window;
            this.cbbDepartmentID.DisplayLayout.Override.CardAreaAppearance = appearance28;
            appearance29.BorderColor = System.Drawing.Color.Silver;
            appearance29.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbDepartmentID.DisplayLayout.Override.CellAppearance = appearance29;
            this.cbbDepartmentID.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbDepartmentID.DisplayLayout.Override.CellPadding = 0;
            appearance30.BackColor = System.Drawing.SystemColors.Control;
            appearance30.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance30.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance30.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance30.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbDepartmentID.DisplayLayout.Override.GroupByRowAppearance = appearance30;
            appearance31.TextHAlignAsString = "Left";
            this.cbbDepartmentID.DisplayLayout.Override.HeaderAppearance = appearance31;
            this.cbbDepartmentID.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbDepartmentID.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance32.BackColor = System.Drawing.SystemColors.Window;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            this.cbbDepartmentID.DisplayLayout.Override.RowAppearance = appearance32;
            this.cbbDepartmentID.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance33.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbDepartmentID.DisplayLayout.Override.TemplateAddRowAppearance = appearance33;
            this.cbbDepartmentID.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbDepartmentID.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbDepartmentID.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbDepartmentID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbDepartmentID.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbDepartmentID.Location = new System.Drawing.Point(116, 77);
            this.cbbDepartmentID.Name = "cbbDepartmentID";
            this.cbbDepartmentID.Size = new System.Drawing.Size(537, 22);
            this.cbbDepartmentID.TabIndex = 3011;
            this.cbbDepartmentID.ValueChanged += new System.EventHandler(this.cbbDepartmentID_ValueChanged);
            this.cbbDepartmentID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.cbbDepartmentID_EditorButtonClick);
            // 
            // ultraLabel5
            // 
            appearance34.BackColor = System.Drawing.Color.Transparent;
            appearance34.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance34;
            this.ultraLabel5.Location = new System.Drawing.Point(10, 77);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel5.TabIndex = 3010;
            this.ultraLabel5.Text = "Phòng ban (*)";
            // 
            // cldIncrementDate
            // 
            appearance35.TextVAlignAsString = "Middle";
            this.cldIncrementDate.Appearance = appearance35;
            this.cldIncrementDate.AutoSize = false;
            this.cldIncrementDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.cldIncrementDate.Location = new System.Drawing.Point(116, 122);
            this.cldIncrementDate.MaskInput = "dd/mm/yyyy";
            this.cldIncrementDate.Name = "cldIncrementDate";
            this.cldIncrementDate.Size = new System.Drawing.Size(194, 22);
            this.cldIncrementDate.TabIndex = 3014;
            this.cldIncrementDate.Value = null;
            this.cldIncrementDate.ValueChanged += new System.EventHandler(this.cldIncrementDate_ValueChanged);
            // 
            // ultraLabel22
            // 
            appearance36.BackColor = System.Drawing.Color.Transparent;
            appearance36.TextVAlignAsString = "Middle";
            this.ultraLabel22.Appearance = appearance36;
            this.ultraLabel22.Location = new System.Drawing.Point(10, 124);
            this.ultraLabel22.Name = "ultraLabel22";
            this.ultraLabel22.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel22.TabIndex = 3013;
            this.ultraLabel22.Text = "Ngày ghi tăng (*)";
            // 
            // cldDepreciationDate
            // 
            appearance37.TextVAlignAsString = "Middle";
            this.cldDepreciationDate.Appearance = appearance37;
            this.cldDepreciationDate.AutoSize = false;
            this.cldDepreciationDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.cldDepreciationDate.Location = new System.Drawing.Point(441, 123);
            this.cldDepreciationDate.MaskInput = "dd/mm/yyyy";
            this.cldDepreciationDate.Name = "cldDepreciationDate";
            this.cldDepreciationDate.Size = new System.Drawing.Size(212, 22);
            this.cldDepreciationDate.TabIndex = 3016;
            this.cldDepreciationDate.Value = null;
            this.cldDepreciationDate.ValueChanged += new System.EventHandler(this.cldDepreciationDate_ValueChanged);
            // 
            // ultraLabel4
            // 
            appearance38.BackColor = System.Drawing.Color.Transparent;
            appearance38.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance38;
            this.ultraLabel4.Location = new System.Drawing.Point(316, 124);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(119, 22);
            this.ultraLabel4.TabIndex = 3015;
            this.ultraLabel4.Text = "Ngày bắt đầu KH (*)";
            // 
            // nmrUsedTime
            // 
            appearance39.TextHAlignAsString = "Right";
            this.nmrUsedTime.Appearance = appearance39;
            this.nmrUsedTime.AutoSize = false;
            this.nmrUsedTime.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.nmrUsedTime.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.nmrUsedTime.InputMask = "{LOC}nnnnnnn.nn";
            this.nmrUsedTime.Location = new System.Drawing.Point(116, 155);
            this.nmrUsedTime.MaxValue = 5000D;
            this.nmrUsedTime.MinValue = 0D;
            this.nmrUsedTime.Name = "nmrUsedTime";
            this.nmrUsedTime.Size = new System.Drawing.Size(113, 22);
            this.nmrUsedTime.TabIndex = 3019;
            this.nmrUsedTime.ValueChanged += new System.EventHandler(this.nmrUsedTime_ValueChanged);
            // 
            // CbbYearMonth
            // 
            this.CbbYearMonth.AutoSize = false;
            valueListItem3.CheckState = System.Windows.Forms.CheckState.Checked;
            valueListItem3.DataValue = "ValueListItem0";
            valueListItem3.DisplayText = "Tháng";
            valueListItem4.DataValue = "ValueListItem1";
            valueListItem4.DisplayText = "Năm";
            this.CbbYearMonth.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem3,
            valueListItem4});
            this.CbbYearMonth.Location = new System.Drawing.Point(235, 155);
            this.CbbYearMonth.Name = "CbbYearMonth";
            this.CbbYearMonth.Size = new System.Drawing.Size(75, 22);
            this.CbbYearMonth.TabIndex = 3020;
            this.CbbYearMonth.Text = "Tháng";
            this.CbbYearMonth.ValueChanged += new System.EventHandler(this.CbbYearMonth_ValueChanged);
            // 
            // ultraLabel32
            // 
            appearance40.BackColor = System.Drawing.Color.Transparent;
            appearance40.TextVAlignAsString = "Middle";
            this.ultraLabel32.Appearance = appearance40;
            this.ultraLabel32.Location = new System.Drawing.Point(10, 158);
            this.ultraLabel32.Name = "ultraLabel32";
            this.ultraLabel32.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel32.TabIndex = 3018;
            this.ultraLabel32.Text = "Thời gian sử dụng ";
            // 
            // nmrUsedTimeRemain
            // 
            appearance41.TextHAlignAsString = "Right";
            this.nmrUsedTimeRemain.Appearance = appearance41;
            this.nmrUsedTimeRemain.AutoSize = false;
            this.nmrUsedTimeRemain.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.nmrUsedTimeRemain.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.nmrUsedTimeRemain.InputMask = "{LOC}nnnnnnn.nn";
            this.nmrUsedTimeRemain.Location = new System.Drawing.Point(441, 157);
            this.nmrUsedTimeRemain.MaxValue = 5000D;
            this.nmrUsedTimeRemain.MinValue = 0D;
            this.nmrUsedTimeRemain.Name = "nmrUsedTimeRemain";
            this.nmrUsedTimeRemain.Size = new System.Drawing.Size(131, 22);
            this.nmrUsedTimeRemain.TabIndex = 3022;
            this.nmrUsedTimeRemain.ValueChanged += new System.EventHandler(this.cbbYearMonth2_ValueChanged);
            // 
            // cbbYearMonth2
            // 
            this.cbbYearMonth2.AutoSize = false;
            valueListItem7.CheckState = System.Windows.Forms.CheckState.Checked;
            valueListItem7.DataValue = "ValueListItem0";
            valueListItem7.DisplayText = "Tháng";
            valueListItem8.DataValue = "ValueListItem1";
            valueListItem8.DisplayText = "Năm";
            this.cbbYearMonth2.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem7,
            valueListItem8});
            this.cbbYearMonth2.Location = new System.Drawing.Point(578, 158);
            this.cbbYearMonth2.Name = "cbbYearMonth2";
            this.cbbYearMonth2.Size = new System.Drawing.Size(75, 22);
            this.cbbYearMonth2.TabIndex = 3023;
            this.cbbYearMonth2.Text = "Tháng";
            this.cbbYearMonth2.ValueChanged += new System.EventHandler(this.cbbYearMonth2_ValueChanged);
            // 
            // ultraLabel6
            // 
            appearance42.BackColor = System.Drawing.Color.Transparent;
            appearance42.TextVAlignAsString = "Middle";
            this.ultraLabel6.Appearance = appearance42;
            this.ultraLabel6.Location = new System.Drawing.Point(316, 157);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(119, 22);
            this.ultraLabel6.TabIndex = 3021;
            this.ultraLabel6.Text = "Thời gian SD còn lại";
            // 
            // nmrOriginalPrice
            // 
            appearance43.TextHAlignAsString = "Right";
            this.nmrOriginalPrice.Appearance = appearance43;
            this.nmrOriginalPrice.AutoSize = false;
            this.nmrOriginalPrice.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.nmrOriginalPrice.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.nmrOriginalPrice.InputMask = "{LOC}nnnnnnn.nn";
            this.nmrOriginalPrice.Location = new System.Drawing.Point(116, 219);
            this.nmrOriginalPrice.MaxValue = 9999999999999D;
            this.nmrOriginalPrice.MinValue = 0D;
            this.nmrOriginalPrice.Name = "nmrOriginalPrice";
            this.nmrOriginalPrice.Size = new System.Drawing.Size(194, 22);
            this.nmrOriginalPrice.TabIndex = 3027;
            this.nmrOriginalPrice.Text = "0";
            this.nmrOriginalPrice.ValueChanged += new System.EventHandler(this.nmrOriginalPrice_ValueChanged);
            // 
            // ultraLabel29
            // 
            appearance44.BackColor = System.Drawing.Color.Transparent;
            appearance44.TextVAlignAsString = "Middle";
            this.ultraLabel29.Appearance = appearance44;
            this.ultraLabel29.Location = new System.Drawing.Point(10, 221);
            this.ultraLabel29.Name = "ultraLabel29";
            this.ultraLabel29.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel29.TabIndex = 3025;
            this.ultraLabel29.Text = "Nguyên giá ";
            // 
            // nmrPurchasePrice
            // 
            appearance45.TextHAlignAsString = "Right";
            this.nmrPurchasePrice.Appearance = appearance45;
            this.nmrPurchasePrice.AutoSize = false;
            this.nmrPurchasePrice.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.nmrPurchasePrice.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.nmrPurchasePrice.InputMask = "{LOC}nn,nnn,nnn.nn";
            this.nmrPurchasePrice.Location = new System.Drawing.Point(441, 221);
            this.nmrPurchasePrice.MaxValue = ((long)(99999999999999999));
            this.nmrPurchasePrice.Name = "nmrPurchasePrice";
            this.nmrPurchasePrice.Size = new System.Drawing.Size(212, 22);
            this.nmrPurchasePrice.TabIndex = 3029;
            this.nmrPurchasePrice.ValueChanged += new System.EventHandler(this.nmrPurchasePrice_ValueChanged);
            // 
            // ultraLabel30
            // 
            appearance46.BackColor = System.Drawing.Color.Transparent;
            appearance46.TextVAlignAsString = "Middle";
            this.ultraLabel30.Appearance = appearance46;
            this.ultraLabel30.Location = new System.Drawing.Point(316, 221);
            this.ultraLabel30.Name = "ultraLabel30";
            this.ultraLabel30.Size = new System.Drawing.Size(119, 22);
            this.ultraLabel30.TabIndex = 3028;
            this.ultraLabel30.Text = "Giá trị tính khấu hao ";
            // 
            // nmrAcDepreciationAmount
            // 
            appearance47.TextHAlignAsString = "Right";
            this.nmrAcDepreciationAmount.Appearance = appearance47;
            this.nmrAcDepreciationAmount.AutoSize = false;
            this.nmrAcDepreciationAmount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.nmrAcDepreciationAmount.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.nmrAcDepreciationAmount.InputMask = "{LOC}nnnnnnn.nn";
            this.nmrAcDepreciationAmount.Location = new System.Drawing.Point(116, 254);
            this.nmrAcDepreciationAmount.MaxValue = 9999999999999D;
            this.nmrAcDepreciationAmount.MinValue = 0D;
            this.nmrAcDepreciationAmount.Name = "nmrAcDepreciationAmount";
            this.nmrAcDepreciationAmount.Size = new System.Drawing.Size(194, 22);
            this.nmrAcDepreciationAmount.TabIndex = 3031;
            this.nmrAcDepreciationAmount.Text = "0";
            this.nmrAcDepreciationAmount.ValueChanged += new System.EventHandler(this.nmrAcDepreciationAmount_ValueChanged);
            // 
            // ultraLabel38
            // 
            appearance48.BackColor = System.Drawing.Color.Transparent;
            appearance48.TextVAlignAsString = "Middle";
            this.ultraLabel38.Appearance = appearance48;
            this.ultraLabel38.Location = new System.Drawing.Point(10, 254);
            this.ultraLabel38.Name = "ultraLabel38";
            this.ultraLabel38.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel38.TabIndex = 3030;
            this.ultraLabel38.Text = "Hao mòn lũy kế";
            // 
            // nmrRemainingAmount
            // 
            appearance49.TextHAlignAsString = "Right";
            this.nmrRemainingAmount.Appearance = appearance49;
            this.nmrRemainingAmount.AutoSize = false;
            this.nmrRemainingAmount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.nmrRemainingAmount.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.nmrRemainingAmount.InputMask = "{LOC}nnnnnnn.nn";
            this.nmrRemainingAmount.Location = new System.Drawing.Point(441, 254);
            this.nmrRemainingAmount.MaxValue = 9999999999999D;
            this.nmrRemainingAmount.MinValue = 0D;
            this.nmrRemainingAmount.Name = "nmrRemainingAmount";
            this.nmrRemainingAmount.Size = new System.Drawing.Size(212, 22);
            this.nmrRemainingAmount.TabIndex = 3033;
            this.nmrRemainingAmount.Text = "0";
            // 
            // ultraLabel37
            // 
            appearance50.BackColor = System.Drawing.Color.Transparent;
            appearance50.TextVAlignAsString = "Middle";
            this.ultraLabel37.Appearance = appearance50;
            this.ultraLabel37.Location = new System.Drawing.Point(316, 254);
            this.ultraLabel37.Name = "ultraLabel37";
            this.ultraLabel37.Size = new System.Drawing.Size(119, 22);
            this.ultraLabel37.TabIndex = 3032;
            this.ultraLabel37.Text = "Giá trị còn lại";
            // 
            // nmrMonthPeriodDepreciationAmount
            // 
            appearance51.TextHAlignAsString = "Right";
            this.nmrMonthPeriodDepreciationAmount.Appearance = appearance51;
            this.nmrMonthPeriodDepreciationAmount.AutoSize = false;
            this.nmrMonthPeriodDepreciationAmount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.nmrMonthPeriodDepreciationAmount.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.nmrMonthPeriodDepreciationAmount.InputMask = "{LOC}nnnnnnn.nn";
            this.nmrMonthPeriodDepreciationAmount.Location = new System.Drawing.Point(116, 287);
            this.nmrMonthPeriodDepreciationAmount.MaxValue = 9999999999999D;
            this.nmrMonthPeriodDepreciationAmount.MinValue = 0D;
            this.nmrMonthPeriodDepreciationAmount.Name = "nmrMonthPeriodDepreciationAmount";
            this.nmrMonthPeriodDepreciationAmount.Size = new System.Drawing.Size(194, 22);
            this.nmrMonthPeriodDepreciationAmount.TabIndex = 3035;
            this.nmrMonthPeriodDepreciationAmount.Text = "0";
            // 
            // ultraLabel36
            // 
            appearance52.BackColor = System.Drawing.Color.Transparent;
            appearance52.TextVAlignAsString = "Middle";
            this.ultraLabel36.Appearance = appearance52;
            this.ultraLabel36.Location = new System.Drawing.Point(10, 287);
            this.ultraLabel36.Name = "ultraLabel36";
            this.ultraLabel36.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel36.TabIndex = 3034;
            this.ultraLabel36.Text = "Giá trị KH tháng";
            // 
            // cbbOriginalPriceAccount
            // 
            this.cbbOriginalPriceAccount.AutoSize = false;
            appearance53.BackColor = System.Drawing.SystemColors.Window;
            appearance53.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbOriginalPriceAccount.DisplayLayout.Appearance = appearance53;
            this.cbbOriginalPriceAccount.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbOriginalPriceAccount.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.cbbOriginalPriceAccount.DisplayLayout.DefaultSelectedForeColor = System.Drawing.Color.Black;
            appearance54.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance54.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance54.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance54.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbOriginalPriceAccount.DisplayLayout.GroupByBox.Appearance = appearance54;
            appearance55.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbOriginalPriceAccount.DisplayLayout.GroupByBox.BandLabelAppearance = appearance55;
            this.cbbOriginalPriceAccount.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance56.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance56.BackColor2 = System.Drawing.SystemColors.Control;
            appearance56.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance56.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbOriginalPriceAccount.DisplayLayout.GroupByBox.PromptAppearance = appearance56;
            this.cbbOriginalPriceAccount.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbOriginalPriceAccount.DisplayLayout.MaxRowScrollRegions = 1;
            appearance57.BackColor = System.Drawing.SystemColors.Window;
            appearance57.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbOriginalPriceAccount.DisplayLayout.Override.ActiveCellAppearance = appearance57;
            appearance58.BackColor = System.Drawing.SystemColors.Highlight;
            appearance58.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbOriginalPriceAccount.DisplayLayout.Override.ActiveRowAppearance = appearance58;
            this.cbbOriginalPriceAccount.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbOriginalPriceAccount.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance59.BackColor = System.Drawing.SystemColors.Window;
            this.cbbOriginalPriceAccount.DisplayLayout.Override.CardAreaAppearance = appearance59;
            appearance60.BorderColor = System.Drawing.Color.Silver;
            appearance60.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbOriginalPriceAccount.DisplayLayout.Override.CellAppearance = appearance60;
            this.cbbOriginalPriceAccount.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbOriginalPriceAccount.DisplayLayout.Override.CellPadding = 0;
            appearance61.BackColor = System.Drawing.SystemColors.Control;
            appearance61.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance61.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance61.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance61.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbOriginalPriceAccount.DisplayLayout.Override.GroupByRowAppearance = appearance61;
            appearance62.TextHAlignAsString = "Left";
            this.cbbOriginalPriceAccount.DisplayLayout.Override.HeaderAppearance = appearance62;
            this.cbbOriginalPriceAccount.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbOriginalPriceAccount.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance63.BackColor = System.Drawing.SystemColors.Window;
            appearance63.BorderColor = System.Drawing.Color.Silver;
            this.cbbOriginalPriceAccount.DisplayLayout.Override.RowAppearance = appearance63;
            this.cbbOriginalPriceAccount.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance64.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbOriginalPriceAccount.DisplayLayout.Override.TemplateAddRowAppearance = appearance64;
            this.cbbOriginalPriceAccount.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbOriginalPriceAccount.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbOriginalPriceAccount.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbOriginalPriceAccount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbOriginalPriceAccount.Location = new System.Drawing.Point(441, 287);
            this.cbbOriginalPriceAccount.Name = "cbbOriginalPriceAccount";
            this.cbbOriginalPriceAccount.Size = new System.Drawing.Size(212, 22);
            this.cbbOriginalPriceAccount.TabIndex = 3037;
            this.cbbOriginalPriceAccount.ItemNotInList += new Infragistics.Win.UltraWinGrid.ItemNotInListEventHandler(this.cbbOriginalPriceAccount_ItemNotInList);
            // 
            // ultraLabel25
            // 
            appearance65.BackColor = System.Drawing.Color.Transparent;
            appearance65.TextVAlignAsString = "Middle";
            this.ultraLabel25.Appearance = appearance65;
            this.ultraLabel25.Location = new System.Drawing.Point(316, 287);
            this.ultraLabel25.Name = "ultraLabel25";
            this.ultraLabel25.Size = new System.Drawing.Size(119, 22);
            this.ultraLabel25.TabIndex = 3036;
            this.ultraLabel25.Text = "TK nguyên giá (*)";
            // 
            // cbbExpenditureAccount
            // 
            this.cbbExpenditureAccount.AutoSize = false;
            appearance66.BackColor = System.Drawing.SystemColors.Window;
            appearance66.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbExpenditureAccount.DisplayLayout.Appearance = appearance66;
            this.cbbExpenditureAccount.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbExpenditureAccount.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.cbbExpenditureAccount.DisplayLayout.DefaultSelectedForeColor = System.Drawing.Color.Black;
            appearance67.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance67.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance67.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance67.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbExpenditureAccount.DisplayLayout.GroupByBox.Appearance = appearance67;
            appearance68.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbExpenditureAccount.DisplayLayout.GroupByBox.BandLabelAppearance = appearance68;
            this.cbbExpenditureAccount.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance69.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance69.BackColor2 = System.Drawing.SystemColors.Control;
            appearance69.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance69.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbExpenditureAccount.DisplayLayout.GroupByBox.PromptAppearance = appearance69;
            this.cbbExpenditureAccount.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbExpenditureAccount.DisplayLayout.MaxRowScrollRegions = 1;
            appearance70.BackColor = System.Drawing.SystemColors.Window;
            appearance70.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbExpenditureAccount.DisplayLayout.Override.ActiveCellAppearance = appearance70;
            appearance71.BackColor = System.Drawing.SystemColors.Highlight;
            appearance71.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbExpenditureAccount.DisplayLayout.Override.ActiveRowAppearance = appearance71;
            this.cbbExpenditureAccount.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbExpenditureAccount.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance72.BackColor = System.Drawing.SystemColors.Window;
            this.cbbExpenditureAccount.DisplayLayout.Override.CardAreaAppearance = appearance72;
            appearance73.BorderColor = System.Drawing.Color.Silver;
            appearance73.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbExpenditureAccount.DisplayLayout.Override.CellAppearance = appearance73;
            this.cbbExpenditureAccount.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbExpenditureAccount.DisplayLayout.Override.CellPadding = 0;
            appearance74.BackColor = System.Drawing.SystemColors.Control;
            appearance74.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance74.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance74.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance74.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbExpenditureAccount.DisplayLayout.Override.GroupByRowAppearance = appearance74;
            appearance75.TextHAlignAsString = "Left";
            this.cbbExpenditureAccount.DisplayLayout.Override.HeaderAppearance = appearance75;
            this.cbbExpenditureAccount.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbExpenditureAccount.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance76.BackColor = System.Drawing.SystemColors.Window;
            appearance76.BorderColor = System.Drawing.Color.Silver;
            this.cbbExpenditureAccount.DisplayLayout.Override.RowAppearance = appearance76;
            this.cbbExpenditureAccount.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance77.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbExpenditureAccount.DisplayLayout.Override.TemplateAddRowAppearance = appearance77;
            this.cbbExpenditureAccount.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbExpenditureAccount.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbExpenditureAccount.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbExpenditureAccount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbExpenditureAccount.Location = new System.Drawing.Point(441, 320);
            this.cbbExpenditureAccount.Name = "cbbExpenditureAccount";
            this.cbbExpenditureAccount.Size = new System.Drawing.Size(212, 22);
            this.cbbExpenditureAccount.TabIndex = 3039;
            this.cbbExpenditureAccount.ItemNotInList += new Infragistics.Win.UltraWinGrid.ItemNotInListEventHandler(this.cbbExpenditureAccount_ItemNotInList);
            // 
            // ultraLabel27
            // 
            appearance78.BackColor = System.Drawing.Color.Transparent;
            appearance78.TextVAlignAsString = "Middle";
            this.ultraLabel27.Appearance = appearance78;
            this.ultraLabel27.Location = new System.Drawing.Point(316, 320);
            this.ultraLabel27.Name = "ultraLabel27";
            this.ultraLabel27.Size = new System.Drawing.Size(119, 22);
            this.ultraLabel27.TabIndex = 3038;
            this.ultraLabel27.Text = "TK chi phí (*)";
            // 
            // cbbDepreciationAccount
            // 
            this.cbbDepreciationAccount.AutoSize = false;
            appearance79.BackColor = System.Drawing.SystemColors.Window;
            appearance79.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbDepreciationAccount.DisplayLayout.Appearance = appearance79;
            this.cbbDepreciationAccount.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbDepreciationAccount.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.cbbDepreciationAccount.DisplayLayout.DefaultSelectedForeColor = System.Drawing.Color.Black;
            appearance80.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance80.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance80.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance80.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbDepreciationAccount.DisplayLayout.GroupByBox.Appearance = appearance80;
            appearance81.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbDepreciationAccount.DisplayLayout.GroupByBox.BandLabelAppearance = appearance81;
            this.cbbDepreciationAccount.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance82.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance82.BackColor2 = System.Drawing.SystemColors.Control;
            appearance82.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance82.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbDepreciationAccount.DisplayLayout.GroupByBox.PromptAppearance = appearance82;
            this.cbbDepreciationAccount.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbDepreciationAccount.DisplayLayout.MaxRowScrollRegions = 1;
            appearance83.BackColor = System.Drawing.SystemColors.Window;
            appearance83.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbDepreciationAccount.DisplayLayout.Override.ActiveCellAppearance = appearance83;
            appearance84.BackColor = System.Drawing.SystemColors.Highlight;
            appearance84.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbDepreciationAccount.DisplayLayout.Override.ActiveRowAppearance = appearance84;
            this.cbbDepreciationAccount.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbDepreciationAccount.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance85.BackColor = System.Drawing.SystemColors.Window;
            this.cbbDepreciationAccount.DisplayLayout.Override.CardAreaAppearance = appearance85;
            appearance86.BorderColor = System.Drawing.Color.Silver;
            appearance86.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbDepreciationAccount.DisplayLayout.Override.CellAppearance = appearance86;
            this.cbbDepreciationAccount.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbDepreciationAccount.DisplayLayout.Override.CellPadding = 0;
            appearance87.BackColor = System.Drawing.SystemColors.Control;
            appearance87.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance87.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance87.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance87.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbDepreciationAccount.DisplayLayout.Override.GroupByRowAppearance = appearance87;
            appearance88.TextHAlignAsString = "Left";
            this.cbbDepreciationAccount.DisplayLayout.Override.HeaderAppearance = appearance88;
            this.cbbDepreciationAccount.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbDepreciationAccount.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance89.BackColor = System.Drawing.SystemColors.Window;
            appearance89.BorderColor = System.Drawing.Color.Silver;
            this.cbbDepreciationAccount.DisplayLayout.Override.RowAppearance = appearance89;
            this.cbbDepreciationAccount.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance90.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbDepreciationAccount.DisplayLayout.Override.TemplateAddRowAppearance = appearance90;
            this.cbbDepreciationAccount.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbDepreciationAccount.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbDepreciationAccount.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbDepreciationAccount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbDepreciationAccount.Location = new System.Drawing.Point(441, 353);
            this.cbbDepreciationAccount.Name = "cbbDepreciationAccount";
            this.cbbDepreciationAccount.Size = new System.Drawing.Size(212, 22);
            this.cbbDepreciationAccount.TabIndex = 3041;
            this.cbbDepreciationAccount.ItemNotInList += new Infragistics.Win.UltraWinGrid.ItemNotInListEventHandler(this.cbbDepreciationAccount_ItemNotInList);
            // 
            // ultraLabel26
            // 
            appearance91.BackColor = System.Drawing.Color.Transparent;
            appearance91.TextVAlignAsString = "Middle";
            this.ultraLabel26.Appearance = appearance91;
            this.ultraLabel26.Location = new System.Drawing.Point(316, 353);
            this.ultraLabel26.Name = "ultraLabel26";
            this.ultraLabel26.Size = new System.Drawing.Size(119, 22);
            this.ultraLabel26.TabIndex = 3040;
            this.ultraLabel26.Text = "TK khấu hao (*)";
            // 
            // cbbBudgetItem
            // 
            this.cbbBudgetItem.AutoSize = false;
            appearance92.BackColor = System.Drawing.SystemColors.Window;
            appearance92.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbBudgetItem.DisplayLayout.Appearance = appearance92;
            this.cbbBudgetItem.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbBudgetItem.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.cbbBudgetItem.DisplayLayout.DefaultSelectedForeColor = System.Drawing.Color.Black;
            appearance93.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance93.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance93.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance93.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbBudgetItem.DisplayLayout.GroupByBox.Appearance = appearance93;
            appearance94.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbBudgetItem.DisplayLayout.GroupByBox.BandLabelAppearance = appearance94;
            this.cbbBudgetItem.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance95.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance95.BackColor2 = System.Drawing.SystemColors.Control;
            appearance95.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance95.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbBudgetItem.DisplayLayout.GroupByBox.PromptAppearance = appearance95;
            this.cbbBudgetItem.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbBudgetItem.DisplayLayout.MaxRowScrollRegions = 1;
            appearance96.BackColor = System.Drawing.SystemColors.Window;
            appearance96.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbBudgetItem.DisplayLayout.Override.ActiveCellAppearance = appearance96;
            appearance97.BackColor = System.Drawing.SystemColors.Highlight;
            appearance97.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbBudgetItem.DisplayLayout.Override.ActiveRowAppearance = appearance97;
            this.cbbBudgetItem.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbBudgetItem.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance98.BackColor = System.Drawing.SystemColors.Window;
            this.cbbBudgetItem.DisplayLayout.Override.CardAreaAppearance = appearance98;
            appearance99.BorderColor = System.Drawing.Color.Silver;
            appearance99.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbBudgetItem.DisplayLayout.Override.CellAppearance = appearance99;
            this.cbbBudgetItem.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbBudgetItem.DisplayLayout.Override.CellPadding = 0;
            appearance100.BackColor = System.Drawing.SystemColors.Control;
            appearance100.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance100.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance100.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance100.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbBudgetItem.DisplayLayout.Override.GroupByRowAppearance = appearance100;
            appearance101.TextHAlignAsString = "Left";
            this.cbbBudgetItem.DisplayLayout.Override.HeaderAppearance = appearance101;
            this.cbbBudgetItem.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbBudgetItem.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance102.BackColor = System.Drawing.SystemColors.Window;
            appearance102.BorderColor = System.Drawing.Color.Silver;
            this.cbbBudgetItem.DisplayLayout.Override.RowAppearance = appearance102;
            this.cbbBudgetItem.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance103.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbBudgetItem.DisplayLayout.Override.TemplateAddRowAppearance = appearance103;
            this.cbbBudgetItem.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbBudgetItem.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbBudgetItem.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbBudgetItem.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbBudgetItem.Location = new System.Drawing.Point(116, 320);
            this.cbbBudgetItem.Name = "cbbBudgetItem";
            this.cbbBudgetItem.Size = new System.Drawing.Size(194, 22);
            this.cbbBudgetItem.TabIndex = 3043;
            this.cbbBudgetItem.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbBudgetItem_RowSelected);
            this.cbbBudgetItem.Validated += new System.EventHandler(this.cbbBudgetItem_Validated);
            // 
            // ultraLabel7
            // 
            appearance104.BackColor = System.Drawing.Color.Transparent;
            appearance104.TextVAlignAsString = "Middle";
            this.ultraLabel7.Appearance = appearance104;
            this.ultraLabel7.Location = new System.Drawing.Point(10, 320);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel7.TabIndex = 3042;
            this.ultraLabel7.Text = "Mục thu/chi";
            // 
            // cbbCostSet
            // 
            this.cbbCostSet.AutoSize = false;
            appearance105.BackColor = System.Drawing.SystemColors.Window;
            appearance105.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbCostSet.DisplayLayout.Appearance = appearance105;
            this.cbbCostSet.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbCostSet.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.cbbCostSet.DisplayLayout.DefaultSelectedForeColor = System.Drawing.Color.Black;
            appearance106.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance106.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance106.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance106.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbCostSet.DisplayLayout.GroupByBox.Appearance = appearance106;
            appearance107.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbCostSet.DisplayLayout.GroupByBox.BandLabelAppearance = appearance107;
            this.cbbCostSet.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance108.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance108.BackColor2 = System.Drawing.SystemColors.Control;
            appearance108.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance108.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbCostSet.DisplayLayout.GroupByBox.PromptAppearance = appearance108;
            this.cbbCostSet.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbCostSet.DisplayLayout.MaxRowScrollRegions = 1;
            appearance109.BackColor = System.Drawing.SystemColors.Window;
            appearance109.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbCostSet.DisplayLayout.Override.ActiveCellAppearance = appearance109;
            appearance110.BackColor = System.Drawing.SystemColors.Highlight;
            appearance110.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbCostSet.DisplayLayout.Override.ActiveRowAppearance = appearance110;
            this.cbbCostSet.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbCostSet.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance111.BackColor = System.Drawing.SystemColors.Window;
            this.cbbCostSet.DisplayLayout.Override.CardAreaAppearance = appearance111;
            appearance112.BorderColor = System.Drawing.Color.Silver;
            appearance112.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbCostSet.DisplayLayout.Override.CellAppearance = appearance112;
            this.cbbCostSet.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbCostSet.DisplayLayout.Override.CellPadding = 0;
            appearance113.BackColor = System.Drawing.SystemColors.Control;
            appearance113.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance113.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance113.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance113.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbCostSet.DisplayLayout.Override.GroupByRowAppearance = appearance113;
            appearance114.TextHAlignAsString = "Left";
            this.cbbCostSet.DisplayLayout.Override.HeaderAppearance = appearance114;
            this.cbbCostSet.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbCostSet.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance115.BackColor = System.Drawing.SystemColors.Window;
            appearance115.BorderColor = System.Drawing.Color.Silver;
            this.cbbCostSet.DisplayLayout.Override.RowAppearance = appearance115;
            this.cbbCostSet.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance116.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbCostSet.DisplayLayout.Override.TemplateAddRowAppearance = appearance116;
            this.cbbCostSet.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbCostSet.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbCostSet.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbCostSet.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbCostSet.Location = new System.Drawing.Point(116, 353);
            this.cbbCostSet.Name = "cbbCostSet";
            this.cbbCostSet.Size = new System.Drawing.Size(194, 22);
            this.cbbCostSet.TabIndex = 3045;
            this.cbbCostSet.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbCostSet_RowSelected);
            this.cbbCostSet.Validating += new System.ComponentModel.CancelEventHandler(this.cbbCostSet_Validating);
            // 
            // ultraLabel28
            // 
            appearance117.BackColor = System.Drawing.Color.Transparent;
            appearance117.TextVAlignAsString = "Middle";
            this.ultraLabel28.Appearance = appearance117;
            this.ultraLabel28.Location = new System.Drawing.Point(10, 353);
            this.ultraLabel28.Name = "ultraLabel28";
            this.ultraLabel28.Size = new System.Drawing.Size(89, 22);
            this.ultraLabel28.TabIndex = 3044;
            this.ultraLabel28.Text = "ĐT tập hợp CP ";
            // 
            // cbbStatisticsCodeID
            // 
            this.cbbStatisticsCodeID.AutoSize = false;
            appearance118.BackColor = System.Drawing.SystemColors.Window;
            appearance118.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbStatisticsCodeID.DisplayLayout.Appearance = appearance118;
            this.cbbStatisticsCodeID.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbStatisticsCodeID.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.cbbStatisticsCodeID.DisplayLayout.DefaultSelectedForeColor = System.Drawing.Color.Black;
            appearance119.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance119.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance119.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance119.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbStatisticsCodeID.DisplayLayout.GroupByBox.Appearance = appearance119;
            appearance120.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbStatisticsCodeID.DisplayLayout.GroupByBox.BandLabelAppearance = appearance120;
            this.cbbStatisticsCodeID.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance121.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance121.BackColor2 = System.Drawing.SystemColors.Control;
            appearance121.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance121.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbStatisticsCodeID.DisplayLayout.GroupByBox.PromptAppearance = appearance121;
            this.cbbStatisticsCodeID.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbStatisticsCodeID.DisplayLayout.MaxRowScrollRegions = 1;
            appearance122.BackColor = System.Drawing.SystemColors.Window;
            appearance122.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbStatisticsCodeID.DisplayLayout.Override.ActiveCellAppearance = appearance122;
            appearance123.BackColor = System.Drawing.SystemColors.Highlight;
            appearance123.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbStatisticsCodeID.DisplayLayout.Override.ActiveRowAppearance = appearance123;
            this.cbbStatisticsCodeID.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbStatisticsCodeID.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance124.BackColor = System.Drawing.SystemColors.Window;
            this.cbbStatisticsCodeID.DisplayLayout.Override.CardAreaAppearance = appearance124;
            appearance125.BorderColor = System.Drawing.Color.Silver;
            appearance125.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbStatisticsCodeID.DisplayLayout.Override.CellAppearance = appearance125;
            this.cbbStatisticsCodeID.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbStatisticsCodeID.DisplayLayout.Override.CellPadding = 0;
            appearance126.BackColor = System.Drawing.SystemColors.Control;
            appearance126.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance126.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance126.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance126.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbStatisticsCodeID.DisplayLayout.Override.GroupByRowAppearance = appearance126;
            appearance127.TextHAlignAsString = "Left";
            this.cbbStatisticsCodeID.DisplayLayout.Override.HeaderAppearance = appearance127;
            this.cbbStatisticsCodeID.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbStatisticsCodeID.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance128.BackColor = System.Drawing.SystemColors.Window;
            appearance128.BorderColor = System.Drawing.Color.Silver;
            this.cbbStatisticsCodeID.DisplayLayout.Override.RowAppearance = appearance128;
            this.cbbStatisticsCodeID.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance129.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbStatisticsCodeID.DisplayLayout.Override.TemplateAddRowAppearance = appearance129;
            this.cbbStatisticsCodeID.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbStatisticsCodeID.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbStatisticsCodeID.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbStatisticsCodeID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbStatisticsCodeID.Location = new System.Drawing.Point(116, 386);
            this.cbbStatisticsCodeID.Name = "cbbStatisticsCodeID";
            this.cbbStatisticsCodeID.Size = new System.Drawing.Size(194, 22);
            this.cbbStatisticsCodeID.TabIndex = 3047;
            this.cbbStatisticsCodeID.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbStatisticsCodeID_RowSelected);
            this.cbbStatisticsCodeID.Validating += new System.ComponentModel.CancelEventHandler(this.cbbStatisticsCodeID_Validating);
            // 
            // ultraLabel8
            // 
            appearance130.BackColor = System.Drawing.Color.Transparent;
            appearance130.TextVAlignAsString = "Middle";
            this.ultraLabel8.Appearance = appearance130;
            this.ultraLabel8.Location = new System.Drawing.Point(10, 386);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(89, 22);
            this.ultraLabel8.TabIndex = 3046;
            this.ultraLabel8.Text = "Mã thống kê";
            // 
            // txtFixedAssetCode
            // 
            this.txtFixedAssetCode.AutoSize = false;
            this.txtFixedAssetCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.txtFixedAssetCode.Location = new System.Drawing.Point(116, 10);
            this.txtFixedAssetCode.MaxLength = 512;
            this.txtFixedAssetCode.Name = "txtFixedAssetCode";
            this.txtFixedAssetCode.Size = new System.Drawing.Size(194, 22);
            this.txtFixedAssetCode.TabIndex = 3007;
            // 
            // chkActive
            // 
            appearance131.TextVAlignAsString = "Middle";
            this.chkActive.Appearance = appearance131;
            this.chkActive.BackColor = System.Drawing.Color.Transparent;
            this.chkActive.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkActive.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkActive.Location = new System.Drawing.Point(12, 435);
            this.chkActive.Name = "chkActive";
            this.chkActive.Size = new System.Drawing.Size(103, 22);
            this.chkActive.TabIndex = 3048;
            this.chkActive.Text = "Ngừng theo dõi";
            // 
            // FFAInitDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(665, 471);
            this.Controls.Add(this.chkActive);
            this.Controls.Add(this.txtFixedAssetCode);
            this.Controls.Add(this.cbbStatisticsCodeID);
            this.Controls.Add(this.ultraLabel8);
            this.Controls.Add(this.cbbCostSet);
            this.Controls.Add(this.ultraLabel28);
            this.Controls.Add(this.cbbBudgetItem);
            this.Controls.Add(this.ultraLabel7);
            this.Controls.Add(this.cbbDepreciationAccount);
            this.Controls.Add(this.ultraLabel26);
            this.Controls.Add(this.cbbExpenditureAccount);
            this.Controls.Add(this.ultraLabel27);
            this.Controls.Add(this.cbbOriginalPriceAccount);
            this.Controls.Add(this.ultraLabel25);
            this.Controls.Add(this.nmrMonthPeriodDepreciationAmount);
            this.Controls.Add(this.ultraLabel36);
            this.Controls.Add(this.nmrRemainingAmount);
            this.Controls.Add(this.ultraLabel37);
            this.Controls.Add(this.nmrAcDepreciationAmount);
            this.Controls.Add(this.ultraLabel38);
            this.Controls.Add(this.nmrPurchasePrice);
            this.Controls.Add(this.ultraLabel30);
            this.Controls.Add(this.nmrOriginalPrice);
            this.Controls.Add(this.ultraLabel29);
            this.Controls.Add(this.nmrUsedTimeRemain);
            this.Controls.Add(this.cbbYearMonth2);
            this.Controls.Add(this.ultraLabel6);
            this.Controls.Add(this.nmrUsedTime);
            this.Controls.Add(this.CbbYearMonth);
            this.Controls.Add(this.ultraLabel32);
            this.Controls.Add(this.cldDepreciationDate);
            this.Controls.Add(this.ultraLabel4);
            this.Controls.Add(this.cldIncrementDate);
            this.Controls.Add(this.ultraLabel22);
            this.Controls.Add(this.cbbDepartmentID);
            this.Controls.Add(this.ultraLabel5);
            this.Controls.Add(this.cbbFixedAssetCategoryID);
            this.Controls.Add(this.ultraLabel1);
            this.Controls.Add(this.txtFixedAssetName);
            this.Controls.Add(this.ultraLabel2);
            this.Controls.Add(this.ultraLabel3);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FFAInitDetail";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thông tin tài sản cố định";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FFAInitDetail_FormClosed);
            this.tsmFixedAssetDetail.ResumeLayout(false);
            this.tsmFixedAssetAccessories.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbFixedAssetCategoryID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFixedAssetName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDepartmentID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cldIncrementDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cldDepreciationDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbbYearMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbYearMonth2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbOriginalPriceAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbExpenditureAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDepreciationAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBudgetItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCostSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbStatisticsCodeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFixedAssetCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkActive)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Core.Domain.FixedAsset temp;
        private System.Windows.Forms.ContextMenuStrip tsmFixedAssetAccessories;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripMenuItem tsmDelete;
        private System.Windows.Forms.ContextMenuStrip tsmFixedAssetDetail;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd1;
        private System.Windows.Forms.ToolStripMenuItem tsmDelete1;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbFixedAssetCategoryID;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtFixedAssetName;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbDepartmentID;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor cldIncrementDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel22;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor cldDepreciationDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit nmrUsedTime;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor CbbYearMonth;
        private Infragistics.Win.Misc.UltraLabel ultraLabel32;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit nmrUsedTimeRemain;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbYearMonth2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit nmrOriginalPrice;
        private Infragistics.Win.Misc.UltraLabel ultraLabel29;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit nmrPurchasePrice;
        private Infragistics.Win.Misc.UltraLabel ultraLabel30;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit nmrAcDepreciationAmount;
        private Infragistics.Win.Misc.UltraLabel ultraLabel38;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit nmrRemainingAmount;
        private Infragistics.Win.Misc.UltraLabel ultraLabel37;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit nmrMonthPeriodDepreciationAmount;
        private Infragistics.Win.Misc.UltraLabel ultraLabel36;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbOriginalPriceAccount;
        private Infragistics.Win.Misc.UltraLabel ultraLabel25;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbExpenditureAccount;
        private Infragistics.Win.Misc.UltraLabel ultraLabel27;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbDepreciationAccount;
        private Infragistics.Win.Misc.UltraLabel ultraLabel26;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbBudgetItem;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbCostSet;
        private Infragistics.Win.Misc.UltraLabel ultraLabel28;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbStatisticsCodeID;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtFixedAssetCode;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkActive;
    }
}