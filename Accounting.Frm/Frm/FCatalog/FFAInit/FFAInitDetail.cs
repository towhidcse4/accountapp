﻿using System;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinEditors;
using Itenso.TimePeriod;
using System.Collections.Generic;
using System.Linq;

namespace Accounting
{
    public partial class FFAInitDetail : DialogForm
    {
        #region khai báo
        private readonly IAccountService _IAccountService = Utils.IAccountService;
        private readonly IFAInitService _IFAInitService = Utils.IFAInitService;
        private readonly IFixedAssetService _IFixedAssetService = Utils.IFixedAssetService;
        private readonly IFixedAssetLedgerService _IFixedAssetLedgerService = Utils.IFixedAssetLedgerService;
        private readonly IOPFixedAssetService _IOPFixedAssetService = Utils.IOPFixedAssetService;
        private readonly ISystemOptionService _SystemOptionService = Utils.ISystemOptionService;
        private readonly IOPAccountService _IOPAccountService = Utils.IOPAccountService;
        private readonly IGeneralLedgerService _IGeneralLedgerService = Utils.IGeneralLedgerService;

        FixedAsset _Select = new FixedAsset();
        public static bool isClose = true;
        private bool _forceReload = false;
        private bool Them = true;
        private DateTime TCKHAC_NgayBDNamTC = DateTime.Now;
        private DateTime ApplicationStartDate = DateTime.Now;
        private bool isNewAccount = false;
        private FixedAsset oldTemp = new FixedAsset();
        List<FixedAssetCategory> lstfac = new List<FixedAssetCategory>();
        bool OldIsActive = false;
        #endregion

        #region khởi tạo
        public FFAInitDetail()
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion
            this.Text = @"Khai báo Tài sản cố định";
            #region  config cho các control
            InitializeGUI();
            #endregion

        }
       

        public FFAInitDetail(FixedAsset temp)
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            this.Text = @"Khai báo tài sản cố định";
            #region Thiết lập ban đầu cho Form
            _Select = temp;
            Them = false;
            #endregion

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            oldTemp = temp.CloneObject();
            OldIsActive = temp.CloneObject().IsActive;
            #region Fill dữ liệu Obj vào control
            FAInit fAInit = _IFAInitService.FindByFixedAssetID(temp.ID);
            if (fAInit != null)
            {
                temp.AcDepreciationAmount = fAInit.AcDepreciationAmount;
                temp.RemainingAmount = fAInit.RemainingAmount;
            }
            ObjandGUI(temp, false);
            #endregion
        }
        #endregion

        #region Event
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!Them)
            {
                if (_IFAInitService.CheckDeleteFAInit(_Select.ID))
                {
                    if (OldIsActive != chkActive.Checked)
                    {

                        var model = _IFixedAssetService.Getbykey(_Select.ID);
                        model.IsActive = chkActive.Checked;
                        _IFixedAssetService.BeginTran();
                        try
                        {
                            _IFixedAssetService.Update(model);
                            _IFixedAssetService.CommitTran();
                        }
                        catch (Exception ex)
                        {
                            _IFixedAssetService.RolbackTran();
                        }
                        isClose = false;
                        _forceReload = true;
                        DialogResult = DialogResult.OK;
                        this.Close();
                        return;
                    }
                    else
                    {
                        MSG.Warning("Không thể sửa thông tin của tài sản này tại đây vì đã phát sinh chứng từ liên quan. Vui lòng thực hiện việc sửa đổi tại nghiệp vụ Điều chỉnh Tài sản cố định");
                        return;
                    }
                }
            }
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion

            //Gui to object
            #region Fill dữ liệu control vào obj
            FixedAsset temp = Them ? new FixedAsset() : _IFixedAssetService.Getbykey(_Select.ID);
            temp = ObjandGUI(temp, true);
            _Select = temp;

            #endregion
            try
            {
                #region Thao tác CSDL
                // Lưu dữ liệu vào 6 bảng bao gổm : FixedAsset, FaInit, OPFixedAsset, FixedAssetLedger, OPAccount, GL
                #region FixedAsset
                _IFixedAssetService.BeginTran();
                if (Them)
                {
                    _IFixedAssetService.CreateNew(temp);
                }
                else
                {
                    _IFixedAssetService.Update(temp);
                }

                #endregion
                #region FaInit
                FAInit fai = Them ? new FAInit() : _IFAInitService.FindByFixedAssetID(_Select.ID);
                fai = CreateFAI(temp, fai);
                if (Them)
                {
                    _IFAInitService.CreateNew(fai);
                }
                else
                {
                    _IFAInitService.Update(fai);
                }

                #endregion
                #region FixedAssetLedger
                FixedAssetLedger fal = Them ? new FixedAssetLedger() : _IFixedAssetLedgerService.FindByFixedAssetID(_Select.ID);
                fal = CreateFAL(temp, fai.ID, fal, fai);
                if (Them)
                {
                    _IFixedAssetLedgerService.CreateNew(fal);
                }
                else
                {
                    _IFixedAssetLedgerService.Update(fal);
                }

                #endregion
                #region OPFixedAsset
                OPFixedAsset opfa = Them ? new OPFixedAsset() : _IOPFixedAssetService.FindByFixedAssetID(_Select.ID);
                opfa = CreateOPFA(temp, opfa);
                if (Them)
                {
                    _IOPFixedAssetService.CreateNew(opfa);
                }
                else
                {
                    _IOPFixedAssetService.Update(opfa);
                }

                #endregion
                #region OPACCOUNT
                //OPAccount oPAccount = CreateOPAccount(temp.OriginalPriceAccount);
                //if (isNewAccount)
                //{
                //    _IOPAccountService.CreateNew(oPAccount);
                //}
                //else
                //{
                //    _IOPAccountService.Update(oPAccount);
                //}

                //OPAccount oPAccount2 = CreateOPAccount(temp.DepreciationAccount);
                //if (isNewAccount)
                //{
                //    _IOPAccountService.CreateNew(oPAccount2);
                //}
                //else
                //{
                //    _IOPAccountService.Update(oPAccount2);
                //}


                #endregion
                #region GenenalLegder
                //GeneralLedger GeneralLedger = CreateGeneralLedger(temp.OriginalPriceAccount, fai);
                //if (isNewAccount)
                //{
                //    _IGeneralLedgerService.CreateNew(GeneralLedger);
                //}
                //else
                //{
                //    _IGeneralLedgerService.Update(GeneralLedger);
                //}

                //GeneralLedger GeneralLedger2 = CreateGeneralLedger(temp.DepreciationAccount, fai);
                //if (isNewAccount)
                //{
                //    _IGeneralLedgerService.CreateNew(GeneralLedger2);
                //}
                //else
                //{
                //    _IGeneralLedgerService.Update(GeneralLedger2);
                //}                //GeneralLedger GeneralLedger = CreateGeneralLedger(temp.OriginalPriceAccount, fai);
                //if (isNewAccount)
                //{
                //    _IGeneralLedgerService.CreateNew(GeneralLedger);
                //}
                //else
                //{
                //    _IGeneralLedgerService.Update(GeneralLedger);
                //}

                //GeneralLedger GeneralLedger2 = CreateGeneralLedger(temp.DepreciationAccount, fai);
                //if (isNewAccount)
                //{
                //    _IGeneralLedgerService.CreateNew(GeneralLedger2);
                //}
                //else
                //{
                //    _IGeneralLedgerService.Update(GeneralLedger2);
                //}


                #endregion
                #region commit and clear cache
                _IFixedAssetService.CommitTran();
                Utils.ClearCacheByType<FixedAsset>();
                Utils.ClearCacheByType<FAInit>();
                Utils.ClearCacheByType<FixedAssetLedger>();
                Utils.ClearCacheByType<OPFixedAsset>();
                #endregion

                #endregion

                #region xử lý form, reset form
                isClose = false;
                _forceReload = true;
                // reset form
                DialogResult = DialogResult.OK;
                this.Close();
                #endregion
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                _IFixedAssetService.RolbackTran();
            }
        }

        private GeneralLedger CreateGeneralLedger(string originalPriceAccount, FAInit fai)
        {
            isNewAccount = false;
            GeneralLedger GeneralLedger = _IGeneralLedgerService.FindByAccountNumber(originalPriceAccount);
            if (GeneralLedger == null)
            {
                isNewAccount = true;
                GeneralLedger = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    TypeID = 701,
                    PostedDate = ApplicationStartDate.AddDays(-1),
                    Account = originalPriceAccount,
                    CurrencyID = "VND",
                    ExchangeRate = 1,
                    OrderPriority = _IGeneralLedgerService.FindLastPriority() + 1,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    RefDate = fai.PostedDate,
                    RefNo = "OPN",
                    Date = ApplicationStartDate.AddDays(-1),
                    InvoiceDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),
                    No = "OPN"
                };

            }
            if (originalPriceAccount.StartsWith("211"))
            {
                GeneralLedger.DebitAmount = GeneralLedger.DebitAmount + (_Select.PurchasePrice ?? 0) - (oldTemp.PurchasePrice ?? 0);
                GeneralLedger.DebitAmountOriginal = GeneralLedger.DebitAmountOriginal + (_Select.PurchasePrice ?? 0 - oldTemp.PurchasePrice ?? 0);
            }
            else if (originalPriceAccount.StartsWith("214"))
            {
                GeneralLedger.CreditAmount = GeneralLedger.CreditAmount + (_Select.AcDepreciationAmount ?? 0) - (oldTemp.AcDepreciationAmount ?? 0);
                GeneralLedger.CreditAmountOriginal = GeneralLedger.CreditAmountOriginal + (_Select.AcDepreciationAmount ?? 0) - (oldTemp.AcDepreciationAmount ?? 0);
            }
            return GeneralLedger;
        }

        private OPAccount CreateOPAccount(string originalPriceAccount)
        {
            isNewAccount = false;
            OPAccount OPAccount = _IOPAccountService.FindByAccountNumber(originalPriceAccount);
            if (OPAccount == null)
            {
                isNewAccount = true;
                OPAccount = new OPAccount
                {
                    ID = new Guid(),
                    TypeID = 701,
                    PostedDate = ApplicationStartDate.AddDays(-1),
                    AccountNumber = originalPriceAccount,
                    CurrencyID = "VND",
                    ExchangeRate = 1,
                    OrderPriority = _IOPAccountService.FindLastPriority() + 1,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0
                };

            }
            if (originalPriceAccount.StartsWith("211"))
            {
                OPAccount.DebitAmount = OPAccount.DebitAmount + (_Select.PurchasePrice ?? 0) - (oldTemp.PurchasePrice ?? 0);
                OPAccount.DebitAmountOriginal = OPAccount.DebitAmountOriginal + (_Select.PurchasePrice ?? 0) - (oldTemp.PurchasePrice ?? 0);
            }
            else if (originalPriceAccount.StartsWith("214"))
            {
                OPAccount.CreditAmount = OPAccount.CreditAmount + (_Select.AcDepreciationAmount ?? 0) - (oldTemp.AcDepreciationAmount ?? 0);
                OPAccount.CreditAmountOriginal = OPAccount.CreditAmountOriginal + (_Select.AcDepreciationAmount ?? 0) - (oldTemp.AcDepreciationAmount ?? 0);
            }
            return OPAccount;
        }

        private FAInit CreateFAI(FixedAsset temp, FAInit fAInit)
        {
            fAInit.ID = fAInit.ID == Guid.Empty ? Guid.NewGuid() : fAInit.ID;
            fAInit.TypeID = 701;
            fAInit.PostedDate = ApplicationStartDate.AddDays(-1);
            fAInit.FixedAssetID = temp.ID;
            fAInit.DepartmentID = temp.DepartmentID;
            fAInit.IncrementDate = temp.IncrementDate;
            fAInit.DepreciationDate = temp.DepreciationDate;
            fAInit.UsedTime = temp.UsedTime;
            fAInit.UsedTimeRemain = temp.UsedTimeRemain;
            fAInit.OriginalPrice = temp.OriginalPrice;
            fAInit.PurchasePrice = temp.PurchasePrice;
            fAInit.AcDepreciationAmount = temp.AcDepreciationAmount;
            fAInit.RemainingAmount = temp.RemainingAmount;
            fAInit.MonthPeriodDepreciationAmount = temp.MonthPeriodDepreciationAmount;
            fAInit.OriginalPriceAccount = temp.OriginalPriceAccount;
            fAInit.ExpenditureAccount = temp.ExpenditureAccount;
            fAInit.DepreciationAccount = temp.DepreciationAccount;
            fAInit.FixedAssetCode = temp.FixedAssetCode;
            fAInit.FixedAssetName = temp.FixedAssetName;
            fAInit.BudgetItemID = temp.BudgetItemID;
            fAInit.CostSetID = temp.CostSetID;
            fAInit.FixedAssetCategoryID = temp.FixedAssetCategoryID;
            fAInit.IsMonthUsedTime = temp.IsDisplayMonth;
            fAInit.IsMonthUsedTimeRemain = temp.IsDisplayMonthRemain;
            fAInit.StatisticsCodeID = temp.StatisticsCodeID;
            return fAInit;
        }

        private OPFixedAsset CreateOPFA(FixedAsset temp, OPFixedAsset opfa)
        {
            opfa.ID = opfa.ID == Guid.Empty ? Guid.NewGuid() : opfa.ID;
            opfa.PostedDate = ApplicationStartDate.AddDays(-1);
            opfa.FixedAssetID = temp.ID;
            opfa.DepartmentID = temp.DepartmentID;
            opfa.OriginalPriceAccount = temp.OriginalPriceAccount;
            opfa.OriginalPriceDebitAmount = temp.PurchasePrice;
            opfa.OriginalPriceDebitAmountOriginal = temp.PurchasePrice;
            opfa.DepreciationAccount = temp.DepreciationAccount;
            opfa.DepreciationCreditAmount = temp.AcDepreciationAmount;
            opfa.DepreciationCreditAmountOriginal = temp.AcDepreciationAmount;
            opfa.TypeID = 701;
            opfa.OrderPriority = _IOPFixedAssetService.FindLastOrderPriority() + 1;

            return opfa;
        }

        private FixedAssetLedger CreateFAL(FixedAsset temp, Guid faInitID, FixedAssetLedger fal, FAInit fAInit)
        {
            fal.ID = fal.ID == Guid.Empty ? Guid.NewGuid() : fal.ID;
            fal.TypeID = 701;
            fal.No = "OPN";
            fal.Date = fAInit.IncrementDate ?? DateTime.Now;
            fal.PostedDate = fAInit.IncrementDate ?? DateTime.Now;
            fal.FixedAssetID = temp.ID;
            fal.DepartmentID = temp.DepartmentID;
            fal.UsedTime = temp.IsDisplayMonth ? temp.UsedTime : temp.UsedTime * 12;
            fal.UsedTimeRemain = temp.IsDisplayMonth ? temp.UsedTimeRemain : temp.UsedTimeRemain * 12;
            fal.DepreciationRate = temp.DepreciationRate;
            fal.MonthDepreciationRate = temp.MonthDepreciationRate;
            fal.DepreciationAmount = temp.PurchasePrice;
            fal.OriginalPriceAccount = temp.OriginalPriceAccount;
            fal.OriginalPriceCreditAmount = temp.PurchasePrice;
            fal.OriginalPriceDebitAmount = temp.PurchasePrice;
            fal.DepreciationAccount = temp.DepreciationAccount;
            fal.DepreciationCreditAmount = temp.AcDepreciationAmount;
            fal.DepreciationDebitAmount = temp.AcDepreciationAmount;
            fal.Reason = temp.Description;
            fal.IsIrrationalCost = temp.IsIrrationalCost;
            fal.OrderPriority = _IFixedAssetLedgerService.FindLastOrderPriority() + 1;
            fal.ReferenceID = faInitID;
            fal.OriginalPrice = fAInit.OriginalPrice;
            fal.RemainingAmount = fAInit.RemainingAmount;
            fal.AcDepreciationAmount = fAInit.AcDepreciationAmount;
            fal.MonthPeriodDepreciationAmount = fAInit.MonthPeriodDepreciationAmount;
            fal.IncrementDate = fAInit.IncrementDate ?? DateTime.Now;
            fal.Nos = "OPN";
            return fal;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            isClose = _forceReload ? false : true;
            DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void InitializeGUI()
        {
            ApplicationStartDate = DateTime.ParseExact(_SystemOptionService.Getbykey(104).Data, "dd/MM/yyyy", null);
            TCKHAC_NgayBDNamTC = DateTime.ParseExact(_SystemOptionService.Getbykey(82).Data, "dd/MM/yyyy", null);
            // tài khoản chỉ phí
            cbbExpenditureAccount.DataSource = _IAccountService.GetListAccountAccountNumberIsActiveWithUpperLever("154;241;631;642;632;811", true, 1);
            cbbExpenditureAccount.DisplayMember = "AccountNumber";
            Utils.ConfigGrid(cbbExpenditureAccount, ConstDatabase.Account_TableName);

            // TK nguyên giá
            cbbOriginalPriceAccount.DataSource = _IAccountService.GetListAccountAccountNumberIsActiveWithUpperLever("211;217", true, 1);
            cbbOriginalPriceAccount.DisplayMember = "AccountNumber";
            Utils.ConfigGrid(cbbOriginalPriceAccount, ConstDatabase.Account_TableName);

            // tài khoản khấu hao
            cbbDepreciationAccount.DataSource = _IAccountService.GetListAccountAccountNumberIsActiveWithUpperLever("214", true, 0);
            cbbDepreciationAccount.DisplayMember = "AccountNumber";
            Utils.ConfigGrid(cbbDepreciationAccount, ConstDatabase.Account_TableName);

            // đối tượng tập hợp chi phí
            cbbCostSet.DataSource = Utils.ListCostSet;
            cbbCostSet.DisplayMember = "CostSetName";
            cbbCostSet.ValueMember = "CostSetName";
            Utils.ConfigGrid<CostSet>(this, cbbCostSet, ConstDatabase.CostSet_TableName, "ParentID", "IsParentNode");

            // phòng ban
            cbbDepartmentID.DataSource = Utils.ListDepartment;
            cbbDepartmentID.DisplayMember = "DepartmentName";
            Utils.ConfigGrid<Department>(this, cbbDepartmentID, ConstDatabase.Department_TableName, "ParentID", "IsParentNode");

            // mã tài sản
            //this.ConfigCombo(Utils.ListFixedAsset, cbbFixedAssetID, "FixedAssetCode", "ID");
            // loại 
            this.ConfigCombo(Utils.ListFixedAssetCategory, cbbFixedAssetCategoryID, "FixedAssetCategoryName", "ID");

            // nguyên giá
            nmrOriginalPrice.FormatNumberic(ConstDatabase.Format_TienVND);

            // giá trị khấu hao
            nmrPurchasePrice.FormatNumberic(ConstDatabase.Format_TienVND);

            // giá trị khấu hao tháng
            nmrMonthPeriodDepreciationAmount.FormatNumberic(ConstDatabase.Format_TienVND);

            // hao mòn lũy kế
            nmrAcDepreciationAmount.FormatNumberic(ConstDatabase.Format_TienVND);

            // giá trị còn lại
            nmrRemainingAmount.FormatNumberic(ConstDatabase.Format_TienVND);

            // thời gian sử dụng
            nmrUsedTime.FormatNumberic(ConstDatabase.Format_Quantity);
            // thời gian sử dụng còn lại
            nmrUsedTimeRemain.FormatNumberic(ConstDatabase.Format_Quantity);
            // ngày ghi tăng
            //cldIncrementDate.Value = Utils.StringToDateTime(Utils.GetDbStartDate());
            CbbYearMonth.SelectedIndex = 1;
            // ngày bắt đầu khấu hao
            //cldDepreciationDate.Value = Utils.StringToDateTime(Utils.GetDbStartDate());
            cbbYearMonth2.SelectedIndex = 1;

            // Mã thống kê
            cbbStatisticsCodeID.DataSource = Utils.ListStatisticsCode;
            cbbStatisticsCodeID.DisplayMember = "StatisticsCode_";
            cbbStatisticsCodeID.ValueMember = "StatisticsCode_";
            Utils.ConfigGrid<StatisticsCode>(this, cbbStatisticsCodeID, ConstDatabase.StatisticsCode_TableName, "ParentID", "IsParentNode");

            // mục thu chi
            cbbBudgetItem.DataSource = Utils.ListBudgetItem;
            cbbBudgetItem.DisplayMember = "BudgetItemName";
            cbbBudgetItem.ValueMember = "BudgetItemName";
            Utils.ConfigGrid<BudgetItem>(this, cbbBudgetItem, ConstDatabase.BudgetItem_TableName, "ParentID", "IsParentNode");
            //this.ConfigCombo(Utils.ListBudgetItem, cbbBudgetItem, "BudgetItemName", "ID");
            this.ConfigCombo(Utils.ListFixedAssetCategory, cbbFixedAssetCategoryID, "FixedAssetCategoryName", "ID");
            this.ConfigCombo(Utils.ListDepartment, cbbDepartmentID, "DepartmentName", "ID");

        }
        void loadFixedAssetCategory()
        {
            this.ConfigCombo(Utils.ListFixedAssetCategory, cbbFixedAssetCategoryID, "FixedAssetCategoryName", "ID");
            this.ConfigCombo(Utils.ListDepartment, cbbDepartmentID, "DepartmentName", "ID");
        }

        private void cbbFixedAssetID_RowSelected(object sender, Infragistics.Win.UltraWinGrid.RowSelectedEventArgs e)
        {
            if (e.Row != null && e.Row.ListObject != null)
            {
                FixedAsset asset = e.Row.ListObject as FixedAsset;
                txtFixedAssetName.Text = asset.FixedAssetName;
                cbbFixedAssetCategoryID.Value = asset.FixedAssetCategoryID;
            }
        }

        //check du lieu
        bool CheckError()
        {
            if (string.IsNullOrEmpty(txtFixedAssetCode.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                txtFixedAssetCode.Focus();
                return false;
            }
            else if (_IFixedAssetService.CheckExistFA(txtFixedAssetCode.Text, _Select.ID) > 0)
            {
                MSG.Error(string.Format(resSystem.MSG_Catalog_Account6, "Tài sản cố định"));
                txtFixedAssetCode.Focus();
                return false;
            }
            else if (string.IsNullOrEmpty(txtFixedAssetName.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                txtFixedAssetName.Focus();
                return false;
            }
            if (!CheckDateTime(cldIncrementDate, true))
            {
                MSG.Warning(resSystem.MSG_Catalog_FFixedAssetDetai20);
                return false;
            }
            if (!CheckDateTime(cldIncrementDate, true))
            {
                MSG.Warning(resSystem.MSG_Catalog_FFixedAssetDetai21);
                return false;
            }
            if (cbbFixedAssetCategoryID.Text.Trim() == "")
            {
                MSG.Error(resSystem.MSG_System_03);
                cbbFixedAssetCategoryID.Focus();
                return false;
            }
            if (cbbDepartmentID.Text.Trim() == "")
            {
                MSG.Warning(resSystem.MSG_Catalog_FFixedAssetDetail8);
                cbbDepartmentID.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(cbbOriginalPriceAccount.Text))
            {
                MSG.Warning(resSystem.MSG_Catalog_FFixedAssetDetail9);
                cbbOriginalPriceAccount.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(cbbDepreciationAccount.Text))
            {
                MSG.Warning(resSystem.MSG_Catalog_FFixedAssetDetail10);
                cbbDepreciationAccount.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(cbbExpenditureAccount.Text))
            {
                MSG.Warning(resSystem.MSG_Catalog_FFixedAssetDetail11);
                cbbExpenditureAccount.Focus();
                return false;
            }
            if (cldIncrementDate.Value == null)
            {
                MSG.Warning(resSystem.MSG_Catalog_FFixedAssetDetail22);
                cldIncrementDate.Focus();
                return false;
            }
            if (cldDepreciationDate.Value == null)
            {
                MSG.Warning(resSystem.MSG_Catalog_FFixedAssetDetail23);
                cldDepreciationDate.Focus();
                return false;
            }
            else if ((cldIncrementDate.Value != null && cldDepreciationDate.Value != null)
                || (cldIncrementDate.NullText != "Chọn ngày tháng" && cldDepreciationDate.NullText != "Chọn ngày tháng"))
            {
                DateTime ghiTang = DateTime.Parse(cldIncrementDate.Value.ToString());
                if (cldDepreciationDate.Value != null)
                {
                    DateTime khauHao = DateTime.Parse(cldDepreciationDate.Value.ToString());
                    if (DateTime.Compare(khauHao, ghiTang) < 0)
                    {
                        MSG.Error(resSystem.MSG_Catalog_FFixedAssetDetail2);
                        cldDepreciationDate.Focus();
                        return false;
                    }
                }
                // Ngày ghi tăng phải nhỏ hơn ngày sử dụng phần mềm
                if (DateTime.Compare(TCKHAC_NgayBDNamTC, ghiTang) <= 0)
                {
                    MSG.Error(resSystem.MSG_Catalog_FFixedAssetDetail24);
                    cldIncrementDate.Focus();
                    return false;
                }
            }

            return true;
        }

        bool CheckDateTime(UltraDateTimeEditor cld, bool dk)
        {
            DateTime dt;
            if (cld.Value != null)
            {
                try
                {
                    dt = DateTime.Parse(cld.Value.ToString());
                    return true;
                }
                catch (Exception)
                {
                    if (dk)
                        MSG.Error(resSystem.MSG_Catalog_FCostSetDetail6);
                    return false;
                }
            }
            return dk;
        }

        FixedAsset ObjandGUI(FixedAsset input, bool isGet)
        {
            if (isGet)
            {
                input.IsActive = chkActive.Checked;
                input.No = "OPN";
                input.PostedDate = input.PostedDate == null ? DateTime.Now : input.PostedDate;
                // bind data từ control vào object

                // Tạo mới Guid 
                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();

                // loại tài sản
                if (!string.IsNullOrEmpty(cbbFixedAssetCategoryID.Text))
                {
                    input.FixedAssetCategoryID = ((FixedAssetCategory)Utils.getSelectCbbItem(cbbFixedAssetCategoryID)).ID;
                }

                // mã phòng ban
                if (!string.IsNullOrEmpty(cbbDepartmentID.Text))
                {
                    input.DepartmentID = ((Department)Utils.getSelectCbbItem(cbbDepartmentID)).ID;
                }

                // mã tài sản
                input.FixedAssetCode = txtFixedAssetCode.Text;

                // tên tài sản
                input.FixedAssetName = txtFixedAssetName.Text;

                // Ngày ghi tăng
                if (CheckDateTime(cldIncrementDate, false))
                {
                    input.IncrementDate = DateTime.Parse(cldIncrementDate.Value.ToString());
                }
                else
                {
                    input.IncrementDate = null;
                }

                // ngày bđ khấu hao
                if (CheckDateTime(cldDepreciationDate, false))
                {
                    input.DepreciationDate = DateTime.Parse(cldDepreciationDate.Value.ToString());
                }
                else
                {
                    input.DepreciationDate = null;
                }

                // Thời gian sử dụng
                if (nmrUsedTime.Value != null)
                {
                    try
                    {
                        input.UsedTime = Utils.decimalTryParse(nmrUsedTime.Value.ToString());
                    }
                    catch (Exception)
                    {

                    }
                }
                if (CbbYearMonth.SelectedIndex == 1)
                {
                    input.IsDisplayMonth = false;
                }
                else
                {
                    input.IsDisplayMonth = true;
                }

                // thời gian sử dụng còn lại 
                if (nmrUsedTimeRemain.Value != null)
                {
                    try
                    {
                        input.UsedTimeRemain = Utils.decimalTryParse(nmrUsedTimeRemain.Value.ToString());
                    }
                    catch (Exception)
                    {

                    }
                }
                if (cbbYearMonth2.SelectedIndex == 1)
                {
                    input.IsDisplayMonthRemain = false;
                }
                else
                {
                    input.IsDisplayMonthRemain = true;
                }

                // nguyên giá
                if (nmrOriginalPrice.Value != null)
                {
                    input.OriginalPrice = Utils.decimalTryParse(nmrOriginalPrice.Value.ToString());
                }

                // Giá trị tính khấu hao
                if ((nmrPurchasePrice.Value != null) && (nmrPurchasePrice.Text.Trim() != ""))
                {
                    input.PurchasePrice = Utils.decimalTryParse(nmrPurchasePrice.Value.ToString());
                }

                //hao mòn lũy kế
                if ((nmrAcDepreciationAmount.Value != null) && (nmrAcDepreciationAmount.Text.Trim() != ""))
                {
                    input.AcDepreciationAmount = Utils.decimalTryParse(nmrAcDepreciationAmount.Value.ToString());
                }

                // giá trị còn lại
                if ((nmrRemainingAmount.Value != null) && (nmrRemainingAmount.Text.Trim() != ""))
                {
                    input.RemainingAmount = Utils.decimalTryParse(nmrRemainingAmount.Value.ToString());
                }

                // giá trị khấu hao tháng
                if ((nmrMonthPeriodDepreciationAmount.Value != null) && (nmrMonthPeriodDepreciationAmount.Text.Trim() != ""))
                {
                    input.MonthPeriodDepreciationAmount = Utils.decimalTryParse(nmrMonthPeriodDepreciationAmount.Value.ToString());
                }

                // tài khoản khấu hao
                if (!string.IsNullOrEmpty(cbbDepreciationAccount.Text))
                {
                    input.DepreciationAccount = ((Account)Utils.getSelectCbbItem(cbbDepreciationAccount)).AccountNumber;
                }

                // tài khoản nguyên giá
                if (!string.IsNullOrEmpty(cbbOriginalPriceAccount.Text))
                {
                    input.OriginalPriceAccount = ((Account)Utils.getSelectCbbItem(cbbOriginalPriceAccount)).AccountNumber;
                }

                // tài khoản chi phí
                if (!string.IsNullOrEmpty(cbbExpenditureAccount.Text))
                {
                    input.ExpenditureAccount = ((Account)Utils.getSelectCbbItem(cbbExpenditureAccount)).AccountNumber;
                }

                // Đối tượng tập hợp chi phí
                if (!string.IsNullOrEmpty(cbbCostSet.Text))
                {
                    input.CostSetID = ((CostSet)Utils.getSelectCbbItem(cbbCostSet)).ID;
                }

                // Mục thu chi
                if (!string.IsNullOrEmpty(cbbBudgetItem.Text))
                {
                    input.BudgetItemID = ((BudgetItem)Utils.getSelectCbbItem(cbbBudgetItem)).ID;
                }

                // Mã thống kê
                if (!string.IsNullOrEmpty(cbbStatisticsCodeID.Text))
                {
                    input.StatisticsCodeID = ((StatisticsCode)Utils.getSelectCbbItem(cbbStatisticsCodeID)).ID;
                }
                if (input.UsedTime != 0)
                {

                    input.MonthDepreciationRate = Math.Round((1 / (input.IsDisplayMonth ? input.UsedTime : input.UsedTime * 12) * 100) ?? 0, 2);
                    input.DepreciationRate = Math.Round((1 / (input.IsDisplayMonth ? input.UsedTime : input.UsedTime * 12) * 1200) ?? 0, 2);
                }
            }
            else
            {
                // binding data từ object vào control

                // mã tài sản
                //foreach (var item in cbbFixedAssetID.Rows)
                //{
                //    if (((item.ListObject as FixedAsset).ID == input.FixedAssetID))
                //    {
                //        cbbFixedAssetID.SelectedRow = item;
                //        break;
                //    }
                //}
                txtFixedAssetCode.Text = input.FixedAssetCode;
                chkActive.Checked = input.IsActive;
                // tên tài sản
                txtFixedAssetName.Text = input.FixedAssetName;

                // loại tài sản
                if (input.FixedAssetCategoryID != null)
                {
                    foreach (var item in cbbFixedAssetCategoryID.Rows)
                    {
                        if (((item.ListObject as FixedAssetCategory).ID == input.FixedAssetCategoryID))
                        {
                            cbbFixedAssetCategoryID.SelectedRow = item;
                            break;
                        }
                    }
                }

                // phòng ban
                if (input.DepartmentID != null)
                {
                    foreach (var item in cbbDepartmentID.Rows)
                    {
                        if ((item.ListObject as Department).ID == input.DepartmentID)
                        {
                            cbbDepartmentID.SelectedRow = item;
                            break;
                        }
                    }
                }

                // ngày ghi tăng
                if (input.IncrementDate != null)
                {
                    cldIncrementDate.Value = input.IncrementDate;
                }

                // ngày bđ khấu hao
                if (input.DepreciationDate != null)
                {
                    cldDepreciationDate.Value = input.DepreciationDate;
                }

                // thời gian sử dụng
                if (input.UsedTime != null)
                {
                    nmrUsedTime.Value = input.UsedTime;
                }
                if (input.IsDisplayMonth)
                {
                    CbbYearMonth.SelectedIndex = 0;
                }

                // thời gian sử dụng còn lại
                if (input.UsedTimeRemain != null)
                {
                    nmrUsedTimeRemain.Value = input.UsedTimeRemain;
                }
                if (input.IsDisplayMonthRemain)
                {
                    cbbYearMonth2.SelectedIndex = 0;
                }

                // nguyên giá
                if (input.OriginalPrice != null)
                {
                    nmrOriginalPrice.Value = input.OriginalPrice;
                }

                // giá trị tính khấu hao
                if (input.PurchasePrice != null)
                {
                    nmrPurchasePrice.Value = input.PurchasePrice;
                }

                // hao mòn lũy kế
                if (input.AcDepreciationAmount != null)
                {
                    nmrAcDepreciationAmount.Value = input.AcDepreciationAmount;
                }

                // giá trị còn lại
                if (input.RemainingAmount != null)
                {
                    nmrRemainingAmount.Value = input.RemainingAmount;
                }

                // giá trị khấu hao tháng
                if (input.MonthPeriodDepreciationAmount != null)
                {
                    nmrMonthPeriodDepreciationAmount.Value = input.MonthPeriodDepreciationAmount;
                }

                // tài khoản nguyên giá
                if (input.OriginalPriceAccount != null)
                {
                    foreach (var item in cbbOriginalPriceAccount.Rows)
                    {
                        if ((item.ListObject as Account).AccountNumber == input.OriginalPriceAccount)
                        {
                            cbbOriginalPriceAccount.SelectedRow = item;
                            break;
                        }
                    }
                }

                // tài khoản khấu hao
                if (input.DepreciationAccount != null)
                {
                    foreach (var item in cbbDepreciationAccount.Rows)
                    {
                        if ((item.ListObject as Account).AccountNumber == input.DepreciationAccount)
                        {
                            cbbDepreciationAccount.SelectedRow = item;
                            break;
                        }
                    }
                }

                // tài khoản chi phí
                if (input.ExpenditureAccount != null)
                {
                    foreach (var item in cbbExpenditureAccount.Rows)
                    {
                        if ((item.ListObject as Account).AccountNumber == input.ExpenditureAccount)
                        {
                            cbbExpenditureAccount.SelectedRow = item;
                            break;
                        }
                    }

                }

                // Đối tượng tập hợp chi phi
                if (input.CostSetID != null)
                {
                    foreach (var item in cbbCostSet.Rows)
                    {
                        if ((item.ListObject as CostSet).ID == input.CostSetID)
                        {
                            cbbCostSet.SelectedRow = item;
                            break;
                        }
                    }
                }

                // mục thu chi
                if (input.CostSetID != null)
                {
                    foreach (var item in cbbCostSet.Rows)
                    {
                        if ((item.ListObject as CostSet).ID == input.CostSetID)
                        {
                            cbbCostSet.SelectedRow = item;
                            break;
                        }
                    }
                }

                // mã thống kê
                if (input.StatisticsCodeID != null)
                {
                    foreach (var item in cbbStatisticsCodeID.Rows)
                    {
                        if ((item.ListObject as StatisticsCode).ID == input.StatisticsCodeID)
                        {
                            cbbStatisticsCodeID.SelectedRow = item;
                            break;
                        }
                    }
                }

                // mục thu chi
                if (input.BudgetItemID != null)
                {
                    foreach (var item in cbbBudgetItem.Rows)
                    {
                        if ((item.ListObject as BudgetItem).ID == input.BudgetItemID)
                        {
                            cbbBudgetItem.SelectedRow = item;
                            break;
                        }
                    }
                }
            }
            return input;
        }

        private void nmrOriginalPrice_ValueChanged(object sender, EventArgs e)
        {
            nmrPurchasePrice.Text = nmrOriginalPrice.Text;
            CalculateAcDepreciationAmount();
        }

        private void nmrPurchasePrice_ValueChanged(object sender, EventArgs e)
        {
            CalculateRemainAccount(nmrPurchasePrice.Text, nmrAcDepreciationAmount.Text);
            CalculateMonthPeriodDepreciationAmount();
            CalculateAcDepreciationAmount();
        }

        private void nmrAcDepreciationAmount_ValueChanged(object sender, EventArgs e)
        {
            CalculateRemainAccount(nmrPurchasePrice.Text, nmrAcDepreciationAmount.Text);

        }

        public void CalculateRemainAccount(string purchasePrice, string AcDepreciationAmount)
        {
            try

            {
                var _nmrPurchasePrice = string.IsNullOrEmpty(nmrPurchasePrice.Value.ToString()) || nmrPurchasePrice.Value == DBNull.Value ? 0 : nmrPurchasePrice.Value;
                var _nmrAcDepreciationAmount = string.IsNullOrEmpty(nmrAcDepreciationAmount.Value.ToString()) || nmrAcDepreciationAmount.Value == DBNull.Value ? 0 : nmrAcDepreciationAmount.Value;
                nmrRemainingAmount.Value = Convert.ToDecimal(_nmrPurchasePrice) - Convert.ToDecimal(_nmrAcDepreciationAmount);
            }
            catch (Exception)
            {
                nmrRemainingAmount.Value = default(decimal);
            }
        }

        private void nmrUsedTime_ValueChanged(object sender, EventArgs e)
        {
            CalculateMonthPeriodDepreciationAmount();
            SetValueForUsedTimeRemain();
            CalculateAcDepreciationAmount();
        }

        public void CalculateMonthPeriodDepreciationAmount()
        {
            bool isDisplayMounth = false;
            if (CbbYearMonth.SelectedIndex == 0)
                isDisplayMounth = true;
            decimal giatriThang = 0;
            decimal tyLeThang = 0;
            if (!string.IsNullOrEmpty(nmrUsedTime.Value.ToString()) && !string.IsNullOrEmpty(nmrPurchasePrice.Value.ToString()))
            {
                decimal giaTriKhauHao = decimal.Parse(nmrPurchasePrice.Value.ToString());
                if (giaTriKhauHao <= 0)
                    return;

                decimal soThang = 0;
                if (isDisplayMounth)
                {
                    soThang = decimal.Parse(nmrUsedTime.Value.ToString());
                }
                else
                {
                    soThang = decimal.Parse(nmrUsedTime.Value.ToString()) * 12;
                }
                giatriThang = soThang != 0 ? decimal.Parse(nmrPurchasePrice.Value.ToString()) / soThang : 0;
                
                tyLeThang = (giatriThang / decimal.Parse(nmrPurchasePrice.Value.ToString())) * 100;
                nmrMonthPeriodDepreciationAmount.Value = giatriThang.ToString();
            }
        }

        public void CalculateAcDepreciationAmount()
        {
            if (!string.IsNullOrEmpty(nmrMonthPeriodDepreciationAmount.Value.ToString())
                && !string.IsNullOrEmpty(nmrUsedTime.Value.ToString())
                && !string.IsNullOrEmpty(nmrUsedTimeRemain.Value.ToString()))
            {
                // Hao mòn lũy kế = thời gian đã sử dụng theo tháng * giá trị khấu hao tháng 
                var usedTime = nmrUsedTime.Value.ToInt() * (CbbYearMonth.SelectedIndex == 0 ? 1 : 12);
                var usedTimeRemain = nmrUsedTimeRemain.Value.ToInt() * (cbbYearMonth2.SelectedIndex == 0 ? 1 : 12);
                var time = usedTime - usedTimeRemain;
                if (time < 0)
                    nmrAcDepreciationAmount.Value = 0;
                else
                    nmrAcDepreciationAmount.Value = time * decimal.Parse(nmrMonthPeriodDepreciationAmount.Value.ToString());
            }
        }

        private void cldIncrementDate_ValueChanged(object sender, EventArgs e)
        {
            SetValueForUsedTimeRemain();
            CalculateAcDepreciationAmount();
            cldDepreciationDate.Value = cldIncrementDate.Value;
        }

        private void cldDepreciationDate_ValueChanged(object sender, EventArgs e)
        {
            SetValueForUsedTimeRemain();
            CalculateAcDepreciationAmount();
        }

        private void CbbYearMonth_ValueChanged(object sender, EventArgs e)
        {
            SetValueForUsedTimeRemain();
            CalculateAcDepreciationAmount();
        }

        private void cbbYearMonth2_ValueChanged(object sender, EventArgs e)
        {
            SetValueForUsedTimeRemain();
            CalculateAcDepreciationAmount();
        }

        public void SetValueForUsedTimeRemain()
        {
            try
            {
                if (!String.IsNullOrEmpty(cldIncrementDate.Value.ToString()) && !String.IsNullOrEmpty(cldDepreciationDate.Value.ToString())
                    && !String.IsNullOrEmpty(nmrUsedTime.Value.ToString()))
                {
                    DateTime from = DateTime.Parse(cldIncrementDate.Value.ToString());
                    DateTime to = DateTime.Parse(cldDepreciationDate.Value.ToString());
                    var DateDiff = (to.Year - from.Year) * 12 + to.Month - from.Month + 1;
                    var yearMonth = CbbYearMonth.SelectedIndex == 0 ? 1 : 12;
                    var yearMonth2 = cbbYearMonth2.SelectedIndex == 0 ? 1 : 12;

                    //nmrUsedTimeRemain.Text = ((yearMonth * Utils.decimalTryParse(nmrUsedTime.Value.ToString()) - DateDiff) / yearMonth2).ToString();
                }
            }
            catch (Exception)
            {
                //nmrUsedTimeRemain.Text = "";
            }
        }

        private void cbbBudgetItem_RowSelected(object sender, Infragistics.Win.UltraWinGrid.RowSelectedEventArgs e)
        {
            if (e.Row != null && e.Row.ListObject != null)
            {
                BudgetItem BudgetItem = e.Row.ListObject as BudgetItem;
                if (BudgetItem.IsParentNode)
                {
                    cbbBudgetItem.Value = null;
                    MSG.Warning(resSystem.MSG_System_29);
                }
            }
        }

        private void cbbCostSet_RowSelected(object sender, Infragistics.Win.UltraWinGrid.RowSelectedEventArgs e)
        {
            if (e.Row != null && e.Row.ListObject != null)
            {
                CostSet CostSet = e.Row.ListObject as CostSet;
                if (CostSet.IsParentNode)
                {
                    cbbCostSet.Value = null;
                    MSG.Warning(resSystem.MSG_System_29);
                }
            }
        }

        private void cbbStatisticsCodeID_RowSelected(object sender, Infragistics.Win.UltraWinGrid.RowSelectedEventArgs e)
        {
            if (e.Row != null && e.Row.ListObject != null)
            {
                StatisticsCode StatisticsCode = e.Row.ListObject as StatisticsCode;
                if (StatisticsCode.IsParentNode)
                {
                    cbbStatisticsCodeID.Value = null;
                    MSG.Warning(resSystem.MSG_System_29);
                }
            }
        }

        private void cbbDepartmentID_ValueChanged(object sender, EventArgs e)
        {
            var row = cbbDepartmentID.SelectedRow;
            if (row == null) return;
            if (row.Cells["IsParentNode"].Value != null && (bool)row.Cells["IsParentNode"].Value == true)
            {
                cbbDepartmentID.Value = null;
                cbbDepartmentID.ToggleDropdown();
                MSG.Warning(resSystem.MSG_System_29);
            }
        }

        private void cbbBudgetItem_Validated(object sender, EventArgs e)
        {
            bool isContain = false;
            if (cbbBudgetItem.Value == null) return;
            foreach (var item in cbbBudgetItem.Rows)
            {
                if ((item.ListObject as BudgetItem).BudgetItemName == cbbBudgetItem.Value.ToString())
                {
                    cbbBudgetItem.SelectedRow = item;
                    isContain = true;
                    break;
                }
            }
            if (!isContain)
            {
                cbbBudgetItem.Value = "";
                cbbBudgetItem.Focus();
                MSG.Warning("Giá trị không tồn tại");
            }
        }

        private void cbbCostSet_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            bool isContain = false;
            if (cbbCostSet.Value == null) return;
            foreach (var item in cbbCostSet.Rows)
            {
                if ((item.ListObject as CostSet).CostSetName == cbbCostSet.Value.ToString())
                {
                    cbbCostSet.SelectedRow = item;
                    isContain = true;
                    break;
                }
            }
            if (!isContain)
            {
                cbbCostSet.Value = "";
                cbbCostSet.Focus();
                MSG.Warning("Giá trị không tồn tại");
            }
        }

        private void cbbStatisticsCodeID_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            bool isContain = false;
            if (cbbStatisticsCodeID.Value == null) return;
            foreach (var item in cbbStatisticsCodeID.Rows)
            {
                if ((item.ListObject as StatisticsCode).StatisticsCode_ == cbbStatisticsCodeID.Value.ToString())
                {
                    cbbStatisticsCodeID.SelectedRow = item;
                    isContain = true;
                    break;
                }
            }
            if (!isContain)
            {
                cbbStatisticsCodeID.Value = "";
                cbbStatisticsCodeID.Focus();
                MSG.Warning("Giá trị không tồn tại");
            }
        }

        private void cbbOriginalPriceAccount_ItemNotInList(object sender, ValidationErrorEventArgs e)
        {
            Account b = new Account();
            if (cbbOriginalPriceAccount.Text != b.AccountNumber && cbbOriginalPriceAccount.Text != "")
            {
                MSG.Warning("Dữ liệu không có trong danh mục!");
                cbbOriginalPriceAccount.Focus();
                return;
            }
        }

        private void cbbExpenditureAccount_ItemNotInList(object sender, ValidationErrorEventArgs e)
        {
            Account b = new Account();
            if (cbbExpenditureAccount.Text != b.AccountNumber && cbbExpenditureAccount.Text != "")
            {
                MSG.Warning("Dữ liệu không có trong danh mục!");
                cbbExpenditureAccount.Focus();
                return;
            }
        }

        private void cbbDepreciationAccount_ItemNotInList(object sender, ValidationErrorEventArgs e)
        {
            Account b = new Account();
            if (cbbDepreciationAccount.Text != b.AccountNumber && cbbDepreciationAccount.Text != "")
            {
                MSG.Warning("Dữ liệu không có trong danh mục!");
                cbbDepreciationAccount.Focus();
                return;
            }
        }

        private void cbbFixedAssetCategoryID_EditorButtonClick(object sender, EditorButtonEventArgs e)
        {
            new FFixedAssetCategoryDetail().ShowDialog(this);

            Utils.ClearCacheByType<FixedAssetCategory>();
            loadFixedAssetCategory();
            if (FFixedAssetCategoryDetail.FixedCategoryCode != null)
            {
                cbbFixedAssetCategoryID.Text = FFixedAssetCategoryDetail.FixedCategoryCode;
                cbbOriginalPriceAccount.Text = FFixedAssetCategoryDetail.OriginalPriceAccount;
                cbbExpenditureAccount.Text = FFixedAssetCategoryDetail.ExpenditureAccount;
                cbbDepreciationAccount.Text = FFixedAssetCategoryDetail.DepreciationAccount;
                FFixedAssetCategoryDetail.FixedCategoryCode = null;
                FFixedAssetCategoryDetail.OriginalPriceAccount = null;
                FFixedAssetCategoryDetail.ExpenditureAccount = null;
                FFixedAssetCategoryDetail.DepreciationAccount = null;
            }
            if (cbbFixedAssetCategoryID.IsDroppedDown)
            {
                cbbFixedAssetCategoryID.ToggleDropdown();
            }
            if (cbbOriginalPriceAccount.IsDroppedDown)
            {
                cbbOriginalPriceAccount.ToggleDropdown();
            }
            if (cbbExpenditureAccount.IsDroppedDown)
            {
                cbbExpenditureAccount.ToggleDropdown();
            }
            if (cbbDepreciationAccount.IsDroppedDown)
            {
                cbbDepreciationAccount.ToggleDropdown();
            }
        }

        private void cbbDepartmentID_EditorButtonClick(object sender, EditorButtonEventArgs e)
        {
            new FDepartmentDetail().ShowDialog(this);

            Utils.ClearCacheByType<Department>();
            loadFixedAssetCategory();
            if (FDepartmentDetail.DepartmentName != null)
            {
                cbbDepartmentID.Text = FDepartmentDetail.DepartmentName;
                FDepartmentDetail.DepartmentName = null;
            }
            if (cbbDepartmentID.IsDroppedDown)
            {
                cbbDepartmentID.ToggleDropdown();
            }
        }

        private void cbbFixedAssetCategoryID_ValueChanged(object sender, EventArgs e)
        {
            if (e == null || cbbFixedAssetCategoryID.SelectedRow == null) return;
            FixedAssetCategory temp = (FixedAssetCategory)cbbFixedAssetCategoryID.SelectedRow.ListObject;
            cbbOriginalPriceAccount.SelectedRow = null;
            cbbDepreciationAccount.SelectedRow = null;
            cbbExpenditureAccount.SelectedRow = null;
            foreach (var item in cbbOriginalPriceAccount.Rows)
            {
                if (temp.OriginalPriceAccount != null)
                {
                    if ((item.ListObject as Account).AccountNumber == temp.OriginalPriceAccount)
                    {
                        cbbOriginalPriceAccount.SelectedRow = item;
                        break;
                    }
                }
            }
            foreach (var item in cbbDepreciationAccount.Rows)
            {
                if (temp.DepreciationAccount != null)
                {
                    if ((item.ListObject as Account).AccountNumber == temp.DepreciationAccount)
                    {
                        cbbDepreciationAccount.SelectedRow = item;
                        break;
                    }
                }
            }
            foreach (var item in cbbExpenditureAccount.Rows)
            {
                if (temp.ExpenditureAccount != null)
                {
                    if ((item.ListObject as Account).AccountNumber == temp.ExpenditureAccount)
                    {
                        cbbExpenditureAccount.SelectedRow = item;
                        break;
                    }
                }
            }
        }
        #endregion

        private void FFAInitDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
