﻿namespace Accounting
{
    partial class FTransportMethodDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtDescription = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblDescription = new Infragistics.Win.Misc.UltraLabel();
            this.chkChiTiet = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.cbbChiTiet = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.txtTransportMethodName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtTransportMethodCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblTransportMethodName = new Infragistics.Win.Misc.UltraLabel();
            this.lblTransportMethodCode = new Infragistics.Win.Misc.UltraLabel();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.chkIsActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkChiTiet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbChiTiet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransportMethodName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransportMethodCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox1.Controls.Add(this.txtDescription);
            this.ultraGroupBox1.Controls.Add(this.lblDescription);
            this.ultraGroupBox1.Controls.Add(this.chkChiTiet);
            this.ultraGroupBox1.Controls.Add(this.cbbChiTiet);
            this.ultraGroupBox1.Controls.Add(this.txtTransportMethodName);
            this.ultraGroupBox1.Controls.Add(this.txtTransportMethodCode);
            this.ultraGroupBox1.Controls.Add(this.lblTransportMethodName);
            this.ultraGroupBox1.Controls.Add(this.lblTransportMethodCode);
            appearance4.FontData.BoldAsString = "True";
            this.ultraGroupBox1.HeaderAppearance = appearance4;
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 1);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(436, 133);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtDescription
            // 
            this.txtDescription.AutoSize = false;
            this.txtDescription.Location = new System.Drawing.Point(184, 77);
            this.txtDescription.MaxLength = 512;
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(244, 49);
            this.txtDescription.TabIndex = 3;
            // 
            // lblDescription
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.TextVAlignAsString = "Middle";
            this.lblDescription.Appearance = appearance1;
            this.lblDescription.Location = new System.Drawing.Point(8, 77);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(161, 22);
            this.lblDescription.TabIndex = 8;
            this.lblDescription.Text = "Mô tả chi tiết";
            // 
            // chkChiTiet
            // 
            this.chkChiTiet.BackColor = System.Drawing.Color.Transparent;
            this.chkChiTiet.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkChiTiet.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkChiTiet.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkChiTiet.Location = new System.Drawing.Point(9, 275);
            this.chkChiTiet.Name = "chkChiTiet";
            this.chkChiTiet.Size = new System.Drawing.Size(101, 20);
            this.chkChiTiet.TabIndex = 11;
            this.chkChiTiet.Text = "Chi tiết theo";
            // 
            // cbbChiTiet
            // 
            this.cbbChiTiet.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbChiTiet.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbbChiTiet.Location = new System.Drawing.Point(129, 275);
            this.cbbChiTiet.Name = "cbbChiTiet";
            this.cbbChiTiet.Size = new System.Drawing.Size(245, 21);
            this.cbbChiTiet.TabIndex = 10;
            // 
            // txtTransportMethodName
            // 
            this.txtTransportMethodName.AutoSize = false;
            this.txtTransportMethodName.Location = new System.Drawing.Point(184, 51);
            this.txtTransportMethodName.MaxLength = 512;
            this.txtTransportMethodName.Name = "txtTransportMethodName";
            this.txtTransportMethodName.Size = new System.Drawing.Size(244, 22);
            this.txtTransportMethodName.TabIndex = 2;
            // 
            // txtTransportMethodCode
            // 
            this.txtTransportMethodCode.AutoSize = false;
            this.txtTransportMethodCode.Location = new System.Drawing.Point(184, 25);
            this.txtTransportMethodCode.MaxLength = 25;
            this.txtTransportMethodCode.Name = "txtTransportMethodCode";
            this.txtTransportMethodCode.Size = new System.Drawing.Size(244, 22);
            this.txtTransportMethodCode.TabIndex = 1;
            // 
            // lblTransportMethodName
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.TextVAlignAsString = "Middle";
            this.lblTransportMethodName.Appearance = appearance2;
            this.lblTransportMethodName.Location = new System.Drawing.Point(8, 51);
            this.lblTransportMethodName.Name = "lblTransportMethodName";
            this.lblTransportMethodName.Size = new System.Drawing.Size(171, 22);
            this.lblTransportMethodName.TabIndex = 7;
            this.lblTransportMethodName.Text = "Tên phương thức vận chuyển (*)";
            // 
            // lblTransportMethodCode
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextVAlignAsString = "Middle";
            this.lblTransportMethodCode.Appearance = appearance3;
            this.lblTransportMethodCode.Location = new System.Drawing.Point(8, 25);
            this.lblTransportMethodCode.Name = "lblTransportMethodCode";
            this.lblTransportMethodCode.Size = new System.Drawing.Size(170, 22);
            this.lblTransportMethodCode.TabIndex = 0;
            this.lblTransportMethodCode.Text = "Mã phương thức vận chuyển (*)";
            // 
            // btnSave
            // 
            appearance5.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance5;
            this.btnSave.Location = new System.Drawing.Point(274, 142);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            appearance6.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance6;
            this.btnClose.Location = new System.Drawing.Point(355, 142);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // chkIsActive
            // 
            appearance7.TextVAlignAsString = "Middle";
            this.chkIsActive.Appearance = appearance7;
            this.chkIsActive.BackColor = System.Drawing.Color.Transparent;
            this.chkIsActive.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsActive.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkIsActive.Checked = true;
            this.chkIsActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIsActive.Location = new System.Drawing.Point(8, 147);
            this.chkIsActive.Name = "chkIsActive";
            this.chkIsActive.Size = new System.Drawing.Size(110, 22);
            this.chkIsActive.TabIndex = 4;
            this.chkIsActive.Text = "Hoạt động";
            // 
            // FTransportMethodDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(437, 178);
            this.Controls.Add(this.chkIsActive);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.ultraGroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FTransportMethodDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FTransportMethodDetail_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkChiTiet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbChiTiet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransportMethodName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransportMethodCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraLabel lblTransportMethodCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTransportMethodName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTransportMethodCode;
        private Infragistics.Win.Misc.UltraLabel lblTransportMethodName;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkChiTiet;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbChiTiet;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDescription;
        private Infragistics.Win.Misc.UltraLabel lblDescription;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsActive;
    }
}
