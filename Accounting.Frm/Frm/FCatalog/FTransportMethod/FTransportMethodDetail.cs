﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using System.ComponentModel;

namespace Accounting
{
    public partial class FTransportMethodDetail : DialogForm //UserControl
    {
        #region khai báo
        private readonly ITransportMethodService _ITransportMethodService;
        TransportMethod _Select = new TransportMethod();
        bool Them = true;
        public static bool isClose = true;
        public static string _TransportMethodCode = "";
        #endregion

        #region khởi tạo
        public FTransportMethodDetail()
        {//Thêm
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _ITransportMethodService = IoC.Resolve<ITransportMethodService>();
            chkIsActive.Visible = false;
            this.Text = "Thêm mới phương thức vận chuyển";
            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion
            //khởi tạo và hiển thị giá trị ban đầu cho các control
            #endregion
        }

        public FTransportMethodDetail(TransportMethod temp)
        {//Sửa
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _Select = temp;
            Them = false;
            txtTransportMethodCode.Enabled = false;
            _ITransportMethodService = IoC.Resolve<ITransportMethodService>();
            this.Text = "Sửa phương thức vận chuyển";
            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion
            #endregion

            #region Fill dữ liệu Obj vào control
            ObjandGUI(temp, false);
            #endregion
        }

        #endregion

        #region Button Event
        /// <summary>
        /// Sự kiện nút Lưu lại
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion

            //Gui to object
            #region Fill dữ liệu control vào obj
            TransportMethod temp = Them ? new TransportMethod() : _ITransportMethodService.Getbykey(_Select.ID);
            temp = ObjandGUI(temp, true);
            #endregion

            #region Thao tác CSDL
            _ITransportMethodService.BeginTran();
            if (Them)
            {
                if (!CheckCode()) return;
                temp.IsActive = true;
                temp.IsSecurity = false;
                _ITransportMethodService.CreateNew(temp);
            }
            else
            {
                if (!CheckError()) return;
                if (!chkIsActive.Checked) temp.IsActive = false;
                else temp.IsActive = true;
                _ITransportMethodService.Update(temp);
            }
            _ITransportMethodService.CommitTran();
            #endregion

            #region xử lý form, kết thúc form     
            Utils.ClearCacheByType<TransportMethod>();
            BindingList<TransportMethod> lst = Utils.ListTransportMethod;
            _TransportMethodCode = temp.TransportMethodCode;
            this.Close();
            isClose = false;
            #endregion
        }

        /// <summary>
        /// Sự kiện nút đóng
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            isClose = true;
            DialogResult = DialogResult.Cancel;
            this.Close();
        }
        #endregion

        #region Utils
        /// <summary>
        /// get hoặc set một đối tượng vào giao diện xử lý
        /// </summary>
        /// <param name="input">đối tượng</param>
        /// <param name="isGet">True: get giá trị từ control gán vào đối tượng| False: set giá trị đối tượng vào control</param>
        /// <returns></returns>
        TransportMethod ObjandGUI(TransportMethod input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();  //Thêm mới
                input.TransportMethodCode = txtTransportMethodCode.Text;
                input.TransportMethodName = txtTransportMethodName.Text;
                input.Description = txtDescription.Text;
                input.IsActive = chkIsActive.Checked;
            }
            else
            {
                txtTransportMethodCode.Text = input.TransportMethodCode;
                txtTransportMethodName.Text = input.TransportMethodName;
                txtDescription.Text = input.Description;
                chkIsActive.Checked = input.IsActive;
            }
            return input;
        }

        bool CheckError()
        {
            bool kq = true;
            if (string.IsNullOrEmpty(txtTransportMethodCode.Text) || string.IsNullOrEmpty(txtTransportMethodName.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }

            return kq;
        }

        bool CheckCode()
        {
            bool kq = true;
            if (string.IsNullOrEmpty(txtTransportMethodCode.Text) || string.IsNullOrEmpty(txtTransportMethodName.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }

            //check trùng code
            //List<string> lst = _ITransportMethodService.Query.Select(c => c.TransportMethodCode).ToList();
            List<string> lst = _ITransportMethodService.GetCode();
            foreach (var x in lst)
            {
                if (txtTransportMethodCode.Text.Equals(x))
                {
                    MSG.Warning(string.Format(resSystem.MSG_Catalog, "Mã", "Phương thức vận chuyển"));
                    return false;
                }
            }
            return kq;
        }
        #endregion

        private void FTransportMethodDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
