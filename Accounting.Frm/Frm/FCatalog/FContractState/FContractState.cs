﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FContractState : CatalogBase
    {

        #region khai báo
        private readonly IContractStateService _IContractStateService;
        #endregion

        #region khởi tạo
        public FContractState()
        {
            #region khởi tạo giá trị mặc định
            InitializeComponent();
            ToolTip ToolTip1 = new ToolTip();
            ToolTip1.SetToolTip(btnAdd, "Thêm (Ctrl + N)");
            ToolTip ToolTip2 = new ToolTip();
            ToolTip2.SetToolTip(btnEdit, "Sửa (Ctrl + E)");
            ToolTip ToolTip3 = new ToolTip();
            ToolTip3.SetToolTip(btnDelete, "Xóa (Ctrl + D)");
            #endregion

            #region thiết lập ban đầu
            _IContractStateService = IoC.Resolve<IContractStateService>();
            this.uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            #endregion

            #region tạo dữ liệu ban đầu
            LoadDuLieu();
            if (uGrid.Rows.Count > 0)
            {
                uGrid.Rows[0].Selected = true;
            }
            #endregion

        }

        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }

        private void LoadDuLieu(bool configGrid)
        {
            WaitingFrm.StartWaiting();
            #region lay du lieu tu csdl do ra list
            //List<ContractState> list = _IContractStateService.GetAll().OrderByDescending(c=>c.ContractStateCode).Reverse().ToList();
            List<ContractState> list = _IContractStateService.GetListContractStateOrder();
            _IContractStateService.UnbindSession(list);
            list = _IContractStateService.GetListContractStateOrder();
            #endregion

            #region hien thi du lieu ra ultragrid
            uGrid.DataSource = list.ToArray();
            if (configGrid) ConfigGrid(uGrid);
            #endregion
            WaitingFrm.StopWaiting();
        }
        #endregion

        #region nghiệp vụ
        #region contentmenu
        private void tsmAdd_Click(object sender, EventArgs e)
        {
            AddFunction();

        }

        private void tsmEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            ResetFunction();
        }
        protected override void ResetFunction()
        {
            LoadDuLieu();
        }
        #endregion

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        protected override void AddFunction()
        {
            new FContractStateDetail().ShowDialog(this);
            if (FContractStateDetail.IsClose) LoadDuLieu();
        }

        //edit
        protected override void EditFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                ContractState temp = uGrid.Selected.Rows[0].ListObject as ContractState;
                new FContractStateDetail(temp).ShowDialog(this);
                if (FContractStateDetail.IsClose) LoadDuLieu();
            }
            else
                Accounting.TextMessage.MSG.Error(string.Format(Accounting.TextMessage.resSystem.MSG_Catalog3,"một tình trạng hợp đồng"));
        }

        //delete
        protected override void DeleteFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                ContractState temp = uGrid.Selected.Rows[0].ListObject as ContractState;
                if (MSG.Question(string.Format(resSystem.MSG_System_05, "Tình trạng hợp đồng <" + temp.ContractStateCode + " - " + temp.ContractStateName + ">")) == System.Windows.Forms.DialogResult.Yes)
                {
                    if (temp.IsSecurity == true)
                    {
                        MSG.Error(string.Format(resSystem.MSG_Catalog7, "tình trạng hợp đồng"));
                        //MessageBox.Show("Không thể xóa tình trạng hợp đồng đã thuộc về hệ thống: " + temp.ContractStateCode + " - " + temp.ContractStateName);
                    }
                    else
                    {
                        _IContractStateService.BeginTran();
                        _IContractStateService.Delete(temp);
                        _IContractStateService.CommitTran();
                        Utils.ClearCacheByType<ContractState>();
                        LoadDuLieu();
                    }
                }
            }
            else
                MSG.Error(string.Format(resSystem.MSG_Catalog2,"một tình trạng hợp đồng"));
        }

        private void uGrid_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }

        private void uGrid_DoubleClick(object sender, EventArgs e)
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                EditFunction();
            }
        }
        #endregion

        #region Utils
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, TextMessage.ConstDatabase.ContractState_TableName);

            uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.AlignWithDataRows;
            this.uGrid.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.uGrid.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGrid.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGrid.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGrid.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
        }
        #endregion

        private void FContractState_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGrid.ResetText();
            uGrid.ResetUpdateMode();
            uGrid.ResetExitEditModeOnLeave();
            uGrid.ResetRowUpdateCancelAction();
            uGrid.DataSource = null;
            uGrid.Layouts.Clear();
            uGrid.ResetLayouts();
            uGrid.ResetDisplayLayout();
            uGrid.Refresh();
            uGrid.ClearUndoHistory();
            uGrid.ClearXsdConstraints();
        }

        private void FContractState_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
