﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;

namespace Accounting
{
    public partial class FContractStateDetail : DialogForm
    {
        #region khai báo
        private readonly IContractStateService _IContractStateService;
        ContractState _Select = new ContractState();
        bool them = true;
        public static bool IsClose = true;
        public static string ContractStateName;
        #endregion

        #region khởi tạo
        public FContractStateDetail()
        {//them
            InitializeComponent();
            _IContractStateService = IoC.Resolve<IContractStateService>();
            this.Text = "Thêm mới tình trạng hợp đồng";
        }

        public FContractStateDetail(ContractState temp)
        {//sua
            InitializeComponent();
            _Select = temp;
            them = false;
            txtContractStateCode.Enabled = false;
            _IContractStateService = IoC.Resolve<IContractStateService>();
            this.Text = "Sửa tình trạng hợp đồng";
            ObjandGUI(temp, false);
        }
        #endregion

        #region nghiệp vụ
        private void btnSave_Click(object sender, EventArgs e)
        {
            #region fill du lieu control vao obj
            ContractState temp = them ? new ContractState() : _IContractStateService.Getbykey(_Select.ID);
            temp = ObjandGUI(temp, true);
            #endregion

            #region thao tac csdl
            _IContractStateService.BeginTran();
            if (them)
            {
                if (!CheckCode()) return;
                temp.IsSecurity = false;
                _IContractStateService.CreateNew(temp);
            }
            else
            {
                if (!CheckError()) return;
                _IContractStateService.Update(temp);
            }
            _IContractStateService.CommitTran();
            #endregion

            this.Close();
            IsClose = true;
            ContractStateName = txtContractStateName.Value.ToString();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            IsClose = true;
            this.Close();
        }
        #endregion

        #region ultil
        //get hoac set 1 doi tuong vao giao dien xu ly
        ContractState ObjandGUI(ContractState input, bool isGet)
        {
            if (isGet)
            {
                input.ContractStateCode = txtContractStateCode.Text;
                input.ContractStateName = txtContractStateName.Text;
                input.Description = txtDescription.Text;
            }
            else
            {
                txtContractStateCode.Text = input.ContractStateCode;
                txtContractStateName.Text = input.ContractStateName;
                txtDescription.Text = input.Description;
            }
            return input;
        }

        //check error
        bool CheckError()
        {
            bool kq = true;
            if (string.IsNullOrEmpty(txtContractStateCode.Text) || string.IsNullOrEmpty(txtContractStateName.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }

            return kq;
        }

        bool CheckCode()
        {
            bool kq = true;
            if (string.IsNullOrEmpty(txtContractStateCode.Text) || string.IsNullOrEmpty(txtContractStateName.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }
            //check trùng code
            List<string> lst = _IContractStateService.GetListContractStateCode();
            foreach (var x in lst)
            {
                if (txtContractStateCode.Text.Equals(x))
                {
                    MSG.Error(string.Format(resSystem.MSG_Catalog_Account6, "tình trạng hợp đồng"));
                    //MessageBox.Show("Mã tình trạng hợp đồng này đã tồn tại");
                    return false;
                }
            }

            return kq;
        }

        #endregion

        private void FContractStateDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
