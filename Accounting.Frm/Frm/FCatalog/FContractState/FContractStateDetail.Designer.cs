﻿namespace Accounting
{
    partial class FContractStateDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtDescription = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtContractStateName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtContractStateCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblDescription = new Infragistics.Win.Misc.UltraLabel();
            this.lblContractStateName = new Infragistics.Win.Misc.UltraLabel();
            this.lblContractStateCode = new Infragistics.Win.Misc.UltraLabel();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContractStateName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContractStateCode)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox1.Controls.Add(this.txtDescription);
            this.ultraGroupBox1.Controls.Add(this.txtContractStateName);
            this.ultraGroupBox1.Controls.Add(this.txtContractStateCode);
            this.ultraGroupBox1.Controls.Add(this.lblDescription);
            this.ultraGroupBox1.Controls.Add(this.lblContractStateName);
            this.ultraGroupBox1.Controls.Add(this.lblContractStateCode);
            appearance4.FontData.BoldAsString = "True";
            this.ultraGroupBox1.HeaderAppearance = appearance4;
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 1);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(399, 165);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtDescription
            // 
            this.txtDescription.AutoSize = false;
            this.txtDescription.Location = new System.Drawing.Point(147, 88);
            this.txtDescription.MaxLength = 512;
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(244, 67);
            this.txtDescription.TabIndex = 3;
            // 
            // txtContractStateName
            // 
            this.txtContractStateName.AutoSize = false;
            this.txtContractStateName.Location = new System.Drawing.Point(147, 58);
            this.txtContractStateName.MaxLength = 512;
            this.txtContractStateName.Name = "txtContractStateName";
            this.txtContractStateName.Size = new System.Drawing.Size(244, 22);
            this.txtContractStateName.TabIndex = 2;
            // 
            // txtContractStateCode
            // 
            this.txtContractStateCode.AutoSize = false;
            this.txtContractStateCode.Location = new System.Drawing.Point(147, 28);
            this.txtContractStateCode.MaxLength = 25;
            this.txtContractStateCode.Name = "txtContractStateCode";
            this.txtContractStateCode.Size = new System.Drawing.Size(244, 22);
            this.txtContractStateCode.TabIndex = 1;
            // 
            // lblDescription
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.TextHAlignAsString = "Left";
            appearance1.TextVAlignAsString = "Middle";
            this.lblDescription.Appearance = appearance1;
            this.lblDescription.Location = new System.Drawing.Point(10, 88);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(68, 22);
            this.lblDescription.TabIndex = 0;
            this.lblDescription.Text = "Mô tả chi tiết";
            // 
            // lblContractStateName
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.TextHAlignAsString = "Left";
            appearance2.TextVAlignAsString = "Middle";
            this.lblContractStateName.Appearance = appearance2;
            this.lblContractStateName.Location = new System.Drawing.Point(10, 58);
            this.lblContractStateName.Name = "lblContractStateName";
            this.lblContractStateName.Size = new System.Drawing.Size(139, 22);
            this.lblContractStateName.TabIndex = 0;
            this.lblContractStateName.Text = "Tên tình trạng hợp đồng (*)";
            this.lblContractStateName.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // lblContractStateCode
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextHAlignAsString = "Left";
            appearance3.TextVAlignAsString = "Middle";
            this.lblContractStateCode.Appearance = appearance3;
            this.lblContractStateCode.Location = new System.Drawing.Point(10, 28);
            this.lblContractStateCode.Name = "lblContractStateCode";
            this.lblContractStateCode.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblContractStateCode.Size = new System.Drawing.Size(135, 22);
            this.lblContractStateCode.TabIndex = 0;
            this.lblContractStateCode.Text = "Mã tình trạng hợp đồng (*)";
            // 
            // btnSave
            // 
            appearance5.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance5;
            this.btnSave.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2010Button;
            this.btnSave.Location = new System.Drawing.Point(231, 172);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Lưu Lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            appearance6.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance6;
            this.btnClose.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2010Button;
            this.btnClose.Location = new System.Drawing.Point(316, 172);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 5;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // FContractStateDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(400, 206);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.ultraGroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FContractStateDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FContractStateDetail_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContractStateName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContractStateCode)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtContractStateCode;
        private Infragistics.Win.Misc.UltraLabel lblContractStateCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtContractStateName;
        private Infragistics.Win.Misc.UltraLabel lblDescription;
        private Infragistics.Win.Misc.UltraLabel lblContractStateName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDescription;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraButton btnClose;
    }
}