﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinTree;

namespace Accounting
{
    public partial class BaseCatalog<T> : CustormForm
    {
        #region khai báo
        public bool IsGridShow = true;
        public string NameTable;
        #endregion

        #region Services
        public IAccountService IaccountService { get { return IoC.Resolve<IAccountService>(); } }
        public IAccountGroupService IaccountGroupService { get { return IoC.Resolve<IAccountGroupService>(); } }
        public IAccountTransferService IaccountTransferService { get { return IoC.Resolve<IAccountTransferService>(); } }
        public IAccountDefaultService IaccountDefaultService { get { return IoC.Resolve<IAccountDefaultService>(); } }
        public IAutoPrincipleService IautoPrincipleService { get { return IoC.Resolve<IAutoPrincipleService>(); } }
        public IDepartmentService IdepartmentService { get { return IoC.Resolve<IDepartmentService>(); } }
        public IAccountingObjectCategoryService IaccountingObjectCategoryService { get { return IoC.Resolve<IAccountingObjectCategoryService>(); } }
        public IAccountingObjectGroupService IaccountingObjectGroupService { get { return IoC.Resolve<IAccountingObjectGroupService>(); } }
        public IAccountingObjectService IaccountingObjectService { get { return IoC.Resolve<IAccountingObjectService>(); } }
        public IPersonalSalaryTaxService IpersonalSalaryTaxService { get { return IoC.Resolve<IPersonalSalaryTaxService>(); } }
        public ITimeSheetSymbolsService ItimeSheetSymbolsService { get { return IoC.Resolve<ITimeSheetSymbolsService>(); } }
        public IRepositoryService IrepositoryService { get { return IoC.Resolve<IRepositoryService>(); } }
        public IMaterialGoodsCategoryService ImaterialGoodsCategoryService { get { return IoC.Resolve<IMaterialGoodsCategoryService>(); } }
        public IMaterialGoodsService ImaterialGoodsService { get { return IoC.Resolve<IMaterialGoodsService>(); } }
        public IMaterialQuantumService ImaterialQuantumService { get { return IoC.Resolve<IMaterialQuantumService>(); } }
        public IMaterialQuantumDetailService ImaterialQuantumDetailService { get { return IoC.Resolve<IMaterialQuantumDetailService>(); } }
        public IFixedAssetCategoryService IfixedAssetCategoryService { get { return IoC.Resolve<IFixedAssetCategoryService>(); } }
        public IFixedAssetService IfixedAssetService { get { return IoC.Resolve<IFixedAssetService>(); } }
        public IFixedAssetDetailService IfixedAssetDetailService { get { return IoC.Resolve<IFixedAssetDetailService>(); } }
        public IExpenseItemService IexpenseItemService { get { return IoC.Resolve<IExpenseItemService>(); } }
        //public ICostFactorCategoryService IcostFactorCategoryService { get { return IoC.Resolve<ICostFactorCategoryService>(); } }
        public ICostSetService IcostSetService { get { return IoC.Resolve<ICostSetService>(); } }
        public IStockCategoryService IstockCategoryService { get { return IoC.Resolve<IStockCategoryService>(); } }
        public IInvestorGroupService IinvestorGroupService { get { return IoC.Resolve<IInvestorGroupService>(); } }
        public IInvestorService IinvestorService { get { return IoC.Resolve<IInvestorService>(); } }
        public IRegistrationGroupService IregistrationGroupService { get { return IoC.Resolve<IRegistrationGroupService>(); } }
        public IShareHolderGroupService IshareHolderGroupService { get { return IoC.Resolve<IShareHolderGroupService>(); } }
        public IBankService IbankService { get { return IoC.Resolve<IBankService>(); } }
        public IBankAccountDetailService IbankAccountDetailService { get { return IoC.Resolve<IBankAccountDetailService>(); } }
        public ICreditCardService IcreditCardService { get { return IoC.Resolve<ICreditCardService>(); } }
        public IStatisticsCodeService IstatisticsCodeService { get { return IoC.Resolve<IStatisticsCodeService>(); } }
        public IPaymentClauseService IpaymentClauseService { get { return IoC.Resolve<IPaymentClauseService>(); } }
        public ITransportMethodService ItransportMethodService { get { return IoC.Resolve<ITransportMethodService>(); } }
        public IBudgetItemService IbudgetItemService { get { return IoC.Resolve<IBudgetItemService>(); } }
        //public ISalePriceGroupService IsalePriceGroup { get { return IoC.Resolve<ISalePriceGroup>(); } }
        //public IGoodsServicePurchaseService IgoodsServicePurchase { get { return IoC.Resolve<IGoodsServicePurchase>(); } }
        //public IMaterialGoodsSpecialTaxGroupService ImaterialGoodsSpecialTaxGroup { get { return IoC.Resolve<IMaterialGoodsSpecialTaxGroup>(); } }
        public ITypeService ItypeService { get { return IoC.Resolve<ITypeService>(); } }
        public ITypeGroupService ItypeGroupService { get { return IoC.Resolve<ITypeGroupService>(); } }
        public ICurrencyService IcurrencyService { get { return IoC.Resolve<ICurrencyService>(); } }
        public IContractStateService IcontractStateService { get { return IoC.Resolve<IContractStateService>(); } }
        #endregion  //danh mục

        #region khởi tạo
        public virtual void InitValueFrm()
        {//Demo (các frm nonBase sẽ override và set lại)
            //init val
            //NameTable = ConstDatabase.Account_TableName;    //tùy từng frm mà thay chuỗi tương ứng
        }

        public BaseCatalog()
        {
            InitializeComponent();

            InitValueFrm();

            #region Config Base Frm
            //kích thước frm
            Size = new Size(800, 450);
            //căn giữa màn hình
            StartPosition = FormStartPosition.CenterScreen;
            //Frm cần show uGrid hay uTree
            uGrid.Size = new Size(1, 1);
            uTree.Size = new Size(1, 1);
            if (IsGridShow) { uGrid.Dock = DockStyle.Fill; }
            else { uTree.Dock = DockStyle.Fill; }
            //
            #endregion

            #region Event Init
            //Button Click
            btnAdd.Click += BtnAddClick;
            btnEdit.Click += BtnEditClick;
            btnDelete.Click += BtnDeleteClick;
            //ToolStripMenuItem (ContentMenu) Click
            tsmAdd.Click += TsmAddClick;
            tsmEdit.Click += TsmEditClick;
            tsmDelete.Click += TsmDeleteClick;
            tsmReLoad.Click += TsmReLoadClick;

            if (IsGridShow)
            {//init uGrid
                uGrid.InitializeLayout += UGridInitializeLayout;
                uGrid.MouseDown += UGridMouseDown;
                uGrid.DoubleClickRow += UGridDoubleClickRow;
            }
            else
            {//init uTree
                uTree.InitializeDataNode += UTreeInitializeDataNode;
                uTree.AfterDataNodesCollectionPopulated += Utils.uTree_AfterDataNodesCollectionPopulated;
                uTree.ColumnSetGenerated += UTreeColumnSetGenerated;
                uTree.MouseDown += UTreeMouseDown;
                uTree.DoubleClick += UTreeDoubleClick;
            }
            #endregion

        }
        #endregion

        #region Nghiệp vụ
        #region ContentMenu
        private void TsmAddClick(object sender, EventArgs e)
        {
            AddFunctionBase();
        }

        private void TsmEditClick(object sender, EventArgs e)
        {
            EditFunctionBase();
        }

        private void TsmDeleteClick(object sender, EventArgs e)
        {
            DeleteFunctionBase();
        }

        private void TsmReLoadClick(object sender, EventArgs e)
        {
            LoadDuLieuBase(false);
        }
        #endregion

        #region Button
        private void BtnAddClick(object sender, EventArgs e)
        {
            AddFunctionBase();
        }

        private void BtnEditClick(object sender, EventArgs e)
        {
            EditFunctionBase();
        }

        private void BtnDeleteClick(object sender, EventArgs e)
        {
            DeleteFunctionBase();
        }
        #endregion

        public virtual void LoadDuLieuBase(bool configTree = true)
        {
            //
        }

        /// <summary>
        /// Nghiệp vụ Add
        /// </summary>
        void AddFunctionBase()
        {
            T temp = uGrid.Selected.Rows.Count <= 0 ? (T)Activator.CreateInstance(typeof(T)) : (T)uGrid.Selected.Rows[0].ListObject;
            AddFunction(temp);
        }
        public virtual void AddFunction(T temp)
        {
            //
        }

        /// <summary>
        /// Nghiệp vụ Edit
        /// </summary>
        void EditFunctionBase()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                T temp = (T)uGrid.Selected.Rows[0].ListObject;
                EditFunction(temp);
            }
            else
                MSG.Error(resSystem.MSG_System_04);
        }
        public virtual void EditFunction(T temp)
        {
            //
        }

        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        void DeleteFunctionBase()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                T temp = (T)uGrid.Selected.Rows[0].ListObject;
                DeleteFunction(temp);
            }
            else
                MSG.Error(resSystem.MSG_System_06);
        }
        public virtual void DeleteFunction(T temp)
        {
            //
        }
        #endregion

        #region Event
        //Ugrid
        private void UGridInitializeLayout(object sender, InitializeLayoutEventArgs e)
        {//Thiết lập cho Grid
            UltraGrid ultraGrid = (UltraGrid)sender;
            //hiển thị 1 band
            ultraGrid.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            //tắt lọc cột
            ultraGrid.DisplayLayout.Override.AllowRowFiltering = ultraGrid.Name.Equals("uGrid") ? Infragistics.Win.DefaultableBoolean.True : Infragistics.Win.DefaultableBoolean.False;
            ultraGrid.DisplayLayout.Override.FilterUIType = ultraGrid.Name.Equals("uGrid") ? FilterUIType.FilterRow : FilterUIType.Default;
            //tự thay đổi kích thước cột
            ultraGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            //tắt tiêu đề
            ultraGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            ////select cả hàng hay ko?
            //ultraGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            //ultraGrid.DisplayLayout.Override.RowSelectorStyle = Infragistics.Win.HeaderStyle.WindowsVista;

            ////Hiện những dòng trống?
            //ultraGrid.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            //ultraGrid.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.ExtendFirstCell;
            ////Fix Header
            //ultraGrid.DisplayLayout.UseFixedHeaders = true;

            uGrid.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.AlignWithDataRows;
        }

        private void UGridMouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }

        private void UGridDoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            EditFunctionBase();
        }

        //Tree
        private void UTreeMouseDown(object sender, MouseEventArgs e)
        {//khi click chuột phải hiện content menu
            Utils.uGrid_MouseDown(sender, e, cms4Grid);
        }

        private void UTreeDoubleClick(object sender, EventArgs e)
        {//click đúp chuột để sửa
            EditFunctionBase();
        }

        private void UTreeColumnSetGenerated(object sender, ColumnSetGeneratedEventArgs e)
        {
            Utils.uTree_ColumnSetGenerated(sender, e, NameTable);
        }

        private void UTreeInitializeDataNode(object sender, InitializeDataNodeEventArgs e)
        {
            Utils.uTree_InitializeDataNode(sender, e, NameTable);
        }
        #endregion

        #region Utils

        #endregion
    }
}
