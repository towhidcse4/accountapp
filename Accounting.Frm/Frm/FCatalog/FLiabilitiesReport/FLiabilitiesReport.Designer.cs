﻿namespace Accounting
{
    partial class FLiabilitiesReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FLiabilitiesReport));
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbCn = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.lblCN = new Infragistics.Win.Misc.UltraLabel();
            this.dateEnd = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.lblDateEnd = new Infragistics.Win.Misc.UltraLabel();
            this.DateStart = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.lblDateS = new Infragistics.Win.Misc.UltraLabel();
            this.dteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.lblDate = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridList)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.cbbCn);
            this.ultraGroupBox1.Controls.Add(this.lblCN);
            this.ultraGroupBox1.Controls.Add(this.dateEnd);
            this.ultraGroupBox1.Controls.Add(this.lblDateEnd);
            this.ultraGroupBox1.Controls.Add(this.DateStart);
            this.ultraGroupBox1.Controls.Add(this.lblDateS);
            this.ultraGroupBox1.Controls.Add(this.dteDate);
            this.ultraGroupBox1.Controls.Add(this.lblDate);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(896, 72);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbCn
            // 
            appearance1.TextVAlignAsString = "Middle";
            this.cbbCn.Appearance = appearance1;
            this.cbbCn.AutoSize = false;
            this.cbbCn.Location = new System.Drawing.Point(763, 23);
            this.cbbCn.Name = "cbbCn";
            this.cbbCn.Size = new System.Drawing.Size(121, 22);
            this.cbbCn.TabIndex = 34;
            // 
            // lblCN
            // 
            appearance2.TextVAlignAsString = "Middle";
            this.lblCN.Appearance = appearance2;
            this.lblCN.Location = new System.Drawing.Point(668, 23);
            this.lblCN.Name = "lblCN";
            this.lblCN.Size = new System.Drawing.Size(89, 22);
            this.lblCN.TabIndex = 33;
            this.lblCN.Text = "Chi Nhánh :";
            // 
            // dateEnd
            // 
            appearance3.TextHAlignAsString = "Center";
            appearance3.TextVAlignAsString = "Middle";
            this.dateEnd.Appearance = appearance3;
            this.dateEnd.AutoSize = false;
            appearance4.TextHAlignAsString = "Center";
            appearance4.TextVAlignAsString = "Middle";
            this.dateEnd.ButtonAppearance = appearance4;
            this.dateEnd.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dateEnd.Location = new System.Drawing.Point(564, 23);
            this.dateEnd.MaskInput = "";
            this.dateEnd.Name = "dateEnd";
            this.dateEnd.Size = new System.Drawing.Size(98, 22);
            this.dateEnd.TabIndex = 32;
            this.dateEnd.Value = null;
            // 
            // lblDateEnd
            // 
            appearance5.TextVAlignAsString = "Middle";
            this.lblDateEnd.Appearance = appearance5;
            this.lblDateEnd.Location = new System.Drawing.Point(463, 23);
            this.lblDateEnd.Name = "lblDateEnd";
            this.lblDateEnd.Size = new System.Drawing.Size(95, 22);
            this.lblDateEnd.TabIndex = 31;
            this.lblDateEnd.Text = "Đến ngày  :";
            // 
            // DateStart
            // 
            appearance6.TextHAlignAsString = "Center";
            appearance6.TextVAlignAsString = "Middle";
            this.DateStart.Appearance = appearance6;
            this.DateStart.AutoSize = false;
            appearance7.TextHAlignAsString = "Center";
            appearance7.TextVAlignAsString = "Middle";
            this.DateStart.ButtonAppearance = appearance7;
            this.DateStart.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.DateStart.Location = new System.Drawing.Point(359, 23);
            this.DateStart.MaskInput = "";
            this.DateStart.Name = "DateStart";
            this.DateStart.Size = new System.Drawing.Size(98, 22);
            this.DateStart.TabIndex = 30;
            this.DateStart.Value = null;
            // 
            // lblDateS
            // 
            appearance8.TextVAlignAsString = "Middle";
            this.lblDateS.Appearance = appearance8;
            this.lblDateS.Location = new System.Drawing.Point(222, 23);
            this.lblDateS.Name = "lblDateS";
            this.lblDateS.Size = new System.Drawing.Size(131, 22);
            this.lblDateS.TabIndex = 29;
            this.lblDateS.Text = "Công nợ tính từ ngày :";
            // 
            // dteDate
            // 
            appearance9.TextHAlignAsString = "Center";
            appearance9.TextVAlignAsString = "Middle";
            this.dteDate.Appearance = appearance9;
            this.dteDate.AutoSize = false;
            appearance10.TextHAlignAsString = "Center";
            appearance10.TextVAlignAsString = "Middle";
            this.dteDate.ButtonAppearance = appearance10;
            this.dteDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteDate.Location = new System.Drawing.Point(118, 23);
            this.dteDate.MaskInput = "";
            this.dteDate.Name = "dteDate";
            this.dteDate.Size = new System.Drawing.Size(98, 22);
            this.dteDate.TabIndex = 28;
            this.dteDate.Value = null;
            // 
            // lblDate
            // 
            appearance11.TextVAlignAsString = "Middle";
            this.lblDate.Appearance = appearance11;
            this.lblDate.Location = new System.Drawing.Point(12, 23);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(100, 22);
            this.lblDate.TabIndex = 0;
            this.lblDate.Text = "Ngày thông báo";
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraGroupBox2.Location = new System.Drawing.Point(0, 352);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(896, 68);
            this.ultraGroupBox2.TabIndex = 1;
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // uGridList
            // 
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridList.DisplayLayout.Appearance = appearance12;
            this.uGridList.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGridList.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            appearance13.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance13.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance13.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridList.DisplayLayout.GroupByBox.Appearance = appearance13;
            appearance14.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance14;
            this.uGridList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance15.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance15.BackColor2 = System.Drawing.SystemColors.Control;
            appearance15.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridList.DisplayLayout.GroupByBox.PromptAppearance = appearance15;
            this.uGridList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance16.BackColor = System.Drawing.SystemColors.Window;
            appearance16.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridList.DisplayLayout.Override.ActiveCellAppearance = appearance16;
            appearance17.BackColor = System.Drawing.SystemColors.Highlight;
            appearance17.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridList.DisplayLayout.Override.ActiveRowAppearance = appearance17;
            this.uGridList.DisplayLayout.Override.AllowColSizing = Infragistics.Win.UltraWinGrid.AllowColSizing.None;
            this.uGridList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance18.BackColor = System.Drawing.SystemColors.Window;
            this.uGridList.DisplayLayout.Override.CardAreaAppearance = appearance18;
            appearance19.BorderColor = System.Drawing.Color.Silver;
            appearance19.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridList.DisplayLayout.Override.CellAppearance = appearance19;
            this.uGridList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridList.DisplayLayout.Override.CellPadding = 0;
            appearance20.BackColor = System.Drawing.SystemColors.Control;
            appearance20.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance20.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance20.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance20.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridList.DisplayLayout.Override.GroupByRowAppearance = appearance20;
            appearance21.TextHAlignAsString = "Left";
            this.uGridList.DisplayLayout.Override.HeaderAppearance = appearance21;
            this.uGridList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance22.BackColor = System.Drawing.SystemColors.Window;
            appearance22.BorderColor = System.Drawing.Color.Silver;
            this.uGridList.DisplayLayout.Override.RowAppearance = appearance22;
            this.uGridList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance23.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridList.DisplayLayout.Override.TemplateAddRowAppearance = appearance23;
            this.uGridList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridList.Location = new System.Drawing.Point(0, 72);
            this.uGridList.Name = "uGridList";
            this.uGridList.Size = new System.Drawing.Size(896, 280);
            this.uGridList.TabIndex = 2;
            this.uGridList.Text = "ultraGrid1";
            // 
            // FLiabilitiesReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(896, 420);
            this.Controls.Add(this.uGridList);
            this.Controls.Add(this.ultraGroupBox2);
            this.Controls.Add(this.ultraGroupBox1);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FLiabilitiesReport";
            this.Text = "FLiabilitiesReport";
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbCn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridList;
        private Infragistics.Win.Misc.UltraLabel lblDate;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbCn;
        private Infragistics.Win.Misc.UltraLabel lblCN;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dateEnd;
        private Infragistics.Win.Misc.UltraLabel lblDateEnd;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor DateStart;
        private Infragistics.Win.Misc.UltraLabel lblDateS;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteDate;

    }
}