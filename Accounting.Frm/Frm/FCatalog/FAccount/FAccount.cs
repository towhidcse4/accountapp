﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using Accounting.TextMessage;
using System.Data;
using Infragistics.Win.UltraWinTree;
using log4net;

namespace Accounting
{
    public partial class FAccount : CatalogBase //UserControl
    {
        #region khai báo
        private readonly IAccountService _IAccountService;
        private readonly IAccountTransferService _IAccountTransferService;
        private readonly log4net.ILog log = log4net.LogManager.GetLogger("Accounting.FAccount");
        private IGeneralLedgerService _IGeneralLedgerService { get { return IoC.Resolve<IGeneralLedgerService>(); } }
        List<Account> dsAccount = new List<Account>();

        #endregion

        #region khởi tạo
        public FAccount()
        {
            #region Khởi tạo giá trị mặc định của Form
            log.Error("FAccount B1");
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.CenterToParent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _IAccountTransferService = IoC.Resolve<IAccountTransferService>();
            _IAccountService = IoC.Resolve<IAccountService>();

            this.uTree.InitializeDataNode += new Infragistics.Win.UltraWinTree.InitializeDataNodeEventHandler(this.uTree_InitializeDataNode);
            this.uTree.AfterDataNodesCollectionPopulated += new Infragistics.Win.UltraWinTree.AfterDataNodesCollectionPopulatedEventHandler(Utils.uTree_AfterDataNodesCollectionPopulated);
            this.uTree.ColumnSetGenerated += new Infragistics.Win.UltraWinTree.ColumnSetGeneratedEventHandler(this.uTree_ColumnSetGenerated);
            this.uTree.MouseDown += new System.Windows.Forms.MouseEventHandler(this.uTree_MouseDown);
            this.uTree.DoubleClick += new System.EventHandler(this.uTree_DoubleClick);

            #endregion

            log.Error("FAccount B2");
            #region Tạo dữ liệu ban đầu
            LoadDuLieu();
            #endregion
            log.Error("FAccount B3");
        }

        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }
        private void LoadDuLieu(bool configTree)
        {
            log.Error("FAccount B2.1");
            WaitingFrm.StartWaiting();
            #region Lấy dữ liệu từ CSDL

            // Convert service
            dsAccount = _IAccountService.GetListOrderBy();
            _IAccountService.UnbindSession(dsAccount);
            dsAccount = _IAccountService.GetListOrderBy();
            #endregion

            log.Error("FAccount B2.2");

            #region Xử lý dữ liệu
            //thiết lập tính chấtp
            foreach (var item in dsAccount) item.AccountGroupKindView = Utils.DicAccountKind[item.AccountGroupKind.ToString()].Name;
            #endregion

            log.Error("FAccount B2.3");
            #region hiển thị và xử lý hiển thị
            DataSet ds = Utils.ToDataSet<Account>(dsAccount, ConstDatabase.Account_TableName);
            uTree.SetDataBinding(ds, ConstDatabase.Account_TableName);
            if (configTree) ConfigTree(uTree);
            //ds.Tables[0].Columns["DetailByAccountObject"].ColumnMapping = MappingType.Hidden;
            #endregion
            log.Error("FAccount B2.4");
            WaitingFrm.StopWaiting();
        }
        #endregion

        #region Nghiệp vụ
        #region ContentMenu
        private void tsmAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void tsmEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            ResetFunction();
        }
        #endregion

        #region Button
        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }
        #endregion

        /// <summary>
        /// Nghiệp vụ Add
        /// </summary>
        protected override void AddFunction()
        {
            if (uTree.SelectedNodes.Count > 0)
            {
                Account temp = dsAccount.Single(k => k.ID == (new Guid(uTree.SelectedNodes[0].Text)));
                new FAccountDetail(temp, true).ShowDialog(this);
                if (!FAccountDetail.isClose)
                {
                    Utils.ClearCacheByType<Account>();
                    LoadDuLieu();
                }
            }
            else
            {
                new FAccountDetail().ShowDialog(this);
                if (!FAccountDetail.isClose)
                {
                    Utils.ClearCacheByType<Account>();
                    LoadDuLieu();
                }
            }
        }

        /// <summary>
        /// Nghiệp vụ Edit
        /// </summary>
        protected override void EditFunction()
        {
            if (uTree.SelectedNodes.Count == 0 && uTree.ActiveNode != null)
                uTree.ActiveNode.Selected = true;
            if (uTree.SelectedNodes.Count > 0)
            {

                Account temp = dsAccount.Single(k => k.ID == (new Guid(uTree.SelectedNodes[0].Text)));
                new FAccountDetail(temp).ShowDialog(this);
                if (!FAccountDetail.isClose)
                {
                    Utils.ClearCacheByType<Account>();
                    LoadDuLieu();
                }
            }
            else
                MSG.Error(string.Format(resSystem.MSG_Catalog3, "Tài khoản"));
        }

        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        protected override void DeleteFunction()
        {
            if (uTree.SelectedNodes.Count == 0 && uTree.ActiveNode != null)
            {
                uTree.ActiveNode.Selected = true;
            }
            if (uTree.SelectedNodes.Count > 0)
            {
                Account temp = dsAccount.Single(k => k.ID == (new Guid(uTree.SelectedNodes[0].Text)));

                List<string> ListFromAccount;
                //Sét tài khoản đã dc chọn là kết chuyển đi và đến ko dc xóa
                ListFromAccount = _IAccountTransferService.GetListFromAccountFull();
                foreach (var FromAccount in ListFromAccount)
                {
                    if (FromAccount == temp.AccountNumber)
                    {
                        MSG.Warning(resSystem.MSG_Catalog_Account1);
                        return;
                    }
                }
                ListFromAccount = _IAccountTransferService.GetListToAccountFull();
                foreach (var FromAccount in ListFromAccount)
                {
                    if (FromAccount == temp.AccountNumber)
                    {
                        MSG.Warning(resSystem.MSG_Catalog_Account2);
                        return;
                    }
                }
                //trungnq check ràng buộc không cho xóa tài khoản khi tài khoản đã có số dư hoặc phát sinh chứng từ liên quan
                if( _IGeneralLedgerService.Query.Any(n=>n.Account==temp.AccountNumber || n.AccountCorresponding==temp.AccountNumber)|| Utils.CheckAccountIncurredData(temp.AccountNumber))
                {
                    MSG.Warning("Không thể xóa vì tài khoản này đã có số dư đầu kỳ hoặc đã phát sinh chứng từ liên quan!");
                    return;
                }
                if (MSG.Question(string.Format(resSystem.MSG_System_05, "Tài khoản " + temp.AccountNumber)) == System.Windows.Forms.DialogResult.Yes)
                {
                    _IAccountService.BeginTran();
                    //Kiểm tra xem đối tượng có con không nếu có con thì không được phép xóa
                    // Convert service
                    List<Account> lstChild =
                            _IAccountService.GetListChildren(temp.ID);
                    if (lstChild.Count > 0)
                    {
                        MSG.Warning(string.Format(resSystem.MSG_Catalog_Tree, "Tài khoản"));
                        return;
                    }
                    //Check xem cha còn có con khác không nếu không thì chuyển trạng thái IsParentNode = false
                    if (temp.ParentID != null)
                    {
                        Account parent = _IAccountService.Getbykey((Guid)temp.ParentID);
                        // Convert service
                        int checkChildOldParent = _IAccountService.GetCountChildren(temp.ID);

                        if (checkChildOldParent <= 1)
                        {
                            parent.IsParentNode = false;
                        }
                        _IAccountService.Update(parent);
                        try
                        {
                            _IAccountService.Delete(temp);
                        }
                        catch (Exception ex)
                        {
                            MSG.Error(ex.Message);
                            return;
                        }

                    }

                    if (temp.ParentID == null)
                    {
                        _IAccountService.Delete(temp);
                    }
                    try
                    {
                        _IAccountService.CommitTran();
                    }
                    catch (Exception)
                    {
                        MSG.Error(string.Format(resSystem.MSG_Catalog1, "Tài khoản", temp.AccountName));
                        return;
                    }
                    Utils.ClearCacheByType<Account>();
                    LoadDuLieu();
                }
            }
            else
            {
                MSG.Error(string.Format(resSystem.MSG_Catalog2, "Tài khoản"));
            }
        }
        protected override void ResetFunction()
        {
            LoadDuLieu(false);
        }
        #endregion

        #region Event
        private void uTree_MouseDown(object sender, MouseEventArgs e)
        {//khi click chuột phải hiện content menu
            Utils.uGrid_MouseDown(sender, e, cms4Grid);
        }

        private void uTree_DoubleClick(object sender, EventArgs e)
        {//click đúp chuột để sửa
            EditFunction();
        }

        private void uTree_ColumnSetGenerated(object sender, ColumnSetGeneratedEventArgs e)
        {
            Utils.uTree_ColumnSetGenerated(sender, e, ConstDatabase.Account_TableName);
        }

        private void uTree_InitializeDataNode(object sender, InitializeDataNodeEventArgs e)
        {
            Utils.uTree_InitializeDataNode(sender, e, ConstDatabase.Account_TableName);
        }
        #endregion

        #region Utils
        void ConfigTree(Infragistics.Win.UltraWinTree.UltraTree ultraTree)
        {//hàm chung
            Utils.ConfigTree(ultraTree, TextMessage.ConstDatabase.Account_TableName);
        }
        #endregion

        private void FAccount_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }

        private void FAccount_FormClosing(object sender, FormClosingEventArgs e)
        {
            uTree = null;
        }
    }
}
