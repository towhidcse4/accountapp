﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using System.Text.RegularExpressions;

namespace Accounting
{
    public partial class FAccountDetail : DialogForm //UserControl
    {
        #region khai báo
        private IAccountService _IAccountService;
        private IAccountGroupService _IAccountGroupService;
        private IGeneralLedgerService _IGeneralLedgerService { get { return IoC.Resolve<IGeneralLedgerService>(); } }

        Account _Select = new Account();
        bool IsAdd = true;

        public static bool isClose = true;
        #endregion

        #region khởi tạo
        public FAccountDetail()
        {//Thêm
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            this.Text = "Thêm mới Tài khoản";
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.CenterToParent();
            Add();
            chkIsActive.Visible = false;
            ChekNgoaite();

            #endregion
        }
        public FAccountDetail(Account temp, bool isAdd)
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            this.Text = "Thêm mới Tài khoản";
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.CenterToParent();
            ChekNgoaite();
            #endregion

            if (isAdd)
            {
                Add();
                //check ngoai te
                ChekNgoaite();
                //set lai
                txtAccountNumber.Text = temp.AccountNumber;
                txtAccountName.Text = temp.AccountName;
                txtAccountNameGlobal.Text = temp.AccountNameGlobal;
                //tài khoản tổng hợp
                foreach (var item in cbbParentID.Rows)
                {
                    if ((item.ListObject as Account).ID == temp.ID) cbbParentID.SelectedRow = item;
                }
                //nhóm tài khoản
                foreach (var item in cbbAccountGroupID.Rows)
                {
                    if ((item.ListObject as AccountGroup).ID == temp.AccountGroupID) cbbAccountGroupID.SelectedRow = item;
                }
                cbbAccountGroupKind.SelectedIndex = temp.AccountGroupKind;
                txtDescription.Text = temp.Description;
                SetDetailType(temp.DetailType);
                chkIsActive.CheckState = temp.IsActive ? CheckState.Unchecked : CheckState.Checked;
            }
            else
            {
                Update(temp);
                ChekNgoaite();
            }
            // SetChkEquasFalse();

            #region Khởi tạo giá trị khi Fill check
            if (!chkDetailType.Checked)
            {
                SetChkEquasFalse();
                ChekNgoaite();
            }
            else if (chkDetailType.Checked)
            {
                chk4DoiTuongTapHopChiPhi.Enabled = true;
                chk5DoiTuongHopDong.Enabled = true;
                chk6VTHHCCDC.Enabled = true;
                chk7TaiKhoanNganHang.Enabled = true;
                chk8ChiTietTheoNgoaiTe.Enabled = true;
                chk9theodoiphatsinhtheophongban.Enabled = true;
                chk10theodoiphatsinhtheokhoanmucchiphi.Enabled = true;
                chk11theodoipstheomucthuchi.Enabled = true;
                chkGroupOBJ.Enabled = true;
                CbbDoituong.Enabled = true;
                ChekNgoaite();

            }
            
            #endregion

        }

        public FAccountDetail(Account temp)
        {//Sửa
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            this.Text = "Sửa Tài khoản";
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.CenterToParent();
            #endregion
            Update(temp);
            #region Khởi tạo giá trị khi Fill check
            if (!chkDetailType.Checked)
            {
                SetChkEquasFalse();
            }
            //else if (chkDetailType.Checked)
            //{
            //    chk4DoiTuongTapHopChiPhi.Enabled = true;
            //    chk5DoiTuongHopDong.Enabled = true;
            //    chk6VTHHCCDC.Enabled = true;
            //    chk7TaiKhoanNganHang.Enabled = true;
            //    chk8ChiTietTheoNgoaiTe.Enabled = true;
            //    chk9theodoiphatsinhtheophongban.Enabled = true;
            //    chk10theodoiphatsinhtheokhoanmucchiphi.Enabled = true;
            //    chk11theodoipstheomucthuchi.Enabled = true;
            //    chkGroupOBJ.Enabled = true;
            //    CbbDoituong.Enabled = true;
            //}

            #endregion
            ChekNgoaite();
        }

        //check ngoại tệ cho 007, 1112, 1122
        #region check ngoại tệ cho 007, 1112, 1122
        public void ChekNgoaite()
        {

            List<string> itemAccountNumber = new List<string>() { "007", "1112", "1122" };
            foreach (string item in itemAccountNumber)
            {
                if (txtAccountNumber.Text == item || txtAccountNumber.Text.StartsWith(item))
                {
                    chk8ChiTietTheoNgoaiTe.Enabled = false;
                    chk8ChiTietTheoNgoaiTe.Checked = true;
                    chkDetailType.Checked = true;
                }
            }
        }
        #endregion
        private void InitializeGUI()
        {
            //khởi tạo tài khoản tổng hợp
            cbbParentID.DataSource = _IAccountService.GetListOrderBy();
            cbbParentID.DisplayMember = "AccountNumber";
            Utils.ConfigGrid(cbbParentID, ConstDatabase.Account_TableName);
            //khởi tạo nhóm tài khoản
            cbbAccountGroupID.DataSource = _IAccountGroupService.GetAll();
            cbbAccountGroupID.DisplayMember = "ID";
            Utils.ConfigGrid(cbbAccountGroupID, ConstDatabase.AccountGroup_TableName);
            //khởi tạo cbb tính chất
            cbbAccountGroupKind.DataSource = Utils.DicAccountKind.Values.Where(k => k.Visible == "1").ToList();
            cbbAccountGroupKind.DisplayMember = "Name";
            cbbAccountGroupKind.ValueMember = "Value";
            cbbAccountGroupKind.SelectedIndex = 0;
            CbbDoituong.SelectedIndex = 0;
            CbbDoituong.Enabled = false;
        }
        #endregion

        #region Button Event
        private void Add()
        {
            chkIsActive.Enabled = false;
            chkIsActive.Visible = false;
            chk8ChiTietTheoNgoaiTe.Enabled = false;
            #region Thiết lập ban đầu cho Form
            //Khai báo các webservices
            _IAccountService = IoC.Resolve<IAccountService>();
            _IAccountGroupService = IoC.Resolve<IAccountGroupService>();
            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>
            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion
            //Chỉnh sửa dữ liệu
            //----
            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            SetChkEquasFalse();

            #endregion
        }
        private void Update(Account temp)
        {
            #region Thiết lập ban đầu cho Form
            _Select = temp;
            IsAdd = false;
            txtAccountNumber.Enabled = false;
            //Khai báo các webservices
            _IAccountService = IoC.Resolve<IAccountService>();
            _IAccountGroupService = IoC.Resolve<IAccountGroupService>();

            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion
            //Chỉnh sửa dữ liệu
            //----
            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion
            #region Fill dữ liệu Obj vào control
            ObjandGUI(temp, false);
            // SetChkEquasFalse();
            #endregion
        }
        /// <summary>
        /// Sự kiện nút Lưu lại
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {

            #region Thao tác CSDL

            #region Check Lỗi
            if (!CheckError()) return;
            #endregion

            #endregion

            #region Thao tác CSDL

            try
            {
                _IAccountService.BeginTran();
                if (IsAdd) // them moi
                {
                    Account temp = IsAdd ? new Account() : _IAccountService.Getbykey(_Select.ID);
                    temp = ObjandGUI(temp, true);
                    // set orderfixcode
                    Account temp0 = (Account)Utils.getSelectCbbItem(cbbParentID);

                    temp.IsParentNode = false; // mac dinh cho insert

                    temp.Grade = temp0 == null ? 1 : temp0.Grade + 1; // quy tac lay gia tri cho grade
                    //========> input.OrderFixCode chưa xét

                    //nếu cha isactive =false thì khi thêm mới con cũng là false
                    if (cbbParentID.Text.Equals(""))
                    {
                        temp.IsActive = true;
                    }
                    else
                    {
                        Account objParentCombox = (Account)Utils.getSelectCbbItem(cbbParentID);
                        //lay cha
                        Account Parent = _IAccountService.Getbykey(objParentCombox.ID);
                        if (Parent.IsActive == false)
                        {
                            temp.IsActive = false;
                        }
                        else
                            temp.IsActive = true;
                    }
                    _IAccountService.CreateNew(temp);
                    // thuc hien cap nhat lai isParentNode cho cha neu co
                    if (temp.ParentID != null)
                    {
                        Account parent = _IAccountService.Getbykey((Guid)temp.ParentID);
                        parent.IsParentNode = true;
                        _IAccountService.Update(parent);
                    }

                }
                else // cap nhat
                {

                    //Lấy về đối tượng cần sửa
                    Account temp1 = _IAccountService.Getbykey(_Select.ID);

                    Account objParentCombox1 = (Account)Utils.getSelectCbbItem(cbbParentID);
                    //Thiết lập trạng thái hoạt động cho cha và con
                    #region Thiết lập trạng thái hoạt động cho cha và con <hủy code>
                    //var results = from c in _IAccountService.Query.Where(p=>p.AccountNumber.StartsWith(temp1.AccountNumber))
                    //              select c;
                    //Account Parent = _IAccountService.Getbykey(objParentCombox1.ID);
                    //if (Parent.IsActive == false)
                    //{
                    //    MessageBox.Show("Đối tượng con không thể hoạt động khi đối tượng cha đang bị ngừng hoạt đông!");
                    //    _IAccountService.RolbackTran();
                    //}else{
                    //    if (!chkIsActive.Checked)
                    //    {
                    //        // kiểm tra xem bó có phải là cha ko
                    //        if (_Select.IsParentNode == true)
                    //        {
                    //            // kiểm tra xem  tài khoản có đang hoạt động hay ko
                    //            if (_Select.IsActive == true)
                    //            {
                    //                foreach (var item in results)
                    //                {
                    //                    List<string> temp = new List<string>() { item.ToString() };
                    //                    foreach (string itemString in temp)
                    //                    {
                    //                        item.IsActive = chkIsActive.CheckState == CheckState.Checked ? false : true;
                    //                        _IAccountService.Update(item);
                    //                    }
                    //                }
                    //            }
                    //            else
                    //            {
                    //                if (MessageBox.Show("Bạn có muốn thiết lập tất cả các danh mục con của danh mục này sang trạng thái 'Theo dõi' không  ?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    //                {
                    //                    foreach (var item in results)
                    //                    {
                    //                        List<string> temp = new List<string>() { item.ToString() };
                    //                        foreach (string itemString in temp)
                    //                        {
                    //                            item.IsActive = chkIsActive.CheckState == CheckState.Checked ? false : true;
                    //                            _IAccountService.Update(item);
                    //                        }
                    //                    }
                    //                }
                    //            }
                    //        }
                    //        else
                    //        {

                    //            foreach (var item in results)
                    //            {
                    //                List<string> temp = new List<string>() { item.ToString() };
                    //                foreach (string itemString in temp)
                    //                {
                    //                    item.IsActive = chkIsActive.CheckState == CheckState.Checked ? false : true;
                    //                    _IAccountService.Update(item);
                    //                }
                    //            }
                    //        }
                    //    }
                    //    else
                    //    {
                    //        foreach (var item in results)
                    //        {
                    //            item.IsActive = chkIsActive.CheckState == CheckState.Checked ? false : true;
                    //            _IAccountService.Update(item);
                    //        }
                    //    }
                    //}
                    #endregion
                    //Lưu đối tượng trước khi sửa
                    Account tempOriginal = (Account)Utils.CloneObject(temp1);
                    //Lấy về đối tượng chọn trong combobox
                    Account objParentCombox = (Account)Utils.getSelectCbbItem(cbbParentID);
                    //Không thay đổi đối tượng cha trong combobox
                    if (((temp1.ParentID == null) && (objParentCombox == null)) || ((temp1.ParentID != null) && (objParentCombox != null) && (temp1.ParentID == objParentCombox.ID)))
                    {
                        temp1 = ObjandGUI(temp1, true);
                        _IAccountService.Update(temp1);
                    }
                    //Thay đổi đối tượng trong combobox
                    else
                    {
                        //Không cho phép chuyển ngược
                        //if (objParentCombox != null)
                        //{

                        //}
                        //Trường hợp có cha cũ
                        //Kiểm tra cha cũ có còn con ngoài con chuyển đi hay không
                        //Chuyển trạng thái IsParentNode Cha cũ nếu hết con
                        if (temp1.ParentID != null)
                        {
                            Account oldParent = _IAccountService.Getbykey((Guid)temp1.ParentID);
                            int checkChildOldParent = _IAccountService.GetCountChildren((Guid)temp1.ParentID);
                            if (checkChildOldParent <= 1)
                            {
                                oldParent.IsParentNode = false;
                            }
                            _IAccountService.Update(oldParent);
                        }
                        //Trường hợp có cha mới
                        //Chuyển trạng IsParentNode cha mới
                        if (objParentCombox != null)
                        {
                            Account newParent = _IAccountService.Getbykey(objParentCombox.ID);
                            newParent.IsParentNode = true;
                            _IAccountService.Update(newParent);

                            List<Account> listChildNewParent = _IAccountService.GetListChildren(objParentCombox.ID);
                            //Tính lại OrderFixCode cho đối tượng chính
                            List<string> lstOfcChildNewParent = new List<string>();
                            foreach (var item in listChildNewParent)
                            {
                                //lstOfcChildNewParent.Add(item.OrderFixCode);
                            }

                            temp1.Grade = newParent.Grade + 1;
                        }
                        //Trường hợp không có cha mới tức là chuyển đối tượng lên bậc 1
                        if (objParentCombox == null)
                        {
                            //Lấy về tất cả đối tượng bậc 1
                            List<int> lstOfcChildGradeNo1 = new List<int>();
                            List<Account> listChildGradeNo1 = _IAccountService.GetListChildren(1);
                            foreach (var item in listChildGradeNo1)
                            {
                                //lstOfcChildGradeNo1.Add(Convert.ToInt32(item.OrderFixCode));
                            }

                            temp1.Grade = 1;

                            temp1 = ObjandGUI(temp1, true);
                            _IAccountService.Update(temp1);
                        }
                    }
                    #region Thiết lập trạng thái hoạt động cho cha và con <mẩu chuẩn>
                    //nếu cha ngừng theo dõi thì con cũng ngừng theo dõi
                    List<Account> lstChild2 = _IAccountService.GetListChildrenStartWith(tempOriginal.AccountNumber);
                    if (chkIsActive.Checked)
                    {
                        foreach (var item2 in lstChild2)
                        {
                            item2.IsActive = true;
                            _IAccountService.Update(item2);
                        }

                    }
                    else
                    {
                        if (temp1.IsParentNode.Equals("0"))
                        {
                            foreach (var item2 in lstChild2)
                            {
                                item2.IsActive = true;
                                _IAccountService.Update(item2);
                            }
                        }
                        else
                        {
                            if (temp1.IsParentNode)
                            {
                                if (MSG.Question(string.Format(resSystem.MSG_Catalog_Tree1, "Tài khoản")) == System.Windows.Forms.DialogResult.Yes)
                                {
                                    foreach (var item2 in lstChild2)
                                    {
                                        item2.IsActive = false;
                                        _IAccountService.Update(item2);
                                    }
                                }
                            }
                        }

                    }

                    //nếu cha isactive = false thì con ko được isactive = true,

                    if (temp1.IsParentNode == false)
                    {
                        //lấy cha

                        if (temp1.ParentID == null)
                        {
                            if (!CheckError()) return;
                            if (chkIsActive.Checked) temp1.IsActive = true;
                            else temp1.IsActive = false;
                            _IAccountService.Update(temp1);
                        }

                        else
                        {
                            Account Parent = _IAccountService.Getbykey(objParentCombox.ID);
                            if (Parent.IsActive == true)
                            {
                                if (!CheckError()) return;
                                if (chkIsActive.Checked) temp1.IsActive = true;
                                else temp1.IsActive = false;
                                _IAccountService.Update(temp1);
                            }
                            else
                            {
                                if (chkIsActive.Checked)
                                    MSG.Warning(string.Format(resSystem.MSG_Catalog_Tree2, "Tài khoản"));
                                _IAccountService.RolbackTran();
                            }
                        }

                    }

                    else //isParentNode = true
                    {
                        if (temp1.ParentID != null) // kiem tra xem có cha không
                        {
                            Account Parent = _IAccountService.Getbykey(objParentCombox.ID);
                            if (Parent.IsActive == true)
                            {
                                if (!CheckError()) return;
                                if (chkIsActive.Checked) temp1.IsActive = true;
                                else temp1.IsActive = false;
                                _IAccountService.Update(temp1);
                            }
                            else
                            {
                                if (chkIsActive.Checked)
                                    MSG.Error(string.Format(resSystem.MSG_Catalog_Tree2, "Tài khoản"));
                                _IAccountService.RolbackTran();
                            }
                        }
                        else
                        {
                            if (!CheckError()) return;
                            if (chkIsActive.Checked) temp1.IsActive = true;
                            else temp1.IsActive = false;
                            _IAccountService.Update(temp1);
                        }
                    }
                    #endregion
                }

                _IAccountService.CommitTran();
                Utils.ListAccount.Clear();
                OPNUntil.ListAccountFollowCurrency(true);
            }
            catch (Exception ex)
            {
                _IAccountService.RolbackTran();
            }
            #endregion

            #region xử lý form, kết thúc form
            this.Close();
            isClose = false;
            #endregion
        }
        /// <summary>
        /// Sự kiện Checked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkDetailType_CheckedChanged(object sender, EventArgs e)
        {
            if (!chkDetailType.Checked)
            {
                SetChkEquasFalse();
            }
            else if (chkDetailType.Checked)
            {
                chk4DoiTuongTapHopChiPhi.Enabled = true;
                chk5DoiTuongHopDong.Enabled = true;
                chk6VTHHCCDC.Enabled = true;
                chk7TaiKhoanNganHang.Enabled = true;
                chk8ChiTietTheoNgoaiTe.Enabled = true;
                chk9theodoiphatsinhtheophongban.Enabled = true;
                chk10theodoiphatsinhtheokhoanmucchiphi.Enabled = true;
                chk11theodoipstheomucthuchi.Enabled = true;
                chkGroupOBJ.Enabled = true;
                ChekNgoaite();
            }
        }
        /// <summary>
        /// Sự kiện Checked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultraCheckEditor1_CheckedChanged(object sender, EventArgs e)
        {
            CbbDoituong.SelectedIndex = 0;
            if (chkGroupOBJ.Checked)
            {
                CbbDoituong.Items.Clear();
                CbbDoituong.Items.Add(0, "Nhà Cung Cấp");
                CbbDoituong.Items.Add(1, "Khách Hàng");
                CbbDoituong.Items.Add(2, "Nhân Viên");
                if(_Select.AccountNumber=="3388"|| _Select.AccountNumber == "1388")//trungnq thêm để sửa bug 6190
                {
                    CbbDoituong.Items.Add(3, "Đối tượng khác");
                }
                CbbDoituong.Enabled = true;
                CbbDoituong.SelectedIndex = 0;
                chk6VTHHCCDC.Enabled = false;
                chk6VTHHCCDC.Checked = false;
                CbbDoituong.Enabled = true;
            }
            else
            {
                CbbDoituong.Enabled = false;
                CbbDoituong.Items.Clear();
                chk6VTHHCCDC.Enabled = true;
            }
        }

        /// <summary>
        /// Sự kiện nút đóng
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            isClose = true;
            this.Close();
        }
        #endregion

        #region Utils
        /// <summary>
        /// get hoặc set một đối tượng vào giao diện xử lý
        /// </summary>
        /// <param name="input">đối tượng</param>
        /// <param name="isGet">True: get giá trị từ control gán vào đối tượng| False: set giá trị đối tượng vào control</param>
        /// <returns></returns>
        Account ObjandGUI(Account input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();  //Thêm mới

                input.AccountNumber = txtAccountNumber.Text;    //Số tài khoản
                input.AccountName = txtAccountName.Text;        //Tên tài khoản
                input.AccountNameGlobal = txtAccountNameGlobal.Text;    //Tên khác
                input.Description = txtDescription.Text;
                //Tài khoản tổng hợp
                Account temp0 = (Account)Utils.getSelectCbbItem(cbbParentID);
                if (temp0 == null) input.ParentID = null;
                else input.ParentID = temp0.ID;
                //Bậc của tài khoản (3 ký tự = bậc 1, 4 ký tự = bậc 2, 5 ký tự = bậc 3, còn lại là bậc 0 <ngoại lệ>)
                input.Grade = (input.AccountNumber.ToString().Length == 3 ? 1 : input.AccountNumber.ToString().Length == 4 ? 2 : input.AccountNumber.ToString().Length == 5 ? 3 : 0);
                //Nhóm tài khoản
                AccountGroup temp1 = (AccountGroup)Utils.getSelectCbbItem(cbbAccountGroupID);
                if (temp1 == null) input.AccountGroupID = string.Empty;
                else input.AccountGroupID = temp1.ID;
                input.AccountGroupKind = cbbAccountGroupKind.SelectedIndex;
                input.IsActive = chkIsActive.Checked;
                input.IsForeignCurrency = chkIsForeignCurrency.Checked;
                input.DetailType = GetDetailType();
                input.DetailByAccountObject = chkGroupOBJ.Checked ? 1 : 0;

            }
            else
            {

                txtAccountNumber.Text = input.AccountNumber.ToString();
                txtAccountName.Text = input.AccountName;
                txtAccountNameGlobal.Text = input.AccountNameGlobal;
                //tài khoản tổng hợp
                foreach (var item in cbbParentID.Rows)
                {
                    if ((item.ListObject as Account).ID == input.ParentID) cbbParentID.SelectedRow = item;
                }
                //nhóm tài khoản
                foreach (var item in cbbAccountGroupID.Rows)
                {
                    if ((item.ListObject as AccountGroup).ID == input.AccountGroupID) cbbAccountGroupID.SelectedRow = item;
                }
                cbbAccountGroupKind.SelectedIndex = input.AccountGroupKind;
                txtDescription.Text = input.Description;
                SetDetailType(input.DetailType);
                chkIsActive.Checked = input.IsActive;
                chkIsForeignCurrency.Checked = input.IsForeignCurrency;

            }
            return input;
        }
        bool CheckError()
        {
            bool kq = true;
            //Check Error Chung

            //Check Error Riêng từng Form
            if (string.IsNullOrEmpty(txtAccountNumber.Text) || string.IsNullOrEmpty(txtAccountName.Text))
            {
                MSG.Warning(resSystem.MSG_System_03);
                return false;
            }
            List<string> list = _IAccountService.GetFullListAccountNumber();

            foreach (var item in list)
            {
                if (item == txtAccountNumber.Text && IsAdd)
                {
                    MSG.Warning(string.Format(resSystem.MSG_Catalog, "Tài khoản", item));
                    txtAccountNumber.Focus();
                    return false;
                }
            }
            //trungnq check ràng buộc khi tài khoản cha đã có số dư hoặc phát sinh chứng từ liên quan thì không cho mở tk con
            if (cbbParentID.Text != null)
            {
                if (cbbParentID.Text.ToString() != "")
                {
                    Account objParentCombox = (Account)Utils.getSelectCbbItem(cbbParentID);
                    if (_IGeneralLedgerService.Query.Any(n => n.Account == objParentCombox.AccountNumber || n.AccountCorresponding == objParentCombox.AccountNumber)||Utils.CheckAccountIncurredData(objParentCombox.AccountNumber))
                    {
                        MSG.Warning("Không thể thêm mới do tài khoản cha đã có phát sinh chứng từ hoặc có số dư đầu kỳ");
                        return false;

                    }
                }
            }
            // Kiểm tra số tài khoản chỉ dc phép là số
            //string AccountNumber = "^\\d+$";
            //Regex objAccountNumber = new Regex(AccountNumber);
            //Match ma = objAccountNumber.Match(txtAccountNumber.Text);

            //if (!ma.Success)
            //{
            //    MSG.Warning(resSystem.MSG_Catalog_Account3);
            //    txtAccountNumber.Focus();
            //    return false;
            //}
            #region Check "số tài khoản chi tiết phải bắt đầu bằng số tài khoản tổng hợp"
            string strTKchitiet = txtAccountNumber.Text;
            string strTKtonghop = cbbParentID.Text;
            if (!((strTKchitiet.Length > strTKtonghop.Length) && strTKchitiet.Substring(0, strTKtonghop.Length).Contains(strTKtonghop)))
            {
                MSG.Warning(resSystem.MSG_Catalog_Account4);
                return false;
            }
            if (strTKchitiet.Contains("007"))
            {
                chk8ChiTietTheoNgoaiTe.Checked = true;
                chk8ChiTietTheoNgoaiTe.Enabled = false;
            }


            if (cbbAccountGroupID.Value == null)
            {
                MSG.Warning(resSystem.MSG_Catalog_Account5);
                cbbAccountGroupID.Focus();
                return false;
            }
            #endregion

            return kq;
        }
        /// <summary>
        /// lấy giá trị DetailType từ các control checkbox thành một giá trị chuỗi để gán vào thuộc tính DetailType của đối tượng
        /// </summary>
        /// <returns></returns>
        string GetDetailType()
        {
            string kq = string.Empty;
            if (!chkDetailType.Checked) return "-1";
            else
            {
                List<string> ltemp = new List<string>();
                if (chkGroupOBJ.Checked)
                {
                    if (CbbDoituong.SelectedIndex == 0) ltemp.Add(CbbDoituong.SelectedItem.DataValue.ToString());
                    if (CbbDoituong.SelectedIndex == 1) ltemp.Add(CbbDoituong.SelectedItem.DataValue.ToString());
                    if (CbbDoituong.SelectedIndex == 2) ltemp.Add(CbbDoituong.SelectedItem.DataValue.ToString());
                    
                }
                if (chk4DoiTuongTapHopChiPhi.Checked) ltemp.Add("3");
                if (chk5DoiTuongHopDong.Checked) ltemp.Add("4");
                if (chk6VTHHCCDC.Checked) ltemp.Add("5");
                if (chk7TaiKhoanNganHang.Checked) ltemp.Add("6");
                if (chk8ChiTietTheoNgoaiTe.Checked) ltemp.Add("8");
                if (chk9theodoiphatsinhtheophongban.Checked) ltemp.Add("9");
                if (chk10theodoiphatsinhtheokhoanmucchiphi.Checked) ltemp.Add("7");
                if (chk11theodoipstheomucthuchi.Checked) ltemp.Add("10");
                if (chkGroupOBJ.Checked)
                {
                    
                    if (CbbDoituong.SelectedIndex == 3) ltemp.Add("11");//trungnq thêm sửa bug 6190
                }
                kq = string.Join(";", ltemp.ToArray());
                return kq;
            }

        }
        /// <summary>
        /// sét Giá tri Enabled cho chk
        /// </summary>
        /// <returns></returns>
        private void SetChkEquasFalse()
        {
            if (chk4DoiTuongTapHopChiPhi.Enabled) chk4DoiTuongTapHopChiPhi.Checked = false;
            if (chk5DoiTuongHopDong.Enabled) chk5DoiTuongHopDong.Checked = false;
            if (chk6VTHHCCDC.Enabled) chk6VTHHCCDC.Checked = false;
            if (chk7TaiKhoanNganHang.Enabled) chk7TaiKhoanNganHang.Checked = false;
            if (chk8ChiTietTheoNgoaiTe.Enabled) chk8ChiTietTheoNgoaiTe.Checked = false;
            if (chk9theodoiphatsinhtheophongban.Enabled) chk9theodoiphatsinhtheophongban.Checked = false;
            if (chk10theodoiphatsinhtheokhoanmucchiphi.Enabled) chk10theodoiphatsinhtheokhoanmucchiphi.Checked = false;
            if (chk11theodoipstheomucthuchi.Enabled) chk11theodoipstheomucthuchi.Checked = false;
            if (chkGroupOBJ.Enabled)
            {
                chkGroupOBJ.Checked = false;
                CbbDoituong.Items.Clear();
            }



            chk4DoiTuongTapHopChiPhi.Enabled = false;
            chk5DoiTuongHopDong.Enabled = false;
            chk6VTHHCCDC.Enabled = false;
            chk7TaiKhoanNganHang.Enabled = false;
            chk8ChiTietTheoNgoaiTe.Enabled = false;
            chk9theodoiphatsinhtheophongban.Enabled = false;
            chk10theodoiphatsinhtheokhoanmucchiphi.Enabled = false;
            chk11theodoipstheomucthuchi.Enabled = false;
            chkGroupOBJ.Enabled = false;
            CbbDoituong.Enabled = false;



        }
        /// <summary>
        /// set giá trị DetailType từ thuộc tính của đối tượng xuống các control checkbox DetailType
        /// </summary>
        /// <param name="input">chuỗi đầu vào dạng 1,2,3,4,5,6...</param>
        void SetDetailType(string input)
        {
            if (string.IsNullOrEmpty(input)) return;
            SetChkEquasFalse();

            if (input.Equals("-1"))
            {
                chkDetailType.Checked = false; return;
            }

            try
            {//nếu có giá trị DetailType thì cắt chuỗi, check và gán lần lượt giá trị cho control
                chkDetailType.Checked = true;
                List<string> ltemp = input.Split(';').ToList();
                foreach (string item in ltemp)
                {

                    if (item.Equals("0"))
                    {
                        chkGroupOBJ.Checked = true;
                        CbbDoituong.SelectedIndex = 0;
                    }

                    if (item.Equals("1"))
                    {
                        chkGroupOBJ.Checked = true;
                        CbbDoituong.SelectedIndex = 1;
                    }

                    if (item.Equals("2"))
                    {
                        chkGroupOBJ.Checked = true;
                        CbbDoituong.SelectedIndex = 2;
                    }
                    
                    if (item.Equals("3")) chk4DoiTuongTapHopChiPhi.Checked = true;
                    if (item.Equals("4")) chk5DoiTuongHopDong.Checked = true;
                    if (item.Equals("5")) chk6VTHHCCDC.Checked = true;
                    if (item.Equals("6")) chk7TaiKhoanNganHang.Checked = true;
                    if (item.Equals("7")) chk10theodoiphatsinhtheokhoanmucchiphi.Checked = true;
                    if (item.Equals("8")) chk8ChiTietTheoNgoaiTe.Checked = true;
                    if (item.Equals("9")) chk9theodoiphatsinhtheophongban.Checked = true;
                    if (item.Equals("10")) chk11theodoipstheomucthuchi.Checked = true;
                    if (item.Equals("11"))//trungnq thêm sửa bug 6190
                    {
                        chkGroupOBJ.Checked = true;
                        CbbDoituong.SelectedIndex = 3;
                    }
                }
            }
            catch { }
        }
        /// <summary>
        /// set 
        /// </summary>
        /// <param name="input"></param>
        private void chk6VTHHCCDC_CheckedChanged(object sender, EventArgs e)
        {
            if (chk6VTHHCCDC.Checked)
            {
                chkGroupOBJ.Enabled = false;
                chkGroupOBJ.Checked = false;
            }
            else
            {
                chkGroupOBJ.Enabled = true;
            }
        }



        //#region chek  tồn tại của chi tiết theo
        //public void Chekchk() { 
        //if()
        //}
        //#endregion

        #endregion

        private void FAccountDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
