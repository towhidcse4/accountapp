﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.TextMessage;

namespace Accounting
{
    public partial class Form1 : MyClass
    {
        public Form1()
        {
            InitializeComponent();
        }

        public override void InitValueFrm()
        {
            base.InitValueFrm();
            NameTable = ConstDatabase.Account_TableName;
            IsGridShow = true;
        }
    }

    public class MyClass : BaseCatalog<Core.Domain.Account>
    {

    }
}
