﻿namespace Accounting
{
    partial class FAccountDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.chkIsForeignCurrency = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.cbbAccountGroupID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbbParentID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtDescription = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountNameGlobal = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbAccountGroupKind = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.txtAccountName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtAccountNumber = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.chkIsActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.chkDetailType = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.chk4DoiTuongTapHopChiPhi = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.chk5DoiTuongHopDong = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.chk6VTHHCCDC = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.chk7TaiKhoanNganHang = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.chk8ChiTietTheoNgoaiTe = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.chk9theodoiphatsinhtheophongban = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.chk10theodoiphatsinhtheokhoanmucchiphi = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.chk11theodoipstheomucthuchi = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.chkGroupOBJ = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.CbbDoituong = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsForeignCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountGroupID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbParentID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountNameGlobal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountGroupKind)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDetailType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk4DoiTuongTapHopChiPhi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk5DoiTuongHopDong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk6VTHHCCDC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk7TaiKhoanNganHang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk8ChiTietTheoNgoaiTe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk9theodoiphatsinhtheophongban)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk10theodoiphatsinhtheokhoanmucchiphi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk11theodoipstheomucthuchi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGroupOBJ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbbDoituong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox1.Controls.Add(this.chkIsForeignCurrency);
            this.ultraGroupBox1.Controls.Add(this.cbbAccountGroupID);
            this.ultraGroupBox1.Controls.Add(this.cbbParentID);
            this.ultraGroupBox1.Controls.Add(this.txtDescription);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel7);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel6);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel5);
            this.ultraGroupBox1.Controls.Add(this.txtAccountNameGlobal);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel4);
            this.ultraGroupBox1.Controls.Add(this.cbbAccountGroupKind);
            this.ultraGroupBox1.Controls.Add(this.txtAccountName);
            this.ultraGroupBox1.Controls.Add(this.txtAccountNumber);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel3);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel1);
            appearance8.FontData.BoldAsString = "True";
            this.ultraGroupBox1.HeaderAppearance = appearance8;
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(6, 1);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(388, 291);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // chkIsForeignCurrency
            // 
            this.chkIsForeignCurrency.BackColor = System.Drawing.Color.Transparent;
            this.chkIsForeignCurrency.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsForeignCurrency.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkIsForeignCurrency.Location = new System.Drawing.Point(130, 265);
            this.chkIsForeignCurrency.Name = "chkIsForeignCurrency";
            this.chkIsForeignCurrency.Size = new System.Drawing.Size(212, 22);
            this.chkIsForeignCurrency.TabIndex = 69;
            this.chkIsForeignCurrency.Text = "Hạch toán ngoại tệ";
            // 
            // cbbAccountGroupID
            // 
            this.cbbAccountGroupID.AutoSize = false;
            this.cbbAccountGroupID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountGroupID.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbAccountGroupID.Location = new System.Drawing.Point(130, 135);
            this.cbbAccountGroupID.Name = "cbbAccountGroupID";
            this.cbbAccountGroupID.NullText = "<Chọn dữ liệu>";
            this.cbbAccountGroupID.Size = new System.Drawing.Size(244, 22);
            this.cbbAccountGroupID.TabIndex = 54;
            // 
            // cbbParentID
            // 
            this.cbbParentID.AutoSize = false;
            this.cbbParentID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbParentID.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbParentID.Location = new System.Drawing.Point(130, 108);
            this.cbbParentID.Name = "cbbParentID";
            this.cbbParentID.NullText = "<Chọn dữ liệu>";
            this.cbbParentID.Size = new System.Drawing.Size(244, 22);
            this.cbbParentID.TabIndex = 53;
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(129, 189);
            this.txtDescription.MaxLength = 512;
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(245, 74);
            this.txtDescription.TabIndex = 56;
            // 
            // ultraLabel7
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.TextVAlignAsString = "Middle";
            this.ultraLabel7.Appearance = appearance1;
            this.ultraLabel7.Location = new System.Drawing.Point(9, 189);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(104, 22);
            this.ultraLabel7.TabIndex = 18;
            this.ultraLabel7.Text = "Diễn giải";
            // 
            // ultraLabel6
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.TextVAlignAsString = "Middle";
            this.ultraLabel6.Appearance = appearance2;
            this.ultraLabel6.Location = new System.Drawing.Point(9, 135);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(105, 22);
            this.ultraLabel6.TabIndex = 16;
            this.ultraLabel6.Text = "Nhóm tài khoản (*)";
            // 
            // ultraLabel5
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance3;
            this.ultraLabel5.Location = new System.Drawing.Point(9, 108);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(105, 22);
            this.ultraLabel5.TabIndex = 14;
            this.ultraLabel5.Text = "Tài khoản tổng hợp";
            // 
            // txtAccountNameGlobal
            // 
            this.txtAccountNameGlobal.AutoSize = false;
            this.txtAccountNameGlobal.Location = new System.Drawing.Point(130, 81);
            this.txtAccountNameGlobal.MaxLength = 512;
            this.txtAccountNameGlobal.Name = "txtAccountNameGlobal";
            this.txtAccountNameGlobal.Size = new System.Drawing.Size(244, 22);
            this.txtAccountNameGlobal.TabIndex = 52;
            // 
            // ultraLabel4
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance4;
            this.ultraLabel4.Location = new System.Drawing.Point(9, 81);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(118, 22);
            this.ultraLabel4.TabIndex = 12;
            this.ultraLabel4.Text = "Tên tiếng Anh";
            // 
            // cbbAccountGroupKind
            // 
            this.cbbAccountGroupKind.AutoSize = false;
            this.cbbAccountGroupKind.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountGroupKind.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbbAccountGroupKind.Location = new System.Drawing.Point(129, 162);
            this.cbbAccountGroupKind.Name = "cbbAccountGroupKind";
            this.cbbAccountGroupKind.Size = new System.Drawing.Size(244, 22);
            this.cbbAccountGroupKind.TabIndex = 55;
            // 
            // txtAccountName
            // 
            this.txtAccountName.AutoSize = false;
            this.txtAccountName.Location = new System.Drawing.Point(130, 54);
            this.txtAccountName.MaxLength = 512;
            this.txtAccountName.Name = "txtAccountName";
            this.txtAccountName.Size = new System.Drawing.Size(244, 22);
            this.txtAccountName.TabIndex = 51;
            // 
            // txtAccountNumber
            // 
            this.txtAccountNumber.AutoSize = false;
            this.txtAccountNumber.Location = new System.Drawing.Point(130, 27);
            this.txtAccountNumber.MaxLength = 25;
            this.txtAccountNumber.Name = "txtAccountNumber";
            this.txtAccountNumber.Size = new System.Drawing.Size(244, 22);
            this.txtAccountNumber.TabIndex = 50;
            // 
            // ultraLabel3
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance5;
            this.ultraLabel3.Location = new System.Drawing.Point(9, 162);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(105, 22);
            this.ultraLabel3.TabIndex = 2;
            this.ultraLabel3.Text = "Tính chất (*)";
            // 
            // ultraLabel2
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance6;
            this.ultraLabel2.Location = new System.Drawing.Point(9, 54);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(118, 22);
            this.ultraLabel2.TabIndex = 1;
            this.ultraLabel2.Text = "Tên tài khoản (*)";
            // 
            // ultraLabel1
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance7;
            this.ultraLabel1.Location = new System.Drawing.Point(9, 27);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(117, 22);
            this.ultraLabel1.TabIndex = 0;
            this.ultraLabel1.Text = "Số tài khoản (*)";
            // 
            // btnSave
            // 
            appearance9.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance9;
            this.btnSave.Location = new System.Drawing.Point(543, 294);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 70;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            appearance10.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance10;
            this.btnClose.Location = new System.Drawing.Point(624, 294);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 71;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // chkIsActive
            // 
            this.chkIsActive.BackColor = System.Drawing.Color.Transparent;
            this.chkIsActive.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsActive.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkIsActive.Checked = true;
            this.chkIsActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIsActive.Location = new System.Drawing.Point(20, 298);
            this.chkIsActive.Name = "chkIsActive";
            this.chkIsActive.Size = new System.Drawing.Size(119, 22);
            this.chkIsActive.TabIndex = 69;
            this.chkIsActive.Text = "Hoạt động";
            // 
            // chkDetailType
            // 
            this.chkDetailType.BackColor = System.Drawing.Color.Transparent;
            this.chkDetailType.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkDetailType.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkDetailType.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkDetailType.Location = new System.Drawing.Point(189, 0);
            this.chkDetailType.Name = "chkDetailType";
            this.chkDetailType.Size = new System.Drawing.Size(15, 22);
            this.chkDetailType.TabIndex = 58;
            this.chkDetailType.CheckedChanged += new System.EventHandler(this.chkDetailType_CheckedChanged);
            // 
            // chk4DoiTuongTapHopChiPhi
            // 
            this.chk4DoiTuongTapHopChiPhi.BackColor = System.Drawing.Color.Transparent;
            this.chk4DoiTuongTapHopChiPhi.BackColorInternal = System.Drawing.Color.Transparent;
            this.chk4DoiTuongTapHopChiPhi.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chk4DoiTuongTapHopChiPhi.Location = new System.Drawing.Point(22, 78);
            this.chk4DoiTuongTapHopChiPhi.Name = "chk4DoiTuongTapHopChiPhi";
            this.chk4DoiTuongTapHopChiPhi.Size = new System.Drawing.Size(147, 22);
            this.chk4DoiTuongTapHopChiPhi.TabIndex = 61;
            this.chk4DoiTuongTapHopChiPhi.Text = "Đối tượng tập hợp chi phí";
            // 
            // chk5DoiTuongHopDong
            // 
            this.chk5DoiTuongHopDong.BackColor = System.Drawing.Color.Transparent;
            this.chk5DoiTuongHopDong.BackColorInternal = System.Drawing.Color.Transparent;
            this.chk5DoiTuongHopDong.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chk5DoiTuongHopDong.Location = new System.Drawing.Point(22, 104);
            this.chk5DoiTuongHopDong.Name = "chk5DoiTuongHopDong";
            this.chk5DoiTuongHopDong.Size = new System.Drawing.Size(147, 22);
            this.chk5DoiTuongHopDong.TabIndex = 62;
            this.chk5DoiTuongHopDong.Text = "Đối tượng hợp đồng";
            // 
            // chk6VTHHCCDC
            // 
            this.chk6VTHHCCDC.BackColor = System.Drawing.Color.Transparent;
            this.chk6VTHHCCDC.BackColorInternal = System.Drawing.Color.Transparent;
            this.chk6VTHHCCDC.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chk6VTHHCCDC.Location = new System.Drawing.Point(22, 130);
            this.chk6VTHHCCDC.Name = "chk6VTHHCCDC";
            this.chk6VTHHCCDC.Size = new System.Drawing.Size(147, 22);
            this.chk6VTHHCCDC.TabIndex = 63;
            this.chk6VTHHCCDC.Text = "VTHH, CCDC";
            this.chk6VTHHCCDC.CheckedChanged += new System.EventHandler(this.chk6VTHHCCDC_CheckedChanged);
            // 
            // chk7TaiKhoanNganHang
            // 
            this.chk7TaiKhoanNganHang.BackColor = System.Drawing.Color.Transparent;
            this.chk7TaiKhoanNganHang.BackColorInternal = System.Drawing.Color.Transparent;
            this.chk7TaiKhoanNganHang.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chk7TaiKhoanNganHang.Location = new System.Drawing.Point(22, 156);
            this.chk7TaiKhoanNganHang.Name = "chk7TaiKhoanNganHang";
            this.chk7TaiKhoanNganHang.Size = new System.Drawing.Size(147, 22);
            this.chk7TaiKhoanNganHang.TabIndex = 64;
            this.chk7TaiKhoanNganHang.Text = "Tài khoản ngân hàng";
            // 
            // chk8ChiTietTheoNgoaiTe
            // 
            this.chk8ChiTietTheoNgoaiTe.BackColor = System.Drawing.Color.Transparent;
            this.chk8ChiTietTheoNgoaiTe.BackColorInternal = System.Drawing.Color.Transparent;
            this.chk8ChiTietTheoNgoaiTe.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chk8ChiTietTheoNgoaiTe.Location = new System.Drawing.Point(22, 182);
            this.chk8ChiTietTheoNgoaiTe.Name = "chk8ChiTietTheoNgoaiTe";
            this.chk8ChiTietTheoNgoaiTe.Size = new System.Drawing.Size(147, 22);
            this.chk8ChiTietTheoNgoaiTe.TabIndex = 65;
            this.chk8ChiTietTheoNgoaiTe.Text = "Ngoại tệ";
            // 
            // chk9theodoiphatsinhtheophongban
            // 
            this.chk9theodoiphatsinhtheophongban.BackColor = System.Drawing.Color.Transparent;
            this.chk9theodoiphatsinhtheophongban.BackColorInternal = System.Drawing.Color.Transparent;
            this.chk9theodoiphatsinhtheophongban.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chk9theodoiphatsinhtheophongban.Location = new System.Drawing.Point(22, 207);
            this.chk9theodoiphatsinhtheophongban.Name = "chk9theodoiphatsinhtheophongban";
            this.chk9theodoiphatsinhtheophongban.Size = new System.Drawing.Size(201, 22);
            this.chk9theodoiphatsinhtheophongban.TabIndex = 66;
            this.chk9theodoiphatsinhtheophongban.Text = "Phát sinh theo phòng ban";
            // 
            // chk10theodoiphatsinhtheokhoanmucchiphi
            // 
            this.chk10theodoiphatsinhtheokhoanmucchiphi.BackColor = System.Drawing.Color.Transparent;
            this.chk10theodoiphatsinhtheokhoanmucchiphi.BackColorInternal = System.Drawing.Color.Transparent;
            this.chk10theodoiphatsinhtheokhoanmucchiphi.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chk10theodoiphatsinhtheokhoanmucchiphi.Location = new System.Drawing.Point(22, 234);
            this.chk10theodoiphatsinhtheokhoanmucchiphi.Name = "chk10theodoiphatsinhtheokhoanmucchiphi";
            this.chk10theodoiphatsinhtheokhoanmucchiphi.Size = new System.Drawing.Size(212, 22);
            this.chk10theodoiphatsinhtheokhoanmucchiphi.TabIndex = 67;
            this.chk10theodoiphatsinhtheokhoanmucchiphi.Text = "Phát sinh theo khoản mục CP";
            // 
            // chk11theodoipstheomucthuchi
            // 
            this.chk11theodoipstheomucthuchi.BackColor = System.Drawing.Color.Transparent;
            this.chk11theodoipstheomucthuchi.BackColorInternal = System.Drawing.Color.Transparent;
            this.chk11theodoipstheomucthuchi.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chk11theodoipstheomucthuchi.Location = new System.Drawing.Point(22, 260);
            this.chk11theodoipstheomucthuchi.Name = "chk11theodoipstheomucthuchi";
            this.chk11theodoipstheomucthuchi.Size = new System.Drawing.Size(212, 22);
            this.chk11theodoipstheomucthuchi.TabIndex = 68;
            this.chk11theodoipstheomucthuchi.Text = "Phát sinh theo mục thu/chi";
            // 
            // chkGroupOBJ
            // 
            appearance11.TextVAlignAsString = "Middle";
            this.chkGroupOBJ.Appearance = appearance11;
            this.chkGroupOBJ.BackColor = System.Drawing.Color.Transparent;
            this.chkGroupOBJ.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkGroupOBJ.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkGroupOBJ.Location = new System.Drawing.Point(22, 52);
            this.chkGroupOBJ.Name = "chkGroupOBJ";
            this.chkGroupOBJ.Size = new System.Drawing.Size(79, 22);
            this.chkGroupOBJ.TabIndex = 59;
            this.chkGroupOBJ.Text = "Đối tượng";
            this.chkGroupOBJ.CheckedChanged += new System.EventHandler(this.ultraCheckEditor1_CheckedChanged);
            // 
            // CbbDoituong
            // 
            appearance12.TextVAlignAsString = "Middle";
            this.CbbDoituong.Appearance = appearance12;
            this.CbbDoituong.AutoSize = false;
            this.CbbDoituong.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.CbbDoituong.Location = new System.Drawing.Point(131, 52);
            this.CbbDoituong.Name = "CbbDoituong";
            this.CbbDoituong.Size = new System.Drawing.Size(157, 22);
            this.CbbDoituong.TabIndex = 60;
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox2.Controls.Add(this.CbbDoituong);
            this.ultraGroupBox2.Controls.Add(this.chkGroupOBJ);
            this.ultraGroupBox2.Controls.Add(this.chk11theodoipstheomucthuchi);
            this.ultraGroupBox2.Controls.Add(this.chkDetailType);
            this.ultraGroupBox2.Controls.Add(this.chk10theodoiphatsinhtheokhoanmucchiphi);
            this.ultraGroupBox2.Controls.Add(this.chk4DoiTuongTapHopChiPhi);
            this.ultraGroupBox2.Controls.Add(this.chk9theodoiphatsinhtheophongban);
            this.ultraGroupBox2.Controls.Add(this.chk5DoiTuongHopDong);
            this.ultraGroupBox2.Controls.Add(this.chk8ChiTietTheoNgoaiTe);
            this.ultraGroupBox2.Controls.Add(this.chk6VTHHCCDC);
            this.ultraGroupBox2.Controls.Add(this.chk7TaiKhoanNganHang);
            appearance13.FontData.BoldAsString = "True";
            this.ultraGroupBox2.HeaderAppearance = appearance13;
            this.ultraGroupBox2.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox2.Location = new System.Drawing.Point(406, 1);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(308, 291);
            this.ultraGroupBox2.TabIndex = 28;
            this.ultraGroupBox2.Text = "Chi tiết theo";
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // FAccountDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(718, 333);
            this.Controls.Add(this.ultraGroupBox2);
            this.Controls.Add(this.chkIsActive);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.ultraGroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FAccountDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Account Detail";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FAccountDetail_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsForeignCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountGroupID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbParentID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountNameGlobal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountGroupKind)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDetailType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk4DoiTuongTapHopChiPhi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk5DoiTuongHopDong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk6VTHHCCDC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk7TaiKhoanNganHang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk8ChiTietTheoNgoaiTe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk9theodoiphatsinhtheophongban)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk10theodoiphatsinhtheokhoanmucchiphi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk11theodoipstheomucthuchi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGroupOBJ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbbDoituong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountNumber;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbAccountGroupKind;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDescription;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountNameGlobal;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsActive;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkDetailType;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chk4DoiTuongTapHopChiPhi;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chk5DoiTuongHopDong;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chk6VTHHCCDC;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chk7TaiKhoanNganHang;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chk8ChiTietTheoNgoaiTe;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chk9theodoiphatsinhtheophongban;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chk10theodoiphatsinhtheokhoanmucchiphi;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chk11theodoipstheomucthuchi;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbParentID;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkGroupOBJ;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor CbbDoituong;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountGroupID;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsForeignCurrency;
    }
}
