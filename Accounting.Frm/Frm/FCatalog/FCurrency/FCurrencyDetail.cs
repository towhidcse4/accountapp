﻿using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Accounting
{
    public partial class FCurrentcyDetail : DialogForm
    {
        #region khai báo
        private readonly IAccountService _IAccountService;
        private readonly ICurrencyService _ICurrencyService;

        Currency _select = new Currency();

        bool them = true;
        public static bool isClose = true;
        #endregion
        public FCurrentcyDetail()
        {
            InitializeComponent();
            this.Text = "Thêm mới Loại tiền";
            this.MaximizeBox = false;
            #region Thiet lap gia tri ban dau cho form
            _IAccountService = IoC.Resolve<IAccountService>();
            _ICurrencyService = IoC.Resolve<ICurrencyService>();
            #region  khoi tạo ultracombox
            initCbb();
            #endregion

            #endregion
            ultraGroupBox2.Visible = false ;
            them = true;
            Load_selectedRowToText(null, true);
            isClose = false;
            this.chkActive.Visible = false;
            cbbBankAccount.Visible = false;
            cbbCashAccount.Visible = false;
            ultraLabel4.Visible = false;
            ultraLabel5.Visible = false;
            ultraGroupBox1.Height = txtExchangeRate.Location.Y + 30;
            ultraPanel2.Location = new Point(ultraLabel4.Location.X, ultraLabel4.Location.Y + 20);
            this.Size = new Size(this.Width, ultraGroupBox1.Height + ultraPanel2.Height + 60);
        }

        public FCurrentcyDetail(Currency temp)
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            this.Text = "Sửa Loại tiền";
            #endregion
            #region Thiet lap gia tri ban dau cho form
            _IAccountService = IoC.Resolve<IAccountService>();
            _ICurrencyService = IoC.Resolve<ICurrencyService>();
            initCbb();
            #endregion

            _select = temp;
            them = false;
            txtID.Enabled = false;
            chkActive.Enabled = !_select.ID.Equals("VND");
            Load_selectedRowToText(temp, false);
            ObjAndGui(temp, true);
            ultraGroupBox2.Visible = false;
            isClose = false;
            cbbBankAccount.Visible = false;
            cbbCashAccount.Visible = false;
            ultraLabel4.Visible = false;
            ultraLabel5.Visible = false;
            ultraGroupBox1.Height = txtExchangeRate.Location.Y + 30;
            ultraPanel2.Location = new Point(ultraLabel4.Location.X, ultraLabel4.Location.Y + 20);
            this.Size = new Size(this.Width, ultraGroupBox1.Height + ultraPanel2.Height + 60);

            //txtExchangeRate.FormatNumberic(ConstDatabase.Format_TienVND);
        }

        #region  set value cho text
        void Load_selectedRowToText(Currency currency, bool dk)
        {

            txtID.Enabled = dk;

            #region display data on text
            if (dk)
            {
                txtID.Text = "";
                txtAfterCharater.Text = "";
                //txtBankAccount.Text = "";
                //txtCashAccount.Text = "";
                cbbCashAccount.Text = "";
                cbbBankAccount.Text = "";
                txtCurrentName.Text = "";
                txtExchangeRate.Value = "";
                txtFirstCharater.Text = "";
                txtSeparator.Text = "";
                chkActive.CheckState = CheckState.Checked;


            }
            else
            {
                if (!string.IsNullOrEmpty(currency.AfterCharater))
                {
                    txtAfterCharater.Text = currency.AfterCharater.ToString();
                }

                //txtBankAccount.Text = currency.BankAccount.ToString();
                //txtCashAccount.Text = currency.CashAccount.ToString();
                //chon thong tin hien thi cbb o day
                foreach (var item in cbbBankAccount.Rows)
                {
                    if ((item.ListObject as Account).AccountNumber.Equals(currency.BankAccount)) cbbBankAccount.SelectedRow = item;

                }
                foreach (var item in cbbCashAccount.Rows)
                {
                    if ((item.ListObject as Account).AccountNumber.Equals(currency.CashAccount)) cbbCashAccount.SelectedRow = item;
                }
                txtCurrentName.Text = currency.CurrencyName.ToString();
                if (!currency.ExchangeRate.ToString().Equals("") && !currency.ExchangeRate.ToString().Equals("0") && !currency.ExchangeRate.ToString().Equals("0,00"))
                {
                    txtExchangeRate.Value = currency.ExchangeRate.ToString();
                }
                else
                {
                    //txtExchangeRate.Value = null;
                    
                }
                if (!string.IsNullOrEmpty(currency.FirstCharater))
                {
                    txtFirstCharater.Text = currency.FirstCharater;
                }

                if (!string.IsNullOrEmpty(currency.FirstCharater))
                {
                    txtSeparator.Text = currency.SeparatorCharater;
                }

                txtID.Text = currency.ID;

                chkActive.Checked = currency.IsActive;



            }

            #endregion
        }
        #endregion

        #region khoi tao gia tri cho cbb
        void initCbb()
        {
            //khởi tạo cbb

            //cbbBankAccount.DataSource = _IAccountService.Query.Where(c => c.AccountNumber.StartsWith("111")).ToList();
            //cbbCashAccount.DataSource = _IAccountService.Query.Where(c => c.AccountNumber.StartsWith("112") ).ToList();

            cbbBankAccount.DataSource = _IAccountService.GetChildrenAccount("111");
            cbbCashAccount.DataSource = _IAccountService.GetChildrenAccount("112");

            //-----------
            cbbBankAccount.DisplayMember = "AccountNumber";
            cbbCashAccount.DisplayMember = "AccountNumber";

            //-----------
            Utils.ConfigGrid(cbbBankAccount, ConstDatabase.Account_TableName);
            Utils.ConfigGrid(cbbCashAccount, ConstDatabase.Account_TableName);

            //List<Account> lstaccount = _IAccountService.GetAll().Where(c => c.AccountGroupID.StartsWith ("1121") && c.AccountGroupID.StartsWith( "1122") && c.AccountGroupID.StartsWith("1123")).ToList();
            //#region khoi tao cho cbb
            //cbbBankAccount.DataSource = lstaccount;
            //cbbBankAccount.DisplayMember = "AccountName";
            //cbbBankAccount.ValueMember = "ID";
            //Utils.ConfigGrid(cbbBankAccount, ConstDatabase.Account_TableName);
            ////Utils.ConfigGrid(cbbBankAccount, ConstDatabase
            //List<Account> lstaccount2 = _IAccountService.GetAll().Where(c => c.AccountGroupID == "1111" &&c.AccountGroupID == "1112" && c.AccountGroupID == "1113").ToList();
            //cbbCashAccount.DisplayMember = "AccountName";
            //cbbCashAccount.ValueMember = "ID";
            //Utils.ConfigGrid(cbbCashAccount, ConstDatabase.Account_TableName);

            //#endregion
        }
        #endregion

        #region kiem tra và bắt lỗi

        bool checkValue()
        {
            if (string.IsNullOrEmpty(txtID.Text))
            {
                txtID.Focus();
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }
            else if (txtID.Text.Length > 3)
            {
                txtID.Focus();
                MSG.Error(resSystem.MSG_Catalog_FCurrencyDetail);
                return false;
            }
            else if (string.IsNullOrEmpty(txtCurrentName.Text))
            {
                txtCurrentName.Focus();
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }
            else if (string.IsNullOrEmpty(txtExchangeRate.Text) || txtExchangeRate.Text == "0" || txtExchangeRate.Text == "0,00")
            {
                MSG.Warning("Tỷ giá phải > 0");
                return false;
            }
            if (cbbBankAccount.SelectedRow != null && cbbCashAccount.SelectedRow != null)
            {
                if (((Account)Utils.getSelectCbbItem(cbbCashAccount)).ID == ((Account)Utils.getSelectCbbItem(cbbBankAccount)).ID)
                {
                    MSG.Error(resSystem.MSG_Catalog_FCurrencyDetail2);
                    cbbCashAccount.Focus();
                    return false;
                }
            }

            //if (them && _ICurrencyService.Query.Where(c => c.ID.Equals(txtID.Text)).ToList().Count > 0)
            if (them && _ICurrencyService.CountListCurrencyID(txtID.Text) > 0)
            {
                MSG.Error(string.Format(resSystem.MSG_Catalog_Account6, "Loại tiền"));
                //MSG.Error("Mã loại tiền đã bị trùng trong danh sách nhập.Xin vui lòng kiểm tra lại!");

                txtID.Focus();
                return false;
            }




            return true;

        }

        #endregion

        #region get obj
        Currency ObjAndGui(Currency obj, bool isGet)
        {

            {
                obj.ID = txtID.Text;
                obj.AfterCharater = txtAfterCharater.Text;
                obj.CurrencyName = txtCurrentName.Text;
                if (!string.IsNullOrEmpty(txtExchangeRate.Value.ToString()) && !txtExchangeRate.Value.ToString().Equals("0") && !txtExchangeRate.Value.ToString().Equals("0,00"))
                {
                    obj.ExchangeRate = decimal.Parse(txtExchangeRate.Value.ToString());
                }
                //else obj.ExchangeRate = null;


                obj.FirstCharater = txtFirstCharater.Text;
                obj.SeparatorCharater = txtSeparator.Text;
                if (!string.IsNullOrEmpty(cbbBankAccount.Text))
                {
                    obj.BankAccount = ((Account)Utils.getSelectCbbItem(cbbBankAccount)).AccountNumber;
                }
                if (!string.IsNullOrEmpty(cbbCashAccount.Text))
                {
                    obj.CashAccount = ((Account)Utils.getSelectCbbItem(cbbCashAccount)).AccountNumber;
                }

                obj.IsActive = chkActive.CheckState == CheckState.Checked ? true : false;
            }
            return obj;
        }
        #endregion

        private void btnSave_Click_1(object sender, EventArgs e)
        {
            if (!checkValue()) return;
            #region fill data to control
            Currency temp = them ? new Currency() : _ICurrencyService.Getbykey(_select.ID);

            temp = ObjAndGui(temp, true);
            #endregion

            #region thao tac database
            _ICurrencyService.BeginTran();
            if (them)
            {
                _ICurrencyService.CreateNew(temp);
            }
            else
            {
                _ICurrencyService.Update(temp);
            }
            _ICurrencyService.CommitTran();
            #endregion

            #region xử lý form, kết thúc form
            Utils.ListCurrency.Clear();
            this.Close();
            isClose = false;
            #endregion

        }

        private void btnClose_Click_1(object sender, EventArgs e)
        {
            isClose = true;
            DialogResult = DialogResult.Cancel;
            this.Close();
        }


        private void ubtnDefault_Click(object sender, EventArgs e)
        {
            #region lấy giá trị mặc định
            getDefault();
            #endregion

        }
        #region lấy giá trị mặc định
        void getDefault()
        {
            txtCurrentName.Text = _select.CurrencyName;
            txtAfterCharater.Text = _select.AfterCharaterDefault;
            txtFirstCharater.Text = _select.FirstCharacterDefault;
            txtSeparator.Text = _select.SeparatorCharaterDefault;
        }
        #endregion



        private void cbbCashAccount_ValueChanged(object sender, EventArgs e)
        {
            if (cbbBankAccount.SelectedRow != null && cbbCashAccount.SelectedRow != null)
            {
                if (((Account)Utils.getSelectCbbItem(cbbCashAccount)).ID == ((Account)Utils.getSelectCbbItem(cbbBankAccount)).ID)
                {
                    MSG.Error(resSystem.MSG_Catalog_FCurrencyDetail2);
                    cbbCashAccount.Focus();
                }

            }
        }

        private void cbbBankAccount_ValueChanged(object sender, EventArgs e)
        {
            if (cbbBankAccount.SelectedRow != null && cbbCashAccount.SelectedRow != null)
            {
                if (((Account)Utils.getSelectCbbItem(cbbCashAccount)).ID == ((Account)Utils.getSelectCbbItem(cbbBankAccount)).ID)
                {
                    MSG.Error(resSystem.MSG_Catalog_FCurrencyDetail2);
                    cbbBankAccount.Focus();
                }

            }
        }

        private void FCurrentcyDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}