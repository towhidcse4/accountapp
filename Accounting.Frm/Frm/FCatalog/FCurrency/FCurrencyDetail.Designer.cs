﻿namespace Accounting
{
    partial class FCurrentcyDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtExchangeRate = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbBankAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbbCashAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.txtCurrentName = new Infragistics.Win.FormattedLinkLabel.UltraFormattedTextEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.chkActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.txtAfterCharater = new Infragistics.Win.FormattedLinkLabel.UltraFormattedTextEditor();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.txtSeparator = new Infragistics.Win.FormattedLinkLabel.UltraFormattedTextEditor();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.txtFirstCharater = new Infragistics.Win.FormattedLinkLabel.UltraFormattedTextEditor();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel2 = new Infragistics.Win.Misc.UltraPanel();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.ultraPanel3 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ubtnDefault = new Infragistics.Win.Misc.UltraButton();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCashAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkActive)).BeginInit();
            this.ultraPanel2.ClientArea.SuspendLayout();
            this.ultraPanel2.SuspendLayout();
            this.ultraPanel3.ClientArea.SuspendLayout();
            this.ultraPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraGroupBox1);
            this.ultraPanel1.Location = new System.Drawing.Point(6, 10);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(485, 211);
            this.ultraPanel1.TabIndex = 0;
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox1.Controls.Add(this.txtExchangeRate);
            this.ultraGroupBox1.Controls.Add(this.txtID);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel10);
            this.ultraGroupBox1.Controls.Add(this.cbbBankAccount);
            this.ultraGroupBox1.Controls.Add(this.cbbCashAccount);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel5);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel4);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel3);
            this.ultraGroupBox1.Controls.Add(this.txtCurrentName);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel2);
            appearance32.FontData.BoldAsString = "True";
            this.ultraGroupBox1.HeaderAppearance = appearance32;
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOutsideBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(485, 211);
            this.ultraGroupBox1.TabIndex = 2;
            this.ultraGroupBox1.Text = "Chi tiết";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtExchangeRate
            // 
            appearance1.TextHAlignAsString = "Right";
            this.txtExchangeRate.Appearance = appearance1;
            this.txtExchangeRate.AutoSize = false;
            this.txtExchangeRate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.txtExchangeRate.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.txtExchangeRate.InputMask = "nn,nnn,nnn,nnn,nnn.nn";
            this.txtExchangeRate.Location = new System.Drawing.Point(139, 105);
            this.txtExchangeRate.Name = "txtExchangeRate";
            this.txtExchangeRate.Size = new System.Drawing.Size(304, 22);
            this.txtExchangeRate.TabIndex = 3;
            // 
            // txtID
            // 
            this.txtID.AutoSize = false;
            this.txtID.Location = new System.Drawing.Point(139, 45);
            this.txtID.MaxLength = 3;
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(304, 22);
            this.txtID.TabIndex = 1;
            // 
            // ultraLabel10
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance2.TextVAlignAsString = "Middle";
            this.ultraLabel10.Appearance = appearance2;
            this.ultraLabel10.Location = new System.Drawing.Point(9, 45);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(124, 22);
            this.ultraLabel10.TabIndex = 30;
            this.ultraLabel10.Text = "Mã tiền tệ (*)";
            // 
            // cbbBankAccount
            // 
            this.cbbBankAccount.AutoSize = false;
            appearance3.BackColor = System.Drawing.SystemColors.Window;
            appearance3.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbBankAccount.DisplayLayout.Appearance = appearance3;
            this.cbbBankAccount.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbBankAccount.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance4.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance4.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance4.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbBankAccount.DisplayLayout.GroupByBox.Appearance = appearance4;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbBankAccount.DisplayLayout.GroupByBox.BandLabelAppearance = appearance5;
            this.cbbBankAccount.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance6.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance6.BackColor2 = System.Drawing.SystemColors.Control;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance6.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbBankAccount.DisplayLayout.GroupByBox.PromptAppearance = appearance6;
            this.cbbBankAccount.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbBankAccount.DisplayLayout.MaxRowScrollRegions = 1;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            appearance7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbBankAccount.DisplayLayout.Override.ActiveCellAppearance = appearance7;
            appearance8.BackColor = System.Drawing.SystemColors.Highlight;
            appearance8.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbBankAccount.DisplayLayout.Override.ActiveRowAppearance = appearance8;
            this.cbbBankAccount.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbBankAccount.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            this.cbbBankAccount.DisplayLayout.Override.CardAreaAppearance = appearance9;
            appearance10.BorderColor = System.Drawing.Color.Silver;
            appearance10.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbBankAccount.DisplayLayout.Override.CellAppearance = appearance10;
            this.cbbBankAccount.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbBankAccount.DisplayLayout.Override.CellPadding = 0;
            appearance11.BackColor = System.Drawing.SystemColors.Control;
            appearance11.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance11.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance11.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance11.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbBankAccount.DisplayLayout.Override.GroupByRowAppearance = appearance11;
            appearance12.TextHAlignAsString = "Left";
            this.cbbBankAccount.DisplayLayout.Override.HeaderAppearance = appearance12;
            this.cbbBankAccount.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbBankAccount.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.Color.Silver;
            this.cbbBankAccount.DisplayLayout.Override.RowAppearance = appearance13;
            this.cbbBankAccount.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbBankAccount.DisplayLayout.Override.TemplateAddRowAppearance = appearance14;
            this.cbbBankAccount.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbBankAccount.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbBankAccount.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbBankAccount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbBankAccount.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbBankAccount.Location = new System.Drawing.Point(139, 135);
            this.cbbBankAccount.Name = "cbbBankAccount";
            this.cbbBankAccount.NullText = "<chọn dữ liệu>";
            this.cbbBankAccount.Size = new System.Drawing.Size(304, 22);
            this.cbbBankAccount.TabIndex = 5;
            this.cbbBankAccount.ValueChanged += new System.EventHandler(this.cbbBankAccount_ValueChanged);
            // 
            // cbbCashAccount
            // 
            this.cbbCashAccount.AutoSize = false;
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            appearance15.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbCashAccount.DisplayLayout.Appearance = appearance15;
            this.cbbCashAccount.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbCashAccount.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance16.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance16.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance16.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbCashAccount.DisplayLayout.GroupByBox.Appearance = appearance16;
            appearance17.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbCashAccount.DisplayLayout.GroupByBox.BandLabelAppearance = appearance17;
            this.cbbCashAccount.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance18.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance18.BackColor2 = System.Drawing.SystemColors.Control;
            appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance18.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbCashAccount.DisplayLayout.GroupByBox.PromptAppearance = appearance18;
            this.cbbCashAccount.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbCashAccount.DisplayLayout.MaxRowScrollRegions = 1;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            appearance19.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbCashAccount.DisplayLayout.Override.ActiveCellAppearance = appearance19;
            appearance20.BackColor = System.Drawing.SystemColors.Highlight;
            appearance20.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbCashAccount.DisplayLayout.Override.ActiveRowAppearance = appearance20;
            this.cbbCashAccount.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbCashAccount.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance21.BackColor = System.Drawing.SystemColors.Window;
            this.cbbCashAccount.DisplayLayout.Override.CardAreaAppearance = appearance21;
            appearance22.BorderColor = System.Drawing.Color.Silver;
            appearance22.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbCashAccount.DisplayLayout.Override.CellAppearance = appearance22;
            this.cbbCashAccount.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbCashAccount.DisplayLayout.Override.CellPadding = 0;
            appearance23.BackColor = System.Drawing.SystemColors.Control;
            appearance23.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance23.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance23.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbCashAccount.DisplayLayout.Override.GroupByRowAppearance = appearance23;
            appearance24.TextHAlignAsString = "Left";
            this.cbbCashAccount.DisplayLayout.Override.HeaderAppearance = appearance24;
            this.cbbCashAccount.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbCashAccount.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.Color.Silver;
            this.cbbCashAccount.DisplayLayout.Override.RowAppearance = appearance25;
            this.cbbCashAccount.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbCashAccount.DisplayLayout.Override.TemplateAddRowAppearance = appearance26;
            this.cbbCashAccount.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbCashAccount.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbCashAccount.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbCashAccount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbCashAccount.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbCashAccount.Location = new System.Drawing.Point(139, 165);
            this.cbbCashAccount.Name = "cbbCashAccount";
            this.cbbCashAccount.NullText = "<chọn dữ liệu>";
            this.cbbCashAccount.Size = new System.Drawing.Size(304, 22);
            this.cbbCashAccount.TabIndex = 4;
            this.cbbCashAccount.ValueChanged += new System.EventHandler(this.cbbCashAccount_ValueChanged);
            // 
            // ultraLabel5
            // 
            appearance27.BackColor = System.Drawing.Color.Transparent;
            appearance27.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance27.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance27;
            this.ultraLabel5.Location = new System.Drawing.Point(9, 165);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(124, 22);
            this.ultraLabel5.TabIndex = 14;
            this.ultraLabel5.Text = "Tài khoản tiền gửi";
            // 
            // ultraLabel4
            // 
            appearance28.BackColor = System.Drawing.Color.Transparent;
            appearance28.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance28.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance28;
            this.ultraLabel4.Location = new System.Drawing.Point(9, 135);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(124, 22);
            this.ultraLabel4.TabIndex = 12;
            this.ultraLabel4.Text = "Tài khoản tiền mặt";
            // 
            // ultraLabel3
            // 
            appearance29.BackColor = System.Drawing.Color.Transparent;
            appearance29.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance29.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance29;
            this.ultraLabel3.Location = new System.Drawing.Point(9, 105);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(124, 22);
            this.ultraLabel3.TabIndex = 10;
            this.ultraLabel3.Text = "Tỷ giá";
            // 
            // txtCurrentName
            // 
            appearance30.FontData.BoldAsString = "False";
            appearance30.FontData.ItalicAsString = "False";
            appearance30.FontData.Name = "Microsoft Sans Serif";
            appearance30.FontData.SizeInPoints = 8.25F;
            appearance30.FontData.StrikeoutAsString = "False";
            appearance30.FontData.UnderlineAsString = "False";
            this.txtCurrentName.Appearance = appearance30;
            this.txtCurrentName.Location = new System.Drawing.Point(139, 75);
            this.txtCurrentName.Name = "txtCurrentName";
            this.txtCurrentName.Size = new System.Drawing.Size(304, 22);
            this.txtCurrentName.TabIndex = 2;
            this.txtCurrentName.Value = "";
            // 
            // ultraLabel2
            // 
            appearance31.BackColor = System.Drawing.Color.Transparent;
            appearance31.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance31.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance31;
            this.ultraLabel2.Location = new System.Drawing.Point(9, 75);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(124, 22);
            this.ultraLabel2.TabIndex = 8;
            this.ultraLabel2.Text = "Tên tiền tệ (*)";
            // 
            // chkActive
            // 
            appearance33.BackColor = System.Drawing.Color.Transparent;
            appearance33.TextVAlignAsString = "Middle";
            this.chkActive.Appearance = appearance33;
            this.chkActive.BackColor = System.Drawing.Color.Transparent;
            this.chkActive.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkActive.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkActive.Checked = true;
            this.chkActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkActive.Location = new System.Drawing.Point(7, 13);
            this.chkActive.Name = "chkActive";
            this.chkActive.Size = new System.Drawing.Size(129, 22);
            this.chkActive.TabIndex = 10;
            this.chkActive.Text = "Hoạt động";
            // 
            // txtAfterCharater
            // 
            this.txtAfterCharater.Location = new System.Drawing.Point(139, 87);
            this.txtAfterCharater.Name = "txtAfterCharater";
            this.txtAfterCharater.Size = new System.Drawing.Size(304, 22);
            this.txtAfterCharater.TabIndex = 8;
            this.txtAfterCharater.Value = "";
            // 
            // ultraLabel8
            // 
            appearance34.BackColor = System.Drawing.Color.Transparent;
            appearance34.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance34.TextVAlignAsString = "Middle";
            this.ultraLabel8.Appearance = appearance34;
            this.ultraLabel8.Location = new System.Drawing.Point(9, 87);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(124, 22);
            this.ultraLabel8.TabIndex = 20;
            this.ultraLabel8.Text = "Ký tự cuối";
            // 
            // txtSeparator
            // 
            this.txtSeparator.Location = new System.Drawing.Point(139, 58);
            this.txtSeparator.Name = "txtSeparator";
            this.txtSeparator.Size = new System.Drawing.Size(304, 22);
            this.txtSeparator.TabIndex = 7;
            this.txtSeparator.Value = "";
            // 
            // ultraLabel7
            // 
            appearance35.BackColor = System.Drawing.Color.Transparent;
            appearance35.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance35.TextVAlignAsString = "Middle";
            this.ultraLabel7.Appearance = appearance35;
            this.ultraLabel7.Location = new System.Drawing.Point(9, 58);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(124, 22);
            this.ultraLabel7.TabIndex = 18;
            this.ultraLabel7.Text = "Phân cách";
            // 
            // txtFirstCharater
            // 
            this.txtFirstCharater.Location = new System.Drawing.Point(139, 29);
            this.txtFirstCharater.Name = "txtFirstCharater";
            this.txtFirstCharater.Size = new System.Drawing.Size(304, 22);
            this.txtFirstCharater.TabIndex = 6;
            this.txtFirstCharater.Value = "";
            // 
            // ultraLabel6
            // 
            appearance36.BackColor = System.Drawing.Color.Transparent;
            appearance36.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance36.TextVAlignAsString = "Middle";
            this.ultraLabel6.Appearance = appearance36;
            this.ultraLabel6.Location = new System.Drawing.Point(9, 29);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(124, 22);
            this.ultraLabel6.TabIndex = 16;
            this.ultraLabel6.Text = "Ký tự đầu";
            // 
            // ultraPanel2
            // 
            // 
            // ultraPanel2.ClientArea
            // 
            this.ultraPanel2.ClientArea.Controls.Add(this.btnSave);
            this.ultraPanel2.ClientArea.Controls.Add(this.btnClose);
            this.ultraPanel2.ClientArea.Controls.Add(this.chkActive);
            this.ultraPanel2.Location = new System.Drawing.Point(0, 224);
            this.ultraPanel2.Name = "ultraPanel2";
            this.ultraPanel2.Size = new System.Drawing.Size(497, 40);
            this.ultraPanel2.TabIndex = 0;
            // 
            // btnSave
            // 
            appearance37.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance37;
            this.btnSave.Location = new System.Drawing.Point(279, 4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 11;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click_1);
            // 
            // btnClose
            // 
            appearance38.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance38;
            this.btnClose.Location = new System.Drawing.Point(368, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 12;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click_1);
            // 
            // ultraPanel3
            // 
            // 
            // ultraPanel3.ClientArea
            // 
            this.ultraPanel3.ClientArea.Controls.Add(this.ultraGroupBox2);
            this.ultraPanel3.Location = new System.Drawing.Point(6, 205);
            this.ultraPanel3.Name = "ultraPanel3";
            this.ultraPanel3.Size = new System.Drawing.Size(485, 19);
            this.ultraPanel3.TabIndex = 1;
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox2.Controls.Add(this.ubtnDefault);
            this.ultraGroupBox2.Controls.Add(this.txtSeparator);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel6);
            this.ultraGroupBox2.Controls.Add(this.txtFirstCharater);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel7);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel8);
            this.ultraGroupBox2.Controls.Add(this.txtAfterCharater);
            appearance40.FontData.BoldAsString = "True";
            this.ultraGroupBox2.HeaderAppearance = appearance40;
            this.ultraGroupBox2.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOutsideBorder;
            this.ultraGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(485, 15);
            this.ultraGroupBox2.TabIndex = 0;
            this.ultraGroupBox2.Text = "Cách đọc số";
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ubtnDefault
            // 
            appearance39.Image = global::Accounting.Properties.Resources.ubtnReset;
            this.ubtnDefault.Appearance = appearance39;
            this.ubtnDefault.Location = new System.Drawing.Point(338, 116);
            this.ubtnDefault.Name = "ubtnDefault";
            this.ubtnDefault.Size = new System.Drawing.Size(105, 34);
            this.ubtnDefault.TabIndex = 9;
            this.ubtnDefault.Text = "Lấy mặc định";
            this.ubtnDefault.Click += new System.EventHandler(this.ubtnDefault_Click);
            // 
            // FCurrentcyDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(497, 264);
            this.Controls.Add(this.ultraPanel3);
            this.Controls.Add(this.ultraPanel2);
            this.Controls.Add(this.ultraPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FCurrentcyDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Loại tiền";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FCurrentcyDetail_FormClosed);
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCashAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkActive)).EndInit();
            this.ultraPanel2.ClientArea.ResumeLayout(false);
            this.ultraPanel2.ResumeLayout(false);
            this.ultraPanel3.ClientArea.ResumeLayout(false);
            this.ultraPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraPanel ultraPanel2;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbBankAccount;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbCashAccount;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkActive;
        private Infragistics.Win.FormattedLinkLabel.UltraFormattedTextEditor txtAfterCharater;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.FormattedLinkLabel.UltraFormattedTextEditor txtSeparator;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.FormattedLinkLabel.UltraFormattedTextEditor txtFirstCharater;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.FormattedLinkLabel.UltraFormattedTextEditor txtCurrentName;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtID;
        private Infragistics.Win.Misc.UltraPanel ultraPanel3;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.Misc.UltraButton ubtnDefault;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtExchangeRate;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnSave;

    }
}