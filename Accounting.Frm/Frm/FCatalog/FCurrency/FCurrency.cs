﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.IService;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinGrid;
using FX.Core;

namespace Accounting
{
    public partial class FCurrency : CatalogBase
    {
        #region khái báo
        private readonly ICurrencyService _IcurrentService;
        Currency _selected = new Currency();
        bool first = true;

        bool them = false;
        public static bool isClose = true;
        #endregion

        #region khởi tạo

        #region khởi tạo giá trị mặc định cho form
        public FCurrency()
        {
            InitializeComponent();

            #region khai báo service để sử dụng
            _IcurrentService = IoC.Resolve<ICurrencyService>();

            #endregion

            this.ugrCurrent.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            loadUgrCurrent(true);
            ToolTip ToolTip1 = new ToolTip();
            ToolTip1.SetToolTip(btnAdd, "Thêm (Ctrl + N)");
            ToolTip ToolTip2 = new ToolTip();
            ToolTip2.SetToolTip(btnEdit, "Xem (Ctrl + E)");
            ToolTip ToolTip3 = new ToolTip();
            ToolTip3.SetToolTip(btnDelete, "Xóa (Ctrl + D)");
            if (ugrCurrent.Rows.Count > 0) ugrCurrent.Rows[0].Selected = true;
        }
        #endregion

        #region Fill data for ultraGrip
        void loadUgrCurrent(bool configGrid)
        {
            WaitingFrm.StartWaiting();
            first = true;
            them = false;
            #region get data from database
            List<Currency> list = _IcurrentService.OrderByID();
            _IcurrentService.UnbindSession(list);
            list = _IcurrentService.OrderByID();
            _selected = list.Count > 0 ? list[0] : new Currency();
            ugrCurrent.DataSource = list.ToList();
            #endregion
            if (ugrCurrent.Rows.Count > 0)
            {
                ugrCurrent.Rows[0].Selected = true;
            }
            if (configGrid) ConfigGrid(ugrCurrent);
            ugrCurrent.DisplayLayout.Bands[0].Columns["ExchangeRate"].FormatNumberic(ConstDatabase.Format_Rate);
            WaitingFrm.StopWaiting();
        }
        #endregion


        #endregion

        #region nghiệp vụ

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            AddFunction();
        }


        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void btnDelete_Click_1(object sender, EventArgs e)
        {
            DeleteFunction();

            loadUgrCurrent(true);
        }

        private void tsmAdd_Click_1(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void tsmEdit_Click_1(object sender, EventArgs e)
        {
            if (ugrCurrent.Selected.Rows.Count > 0 || first)
            {

                new FCurrentcyDetail(_selected).ShowDialog(this);
                if (!FCurrentcyDetail.isClose) loadUgrCurrent(true);
            }
            else
                Accounting.TextMessage.MSG.Error(string.Format(Accounting.TextMessage.resSystem.MSG_Catalog_Account4, "một Loại tiền"));
        }


        private void tsmDelete_Click_1(object sender, EventArgs e)
        {
            DeleteFunction();
            loadUgrCurrent(true);
        }

        private void tmsReLoad_Click_1(object sender, EventArgs e)
        {
            ResetFunction();
        }
        protected override void ResetFunction()
        {
            loadUgrCurrent(true);
        }

        private void ugrCurrent_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, ugrCurrent, cms4Grid);
        }

        private void ugrCurrent_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (e.Row.Index >= 0)
            {
                Currency temp = e.Row.ListObject as Currency;
                _selected = temp;
                new FCurrentcyDetail(temp).ShowDialog(this);
                if (!FAccountDetail.isClose) loadUgrCurrent(true);
            }
        }

        protected override void AddFunction()
        {
            new FCurrentcyDetail().ShowDialog(this);
            if (!FCurrentcyDetail.isClose) loadUgrCurrent(true);
        }
        protected override void EditFunction()
        {
            if (ugrCurrent.Selected.Rows.Count > 0 || first)
            {
                Currency temp = ugrCurrent.Selected.Rows[0].ListObject as Currency;
                new FCurrentcyDetail(temp).ShowDialog(this);
                if (!FCurrentcyDetail.isClose) loadUgrCurrent(true);
            }
            else
                Accounting.TextMessage.MSG.Error(string.Format(Accounting.TextMessage.resSystem.MSG_Catalog_Account4, "một Loại tiền"));
        }
        protected override void DeleteFunction()
        {
            if (ugrCurrent.Selected.Rows.Count > 0)
            {
                Currency currency = ugrCurrent.Selected.Rows[0].ListObject as Currency;
                if (MSG.Question(string.Format(resSystem.MSG_System_05, currency.CurrencyName)) == System.Windows.Forms.DialogResult.Yes)
                {
                    _IcurrentService.BeginTran();
                    _IcurrentService.Delete(currency);
                    _IcurrentService.CommitTran();
                }
            }
            else if (first)
            {
                if (MSG.Question(string.Format(resSystem.MSG_System_05, _selected.CurrencyName)) == System.Windows.Forms.DialogResult.Yes)
                {
                    _IcurrentService.BeginTran();
                    _IcurrentService.Delete(_selected);
                    _IcurrentService.CommitTran();
                }
            }

            Utils.ClearCacheByType<Currency>();
        }

        #endregion

        #region Utils hàm chung
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {
            Utils.ConfigGrid(utralGrid, TextMessage.ConstDatabase.Currency_TableName);
        }
        #endregion

        private void FCurrency_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            ugrCurrent.ResetText();
            ugrCurrent.ResetUpdateMode();
            ugrCurrent.ResetExitEditModeOnLeave();
            ugrCurrent.ResetRowUpdateCancelAction();
            ugrCurrent.DataSource = null;
            ugrCurrent.Layouts.Clear();
            ugrCurrent.ResetLayouts();
            ugrCurrent.ResetDisplayLayout();
            ugrCurrent.Refresh();
            ugrCurrent.ClearUndoHistory();
            ugrCurrent.ClearXsdConstraints();
        }

        private void FCurrency_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
