﻿namespace Accounting
{
    partial class FDepartmentSearch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.btnSearch = new Infragistics.Win.Misc.UltraButton();
            this.btnCancel = new Infragistics.Win.Misc.UltraButton();
            this.chkUppercase = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.cbbSort = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbbSearchStyle = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbbSearchFor = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbbCondition = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.lblSort = new Infragistics.Win.Misc.UltraLabel();
            this.lblSearchStyle = new Infragistics.Win.Misc.UltraLabel();
            this.lblSearchFor = new Infragistics.Win.Misc.UltraLabel();
            this.lblCondition = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkUppercase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbSort)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbSearchStyle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbSearchFor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCondition)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.btnSearch);
            this.ultraGroupBox1.Controls.Add(this.btnCancel);
            this.ultraGroupBox1.Controls.Add(this.chkUppercase);
            this.ultraGroupBox1.Controls.Add(this.cbbSort);
            this.ultraGroupBox1.Controls.Add(this.cbbSearchStyle);
            this.ultraGroupBox1.Controls.Add(this.cbbSearchFor);
            this.ultraGroupBox1.Controls.Add(this.cbbCondition);
            this.ultraGroupBox1.Controls.Add(this.lblSort);
            this.ultraGroupBox1.Controls.Add(this.lblSearchStyle);
            this.ultraGroupBox1.Controls.Add(this.lblSearchFor);
            this.ultraGroupBox1.Controls.Add(this.lblCondition);
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(423, 194);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // btnSearch
            // 
            appearance1.Image = global::Accounting.Properties.Resources.btnsearch;
            this.btnSearch.Appearance = appearance1;
            this.btnSearch.Location = new System.Drawing.Point(246, 156);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(84, 30);
            this.btnSearch.TabIndex = 4;
            this.btnSearch.Text = "Tìm kiếm";
            // 
            // btnCancel
            // 
            appearance2.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnCancel.Appearance = appearance2;
            this.btnCancel.Location = new System.Drawing.Point(336, 156);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 30);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Hủy bỏ";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // chkUppercase
            // 
            appearance3.TextVAlignAsString = "Middle";
            this.chkUppercase.Appearance = appearance3;
            this.chkUppercase.BackColor = System.Drawing.Color.Transparent;
            this.chkUppercase.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkUppercase.Location = new System.Drawing.Point(131, 98);
            this.chkUppercase.Name = "chkUppercase";
            this.chkUppercase.Size = new System.Drawing.Size(182, 22);
            this.chkUppercase.TabIndex = 3;
            this.chkUppercase.Text = "Phân biệt chữ hoa, chữ thường";
            // 
            // cbbSort
            // 
            this.cbbSort.AutoSize = false;
            this.cbbSort.Location = new System.Drawing.Point(131, 128);
            this.cbbSort.Name = "cbbSort";
            this.cbbSort.Size = new System.Drawing.Size(280, 22);
            this.cbbSort.TabIndex = 2;
            // 
            // cbbSearchStyle
            // 
            this.cbbSearchStyle.AutoSize = false;
            this.cbbSearchStyle.Location = new System.Drawing.Point(131, 69);
            this.cbbSearchStyle.Name = "cbbSearchStyle";
            this.cbbSearchStyle.Size = new System.Drawing.Size(280, 22);
            this.cbbSearchStyle.TabIndex = 2;
            // 
            // cbbSearchFor
            // 
            this.cbbSearchFor.AutoSize = false;
            this.cbbSearchFor.Location = new System.Drawing.Point(131, 41);
            this.cbbSearchFor.Name = "cbbSearchFor";
            this.cbbSearchFor.Size = new System.Drawing.Size(280, 22);
            this.cbbSearchFor.TabIndex = 2;
            // 
            // cbbCondition
            // 
            this.cbbCondition.AutoSize = false;
            this.cbbCondition.Location = new System.Drawing.Point(131, 13);
            this.cbbCondition.Name = "cbbCondition";
            this.cbbCondition.Size = new System.Drawing.Size(280, 22);
            this.cbbCondition.TabIndex = 1;
            // 
            // lblSort
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.lblSort.Appearance = appearance4;
            this.lblSort.Location = new System.Drawing.Point(12, 128);
            this.lblSort.Name = "lblSort";
            this.lblSort.Size = new System.Drawing.Size(100, 22);
            this.lblSort.TabIndex = 0;
            this.lblSort.Text = "Hướng tìm";
            // 
            // lblSearchStyle
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.TextVAlignAsString = "Middle";
            this.lblSearchStyle.Appearance = appearance5;
            this.lblSearchStyle.Location = new System.Drawing.Point(12, 69);
            this.lblSearchStyle.Name = "lblSearchStyle";
            this.lblSearchStyle.Size = new System.Drawing.Size(100, 22);
            this.lblSearchStyle.TabIndex = 0;
            this.lblSearchStyle.Text = "Cách tìm";
            // 
            // lblSearchFor
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextVAlignAsString = "Middle";
            this.lblSearchFor.Appearance = appearance6;
            this.lblSearchFor.Location = new System.Drawing.Point(12, 41);
            this.lblSearchFor.Name = "lblSearchFor";
            this.lblSearchFor.Size = new System.Drawing.Size(100, 22);
            this.lblSearchFor.TabIndex = 0;
            this.lblSearchFor.Text = "Tìm theo";
            // 
            // lblCondition
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.TextVAlignAsString = "Middle";
            this.lblCondition.Appearance = appearance7;
            this.lblCondition.Location = new System.Drawing.Point(12, 13);
            this.lblCondition.Name = "lblCondition";
            this.lblCondition.Size = new System.Drawing.Size(100, 22);
            this.lblCondition.TabIndex = 0;
            this.lblCondition.Text = "Điều kiện";
            // 
            // FDepartmentSearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(423, 193);
            this.Controls.Add(this.ultraGroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FDepartmentSearch";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tìm Kiếm Phòng ban";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FDepartmentSearch_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkUppercase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbSort)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbSearchStyle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbSearchFor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCondition)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbCondition;
        private Infragistics.Win.Misc.UltraLabel lblCondition;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbSearchFor;
        private Infragistics.Win.Misc.UltraLabel lblSearchFor;
        private Infragistics.Win.Misc.UltraButton btnCancel;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkUppercase;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbSort;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbSearchStyle;
        private Infragistics.Win.Misc.UltraLabel lblSort;
        private Infragistics.Win.Misc.UltraLabel lblSearchStyle;
        private Infragistics.Win.Misc.UltraButton btnSearch;

    }
}