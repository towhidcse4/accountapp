﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win.UltraWinGrid;


namespace Accounting
{
    public partial class FDepartmentDetail : DialogForm //UserControl
    {
        #region khai báo
        private IDepartmentService _IDepartmentService;
        Department _Select = new Department();
        private Guid _id;
        bool checkActive = true;
        public Guid Id
        {
            get { return _id; }
            set { _id = value; }
        }
        bool Them = true;
        public static bool isClose = true;
        public static string DepartmentName;
        #endregion

        #region khởi tạo
        public FDepartmentDetail()
        {//Thêm
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            //tài khoản tổng hợp
            Add();

        }
        private void Add()
        {
            #region Thiết lập ban đầu cho Form
            //Khai báo các webservices
            _IDepartmentService = IoC.Resolve<IDepartmentService>();
            chkIsActive.Visible = false;
            this.Text = "Thêm mới phòng ban";
            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion

        }

        private void Edit(Department temp)
        {
            #region Thiết lập ban đầu cho Form
            _Select = temp;
            Them = false;
            txtDepartmentCode.Enabled = false;

            //Khai báo các webservices
            _IDepartmentService = IoC.Resolve<IDepartmentService>();
            this.Text = "Sửa phòng ban";
            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion

            #region Fill dữ liệu Obj vào control
            ObjandGUI(temp, false);
            #endregion
        }

        public FDepartmentDetail(Department temp)
        {//Sửa
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            Edit(temp);
        }

        public FDepartmentDetail(Department temp, bool Them)
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion
            if (Them)
            {
                Add();
                foreach (var item in cbbParentID.Rows)
                {
                    if ((item.ListObject as Department).ID == temp.ID) cbbParentID.SelectedRow = item;
                }

                foreach (var item in cbbCostAccount.Rows)
                {
                    if ((item.ListObject as Account).AccountNumber == temp.CostAccount) cbbCostAccount.SelectedRow = item;
                }
            }
            else
            {
                Edit(temp);
            }
        }

        private void InitializeGUI()
        {
            //Khởi tạo phòng ban cha
            //cbbParentID.DataSource = _IDepartmentService.GetAll().OrderByDescending(c => c.DepartmentName).Reverse().ToList();
            cbbParentID.DataSource = _IDepartmentService.GetListDeparmentOrderDepartmentCode();
            cbbParentID.DisplayMember = "DepartmentCode";
            Utils.ConfigGrid(cbbParentID, ConstDatabase.Department_TableName);
            //Khởi tạo TK chi phí lương
            cbbCostAccount.DataSource = Utils.ListAccount.Where(k => k.IsActive == true && !k.IsParentNode).OrderBy(p => p.AccountNumber).ToList();
            cbbCostAccount.DisplayMember = "AccountNumber";
            Utils.ConfigGrid(cbbCostAccount, ConstDatabase.Account_TableName);

        }
        #endregion

        #region Button Event
        /// <summary>
        /// Sự kiện nút Lưu lại
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion

            #region Thao tác CSDL
            try
            {
                _IDepartmentService.BeginTran();
                Department temp;
                if (Them)//them moi
                {
                    temp = Them ? new Department() : _IDepartmentService.Getbykey(_Select.ID);
                    temp = ObjandGUI(temp, true);

                    //set order fixcode
                    Department temp0 = (Department)Utils.getSelectCbbItem(cbbParentID);

                    //List<string> lstOrderFixCodeChild = _IDepartmentService.Query.Where(a => a.ParentID == temp.ParentID)
                    //    .Select(a => a.OrderFixCode).ToList();
                    List<string> lstOrderFixCodeChild = _IDepartmentService.GetListOrderFixCodeParentID(temp.ParentID);
                    string orderFixCodeParent = temp0 == null ? "" : temp0.OrderFixCode;
                    temp.OrderFixCode = Utils.GetOrderFixCode(lstOrderFixCodeChild, orderFixCodeParent);
                    temp.IsParentNode = false; // mac dinh cho insert
                    temp.Grade = temp0 == null ? 1 : temp0.Grade + 1; // quy tac lay gia tri cho grade

                    //nếu cha isactive =false thì khi thêm mới con cũng là false

                    if (cbbParentID.Text.Equals(""))
                    {
                        if (!CheckCode()) return;
                        temp.IsActive = true;
                        _IDepartmentService.CreateNew(temp);
                    }
                    else
                    {
                        Department objParentCombox = (Department)Utils.getSelectCbbItem(cbbParentID);

                        if (Utils.ListAccountingObject.Any(o => o.DepartmentID == objParentCombox.ID && o.IsEmployee == true))
                        {
                            MSG.Error("Thêm mới thất bại vì phòng ban này đã có phát sinh chứng từ");
                            return;
                        }
                        if (Utils.checkRelationVoucher_Department(objParentCombox))
                        {
                            MSG.Error("Thêm mới thất bại vì phòng ban này đã có phát sinh chứng từ");
                            return;
                        }
                        //lay cha
                        Department Parent = _IDepartmentService.Getbykey(objParentCombox.ID);
                        if (Parent.IsActive == false)
                        {
                            temp.IsActive = false;
                        }
                        else
                            temp.IsActive = true;

                        //check loi
                        if (!CheckCode()) return;
                        //temp.IsActive = true;
                        _IDepartmentService.CreateNew(temp);
                    }

                    //update lai isparentnode neu co
                    if (temp.ParentID != null)
                    {
                        Department parent = _IDepartmentService.Getbykey((Guid)temp.ParentID);
                        parent.IsParentNode = true;
                        _IDepartmentService.Update(parent);
                    }
                }
                else //cap nhat
                {
                    temp = _IDepartmentService.Getbykey(_Select.ID);
                    //Lưu đối tượng trước khi sửa
                    Department tempOriginal = (Department)Utils.CloneObject(temp);
                    //Lấy về đối tượng chọn trong combobox
                    Department objParentCombox = (Department)Utils.getSelectCbbItem(cbbParentID);

                    //if(objParentCombox != null && objParentCombox.ToString() != "")
                    //{
                    //    if (Utils.checkRelationVoucher_Department(objParentCombox))
                    //    {
                    //        MSG.Error("Thêm mới thất bại vì phòng ban này đã có phát sinh chứng từ");
                    //        return;
                    //    }
                    //    if (Utils.ListAccountingObject.Any(o => o.DepartmentID == objParentCombox.ID && o.IsEmployee == true))
                    //    {
                    //        MSG.Error("Thêm mới thất bại vì phòng ban này đã có phát sinh chứng từ");
                    //        return;
                    //    }
                    //}

                    //Không thay đổi đối tượng cha trong combobox
                    if (((temp.ParentID == null) && (objParentCombox == null)) || ((temp.ParentID != null) && (objParentCombox != null) && (temp.ParentID == objParentCombox.ID)))
                    {
                        temp = ObjandGUI(temp, true);
                        _IDepartmentService.Update(temp);
                    }
                    else
                    {
                        //không cho phép chuyển ngược
                        if (objParentCombox != null)
                        {
                            //List<Department> lstChildCheck =
                            //    _IDepartmentService.Query.Where(p => p.OrderFixCode.StartsWith(tempOriginal.OrderFixCode)).ToList();
                            List<Department> lstChildCheck =
                                _IDepartmentService.GetListDepartmentOrderFixCode(tempOriginal.OrderFixCode);
                            foreach (var item in lstChildCheck)
                            {
                                if (objParentCombox.ID == item.ID)
                                {
                                    MSG.Error(string.Format(resSystem.MSG_Catalog6, temp.DepartmentCode));
                                    //MessageBox.Show("danh mục <<" + temp.DepartmentCode + ">> không được phép chọn chi tiết của nó làm danh mục tổng hợp!");
                                    return;
                                }
                            }
                        }
                        //Trường hợp có cha cũ
                        //Kiểm tra cha cũ có còn con ngoài con chuyển đi hay không
                        //Chuyển trạng thái IsParentNode Cha cũ nếu hết con
                        if (temp.ParentID != null)
                        {
                            Department oldParent = _IDepartmentService.Getbykey((Guid)temp.ParentID);
                            //int checkChildOldParent = _IDepartmentService.Query.Count(p => p.ParentID == temp.ParentID);
                            int checkChildOldParent = _IDepartmentService.CountListDepartmentParentID(temp.ParentID);
                            if (checkChildOldParent <= 1)
                            {
                                oldParent.IsParentNode = false;
                            }
                            _IDepartmentService.Update(oldParent);
                        }
                        //Trường hợp có cha mới
                        //Chuyển trạng IsParentNode cha mới
                        if (objParentCombox != null)
                        {
                            Department newParent = _IDepartmentService.Getbykey(objParentCombox.ID);
                            newParent.IsParentNode = true;
                            _IDepartmentService.Update(newParent);

                            //List<Department> listChildNewParent = _IDepartmentService.Query.Where(
                            //    a => a.ParentID == objParentCombox.ID).ToList();
                            List<Department> listChildNewParent =
                                _IDepartmentService.GetListDepartmentParentID(objParentCombox.ID);
                            //Tính lại OrderFixCode cho đối tượng chính
                            List<string> lstOfcChildNewParent = new List<string>();
                            foreach (var item in listChildNewParent)
                            {
                                lstOfcChildNewParent.Add(item.OrderFixCode);
                            }
                            string orderFixCodeParent = newParent.OrderFixCode;
                            temp.OrderFixCode = Utils.GetOrderFixCode(lstOfcChildNewParent, orderFixCodeParent);
                            //Tính lại bậc cho đối tượng chính
                            temp.Grade = newParent.Grade + 1;
                        }
                        //Trường hợp không có cha mới tức là chuyển đối tượng lên bậc 1
                        if (objParentCombox == null)
                        {
                            //Lấy về tất cả đối tượng bậc 1
                            List<int> lstOfcChildGradeNo1 = new List<int>();
                            //List<Department> listChildGradeNo1 = _IDepartmentService.Query.Where(
                            //    a => a.Grade == 1).ToList();
                            List<Department> listChildGradeNo1 = _IDepartmentService.GetListDepartmentGrade(1);
                            foreach (var item in listChildGradeNo1)
                            {
                                lstOfcChildGradeNo1.Add(Convert.ToInt32(item.OrderFixCode));
                            }
                            //Tính lại OrderFixCode cho đối tượng chính
                            temp.OrderFixCode = (lstOfcChildGradeNo1.Max() + 1).ToString(CultureInfo.InvariantCulture);
                            //Tính lại bậc cho đối tượng chính
                            temp.Grade = 1;
                            temp = ObjandGUI(temp, true);
                            _IDepartmentService.Update(temp);
                        }
                        //Xử lý các con của nó 
                        //Lấy về các con
                        //List<Department> lstChild =
                        //    _IDepartmentService.Query.Where(p => p.OrderFixCode.StartsWith(tempOriginal.OrderFixCode)).ToList();
                        List<Department> lstChild = _IDepartmentService.GetListDepartmentOrderFixCode(tempOriginal.OrderFixCode);
                        foreach (var item in lstChild)
                        {
                            string tempOldFixCode = tempOriginal.OrderFixCode;
                            item.OrderFixCode = temp.OrderFixCode + item.OrderFixCode.Substring(tempOldFixCode.Length);
                            item.Grade = temp.Grade - tempOriginal.Grade + item.Grade;
                            _IDepartmentService.Update(item);
                        }

                        temp = ObjandGUI(temp, true);
                    }
                    //nếu cha ngừng theo dõi thì con cũng ngừng theo dõi
                    //Lấy về danh sách các con
                    //List<Department> lstChild2 =
                    //        _IDepartmentService.Query.Where(p => p.OrderFixCode.StartsWith(tempOriginal.OrderFixCode) && p.ID != tempOriginal.ID).ToList();
                    if (!checkActive)
                    {
                        List<Department> lstChild2 = _IDepartmentService.GetListDepartmentOrderFixCodeNotParent(tempOriginal.OrderFixCode, tempOriginal.ID);
                        //Chuyen True -> False
                        if (chkIsActive.CheckState == CheckState.Unchecked && CheckError())
                        {
                            temp.IsActive = false;
                            //neu la cha
                            if (lstChild2.Count > 0)
                            {
                                foreach (var itemChild in lstChild2)
                                {
                                    itemChild.IsActive = false;
                                    _IDepartmentService.Update(itemChild);
                                }
                            }
                            //neu ko phai la cha
                            _IDepartmentService.Update(temp);
                        }

                        //Chuyen False -> True
                        else if (chkIsActive.CheckState == CheckState.Checked && CheckError())
                        {
                            //Co Cha
                            if (temp.ParentID != null)
                            {
                                Department Parent = _IDepartmentService.Getbykey(objParentCombox.ID);
                                //Cha la false khong cho chuyen
                                if (Parent.IsActive == false)
                                {
                                    MSG.Error(string.Format(resSystem.MSG_Catalog_Tree2, "phòng ban"));
                                    //MessageBox.Show("Đối tượng con không thể hoạt động khi đối tượng cha đang bị ngừng hoạt đông!");
                                    return;
                                }
                                //Cha la True duoc phep chuyen
                                temp.IsActive = true;
                                if (lstChild2.Count > 0)
                                {

                                    if (MSG.Question(string.Format(resSystem.MSG_Catalog8)) == System.Windows.Forms.DialogResult.Yes)
                                    {
                                        foreach (var item2 in lstChild2)
                                        {
                                            item2.IsActive = true;
                                            _IDepartmentService.Update(item2);
                                        }
                                    }

                                }
                                _IDepartmentService.Update(temp);
                            }
                            //Khong Co cha
                            else
                            {
                                temp.IsActive = true;
                                if (lstChild2.Count > 0)
                                {

                                    if (MSG.Question(string.Format(resSystem.MSG_Catalog8)) == System.Windows.Forms.DialogResult.Yes)
                                    {
                                        foreach (var item2 in lstChild2)
                                        {
                                            item2.IsActive = true;
                                            _IDepartmentService.Update(item2);
                                        }
                                    }

                                }

                                _IDepartmentService.Update(temp);
                            }
                        }
                    }
                }
                _IDepartmentService.CommitTran();
                _id = temp.ID;
            }
            catch
            {
                _IDepartmentService.RolbackTran();
            }

            #endregion
            DepartmentName = txtDepartmentName.Text;
            #region xử lý form, kết thúc form

            Utils.ListDepartment.Clear();
            this.Close();
            isClose = false;
            #endregion
        }

        /// <summary>
        /// Sự kiện nút đóng
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            isClose = true;
            DialogResult = DialogResult.Cancel;
            this.Close();
        }
        #endregion

        #region Utils
        /// <summary>
        /// get hoặc set một đối tượng vào giao diện xử lý
        /// </summary>
        /// <param name="input">đối tượng</param>
        /// <param name="isGet">True: get giá trị từ control gán vào đối tượng| False: set giá trị đối tượng vào control</param>
        /// <returns></returns>
        Department ObjandGUI(Department input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();  //Thêm mới

                input.DepartmentCode = txtDepartmentCode.Text;
                input.DepartmentName = txtDepartmentName.Text;
                Department temp0 = (Department)Utils.getSelectCbbItem(cbbParentID);
                if (temp0 == null) input.ParentID = null;
                else input.ParentID = temp0.ID;
                input.Description = txtDescription.Text;
                Account _tkcp = (Account)Utils.getSelectCbbItem(cbbCostAccount);
                if (_tkcp == null) input.CostAccount = string.Empty;
                else input.CostAccount = _tkcp.AccountNumber;
                input.IsActive = chkIsActive.Checked;
            }
            else
            {
                txtDepartmentCode.Text = input.DepartmentCode;
                txtDepartmentName.Text = input.DepartmentName;
                foreach (var item in cbbParentID.Rows)
                {
                    if ((item.ListObject as Department).ID == input.ParentID) cbbParentID.SelectedRow = item;
                }
                txtDescription.Text = input.Description;
                foreach (var item in cbbCostAccount.Rows)
                {
                    if ((item.ListObject as Account).AccountNumber == input.CostAccount) cbbCostAccount.SelectedRow = item;
                }
                chkIsActive.Checked = input.IsActive;
            }
            return input;
        }

        bool CheckError()
        {
            bool kq = true;
            //Check Error Chung

            //Check Error Riêng từng Form
            if (string.IsNullOrEmpty(txtDepartmentCode.Text) || string.IsNullOrEmpty(txtDepartmentName.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }
            if (!string.IsNullOrEmpty(cbbParentID.Text))
                if (!Utils.ListDepartment.Any(x => x.DepartmentCode == cbbParentID.Text.Trim()))
                {
                    MSG.Error("Dữ liệu phòng ban không có trong danh mục");
                    return false;
                }
            return kq;
        }

        bool CheckCode()
        {
            bool kq = true;
            if (string.IsNullOrEmpty(txtDepartmentCode.Text) || string.IsNullOrEmpty(txtDepartmentName.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }

            //check trùng code
            //List<string> lst = _IDepartmentService.Query.Select(c => c.DepartmentCode).ToList();
            List<string> lst = _IDepartmentService.GetListDepartmentCode();
            foreach (var x in lst)
            {
                if (txtDepartmentCode.Text.Equals(x))
                {
                    MSG.Error(string.Format(resSystem.MSG_Catalog_Account6, "Phòng ban"));
                    return false;
                }
            }
            return kq;
        }
        #endregion

        private void cbbParentID_Validated(object sender, EventArgs e)
        {
            //UltraCombo combo = (UltraCombo)sender;
            //if(!Utils.ListDepartment.Any(x => x.DepartmentCode == combo.Text.Trim()))
            //{
            //    MSG.Error("Dữ liệu phòng ban không có trong danh mục");
            //    combo.Focus();
            //    return;
            //}
        }

        private void chkIsActive_ValidateCheckState(object sender, Infragistics.Win.ValidateCheckStateEventArgs e)
        {
            checkActive = false;
        }

        private void cbbCostAccount_ItemNotInList(object sender, Infragistics.Win.UltraWinEditors.ValidationErrorEventArgs e)
        {
            Account b = new Account();
            if (cbbCostAccount.Text != b.AccountNumber && cbbCostAccount.Text != "")
            {
                MSG.Warning(string.Format("Dữ liệu không có trong danh mục", cbbCostAccount.Text));
                cbbCostAccount.Focus();
                return;
            }
        }

        private void FDepartmentDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}