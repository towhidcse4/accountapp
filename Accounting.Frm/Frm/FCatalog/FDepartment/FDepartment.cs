﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Accounting.TextMessage;
using System.Data;
using System.ComponentModel;

namespace Accounting
{
    public partial class FDepartment : CatalogBase //UserControl
    {
        #region khai báo
        private readonly IDepartmentService _IDepartmentService;
        List<Department> dsDepartment = new List<Department>();
        #endregion

        #region khởi tạo
        public FDepartment()
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            ToolTip ToolTip1 = new ToolTip();
            ToolTip1.SetToolTip(btnAdd, "Thêm (Ctrl + N)");
            ToolTip ToolTip2 = new ToolTip();
            ToolTip2.SetToolTip(btnEdit, "Sửa (Ctrl + E)");
            ToolTip ToolTip3 = new ToolTip();
            ToolTip3.SetToolTip(btnDelete, "Xóa (Ctrl + D)");
            #endregion

            #region Thiết lập ban đầu cho Form
            _IDepartmentService = IoC.Resolve<IDepartmentService>();

            this.uTree.InitializeDataNode += new Infragistics.Win.UltraWinTree.InitializeDataNodeEventHandler(Utils.uTree_InitializeDataNode);
            this.uTree.AfterDataNodesCollectionPopulated += new Infragistics.Win.UltraWinTree.AfterDataNodesCollectionPopulatedEventHandler(Utils.uTree_AfterDataNodesCollectionPopulated);
            this.uTree.ColumnSetGenerated += new Infragistics.Win.UltraWinTree.ColumnSetGeneratedEventHandler(this.uTree_ColumnSetGenerated);
            this.uTree.MouseDown += new System.Windows.Forms.MouseEventHandler(this.uTree_MouseDown);
            this.uTree.DoubleClick += new System.EventHandler(this.uTree_DoubleClick);
            btnSearch.Visible = false;
            #endregion

            #region Tạo dữ liệu ban đầu
            LoadDuLieu();
            #endregion
        }

        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }
        private void LoadDuLieu(bool configTree)
        {
            WaitingFrm.StartWaiting();
            #region Lấy dữ liệu từ CSDL
            //dsDepartment = _IDepartmentService.GetAll().OrderByDescending(c => c.DepartmentCode).Reverse().ToList();
            dsDepartment = _IDepartmentService.GetListDeparmentOrderDepartmentCode();
            _IDepartmentService.UnbindSession(dsDepartment);
            dsDepartment = _IDepartmentService.GetListDeparmentOrderDepartmentCode();
            #endregion

            #region Xử lý dữ liệu
            #endregion

            #region hiển thị và xử lý hiển thị
            DataSet ds = Utils.ToDataSet<Department>(dsDepartment, ConstDatabase.Department_TableName);
            uTree.SetDataBinding(ds, ConstDatabase.Department_TableName);

            if (configTree) ConfigTree(uTree);
            #endregion
            WaitingFrm.StopWaiting();
        }

        #endregion

        #region Nghiệp vụ
        #region ContentMenu
        private void tsmAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void tsmEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            ResetFunction();
        }
        protected override void ResetFunction()
        {
            LoadDuLieu(true);
        }
        #endregion

        #region Button
        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            searchFunction();
        }
        #endregion



        /// <summary>
        /// Nghiệp vụ Add
        /// </summary>
        protected override void AddFunction()
        {
            if (uTree.SelectedNodes.Count > 0)
            {
                Department temp = dsDepartment.Single(k => k.ID == (new Guid(uTree.SelectedNodes[0].Text)));
                new FDepartmentDetail(temp, true).ShowDialog(this);
                if (!FDepartmentDetail.isClose) { Utils.ClearCacheByType<Objects>(); LoadDuLieu(); } 
            }
            else
            {
                new FDepartmentDetail().ShowDialog(this);
                if (!FDepartmentDetail.isClose) { Utils.ClearCacheByType<Objects>(); LoadDuLieu(); }
            }
        }

        /// <summary>
        /// Nghiệp vụ Edit
        /// </summary>
        protected override void EditFunction()
        {
            if (uTree.SelectedNodes.Count == 0 && uTree.ActiveNode != null)
                uTree.ActiveNode.Selected = true;
            if (uTree.SelectedNodes.Count > 0)
            {
                Department temp = dsDepartment.Single(k => k.ID == (new Guid(uTree.SelectedNodes[0].Text)));
                //if (Utils.checkRelationVoucher_Department(temp))
                //{
                //    MSG.Error("Không thể chỉnh sửa phòng ban vì đã có phát sinh chứng từ kế toán liên quan.");
                //    return;
                //}
                //if (Utils.checkRelationVoucher(temp))
                //{
                //    MSG.Error("Không thể chỉnh sửa phòng ban vì đã có phát sinh chứng từ liên quan.");
                //    return;
                //}
                //if (Utils.ListAccountingObject.Any(o => o.DepartmentID == temp.ID && o.IsEmployee == true))
                //{
                //    MSG.Error("Không thể chỉnh sửa phòng ban vì đã có phát sinh chứng từ liên quan.");
                //    return;
                //}
                //else
                //{
                new FDepartmentDetail(temp).ShowDialog(this);
                    if (!FDepartmentDetail.isClose) { Utils.ClearCacheByType<Objects>(); LoadDuLieu(); }
                //}
            }
            else
                Accounting.TextMessage.MSG.Error(string.Format(Accounting.TextMessage.resSystem.MSG_Catalog3,"một Phòng ban"));
        }

        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        protected override void DeleteFunction()
        {
            if (uTree.SelectedNodes.Count == 0 && uTree.ActiveNode != null)
            {
                uTree.ActiveNode.Selected = true;
            }
            if (uTree.SelectedNodes.Count > 0)
            {
                Department temp = dsDepartment.Single(k => k.ID == (new Guid(uTree.SelectedNodes[0].Text)));
                if (MSG.Question(string.Format(resSystem.MSG_System_05, "<" + temp.DepartmentCode + ">")) == System.Windows.Forms.DialogResult.Yes)
                {
                    if (Utils.checkRelationVoucher_Department(temp))
                    {
                        MSG.Error("Cần xóa chứng từ kế toán có liên quan trước khi xóa dữ liệu danh mục");
                        return;
                    }

                    //if(Utils.ListAccountingObject.Any(o => o.DepartmentID == temp.ID && o.IsEmployee == true))
                    //{
                    //    MSG.Error("Cần xóa dữ liệu nhân viên trước khi xóa dữ liệu phòng ban");
                    //    return;
                    //}

                    _IDepartmentService.BeginTran();
                    //Kiểm tra xem đối tượng có con không nếu có con thì không được phép xóa
                    //List<Department> lstChild =
                    //        _IDepartmentService.Query.Where(p => p.ParentID == temp.ID).ToList();
                    List<Department> lstChild =
                        _IDepartmentService.GetListDepartmentParentID(temp.ID);
                    if (lstChild.Count > 0)
                    {
                        MSG.Error(string.Format(resSystem.MSG_Catalog_Tree, "phòng ban"));
                        return;
                    }
                    //Check xem cha còn có con khác không nếu không thì chuyển trạng thái IsParentNode = false
                    if (temp.ParentID != null)
                    {
                        Department parent = _IDepartmentService.Getbykey((Guid)temp.ParentID);
                        //int checkChildOldParent = _IDepartmentService.Query.Count(p => p.ParentID == temp.ParentID);
                        //int checkChildOldParent = _IDepartmentService.GetListDepartmentParentID((Guid)temp.ParentID).Count;
                        int checkChildOldParent = _IDepartmentService.CountListDepartmentParentID(temp.ParentID);
                        if (checkChildOldParent <= 1)
                        {
                            parent.IsParentNode = false;
                        }
                        _IDepartmentService.Update(parent);
                        _IDepartmentService.Delete(temp);
                    }
                    if (temp.ParentID == null)
                    {
                        _IDepartmentService.Delete(temp);
                    }
                    _IDepartmentService.CommitTran();
                    Utils.ClearCacheByType<Department>();
                    Utils.ClearCacheByType<Objects>();
                    BindingList<Department> lst = Utils.ListDepartment;
                    LoadDuLieu();
                }
            }
            else
                MSG.Error(string.Format(resSystem.MSG_Catalog2,"một Phòng ban"));
        }

        void searchFunction()
        {
            new FDepartmentSearch().ShowDialog(this);
        }

        private void uTree_DoubleClick(object sender, EventArgs e)
        {
            if (uTree.SelectedNodes.Count > 0)
            {
                EditFunction();
            }

        }

        private void uTree_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, cms4Grid);
        }

        private void uTree_ColumnSetGenerated(object sender, Infragistics.Win.UltraWinTree.ColumnSetGeneratedEventArgs e)
        {
            Utils.uTree_ColumnSetGenerated(sender, e, ConstDatabase.Department_TableName);
        }
        #endregion

        #region Utils
        void ConfigTree(Infragistics.Win.UltraWinTree.UltraTree ultraTree)
        {//hàm chung
            Utils.ConfigTree(ultraTree, TextMessage.ConstDatabase.Department_TableName);
        }
        #endregion

        private void uTree_ColumnSetGenerated_1(object sender, Infragistics.Win.UltraWinTree.ColumnSetGeneratedEventArgs e)
        {
            Utils.uTree_ColumnSetGenerated(sender, e, ConstDatabase.Department_TableName);
            e.ColumnSet.Columns["CostAccount"].CellAppearance.TextHAlign = Infragistics.Win.HAlign.Center;
        }

        private void FDepartment_FormClosing(object sender, FormClosingEventArgs e)
        {
            uTree = null;
        }

        private void FDepartment_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
