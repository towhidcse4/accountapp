﻿using System.Collections.Generic;
using System.Linq;

namespace Accounting
{
    public partial class FDepartmentSearch : DialogForm
    {
        #region khởi tạo
        public FDepartmentSearch()
        {
            #region khởi tạo mặc định
            InitializeComponent();
            #endregion
            //load các cột cần tìm kiếm
            List<string> searchFor = new List<string>() { "Tất cả", "Mã phòng ban", "Tên phòng ban", "Mô tả", "TK chi phí lương" };
            cbbSearchFor.DataSource = searchFor.ToList();

            //load cách tìm
            List<string> searchStyle = new List<string>() { "Chứa", "Không chứa", "Bắt đầu với", "Kết thúc với", "Bằng", "Khác" };
            cbbSearchStyle.DataSource = searchStyle.ToList();

            //load hướng tìm
            List<string> Sort = new List<string>() { "Từ trên xuống", "Từ dưới lên" };
            cbbSort.DataSource = Sort.ToList();
        }
        #endregion

        private void btnCancel_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }

        private void FDepartmentSearch_FormClosed(object sender, System.Windows.Forms.FormClosedEventArgs e)
        {
            
        }
    }
}
