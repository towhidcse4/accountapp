﻿namespace Accounting
{
    partial class FDepartmentDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbParentID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbbCostAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.lblTaiKhoanChiPhiLuong = new Infragistics.Win.Misc.UltraLabel();
            this.txtDescription = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.txtDepartmentName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtDepartmentCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblThuoc = new Infragistics.Win.Misc.UltraLabel();
            this.lblTen = new Infragistics.Win.Misc.UltraLabel();
            this.lblMa = new Infragistics.Win.Misc.UltraLabel();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.chkIsActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbParentID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCostAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDepartmentName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDepartmentCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox1.Controls.Add(this.cbbParentID);
            this.ultraGroupBox1.Controls.Add(this.cbbCostAccount);
            this.ultraGroupBox1.Controls.Add(this.lblTaiKhoanChiPhiLuong);
            this.ultraGroupBox1.Controls.Add(this.txtDescription);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox1.Controls.Add(this.txtDepartmentName);
            this.ultraGroupBox1.Controls.Add(this.txtDepartmentCode);
            this.ultraGroupBox1.Controls.Add(this.lblThuoc);
            this.ultraGroupBox1.Controls.Add(this.lblTen);
            this.ultraGroupBox1.Controls.Add(this.lblMa);
            appearance6.FontData.BoldAsString = "True";
            this.ultraGroupBox1.HeaderAppearance = appearance6;
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(1, 2);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(375, 205);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbParentID
            // 
            this.cbbParentID.AutoSize = false;
            this.cbbParentID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbParentID.Location = new System.Drawing.Point(133, 83);
            this.cbbParentID.Name = "cbbParentID";
            this.cbbParentID.NullText = "<chọn dữ liệu>";
            this.cbbParentID.Size = new System.Drawing.Size(237, 22);
            this.cbbParentID.TabIndex = 3;
            this.cbbParentID.Validated += new System.EventHandler(this.cbbParentID_Validated);
            // 
            // cbbCostAccount
            // 
            this.cbbCostAccount.AutoSize = false;
            this.cbbCostAccount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbCostAccount.Location = new System.Drawing.Point(133, 169);
            this.cbbCostAccount.Name = "cbbCostAccount";
            this.cbbCostAccount.Size = new System.Drawing.Size(237, 22);
            this.cbbCostAccount.TabIndex = 5;
            this.cbbCostAccount.ItemNotInList += new Infragistics.Win.UltraWinGrid.ItemNotInListEventHandler(this.cbbCostAccount_ItemNotInList);
            // 
            // lblTaiKhoanChiPhiLuong
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.TextVAlignAsString = "Middle";
            this.lblTaiKhoanChiPhiLuong.Appearance = appearance1;
            this.lblTaiKhoanChiPhiLuong.Location = new System.Drawing.Point(8, 169);
            this.lblTaiKhoanChiPhiLuong.Name = "lblTaiKhoanChiPhiLuong";
            this.lblTaiKhoanChiPhiLuong.Size = new System.Drawing.Size(127, 22);
            this.lblTaiKhoanChiPhiLuong.TabIndex = 12;
            this.lblTaiKhoanChiPhiLuong.Text = "Tài khoản chi phí lương";
            // 
            // txtDescription
            // 
            this.txtDescription.AutoSize = false;
            this.txtDescription.Location = new System.Drawing.Point(133, 110);
            this.txtDescription.MaxLength = 512;
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(237, 54);
            this.txtDescription.TabIndex = 4;
            // 
            // ultraLabel1
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance2;
            this.ultraLabel1.Location = new System.Drawing.Point(8, 110);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(107, 22);
            this.ultraLabel1.TabIndex = 10;
            this.ultraLabel1.Text = "Mô tả chi tiết";
            // 
            // txtDepartmentName
            // 
            this.txtDepartmentName.AutoSize = false;
            this.txtDepartmentName.Location = new System.Drawing.Point(133, 56);
            this.txtDepartmentName.MaxLength = 512;
            this.txtDepartmentName.Name = "txtDepartmentName";
            this.txtDepartmentName.Size = new System.Drawing.Size(237, 22);
            this.txtDepartmentName.TabIndex = 2;
            // 
            // txtDepartmentCode
            // 
            this.txtDepartmentCode.AutoSize = false;
            this.txtDepartmentCode.Location = new System.Drawing.Point(133, 29);
            this.txtDepartmentCode.MaxLength = 25;
            this.txtDepartmentCode.Name = "txtDepartmentCode";
            this.txtDepartmentCode.Size = new System.Drawing.Size(237, 22);
            this.txtDepartmentCode.TabIndex = 1;
            // 
            // lblThuoc
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextVAlignAsString = "Middle";
            this.lblThuoc.Appearance = appearance3;
            this.lblThuoc.Location = new System.Drawing.Point(8, 83);
            this.lblThuoc.Name = "lblThuoc";
            this.lblThuoc.Size = new System.Drawing.Size(105, 22);
            this.lblThuoc.TabIndex = 2;
            this.lblThuoc.Text = "Thuộc";
            // 
            // lblTen
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.lblTen.Appearance = appearance4;
            this.lblTen.Location = new System.Drawing.Point(8, 56);
            this.lblTen.Name = "lblTen";
            this.lblTen.Size = new System.Drawing.Size(105, 22);
            this.lblTen.TabIndex = 1;
            this.lblTen.Text = "Tên phòng ban (*)";
            // 
            // lblMa
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.TextVAlignAsString = "Middle";
            this.lblMa.Appearance = appearance5;
            this.lblMa.Location = new System.Drawing.Point(8, 29);
            this.lblMa.Name = "lblMa";
            this.lblMa.Size = new System.Drawing.Size(105, 22);
            this.lblMa.TabIndex = 0;
            this.lblMa.Text = "Mã phòng ban (*)";
            // 
            // btnSave
            // 
            appearance7.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance7;
            this.btnSave.Location = new System.Drawing.Point(214, 211);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 7;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            appearance8.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance8;
            this.btnClose.Location = new System.Drawing.Point(295, 211);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 8;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // chkIsActive
            // 
            appearance9.TextVAlignAsString = "Middle";
            this.chkIsActive.Appearance = appearance9;
            this.chkIsActive.BackColor = System.Drawing.Color.Transparent;
            this.chkIsActive.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsActive.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkIsActive.Checked = true;
            this.chkIsActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIsActive.Location = new System.Drawing.Point(9, 215);
            this.chkIsActive.Name = "chkIsActive";
            this.chkIsActive.Size = new System.Drawing.Size(116, 22);
            this.chkIsActive.TabIndex = 6;
            this.chkIsActive.Text = "Hoạt động";
            this.chkIsActive.ValidateCheckState += new Infragistics.Win.CheckEditor.ValidateCheckStateHandler(this.chkIsActive_ValidateCheckState);
            // 
            // FDepartmentDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(377, 247);
            this.Controls.Add(this.chkIsActive);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.ultraGroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FDepartmentDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FDepartmentDetail_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbParentID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCostAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDepartmentName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDepartmentCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraLabel lblMa;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDepartmentName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDepartmentCode;
        private Infragistics.Win.Misc.UltraLabel lblTen;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraLabel lblThuoc;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsActive;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDescription;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel lblTaiKhoanChiPhiLuong;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbParentID;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbCostAccount;
    }
}
