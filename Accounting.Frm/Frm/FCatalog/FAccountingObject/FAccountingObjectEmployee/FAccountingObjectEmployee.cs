﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using Accounting.TextMessage;
using System.Linq;
using Accounting.Core;

namespace Accounting
{
    public partial class FAccountingObjectEmployee : CatalogBase
    {
        #region Khai Báo
        public readonly IDepartmentService _IDepartmentService;
        public readonly IAccountingObjectService _IAccountingObjectService;
        public readonly IGeneralLedgerService _IGeneralLedgerService;
        List<AccountingObject> listAccountingObject = new List<AccountingObject>();
        List<Department> listDepartment = new List<Department>();
        List<GeneralLeged_TypeName> lstGeneral = new List<GeneralLeged_TypeName>();
        private readonly List<Item> _lstItems = Utils.ObjConstValue.SelectTimes;
        List<VoucherAccountingObject> listVoucherAccountingObject = new List<VoucherAccountingObject>();
        AccountingObject temp = new AccountingObject();
        public static bool isClose = true;
        private DateTime ngayHoachToan;
        private DateTime dtBegin;
        private DateTime dtEnd;
        private Guid selectedGuid;
        #endregion
        #region Khởi tạo
        public FAccountingObjectEmployee()
        {
            Utils.ClearCacheByType<AccountingObject>();
            Utils.ClearCacheByType<AccountingObjectBankAccount>();

            InitializeComponent();
            //this.FormBorderStyle = FormBorderStyle.FixedSingle;
            //this.CenterToScreen();
            _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();
            _IDepartmentService = IoC.Resolve<IDepartmentService>();
            _IGeneralLedgerService = IoC.Resolve<IGeneralLedgerService>();
            //this.uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            LoadDulieu();
            System.Windows.Forms.ToolTip ToolTip1 = new System.Windows.Forms.ToolTip();
            ToolTip1.SetToolTip(btnAdd, "Thêm (Ctrl + N)");
            System.Windows.Forms.ToolTip ToolTip2 = new System.Windows.Forms.ToolTip();
            ToolTip2.SetToolTip(btnEdit, "Xem (Ctrl + E)");
            System.Windows.Forms.ToolTip ToolTip3 = new System.Windows.Forms.ToolTip();
            ToolTip3.SetToolTip(btnDelete, "Xóa (Ctrl + D)");
            if (uGrid.Rows.Count > 0) uGrid.Rows[0].Selected = true;
            ngayHoachToan = (DateTime)Utils.StringToDateTime(ConstFrm.DbStartDate);
            cbbAboutTime.DataSource = _lstItems;
            cbbAboutTime.DisplayMember = "Name";
            Utils.ConfigGrid(cbbAboutTime, ConstDatabase.ConfigXML_TableName);
            cbbAboutTime.SelectedRow = cbbAboutTime.Rows[7];
            cbbAboutTime.DisplayLayout.AutoFitStyle = AutoFitStyle.ExtendLastColumn;
            //dtBegin = ngayHoachToan.AddDays(1 - ngayHoachToan.Day);
            //dtEnd = dtBegin.AddMonths(1).AddSeconds(-1);
            //dteFromDate.DateTime = dtBegin;
            //dteToDate.DateTime = dtEnd;
        }
        #endregion
        #region Hàm
        void LoadDulieu()
        {
            LoadDuLieu(true);
        }
        void LoadDuLieuExport()
        {
            WaitingFrm.StartWaiting();
            #region Lấy dữ liệu từ CSDL
            //listAccountingObject = _IAccountingObjectService.GetAll().Where(p => p.IsEmployee == true).ToList();
            listAccountingObject = _IAccountingObjectService.GetListAccountingObjectByEmployee(true);//thành duy đã sửa chỗ này
            _IAccountingObjectService.UnbindSession(listAccountingObject);
            listAccountingObject = _IAccountingObjectService.GetListAccountingObjectByEmployee(true);
            listDepartment = _IDepartmentService.GetAll();
            //List<BankH> list = _IBankServiceH.GetAll();
            listVoucherAccountingObject = _IAccountingObjectService.GetListVoucherAccoungtingObjectView();
            #endregion
            #region Xử lý dữ liệu
            #endregion
           
            uGridExport.DataSource = listAccountingObject.OrderBy(x => x.AccountingObjectCode).ToList();
            //if (configGrid) ConfigGrid(uGrid);
            if (uGridExport.Rows.Count > 0)
            {
                uGridExport.Rows[0].Selected = true;
            }
            foreach (var item in listAccountingObject)
            {
                if (item.DepartmentID == null) { }
                else
                {
                    Department @default = listDepartment.FirstOrDefault(p => p.ID == item.DepartmentID);
                    if (@default != null)
                        item.DepartmentCode = @default.DepartmentCode;
                }
            }
            
            //gridResult.DataSource = lstGeneral;
            //ConfigGrid(gridResult);
            WaitingFrm.StopWaiting();
        }
        private void LoadDuLieu(bool configGrid)
        {
            WaitingFrm.StartWaiting();
            #region Lấy dữ liệu từ CSDL
            //listAccountingObject = _IAccountingObjectService.GetAll().Where(p => p.IsEmployee == true).ToList();
            listAccountingObject = _IAccountingObjectService.GetListAccountingObjectByEmployee(true);//thành duy đã sửa chỗ này
            _IAccountingObjectService.UnbindSession(listAccountingObject);
            listAccountingObject = _IAccountingObjectService.GetListAccountingObjectByEmployee(true);
            listDepartment = _IDepartmentService.GetAll();
            //List<BankH> list = _IBankServiceH.GetAll();
            listVoucherAccountingObject = _IAccountingObjectService.GetListVoucherAccoungtingObjectView();
            #endregion
            #region Xử lý dữ liệu
            #endregion
            #region hiển thị và xử lý hiển thị
            uGrid.DataSource = listAccountingObject.OrderBy(x=>x.AccountingObjectCode).ToList();
            //if (configGrid) ConfigGrid(uGrid);
            if (uGrid.Rows.Count > 0)
            {
                uGrid.Rows[0].Selected = true;
            }
            foreach (var item in listAccountingObject)
            {
                if (item.DepartmentID == null) { }
                else
                {
                    Department @default = listDepartment.FirstOrDefault(p => p.ID == item.DepartmentID);
                    if (@default != null)
                        item.DepartmentIDViewName = @default.DepartmentName;
                }
            }
            if (configGrid) ConfigGrid(uGrid);
            //gridResult.DataSource = lstGeneral;
            //ConfigGrid(gridResult);
            WaitingFrm.StopWaiting();
            #endregion
            WaitingFrm.StopWaiting();
        }
        #endregion
        #region Nghiệp vụ
        /// Nghiệp vụ Update
        protected override void EditFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                AccountingObject temp = uGrid.Selected.Rows[0].ListObject as AccountingObject;
                Utils.IAccountingObjectBankAccountService.RolbackTran();
                Utils.IAccountingObjectService.RolbackTran();
                temp = Utils.IAccountingObjectService.Getbykey(temp.ID);
                new FAccountingObjectEmployeeDetail(temp).ShowDialog(this);
                if (!FAccountingObjectEmployeeDetail.isClose) { Utils.ClearCacheByType<AccountingObject>(); LoadDulieu(); }

                }
            else
                Accounting.TextMessage.MSG.Error(Accounting.TextMessage.resSystem.MSG_System_04);
        }
        protected override void AddFunction()
        {
            new FAccountingObjectEmployeeDetail().ShowDialog(this);
            if (!FAccountingObjectEmployeeDetail.isClose) {  Utils.ClearCacheByType<AccountingObject>(); LoadDulieu(); }
        }
        protected override void DeleteFunction()
        {
            bool a = uGrid.Selected.Rows.Count > 0;
            bool b = uGrid.ActiveRow != null;
            if (a || b)
            {
                if (!(DialogResult.Yes == MessageBox.Show("Bạn có muốn xóa những dữ liệu này không ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question)))
                    return;
                SelectedRowsCollection lst = uGrid.Selected.Rows;
                var strResult = new List<Result>();
                foreach (UltraGridRow ultraGridRow in lst)
                {
                    var result = new Result();
                    AccountingObject temp = (AccountingObject)ultraGridRow.ListObject;
                    AccountingObject data = Utils.IAccountingObjectService.Getbykey(temp.ID);
                    try
                    {
                        if (!Utils.checkRelationVoucher(data))
                        {
                            Utils.IAccountingObjectService.BeginTran();
                            Utils.IAccountingObjectService.Delete(data);
                            Utils.IAccountingObjectService.CommitTran();
                            Utils.ClearCacheByType<AccountingObject>();
                            SetLabe();
                            result.Reason = "Xóa thành công";
                            result.Code = data.AccountingObjectCode;
                            result.IsSucceed = true;
                            strResult.Add(result);
                            

                        }
                        else
                        {
                            result.Reason = "Không thể xóa do dữ liệu đã phát sinh chứng từ liên quan";
                            result.Code = data.AccountingObjectCode;
                            result.IsSucceed = false;
                            strResult.Add(result);
                        }
                    }
                    catch (Exception ex)
                    {
                        Utils.IAccountingObjectService.RolbackTran();
                        MSG.Error("Xóa chứng từ thất bại");
                    }
                }
                if (lst.Count > 1 && strResult.Count > 0) new MsgResult(strResult, false).ShowDialog(this);
                else MSG.Information(strResult.First().Reason);
                LoadDulieu();
            }
            
            //if (uGrid.Selected.Rows.Count > 0)
            //{
            //    AccountingObject temp = uGrid.Selected.Rows[0].ListObject as AccountingObject;
            //    if (MSG.Question(string.Format(resSystem.MSG_System_05, temp.AccountingObjectCode)) == System.Windows.Forms.DialogResult.Yes)
            //    {
            //        if (!Utils.checkRelationVoucher(temp))
            //        {
            //            _IAccountingObjectService.BeginTran();
            //            _IAccountingObjectService.Delete(temp);
            //            _IAccountingObjectService.CommitTran();
            //            Utils.ClearCacheByType<AccountingObject>();
            //            SetLabe();
            //            LoadDulieu();
            //        }
            //        else
            //        {
            //            MSG.Error("Cần xóa chứng từ kế toán có liên quan trước khi xóa dữ liệu danh mục");
            //        }
            //    }
            //}
            //else
            //    MSG.Error(resSystem.MSG_System_06);
        }
        #endregion
        #region Utils
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, TextMessage.ConstDatabase.AccountingObject_TableName);
            Utils.ConfigGrid(gridResult, "", DanhSach());
            utralGrid.ActiveRow = utralGrid.ActiveRowScrollRegion.FirstRow;
            uGrid.DisplayLayout.Bands[0].Columns["EmployeeBirthday"].Format = Constants.DdMMyyyy;
            uGrid.DisplayLayout.Bands[0].Columns["IssueDate"].Format = Constants.DdMMyyyy;
            uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectCode"].Header.Caption = "Mã nhân viên";
            uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectName"].Header.Caption = "Tên nhân viên";
            uGrid.DisplayLayout.Bands[0].Columns["AccountObjectGroupIdNameView"].Hidden = true;
            uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectCategory"].Hidden = true;
            uGrid.DisplayLayout.Bands[0].Columns["TaxCode"].Hidden = true;
            gridResult.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            uGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Extended;

        }
        #endregion
        #region Event
        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void tsmAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void tsmEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            ResetFunction();
        }
        protected override void ResetFunction()
        {
            LoadDulieu();
        }

        private void uGrid_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (e.Row.Index == -1) return;
            EditFunction();
        }

        private void uGrid_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                AccountingObject temp = uGrid.Selected.Rows[0].ListObject as AccountingObject;

                labeAccountingObjectCode.Text = temp.AccountingObjectCode;
                labeAccountingObjectName.Text = temp.AccountingObjectName;
                if (temp.ContactSex == 0)
                {
                    LabeSex.Text = "Nam";
                }
                else if (temp.ContactSex == 1)
                {
                    LabeSex.Text = "Nữ";
                }
                else
                {
                    LabeSex.Text = "Không xác định";
                }
                if (temp.Department != null)
                    LabeDepartment.Text = temp.Department.DepartmentName;
                labePhone.Text = temp.ContactMobile;
                labeContacTitle.Text = temp.ContactTitle;
                //labeFamilyDeductionAmount.Text = Utils.FormatNumberic(temp.FamilyDeductionAmount.ToString(), ConstDatabase.Format_TienVND);
                LabeAddress.Text = temp.Address;
                labePhone.Text = temp.Tel;
                labePhoneHome.Text = temp.ContactHomeTel;
                LabeOfficePhone.Text = temp.ContactOfficeTel;
                labeIdentificationNo.Text = temp.IdentificationNo;
                LabeEmail.Text = temp.Email;
                labeTaxCode.Text = temp.TaxCode;
                LabeAcbank.Text = temp.BankAccount;
                labeNameBank.Text = temp.BankName;
                selectedGuid = temp.ID;
            }

        }
        #endregion

        #region load lại labe khi xóa
        // đồng bộ lại labe khi xóa
        public void SetLabe()
        {
            labeAccountingObjectCode.Text = "";
            labeAccountingObjectName.Text = "";
            //LabeSex.Text = temp.ContactSex;
            //LabeDepartment.Text = temp.Address;
            labePhone.Text = "";
            labeContacTitle.Text = "";
            //labeFamilyDeductionAmount.Text = "";
            LabeAddress.Text = "";
            labePhone.Text = "";
            labePhoneHome.Text = "";
            LabeOfficePhone.Text = "";
            labeIdentificationNo.Text = "";
            LabeEmail.Text = "";
            labeTaxCode.Text = "";
            //LabeAcbank.Text = temp.BankAccountDetailID.ToString();
            labeNameBank.Text = "";
        }
        #endregion

        private void uGrid_ClickCell(object sender, ClickCellEventArgs e)
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                temp = uGrid.Selected.Rows[0].ListObject as AccountingObject;
                //lstGeneral = _IGeneralLedgerService.GetListEmp(temp.ID, (DateTime)dteFromDate.Value, (DateTime)dteToDate.Value);
                List<VoucherAccountingObject> voucher = listVoucherAccountingObject.Where(p => p.AccountingObjectId == selectedGuid || p.EmployeeId == selectedGuid && p.Date >= (DateTime)dteFromDate.Value && p.Date <= (DateTime)dteToDate.Value).OrderByDescending(n => n.Date).ToList();
                gridResult.DataSource = voucher;
                ConfigGrid(gridResult);
            }
        }

        private List<TemplateColumn> DanhSach()
        {
            return new List<TemplateColumn>{
                new TemplateColumn{
                    ColumnName = "TypeName",
                    ColumnCaption = "Loại chứng từ",
                    //ColumnWidth = 120,
                    //ColumnMaxWidth = 120,
                    IsVisible = true,
                    IsVisibleCbb = true,
                    VisiblePosition = 1
                },
                new TemplateColumn{
                    ColumnName = "Date",
                    ColumnCaption = "Ngày chứng từ",
                    //ColumnWidth = 120,
                    //ColumnMaxWidth = 120,
                    IsVisible = true,
                    IsVisibleCbb = true,
                    VisiblePosition =2
                },
                 new TemplateColumn{
                    ColumnName = "No",
                    ColumnCaption = "Số chứng từ",
                    //ColumnWidth = 120,
                    //ColumnMaxWidth = 120,
                    IsVisible = true,
                    IsVisibleCbb = true,
                    VisiblePosition =3
                },
                 new TemplateColumn{
                    ColumnName = "Description",
                    ColumnCaption = "Diễn giải",
                    //ColumnWidth = 200,
                    //ColumnMaxWidth = 200,
                    IsVisible = true,
                    IsVisibleCbb = true,
                    VisiblePosition =4
                },
            };
        }

        //private void cbxTime_ValueChanged(object sender, EventArgs e)
        //{
        //    #region config ultradatetime
        //    String choice = cbbAboutTime.Value.ToString();
        //    DateTime dNow = DateTime.Now;
        //    DateTime dFrom = new DateTime();
        //    DateTime dTo = new DateTime();
        //    switch (choice)
        //    {
        //        case "1":
        //            dFrom = new DateTime(dNow.Year, 1, 1);
        //            dTo = new DateTime(dNow.Year, 1, 31);
        //            dateTimeTo.Value = dFrom;
        //            dateTimeFrom.Value = dTo;
        //            break;
        //        case "2":
        //            dFrom = new DateTime(dNow.Year, 2, 1);
        //            if (dNow.Year % 4 == 0 && dNow.Year % 100 == 0)
        //            {
        //                dTo = new DateTime(dNow.Year, 2, 29);
        //                dateTimeTo.Value = dFrom;
        //                dateTimeFrom.Value = dTo;
        //            }
        //            else
        //            {
        //                dTo = new DateTime(dNow.Year, 2, 28);
        //                dateTimeTo.Value = dFrom;
        //                dateTimeFrom.Value = dTo;
        //            }
        //            break;
        //        case "3":
        //            dFrom = new DateTime(dNow.Year, 3, 1);
        //            dTo = new DateTime(dNow.Year, 3, 31);
        //            dateTimeTo.Value = dFrom;
        //            dateTimeFrom.Value = dTo;
        //            break;
        //        case "4":
        //            dFrom = new DateTime(dNow.Year, 4, 1);
        //            dTo = new DateTime(dNow.Year, 4, 30);
        //            dateTimeTo.Value = dFrom;
        //            dateTimeFrom.Value = dTo;
        //            break;
        //        case "5":
        //            dFrom = new DateTime(dNow.Year, 5, 1);
        //            dTo = new DateTime(dNow.Year, 5, 31);
        //            dateTimeTo.Value = dFrom;
        //            dateTimeFrom.Value = dTo;
        //            break;
        //        case "6":
        //            dFrom = new DateTime(dNow.Year, 6, 1);
        //            dTo = new DateTime(dNow.Year, 6, 30);
        //            dateTimeTo.Value = dFrom;
        //            dateTimeFrom.Value = dTo;
        //            break;
        //        case "7":
        //            dFrom = new DateTime(dNow.Year, 7, 1);
        //            dTo = new DateTime(dNow.Year, 7, 31);
        //            dateTimeTo.Value = dFrom;
        //            dateTimeFrom.Value = dTo;
        //            break;
        //        case "8":
        //            dFrom = new DateTime(dNow.Year, 8, 1);
        //            dTo = new DateTime(dNow.Year, 8, 31);
        //            dateTimeTo.Value = dFrom;
        //            dateTimeFrom.Value = dTo;
        //            break;
        //        case "9":
        //            dFrom = new DateTime(dNow.Year, 9, 1);
        //            dTo = new DateTime(dNow.Year, 9, 30);
        //            dateTimeTo.Value = dFrom;
        //            dateTimeFrom.Value = dTo;
        //            break;
        //        case "10":
        //            dFrom = new DateTime(dNow.Year, 10, 1);
        //            dTo = new DateTime(dNow.Year, 10, 31);
        //            dateTimeTo.Value = dFrom;
        //            dateTimeFrom.Value = dTo;
        //            break;
        //        case "11":
        //            dFrom = new DateTime(dNow.Year, 11, 1);
        //            dTo = new DateTime(dNow.Year, 11, 30);
        //            dateTimeTo.Value = dFrom;
        //            dateTimeFrom.Value = dTo;
        //            break;
        //        case "12":
        //            dFrom = new DateTime(dNow.Year, 12, 1);
        //            dTo = new DateTime(dNow.Year, 12, 31);
        //            dateTimeTo.Value = dFrom;
        //            dateTimeFrom.Value = dTo;
        //            break;
        //    }
        //    #endregion
        //}

        private void dateTimeTo_ValueChanged(object sender, EventArgs e)
        {
            //lstGeneral = _IGeneralLedgerService.GetListEmp(temp.ID, (DateTime)dteFromDate.Value, (DateTime)dteToDate.Value);

            List<VoucherAccountingObject> voucher = listVoucherAccountingObject.Where(p => p.AccountingObjectId == selectedGuid || p.EmployeeId == selectedGuid && p.Date >= (DateTime)dteFromDate.Value && p.Date <= (DateTime)dteToDate.Value).ToList();
            gridResult.DataSource = voucher;
            ConfigGrid(gridResult);
        }

        private void dateTimeFrom_ValueChanged(object sender, EventArgs e)
        {
            //lstGeneral = _IGeneralLedgerService.GetListEmp(temp.ID, (DateTime)dteFromDate.Value, (DateTime)dteToDate.Value);
            List<VoucherAccountingObject> voucher = listVoucherAccountingObject.Where(p => p.AccountingObjectId == selectedGuid || p.EmployeeId == selectedGuid && p.Date >= (DateTime)dteFromDate.Value && p.Date <= (DateTime)dteToDate.Value).ToList();
            gridResult.DataSource = voucher;
            ConfigGrid(gridResult);
        }

        private void cbbAboutTime_ValueChanged(object sender, EventArgs e)
        {
            if (cbbAboutTime.SelectedRow != null)
            {
                var model = cbbAboutTime.SelectedRow.ListObject as Item;

                Utils.GetDateBeginDateEnd(ngayHoachToan.Year,ngayHoachToan, model, out dtBegin, out dtEnd);
                dteFromDate.DateTime = dtBegin;
                dteToDate.DateTime = dtEnd;
                List<VoucherAccountingObject> voucher = listVoucherAccountingObject.
    Where(p => p.AccountingObjectId == selectedGuid || p.EmployeeId == selectedGuid && p.Date >= dtBegin && p.Date <= dtEnd).ToList();
                foreach (var item in voucher)
                {
                    var firstOrDefault = Utils.ListType.FirstOrDefault(x => x.ID == item.TypeID);
                    if (firstOrDefault != null)
                        item.TypeName = firstOrDefault.TypeName;
                }

                gridResult.DataSource = voucher;
                gridResult.ConfigGrid(ConstDatabase.AccountingObject_TableName);
                Utils.ConfigGrid(gridResult, "", DanhSach());
            }
        }

        private void btnViewVoucher_Click(object sender, EventArgs e)
        {
            if (gridResult.ActiveRow == null) return;
            var voucher = gridResult.ActiveRow.ListObject as VoucherAccountingObject;
            if (voucher == null) return;
            //ViewVoucherSelected(voucher);
            Utils.ViewVoucherSelected(voucher.ID, voucher.TypeID);
        }

        private void gridResult_DoubleClick(object sender, EventArgs e)
        {
            btnViewVoucher_Click(sender,e);
        }

        private void FAccountingObjectEmployee_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGrid.ResetText();
            uGrid.ResetUpdateMode();
            uGrid.ResetExitEditModeOnLeave();
            uGrid.ResetRowUpdateCancelAction();
            uGrid.DataSource = null;
            uGrid.Layouts.Clear();
            uGrid.ResetLayouts();
            uGrid.ResetDisplayLayout();
            uGrid.Refresh();
            uGrid.ClearUndoHistory();
            uGrid.ClearXsdConstraints();

            gridResult.ResetText();
            gridResult.ResetUpdateMode();
            gridResult.ResetExitEditModeOnLeave();
            gridResult.ResetRowUpdateCancelAction();
            gridResult.DataSource = null;
            gridResult.Layouts.Clear();
            gridResult.ResetLayouts();
            gridResult.ResetDisplayLayout();
            gridResult.Refresh();
            gridResult.ClearUndoHistory();
            gridResult.ClearXsdConstraints();
        }

        private void FAccountingObjectEmployee_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            UltraGrid ultraGridExport = uGridExport;
            ultraGridExport.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            //LoadDuLieuGridExport(ultraGridExport);
            LoadDuLieuExport();
            Utils.ConfigGrid(ultraGridExport, TextMessage.ConstDatabase.EmployeeExportExcel);
            ultraGridExport.DisplayLayout.Bands[0].Columns["EmployeeBirthday"].Format = Constants.DdMMyyyy;
            Utils.ExportExcel(ultraGridExport, "Export_DMNhanVien");
        }
    }
}
