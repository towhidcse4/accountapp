﻿namespace Accounting
{
    partial class FAccountingObjectEmployeeDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem5 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem10 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem8 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem4 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.txtTel = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uttraTTNLL = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel24 = new Infragistics.Win.Misc.UltraLabel();
            this.txtContactOfficeTel = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel17 = new Infragistics.Win.Misc.UltraLabel();
            this.txtContactHomeTel = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtEmail = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtAddress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.chkIsUnofficialStaff = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.txtSalaryCoefficient = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel31 = new Infragistics.Win.Misc.UltraLabel();
            this.txtInsuranceSalary = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtAccounttingObjectCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccounttingObjectName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtNumberOfDependent = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.lblGroupCode = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.lblGroupName = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAgreementSalary = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtIssueDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.txtEmployeeBirthday = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.cbbObjectType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.txtTaxCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel21 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.CbbContactSex = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.txtIssueBy = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbDepartment = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel28 = new Infragistics.Win.Misc.UltraLabel();
            this.txtIdentificationNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel32 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel34 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel33 = new Infragistics.Win.Misc.UltraLabel();
            this.txtContactTitle = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.cms4Grid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.UtraThongtintochuc = new Infragistics.Win.Misc.UltraGroupBox();
            this.chkIsLabourUnionFree = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.chkIsInsured = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ckeIsActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.btnSaveContinue = new Infragistics.Win.Misc.UltraButton();
            this.ultraTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uttraTTNLL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactOfficeTel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactHomeTel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsUnofficialStaff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccounttingObjectCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccounttingObjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIssueDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeBirthday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbObjectType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbbContactSex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIssueBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDepartment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdentificationNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactTitle)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            this.cms4Grid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UtraThongtintochuc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsLabourUnionFree)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsInsured)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckeIsActive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).BeginInit();
            this.ultraTabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel5);
            this.ultraTabPageControl1.Controls.Add(this.txtTel);
            this.ultraTabPageControl1.Controls.Add(this.uttraTTNLL);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel24);
            this.ultraTabPageControl1.Controls.Add(this.txtContactOfficeTel);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel17);
            this.ultraTabPageControl1.Controls.Add(this.txtContactHomeTel);
            this.ultraTabPageControl1.Controls.Add(this.txtEmail);
            this.ultraTabPageControl1.Controls.Add(this.txtAddress);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel8);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel4);
            this.ultraTabPageControl1.Controls.Add(this.chkIsUnofficialStaff);
            this.ultraTabPageControl1.Controls.Add(this.txtSalaryCoefficient);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel7);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel31);
            this.ultraTabPageControl1.Controls.Add(this.txtInsuranceSalary);
            this.ultraTabPageControl1.Controls.Add(this.txtAccounttingObjectCode);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel3);
            this.ultraTabPageControl1.Controls.Add(this.txtAccounttingObjectName);
            this.ultraTabPageControl1.Controls.Add(this.txtNumberOfDependent);
            this.ultraTabPageControl1.Controls.Add(this.lblGroupCode);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel1);
            this.ultraTabPageControl1.Controls.Add(this.lblGroupName);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel13);
            this.ultraTabPageControl1.Controls.Add(this.txtAgreementSalary);
            this.ultraTabPageControl1.Controls.Add(this.txtIssueDate);
            this.ultraTabPageControl1.Controls.Add(this.txtEmployeeBirthday);
            this.ultraTabPageControl1.Controls.Add(this.cbbObjectType);
            this.ultraTabPageControl1.Controls.Add(this.txtTaxCode);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel21);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel16);
            this.ultraTabPageControl1.Controls.Add(this.CbbContactSex);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel10);
            this.ultraTabPageControl1.Controls.Add(this.txtIssueBy);
            this.ultraTabPageControl1.Controls.Add(this.cbbDepartment);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel28);
            this.ultraTabPageControl1.Controls.Add(this.txtIdentificationNo);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel32);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel34);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel6);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel33);
            this.ultraTabPageControl1.Controls.Add(this.txtContactTitle);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(1004, 323);
            // 
            // ultraLabel5
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance1;
            this.ultraLabel5.Location = new System.Drawing.Point(25, 291);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(98, 22);
            this.ultraLabel5.TabIndex = 39;
            this.ultraLabel5.Text = "Điện thoại di động";
            // 
            // txtTel
            // 
            this.txtTel.AutoSize = false;
            this.txtTel.Location = new System.Drawing.Point(184, 291);
            this.txtTel.MaxLength = 30;
            this.txtTel.Name = "txtTel";
            this.txtTel.Size = new System.Drawing.Size(168, 21);
            this.txtTel.TabIndex = 35;
            this.txtTel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTel_KeyPress);
            // 
            // uttraTTNLL
            // 
            appearance2.FontData.BoldAsString = "True";
            this.uttraTTNLL.Appearance = appearance2;
            this.uttraTTNLL.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.uttraTTNLL.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.uttraTTNLL.Location = new System.Drawing.Point(3, 195);
            this.uttraTTNLL.Name = "uttraTTNLL";
            this.uttraTTNLL.Size = new System.Drawing.Size(998, 18);
            this.uttraTTNLL.TabIndex = 31;
            this.uttraTTNLL.Text = "Thông tin liên lạc";
            this.uttraTTNLL.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel24
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextVAlignAsString = "Middle";
            this.ultraLabel24.Appearance = appearance3;
            this.ultraLabel24.Location = new System.Drawing.Point(25, 263);
            this.ultraLabel24.Name = "ultraLabel24";
            this.ultraLabel24.Size = new System.Drawing.Size(105, 22);
            this.ultraLabel24.TabIndex = 40;
            this.ultraLabel24.Text = "Điện thoại cơ quan ";
            // 
            // txtContactOfficeTel
            // 
            this.txtContactOfficeTel.AutoSize = false;
            this.txtContactOfficeTel.Location = new System.Drawing.Point(184, 263);
            this.txtContactOfficeTel.MaxLength = 30;
            this.txtContactOfficeTel.Name = "txtContactOfficeTel";
            this.txtContactOfficeTel.Size = new System.Drawing.Size(168, 21);
            this.txtContactOfficeTel.TabIndex = 33;
            this.txtContactOfficeTel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtContactOfficeTel_KeyPress);
            // 
            // ultraLabel17
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.ultraLabel17.Appearance = appearance4;
            this.ultraLabel17.Location = new System.Drawing.Point(389, 263);
            this.ultraLabel17.Name = "ultraLabel17";
            this.ultraLabel17.Size = new System.Drawing.Size(111, 22);
            this.ultraLabel17.TabIndex = 38;
            this.ultraLabel17.Text = "Điện thoại nhà riêng ";
            // 
            // txtContactHomeTel
            // 
            this.txtContactHomeTel.AutoSize = false;
            this.txtContactHomeTel.Location = new System.Drawing.Point(532, 263);
            this.txtContactHomeTel.MaxLength = 30;
            this.txtContactHomeTel.Name = "txtContactHomeTel";
            this.txtContactHomeTel.Size = new System.Drawing.Size(168, 21);
            this.txtContactHomeTel.TabIndex = 34;
            this.txtContactHomeTel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtContactHomeTel_KeyPress);
            // 
            // txtEmail
            // 
            this.txtEmail.AutoSize = false;
            this.txtEmail.Location = new System.Drawing.Point(532, 291);
            this.txtEmail.MaxLength = 512;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(168, 21);
            this.txtEmail.TabIndex = 36;
            // 
            // txtAddress
            // 
            this.txtAddress.AutoSize = false;
            this.txtAddress.Location = new System.Drawing.Point(184, 224);
            this.txtAddress.MaxLength = 512;
            this.txtAddress.Multiline = true;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(794, 33);
            this.txtAddress.TabIndex = 32;
            // 
            // ultraLabel8
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.TextVAlignAsString = "Middle";
            this.ultraLabel8.Appearance = appearance5;
            this.ultraLabel8.Location = new System.Drawing.Point(389, 291);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(36, 22);
            this.ultraLabel8.TabIndex = 37;
            this.ultraLabel8.Text = "Email";
            // 
            // ultraLabel4
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance6;
            this.ultraLabel4.Location = new System.Drawing.Point(26, 224);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(42, 22);
            this.ultraLabel4.TabIndex = 41;
            this.ultraLabel4.Text = "Địa chỉ";
            // 
            // chkIsUnofficialStaff
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            this.chkIsUnofficialStaff.Appearance = appearance7;
            this.chkIsUnofficialStaff.BackColor = System.Drawing.Color.Transparent;
            this.chkIsUnofficialStaff.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsUnofficialStaff.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
            this.chkIsUnofficialStaff.Location = new System.Drawing.Point(365, 154);
            this.chkIsUnofficialStaff.Name = "chkIsUnofficialStaff";
            this.chkIsUnofficialStaff.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkIsUnofficialStaff.Size = new System.Drawing.Size(343, 16);
            this.chkIsUnofficialStaff.TabIndex = 26;
            this.chkIsUnofficialStaff.Text = "Là nhân viên không chính thức";
            // 
            // txtSalaryCoefficient
            // 
            appearance8.TextHAlignAsString = "Right";
            this.txtSalaryCoefficient.Appearance = appearance8;
            this.txtSalaryCoefficient.AutoSize = false;
            this.txtSalaryCoefficient.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.txtSalaryCoefficient.InputMask = "{LOC}nn,nnn,nnn.nn";
            this.txtSalaryCoefficient.Location = new System.Drawing.Point(798, 125);
            this.txtSalaryCoefficient.Name = "txtSalaryCoefficient";
            this.txtSalaryCoefficient.PromptChar = ' ';
            this.txtSalaryCoefficient.Size = new System.Drawing.Size(168, 22);
            this.txtSalaryCoefficient.TabIndex = 13;
            // 
            // ultraLabel7
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.TextVAlignAsString = "Middle";
            this.ultraLabel7.Appearance = appearance9;
            this.ultraLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.ultraLabel7.Location = new System.Drawing.Point(669, 125);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(68, 22);
            this.ultraLabel7.TabIndex = 30;
            this.ultraLabel7.Text = "Hệ số lương";
            // 
            // ultraLabel31
            // 
            appearance10.BackColor = System.Drawing.Color.Transparent;
            appearance10.TextVAlignAsString = "Middle";
            this.ultraLabel31.Appearance = appearance10;
            this.ultraLabel31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.ultraLabel31.Location = new System.Drawing.Point(365, 41);
            this.ultraLabel31.Name = "ultraLabel31";
            this.ultraLabel31.Size = new System.Drawing.Size(79, 22);
            this.ultraLabel31.TabIndex = 22;
            this.ultraLabel31.Text = "Phòng ban (*)";
            // 
            // txtInsuranceSalary
            // 
            appearance11.TextHAlignAsString = "Right";
            this.txtInsuranceSalary.Appearance = appearance11;
            this.txtInsuranceSalary.AutoSize = false;
            this.txtInsuranceSalary.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.txtInsuranceSalary.InputMask = "{LOC}nn,nnn,nnn.nn";
            this.txtInsuranceSalary.Location = new System.Drawing.Point(465, 125);
            this.txtInsuranceSalary.Name = "txtInsuranceSalary";
            this.txtInsuranceSalary.PromptChar = ' ';
            this.txtInsuranceSalary.Size = new System.Drawing.Size(168, 22);
            this.txtInsuranceSalary.TabIndex = 12;
            // 
            // txtAccounttingObjectCode
            // 
            this.txtAccounttingObjectCode.AutoSize = false;
            this.txtAccounttingObjectCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtAccounttingObjectCode.Location = new System.Drawing.Point(159, 13);
            this.txtAccounttingObjectCode.MaxLength = 25;
            this.txtAccounttingObjectCode.Name = "txtAccounttingObjectCode";
            this.txtAccounttingObjectCode.Size = new System.Drawing.Size(168, 22);
            this.txtAccounttingObjectCode.TabIndex = 0;
            // 
            // ultraLabel3
            // 
            appearance12.BackColor = System.Drawing.Color.Transparent;
            appearance12.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance12;
            this.ultraLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.ultraLabel3.Location = new System.Drawing.Point(365, 125);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(135, 22);
            this.ultraLabel3.TabIndex = 25;
            this.ultraLabel3.Text = "Lương đóng BH";
            // 
            // txtAccounttingObjectName
            // 
            this.txtAccounttingObjectName.AutoSize = false;
            this.txtAccounttingObjectName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtAccounttingObjectName.Location = new System.Drawing.Point(465, 13);
            this.txtAccounttingObjectName.MaxLength = 512;
            this.txtAccounttingObjectName.Name = "txtAccounttingObjectName";
            this.txtAccounttingObjectName.Size = new System.Drawing.Size(501, 22);
            this.txtAccounttingObjectName.TabIndex = 1;
            // 
            // txtNumberOfDependent
            // 
            appearance13.TextHAlignAsString = "Right";
            this.txtNumberOfDependent.Appearance = appearance13;
            this.txtNumberOfDependent.AutoSize = false;
            this.txtNumberOfDependent.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.txtNumberOfDependent.InputMask = "nnnnnnnnn";
            this.txtNumberOfDependent.Location = new System.Drawing.Point(798, 68);
            this.txtNumberOfDependent.Name = "txtNumberOfDependent";
            this.txtNumberOfDependent.PromptChar = ' ';
            this.txtNumberOfDependent.Size = new System.Drawing.Size(168, 22);
            this.txtNumberOfDependent.TabIndex = 7;
            // 
            // lblGroupCode
            // 
            appearance14.BackColor = System.Drawing.Color.Transparent;
            appearance14.TextVAlignAsString = "Middle";
            this.lblGroupCode.Appearance = appearance14;
            this.lblGroupCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.lblGroupCode.Location = new System.Drawing.Point(25, 13);
            this.lblGroupCode.Name = "lblGroupCode";
            this.lblGroupCode.Size = new System.Drawing.Size(93, 22);
            this.lblGroupCode.TabIndex = 15;
            this.lblGroupCode.Text = "Mã nhân viên (*) ";
            // 
            // ultraLabel1
            // 
            appearance15.BackColor = System.Drawing.Color.Transparent;
            appearance15.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance15;
            this.ultraLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.ultraLabel1.Location = new System.Drawing.Point(669, 68);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(102, 22);
            this.ultraLabel1.TabIndex = 28;
            this.ultraLabel1.Text = "Số người phụ thuộc ";
            // 
            // lblGroupName
            // 
            appearance16.BackColor = System.Drawing.Color.Transparent;
            appearance16.TextVAlignAsString = "Middle";
            this.lblGroupName.Appearance = appearance16;
            this.lblGroupName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.lblGroupName.Location = new System.Drawing.Point(365, 13);
            this.lblGroupName.Name = "lblGroupName";
            this.lblGroupName.Size = new System.Drawing.Size(94, 22);
            this.lblGroupName.TabIndex = 21;
            this.lblGroupName.Text = "Tên nhân viên (*)";
            // 
            // ultraLabel13
            // 
            appearance17.BackColor = System.Drawing.Color.Transparent;
            appearance17.TextVAlignAsString = "Middle";
            this.ultraLabel13.Appearance = appearance17;
            this.ultraLabel13.Location = new System.Drawing.Point(25, 41);
            this.ultraLabel13.Name = "ultraLabel13";
            this.ultraLabel13.Size = new System.Drawing.Size(49, 22);
            this.ultraLabel13.TabIndex = 16;
            this.ultraLabel13.Text = "Chức vụ";
            // 
            // txtAgreementSalary
            // 
            appearance18.TextHAlignAsString = "Right";
            this.txtAgreementSalary.Appearance = appearance18;
            this.txtAgreementSalary.AutoSize = false;
            this.txtAgreementSalary.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.txtAgreementSalary.InputMask = "{LOC}nn,nnn,nnn.nn";
            this.txtAgreementSalary.Location = new System.Drawing.Point(159, 125);
            this.txtAgreementSalary.Name = "txtAgreementSalary";
            this.txtAgreementSalary.PromptChar = ' ';
            this.txtAgreementSalary.Size = new System.Drawing.Size(168, 22);
            this.txtAgreementSalary.TabIndex = 11;
            // 
            // txtIssueDate
            // 
            appearance19.TextHAlignAsString = "Left";
            appearance19.TextVAlignAsString = "Middle";
            this.txtIssueDate.Appearance = appearance19;
            this.txtIssueDate.AutoSize = false;
            this.txtIssueDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.txtIssueDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtIssueDate.Location = new System.Drawing.Point(465, 96);
            this.txtIssueDate.MaskInput = "dd/mm/yyyy";
            this.txtIssueDate.Name = "txtIssueDate";
            this.txtIssueDate.Size = new System.Drawing.Size(168, 22);
            this.txtIssueDate.SpinButtonAlignment = Infragistics.Win.ButtonAlignment.Left;
            this.txtIssueDate.TabIndex = 9;
            this.txtIssueDate.Value = null;
            // 
            // txtEmployeeBirthday
            // 
            appearance20.TextHAlignAsString = "Left";
            appearance20.TextVAlignAsString = "Middle";
            this.txtEmployeeBirthday.Appearance = appearance20;
            this.txtEmployeeBirthday.AutoSize = false;
            this.txtEmployeeBirthday.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.txtEmployeeBirthday.DropDownCalendarAlignment = Infragistics.Win.DropDownListAlignment.Left;
            this.txtEmployeeBirthday.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtEmployeeBirthday.Location = new System.Drawing.Point(798, 41);
            this.txtEmployeeBirthday.MaskInput = "dd/mm/yyyy";
            this.txtEmployeeBirthday.Name = "txtEmployeeBirthday";
            this.txtEmployeeBirthday.Size = new System.Drawing.Size(168, 22);
            this.txtEmployeeBirthday.TabIndex = 4;
            this.txtEmployeeBirthday.Value = null;
            // 
            // cbbObjectType
            // 
            this.cbbObjectType.AutoSize = false;
            this.cbbObjectType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            valueListItem5.DataValue = "0";
            valueListItem5.DisplayText = "Khách hàng";
            valueListItem10.DataValue = "1";
            valueListItem10.DisplayText = "Nhà cung cấp";
            valueListItem8.DataValue = "2";
            valueListItem8.DisplayText = "Khách hàng/Nhà cung cấp";
            valueListItem4.DataValue = "3";
            valueListItem4.DisplayText = "Khác";
            this.cbbObjectType.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem5,
            valueListItem10,
            valueListItem8,
            valueListItem4});
            this.cbbObjectType.Location = new System.Drawing.Point(159, 154);
            this.cbbObjectType.Name = "cbbObjectType";
            this.cbbObjectType.Size = new System.Drawing.Size(168, 22);
            this.cbbObjectType.TabIndex = 14;
            // 
            // txtTaxCode
            // 
            this.txtTaxCode.AutoSize = false;
            this.txtTaxCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtTaxCode.Location = new System.Drawing.Point(465, 68);
            this.txtTaxCode.MaxLength = 14;
            this.txtTaxCode.Name = "txtTaxCode";
            this.txtTaxCode.Size = new System.Drawing.Size(168, 22);
            this.txtTaxCode.TabIndex = 6;
            // 
            // ultraLabel21
            // 
            appearance21.BackColor = System.Drawing.Color.Transparent;
            appearance21.TextVAlignAsString = "Middle";
            this.ultraLabel21.Appearance = appearance21;
            this.ultraLabel21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.ultraLabel21.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ultraLabel21.Location = new System.Drawing.Point(25, 154);
            this.ultraLabel21.Name = "ultraLabel21";
            this.ultraLabel21.Size = new System.Drawing.Size(119, 22);
            this.ultraLabel21.TabIndex = 20;
            this.ultraLabel21.Text = "Loại đối tượng kế toán ";
            // 
            // ultraLabel16
            // 
            appearance22.BackColor = System.Drawing.Color.Transparent;
            appearance22.TextVAlignAsString = "Middle";
            this.ultraLabel16.Appearance = appearance22;
            this.ultraLabel16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.ultraLabel16.Location = new System.Drawing.Point(25, 68);
            this.ultraLabel16.Name = "ultraLabel16";
            this.ultraLabel16.Size = new System.Drawing.Size(68, 22);
            this.ultraLabel16.TabIndex = 17;
            this.ultraLabel16.Text = "Giới tính";
            // 
            // CbbContactSex
            // 
            this.CbbContactSex.AutoSize = false;
            this.CbbContactSex.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            valueListItem1.DataValue = "0";
            valueListItem1.DisplayText = "Nam";
            valueListItem2.DataValue = "1";
            valueListItem2.DisplayText = "Nữ";
            valueListItem3.DataValue = "2";
            valueListItem3.DisplayText = "Không xác định";
            this.CbbContactSex.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2,
            valueListItem3});
            this.CbbContactSex.Location = new System.Drawing.Point(159, 68);
            this.CbbContactSex.Name = "CbbContactSex";
            this.CbbContactSex.Size = new System.Drawing.Size(168, 22);
            this.CbbContactSex.TabIndex = 5;
            this.CbbContactSex.Text = "<Chọn giới tính>";
            // 
            // ultraLabel10
            // 
            appearance23.BackColor = System.Drawing.Color.Transparent;
            appearance23.TextVAlignAsString = "Middle";
            this.ultraLabel10.Appearance = appearance23;
            this.ultraLabel10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.ultraLabel10.Location = new System.Drawing.Point(365, 68);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(60, 22);
            this.ultraLabel10.TabIndex = 23;
            this.ultraLabel10.Text = "Mã số thuế";
            // 
            // txtIssueBy
            // 
            this.txtIssueBy.AutoSize = false;
            this.txtIssueBy.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtIssueBy.Location = new System.Drawing.Point(798, 96);
            this.txtIssueBy.MaxLength = 512;
            this.txtIssueBy.Name = "txtIssueBy";
            this.txtIssueBy.Size = new System.Drawing.Size(168, 22);
            this.txtIssueBy.TabIndex = 10;
            // 
            // cbbDepartment
            // 
            this.cbbDepartment.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.SuggestAppend;
            this.cbbDepartment.AutoSize = false;
            appearance24.BackColor = System.Drawing.Color.Transparent;
            appearance24.Image = global::Accounting.Properties.Resources.plus;
            editorButton1.Appearance = appearance24;
            appearance25.Image = global::Accounting.Properties.Resources.plus_press;
            editorButton1.PressedAppearance = appearance25;
            this.cbbDepartment.ButtonsRight.Add(editorButton1);
            this.cbbDepartment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.cbbDepartment.Location = new System.Drawing.Point(465, 41);
            this.cbbDepartment.Name = "cbbDepartment";
            this.cbbDepartment.Size = new System.Drawing.Size(168, 22);
            this.cbbDepartment.TabIndex = 3;
            this.cbbDepartment.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.cbbDepartment_EditorButtonClick);
            // 
            // ultraLabel28
            // 
            appearance26.BackColor = System.Drawing.Color.Transparent;
            appearance26.TextVAlignAsString = "Middle";
            this.ultraLabel28.Appearance = appearance26;
            this.ultraLabel28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.ultraLabel28.Location = new System.Drawing.Point(25, 125);
            this.ultraLabel28.Name = "ultraLabel28";
            this.ultraLabel28.Size = new System.Drawing.Size(105, 22);
            this.ultraLabel28.TabIndex = 19;
            this.ultraLabel28.Text = "Lương thỏa thuận";
            // 
            // txtIdentificationNo
            // 
            this.txtIdentificationNo.AutoSize = false;
            this.txtIdentificationNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtIdentificationNo.Location = new System.Drawing.Point(159, 96);
            this.txtIdentificationNo.MaxLength = 50;
            this.txtIdentificationNo.Name = "txtIdentificationNo";
            this.txtIdentificationNo.Size = new System.Drawing.Size(168, 22);
            this.txtIdentificationNo.TabIndex = 8;
            // 
            // ultraLabel32
            // 
            appearance27.BackColor = System.Drawing.Color.Transparent;
            appearance27.TextVAlignAsString = "Middle";
            this.ultraLabel32.Appearance = appearance27;
            this.ultraLabel32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.ultraLabel32.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ultraLabel32.Location = new System.Drawing.Point(669, 96);
            this.ultraLabel32.Name = "ultraLabel32";
            this.ultraLabel32.Size = new System.Drawing.Size(49, 22);
            this.ultraLabel32.TabIndex = 29;
            this.ultraLabel32.Text = "Nơi cấp ";
            // 
            // ultraLabel34
            // 
            appearance28.BackColor = System.Drawing.Color.Transparent;
            appearance28.TextVAlignAsString = "Middle";
            this.ultraLabel34.Appearance = appearance28;
            this.ultraLabel34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.ultraLabel34.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ultraLabel34.Location = new System.Drawing.Point(25, 96);
            this.ultraLabel34.Name = "ultraLabel34";
            this.ultraLabel34.Size = new System.Drawing.Size(68, 22);
            this.ultraLabel34.TabIndex = 18;
            this.ultraLabel34.Text = "Số CMTND ";
            // 
            // ultraLabel6
            // 
            appearance29.BackColor = System.Drawing.Color.Transparent;
            appearance29.TextVAlignAsString = "Middle";
            this.ultraLabel6.Appearance = appearance29;
            this.ultraLabel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.ultraLabel6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ultraLabel6.Location = new System.Drawing.Point(365, 96);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(61, 22);
            this.ultraLabel6.TabIndex = 24;
            this.ultraLabel6.Text = "Ngày Cấp ";
            // 
            // ultraLabel33
            // 
            appearance30.BackColor = System.Drawing.Color.Transparent;
            appearance30.TextVAlignAsString = "Middle";
            this.ultraLabel33.Appearance = appearance30;
            this.ultraLabel33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.ultraLabel33.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ultraLabel33.Location = new System.Drawing.Point(669, 41);
            this.ultraLabel33.Name = "ultraLabel33";
            this.ultraLabel33.Size = new System.Drawing.Size(61, 22);
            this.ultraLabel33.TabIndex = 27;
            this.ultraLabel33.Text = "Ngày sinh ";
            this.ultraLabel33.Click += new System.EventHandler(this.ultraLabel33_Click);
            // 
            // txtContactTitle
            // 
            this.txtContactTitle.AutoSize = false;
            this.txtContactTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtContactTitle.Location = new System.Drawing.Point(159, 41);
            this.txtContactTitle.MaxLength = 512;
            this.txtContactTitle.Name = "txtContactTitle";
            this.txtContactTitle.Size = new System.Drawing.Size(168, 22);
            this.txtContactTitle.TabIndex = 2;
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.uGrid);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(1004, 323);
            // 
            // uGrid
            // 
            this.uGrid.ContextMenuStrip = this.cms4Grid;
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.AlignWithDataRows;
            this.uGrid.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.uGrid.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGrid.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGrid.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGrid.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.uGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid.Location = new System.Drawing.Point(0, 0);
            this.uGrid.Name = "uGrid";
            this.uGrid.Size = new System.Drawing.Size(1004, 323);
            this.uGrid.TabIndex = 0;
            this.uGrid.Text = "uGrid";
            this.uGrid.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid_CellChange);
            // 
            // cms4Grid
            // 
            this.cms4Grid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmAdd,
            this.tsmDelete});
            this.cms4Grid.Name = "cms4Grid";
            this.cms4Grid.Size = new System.Drawing.Size(148, 48);
            // 
            // tsmAdd
            // 
            this.tsmAdd.Image = global::Accounting.Properties.Resources._1437136799_add;
            this.tsmAdd.Name = "tsmAdd";
            this.tsmAdd.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.tsmAdd.Size = new System.Drawing.Size(147, 22);
            this.tsmAdd.Text = "Thêm";
            this.tsmAdd.Click += new System.EventHandler(this.tsmAdd_Click);
            // 
            // tsmDelete
            // 
            this.tsmDelete.Image = global::Accounting.Properties.Resources.ubtnDelete;
            this.tsmDelete.Name = "tsmDelete";
            this.tsmDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.tsmDelete.Size = new System.Drawing.Size(147, 22);
            this.tsmDelete.Text = "Xóa";
            this.tsmDelete.Click += new System.EventHandler(this.tsmDelete_Click);
            // 
            // UtraThongtintochuc
            // 
            appearance31.FontData.BoldAsString = "True";
            this.UtraThongtintochuc.Appearance = appearance31;
            this.UtraThongtintochuc.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.UtraThongtintochuc.ContentPadding.Bottom = 10;
            this.UtraThongtintochuc.ContentPadding.Top = 10;
            this.UtraThongtintochuc.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.UtraThongtintochuc.Location = new System.Drawing.Point(0, 1);
            this.UtraThongtintochuc.Name = "UtraThongtintochuc";
            this.UtraThongtintochuc.Size = new System.Drawing.Size(1011, 21);
            this.UtraThongtintochuc.TabIndex = 0;
            this.UtraThongtintochuc.Text = "Thông tin chung";
            this.UtraThongtintochuc.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // chkIsLabourUnionFree
            // 
            this.chkIsLabourUnionFree.Checked = true;
            this.chkIsLabourUnionFree.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIsLabourUnionFree.Location = new System.Drawing.Point(255, 389);
            this.chkIsLabourUnionFree.Name = "chkIsLabourUnionFree";
            this.chkIsLabourUnionFree.Size = new System.Drawing.Size(98, 20);
            this.chkIsLabourUnionFree.TabIndex = 4;
            this.chkIsLabourUnionFree.Text = "Đóng đoàn phí";
            this.chkIsLabourUnionFree.Visible = false;
            // 
            // chkIsInsured
            // 
            this.chkIsInsured.Checked = true;
            this.chkIsInsured.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIsInsured.Location = new System.Drawing.Point(134, 389);
            this.chkIsInsured.Name = "chkIsInsured";
            this.chkIsInsured.Size = new System.Drawing.Size(112, 20);
            this.chkIsInsured.TabIndex = 3;
            this.chkIsInsured.Text = "Đóng bảo hiểm";
            this.chkIsInsured.Visible = false;
            // 
            // ckeIsActive
            // 
            appearance32.BackColor = System.Drawing.Color.Transparent;
            appearance32.BackColor2 = System.Drawing.Color.Transparent;
            appearance32.BackColorDisabled = System.Drawing.Color.Transparent;
            appearance32.BackColorDisabled2 = System.Drawing.Color.Transparent;
            this.ckeIsActive.Appearance = appearance32;
            this.ckeIsActive.BackColor = System.Drawing.Color.Transparent;
            this.ckeIsActive.BackColorInternal = System.Drawing.Color.Transparent;
            this.ckeIsActive.Checked = true;
            this.ckeIsActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckeIsActive.Location = new System.Drawing.Point(30, 389);
            this.ckeIsActive.Name = "ckeIsActive";
            this.ckeIsActive.Size = new System.Drawing.Size(95, 20);
            this.ckeIsActive.TabIndex = 2;
            this.ckeIsActive.Text = "Hoạt động";
            // 
            // btnClose
            // 
            appearance33.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance33;
            this.btnClose.Location = new System.Drawing.Point(911, 379);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 7;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            appearance34.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance34;
            this.btnSave.Location = new System.Drawing.Point(830, 379);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnSaveContinue
            // 
            appearance35.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSaveContinue.Appearance = appearance35;
            this.btnSaveContinue.Location = new System.Drawing.Point(698, 379);
            this.btnSaveContinue.Name = "btnSaveContinue";
            this.btnSaveContinue.Size = new System.Drawing.Size(126, 30);
            this.btnSaveContinue.TabIndex = 5;
            this.btnSaveContinue.Text = "Lưu và Thêm mới";
            this.btnSaveContinue.Click += new System.EventHandler(this.btnSaveContinue_Click);
            // 
            // ultraTabControl1
            // 
            this.ultraTabControl1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl2);
            this.ultraTabControl1.Location = new System.Drawing.Point(3, 24);
            this.ultraTabControl1.Name = "ultraTabControl1";
            this.ultraTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.ultraTabControl1.Size = new System.Drawing.Size(1008, 349);
            this.ultraTabControl1.TabIndex = 1;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "Thông tin chung";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "Tài khoản ngân hàng";
            this.ultraTabControl1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(1004, 323);
            // 
            // FAccountingObjectEmployeeDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1013, 424);
            this.Controls.Add(this.ultraTabControl1);
            this.Controls.Add(this.btnSaveContinue);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.chkIsInsured);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.chkIsLabourUnionFree);
            this.Controls.Add(this.ckeIsActive);
            this.Controls.Add(this.UtraThongtintochuc);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FAccountingObjectEmployeeDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Chi tiết nhân viên";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FAccountingObjectEmployeeDetail_FormClosed);
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uttraTTNLL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactOfficeTel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactHomeTel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsUnofficialStaff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccounttingObjectCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccounttingObjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIssueDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeBirthday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbObjectType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbbContactSex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIssueBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDepartment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdentificationNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactTitle)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            this.cms4Grid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.UtraThongtintochuc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsLabourUnionFree)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsInsured)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckeIsActive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).EndInit();
            this.ultraTabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox UtraThongtintochuc;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsLabourUnionFree;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsInsured;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor ckeIsActive;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraButton btnSaveContinue;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsUnofficialStaff;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtSalaryCoefficient;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.Misc.UltraLabel ultraLabel31;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtInsuranceSalary;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccounttingObjectCode;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccounttingObjectName;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtNumberOfDependent;
        private Infragistics.Win.Misc.UltraLabel lblGroupCode;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel lblGroupName;
        private Infragistics.Win.Misc.UltraLabel ultraLabel13;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtAgreementSalary;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor txtIssueDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor txtEmployeeBirthday;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbObjectType;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTaxCode;
        private Infragistics.Win.Misc.UltraLabel ultraLabel21;
        private Infragistics.Win.Misc.UltraLabel ultraLabel16;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor CbbContactSex;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtIssueBy;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbDepartment;
        private Infragistics.Win.Misc.UltraLabel ultraLabel28;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtIdentificationNo;
        private Infragistics.Win.Misc.UltraLabel ultraLabel32;
        private Infragistics.Win.Misc.UltraLabel ultraLabel34;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel33;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtContactTitle;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
        private System.Windows.Forms.ContextMenuStrip cms4Grid;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripMenuItem tsmDelete;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTel;
        private Infragistics.Win.Misc.UltraGroupBox uttraTTNLL;
        private Infragistics.Win.Misc.UltraLabel ultraLabel24;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtContactOfficeTel;
        private Infragistics.Win.Misc.UltraLabel ultraLabel17;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtContactHomeTel;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtEmail;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAddress;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
    }
}