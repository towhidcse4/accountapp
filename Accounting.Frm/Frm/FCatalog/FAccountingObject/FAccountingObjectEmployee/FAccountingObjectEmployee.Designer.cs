﻿namespace Accounting
{
    partial class FAccountingObjectEmployee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab5 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.labeNameBank = new Infragistics.Win.Misc.UltraLabel();
            this.LabeAcbank = new Infragistics.Win.Misc.UltraLabel();
            this.labeTaxCode = new Infragistics.Win.Misc.UltraLabel();
            this.LabeEmail = new Infragistics.Win.Misc.UltraLabel();
            this.labeIdentificationNo = new Infragistics.Win.Misc.UltraLabel();
            this.LabeOfficePhone = new Infragistics.Win.Misc.UltraLabel();
            this.labePhoneHome = new Infragistics.Win.Misc.UltraLabel();
            this.labePhone = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel29 = new Infragistics.Win.Misc.UltraLabel();
            this.Labe = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel31 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel32 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel33 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel34 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel35 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel36 = new Infragistics.Win.Misc.UltraLabel();
            this.LabeAddress = new Infragistics.Win.Misc.UltraLabel();
            this.labeContacTitle = new Infragistics.Win.Misc.UltraLabel();
            this.LabeDepartment = new Infragistics.Win.Misc.UltraLabel();
            this.LabeSex = new Infragistics.Win.Misc.UltraLabel();
            this.labeAccountingObjectName = new Infragistics.Win.Misc.UltraLabel();
            this.labeAccountingObjectCode = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel18 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel17 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraPanel3 = new Infragistics.Win.Misc.UltraPanel();
            this.gridResult = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraPanel2 = new Infragistics.Win.Misc.UltraPanel();
            this.cbbAboutTime = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.btnViewVoucher = new Infragistics.Win.Misc.UltraButton();
            this.ultraLabel22 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel21 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel20 = new Infragistics.Win.Misc.UltraLabel();
            this.dteToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.dteFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnAdd = new Infragistics.Win.Misc.UltraButton();
            this.btnDelete = new Infragistics.Win.Misc.UltraButton();
            this.btnEdit = new Infragistics.Win.Misc.UltraButton();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.cms4Grid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.tmsReLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.ultraTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraSplitter1 = new Infragistics.Win.Misc.UltraSplitter();
            this.uGridExport = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.btnExportExcel = new Infragistics.Win.Misc.UltraButton();
            this.ultraTabPageControl1.SuspendLayout();
            this.ultraTabPageControl2.SuspendLayout();
            this.ultraPanel3.ClientArea.SuspendLayout();
            this.ultraPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridResult)).BeginInit();
            this.ultraPanel2.ClientArea.SuspendLayout();
            this.ultraPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAboutTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFromDate)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            this.cms4Grid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).BeginInit();
            this.ultraTabControl1.SuspendLayout();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridExport)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.labeNameBank);
            this.ultraTabPageControl1.Controls.Add(this.LabeAcbank);
            this.ultraTabPageControl1.Controls.Add(this.labeTaxCode);
            this.ultraTabPageControl1.Controls.Add(this.LabeEmail);
            this.ultraTabPageControl1.Controls.Add(this.labeIdentificationNo);
            this.ultraTabPageControl1.Controls.Add(this.LabeOfficePhone);
            this.ultraTabPageControl1.Controls.Add(this.labePhoneHome);
            this.ultraTabPageControl1.Controls.Add(this.labePhone);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel29);
            this.ultraTabPageControl1.Controls.Add(this.Labe);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel31);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel32);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel33);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel34);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel35);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel36);
            this.ultraTabPageControl1.Controls.Add(this.LabeAddress);
            this.ultraTabPageControl1.Controls.Add(this.labeContacTitle);
            this.ultraTabPageControl1.Controls.Add(this.LabeDepartment);
            this.ultraTabPageControl1.Controls.Add(this.LabeSex);
            this.ultraTabPageControl1.Controls.Add(this.labeAccountingObjectName);
            this.ultraTabPageControl1.Controls.Add(this.labeAccountingObjectCode);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel18);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel13);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel14);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel15);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel16);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel17);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(907, 236);
            // 
            // labeNameBank
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            this.labeNameBank.Appearance = appearance1;
            this.labeNameBank.Location = new System.Drawing.Point(619, 183);
            this.labeNameBank.Name = "labeNameBank";
            this.labeNameBank.Size = new System.Drawing.Size(271, 25);
            this.labeNameBank.TabIndex = 36;
            this.labeNameBank.Visible = false;
            // 
            // LabeAcbank
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            this.LabeAcbank.Appearance = appearance2;
            this.LabeAcbank.Location = new System.Drawing.Point(619, 152);
            this.LabeAcbank.Name = "LabeAcbank";
            this.LabeAcbank.Size = new System.Drawing.Size(271, 25);
            this.LabeAcbank.TabIndex = 35;
            this.LabeAcbank.Visible = false;
            // 
            // labeTaxCode
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            this.labeTaxCode.Appearance = appearance3;
            this.labeTaxCode.Location = new System.Drawing.Point(618, 123);
            this.labeTaxCode.Name = "labeTaxCode";
            this.labeTaxCode.Size = new System.Drawing.Size(272, 23);
            this.labeTaxCode.TabIndex = 34;
            // 
            // LabeEmail
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            this.LabeEmail.Appearance = appearance4;
            this.LabeEmail.Location = new System.Drawing.Point(618, 92);
            this.LabeEmail.Name = "LabeEmail";
            this.LabeEmail.Size = new System.Drawing.Size(272, 25);
            this.LabeEmail.TabIndex = 33;
            // 
            // labeIdentificationNo
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            this.labeIdentificationNo.Appearance = appearance5;
            this.labeIdentificationNo.Location = new System.Drawing.Point(618, 61);
            this.labeIdentificationNo.Name = "labeIdentificationNo";
            this.labeIdentificationNo.Size = new System.Drawing.Size(278, 25);
            this.labeIdentificationNo.TabIndex = 32;
            // 
            // LabeOfficePhone
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            this.LabeOfficePhone.Appearance = appearance6;
            this.LabeOfficePhone.Location = new System.Drawing.Point(617, 32);
            this.LabeOfficePhone.Name = "LabeOfficePhone";
            this.LabeOfficePhone.Size = new System.Drawing.Size(279, 23);
            this.LabeOfficePhone.TabIndex = 31;
            // 
            // labePhoneHome
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            this.labePhoneHome.Appearance = appearance7;
            this.labePhoneHome.Location = new System.Drawing.Point(618, 3);
            this.labePhoneHome.Name = "labePhoneHome";
            this.labePhoneHome.Size = new System.Drawing.Size(272, 23);
            this.labePhoneHome.TabIndex = 30;
            // 
            // labePhone
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            this.labePhone.Appearance = appearance8;
            this.labePhone.Location = new System.Drawing.Point(164, 173);
            this.labePhone.Name = "labePhone";
            this.labePhone.Size = new System.Drawing.Size(272, 23);
            this.labePhone.TabIndex = 29;
            // 
            // ultraLabel29
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel29.Appearance = appearance9;
            this.ultraLabel29.Location = new System.Drawing.Point(465, 183);
            this.ultraLabel29.Name = "ultraLabel29";
            this.ultraLabel29.Size = new System.Drawing.Size(148, 25);
            this.ultraLabel29.TabIndex = 28;
            this.ultraLabel29.Text = "Tên ngân hàng              :";
            this.ultraLabel29.Visible = false;
            // 
            // Labe
            // 
            appearance10.BackColor = System.Drawing.Color.Transparent;
            this.Labe.Appearance = appearance10;
            this.Labe.Location = new System.Drawing.Point(465, 152);
            this.Labe.Name = "Labe";
            this.Labe.Size = new System.Drawing.Size(134, 25);
            this.Labe.TabIndex = 27;
            this.Labe.Text = "Tài Khoản Ngân hàng   :";
            this.Labe.Visible = false;
            // 
            // ultraLabel31
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel31.Appearance = appearance11;
            this.ultraLabel31.Location = new System.Drawing.Point(464, 123);
            this.ultraLabel31.Name = "ultraLabel31";
            this.ultraLabel31.Size = new System.Drawing.Size(135, 23);
            this.ultraLabel31.TabIndex = 26;
            this.ultraLabel31.Text = "Mã số thuế                    :";
            // 
            // ultraLabel32
            // 
            appearance12.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel32.Appearance = appearance12;
            this.ultraLabel32.Location = new System.Drawing.Point(466, 92);
            this.ultraLabel32.Name = "ultraLabel32";
            this.ultraLabel32.Size = new System.Drawing.Size(135, 25);
            this.ultraLabel32.TabIndex = 25;
            this.ultraLabel32.Text = "Email                            :";
            // 
            // ultraLabel33
            // 
            appearance13.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel33.Appearance = appearance13;
            this.ultraLabel33.Location = new System.Drawing.Point(464, 61);
            this.ultraLabel33.Name = "ultraLabel33";
            this.ultraLabel33.Size = new System.Drawing.Size(123, 25);
            this.ultraLabel33.TabIndex = 24;
            this.ultraLabel33.Text = "Số CMTND                   :";
            // 
            // ultraLabel34
            // 
            appearance14.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel34.Appearance = appearance14;
            this.ultraLabel34.Location = new System.Drawing.Point(463, 32);
            this.ultraLabel34.Name = "ultraLabel34";
            this.ultraLabel34.Size = new System.Drawing.Size(124, 23);
            this.ultraLabel34.TabIndex = 23;
            this.ultraLabel34.Text = "Điện thoại cơ  quan      :";
            // 
            // ultraLabel35
            // 
            appearance15.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel35.Appearance = appearance15;
            this.ultraLabel35.Location = new System.Drawing.Point(464, 3);
            this.ultraLabel35.Name = "ultraLabel35";
            this.ultraLabel35.Size = new System.Drawing.Size(123, 23);
            this.ultraLabel35.TabIndex = 22;
            this.ultraLabel35.Text = "Điện thoại nhà riêng    : ";
            // 
            // ultraLabel36
            // 
            appearance16.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel36.Appearance = appearance16;
            this.ultraLabel36.Location = new System.Drawing.Point(10, 173);
            this.ultraLabel36.Name = "ultraLabel36";
            this.ultraLabel36.Size = new System.Drawing.Size(123, 23);
            this.ultraLabel36.TabIndex = 21;
            this.ultraLabel36.Text = "Điện thoại di động       :";
            // 
            // LabeAddress
            // 
            appearance17.BackColor = System.Drawing.Color.Transparent;
            this.LabeAddress.Appearance = appearance17;
            this.LabeAddress.Location = new System.Drawing.Point(148, 141);
            this.LabeAddress.Name = "LabeAddress";
            this.LabeAddress.Size = new System.Drawing.Size(298, 27);
            this.LabeAddress.TabIndex = 20;
            // 
            // labeContacTitle
            // 
            appearance18.BackColor = System.Drawing.Color.Transparent;
            this.labeContacTitle.Appearance = appearance18;
            this.labeContacTitle.Location = new System.Drawing.Point(149, 113);
            this.labeContacTitle.Name = "labeContacTitle";
            this.labeContacTitle.Size = new System.Drawing.Size(285, 25);
            this.labeContacTitle.TabIndex = 17;
            // 
            // LabeDepartment
            // 
            appearance19.BackColor = System.Drawing.Color.Transparent;
            this.LabeDepartment.Appearance = appearance19;
            this.LabeDepartment.Location = new System.Drawing.Point(148, 90);
            this.LabeDepartment.Name = "LabeDepartment";
            this.LabeDepartment.Size = new System.Drawing.Size(286, 25);
            this.LabeDepartment.TabIndex = 16;
            // 
            // LabeSex
            // 
            appearance20.BackColor = System.Drawing.Color.Transparent;
            this.LabeSex.Appearance = appearance20;
            this.LabeSex.Location = new System.Drawing.Point(148, 61);
            this.LabeSex.Name = "LabeSex";
            this.LabeSex.Size = new System.Drawing.Size(286, 23);
            this.LabeSex.TabIndex = 15;
            // 
            // labeAccountingObjectName
            // 
            appearance21.BackColor = System.Drawing.Color.Transparent;
            this.labeAccountingObjectName.Appearance = appearance21;
            this.labeAccountingObjectName.Location = new System.Drawing.Point(149, 32);
            this.labeAccountingObjectName.Name = "labeAccountingObjectName";
            this.labeAccountingObjectName.Size = new System.Drawing.Size(285, 23);
            this.labeAccountingObjectName.TabIndex = 14;
            this.labeAccountingObjectName.Text = " ";
            // 
            // labeAccountingObjectCode
            // 
            appearance22.BackColor = System.Drawing.Color.Transparent;
            this.labeAccountingObjectCode.Appearance = appearance22;
            this.labeAccountingObjectCode.Location = new System.Drawing.Point(149, 3);
            this.labeAccountingObjectCode.Name = "labeAccountingObjectCode";
            this.labeAccountingObjectCode.Size = new System.Drawing.Size(285, 23);
            this.labeAccountingObjectCode.TabIndex = 13;
            // 
            // ultraLabel18
            // 
            appearance23.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel18.Appearance = appearance23;
            this.ultraLabel18.Location = new System.Drawing.Point(12, 143);
            this.ultraLabel18.Name = "ultraLabel18";
            this.ultraLabel18.Size = new System.Drawing.Size(124, 25);
            this.ultraLabel18.TabIndex = 12;
            this.ultraLabel18.Text = "Địa chỉ                         :";
            // 
            // ultraLabel13
            // 
            appearance24.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel13.Appearance = appearance24;
            this.ultraLabel13.Location = new System.Drawing.Point(11, 115);
            this.ultraLabel13.Name = "ultraLabel13";
            this.ultraLabel13.Size = new System.Drawing.Size(124, 25);
            this.ultraLabel13.TabIndex = 9;
            this.ultraLabel13.Text = "Chức vụ                       :";
            // 
            // ultraLabel14
            // 
            appearance25.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel14.Appearance = appearance25;
            this.ultraLabel14.Location = new System.Drawing.Point(12, 90);
            this.ultraLabel14.Name = "ultraLabel14";
            this.ultraLabel14.Size = new System.Drawing.Size(123, 25);
            this.ultraLabel14.TabIndex = 8;
            this.ultraLabel14.Text = "Phòng ban                   :";
            // 
            // ultraLabel15
            // 
            appearance26.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel15.Appearance = appearance26;
            this.ultraLabel15.Location = new System.Drawing.Point(10, 61);
            this.ultraLabel15.Name = "ultraLabel15";
            this.ultraLabel15.Size = new System.Drawing.Size(139, 23);
            this.ultraLabel15.TabIndex = 7;
            this.ultraLabel15.Text = "Giới tính                       :";
            // 
            // ultraLabel16
            // 
            appearance27.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel16.Appearance = appearance27;
            this.ultraLabel16.Location = new System.Drawing.Point(11, 32);
            this.ultraLabel16.Name = "ultraLabel16";
            this.ultraLabel16.Size = new System.Drawing.Size(123, 23);
            this.ultraLabel16.TabIndex = 6;
            this.ultraLabel16.Text = "Tên nhân viên             : ";
            // 
            // ultraLabel17
            // 
            appearance28.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel17.Appearance = appearance28;
            this.ultraLabel17.Location = new System.Drawing.Point(11, 3);
            this.ultraLabel17.Name = "ultraLabel17";
            this.ultraLabel17.Size = new System.Drawing.Size(123, 23);
            this.ultraLabel17.TabIndex = 5;
            this.ultraLabel17.Text = "Mã nhân viên              : ";
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.ultraPanel3);
            this.ultraTabPageControl2.Controls.Add(this.ultraPanel2);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(907, 236);
            // 
            // ultraPanel3
            // 
            // 
            // ultraPanel3.ClientArea
            // 
            this.ultraPanel3.ClientArea.Controls.Add(this.gridResult);
            this.ultraPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel3.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel3.Name = "ultraPanel3";
            this.ultraPanel3.Size = new System.Drawing.Size(907, 197);
            this.ultraPanel3.TabIndex = 7;
            // 
            // gridResult
            // 
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.gridResult.DisplayLayout.Appearance = appearance29;
            this.gridResult.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.gridResult.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance30.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance30.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance30.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance30.BorderColor = System.Drawing.SystemColors.Window;
            this.gridResult.DisplayLayout.GroupByBox.Appearance = appearance30;
            appearance31.ForeColor = System.Drawing.SystemColors.GrayText;
            this.gridResult.DisplayLayout.GroupByBox.BandLabelAppearance = appearance31;
            this.gridResult.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance32.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance32.BackColor2 = System.Drawing.SystemColors.Control;
            appearance32.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance32.ForeColor = System.Drawing.SystemColors.GrayText;
            this.gridResult.DisplayLayout.GroupByBox.PromptAppearance = appearance32;
            appearance33.BackColor = System.Drawing.SystemColors.Window;
            appearance33.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gridResult.DisplayLayout.Override.ActiveCellAppearance = appearance33;
            appearance34.BackColor = System.Drawing.SystemColors.Highlight;
            appearance34.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.gridResult.DisplayLayout.Override.ActiveRowAppearance = appearance34;
            this.gridResult.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.gridResult.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            this.gridResult.DisplayLayout.Override.CardAreaAppearance = appearance35;
            appearance36.BorderColor = System.Drawing.Color.Silver;
            appearance36.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.gridResult.DisplayLayout.Override.CellAppearance = appearance36;
            this.gridResult.DisplayLayout.Override.CellPadding = 0;
            appearance37.BackColor = System.Drawing.SystemColors.Control;
            appearance37.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance37.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance37.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance37.BorderColor = System.Drawing.SystemColors.Window;
            this.gridResult.DisplayLayout.Override.GroupByRowAppearance = appearance37;
            appearance38.TextHAlignAsString = "Left";
            this.gridResult.DisplayLayout.Override.HeaderAppearance = appearance38;
            this.gridResult.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance39.BackColor = System.Drawing.SystemColors.Window;
            appearance39.BorderColor = System.Drawing.Color.Silver;
            this.gridResult.DisplayLayout.Override.RowAppearance = appearance39;
            appearance40.BackColor = System.Drawing.SystemColors.ControlLight;
            this.gridResult.DisplayLayout.Override.TemplateAddRowAppearance = appearance40;
            this.gridResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridResult.Location = new System.Drawing.Point(0, 0);
            this.gridResult.Name = "gridResult";
            this.gridResult.Size = new System.Drawing.Size(907, 197);
            this.gridResult.TabIndex = 5;
            this.gridResult.DoubleClick += new System.EventHandler(this.gridResult_DoubleClick);
            // 
            // ultraPanel2
            // 
            // 
            // ultraPanel2.ClientArea
            // 
            this.ultraPanel2.ClientArea.Controls.Add(this.cbbAboutTime);
            this.ultraPanel2.ClientArea.Controls.Add(this.btnViewVoucher);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel22);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel21);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel20);
            this.ultraPanel2.ClientArea.Controls.Add(this.dteToDate);
            this.ultraPanel2.ClientArea.Controls.Add(this.dteFromDate);
            this.ultraPanel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraPanel2.Location = new System.Drawing.Point(0, 197);
            this.ultraPanel2.Name = "ultraPanel2";
            this.ultraPanel2.Size = new System.Drawing.Size(907, 39);
            this.ultraPanel2.TabIndex = 6;
            // 
            // cbbAboutTime
            // 
            this.cbbAboutTime.AutoSize = false;
            appearance41.BackColor = System.Drawing.SystemColors.Window;
            appearance41.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbAboutTime.DisplayLayout.Appearance = appearance41;
            this.cbbAboutTime.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbAboutTime.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance42.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance42.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance42.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance42.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAboutTime.DisplayLayout.GroupByBox.Appearance = appearance42;
            appearance43.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAboutTime.DisplayLayout.GroupByBox.BandLabelAppearance = appearance43;
            this.cbbAboutTime.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance44.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance44.BackColor2 = System.Drawing.SystemColors.Control;
            appearance44.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance44.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAboutTime.DisplayLayout.GroupByBox.PromptAppearance = appearance44;
            this.cbbAboutTime.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbAboutTime.DisplayLayout.MaxRowScrollRegions = 1;
            appearance45.BackColor = System.Drawing.SystemColors.Window;
            appearance45.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbAboutTime.DisplayLayout.Override.ActiveCellAppearance = appearance45;
            appearance46.BackColor = System.Drawing.SystemColors.Highlight;
            appearance46.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbAboutTime.DisplayLayout.Override.ActiveRowAppearance = appearance46;
            this.cbbAboutTime.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbAboutTime.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance47.BackColor = System.Drawing.SystemColors.Window;
            this.cbbAboutTime.DisplayLayout.Override.CardAreaAppearance = appearance47;
            appearance48.BorderColor = System.Drawing.Color.Silver;
            appearance48.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbAboutTime.DisplayLayout.Override.CellAppearance = appearance48;
            this.cbbAboutTime.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbAboutTime.DisplayLayout.Override.CellPadding = 0;
            appearance49.BackColor = System.Drawing.SystemColors.Control;
            appearance49.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance49.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance49.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance49.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAboutTime.DisplayLayout.Override.GroupByRowAppearance = appearance49;
            appearance50.TextHAlignAsString = "Left";
            this.cbbAboutTime.DisplayLayout.Override.HeaderAppearance = appearance50;
            this.cbbAboutTime.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbAboutTime.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance51.BackColor = System.Drawing.SystemColors.Window;
            appearance51.BorderColor = System.Drawing.Color.Silver;
            this.cbbAboutTime.DisplayLayout.Override.RowAppearance = appearance51;
            this.cbbAboutTime.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance52.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbAboutTime.DisplayLayout.Override.TemplateAddRowAppearance = appearance52;
            this.cbbAboutTime.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAboutTime.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbAboutTime.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbAboutTime.Location = new System.Drawing.Point(107, 6);
            this.cbbAboutTime.Name = "cbbAboutTime";
            this.cbbAboutTime.Size = new System.Drawing.Size(204, 22);
            this.cbbAboutTime.TabIndex = 7;
            this.cbbAboutTime.ValueChanged += new System.EventHandler(this.cbbAboutTime_ValueChanged);
            // 
            // btnViewVoucher
            // 
            this.btnViewVoucher.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnViewVoucher.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2010Button;
            this.btnViewVoucher.Location = new System.Drawing.Point(791, 6);
            this.btnViewVoucher.Name = "btnViewVoucher";
            this.btnViewVoucher.Size = new System.Drawing.Size(75, 23);
            this.btnViewVoucher.TabIndex = 6;
            this.btnViewVoucher.Text = "&Xem ...";
            this.btnViewVoucher.Click += new System.EventHandler(this.btnViewVoucher_Click);
            // 
            // ultraLabel22
            // 
            appearance53.BackColor = System.Drawing.Color.Transparent;
            appearance53.TextVAlignAsString = "Middle";
            this.ultraLabel22.Appearance = appearance53;
            this.ultraLabel22.Location = new System.Drawing.Point(520, 6);
            this.ultraLabel22.Name = "ultraLabel22";
            this.ultraLabel22.Size = new System.Drawing.Size(33, 22);
            this.ultraLabel22.TabIndex = 5;
            this.ultraLabel22.Text = "Đến";
            // 
            // ultraLabel21
            // 
            appearance54.BackColor = System.Drawing.Color.Transparent;
            appearance54.TextVAlignAsString = "Middle";
            this.ultraLabel21.Appearance = appearance54;
            this.ultraLabel21.Location = new System.Drawing.Point(317, 6);
            this.ultraLabel21.Name = "ultraLabel21";
            this.ultraLabel21.Size = new System.Drawing.Size(27, 22);
            this.ultraLabel21.TabIndex = 5;
            this.ultraLabel21.Text = "Từ";
            // 
            // ultraLabel20
            // 
            appearance55.BackColor = System.Drawing.Color.Transparent;
            appearance55.TextVAlignAsString = "Middle";
            this.ultraLabel20.Appearance = appearance55;
            this.ultraLabel20.Location = new System.Drawing.Point(9, 6);
            this.ultraLabel20.Name = "ultraLabel20";
            this.ultraLabel20.Size = new System.Drawing.Size(102, 22);
            this.ultraLabel20.TabIndex = 5;
            this.ultraLabel20.Text = "Khoảng thời gian";
            // 
            // dteToDate
            // 
            this.dteToDate.AutoSize = false;
            this.dteToDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteToDate.Location = new System.Drawing.Point(559, 6);
            this.dteToDate.Name = "dteToDate";
            this.dteToDate.Size = new System.Drawing.Size(144, 22);
            this.dteToDate.TabIndex = 4;
            this.dteToDate.ValueChanged += new System.EventHandler(this.dateTimeFrom_ValueChanged);
            // 
            // dteFromDate
            // 
            this.dteFromDate.AutoSize = false;
            this.dteFromDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteFromDate.Location = new System.Drawing.Point(350, 6);
            this.dteFromDate.Name = "dteFromDate";
            this.dteFromDate.Size = new System.Drawing.Size(144, 22);
            this.dteFromDate.TabIndex = 3;
            this.dteFromDate.ValueChanged += new System.EventHandler(this.dateTimeTo_ValueChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnAdd);
            this.panel1.Controls.Add(this.btnExportExcel);
            this.panel1.Controls.Add(this.btnDelete);
            this.panel1.Controls.Add(this.btnEdit);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(911, 52);
            this.panel1.TabIndex = 6;
            // 
            // btnAdd
            // 
            appearance56.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.btnAdd.Appearance = appearance56;
            this.btnAdd.Location = new System.Drawing.Point(10, 12);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 30);
            this.btnAdd.TabIndex = 14;
            this.btnAdd.Text = "Thêm";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnDelete
            // 
            appearance60.Image = global::Accounting.Properties.Resources.xoa1_xoa_du_lieu_ke_toan_khoi_database_;
            this.btnDelete.Appearance = appearance60;
            this.btnDelete.Location = new System.Drawing.Point(172, 12);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 30);
            this.btnDelete.TabIndex = 13;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnEdit
            // 
            appearance61.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            this.btnEdit.Appearance = appearance61;
            this.btnEdit.Location = new System.Drawing.Point(91, 12);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(75, 30);
            this.btnEdit.TabIndex = 12;
            this.btnEdit.Text = "Sửa";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(3, 14);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(123, 23);
            this.ultraLabel1.TabIndex = 0;
            // 
            // ultraLabel2
            // 
            this.ultraLabel2.Location = new System.Drawing.Point(3, 43);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(123, 23);
            this.ultraLabel2.TabIndex = 1;
            // 
            // ultraLabel3
            // 
            this.ultraLabel3.Location = new System.Drawing.Point(3, 101);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(124, 23);
            this.ultraLabel3.TabIndex = 2;
            // 
            // ultraLabel4
            // 
            this.ultraLabel4.Location = new System.Drawing.Point(4, 130);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(123, 25);
            this.ultraLabel4.TabIndex = 3;
            // 
            // ultraLabel5
            // 
            this.ultraLabel5.Location = new System.Drawing.Point(4, 161);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(124, 25);
            this.ultraLabel5.TabIndex = 4;
            // 
            // ultraLabel10
            // 
            this.ultraLabel10.Location = new System.Drawing.Point(551, 14);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(118, 23);
            this.ultraLabel10.TabIndex = 5;
            // 
            // ultraLabel9
            // 
            this.ultraLabel9.Location = new System.Drawing.Point(551, 43);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(118, 23);
            this.ultraLabel9.TabIndex = 6;
            // 
            // ultraLabel8
            // 
            this.ultraLabel8.Location = new System.Drawing.Point(551, 72);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(118, 23);
            this.ultraLabel8.TabIndex = 7;
            // 
            // ultraLabel7
            // 
            this.ultraLabel7.Location = new System.Drawing.Point(551, 101);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(118, 25);
            this.ultraLabel7.TabIndex = 8;
            // 
            // ultraLabel6
            // 
            this.ultraLabel6.Location = new System.Drawing.Point(551, 132);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(118, 25);
            this.ultraLabel6.TabIndex = 9;
            // 
            // ultraLabel12
            // 
            this.ultraLabel12.Location = new System.Drawing.Point(5, 70);
            this.ultraLabel12.Name = "ultraLabel12";
            this.ultraLabel12.Size = new System.Drawing.Size(123, 25);
            this.ultraLabel12.TabIndex = 20;
            this.ultraLabel12.Text = "Điện thoại : ";
            // 
            // ultraLabel11
            // 
            this.ultraLabel11.Location = new System.Drawing.Point(134, 70);
            this.ultraLabel11.Name = "ultraLabel11";
            this.ultraLabel11.Size = new System.Drawing.Size(225, 25);
            this.ultraLabel11.TabIndex = 21;
            // 
            // uGrid
            // 
            this.uGrid.ContextMenuStrip = this.cms4Grid;
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.AlignWithDataRows;
            this.uGrid.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.uGrid.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGrid.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGrid.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGrid.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.uGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid.Location = new System.Drawing.Point(0, 0);
            this.uGrid.Name = "uGrid";
            this.uGrid.Size = new System.Drawing.Size(911, 236);
            this.uGrid.TabIndex = 5;
            this.uGrid.Text = "uGrid";
            this.uGrid.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.uGrid_AfterSelectChange);
            this.uGrid.ClickCell += new Infragistics.Win.UltraWinGrid.ClickCellEventHandler(this.uGrid_ClickCell);
            this.uGrid.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGrid_DoubleClickRow);
            this.uGrid.MouseDown += new System.Windows.Forms.MouseEventHandler(this.uGrid_MouseDown);
            // 
            // cms4Grid
            // 
            this.cms4Grid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.tsmAdd,
            this.tsmEdit,
            this.tsmDelete,
            this.tmsReLoad});
            this.cms4Grid.Name = "cms4Grid";
            this.cms4Grid.Size = new System.Drawing.Size(148, 98);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(144, 6);
            // 
            // tsmAdd
            // 
            this.tsmAdd.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.tsmAdd.Name = "tsmAdd";
            this.tsmAdd.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.tsmAdd.Size = new System.Drawing.Size(147, 22);
            this.tsmAdd.Text = "Thêm";
            this.tsmAdd.Click += new System.EventHandler(this.tsmAdd_Click);
            // 
            // tsmEdit
            // 
            this.tsmEdit.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            this.tsmEdit.Name = "tsmEdit";
            this.tsmEdit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.tsmEdit.Size = new System.Drawing.Size(147, 22);
            this.tsmEdit.Text = "Sửa";
            this.tsmEdit.Click += new System.EventHandler(this.tsmEdit_Click);
            // 
            // tsmDelete
            // 
            this.tsmDelete.Image = global::Accounting.Properties.Resources.ubtnDelete;
            this.tsmDelete.Name = "tsmDelete";
            this.tsmDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.tsmDelete.Size = new System.Drawing.Size(147, 22);
            this.tsmDelete.Text = "Xóa";
            this.tsmDelete.Click += new System.EventHandler(this.tsmDelete_Click);
            // 
            // tmsReLoad
            // 
            this.tmsReLoad.Image = global::Accounting.Properties.Resources.ubtnReset;
            this.tmsReLoad.Name = "tmsReLoad";
            this.tmsReLoad.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.tmsReLoad.Size = new System.Drawing.Size(147, 22);
            this.tmsReLoad.Text = "Tải Lại";
            this.tmsReLoad.Click += new System.EventHandler(this.tmsReLoad_Click);
            this.tmsReLoad.DoubleClick += new System.EventHandler(this.tmsReLoad_Click);
            // 
            // ultraTabControl1
            // 
            this.ultraTabControl1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl2);
            this.ultraTabControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraTabControl1.Location = new System.Drawing.Point(0, 246);
            this.ultraTabControl1.Name = "ultraTabControl1";
            this.ultraTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.ultraTabControl1.Size = new System.Drawing.Size(911, 262);
            this.ultraTabControl1.TabIndex = 8;
            ultraTab4.TabPage = this.ultraTabPageControl1;
            ultraTab4.Text = "1. Thông tin chung";
            ultraTab5.TabPage = this.ultraTabPageControl2;
            ultraTab5.Text = "2. Chứng từ";
            this.ultraTabControl1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab4,
            ultraTab5});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(907, 236);
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.uGridExport);
            this.ultraPanel1.ClientArea.Controls.Add(this.uGrid);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraSplitter1);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraTabControl1);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 52);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(911, 508);
            this.ultraPanel1.TabIndex = 9;
            // 
            // ultraSplitter1
            // 
            this.ultraSplitter1.BackColor = System.Drawing.SystemColors.Control;
            this.ultraSplitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraSplitter1.Location = new System.Drawing.Point(0, 236);
            this.ultraSplitter1.Name = "ultraSplitter1";
            this.ultraSplitter1.RestoreExtent = 224;
            this.ultraSplitter1.Size = new System.Drawing.Size(911, 10);
            this.ultraSplitter1.TabIndex = 36;
            // 
            // uGridExport
            // 
            this.uGridExport.ContextMenuStrip = this.cms4Grid;
            this.uGridExport.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGridExport.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGridExport.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGridExport.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGridExport.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.uGridExport.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGridExport.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGridExport.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGridExport.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGridExport.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGridExport.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGridExport.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGridExport.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.uGridExport.Location = new System.Drawing.Point(513, -40);
            this.uGridExport.Name = "uGridExport";
            this.uGridExport.Size = new System.Drawing.Size(368, 297);
            this.uGridExport.TabIndex = 66;
            this.uGridExport.Text = "ultraGrid1";
            this.uGridExport.Visible = false;
            // 
            // btnExportExcel
            // 
            appearance57.BackColor = System.Drawing.Color.DarkRed;
            appearance57.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance57.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance57.Image = global::Accounting.Properties.Resources.Excel1;
            this.btnExportExcel.Appearance = appearance57;
            this.btnExportExcel.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance58.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance58.BorderAlpha = Infragistics.Win.Alpha.Opaque;
            this.btnExportExcel.HotTrackAppearance = appearance58;
            this.btnExportExcel.Location = new System.Drawing.Point(253, 12);
            this.btnExportExcel.Name = "btnExportExcel";
            appearance59.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.btnExportExcel.PressedAppearance = appearance59;
            this.btnExportExcel.Size = new System.Drawing.Size(75, 30);
            this.btnExportExcel.TabIndex = 65;
            this.btnExportExcel.Text = "Kết xuất";
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // FAccountingObjectEmployee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(911, 560);
            this.Controls.Add(this.ultraPanel1);
            this.Controls.Add(this.panel1);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FAccountingObjectEmployee";
            this.Text = "Nhân viên";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FAccountingObjectEmployee_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FAccountingObjectEmployee_FormClosed);
            this.ultraTabPageControl1.ResumeLayout(false);
            this.ultraTabPageControl2.ResumeLayout(false);
            this.ultraPanel3.ClientArea.ResumeLayout(false);
            this.ultraPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridResult)).EndInit();
            this.ultraPanel2.ClientArea.ResumeLayout(false);
            this.ultraPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbAboutTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFromDate)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            this.cms4Grid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).EndInit();
            this.ultraTabControl1.ResumeLayout(false);
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridExport)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel12;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.Misc.UltraLabel labeNameBank;
        private Infragistics.Win.Misc.UltraLabel LabeAcbank;
        private Infragistics.Win.Misc.UltraLabel labeTaxCode;
        private Infragistics.Win.Misc.UltraLabel LabeEmail;
        private Infragistics.Win.Misc.UltraLabel labeIdentificationNo;
        private Infragistics.Win.Misc.UltraLabel LabeOfficePhone;
        private Infragistics.Win.Misc.UltraLabel labePhoneHome;
        private Infragistics.Win.Misc.UltraLabel labePhone;
        private Infragistics.Win.Misc.UltraLabel ultraLabel29;
        private Infragistics.Win.Misc.UltraLabel Labe;
        private Infragistics.Win.Misc.UltraLabel ultraLabel31;
        private Infragistics.Win.Misc.UltraLabel ultraLabel32;
        private Infragistics.Win.Misc.UltraLabel ultraLabel33;
        private Infragistics.Win.Misc.UltraLabel ultraLabel34;
        private Infragistics.Win.Misc.UltraLabel ultraLabel35;
        private Infragistics.Win.Misc.UltraLabel ultraLabel36;
        private Infragistics.Win.Misc.UltraLabel LabeAddress;
        private Infragistics.Win.Misc.UltraLabel labeContacTitle;
        private Infragistics.Win.Misc.UltraLabel LabeDepartment;
        private Infragistics.Win.Misc.UltraLabel LabeSex;
        private Infragistics.Win.Misc.UltraLabel labeAccountingObjectName;
        private Infragistics.Win.Misc.UltraLabel labeAccountingObjectCode;
        private Infragistics.Win.Misc.UltraLabel ultraLabel18;
        private Infragistics.Win.Misc.UltraLabel ultraLabel13;
        private Infragistics.Win.Misc.UltraLabel ultraLabel14;
        private Infragistics.Win.Misc.UltraLabel ultraLabel15;
        private Infragistics.Win.Misc.UltraLabel ultraLabel16;
        private Infragistics.Win.Misc.UltraLabel ultraLabel17;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.Misc.UltraButton btnAdd;
        private Infragistics.Win.Misc.UltraButton btnDelete;
        private Infragistics.Win.Misc.UltraButton btnEdit;
        private System.Windows.Forms.ContextMenuStrip cms4Grid;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripMenuItem tsmEdit;
        private System.Windows.Forms.ToolStripMenuItem tsmDelete;
        private System.Windows.Forms.ToolStripMenuItem tmsReLoad;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteToDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteFromDate;
        private Infragistics.Win.Misc.UltraSplitter ultraSplitter1;
        private Infragistics.Win.Misc.UltraPanel ultraPanel3;
        private Infragistics.Win.UltraWinGrid.UltraGrid gridResult;
        private Infragistics.Win.Misc.UltraPanel ultraPanel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel20;
        private Infragistics.Win.Misc.UltraLabel ultraLabel22;
        private Infragistics.Win.Misc.UltraLabel ultraLabel21;
        private Infragistics.Win.Misc.UltraButton btnViewVoucher;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAboutTime;
        public Infragistics.Win.Misc.UltraButton btnExportExcel;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridExport;
    }
}