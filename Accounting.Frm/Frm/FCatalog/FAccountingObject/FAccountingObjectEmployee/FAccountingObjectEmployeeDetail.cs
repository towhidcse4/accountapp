﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Accounting.TextMessage;
using System.Text.RegularExpressions;
using System.Threading;
using System.Globalization;
using System.ComponentModel;
using System.Linq;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Accounting.Core;

namespace Accounting
{
    public partial class FAccountingObjectEmployeeDetail : DialogForm
    {
        #region khai báo
        private IAccountingObjectService _IAccountingObjectService;
        //private IBankAccountDetailService _IBankAccountDetailService;
        private IDepartmentService _IDepartmentService;
        //private IAccountingObjectGroupService _IAccountingObjectGroupService;
        //private IPaymentClauseService _IPaymentClauseService;
        private IAccountingObjectBankAccountService _IAccountingObjectBankAccountService;
        private IGenCodeService _IGenCodeService;
        AccountingObject _Select = new AccountingObject();
        bool isFirst = false;
        BindingList<AccountingObjectBankAccount> dsAccountingObjectBankAccount = new BindingList<AccountingObjectBankAccount>();
        public AccountingObject SelectAccounting
        {
            get { return _Select; }
            set { _Select = value; }
        }
        private Guid _id;
        public Guid Id
        {
            get { return _id; }
            set { _id = value; }
        }
        bool IsAdd = true;
        public static bool isClose = true;
        public static string AccounttingObjectCode;
        #endregion
        #region Khoi tạo
        public FAccountingObjectEmployeeDetail()
        {
            #region Khởi tạo giá trị ban đấu
            InitializeComponent();
            this.CenterToParent();
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Text = "Thêm chi tiết nhân viên";
            //cbbObjectType.SelectedIndex = 0;
            //txtEmployeeBirthday.DateTime = DateTime.Now;
            //txtIssueDate.DateTime = DateTime.Now;
            CbbContactSex.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            #endregion
            #region Thiết lập ban đầu cho Form
            _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();
            //_IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            _IDepartmentService = IoC.Resolve<IDepartmentService>();
            //_IAccountingObjectGroupService = IoC.Resolve<IAccountingObjectGroupService>();
            //_IPaymentClauseService = IoC.Resolve<IPaymentClauseService>();
            _IAccountingObjectBankAccountService = IoC.Resolve<IAccountingObjectBankAccountService>();
            _IGenCodeService = IoC.Resolve<IGenCodeService>();
            txtAccounttingObjectCode.Text = Utils.TaoMaChungTu(_IGenCodeService.getGenCode(101));
            Utils.ClearCacheByType<AccountingObject>();
            Utils.ClearCacheByType<AccountingObjectBankAccount>();
            InitializeGUI();

            uGrid.DataSource = dsAccountingObjectBankAccount;

            //config
            uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            uGrid.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
            uGrid.DisplayLayout.Override.TemplateAddRowPrompt = "Bấm vào đây để thêm mới....";
            uGrid.DisplayLayout.Override.SpecialRowSeparator = Infragistics.Win.UltraWinGrid.SpecialRowSeparator.None;
            uGrid.DisplayLayout.Override.AllowMultiCellOperations = Infragistics.Win.UltraWinGrid.AllowMultiCellOperation.All;
            //Utils.ConfigGrid(uGrid, ConstDatabase.AccountingObjectBankAccount_TableName, false);
            Utils.ConfigGrid(uGrid, ConstDatabase.AccountingObjectBankAccount_TableName, new List<TemplateColumn>(), true);
            //uGrid.DisplayLayout.Bands[0].Columns["IsSelect"].Hidden = true;
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            #endregion
        }
        public FAccountingObjectEmployeeDetail(AccountingObject temp)
        {
            //Sửa
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            this.CenterToParent();
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Text = "Sửa thông tin chi tiết của nhân viên";
            #endregion

            #region Thiết lập ban đầu cho Form
            _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();
            //_IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            _IDepartmentService = IoC.Resolve<IDepartmentService>();
            //_IAccountingObjectGroupService = IoC.Resolve<IAccountingObjectGroupService>();
            //_IPaymentClauseService = IoC.Resolve<IPaymentClauseService>();
            _IAccountingObjectBankAccountService = IoC.Resolve<IAccountingObjectBankAccountService>();
            _IGenCodeService = IoC.Resolve<IGenCodeService>();
            _Select = temp.CloneObject();
            _Select.BankAccounts = temp.BankAccounts;
            IsAdd = false;
            isFirst = true;
            txtAccounttingObjectCode.Enabled = false;
            CbbContactSex.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            //Khai báo các webservices
            _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();

            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            Utils.ClearCacheByType<AccountingObject>();
            Utils.ClearCacheByType<AccountingObjectBankAccount>();
            InitializeGUI();



            #region Fill dữ liệu Obj vào control
            ObjandGUI(_Select, false);
            #endregion
            //config
            uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            uGrid.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
            uGrid.DisplayLayout.Override.TemplateAddRowPrompt = "Bấm vào đây để thêm mới....";
            uGrid.DisplayLayout.Override.SpecialRowSeparator = Infragistics.Win.UltraWinGrid.SpecialRowSeparator.None;
            uGrid.DisplayLayout.Override.AllowMultiCellOperations = Infragistics.Win.UltraWinGrid.AllowMultiCellOperation.All;
            //Utils.ConfigGrid(uGrid, ConstDatabase.AccountingObjectBankAccount_TableName, false);
            Utils.ConfigGrid(uGrid, ConstDatabase.AccountingObjectBankAccount_TableName, new List<TemplateColumn>(), true);
            //uGrid.DisplayLayout.Bands[0].Columns["IsSelect"].Width = 10;
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            #endregion

        }
        private void InitializeGUI()
        {
            //if (!IsAdd)
            //{
            //    dsAccountingObjectBankAccount.Clear();
            //    dsAccountingObjectBankAccount.AddRange(_Select.BankAccounts);  // = (List<AccountingObjectBankAccount>)Utils.CloneObject(_Select.BankAccounts);
            //}
            //else
            //{
            //    dsAccountingObjectBankAccount = new BindingList<AccountingObjectBankAccount>(Utils.CloneObject(_Select.BankAccounts)).ToList();
            //}
            //cbbDepartment.DataSource = _IDepartmentService.GetAll().Where(p => p.IsActive == true).ToList().OrderBy(p=>p.DepartmentCode).ToList();
            //cbbDepartment.DataSource = _IDepartmentService.GetAll_ByIsActive_OrderByDepartmentCode(true);//thành duy đã sửa chỗ này
            //cbbDepartment.DisplayMember = "DepartmentName";
            //Utils.ConfigGrid(cbbDepartment, ConstDatabase.Department_TableName);
            cbbDepartment.DataSource = Utils.ListDepartment;
            cbbDepartment.DisplayMember = "DepartmentName";
            //cbbDepartment.ValueMember = "ID";
            Utils.ConfigGrid<Department>(this, cbbDepartment, ConstDatabase.Department_TableName, "ParentID", "IsParentNode");

            if (Utils.ListSystemOption.Where(x => x.Code == "TL_TheoHeSoLuong").First().Data == "1")
            {
                txtAgreementSalary.ReadOnly = true;
                txtSalaryCoefficient.ReadOnly = false;
            }
            else
            {
                txtAgreementSalary.ReadOnly = false;
                txtSalaryCoefficient.ReadOnly = true;
            }


            //this.ConfigCombo(Utils.ListDepartment, cbbDepartment, "DepartmentCode", "ID");
            txtAgreementSalary.FormatNumberic(ConstDatabase.Format_TienVND);
        }
        #endregion
        #region Utils
        /// <summary>
        /// get hoặc set một đối tượng vào giao diện xử lý
        /// </summary>
        /// <param name="input">đối tượng</param>
        /// <param name="isGet">True: get giá trị từ control gán vào đối tượng| False: set giá trị đối tượng vào control</param>
        /// <returns></returns>
        AccountingObject ObjandGUI(AccountingObject input, bool isGet)
        {
            if (isGet)
            {

                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();  //Thêm mới

                input.AccountingObjectCode = txtAccounttingObjectCode.Text;
                input.AccountingObjectName = txtAccounttingObjectName.Text;
                string thisEmployeeBirthday = txtEmployeeBirthday.Text;
                DateTime? date = thisEmployeeBirthday.StringToDateTime();
                input.EmployeeBirthday = date;
                input.Address = txtAddress.Text;
                input.Tel = txtTel.Text;
                //input.BankAccount = txtBankAccount.Text;
                input.Email = txtEmail.Text;
                //input.BankName = txtBankName.Text;
                input.TaxCode = txtTaxCode.Text;
                input.ContactTitle = txtContactTitle.Text;
                if (CbbContactSex.SelectedIndex != 2)
                {
                    input.ContactSex = CbbContactSex.SelectedIndex;
                }
                else
                {
                    input.ContactSex = null;
                }
                if (cbbObjectType.SelectedItem != null)
                {
                    input.ObjectType = int.Parse(cbbObjectType.SelectedItem.DataValue.ToString());
                    if (cbbObjectType.SelectedIndex != 3)
                    {
                        input.ObjectType = cbbObjectType.SelectedIndex;
                    }
                    else
                    {
                        input.ObjectType = null;
                    }
                }
                input.ContactHomeTel = txtContactHomeTel.Text;
                input.ContactOfficeTel = txtContactOfficeTel.Text;
                //if(FAccountingObjectBankAccount.dsAccountingObjectBankAccount.Count > 0)
                //{
                //    foreach (AccountingObjectBankAccount item in FAccountingObjectBankAccount.dsAccountingObjectBankAccount) item.AccountingObjectID = input.ID;
                //    input.BankAccounts = FAccountingObjectBankAccount.dsAccountingObjectBankAccount;
                //}               
                input.IsEmployee = true;
                input.IdentificationNo = txtIdentificationNo.Text;
                string thisIssueBy = txtIssueDate.Text;
                DateTime? IssueByUp = thisIssueBy.StringToDateTime();
                input.IssueDate = IssueByUp;
                input.IssueBy = txtIssueBy.Text;
                //input.IssueDate = DateTime.Parse(txtIssueDate.Text);
                //Phòng Ban       
                //Department temp1 = (Department)Utils.getSelectCbbItem(cbbDepartment);
                //if (temp1 == null) input.DepartmentID = null;
                //else input.DepartmentID = temp1.ID;
                if (cbbDepartment.SelectedRow != null && !string.IsNullOrEmpty(cbbDepartment.Text))
                {
                    input.Department = (Department)cbbDepartment.SelectedRow.ListObject ?? null;
                    input.DepartmentID = input.Department.ID;
                }
                else
                {
                    input.Department = null;
                    input.DepartmentID = null;
                }
                input.IsInsured = chkIsInsured.CheckState == CheckState.Checked ? true : false;
                input.IsLabourUnionFree = chkIsLabourUnionFree.CheckState == CheckState.Checked ? true : false;
                if (txtAgreementSalary.Text == "")
                {
                    input.FamilyDeductionAmount = 0;
                }
                else
                {
                    input.FamilyDeductionAmount = decimal.Parse(txtAgreementSalary.Text.ToString());
                }
                input.IsActive = ckeIsActive.CheckState == CheckState.Checked ? true : false;
                input.IsUnofficialStaff = chkIsUnofficialStaff.CheckState == CheckState.Checked ? true : false;
                input.NumberOfDependent = string.IsNullOrEmpty(txtNumberOfDependent.Text) ? 0 : int.Parse(txtNumberOfDependent.Text);
                if (txtSalaryCoefficient.Text.IsNullOrEmpty())
                {
                    input.SalaryCoefficient = 0;
                }
                else
                {
                    input.SalaryCoefficient = decimal.Parse(txtSalaryCoefficient.Value.ToString());
                }
                if (txtInsuranceSalary.Text.IsNullOrEmpty())
                {
                    input.InsuranceSalary = 0;
                }
                else
                {
                    input.InsuranceSalary = decimal.Parse(txtInsuranceSalary.Value.ToString());
                }
                if (txtAgreementSalary.Text.IsNullOrEmpty())
                {
                    input.AgreementSalary = 0;
                }
                else
                {
                    input.AgreementSalary = decimal.Parse(txtAgreementSalary.Value.ToString());
                }
                input.BankAccounts = dsAccountingObjectBankAccount;
                foreach (var item in input.BankAccounts)
                {
                    item.AccountingObjectID = input.ID;
                }
                //input.BankAccounts.Clear();
                //for (int i = 0; i < uGrid.Rows.Count; i++)
                //{
                //    var BankAccount = uGrid.Rows[i].Cells["BankAccount"].Value;
                //    var BankName = uGrid.Rows[i].Cells["BankName"].Value;
                //    var BankBranchName = uGrid.Rows[i].Cells["BankBranchName"].Value;
                //    var AccountHolderName = uGrid.Rows[i].Cells["AccountHolderName"].Value;
                //    input.BankAccounts.Add(new AccountingObjectBankAccount
                //    {
                //        AccountingObjectID = input.ID,
                //        BankAccount = BankAccount.ToString(),
                //        BankName = BankName.ToString(),
                //        BankBranchName = BankBranchName.ToString(),
                //        AccountHolderName = AccountHolderName.ToString(),
                //    });

                //}
            }
            else
            {
                if (input.BankAccounts.Count > 0)
                    dsAccountingObjectBankAccount = new BindingList<AccountingObjectBankAccount>(input.BankAccounts);
                txtAccounttingObjectCode.Text = input.AccountingObjectCode;
                txtAccounttingObjectName.Text = input.AccountingObjectName;
                txtEmployeeBirthday.Text = input.EmployeeBirthday.ToString();
                txtAddress.Text = input.Address;
                txtTel.Text = input.Tel;
                txtEmail.Text = input.Email;
                uGrid.DataSource = dsAccountingObjectBankAccount;
                //txtBankName.Text = input.BankName;
                txtTaxCode.Text = input.TaxCode;
                txtContactTitle.Text = input.ContactTitle;
                CbbContactSex.SelectedIndex = input.ContactSex ?? 2;
                txtContactHomeTel.Text = input.ContactHomeTel;
                txtContactOfficeTel.Text = input.ContactOfficeTel;
                txtIdentificationNo.Text = input.IdentificationNo;
                txtIssueDate.Text = input.IssueDate.ToString();
                txtIssueBy.Text = input.IssueBy;
                //Phòng Ban
                foreach (var item in cbbDepartment.Rows)
                {
                    if ((item.ListObject as Department).ID == input.DepartmentID) cbbDepartment.SelectedRow = item;
                }
                chkIsInsured.CheckState = input.IsInsured ? CheckState.Checked : CheckState.Unchecked;
                chkIsLabourUnionFree.CheckState = input.IsLabourUnionFree ? CheckState.Checked : CheckState.Unchecked;
                if (txtAgreementSalary.Text != null)
                {
                    txtAgreementSalary.Text = input.FamilyDeductionAmount.ToString();
                }
                cbbObjectType.SelectedIndex = input.ObjectType ?? 3;
                ckeIsActive.CheckState = input.IsActive ? CheckState.Checked : CheckState.Unchecked;
                chkIsUnofficialStaff.CheckState = input.IsUnofficialStaff ? CheckState.Checked : CheckState.Unchecked;
                txtNumberOfDependent.Text = input.NumberOfDependent.ToString();
                txtAgreementSalary.Text = input.AgreementSalary.ToString();
                txtInsuranceSalary.Text = input.InsuranceSalary.ToString();
                txtSalaryCoefficient.Text = input.SalaryCoefficient.ToString();
            }
            return input;
        }
        #region Check
        bool CheckError()
        {
            bool kq = true;
            //Check Error Chung

            if (!txtTaxCode.Text.Equals(""))
            {
                if (!Core.Utils.CheckMST(txtTaxCode.Text))
                {
                    MSG.Error(resSystem.MSG_System_44);
                    txtTaxCode.Focus();
                    return false;
                }
            }

            //Check Error Riêng từng Form
            if (string.IsNullOrEmpty(txtAccounttingObjectCode.Text) || string.IsNullOrEmpty(txtAccounttingObjectName.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }

            if (cbbDepartment.Value == null)
            {
                MSG.Error("Bạn chưa nhập thông tin phòng ban");
                return false;
            }
            bool checkDp = true;
            if (cbbDepartment.Rows != null && cbbDepartment.Rows.Count != 0)
            {
                if (!cbbDepartment.Rows.Any(n => (n.ListObject as Department).DepartmentName == cbbDepartment.Text))
                {
                    MSG.Error("Thông tin phòng ban không có trong danh mục");
                    return false;
                }
                var row = cbbDepartment.SelectedRow;
                if (row.Cells["IsParentNode"].Value != null && (bool)row.Cells["IsParentNode"].Value == true)
                {
                    cbbDepartment.Value = null;
                    MSG.Error(resSystem.MSG_System_29);
                    return false;
                }
            }

            string patent = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
              @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
              @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

            Regex objNotWholePattern = new Regex(patent);
            // Bắt kiếu số cho loại sổ lưu

            Match Email = objNotWholePattern.Match(txtEmail.Text);
            if (!txtEmail.Text.Equals(""))
            {
                if (!txtEmail.Text.Equals(""))
                {
                    if (!Email.Success)
                    {
                        MSG.Error(string.Format(resSystem.MSG_System_58, "thông tin nhân viên"));
                        txtEmail.Focus();
                        return false;

                    }
                }
            }


            decimal val;
            bool validDecimal = decimal.TryParse(txtAgreementSalary.Text, out val);
            //if (txtFamilyDeductionAmunt.Text == null)
            //{

            //}
            if (txtAgreementSalary.Text != null)
            {
                if (!validDecimal && !txtAgreementSalary.Text.Equals(""))
                {
                    MSG.Error(resSystem.MSG_Catalog_AccountingObject2);
                    txtAgreementSalary.Focus();
                    return false;
                }
            }

            //chek Code
            //List<string> list = _IAccountingObjectService.Query.Select(p => p.AccountingObjectCode).ToList();
            List<string> list = _IAccountingObjectService.GetListAccountingObjectCode();//thành duy đã sửa chỗ này
            foreach (var item in list)
            {
                if (item == txtAccounttingObjectCode.Text && IsAdd)
                {
                    MSG.Error("Mã : " + item + " Đã tồn tại ");
                    txtAccounttingObjectCode.Focus();
                    return false;
                }

            }
            if (dsAccountingObjectBankAccount.Any(n => n.BankAccount.IsNullOrEmpty()))
            {
                MSG.Error("Chưa nhập số tài khoản ngân hàng");
                return false;
            }
            return kq;
        }
        #endregion
        #endregion
        #region Event
        private void btnSave_Click(object sender, EventArgs e)
        {
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion

            #region Fill dữ liệu control vào obj
            AccountingObject temp = IsAdd ? new AccountingObject() : _IAccountingObjectService.Getbykey(_Select.ID);

            if (IsAdd)
            {
                temp = ObjandGUI(temp, true);

            }
            else
            {
                _IAccountingObjectService.UnbindSession(temp);
                temp = ObjandGUI(temp.CloneObject(), true);
            }

            #endregion

            #region Thao tác CSDL
            try
            {
                //dsAccountingObjectBankAccount = ((BindingList<AccountingObjectBankAccount>)uGrid.DataSource).ToList();
                _IAccountingObjectService.BeginTran();
                if (IsAdd)
                {
                    _IAccountingObjectService.CreateNew(temp);
                    _IGenCodeService.UpdateGenCodeCatalog(101, txtAccounttingObjectCode.Text);
                }
                else _IAccountingObjectService.Update(temp);
                Utils.IAccountingObjectBankAccountService.BeginTran();
                foreach (var item in Utils.ListAccountingObjectBank.Where(n=>n.AccountingObjectID== temp.ID).ToList())
                {
                    if(!temp.BankAccounts.Any(n=>n.ID == item.ID))
                    {
                        Utils.IAccountingObjectBankAccountService.Delete(item);
                    }
                }
                //thao tac vao bang AccountingObjectBankAccount
                //foreach (AccountingObjectBankAccount item in FAccountingObjectBankAccount.dsAccountingObjectBankAccount) item.AccountingObjectID = temp.ID;
                //List<AccountingObjectBankAccount> AccountingObjectBankAccounts = _IAccountingObjectBankAccountService.Query.Where(k => k.AccountingObjectID == temp.ID).ToList();
                //List<AccountingObjectBankAccount> AccountingObjectBankAccounts = _IAccountingObjectBankAccountService.GetByAccountingObjectID(temp.ID);//thành duy đã sửa chỗ này
                //try
                //{
                //    foreach (AccountingObjectBankAccount item in AccountingObjectBankAccounts)
                //    {
                //        _IAccountingObjectBankAccountService.Delete(item);
                //        _IAccountingObjectBankAccountService.CommitChanges();
                //    }
                //}
                //catch(Exception ex)
                //{
                //    MSG.Warning(ex.ToString());
                //    return;
                //}
                //foreach (AccountingObjectBankAccount item in FAccountingObjectBankAccount.dsAccountingObjectBankAccount)
                //{
                //    item.ID = Guid.NewGuid();
                //    _IAccountingObjectBankAccountService.CreateNew(item);
                //} 
                try
                {
                    Utils.IAccountingObjectBankAccountService.CommitTran();
                    _IAccountingObjectService.CommitTran();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                //temp.BankAccounts.Clear();
                //temp.BankAccounts = Utils.CloneObject(FAccountingObjectBankAccount.dsAccountingObjectBankAccount);
                Utils.ClearCacheByType<AccountingObject>();
                Utils.ClearCacheByType<AccountingObjectBankAccount>();
                Id = temp.ID;
                Post_Customers(temp); // Add by Hautv
                DialogResult = DialogResult.OK;

            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(resSystem.MSG_Catalog5, "nhân viên"));
                _IAccountingObjectService.RolbackTran();
                return;
            }


            #endregion

            #region xử lý form, kết thúc form
            AccounttingObjectCode = txtAccounttingObjectCode.Text;
            this.SelectAccounting = temp;
            Utils.ClearCacheByType<AccountingObject>();
            BindingList<AccountingObject> lst = Utils.ListAccountingObject;
            //Utils.ListAccountingObject.Clear();
            //Utils.IAccountingObjectService.GetAccountingObjects(2, true).AddToBindingList(Utils.ListEmployee);
            this.Close();
            isClose = false;

            #endregion
        }

        private void btnSaveContinue_Click(object sender, EventArgs e)
        {
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion

            #region Fill dữ liệu control vào obj  
            AccountingObject temp = IsAdd ? new AccountingObject() : _IAccountingObjectService.Getbykey(_Select.ID);

            if (IsAdd)//đoạn dưới trungnq copy ở nút lưu xuống
            {
                temp = ObjandGUI(temp, true);

            }
            else
            {
                _IAccountingObjectService.UnbindSession(temp);
                temp = ObjandGUI(temp.CloneObject(), true);
            }

            #endregion

            #region Thao tác CSDL
            try
            {
                //dsAccountingObjectBankAccount = ((BindingList<AccountingObjectBankAccount>)uGrid.DataSource).ToList();
                _IAccountingObjectService.BeginTran();
                if (IsAdd)
                {
                    //_IAccountingObjectService.UnbindSession(temp);
                    _IAccountingObjectService.CreateNew(temp);
                    _IGenCodeService.UpdateGenCodeCatalog(101, txtAccounttingObjectCode.Text);
                }
                else _IAccountingObjectService.Update(temp);
                Utils.IAccountingObjectBankAccountService.BeginTran();
                foreach (var item in Utils.ListAccountingObjectBank.Where(n => n.AccountingObjectID == temp.ID).ToList())
                {
                    if (!temp.BankAccounts.Any(n => n.ID == item.ID))
                    {
                        Utils.IAccountingObjectBankAccountService.Delete(item);
                    }
                }
                //thao tac vao bang AccountingObjectBankAccount
                //foreach (AccountingObjectBankAccount item in FAccountingObjectBankAccount.dsAccountingObjectBankAccount) item.AccountingObjectID = temp.ID;
                //List<AccountingObjectBankAccount> AccountingObjectBankAccounts = _IAccountingObjectBankAccountService.Query.Where(k => k.AccountingObjectID == temp.ID).ToList();
                //List<AccountingObjectBankAccount> AccountingObjectBankAccounts = _IAccountingObjectBankAccountService.GetByAccountingObjectID(temp.ID);//thành duy đã sửa chỗ này
                //try
                //{
                //    foreach (AccountingObjectBankAccount item in AccountingObjectBankAccounts)
                //    {
                //        _IAccountingObjectBankAccountService.Delete(item);
                //        _IAccountingObjectBankAccountService.CommitChanges();
                //    }
                //}
                //catch(Exception ex)
                //{
                //    MSG.Warning(ex.ToString());
                //    return;
                //}
                //foreach (AccountingObjectBankAccount item in FAccountingObjectBankAccount.dsAccountingObjectBankAccount)
                //{
                //    item.ID = Guid.NewGuid();
                //    _IAccountingObjectBankAccountService.CreateNew(item);
                //} 
                try
                {
                    Utils.IAccountingObjectBankAccountService.CommitTran();
                    
                    _IAccountingObjectService.CommitTran();
                    Utils.IAccountingObjectBankAccountService.UnbindSession(temp.BankAccount);//trungnq
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                //temp.BankAccounts.Clear();
                //temp.BankAccounts = Utils.CloneObject(FAccountingObjectBankAccount.dsAccountingObjectBankAccount);
                Utils.ClearCacheByType<AccountingObject>();
                Utils.ClearCacheByType<AccountingObjectBankAccount>();
                Id = temp.ID;
                Post_Customers(temp); //Add by Hautv
                #endregion

                #region xử lý form, reset form            
                AccounttingObjectCode = txtAccounttingObjectCode.Text;
                this.SelectAccounting = temp;
                Utils.ClearCacheByType<AccountingObject>();
                Utils.ClearCacheByType<AccountingObjectBankAccount>();
                BindingList<AccountingObject> lst = Utils.ListAccountingObject;

                // reset form
                IsAdd = true;
                isClose = false;
                _Select = new AccountingObject();
                ObjandGUI(_Select, false);
                uGrid.DataSource = new BindingList<AccountingObjectBankAccount>();
                Utils.ClearCacheByType<AccountingObject>();
                Utils.ClearCacheByType<AccountingObjectBankAccount>();
                SetDefaultValueControls();
                txtAccounttingObjectCode.Text = Utils.TaoMaChungTu(_IGenCodeService.getGenCode(101));
                txtAccounttingObjectCode.Enabled = true;
                #endregion
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(resSystem.MSG_Catalog5, "nhân viên"));
                _IAccountingObjectService.RolbackTran();
                return;
            }

        }

        private void SetDefaultValueControls()
        {
            CbbContactSex.Text = CbbContactSex.NullText;
            cbbDepartment.Text = cbbDepartment.NullText;
            cbbObjectType.Text = cbbObjectType.NullText;
            ckeIsActive.Checked = true;
            txtAccounttingObjectCode.Focus();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            #region xử lý form, kết thúc form
            DialogResult = DialogResult.Ignore;
            this.Close();
            #endregion
        }

        private void txtTaxCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            ValidatePhoneNumber(sender, e);
            if (e.KeyChar >= 48 && e.KeyChar <= 57 || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void cbbDepartment_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            new FDepartmentDetail().ShowDialog(this);
            //InitializeGUI();
            Utils.ClearCacheByType<Department>();
            if (FDepartmentDetail.DepartmentName != null)
            {
                cbbDepartment.Text = FDepartmentDetail.DepartmentName;
                FDepartmentDetail.DepartmentName = null;
            }
        }

        private void cbbDepartment_MouseClick(object sender, MouseEventArgs e)
        {
            //InitializeGUI();
        }
        private void cbbDepartment_ValueChanged(object sender, EventArgs e)
        {
            var row = cbbDepartment.SelectedRow;
            if (row == null) return;
            if (!isFirst && row.Cells["IsParentNode"].Value != null && (bool)row.Cells["IsParentNode"].Value == true)
            {
                cbbDepartment.Value = null;
                MSG.Error(resSystem.MSG_System_29);
            }
            isFirst = false;
        }
        #endregion
        //button đã bị ẩn đi -> hàm này k dùng tới
        private void txtBankAccount_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            //new FAccountingObjectBankAccount(FAccountingObjectBankAccount.dsAccountingObjectBankAccount).ShowDialog(this);
            //foreach (AccountingObjectBankAccount item in FAccountingObjectBankAccount.dsAccountingObjectBankAccount)
            //{
            //    if (item.IsSelect)
            //    {
            //        txtBankAccount.Text = item.BankAccount;
            //        txtBankName.Text = item.BankName;
            //        break;
            //    }
            //}
        }

        private void FAccountingObjectEmployeeDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            //FAccountingObjectBankAccount.dsAccountingObjectBankAccount.Clear();
            //_Select.BankAccounts = FAccountingObjectBankAccount.dsAccountingObjectBankAccount;
        }

        private void txtBankAccount_EditorButtonClick(object sender, EventArgs e)
        {
            //new FAccountingObjectBankAccount(FAccountingObjectBankAccount.dsAccountingObjectBankAccount).ShowDialog(this);
            //bool T = true;
            //foreach (AccountingObjectBankAccount item in FAccountingObjectBankAccount.dsAccountingObjectBankAccount)
            //{
            //    if (item.IsSelect)
            //    {
            //        T = false;
            //        txtBankAccount.Text = item.BankAccount;
            //        txtBankName.Text = item.BankName;
            //        break;
            //    }
            //}
            //if (T)
            //{
            //    txtBankAccount.Text = null;
            //    txtBankName.Text = null;
            //}
        }

        private void txtContactOfficeTel_KeyPress(object sender, KeyPressEventArgs e)
        {
            ValidatePhoneNumber(sender, e);
            //if (e.KeyChar >= 48 && e.KeyChar <= 57 || e.KeyChar == 8)
            //{
            //    e.Handled = false;
            //}
            //else
            //{
            //    e.Handled = true;
            //}
        }

        private void txtContactHomeTel_KeyPress(object sender, KeyPressEventArgs e)
        {
            ValidatePhoneNumber(sender, e);
            //if (e.KeyChar >= 48 && e.KeyChar <= 57 || e.KeyChar == 8)
            //{
            //    e.Handled = false;
            //}
            //else
            //{
            //    e.Handled = true;
            //}
        }

        private void txtTel_KeyPress(object sender, KeyPressEventArgs e)
        {
            ValidatePhoneNumber(sender, e);
            //if (e.KeyChar >= 48 && e.KeyChar <= 57 || e.KeyChar == 8)
            //{
            //    e.Handled = false;
            //}
            //else
            //{
            //    e.Handled = true;
            //}
        }

        private void txtEmployeeBirthday_Validated(object sender, EventArgs e)
        {
            UltraDateTimeEditor cbb = (UltraDateTimeEditor)sender;
            DateTime DoB = (DateTime)cbb.DateTime;
            if ((DoB != null) && (DoB.Day > System.DateTime.DaysInMonth(DoB.Year, DoB.Month) || (DoB.Year > 1753 && DoB.Year > DateTime.Now.Year - 18))) // Ngày sinh nhập vào phải nhỏ hơn số ngày trong tháng và tuổi nhân viên >= 18
            {
                MSG.Error("Ngày sinh không hợp lệ");
                txtEmployeeBirthday.Focus();
                return;
            }

        }

        private void cbbObjectType_Leave(object sender, EventArgs e)
        {
            if ((!cbbObjectType.Text.IsNullOrEmpty()))
            {


                if ((cbbObjectType.Text != "Khác") && (cbbObjectType.Text != "Khách hàng") && (cbbObjectType.Text != "Nhà cung cấp") && (cbbObjectType.Text != "Khách hàng/Nhà cung cấp") && (cbbObjectType.Text != "") && (cbbObjectType.Text != null))
                {
                    MSG.Error("Loại đối tượng kế toán không tồn tại ");
                }

            }
        }
        private void tsmAdd_Click(object sender, EventArgs e)
        {
            uGrid.AddNewRow4Grid();
        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            if (uGrid.ActiveCell != null)
            {
                UltraGridRow row = null;
                if (uGrid.ActiveCell != null || uGrid.ActiveRow != null)
                {
                    row = uGrid.ActiveCell != null ? uGrid.ActiveCell.Row : uGrid.ActiveRow;
                }
                else
                {
                    if (uGrid.Rows.Count > 0) row = uGrid.Rows[uGrid.Rows.Count - 1];
                }
                if (row != null) row.Delete(false);
                uGrid.Update();
            }
        }

        private void ultraLabel33_Click(object sender, EventArgs e)
        {

        }

        private void uGrid_CellChange(object sender, CellEventArgs e)
        {
            if (e.Cell.Column.Key == "IsSelect")
            {
                if (!(bool)e.Cell.Value)
                    foreach (var item in uGrid.Rows)
                    {
                        if (item.Cells["IsSelect"] != e.Cell)
                        {
                            item.Cells["IsSelect"].Value = false;
                        }
                    }
            }

        }
        public void ValidatePhoneNumber(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r' || e.KeyChar == '\b' || (char.IsDigit(e.KeyChar)) || e.KeyChar == '(' || e.KeyChar == ')' || e.KeyChar == '-' || e.KeyChar == '+' || e.KeyChar == '.' || e.KeyChar == ' ')
            {
                e.Handled = false;
            }
            else if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // If you want, you can allow decimal (float) numbers
            else if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        #region Add thông tin khách hàng lên web hóa đơn điện tử SDS add by Hautv
        private void Post_Customers(AccountingObject accountingObject)
        {
            if (accountingObject.ObjectType == 0 || accountingObject.ObjectType == 2) // Khách hàng
            {
                if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "HDDT").Data != "1") return;
                SystemOption systemOption = Utils.ListSystemOption.FirstOrDefault(n => n.Code == "EI_IDNhaCungCapDichVu");
                if (string.IsNullOrEmpty(systemOption.Data))
                {
                    //MSG.Error("Chưa kết nối hóa đơn điện tử");
                    return;
                }
                SupplierService supplierService = Utils.ISupplierServiceService.Getbykey(Guid.Parse(systemOption.Data));
                if (supplierService == null) return;
                if (supplierService.SupplierServiceCode == "SDS")
                {
                    Customers customers = new Customers();
                    Customer_If customer = new Customer_If();
                    customer.Code = accountingObject.AccountingObjectCode;
                    customer.AccountName = accountingObject.AccountingObjectCode;
                    customer.Name = accountingObject.AccountingObjectName;
                    //customer.Buyer = accountingObject.ContactName; // Người mua
                    customer.Address = accountingObject.Address;
                    customer.TaxCode = accountingObject.TaxCode;
                    if (accountingObject.BankAccounts.Count > 0)
                    {
                        customer.BankName = accountingObject.BankAccounts.First().BankName;
                        customer.BankNumber = accountingObject.BankAccounts.First().BankAccount;
                        //customer.BankAccountName = 
                    }
                    customer.Email = accountingObject.Email;
                    customer.Fax = accountingObject.Fax;
                    customer.Phone = accountingObject.Tel;
                    customer.ContactPerson = accountingObject.ContactName;
                    //customer.RepresentPerson = accountingObject.
                    customers.Customer.Add(customer);
                    var xmldata = Utils.Serialize<Customers>(customers);
                    var request = new Request();
                    request.XmlData = xmldata;
                    RestApiService client = new RestApiService(Utils.GetPathAccess(), Utils.GetUserName(), Core.ITripleDes.TripleDesDefaultDecryption(Utils.GetPassWords()));
                    Response response = client.Post(supplierService.ApiService.FirstOrDefault(n => n.ApiName == "ThemSuaKH").ApiPath, request);
                    if (response.Status != 2)
                    {
                        MSG.Error("Lỗi khi cập nhật lên web hóa đơn điện tử: \n" + response.Message);
                    }
                }
            }
        }
        #endregion
    }
}
