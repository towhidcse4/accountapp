﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;

namespace Accounting
{
    public partial class FQuickAccountingObjectBankAccount : DialogForm
    {
        private Guid _accountingObjectId;
        private Guid _id;
        public Guid Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public FQuickAccountingObjectBankAccount(Guid accountingObjectId)
        {
            InitializeComponent();
            _accountingObjectId = accountingObjectId;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtBankAccount.Text))
            {
                MSG.Warning("Bạn chưa nhập số tài khoản");
                txtBankAccount.Focus();
                return;
            }
            if (string.IsNullOrEmpty(txtBankName.Text))
            {
                MSG.Warning("Bạn chưa nhập tên ngân hàng");
                txtBankName.Focus();
                return;
            }
            var newBankAccount = new AccountingObjectBankAccount
            {
                AccountingObjectID = _accountingObjectId,
                BankAccount = txtBankAccount.Text,
                BankName = txtBankName.Text,
                BankBranchName = txtBankBranchName.Text,
                AccountHolderName = txtAccountHolderName.Text,
                IsSelect = true

            };
            var srv = IoC.Resolve<IAccountingObjectBankAccountService>();
            try
            {
                srv.BeginTran();
                srv.CreateNew(newBankAccount);
                srv.CommitTran();
                _id = newBankAccount.ID;
                Close();
            }
            catch (Exception)
            {
                srv.RolbackTran();
                MSG.Warning("Có lỗi xảy ra ! \r\n Vui lòng thực hiện lại !");
            }
        }
    }
}
