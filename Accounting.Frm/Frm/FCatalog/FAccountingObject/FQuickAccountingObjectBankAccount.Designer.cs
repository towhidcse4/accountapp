﻿namespace Accounting
{
    partial class FQuickAccountingObjectBankAccount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FQuickAccountingObjectBankAccount));
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.txtBankAccount = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtBankName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtBankBranchName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtAccountHolderName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankBranchName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountHolderName)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraLabel1
            // 
            appearance1.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance1;
            this.ultraLabel1.Location = new System.Drawing.Point(12, 12);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel1.TabIndex = 1;
            this.ultraLabel1.Text = "Số tài khoản";
            // 
            // ultraLabel2
            // 
            appearance2.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance2;
            this.ultraLabel2.Location = new System.Drawing.Point(12, 41);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel2.TabIndex = 2;
            this.ultraLabel2.Text = "Tên ngân hàng";
            // 
            // ultraLabel3
            // 
            appearance3.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance3;
            this.ultraLabel3.Location = new System.Drawing.Point(254, 41);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(58, 22);
            this.ultraLabel3.TabIndex = 3;
            this.ultraLabel3.Text = "Chi nhánh";
            // 
            // ultraLabel4
            // 
            appearance4.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance4;
            this.ultraLabel4.Location = new System.Drawing.Point(12, 70);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel4.TabIndex = 4;
            this.ultraLabel4.Text = "Tên chủ tài khoản";
            // 
            // txtBankAccount
            // 
            this.txtBankAccount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBankAccount.AutoSize = false;
            this.txtBankAccount.Location = new System.Drawing.Point(119, 12);
            this.txtBankAccount.Name = "txtBankAccount";
            this.txtBankAccount.Size = new System.Drawing.Size(381, 22);
            this.txtBankAccount.TabIndex = 5;
            // 
            // txtBankName
            // 
            this.txtBankName.AutoSize = false;
            this.txtBankName.Location = new System.Drawing.Point(119, 41);
            this.txtBankName.Name = "txtBankName";
            this.txtBankName.Size = new System.Drawing.Size(129, 22);
            this.txtBankName.TabIndex = 6;
            // 
            // txtBankBranchName
            // 
            this.txtBankBranchName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBankBranchName.AutoSize = false;
            this.txtBankBranchName.Location = new System.Drawing.Point(318, 41);
            this.txtBankBranchName.Name = "txtBankBranchName";
            this.txtBankBranchName.Size = new System.Drawing.Size(182, 22);
            this.txtBankBranchName.TabIndex = 7;
            // 
            // txtAccountHolderName
            // 
            this.txtAccountHolderName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountHolderName.AutoSize = false;
            this.txtAccountHolderName.Location = new System.Drawing.Point(119, 70);
            this.txtAccountHolderName.Name = "txtAccountHolderName";
            this.txtAccountHolderName.Size = new System.Drawing.Size(381, 22);
            this.txtAccountHolderName.TabIndex = 8;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance5.Image = global::Accounting.Properties.Resources.cancel_16;
            this.btnClose.Appearance = appearance5;
            this.btnClose.Location = new System.Drawing.Point(425, 102);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 28);
            this.btnClose.TabIndex = 9;
            this.btnClose.Text = "Hủy bỏ";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance6.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance6;
            this.btnSave.Location = new System.Drawing.Point(344, 102);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 28);
            this.btnSave.TabIndex = 10;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // FQuickAccountingObjectBankAccount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(512, 138);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.txtAccountHolderName);
            this.Controls.Add(this.txtBankBranchName);
            this.Controls.Add(this.txtBankName);
            this.Controls.Add(this.txtBankAccount);
            this.Controls.Add(this.ultraLabel4);
            this.Controls.Add(this.ultraLabel3);
            this.Controls.Add(this.ultraLabel2);
            this.Controls.Add(this.ultraLabel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FQuickAccountingObjectBankAccount";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thêm mới tài khoản ngân hàng";
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankBranchName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountHolderName)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtBankAccount;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtBankName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtBankBranchName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountHolderName;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnSave;
    }
}