﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using Accounting.TextMessage;
using System.Linq;
using Accounting.Core;

namespace Accounting
{
    public partial class FAccountingObjectCustomers : CatalogBase
    {
        #region Khai Báo
        public readonly IAccountingObjectService _IAccountingObjectService;
        private IAccountingObjectGroupService _IAccountingObjectGroupService;
        List<AccountingObject> listAccountingObject = new List<AccountingObject>();
        List<AccountingObjectGroup> listAccountingObjectGroup = new List<AccountingObjectGroup>();
        List<VoucherAccountingObject> listVoucherAccountingObject = new List<VoucherAccountingObject>();
        public static bool isClose = true;
        private readonly List<Item> _lstItems = Utils.ObjConstValue.SelectTimes;
        private DateTime ngayHoachToan;
        private DateTime dtBegin;
        private DateTime dtEnd;
        private Guid selectedGuid;
        #endregion
        #region Khởi tạo
        public FAccountingObjectCustomers()
        {
            InitializeComponent();

            //this.FormBorderStyle = FormBorderStyle.FixedSingle;
            //this.CenterToScreen();
            _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();
            _IAccountingObjectGroupService = IoC.Resolve<IAccountingObjectGroupService>();
            //this.uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;

            LoadDulieu();
            if (uGrid.Rows.Count > 0) uGrid.Rows[0].Selected = true;
            ToolTip ToolTip1 = new ToolTip();
            ToolTip1.SetToolTip(btnAdd, "Thêm (Ctrl + N)");
            ToolTip ToolTip2 = new ToolTip();
            ToolTip2.SetToolTip(btnEdit, "Xem (Ctrl + E)");
            ToolTip ToolTip3 = new ToolTip();
            ToolTip3.SetToolTip(btnDelete, "Xóa (Ctrl + D)");

            ngayHoachToan = (DateTime)Utils.StringToDateTime(ConstFrm.DbStartDate);
            //dtBegin = ngayHoachToan.AddDays(1 - ngayHoachToan.Day);
            //dtEnd = ngayHoachToan;
            //dteToDate.DateTime = dtEnd;
            //dteFromDate.DateTime = dtBegin;
            cbbAboutTime.DataSource = _lstItems;
            cbbAboutTime.DisplayMember = "Name";
            Utils.ConfigGrid(cbbAboutTime, ConstDatabase.ConfigXML_TableName);
            cbbAboutTime.SelectedRow = cbbAboutTime.Rows[7];
            cbbAboutTime.DisplayLayout.AutoFitStyle = AutoFitStyle.ExtendLastColumn;

        }
        #endregion
        #region Hàm
        void LoadDulieu()
        {
            LoadDuLieu(true);
        }
        private void LoadDuLieu(bool configGrid)
        {
            WaitingFrm.StartWaiting();
            #region Lấy dữ liệu từ CSDL
            // listAccountingObject = _IAccountingObjectService.GetAll().Where(p => p.IsEmployee == false).ToList();
            //_IAccountingObjectService.UnbindSession(listAccountingObject);
            listAccountingObject.Clear();
            listAccountingObject = _IAccountingObjectService.GetListAccountingObjectByEmployee(false);
            var listStartN = listAccountingObject.Where(p => p.AccountingObjectName.StartsWith("N")).ToList();
            listAccountingObjectGroup.Clear();
            listVoucherAccountingObject.Clear();
            listAccountingObjectGroup = _IAccountingObjectGroupService.GetAll();
            listVoucherAccountingObject = _IAccountingObjectService.GetListVoucherAccoungtingObjectView();
            //List<VoucherAccountingObject> a = _IAccountingObjectService.GetListVoucherAccoungtingObjectView();
            //List<BankH> list = _IBankServiceH.GetAll();
            #endregion

            #region Xử lý dữ liệu
            #endregion

            #region hiển thị và xử lý hiển thị
            uGrid.DataSource = listAccountingObject.OrderBy(p => p.AccountingObjectCode).ToArray(); //sắp xếp danh sách theo thứ tự Mã KH/NCC tăng dần
            foreach (var item in listAccountingObject)
            {
                if (item.AccountObjectGroupID != null)
                {
                    AccountingObjectGroup accGroup = listAccountingObjectGroup.Where(p => p.ID == item.AccountObjectGroupID).SingleOrDefault();

                    item.AccountObjectGroupIdNameView = accGroup != null ? accGroup.AccountingObjectGroupCode : null;
                }

            }
            if (uGrid.Rows.Count > 0) uGrid.Rows[0].Selected = true;
            if (configGrid) ConfigGrid(uGrid);
            #endregion
            WaitingFrm.StopWaiting();
        }
        #endregion
        #region Event

        private void uGrid_BeforeRowActivate(object sender, RowEventArgs e)
        {
            if (e.Row == null) return;
            var temp = e.Row.ListObject as AccountingObject;
            if (temp == null) return;
            labeID.Text = temp.AccountingObjectCode;
            labeName.Text = temp.AccountingObjectName;
            labeAddress.Text = temp.Address;
            labePhone.Text = temp.ContactMobile;
            labeFax.Text = temp.Fax;

            labeTaxCode.Text = temp.TaxCode;
            labeEmail.Text = temp.Email;
            labeWebsite.Text = temp.Website;
            labeBankAcount.Text = temp.BankAccount;
            labeBankName.Text = temp.BankName;

            List<VoucherAccountingObject> voucher = listVoucherAccountingObject.
                Where(p => p.AccountingObjectId == temp.ID && p.Date >= dtBegin && p.Date <= dtEnd).OrderByDescending(u => u.Date).ToList();
            foreach (var item in voucher)
            {
                var firstOrDefault = Utils.ListType.FirstOrDefault(x => x.ID == item.TypeID);
                if (firstOrDefault != null)
                    item.TypeName = firstOrDefault.TypeName;
            }

            ultraGridVoucher.DataSource = voucher;
            ultraGridVoucher.ConfigGrid(ConstDatabase.VoucherAccountingObject_KeyName);
            ultraGridVoucher.DisplayLayout.Bands[0].Columns["TotalAmount"].FormatNumberic(ConstDatabase.Format_TienVND);            
            selectedGuid = temp.ID;
        }

        private void btnAdd_Click_1(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void btnEdit_Click_1(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void btnDelete_Click_1(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            EditFunction();
        }
        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            ResetFunction();
        }
        protected override void ResetFunction()
        {
            LoadDuLieu(false);
        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void tsmEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tsmAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void uGrid_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }

        private void btnViewVoucher_Click(object sender, EventArgs e)
        {
            if (ultraGridVoucher.ActiveRow == null) return;
            var voucher = ultraGridVoucher.ActiveRow.ListObject as VoucherAccountingObject;
            if (voucher == null) return;
            //ViewVoucherSelected(voucher);
            Utils.ViewVoucherSelected(voucher.ID, voucher.TypeID);
        }
        private void ultraGridVoucher_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (ultraGridVoucher.ActiveRow == null) return;
            var voucher = ultraGridVoucher.ActiveRow.ListObject as VoucherAccountingObject;
            if (voucher == null) return;
            //ViewVoucherSelected(voucher);
            Utils.ViewVoucherSelected(voucher.ID, voucher.TypeID);
        }
        private void cbbAboutTime_ValueChanged(object sender, EventArgs e)
        {
            if (cbbAboutTime.SelectedRow != null)
            {
                var model = cbbAboutTime.SelectedRow.ListObject as Item;

                Utils.GetDateBeginDateEnd(ngayHoachToan.Year,ngayHoachToan, model, out dtBegin, out dtEnd);
                dteFromDate.DateTime = dtBegin;
                dteToDate.DateTime = dtEnd;
                List<VoucherAccountingObject> voucher = listVoucherAccountingObject.
    Where(p => p.AccountingObjectId == selectedGuid && p.Date >= dtBegin && p.Date <= dtEnd).OrderByDescending(u => u.Date).ToList();
                foreach (var item in voucher)
                {
                    var firstOrDefault = Utils.ListType.FirstOrDefault(x => x.ID == item.TypeID);
                    if (firstOrDefault != null)
                        item.TypeName = firstOrDefault.TypeName;
                }

                ultraGridVoucher.DataSource = voucher;
                ultraGridVoucher.ConfigGrid(ConstDatabase.VoucherAccountingObject_KeyName);
                ultraGridVoucher.DisplayLayout.Bands[0].Columns["TotalAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            }
        }
        private void dteToDate_ValueChanged(object sender, EventArgs e)
        {
            if (dteFromDate.DateTime <= dteToDate.DateTime)
            {
                List<VoucherAccountingObject> voucher = listVoucherAccountingObject.
    Where(p => p.AccountingObjectId == selectedGuid && p.Date >= dteFromDate.DateTime && p.Date <= dteToDate.DateTime).ToList();
                foreach (var item in voucher)
                {
                    var firstOrDefault = Utils.ListType.FirstOrDefault(x => x.ID == item.TypeID);
                    if (firstOrDefault != null)
                        item.TypeName = firstOrDefault.TypeName;
                }

                ultraGridVoucher.DataSource = voucher;
                ultraGridVoucher.ConfigGrid(ConstDatabase.VoucherAccountingObject_KeyName);
                ultraGridVoucher.DisplayLayout.Bands[0].Columns["TotalAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            }
            else
            {
               // MSG.Warning("Ngày đến phải lớn hơn ngày bắt đầu !");
            }
        }
        private void dteFromDate_ValueChanged(object sender, EventArgs e)
        {
            if (dteFromDate.DateTime <= dteToDate.DateTime)
            {               
                List<VoucherAccountingObject> voucher = listVoucherAccountingObject.
    Where(p => p.AccountingObjectId == selectedGuid && p.Date >= dteFromDate.DateTime && p.Date <= dteToDate.DateTime).OrderByDescending(u => u.Date).ToList();
                foreach (var item in voucher)
                {
                    var firstOrDefault = Utils.ListType.FirstOrDefault(x => x.ID == item.TypeID);
                    if (firstOrDefault != null)
                        item.TypeName = firstOrDefault.TypeName;
                }

                ultraGridVoucher.DataSource = voucher;
                ultraGridVoucher.ConfigGrid(ConstDatabase.VoucherAccountingObject_KeyName);
                ultraGridVoucher.DisplayLayout.Bands[0].Columns["TotalAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            }
            else
            {
                //MSG.Warning("Ngày bắt đầu phải nhỏ hơn ngày đến !");
            }
        }
        #endregion
        #region Utils
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, TextMessage.ConstDatabase.AccountingObject_TableName);
            uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectCode"].Header.Caption = "Mã khách hàng, NCC";
            uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectName"].Header.Caption = "Tên khách hàng, NCC";
            uGrid.DisplayLayout.Bands[0].Columns["DepartmentName"].Hidden = true;
            uGrid.DisplayLayout.Bands[0].Columns["ContactTitle"].Hidden = true;
            uGrid.DisplayLayout.Bands[0].Columns["TaxCode"].Hidden = false;
            uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            uGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Extended;
        }

        #endregion
        #region Nghiệp vụ
        /// Nghiệp vụ Update
        protected override void EditFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                AccountingObject temp = uGrid.Selected.Rows[0].ListObject as AccountingObject;
                new FAccountingObjectCustomersDetail(temp).ShowDialog(this);
                if (!FAccountingObjectCustomersDetail.isClose) { Utils.ClearCacheByType<AccountingObject>(); LoadDuLieu(false); }

            }
            else
                Accounting.TextMessage.MSG.Error(string.Format(Accounting.TextMessage.resSystem.MSG_Catalog3, "Đối tượng kế toán"));
        }

        /// Nghiệp vụ Add
        protected override void AddFunction()
        {
            new FAccountingObjectCustomersDetail().ShowDialog(this);
            if (!FAccountingObjectCustomersDetail.isClose) { Utils.ClearCacheByType<AccountingObject>(); LoadDuLieu(false); }
        }

        /// Nghiệp vụ Delete
        protected override void DeleteFunction()
        {
            bool a = uGrid.Selected.Rows.Count > 0;
            bool b = uGrid.ActiveRow != null;
            if (a || b)
            {
                if (!(DialogResult.Yes == MessageBox.Show("Bạn có muốn xóa những dữ liệu này không ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question)))
                    return;
                SelectedRowsCollection lst = uGrid.Selected.Rows;
                var strResult = new List<Result>();
                foreach (UltraGridRow ultraGridRow in lst)
                {
                    var result = new Result();
                    AccountingObject temp = ultraGridRow.ListObject as AccountingObject;
                    AccountingObject data = Utils.IAccountingObjectService.Getbykey(temp.ID);
                    try
                    {
                        if (!Utils.checkRelationVoucher(data) && !Utils.IOPAccountService.Query.Any(o => o.AccountingObjectID == data.ID || o.EmployeeID == data.ID))
                        {
                            Utils.IAccountingObjectService.BeginTran();
                            Utils.IAccountingObjectService.Delete(data);
                            Utils.IAccountingObjectService.CommitTran();
                            Utils.ClearCacheByType<AccountingObject>();
                            result.Reason = "Xóa thành công";
                            result.Code = data.AccountingObjectCode;
                            result.IsSucceed = true;
                            strResult.Add(result);


                        }
                        else
                        {
                            result.Reason = "Không thể xóa do dữ liệu đã phát sinh chứng từ liên quan";
                            result.Code = data.AccountingObjectCode;
                            result.IsSucceed = false;
                            strResult.Add(result);
                        }
                    }
                    catch (Exception ex)
                    {
                        Utils.IAccountingObjectService.RolbackTran();
                        MSG.Error("Xóa chứng từ thất bại");
                    }
                }
                if (lst.Count > 1 && strResult.Count > 0) new MsgResult(strResult, false).ShowDialog(this);
                else MSG.Information(strResult.First().Reason);
                LoadDuLieu(false);
            }
            
            //if (uGrid.Selected.Rows.Count > 0)
            //{
            //    AccountingObject temp = uGrid.Selected.Rows[0].ListObject as AccountingObject;

            //    AccountingObject data = _IAccountingObjectService.Getbykey(temp.ID);

            //    if (MSG.Question(string.Format(resSystem.MSG_System_05, temp.AccountingObjectCode)) == System.Windows.Forms.DialogResult.Yes)
            //    {
            //        if(!Utils.checkRelationVoucher(data) && !Utils.IOPAccountService.Query.Any(o => o.AccountingObjectID == data.ID || o.EmployeeID == data.ID))
            //        {
            //            _IAccountingObjectService.BeginTran();
            //            _IAccountingObjectService.Delete(data);
            //            _IAccountingObjectService.CommitTran();
            //            Utils.ClearCacheByType<AccountingObject>();
            //            LoadDulieu();
            //        }
            //        else
            //        {
            //            MSG.Error("Cần xóa chứng từ kế toán có liên quan trước khi xóa dữ liệu danh mục");
            //        }
            //    }
            //}
            //else
            //    MSG.Error(resSystem.MSG_System_06);
        }

        #endregion

        private void FAccountingObjectCustomers_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGrid.ResetText();
            uGrid.ResetUpdateMode();
            uGrid.ResetExitEditModeOnLeave();
            uGrid.ResetRowUpdateCancelAction();
            uGrid.DataSource = null;
            uGrid.Layouts.Clear();
            uGrid.ResetLayouts();
            uGrid.ResetDisplayLayout();
            uGrid.Refresh();
            uGrid.ClearUndoHistory();
            uGrid.ClearXsdConstraints();

            ultraGridVoucher.ResetText();
            ultraGridVoucher.ResetUpdateMode();
            ultraGridVoucher.ResetExitEditModeOnLeave();
            ultraGridVoucher.ResetRowUpdateCancelAction();
            ultraGridVoucher.DataSource = null;
            ultraGridVoucher.Layouts.Clear();
            ultraGridVoucher.ResetLayouts();
            ultraGridVoucher.ResetDisplayLayout();
            ultraGridVoucher.Refresh();
            ultraGridVoucher.ClearUndoHistory();
            ultraGridVoucher.ClearXsdConstraints();
        }

        private void FAccountingObjectCustomers_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            FExportExcelAccountingObject fExportExcelAccountingObject = new FExportExcelAccountingObject();
            fExportExcelAccountingObject.Show();
        }
    }

}
