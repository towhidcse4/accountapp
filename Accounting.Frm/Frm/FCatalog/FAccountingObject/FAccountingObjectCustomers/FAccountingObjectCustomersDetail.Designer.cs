﻿namespace Accounting
{
    partial class FAccountingObjectCustomersDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FAccountingObjectCustomersDetail));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem4 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem9 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem7 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem6 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem5 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.txtContactOfficeTel = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtIssueBy = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel24 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel17 = new Infragistics.Win.Misc.UltraLabel();
            this.labenoicap = new Infragistics.Win.Misc.UltraLabel();
            this.txtContactHomeTel = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel18 = new Infragistics.Win.Misc.UltraLabel();
            this.txtContactEmail = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtTel = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel25 = new Infragistics.Win.Misc.UltraLabel();
            this.txtTaxCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbAccountObjectGroup = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.txtContactAddress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtIssueDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel23 = new Infragistics.Win.Misc.UltraLabel();
            this.txtContactMobile = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.labengaycap = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.CbbContactSex = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel34 = new Infragistics.Win.Misc.UltraLabel();
            this.txtIdentificationNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.txtContactTitle = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.txtContactName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbPaymentClauseID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtDueTime = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel19 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAddress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel26 = new Infragistics.Win.Misc.UltraLabel();
            this.txtMaximizaDebtAmount = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.txtFax = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel27 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.txtWebsite = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.txtEmail = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblGroupName = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccounttingObjectName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblGroupCode = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccounttingObjectCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.cms4Grid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.pn_action = new System.Windows.Forms.Panel();
            this.btnSaveContinue = new Infragistics.Win.Misc.UltraButton();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.ckeIsActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.chkIsLabourUnionFree = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.chkIsInsured = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.chkIsEmployee = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraLabel22 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel21 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbScaleType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.cbbObjectType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactOfficeTel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIssueBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactHomeTel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountObjectGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIssueDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactMobile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbbContactSex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdentificationNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPaymentClauseID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWebsite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccounttingObjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccounttingObjectCode)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            this.cms4Grid.SuspendLayout();
            this.pn_action.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ckeIsActive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsLabourUnionFree)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsInsured)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsEmployee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbScaleType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbObjectType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).BeginInit();
            this.ultraTabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.txtContactOfficeTel);
            this.ultraTabPageControl1.Controls.Add(this.txtIssueBy);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel24);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel17);
            this.ultraTabPageControl1.Controls.Add(this.labenoicap);
            this.ultraTabPageControl1.Controls.Add(this.txtContactHomeTel);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel18);
            this.ultraTabPageControl1.Controls.Add(this.txtContactEmail);
            this.ultraTabPageControl1.Controls.Add(this.txtTel);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel5);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel25);
            this.ultraTabPageControl1.Controls.Add(this.txtTaxCode);
            this.ultraTabPageControl1.Controls.Add(this.cbbAccountObjectGroup);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel10);
            this.ultraTabPageControl1.Controls.Add(this.txtContactAddress);
            this.ultraTabPageControl1.Controls.Add(this.txtIssueDate);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel23);
            this.ultraTabPageControl1.Controls.Add(this.txtContactMobile);
            this.ultraTabPageControl1.Controls.Add(this.labengaycap);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel16);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel15);
            this.ultraTabPageControl1.Controls.Add(this.CbbContactSex);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel34);
            this.ultraTabPageControl1.Controls.Add(this.txtIdentificationNo);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel13);
            this.ultraTabPageControl1.Controls.Add(this.txtContactTitle);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel14);
            this.ultraTabPageControl1.Controls.Add(this.txtContactName);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel11);
            this.ultraTabPageControl1.Controls.Add(this.cbbPaymentClauseID);
            this.ultraTabPageControl1.Controls.Add(this.txtDueTime);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel19);
            this.ultraTabPageControl1.Controls.Add(this.txtAddress);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel26);
            this.ultraTabPageControl1.Controls.Add(this.txtMaximizaDebtAmount);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel4);
            this.ultraTabPageControl1.Controls.Add(this.txtFax);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel27);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel6);
            this.ultraTabPageControl1.Controls.Add(this.txtWebsite);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel7);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel8);
            this.ultraTabPageControl1.Controls.Add(this.txtEmail);
            this.ultraTabPageControl1.Controls.Add(this.lblGroupName);
            this.ultraTabPageControl1.Controls.Add(this.txtAccounttingObjectName);
            this.ultraTabPageControl1.Controls.Add(this.lblGroupCode);
            this.ultraTabPageControl1.Controls.Add(this.txtAccounttingObjectCode);
            resources.ApplyResources(this.ultraTabPageControl1, "ultraTabPageControl1");
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            // 
            // txtContactOfficeTel
            // 
            resources.ApplyResources(this.txtContactOfficeTel, "txtContactOfficeTel");
            this.txtContactOfficeTel.MaxLength = 25;
            this.txtContactOfficeTel.Name = "txtContactOfficeTel";
            this.txtContactOfficeTel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtContactOfficeTel_KeyPress);
            // 
            // txtIssueBy
            // 
            resources.ApplyResources(this.txtIssueBy, "txtIssueBy");
            this.txtIssueBy.MaxLength = 512;
            this.txtIssueBy.Name = "txtIssueBy";
            // 
            // ultraLabel24
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel24.Appearance = appearance1;
            resources.ApplyResources(this.ultraLabel24, "ultraLabel24");
            this.ultraLabel24.Name = "ultraLabel24";
            // 
            // ultraLabel17
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel17.Appearance = appearance2;
            resources.ApplyResources(this.ultraLabel17, "ultraLabel17");
            this.ultraLabel17.Name = "ultraLabel17";
            // 
            // labenoicap
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            this.labenoicap.Appearance = appearance3;
            resources.ApplyResources(this.labenoicap, "labenoicap");
            this.labenoicap.Name = "labenoicap";
            // 
            // txtContactHomeTel
            // 
            resources.ApplyResources(this.txtContactHomeTel, "txtContactHomeTel");
            this.txtContactHomeTel.MaxLength = 25;
            this.txtContactHomeTel.Name = "txtContactHomeTel";
            this.txtContactHomeTel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtContactHomeTel_KeyPress);
            // 
            // ultraLabel18
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel18.Appearance = appearance4;
            resources.ApplyResources(this.ultraLabel18, "ultraLabel18");
            this.ultraLabel18.Name = "ultraLabel18";
            // 
            // txtContactEmail
            // 
            resources.ApplyResources(this.txtContactEmail, "txtContactEmail");
            this.txtContactEmail.MaxLength = 100;
            this.txtContactEmail.Name = "txtContactEmail";
            // 
            // txtTel
            // 
            resources.ApplyResources(this.txtTel, "txtTel");
            this.txtTel.MaxLength = 25;
            this.txtTel.Name = "txtTel";
            this.txtTel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTel_KeyPress_1);
            // 
            // ultraLabel5
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel5.Appearance = appearance5;
            resources.ApplyResources(this.ultraLabel5, "ultraLabel5");
            this.ultraLabel5.Name = "ultraLabel5";
            // 
            // ultraLabel25
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel25.Appearance = appearance6;
            resources.ApplyResources(this.ultraLabel25, "ultraLabel25");
            this.ultraLabel25.Name = "ultraLabel25";
            // 
            // txtTaxCode
            // 
            resources.ApplyResources(this.txtTaxCode, "txtTaxCode");
            this.txtTaxCode.MaxLength = 25;
            this.txtTaxCode.Name = "txtTaxCode";
            // 
            // cbbAccountObjectGroup
            // 
            resources.ApplyResources(this.cbbAccountObjectGroup, "cbbAccountObjectGroup");
            this.cbbAccountObjectGroup.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountObjectGroup.Name = "cbbAccountObjectGroup";
            this.cbbAccountObjectGroup.NullText = "<Chọn dữ liệu>";
            // 
            // ultraLabel10
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel10.Appearance = appearance7;
            resources.ApplyResources(this.ultraLabel10, "ultraLabel10");
            this.ultraLabel10.Name = "ultraLabel10";
            // 
            // txtContactAddress
            // 
            resources.ApplyResources(this.txtContactAddress, "txtContactAddress");
            this.txtContactAddress.MaxLength = 512;
            this.txtContactAddress.Name = "txtContactAddress";
            // 
            // txtIssueDate
            // 
            resources.ApplyResources(this.txtIssueDate, "txtIssueDate");
            this.txtIssueDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.txtIssueDate.MaskInput = "dd/mm/yyyy";
            this.txtIssueDate.Name = "txtIssueDate";
            this.txtIssueDate.Value = null;
            // 
            // ultraLabel23
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel23.Appearance = appearance8;
            resources.ApplyResources(this.ultraLabel23, "ultraLabel23");
            this.ultraLabel23.Name = "ultraLabel23";
            // 
            // txtContactMobile
            // 
            resources.ApplyResources(this.txtContactMobile, "txtContactMobile");
            this.txtContactMobile.Name = "txtContactMobile";
            this.txtContactMobile.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtContactMobile_KeyPress);
            // 
            // labengaycap
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            this.labengaycap.Appearance = appearance9;
            resources.ApplyResources(this.labengaycap, "labengaycap");
            this.labengaycap.Name = "labengaycap";
            // 
            // ultraLabel16
            // 
            appearance10.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel16.Appearance = appearance10;
            resources.ApplyResources(this.ultraLabel16, "ultraLabel16");
            this.ultraLabel16.Name = "ultraLabel16";
            // 
            // ultraLabel15
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel15.Appearance = appearance11;
            resources.ApplyResources(this.ultraLabel15, "ultraLabel15");
            this.ultraLabel15.Name = "ultraLabel15";
            // 
            // CbbContactSex
            // 
            resources.ApplyResources(this.CbbContactSex, "CbbContactSex");
            this.CbbContactSex.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            valueListItem1.DataValue = "0";
            resources.ApplyResources(valueListItem1, "valueListItem1");
            valueListItem1.ForceApplyResources = "";
            valueListItem2.DataValue = "1";
            resources.ApplyResources(valueListItem2, "valueListItem2");
            valueListItem2.ForceApplyResources = "";
            this.CbbContactSex.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2});
            this.CbbContactSex.Name = "CbbContactSex";
            // 
            // ultraLabel34
            // 
            appearance12.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel34.Appearance = appearance12;
            resources.ApplyResources(this.ultraLabel34, "ultraLabel34");
            this.ultraLabel34.Name = "ultraLabel34";
            // 
            // txtIdentificationNo
            // 
            resources.ApplyResources(this.txtIdentificationNo, "txtIdentificationNo");
            this.txtIdentificationNo.MaxLength = 50;
            this.txtIdentificationNo.Name = "txtIdentificationNo";
            // 
            // ultraLabel13
            // 
            appearance13.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel13.Appearance = appearance13;
            resources.ApplyResources(this.ultraLabel13, "ultraLabel13");
            this.ultraLabel13.Name = "ultraLabel13";
            // 
            // txtContactTitle
            // 
            resources.ApplyResources(this.txtContactTitle, "txtContactTitle");
            this.txtContactTitle.MaxLength = 512;
            this.txtContactTitle.Name = "txtContactTitle";
            // 
            // ultraLabel14
            // 
            appearance14.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel14.Appearance = appearance14;
            resources.ApplyResources(this.ultraLabel14, "ultraLabel14");
            this.ultraLabel14.Name = "ultraLabel14";
            // 
            // txtContactName
            // 
            resources.ApplyResources(this.txtContactName, "txtContactName");
            this.txtContactName.MaxLength = 512;
            this.txtContactName.Name = "txtContactName";
            // 
            // ultraLabel11
            // 
            appearance15.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ultraLabel11.Appearance = appearance15;
            resources.ApplyResources(this.ultraLabel11, "ultraLabel11");
            this.ultraLabel11.Name = "ultraLabel11";
            // 
            // cbbPaymentClauseID
            // 
            resources.ApplyResources(this.cbbPaymentClauseID, "cbbPaymentClauseID");
            this.cbbPaymentClauseID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbPaymentClauseID.Name = "cbbPaymentClauseID";
            this.cbbPaymentClauseID.NullText = "<Chọn dữ liệu>";
            // 
            // txtDueTime
            // 
            resources.ApplyResources(this.txtDueTime, "txtDueTime");
            this.txtDueTime.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.txtDueTime.InputMask = "nnnnnnnnn";
            this.txtDueTime.Name = "txtDueTime";
            this.txtDueTime.PromptChar = ' ';
            // 
            // ultraLabel19
            // 
            appearance16.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel19.Appearance = appearance16;
            resources.ApplyResources(this.ultraLabel19, "ultraLabel19");
            this.ultraLabel19.Name = "ultraLabel19";
            // 
            // txtAddress
            // 
            resources.ApplyResources(this.txtAddress, "txtAddress");
            this.txtAddress.MaxLength = 512;
            this.txtAddress.Name = "txtAddress";
            // 
            // ultraLabel26
            // 
            appearance17.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel26.Appearance = appearance17;
            resources.ApplyResources(this.ultraLabel26, "ultraLabel26");
            this.ultraLabel26.Name = "ultraLabel26";
            // 
            // txtMaximizaDebtAmount
            // 
            resources.ApplyResources(this.txtMaximizaDebtAmount, "txtMaximizaDebtAmount");
            this.txtMaximizaDebtAmount.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.txtMaximizaDebtAmount.InputMask = "{LOC}nn,nnn,nnn.nn";
            this.txtMaximizaDebtAmount.Name = "txtMaximizaDebtAmount";
            this.txtMaximizaDebtAmount.PromptChar = ' ';
            // 
            // ultraLabel4
            // 
            appearance18.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel4.Appearance = appearance18;
            resources.ApplyResources(this.ultraLabel4, "ultraLabel4");
            this.ultraLabel4.Name = "ultraLabel4";
            // 
            // txtFax
            // 
            resources.ApplyResources(this.txtFax, "txtFax");
            this.txtFax.MaxLength = 25;
            this.txtFax.Name = "txtFax";
            // 
            // ultraLabel27
            // 
            appearance19.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel27.Appearance = appearance19;
            resources.ApplyResources(this.ultraLabel27, "ultraLabel27");
            this.ultraLabel27.Name = "ultraLabel27";
            // 
            // ultraLabel6
            // 
            appearance20.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel6.Appearance = appearance20;
            resources.ApplyResources(this.ultraLabel6, "ultraLabel6");
            this.ultraLabel6.Name = "ultraLabel6";
            // 
            // txtWebsite
            // 
            resources.ApplyResources(this.txtWebsite, "txtWebsite");
            this.txtWebsite.MaxLength = 100;
            this.txtWebsite.Name = "txtWebsite";
            // 
            // ultraLabel7
            // 
            appearance21.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel7.Appearance = appearance21;
            resources.ApplyResources(this.ultraLabel7, "ultraLabel7");
            this.ultraLabel7.Name = "ultraLabel7";
            // 
            // ultraLabel8
            // 
            appearance22.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel8.Appearance = appearance22;
            resources.ApplyResources(this.ultraLabel8, "ultraLabel8");
            this.ultraLabel8.Name = "ultraLabel8";
            // 
            // txtEmail
            // 
            resources.ApplyResources(this.txtEmail, "txtEmail");
            this.txtEmail.MaxLength = 100;
            this.txtEmail.Name = "txtEmail";
            // 
            // lblGroupName
            // 
            appearance23.BackColor = System.Drawing.Color.Transparent;
            this.lblGroupName.Appearance = appearance23;
            resources.ApplyResources(this.lblGroupName, "lblGroupName");
            this.lblGroupName.Name = "lblGroupName";
            // 
            // txtAccounttingObjectName
            // 
            resources.ApplyResources(this.txtAccounttingObjectName, "txtAccounttingObjectName");
            this.txtAccounttingObjectName.MaxLength = 512;
            this.txtAccounttingObjectName.Name = "txtAccounttingObjectName";
            // 
            // lblGroupCode
            // 
            appearance24.BackColor = System.Drawing.Color.Transparent;
            this.lblGroupCode.Appearance = appearance24;
            resources.ApplyResources(this.lblGroupCode, "lblGroupCode");
            this.lblGroupCode.Name = "lblGroupCode";
            // 
            // txtAccounttingObjectCode
            // 
            resources.ApplyResources(this.txtAccounttingObjectCode, "txtAccounttingObjectCode");
            this.txtAccounttingObjectCode.MaxLength = 25;
            this.txtAccounttingObjectCode.Name = "txtAccounttingObjectCode";
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.uGrid);
            resources.ApplyResources(this.ultraTabPageControl2, "ultraTabPageControl2");
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            // 
            // uGrid
            // 
            this.uGrid.ContextMenuStrip = this.cms4Grid;
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.AlignWithDataRows;
            this.uGrid.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.uGrid.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGrid.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGrid.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGrid.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            resources.ApplyResources(this.uGrid, "uGrid");
            this.uGrid.Name = "uGrid";
            // 
            // cms4Grid
            // 
            this.cms4Grid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmAdd,
            this.tsmDelete});
            this.cms4Grid.Name = "cms4Grid";
            resources.ApplyResources(this.cms4Grid, "cms4Grid");
            // 
            // tsmAdd
            // 
            this.tsmAdd.Image = global::Accounting.Properties.Resources._1437136799_add;
            this.tsmAdd.Name = "tsmAdd";
            resources.ApplyResources(this.tsmAdd, "tsmAdd");
            this.tsmAdd.Click += new System.EventHandler(this.tsmAdd_Click);
            // 
            // tsmDelete
            // 
            this.tsmDelete.Image = global::Accounting.Properties.Resources.ubtnDelete;
            this.tsmDelete.Name = "tsmDelete";
            resources.ApplyResources(this.tsmDelete, "tsmDelete");
            this.tsmDelete.Click += new System.EventHandler(this.tsmDelete_Click);
            // 
            // pn_action
            // 
            this.pn_action.BackColor = System.Drawing.Color.Transparent;
            this.pn_action.Controls.Add(this.btnSaveContinue);
            this.pn_action.Controls.Add(this.btnClose);
            this.pn_action.Controls.Add(this.btnSave);
            this.pn_action.Controls.Add(this.ckeIsActive);
            this.pn_action.Controls.Add(this.chkIsLabourUnionFree);
            this.pn_action.Controls.Add(this.chkIsInsured);
            this.pn_action.Controls.Add(this.chkIsEmployee);
            resources.ApplyResources(this.pn_action, "pn_action");
            this.pn_action.Name = "pn_action";
            this.pn_action.TabStop = true;
            // 
            // btnSaveContinue
            // 
            appearance25.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSaveContinue.Appearance = appearance25;
            resources.ApplyResources(this.btnSaveContinue, "btnSaveContinue");
            this.btnSaveContinue.Name = "btnSaveContinue";
            this.btnSaveContinue.Click += new System.EventHandler(this.btnSaveContinue_Click);
            // 
            // btnClose
            // 
            appearance26.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance26;
            resources.ApplyResources(this.btnClose, "btnClose");
            this.btnClose.Name = "btnClose";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            appearance27.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance27;
            resources.ApplyResources(this.btnSave, "btnSave");
            this.btnSave.Name = "btnSave";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // ckeIsActive
            // 
            this.ckeIsActive.BackColor = System.Drawing.Color.Transparent;
            this.ckeIsActive.BackColorInternal = System.Drawing.Color.Transparent;
            this.ckeIsActive.Checked = true;
            this.ckeIsActive.CheckState = System.Windows.Forms.CheckState.Checked;
            resources.ApplyResources(this.ckeIsActive, "ckeIsActive");
            this.ckeIsActive.Name = "ckeIsActive";
            // 
            // chkIsLabourUnionFree
            // 
            this.chkIsLabourUnionFree.BackColor = System.Drawing.Color.Transparent;
            this.chkIsLabourUnionFree.BackColorInternal = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.chkIsLabourUnionFree, "chkIsLabourUnionFree");
            this.chkIsLabourUnionFree.Name = "chkIsLabourUnionFree";
            // 
            // chkIsInsured
            // 
            this.chkIsInsured.BackColor = System.Drawing.Color.Transparent;
            this.chkIsInsured.BackColorInternal = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.chkIsInsured, "chkIsInsured");
            this.chkIsInsured.Name = "chkIsInsured";
            // 
            // chkIsEmployee
            // 
            this.chkIsEmployee.BackColor = System.Drawing.Color.Transparent;
            this.chkIsEmployee.BackColorInternal = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.chkIsEmployee, "chkIsEmployee");
            this.chkIsEmployee.Name = "chkIsEmployee";
            // 
            // ultraLabel22
            // 
            appearance28.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel22.Appearance = appearance28;
            resources.ApplyResources(this.ultraLabel22, "ultraLabel22");
            this.ultraLabel22.Name = "ultraLabel22";
            // 
            // ultraLabel21
            // 
            appearance29.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel21.Appearance = appearance29;
            resources.ApplyResources(this.ultraLabel21, "ultraLabel21");
            this.ultraLabel21.Name = "ultraLabel21";
            this.ultraLabel21.Click += new System.EventHandler(this.ultraLabel21_Click);
            // 
            // cbbScaleType
            // 
            resources.ApplyResources(this.cbbScaleType, "cbbScaleType");
            this.cbbScaleType.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            valueListItem4.DataValue = ((short)(0));
            resources.ApplyResources(valueListItem4, "valueListItem4");
            valueListItem4.ForceApplyResources = "";
            valueListItem9.DataValue = ((short)(1));
            resources.ApplyResources(valueListItem9, "valueListItem9");
            valueListItem9.ForceApplyResources = "";
            this.cbbScaleType.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem4,
            valueListItem9});
            this.cbbScaleType.Name = "cbbScaleType";
            this.cbbScaleType.ValueChanged += new System.EventHandler(this.cbbScaleType_ValueChanged);
            // 
            // cbbObjectType
            // 
            resources.ApplyResources(this.cbbObjectType, "cbbObjectType");
            this.cbbObjectType.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            valueListItem7.DataValue = ((short)(1));
            resources.ApplyResources(valueListItem7, "valueListItem7");
            valueListItem7.ForceApplyResources = "";
            valueListItem6.DataValue = ((short)(0));
            resources.ApplyResources(valueListItem6, "valueListItem6");
            valueListItem6.ForceApplyResources = "";
            valueListItem3.DataValue = ((short)(2));
            resources.ApplyResources(valueListItem3, "valueListItem3");
            valueListItem3.ForceApplyResources = "";
            valueListItem5.DataValue = ((short)(3));
            resources.ApplyResources(valueListItem5, "valueListItem5");
            valueListItem5.ForceApplyResources = "";
            this.cbbObjectType.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem7,
            valueListItem6,
            valueListItem3,
            valueListItem5});
            this.cbbObjectType.Name = "cbbObjectType";
            this.cbbObjectType.ValueChanged += new System.EventHandler(this.cbbObjectType_ValueChanged_1);
            // 
            // ultraLabel1
            // 
            appearance30.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ultraLabel1.Appearance = appearance30;
            resources.ApplyResources(this.ultraLabel1, "ultraLabel1");
            this.ultraLabel1.Name = "ultraLabel1";
            // 
            // ultraTabControl1
            // 
            this.ultraTabControl1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl2);
            resources.ApplyResources(this.ultraTabControl1, "ultraTabControl1");
            this.ultraTabControl1.Name = "ultraTabControl1";
            this.ultraTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            resources.ApplyResources(ultraTab1, "ultraTab1");
            ultraTab1.ForceApplyResources = "";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            resources.ApplyResources(ultraTab2, "ultraTab2");
            ultraTab2.ForceApplyResources = "";
            this.ultraTabControl1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            // 
            // ultraTabSharedControlsPage1
            // 
            resources.ApplyResources(this.ultraTabSharedControlsPage1, "ultraTabSharedControlsPage1");
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            // 
            // FAccountingObjectCustomersDetail
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ultraTabControl1);
            this.Controls.Add(this.ultraLabel1);
            this.Controls.Add(this.cbbObjectType);
            this.Controls.Add(this.ultraLabel21);
            this.Controls.Add(this.cbbScaleType);
            this.Controls.Add(this.ultraLabel22);
            this.Controls.Add(this.pn_action);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FAccountingObjectCustomersDetail";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FAccountingObjectCustomersDetail_FormClosed);
            this.ultraTabPageControl1.ResumeLayout(false);
            this.ultraTabPageControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactOfficeTel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIssueBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactHomeTel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountObjectGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIssueDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactMobile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbbContactSex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdentificationNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPaymentClauseID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWebsite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccounttingObjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccounttingObjectCode)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            this.cms4Grid.ResumeLayout(false);
            this.pn_action.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ckeIsActive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsLabourUnionFree)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsInsured)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsEmployee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbScaleType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbObjectType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).EndInit();
            this.ultraTabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor ckeIsActive;
        private System.Windows.Forms.Panel pn_action;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsEmployee;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsLabourUnionFree;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsInsured;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraButton btnSaveContinue;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbObjectType;
        private Infragistics.Win.Misc.UltraLabel ultraLabel21;
        private Infragistics.Win.Misc.UltraLabel ultraLabel22;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbScaleType;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbPaymentClauseID;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtDueTime;
        private Infragistics.Win.Misc.UltraLabel ultraLabel19;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAddress;
        private Infragistics.Win.Misc.UltraLabel ultraLabel26;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtMaximizaDebtAmount;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtFax;
        private Infragistics.Win.Misc.UltraLabel ultraLabel27;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtWebsite;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountObjectGroup;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.Misc.UltraLabel ultraLabel25;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtEmail;
        private Infragistics.Win.Misc.UltraLabel lblGroupName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccounttingObjectName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTel;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTaxCode;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel lblGroupCode;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccounttingObjectCode;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
        private System.Windows.Forms.ContextMenuStrip cms4Grid;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripMenuItem tsmDelete;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtContactOfficeTel;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtIssueBy;
        private Infragistics.Win.Misc.UltraLabel ultraLabel24;
        private Infragistics.Win.Misc.UltraLabel ultraLabel17;
        private Infragistics.Win.Misc.UltraLabel labenoicap;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtContactHomeTel;
        private Infragistics.Win.Misc.UltraLabel ultraLabel18;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtContactEmail;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtContactAddress;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor txtIssueDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel23;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtContactMobile;
        private Infragistics.Win.Misc.UltraLabel labengaycap;
        private Infragistics.Win.Misc.UltraLabel ultraLabel16;
        private Infragistics.Win.Misc.UltraLabel ultraLabel15;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor CbbContactSex;
        private Infragistics.Win.Misc.UltraLabel ultraLabel34;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtIdentificationNo;
        private Infragistics.Win.Misc.UltraLabel ultraLabel13;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtContactTitle;
        private Infragistics.Win.Misc.UltraLabel ultraLabel14;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtContactName;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
    }
}