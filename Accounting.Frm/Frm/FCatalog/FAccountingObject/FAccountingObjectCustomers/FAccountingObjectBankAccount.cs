﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Infragistics.Win.UltraWinGrid;
using Accounting.TextMessage;
using System.ComponentModel;
using System.Linq;

namespace Accounting
{
    public partial class FAccountingObjectBankAccount : CatalogBase //UserControl
    {
        #region khai báo
        public static List<AccountingObjectBankAccount> dsAccountingObjectBankAccount = new List<AccountingObjectBankAccount>();
        public static List<AccountingObjectBankAccount> backdsAccountingObjectBankAccount = new List<AccountingObjectBankAccount>();
        #endregion

        #region khởi tạo
        public FAccountingObjectBankAccount(List<AccountingObjectBankAccount> dsAccountingObjectBankAccount)
        {
            InitializeComponent();
            backdsAccountingObjectBankAccount = dsAccountingObjectBankAccount;
            uGrid.DataSource = new BindingList<AccountingObjectBankAccount>(dsAccountingObjectBankAccount);

            //config
            uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            uGrid.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
            uGrid.DisplayLayout.Override.TemplateAddRowPrompt = "Bấm vào đây để thêm mới....";
            uGrid.DisplayLayout.Override.SpecialRowSeparator = Infragistics.Win.UltraWinGrid.SpecialRowSeparator.None;
            uGrid.DisplayLayout.Override.AllowMultiCellOperations = Infragistics.Win.UltraWinGrid.AllowMultiCellOperation.All;
            //Utils.ConfigGrid(uGrid, ConstDatabase.AccountingObjectBankAccount_TableName, false);
            Utils.ConfigGrid(uGrid, ConstDatabase.AccountingObjectBankAccount_TableName, new List<TemplateColumn>(), true);
        }
        #endregion

        #region Nghiệp vụ
        #region ContentMenu
        //private void tsmDelete_Click(object sender, EventArgs e)
        //{
        //    if (uGrid.Selected.Rows.Count > 0)
        //    {
        //        AccountingObjectBankAccount temp = uGrid.Selected.Rows[0].ListObject as AccountingObjectBankAccount;
        //        dsAccountingObjectBankAccount.Remove(temp);
        //        uGrid.DataSource = new BindingList<AccountingObjectBankAccount>(dsAccountingObjectBankAccount);
        //    }
        //    else
        //        MSG.Error(string.Format(resSystem.MSG_Catalog2, "Tài khoản ngân hàng"));
        //}
        #endregion

        private void uGrid_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }
        #endregion

        #region Utils
        //void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        //{//hàm chung
        //    Utils.ConfigGrid(utralGrid, TextMessage.ConstDatabase.AccountingObjectBankAccount_TableName, false);
        //}
        #endregion

        private void btnClose_Click(object sender, EventArgs e)
        {
            //foreach (AccountingObjectBankAccount obj in dsAccountingObjectBankAccount)
            //{
            //    obj.IsSelect = false;
            //}
            dsAccountingObjectBankAccount = backdsAccountingObjectBankAccount;
            this.Close();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            //if ()
            dsAccountingObjectBankAccount = ((BindingList<AccountingObjectBankAccount>)uGrid.DataSource).ToList();
            //Utils.ListAccountingObjectBank.Clear();
            Close();
        }

        //private void btnExit_Click(object sender, EventArgs e)
        //{
        //    this.Close();
        //}

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            if (uGrid.ActiveCell != null)
            {
                UltraGridRow row = null;
                if (uGrid.ActiveCell != null || uGrid.ActiveRow != null)
                {
                    row = uGrid.ActiveCell != null ? uGrid.ActiveCell.Row : uGrid.ActiveRow;
                }
                else
                {
                    if (uGrid.Rows.Count > 0) row = uGrid.Rows[uGrid.Rows.Count - 1];
                }
                if (row != null) row.Delete(false);
                uGrid.Update();
            }
        }

        private void tsmAdd_Click(object sender, EventArgs e)
        {
            uGrid.AddNewRow4Grid();
        }

    }
}
