﻿namespace Accounting
{
    partial class FAccountingObjectCustomers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.labeBankName = new Infragistics.Win.Misc.UltraLabel();
            this.labeBankAcount = new Infragistics.Win.Misc.UltraLabel();
            this.labeWebsite = new Infragistics.Win.Misc.UltraLabel();
            this.labeEmail = new Infragistics.Win.Misc.UltraLabel();
            this.labeTaxCode = new Infragistics.Win.Misc.UltraLabel();
            this.labeFax = new Infragistics.Win.Misc.UltraLabel();
            this.labePhone = new Infragistics.Win.Misc.UltraLabel();
            this.labeAddress = new Infragistics.Win.Misc.UltraLabel();
            this.labeName = new Infragistics.Win.Misc.UltraLabel();
            this.labeID = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.tab1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGridVoucher = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uPalBottom = new Infragistics.Win.Misc.UltraPanel();
            this.btnViewVoucher = new Infragistics.Win.Misc.UltraButton();
            this.ultraLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.dteToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.cbbAboutTime = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.dteFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.cms4Grid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.tmsReLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraSplitter1 = new Infragistics.Win.Misc.UltraSplitter();
            this.TabInformation = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnAdd = new Infragistics.Win.Misc.UltraButton();
            this.btnDelete = new Infragistics.Win.Misc.UltraButton();
            this.btnEdit = new Infragistics.Win.Misc.UltraButton();
            this.btnExportExcel = new Infragistics.Win.Misc.UltraButton();
            this.ultraTabPageControl1.SuspendLayout();
            this.tab1.SuspendLayout();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGridVoucher)).BeginInit();
            this.uPalBottom.ClientArea.SuspendLayout();
            this.uPalBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dteToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAboutTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFromDate)).BeginInit();
            this.cms4Grid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TabInformation)).BeginInit();
            this.TabInformation.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.labeBankName);
            this.ultraTabPageControl1.Controls.Add(this.labeBankAcount);
            this.ultraTabPageControl1.Controls.Add(this.labeWebsite);
            this.ultraTabPageControl1.Controls.Add(this.labeEmail);
            this.ultraTabPageControl1.Controls.Add(this.labeTaxCode);
            this.ultraTabPageControl1.Controls.Add(this.labeFax);
            this.ultraTabPageControl1.Controls.Add(this.labePhone);
            this.ultraTabPageControl1.Controls.Add(this.labeAddress);
            this.ultraTabPageControl1.Controls.Add(this.labeName);
            this.ultraTabPageControl1.Controls.Add(this.labeID);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel6);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel7);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel8);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel9);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel10);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel5);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel4);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel3);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel2);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel1);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(798, 175);
            // 
            // labeBankName
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            this.labeBankName.Appearance = appearance1;
            this.labeBankName.Location = new System.Drawing.Point(680, 132);
            this.labeBankName.Name = "labeBankName";
            this.labeBankName.Size = new System.Drawing.Size(225, 25);
            this.labeBankName.TabIndex = 19;
            // 
            // labeBankAcount
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            this.labeBankAcount.Appearance = appearance2;
            this.labeBankAcount.Location = new System.Drawing.Point(680, 101);
            this.labeBankAcount.Name = "labeBankAcount";
            this.labeBankAcount.Size = new System.Drawing.Size(225, 25);
            this.labeBankAcount.TabIndex = 18;
            // 
            // labeWebsite
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            this.labeWebsite.Appearance = appearance3;
            this.labeWebsite.Location = new System.Drawing.Point(680, 72);
            this.labeWebsite.Name = "labeWebsite";
            this.labeWebsite.Size = new System.Drawing.Size(225, 23);
            this.labeWebsite.TabIndex = 17;
            // 
            // labeEmail
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            this.labeEmail.Appearance = appearance4;
            this.labeEmail.Location = new System.Drawing.Point(680, 43);
            this.labeEmail.Name = "labeEmail";
            this.labeEmail.Size = new System.Drawing.Size(225, 23);
            this.labeEmail.TabIndex = 16;
            // 
            // labeTaxCode
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            this.labeTaxCode.Appearance = appearance5;
            this.labeTaxCode.Location = new System.Drawing.Point(680, 14);
            this.labeTaxCode.Name = "labeTaxCode";
            this.labeTaxCode.Size = new System.Drawing.Size(225, 23);
            this.labeTaxCode.TabIndex = 15;
            // 
            // labeFax
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            this.labeFax.Appearance = appearance6;
            this.labeFax.Location = new System.Drawing.Point(132, 132);
            this.labeFax.Name = "labeFax";
            this.labeFax.Size = new System.Drawing.Size(225, 25);
            this.labeFax.TabIndex = 14;
            // 
            // labePhone
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            this.labePhone.Appearance = appearance7;
            this.labePhone.Location = new System.Drawing.Point(132, 101);
            this.labePhone.Name = "labePhone";
            this.labePhone.Size = new System.Drawing.Size(225, 25);
            this.labePhone.TabIndex = 13;
            // 
            // labeAddress
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            this.labeAddress.Appearance = appearance8;
            this.labeAddress.Location = new System.Drawing.Point(131, 72);
            this.labeAddress.Name = "labeAddress";
            this.labeAddress.Size = new System.Drawing.Size(390, 23);
            this.labeAddress.TabIndex = 12;
            // 
            // labeName
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            this.labeName.Appearance = appearance9;
            this.labeName.Location = new System.Drawing.Point(129, 43);
            this.labeName.Name = "labeName";
            this.labeName.Size = new System.Drawing.Size(228, 23);
            this.labeName.TabIndex = 11;
            // 
            // labeID
            // 
            appearance10.BackColor = System.Drawing.Color.Transparent;
            this.labeID.Appearance = appearance10;
            this.labeID.Location = new System.Drawing.Point(132, 14);
            this.labeID.Name = "labeID";
            this.labeID.Size = new System.Drawing.Size(225, 23);
            this.labeID.TabIndex = 10;
            // 
            // ultraLabel6
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel6.Appearance = appearance11;
            this.ultraLabel6.Location = new System.Drawing.Point(551, 132);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(118, 25);
            this.ultraLabel6.TabIndex = 9;
            this.ultraLabel6.Text = "Tên ngân hàng :";
            // 
            // ultraLabel7
            // 
            appearance12.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel7.Appearance = appearance12;
            this.ultraLabel7.Location = new System.Drawing.Point(551, 101);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(118, 25);
            this.ultraLabel7.TabIndex = 8;
            this.ultraLabel7.Text = "Tài khoản ngân hàng :";
            // 
            // ultraLabel8
            // 
            appearance13.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel8.Appearance = appearance13;
            this.ultraLabel8.Location = new System.Drawing.Point(551, 72);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(118, 23);
            this.ultraLabel8.TabIndex = 7;
            this.ultraLabel8.Text = "Website :";
            // 
            // ultraLabel9
            // 
            appearance14.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel9.Appearance = appearance14;
            this.ultraLabel9.Location = new System.Drawing.Point(551, 43);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(118, 23);
            this.ultraLabel9.TabIndex = 6;
            this.ultraLabel9.Text = "Email :";
            // 
            // ultraLabel10
            // 
            appearance15.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel10.Appearance = appearance15;
            this.ultraLabel10.Location = new System.Drawing.Point(551, 14);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(118, 23);
            this.ultraLabel10.TabIndex = 5;
            this.ultraLabel10.Text = "Mã số thuế :";
            // 
            // ultraLabel5
            // 
            appearance16.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel5.Appearance = appearance16;
            this.ultraLabel5.Location = new System.Drawing.Point(3, 132);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(124, 25);
            this.ultraLabel5.TabIndex = 4;
            this.ultraLabel5.Text = "Fax :";
            // 
            // ultraLabel4
            // 
            appearance17.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel4.Appearance = appearance17;
            this.ultraLabel4.Location = new System.Drawing.Point(3, 101);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(123, 25);
            this.ultraLabel4.TabIndex = 3;
            this.ultraLabel4.Text = "Điện thoại : ";
            // 
            // ultraLabel3
            // 
            appearance18.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel3.Appearance = appearance18;
            this.ultraLabel3.Location = new System.Drawing.Point(2, 72);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(124, 23);
            this.ultraLabel3.TabIndex = 2;
            this.ultraLabel3.Text = "Địa chỉ :";
            // 
            // ultraLabel2
            // 
            appearance19.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel2.Appearance = appearance19;
            this.ultraLabel2.Location = new System.Drawing.Point(3, 43);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(123, 23);
            this.ultraLabel2.TabIndex = 1;
            this.ultraLabel2.Text = "Tên KH/NCC : ";
            // 
            // ultraLabel1
            // 
            appearance20.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel1.Appearance = appearance20;
            this.ultraLabel1.Location = new System.Drawing.Point(3, 14);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(123, 23);
            this.ultraLabel1.TabIndex = 0;
            this.ultraLabel1.Text = "Mã KH/NCC :";
            // 
            // tab1
            // 
            this.tab1.Controls.Add(this.ultraPanel1);
            this.tab1.Controls.Add(this.uPalBottom);
            this.tab1.Location = new System.Drawing.Point(-10000, -10000);
            this.tab1.Name = "tab1";
            this.tab1.Size = new System.Drawing.Size(798, 175);
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraGridVoucher);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(798, 142);
            this.ultraPanel1.TabIndex = 2;
            // 
            // ultraGridVoucher
            // 
            appearance21.BackColor = System.Drawing.SystemColors.Window;
            appearance21.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGridVoucher.DisplayLayout.Appearance = appearance21;
            this.ultraGridVoucher.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.ultraGridVoucher.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGridVoucher.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance22.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance22.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance22.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGridVoucher.DisplayLayout.GroupByBox.Appearance = appearance22;
            appearance23.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGridVoucher.DisplayLayout.GroupByBox.BandLabelAppearance = appearance23;
            this.ultraGridVoucher.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance24.BackColor2 = System.Drawing.SystemColors.Control;
            appearance24.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance24.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGridVoucher.DisplayLayout.GroupByBox.PromptAppearance = appearance24;
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGridVoucher.DisplayLayout.Override.ActiveCellAppearance = appearance25;
            appearance26.BackColor = System.Drawing.SystemColors.Highlight;
            appearance26.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGridVoucher.DisplayLayout.Override.ActiveRowAppearance = appearance26;
            this.ultraGridVoucher.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGridVoucher.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance27.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGridVoucher.DisplayLayout.Override.CardAreaAppearance = appearance27;
            appearance28.BorderColor = System.Drawing.Color.Silver;
            appearance28.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGridVoucher.DisplayLayout.Override.CellAppearance = appearance28;
            this.ultraGridVoucher.DisplayLayout.Override.CellPadding = 0;
            appearance29.BackColor = System.Drawing.SystemColors.Control;
            appearance29.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance29.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance29.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance29.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGridVoucher.DisplayLayout.Override.GroupByRowAppearance = appearance29;
            appearance30.TextHAlignAsString = "Left";
            this.ultraGridVoucher.DisplayLayout.Override.HeaderAppearance = appearance30;
            this.ultraGridVoucher.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            appearance31.BorderColor = System.Drawing.Color.Silver;
            this.ultraGridVoucher.DisplayLayout.Override.RowAppearance = appearance31;
            appearance32.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGridVoucher.DisplayLayout.Override.TemplateAddRowAppearance = appearance32;
            this.ultraGridVoucher.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGridVoucher.Location = new System.Drawing.Point(0, 0);
            this.ultraGridVoucher.Name = "ultraGridVoucher";
            this.ultraGridVoucher.Size = new System.Drawing.Size(798, 142);
            this.ultraGridVoucher.TabIndex = 1;
            this.ultraGridVoucher.Text = "ultraGrid1";
            this.ultraGridVoucher.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.ultraGridVoucher_DoubleClickRow);
            // 
            // uPalBottom
            // 
            // 
            // uPalBottom.ClientArea
            // 
            this.uPalBottom.ClientArea.Controls.Add(this.btnViewVoucher);
            this.uPalBottom.ClientArea.Controls.Add(this.ultraLabel13);
            this.uPalBottom.ClientArea.Controls.Add(this.dteToDate);
            this.uPalBottom.ClientArea.Controls.Add(this.cbbAboutTime);
            this.uPalBottom.ClientArea.Controls.Add(this.ultraLabel12);
            this.uPalBottom.ClientArea.Controls.Add(this.dteFromDate);
            this.uPalBottom.ClientArea.Controls.Add(this.ultraLabel11);
            this.uPalBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uPalBottom.Location = new System.Drawing.Point(0, 142);
            this.uPalBottom.Name = "uPalBottom";
            this.uPalBottom.Size = new System.Drawing.Size(798, 33);
            this.uPalBottom.TabIndex = 0;
            // 
            // btnViewVoucher
            // 
            this.btnViewVoucher.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnViewVoucher.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2010Button;
            this.btnViewVoucher.Location = new System.Drawing.Point(714, 4);
            this.btnViewVoucher.Name = "btnViewVoucher";
            this.btnViewVoucher.Size = new System.Drawing.Size(75, 23);
            this.btnViewVoucher.TabIndex = 0;
            this.btnViewVoucher.Text = "&Xem ";
            this.btnViewVoucher.Click += new System.EventHandler(this.btnViewVoucher_Click);
            // 
            // ultraLabel13
            // 
            appearance33.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel13.Appearance = appearance33;
            this.ultraLabel13.Location = new System.Drawing.Point(501, 6);
            this.ultraLabel13.Name = "ultraLabel13";
            this.ultraLabel13.Size = new System.Drawing.Size(25, 19);
            this.ultraLabel13.TabIndex = 75;
            this.ultraLabel13.Text = "Đến";
            // 
            // dteToDate
            // 
            appearance34.TextHAlignAsString = "Center";
            appearance34.TextVAlignAsString = "Middle";
            this.dteToDate.Appearance = appearance34;
            this.dteToDate.AutoSize = false;
            this.dteToDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteToDate.Location = new System.Drawing.Point(532, 5);
            this.dteToDate.MaskInput = "dd/mm/yyyy";
            this.dteToDate.Name = "dteToDate";
            this.dteToDate.Size = new System.Drawing.Size(85, 22);
            this.dteToDate.TabIndex = 73;
            this.dteToDate.Value = null;
            this.dteToDate.ValueChanged += new System.EventHandler(this.dteToDate_ValueChanged);
            // 
            // cbbAboutTime
            // 
            this.cbbAboutTime.AutoSize = false;
            this.cbbAboutTime.Location = new System.Drawing.Point(91, 4);
            this.cbbAboutTime.Name = "cbbAboutTime";
            this.cbbAboutTime.Size = new System.Drawing.Size(222, 23);
            this.cbbAboutTime.TabIndex = 71;
            this.cbbAboutTime.ValueChanged += new System.EventHandler(this.cbbAboutTime_ValueChanged);
            // 
            // ultraLabel12
            // 
            appearance35.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel12.Appearance = appearance35;
            this.ultraLabel12.Location = new System.Drawing.Point(369, 6);
            this.ultraLabel12.Name = "ultraLabel12";
            this.ultraLabel12.Size = new System.Drawing.Size(17, 19);
            this.ultraLabel12.TabIndex = 74;
            this.ultraLabel12.Text = "Từ";
            // 
            // dteFromDate
            // 
            appearance36.TextHAlignAsString = "Center";
            appearance36.TextVAlignAsString = "Middle";
            this.dteFromDate.Appearance = appearance36;
            this.dteFromDate.AutoSize = false;
            this.dteFromDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteFromDate.Location = new System.Drawing.Point(392, 4);
            this.dteFromDate.MaskInput = "dd/mm/yyyy";
            this.dteFromDate.Name = "dteFromDate";
            this.dteFromDate.Size = new System.Drawing.Size(93, 23);
            this.dteFromDate.TabIndex = 72;
            this.dteFromDate.Value = null;
            this.dteFromDate.ValueChanged += new System.EventHandler(this.dteFromDate_ValueChanged);
            // 
            // ultraLabel11
            // 
            appearance37.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel11.Appearance = appearance37;
            this.ultraLabel11.Location = new System.Drawing.Point(-1, 6);
            this.ultraLabel11.Name = "ultraLabel11";
            this.ultraLabel11.Size = new System.Drawing.Size(89, 19);
            this.ultraLabel11.TabIndex = 70;
            this.ultraLabel11.Text = "Khoảng thời gian";
            // 
            // cms4Grid
            // 
            this.cms4Grid.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.cms4Grid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.tsmAdd,
            this.tsmEdit,
            this.tsmDelete,
            this.tmsReLoad});
            this.cms4Grid.Name = "cms4Grid";
            this.cms4Grid.Size = new System.Drawing.Size(164, 162);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(160, 6);
            // 
            // tsmAdd
            // 
            this.tsmAdd.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.tsmAdd.Name = "tsmAdd";
            this.tsmAdd.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.tsmAdd.Size = new System.Drawing.Size(163, 38);
            this.tsmAdd.Text = "Thêm";
            this.tsmAdd.Click += new System.EventHandler(this.tsmAdd_Click);
            // 
            // tsmEdit
            // 
            this.tsmEdit.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            this.tsmEdit.Name = "tsmEdit";
            this.tsmEdit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.tsmEdit.Size = new System.Drawing.Size(163, 38);
            this.tsmEdit.Text = "Sửa";
            this.tsmEdit.Click += new System.EventHandler(this.tsmEdit_Click);
            // 
            // tsmDelete
            // 
            this.tsmDelete.Image = global::Accounting.Properties.Resources.ubtnDelete;
            this.tsmDelete.Name = "tsmDelete";
            this.tsmDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.tsmDelete.Size = new System.Drawing.Size(163, 38);
            this.tsmDelete.Text = "Xóa";
            this.tsmDelete.Click += new System.EventHandler(this.tsmDelete_Click);
            // 
            // tmsReLoad
            // 
            this.tmsReLoad.Image = global::Accounting.Properties.Resources.ubtnReset;
            this.tmsReLoad.Name = "tmsReLoad";
            this.tmsReLoad.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.tmsReLoad.Size = new System.Drawing.Size(163, 38);
            this.tmsReLoad.Text = "Tải Lại";
            this.tmsReLoad.Click += new System.EventHandler(this.tmsReLoad_Click);
            // 
            // uGrid
            // 
            this.uGrid.ContextMenuStrip = this.cms4Grid;
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.AlignWithDataRows;
            this.uGrid.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.uGrid.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGrid.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGrid.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGrid.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.uGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid.Location = new System.Drawing.Point(0, 52);
            this.uGrid.Name = "uGrid";
            this.uGrid.Size = new System.Drawing.Size(802, 134);
            this.uGrid.TabIndex = 8;
            this.uGrid.Text = "uGrid";
            this.uGrid.BeforeRowActivate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.uGrid_BeforeRowActivate);
            this.uGrid.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGrid_DoubleClickRow);
            this.uGrid.MouseDown += new System.Windows.Forms.MouseEventHandler(this.uGrid_MouseDown);
            // 
            // ultraSplitter1
            // 
            this.ultraSplitter1.BackColor = System.Drawing.SystemColors.Control;
            this.ultraSplitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraSplitter1.Location = new System.Drawing.Point(0, 186);
            this.ultraSplitter1.Name = "ultraSplitter1";
            this.ultraSplitter1.RestoreExtent = 213;
            this.ultraSplitter1.Size = new System.Drawing.Size(802, 13);
            this.ultraSplitter1.TabIndex = 35;
            // 
            // TabInformation
            // 
            this.TabInformation.Controls.Add(this.ultraTabSharedControlsPage1);
            this.TabInformation.Controls.Add(this.ultraTabPageControl1);
            this.TabInformation.Controls.Add(this.tab1);
            this.TabInformation.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.TabInformation.Location = new System.Drawing.Point(0, 199);
            this.TabInformation.Name = "TabInformation";
            this.TabInformation.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.TabInformation.Size = new System.Drawing.Size(802, 201);
            this.TabInformation.TabIndex = 9;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "1 Thông tin chung";
            ultraTab2.TabPage = this.tab1;
            ultraTab2.Text = "2  Chứng từ";
            this.TabInformation.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(798, 175);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnExportExcel);
            this.panel1.Controls.Add(this.btnAdd);
            this.panel1.Controls.Add(this.btnDelete);
            this.panel1.Controls.Add(this.btnEdit);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(802, 52);
            this.panel1.TabIndex = 7;
            // 
            // btnAdd
            // 
            appearance41.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.btnAdd.Appearance = appearance41;
            this.btnAdd.Location = new System.Drawing.Point(10, 12);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 30);
            this.btnAdd.TabIndex = 14;
            this.btnAdd.Text = "Thêm";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click_1);
            // 
            // btnDelete
            // 
            appearance42.Image = global::Accounting.Properties.Resources.xoa1_xoa_du_lieu_ke_toan_khoi_database_;
            this.btnDelete.Appearance = appearance42;
            this.btnDelete.Location = new System.Drawing.Point(172, 12);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 30);
            this.btnDelete.TabIndex = 13;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click_1);
            // 
            // btnEdit
            // 
            appearance43.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            this.btnEdit.Appearance = appearance43;
            this.btnEdit.Location = new System.Drawing.Point(91, 12);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(75, 30);
            this.btnEdit.TabIndex = 12;
            this.btnEdit.Text = "Sửa";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click_1);
            // 
            // btnExportExcel
            // 
            appearance38.BackColor = System.Drawing.Color.DarkRed;
            appearance38.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance38.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance38.Image = global::Accounting.Properties.Resources.Excel1;
            this.btnExportExcel.Appearance = appearance38;
            this.btnExportExcel.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance39.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance39.BorderAlpha = Infragistics.Win.Alpha.Opaque;
            this.btnExportExcel.HotTrackAppearance = appearance39;
            this.btnExportExcel.Location = new System.Drawing.Point(253, 12);
            this.btnExportExcel.Name = "btnExportExcel";
            appearance40.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.btnExportExcel.PressedAppearance = appearance40;
            this.btnExportExcel.Size = new System.Drawing.Size(75, 30);
            this.btnExportExcel.TabIndex = 64;
            this.btnExportExcel.Text = "Kết xuất";
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // FAccountingObjectCustomers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(802, 400);
            this.Controls.Add(this.uGrid);
            this.Controls.Add(this.ultraSplitter1);
            this.Controls.Add(this.TabInformation);
            this.Controls.Add(this.panel1);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FAccountingObjectCustomers";
            this.Text = "Khách hàng nhà cung cấp";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FAccountingObjectCustomers_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FAccountingObjectCustomers_FormClosed);
            this.ultraTabPageControl1.ResumeLayout(false);
            this.tab1.ResumeLayout(false);
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGridVoucher)).EndInit();
            this.uPalBottom.ClientArea.ResumeLayout(false);
            this.uPalBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dteToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAboutTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFromDate)).EndInit();
            this.cms4Grid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TabInformation)).EndInit();
            this.TabInformation.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
        private System.Windows.Forms.Panel panel1;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl TabInformation;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tab1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel labeFax;
        private Infragistics.Win.Misc.UltraLabel labePhone;
        private Infragistics.Win.Misc.UltraLabel labeAddress;
        private Infragistics.Win.Misc.UltraLabel labeName;
        private Infragistics.Win.Misc.UltraLabel labeID;
        private Infragistics.Win.Misc.UltraLabel labeBankName;
        private Infragistics.Win.Misc.UltraLabel labeBankAcount;
        private Infragistics.Win.Misc.UltraLabel labeWebsite;
        private Infragistics.Win.Misc.UltraLabel labeEmail;
        private Infragistics.Win.Misc.UltraLabel labeTaxCode;
        private System.Windows.Forms.ContextMenuStrip cms4Grid;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripMenuItem tsmEdit;
        private System.Windows.Forms.ToolStripMenuItem tsmDelete;
        private System.Windows.Forms.ToolStripMenuItem tmsReLoad;
        private Infragistics.Win.Misc.UltraButton btnAdd;
        private Infragistics.Win.Misc.UltraButton btnDelete;
        private Infragistics.Win.Misc.UltraButton btnEdit;
        private Infragistics.Win.Misc.UltraPanel uPalBottom;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGridVoucher;
        private Infragistics.Win.Misc.UltraButton btnViewVoucher;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAboutTime;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteFromDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteToDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel12;
        private Infragistics.Win.Misc.UltraLabel ultraLabel13;
        private Infragistics.Win.Misc.UltraSplitter ultraSplitter1;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        public Infragistics.Win.Misc.UltraButton btnExportExcel;
    }
}