﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Accounting.TextMessage;
using System.Text.RegularExpressions;
using System.ComponentModel;
using Infragistics.Win.UltraWinGrid;
using Accounting.Core;

namespace Accounting
{
    public partial class FAccountingObjectCustomersDetail : DialogForm
    {
        #region khai báo
        private IAccountingObjectService _IAccountingObjectService;
        //private IBankAccountDetailService _IBankAccountDetailService;
        private IAccountingObjectCategoryService _IAccountingObjectCategoryService;
        private IAccountingObjectGroupService _IAccountingObjectGroupService;
        private IPaymentClauseService _IPaymentClauseService;
        //private IAccountingObjectBankAccountService _IAccountingObjectBankAccountService;
        private IGenCodeService _IGenCodeService;
        AccountingObject _select = new AccountingObject();
        public static List<AccountingObjectBankAccount> dsAccountingObjectBankAccount = new List<AccountingObjectBankAccount>();
        public static List<AccountingObjectBankAccount> backdsAccountingObjectBankAccount = new List<AccountingObjectBankAccount>();
        public AccountingObject SelectAccounting
        {
            get { return _select; }
            set { _select = value; }
        }

        private Guid _id;
        public Guid Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public static string AccounttingObjectCode;
        bool IsAdd = true;
        public static bool isClose = true;
        #endregion

        #region Khởi tạo
        public FAccountingObjectCustomersDetail(AccountingObject temp, bool IsAdd)
        {
            #region Khởi tạo giá trị ban đấu
            InitializeComponent();
            this.CenterToParent();
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Text = "Thêm mới thông tin khách hàng, NCC";
            SetDefault();
            SetEnabledChk();

            switch (temp.ObjectType)
            {
                case 0:     // TH 1 >> admin
                    cbbObjectType.Value = 0;
                    break;

                case 1:     //TH 2 >> head
                    cbbObjectType.Value = 1;

                    break;

                case 2:     //Th 3 >> assigneed

                    cbbObjectType.Value = 2;

                    break;

                case 3:     //Th 4 >> different

                    cbbObjectType.Value = 3;

                    break;
            }
            #endregion
            #region Thiết lập ban đầu cho Form
            _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();
            //_IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            _IAccountingObjectCategoryService = IoC.Resolve<IAccountingObjectCategoryService>();
            _IAccountingObjectGroupService = IoC.Resolve<IAccountingObjectGroupService>();
            _IPaymentClauseService = IoC.Resolve<IPaymentClauseService>();
            //_IAccountingObjectBankAccountService = IoC.Resolve<IAccountingObjectBankAccountService>();
            _IGenCodeService = IoC.Resolve<IGenCodeService>();
            InitializeGUI();
            txtAccounttingObjectCode.Text = Utils.TaoMaChungTu(_IGenCodeService.getGenCode(100));
            backdsAccountingObjectBankAccount = dsAccountingObjectBankAccount;
            uGrid.DataSource = new BindingList<AccountingObjectBankAccount>(dsAccountingObjectBankAccount);

            //config
            uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            uGrid.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
            uGrid.DisplayLayout.Override.TemplateAddRowPrompt = "Bấm vào đây để thêm mới....";
            uGrid.DisplayLayout.Override.SpecialRowSeparator = Infragistics.Win.UltraWinGrid.SpecialRowSeparator.None;
            uGrid.DisplayLayout.Override.AllowMultiCellOperations = Infragistics.Win.UltraWinGrid.AllowMultiCellOperation.All;
            //Utils.ConfigGrid(uGrid, ConstDatabase.AccountingObjectBankAccount_TableName, false);
            Utils.ConfigGrid(uGrid, ConstDatabase.AccountingObjectBankAccount_TableName, new List<TemplateColumn>(), true);
            uGrid.DisplayLayout.Bands[0].Columns["IsSelect"].Hidden = true;
            #endregion
            if (Utils.isPPInvoice == true)
            {
                int i = 0;
                while (cbbObjectType.Items.Count > 0)
                {
                    cbbObjectType.Items.RemoveAt(i);
                }
                cbbObjectType.Items.Add(1, "Nhà cung cấp");
                cbbObjectType.Items.Add(2, "Khách hàng/ Nhà cung cấp");
                cbbObjectType.Value = 1;
            }
            else if (Utils.isSAInvoice == true)
            {
                int i = 0;
                while (cbbObjectType.Items.Count > 0)
                {
                    cbbObjectType.Items.RemoveAt(i);
                }
                cbbObjectType.Items.Add(0, "Khách hàng");
                cbbObjectType.Items.Add(2, "Khách hàng/ Nhà cung cấp");
                cbbObjectType.Value = 0;
            }
        }
        public FAccountingObjectCustomersDetail()
        {
            #region Khởi tạo giá trị ban đấu
            InitializeComponent();
            this.CenterToParent();
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Text = "Thêm mới thông tin khách hàng, NCC";
            SetDefault();
            SetEnabledChk();


            #endregion
            #region Thiết lập ban đầu cho Form
            _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();
            //_IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            _IAccountingObjectCategoryService = IoC.Resolve<IAccountingObjectCategoryService>();
            _IAccountingObjectGroupService = IoC.Resolve<IAccountingObjectGroupService>();
            _IPaymentClauseService = IoC.Resolve<IPaymentClauseService>();
            //_IAccountingObjectBankAccountService = IoC.Resolve<IAccountingObjectBankAccountService>();
            _IGenCodeService = IoC.Resolve<IGenCodeService>();
            txtAccounttingObjectCode.Text = Utils.TaoMaChungTu(_IGenCodeService.getGenCode(100));
            Utils.ClearCacheByType<AccountingObject>();
            Utils.ClearCacheByType<AccountingObjectBankAccount>();
            InitializeGUI();
            cbbAccountObjectGroup.ReadOnly = false;
            //cbbAcountingObjectCategory.ReadOnly = false;
            cbbPaymentClauseID.ReadOnly = false;
            cbbAccountObjectGroup.Enabled = true;
            //cbbAcountingObjectCategory.Enabled = true;
            cbbPaymentClauseID.Enabled = true;
            //cbbScaleType.Focused=true;

            uGrid.DataSource = new BindingList<AccountingObjectBankAccount>(dsAccountingObjectBankAccount);

            //config
            uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            uGrid.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
            uGrid.DisplayLayout.Override.TemplateAddRowPrompt = "Bấm vào đây để thêm mới....";
            uGrid.DisplayLayout.Override.SpecialRowSeparator = Infragistics.Win.UltraWinGrid.SpecialRowSeparator.None;
            uGrid.DisplayLayout.Override.AllowMultiCellOperations = Infragistics.Win.UltraWinGrid.AllowMultiCellOperation.All;
            //Utils.ConfigGrid(uGrid, ConstDatabase.AccountingObjectBankAccount_TableName, false);
            Utils.ConfigGrid(uGrid, ConstDatabase.AccountingObjectBankAccount_TableName, new List<TemplateColumn>(), true);
            uGrid.DisplayLayout.Bands[0].Columns["IsSelect"].Hidden = true;
            #endregion
        }
        public FAccountingObjectCustomersDetail(string ma)
        {
            #region Khởi tạo giá trị ban đấu
            InitializeComponent();
            this.CenterToParent();
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Text = "Thêm mới thông tin khách hàng, NCC";
            SetDefault();
            SetEnabledChk();


            #endregion
            #region Thiết lập ban đầu cho Form
            _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();
            //_IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            _IAccountingObjectCategoryService = IoC.Resolve<IAccountingObjectCategoryService>();
            _IAccountingObjectGroupService = IoC.Resolve<IAccountingObjectGroupService>();
            _IPaymentClauseService = IoC.Resolve<IPaymentClauseService>();
            //_IAccountingObjectBankAccountService = IoC.Resolve<IAccountingObjectBankAccountService>();
            _IGenCodeService = IoC.Resolve<IGenCodeService>();
            txtAccounttingObjectCode.Text = Utils.TaoMaChungTu(_IGenCodeService.getGenCode(100));
            Utils.ClearCacheByType<AccountingObject>();
            Utils.ClearCacheByType<AccountingObjectBankAccount>();
            InitializeGUI();
            cbbAccountObjectGroup.ReadOnly = false;
            //cbbAcountingObjectCategory.ReadOnly = false;
            cbbPaymentClauseID.ReadOnly = false;
            cbbAccountObjectGroup.Enabled = true;
            // cbbAcountingObjectCategory.Enabled = true;
            cbbPaymentClauseID.Enabled = true;
            //cbbScaleType.Focused=true;
            #endregion
            if (ma == "1")
            { cbbObjectType.SelectedIndex = 1; }

            uGrid.DataSource = new BindingList<AccountingObjectBankAccount>(dsAccountingObjectBankAccount);

            //config
            uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            uGrid.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
            uGrid.DisplayLayout.Override.TemplateAddRowPrompt = "Bấm vào đây để thêm mới....";
            uGrid.DisplayLayout.Override.SpecialRowSeparator = Infragistics.Win.UltraWinGrid.SpecialRowSeparator.None;
            uGrid.DisplayLayout.Override.AllowMultiCellOperations = Infragistics.Win.UltraWinGrid.AllowMultiCellOperation.All;
            //Utils.ConfigGrid(uGrid, ConstDatabase.AccountingObjectBankAccount_TableName, false);
            Utils.ConfigGrid(uGrid, ConstDatabase.AccountingObjectBankAccount_TableName, new List<TemplateColumn>(), true);
            uGrid.DisplayLayout.Bands[0].Columns["IsSelect"].Hidden = true;
        }
        public FAccountingObjectCustomersDetail(AccountingObject temp)
        {
            //Sửa
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            this.CenterToParent();
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Text = "Sửa thông tin khách hàng, NCC";
            txtAccounttingObjectCode.Enabled = false;
            SetEnabledChk();
            txtIssueDate.Enabled = true;
            #endregion
            #region Thiết lập ban đầu cho Form
            _select = temp;
            
            IsAdd = false;
            //txtAccounttingObjectCode.Enabled = false;

            //Khai báo các webservices

            #region Thiết lập ban đầu cho Form
            _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();
            //_IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            _IAccountingObjectCategoryService = IoC.Resolve<IAccountingObjectCategoryService>();
            _IAccountingObjectGroupService = IoC.Resolve<IAccountingObjectGroupService>();
            _IPaymentClauseService = IoC.Resolve<IPaymentClauseService>();
            _IGenCodeService = IoC.Resolve<IGenCodeService>();
            //_IAccountingObjectBankAccountService = IoC.Resolve<IAccountingObjectBankAccountService>();
            //InitializeGUI();
            #endregion
            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            //Utils.ClearCacheByType<AccountingObject>();   trungnq sửa lỗi 6182 click 3 lần đối tượng bị lỗi đỏ
            //Utils.ClearCacheByType<AccountingObjectBankAccount>();
            InitializeGUI();
            dsAccountingObjectBankAccount = temp.BankAccounts.ToList();
            uGrid.DataSource = new BindingList<AccountingObjectBankAccount>(dsAccountingObjectBankAccount);

            //config
            uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            uGrid.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
            uGrid.DisplayLayout.Override.TemplateAddRowPrompt = "Bấm vào đây để thêm mới....";
            uGrid.DisplayLayout.Override.SpecialRowSeparator = Infragistics.Win.UltraWinGrid.SpecialRowSeparator.None;
            uGrid.DisplayLayout.Override.AllowMultiCellOperations = Infragistics.Win.UltraWinGrid.AllowMultiCellOperation.All;
            //Utils.ConfigGrid(uGrid, ConstDatabase.AccountingObjectBankAccount_TableName, false);
            Utils.ConfigGrid(uGrid, ConstDatabase.AccountingObjectBankAccount_TableName, new List<TemplateColumn>(), true);
            uGrid.DisplayLayout.Bands[0].Columns["IsSelect"].Hidden = true;
            #endregion

            #region Fill dữ liệu Obj vào control
            ObjandGUI(temp, false);
            #endregion
        }
        #endregion
        private void InitializeGUI()
        {
            if (!IsAdd)
            {

                // if (FAccountingObjectBankAccount.dsAccountingObjectBankAccount == null) FAccountingObjectBankAccount.dsAccountingObjectBankAccount = new List<AccountingObjectBankAccount>();

                //foreach (AccountingObjectBankAccount item1 in FAccountingObjectBankAccount.dsAccountingObjectBankAccount)
                //    FAccountingObjectBankAccount.dsAccountingObjectBankAccount.Remove(item1);

                dsAccountingObjectBankAccount.Clear();
                dsAccountingObjectBankAccount.AddRange(_select.BankAccounts);  // = (List<AccountingObjectBankAccount>)Utils.CloneObject(_Select.BankAccounts);
            }
            else
            {
                dsAccountingObjectBankAccount = new BindingList<AccountingObjectBankAccount>(Utils.CloneObject(_select.BankAccounts)).ToList();
            }
            //cbbBankAccountDetailID.DataSource = _IBankAccountDetailService.GetAll().OrderBy(p => p.BankAccount).Where(p=>p.IsActive==true).ToList();
            //cbbBankAccountDetailID.DisplayMember = "BankAccount";
            //Utils.ConfigGrid(cbbBankAccountDetailID, ConstDatabase.BankAccountDetail_TableName);

            // cbbAccountObjectGroup.DataSource = _IAccountingObjectGroupService.GetAll().OrderBy(p => p.AccountingObjectGroupCode).Where(p => p.IsActive == true).ToList();
            cbbAccountObjectGroup.DataSource = _IAccountingObjectGroupService.GetListByIsActiveOrderCode(true);
            cbbAccountObjectGroup.DisplayMember = "AccountingObjectGroupName";
            Utils.ConfigGrid(cbbAccountObjectGroup, ConstDatabase.AccountingObjectGroup_TableName);

            // cbbAcountingObjectCategory.DataSource = _IAccountingObjectCategoryService.GetAll().OrderBy(p => p.AccountingObjectCategoryCode).Where(p => p.IsActive == true).ToList();
            //cbbAcountingObjectCategory.DataSource = _IAccountingObjectCategoryService.GetByIsActiveOrderByCode(true);
            // cbbAcountingObjectCategory.DisplayMember = "AccountingObjectCategoryName";
            // Utils.ConfigGrid(cbbAcountingObjectCategory, ConstDatabase.AccountingObjectCategory_TableName);

            // cbbPaymentClauseID.DataSource = _IPaymentClauseService.GetAll().OrderBy(p => p.PaymentClauseCode).Where(p => p.IsActive == true).ToList();
            cbbPaymentClauseID.DataSource = _IPaymentClauseService.GetIsActive(true).OrderBy(c => c.PaymentClauseCode).ToList();
            cbbPaymentClauseID.DisplayMember = "PaymentClauseName";
            Utils.ConfigGrid(cbbPaymentClauseID, ConstDatabase.PaymentClause_TableName);
            txtMaximizaDebtAmount.FormatNumberic(ConstDatabase.Format_TienVND);

        }
        #region Utils
        /// <summary>
        /// get hoặc set một đối tượng vào giao diện xử lý
        /// </summary>
        /// <param name="input">đối tượng</param>
        /// <param name="isGet">True: get giá trị từ control gán vào đối tượng| False: set giá trị đối tượng vào control</param>
        /// <returns></returns>
        AccountingObject ObjandGUI(AccountingObject input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();  //Thêm mới

                input.AccountingObjectCode = txtAccounttingObjectCode.Text;
                input.AccountingObjectName = txtAccounttingObjectName.Text;
                // input.AccountingObjectCategory = cbb.Text;
                //if (!txtBankAccount.Text.Equals(""))
                //{
                //    input.BankAccount = txtBankAccount.Text;
                //}
                //else {

                //    input.BankAccount = null;
                //}
                input.Address = txtAddress.Text;
                input.Tel = txtTel.Text;
                input.Fax = txtFax.Text;
                input.Email = txtEmail.Text;
                input.Website = txtWebsite.Text;
                if (!txtMaximizaDebtAmount.Text.Equals(""))
                {
                    decimal maximizaDebtAmount = 0;
                    decimal.TryParse(txtMaximizaDebtAmount.Text, out maximizaDebtAmount);
                    input.MaximizaDebtAmount = maximizaDebtAmount;
                }
                else
                {
                    input.MaximizaDebtAmount = 0;
                }

                if (!txtDueTime.Text.Equals(""))
                {
                    input.DueTime = int.Parse(txtDueTime.Text);
                }
                else
                {
                    input.DueTime = null;
                }
                ////BankAccountDetail temp = (BankAccountDetail)Utils.getSelectCbbItem(cbbBankAccountDetailID);
                ////if (temp == null) input.BankAccountDetailID = null;
                ////else input.BankAccountDetailID = temp.ID;
                //Nhom doi tuong ke toan 
                //input.BankAccount = txtBankAccount.Text;
                AccountingObjectGroup temp1 = (AccountingObjectGroup)Utils.getSelectCbbItem(cbbAccountObjectGroup);
                if (temp1 == null) input.AccountObjectGroupID = null;
                else input.AccountObjectGroupID = temp1.ID;

                //Loai doi tuong ke toan 
                //   AccountingObjectCategory temp2 = (AccountingObjectCategory)Utils.getSelectCbbItem(cbbAcountingObjectCategory);
                //  if (temp2 == null) input.AccountingObjectCategory = null;
                //  else input.AccountingObjectCategory = temp2.AccountingObjectCategoryCode.ToString();

                //Dieu khoan thanh toan
                PaymentClause temp3 = (PaymentClause)Utils.getSelectCbbItem(cbbPaymentClauseID);
                if (temp3 == null) input.PaymentClauseID = null;
                else input.PaymentClauseID = temp3.ID;
                //input.BankName = txtBankName.Text;
                input.TaxCode = txtTaxCode.Text;
                //input.Description = txtDescripton.Text;
                input.ContactName = txtContactName.Text;
                input.ContactTitle = txtContactTitle.Text;
                if (CbbContactSex.Value != null)
                {
                    int sex = CbbContactSex.SelectedIndex;
                    int.TryParse(CbbContactSex.Value.ToString(), out sex);
                    input.ContactSex = sex;
                }
                else input.ContactSex = null;
                input.ContactMobile = txtContactMobile.Text;
                input.ContactEmail = txtContactEmail.Text;
                input.ContactHomeTel = txtContactHomeTel.Text;
                input.ContactOfficeTel = txtContactOfficeTel.Text;
                input.ContactAddress = txtContactAddress.Text;
                //input.ScaleType = int.Parse(cbbScaleType.SelectedItem.DataValue.ToString());

                int scaleType = 0;
                if (cbbScaleType.Value != null)
                {
                    int.TryParse(cbbScaleType.Value.ToString(), out scaleType);
                }
                input.ScaleType = scaleType;

                int objectType = 0;
                if (cbbObjectType.Value != null)
                {
                    int.TryParse(cbbObjectType.SelectedItem.DataValue.ToString(), out objectType);
                }
                input.ObjectType = objectType;

                input.IsEmployee = chkIsEmployee.CheckState == CheckState.Checked ? true : false;
                input.IdentificationNo = txtIdentificationNo.Text;
                //try
                //{
                //    input.IssueDate = DateTime.Parse(txtIssueDate.Text);
                //}
                //catch (Exception)
                //{

                //    input.IssueDate = null;
                //}
                input.IssueDate = Utils.StringToDateTime(txtIssueDate.Text);
                input.IssueBy = txtIssueBy.Text;
                input.IsInsured = chkIsInsured.CheckState == CheckState.Checked ? true : false;
                input.IsLabourUnionFree = chkIsLabourUnionFree.CheckState == CheckState.Checked ? true : false;
                input.IsActive = ckeIsActive.CheckState == CheckState.Checked ? true : false;
                input.BankAccounts.Clear();
                for (int i = 0; i < uGrid.Rows.Count; i++)
                {
                    var BankAccount = uGrid.Rows[i].Cells["BankAccount"].Value;
                    var BankName = uGrid.Rows[i].Cells["BankName"].Value;
                    var BankBranchName = uGrid.Rows[i].Cells["BankBranchName"].Value;
                    var AccountHolderName = uGrid.Rows[i].Cells["AccountHolderName"].Value;
                    if (BankAccount != null && BankName != null && BankBranchName != null && AccountHolderName != null)
                    {
                        input.BankAccounts.Add(new AccountingObjectBankAccount
                        {
                            AccountingObjectID = input.ID,
                            BankAccount = BankAccount.ToString(),
                            BankName = BankName.ToString(),
                            BankBranchName = BankBranchName.ToString(),
                            AccountHolderName = AccountHolderName.ToString(),
                        });
                    }

                }
            }
            else
            {
                txtAccounttingObjectCode.Text = input.AccountingObjectCode;
                txtAccounttingObjectName.Text = input.AccountingObjectName;
                ////foreach (var item in cbbBankAccountDetailID.Rows)
                ////{
                ////    if ((item.ListObject as BankAccountDetail).ID == input.BankAccountDetailID) cbbBankAccountDetailID.SelectedRow = item;

                ////}
                // cbbBankAccountDetailID.Text = input.BankAccount;
                //txtEmployeeBirthday.Text = input.EmployeeBirthday.ToString();

                txtAddress.Text = input.Address;
                txtTel.Text = input.Tel;
                txtFax.Text = input.Fax;
                txtEmail.Text = input.Email;
                txtWebsite.Text = input.Website;
                uGrid.DataSource = new BindingList<AccountingObjectBankAccount>(input.BankAccounts.ToList());
                //txtBankName.Text = input.BankName;
                txtTaxCode.Text = input.TaxCode;
                //txtDescripton.Text = input.Description;
                txtContactName.Text = input.ContactName;
                txtContactTitle.Text = input.ContactTitle;
                //input.ContactSex = int.Parse(CbbContactSex.SelectedItem.DisplayText);
                CbbContactSex.Text = input.ContactSex.ToString();
                txtContactMobile.Text = input.ContactMobile;
                txtContactEmail.Text = input.ContactEmail;
                txtContactHomeTel.Text = input.ContactHomeTel;
                txtContactOfficeTel.Text = input.ContactOfficeTel;
                txtContactAddress.Text = input.ContactAddress;
                cbbScaleType.SelectedIndex = input.ScaleType ?? -1;
                cbbObjectType.Value = input.ObjectType ?? -1;
                //input.IsEmployee = chkIsEmployee.CheckState == CheckState.Checked ? false : true;
                chkIsEmployee.CheckState = input.IsEmployee ? CheckState.Checked : CheckState.Unchecked;
                txtIdentificationNo.Text = input.IdentificationNo;
                txtIssueDate.Text = input.IssueDate == null ? "" : input.IssueDate.ToString();
                txtIssueBy.Text = input.IssueBy;
                //Phòng Ban
                //foreach (var item in cbbDepartment.Rows)
                //{
                //    if ((item.ListObject as Department).ID == input.DepartmentID) cbbDepartment.SelectedRow = item;
                //}
                //input.IsInsured = chkIsInsured.CheckState == CheckState.Checked ? false : true;
                chkIsInsured.CheckState = input.IsInsured ? CheckState.Checked : CheckState.Unchecked;
                //input.IsLabourUnionFree = chkIsLabourUnionFree.CheckState == CheckState.Checked ? false : true;
                chkIsLabourUnionFree.CheckState = input.IsLabourUnionFree ? CheckState.Checked : CheckState.Unchecked;
                //txtFamilyDeductionAmunt.Text = input.FamilyDeductionAmount.ToString();
                txtMaximizaDebtAmount.Text = input.MaximizaDebtAmount.ToString();
                txtDueTime.Text = input.DueTime.ToString();
                //foreach (var item in cbbAcountingObjectCategory.Rows)
                //{
                //    if ((item.ListObject as AccountingObjectCategory).AccountingObjectCategoryCode == input.AccountingObjectCategory) cbbAcountingObjectCategory.SelectedRow = item;
                //}
                //Nhóm đối tượng ké toán
                foreach (var item in cbbAccountObjectGroup.Rows)
                {
                    if ((item.ListObject as AccountingObjectGroup).ID == input.AccountObjectGroupID) cbbAccountObjectGroup.SelectedRow = item;
                }
                //Diều khoản thanh toán
                foreach (var item in cbbPaymentClauseID.Rows)
                {
                    if ((item.ListObject as PaymentClause).ID == input.PaymentClauseID) cbbPaymentClauseID.SelectedRow = item;
                }
                ckeIsActive.CheckState = input.IsActive ? CheckState.Checked : CheckState.Unchecked;
            }
            return input;
        }

        bool CheckError()
        {
            bool kq = true;
            //Check Error Chung
            if (!txtTaxCode.Text.Equals("") && !string.IsNullOrEmpty(txtTaxCode.Text))
            {
                if (!Core.Utils.CheckMST(txtTaxCode.Text))
                {
                    MSG.Error(resSystem.MSG_System_44);
                    txtTaxCode.Focus();
                    return false;
                }
            }
            else
            {
            }
            // check tên nhóm khách hàng , loại khách hàng, điều khoản thanh toán 
            if ((!cbbAccountObjectGroup.Text.IsNullOrEmpty()))
            {
                if ((cbbAccountObjectGroup.Text != "<Chọn dữ liệu>"))
                {
                    AccountingObjectGroup temp1 = (AccountingObjectGroup)Utils.getSelectCbbItem(cbbAccountObjectGroup);
                    if (temp1 == null)
                    {
                        MSG.Error("Nhóm khách hàng , nhà cung cấp không tồn tại ");
                        return false;
                    }
                }
            }
            //if ((!cbbAcountingObjectCategory.Text.StrIsNullOrEmpty()) )
            //{
            //    if ((cbbAcountingObjectCategory.Text != "<Chọn dữ liệu>"))
            //    {
            //        AccountingObjectCategory temp1 = (AccountingObjectCategory)Utils.getSelectCbbItem(cbbAcountingObjectCategory);
            //        if (temp1 == null)
            //        {
            //            MSG.Error("Loại khách hàng , nhà cung cấp không tồn tại ");
            //            return false;
            //        }
            //    }
            //}
            if ((!cbbPaymentClauseID.Text.StrIsNullOrEmpty()))
            {
                if ((cbbPaymentClauseID.Text != "<Chọn dữ liệu>"))
                {
                    PaymentClause temp1 = (PaymentClause)Utils.getSelectCbbItem(cbbPaymentClauseID);
                    if (temp1 == null)
                    {
                        MSG.Error("Điều khoản thanh toán không tồn tại ");
                        return false;
                    }
                }
            }
            //Check Error Riêng từng Form
            if (string.IsNullOrEmpty(txtAccounttingObjectCode.Text) || string.IsNullOrEmpty(txtAccounttingObjectName.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }
            // chek Email

            string patent = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
              @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
              @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

            Regex objNotWholePattern = new Regex(patent);
            // Bắt kiếu số cho loại sổ lưu
            Match Email = objNotWholePattern.Match(txtEmail.Text);
            if (!txtEmail.Text.Equals(""))
            {
                if (!Email.Success)
                {
                    if (cbbScaleType.Text == "Tổ chức")
                        MSG.Error(string.Format(resSystem.MSG_System_58, "thông tin tổ chức"));
                    else if (cbbScaleType.Text == "Cá nhân")
                        MSG.Error(string.Format(resSystem.MSG_System_58, "thông tin cá nhân"));
                    txtEmail.Focus();
                    return false;

                }
            }

            string patent2 = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
            @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
            @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

            Regex objNotWholePattern2 = new Regex(patent2);
            // Bắt kiếu số cho loại sổ lưu
            Match Email2 = objNotWholePattern2.Match(txtContactEmail.Text);
            if (!txtContactEmail.Text.Equals(""))
            {
                if (!Email2.Success)
                {
                    MSG.Error(string.Format(resSystem.MSG_System_58, "thông tin người liên hệ"));
                    txtContactEmail.Focus();
                    return false;

                }
            }
            //decimal val;
            //bool validDecimal = decimal.TryParse(txtMaximizaDebtAmount.Text, out val);
            //if (!validDecimal) {
            //    MSG.Error("Số nợ tối đa phải là số ' ex: 1.0000' ");
            //     txtMaximizaDebtAmount.Focus();
            //    return false;
            //}
            //isvalid = false;
            string Number = "^\\d+$";
            Regex ChekNumber = new Regex(Number);
            Match Num2 = ChekNumber.Match(txtDueTime.Text);
            if (!txtDueTime.Text.Equals(""))
            {
                if (!Num2.Success)
                {
                    MSG.Error(resSystem.MSG_Catalog_AccountingObject1);
                    txtDueTime.Focus();
                    return false;
                }
            }

            //chek Code
            // List<string> list = _IAccountingObjectService.Query.Select(p => p.AccountingObjectCode).ToList();
            List<string> list = _IAccountingObjectService.GetListAccountingObjectCode();

            foreach (var item in list)
            {
                if (item.ToUpper() == txtAccounttingObjectCode.Text.ToUpper() && IsAdd)
                {
                    MSG.Error("Mã : " + item + " Đã tồn tại ");
                    txtAccounttingObjectCode.Focus();
                    return false;
                }

            }

            return kq;
        }
        #endregion
        #region Set  Giá trị cho hàm khởi tạo ban đầu
        public void SetDefault()
        {
            txtIssueDate.Value = null;
            cbbObjectType.SelectedIndex = 0;
            cbbScaleType.SelectedIndex = 1;
            chkIsEmployee.Visible = false;
            chkIsInsured.Visible = false;
            chkIsLabourUnionFree.Visible = false;
            ckeIsActive.Visible = false;
            ckeIsActive.Checked = true;
            cbbAccountObjectGroup.Text = cbbAccountObjectGroup.NullText;
            //cbbAcountingObjectCategory.Text = cbbAcountingObjectCategory.NullText;
            cbbPaymentClauseID.Text = cbbPaymentClauseID.NullText;
        }
        #endregion
        #region Set  Giá trị cho hàm khởi tạo ban đầu SetEnabledChk
        public void SetEnabledChk()
        {
            chkIsEmployee.Visible = true;
            chkIsInsured.Visible = false;
            chkIsLabourUnionFree.Visible = false;
        }
        #endregion
        #region Event
        private void btnClose_Click(object sender, EventArgs e)
        {
            #region xử lý form, kết thúc form
            //dsAccountingObjectBankAccount = backdsAccountingObjectBankAccount;
            DialogResult = DialogResult.Ignore;
            AccounttingObjectCode = null;
            Dispose(true);
            this.Close();
            #endregion
            
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion

            #region Fill dữ liệu control vào obj
            AccountingObject temp = IsAdd ? new AccountingObject() : _IAccountingObjectService.Getbykey(_select.ID);
            temp = ObjandGUI(temp, true);
            #endregion

            #region Thao tác CSDL
            try
            {
                _IAccountingObjectService.BeginTran();
                //temp.BankAccounts.AddA;                
                if (IsAdd)
                {
                    _IAccountingObjectService.CreateNew(temp);
                    _IGenCodeService.UpdateGenCodeCatalog(100, txtAccounttingObjectCode.Text);
                }
                else _IAccountingObjectService.Update(temp);
                //thao tac vao bang AccountingObjectBankAccount
                //foreach (AccountingObjectBankAccount item in FAccountingObjectBankAccount.dsAccountingObjectBankAccount) item.AccountingObjectID = temp.ID;
                // List<AccountingObjectBankAccount> AccountingObjectBankAccounts = _IAccountingObjectBankAccountService.Query.Where(k => k.AccountingObjectID == temp.ID).ToList();
                //List<AccountingObjectBankAccount> AccountingObjectBankAccounts = _IAccountingObjectBankAccountService.GetByAccountingObjectID(temp.ID);
                //if (AccountingObjectBankAccounts != null)
                //    foreach (AccountingObjectBankAccount item in AccountingObjectBankAccounts) _IAccountingObjectBankAccountService.Delete(item);
                //foreach (AccountingObjectBankAccount item in FAccountingObjectBankAccount.dsAccountingObjectBankAccount) _IAccountingObjectBankAccountService.CreateNew(item);
                _IAccountingObjectService.CommitTran();
                //temp.BankAccounts.Clear(); 
                //temp.BankAccounts = (List<AccountingObjectBankAccount>)Utils.CloneObject(FAccountingObjectBankAccount.dsAccountingObjectBankAccount);
                _id = temp.ID;

                #region xử lý form, kết thúc form
                AccounttingObjectCode = txtAccounttingObjectCode.Text;
                this.SelectAccounting = temp;
                DialogResult = DialogResult.OK;

                Utils.ClearCacheByType<AccountingObject>();
                Utils.ClearCacheByType<AccountingObjectBankAccount>();
                //BindingList<AccountingObject> lst = Utils.ListAccountingObject;
                Post_Customers(temp); //add by Hautv
                this.Close();
                isClose = false;
                #endregion
            }
            catch (Exception ex)
            {

                _IAccountingObjectService.RolbackTran();
            }
            #endregion
        }

        private void btnSaveContinue_Click(object sender, EventArgs e)
        {
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion

            #region Fill dữ liệu control vào obj
            AccountingObject temp = IsAdd ? new AccountingObject() : _IAccountingObjectService.Getbykey(_select.ID);
            temp = ObjandGUI(temp, true);
            #endregion

            #region Thao tác CSDL
            try
            {
                _IAccountingObjectService.BeginTran();
                if (IsAdd)
                {
                    _IAccountingObjectService.CreateNew(temp);
                    _IGenCodeService.UpdateGenCodeCatalog(100, txtAccounttingObjectCode.Text);
                }
                else _IAccountingObjectService.Update(temp);
                _IAccountingObjectService.CommitTran();
                _id = temp.ID;

                #region xử lý form, kết thúc form
                AccounttingObjectCode = txtAccounttingObjectCode.Text;
                this.SelectAccounting = temp;

                Utils.ClearCacheByType<AccountingObject>();
                Utils.ClearCacheByType<AccountingObjectBankAccount>();
                BindingList<AccountingObject> lst = Utils.ListAccountingObject;
                Post_Customers(temp); //add by Hautv
                #endregion

                // reset form 
                _select = new AccountingObject();

                IsAdd = true;
                isClose = false;
                txtAccounttingObjectCode.Enabled = true;
                ObjandGUI(_select, false);
                uGrid.DataSource = new BindingList<AccountingObjectBankAccount>();
                Utils.ClearCacheByType<AccountingObject>();
                Utils.ClearCacheByType<AccountingObjectBankAccount>();
                SetDefault();
                SetEnabledChk();
                txtAccounttingObjectCode.Text = Utils.TaoMaChungTu(_IGenCodeService.getGenCode(100));
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                _IAccountingObjectService.RolbackTran();
            }
            #endregion
        }

        private void txtTaxCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= 48 && e.KeyChar <= 57 || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void cbbBankAccountDetailID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            //try
            //{
            //    bool T = true;
            //    new FAccountingObjectBankAccount(FAccountingObjectBankAccount.dsAccountingObjectBankAccount).ShowDialog(this);
            //    foreach (AccountingObjectBankAccount item in FAccountingObjectBankAccount.dsAccountingObjectBankAccount)
            //    {
            //        if (item.IsSelect)
            //        {
            //            T = false;
            //            txtBankAccount.Text = item.BankAccount;
            //            txtBankName.Text = item.BankName;
            //            break;
            //        }
            //    }
            //    if (T)
            //    {
            //        txtBankAccount.Text = null;
            //        txtBankName.Text = null;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            //    return;
            //}
        }

        private void FAccountingObjectCustomersDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            FAccountingObjectBankAccount.dsAccountingObjectBankAccount.Clear();
        }


        private void txtIdentificationNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= 48 && e.KeyChar <= 57 || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
        #region Set vị trí .....
        private void cbbScaleType_ValueChanged(object sender, EventArgs e)
        {
            #region Code cũ
            //if (cbbScaleType.SelectedIndex == 1)
            //{
            //    chkIsEmployee.Visible = true;
            //    if(IsAdd)
            //        chkIsEmployee.Location = ckeIsActive.Location;
            //    else
            //        chkIsEmployee.Location = chkIsInsured.Location;
            //    UtraThongtintochuc.Text = "Thông tin cá nhân";
            //    this.Size = new Size(945, 300);
            //    UtraThongtintochuc.Size = new Size(938, 350);
            //    ultraGroupBox1.Size = new Size(938, 31);
            //    ultraGroupBox1.Dock = DockStyle.Top;
            //    ultraGroupBox1.Location = new Point(0, 0);
            //    UtraThongtintochuc.Location = new Point(1, 30);
            //    UtraThongtintochuc.Dock = DockStyle.Bottom;
            //    uttraGroupboxTTNLL.Hide();
            //    //GMASOTHUE.Hide();
            //    GDIENTHOAI.Hide();
            //    GFAX.Hide();
            //    GCHUCDANHNGLIENHE.Hide();
            //    GGIOITINH.Hide();
            //    GEMAILNGLIENHE.Hide();
            //    GDTNHARIENG.Hide();
            //    GDIACHILIENHE.Hide();
            //    GCHUCDANHNGLIENHE.Hide();
            //    GGIOITINH.Show();
            //    GDC.Show();


            //    GDTCOQUAN.Show();
            //    GDTNGLIENHE.Show();
            //    GTENNGLIENHE.Hide();
            //    GSOCMTNN.Show();
            //    GNOICAP.Show();
            //    GNGAYCAP.Show();

            //    GMDTKT.Location = new Point(12, 39);
            //    UtraThongtintochuc.Controls.Add(GMDTKT);
            //    GMDTKT.TabIndex = 101;

            //    GTDTKT.Location = new Point(12, 72);
            //    UtraThongtintochuc.Controls.Add(GTDTKT);
            //    GTDTKT.TabIndex = 102;

            //    GNDTKT.Location = new Point(12, 106);
            //    UtraThongtintochuc.Controls.Add(GNDTKT);
            //    GNDTKT.TabIndex = 103;

            //    GLDTKT.Location = new Point(12, 140);
            //    UtraThongtintochuc.Controls.Add(GLDTKT);
            //    GLDTKT.TabIndex = 104;

            //    GGIOITINH.Location = new Point(12, 207);
            //    UtraThongtintochuc.Controls.Add(GGIOITINH);
            //    GGIOITINH.TabIndex = 106;

            //    GDC.Location = new Point(12, 231);
            //    UtraThongtintochuc.Controls.Add(GDC);
            //    GDC.TabIndex = 107;

            //    GEMAIL.Location = new Point(12, 174);
            //    UtraThongtintochuc.Controls.Add(GEMAIL);
            //    GEMAIL.TabIndex = 105;

            //    GTENNGLIENHE.Location = new Point(318, 30);
            //    UtraThongtintochuc.Controls.Add(GTENNGLIENHE);
            //    GTENNGLIENHE.TabIndex = 108;

            //    GDTNGLIENHE.Location = new Point(318, 40);
            //    UtraThongtintochuc.Controls.Add(GDTNGLIENHE);
            //    GDTNGLIENHE.TabIndex = 109;


            //    GDTCOQUAN.Location = new Point(318, 72);
            //    UtraThongtintochuc.Controls.Add(GDTCOQUAN);
            //    GDTCOQUAN.TabIndex = 110;

            //    GSOCMTNN.Location = new Point(318, 106);
            //    UtraThongtintochuc.Controls.Add(GSOCMTNN);
            //    ultraLabel34.Location = new Point(9, 3);
            //    GSOCMTNN.Controls.Add(ultraLabel34);
            //    GSOCMTNN.TabIndex = 111;

            //    GNGAYCAP.Location = new Point(318, 140);
            //    UtraThongtintochuc.Controls.Add(GNGAYCAP);
            //    labengaycap.Location = new Point(9, 4);
            //    GNGAYCAP.Controls.Add(labengaycap);
            //    GNGAYCAP.TabIndex = 112;

            //    GNOICAP.Location = new Point(318, 174);
            //    UtraThongtintochuc.Controls.Add(GNOICAP);
            //    labenoicap.Location = new Point(10, 5);
            //    GNOICAP.Controls.Add(labenoicap);
            //    GNOICAP.TabIndex = 113;

            //    GWEBSITE.Location = new Point(318, 207);
            //    UtraThongtintochuc.Controls.Add(GWEBSITE);
            //    GWEBSITE.TabIndex = 114;

            //    GMASOTHUE.Location = new Point(318, 241);
            //    UtraThongtintochuc.Controls.Add(GMASOTHUE);
            //    GMASOTHUE.TabIndex = 115;

            //    GSOTAIKHOANNGANHANG.Location = new Point(624, 40);
            //    UtraThongtintochuc.Controls.Add(GSOTAIKHOANNGANHANG);
            //    GSOTAIKHOANNGANHANG.TabIndex = 116;

            //    GTENNGANHANG.Location = new Point(624, 78);
            //    UtraThongtintochuc.Controls.Add(GTENNGANHANG);
            //    GTENNGANHANG.TabIndex = 117;

            //    GSONOTOIDA.Location = new Point(624, 112);
            //    UtraThongtintochuc.Controls.Add(GSONOTOIDA);
            //    GSONOTOIDA.TabIndex = 118;

            //    GHANNO.Location = new Point(624, 146);
            //    UtraThongtintochuc.Controls.Add(GHANNO);
            //    GHANNO.TabIndex = 119;

            //    GDIEUKHOANTHANHTOAN.Location = new Point(624, 180);
            //    UtraThongtintochuc.Controls.Add(GDIEUKHOANTHANHTOAN);
            //    GDIEUKHOANTHANHTOAN.TabIndex = 120;

            //    GMOTA.Location = new Point(624, 214);
            //    UtraThongtintochuc.Controls.Add(GMOTA);
            //    GMOTA.TabIndex = 121;

            //    pn_action.Location = new Point(12, 275);
            //    pn_action.Size = new Size(927, 37);
            //}
            //else
            //{
            //    chkIsEmployee.Visible = false;
            //    chkIsEmployee.Location = chkIsInsured.Location;
            //    UtraThongtintochuc.Text = "Thông tin tổ chức";
            //    uttraGroupboxTTNLL.Show();
            //    //UtraThongtintochuc.Controls.Clear();
            //    // uttraGroupboxTTNLL.Controls.Clear();
            //    this.Size = new Size(928, 440);
            //    UtraThongtintochuc.Size = new Size(938, 381);
            //    UtraThongtintochuc.Location = new Point(0, 29);
            //    UtraThongtintochuc.Dock = DockStyle.Bottom;
            //    ultraGroupBox1.Size = new Size(938, 31);
            //    ultraGroupBox1.Dock = DockStyle.Top;
            //    ultraGroupBox1.Location = new Point(0, 0);
            //    uttraGroupboxTTNLL.Size = new Size(938, 158);
            //    uttraGroupboxTTNLL.Location = new Point(5, 177);
            //    uttraGroupboxTTNLL.TabIndex = 1018;
            //    pn_action.TabIndex = 1029;

            //    GMASOTHUE.Show();
            //    GDIENTHOAI.Show();
            //    GFAX.Show();
            //    GCHUCDANHNGLIENHE.Show();
            //    GGIOITINH.Show();
            //    GEMAILNGLIENHE.Show();
            //    GDTNHARIENG.Show();
            //    GDIACHILIENHE.Show();
            //    GCHUCDANHNGLIENHE.Show();
            //    GGIOITINH.Show();
            //    GDTCOQUAN.Show();
            //    GDTNGLIENHE.Show();
            //    GTENNGLIENHE.Show();
            //    GSOCMTNN.Show();
            //    GNOICAP.Show();
            //    GNGAYCAP.Show();

            //    GMDTKT.Location = new Point(0, 19);
            //    UtraThongtintochuc.Controls.Add(GMDTKT);
            //    GMDTKT.TabIndex = 121;

            //    GTDTKT.Location = new Point(0, 46);
            //    UtraThongtintochuc.Controls.Add(GTDTKT);
            //    GTDTKT.TabIndex = 122;

            //    GNDTKT.Location = new Point(0, 73);
            //    UtraThongtintochuc.Controls.Add(GNDTKT);
            //    GNDTKT.TabIndex = 123;

            //    GLDTKT.Location = new Point(0, 100);
            //    UtraThongtintochuc.Controls.Add(GLDTKT);
            //    GLDTKT.TabIndex = 124;


            //    GDC.Location = new Point(0, 127);
            //    UtraThongtintochuc.Controls.Add(GDC);
            //    GDC.TabIndex = 125;

            //    GDIENTHOAI.Location = new Point(306, 19);
            //    UtraThongtintochuc.Controls.Add(GDIENTHOAI);
            //    GDIENTHOAI.TabIndex = 126;

            //    GEMAIL.Location = new Point(306, 46);
            //    UtraThongtintochuc.Controls.Add(GEMAIL);
            //    ultraLabel8.Location = new Point(11, 3);
            //    GEMAIL.Controls.Add(ultraLabel8);
            //    GEMAIL.TabIndex = 127;

            //    GWEBSITE.Location = new Point(306, 73);
            //    UtraThongtintochuc.Controls.Add(GWEBSITE);
            //    GWEBSITE.TabIndex = 128;

            //    GFAX.Location = new Point(306, 99);
            //    UtraThongtintochuc.Controls.Add(GFAX);
            //    GFAX.TabIndex = 129;

            //    GMOTA.Location = new Point(306, 127);
            //    UtraThongtintochuc.Controls.Add(GMOTA);
            //    GMOTA.TabIndex = 130;

            //    GMASOTHUE.Location = new Point(612, 22);
            //    UtraThongtintochuc.Controls.Add(GMASOTHUE);
            //    GMASOTHUE.TabIndex = 131;

            //    GSOTAIKHOANNGANHANG.Location = new Point(612, 49);
            //    UtraThongtintochuc.Controls.Add(GSOTAIKHOANNGANHANG);
            //    GSOTAIKHOANNGANHANG.TabIndex = 132;

            //    GTENNGANHANG.Location = new Point(612, 76);
            //    UtraThongtintochuc.Controls.Add(GTENNGANHANG);
            //    GTENNGANHANG.TabIndex = 133;

            //    GSONOTOIDA.Location = new Point(612, 100);
            //    UtraThongtintochuc.Controls.Add(GSONOTOIDA);
            //    GSONOTOIDA.TabIndex = 134;

            //    GHANNO.Location = new Point(612, 126);
            //    UtraThongtintochuc.Controls.Add(GHANNO);
            //    GHANNO.TabIndex = 135;

            //    GDIEUKHOANTHANHTOAN.Location = new Point(612, 153);
            //    UtraThongtintochuc.Controls.Add(GDIEUKHOANTHANHTOAN);
            //    GDIEUKHOANTHANHTOAN.TabIndex = 136;
            //    //--------------------------//
            //    GTENNGLIENHE.Location = new Point(0, 23);
            //    uttraGroupboxTTNLL.Controls.Add(GTENNGLIENHE);
            //    GTENNGLIENHE.TabIndex = 137;

            //    GGIOITINH.Location = new Point(0, 60);
            //    uttraGroupboxTTNLL.Controls.Add(GGIOITINH);
            //    GGIOITINH.TabIndex = 138;

            //    GDIACHILIENHE.Location = new Point(0, 96);
            //    uttraGroupboxTTNLL.Controls.Add(GDIACHILIENHE);
            //    GDIACHILIENHE.TabIndex = 139;

            //    GCHUCDANHNGLIENHE.Location = new Point(306, 24);
            //    uttraGroupboxTTNLL.Controls.Add(GCHUCDANHNGLIENHE);
            //    GCHUCDANHNGLIENHE.TabIndex = 140;

            //    GDTNGLIENHE.Location = new Point(306, 49);
            //    uttraGroupboxTTNLL.Controls.Add(GDTNGLIENHE);
            //    GDTNGLIENHE.TabIndex = 141;


            //    GEMAILNGLIENHE.Location = new Point(306, 76);
            //    uttraGroupboxTTNLL.Controls.Add(GEMAILNGLIENHE);
            //    GEMAILNGLIENHE.TabIndex = 142;


            //    GDTNHARIENG.Location = new Point(306, 102);
            //    uttraGroupboxTTNLL.Controls.Add(GDTNHARIENG);
            //    GDTNHARIENG.TabIndex = 143;

            //    GDTCOQUAN.Location = new Point(306, 127);
            //    uttraGroupboxTTNLL.Controls.Add(GDTCOQUAN);
            //    GDTCOQUAN.TabIndex = 144;


            //    GSOCMTNN.Location = new Point(612, 22);
            //    uttraGroupboxTTNLL.Controls.Add(GSOCMTNN);
            //    GSOCMTNN.TabIndex = 145;


            //    GNGAYCAP.Location = new Point(612, 48);
            //    uttraGroupboxTTNLL.Controls.Add(GNGAYCAP);
            //    GNGAYCAP.TabIndex = 146;


            //    GNOICAP.Location = new Point(612, 85);
            //    uttraGroupboxTTNLL.Controls.Add(GNOICAP);
            //    GNOICAP.TabIndex = 147;

            //    pn_action.Location = new Point(3, 334);
            //    pn_action.Size = new Size(927, 37);

            //}
            #endregion
            #region Code mới
            if (cbbScaleType.SelectedIndex == 1)
            {
                chkIsEmployee.Visible = true;
                if (IsAdd)
                    chkIsEmployee.Location = ckeIsActive.Location;
                else
                    chkIsEmployee.Location = chkIsInsured.Location;
                ultraLabel1.Text = "Thông tin cá nhân";
                visibleLable(false);

                //UtraThongtintochuc.Location = new Point(ultraGroupBox1.Location.X, ultraGroupBox1.Location.Y + ultraGroupBox1.Height + 5);
                // uttraGroupboxTTNLL.Hide();
                // pn_action.Location = new Point(UtraThongtintochuc.Location.X, UtraThongtintochuc.Location.Y + UtraThongtintochuc.Height +5);
                ultraTabControl1.Height = txtDueTime.Location.Y + cbbScaleType.Location.Y + ultraLabel1.Location.Y + 10;
                pn_action.Location = new Point(txtDueTime.Location.X, txtDueTime.Location.Y);
                this.Size = new Size(this.Width, ultraTabControl1.Location.Y + ultraTabControl1.Height - 20 + pn_action.Height + 60);
                //splitContainer1.Panel2.Hide();
            }
            else
            {
                chkIsEmployee.Visible = false;
                chkIsEmployee.Location = chkIsInsured.Location;
                ultraLabel1.Text = "Thông tin tổ chức";
                visibleLable(true);
                ultraTabControl1.Height = txtContactOfficeTel.Location.Y + cbbScaleType.Location.Y + ultraLabel1.Location.Y;
                pn_action.Location = new Point(txtContactOfficeTel.Location.X, txtContactOfficeTel.Location.Y);
                this.Size = new Size(this.Width, txtContactOfficeTel.Location.Y + txtContactOfficeTel.Height + pn_action.Height + 175);
                ////splitContainer1.Panel2.Show();
                //UtraThongtintochuc.Location = new Point(ultraGroupBox1.Location.X, ultraGroupBox1.Location.Y + ultraGroupBox1.Height + 5);
                // uttraGroupboxTTNLL.Show();
                //uttraGroupboxTTNLL.Location = new Point(UtraThongtintochuc.Location.X, UtraThongtintochuc.Location.Y + UtraThongtintochuc.Height + 5);
                // pn_action.Location = new Point(uttraGroupboxTTNLL.Location.X, uttraGroupboxTTNLL.Location.Y + uttraGroupboxTTNLL.Height + 5);
                //this.Size = new Size(ultraGroupBox1.Width, ultraGroupBox1.Height + UtraThongtintochuc.Height + uttraGroupboxTTNLL.Height + pn_action.Height + 80);
            }
            #endregion
        }
        #endregion

        #endregion
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams handleParam = base.CreateParams;
                handleParam.ExStyle |= 0x02000000;   // WS_EX_COMPOSITED       
                return handleParam;
            }
        }
        private void cbbObjectType_ValueChanged(object sender, EventArgs e)
        {
            string txt = txtAccounttingObjectCode.Text;
            bool T = false;
            if (txt != "")
            {
                string temp = "";
                if (txt.StartsWith("KH") || txt.StartsWith("DT"))
                {
                    temp = txt.Substring(2);
                    T = true;
                }
                else
                {
                    temp = txt.Substring(3);
                    T = true;
                }
                string t = (string)cbbObjectType.SelectedItem.DataValue;  //cbbObjectType 0 = Khách hàng; 1 = Nhà cung cấp; 2 = Khách hàng/Nhà cung cấp
                if (t == "0" && T)
                    txtAccounttingObjectCode.Text = "KH" + temp;
                else if (t == "1" && T)
                    txtAccounttingObjectCode.Text = "NCC" + temp;
                else if (t == "2" && T)
                    txtAccounttingObjectCode.Text = "DT" + temp;
            }

        }

        private void splitContainer1_SplitterMoved(object sender, SplitterEventArgs e)
        {

        }

        private void txtTel_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r' || e.KeyChar == '\b' || (char.IsDigit(e.KeyChar)) || e.KeyChar == '(' || e.KeyChar == ')' || e.KeyChar == '-' || e.KeyChar == '+' || e.KeyChar == '.' || e.KeyChar == ' ')
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
        private void visibleLable(bool visible)
        {
            ultraLabel11.Visible = visible;
            ultraLabel14.Visible = visible;
            ultraLabel16.Visible = visible;
            ultraLabel23.Visible = visible;
            ultraLabel13.Visible = visible;
            ultraLabel17.Visible = visible;
            ultraLabel15.Visible = visible;
            ultraLabel18.Visible = visible;
            ultraLabel24.Visible = visible;
            ultraLabel34.Visible = visible;
            labengaycap.Visible = visible;
            labenoicap.Visible = visible;
            txtContactName.Visible = visible;
            CbbContactSex.Visible = visible;
            txtContactAddress.Visible = visible;
            txtContactTitle.Visible = visible;
            txtContactMobile.Visible = visible;
            txtContactEmail.Visible = visible;
            txtContactHomeTel.Visible = visible;
            txtContactOfficeTel.Visible = visible;
            txtIdentificationNo.Visible = visible;
            txtIssueDate.Visible = visible;
            txtIssueBy.Visible = visible;
        }
        private void ultraLabel21_Click(object sender, EventArgs e)
        {

        }

        private void cbbObjectType_ValueChanged_1(object sender, EventArgs e)
        {

        }

        private void tsmAdd_Click(object sender, EventArgs e)
        {
            uGrid.AddNewRow4Grid();
        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            if (uGrid.ActiveCell != null)
            {
                UltraGridRow row = null;
                if (uGrid.ActiveCell != null || uGrid.ActiveRow != null)
                {
                    row = uGrid.ActiveCell != null ? uGrid.ActiveCell.Row : uGrid.ActiveRow;
                }
                else
                {
                    if (uGrid.Rows.Count > 0) row = uGrid.Rows[uGrid.Rows.Count - 1];
                }
                if (row != null) row.Delete(false);
                uGrid.Update();
            }
        }

        private void ultraLabel3_Click(object sender, EventArgs e)
        {

        }

        private void cbbAcountingObjectCategory_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {

        }
        public void ValidatePhoneNumber(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r' || e.KeyChar == '\b' || (char.IsDigit(e.KeyChar)) || e.KeyChar == '(' || e.KeyChar == ')' || e.KeyChar == '-' || e.KeyChar == '+' || e.KeyChar == '.' || e.KeyChar == ' ')
            {
                e.Handled = false;
            }
            else if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // If you want, you can allow decimal (float) numbers
            else if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtContactHomeTel_KeyPress(object sender, KeyPressEventArgs e)
        {
            ValidatePhoneNumber(sender, e);
        }

        private void txtContactMobile_KeyPress(object sender, KeyPressEventArgs e)
        {
            ValidatePhoneNumber(sender, e);
        }

        private void txtContactOfficeTel_KeyPress(object sender, KeyPressEventArgs e)
        {
            ValidatePhoneNumber(sender, e);
        }

        private void txtTel_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            ValidatePhoneNumber(sender, e);
        }
        #region Add thông tin khách hàng lên web hóa đơn điện tử SDS add by Hautv
        private void Post_Customers(AccountingObject accountingObject)
        {
            if(accountingObject.ObjectType == 0 || accountingObject.ObjectType == 2) // Khách hàng
            {
                if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "HDDT").Data != "1") return;
                SystemOption systemOption = Utils.ListSystemOption.FirstOrDefault(n => n.Code == "EI_IDNhaCungCapDichVu");
                if (string.IsNullOrEmpty(systemOption.Data))
                {
                    //MSG.Error("Chưa kết nối hóa đơn điện tử");
                    return;
                }
                SupplierService supplierService = Utils.ISupplierServiceService.Getbykey(Guid.Parse(systemOption.Data));
                if (supplierService == null) return;
                if (supplierService.SupplierServiceCode == "SDS")
                {
                    Customers customers = new Customers();
                    Customer_If customer = new Customer_If();
                    customer.Code = accountingObject.AccountingObjectCode;
                    customer.AccountName = accountingObject.AccountingObjectCode;
                    customer.Name = accountingObject.AccountingObjectName;
                    //customer.Buyer = accountingObject.ContactName; // Người mua
                    customer.Address = accountingObject.Address;
                    customer.TaxCode = accountingObject.TaxCode;
                    if (accountingObject.BankAccounts.Count > 0)
                    {
                        customer.BankName = accountingObject.BankAccounts.First().BankName;
                        customer.BankNumber = accountingObject.BankAccounts.First().BankAccount;
                        //customer.BankAccountName = 
                    }
                    customer.Email = accountingObject.Email;
                    customer.Fax = accountingObject.Fax;
                    customer.Phone = accountingObject.Tel;
                    customer.ContactPerson = accountingObject.ContactName;
                    //customer.RepresentPerson = accountingObject.
                    customers.Customer.Add(customer);
                    var xmldata = Utils.Serialize<Customers>(customers);
                    var request = new Request();
                    request.XmlData = xmldata;
                    RestApiService client = new RestApiService(Utils.GetPathAccess(), Utils.GetUserName(), Core.ITripleDes.TripleDesDefaultDecryption(Utils.GetPassWords()));
                    Response response = client.Post(supplierService.ApiService.FirstOrDefault(n => n.ApiName == "ThemSuaKH").ApiPath, request);
                    if (response.Status != 2)
                    {
                        MSG.Error("Lỗi khi cập nhật lên web hóa đơn điện tử: \n" + response.Message);
                    }
                }
            }
        }
        #endregion
    }
}
