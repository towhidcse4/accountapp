﻿namespace Accounting
{
    partial class FExpenseItemDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbExpenseItemType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.txtDescrip = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblRegistrationGroupName = new Infragistics.Win.Misc.UltraLabel();
            this.txtExpenseItemCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbGroup = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.lblParentID = new Infragistics.Win.Misc.UltraLabel();
            this.txtExpenseItemName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblRegistrationGroupCode = new Infragistics.Win.Misc.UltraLabel();
            this.chkActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbExpenseItemType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescrip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExpenseItemCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExpenseItemName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkActive)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox1.Controls.Add(this.cbbExpenseItemType);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox1.Controls.Add(this.txtDescrip);
            this.ultraGroupBox1.Controls.Add(this.lblRegistrationGroupName);
            this.ultraGroupBox1.Controls.Add(this.txtExpenseItemCode);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox1.Controls.Add(this.cbbGroup);
            this.ultraGroupBox1.Controls.Add(this.lblParentID);
            this.ultraGroupBox1.Controls.Add(this.txtExpenseItemName);
            this.ultraGroupBox1.Controls.Add(this.lblRegistrationGroupCode);
            appearance6.FontData.BoldAsString = "True";
            this.ultraGroupBox1.HeaderAppearance = appearance6;
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(12, 12);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(377, 187);
            this.ultraGroupBox1.TabIndex = 28;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbExpenseItemType
            // 
            this.cbbExpenseItemType.AutoSize = false;
            this.cbbExpenseItemType.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbExpenseItemType.Location = new System.Drawing.Point(134, 82);
            this.cbbExpenseItemType.Name = "cbbExpenseItemType";
            this.cbbExpenseItemType.NullText = "<Chọn loại đối tượng>";
            this.cbbExpenseItemType.Size = new System.Drawing.Size(237, 22);
            this.cbbExpenseItemType.TabIndex = 55;
            // 
            // ultraLabel2
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance1;
            this.ultraLabel2.Location = new System.Drawing.Point(9, 83);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(92, 22);
            this.ultraLabel2.TabIndex = 54;
            this.ultraLabel2.Text = "Loại ";
            // 
            // txtDescrip
            // 
            this.txtDescrip.AutoSize = false;
            this.txtDescrip.Location = new System.Drawing.Point(134, 136);
            this.txtDescrip.MaxLength = 512;
            this.txtDescrip.Multiline = true;
            this.txtDescrip.Name = "txtDescrip";
            this.txtDescrip.Size = new System.Drawing.Size(237, 37);
            this.txtDescrip.TabIndex = 53;
            // 
            // lblRegistrationGroupName
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.TextVAlignAsString = "Middle";
            this.lblRegistrationGroupName.Appearance = appearance2;
            this.lblRegistrationGroupName.Location = new System.Drawing.Point(9, 136);
            this.lblRegistrationGroupName.Name = "lblRegistrationGroupName";
            this.lblRegistrationGroupName.Size = new System.Drawing.Size(126, 22);
            this.lblRegistrationGroupName.TabIndex = 28;
            this.lblRegistrationGroupName.Text = "Diễn giải";
            // 
            // txtExpenseItemCode
            // 
            this.txtExpenseItemCode.AutoSize = false;
            this.txtExpenseItemCode.Location = new System.Drawing.Point(134, 30);
            this.txtExpenseItemCode.MaxLength = 25;
            this.txtExpenseItemCode.Name = "txtExpenseItemCode";
            this.txtExpenseItemCode.Size = new System.Drawing.Size(237, 22);
            this.txtExpenseItemCode.TabIndex = 50;
            // 
            // ultraLabel1
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance3;
            this.ultraLabel1.Location = new System.Drawing.Point(9, 30);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(126, 22);
            this.ultraLabel1.TabIndex = 24;
            this.ultraLabel1.Text = "Mã  (*)";
            // 
            // cbbGroup
            // 
            this.cbbGroup.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.SuggestAppend;
            this.cbbGroup.AutoSize = false;
            this.cbbGroup.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbGroup.Location = new System.Drawing.Point(134, 110);
            this.cbbGroup.Name = "cbbGroup";
            this.cbbGroup.Size = new System.Drawing.Size(237, 22);
            this.cbbGroup.TabIndex = 52;
            this.cbbGroup.ItemNotInList += new Infragistics.Win.UltraWinGrid.ItemNotInListEventHandler(this.cbbGroup_ItemNotInList);
            // 
            // lblParentID
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.lblParentID.Appearance = appearance4;
            this.lblParentID.Location = new System.Drawing.Point(9, 110);
            this.lblParentID.Name = "lblParentID";
            this.lblParentID.Size = new System.Drawing.Size(92, 22);
            this.lblParentID.TabIndex = 16;
            this.lblParentID.Text = "Thuộc nhóm";
            // 
            // txtExpenseItemName
            // 
            this.txtExpenseItemName.AutoSize = false;
            this.txtExpenseItemName.Location = new System.Drawing.Point(134, 56);
            this.txtExpenseItemName.MaxLength = 512;
            this.txtExpenseItemName.Name = "txtExpenseItemName";
            this.txtExpenseItemName.Size = new System.Drawing.Size(237, 22);
            this.txtExpenseItemName.TabIndex = 51;
            // 
            // lblRegistrationGroupCode
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.TextVAlignAsString = "Middle";
            this.lblRegistrationGroupCode.Appearance = appearance5;
            this.lblRegistrationGroupCode.Location = new System.Drawing.Point(9, 56);
            this.lblRegistrationGroupCode.Name = "lblRegistrationGroupCode";
            this.lblRegistrationGroupCode.Size = new System.Drawing.Size(126, 22);
            this.lblRegistrationGroupCode.TabIndex = 50;
            this.lblRegistrationGroupCode.Text = "Tên(*)";
            // 
            // chkActive
            // 
            appearance7.TextVAlignAsString = "Middle";
            this.chkActive.Appearance = appearance7;
            this.chkActive.BackColor = System.Drawing.Color.Transparent;
            this.chkActive.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkActive.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkActive.Checked = true;
            this.chkActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkActive.Location = new System.Drawing.Point(21, 253);
            this.chkActive.Name = "chkActive";
            this.chkActive.Size = new System.Drawing.Size(103, 22);
            this.chkActive.TabIndex = 54;
            this.chkActive.Text = "Hoạt động";
            this.chkActive.ValidateCheckState += new Infragistics.Win.CheckEditor.ValidateCheckStateHandler(this.chkActive_ValidateCheckState);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance8.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance8;
            this.btnClose.Location = new System.Drawing.Point(314, 253);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 56;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click_2);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance9.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance9;
            this.btnSave.Location = new System.Drawing.Point(233, 253);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 55;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // ultraLabel4
            // 
            appearance10.Image = global::Accounting.Properties.Resources.warning;
            this.ultraLabel4.Appearance = appearance10;
            this.ultraLabel4.ImageSize = new System.Drawing.Size(32, 32);
            this.ultraLabel4.Location = new System.Drawing.Point(13, 204);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(35, 43);
            this.ultraLabel4.TabIndex = 60;
            // 
            // ultraLabel3
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            appearance11.TextVAlignAsString = "Top";
            this.ultraLabel3.Appearance = appearance11;
            this.ultraLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel3.Location = new System.Drawing.Point(54, 205);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(329, 43);
            this.ultraLabel3.TabIndex = 59;
            this.ultraLabel3.Text = "Với các doanh nghiệp có tính giá thành thì cần lựa chọn Loại khoản mục chi phí để" +
    " phần mềm tổng hợp chi phí chi tiết theo từng loại";
            // 
            // FExpenseItemDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(402, 295);
            this.Controls.Add(this.ultraLabel4);
            this.Controls.Add(this.ultraLabel3);
            this.Controls.Add(this.chkActive);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.ultraGroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FExpenseItemDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Chi Tiết Mục Thu/Chi";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FExpenseItemDetail_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbExpenseItemType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescrip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExpenseItemCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExpenseItemName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkActive)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDescrip;
        private Infragistics.Win.Misc.UltraLabel lblRegistrationGroupName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtExpenseItemCode;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbGroup;
        private Infragistics.Win.Misc.UltraLabel lblParentID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtExpenseItemName;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkActive;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraLabel lblRegistrationGroupCode;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbExpenseItemType;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
    }
}