﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Accounting.TextMessage;

namespace Accounting
{
    public partial class FExpenseItemDetail : DialogForm
    {
        #region khai báo
        private readonly IExpenseItemService _IExpenseItemService;
        ExpenseItem _select = new ExpenseItem();
        private Guid _id;
        bool checkActive = true;
        public Guid Id
        {
            get { return _id; }
            set { _id = value; }
        }
        bool IsAdd = true;
        public static bool isClose = true;

        #endregion
        public FExpenseItemDetail()
        {
            InitializeComponent();
            _IExpenseItemService = IoC.Resolve<IExpenseItemService>();
            initializeGUI();
            this.Text = "Thêm Khoản Mục Chi Phí";
            chkActive.Visible = false;
        }
        public FExpenseItemDetail(ExpenseItem temp, bool isAdd)
        {
            //Sửa
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _select = isAdd ? new ExpenseItem() : temp;
            IsAdd = isAdd;

            //Khai báo các webservices
            _IExpenseItemService = IoC.Resolve<IExpenseItemService>();

            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----


            //khởi tạo và hiển thị giá trị ban đầu cho các control
            initializeGUI();
            #endregion

            if (!isAdd)
            {
                #region Fill dữ liệu Obj vào control
                ObjandGUI(temp, false);
                #endregion
                this.Text = "Sửa Khoản Mục Chi Phí";
                txtExpenseItemCode.Enabled = false;
                //ultraComboEditor1.Enabled = false;

            }
            else
            {
                this.Text = "Thêm Khoản Mục Chi Phí";
                chkActive.Visible = false;
                //Hỗ trợ chọn ID cha
                foreach (var item in cbbGroup.Rows)
                {
                    if ((item.ListObject as ExpenseItem).ID == temp.ID) cbbGroup.SelectedRow = item;
                }
            }
        }
        ExpenseItem ObjandGUI(ExpenseItem input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();
                input.ExpenseItemCode = txtExpenseItemCode.Text;
                input.ExpenseItemName = txtExpenseItemName.Text;
                input.Description = txtDescrip.Text;
                input.ExpenseType = cbbExpenseItemType.SelectedIndex;
                //ExpenseItem temp0 = (ExpenseItem)Utils.getSelectCbbItem(cbbGroup);
                
                input.IsActive = chkActive.Checked;
                input.IsSecurity = false;
            }
            else
            {
                txtExpenseItemName.Text = input.ExpenseItemName;
                txtExpenseItemCode.Text = input.ExpenseItemCode;
                txtDescrip.Text = input.Description;
                cbbExpenseItemType.SelectedIndex = input.ExpenseType;
                foreach (var item in cbbGroup.Rows)
                {
                    if ((item.ListObject as ExpenseItem).ID == input.ParentID) cbbGroup.SelectedRow = item;
                }
                chkActive.Checked = input.IsActive;
            }
            return input;
        }
        private void initializeGUI()
        {
            cbbExpenseItemType.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            //list = _IExpenseItemService.GetAll();
            cbbExpenseItemType.DataSource = FExpenseItem.dicExpenseItemType.Values.ToList();
            cbbExpenseItemType.SelectedIndex = 3;
            cbbGroup.DataSource = _IExpenseItemService.GetAll();
            cbbGroup.DisplayMember = "ExpenseItemName";
            Utils.ConfigGrid(cbbGroup, ConstDatabase.ExpenseItem_TableName);
        }
        bool CheckCode()
        {
            bool kq = true;
            if (string.IsNullOrEmpty(txtExpenseItemCode.Text) || string.IsNullOrEmpty(txtExpenseItemName.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }

            //check trùng code
            //List<string> lst = _IExpenseItemService.Query.Select(c => c.ExpenseItemCode).ToList();
            List<string> lst = _IExpenseItemService.GetListExpenseItemCode();
            foreach (var x in lst)
            {
                if (txtExpenseItemCode.Text.Equals(x))
                {
                    MSG.Error(string.Format(resSystem.MSG_Catalog_Account6, "khoản mục chi phí"));
                    return false;
                }
            }
            return kq;
        }

        bool CheckError()
        {
            bool kq = true;
            //Check Error Chung

            //Check Error Riêng từng Form
            if (string.IsNullOrEmpty(txtExpenseItemCode.Text) || string.IsNullOrEmpty(txtExpenseItemName.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }
            return kq;
        }
        /// <summary>
        /// Sự kiện nút đóng
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>   

        private void btnSave_Click(object sender, EventArgs e)
        {
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion

            #region Thao tác CSDL
            try
            {
                _IExpenseItemService.BeginTran();
                ExpenseItem temp;
                if (IsAdd)//them moi
                {
                    temp = IsAdd ? new ExpenseItem() : _IExpenseItemService.Getbykey(_select.ID);
                    temp = ObjandGUI(temp, true);

                    //set order fixcode
                    ExpenseItem temp0 = (ExpenseItem)Utils.getSelectCbbItem(cbbGroup);
                    //List<string> lstOrderFixCodeChild = _IExpenseItemService.Query.Where(a => a.ParentID == temp.ParentID)
                    //    .Select(a => a.OrderFixCode).ToList();
                    List<string> lstOrderFixCodeChild =
                        _IExpenseItemService.GetListOrderFixCodeExpenseItemChild(temp.ParentID);
                    string orderFixCodeParent = temp0 == null ? "" : temp0.OrderFixCode;
                    temp.OrderFixCode = Utils.GetOrderFixCode(lstOrderFixCodeChild, orderFixCodeParent);
                    if (temp0 == null)
                    {
                        temp.ParentID = null;
                        temp.IsParentNode = false;
                    }
                    else
                    {
                        temp.ParentID = temp0.ID;
                        temp0.IsParentNode = true;
                        temp.IsParentNode = false;
                    }
                    temp.Grade = temp0 == null ? 1 : temp0.Grade + 1; // quy tac lay gia tri cho grade

                    //nếu cha isactive =false thì khi thêm mới con cũng là false

                    if (cbbGroup.Text.Equals(""))
                    {
                        if (!CheckCode()) return;
                        temp.IsActive = true;
                        _IExpenseItemService.CreateNew(temp);
                    }
                    else
                    {
                        ExpenseItem objParentCombox = (ExpenseItem)Utils.getSelectCbbItem(cbbGroup);
                        //lay cha
                        ExpenseItem Parent = _IExpenseItemService.Getbykey(objParentCombox.ID);
                        if (Parent.IsActive == false)
                        {
                            temp.IsActive = false;
                        }
                        else
                            temp.IsActive = true;

                        //check loi
                        if (!CheckCode()) return;
                        //temp.IsActive = true;
                        _IExpenseItemService.CreateNew(temp);
                    }

                    //update lai isparentnode neu co

                    if (temp.ParentID != null)
                    {
                        ExpenseItem parent = _IExpenseItemService.Getbykey((Guid)temp.ParentID);
                        parent.IsParentNode = true;
                        _IExpenseItemService.Update(parent);
                    }
                    //else
                    //{
                    //    ExpenseItem parent = _IExpenseItemService.Getbykey((Guid)temp.ParentID);
                    //    parent.IsParentNode = false;
                    //    _IExpenseItemService.Update(parent);
                    //}
                }
                else //cap nhat
                {
                    temp = _IExpenseItemService.Getbykey(_select.ID);

                    //Lưu đối tượng trước khi sửa
                    ExpenseItem tempOriginal = (ExpenseItem)Utils.CloneObject(temp);
                    //Lấy về đối tượng chọn trong combobox
                    ExpenseItem objParentCombox = (ExpenseItem)Utils.getSelectCbbItem(cbbGroup);
                    //Không thay đổi đối tượng cha trong combobox
                    if (((temp.ParentID == null) && (objParentCombox == null)) || ((temp.ParentID != null) && (objParentCombox != null) && (temp.ParentID == objParentCombox.ID)))
                    {
                        temp = ObjandGUI(temp, true);
                        _IExpenseItemService.Update(temp);
                    }
                    else
                    {
                        //không cho phép chuyển ngược
                        if (objParentCombox != null)
                        {
                            //List<ExpenseItem> lstChildCheck =
                            //    _IExpenseItemService.Query.Where(p => p.OrderFixCode.StartsWith(tempOriginal.OrderFixCode)).ToList();
                            List<ExpenseItem> lstChildCheck = _IExpenseItemService.GetListExpenseItemOrderFixCode(tempOriginal.OrderFixCode);
                            foreach (var item in lstChildCheck)
                            {
                                if (objParentCombox.ID == item.ID)
                                {
                                    MSG.Error(string.Format(resSystem.MSG_Catalog6, temp.ExpenseItemCode));
                                    return;
                                }
                            }
                        }
                        //Trường hợp có cha cũ
                        //Kiểm tra cha cũ có còn con ngoài con chuyển đi hay không
                        //Chuyển trạng thái IsParentNode Cha cũ nếu hết con
                        if (temp.ParentID != null)
                        {
                            ExpenseItem oldParent = _IExpenseItemService.Getbykey((Guid)temp.ParentID);
                            //int checkChildOldParent = _IExpenseItemService.Query.Count(p => p.ParentID == temp.ParentID);
                            int checkChildOldParent = _IExpenseItemService.GetListExpenseItemParentID(temp.ParentID).Count;
                            if (checkChildOldParent <= 1)
                            {
                                oldParent.IsParentNode = false;
                            }
                            _IExpenseItemService.Update(oldParent);
                        }
                        //Trường hợp có cha mới
                        //Chuyển trạng IsParentNode cha mới
                        if (objParentCombox != null)
                        {

                            ExpenseItem newParent = _IExpenseItemService.Getbykey(objParentCombox.ID);
                            newParent.IsParentNode = true;
                            //if (true)
                            //    {

                            //    }

                            _IExpenseItemService.Update(newParent);

                            //List<ExpenseItem> listChildNewParent = _IExpenseItemService.Query.Where(
                            //    a => a.ParentID == objParentCombox.ID).ToList();
                            List<ExpenseItem> listChildNewParent = _IExpenseItemService.GetListExpenseItemParentID(objParentCombox.ID);
                            //Tính lại OrderFixCode cho đối tượng chính
                            List<string> lstOfcChildNewParent = new List<string>();
                            foreach (var item in listChildNewParent)
                            {
                                lstOfcChildNewParent.Add(item.OrderFixCode);
                            }
                            string orderFixCodeParent = newParent.OrderFixCode;
                            temp.OrderFixCode = Utils.GetOrderFixCode(lstOfcChildNewParent, orderFixCodeParent);
                            //Tính lại bậc cho đối tượng chính
                            temp.Grade = newParent.Grade + 1;
                        }
                        //Trường hợp không có cha mới tức là chuyển đối tượng lên bậc 1
                        if (objParentCombox == null)
                        {
                            //Lấy về tất cả đối tượng bậc 1
                            List<int> lstOfcChildGradeNo1 = new List<int>();
                            //List<ExpenseItem> listChildGradeNo1 = _IExpenseItemService.Query.Where(
                            //    a => a.Grade == 1).ToList();
                            List<ExpenseItem> listChildGradeNo1 = _IExpenseItemService.GetListExpenseItemGrade(1);
                            foreach (var item in listChildGradeNo1)
                            {
                                lstOfcChildGradeNo1.Add(Convert.ToInt32(item.OrderFixCode));
                            }
                            //Tính lại OrderFixCode cho đối tượng chính
                            temp.OrderFixCode = (lstOfcChildGradeNo1.Max() + 1).ToString(CultureInfo.InvariantCulture);
                            //Tính lại bậc cho đối tượng chính
                            temp.Grade = 1;
                            temp = ObjandGUI(temp, true);
                            _IExpenseItemService.Update(temp);
                        }
                        //Xử lý các con của nó 
                        //Lấy về các con
                        //List<ExpenseItem> lstChild =
                        //       _IExpenseItemService.Query.Where(p => p.OrderFixCode.StartsWith(tempOriginal.OrderFixCode)).ToList();
                        List<ExpenseItem> lstChild = _IExpenseItemService.GetListExpenseItemOrderFixCode(tempOriginal.OrderFixCode);
                        foreach (var item in lstChild)
                        {
                            string tempOldFixCode = tempOriginal.OrderFixCode;
                            item.OrderFixCode = temp.OrderFixCode + item.OrderFixCode.Substring(tempOldFixCode.Length);
                            item.Grade = temp.Grade - tempOriginal.Grade + item.Grade;
                            _IExpenseItemService.Update(item);
                        }
                        temp = ObjandGUI(temp, true);
                    }
                    //nếu cha ngừng theo dõi thì con cũng ngừng theo dõi
                    //List<ExpenseItem> lstChild2 =
                    //        _IExpenseItemService.Query.Where(p => p.OrderFixCode.StartsWith(tempOriginal.OrderFixCode)).ToList();
                    if (!checkActive)
                    {
                        List<ExpenseItem> lstChild2 = _IExpenseItemService.GetListExpenseItemOrderFixCode(tempOriginal.OrderFixCode);
                        if (!chkActive.Checked)
                        {
                            foreach (var item2 in lstChild2)
                            {
                                item2.IsActive = false;
                                _IExpenseItemService.Update(item2);
                            }

                        }
                        else
                        {
                            if (temp.IsParentNode)
                            {
                                if (MSG.Question(resSystem.MSG_Catalog8) == System.Windows.Forms.DialogResult.Yes)
                                {
                                    foreach (var item2 in lstChild2)
                                    {
                                        item2.IsActive = true;
                                        _IExpenseItemService.Update(item2);
                                    }
                                }
                            }
                        }

                        //nếu cha isactive = false thì con ko được isactive = true,
                        if (temp.IsParentNode == false)
                        {
                            //lấy cha

                            if (temp.ParentID == null)
                            {
                                if (!CheckError()) return;
                                if (!chkActive.Checked) temp.IsActive = false;
                                else temp.IsActive = true;
                                _IExpenseItemService.Update(temp);
                            }

                            else
                            {
                                ExpenseItem Parent = _IExpenseItemService.Getbykey(objParentCombox.ID);
                                if (Parent.IsActive == true)
                                {
                                    if (!CheckError()) return;
                                    //if (!chkActive.Checked) temp.IsActive = false;
                                    //else temp.IsActive = true;
                                    //_IExpenseItemService.Update(temp);
                                }
                                else
                                {
                                    if (chkActive.Checked)
                                        MSG.Error(string.Format(resSystem.MSG_Catalog_Tree2, "Khoản mục chi phí"));
                                    _IExpenseItemService.RolbackTran();
                                }
                            }

                        }
                        else
                        {
                            if (temp.ParentID != null)
                            {
                                ExpenseItem Parent = _IExpenseItemService.Getbykey(objParentCombox.ID);
                                if (Parent.IsActive == true)
                                {
                                    if (!CheckError()) return;
                                    //if (chkActive.Checked) temp.IsActive = false;
                                    //else temp.IsActive = true;
                                    //_IExpenseItemService.Update(temp);
                                }
                                else
                                {
                                    if (!chkActive.Checked)
                                        MSG.Error(string.Format(resSystem.MSG_Catalog_Tree2, "Khoản mục chi phí"));
                                    _IExpenseItemService.RolbackTran();
                                }
                            }
                            else
                            {
                                if (!CheckError()) return;
                                //if (!chkActive.Checked) temp.IsActive = false;
                                //else temp.IsActive = true;
                                //_IExpenseItemService.Update(temp);
                            }
                        }
                    }
                }
                _IExpenseItemService.CommitTran();
                _id = temp.ID;
            }
            catch (Exception ex)
            {
                MSG.Error(ex.Message);
                return;
            }

            #endregion

            #region xử lý form, kết thúc form
            Utils.ListExpenseItem.Clear();
            //Utils.AddToBindingList(Utils.ListExpenseItem, _IExpenseItemService.GetByActive_OrderByTreeIsParentNode(true));
            isClose = false;
            this.Close();

            #endregion
        }

        private void btnClose_Click_2(object sender, EventArgs e)
        {

            //ExpenseItem temp = _IExpenseItemService.Getbykey(_select.ID);
            //MessageBox.Show(temp.ID+"");
            //_IExpenseItemService.Update(temp);
            isClose = false;
            DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void chkActive_ValidateCheckState(object sender, Infragistics.Win.ValidateCheckStateEventArgs e)
        {
            checkActive = false;
        }

        private void cbbGroup_ItemNotInList(object sender, Infragistics.Win.UltraWinEditors.ValidationErrorEventArgs e)
        {
            ExpenseItem b = new ExpenseItem();
            if (cbbGroup.Text != b.ExpenseItemCode && cbbGroup.Text != "")
            {
                MSG.Warning(string.Format("Dữ liệu không có trong danh mục", cbbGroup.Text));
                cbbGroup.Focus();
                return;
            }
        }

        private void FExpenseItemDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
        //#endregion
    }
}
