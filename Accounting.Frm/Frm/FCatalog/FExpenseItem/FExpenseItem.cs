﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Accounting.TextMessage;
using System.Data;
namespace Accounting
{
    public partial class FExpenseItem : CatalogBase
    {
        #region khai bao
        private readonly IExpenseItemService _ExpenseItemService;
        List<ExpenseItem> dsExpenseItem = new List<ExpenseItem>();
        static Dictionary<int, string> DicExpenseItemType;


        public static Dictionary<int, string> dicExpenseItemType
        {
            get { DicExpenseItemType = DicExpenseItemType ?? (Dictionary<int, string>)BuildConfig(1); return DicExpenseItemType; }
        }
        #endregion
        #region khởi tạo
        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }
        private void LoadDuLieu(bool configTree)
        {
            ToolTip ToolTip1 = new ToolTip();
            ToolTip1.SetToolTip(btnAdd, "Thêm (Ctrl + N)");
            ToolTip ToolTip2 = new ToolTip();
            ToolTip2.SetToolTip(btnEdit, "Sửa (Ctrl + E)");
            ToolTip ToolTip3 = new ToolTip();
            ToolTip3.SetToolTip(btnDelete, "Xóa (Ctrl + D)");
            WaitingFrm.StartWaiting();
            //Dictionary<int, string> tpye = new Dictionary<int, string>();
            //tpye.Add(0, "Chi");
            //tpye.Add(1, "Thu");

            #region Lấy dữ liệu từ CSDL
            dsExpenseItem = _ExpenseItemService.GetAll_OrderBy();
            _ExpenseItemService.UnbindSession(dsExpenseItem);
            dsExpenseItem = _ExpenseItemService.GetAll_OrderBy();
            #endregion

            #region Xử lý dữ liệu
            foreach (var item in dsExpenseItem)
            {
                item.ExpenseItemTypeView = dicExpenseItemType[item.ExpenseType];
            }
            #endregion

            #region hiển thị và xử lý hiển thị
            DataSet ds = Utils.ToDataSet<ExpenseItem>(dsExpenseItem, ConstDatabase.ExpenseItem_TableName);
            uTree.SetDataBinding(ds, ConstDatabase.ExpenseItem_TableName);
            if (configTree) ConfigTree(uTree);
            #endregion
            WaitingFrm.StopWaiting();
        }
        public FExpenseItem()
        {
            InitializeComponent();
            ToolTip ToolTip1 = new ToolTip();
            ToolTip1.SetToolTip(btnAdd, "Thêm (Ctrl + N)");
            ToolTip ToolTip2 = new ToolTip();
            ToolTip2.SetToolTip(btnEdit, "Sửa (Ctrl + E)");
            ToolTip ToolTip3 = new ToolTip();
            ToolTip3.SetToolTip(btnDelete, "Xóa (Ctrl + D)");
            #region Thiết lập ban đầu cho Form
            _ExpenseItemService = IoC.Resolve<IExpenseItemService>();

            this.uTree.InitializeDataNode += new Infragistics.Win.UltraWinTree.InitializeDataNodeEventHandler(Utils.uTree_InitializeDataNode);
            this.uTree.AfterDataNodesCollectionPopulated += new Infragistics.Win.UltraWinTree.AfterDataNodesCollectionPopulatedEventHandler(Utils.uTree_AfterDataNodesCollectionPopulated);
            this.uTree.ColumnSetGenerated += new Infragistics.Win.UltraWinTree.ColumnSetGeneratedEventHandler(this.uTree_ColumnSetGenerated);
            this.uTree.MouseDown += new System.Windows.Forms.MouseEventHandler(this.uTree_MouseDown);
            this.uTree.DoubleClick += new System.EventHandler(this.uTree_DoubleClick1);
            #endregion

            #region Tạo dữ liệu ban đầu
            LoadDuLieu();
            #endregion
        }
        #endregion
        #region nghiệp vụ
        #region Sửa
        protected override void EditFunction()
        {
            if (uTree.SelectedNodes.Count == 0 && uTree.ActiveNode != null)
            {
                uTree.ActiveNode.Selected = true;
            }
            if (uTree.SelectedNodes.Count > 0)
            {
                var temp = dsExpenseItem.Single(k => k.ID == (new Guid(uTree.SelectedNodes[0].Text)));
                //if (Utils.checkRelationVoucher_ExpenseItem(temp))
                //{
                    //MSG.Error("Không thể sửa Khoản Mục Chi Phí vì có phát sinh chứng từ liên quan");
                    //return;
                //}
                //else
                //{
                    new FExpenseItemDetail(temp, false).ShowDialog(this);
                    if (!FExpenseItemDetail.isClose) LoadDuLieu();
                //}
            }
            else
                Accounting.TextMessage.MSG.Error(string.Format(Accounting.TextMessage.resSystem.MSG_Catalog3,"một khoản mục chi phí"));
        }
        #endregion

        #region Thêm
        protected override void AddFunction()
        {
            if (uTree.SelectedNodes.Count > 0)
            {
                new FExpenseItemDetail(dsExpenseItem.Single(k => k.ID == (new Guid(uTree.SelectedNodes[0].Text))), true).ShowDialog(this);

                if (!FExpenseItemDetail.isClose) LoadDuLieu();
            }
            else
            {

                new FExpenseItemDetail().ShowDialog(this);

                if (!FExpenseItemDetail.isClose) LoadDuLieu();
            }
        }
        #endregion

        #region Xoa
        protected override void DeleteFunction()
        {
            if (uTree.SelectedNodes.Count == 0 && uTree.ActiveNode != null)
            {
                uTree.ActiveNode.Selected = true;
            }
            if (uTree.SelectedNodes.Count > 0)
            {
                ExpenseItem temp = dsExpenseItem.Single(k => k.ID == (new Guid(uTree.SelectedNodes[0].Text)));
                if (MSG.Question(string.Format(resSystem.MSG_System_05, temp.ExpenseItemCode)) == System.Windows.Forms.DialogResult.Yes)
                {
                    if (Utils.checkRelationVoucher_ExpenseItem(temp))
                    {
                        MSG.Error("Không thể xóa Khoản Mục Chi Phí vì có phát sinh chứng từ liên quan");
                        return;
                    }
                    _ExpenseItemService.BeginTran();
                    //Kiểm tra xem đối tượng có con không nếu có con thì không được phép xóa
                    //List<ExpenseItem> lstChild =
                    //        _ExpenseItemService.Query.Where(p => p.ParentID == temp.ID).ToList();
                    List<ExpenseItem> lstChild = _ExpenseItemService.GetListExpenseItemParentID(temp.ID);
                    if (lstChild.Count > 0)
                    {
                        MSG.Error(string.Format(resSystem.MSG_Catalog_Tree, "Khoản mục chi phí"));
                        return;
                    }
                    //Check xem cha còn có con khác không nếu không thì chuyển trạng thái IsParentNode = false
                    if (temp.ParentID != null)
                    {
                        ExpenseItem parent = _ExpenseItemService.Getbykey((Guid)temp.ParentID);
                        //int checkChildOldParent = _ExpenseItemService.Query.Count(p => p.ParentID == temp.ParentID);
                        //int checkChildOldParent = _ExpenseItemService.GetListExpenseItemParentID(temp.ParentID).Count;
                        int checkChildOldParent = _ExpenseItemService.CountListExpenseItemParentID(temp.ParentID);
                        if (checkChildOldParent <= 1)
                        {
                            parent.IsParentNode = false;
                        }
                        _ExpenseItemService.Update(parent);
                        _ExpenseItemService.Delete(temp);
                    }
                    if (temp.ParentID == null)
                    {
                        _ExpenseItemService.Delete(temp);
                    }
                    _ExpenseItemService.CommitTran();
                    Utils.ClearCacheByType<ExpenseItem>();
                    LoadDuLieu();
                }
            }
            else
                MSG.Error(string.Format(resSystem.MSG_Catalog2,"một khoản mục chi phí"));
        }
        #endregion
        #endregion

        #region Utils
        void ConfigTree(Infragistics.Win.UltraWinTree.UltraTree ultraTree)
        {//hàm chung
            Utils.ConfigTree(ultraTree, TextMessage.ConstDatabase.ExpenseItem_TableName);
        }
        static object BuildConfig(int obj)
        {
            Dictionary<int, string> temp = new Dictionary<int, string>();
            switch (obj)
            {
                case 1:
                    {                        
                        temp.Add(0, "Chi phí NVL trực tiếp");
                        temp.Add(1, "Chi phí nhân công trực tiếp");
                        temp.Add(2, "Chi phí khác");
                        temp.Add(3, "");
                    }
                    break;

            }
            return temp;
        }
        #endregion

        private void uTree_ColumnSetGenerated(object sender, Infragistics.Win.UltraWinTree.ColumnSetGeneratedEventArgs e)
        {
            Utils.uTree_ColumnSetGenerated(sender, e, ConstDatabase.ExpenseItem_TableName);
        }

        private void uTree_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, cms4Grid);
        }

        private void tsmAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void tsmEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            ResetFunction();
        }
        protected override void ResetFunction()
        {
            LoadDuLieu(false);
        }

        private void uTree_DoubleClick1(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void btnEdit_Click_2(object sender, EventArgs e)
        {
            EditFunction();

        }

        private void btnAdd_Click_2(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void FExpenseItem_FormClosing(object sender, FormClosingEventArgs e)
        {
            uTree = null;
        }

        private void FExpenseItem_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
