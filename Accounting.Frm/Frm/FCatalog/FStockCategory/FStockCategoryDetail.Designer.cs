﻿namespace Accounting
{
    partial class FStockCategoryDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.chkIsActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtYearTransLimited = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.lblYearTransLimited = new Infragistics.Win.Misc.UltraLabel();
            this.txtTransferCondition = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblTransferCondition = new Infragistics.Win.Misc.UltraLabel();
            this.txtStockCategoryName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtUnitPrice = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.lblUnitPrice = new Infragistics.Win.Misc.UltraLabel();
            this.lblStockCategoryName = new Infragistics.Win.Misc.UltraLabel();
            this.cbbStockKind = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.txtIncentives = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtStockCategoryCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblStockKind = new Infragistics.Win.Misc.UltraLabel();
            this.lblIncentives = new Infragistics.Win.Misc.UltraLabel();
            this.lblStockCategoryCode = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtYearTransLimited)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransferCondition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStockCategoryName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnitPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbStockKind)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIncentives)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStockCategoryCode)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            appearance1.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance1;
            this.btnSave.Location = new System.Drawing.Point(210, 307);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 11;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            appearance2.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance2;
            this.btnClose.Location = new System.Drawing.Point(293, 307);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 12;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // chkIsActive
            // 
            appearance3.TextVAlignAsString = "Middle";
            this.chkIsActive.Appearance = appearance3;
            this.chkIsActive.BackColor = System.Drawing.Color.Transparent;
            this.chkIsActive.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsActive.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkIsActive.Checked = true;
            this.chkIsActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIsActive.Location = new System.Drawing.Point(9, 312);
            this.chkIsActive.Name = "chkIsActive";
            this.chkIsActive.Size = new System.Drawing.Size(103, 20);
            this.chkIsActive.TabIndex = 13;
            this.chkIsActive.Text = "Hoạt động";
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox1.Controls.Add(this.txtYearTransLimited);
            this.ultraGroupBox1.Controls.Add(this.lblYearTransLimited);
            this.ultraGroupBox1.Controls.Add(this.txtTransferCondition);
            this.ultraGroupBox1.Controls.Add(this.lblTransferCondition);
            this.ultraGroupBox1.Controls.Add(this.txtStockCategoryName);
            this.ultraGroupBox1.Controls.Add(this.txtUnitPrice);
            this.ultraGroupBox1.Controls.Add(this.lblUnitPrice);
            this.ultraGroupBox1.Controls.Add(this.lblStockCategoryName);
            this.ultraGroupBox1.Controls.Add(this.cbbStockKind);
            this.ultraGroupBox1.Controls.Add(this.txtIncentives);
            this.ultraGroupBox1.Controls.Add(this.txtStockCategoryCode);
            this.ultraGroupBox1.Controls.Add(this.lblStockKind);
            this.ultraGroupBox1.Controls.Add(this.lblIncentives);
            this.ultraGroupBox1.Controls.Add(this.lblStockCategoryCode);
            appearance11.FontData.BoldAsString = "True";
            this.ultraGroupBox1.HeaderAppearance = appearance11;
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(3, 6);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(377, 295);
            this.ultraGroupBox1.TabIndex = 26;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtYearTransLimited
            // 
            this.txtYearTransLimited.AutoSize = false;
            this.txtYearTransLimited.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.txtYearTransLimited.Location = new System.Drawing.Point(119, 263);
            this.txtYearTransLimited.MaxValue = 99999;
            this.txtYearTransLimited.MinValue = 0;
            this.txtYearTransLimited.Name = "txtYearTransLimited";
            this.txtYearTransLimited.PromptChar = ' ';
            this.txtYearTransLimited.Size = new System.Drawing.Size(249, 22);
            this.txtYearTransLimited.TabIndex = 25;
            // 
            // lblYearTransLimited
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.lblYearTransLimited.Appearance = appearance4;
            this.lblYearTransLimited.Location = new System.Drawing.Point(9, 252);
            this.lblYearTransLimited.Name = "lblYearTransLimited";
            this.lblYearTransLimited.Size = new System.Drawing.Size(109, 44);
            this.lblYearTransLimited.TabIndex = 24;
            this.lblYearTransLimited.Text = "Số năm hạn chế chuyển nhượng";
            // 
            // txtTransferCondition
            // 
            this.txtTransferCondition.AutoSize = false;
            this.txtTransferCondition.Location = new System.Drawing.Point(119, 199);
            this.txtTransferCondition.Multiline = true;
            this.txtTransferCondition.Name = "txtTransferCondition";
            this.txtTransferCondition.Size = new System.Drawing.Size(249, 59);
            this.txtTransferCondition.TabIndex = 23;
            // 
            // lblTransferCondition
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.TextVAlignAsString = "Middle";
            this.lblTransferCondition.Appearance = appearance5;
            this.lblTransferCondition.Location = new System.Drawing.Point(9, 199);
            this.lblTransferCondition.Name = "lblTransferCondition";
            this.lblTransferCondition.Size = new System.Drawing.Size(110, 44);
            this.lblTransferCondition.TabIndex = 22;
            this.lblTransferCondition.Text = "Điều kiện chuyển nhượng";
            // 
            // txtStockCategoryName
            // 
            this.txtStockCategoryName.AutoSize = false;
            this.txtStockCategoryName.Location = new System.Drawing.Point(119, 54);
            this.txtStockCategoryName.Name = "txtStockCategoryName";
            this.txtStockCategoryName.Size = new System.Drawing.Size(249, 22);
            this.txtStockCategoryName.TabIndex = 21;
            // 
            // txtUnitPrice
            // 
            this.txtUnitPrice.AutoSize = false;
            this.txtUnitPrice.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.txtUnitPrice.Location = new System.Drawing.Point(119, 81);
            this.txtUnitPrice.MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiteralsWithPadding;
            this.txtUnitPrice.MaskInput = "nn,nnn";
            this.txtUnitPrice.MaxValue = 99999;
            this.txtUnitPrice.MinValue = 0;
            this.txtUnitPrice.Name = "txtUnitPrice";
            this.txtUnitPrice.Size = new System.Drawing.Size(249, 22);
            this.txtUnitPrice.TabIndex = 20;
            // 
            // lblUnitPrice
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextVAlignAsString = "Middle";
            this.lblUnitPrice.Appearance = appearance6;
            this.lblUnitPrice.Location = new System.Drawing.Point(9, 81);
            this.lblUnitPrice.Name = "lblUnitPrice";
            this.lblUnitPrice.Size = new System.Drawing.Size(111, 22);
            this.lblUnitPrice.TabIndex = 14;
            this.lblUnitPrice.Text = "Giá phát hành (*)";
            // 
            // lblStockCategoryName
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.TextVAlignAsString = "Middle";
            this.lblStockCategoryName.Appearance = appearance7;
            this.lblStockCategoryName.Location = new System.Drawing.Point(9, 54);
            this.lblStockCategoryName.Name = "lblStockCategoryName";
            this.lblStockCategoryName.Size = new System.Drawing.Size(109, 22);
            this.lblStockCategoryName.TabIndex = 12;
            this.lblStockCategoryName.Text = "Tên cổ phần (*)";
            // 
            // cbbStockKind
            // 
            this.cbbStockKind.AutoSize = false;
            this.cbbStockKind.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbStockKind.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbbStockKind.Location = new System.Drawing.Point(119, 108);
            this.cbbStockKind.Name = "cbbStockKind";
            this.cbbStockKind.Size = new System.Drawing.Size(249, 22);
            this.cbbStockKind.TabIndex = 9;
            // 
            // txtIncentives
            // 
            this.txtIncentives.AutoSize = false;
            this.txtIncentives.Location = new System.Drawing.Point(119, 135);
            this.txtIncentives.Multiline = true;
            this.txtIncentives.Name = "txtIncentives";
            this.txtIncentives.Size = new System.Drawing.Size(249, 59);
            this.txtIncentives.TabIndex = 6;
            // 
            // txtStockCategoryCode
            // 
            this.txtStockCategoryCode.AutoSize = false;
            this.txtStockCategoryCode.Location = new System.Drawing.Point(119, 27);
            this.txtStockCategoryCode.Name = "txtStockCategoryCode";
            this.txtStockCategoryCode.Size = new System.Drawing.Size(249, 22);
            this.txtStockCategoryCode.TabIndex = 5;
            // 
            // lblStockKind
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.TextVAlignAsString = "Middle";
            this.lblStockKind.Appearance = appearance8;
            this.lblStockKind.Location = new System.Drawing.Point(9, 108);
            this.lblStockKind.Name = "lblStockKind";
            this.lblStockKind.Size = new System.Drawing.Size(109, 22);
            this.lblStockKind.TabIndex = 2;
            this.lblStockKind.Text = "Loại cổ phần (*)";
            // 
            // lblIncentives
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.TextVAlignAsString = "Middle";
            this.lblIncentives.Appearance = appearance9;
            this.lblIncentives.Location = new System.Drawing.Point(9, 135);
            this.lblIncentives.Name = "lblIncentives";
            this.lblIncentives.Size = new System.Drawing.Size(107, 22);
            this.lblIncentives.TabIndex = 1;
            this.lblIncentives.Text = "Hình thức ưu đãi";
            // 
            // lblStockCategoryCode
            // 
            appearance10.BackColor = System.Drawing.Color.Transparent;
            appearance10.TextVAlignAsString = "Middle";
            this.lblStockCategoryCode.Appearance = appearance10;
            this.lblStockCategoryCode.Location = new System.Drawing.Point(9, 27);
            this.lblStockCategoryCode.Name = "lblStockCategoryCode";
            this.lblStockCategoryCode.Size = new System.Drawing.Size(108, 22);
            this.lblStockCategoryCode.TabIndex = 0;
            this.lblStockCategoryCode.Text = "Mã cổ phần (*)";
            // 
            // FStockCategoryDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(383, 341);
            this.Controls.Add(this.ultraGroupBox1);
            this.Controls.Add(this.chkIsActive);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FStockCategoryDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FStockCategoryDetail_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtYearTransLimited)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransferCondition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStockCategoryName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnitPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbStockKind)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIncentives)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStockCategoryCode)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsActive;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraLabel lblUnitPrice;
        private Infragistics.Win.Misc.UltraLabel lblStockCategoryName;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbStockKind;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtStockCategoryCode;
        private Infragistics.Win.Misc.UltraLabel lblStockKind;
        private Infragistics.Win.Misc.UltraLabel lblStockCategoryCode;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor txtUnitPrice;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor txtYearTransLimited;
        private Infragistics.Win.Misc.UltraLabel lblYearTransLimited;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTransferCondition;
        private Infragistics.Win.Misc.UltraLabel lblTransferCondition;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtStockCategoryName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtIncentives;
        private Infragistics.Win.Misc.UltraLabel lblIncentives;
    }
}
