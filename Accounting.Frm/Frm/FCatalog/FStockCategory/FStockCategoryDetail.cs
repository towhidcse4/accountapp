﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Accounting.TextMessage;

namespace Accounting
{
    public partial class FStockCategoryDetail : DialogForm //UserControl
    {
        #region khai báo
        private readonly IStockCategoryService _IStockCategoryService;

        StockCategory _Select = new StockCategory();
        bool Them = true;
        public static bool isClose = true;
        #endregion

        #region khởi tạo
        public FStockCategoryDetail()
        {//Thêm
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion
            chkIsActive.Visible = false;
            #region Thiết lập ban đầu cho Form
            this.Text = "Thêm mới loại cổ phần";
            
            //Khai báo các webservices
            _IStockCategoryService = IoC.Resolve<IStockCategoryService>();

            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion
        }

        public FStockCategoryDetail(StockCategory temp)
        {//Sửa
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            this.Text = "Sửa loại cổ phần";
            _Select = temp;
            Them = false;
            txtStockCategoryCode.Enabled = false;

            //Khai báo các webservices
            _IStockCategoryService = IoC.Resolve<IStockCategoryService>();

            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion

            #region Fill dữ liệu Obj vào control
            ObjandGUI(temp, false);
            #endregion
        }

        private void InitializeGUI()
        {
            Dictionary<int, string> temp = new Dictionary<int, string>();
            temp.Add(0, "Cổ phần phổ thông");
            temp.Add(1, "Cổ phần ưu đãi");
            //khởi tạo cbb loại cổ phần
            cbbStockKind.DataSource = temp.Values.ToList();
            cbbStockKind.SelectedIndex = 0;
            txtUnitPrice.FormatNumberic(ConstDatabase.Format_TienVND);
            txtYearTransLimited.FormatNumberic(ConstDatabase.Format_Quantity);
        }
        #endregion

        #region Button Event
        /// <summary>
        /// Sự kiện nút Lưu lại
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion

            //Gui to object
            #region Fill dữ liệu control vào obj
            StockCategory temp = Them ? new StockCategory() : _IStockCategoryService.Getbykey(_Select.ID);
            temp = ObjandGUI(temp, true);
            #endregion

            #region Thao tác CSDL
            _IStockCategoryService.BeginTran();
            if (Them)
            {
                if (!CheckCode()) return;
                _IStockCategoryService.CreateNew(temp);
            }
            else _IStockCategoryService.Update(temp);
            _IStockCategoryService.CommitTran();
            #endregion

            #region xử lý form, kết thúc form
            this.Close();
            isClose = false;
            #endregion
        }

        /// <summary>
        /// Sự kiện nút đóng
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            isClose = true;
            DialogResult = DialogResult.Cancel;
            this.Close();
        }
        #endregion

        #region Utils
        /// <summary>
        /// get hoặc set một đối tượng vào giao diện xử lý
        /// </summary>
        /// <param name="input">đối tượng</param>
        /// <param name="isGet">True: get giá trị từ control gán vào đối tượng| False: set giá trị đối tượng vào control</param>
        /// <returns></returns>
        StockCategory ObjandGUI(StockCategory input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();  //Thêm mới

                input.StockCategoryCode = txtStockCategoryCode.Text;
                input.StockCategoryName = txtStockCategoryName.Text;
                input.UnitPrice = decimal.Parse(txtUnitPrice.Value.ToString());
                input.StockKind = cbbStockKind.SelectedIndex;
                input.Incentives = txtIncentives.Text;
                input.TransferCondition = txtTransferCondition.Text;
                input.YearTransLimited = decimal.Parse(txtYearTransLimited.Value.ToString());
                input.IsActive = chkIsActive.Checked;
            }
            else
            {
                txtStockCategoryCode.Text = input.StockCategoryCode;
                txtStockCategoryName.Text = input.StockCategoryName;
                txtUnitPrice.Value = input.UnitPrice;
                cbbStockKind.SelectedIndex = input.StockKind;
                txtIncentives.Text = input.Incentives;
                txtTransferCondition.Text = input.TransferCondition;
                txtYearTransLimited.Value = input.YearTransLimited;
                chkIsActive.Checked = input.IsActive;
            }
            return input;
        }

        bool CheckCode()
        {
            bool kq = true;
            //Check Error Chung

            //List<string> hh = _IStockCategoryService.Query.Select(a => a.StockCategoryCode).ToList();
            List<string> hh = _IStockCategoryService.GetCode();
            foreach (var item in hh)
            {
                if (item.Equals(txtStockCategoryCode.Text))
                {
                    MSG.Warning(string.Format(resSystem.MSG_Catalog, "Mã cổ phần", item));
                    return false;
                }
            }

            return kq;
        }

        bool CheckError()
        {
            bool kq = true;
            //Check Error Chung

            //Check Error Riêng từng Form
            if (string.IsNullOrEmpty(txtStockCategoryCode.Text) || string.IsNullOrEmpty(txtStockCategoryName.Text) || string.IsNullOrEmpty(txtUnitPrice.Value.ToString()))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }
            return kq;
        }
        #endregion

        private void FStockCategoryDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
