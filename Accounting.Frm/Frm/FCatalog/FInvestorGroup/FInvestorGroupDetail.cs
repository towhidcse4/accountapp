﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Accounting.TextMessage;
using System.Globalization;

namespace Accounting
{
    public partial class FInvestorGroupDetail : CustormForm //UserControl
    {
        #region khai báo
        private IInvestorGroupService _IInvestorGroupService;
        InvestorGroup _Select = new InvestorGroup();
        bool Them = true;
        public static string InvestorGroupCode;
        public static bool isClose = true;
        #endregion

        #region khởi tạo

        private void Add()
        {
            #region Thiết lập ban đầu cho Form
            //Khai báo các webservices
            _IInvestorGroupService = IoC.Resolve<IInvestorGroupService>();
            chkIsActive.Visible = false;
            this.Text = "Thêm mới nhóm nhà đầu tư";
            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion
        }

        private void Edit(InvestorGroup temp)
        {
            #region Thiết lập ban đầu cho Form
            _Select = temp;
            Them = false;
            txtInvestorGroupCode.Enabled = false;
            this.Text = "Sửa nhóm nhà đầu tư";
            //Khai báo các webservices
            _IInvestorGroupService = IoC.Resolve<IInvestorGroupService>();

            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion

            #region Fill dữ liệu Obj vào control
            ObjandGUI(temp, false);
            #endregion
        }
        public FInvestorGroupDetail()
        {//Thêm
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion
            Add();
        }

        public FInvestorGroupDetail(InvestorGroup temp)
        {//Sửa
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion
            Edit(temp);
        }

        public FInvestorGroupDetail(InvestorGroup temp, bool Them)
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion
            if (Them)
            {
                Add();
                foreach (var item in cbbParentID.Rows)
                {
                    if ((item.ListObject as InvestorGroup).ID == temp.ID) cbbParentID.SelectedRow = item;
                }
            }
            else
            {
                Edit(temp);
            }
        }

        private void InitializeGUI()
        {
            //khởi tạo cbb
            //cbbParentID.DataSource = _IInvestorGroupService.GetAll().OrderByDescending(c=>c.InvestorGroupName).Reverse().ToList();
            cbbParentID.DataSource = _IInvestorGroupService.OrderByGroupName();
            cbbParentID.DisplayMember = "InvestorGroupName";
            Utils.ConfigGrid(cbbParentID, ConstDatabase.InvestorGroup_TableName);
        }
        #endregion

        #region Button Event
        /// <summary>
        /// Sự kiện nút Lưu lại
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion

            #region Thao tác CSDL
            try
            {
                _IInvestorGroupService.BeginTran();

                if (Them)//them moi
                {
                    InvestorGroup temp = Them ? new InvestorGroup() : _IInvestorGroupService.Getbykey(_Select.ID);
                    temp = ObjandGUI(temp, true);

                    //set order fixcode
                    InvestorGroup temp0 = (InvestorGroup)Utils.getSelectCbbItem(cbbParentID);
                    List<string> lstOrderFixCodeChild = _IInvestorGroupService.Query.Where(a => a.ParentID == temp.ParentID)
                        .Select(a => a.OrderFixCode).ToList();
                    string orderFixCodeParent = temp0 == null ? "" : temp0.OrderFixCode;
                    temp.OrderFixCode = Utils.GetOrderFixCode(lstOrderFixCodeChild, orderFixCodeParent);
                    temp.IsParentNode = false; // mac dinh cho insert
                    temp.Grade = temp0 == null ? 1 : temp0.Grade + 1; // quy tac lay gia tri cho grade

                    //nếu cha isactive =false thì khi thêm mới con cũng là false

                    if (cbbParentID.Text.Equals(""))
                    {
                        if (!CheckCode()) return;
                        temp.IsActive = true;
                        _IInvestorGroupService.CreateNew(temp);
                    }
                    else
                    {
                        InvestorGroup objParentCombox = (InvestorGroup)Utils.getSelectCbbItem(cbbParentID);
                        //lay cha
                        InvestorGroup Parent = _IInvestorGroupService.Getbykey(objParentCombox.ID);
                        if (Parent.IsActive == false)
                        {
                            temp.IsActive = false;
                        }
                        else
                            temp.IsActive = true;

                        //check loi
                        if (!CheckCode()) return;
                        //temp.IsActive = true;
                        _IInvestorGroupService.CreateNew(temp);
                    }

                    //update lai isparentnode neu co
                    if (temp.ParentID != null)
                    {
                        InvestorGroup parent = _IInvestorGroupService.Getbykey((Guid)temp.ParentID);
                        parent.IsParentNode = true;
                        _IInvestorGroupService.Update(parent);
                    }

                }
                else //cap nhat
                {
                    InvestorGroup temp = _IInvestorGroupService.Getbykey(_Select.ID);
                    //Lưu đối tượng trước khi sửa
                    InvestorGroup tempOriginal = (InvestorGroup)Utils.CloneObject(temp);
                    //Lấy về đối tượng chọn trong combobox
                    InvestorGroup objParentCombox = (InvestorGroup)Utils.getSelectCbbItem(cbbParentID);
                    //Không thay đổi đối tượng cha trong combobox
                    if (((temp.ParentID == null) && (objParentCombox == null)) || ((temp.ParentID != null) && (objParentCombox != null) && (temp.ParentID == objParentCombox.ID)))
                    {
                        temp = ObjandGUI(temp, true);
                        _IInvestorGroupService.Update(temp);
                    }
                    else
                    {
                        //không cho phép chuyển ngược
                        if (objParentCombox != null)
                        {
                            List<InvestorGroup> lstChildCheck =
                                //_IInvestorGroupService.Query.Where(p => p.OrderFixCode.StartsWith(tempOriginal.OrderFixCode)).ToList();
                                _IInvestorGroupService.GetListByOrderFixCode(tempOriginal.OrderFixCode);
                            foreach (var item in lstChildCheck)
                            {
                                if (objParentCombox.ID == item.ID)
                                {
                                    MSG.Error(string.Format(resSystem.MSG_Catalog6, temp.InvestorGroupCode));
                                    return;
                                }
                            }
                        }
                        //Trường hợp có cha cũ
                        //Kiểm tra cha cũ có còn con ngoài con chuyển đi hay không
                        //Chuyển trạng thái IsParentNode Cha cũ nếu hết con
                        if (temp.ParentID != null)
                        {
                            InvestorGroup oldParent = _IInvestorGroupService.Getbykey((Guid)temp.ParentID);
                            //int checkChildOldParent = _IInvestorGroupService.Query.Count(p => p.ParentID == temp.ParentID);
                            int checkChildOldParent =
                                _IInvestorGroupService.GetListByParentID(temp.ParentID.Value).Count;
                            if (checkChildOldParent <= 1)
                            {
                                oldParent.IsParentNode = false;
                            }
                            _IInvestorGroupService.Update(oldParent);
                        }
                        //Trường hợp có cha mới
                        //Chuyển trạng IsParentNode cha mới
                        if (objParentCombox != null)
                        {
                            InvestorGroup newParent = _IInvestorGroupService.Getbykey(objParentCombox.ID);
                            newParent.IsParentNode = true;
                            _IInvestorGroupService.Update(newParent);

                            //List<InvestorGroup> listChildNewParent = _IInvestorGroupService.Query.Where(
                            //    a => a.ParentID == objParentCombox.ID).ToList();
                            List<InvestorGroup> listChildNewParent =
                                _IInvestorGroupService.GetListByParentID(objParentCombox.ID);
                            //Tính lại OrderFixCode cho đối tượng chính
                            List<string> lstOfcChildNewParent = new List<string>();
                            foreach (var item in listChildNewParent)
                            {
                                lstOfcChildNewParent.Add(item.OrderFixCode);
                            }
                            string orderFixCodeParent = newParent.OrderFixCode;
                            temp.OrderFixCode = Utils.GetOrderFixCode(lstOfcChildNewParent, orderFixCodeParent);
                            //Tính lại bậc cho đối tượng chính
                            temp.Grade = newParent.Grade + 1;
                        }
                        //Trường hợp không có cha mới tức là chuyển đối tượng lên bậc 1
                        if (objParentCombox == null)
                        {
                            //Lấy về tất cả đối tượng bậc 1
                            List<int> lstOfcChildGradeNo1 = new List<int>();
                            List<InvestorGroup> listChildGradeNo1 = _IInvestorGroupService.GetListByGrade(1);
                            foreach (var item in listChildGradeNo1)
                            {
                                lstOfcChildGradeNo1.Add(Convert.ToInt32(item.OrderFixCode));
                            }
                            //Tính lại OrderFixCode cho đối tượng chính
                            temp.OrderFixCode = (lstOfcChildGradeNo1.Max() + 1).ToString(CultureInfo.InvariantCulture);
                            //Tính lại bậc cho đối tượng chính
                            temp.Grade = 1;
                            temp = ObjandGUI(temp, true);
                            _IInvestorGroupService.Update(temp);
                        }
                        //Xử lý các con của nó 
                        //Lấy về các con
                        List<InvestorGroup> lstChild =
                            _IInvestorGroupService.Query.Where(p => p.OrderFixCode.StartsWith(tempOriginal.OrderFixCode)).ToList();
                        foreach (var item in lstChild)
                        {
                            string tempOldFixCode = tempOriginal.OrderFixCode;
                            item.OrderFixCode = temp.OrderFixCode + item.OrderFixCode.Substring(tempOldFixCode.Length);
                            item.Grade = temp.Grade - tempOriginal.Grade + item.Grade;
                            _IInvestorGroupService.Update(item);
                        }
                        temp = ObjandGUI(temp, true);
                    }

                    //nếu cha ngừng theo dõi thì con cũng ngừng theo dõi
                    //Lấy về danh sách các con
                    List<InvestorGroup> lstChild2 =
                            _IInvestorGroupService.Query.Where(p => p.OrderFixCode.StartsWith(tempOriginal.OrderFixCode) && p.ID != tempOriginal.ID).ToList();

                    //Chuyen True -> False
                    if (chkIsActive.CheckState == CheckState.Unchecked && CheckError())
                    {
                        temp.IsActive = false;
                        //neu la cha
                        if (lstChild2.Count > 0)
                        {
                            foreach (var itemChild in lstChild2)
                            {
                                itemChild.IsActive = false;
                                _IInvestorGroupService.Update(itemChild);
                            }
                        }
                        //neu ko phai la cha
                        _IInvestorGroupService.Update(temp);
                    }

                    //Chuyen False -> True
                    else if (chkIsActive.CheckState == CheckState.Checked && CheckError())
                    {
                        //Co Cha
                        if (temp.ParentID != null)
                        {
                            InvestorGroup Parent = _IInvestorGroupService.Getbykey(objParentCombox.ID);
                            //Cha la false khong cho chuyen
                            if (Parent.IsActive == false)
                            {
                                MSG.Warning(string.Format(resSystem.MSG_Catalog_Tree2, "nhóm Nhà đầu tư"));
                                return;
                            }
                            //Cha la True duoc phep chuyen
                            temp.IsActive = true;
                            if (lstChild2.Count > 0)
                            {

                                if (MSG.Question(string.Format(resSystem.MSG_Catalog_Tree3,"nhóm nhà đầu tư")) == System.Windows.Forms.DialogResult.Yes)
                                {
                                    foreach (var item2 in lstChild2)
                                    {
                                        item2.IsActive = true;
                                        _IInvestorGroupService.Update(item2);
                                    }
                                }

                            }
                            _IInvestorGroupService.Update(temp);
                        }
                        //Khong Co cha
                        else
                        {
                            temp.IsActive = true;
                            if (lstChild2.Count > 0)
                            {

                                if (MSG.Question(string.Format(resSystem.MSG_Catalog_Tree3, "nhóm nhà đầu tư")) == System.Windows.Forms.DialogResult.Yes)
                                {
                                    foreach (var item2 in lstChild2)
                                    {
                                        item2.IsActive = true;
                                        _IInvestorGroupService.Update(item2);
                                    }
                                }

                            }

                            _IInvestorGroupService.Update(temp);
                        }
                    }
                }
                _IInvestorGroupService.CommitTran();
            }
            catch
            {
                _IInvestorGroupService.RolbackTran();
            }

            #endregion

            #region xử lý form, kết thúc form
            InvestorGroupCode = txtInvestorGroupCode.Text;
            this.Close();
            isClose = false;
            #endregion
        }

        /// <summary>
        /// Sự kiện nút đóng
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            isClose = true;
            this.Close();
        }
        #endregion

        #region Utils
        /// <summary>
        /// get hoặc set một đối tượng vào giao diện xử lý
        /// </summary>
        /// <param name="input">đối tượng</param>
        /// <param name="isGet">True: get giá trị từ control gán vào đối tượng| False: set giá trị đối tượng vào control</param>
        /// <returns></returns>
        InvestorGroup ObjandGUI(InvestorGroup input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();  //Thêm mới
                input.InvestorGroupCode = txtInvestorGroupCode.Text;
                input.InvestorGroupName = txtInvestorGroupName.Text;
                //từ tài khoản
                InvestorGroup temp0 = (InvestorGroup)Utils.getSelectCbbItem(cbbParentID);
                if (temp0 == null) input.ParentID = null;
                else input.ParentID = temp0.ID;
                //========> input.OrderFixCode chưa xét
                input.IsActive = chkIsActive.Checked;
            }
            else
            {
                txtInvestorGroupCode.Text = input.InvestorGroupCode;
                txtInvestorGroupName.Text = input.InvestorGroupName;
                //từ tài khoản
                foreach (var item in cbbParentID.Rows)
                {
                    if ((item.ListObject as InvestorGroup).ID == input.ParentID) cbbParentID.SelectedRow = item;
                }
                chkIsActive.Checked = input.IsActive;
            }
            return input;
        }

        bool CheckError()
        {
            bool kq = true;
            //Check Error Chung

            //Check Error Riêng từng Form
            if (string.IsNullOrEmpty(txtInvestorGroupCode.Text) || string.IsNullOrEmpty(txtInvestorGroupName.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }
            return kq;
        }

        bool CheckCode()
        {
            bool kq = true;
            if (string.IsNullOrEmpty(txtInvestorGroupCode.Text) || string.IsNullOrEmpty(txtInvestorGroupName.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }

            //check trùng code
            List<string> lst = _IInvestorGroupService.Query.Select(c => c.InvestorGroupCode).ToList();
            foreach (var x in lst)
            {
                if (txtInvestorGroupCode.Text.Equals(x))
                {
                    MSG.Warning(string.Format(resSystem.MSG_Catalog_Account6, "nhóm Nhà đầu tư"));
                    return false;
                }
            }
            return kq;
        }
        #endregion

        private void FInvestorGroupDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
