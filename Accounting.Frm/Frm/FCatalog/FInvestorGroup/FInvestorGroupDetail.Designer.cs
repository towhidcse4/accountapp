﻿namespace Accounting
{
    partial class FInvestorGroupDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.chkIsActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtInvestorGroupName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblInvestorGroupName = new Infragistics.Win.Misc.UltraLabel();
            this.cbbParentID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.lblParentID = new Infragistics.Win.Misc.UltraLabel();
            this.txtInvestorGroupCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblInvestorGroupCode = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvestorGroupName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbParentID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvestorGroupCode)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            appearance1.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance1;
            this.btnSave.Location = new System.Drawing.Point(218, 122);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            appearance2.Image = global::Accounting.Properties.Resources.ubtnDelete;
            this.btnClose.Appearance = appearance2;
            this.btnClose.Location = new System.Drawing.Point(305, 122);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // chkIsActive
            // 
            appearance3.TextVAlignAsString = "Middle";
            this.chkIsActive.Appearance = appearance3;
            this.chkIsActive.BackColor = System.Drawing.Color.Transparent;
            this.chkIsActive.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsActive.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkIsActive.Checked = true;
            this.chkIsActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIsActive.Location = new System.Drawing.Point(9, 126);
            this.chkIsActive.Name = "chkIsActive";
            this.chkIsActive.Size = new System.Drawing.Size(111, 22);
            this.chkIsActive.TabIndex = 4;
            this.chkIsActive.Text = "Hoạt động";
            // 
            // ultraGroupBox1
            // 
            appearance4.FontData.BoldAsString = "True";
            this.ultraGroupBox1.Appearance = appearance4;
            this.ultraGroupBox1.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox1.Controls.Add(this.txtInvestorGroupName);
            this.ultraGroupBox1.Controls.Add(this.lblInvestorGroupName);
            this.ultraGroupBox1.Controls.Add(this.cbbParentID);
            this.ultraGroupBox1.Controls.Add(this.lblParentID);
            this.ultraGroupBox1.Controls.Add(this.txtInvestorGroupCode);
            this.ultraGroupBox1.Controls.Add(this.lblInvestorGroupCode);
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(3, 6);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(377, 112);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtInvestorGroupName
            // 
            this.txtInvestorGroupName.AutoSize = false;
            this.txtInvestorGroupName.Location = new System.Drawing.Point(134, 54);
            this.txtInvestorGroupName.MaxLength = 512;
            this.txtInvestorGroupName.Name = "txtInvestorGroupName";
            this.txtInvestorGroupName.Size = new System.Drawing.Size(237, 22);
            this.txtInvestorGroupName.TabIndex = 2;
            // 
            // lblInvestorGroupName
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.TextVAlignAsString = "Middle";
            this.lblInvestorGroupName.Appearance = appearance5;
            this.lblInvestorGroupName.Location = new System.Drawing.Point(9, 54);
            this.lblInvestorGroupName.Name = "lblInvestorGroupName";
            this.lblInvestorGroupName.Size = new System.Drawing.Size(126, 22);
            this.lblInvestorGroupName.TabIndex = 22;
            this.lblInvestorGroupName.Text = "Tên nhóm nhà đầu tư (*)";
            // 
            // cbbParentID
            // 
            this.cbbParentID.AllowNull = Infragistics.Win.DefaultableBoolean.True;
            this.cbbParentID.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.SuggestAppend;
            this.cbbParentID.AutoSize = false;
            this.cbbParentID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbParentID.LimitToList = true;
            this.cbbParentID.Location = new System.Drawing.Point(134, 81);
            this.cbbParentID.Name = "cbbParentID";
            this.cbbParentID.Size = new System.Drawing.Size(237, 22);
            this.cbbParentID.TabIndex = 3;
            this.cbbParentID.TabStop = false;
            // 
            // lblParentID
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextVAlignAsString = "Middle";
            this.lblParentID.Appearance = appearance6;
            this.lblParentID.Location = new System.Drawing.Point(9, 81);
            this.lblParentID.Name = "lblParentID";
            this.lblParentID.Size = new System.Drawing.Size(92, 22);
            this.lblParentID.TabIndex = 16;
            this.lblParentID.Text = "Thuộc nhóm";
            // 
            // txtInvestorGroupCode
            // 
            this.txtInvestorGroupCode.AutoSize = false;
            this.txtInvestorGroupCode.Location = new System.Drawing.Point(134, 27);
            this.txtInvestorGroupCode.MaxLength = 25;
            this.txtInvestorGroupCode.Name = "txtInvestorGroupCode";
            this.txtInvestorGroupCode.Size = new System.Drawing.Size(237, 22);
            this.txtInvestorGroupCode.TabIndex = 1;
            // 
            // lblInvestorGroupCode
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.TextVAlignAsString = "Middle";
            this.lblInvestorGroupCode.Appearance = appearance7;
            this.lblInvestorGroupCode.Location = new System.Drawing.Point(9, 27);
            this.lblInvestorGroupCode.Name = "lblInvestorGroupCode";
            this.lblInvestorGroupCode.Size = new System.Drawing.Size(126, 22);
            this.lblInvestorGroupCode.TabIndex = 0;
            this.lblInvestorGroupCode.Text = "Mã nhóm nhà đầu tư (*)";
            // 
            // FInvestorGroupDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(383, 156);
            this.Controls.Add(this.ultraGroupBox1);
            this.Controls.Add(this.chkIsActive);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.Name = "FInvestorGroupDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FInvestorGroupDetail_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtInvestorGroupName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbParentID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvestorGroupCode)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsActive;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraLabel lblParentID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtInvestorGroupCode;
        private Infragistics.Win.Misc.UltraLabel lblInvestorGroupCode;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbParentID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtInvestorGroupName;
        private Infragistics.Win.Misc.UltraLabel lblInvestorGroupName;
    }
}
