﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Accounting.TextMessage;
using System.Data;

namespace Accounting
{
    public partial class FInvestorGroup : CatalogBase //UserControl
    {
        #region khai báo
        private readonly IInvestorGroupService _IInvestorGroupService;
        List<InvestorGroup> dsInvestorGroup = new List<InvestorGroup>();
        #endregion

        #region khởi tạo
        public FInvestorGroup()
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _IInvestorGroupService = IoC.Resolve<IInvestorGroupService>();

            this.uTree.InitializeDataNode += new Infragistics.Win.UltraWinTree.InitializeDataNodeEventHandler(Utils.uTree_InitializeDataNode);
            this.uTree.AfterDataNodesCollectionPopulated += new Infragistics.Win.UltraWinTree.AfterDataNodesCollectionPopulatedEventHandler(Utils.uTree_AfterDataNodesCollectionPopulated);
            this.uTree.ColumnSetGenerated += new Infragistics.Win.UltraWinTree.ColumnSetGeneratedEventHandler(this.uTree_ColumnSetGenerated);
            this.uTree.MouseDown += new System.Windows.Forms.MouseEventHandler(this.uTree_MouseDown);
            this.uTree.DoubleClick += new System.EventHandler(this.uTree_DoubleClick);
            #endregion

            #region Tạo dữ liệu ban đầu
            LoadDuLieu();
            #endregion
        }

        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }
        private void LoadDuLieu(bool configTree)
        {
            WaitingFrm.StartWaiting();
            #region Lấy dữ liệu từ CSDL
            dsInvestorGroup = _IInvestorGroupService.GetAll_OrderBy();
            _IInvestorGroupService.UnbindSession(dsInvestorGroup);
            dsInvestorGroup = _IInvestorGroupService.GetAll_OrderBy();
            #endregion

            #region Xử lý dữ liệu
            #endregion

            #region hiển thị và xử lý hiển thị
            DataSet ds = Utils.ToDataSet<InvestorGroup>(dsInvestorGroup, ConstDatabase.InvestorGroup_TableName);
            uTree.SetDataBinding(ds, ConstDatabase.InvestorGroup_TableName);

            if (configTree) ConfigTree(uTree);
            #endregion
            WaitingFrm.StopWaiting();
        }
        #endregion

        #region Nghiệp vụ
        #region ContentMenu
        private void tsmAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void tsmEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            ResetFunction();
        }
        protected override void ResetFunction()
        {
            LoadDuLieu(false);
        }
        #endregion

        #region Button
        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }
        #endregion


        /// <summary>
        /// Nghiệp vụ Add
        /// </summary>
        protected override void AddFunction()
        {
            if (uTree.SelectedNodes.Count > 0)
            {
                InvestorGroup temp = dsInvestorGroup.Single(k => k.ID == (new Guid(uTree.SelectedNodes[0].Text)));
                new FInvestorGroupDetail(temp, true).ShowDialog(this);
                if (!FInvestorGroupDetail.isClose) LoadDuLieu();
            }
            else
            {
                new FInvestorGroupDetail().ShowDialog(this);
                if (!FInvestorGroupDetail.isClose) LoadDuLieu();
            }
        }

        /// <summary>
        /// Nghiệp vụ Edit
        /// </summary>
        protected override void EditFunction()
        {
            if (uTree.SelectedNodes.Count == 0 && uTree.ActiveNode != null)
            {
                uTree.ActiveNode.Selected = true;
            }
            if (uTree.SelectedNodes.Count > 0)
            {
                InvestorGroup temp = dsInvestorGroup.Single(k => k.ID == (new Guid(uTree.SelectedNodes[0].Text)));
                if (Utils.checkRelationVoucher_InvestorGroup(temp))
                {
                    MSG.Error("Không được sửa Nhóm nhà đầu tư vì có phát sinh chứng từ liên quan");
                    return;
                }
                else
                {
                new FInvestorGroupDetail(temp).ShowDialog(this);
                if (!FInvestorGroupDetail.isClose) LoadDuLieu();
                }
            }
            else
                Accounting.TextMessage.MSG.Error(string.Format(resSystem.MSG_Catalog3,"một Nhóm nhà đầu tư"));
        }

        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        protected override void DeleteFunction()
        {
            if (uTree.SelectedNodes.Count == 0 && uTree.ActiveNode != null)
            {
                uTree.ActiveNode.Selected = true;
            }
            if (uTree.SelectedNodes.Count > 0)
            {
                InvestorGroup temp = dsInvestorGroup.Single(k => k.ID == (new Guid(uTree.SelectedNodes[0].Text)));
                if (MSG.Question(string.Format(resSystem.MSG_System_05, "<" + temp.InvestorGroupCode + ">")) == System.Windows.Forms.DialogResult.Yes)
                {
                    if (Utils.checkRelationVoucher_InvestorGroup(temp))
                    {
                        MSG.Error("Không được xóa Nhóm nhà đầu tư vì có phát sinh chứng từ liên quan");
                        return;
                    }
                    _IInvestorGroupService.BeginTran();
                    //Kiểm tra xem đối tượng có con không nếu có con thì không được phép xóa
                    List<InvestorGroup> lstChild =
                            _IInvestorGroupService.Query.Where(p => p.ParentID == temp.ID).ToList();
                    if (lstChild.Count > 0)
                    {
                        MSG.Error(string.Format(resSystem.MSG_Catalog_Tree, "nhóm Nhà đầu tư"));
                        return;
                    }
                    //Check xem cha còn có con khác không nếu không thì chuyển trạng thái IsParentNode = false
                    if (temp.ParentID != null)
                    {
                        InvestorGroup parent = _IInvestorGroupService.Getbykey((Guid)temp.ParentID);
                        int checkChildOldParent = _IInvestorGroupService.Query.Count(p => p.ParentID == temp.ParentID);
                        if (checkChildOldParent <= 1)
                        {
                            parent.IsParentNode = false;
                        }
                        _IInvestorGroupService.Update(parent);
                        _IInvestorGroupService.Delete(temp);
                    }
                    if (temp.ParentID == null)
                    {
                        _IInvestorGroupService.Delete(temp);
                    }
                    _IInvestorGroupService.CommitTran();
                    Utils.ClearCacheByType<InvestorGroup>();
                    LoadDuLieu();
                }
            }
            else
                MSG.Error(string.Format(resSystem.MSG_Catalog2,"một Nhóm nhà đầu tư"));
        }

        private void uTree_DoubleClick(object sender, EventArgs e)
        {
            if (uTree.SelectedNodes.Count > 0)
                EditFunction();
        }

        private void uTree_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, cms4Grid);
        }

        private void uTree_ColumnSetGenerated(object sender, Infragistics.Win.UltraWinTree.ColumnSetGeneratedEventArgs e)
        {
            Utils.uTree_ColumnSetGenerated(sender, e, ConstDatabase.InvestorGroup_TableName);
        }
        #endregion

        #region Utils
        void ConfigTree(Infragistics.Win.UltraWinTree.UltraTree ultraTree)
        {//hàm chung
            Utils.ConfigTree(ultraTree, TextMessage.ConstDatabase.InvestorGroup_TableName);
        }

        #endregion


        //Tìm kiếm
        

        void searchFunction()
        {
            new FInvestorGroupSearch().ShowDialog(this);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            searchFunction();
        }

        private void FInvestorGroup_FormClosing(object sender, FormClosingEventArgs e)
        {
            uTree = null;
        }

        private void FInvestorGroup_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
