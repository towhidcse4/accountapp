﻿using System;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using System.Collections.Generic;
using System.Drawing;
namespace Accounting
{
    public partial class FBankAccountDetailDetail : DialogForm //UserControl
    {
        #region khai báo
        private readonly IBankAccountDetailService _IBankAccountDetailService;
        private readonly IBankService _IBankService;

        BankAccountDetail _Select = new BankAccountDetail();

        private Guid _id;
        public Guid Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private bool Them = true;
        public static bool isClose = true;
        #endregion

        #region khởi tạo
        public FBankAccountDetailDetail()
        {//Thêm
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            //Khai báo các webservices
            _IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            _IBankService = IoC.Resolve<IBankService>();

            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----
            imgIcon.Enabled = false;
            this.Text = "Thêm Tài Khoản Ngân Hàng";
            chkIsActive.CheckState = CheckState.Checked;
            chkIsActive.Visible = false;
            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion
        }

        public FBankAccountDetailDetail(BankAccountDetail temp)
        {//Sửa
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _Select = temp;
            Them = false;
            c.Enabled = false;

            //Khai báo các webservices
            _IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            _IBankService = IoC.Resolve<IBankService>();

            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion
            imgIcon.Enabled = false;
            #region Fill dữ liệu Obj vào control
            this.Text = "Sửa Tài Khoản Ngân Hàng";
            chkIsActive.Visible = true;
            ObjandGUI(temp, false);
            #endregion
        }

        private void InitializeGUI()
        {
            //khởi tạo cbb
            //cbbBankID.DataSource = _IBankService.GetListBankIsActive(true);
            //cbbBankID.DisplayMember = "BankCode";
            //Utils.ConfigGrid(cbbBankID, ConstDatabase.Bank_TableName);
            this.ConfigCombo(Utils.ListBank, cbbBankID, "BankCode", "ID");
        }
        #endregion

        #region Button Event
        /// <summary>
        /// Sự kiện nút Lưu lại
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion

            //Gui to object
            #region Fill dữ liệu control vào obj
            BankAccountDetail temp = Them ? new BankAccountDetail() : _IBankAccountDetailService.Getbykey(_Select.ID);
            temp = ObjandGUI(temp, true);
            #endregion

            #region Thao tác CSDL
            _IBankAccountDetailService.BeginTran();
            if (Them)
            {
                if (!CheckCode()) return;
                _IBankAccountDetailService.CreateNew(temp);
            }
            else _IBankAccountDetailService.Update(temp);

            _IBankAccountDetailService.CommitTran();
            _id = temp.ID;
            #endregion

            #region xử lý form, kết thúc form
            Utils.ListBankAccountDetail.Clear();
            //Utils.AddToBindingList(Utils.ListBankAccountDetail, _IBankAccountDetailService.GetIsActive(true));
            this.Close();
            isClose = false;
            #endregion
        }

        /// <summary>
        /// Sự kiện nút đóng
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            isClose = true;
            DialogResult = DialogResult.Cancel;
            this.Close();
        }
        #endregion

        #region Utils
        /// <summary>
        /// get hoặc set một đối tượng vào giao diện xử lý
        /// </summary>
        /// <param name="input">đối tượng</param>
        /// <param name="isGet">True: get giá trị từ control gán vào đối tượng| False: set giá trị đối tượng vào control</param>
        /// <returns></returns>
        /// Bank temp0

        BankAccountDetail ObjandGUI(BankAccountDetail input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();  //Thêm mới
                //Lấy cbb ID ngân hàng
                Bank temp0 = (Bank)Utils.getSelectCbbItem(cbbBankID); if (temp0 != null)
                {
                    input.BankID = temp0.ID;
                    input.BankName = temp0.BankName;
                }
                input.BankAccount = c.Text;
                input.BankBranchName = txtBankBranchName.Text;
                input.Address = txtAddress.Text;
                input.Description = txtDescription.Text;
                //input.BankBranchName = txtBankBranchName.Text;
                input.IsActive = chkIsActive.Checked;

                foreach (var item in cbbBankID.Rows)
                {
                    Bank temp = item.ListObject as Bank;
                    if (temp0.ID == input.BankID)
                    {
                        //cbbBankID.SelectedRow = item;
                        if (!Them)
                        {
                            if(imgIcon.ContentAreaAppearance.ImageBackground != null)
                            {
                                temp0.Icon = imgIcon.ContentAreaAppearance.ImageBackground.ImageToByteArray();
                            }
                            
                            _IBankService.Update(temp0);
                        }

                    }
                }
            }
            else
            {
                foreach (var item in cbbBankID.Rows)
                {
                    Bank temp = item.ListObject as Bank;
                    if (temp.ID == input.BankID)
                    {
                        cbbBankID.SelectedRow = item;
                        if (temp.Icon != null)
                        {
                            imgIcon.ContentAreaAppearance.ImageBackground = Utils.ByteArrayToImage(temp.Icon);
                        }
                    }
                }
                c.Text = input.BankAccount;
                txtBankBranchName.Text = input.BankBranchName;
                txtAddress.Text = input.Address;
                txtDescription.Text = input.Description;
                //txtBankBranchName.Text = input.BankBranchName;
                chkIsActive.Checked = input.IsActive;
            }
            return input;
        }

        bool CheckCode()
        {
            bool kq = true;
            //Check Error Chung

            //List<string> hh = _IBankAccountDetailService.Query.Select(a => a.BankAccount).ToList();
            List<string> hh = _IBankAccountDetailService.GetListBankAccount();
            foreach (var item in hh)
            {
                if (item.Equals(c.Text))
                {
                    MSG.Warning(string.Format(resSystem.MSG_Catalog, "số Tài khoản ngân hàng", item));
                    //MessageBox.Show("Số tài khoản " + item + " đã tồn tại trong danh sách", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }

            return kq;
        }

        bool CheckError()
        {
            bool kq = true;
            //Check Error Chung

            //Check Error Riêng từng Form
            if (string.IsNullOrEmpty(c.Text) || cbbBankID.SelectedRow == null)
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }

            return kq;
        }
        #endregion

        private void imgIcon_Click(object sender, EventArgs e)
        {
            var openFile = new OpenFileDialog
            {
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                Filter = "file hinh|*.jpg|all file|*.*"
            };
            openFile.InitialDirectory = @"E:\";

            var result = openFile.ShowDialog(this);
            if (result == DialogResult.OK || result == DialogResult.Yes)
            {
                //giới hạn dung lượng ảnh cho phép là 50 kb
                double fileSize = (double)new System.IO.FileInfo(openFile.FileName).Length / 1024;
                if (fileSize > 50)
                {
                    MSG.Error(resSystem.MSG_Catalog_FBankAccountDetail);
                    return;
                }
                //Kích thước ảnh cho phép không lớn hơn 128x128
                Image temp = System.Drawing.Image.FromFile(openFile.FileName);
                if (temp.Width > 128 || temp.Height > 128)
                {
                    MSG.Error(resSystem.MSG_Catalog_FBankAccountDetail1);
                    return;
                }
                imgIcon.ContentAreaAppearance.ImageBackground = System.Drawing.Image.FromFile(openFile.FileName);
            }
        }

        private void txtBankAccount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar) || Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
        private void cbbBankID_Close(object sender, EventArgs e)
        {
            Bank temp0 = (Bank)Utils.getSelectCbbItem(cbbBankID);
            if (temp0!=null && temp0.Icon != null)
                imgIcon.ContentAreaAppearance.ImageBackground = Utils.ByteArrayToImage(temp0.Icon);
            else
                imgIcon.ContentAreaAppearance.ImageBackground = null;
        }
        private void cbbBankID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            new FBankDetail().ShowDialog(this);
            InitializeGUI();
            Utils.ClearCacheByType<Bank>();
            cbbBankID.SelectedText = FBankDetail.bankcode;

            Bank temp0 = (Bank)Utils.getSelectCbbItem(cbbBankID);
            if (temp0 != null && temp0.Icon != null)
                imgIcon.ContentAreaAppearance.ImageBackground = Utils.ByteArrayToImage(temp0.Icon);
            else
                imgIcon.ContentAreaAppearance.ImageBackground = null;
            //imgIcon.ContentAreaAppearance.ImageBackground = Utils.ByteArrayToImage(temp.Icon);
        }

        private void FBankAccountDetailDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
