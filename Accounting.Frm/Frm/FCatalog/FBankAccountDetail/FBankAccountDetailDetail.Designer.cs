﻿namespace Accounting
{
    partial class FBankAccountDetailDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.lblBankBranchName = new Infragistics.Win.Misc.UltraLabel();
            this.cbbBankID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.imgIcon = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtDescription = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblDescription = new Infragistics.Win.Misc.UltraLabel();
            this.txtAddress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblAddress = new Infragistics.Win.Misc.UltraLabel();
            this.txtBankBranchName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblBankID = new Infragistics.Win.Misc.UltraLabel();
            this.c = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblBankAccount = new Infragistics.Win.Misc.UltraLabel();
            this.chkIsActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankBranchName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox1.Controls.Add(this.lblBankBranchName);
            this.ultraGroupBox1.Controls.Add(this.cbbBankID);
            this.ultraGroupBox1.Controls.Add(this.imgIcon);
            this.ultraGroupBox1.Controls.Add(this.txtDescription);
            this.ultraGroupBox1.Controls.Add(this.lblDescription);
            this.ultraGroupBox1.Controls.Add(this.txtAddress);
            this.ultraGroupBox1.Controls.Add(this.lblAddress);
            this.ultraGroupBox1.Controls.Add(this.txtBankBranchName);
            this.ultraGroupBox1.Controls.Add(this.lblBankID);
            this.ultraGroupBox1.Controls.Add(this.c);
            this.ultraGroupBox1.Controls.Add(this.lblBankAccount);
            appearance8.FontData.BoldAsString = "True";
            this.ultraGroupBox1.HeaderAppearance = appearance8;
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(5, 6);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(522, 252);
            this.ultraGroupBox1.TabIndex = 26;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // lblBankBranchName
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.TextVAlignAsString = "Middle";
            this.lblBankBranchName.Appearance = appearance1;
            this.lblBankBranchName.Location = new System.Drawing.Point(9, 85);
            this.lblBankBranchName.Name = "lblBankBranchName";
            this.lblBankBranchName.Size = new System.Drawing.Size(148, 22);
            this.lblBankBranchName.TabIndex = 31;
            this.lblBankBranchName.Text = "Mở tại chi nhánh";
            // 
            // cbbBankID
            // 
            this.cbbBankID.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.SuggestAppend;
            this.cbbBankID.AutoSize = false;
            appearance2.Image = global::Accounting.Properties.Resources.plus;
            editorButton1.Appearance = appearance2;
            appearance3.Image = global::Accounting.Properties.Resources.plus_press;
            editorButton1.PressedAppearance = appearance3;
            this.cbbBankID.ButtonsRight.Add(editorButton1);
            this.cbbBankID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbBankID.Location = new System.Drawing.Point(159, 56);
            this.cbbBankID.MaxLength = 512;
            this.cbbBankID.Name = "cbbBankID";
            this.cbbBankID.Size = new System.Drawing.Size(237, 22);
            this.cbbBankID.TabIndex = 501;
            this.cbbBankID.AfterCloseUp += new System.EventHandler(this.cbbBankID_Close);
            this.cbbBankID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.cbbBankID_EditorButtonClick);
            // 
            // imgIcon
            // 
            this.imgIcon.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.imgIcon.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOutsideBorder;
            this.imgIcon.Location = new System.Drawing.Point(405, 27);
            this.imgIcon.Name = "imgIcon";
            this.imgIcon.Size = new System.Drawing.Size(104, 82);
            this.imgIcon.TabIndex = 30;
            this.imgIcon.Text = "Biểu tượng";
            this.imgIcon.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            this.imgIcon.Click += new System.EventHandler(this.imgIcon_Click);
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(159, 143);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(350, 71);
            this.txtDescription.TabIndex = 504;
            // 
            // lblDescription
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.lblDescription.Appearance = appearance4;
            this.lblDescription.Location = new System.Drawing.Point(9, 143);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(148, 22);
            this.lblDescription.TabIndex = 28;
            this.lblDescription.Text = "Ghi chú";
            // 
            // txtAddress
            // 
            this.txtAddress.AutoSize = false;
            this.txtAddress.Location = new System.Drawing.Point(159, 114);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(350, 22);
            this.txtAddress.TabIndex = 503;
            // 
            // lblAddress
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.TextVAlignAsString = "Middle";
            this.lblAddress.Appearance = appearance5;
            this.lblAddress.Location = new System.Drawing.Point(9, 114);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(148, 22);
            this.lblAddress.TabIndex = 26;
            this.lblAddress.Text = "Địa chỉ nơi mở";
            // 
            // txtBankBranchName
            // 
            this.txtBankBranchName.AutoSize = false;
            this.txtBankBranchName.Location = new System.Drawing.Point(159, 85);
            this.txtBankBranchName.Name = "txtBankBranchName";
            this.txtBankBranchName.Size = new System.Drawing.Size(237, 22);
            this.txtBankBranchName.TabIndex = 502;
            // 
            // lblBankID
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextVAlignAsString = "Middle";
            this.lblBankID.Appearance = appearance6;
            this.lblBankID.Location = new System.Drawing.Point(9, 56);
            this.lblBankID.Name = "lblBankID";
            this.lblBankID.Size = new System.Drawing.Size(148, 22);
            this.lblBankID.TabIndex = 22;
            this.lblBankID.Text = "Tên NH(*)";
            // 
            // c
            // 
            this.c.AutoSize = false;
            this.c.Location = new System.Drawing.Point(159, 27);
            this.c.MaxLength = 50;
            this.c.Name = "c";
            this.c.Size = new System.Drawing.Size(237, 22);
            this.c.TabIndex = 500;
            // 
            // lblBankAccount
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.TextVAlignAsString = "Middle";
            this.lblBankAccount.Appearance = appearance7;
            this.lblBankAccount.Location = new System.Drawing.Point(9, 27);
            this.lblBankAccount.Name = "lblBankAccount";
            this.lblBankAccount.Size = new System.Drawing.Size(148, 22);
            this.lblBankAccount.TabIndex = 0;
            this.lblBankAccount.Text = "Số tài khoản (*)";
            // 
            // chkIsActive
            // 
            this.chkIsActive.BackColor = System.Drawing.Color.Transparent;
            this.chkIsActive.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsActive.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkIsActive.Checked = true;
            this.chkIsActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIsActive.Location = new System.Drawing.Point(9, 268);
            this.chkIsActive.Name = "chkIsActive";
            this.chkIsActive.Size = new System.Drawing.Size(103, 22);
            this.chkIsActive.TabIndex = 505;
            this.chkIsActive.Text = "Hoạt động";
            // 
            // btnClose
            // 
            appearance9.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance9;
            this.btnClose.Location = new System.Drawing.Point(436, 264);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 507;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            appearance10.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance10;
            this.btnSave.Location = new System.Drawing.Point(355, 264);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 506;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // FBankAccountDetailDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(533, 301);
            this.Controls.Add(this.ultraGroupBox1);
            this.Controls.Add(this.chkIsActive);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FBankAccountDetailDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thông tin tài khoản ngân hàng";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FBankAccountDetailDetail_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankBranchName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsActive;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor c;
        private Infragistics.Win.Misc.UltraLabel lblBankAccount;
        private Infragistics.Win.Misc.UltraLabel lblBankID;
        private Infragistics.Win.Misc.UltraGroupBox imgIcon;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDescription;
        private Infragistics.Win.Misc.UltraLabel lblDescription;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAddress;
        private Infragistics.Win.Misc.UltraLabel lblAddress;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtBankBranchName;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbBankID;
        private Infragistics.Win.Misc.UltraLabel lblBankBranchName;
    }
}
