﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using Accounting.TextMessage;
using System.Linq;

namespace Accounting
{
    public partial class FBankAccountDetail : CatalogBase //UserControl
    {
        #region khai báo
        private readonly IBankAccountDetailService _IBankAccountDetailService;
        private readonly IBankService _IIBankService;
        #endregion

        #region khởi tạo
        public FBankAccountDetail()
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            ToolTip ToolTip1 = new ToolTip();
            ToolTip1.SetToolTip(btnAdd, "Thêm (Ctrl + N)");
            ToolTip ToolTip2 = new ToolTip();
            ToolTip2.SetToolTip(btnEdit, "Sửa (Ctrl + E)");
            ToolTip ToolTip3 = new ToolTip();
            ToolTip3.SetToolTip(btnDelete, "Xóa (Ctrl + D)");
            #endregion

            #region Thiết lập ban đầu cho Form
            _IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            _IIBankService = IoC.Resolve<IBankService>();
            this.uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            #endregion

            #region Tạo dữ liệu ban đầu
            LoadDuLieu();
            if (uGrid.Rows.Count > 0)
            {
                uGrid.Rows[0].Selected = true;
            }
            #endregion
        }

        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }
        private void LoadDuLieu(bool configGrid)
        {
            WaitingFrm.StartWaiting();
            #region Lấy dữ liệu từ CSDL
            List<BankAccountDetail> list = _IBankAccountDetailService.GetAllOrderBy();
            _IBankAccountDetailService.UnbindSession(list);
            list = _IBankAccountDetailService.GetAllOrderBy();
            List<Bank> listBank = _IIBankService.GetAll();
            #endregion

            #region Xử lý dữ liệu
            #endregion

            #region hiển thị và xử lý hiển thị
            uGrid.DataSource = list.ToArray();

            foreach (var item in list)
            {
                Bank temp = listBank.Where(p => p.ID == item.BankID).SingleOrDefault();
                item.BankIDView = temp != null ? temp.BankName : string.Empty;
            }
            if (configGrid) ConfigGrid(uGrid);
            #endregion
            WaitingFrm.StopWaiting();
        }
        #endregion

        #region Nghiệp vụ
        #region ContentMenu
        private void tsmAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void tsmEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            ResetFunction();
        }
        protected override void ResetFunction()
        {
            LoadDuLieu(false);
        }
        #endregion

        #region Button
        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }
        #endregion

        private void uGrid_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            EditFunction();
        }

        /// <summary>
        /// Nghiệp vụ Add
        /// </summary>
        protected override void AddFunction()
        {
            new FBankAccountDetailDetail().ShowDialog(this);
            if (!FBankAccountDetailDetail.isClose) LoadDuLieu();
        }

        /// <summary>
        /// Nghiệp vụ Edit
        /// </summary>
        protected override void EditFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                BankAccountDetail temp = uGrid.Selected.Rows[0].ListObject as BankAccountDetail;
                new FBankAccountDetailDetail(temp).ShowDialog(this);
                if (!FBankAccountDetailDetail.isClose) LoadDuLieu();
            }
            else
                Accounting.TextMessage.MSG.Error(string.Format(Accounting.TextMessage.resSystem.MSG_Catalog3,"một Tài khoản ngân hàng"));
        }

        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        protected override void DeleteFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                BankAccountDetail temp = uGrid.Selected.Rows[0].ListObject as BankAccountDetail;
                if (MSG.Question(string.Format(resSystem.MSG_System_05, temp.BankAccount)) == System.Windows.Forms.DialogResult.Yes)
                {
                    try
                    {
                        if (!Utils.checkRelationVoucher(temp))
                        {
                            _IBankAccountDetailService.BeginTran();
                            _IBankAccountDetailService.Delete(temp);
                            _IBankAccountDetailService.CommitTran();
                            Utils.ClearCacheByType<BankAccountDetail>();
                            LoadDuLieu();
                        }
                        else
                        {
                            MSG.Error("Cần xóa chứng từ kế toán có liên quan trước khi xóa dữ liệu danh mục");
                        }
                    }
                    catch
                    {
                        _IBankAccountDetailService.RolbackTran();
                    }


                }
            }
            else
                MSG.Error(string.Format(resSystem.MSG_Catalog2,"một Tài khoản ngân hàng"));
        }
        #endregion

        #region Utils
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, TextMessage.ConstDatabase.BankAccountDetail_TableName);
        }
        #endregion

        private void FBankAccountDetail_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGrid.ResetText();
            uGrid.ResetUpdateMode();
            uGrid.ResetExitEditModeOnLeave();
            uGrid.ResetRowUpdateCancelAction();
            uGrid.DataSource = null;
            uGrid.Layouts.Clear();
            uGrid.ResetLayouts();
            uGrid.ResetDisplayLayout();
            uGrid.Refresh();
            uGrid.ClearUndoHistory();
            uGrid.ClearXsdConstraints();
        }

        private void FBankAccountDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
