﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.IService;
using Accounting.Core.Domain;
using FX.Core;
using Accounting.TextMessage;
using System.Globalization;
using Accounting.Model;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using System.Drawing;

namespace Accounting
{
    public partial class FCostSetDetail : DialogForm
    {
        #region khai báo
        private readonly ICostSetService _ICostSetService;
        private readonly IAccountingObjectService _IAccountingObjectService;
        private readonly IMaterialGoodsService _IMaterialGoodsService;
        private readonly ICostSetMaterialGoodsService _ICostSetMaterialGoodsService;
        CostSet _Select = new CostSet();
        private Guid _id;
        public Guid Id
        {
            get { return _id; }
            set { _id = value; }
        }
        bool IsAdd = true;
        public static bool isClose = true;
        public static string CostSetCode;
        #endregion

        #region khởi tạo
        public FCostSetDetail()
        {//cho thêm
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            this.Text = "Thêm mới Đối tượng tập hợp chi phí";
            #endregion

            #region Thiết lập ban đầu cho Form
            //Khai báo các webservices
            _ICostSetService = IoC.Resolve<ICostSetService>();
            _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();
            _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
            _ICostSetMaterialGoodsService = IoC.Resolve<ICostSetMaterialGoodsService>();
            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>
            IsAdd = true;
            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            InitializeGUI();
            test1();
            #endregion
            #endregion
        }
        public FCostSetDetail(CostSet temp, bool edit)
        {// cho Sửa
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _Select = temp;
            IsAdd = !edit;
            cbbCostSetType.Enabled = false;
            txtCostSetCode.Enabled = false;
            //Khai báo các webservices
            _ICostSetService = IoC.Resolve<ICostSetService>();
            _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();
            _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
            _ICostSetMaterialGoodsService = IoC.Resolve<ICostSetMaterialGoodsService>();
            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion

            #region Fill dữ liệu Obj vào control
            if (temp.CostSetType == 2) test(temp);
            ObjandGUI(temp, false);
            this.Text = "Sửa Đối tượng tập hợp chi phí";
            #endregion
        }
        private void InitializeGUI()
        {
            //khởi tạo cbb
            cbbCostSetType.DataSource = FCostSet.dicCostSetType.Values.ToList();
            cbbCostSetType.SelectedIndex = 1;
            var lst = Utils.ListMaterialGoods.Where(x => x.MaterialGoodsType == 3 || x.MaterialGoodsType == 1).ToList();
            if (lst.Count > 0)
            {
                for (int i = 0; i < lst.Count; i++)
                    if (Utils.ListCostSet.Where(x => x.CostSetType == 4 && x.ID != _Select.ID).ToList().Any(x => x.CostSetCode == lst[i].MaterialGoodsCode))
                    {
                        lst.RemoveAt(i);
                        i--;
                    }
            }
            this.ConfigCombo(lst, cbbThanhPham, "MaterialGoodsCode", "MaterialGoodsCode");
        }
        #endregion
        List<UltraGridRow> lsRowHasNotificationCell = new List<UltraGridRow>();
        UltraGridRow indexRow;
        #region Button Event
        private void btnSave_Click_1(object sender, EventArgs e)
        {
            if (lsRowHasNotificationCell.Count > 0)
            {
                MSG.Error("Có lỗi xảy ra khi lưu đối tượng chi phí.");
                return;
            }
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion

            #region Thao tác CSDL
            try
            {
                _ICostSetService.BeginTran();
                CostSet temp;
                if (IsAdd) // them moi
                {
                    temp = new CostSet();
                    temp = ObjandGUI(temp, true);
                    List<CostSet> costSets = Utils.ListCostSet.ToList();
                    if (costSets.Count > 0)
                    {
                        if (costSets.Any(x => x.CostSetCode == temp.CostSetCode))
                        {
                            MSG.Warning("Mã ĐTTHCP đã tồn tại!");
                            _ICostSetService.RolbackTran();
                            return;
                        }
                    }
                    _ICostSetService.CreateNew(temp);
                    if (temp.CostSetType == 2)
                    {
                        List<Giathanh> lstuGrid1 = ((BindingList<Giathanh>)uGrid1.DataSource).ToList();
                        foreach (Giathanh item in lstuGrid1.GroupBy(x => x.MaterialGoodsCode).Select(x => x.First()))
                        {
                            MaterialGoods mg = _IMaterialGoodsService.GetAll_ByMaterialGoodsCode(item.MaterialGoodsCode);
                            CostSetMaterialGoods csmg = new CostSetMaterialGoods();
                            if (csmg.ID == Guid.Empty) csmg.ID = Guid.NewGuid();
                            csmg.CostSetID = temp.ID;
                            csmg.MaterialGoodsID = mg.ID;
                            csmg.Description = item.Description;
                            _ICostSetMaterialGoodsService.CreateNew(csmg);
                        }
                    }
                }
                else // cap nhat
                {
                    //Lấy về đối tượng cần sửa
                    temp = _ICostSetService.Getbykey(_Select.ID);
                    //Lưu đối tượng trước khi sửa
                    CostSet tempOriginal = (CostSet)Utils.CloneObject(temp);
                    temp = ObjandGUI(temp, true);
                    _ICostSetService.Update(temp);
                    if (temp.CostSetType == 2)
                    {
                        List<CostSetMaterialGoods> test1s = _ICostSetMaterialGoodsService.GetCostSetMaterialGoodsCostSetID(temp.ID);
                        List<Giathanh> lstuGrid1 = ((BindingList<Giathanh>)uGrid1.DataSource).ToList();
                        if (lstuGrid1.Count() > 0)
                        {
                            foreach (Giathanh item in lstuGrid1.GroupBy(x => x.MaterialGoodsCode).Select(x => x.First()))
                            {
                                MaterialGoods mg = _IMaterialGoodsService.GetAll_ByMaterialGoodsCode(item.MaterialGoodsCode);
                                CostSetMaterialGoods csmg = new CostSetMaterialGoods();
                                csmg.ID = Guid.NewGuid();
                                csmg.CostSetID = temp.ID;
                                csmg.MaterialGoodsID = mg.ID;
                                csmg.Description = item.Description;
                                _ICostSetMaterialGoodsService.CreateNew(csmg);
                            }
                        }
                        if (test1s.Count() > 0)
                        {
                            foreach (CostSetMaterialGoods item in test1s)
                            {
                                _ICostSetMaterialGoodsService.Delete(item);
                            }
                        }
                    }
                }
                _ICostSetService.CommitTran();
                _ICostSetMaterialGoodsService.CommitTran();
                CostSetCode = temp.CostSetCode;
                _id = temp.ID;
            }
            catch (Exception ex)
            {
                _ICostSetService.RolbackTran();
                _ICostSetMaterialGoodsService.RolbackTran();
                MSG.Error(ex.StackTrace); return;
            }
            #endregion

            #region xử lý form, kết thúc form
            Utils.ListCostSet.Clear();
            //Utils.AddToBindingList(Utils.ListCostSet, _ICostSetService.GetByActive_OrderByTreeIsParentNode(true));
            this.Close();
            isClose = false;
            //CostSetCode = txtCostSetCode.Value.ToString();
            #endregion
        }


        private void btnClose_Click_1(object sender, EventArgs e)
        {
            isClose = true;
            DialogResult = DialogResult.Cancel;
            this.Close();
        }
        #endregion

        #region Utils
        /// <summary>
        /// get hoặc set một đối tượng vào giao diện xử lý
        /// </summary>
        /// <param name="input">đối tượng</param>
        /// <param name="isGet">True: get giá trị từ control gán vào đối tượng| False: set giá trị đối tượng vào control</param>
        /// <returns></returns>
        CostSet ObjandGUI(CostSet input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();  //Thêm mới
                input.AccountingObjectID = null;
                input.IsParentNode = false;
                input.Grade = 1;
                input.CostSetType = cbbCostSetType.SelectedIndex;
                input.FinishDate = null;
                input.StartDate = null;
                input.AmountContract = 0;
                input.OrderFixCode = "1";
                if (input.CostSetType == 4)
                {
                    input.CostSetCode = cbbThanhPham.Value.ToString();
                    input.CostSetName = txtThanhPham.Text;
                }
                else
                {
                    input.CostSetCode = txtCostSetCode.Text;
                    input.CostSetName = txtCostSetName.Text;
                }
                input.Description = txtDescription.Text;
                input.IsActive = chkIsActive.CheckState == CheckState.Checked;
            }
            else
            {
                if (IsAdd)
                {
                    txtCostSetCode.Enabled = true;
                    txtCostSetName.Text = "";
                }
                else
                {
                    if (input.CostSetType == 4)
                    {
                        cbbThanhPham.Value = input.CostSetCode;
                        txtThanhPham.Text = input.CostSetName;
                    }
                    else
                    {
                        txtCostSetName.Text = input.CostSetName;
                        txtCostSetCode.Text = input.CostSetCode;
                    }
                    txtDescription.Text = input.Description;
                    cbbCostSetType.SelectedIndex = input.CostSetType;
                    chkIsActive.CheckState = input.IsActive ? CheckState.Checked : CheckState.Unchecked;
                }
            }
            return input;
        }
        String ReadNumber(string number)
        {
            return "";
        }

        #region check error       
        bool CheckError()
        {
            bool kq = true;
            //Check Error Chung

            //Check Error Riêng từng Form
            if (string.IsNullOrEmpty(txtCostSetCode.Text) && cbbCostSetType.SelectedIndex != 4)
            {
                MSG.Error(string.Format(resSystem.MSG_Catalog9, "đối tượng tập hợp chi phí"));
                //MSG.Error("Mã ĐT-THCP không được để trống! Xin nhập lại");
                txtCostSetCode.Focus();
                return false;
            }
            else if (string.IsNullOrEmpty(cbbThanhPham.Text) && cbbCostSetType.SelectedIndex == 4)
            {
                MSG.Error(string.Format(resSystem.MSG_Catalog9, "đối tượng tập hợp chi phí"));
                //MSG.Error("Mã ĐT-THCP không được để trống! Xin nhập lại");
                cbbThanhPham.Focus();
                return false;
            }
            else if (string.IsNullOrEmpty(txtThanhPham.Text) && cbbCostSetType.SelectedIndex == 4)
            {
                MSG.Error(string.Format(resSystem.MSG_Catalog10, "đối tượng tập hợp chi phí"));
                //MSG.Error("Mã ĐT-THCP không được để trống! Xin nhập lại");
                txtThanhPham.Focus();
                return false;
            }
            else if (string.IsNullOrEmpty(txtCostSetName.Text) && cbbCostSetType.SelectedIndex != 4)
            {
                MSG.Error(string.Format(resSystem.MSG_Catalog10, "đối tượng tập hợp chi phí"));
                txtCostSetName.Focus();
                return false;
            }
            else if (string.IsNullOrEmpty(cbbCostSetType.Text))
            {
                MSG.Error(string.Format(resSystem.MSG_Catalog11, "đối tượng tập hợp chi phí"));
                cbbCostSetType.Focus();
                return false;
            }
            if (uGrid1.DataSource != null)
            {
                foreach (var row in uGrid1.Rows)
                {
                    int count = _IMaterialGoodsService.GetAll_ByMaterialGoodsType(3).Count(x => x.MaterialGoodsCode == row.Cells["MaterialGoodsCode"].Text);
                    int count1 = _IMaterialGoodsService.GetAll_ByMaterialGoodsType(2).Count(x => x.MaterialGoodsCode == row.Cells["MaterialGoodsCode"].Text);
                    int count2 = _IMaterialGoodsService.GetAll_ByMaterialGoodsType(1).Count(x => x.MaterialGoodsCode == row.Cells["MaterialGoodsCode"].Text);
                    int count3 = _IMaterialGoodsService.GetAll_ByMaterialGoodsType(0).Count(x => x.MaterialGoodsCode == row.Cells["MaterialGoodsCode"].Text);
                    if (count == 0 && count1 == 0 && count2 == 0 && count3 == 0)
                    {
                        MSG.Error("Dữ liệu không có trong danh mục");
                        return false;
                    }
                }
            }
            return kq;
        }
        #endregion
        #endregion
        #region Template
        public void test(CostSet cd)
        {
            List<Giathanh> listgt = new List<Giathanh>();
            List<CostSetMaterialGoods> test1s = _ICostSetMaterialGoodsService.GetCostSetMaterialGoodsCostSetID(cd.ID);
            List<MaterialGoods> listAllMaterialGoods = Utils.ListMaterialGoods.ToList();
            foreach (var item in test1s)
            {
                //var it = _IMaterialGoodsService.Getbykey(item.MaterialGoodsID);
                var it = listAllMaterialGoods.FirstOrDefault(o => o.ID == item.MaterialGoodsID);
                if (it == null)
                    continue;
                Giathanh giathanh = new Giathanh();
                giathanh.MaterialGoodsCode = it.MaterialGoodsCode;
                giathanh.MaterialGoodsName = it.MaterialGoodsName;
                giathanh.Description = item.Description;
                listgt.Add(giathanh);
            }
            BindingList<Giathanh> bindingList = new BindingList<Giathanh>(listgt);
            uGrid1.DataSource = bindingList;
            Config(uGrid1);
            ConfigGridDong(mauGiaoDien);
        }
        public void test1()
        {

            List<Giathanh> listcp = new List<Giathanh>();

            BindingList<Giathanh> bindingList = new BindingList<Giathanh>(listcp);
            uGrid1.DataSource = bindingList;
            Config(uGrid1);
            ConfigGridDong(mauGiaoDien);
        }

        // Load tab "uGrid2"
        private void Config(UltraGrid utralGrid)
        {
            List<TemplateColumn> dstemplatecolums = new List<TemplateColumn>();

            Dictionary<string, Dictionary<string, TemplateColumn>> dicTemp = new Dictionary<string, Dictionary<string, TemplateColumn>>();
            string nameTable = string.Empty;
            List<string> strColumnName, strColumnCaption, strColumnToolTip = new List<string>();
            List<bool> bolIsReadOnly, bolIsVisible, bolIsVisibleCbb = new List<bool>();
            List<int> intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition = new List<int>();
            List<int> VTbolIsVisible, VTbolIsVisibleCbb = new List<int>();

            strColumnName = new List<string>() { "MaterialGoodsCode", "MaterialGoodsName", "Description" };
            strColumnCaption = strColumnToolTip = new List<string>() { "Mã thành phẩm", "Tên thành phẩm", "Ghi chú" };
            bolIsReadOnly = new List<bool>();
            bolIsVisible = new List<bool>();
            bolIsVisibleCbb = new List<bool>();
            intColumnWidth = new List<int>();
            intColumnMaxWidth = new List<int>();
            intColumnMinWidth = new List<int>();
            intVisiblePosition = new List<int>();
            VTbolIsVisible = new List<int>() { 0, 1, 2 };    //vị trí có giá trị bằng true
            VTbolIsVisibleCbb = new List<int>() { 1, 2 };
            for (int i = 0; i < 3; i++)
            {
                bolIsVisible.Add(VTbolIsVisible.Contains(i));
                bolIsVisibleCbb.Add(VTbolIsVisibleCbb.Contains(i));
                bolIsReadOnly.Add(false);
                intColumnWidth.Add(-1);
                intColumnMaxWidth.Add(-1);
                intColumnMinWidth.Add(-1);
                intVisiblePosition.Add(-1);
            }
            dstemplatecolums = ConstDatabase.CreateData(strColumnName, strColumnCaption, strColumnToolTip, bolIsReadOnly, bolIsVisible, bolIsVisibleCbb, intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition).Values.ToList();

            Utils.ConfigGrid(utralGrid, string.Empty, dstemplatecolums, 0);
        }

        private void ConfigGridDong(Template mauGiaoDien)
        {
            #region Grid động
            uGrid1.DisplayLayout.Override.AllowAddNew = AllowAddNew.TemplateOnBottom;
            uGrid1.DisplayLayout.Override.TemplateAddRowPrompt = resSystem.MSG_System_07;
            uGrid1.DisplayLayout.Override.SpecialRowSeparator = SpecialRowSeparator.None;


            //hiển thị 1 band
            uGrid1.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            //tắt lọc cột
            uGrid1.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.False;
            uGrid1.DisplayLayout.Override.FilterUIType = FilterUIType.Default;
            //tự thay đổi kích thước cột
            uGrid1.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            //tắt tiêu đề
            uGrid1.DisplayLayout.CaptionVisible = DefaultableBoolean.False;
            //select cả hàng hay ko?
            uGrid1.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid1.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            //Hiển thị SupportDataErrorInfo
            uGrid1.DisplayLayout.Override.SupportDataErrorInfo = SupportDataErrorInfo.CellsOnly;

            uGrid1.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            uGrid1.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.ExtendFirstCell;

            uGrid1.DisplayLayout.UseFixedHeaders = true;

            uGrid1.DisplayLayout.Override.AllowMultiCellOperations = Infragistics.Win.UltraWinGrid.AllowMultiCellOperation.All;

            Infragistics.Win.UltraWinGrid.UltraGridBand band = uGrid1.DisplayLayout.Bands[0];

            //Infragistics.Win.UltraWinGrid.UltraGridBand band = uGrid2.DisplayLayout.Bands[0];
            //band.Summaries.Clear();
            ////if (mauGiaoDien != null)
            ////    Utils.ConfigGrid(uGrid2, mauGiaoDien.TemplateDetails.Single(k => k.TabIndex == 100).TemplateColumns, 1);

            ////bỏ dòng tổng số
            //uGrid2.DisplayLayout.Bands[0].Summaries.Clear();
            //uGrid2.DisplayLayout.Bands[0].Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.Default;
            //uGrid2.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            //uGrid2.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.ExtendFirstCell;

            //uGrid2.DisplayLayout.UseFixedHeaders = true;

            foreach (var item in band.Columns)
            {
                if (item.Key.Equals("MaterialGoodsCode"))
                {
                    //List<MaterialGoods> cbb = (from a in _IMaterialGoodsService.Query where a.MaterialGoodsType == 1 select a).ToList();
                    List<MaterialGoods> cbb = Utils.ListMaterialGoods.Where(x => x.MaterialGoodsType == 1 || x.MaterialGoodsType == 3).ToList();
                    //item.Editor = this.CreateCombobox(cbb, ConstDatabase.MaterialGoods_TableName, "MaterialGoodsCode", "MaterialGoodsCode");
                    //item.Editor = GetEmbeddableEditorBase(Controls, cbb, "ID", "MaterialGoodsCode", ConstDatabase.MaterialGoods_TableName);
                    item.Editor = GetEmbeddableEditorBase(Controls, cbb, "MaterialGoodsCode", "MaterialGoodsCode", ConstDatabase.MaterialGoods_TableName);
                }
            }
            #endregion
        }
        //Set Giá trị Combobox trong UltraGrid
        private static EmbeddableEditorBase GetEmbeddableEditorBase<T>(Control.ControlCollection _this, List<T> inputItem, string valueMember, string displayMember, string nameTable)
        {

            Infragistics.Win.UltraWinGrid.UltraCombo cbb = new Infragistics.Win.UltraWinGrid.UltraCombo
            {
                Name = "dropDown1",
                Visible = false,
                DataSource = inputItem,
                ValueMember = valueMember,
                DisplayMember = displayMember
            };
            _this.Add(cbb);
            Utils.ConfigGrid(cbb, nameTable);
            EmbeddableEditorBase editor = new EditorWithCombo(new DefaultEditorOwner(new DefaultEditorOwnerSettings { ValueList = cbb }));
            return editor;
        }
        public Template mauGiaoDien { get; set; }
        #endregion
        //btnSave
        #region Event
        private void uGrid1_AfterCellUpdate(object sender, CellEventArgs e)
        {
            if (e.Cell.Column.Key.Equals("MaterialGoodsCode"))
            {
                List<Giathanh> lstuGrid1s = ((BindingList<Giathanh>)uGrid1.DataSource).ToList();
                UltraGridCell cell = uGrid1.ActiveCell;
                UltraCombo test = (UltraCombo)Controls["dropDown1"];
                MaterialGoods select = (MaterialGoods)(test.SelectedRow == null ? test.SelectedRow : test.SelectedRow.ListObject);
                if (select != null)
                {
                    int count = lstuGrid1s.Count(x => x.MaterialGoodsCode == select.MaterialGoodsCode);
                    bool T = true;
                    if (count > 1)
                    {
                        MSG.Error("Đối tượng tính giá thành đã được chọn vui lòng chọn lại");
                        Utils.NotificationCell(uGrid1, cell, "Đối tượng tính giá thành đã được chọn vui lòng chọn lại");
                        if (lsRowHasNotificationCell.Count() > 0)
                        {
                            foreach (UltraGridRow row in lsRowHasNotificationCell)
                            {
                                if (row.Equals(cell.Row)) T = false;
                            }
                            if (T) lsRowHasNotificationCell.Add(cell.Row);
                        }
                        else
                        {
                            lsRowHasNotificationCell.Add(cell.Row);
                        }
                        e.Cell.Row.Cells["Description"].Value = select.ItemSource;
                        e.Cell.Row.Cells["MaterialGoodsName"].Value = select.MaterialGoodsName;
                        return;
                    }
                    else
                    {
                        foreach (UltraGridRow row in lsRowHasNotificationCell)
                        {
                            if (row.Equals(cell.Row))
                            {
                                T = false;
                                indexRow = row;
                            }
                        }
                        if (!T) lsRowHasNotificationCell.Remove(indexRow);
                        e.Cell.Row.Cells["Description"].Value = select.ItemSource;
                        e.Cell.Row.Cells["MaterialGoodsName"].Value = select.MaterialGoodsName;
                        Utils.RemoveNotificationCell(uGrid1, cell);
                    }
                }
                else
                {
                    Utils.NotificationCell(uGrid1, cell, "Dữ liệu không có trong danh mục", true);
                }
            }
        }

        private void uGrid1_Error(object sender, Infragistics.Win.UltraWinGrid.ErrorEventArgs e)
        {
            UltraGridCell cell = uGrid1.ActiveCell;
            if (cell.Column.Key.Equals("MaterialGoodsCode"))
            {
                int count = _IMaterialGoodsService.GetAll_ByMaterialGoodsType(3).Count(x => x.MaterialGoodsCode == cell.Text);

                if (count == 0)
                {
                    MSG.Error("Dữ liệu không có trong danh mục");
                }
            }
            e.Cancel = true;
        }
        private void UGrid1RightCel(object sender, EventArgs e)
        {

        }
        private void tsmDelete_Click(object sender, EventArgs e)
        {
            bool T = true;
            UltraGridCell cell = uGrid1.ActiveCell;
            foreach (UltraGridRow row in lsRowHasNotificationCell)
            {
                if (row.Equals(cell.Row))
                {
                    T = false;
                    indexRow = row;
                }
            }
            if (!T) lsRowHasNotificationCell.Remove(indexRow);
            if (uGrid1.ActiveCell != null)
            {
                UltraGridRow row = null;
                if (uGrid1.ActiveCell != null || uGrid1.ActiveRow != null)
                {
                    row = uGrid1.ActiveCell != null ? uGrid1.ActiveCell.Row : uGrid1.ActiveRow;
                }
                else
                {
                    if (uGrid1.Rows.Count > 0) row = uGrid1.Rows[uGrid1.Rows.Count - 1];
                }
                if (row != null) row.Delete(false);
                uGrid1.Update();
            }
        }

        private void tsmAdd_Click(object sender, EventArgs e)
        {
            uGrid1.AddNewRow4Grid();
        }
        private void uGrid1_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {

            UltraGridCell cell = uGrid1.ActiveCell;
            indexRow = cell.Row;
            bool T = true;
            if (cell.Column.Key.Equals("MaterialGoodsCode"))
            {
                List<Giathanh> lstuGrid1s = ((BindingList<Giathanh>)uGrid1.DataSource).ToList();
                int count = lstuGrid1s.Count(x => x.MaterialGoodsCode == cell.Text);
                if (count == 1)
                {
                    foreach (UltraGridRow row in lsRowHasNotificationCell)
                    {
                        if (row.Equals(cell.Row))
                        {
                            T = false;
                            indexRow = row;
                        }
                    }
                    if (!T) lsRowHasNotificationCell.Remove(indexRow);
                    Utils.RemoveNotificationCell(uGrid1, cell);
                }
            }
            //if (uGrid1.ActiveCell == null) return;
            //if (cell.Column.Key.Equals("MaterialGoodsCode"))
            //{
            //    bool T = true;
            //    int count = _IMaterialGoodsService.GetAll_ByMaterialGoodsType(3).Count(x => x.MaterialGoodsCode == cell.Text);
            //    List < Giathanh > lstuGrid1s = ((BindingList<Giathanh>)uGrid1.DataSource).ToList();
            //    int count_ = lstuGrid1s.Count(x => x.MaterialGoodsCode == cell.Text);
            //    if (count == 1)
            //    {
            //        foreach (UltraGridRow row in lsRowHasNotificationCell)
            //        {
            //            if (row.Equals(cell.Row))
            //            {
            //                T = false;
            //                indexRow = row;
            //            }
            //        }
            //        if (!T) lsRowHasNotificationCell.Remove(indexRow);
            //    }
            //    else if (err== null && count_>0)
            //    {
            //        MSG.Error("Đối tượng tính giá thành đã được chọn vui lòng chọn lại");
            //        Utils.NotificationCell(uGrid1, cell, "Đối tượng tính giá thành đã được chọn vui lòng chọn lại");
            //        if (lsRowHasNotificationCell.Count() > 0)
            //        {
            //            foreach (UltraGridRow row in lsRowHasNotificationCell)
            //            {
            //                if (row.Equals(cell.Row)) T = false;
            //            }
            //            if (T) lsRowHasNotificationCell.Add(cell.Row);
            //        }
            //        else
            //        {
            //            lsRowHasNotificationCell.Add(cell.Row);
            //        }
            //        return;
            //    }
            //}
        }

        private void cbbCostSetType_ValueChanged(object sender, EventArgs e)
        {
            #region Code cũ
            //if (cbbCostSetType.SelectedIndex == 2)
            //{
            //    this.Size = new Size(509, 447);
            //    ultraGroupBox2.Show();
            //    ultraGroupBox2.Location = new Point(3, 177);
            //    chkIsActive.Location = new Point(12, 375);
            //    btnSave.Location = new Point(320, 370);
            //    btnClose.Location = new Point(405, 370);
            //    lblTenThanhPham.Visible = false;
            //    lblThanhPham.Visible = false;
            //    txtThanhPham.Visible = false;
            //    cbbThanhPham.Visible = false;
            //    ultraLabel2.Visible = true;
            //    ultraLabel4.Visible = true;
            //    txtCostSetCode.Visible = true;
            //    txtCostSetName.Visible = true;
            //}
            //else if (cbbCostSetType.SelectedIndex == 4)
            //{
            //    this.Size = new Size(509, 264);
            //    ultraGroupBox2.Hide();
            //    chkIsActive.Location = new Point(12, 187);
            //    btnSave.Location = new Point(320, 182);
            //    btnClose.Location = new Point(405, 182);
            //    lblTenThanhPham.Visible = true;
            //    lblThanhPham.Visible = true;
            //    txtThanhPham.Visible = true;
            //    cbbThanhPham.Visible = true;
            //    ultraLabel2.Visible = false;
            //    ultraLabel4.Visible = false;
            //    txtCostSetCode.Visible = false;
            //    txtCostSetName.Visible = false;
            //}
            //else
            //{
            //    this.Size = new Size(509, 264);
            //    ultraGroupBox2.Hide();
            //    chkIsActive.Location = new Point(12, 187);
            //    btnSave.Location = new Point(320, 182);
            //    btnClose.Location = new Point(405, 182);
            //    lblTenThanhPham.Visible = false;
            //    lblThanhPham.Visible = false;
            //    txtThanhPham.Visible = false;
            //    cbbThanhPham.Visible = false;
            //    ultraLabel2.Visible = true;
            //    ultraLabel4.Visible = true;
            //    txtCostSetCode.Visible = true;
            //    txtCostSetName.Visible = true;
            //}
            #endregion
            #region Code mới _Hau Sửa
            if (cbbCostSetType.SelectedIndex == 2)
            {
                ultraGroupBox2.Show();
                ultraGroupBox2.Location = new Point(ultraGroupBox1.Location.X, ultraGroupBox1.Location.Y + ultraGroupBox1.Height + 6);
                ultraGroupBox3.Location = new Point(ultraGroupBox2.Location.X, ultraGroupBox2.Location.Y + ultraGroupBox2.Height + 6);
                this.Size = new Size(19 + ultraPanel1.Width, ultraPanel1.Height + ultraGroupBox2.Height + ultraGroupBox3.Height + 70);
                lblTenThanhPham.Visible = false;
                lblThanhPham.Visible = false;
                txtThanhPham.Visible = false;
                cbbThanhPham.Visible = false;
                ultraLabel2.Visible = true;
                ultraLabel4.Visible = true;
                txtCostSetCode.Visible = true;
                txtCostSetName.Visible = true;
            }
            else if (cbbCostSetType.SelectedIndex == 4)
            {
                ultraGroupBox2.Hide();
                ultraGroupBox3.Location = new Point(ultraGroupBox1.Location.X, ultraGroupBox1.Location.Y + ultraGroupBox1.Height + 6);
                this.Size = new Size(19 + ultraPanel1.Width, ultraPanel1.Height + ultraGroupBox3.Height + 70);
                lblTenThanhPham.Visible = true;
                lblThanhPham.Visible = true;
                txtThanhPham.Visible = true;
                cbbThanhPham.Visible = true;
                ultraLabel2.Visible = false;
                ultraLabel4.Visible = false;
                txtCostSetCode.Visible = false;
                txtCostSetName.Visible = false;
            }
            else
            {
                ultraGroupBox2.Hide();
                ultraGroupBox3.Location = new Point(ultraGroupBox1.Location.X, ultraGroupBox1.Location.Y + ultraGroupBox1.Height + 6);
                this.Size = new Size(19 + ultraPanel1.Width, ultraPanel1.Height + ultraGroupBox3.Height + 70);
                lblTenThanhPham.Visible = false;
                lblThanhPham.Visible = false;
                txtThanhPham.Visible = false;
                cbbThanhPham.Visible = false;
                ultraLabel2.Visible = true;
                ultraLabel4.Visible = true;
                txtCostSetCode.Visible = true;
                txtCostSetName.Visible = true;
            }
            #endregion
        }
        private void cbbThanhPham_EditorButtonClick_1(object sender, EditorButtonEventArgs e)
        {
            new FMaterialGoodsDetail().ShowDialog(this);

            Utils.ClearCacheByType<MaterialGoods>();
            Utils.ClearCacheByType<MaterialGoodsCustom>();
            var lst = Utils.ListMaterialGoods.Where(x => x.MaterialGoodsType == 3).ToList();
            if (lst.Count > 0)
            {
                for (int i = 0; i < lst.Count; i++)
                    if (Utils.ListCostSet.Where(x => x.CostSetType == 4 && x.ID != _Select.ID).ToList().Any(x => x.CostSetCode == lst[i].MaterialGoodsCode))
                    {
                        lst.RemoveAt(i);
                        i--;
                    }
            }
            this.ConfigCombo(lst, cbbThanhPham, "MaterialGoodsCode", "MaterialGoodsCode");
            cbbThanhPham.SelectedText = FMaterialGoodsDetail.MaterialGoodsCode;
        }
        private void cbbThanhPham_RowSelected(object sender, RowSelectedEventArgs e)
        {
            var model = cbbThanhPham.SelectedRow.ListObject as MaterialGoods;
            if (model != null) txtThanhPham.Text = model.MaterialGoodsName;
        }

        #endregion

        private void FCostSetDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
