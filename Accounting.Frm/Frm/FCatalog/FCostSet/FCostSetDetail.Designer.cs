﻿namespace Accounting
{
    partial class FCostSetDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbThanhPham = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.lblTenThanhPham = new Infragistics.Win.Misc.UltraLabel();
            this.txtThanhPham = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblThanhPham = new Infragistics.Win.Misc.UltraLabel();
            this.cbbCostSetType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.txtCostSetName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.txtDescription = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.txtCostSetCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.chkIsActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.contextMenuStrip1.SuspendLayout();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbThanhPham)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThanhPham)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCostSetType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCostSetName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCostSetCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmAdd,
            this.tsmDelete});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(205, 48);
            // 
            // tsmAdd
            // 
            this.tsmAdd.Name = "tsmAdd";
            this.tsmAdd.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.tsmAdd.Size = new System.Drawing.Size(204, 22);
            this.tsmAdd.Text = "Thêm một dòng";
            this.tsmAdd.Click += new System.EventHandler(this.tsmAdd_Click);
            // 
            // tsmDelete
            // 
            this.tsmDelete.Name = "tsmDelete";
            this.tsmDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.tsmDelete.Size = new System.Drawing.Size(204, 22);
            this.tsmDelete.Text = "Xóa một dòng";
            this.tsmDelete.Click += new System.EventHandler(this.tsmDelete_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance1;
            this.btnClose.Location = new System.Drawing.Point(399, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 14;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click_1);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance2;
            this.btnSave.Location = new System.Drawing.Point(318, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 13;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click_1);
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraGroupBox1);
            this.ultraPanel1.Location = new System.Drawing.Point(3, 2);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(490, 174);
            this.ultraPanel1.TabIndex = 2;
            // 
            // ultraGroupBox1
            // 
            appearance3.TextVAlignAsString = "Middle";
            this.ultraGroupBox1.Appearance = appearance3;
            this.ultraGroupBox1.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox1.Controls.Add(this.cbbThanhPham);
            this.ultraGroupBox1.Controls.Add(this.lblTenThanhPham);
            this.ultraGroupBox1.Controls.Add(this.txtThanhPham);
            this.ultraGroupBox1.Controls.Add(this.lblThanhPham);
            this.ultraGroupBox1.Controls.Add(this.cbbCostSetType);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel3);
            this.ultraGroupBox1.Controls.Add(this.txtCostSetName);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel5);
            this.ultraGroupBox1.Controls.Add(this.txtDescription);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel4);
            this.ultraGroupBox1.Controls.Add(this.txtCostSetCode);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel2);
            appearance12.FontData.BoldAsString = "True";
            this.ultraGroupBox1.HeaderAppearance = appearance12;
            this.ultraGroupBox1.Location = new System.Drawing.Point(3, 3);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(483, 168);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbThanhPham
            // 
            this.cbbThanhPham.AllowNull = Infragistics.Win.DefaultableBoolean.True;
            this.cbbThanhPham.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.SuggestAppend;
            this.cbbThanhPham.AutoSize = false;
            appearance4.Image = global::Accounting.Properties.Resources.plus;
            editorButton1.Appearance = appearance4;
            appearance5.Image = global::Accounting.Properties.Resources.plus_press;
            editorButton1.PressedAppearance = appearance5;
            this.cbbThanhPham.ButtonsRight.Add(editorButton1);
            this.cbbThanhPham.DisplayLayout.DefaultSelectedForeColor = System.Drawing.Color.Black;
            this.cbbThanhPham.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbThanhPham.LimitToList = true;
            this.cbbThanhPham.Location = new System.Drawing.Point(124, 46);
            this.cbbThanhPham.Name = "cbbThanhPham";
            this.cbbThanhPham.Size = new System.Drawing.Size(347, 22);
            this.cbbThanhPham.TabIndex = 205;
            this.cbbThanhPham.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbThanhPham_RowSelected);
            this.cbbThanhPham.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.cbbThanhPham_EditorButtonClick_1);
            // 
            // lblTenThanhPham
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextHAlignAsString = "Left";
            appearance6.TextVAlignAsString = "Middle";
            this.lblTenThanhPham.Appearance = appearance6;
            this.lblTenThanhPham.Location = new System.Drawing.Point(6, 73);
            this.lblTenThanhPham.Name = "lblTenThanhPham";
            this.lblTenThanhPham.Size = new System.Drawing.Size(111, 22);
            this.lblTenThanhPham.TabIndex = 24;
            this.lblTenThanhPham.Text = "Tên sản phẩm (*)";
            // 
            // txtThanhPham
            // 
            this.txtThanhPham.AutoSize = false;
            this.txtThanhPham.Location = new System.Drawing.Point(124, 73);
            this.txtThanhPham.Name = "txtThanhPham";
            this.txtThanhPham.Size = new System.Drawing.Size(348, 22);
            this.txtThanhPham.TabIndex = 22;
            // 
            // lblThanhPham
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.TextHAlignAsString = "Left";
            appearance7.TextVAlignAsString = "Middle";
            this.lblThanhPham.Appearance = appearance7;
            this.lblThanhPham.Location = new System.Drawing.Point(6, 47);
            this.lblThanhPham.Name = "lblThanhPham";
            this.lblThanhPham.Size = new System.Drawing.Size(111, 22);
            this.lblThanhPham.TabIndex = 23;
            this.lblThanhPham.Text = "Mã sản phẩm (*)";
            // 
            // cbbCostSetType
            // 
            this.cbbCostSetType.AutoSize = false;
            this.cbbCostSetType.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbCostSetType.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbbCostSetType.Location = new System.Drawing.Point(124, 21);
            this.cbbCostSetType.Name = "cbbCostSetType";
            this.cbbCostSetType.NullText = "<Chọn loại đối tượng>";
            this.cbbCostSetType.Size = new System.Drawing.Size(348, 22);
            this.cbbCostSetType.TabIndex = 20;
            this.cbbCostSetType.ValueChanged += new System.EventHandler(this.cbbCostSetType_ValueChanged);
            // 
            // ultraLabel3
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.TextHAlignAsString = "Left";
            appearance8.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance8;
            this.ultraLabel3.Location = new System.Drawing.Point(7, 19);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(111, 22);
            this.ultraLabel3.TabIndex = 21;
            this.ultraLabel3.Text = "Loại (*)";
            // 
            // txtCostSetName
            // 
            this.txtCostSetName.AutoSize = false;
            this.txtCostSetName.Location = new System.Drawing.Point(123, 73);
            this.txtCostSetName.Name = "txtCostSetName";
            this.txtCostSetName.Size = new System.Drawing.Size(348, 22);
            this.txtCostSetName.TabIndex = 2;
            // 
            // ultraLabel5
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.TextHAlignAsString = "Left";
            appearance9.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance9;
            this.ultraLabel5.Location = new System.Drawing.Point(6, 100);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(111, 22);
            this.ultraLabel5.TabIndex = 9;
            this.ultraLabel5.Text = "Diễn giải";
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(123, 100);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(348, 59);
            this.txtDescription.TabIndex = 9;
            // 
            // ultraLabel4
            // 
            appearance10.BackColor = System.Drawing.Color.Transparent;
            appearance10.TextHAlignAsString = "Left";
            appearance10.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance10;
            this.ultraLabel4.Location = new System.Drawing.Point(6, 73);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(111, 22);
            this.ultraLabel4.TabIndex = 7;
            this.ultraLabel4.Text = "Tên đối tượng (*)";
            // 
            // txtCostSetCode
            // 
            this.txtCostSetCode.AutoSize = false;
            this.txtCostSetCode.Location = new System.Drawing.Point(123, 46);
            this.txtCostSetCode.Name = "txtCostSetCode";
            this.txtCostSetCode.Size = new System.Drawing.Size(348, 22);
            this.txtCostSetCode.TabIndex = 1;
            // 
            // ultraLabel2
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            appearance11.TextHAlignAsString = "Left";
            appearance11.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance11;
            this.ultraLabel2.Location = new System.Drawing.Point(6, 46);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(111, 22);
            this.ultraLabel2.TabIndex = 2;
            this.ultraLabel2.Text = "Mã ĐT-THCP (*)";
            // 
            // chkIsActive
            // 
            this.chkIsActive.Checked = true;
            this.chkIsActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIsActive.Location = new System.Drawing.Point(10, 8);
            this.chkIsActive.Name = "chkIsActive";
            this.chkIsActive.Size = new System.Drawing.Size(111, 20);
            this.chkIsActive.TabIndex = 2;
            this.chkIsActive.Text = "Hoạt động";
            // 
            // uGrid1
            // 
            this.uGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.uGrid1.ContextMenuStrip = this.contextMenuStrip1;
            this.uGrid1.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid1.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid1.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.AlignWithDataRows;
            this.uGrid1.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.uGrid1.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGrid1.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid1.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid1.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid1.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGrid1.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGrid1.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.uGrid1.Location = new System.Drawing.Point(6, 19);
            this.uGrid1.Name = "uGrid1";
            this.uGrid1.Size = new System.Drawing.Size(474, 158);
            this.uGrid1.TabIndex = 2;
            this.uGrid1.Text = "uGrid";
            this.uGrid1.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid1_AfterCellUpdate);
            this.uGrid1.BeforeExitEditMode += new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventHandler(this.uGrid1_BeforeExitEditMode);
            this.uGrid1.Error += new Infragistics.Win.UltraWinGrid.ErrorEventHandler(this.uGrid1_Error);
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.uGrid1);
            this.ultraGroupBox2.Location = new System.Drawing.Point(3, 177);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(486, 183);
            this.ultraGroupBox2.TabIndex = 72;
            this.ultraGroupBox2.Text = "Đối tượng tính giá thành";
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Controls.Add(this.chkIsActive);
            this.ultraGroupBox3.Controls.Add(this.btnSave);
            this.ultraGroupBox3.Controls.Add(this.btnClose);
            this.ultraGroupBox3.Location = new System.Drawing.Point(3, 366);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(486, 34);
            this.ultraGroupBox3.TabIndex = 73;
            // 
            // FCostSetDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(493, 408);
            this.Controls.Add(this.ultraGroupBox3);
            this.Controls.Add(this.ultraGroupBox2);
            this.Controls.Add(this.ultraPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FCostSetDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thông tin đối tượng tập hợp chi phí";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FCostSetDetail_FormClosed);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbThanhPham)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThanhPham)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCostSetType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCostSetName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCostSetCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCostSetName;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDescription;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCostSetCode;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsActive;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripMenuItem tsmDelete;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbCostSetType;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.Misc.UltraLabel lblTenThanhPham;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtThanhPham;
        private Infragistics.Win.Misc.UltraLabel lblThanhPham;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbThanhPham;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
    }
}