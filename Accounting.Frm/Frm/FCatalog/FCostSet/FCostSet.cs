﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinTree;

namespace Accounting
{
    public partial class FCostSet : CatalogBase
    {
        #region khai báo
        private readonly ICostSetService _ICostSetService;
        private readonly ICostSetMaterialGoodsService _ICostSetMaterialGoodsService;

        List<CostSet> dsCostSet = new List<CostSet>();

        static Dictionary<int, string> DicCostSetType;
        public static Dictionary<int, string> dicCostSetType
        {
            get { DicCostSetType = DicCostSetType ?? (Dictionary<int, string>)BuildConfig(1); return DicCostSetType; }
        }

        #endregion

        public FCostSet()
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _ICostSetService = IoC.Resolve<ICostSetService>();
            _ICostSetMaterialGoodsService = IoC.Resolve<ICostSetMaterialGoodsService>();

            #endregion

            #region Tạo dữ liệu ban đầu
            LoadDuLieu();
            #endregion
        }

        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }

        private void LoadDuLieu(bool configTree)
        {
            WaitingFrm.StartWaiting();
            #region Lấy dữ liệu từ CSDL
            dsCostSet = _ICostSetService.GetAll_OrderBy();
            _ICostSetService.UnbindSession(dsCostSet);
            dsCostSet = _ICostSetService.GetAll_OrderBy();
            #endregion

            #region Xử lý dữ liệu
            foreach (var item in dsCostSet)
            {
                item.CostSetTypeView = dicCostSetType[item.CostSetType];
            }
            #endregion

            #region hiển thị và xử lý hiển thị
            uGridDS.DataSource = dsCostSet;
            ConfigGrid(uGridDS);
            ToolTip ToolTip1 = new ToolTip();
            ToolTip1.SetToolTip(btnAdd, "Thêm (Ctrl + N)");
            ToolTip ToolTip2 = new ToolTip();
            ToolTip2.SetToolTip(btnEdit, "Xem (Ctrl + E)");
            ToolTip ToolTip3 = new ToolTip();
            ToolTip3.SetToolTip(btnDelete, "Xóa (Ctrl + D)");
            if (uGridDS.Rows.Count > 0) uGridDS.Rows[0].Selected = true;
            #endregion
            WaitingFrm.StopWaiting();
        }

        #region Nghiệp vụ
        #region ContentMenu
        private void tsmAdd_Click_1(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void tsmEdit_Click_1(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tsmDelete_Click_1(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void tmsReLoad_Click_1(object sender, EventArgs e)
        {
            ResetFunction();
        }
        protected override void ResetFunction()
        {
            LoadDuLieu(false);
        }

        #endregion

        #region Button
        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void btnDelete_Click_1(object sender, EventArgs e)
        {
            DeleteFunction();
        }
        #endregion

        /// <summary>
        /// Nghiệp vụ Add
        /// </summary>
        protected override void AddFunction()
        {

            new FCostSetDetail().ShowDialog(this);
            if (!FCostSetDetail.isClose) { Utils.ClearCacheByType<CostSet>(); Utils.ClearCacheByType<Objects>(); LoadDuLieu(); }

        }

        /// <summary>
        /// Nghiệp vụ Edit
        /// </summary>
        protected override void EditFunction()
        {
            if (uGridDS.Selected.Rows.Count > 0)
            {
                CostSet temp = uGridDS.Selected.Rows[0].ListObject as CostSet;
                if (Utils.checkRelationVoucher_CostSet(temp))
                {
                    MSG.Error("Không thể sửa Đối tượng tập hợp CP vì có phát sinh chứng từ liên quan");
                    return;
                }
                new FCostSetDetail(temp, true).ShowDialog(this);
                if (!FCostSetDetail.isClose) { Utils.ClearCacheByType<CostSet>(); Utils.ClearCacheByType<Objects>(); LoadDuLieu(); }
            }
            else
                Accounting.TextMessage.MSG.Error(string.Format(Accounting.TextMessage.resSystem.MSG_Catalog3, "một ĐTTHCP"));
        }

        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        protected override void DeleteFunction()
        {
            if (uGridDS.Selected.Rows.Count > 0)
            {
                CostSet temp = uGridDS.Selected.Rows[0].ListObject as CostSet;
                if (Utils.checkRelationVoucher_CostSet(temp))
                {
                    MSG.Error("Không thể xóa Đối tượng tập hợp CP vì có phát sinh chứng từ liên quan");
                    return;
                }
                if (MSG.Question(string.Format(resSystem.MSG_System_05, "ĐTTHCP")) == System.Windows.Forms.DialogResult.Yes)
                {

                    _ICostSetService.BeginTran();
                    _ICostSetService.Delete(temp);
                    _ICostSetService.CommitTran();
                    Utils.ClearCacheByType<CostSet>();
                    Utils.ClearCacheByType<Objects>();
                    LoadDuLieu();
                }
            }
            else
                MSG.Error(string.Format(resSystem.MSG_Catalog2, "một Định mức nguyên vật liệu"));
        }
        #endregion

        #region Event
        private void uGridDS_MouseDown(object sender, MouseEventArgs e)
        {//khi click chuột phải hiện content menu
            Utils.uGrid_MouseDown(sender, e, cms4Grid);
        }

        private void uGridDS_DoubleClick(object sender, EventArgs e)
        {//click đúp chuột để sửa
            EditFunction();
        }
        #endregion

        #region Utils
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, TextMessage.ConstDatabase.CostSet_TableName);

            uGridDS.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridDS.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGridDS.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGridDS.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGridDS.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.AlignWithDataRows;
            this.uGridDS.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.uGridDS.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGridDS.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGridDS.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGridDS.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGridDS.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGridDS.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGridDS.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGridDS.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            uGridDS.DisplayLayout.Bands[0].Columns["Select"].Hidden = true;
        }
        static object BuildConfig(int obj)
        {
            Dictionary<int, string> temp = new Dictionary<int, string>();
            switch (obj)
            {
                case 1:
                    {
                        temp.Add(0, "Đơn hàng");
                        temp.Add(1, "Công trình, vụ việc");
                        temp.Add(2, "Phân xưởng, phòng ban");
                        temp.Add(3, "Quy trình công nghệ sản xuất");
                        temp.Add(4, "Sản phẩm");
                        temp.Add(5, "Khác");
                    }
                    break;

            }
            return temp;
        }
        #endregion

        private void uGridDS_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            if (uGridDS.Selected.Rows.Count > 0)
            {
                CostSet temp = uGridDS.Selected.Rows[0].ListObject as CostSet;
                new FCostSetDetail(temp,true).ShowDialog(this);
                if (!FCostSetDetail.isClose)
                {
                    Utils.ClearCacheByType<CostSet>();
                    Utils.ClearCacheByType<Objects>();
                    LoadDuLieu();
                }
            }
        }

        private void FCostSet_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGridDS.ResetText();
            uGridDS.ResetUpdateMode();
            uGridDS.ResetExitEditModeOnLeave();
            uGridDS.ResetRowUpdateCancelAction();
            uGridDS.DataSource = null;
            uGridDS.Layouts.Clear();
            uGridDS.ResetLayouts();
            uGridDS.ResetDisplayLayout();
            uGridDS.Refresh();
            uGridDS.ClearUndoHistory();
            uGridDS.ClearXsdConstraints();
        }

        private void FCostSet_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
