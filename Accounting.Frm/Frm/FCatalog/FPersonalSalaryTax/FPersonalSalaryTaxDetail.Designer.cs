﻿namespace Accounting
{
    partial class FPersonalSalaryTaxDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtPersonalSalaryTaxGrade = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtToAmount = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtFromAmount = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtTaxRate = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.lblToAmount = new Infragistics.Win.Misc.UltraLabel();
            this.lblTaxRate = new Infragistics.Win.Misc.UltraLabel();
            this.lblFromAmount = new Infragistics.Win.Misc.UltraLabel();
            this.cbbSalaryType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.lblSalaryType = new Infragistics.Win.Misc.UltraLabel();
            this.chkChiTiet = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.cbbChiTiet = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.txtPersonalSalaryTaxName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblPersonalSalaryTaxGrade = new Infragistics.Win.Misc.UltraLabel();
            this.lblPersonalSalaryTaxName = new Infragistics.Win.Misc.UltraLabel();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbSalaryType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkChiTiet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbChiTiet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPersonalSalaryTaxName)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            appearance1.FontData.BoldAsString = "True";
            this.ultraGroupBox1.Appearance = appearance1;
            this.ultraGroupBox1.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox1.Controls.Add(this.txtPersonalSalaryTaxGrade);
            this.ultraGroupBox1.Controls.Add(this.txtToAmount);
            this.ultraGroupBox1.Controls.Add(this.txtFromAmount);
            this.ultraGroupBox1.Controls.Add(this.txtTaxRate);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel4);
            this.ultraGroupBox1.Controls.Add(this.lblToAmount);
            this.ultraGroupBox1.Controls.Add(this.lblTaxRate);
            this.ultraGroupBox1.Controls.Add(this.lblFromAmount);
            this.ultraGroupBox1.Controls.Add(this.cbbSalaryType);
            this.ultraGroupBox1.Controls.Add(this.lblSalaryType);
            this.ultraGroupBox1.Controls.Add(this.chkChiTiet);
            this.ultraGroupBox1.Controls.Add(this.cbbChiTiet);
            this.ultraGroupBox1.Controls.Add(this.txtPersonalSalaryTaxName);
            this.ultraGroupBox1.Controls.Add(this.lblPersonalSalaryTaxGrade);
            this.ultraGroupBox1.Controls.Add(this.lblPersonalSalaryTaxName);
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(1, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(414, 171);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtPersonalSalaryTaxGrade
            // 
            this.txtPersonalSalaryTaxGrade.AutoSize = false;
            this.txtPersonalSalaryTaxGrade.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtPersonalSalaryTaxGrade.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.txtPersonalSalaryTaxGrade.InputMask = "nnnnnnnnn";
            this.txtPersonalSalaryTaxGrade.Location = new System.Drawing.Point(347, 31);
            this.txtPersonalSalaryTaxGrade.Name = "txtPersonalSalaryTaxGrade";
            this.txtPersonalSalaryTaxGrade.PromptChar = ' ';
            this.txtPersonalSalaryTaxGrade.Size = new System.Drawing.Size(63, 22);
            this.txtPersonalSalaryTaxGrade.TabIndex = 2;
            // 
            // txtToAmount
            // 
            appearance2.TextHAlignAsString = "Right";
            this.txtToAmount.Appearance = appearance2;
            this.txtToAmount.AutoSize = false;
            this.txtToAmount.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtToAmount.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.txtToAmount.Location = new System.Drawing.Point(134, 111);
            this.txtToAmount.Name = "txtToAmount";
            this.txtToAmount.PromptChar = ' ';
            this.txtToAmount.Size = new System.Drawing.Size(134, 22);
            this.txtToAmount.TabIndex = 5;
            // 
            // txtFromAmount
            // 
            appearance3.TextHAlignAsString = "Right";
            this.txtFromAmount.Appearance = appearance3;
            this.txtFromAmount.AutoSize = false;
            this.txtFromAmount.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtFromAmount.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.txtFromAmount.Location = new System.Drawing.Point(134, 84);
            this.txtFromAmount.Name = "txtFromAmount";
            this.txtFromAmount.PromptChar = ' ';
            this.txtFromAmount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtFromAmount.Size = new System.Drawing.Size(134, 22);
            this.txtFromAmount.TabIndex = 4;
            // 
            // txtTaxRate
            // 
            appearance4.TextHAlignAsString = "Right";
            this.txtTaxRate.Appearance = appearance4;
            this.txtTaxRate.AutoSize = false;
            this.txtTaxRate.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtTaxRate.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.txtTaxRate.Location = new System.Drawing.Point(134, 138);
            this.txtTaxRate.Name = "txtTaxRate";
            this.txtTaxRate.PromptChar = ' ';
            this.txtTaxRate.Size = new System.Drawing.Size(134, 22);
            this.txtTaxRate.TabIndex = 6;
            // 
            // ultraLabel4
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance5;
            this.ultraLabel4.AutoSize = true;
            this.ultraLabel4.Location = new System.Drawing.Point(273, 138);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(22, 14);
            this.ultraLabel4.TabIndex = 0;
            this.ultraLabel4.Text = "(%)";
            // 
            // lblToAmount
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextVAlignAsString = "Middle";
            this.lblToAmount.Appearance = appearance6;
            this.lblToAmount.AutoSize = true;
            this.lblToAmount.Location = new System.Drawing.Point(10, 111);
            this.lblToAmount.Name = "lblToAmount";
            this.lblToAmount.Size = new System.Drawing.Size(89, 14);
            this.lblToAmount.TabIndex = 20;
            this.lblToAmount.Text = "Thu nhập đến (*)";
            // 
            // lblTaxRate
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.TextVAlignAsString = "Middle";
            this.lblTaxRate.Appearance = appearance7;
            this.lblTaxRate.AutoSize = true;
            this.lblTaxRate.Location = new System.Drawing.Point(10, 138);
            this.lblTaxRate.Name = "lblTaxRate";
            this.lblTaxRate.Size = new System.Drawing.Size(69, 14);
            this.lblTaxRate.TabIndex = 16;
            this.lblTaxRate.Text = "Thuế suất (*)";
            // 
            // lblFromAmount
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.TextVAlignAsString = "Middle";
            this.lblFromAmount.Appearance = appearance8;
            this.lblFromAmount.AutoSize = true;
            this.lblFromAmount.Location = new System.Drawing.Point(10, 84);
            this.lblFromAmount.Name = "lblFromAmount";
            this.lblFromAmount.Size = new System.Drawing.Size(79, 14);
            this.lblFromAmount.TabIndex = 18;
            this.lblFromAmount.Text = "Thu nhập từ (*)";
            // 
            // cbbSalaryType
            // 
            this.cbbSalaryType.AutoSize = false;
            this.cbbSalaryType.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbSalaryType.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbbSalaryType.Location = new System.Drawing.Point(134, 31);
            this.cbbSalaryType.Name = "cbbSalaryType";
            this.cbbSalaryType.Size = new System.Drawing.Size(134, 22);
            this.cbbSalaryType.TabIndex = 1;
            // 
            // lblSalaryType
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.TextVAlignAsString = "Middle";
            this.lblSalaryType.Appearance = appearance9;
            this.lblSalaryType.AutoSize = true;
            this.lblSalaryType.Location = new System.Drawing.Point(10, 31);
            this.lblSalaryType.Name = "lblSalaryType";
            this.lblSalaryType.Size = new System.Drawing.Size(88, 14);
            this.lblSalaryType.TabIndex = 14;
            this.lblSalaryType.Text = "Loại thu nhập (*)";
            // 
            // chkChiTiet
            // 
            this.chkChiTiet.BackColor = System.Drawing.Color.Transparent;
            this.chkChiTiet.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkChiTiet.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkChiTiet.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkChiTiet.Location = new System.Drawing.Point(9, 275);
            this.chkChiTiet.Name = "chkChiTiet";
            this.chkChiTiet.Size = new System.Drawing.Size(101, 20);
            this.chkChiTiet.TabIndex = 11;
            this.chkChiTiet.Text = "Chi tiết theo";
            // 
            // cbbChiTiet
            // 
            this.cbbChiTiet.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbChiTiet.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbbChiTiet.Location = new System.Drawing.Point(129, 275);
            this.cbbChiTiet.Name = "cbbChiTiet";
            this.cbbChiTiet.Size = new System.Drawing.Size(245, 21);
            this.cbbChiTiet.TabIndex = 10;
            // 
            // txtPersonalSalaryTaxName
            // 
            this.txtPersonalSalaryTaxName.AutoSize = false;
            this.txtPersonalSalaryTaxName.Location = new System.Drawing.Point(134, 58);
            this.txtPersonalSalaryTaxName.Name = "txtPersonalSalaryTaxName";
            this.txtPersonalSalaryTaxName.Size = new System.Drawing.Size(276, 21);
            this.txtPersonalSalaryTaxName.TabIndex = 3;
            // 
            // lblPersonalSalaryTaxGrade
            // 
            appearance10.BackColor = System.Drawing.Color.Transparent;
            appearance10.TextVAlignAsString = "Middle";
            this.lblPersonalSalaryTaxGrade.Appearance = appearance10;
            this.lblPersonalSalaryTaxGrade.AutoSize = true;
            this.lblPersonalSalaryTaxGrade.Location = new System.Drawing.Point(274, 31);
            this.lblPersonalSalaryTaxGrade.Name = "lblPersonalSalaryTaxGrade";
            this.lblPersonalSalaryTaxGrade.Size = new System.Drawing.Size(49, 14);
            this.lblPersonalSalaryTaxGrade.TabIndex = 1;
            this.lblPersonalSalaryTaxGrade.Text = "Bậc thuế";
            // 
            // lblPersonalSalaryTaxName
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            appearance11.TextVAlignAsString = "Middle";
            this.lblPersonalSalaryTaxName.Appearance = appearance11;
            this.lblPersonalSalaryTaxName.AutoSize = true;
            this.lblPersonalSalaryTaxName.Location = new System.Drawing.Point(10, 57);
            this.lblPersonalSalaryTaxName.Name = "lblPersonalSalaryTaxName";
            this.lblPersonalSalaryTaxName.Size = new System.Drawing.Size(86, 14);
            this.lblPersonalSalaryTaxName.TabIndex = 0;
            this.lblPersonalSalaryTaxName.Text = "Tên thu nhập (*)";
            // 
            // btnSave
            // 
            appearance12.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance12;
            this.btnSave.Location = new System.Drawing.Point(240, 177);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(84, 30);
            this.btnSave.TabIndex = 7;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click_1);
            // 
            // btnClose
            // 
            appearance13.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance13;
            this.btnClose.Location = new System.Drawing.Point(330, 177);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(81, 30);
            this.btnClose.TabIndex = 8;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click_1);
            // 
            // FPersonalSalaryTaxDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(415, 214);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.ultraGroupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FPersonalSalaryTaxDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Biểu tính thuế thu nhập";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FPersonalSalaryTaxDetail_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbSalaryType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkChiTiet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbChiTiet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPersonalSalaryTaxName)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraLabel lblPersonalSalaryTaxName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtPersonalSalaryTaxName;
        private Infragistics.Win.Misc.UltraLabel lblPersonalSalaryTaxGrade;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkChiTiet;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbChiTiet;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbSalaryType;
        private Infragistics.Win.Misc.UltraLabel lblSalaryType;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel lblToAmount;
        private Infragistics.Win.Misc.UltraLabel lblFromAmount;
        private Infragistics.Win.Misc.UltraLabel lblTaxRate;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtTaxRate;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtToAmount;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtFromAmount;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtPersonalSalaryTaxGrade;
    }
}
