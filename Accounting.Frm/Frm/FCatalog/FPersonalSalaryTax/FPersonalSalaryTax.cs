﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using Accounting.TextMessage;
using System.Data;
using System.Linq;

namespace Accounting
{
    public partial class FPersonalSalaryTax : CatalogBase //UserControl
    {
        #region khai báo
        private readonly IPersonalSalaryTaxService _IPersonalSalaryTaxService;

        static Dictionary<int, string> DicSalaryType;
        public static Dictionary<int, string> dicSalaryType
        {
            get { DicSalaryType = DicSalaryType ?? (Dictionary<int, string>)BuildConfig(1); return DicSalaryType; }
        }
        #endregion

        #region khởi tạo
        public FPersonalSalaryTax()
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _IPersonalSalaryTaxService = IoC.Resolve<IPersonalSalaryTaxService>();
            this.uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            #endregion

            #region Tạo dữ liệu ban đầu
            LoadDuLieu();
            #endregion
            System.Windows.Forms.ToolTip ToolTip1 = new System.Windows.Forms.ToolTip();
            ToolTip1.SetToolTip(btnAdd, "Thêm (Ctrl + N)");
            System.Windows.Forms.ToolTip ToolTip2 = new System.Windows.Forms.ToolTip();
            ToolTip2.SetToolTip(btnEdit, "Xem (Ctrl + E)");
            System.Windows.Forms.ToolTip ToolTip3 = new System.Windows.Forms.ToolTip();
            ToolTip3.SetToolTip(btnDelete, "Xóa (Ctrl + D)");
            if (uGrid.Rows.Count > 0) uGrid.Rows[0].Selected = true;
        }

        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }
        private void LoadDuLieu(bool configGrid)
        {
            WaitingFrm.StartWaiting();
            #region Lấy dữ liệu từ CSDL
            List<PersonalSalaryTax> listsalary = _IPersonalSalaryTaxService.GetAll();
            _IPersonalSalaryTaxService.UnbindSession(listsalary);
            listsalary = _IPersonalSalaryTaxService.GetAll();
            #endregion

            #region Xử lý dữ liệu           
            foreach (var item in listsalary)
            {
                item.SalaryTypeView = dicSalaryType[item.SalaryType];
            }
            #endregion

            #region hiển thị và xử lý hiển thị
            uGrid.DataSource = listsalary.OrderBy(x => x.SalaryType).ThenBy(x => x.TaxRate).ToList();

            if (configGrid) ConfigGrid(uGrid);
            uGrid.DisplayLayout.Bands[0].Columns["TaxRate"].FormatNumberic(ConstDatabase.Format_Rate);
            uGrid.DisplayLayout.Bands[0].Columns["FromAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["ToAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            #endregion
            WaitingFrm.StopWaiting();
        }
        #endregion

        #region Nghiệp vụ
        #region ContentMenu

        private void tsmAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void tsmEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }
        private void tsmDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            ResetFunction();
        }
        protected override void ResetFunction()
        {
            LoadDuLieu();
        }
        #endregion

        #region Button
        private void btnAdd_Click_1(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void btnDelete_Click_1(object sender, EventArgs e)
        {
            DeleteFunction();
        }
        #endregion

        private void uGrid_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (e.Row.Index >= 0)
            {
                PersonalSalaryTax temp = e.Row.ListObject as PersonalSalaryTax;
                new FPersonalSalaryTaxDetail(temp).ShowDialog(this);
                if (!FPersonalSalaryTaxDetail.isClose) LoadDuLieu();
            }
        }

        /// <summary>
        /// Nghiệp vụ Add
        /// </summary>
        protected override void AddFunction()
        {
            new FPersonalSalaryTaxDetail().ShowDialog(this);
            if (!FPersonalSalaryTaxDetail.isClose) LoadDuLieu();
        }

        /// <summary>
        /// Nghiệp vụ Edit
        /// </summary>
        protected override void EditFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                PersonalSalaryTax temp = uGrid.Selected.Rows[0].ListObject as PersonalSalaryTax;
                new FPersonalSalaryTaxDetail(temp).ShowDialog(this);
                if (!FPersonalSalaryTaxDetail.isClose) LoadDuLieu();
            }
            else
                MSG.Error(string.Format(resSystem.MSG_Catalog3,"một Biểu tính thuế thu nhập cá nhân"));
        }

        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        protected override void DeleteFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                PersonalSalaryTax temp = uGrid.Selected.Rows[0].ListObject as PersonalSalaryTax;
                if (MSG.Question(string.Format(resSystem.MSG_System_05, temp.PersonalSalaryTaxName)) == System.Windows.Forms.DialogResult.Yes)
                {
                    try
                    {
                        _IPersonalSalaryTaxService.BeginTran();
                        _IPersonalSalaryTaxService.Delete(temp);
                        _IPersonalSalaryTaxService.CommitTran();
                        Utils.ClearCacheByType<PersonalSalaryTax>();
                        LoadDuLieu();
                    }
                    catch
                    {
                        _IPersonalSalaryTaxService.RolbackTran();
                    }
                }
            }
            else
                MSG.Error(string.Format(resSystem.MSG_Catalog2,"một Biểu tính thuế thu nhập cá nhân"));
            
        }
        #endregion

        #region Utils
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, TextMessage.ConstDatabase.PersonalSalaryTax_TableName);
        }
        static object BuildConfig(int obj)
        {
            Dictionary<int, string> temp = new Dictionary<int, string>();
            switch (obj)
            {
                case 1:
                    {
                        temp.Add(0, "Thường xuyên");
                        temp.Add(1, "Không thường xuyên");
                    }
                    break;
            }
            return temp;
        }

        #endregion

        private void FPersonalSalaryTax_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGrid.ResetText();
            uGrid.ResetUpdateMode();
            uGrid.ResetExitEditModeOnLeave();
            uGrid.ResetRowUpdateCancelAction();
            uGrid.DataSource = null;
            uGrid.Layouts.Clear();
            uGrid.ResetLayouts();
            uGrid.ResetDisplayLayout();
            uGrid.Refresh();
            uGrid.ClearUndoHistory();
            uGrid.ClearXsdConstraints();
        }

        private void FPersonalSalaryTax_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
