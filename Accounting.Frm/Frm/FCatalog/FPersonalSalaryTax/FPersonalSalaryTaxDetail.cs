﻿using System;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Accounting.TextMessage;
using System.Drawing;

namespace Accounting
{
    public partial class FPersonalSalaryTaxDetail : DialogForm //UserControl
    {
        #region khai báo
        private readonly IPersonalSalaryTaxService _IPersonalSalaryTaxService;
        PersonalSalaryTax _Select = new PersonalSalaryTax();
        bool Them = true;
        public static bool isClose = true;
        #endregion

        #region khởi tạo
        public FPersonalSalaryTaxDetail()
        {//Thêm
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            this.Text = "Thêm mới Biểu tính thuế thu nhập";
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.CenterToParent();

            #endregion

            #region Thiết lập ban đầu cho Form
            //Khai báo các webservices
            _IPersonalSalaryTaxService = IoC.Resolve<IPersonalSalaryTaxService>();

            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            InitializeGUI();
            #endregion
            setSize();
        }
        private void setSize()
        {
            cbbSalaryType.Location = new Point(lblSalaryType.Location.X + lblSalaryType.Width + 38, cbbSalaryType.Location.Y);
            lblPersonalSalaryTaxGrade.Location = new Point(cbbSalaryType.Location.X + cbbSalaryType.Width + 8, lblPersonalSalaryTaxGrade.Location.Y);
            txtPersonalSalaryTaxGrade.Location = new Point(lblPersonalSalaryTaxGrade.Location.X + lblPersonalSalaryTaxGrade.Width + 26, txtPersonalSalaryTaxGrade.Location.Y);
            txtPersonalSalaryTaxName.Location = new Point(cbbSalaryType.Location.X, txtPersonalSalaryTaxName.Location.Y);
            txtPersonalSalaryTaxName.Width = txtPersonalSalaryTaxGrade.Location.X - cbbSalaryType.Location.X + txtPersonalSalaryTaxGrade.Width;
            txtFromAmount.Location = new Point(cbbSalaryType.Location.X, txtFromAmount.Location.Y);
            txtToAmount.Location = new Point(cbbSalaryType.Location.X, txtToAmount.Location.Y);
            txtTaxRate.Location = new Point(cbbSalaryType.Location.X, txtTaxRate.Location.Y);
            ultraLabel4.Location = new Point(txtTaxRate.Location.X + txtTaxRate.Width + 3, ultraLabel4.Location.Y);
            ultraGroupBox1.Size = new Size(txtPersonalSalaryTaxGrade.Location.X - lblSalaryType.Location.X + txtPersonalSalaryTaxGrade.Width + 13, ultraGroupBox1.Height);
            this.Size = new Size(ultraGroupBox1.Width + 38, this.Size.Height);
            btnClose.Location = new Point(txtPersonalSalaryTaxGrade.Location.X + txtPersonalSalaryTaxGrade.Width - btnClose.Width, btnClose.Location.Y);
            btnSave.Location = new Point(btnClose.Location.X - 4 - btnClose.Width, btnSave.Location.Y);

        }
        public FPersonalSalaryTaxDetail(PersonalSalaryTax temp)
        {//Sửa
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            //khai báo các webservice
            _IPersonalSalaryTaxService = IoC.Resolve<IPersonalSalaryTaxService>();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.CenterToParent();
            #endregion
            this.Text = "Sửa Biểu tính thuế thu nhập";
            #region Thiết lập ban đầu cho Form
            _Select = temp;
            Them = false;

            //Khai báo các webservices
            _IPersonalSalaryTaxService = IoC.Resolve<IPersonalSalaryTaxService>();

            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion

            #region Fill dữ liệu Obj vào control
            ObjandGUI(temp, false);
            #endregion
        }

        private void InitializeGUI()
        {
            //Khởi tạo cbb loại thu nhập

            cbbSalaryType.DataSource = FPersonalSalaryTax.dicSalaryType.Values.ToList();
            cbbSalaryType.SelectedIndex = 0;
            txtTaxRate.FormatNumberic(ConstDatabase.Format_Rate);
            txtTaxRate.MaxValue = 100;
            txtFromAmount.FormatNumberic(ConstDatabase.Format_TienVND);
            txtToAmount.FormatNumberic(ConstDatabase.Format_TienVND);
        }
        #endregion

        #region Button Event
        /// <summary>
        /// Sự kiện nút Lưu lại

        private void btnSave_Click_1(object sender, EventArgs e)
        {
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion

            //Gui to object
            #region Fill dữ liệu control vào obj
            PersonalSalaryTax temp = Them ? new PersonalSalaryTax() : _IPersonalSalaryTaxService.Getbykey(_Select.ID);
            temp = ObjandGUI(temp, true);
            #endregion

            #region Thao tác CSDL
            try
            {
                //_IPersonalSalaryTaxService.BeginTran();
                if (Them) _IPersonalSalaryTaxService.CreateNew(temp);
                else _IPersonalSalaryTaxService.Update(temp);
                _IPersonalSalaryTaxService.CommitChanges();
            }
            catch
            {
                _IPersonalSalaryTaxService.RolbackTran();
            }

            #endregion

            #region xử lý form, kết thúc form
            this.Close();
            isClose = false;
            #endregion
        }

        /// <summary>
        /// Sự kiện nút đóng

        private void btnClose_Click_1(object sender, EventArgs e)
        {
            isClose = true;
            this.Close();
        }
        #endregion

        #region Utils
        /// <summary>
        /// get hoặc set một đối tượng vào giao diện xử lý
        /// </summary>
        /// <param name="input">đối tượng</param>
        /// <param name="isGet">True: get giá trị từ control gán vào đối tượng| False: set giá trị đối tượng vào control</param>
        /// <returns></returns>
        PersonalSalaryTax ObjandGUI(PersonalSalaryTax input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();  //Thêm mới

                //tên thu nhập
                input.PersonalSalaryTaxName = txtPersonalSalaryTaxName.Text;
                //bậc thuế
                if (txtPersonalSalaryTaxGrade.Text != "")
                    input.PersonalSalaryTaxGrade = int.Parse(txtPersonalSalaryTaxGrade.Text);   //chưa tính bậc thuế
                //loại thu nhập
                input.SalaryType = cbbSalaryType.SelectedIndex;
                //thuế suất
                input.TaxRate = decimal.Parse(txtTaxRate.Text);
                //thu nhập từ
                input.FromAmount = decimal.Parse(txtFromAmount.Text);
                //thu nhập đến
                input.ToAmount = decimal.Parse(txtToAmount.Text);
            }
            else
            {
                //tên thu nhập
                txtPersonalSalaryTaxName.Text = input.PersonalSalaryTaxName;
                //bậc thuế
                txtPersonalSalaryTaxGrade.Text = input.PersonalSalaryTaxGrade.ToString();
                //loại thu nhập
                cbbSalaryType.SelectedIndex = input.SalaryType;
                //thuế suất
                txtTaxRate.Text = input.TaxRate.ToString();
                //thu nhập từ
                txtFromAmount.Text = input.FromAmount.ToString();
                //thu nhập đến
                txtToAmount.Text = input.ToAmount.ToString();
            }
            return input;
        }

        bool CheckError()
        {
            bool kq = true;
            //Check Error Chung

            //Check Error Riêng từng Form
            if (string.IsNullOrEmpty(txtPersonalSalaryTaxName.Text) || string.IsNullOrEmpty(txtFromAmount.Text) || string.IsNullOrEmpty(txtToAmount.Text) || string.IsNullOrEmpty(txtTaxRate.Text) || string.IsNullOrEmpty(cbbSalaryType.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }

            //Check thu nhập từ phải nhỏ hơn thu nhập đến
            long fromAmount = 0;
            long toAmount = 0;
            if ((long.TryParse(txtFromAmount.Text, out fromAmount) && long.TryParse(txtToAmount.Text, out toAmount)) && (fromAmount >= toAmount))
            {
                MSG.Error("Thu nhập từ không được lớn hơn Thu nhập đến");
                return false;
            }
            return kq;


        }
        #endregion

        private void FPersonalSalaryTaxDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
