﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.IService;
using Accounting.Core.Domain;
using FX.Core;
using Accounting.TextMessage;
using System.Globalization;

namespace Accounting
{
    public partial class FMaterialGoodsResourceTaxGroupDetail : CustormForm
    {
        #region khai báo
        private IMaterialGoodsResourceTaxGroupService _IMaterialGoodsResourceTaxGroupService;
        MaterialGoodsResourceTaxGroup _Select = new MaterialGoodsResourceTaxGroup();
        bool ad;
        private Guid _id;
        public Guid Id
        {
            get { return _id; }
            set { _id = value; }
        }
        bool Them = true;
        public static bool isClose = true;
        #endregion
        public FMaterialGoodsResourceTaxGroupDetail()
        {
            InitializeComponent();
            Add();
        }
        private void Loaddulieu()
        {

        }

        public FMaterialGoodsResourceTaxGroupDetail(MaterialGoodsResourceTaxGroup temp)
        {
            InitializeComponent();
            _IMaterialGoodsResourceTaxGroupService = IoC.Resolve<IMaterialGoodsResourceTaxGroupService>();
            Edit(temp);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public FMaterialGoodsResourceTaxGroupDetail(MaterialGoodsResourceTaxGroup temp, bool them)
        {
            InitializeComponent();
            if (them)
            {
                Add();
                foreach (var item in ultraCombo1.Rows)
                {
                    if ((item.ListObject as MaterialGoodsResourceTaxGroup).ID == temp.ID) ultraCombo1.SelectedRow = item;
                }
            }
            else
            {
                Edit(temp);
            }
        }
        private void Add()
        {
            _IMaterialGoodsResourceTaxGroupService = IoC.Resolve<IMaterialGoodsResourceTaxGroupService>();
            this.Text = "Thêm mới Biểu thuế tài nguyên";
            chkIsActive.Visible = false;
            InitializeGUI();
        }
        private void Edit(MaterialGoodsResourceTaxGroup temp)
        {
            _Select = temp;
            Them = false;
            txtMa.Enabled = false;
            _IMaterialGoodsResourceTaxGroupService = IoC.Resolve<IMaterialGoodsResourceTaxGroupService>();
            InitializeGUI();
            ObjandGUI(temp, false);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            _IMaterialGoodsResourceTaxGroupService.BeginTran();
            MaterialGoodsResourceTaxGroup temp;
            if (!CheckError()) return;
            if (Them)
            {
                //if (!CheckCode()) return;

                ///temp = new MaterialGoodsResourceTaxGroup();
                //ObjandGUI(temp,true);
                //_IMaterialGoodsResourceTaxGroupService.CreateNew(temp);
                if (!CheckCode()) return;
                temp = Them ? new MaterialGoodsResourceTaxGroup() : _IMaterialGoodsResourceTaxGroupService.Getbykey(_Select.ID);
                temp = ObjandGUI(temp, true);

                //set order fixcode
                MaterialGoodsResourceTaxGroup temp0 = (MaterialGoodsResourceTaxGroup)Utils.getSelectCbbItem(ultraCombo1);
                //List<string> lstOrderFixCodeChild = _IDepartmentService.Query.Where(a => a.ParentID == temp.ParentID)
                //    .Select(a => a.OrderFixCode).ToList();
                List<string> lstOrderFixCodeChild = _IMaterialGoodsResourceTaxGroupService.GetListOrderFixCodeParentID(temp.ParentID);
                string orderFixCodeParent = temp0 == null ? "" : temp0.OrderFixCode;
                temp.OrderFixCode = Utils.GetOrderFixCode(lstOrderFixCodeChild, orderFixCodeParent);
                temp.IsParentNode = false; // mac dinh cho insert
                temp.Grade = temp0 == null ? 1 : temp0.Grade + 1; // quy tac lay gia tri cho grade

                //nếu cha isactive =false thì khi thêm mới con cũng là false

                if (ultraCombo1.Text.Equals(""))
                {
                    if (!CheckCode()) return;
                    temp.IsActive = true;
                    _IMaterialGoodsResourceTaxGroupService.CreateNew(temp);
                }
                else
                {
                    MaterialGoodsResourceTaxGroup objParentCombox = (MaterialGoodsResourceTaxGroup)Utils.getSelectCbbItem(ultraCombo1);
                    //lay cha
                    MaterialGoodsResourceTaxGroup Parent = _IMaterialGoodsResourceTaxGroupService.Getbykey(objParentCombox.ID);
                    if (Parent.IsActive == false)
                    {
                        temp.IsActive = false;
                    }
                    else
                        temp.IsActive = true;

                    //check loi
                    if (!CheckCode()) return;
                    //temp.IsActive = true;
                    _IMaterialGoodsResourceTaxGroupService.CreateNew(temp);
                }

                //update lai isparentnode neu co
                if (temp.ParentID != null)
                {
                    MaterialGoodsResourceTaxGroup parent = _IMaterialGoodsResourceTaxGroupService.Getbykey((Guid)temp.ParentID);
                    parent.IsParentNode = true;
                    _IMaterialGoodsResourceTaxGroupService.Update(parent);
                }
            }
            else
            {// cập nhật
                //temp = _IMaterialGoodsResourceTaxGroupService.Getbykey(_Select.ID);
                //ObjandGUI(temp, true);
                //_IMaterialGoodsResourceTaxGroupService.Update(temp);
                temp = _IMaterialGoodsResourceTaxGroupService.Getbykey(_Select.ID);
                //Lưu đối tượng trước khi sửa
                MaterialGoodsResourceTaxGroup tempOriginal = (MaterialGoodsResourceTaxGroup)Utils.CloneObject(temp);
                //Lấy về đối tượng chọn trong combobox
                MaterialGoodsResourceTaxGroup objParentCombox = (MaterialGoodsResourceTaxGroup)Utils.getSelectCbbItem(ultraCombo1);
                //Không thay đổi đối tượng cha trong combobox
                if (((temp.ParentID == null) && (objParentCombox == null)) || ((temp.ParentID != null) && (objParentCombox != null) && (temp.ParentID == objParentCombox.ID)))
                {
                    temp = ObjandGUI(temp, true);
                    _IMaterialGoodsResourceTaxGroupService.Update(temp);
                }
                else
                {
                    //không cho phép chuyển ngược
                    if (objParentCombox != null)
                    {
                        //List<Department> lstChildCheck =
                        //    _IDepartmentService.Query.Where(p => p.OrderFixCode.StartsWith(tempOriginal.OrderFixCode)).ToList();
                        List<MaterialGoodsResourceTaxGroup> lstChildCheck =_IMaterialGoodsResourceTaxGroupService.GetListMaterialGoodsResourceTaxGroupOrderFixCode(tempOriginal.OrderFixCode);
                        foreach (var item in lstChildCheck)
                        {
                            if (objParentCombox.ID == item.ID)
                            {
                                MSG.Warning(string.Format(resSystem.MSG_Catalog6, temp.MaterialGoodsResourceTaxGroupCode));
                                return;
                            }
                        }
                    }
                    //Trường hợp có cha cũ
                    //Kiểm tra cha cũ có còn con ngoài con chuyển đi hay không
                    //Chuyển trạng thái IsParentNode Cha cũ nếu hết con
                    if (temp.ParentID != null)
                    {
                        MaterialGoodsResourceTaxGroup oldParent = _IMaterialGoodsResourceTaxGroupService.Getbykey((Guid)temp.ParentID);
                        //int checkChildOldParent = _IDepartmentService.Query.Count(p => p.ParentID == temp.ParentID);
                        int checkChildOldParent = _IMaterialGoodsResourceTaxGroupService.CountListMaterialGoodsResourceTaxGroupParentID(temp.ParentID);
                        if (checkChildOldParent <= 1)
                        {
                            oldParent.IsParentNode = false;
                        }
                        _IMaterialGoodsResourceTaxGroupService.Update(oldParent);
                    }
                    //Trường hợp có cha mới
                    //Chuyển trạng IsParentNode cha mới
                    if (objParentCombox != null)
                    {
                        MaterialGoodsResourceTaxGroup newParent = _IMaterialGoodsResourceTaxGroupService.Getbykey(objParentCombox.ID);
                        newParent.IsParentNode = true;
                        _IMaterialGoodsResourceTaxGroupService.Update(newParent);

                        //List<Department> listChildNewParent = _IDepartmentService.Query.Where(
                        //    a => a.ParentID == objParentCombox.ID).ToList();
                        List<MaterialGoodsResourceTaxGroup> listChildNewParent =
                            _IMaterialGoodsResourceTaxGroupService.GetListMaterialGoodsResourceTaxGroupParentID(objParentCombox.ID);
                        //Tính lại OrderFixCode cho đối tượng chính
                        List<string> lstOfcChildNewParent = new List<string>();
                        foreach (var item in listChildNewParent)
                        {
                            lstOfcChildNewParent.Add(item.OrderFixCode);
                        }
                        string orderFixCodeParent = newParent.OrderFixCode;
                        temp.OrderFixCode = Utils.GetOrderFixCode(lstOfcChildNewParent, orderFixCodeParent);
                        //Tính lại bậc cho đối tượng chính
                        temp.Grade = newParent.Grade + 1;
                    }
                    //Trường hợp không có cha mới tức là chuyển đối tượng lên bậc 1
                    if (objParentCombox == null)
                    {
                        //Lấy về tất cả đối tượng bậc 1
                        List<int> lstOfcChildGradeNo1 = new List<int>();
                        //List<Department> listChildGradeNo1 = _IDepartmentService.Query.Where(
                        //    a => a.Grade == 1).ToList();
                        List<MaterialGoodsResourceTaxGroup> listChildGradeNo1 = _IMaterialGoodsResourceTaxGroupService.GetListMaterialGoodsResourceTaxGroupGrade(1);
                        foreach (var item in listChildGradeNo1)
                        {
                            lstOfcChildGradeNo1.Add(Convert.ToInt32(item.OrderFixCode));
                        }
                        //Tính lại OrderFixCode cho đối tượng chính
                        temp.OrderFixCode = (lstOfcChildGradeNo1.Max() + 1).ToString(CultureInfo.InvariantCulture);
                        //Tính lại bậc cho đối tượng chính
                        temp.Grade = 1;
                        temp = ObjandGUI(temp, true);
                        _IMaterialGoodsResourceTaxGroupService.Update(temp);
                    }
                    //Xử lý các con của nó 
                    //Lấy về các con
                    //List<Department> lstChild =
                    //    _IDepartmentService.Query.Where(p => p.OrderFixCode.StartsWith(tempOriginal.OrderFixCode)).ToList();
                    List<MaterialGoodsResourceTaxGroup> lstChild = _IMaterialGoodsResourceTaxGroupService.GetListMaterialGoodsResourceTaxGroupOrderFixCode(tempOriginal.OrderFixCode);
                    foreach (var item in lstChild)
                    {
                        string tempOldFixCode = tempOriginal.OrderFixCode;
                        item.OrderFixCode = temp.OrderFixCode + item.OrderFixCode.Substring(tempOldFixCode.Length);
                        item.Grade = temp.Grade - tempOriginal.Grade + item.Grade;
                        _IMaterialGoodsResourceTaxGroupService.Update(item);
                    }

                    temp = ObjandGUI(temp, true);
                }
                //nếu cha ngừng theo dõi thì con cũng ngừng theo dõi
                //Lấy về danh sách các con
                //List<Department> lstChild2 =
                //        _IDepartmentService.Query.Where(p => p.OrderFixCode.StartsWith(tempOriginal.OrderFixCode) && p.ID != tempOriginal.ID).ToList();
                List<MaterialGoodsResourceTaxGroup> lstChild2 = _IMaterialGoodsResourceTaxGroupService.GetListMaterialGoodsResourceTaxGroupParentID(tempOriginal.ID);
              // List <MaterialGoodsResourceTaxGroup> lstChild2 = _IMaterialGoodsResourceTaxGroupService.GetListMaterialGoodsResourceTaxGroupOrderFixCodeNotParent(tempOriginal.OrderFixCode, tempOriginal.ID);
                //Chuyen True -> False
                if (chkIsActive.CheckState == CheckState.Unchecked && CheckError())
                {
                    if (ad == true)
                    {
                        temp.IsActive = false;
                        //neu la cha
                        if (lstChild2.Count > 0)
                        {
                            if (MSG.Question("Bạn có muốn thiết lập cho tất cả các danh mục con của danh mục này sang trạng thái <<Không hoạt động>> không?") == System.Windows.Forms.DialogResult.Yes)
                            {
                                foreach (var itemChild in lstChild2)
                                {
                                    itemChild.IsActive = false;
                                    _IMaterialGoodsResourceTaxGroupService.Update(itemChild);
                                }
                            }
                        }
                    }
                    //neu ko phai la cha
                    _IMaterialGoodsResourceTaxGroupService.Update(temp);
                }

                //Chuyen False -> True
                else if (chkIsActive.CheckState == CheckState.Checked && CheckError())
                {
                    //Co Cha
                    if (temp.ParentID != null)
                    {
                        MaterialGoodsResourceTaxGroup Parent = _IMaterialGoodsResourceTaxGroupService.Getbykey(objParentCombox.ID);
                        //Cha la false khong cho chuyen
                        if (Parent.IsActive == false)
                        {
                            MSG.Warning(string.Format(resSystem.MSG_Catalog_Tree2, "Biểu thuế tài nguyên"));
                            return;
                        }
                        //Cha la True duoc phep chuyen
                        temp.IsActive = true;
                        if (lstChild2.Count > 0)
                        {

                            if (MSG.Question(string.Format(resSystem.MSG_Catalog_Tree3,"Biểu thuế tài nguyên")) == System.Windows.Forms.DialogResult.Yes)
                            {
                                foreach (var item2 in lstChild2)
                                {
                                    item2.IsActive = true;
                                    _IMaterialGoodsResourceTaxGroupService.Update(item2);
                                }
                            }

                        }
                        _IMaterialGoodsResourceTaxGroupService.Update(temp);
                    }
                    //Khong Co cha
                    else
                    {
                        if (ad == false)
                        {
                            temp.IsActive = true;
                            if (lstChild2.Count > 0)
                            {


                                if (MSG.Question("Bạn có muốn thiết lập cho tất cả các danh mục con của danh mục này sang trạng thái <<Hoạt động>> không?") == System.Windows.Forms.DialogResult.Yes)
                                {
                                    foreach (var item2 in lstChild2)
                                    {
                                        item2.IsActive = true;
                                        _IMaterialGoodsResourceTaxGroupService.Update(item2);
                                    }
                                }

                            }
                        }
                        _IMaterialGoodsResourceTaxGroupService.Update(temp);
                    }
                }
            }
            isClose = false;
            _IMaterialGoodsResourceTaxGroupService.CommitTran();
            Utils.ListMaterialGoodsResourceTaxGroup.Clear();
            this.Close();
        }
        private void InitializeGUI()
        {
            ultraCombo1.DataSource = _IMaterialGoodsResourceTaxGroupService.GetAll();
            ultraCombo1.DisplayMember = "MaterialGoodsResourceTaxGroupCode";
            Utils.ConfigGrid(ultraCombo1, ConstDatabase.MaterialGoodsResourceTaxGroup_TableName);
            txtThuesuat.FormatNumberic(ConstDatabase.Format_Coefficient);
            txtThuesuat.MaxValue = 100;

        }
        MaterialGoodsResourceTaxGroup ObjandGUI(MaterialGoodsResourceTaxGroup input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();
                input.MaterialGoodsResourceTaxGroupCode = txtMa.Text;
                input.MaterialGoodsResourceTaxGroupName = txtTen.Text;
                input.Unit = txtDonvitinh.Text;
                if (!string.IsNullOrEmpty(txtThuesuat.Text))
                {
                    input.TaxRate = Convert.ToDecimal(txtThuesuat.Text);
                }
                input.IsActive = chkIsActive.Checked;
                MaterialGoodsResourceTaxGroup temp0 = (MaterialGoodsResourceTaxGroup)Utils.getSelectCbbItem(ultraCombo1);
                if (temp0 == null) input.ParentID = null;
                else input.ParentID = temp0.ID;
                
            }
            else
            {
                txtMa.Text = input.MaterialGoodsResourceTaxGroupCode;
                txtTen.Text = input.MaterialGoodsResourceTaxGroupName;
                txtDonvitinh.Text = input.Unit;
                txtThuesuat.Text = input.TaxRate.ToString();
                foreach (var item in ultraCombo1.Rows)
                {
                    if ((item.ListObject as MaterialGoodsResourceTaxGroup).ID == input.ParentID) ultraCombo1.SelectedRow = item;
                }
                chkIsActive.Checked = input.IsActive;
                 ad = input.IsActive;
            }
            return input;
        }
        bool CheckError()
        {
            bool kq = true;
            if (string.IsNullOrEmpty(txtMa.Text) || string.IsNullOrEmpty(txtTen.Text))
            {
                //MSG.Error(resSystem.MSG_Error_18);
                return false;
            }
            return kq;
        }
        bool CheckCode()
        {
            bool kq = true;
            //check trùng code
            List<string> lst = _IMaterialGoodsResourceTaxGroupService.GetListMaterialGoodsResourceTaxGroupCode();
            foreach (var x in lst)
            {
                if (txtMa.Text.Equals(x))
                {
                    MSG.Warning(string.Format(resSystem.MSG_Catalog_Account6, "Biểu thuế tài nguyên"));
                    return false;
                }
            }
            return kq;
        }

        private void FMaterialGoodsResourceTaxGroupDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
