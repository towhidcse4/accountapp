﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Accounting.TextMessage;
using System.Data;
using Infragistics.Win.UltraWinTree;

namespace Accounting
{
    public partial class FMaterialGoodsResourceTaxGroup : CatalogBase
    {
        #region Khai báo
        private readonly IMaterialGoodsResourceTaxGroupService _IMaterialGoodsResourceTaxGroupService;
        List<MaterialGoodsResourceTaxGroup> dsMaterialGoodsResourceTaxGroup = new List<MaterialGoodsResourceTaxGroup>();
        #endregion

        public FMaterialGoodsResourceTaxGroup()
        {
            InitializeComponent();
            ToolTip ToolTip1 = new ToolTip();
            ToolTip1.SetToolTip(btnAdd, "Thêm (Ctrl + N)");
            ToolTip ToolTip2 = new ToolTip();
            ToolTip2.SetToolTip(btnEdit, "Sửa (Ctrl + E)");
            ToolTip ToolTip3 = new ToolTip();
            ToolTip3.SetToolTip(btnDelete, "Xóa (Ctrl + D)");
            _IMaterialGoodsResourceTaxGroupService = IoC.Resolve<IMaterialGoodsResourceTaxGroupService>();


            this.uTree.InitializeDataNode += new Infragistics.Win.UltraWinTree.InitializeDataNodeEventHandler(Utils.uTree_InitializeDataNode);
            this.uTree.AfterDataNodesCollectionPopulated += new Infragistics.Win.UltraWinTree.AfterDataNodesCollectionPopulatedEventHandler(Utils.uTree_AfterDataNodesCollectionPopulated);
            this.uTree.ColumnSetGenerated += new Infragistics.Win.UltraWinTree.ColumnSetGeneratedEventHandler(this.uTree_ColumnSetGenerated);
            this.uTree.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ultraTree1_MouseDown);
            LoadDuLieu();

        }
        #region Utils

        void ConfigTree(Infragistics.Win.UltraWinTree.UltraTree ultraTree)
        {//hàm chung
            Utils.ConfigTree(ultraTree, TextMessage.ConstDatabase.MaterialGoodsResourceTaxGroup_TableName);
            //UltraTreeColumnSet rootColumnSet = this.uTree.ColumnSettings.RootColumnSet;
            //rootColumnSet.Columns["TaxRate"].FormatNumberic(ConstDatabase.Format_Rate);
        }

        private void uTree_ColumnSetGenerated(object sender, Infragistics.Win.UltraWinTree.ColumnSetGeneratedEventArgs e)
        {
            Utils.uTree_ColumnSetGenerated(sender, e, ConstDatabase.MaterialGoodsResourceTaxGroup_TableName);
            e.ColumnSet.Columns["TaxRate"].FormatNumberic(ConstDatabase.Format_Rate);
            e.ColumnSet.Columns["TaxRate"].MaxLength = 100;
        }
        #endregion
        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }
        private void LoadDuLieu(bool configTree)
        {
            WaitingFrm.StartWaiting();
            dsMaterialGoodsResourceTaxGroup = _IMaterialGoodsResourceTaxGroupService.GetAll_OrderBy();
            _IMaterialGoodsResourceTaxGroupService.UnbindSession(dsMaterialGoodsResourceTaxGroup);
            dsMaterialGoodsResourceTaxGroup = _IMaterialGoodsResourceTaxGroupService.GetAll_OrderBy();
            DataSet ds = Utils.ToDataSet<MaterialGoodsResourceTaxGroup>(dsMaterialGoodsResourceTaxGroup, ConstDatabase.MaterialGoodsResourceTaxGroup_TableName);
            uTree.SetDataBinding(ds, ConstDatabase.MaterialGoodsResourceTaxGroup_TableName);
            ConfigTree(uTree);
            WaitingFrm.StopWaiting();
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }
        protected override void AddFunction()
        {
            if (uTree.SelectedNodes.Count > 0)
            {
                MaterialGoodsResourceTaxGroup temp = dsMaterialGoodsResourceTaxGroup.Single(k => k.ID == (new Guid(uTree.SelectedNodes[0].Text)));
                new FMaterialGoodsResourceTaxGroupDetail(temp, true).ShowDialog(this);
                if (!FMaterialGoodsResourceTaxGroupDetail.isClose) LoadDuLieu();
            }
            else
                new FMaterialGoodsResourceTaxGroupDetail().ShowDialog(this);
            if (!FMaterialGoodsResourceTaxGroupDetail.isClose) LoadDuLieu();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }
        protected override void EditFunction()
        {
            if (uTree.SelectedNodes.Count == 0 && uTree.ActiveNode != null)
            {
                uTree.ActiveNode.Selected = true;
            }
            if (uTree.SelectedNodes.Count > 0)
            {
                MaterialGoodsResourceTaxGroup temp = dsMaterialGoodsResourceTaxGroup.Single(k => k.ID == (new Guid(uTree.SelectedNodes[0].Text)));
                new FMaterialGoodsResourceTaxGroupDetail(temp).ShowDialog(this);
                if (!FMaterialGoodsResourceTaxGroupDetail.isClose) LoadDuLieu();
            }
            else
                Accounting.TextMessage.MSG.Error(string.Format(Accounting.TextMessage.resSystem.MSG_Catalog3, "một Biểu thuế tài nguyên"));
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }
        protected override void DeleteFunction()
        {
            if (uTree.SelectedNodes.Count == 0 && uTree.ActiveNode != null)
            {
                uTree.ActiveNode.Selected = true;
            }
            if (uTree.SelectedNodes.Count > 0)
            {
                //MaterialGoodsResourceTaxGroup temp = dsMaterialGoodsResourceTaxGroup.Single(k => k.ID == (new Guid(ultraTree1.SelectedNodes[0].Text)));
                //_IMaterialGoodsResourceTaxGroupService.BeginTran();
                //_IMaterialGoodsResourceTaxGroupService.Delete(temp);
                //_IMaterialGoodsResourceTaxGroupService.CommitTran();
                //LoadDuLieu();
                MaterialGoodsResourceTaxGroup temp = dsMaterialGoodsResourceTaxGroup.Single(k => k.ID == (new Guid(uTree.SelectedNodes[0].Text)));
                if (MSG.Question(string.Format(resSystem.MSG_System_05, "<" + temp.MaterialGoodsResourceTaxGroupCode + ">")) == System.Windows.Forms.DialogResult.Yes)
                {
                    _IMaterialGoodsResourceTaxGroupService.BeginTran();
                    //Kiểm tra xem đối tượng có con không nếu có con thì không được phép xóa
                    List<MaterialGoodsResourceTaxGroup> lstChild = _IMaterialGoodsResourceTaxGroupService.GetListMaterialGoodsResourceTaxGroupParentID(temp.ID);
                    if (lstChild.Count > 0)
                    {
                        MSG.Warning(string.Format(resSystem.MSG_Catalog_Tree, "Biểu thuế tài nguyên"));
                        return;
                    }
                    //Check xem cha còn có con khác không nếu không thì chuyển trạng thái IsParentNode = false
                    if (temp.ParentID != null)
                    {
                        MaterialGoodsResourceTaxGroup parent = _IMaterialGoodsResourceTaxGroupService.Getbykey((Guid)temp.ParentID);
                        //int checkChildOldParent = _IDepartmentService.Query.Count(p => p.ParentID == temp.ParentID);
                        //int checkChildOldParent = _IDepartmentService.GetListDepartmentParentID((Guid)temp.ParentID).Count;
                        int checkChildOldParent = _IMaterialGoodsResourceTaxGroupService.CountListMaterialGoodsResourceTaxGroupParentID(temp.ParentID);
                        if (checkChildOldParent <= 1)
                        {
                            parent.IsParentNode = false;
                        }
                        _IMaterialGoodsResourceTaxGroupService.Update(parent);
                        _IMaterialGoodsResourceTaxGroupService.Delete(temp);
                    }
                    if (temp.ParentID == null)
                    {
                        _IMaterialGoodsResourceTaxGroupService.Delete(temp);
                    }
                    _IMaterialGoodsResourceTaxGroupService.CommitTran();
                    Utils.ClearCacheByType<MaterialGoodsResourceTaxGroup>();
                    LoadDuLieu();
                }

            }
            else
                MSG.Error(string.Format(resSystem.MSG_Catalog2, "một Biểu thuế tài nguyên"));
            
        }
        private void ultraTree1_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, cms4Grid);
        }
        private void ultraTree1_DoubleClick(object sender, EventArgs e)
        {
            if (uTree.SelectedNodes.Count > 0)
            {
                EditFunction();
            }

        }

        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            ResetFunction();
        }
        protected override void ResetFunction()
        {
            LoadDuLieu();
        }

        private void FMaterialGoodsResourceTaxGroup_FormClosing(object sender, FormClosingEventArgs e)
        {
            uTree = null;
        }

        private void FMaterialGoodsResourceTaxGroup_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
