﻿namespace Accounting
{
    partial class FFixedAssetCategoryDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.nmrUsedTime = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.nmrDepreciationRate = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbExpenditureAcount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbDepreciationAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbOriginalPriceAcount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtFixedAssetCagoryName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.txtDescription = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbParentId = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtFixedCategoryCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel2 = new Infragistics.Win.Misc.UltraPanel();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.chkIsActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbExpenditureAcount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDepreciationAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbOriginalPriceAcount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtFixedAssetCagoryName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbParentId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFixedCategoryCode)).BeginInit();
            this.ultraPanel2.ClientArea.SuspendLayout();
            this.ultraPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraGroupBox2);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraGroupBox1);
            this.ultraPanel1.Location = new System.Drawing.Point(4, 2);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(541, 355);
            this.ultraPanel1.TabIndex = 0;
            // 
            // ultraGroupBox2
            // 
            appearance1.FontData.BoldAsString = "True";
            this.ultraGroupBox2.Appearance = appearance1;
            this.ultraGroupBox2.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox2.Controls.Add(this.nmrUsedTime);
            this.ultraGroupBox2.Controls.Add(this.nmrDepreciationRate);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel10);
            this.ultraGroupBox2.Controls.Add(this.cbbExpenditureAcount);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel9);
            this.ultraGroupBox2.Controls.Add(this.cbbDepreciationAccount);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel8);
            this.ultraGroupBox2.Controls.Add(this.cbbOriginalPriceAcount);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel7);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel6);
            this.ultraGroupBox2.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox2.Location = new System.Drawing.Point(3, 173);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ultraGroupBox2.Size = new System.Drawing.Size(535, 128);
            this.ultraGroupBox2.TabIndex = 1;
            this.ultraGroupBox2.Text = "Chi tiết sử dụng";
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // nmrUsedTime
            // 
            appearance2.ImageHAlign = Infragistics.Win.HAlign.Left;
            appearance2.TextHAlignAsString = "Right";
            appearance2.TextVAlignAsString = "Bottom";
            this.nmrUsedTime.Appearance = appearance2;
            this.nmrUsedTime.AutoSize = false;
            this.nmrUsedTime.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.nmrUsedTime.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.nmrUsedTime.InputMask = "nnn,nnn,nnn.nn";
            this.nmrUsedTime.Location = new System.Drawing.Point(123, 31);
            this.nmrUsedTime.Name = "nmrUsedTime";
            this.nmrUsedTime.NullText = "0";
            this.nmrUsedTime.PromptChar = ' ';
            this.nmrUsedTime.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.nmrUsedTime.Size = new System.Drawing.Size(149, 22);
            this.nmrUsedTime.TabIndex = 5;
            this.nmrUsedTime.Text = "000";
            // 
            // nmrDepreciationRate
            // 
            appearance3.ImageHAlign = Infragistics.Win.HAlign.Left;
            appearance3.TextHAlignAsString = "Right";
            appearance3.TextVAlignAsString = "Bottom";
            this.nmrDepreciationRate.Appearance = appearance3;
            this.nmrDepreciationRate.AutoSize = false;
            this.nmrDepreciationRate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.nmrDepreciationRate.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.nmrDepreciationRate.InputMask = "nnn,nnn,nnn.nnnn";
            this.nmrDepreciationRate.Location = new System.Drawing.Point(123, 59);
            this.nmrDepreciationRate.Name = "nmrDepreciationRate";
            this.nmrDepreciationRate.NullText = "0";
            this.nmrDepreciationRate.PromptChar = ' ';
            this.nmrDepreciationRate.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.nmrDepreciationRate.Size = new System.Drawing.Size(149, 22);
            this.nmrDepreciationRate.TabIndex = 7;
            this.nmrDepreciationRate.Text = "00,000";
            // 
            // ultraLabel10
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextHAlignAsString = "Left";
            appearance4.TextVAlignAsString = "Middle";
            this.ultraLabel10.Appearance = appearance4;
            this.ultraLabel10.Location = new System.Drawing.Point(278, 87);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(91, 22);
            this.ultraLabel10.TabIndex = 20;
            this.ultraLabel10.Text = "TK Chi phí";
            // 
            // cbbExpenditureAcount
            // 
            this.cbbExpenditureAcount.AutoSize = false;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbExpenditureAcount.DisplayLayout.Appearance = appearance5;
            this.cbbExpenditureAcount.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbExpenditureAcount.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance6.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance6.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbExpenditureAcount.DisplayLayout.GroupByBox.Appearance = appearance6;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbExpenditureAcount.DisplayLayout.GroupByBox.BandLabelAppearance = appearance7;
            this.cbbExpenditureAcount.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance8.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance8.BackColor2 = System.Drawing.SystemColors.Control;
            appearance8.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance8.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbExpenditureAcount.DisplayLayout.GroupByBox.PromptAppearance = appearance8;
            this.cbbExpenditureAcount.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbExpenditureAcount.DisplayLayout.MaxRowScrollRegions = 1;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            appearance9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbExpenditureAcount.DisplayLayout.Override.ActiveCellAppearance = appearance9;
            appearance10.BackColor = System.Drawing.SystemColors.Highlight;
            appearance10.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbExpenditureAcount.DisplayLayout.Override.ActiveRowAppearance = appearance10;
            this.cbbExpenditureAcount.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbExpenditureAcount.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            this.cbbExpenditureAcount.DisplayLayout.Override.CardAreaAppearance = appearance11;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            appearance12.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbExpenditureAcount.DisplayLayout.Override.CellAppearance = appearance12;
            this.cbbExpenditureAcount.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbExpenditureAcount.DisplayLayout.Override.CellPadding = 0;
            appearance13.BackColor = System.Drawing.SystemColors.Control;
            appearance13.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance13.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance13.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbExpenditureAcount.DisplayLayout.Override.GroupByRowAppearance = appearance13;
            appearance14.TextHAlignAsString = "Left";
            this.cbbExpenditureAcount.DisplayLayout.Override.HeaderAppearance = appearance14;
            this.cbbExpenditureAcount.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbExpenditureAcount.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            appearance15.BorderColor = System.Drawing.Color.Silver;
            this.cbbExpenditureAcount.DisplayLayout.Override.RowAppearance = appearance15;
            this.cbbExpenditureAcount.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbExpenditureAcount.DisplayLayout.Override.TemplateAddRowAppearance = appearance16;
            this.cbbExpenditureAcount.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbExpenditureAcount.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbExpenditureAcount.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbExpenditureAcount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbExpenditureAcount.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbExpenditureAcount.Location = new System.Drawing.Point(380, 87);
            this.cbbExpenditureAcount.Name = "cbbExpenditureAcount";
            this.cbbExpenditureAcount.NullText = "<Xin chọn>";
            this.cbbExpenditureAcount.Size = new System.Drawing.Size(149, 22);
            this.cbbExpenditureAcount.TabIndex = 9;
            // 
            // ultraLabel9
            // 
            appearance17.BackColor = System.Drawing.Color.Transparent;
            appearance17.TextHAlignAsString = "Left";
            appearance17.TextVAlignAsString = "Middle";
            this.ultraLabel9.Appearance = appearance17;
            this.ultraLabel9.Location = new System.Drawing.Point(278, 59);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(91, 22);
            this.ultraLabel9.TabIndex = 18;
            this.ultraLabel9.Text = "TK Khấu hao";
            // 
            // cbbDepreciationAccount
            // 
            this.cbbDepreciationAccount.AutoSize = false;
            appearance18.BackColor = System.Drawing.SystemColors.Window;
            appearance18.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbDepreciationAccount.DisplayLayout.Appearance = appearance18;
            this.cbbDepreciationAccount.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbDepreciationAccount.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance19.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance19.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance19.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance19.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbDepreciationAccount.DisplayLayout.GroupByBox.Appearance = appearance19;
            appearance20.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbDepreciationAccount.DisplayLayout.GroupByBox.BandLabelAppearance = appearance20;
            this.cbbDepreciationAccount.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance21.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance21.BackColor2 = System.Drawing.SystemColors.Control;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbDepreciationAccount.DisplayLayout.GroupByBox.PromptAppearance = appearance21;
            this.cbbDepreciationAccount.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbDepreciationAccount.DisplayLayout.MaxRowScrollRegions = 1;
            appearance22.BackColor = System.Drawing.SystemColors.Window;
            appearance22.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbDepreciationAccount.DisplayLayout.Override.ActiveCellAppearance = appearance22;
            appearance23.BackColor = System.Drawing.SystemColors.Highlight;
            appearance23.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbDepreciationAccount.DisplayLayout.Override.ActiveRowAppearance = appearance23;
            this.cbbDepreciationAccount.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbDepreciationAccount.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            this.cbbDepreciationAccount.DisplayLayout.Override.CardAreaAppearance = appearance24;
            appearance25.BorderColor = System.Drawing.Color.Silver;
            appearance25.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbDepreciationAccount.DisplayLayout.Override.CellAppearance = appearance25;
            this.cbbDepreciationAccount.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbDepreciationAccount.DisplayLayout.Override.CellPadding = 0;
            appearance26.BackColor = System.Drawing.SystemColors.Control;
            appearance26.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance26.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance26.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbDepreciationAccount.DisplayLayout.Override.GroupByRowAppearance = appearance26;
            appearance27.TextHAlignAsString = "Left";
            this.cbbDepreciationAccount.DisplayLayout.Override.HeaderAppearance = appearance27;
            this.cbbDepreciationAccount.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbDepreciationAccount.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance28.BackColor = System.Drawing.SystemColors.Window;
            appearance28.BorderColor = System.Drawing.Color.Silver;
            this.cbbDepreciationAccount.DisplayLayout.Override.RowAppearance = appearance28;
            this.cbbDepreciationAccount.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance29.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbDepreciationAccount.DisplayLayout.Override.TemplateAddRowAppearance = appearance29;
            this.cbbDepreciationAccount.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbDepreciationAccount.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbDepreciationAccount.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbDepreciationAccount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbDepreciationAccount.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbDepreciationAccount.Location = new System.Drawing.Point(380, 59);
            this.cbbDepreciationAccount.Name = "cbbDepreciationAccount";
            this.cbbDepreciationAccount.NullText = "<Xin Chọn>";
            this.cbbDepreciationAccount.Size = new System.Drawing.Size(149, 22);
            this.cbbDepreciationAccount.TabIndex = 8;
            // 
            // ultraLabel8
            // 
            appearance30.BackColor = System.Drawing.Color.Transparent;
            appearance30.TextHAlignAsString = "Left";
            appearance30.TextVAlignAsString = "Middle";
            this.ultraLabel8.Appearance = appearance30;
            this.ultraLabel8.Location = new System.Drawing.Point(278, 31);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(91, 22);
            this.ultraLabel8.TabIndex = 16;
            this.ultraLabel8.Text = "TK Nguyên giá";
            // 
            // cbbOriginalPriceAcount
            // 
            this.cbbOriginalPriceAcount.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.SuggestAppend;
            this.cbbOriginalPriceAcount.AutoSize = false;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            appearance31.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbOriginalPriceAcount.DisplayLayout.Appearance = appearance31;
            this.cbbOriginalPriceAcount.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbOriginalPriceAcount.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance32.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance32.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance32.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance32.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbOriginalPriceAcount.DisplayLayout.GroupByBox.Appearance = appearance32;
            appearance33.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbOriginalPriceAcount.DisplayLayout.GroupByBox.BandLabelAppearance = appearance33;
            this.cbbOriginalPriceAcount.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance34.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance34.BackColor2 = System.Drawing.SystemColors.Control;
            appearance34.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance34.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbOriginalPriceAcount.DisplayLayout.GroupByBox.PromptAppearance = appearance34;
            this.cbbOriginalPriceAcount.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbOriginalPriceAcount.DisplayLayout.MaxRowScrollRegions = 1;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            appearance35.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbOriginalPriceAcount.DisplayLayout.Override.ActiveCellAppearance = appearance35;
            appearance36.BackColor = System.Drawing.SystemColors.Highlight;
            appearance36.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbOriginalPriceAcount.DisplayLayout.Override.ActiveRowAppearance = appearance36;
            this.cbbOriginalPriceAcount.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbOriginalPriceAcount.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance37.BackColor = System.Drawing.SystemColors.Window;
            this.cbbOriginalPriceAcount.DisplayLayout.Override.CardAreaAppearance = appearance37;
            appearance38.BorderColor = System.Drawing.Color.Silver;
            appearance38.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbOriginalPriceAcount.DisplayLayout.Override.CellAppearance = appearance38;
            this.cbbOriginalPriceAcount.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbOriginalPriceAcount.DisplayLayout.Override.CellPadding = 0;
            appearance39.BackColor = System.Drawing.SystemColors.Control;
            appearance39.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance39.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance39.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance39.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbOriginalPriceAcount.DisplayLayout.Override.GroupByRowAppearance = appearance39;
            appearance40.TextHAlignAsString = "Left";
            this.cbbOriginalPriceAcount.DisplayLayout.Override.HeaderAppearance = appearance40;
            this.cbbOriginalPriceAcount.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbOriginalPriceAcount.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance41.BackColor = System.Drawing.SystemColors.Window;
            appearance41.BorderColor = System.Drawing.Color.Silver;
            this.cbbOriginalPriceAcount.DisplayLayout.Override.RowAppearance = appearance41;
            this.cbbOriginalPriceAcount.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance42.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbOriginalPriceAcount.DisplayLayout.Override.TemplateAddRowAppearance = appearance42;
            this.cbbOriginalPriceAcount.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbOriginalPriceAcount.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbOriginalPriceAcount.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbOriginalPriceAcount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbOriginalPriceAcount.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbOriginalPriceAcount.Location = new System.Drawing.Point(380, 31);
            this.cbbOriginalPriceAcount.Name = "cbbOriginalPriceAcount";
            this.cbbOriginalPriceAcount.NullText = "<Xin chọn>";
            this.cbbOriginalPriceAcount.Size = new System.Drawing.Size(149, 22);
            this.cbbOriginalPriceAcount.TabIndex = 6;
            // 
            // ultraLabel7
            // 
            appearance43.BackColor = System.Drawing.Color.Transparent;
            appearance43.TextHAlignAsString = "Left";
            appearance43.TextVAlignAsString = "Middle";
            this.ultraLabel7.Appearance = appearance43;
            this.ultraLabel7.Location = new System.Drawing.Point(9, 59);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(111, 22);
            this.ultraLabel7.TabIndex = 12;
            this.ultraLabel7.Text = "Tỷ lệ % KH(năm)";
            // 
            // ultraLabel6
            // 
            appearance44.BackColor = System.Drawing.Color.Transparent;
            appearance44.TextHAlignAsString = "Left";
            appearance44.TextVAlignAsString = "Middle";
            this.ultraLabel6.Appearance = appearance44;
            this.ultraLabel6.Location = new System.Drawing.Point(9, 31);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(111, 22);
            this.ultraLabel6.TabIndex = 10;
            this.ultraLabel6.Text = "Thời gian SD(năm)";
            // 
            // ultraGroupBox1
            // 
            appearance45.FontData.BoldAsString = "True";
            this.ultraGroupBox1.Appearance = appearance45;
            this.ultraGroupBox1.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox1.Controls.Add(this.txtFixedAssetCagoryName);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel5);
            this.ultraGroupBox1.Controls.Add(this.txtDescription);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel4);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel3);
            this.ultraGroupBox1.Controls.Add(this.cbbParentId);
            this.ultraGroupBox1.Controls.Add(this.txtFixedCategoryCode);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(3, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(535, 170);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtFixedAssetCagoryName
            // 
            this.txtFixedAssetCagoryName.AutoSize = false;
            this.txtFixedAssetCagoryName.Location = new System.Drawing.Point(123, 48);
            this.txtFixedAssetCagoryName.MaxLength = 512;
            this.txtFixedAssetCagoryName.Name = "txtFixedAssetCagoryName";
            this.txtFixedAssetCagoryName.Size = new System.Drawing.Size(406, 22);
            this.txtFixedAssetCagoryName.TabIndex = 2;
            // 
            // ultraLabel5
            // 
            appearance46.BackColor = System.Drawing.Color.Transparent;
            appearance46.TextHAlignAsString = "Left";
            appearance46.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance46;
            this.ultraLabel5.Location = new System.Drawing.Point(6, 104);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(111, 22);
            this.ultraLabel5.TabIndex = 9;
            this.ultraLabel5.Text = "Diễn giải";
            // 
            // txtDescription
            // 
            this.txtDescription.AutoSize = false;
            this.txtDescription.Location = new System.Drawing.Point(123, 104);
            this.txtDescription.MaxLength = 512;
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(406, 59);
            this.txtDescription.TabIndex = 4;
            // 
            // ultraLabel4
            // 
            appearance47.BackColor = System.Drawing.Color.Transparent;
            appearance47.TextHAlignAsString = "Left";
            appearance47.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance47;
            this.ultraLabel4.Location = new System.Drawing.Point(6, 48);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(111, 22);
            this.ultraLabel4.TabIndex = 7;
            this.ultraLabel4.Text = "Tên loại TSCĐ (*)";
            // 
            // ultraLabel3
            // 
            appearance48.BackColor = System.Drawing.Color.Transparent;
            appearance48.TextHAlignAsString = "Left";
            appearance48.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance48;
            this.ultraLabel3.Location = new System.Drawing.Point(6, 76);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(111, 22);
            this.ultraLabel3.TabIndex = 5;
            this.ultraLabel3.Text = "Thuộc loại";
            // 
            // cbbParentId
            // 
            this.cbbParentId.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.SuggestAppend;
            this.cbbParentId.AutoSize = false;
            appearance49.BackColor = System.Drawing.SystemColors.Window;
            appearance49.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbParentId.DisplayLayout.Appearance = appearance49;
            this.cbbParentId.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbParentId.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance50.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance50.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance50.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance50.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbParentId.DisplayLayout.GroupByBox.Appearance = appearance50;
            appearance51.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbParentId.DisplayLayout.GroupByBox.BandLabelAppearance = appearance51;
            this.cbbParentId.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance52.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance52.BackColor2 = System.Drawing.SystemColors.Control;
            appearance52.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance52.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbParentId.DisplayLayout.GroupByBox.PromptAppearance = appearance52;
            this.cbbParentId.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbParentId.DisplayLayout.MaxRowScrollRegions = 1;
            appearance53.BackColor = System.Drawing.SystemColors.Window;
            appearance53.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbParentId.DisplayLayout.Override.ActiveCellAppearance = appearance53;
            appearance54.BackColor = System.Drawing.SystemColors.Highlight;
            appearance54.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbParentId.DisplayLayout.Override.ActiveRowAppearance = appearance54;
            this.cbbParentId.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbParentId.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance55.BackColor = System.Drawing.SystemColors.Window;
            this.cbbParentId.DisplayLayout.Override.CardAreaAppearance = appearance55;
            appearance56.BorderColor = System.Drawing.Color.Silver;
            appearance56.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbParentId.DisplayLayout.Override.CellAppearance = appearance56;
            this.cbbParentId.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbParentId.DisplayLayout.Override.CellPadding = 0;
            appearance57.BackColor = System.Drawing.SystemColors.Control;
            appearance57.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance57.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance57.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance57.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbParentId.DisplayLayout.Override.GroupByRowAppearance = appearance57;
            appearance58.TextHAlignAsString = "Left";
            this.cbbParentId.DisplayLayout.Override.HeaderAppearance = appearance58;
            this.cbbParentId.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbParentId.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance59.BackColor = System.Drawing.SystemColors.Window;
            appearance59.BorderColor = System.Drawing.Color.Silver;
            this.cbbParentId.DisplayLayout.Override.RowAppearance = appearance59;
            this.cbbParentId.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance60.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbParentId.DisplayLayout.Override.TemplateAddRowAppearance = appearance60;
            this.cbbParentId.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbParentId.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbParentId.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbParentId.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbParentId.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbParentId.Location = new System.Drawing.Point(123, 76);
            this.cbbParentId.Name = "cbbParentId";
            this.cbbParentId.NullText = "<Chọn danh mục TSCĐ cha>";
            this.cbbParentId.Size = new System.Drawing.Size(406, 22);
            this.cbbParentId.TabIndex = 3;
            // 
            // txtFixedCategoryCode
            // 
            this.txtFixedCategoryCode.AutoSize = false;
            this.txtFixedCategoryCode.Location = new System.Drawing.Point(123, 20);
            this.txtFixedCategoryCode.MaxLength = 25;
            this.txtFixedCategoryCode.Name = "txtFixedCategoryCode";
            this.txtFixedCategoryCode.Size = new System.Drawing.Size(406, 22);
            this.txtFixedCategoryCode.TabIndex = 1;
            // 
            // ultraLabel2
            // 
            appearance61.BackColor = System.Drawing.Color.Transparent;
            appearance61.TextHAlignAsString = "Left";
            appearance61.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance61;
            this.ultraLabel2.Location = new System.Drawing.Point(6, 20);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(111, 22);
            this.ultraLabel2.TabIndex = 2;
            this.ultraLabel2.Text = "Mã loại TSCĐ (*)";
            // 
            // ultraPanel2
            // 
            // 
            // ultraPanel2.ClientArea
            // 
            this.ultraPanel2.ClientArea.Controls.Add(this.btnClose);
            this.ultraPanel2.ClientArea.Controls.Add(this.btnSave);
            this.ultraPanel2.ClientArea.Controls.Add(this.chkIsActive);
            this.ultraPanel2.Location = new System.Drawing.Point(4, 305);
            this.ultraPanel2.Name = "ultraPanel2";
            this.ultraPanel2.Size = new System.Drawing.Size(541, 52);
            this.ultraPanel2.TabIndex = 1;
            // 
            // btnClose
            // 
            appearance62.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance62;
            this.btnClose.Location = new System.Drawing.Point(456, 11);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 12;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click_1);
            // 
            // btnSave
            // 
            appearance63.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance63;
            this.btnSave.Location = new System.Drawing.Point(358, 11);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 11;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click_1);
            // 
            // chkIsActive
            // 
            appearance64.TextVAlignAsString = "Middle";
            this.chkIsActive.Appearance = appearance64;
            this.chkIsActive.Checked = true;
            this.chkIsActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIsActive.Location = new System.Drawing.Point(3, 15);
            this.chkIsActive.Name = "chkIsActive";
            this.chkIsActive.Size = new System.Drawing.Size(183, 22);
            this.chkIsActive.TabIndex = 10;
            this.chkIsActive.Text = "Hoạt động";
            this.chkIsActive.ValidateCheckState += new Infragistics.Win.CheckEditor.ValidateCheckStateHandler(this.chkIsActive_ValidateCheckState);
            // 
            // FFixedAssetCategoryDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(547, 358);
            this.Controls.Add(this.ultraPanel2);
            this.Controls.Add(this.ultraPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FFixedAssetCategoryDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Chi tiết danh mục loại TSCĐ";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FFixedAssetCategoryDetail_FormClosed);
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbExpenditureAcount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDepreciationAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbOriginalPriceAcount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtFixedAssetCagoryName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbParentId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFixedCategoryCode)).EndInit();
            this.ultraPanel2.ClientArea.ResumeLayout(false);
            this.ultraPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraPanel ultraPanel2;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbParentId;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtFixedCategoryCode;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDescription;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbExpenditureAcount;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbDepreciationAccount;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbOriginalPriceAcount;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtFixedAssetCagoryName;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit nmrDepreciationRate;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsActive;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit nmrUsedTime;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraButton btnClose;
    }
}