﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;

namespace Accounting
{
    public partial class FFixedAssetCategoryDetail : DialogForm
    {

        #region khai báo
        private readonly IFixedAssetCategoryService _IFixxAssetCategoryService;
        private readonly IAccountService _IAccountService;
        FixedAssetCategory _Select = new FixedAssetCategory();
        public static string FixedCategoryCode;
        public static string OriginalPriceAccount;
        public static string ExpenditureAccount;
        public static string DepreciationAccount;
        bool checkActive = true;



        bool IsAdd = true;
        public static bool isClose = true;
        #endregion


        #region khởi tạo
        public FFixedAssetCategoryDetail()
        {//cho thêm
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion
            this.Text = "Thêm mới Loại tài sản cố định";
            this.MaximizeBox = false;
            #region Thiết lập ban đầu cho Form
            //Khai báo các webservices
            _IFixxAssetCategoryService = IoC.Resolve<IFixedAssetCategoryService>();
            _IAccountService = IoC.Resolve<IAccountService>();
            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            InitializeGUI();

            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control

            #endregion
            this.chkIsActive.Visible = false;
        }

        public FFixedAssetCategoryDetail(FixedAssetCategory temp, bool edit)
        {// cho Sửa
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion
            this.MaximizeBox = false;
            #region Thiết lập ban đầu cho Form
            _Select = temp;
            IsAdd = !edit;
            txtFixedCategoryCode.Enabled = false;

            //Khai báo các webservices
            _IFixxAssetCategoryService = IoC.Resolve<IFixedAssetCategoryService>();
            _IAccountService = IoC.Resolve<IAccountService>();
            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion
            if (edit)
            {
                this.Text = "Sửa Loại tài sản cố định";
            }
            else this.Text = "Thêm mới Loại tài sản cố định";

            #region Fill dữ liệu Obj vào control

            ObjandGUI(temp, false);

            #endregion
            this.chkIsActive.Visible = edit;
        }

        private void InitializeGUI()
        {
            cbbDepreciationAccount.DataSource = _IAccountService.GetListAccountAccountNumberIsActiveWithUpperLever("214", true, 0);
            cbbDepreciationAccount.DisplayMember = "AccountNumber";
            Utils.ConfigGrid(cbbDepreciationAccount, ConstDatabase.Account_TableName);

            cbbExpenditureAcount.DataSource = _IAccountService.GetListAccountAccountNumberIsActiveWithUpperLever("154;241;631;642;632;811", true, 1);
            cbbExpenditureAcount.DisplayMember = "AccountNumber";
            Utils.ConfigGrid(cbbExpenditureAcount, ConstDatabase.Account_TableName);

            cbbOriginalPriceAcount.DataSource = _IAccountService.GetListAccountAccountNumberIsActiveWithUpperLever("211;217", true, 1);
            cbbOriginalPriceAcount.DisplayMember = "AccountNumber";
            Utils.ConfigGrid(cbbOriginalPriceAcount, ConstDatabase.Account_TableName);

            cbbParentId.DataSource = _IFixxAssetCategoryService.GetAll();
            cbbParentId.DisplayMember = "FixedAssetCategoryName";
            Utils.ConfigGrid(cbbParentId, ConstDatabase.FixedAssetCategory_TableName);
            nmrUsedTime.FormatNumberic(ConstDatabase.Format_Quantity);
            nmrDepreciationRate.FormatNumberic(ConstDatabase.Format_Coefficient);
        }
        #endregion


        #region Button Event
        private void btnSave_Click_1(object sender, EventArgs e)
        {

            #region Check Lỗi
            if (!CheckError()) return;
            #endregion

            //Gui to object
            #region Fill dữ liệu control vào obj

            // thuc hien trong tung thao tac (them moi/ cap nhat)
            #endregion

            #region Thao tác CSDL
            try
            {
                _IFixxAssetCategoryService.BeginTran();
                if (IsAdd) // them moi
                {
                    FixedAssetCategory temp = IsAdd ? new FixedAssetCategory() : _IFixxAssetCategoryService.Getbykey(_Select.ID);
                    temp = ObjandGUI(temp, true);

                    // set orderfixcode
                    FixedAssetCategory temp0 = (FixedAssetCategory)Utils.getSelectCbbItem(cbbParentId);
                    //List<string> lstOrderFixCodeChild = _IFixxAssetCategoryService.Query.Where(a => a.ParentID == temp.ParentID)
                    //    .Select(a => a.OrderFixCode).ToList();
                    List<string> lstOrderFixCodeChild =
                        _IFixxAssetCategoryService.GetListOrderFixCodeFixedAssetCategory(temp.ParentID);
                    string orderFixCodeParent = temp0 == null ? "" : temp0.OrderFixCode;
                    temp.OrderFixCode = Utils.GetOrderFixCode(lstOrderFixCodeChild, orderFixCodeParent);
                    temp.IsParentNode = false; // mac dinh cho insert
                    temp.Grade = temp0 == null ? 1 : temp0.Grade + 1; // quy tac lay gia tri cho grade
                    //========> input.OrderFixCode chưa xét

                    _IFixxAssetCategoryService.CreateNew(temp);
                    // thuc hien cap nhat lai isParentNode cho cha neu co
                    if (temp.ParentID != null)
                    {
                        FixedAssetCategory parent = _IFixxAssetCategoryService.Getbykey((Guid)temp.ParentID);
                        parent.IsParentNode = true;
                        _IFixxAssetCategoryService.Update(parent);
                    }

                }
                else // cap nhat
                {
                    //Lấy về đối tượng cần sửa
                    FixedAssetCategory temp = _IFixxAssetCategoryService.Getbykey(_Select.ID);
                    //Lưu đối tượng trước khi sửa
                    FixedAssetCategory tempOriginal = (FixedAssetCategory)Utils.CloneObject(temp);
                    //Lấy về đối tượng chọn trong combobox
                    FixedAssetCategory objParentCombox = (FixedAssetCategory)Utils.getSelectCbbItem(cbbParentId);
                    //List<FixedAssetCategory> CheckChirent = _IFixxAssetCategoryService.Query.Where(c => c.OrderFixCode.StartsWith(_Select.OrderFixCode)).ToList();
                    List<FixedAssetCategory> CheckChirent = _IFixxAssetCategoryService.GetListFixedAssetCategoryOrderFixCode(_Select.OrderFixCode);
                    if (chkIsActive.Checked)
                    {

                        if (CheckChirent.Count > 1 /*&& MSG.MessageBoxStand(resSystem.MSG_Catalog_FFixedAssetCategoryDetail, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes*/)
                        {

                            foreach (var item in CheckChirent)
                            {
                                item.IsActive = true;
                                FixedAssetCategory fas = (FixedAssetCategory)item;
                                _IFixxAssetCategoryService.Update(fas);
                            }

                        }


                    }
                    else
                    {


                        foreach (var item in CheckChirent)
                        {
                            item.IsActive = false;
                            FixedAssetCategory fas = (FixedAssetCategory)item;
                            _IFixxAssetCategoryService.Update(fas);
                        }
                    }
                    //Không thay đổi đối tượng cha trong combobox
                    if (((temp.ParentID == null) && (objParentCombox == null)) || ((temp.ParentID != null) && (objParentCombox != null) && (temp.ParentID == objParentCombox.ID)))
                    {
                        temp = ObjandGUI(temp, true);
                        _IFixxAssetCategoryService.Update(temp);
                    }
                    //Thay đổi đối tượng trong combobox
                    else
                    {
                        //Không cho phép chuyển ngược
                        if (objParentCombox != null)
                        {
                            //List<FixedAssetCategory> lstChildCheck =
                            //    _IFixxAssetCategoryService.Query.Where(p => p.OrderFixCode.StartsWith(tempOriginal.OrderFixCode)).ToList();
                            List<FixedAssetCategory> lstChildCheck = _IFixxAssetCategoryService.GetListFixedAssetCategoryOrderFixCode(tempOriginal.OrderFixCode);
                            foreach (var item in lstChildCheck)
                            {
                                if (objParentCombox.ID == item.ID)
                                {
                                    MSG.Error(string.Format(resSystem.MSG_Catalog6, temp.FixedAssetCategoryCode));
                                    return;
                                }
                            }
                        }
                        //Trường hợp có cha cũ
                        //Kiểm tra cha cũ có còn con ngoài con chuyển đi hay không
                        //Chuyển trạng thái IsParentNode Cha cũ nếu hết con
                        if (temp.ParentID != null)
                        {
                            FixedAssetCategory oldParent = _IFixxAssetCategoryService.Getbykey((Guid)temp.ParentID);
                            //int checkChildOldParent = _IFixxAssetCategoryService.Query.Count(p => p.ParentID == temp.ParentID);
                            int checkChildOldParent = _IFixxAssetCategoryService.CountListFixedAssetCategoryParentID(temp.ParentID);
                            if (checkChildOldParent <= 1)
                            {
                                oldParent.IsParentNode = false;
                            }
                            _IFixxAssetCategoryService.Update(oldParent);
                        }
                        //Trường hợp có cha mới
                        //Chuyển trạng IsParentNode cha mới
                        if (objParentCombox != null)
                        {
                            FixedAssetCategory newParent = _IFixxAssetCategoryService.Getbykey(objParentCombox.ID);
                            newParent.IsParentNode = true;
                            _IFixxAssetCategoryService.Update(newParent);

                            //List<FixedAssetCategory> listChildNewParent = _IFixxAssetCategoryService.Query.Where(
                            //    a => a.ParentID == objParentCombox.ID).ToList();
                            List<FixedAssetCategory> listChildNewParent =
                                _IFixxAssetCategoryService.GetListFixedAssetCategoryParentID(objParentCombox.ID);
                            //Tính lại OrderFixCode cho đối tượng chính
                            List<string> lstOfcChildNewParent = new List<string>();
                            foreach (var item in listChildNewParent)
                            {
                                lstOfcChildNewParent.Add(item.OrderFixCode);
                            }
                            string orderFixCodeParent = newParent.OrderFixCode;
                            temp.OrderFixCode = Utils.GetOrderFixCode(lstOfcChildNewParent, orderFixCodeParent);
                            //Tính lại bậc cho đối tượng chính
                            temp.Grade = newParent.Grade + 1;
                        }
                        //Trường hợp không có cha mới tức là chuyển đối tượng lên bậc 1
                        if (objParentCombox == null)
                        {
                            //Lấy về tất cả đối tượng bậc 1
                            List<int> lstOfcChildGradeNo1 = new List<int>();
                            //List<FixedAssetCategory> listChildGradeNo1 = _IFixxAssetCategoryService.Query.Where(
                            //    a => a.Grade == 1).ToList();
                            List<FixedAssetCategory> listChildGradeNo1 = _IFixxAssetCategoryService.GetListFixedAssetCategoryGrade(1);
                            foreach (var item in listChildGradeNo1)
                            {
                                lstOfcChildGradeNo1.Add(Convert.ToInt32(item.OrderFixCode));
                            }
                            //Tính lại OrderFixCode cho đối tượng chính
                            temp.OrderFixCode = (lstOfcChildGradeNo1.Max() + 1).ToString(CultureInfo.InvariantCulture);
                            //Tính lại bậc cho đối tượng chính
                            temp.Grade = 1;
                            temp = ObjandGUI(temp, true);
                            _IFixxAssetCategoryService.Update(temp);
                        }
                        //Xử lý các con của nó 
                        //Lấy về các con
                        //List<FixedAssetCategory> lstChild =
                        //    _IFixxAssetCategoryService.Query.Where(p => p.OrderFixCode.StartsWith(tempOriginal.OrderFixCode)).ToList();
                        List<FixedAssetCategory> lstChild = _IFixxAssetCategoryService.GetListFixedAssetCategoryOrderFixCode(tempOriginal.OrderFixCode);
                        foreach (var item in lstChild)
                        {
                            string tempOldFixCode = tempOriginal.OrderFixCode;
                            item.OrderFixCode = temp.OrderFixCode + item.OrderFixCode.Substring(tempOldFixCode.Length);
                            item.Grade = temp.Grade - tempOriginal.Grade + item.Grade;
                            _IFixxAssetCategoryService.Update(item);
                        }
                        temp = ObjandGUI(temp, true);



                        _IFixxAssetCategoryService.Update(temp);
                    }
                }

                _IFixxAssetCategoryService.CommitTran();
            }
            catch (Exception ex)
            {
                _IFixxAssetCategoryService.RolbackTran();
                MSG.Error(ex.StackTrace); return;
            }
            #endregion

            #region xử lý form, kết thúc form
            FixedCategoryCode = txtFixedAssetCagoryName.Text;
            OriginalPriceAccount = cbbOriginalPriceAcount.Text;
            ExpenditureAccount = cbbExpenditureAcount.Text;
            DepreciationAccount = cbbDepreciationAccount.Text;
            this.Close();
            isClose = false;
            #endregion
        }


        private void btnClose_Click_1(object sender, EventArgs e)
        {
            isClose = true;
            this.Close();
        }
        #endregion

        #region Utils
        /// <summary>
        /// get hoặc set một đối tượng vào giao diện xử lý
        /// </summary>
        /// <param name="input">đối tượng</param>
        /// <param name="isGet">True: get giá trị từ control gán vào đối tượng| False: set giá trị đối tượng vào control</param>
        /// <returns></returns>
        FixedAssetCategory ObjandGUI(FixedAssetCategory input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();  //Thêm mới

                //input.FixedAssetCategoryCode = txtFixedAssetCategoryCode.Text;
                //input.FixedAssetCategoryName = txtFixedAssetCategoryName.Text;
                //FixedAssetCategory temp0 = (FixedAssetCategory)Utils.getSelectCbbItem(cbbParentID);
                ////Utils.CloneObject(temp0);
                //if (temp0 == null) input.ParentID = null;
                //else input.ParentID = temp0.ID;
                //input.IsActive = chkIsActive.CheckState == CheckState.Checked ? false : true;

                decimal usedTime = 0;
                decimal.TryParse(nmrUsedTime.Value.ToString(), out usedTime);

                if (usedTime != 0)
                {
                    input.UsedTime = usedTime;
                }

                decimal depreciationRate = 0;
                decimal.TryParse(nmrDepreciationRate.Value.ToString(), out depreciationRate);
                if (depreciationRate != 0)
                {
                    input.DepreciationRate = depreciationRate;
                }
                if (!string.IsNullOrEmpty(cbbDepreciationAccount.Text))
                {
                    input.DepreciationAccount = ((Account)Utils.getSelectCbbItem(cbbDepreciationAccount)).AccountNumber;
                }
                if (!string.IsNullOrEmpty(cbbExpenditureAcount.Text))
                {
                    input.ExpenditureAccount = ((Account)Utils.getSelectCbbItem(cbbExpenditureAcount)).AccountNumber;
                }
                if (!string.IsNullOrEmpty(cbbOriginalPriceAcount.Text))
                {
                    input.OriginalPriceAccount = ((Account)Utils.getSelectCbbItem(cbbOriginalPriceAcount)).AccountNumber;
                }

                //Utils.CloneObject(temp0);
                FixedAssetCategory temp0 = (FixedAssetCategory)Utils.getSelectCbbItem(cbbParentId);
                //Utils.CloneObject(temp0);
                if (temp0 == null) input.ParentID = null;
                else input.ParentID = temp0.ID;
                input.FixedAssetCategoryCode = txtFixedCategoryCode.Text;
                input.FixedAssetCategoryName = txtFixedAssetCagoryName.Text;
                input.Description = txtDescription.Text;

                input.IsActive = chkIsActive.Checked;

            }
            else
            {

                txtDescription.Text = input.Description;

                txtFixedCategoryCode.Text = input.FixedAssetCategoryCode;
                if (IsAdd)
                {
                    txtFixedCategoryCode.Enabled = true;
                    foreach (var item in cbbParentId.Rows)
                    {
                        if ((item.ListObject as FixedAssetCategory).ID == input.ID) cbbParentId.SelectedRow = item;
                    }
                    txtFixedAssetCagoryName.Text = "";
                }
                else
                {
                    foreach (var item in cbbParentId.Rows)
                    {
                        if ((item.ListObject as FixedAssetCategory).ID == input.ParentID) cbbParentId.SelectedRow = item;
                    }
                    txtFixedAssetCagoryName.Text = input.FixedAssetCategoryName;
                }

                nmrUsedTime.Value = input.UsedTime;
                nmrDepreciationRate.Value = input.DepreciationRate;
                foreach (var item in cbbDepreciationAccount.Rows)
                {
                    if (!string.IsNullOrEmpty(input.DepreciationAccount))
                    {
                        if ((item.ListObject as Account).AccountNumber == input.DepreciationAccount) cbbDepreciationAccount.SelectedRow = item;
                    }


                }
                foreach (var item in cbbExpenditureAcount.Rows)
                {
                    if (!string.IsNullOrEmpty(input.ExpenditureAccount))
                    {
                        if ((item.ListObject as Account).AccountNumber == input.ExpenditureAccount) cbbExpenditureAcount.SelectedRow = item;
                    }
                }
                foreach (var item in cbbOriginalPriceAcount.Rows)
                {
                    if (!string.IsNullOrEmpty(input.OriginalPriceAccount))
                    {
                        if ((item.ListObject as Account).AccountNumber == input.OriginalPriceAccount) cbbOriginalPriceAcount.SelectedRow = item;
                    }
                }
                chkIsActive.CheckState = input.IsActive ? CheckState.Checked : CheckState.Unchecked;

            }


            return input;
        }

        bool CheckError()
        {
            bool kq = true;
            //Check Error Chung

            //Check Error Riêng từng Form
            if (string.IsNullOrEmpty(txtFixedCategoryCode.Text))
            {
                MSG.Error(string.Format(resSystem.MSG_Catalog9,"loại Tài sản cố định"));
                txtFixedCategoryCode.Focus();

                return false;
            }
            else if (string.IsNullOrEmpty(txtFixedAssetCagoryName.Text))
            {
                MSG.Error(string.Format(resSystem.MSG_Catalog10,"loại Tài sản cố định"));
                txtFixedAssetCagoryName.Focus();
                return false;
            }
            else if (string.IsNullOrEmpty(nmrUsedTime.Value.ToString()))
            {
                decimal usedTime = 0;
                decimal.TryParse(nmrUsedTime.Value.ToString(), out usedTime);
                if (usedTime < 0)
                {
                    MSG.Error(resSystem.MSG_Catalog_FFixedAssetCategoryDetail1);
                    nmrUsedTime.Focus();
                    return false;
                }
            }
            else if (string.IsNullOrEmpty(nmrDepreciationRate.Value.ToString()))
            {
                decimal depreciationRate = 0;
                decimal.TryParse(nmrDepreciationRate.Value.ToString(), out depreciationRate);
                if (depreciationRate < 0)
                {
                    MSG.Error(resSystem.MSG_Catalog_FFixedAssetCategoryDetail3);
                    nmrDepreciationRate.Focus();
                    return false;
                }
            }
            if (IsAdd)
            {
                foreach (var item in cbbParentId.Rows)
                {
                    if ((item.ListObject as FixedAssetCategory).FixedAssetCategoryCode.Equals(txtFixedCategoryCode.Text))
                    {
                        MSG.Error(string.Format(resSystem.MSG_Catalog_Account6,"loại Tài sản cố định"));
                        txtFixedCategoryCode.Focus();
                        return false;
                    }
                }
            }
            else
            {
                if (_Select.ParentID != null)
                {
                    Guid id = new Guid(_Select.ParentID.ToString());
                    FixedAssetCategory fac = _IFixxAssetCategoryService.Getbykey(id);
                    if (!fac.IsActive)
                    {
                        if (chkIsActive.Checked)
                        {
                            MSG.Error(string.Format(resSystem.MSG_Catalog_Tree2,"loại Tài sản cố định"));
                            chkIsActive.Focus();
                            return false;
                        }
                    }
                }
            }

            return kq;
        }
        #endregion

        private void chkIsActive_ValidateCheckState(object sender, Infragistics.Win.ValidateCheckStateEventArgs e)
        {
            checkActive = false;
        }

        private void FFixedAssetCategoryDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
