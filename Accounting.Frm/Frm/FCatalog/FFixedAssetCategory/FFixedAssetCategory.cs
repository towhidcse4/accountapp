﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinTree;
using System.Data;

namespace Accounting
{
    public partial class FFixedAssetCategory : CatalogBase
    {
        #region khai báo
        private readonly IFixedAssetCategoryService _IFixxAssetCategoryService;
        List<FixedAssetCategory> dsFixedAssetCategory = new List<FixedAssetCategory>();
        #endregion

        public FFixedAssetCategory()
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            ToolTip ToolTip1 = new ToolTip();
            ToolTip1.SetToolTip(btnAdd, "Thêm (Ctrl + N)");
            ToolTip ToolTip2 = new ToolTip();
            ToolTip2.SetToolTip(btnEdit, "Sửa (Ctrl + E)");
            ToolTip ToolTip3 = new ToolTip();
            ToolTip3.SetToolTip(btnDelete, "Xóa (Ctrl + D)");
            #endregion

            #region Thiết lập ban đầu cho Form
            _IFixxAssetCategoryService = IoC.Resolve<IFixedAssetCategoryService>();

            this.uTree.InitializeDataNode += new Infragistics.Win.UltraWinTree.InitializeDataNodeEventHandler(Utils.uTree_InitializeDataNode);
            this.uTree.AfterDataNodesCollectionPopulated += new Infragistics.Win.UltraWinTree.AfterDataNodesCollectionPopulatedEventHandler(Utils.uTree_AfterDataNodesCollectionPopulated);
            this.uTree.ColumnSetGenerated += new Infragistics.Win.UltraWinTree.ColumnSetGeneratedEventHandler(this.uTree_ColumnSetGenerated);
            this.uTree.MouseDown += new System.Windows.Forms.MouseEventHandler(this.uTree_MouseDown);
            this.uTree.DoubleClick += new System.EventHandler(this.uTree_DoubleClick);
            #endregion

            #region Tạo dữ liệu ban đầu
            LoadDuLieu();
            #endregion
        }
        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }
        private void LoadDuLieu(bool configTree)
        {
            WaitingFrm.StartWaiting();
            #region Lấy dữ liệu từ CSDL
            dsFixedAssetCategory = _IFixxAssetCategoryService.GetAll_OrderBy();
            _IFixxAssetCategoryService.UnbindSession(dsFixedAssetCategory);
            dsFixedAssetCategory = _IFixxAssetCategoryService.GetAll_OrderBy();
            #endregion

            #region Xử lý dữ liệu
            #endregion

            #region hiển thị và xử lý hiển thị
            DataSet ds = Utils.ToDataSet<FixedAssetCategory>(dsFixedAssetCategory, ConstDatabase.FixedAssetCategory_TableName);
            uTree.SetDataBinding(ds, ConstDatabase.FixedAssetCategory_TableName);

            if (configTree) ConfigTree(uTree);
            #endregion
            WaitingFrm.StopWaiting();
        }

        #region Nghiệp vụ
        #region ContentMenu
        private void tsmAdd_Click_1(object sender, EventArgs e)
        {
            AddFunction(false);
        }

        private void tsmEdit_Click_1(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tsmDelete_Click_1(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void tmsReLoad_Click_1(object sender, EventArgs e)
        {
            ResetFunction();
        }
        protected override void ResetFunction()
        {
            LoadDuLieu(false);
        }

        #endregion

        #region Button
        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddFunction(false);
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void btnDelete_Click_1(object sender, EventArgs e)
        {
            DeleteFunction();
        }
        #endregion

        /// <summary>
        /// Nghiệp vụ Add
        /// </summary>
        protected void AddFunction(bool isEdit)
        {
            if (uTree.SelectedNodes.Count > 0 && isEdit)
            {
                FixedAssetCategory temp = dsFixedAssetCategory.Single(k => k.ID == (new Guid(uTree.SelectedNodes[0].Text)));
                new FFixedAssetCategoryDetail(temp, false).ShowDialog(this);
                if (!FFixedAssetCategoryDetail.isClose) LoadDuLieu();
            }
            else
            {
                new FFixedAssetCategoryDetail().ShowDialog(this);
                if (!FFixedAssetCategoryDetail.isClose) LoadDuLieu();
            }
        }

        /// <summary>
        /// Nghiệp vụ Edit
        /// </summary>
        protected override void EditFunction()
        {
            if (uTree.SelectedNodes.Count == 0 && uTree.ActiveNode != null)
            {
                uTree.ActiveNode.Selected = true;
            }
            if (uTree.SelectedNodes.Count > 0)
            {
                FixedAssetCategory temp = dsFixedAssetCategory.Single(k => k.ID == (new Guid(uTree.SelectedNodes[0].Text)));
                new FFixedAssetCategoryDetail(temp, true).ShowDialog(this);
                if (!FFixedAssetCategoryDetail.isClose) LoadDuLieu();
            }
            else
                Accounting.TextMessage.MSG.Error(string.Format(Accounting.TextMessage.resSystem.MSG_Catalog3,"một loại TSCĐ"));
        }

        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        protected override void DeleteFunction()
        {
            if (uTree.SelectedNodes.Count == 0 && uTree.ActiveNode != null)
            {
                uTree.ActiveNode.Selected = true;
            }
            if (uTree.SelectedNodes.Count > 0)
            {
                FixedAssetCategory temp = dsFixedAssetCategory.Single(k => k.ID == (new Guid(uTree.SelectedNodes[0].Text)));
                if (MSG.Question(string.Format(resSystem.MSG_System_05, temp.FixedAssetCategoryCode)) == System.Windows.Forms.DialogResult.Yes)
                {
                    _IFixxAssetCategoryService.BeginTran();
                    //Kiểm tra xem đối tượng có con không nếu có con thì không được phép xóa
                    //List<FixedAssetCategory> lstChild =
                    //        _IFixxAssetCategoryService.Query.Where(p => p.ParentID == temp.ID).ToList();
                    List<FixedAssetCategory> lstChild = _IFixxAssetCategoryService.GetListFixedAssetCategoryParentID(temp.ID);
                    if (lstChild.Count > 0)
                    {
                        
                        MSG.Error(string.Format(resSystem.MSG_Catalog_Tree, "loại TSCĐ"));
                        return;
                    }
                    //Check xem cha còn có con khác không nếu không thì chuyển trạng thái IsParentNode = false
                    if (temp.ParentID != null)
                    {
                        FixedAssetCategory parent = _IFixxAssetCategoryService.Getbykey((Guid)temp.ParentID);
                        //int checkChildOldParent = _IFixxAssetCategoryService.Query.Count(p => p.ParentID == temp.ParentID);
                        int checkChildOldParent = _IFixxAssetCategoryService.CountListFixedAssetCategoryParentID(temp.ParentID);
                        if (checkChildOldParent <= 1)
                        {
                            //parent.IsParentNode = false;
                        }
                        _IFixxAssetCategoryService.Update(parent);
                        _IFixxAssetCategoryService.Delete(temp);
                    }
                    if (temp.ParentID == null)
                    {
                        _IFixxAssetCategoryService.Delete(temp);
                    }
                    _IFixxAssetCategoryService.CommitTran();
                    Utils.ClearCacheByType<FixedAssetCategory>();
                    LoadDuLieu();
                }
            }
            else
                MSG.Error(string.Format(resSystem.MSG_Catalog2,"một loại TSCĐ"));
        }
        #endregion

        #region Event
        private void uTree_MouseDown(object sender, MouseEventArgs e)
        {//khi click chuột phải hiện content menu
            Utils.uGrid_MouseDown(sender, e, cms4Grid);
        }

        private void uTree_DoubleClick(object sender, EventArgs e)
        {//click đúp chuột để sửa
            EditFunction();
        }

        private void uTree_ColumnSetGenerated(object sender, ColumnSetGeneratedEventArgs e)
        {
            Utils.uTree_ColumnSetGenerated(sender, e, ConstDatabase.FixedAssetCategory_TableName);
        }
        #endregion

        #region Utils
        void ConfigTree(Infragistics.Win.UltraWinTree.UltraTree ultraTree)
        {//hàm chung
            Utils.ConfigTree(ultraTree, TextMessage.ConstDatabase.FixedAssetCategory_TableName);
        }

        #endregion

        private void FFixedAssetCategory_FormClosing(object sender, FormClosingEventArgs e)
        {
            uTree = null;
        }

        private void FFixedAssetCategory_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}