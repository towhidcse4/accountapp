﻿namespace Accounting
{
    partial class FBankDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            this.chkIsActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.imgIcon = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtDescription = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblDescription = new Infragistics.Win.Misc.UltraLabel();
            this.txtAddress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblAddress = new Infragistics.Win.Misc.UltraLabel();
            this.txtBankNameRepresent = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblBankNameRepresent = new Infragistics.Win.Misc.UltraLabel();
            this.txtBankName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblBankName = new Infragistics.Win.Misc.UltraLabel();
            this.txtBankCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblBankCode = new Infragistics.Win.Misc.UltraLabel();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankNameRepresent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankCode)).BeginInit();
            this.SuspendLayout();
            // 
            // chkIsActive
            // 
            appearance1.TextVAlignAsString = "Middle";
            this.chkIsActive.Appearance = appearance1;
            this.chkIsActive.BackColor = System.Drawing.Color.Transparent;
            this.chkIsActive.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsActive.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkIsActive.Checked = true;
            this.chkIsActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIsActive.Location = new System.Drawing.Point(9, 249);
            this.chkIsActive.Name = "chkIsActive";
            this.chkIsActive.Size = new System.Drawing.Size(103, 22);
            this.chkIsActive.TabIndex = 13;
            this.chkIsActive.Text = "Hoạt động";
            // 
            // ultraGroupBox1
            // 
            appearance2.TextVAlignAsString = "Middle";
            this.ultraGroupBox1.Appearance = appearance2;
            this.ultraGroupBox1.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox1.Controls.Add(this.imgIcon);
            this.ultraGroupBox1.Controls.Add(this.txtDescription);
            this.ultraGroupBox1.Controls.Add(this.lblDescription);
            this.ultraGroupBox1.Controls.Add(this.txtAddress);
            this.ultraGroupBox1.Controls.Add(this.lblAddress);
            this.ultraGroupBox1.Controls.Add(this.txtBankNameRepresent);
            this.ultraGroupBox1.Controls.Add(this.lblBankNameRepresent);
            this.ultraGroupBox1.Controls.Add(this.txtBankName);
            this.ultraGroupBox1.Controls.Add(this.lblBankName);
            this.ultraGroupBox1.Controls.Add(this.txtBankCode);
            this.ultraGroupBox1.Controls.Add(this.lblBankCode);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            appearance11.FontData.BoldAsString = "True";
            this.ultraGroupBox1.HeaderAppearance = appearance11;
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(525, 230);
            this.ultraGroupBox1.TabIndex = 26;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // imgIcon
            // 
            appearance3.Image = global::Accounting.Properties.Resources._1437136799_add;
            this.imgIcon.Appearance = appearance3;
            this.imgIcon.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            appearance4.Image = global::Accounting.Properties.Resources._1437136799_add;
            this.imgIcon.ContentAreaAppearance = appearance4;
            appearance5.Image = global::Accounting.Properties.Resources._1437136799_add;
            this.imgIcon.HeaderAppearance = appearance5;
            this.imgIcon.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOutsideBorder;
            this.imgIcon.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.imgIcon.Location = new System.Drawing.Point(402, 27);
            this.imgIcon.Name = "imgIcon";
            this.imgIcon.Size = new System.Drawing.Size(104, 82);
            this.imgIcon.TabIndex = 30;
            this.imgIcon.Text = "Biểu tượng";
            this.imgIcon.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            this.imgIcon.Click += new System.EventHandler(this.imgIcon_Click);
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(159, 147);
            this.txtDescription.MaxLength = 512;
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Scrollbars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDescription.Size = new System.Drawing.Size(347, 71);
            this.txtDescription.TabIndex = 29;
            // 
            // lblDescription
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextVAlignAsString = "Middle";
            this.lblDescription.Appearance = appearance6;
            this.lblDescription.Location = new System.Drawing.Point(9, 147);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(113, 22);
            this.lblDescription.TabIndex = 28;
            this.lblDescription.Text = "Diễn giải";
            // 
            // txtAddress
            // 
            this.txtAddress.AutoSize = false;
            this.txtAddress.Location = new System.Drawing.Point(159, 117);
            this.txtAddress.MaxLength = 512;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(347, 22);
            this.txtAddress.TabIndex = 27;
            // 
            // lblAddress
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.TextVAlignAsString = "Middle";
            this.lblAddress.Appearance = appearance7;
            this.lblAddress.Location = new System.Drawing.Point(9, 117);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(148, 22);
            this.lblAddress.TabIndex = 26;
            this.lblAddress.Text = "Địa chỉ ngân hàng";
            // 
            // txtBankNameRepresent
            // 
            this.txtBankNameRepresent.AutoSize = false;
            this.txtBankNameRepresent.Location = new System.Drawing.Point(159, 87);
            this.txtBankNameRepresent.MaxLength = 512;
            this.txtBankNameRepresent.Name = "txtBankNameRepresent";
            this.txtBankNameRepresent.Size = new System.Drawing.Size(237, 22);
            this.txtBankNameRepresent.TabIndex = 25;
            // 
            // lblBankNameRepresent
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.TextVAlignAsString = "Middle";
            this.lblBankNameRepresent.Appearance = appearance8;
            this.lblBankNameRepresent.Location = new System.Drawing.Point(9, 87);
            this.lblBankNameRepresent.Name = "lblBankNameRepresent";
            this.lblBankNameRepresent.Size = new System.Drawing.Size(147, 22);
            this.lblBankNameRepresent.TabIndex = 24;
            this.lblBankNameRepresent.Text = "Tên tiếng Anh";
            // 
            // txtBankName
            // 
            this.txtBankName.AutoSize = false;
            this.txtBankName.Location = new System.Drawing.Point(159, 57);
            this.txtBankName.MaxLength = 512;
            this.txtBankName.Name = "txtBankName";
            this.txtBankName.Size = new System.Drawing.Size(237, 22);
            this.txtBankName.TabIndex = 23;
            // 
            // lblBankName
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.TextVAlignAsString = "Middle";
            this.lblBankName.Appearance = appearance9;
            this.lblBankName.Location = new System.Drawing.Point(9, 57);
            this.lblBankName.Name = "lblBankName";
            this.lblBankName.Size = new System.Drawing.Size(148, 22);
            this.lblBankName.TabIndex = 22;
            this.lblBankName.Text = "Tên ngân hàng (*)";
            // 
            // txtBankCode
            // 
            this.txtBankCode.AutoSize = false;
            this.txtBankCode.Location = new System.Drawing.Point(159, 27);
            this.txtBankCode.MaxLength = 25;
            this.txtBankCode.Name = "txtBankCode";
            this.txtBankCode.Size = new System.Drawing.Size(237, 22);
            this.txtBankCode.TabIndex = 5;
            // 
            // lblBankCode
            // 
            appearance10.BackColor = System.Drawing.Color.Transparent;
            appearance10.TextVAlignAsString = "Middle";
            this.lblBankCode.Appearance = appearance10;
            this.lblBankCode.Location = new System.Drawing.Point(9, 27);
            this.lblBankCode.Name = "lblBankCode";
            this.lblBankCode.Size = new System.Drawing.Size(126, 22);
            this.lblBankCode.TabIndex = 0;
            this.lblBankCode.Text = "Mã ngân hàng (*)";
            // 
            // btnClose
            // 
            appearance12.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance12;
            this.btnClose.Location = new System.Drawing.Point(431, 245);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 64;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            appearance13.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance13;
            this.btnSave.Location = new System.Drawing.Point(348, 245);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 63;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // FBankDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(525, 280);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.ultraGroupBox1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.chkIsActive);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FBankDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Chi tiết Ngân hàng";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FBankDetail_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankNameRepresent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankCode)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsActive;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtBankCode;
        private Infragistics.Win.Misc.UltraLabel lblBankCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtBankName;
        private Infragistics.Win.Misc.UltraLabel lblBankName;
        private Infragistics.Win.Misc.UltraGroupBox imgIcon;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDescription;
        private Infragistics.Win.Misc.UltraLabel lblDescription;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAddress;
        private Infragistics.Win.Misc.UltraLabel lblAddress;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtBankNameRepresent;
        private Infragistics.Win.Misc.UltraLabel lblBankNameRepresent;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnSave;
    }
}
