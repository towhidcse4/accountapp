﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using System.Drawing;

namespace Accounting
{
    public partial class FBankDetail : DialogForm //UserControl
    {
        #region khai báo
        private readonly IBankService _IBankService;

        Bank _Select = new Bank();

        bool Them = true;
        public static bool IsClose = true;
        public static string bankcode;
        #endregion

        #region khởi tạo
        public FBankDetail()
        {//Thêm
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            this.Text = "Thêm mới Ngân hàng";
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.CenterToParent();
            chkIsActive.CheckState = CheckState.Checked;
            chkIsActive.Visible = false;
            #endregion

            #region Thiết lập ban đầu cho Form
            //Khai báo các webservices
            _IBankService = IoC.Resolve<IBankService>();

            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion
            WaitingFrm.StopWaiting();
        }

        public FBankDetail(Bank temp)
        {//Sửa
            #region Khởi tạo giá trị mặc định của Form

            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.CenterToParent();
            #endregion
            this.Text = "Sửa Ngân hàng";
            #region Thiết lập ban đầu cho Form
            _Select = temp;
            Them = false;
            txtBankCode.Enabled = false;

            //Khai báo các webservices
            _IBankService = IoC.Resolve<IBankService>();

            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion

            #region Fill dữ liệu Obj vào control
            ObjandGUI(temp, false);
            #endregion
            WaitingFrm.StopWaiting();
        }

        private void InitializeGUI()
        {
            //
        }
        #endregion

        #region Button Event
        /// <summary>
        /// Sự kiện nút Lưu lại
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion

            //Gui to object
            #region Fill dữ liệu control vào obj
            Bank temp = Them ? new Bank() : _IBankService.Getbykey(_Select.ID);
            temp = ObjandGUI(temp, true);
            #endregion

            #region Thao tác CSDL
            _IBankService.BeginTran();
            if (Them) _IBankService.CreateNew(temp);
            else _IBankService.Update(temp);
            _IBankService.CommitTran();
            #endregion

            #region xử lý form, kết thúc form
            bankcode = txtBankCode.Text;
            this.Close();

            IsClose = false;
            #endregion
        }

        /// <summary>
        /// Sự kiện nút đóng
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            IsClose = true;
            DialogResult = DialogResult.Cancel;
            Dispose(true);
            bankcode = null;
            Close();
        }
        #endregion

        #region Utils
        /// <summary>
        /// get hoặc set một đối tượng vào giao diện xử lý
        /// </summary>
        /// <param name="input">đối tượng</param>
        /// <param name="isGet">True: get giá trị từ control gán vào đối tượng| False: set giá trị đối tượng vào control</param>
        /// <returns></returns>
        Bank ObjandGUI(Bank input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();  //Thêm mới

                input.BankCode = txtBankCode.Text;
                input.BankName = txtBankName.Text;
                input.BankNameRepresent = txtBankNameRepresent.Text;
                input.Address = txtAddress.Text;
                input.Description = txtDescription.Text;
                input.Icon = imgIcon.ContentAreaAppearance.ImageBackground == null ? null : imgIcon.ContentAreaAppearance.ImageBackground.ImageToByteArray();
                input.IsActive = chkIsActive.Checked;
            }
            else
            {
                txtBankCode.Text = input.BankCode;
                txtBankName.Text = input.BankName;
                txtBankNameRepresent.Text = input.BankNameRepresent;
                txtAddress.Text = input.Address;
                txtDescription.Text = input.Description;

                if (input.Icon != null) imgIcon.ContentAreaAppearance.ImageBackground = Utils.ByteArrayToImage(input.Icon);
                chkIsActive.Checked = input.IsActive;
            }
            return input;
        }

        bool CheckError()
        {
            bool kq = true;
            //Check Error Chung

            //Check Error Riêng từng Form
            if (string.IsNullOrEmpty(txtBankCode.Text) || string.IsNullOrEmpty(txtBankName.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }
            //List<string> list = _IBankService.Query.Select(p => p.BankCode).ToList();
            List<string> list = _IBankService.GetListBankCode();

            foreach (var item in list)
            {
                if (item == txtBankCode.Text && Them)
                {
                    MSG.Warning(string.Format(resSystem.MSG_Catalog_Account6, item));
                    //MSG.Error("Mã : " + item + " Đã tồn tại ");
                    return false;
                }
            }

            return kq;
        }
        #endregion

        private void imgIcon_Click(object sender, EventArgs e)
        {
            var openFile = new OpenFileDialog
            {
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                Filter = "file hinh|*.jpg|all file|*.*"

            };


            var result = openFile.ShowDialog(this);
            if (result == DialogResult.OK || result == DialogResult.Yes)
            {
                //giới hạn dung lượng ảnh cho phép là 50 kb
                double fileSize = (double)new System.IO.FileInfo(openFile.FileName).Length / 1024;
                if (fileSize > 50)
                {
                    MSG.Error(resSystem.MSG_Catalog_FBankAccountDetail);
                    return;
                }
                //Kích thước ảnh cho phép không lớn hơn 128x128
                try
                {
                    Image temp = System.Drawing.Image.FromFile(openFile.FileName);
                    if (temp.Width > 128 || temp.Height > 128)
                    {
                        MSG.Error(resSystem.MSG_Catalog_FBankAccountDetail1);
                        return;
                    }
                    imgIcon.ContentAreaAppearance.ImageBackground = System.Drawing.Image.FromFile(openFile.FileName);
                }
                catch (Exception)
                {
                    MSG.Error(resSystem.MSG_Catalog_FBank);
                    return;
                }

            }
        }

        private void FBankDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
