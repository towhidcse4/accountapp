﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using Accounting.TextMessage;
using Accounting.Core.DAO;

namespace Accounting
{
    public partial class FBank : CatalogBase //UserControl
    {
        #region khai báo
        private readonly IBankService _IBankService;
        private readonly IBankAccountDetailService _IBankAccountDetailService;
        #endregion

        #region khởi tạo
        public FBank()
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            #endregion

            #region Thiết lập ban đầu cho Form
            _IBankService = IoC.Resolve<IBankService>();
            _IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            this.uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            #endregion

            #region Tạo dữ liệu ban đầu
            LoadDuLieu();
            #endregion
            ToolTip ToolTip1 = new ToolTip();
            ToolTip1.SetToolTip(btnAdd, "Thêm (Ctrl + N)");
            ToolTip ToolTip2 = new ToolTip();
            ToolTip2.SetToolTip(btnEdit, "Xem (Ctrl + E)");
            ToolTip ToolTip3 = new ToolTip();
            ToolTip3.SetToolTip(btnDelete, "Xóa (Ctrl + D)");
            if (uGrid.Rows.Count > 0) uGrid.Rows[0].Selected = true;
        }

        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }
        private void LoadDuLieu(bool configGrid)
        {
            WaitingFrm.StartWaiting();
            #region Lấy dữ liệu từ CSDL
            List<Bank> list = _IBankService.GetAll_OrderBy();
            _IBankService.UnbindSession(list);
            list = _IBankService.GetAll_OrderBy();
            #endregion

            #region Xử lý dữ liệu
            #endregion

            #region hiển thị và xử lý hiển thị
            uGrid.DataSource = list.ToArray();
            if (uGrid.Rows.Count > 0)
            {
                uGrid.Rows[0].Selected = true;
            }
            if (configGrid) ConfigGrid(uGrid);
            #endregion
            WaitingFrm.StopWaiting();
        }
        #endregion

        #region Nghiệp vụ
        #region ContentMenu
        private void tsmAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void tsmEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            ResetFunction();
        }
        protected override void ResetFunction()
        {
            LoadDuLieu(false);
        }
        #endregion

        #region Button
        private void btnAdd_Click(object sender, EventArgs e)
        {
            //ReportProcedureSDS clsSP = new ReportProcedureSDS();
            //List<Bank> lstBank = clsSP.Bank_SearchByCode("Techcombank");
            AddFunction();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }
        #endregion

        private void uGrid_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (e.Row.Index == -1) return;
            EditFunction();
        }

        /// <summary>
        /// Nghiệp vụ Add
        /// </summary>
        protected override void AddFunction()
        {
            new FBankDetail().ShowDialog(this);
            if (!FBankDetail.IsClose) LoadDuLieu();
        }

        /// <summary>
        /// Nghiệp vụ Edit
        /// </summary>
        protected override void EditFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                Bank temp = uGrid.Selected.Rows[0].ListObject as Bank;
                new FBankDetail(temp).ShowDialog(this);
                if (!FBankDetail.IsClose) LoadDuLieu();
            }
            else
                Accounting.TextMessage.MSG.Error(string.Format(Accounting.TextMessage.resSystem.MSG_Catalog3, "một Ngân hàng"));
        }

        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        protected override void DeleteFunction()
        {
            List<BankAccountDetail> list = _IBankAccountDetailService.GetAllOrderBy();
            //List<Bank> list2 = _IBankService.GetAll_OrderBy();
            bool kq = true;
            if (uGrid.Selected.Rows.Count > 0)
            {
                Bank temp = uGrid.Selected.Rows[0].ListObject as Bank;
                foreach (var item in list)
                {
                    if (temp.BankName == item.BankName)
                    {
                        kq = false;
                        break;
                    }
                }
                if (kq == true)
                {
                    if (MSG.Question(string.Format(resSystem.MSG_System_05, temp.BankCode)) == System.Windows.Forms.DialogResult.Yes)
                    {
                        _IBankService.BeginTran();
                        _IBankService.Delete(temp);
                        _IBankService.CommitTran();
                        Utils.ClearCacheByType<Bank>();
                        LoadDuLieu();
                    }
                }
                else MSG.Warning(resSystem.MSG_Catalog_FBank1);
            }

            else
                MSG.Error(resSystem.MSG_System_06);
        }
        #endregion

        #region Utils
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, TextMessage.ConstDatabase.Bank_TableName);
        }
        #endregion

        private void FBank_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGrid.ResetText();
            uGrid.ResetUpdateMode();
            uGrid.ResetExitEditModeOnLeave();
            uGrid.ResetRowUpdateCancelAction();
            uGrid.DataSource = null;
            uGrid.Layouts.Clear();
            uGrid.ResetLayouts();
            uGrid.ResetDisplayLayout();
            uGrid.Refresh();
            uGrid.ClearUndoHistory();
            uGrid.ClearXsdConstraints();
        }

        private void FBank_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
