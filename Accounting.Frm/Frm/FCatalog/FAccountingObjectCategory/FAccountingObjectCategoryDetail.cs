﻿using System;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using System.Collections.Generic;


namespace Accounting
{
    public partial class FAccountingObjectCategoryDetail : DialogForm //UserControl
    {
        #region khai báo
        private readonly IAccountingObjectCategoryService _IAccountingObjectCategoryService;

        AccountingObjectCategory _Select = new AccountingObjectCategory();

        bool Them = true;
        public static bool isClose = true;
        #endregion

        #region khởi tạo
        public FAccountingObjectCategoryDetail()
        {//Thêm
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            //Khai báo các webservices
            _IAccountingObjectCategoryService = IoC.Resolve<IAccountingObjectCategoryService>();
            this.Text = "Thêm mới ";
            chkIsActive.Visible = false;
            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion
        }

        public FAccountingObjectCategoryDetail(AccountingObjectCategory temp)
        {//Sửa
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _Select = temp;
            Them = false;
            txtAccountingObjectCategoryCode.Enabled = false;
            this.Text = "Sửa Loại khách hàng, Nhà cung cấp.";
            //Khai báo các webservices
            _IAccountingObjectCategoryService = IoC.Resolve<IAccountingObjectCategoryService>();

            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion

            #region Fill dữ liệu Obj vào control
            ObjandGUI(temp, false);
            #endregion
        }

        private void InitializeGUI()
        {
            //
        }
        #endregion

        #region Button Event
        /// <summary>
        /// Sự kiện nút Lưu lại
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click_1(object sender, EventArgs e)
        {
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion

            //Gui to object
            #region Fill dữ liệu control vào obj
            AccountingObjectCategory temp = Them ? new AccountingObjectCategory() : _IAccountingObjectCategoryService.Getbykey(_Select.ID);
            temp = ObjandGUI(temp, true);
            #endregion

            #region Thao tác CSDL

            _IAccountingObjectCategoryService.BeginTran();
            if (Them)
            {
                if (!CheckCode()) return;
                temp.IsSecurity = false;
                _IAccountingObjectCategoryService.CreateNew(temp);
            }
            else
            {
                if (!CheckError()) return;
                _IAccountingObjectCategoryService.Update(temp);
            }
            _IAccountingObjectCategoryService.CommitTran();

            #endregion

            #region xử lý form, kết thúc form
            this.Close();
            isClose = false;
            #endregion
        }

        /// <summary>
        /// Sự kiện nút đóng
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click_1(object sender, EventArgs e)
        {
            isClose = true;
            this.Close();
        }

        #endregion

        #region Utils
        /// <summary>
        /// get hoặc set một đối tượng vào giao diện xử lý
        /// </summary>
        /// <param name="input">đối tượng</param>
        /// <param name="isGet">True: get giá trị từ control gán vào đối tượng| False: set giá trị đối tượng vào control</param>
        /// <returns></returns>
        AccountingObjectCategory ObjandGUI(AccountingObjectCategory input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();  //Thêm mới

                input.AccountingObjectCategoryCode = txtAccountingObjectCategoryCode.Text;
                input.AccountingObjectCategoryName = txtAccountingObjectCategoryName.Text;
                input.IsActive = chkIsActive.Checked;
            }
            else
            {
                txtAccountingObjectCategoryCode.Text = input.AccountingObjectCategoryCode;
                txtAccountingObjectCategoryName.Text = input.AccountingObjectCategoryName;
                chkIsActive.Checked = input.IsActive;
            }
            return input;
        }

        //check error
        bool CheckError()
        {
            bool kq = true;
            if (string.IsNullOrEmpty(txtAccountingObjectCategoryCode.Text) || string.IsNullOrEmpty(txtAccountingObjectCategoryName.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }

            return kq;
        }

        bool CheckCode()
        {
            bool kq = true;
            if (string.IsNullOrEmpty(txtAccountingObjectCategoryCode.Text) || string.IsNullOrEmpty(txtAccountingObjectCategoryName.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }
            //check trùng code
            //List<string> lst = _IAccountingObjectCategoryService.Query.Select(c => c.AccountingObjectCategoryCode).ToList();
            List<string> lst = _IAccountingObjectCategoryService.GetAccountingObjectCategoryCode();//thành duy đã sửa chỗ này
            foreach (var x in lst)
            {
                if (txtAccountingObjectCategoryCode.Text.Equals(x))
                {
                    MSG.Warning(string.Format(resSystem.MSG_Catalog_Account6, "loại khách hàng, nhà cung cấp"));
                    return false;
                }
            }

            return kq;
        }



        #endregion

        private void FAccountingObjectCategoryDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
