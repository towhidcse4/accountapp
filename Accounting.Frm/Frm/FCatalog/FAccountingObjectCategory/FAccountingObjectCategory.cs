﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using Accounting.TextMessage;

namespace Accounting
{
    public partial class FAccountingObjectCategory : CatalogBase //UserControl
    {
        #region khai báo
        private readonly IAccountingObjectCategoryService _IAccountingObjectCategoryService;
        #endregion

        #region khởi tạo
        public FAccountingObjectCategory()
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            ToolTip ToolTip1 = new ToolTip();
            ToolTip1.SetToolTip(btnAdd, "Thêm (Ctrl + N)");
            ToolTip ToolTip2 = new ToolTip();
            ToolTip2.SetToolTip(btnEdit, "Sửa (Ctrl + E)");
            ToolTip ToolTip3 = new ToolTip();
            ToolTip3.SetToolTip(btnDelete, "Xóa (Ctrl + D)");
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            #endregion

            #region Thiết lập ban đầu cho Form
            _IAccountingObjectCategoryService = IoC.Resolve<IAccountingObjectCategoryService>();
            this.uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            #endregion

            #region Tạo dữ liệu ban đầu
            LoadDuLieu();
            #endregion
        }

        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }
        private void LoadDuLieu(bool configGrid)
        {
            WaitingFrm.StartWaiting();
            #region Lấy dữ liệu từ CSDL
            List<AccountingObjectCategory> list = _IAccountingObjectCategoryService.GetAll_OrderBy();
            _IAccountingObjectCategoryService.UnbindSession(list);
            list = _IAccountingObjectCategoryService.GetAll_OrderBy();
            #endregion

            #region Xử lý dữ liệu
            #endregion

            #region hiển thị và xử lý hiển thị
            uGrid.DataSource = list.ToArray();
            if (uGrid.Rows.Count > 0)
            {
                uGrid.Rows[0].Selected = true;
            }
            if (configGrid) ConfigGrid(uGrid);
            #endregion
            WaitingFrm.StopWaiting();
        }
        #endregion

        #region Nghiệp vụ
        #region ContentMenu

        private void tsmAdd_Click_1(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void tsmEdit_Click_1(object sender, EventArgs e)
        {
            EditFunction();
        }


        private void tsmDelete_Click_1(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void tmsReLoad_Click_1(object sender, EventArgs e)
        {
            ResetFunction();
        }
        protected override void ResetFunction()
        {
            LoadDuLieu(false);
        }
        #endregion

        #region Button
        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void btnDelete_Click_1(object sender, EventArgs e)
        {
            DeleteFunction();
        }
        #endregion

        private void uGrid_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (e.Row.Index == -1) return;
            EditFunction();
        }

        /// <summary>
        /// Nghiệp vụ Add
        /// </summary>
        protected override void AddFunction()
        {
            new FAccountingObjectCategoryDetail().ShowDialog(this);
            if (!FAccountingObjectCategoryDetail.isClose) LoadDuLieu();
        }

        /// <summary>
        /// Nghiệp vụ Edit
        /// </summary>
        protected override void EditFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                AccountingObjectCategory temp = uGrid.Selected.Rows[0].ListObject as AccountingObjectCategory;
                new FAccountingObjectCategoryDetail(temp).ShowDialog(this);
                if (!FAccountingObjectCategoryDetail.isClose) LoadDuLieu();
            }
            else
                MSG.Error(string.Format(resSystem.MSG_Catalog3, "danh mục"));
        }

        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        protected override void DeleteFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                AccountingObjectCategory temp = uGrid.Selected.Rows[0].ListObject as AccountingObjectCategory;
                if (MSG.Question(string.Format(resSystem.MSG_System_05, temp.AccountingObjectCategoryCode)) == System.Windows.Forms.DialogResult.Yes)
                {
                    _IAccountingObjectCategoryService.BeginTran();
                    _IAccountingObjectCategoryService.Delete(temp);
                    _IAccountingObjectCategoryService.CommitTran();
                    Utils.ClearCacheByType<AccountingObjectCategory>();
                    LoadDuLieu();
                }
            }
            else
                MSG.Error(string.Format(resSystem.MSG_Catalog2, "danh mục"));
        }
        #endregion

        #region Utils
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, TextMessage.ConstDatabase.AccountingObjectCategory_TableName);
        }


        #endregion

        private void FAccountingObjectCategory_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGrid.ResetText();
            uGrid.ResetUpdateMode();
            uGrid.ResetExitEditModeOnLeave();
            uGrid.ResetRowUpdateCancelAction();
            uGrid.DataSource = null;
            uGrid.Layouts.Clear();
            uGrid.ResetLayouts();
            uGrid.ResetDisplayLayout();
            uGrid.Refresh();
            uGrid.ClearUndoHistory();
            uGrid.ClearXsdConstraints();
        }

        private void FAccountingObjectCategory_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
