﻿namespace Accounting
{
    partial class FAccountingObjectCategoryDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.lblGroupName = new Infragistics.Win.Misc.UltraLabel();
            this.lblGroupCode = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectCategoryName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtAccountingObjectCategoryCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.chkIsActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectCategoryName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectCategoryCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            appearance1.FontData.BoldAsString = "True";
            this.ultraGroupBox1.Appearance = appearance1;
            this.ultraGroupBox1.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox1.ContentPadding.Bottom = 10;
            this.ultraGroupBox1.ContentPadding.Top = 10;
            this.ultraGroupBox1.Controls.Add(this.lblGroupName);
            this.ultraGroupBox1.Controls.Add(this.lblGroupCode);
            this.ultraGroupBox1.Controls.Add(this.txtAccountingObjectCategoryName);
            this.ultraGroupBox1.Controls.Add(this.txtAccountingObjectCategoryCode);
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(3, 5);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(388, 105);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // lblGroupName
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.TextVAlignAsString = "Middle";
            this.lblGroupName.Appearance = appearance2;
            this.lblGroupName.Location = new System.Drawing.Point(10, 64);
            this.lblGroupName.Name = "lblGroupName";
            this.lblGroupName.Size = new System.Drawing.Size(63, 22);
            this.lblGroupName.TabIndex = 0;
            this.lblGroupName.Text = "Tên loại(*)";
            // 
            // lblGroupCode
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextVAlignAsString = "Middle";
            this.lblGroupCode.Appearance = appearance3;
            this.lblGroupCode.Location = new System.Drawing.Point(10, 31);
            this.lblGroupCode.Name = "lblGroupCode";
            this.lblGroupCode.Size = new System.Drawing.Size(59, 22);
            this.lblGroupCode.TabIndex = 0;
            this.lblGroupCode.Text = "Mã loại(*)";
            // 
            // txtAccountingObjectCategoryName
            // 
            this.txtAccountingObjectCategoryName.AutoSize = false;
            this.txtAccountingObjectCategoryName.Location = new System.Drawing.Point(89, 64);
            this.txtAccountingObjectCategoryName.Name = "txtAccountingObjectCategoryName";
            this.txtAccountingObjectCategoryName.Size = new System.Drawing.Size(290, 22);
            this.txtAccountingObjectCategoryName.TabIndex = 2;
            // 
            // txtAccountingObjectCategoryCode
            // 
            this.txtAccountingObjectCategoryCode.AutoSize = false;
            this.txtAccountingObjectCategoryCode.Location = new System.Drawing.Point(89, 31);
            this.txtAccountingObjectCategoryCode.Name = "txtAccountingObjectCategoryCode";
            this.txtAccountingObjectCategoryCode.Size = new System.Drawing.Size(290, 22);
            this.txtAccountingObjectCategoryCode.TabIndex = 1;
            // 
            // chkIsActive
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.BackColor2 = System.Drawing.Color.Transparent;
            appearance4.BackColorDisabled = System.Drawing.Color.Transparent;
            appearance4.BackColorDisabled2 = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.chkIsActive.Appearance = appearance4;
            this.chkIsActive.BackColor = System.Drawing.Color.Transparent;
            this.chkIsActive.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsActive.Checked = true;
            this.chkIsActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIsActive.Location = new System.Drawing.Point(18, 120);
            this.chkIsActive.Name = "chkIsActive";
            this.chkIsActive.Size = new System.Drawing.Size(101, 22);
            this.chkIsActive.TabIndex = 3;
            this.chkIsActive.Text = "Hoạt động";
            // 
            // btnSave
            // 
            appearance5.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance5;
            this.btnSave.Location = new System.Drawing.Point(226, 116);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click_1);
            // 
            // btnClose
            // 
            appearance6.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance6;
            this.btnClose.Location = new System.Drawing.Point(307, 116);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 5;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click_1);
            // 
            // FAccountingObjectCategoryDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(394, 151);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.ultraGroupBox1);
            this.Controls.Add(this.chkIsActive);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FAccountingObjectCategoryDetail";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thêm nhóm mới...";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FAccountingObjectCategoryDetail_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectCategoryName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectCategoryCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraLabel lblGroupName;
        private Infragistics.Win.Misc.UltraLabel lblGroupCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectCategoryName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectCategoryCode;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsActive;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraButton btnClose;
    }
}
