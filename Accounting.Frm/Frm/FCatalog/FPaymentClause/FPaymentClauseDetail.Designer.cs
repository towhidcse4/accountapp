﻿namespace Accounting
{
    partial class FPaymentClauseDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.chkChiTiet = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.cbbChiTiet = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.txtPaymentClauseName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtPaymentClauseCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblPaymentClauseName = new Infragistics.Win.Misc.UltraLabel();
            this.lblPaymentClauseCode = new Infragistics.Win.Misc.UltraLabel();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.chkIsActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtDiscountDate = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.txtDueDate = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.txtOverdueInterestPercent = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtDiscountPercent = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.lblDiscountDate = new Infragistics.Win.Misc.UltraLabel();
            this.lblOverdueInterestPercent = new Infragistics.Win.Misc.UltraLabel();
            this.lblDiscountPercent = new Infragistics.Win.Misc.UltraLabel();
            this.lblDate2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.lblPercent = new Infragistics.Win.Misc.UltraLabel();
            this.lblDate = new Infragistics.Win.Misc.UltraLabel();
            this.lblDuaDate = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkChiTiet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbChiTiet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentClauseName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentClauseCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiscountDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDueDate)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            appearance1.FontData.BoldAsString = "True";
            this.ultraGroupBox1.Appearance = appearance1;
            this.ultraGroupBox1.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox1.Controls.Add(this.chkChiTiet);
            this.ultraGroupBox1.Controls.Add(this.cbbChiTiet);
            this.ultraGroupBox1.Controls.Add(this.txtPaymentClauseName);
            this.ultraGroupBox1.Controls.Add(this.txtPaymentClauseCode);
            this.ultraGroupBox1.Controls.Add(this.lblPaymentClauseName);
            this.ultraGroupBox1.Controls.Add(this.lblPaymentClauseCode);
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(8, 3);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(420, 86);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // chkChiTiet
            // 
            this.chkChiTiet.BackColor = System.Drawing.Color.Transparent;
            this.chkChiTiet.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkChiTiet.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkChiTiet.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkChiTiet.Location = new System.Drawing.Point(9, 275);
            this.chkChiTiet.Name = "chkChiTiet";
            this.chkChiTiet.Size = new System.Drawing.Size(101, 20);
            this.chkChiTiet.TabIndex = 11;
            this.chkChiTiet.Text = "Chi tiết theo";
            // 
            // cbbChiTiet
            // 
            this.cbbChiTiet.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbChiTiet.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbbChiTiet.Location = new System.Drawing.Point(129, 275);
            this.cbbChiTiet.Name = "cbbChiTiet";
            this.cbbChiTiet.Size = new System.Drawing.Size(245, 21);
            this.cbbChiTiet.TabIndex = 10;
            // 
            // txtPaymentClauseName
            // 
            this.txtPaymentClauseName.AutoSize = false;
            this.txtPaymentClauseName.Location = new System.Drawing.Point(184, 52);
            this.txtPaymentClauseName.MaxLength = 512;
            this.txtPaymentClauseName.Name = "txtPaymentClauseName";
            this.txtPaymentClauseName.Size = new System.Drawing.Size(221, 22);
            this.txtPaymentClauseName.TabIndex = 2;
            // 
            // txtPaymentClauseCode
            // 
            this.txtPaymentClauseCode.AutoSize = false;
            this.txtPaymentClauseCode.Location = new System.Drawing.Point(184, 26);
            this.txtPaymentClauseCode.MaxLength = 25;
            this.txtPaymentClauseCode.Name = "txtPaymentClauseCode";
            this.txtPaymentClauseCode.Size = new System.Drawing.Size(221, 22);
            this.txtPaymentClauseCode.TabIndex = 1;
            // 
            // lblPaymentClauseName
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.TextVAlignAsString = "Middle";
            this.lblPaymentClauseName.Appearance = appearance2;
            this.lblPaymentClauseName.Location = new System.Drawing.Point(6, 52);
            this.lblPaymentClauseName.Name = "lblPaymentClauseName";
            this.lblPaymentClauseName.Size = new System.Drawing.Size(171, 22);
            this.lblPaymentClauseName.TabIndex = 44;
            this.lblPaymentClauseName.Text = "Tên điều khoản thanh toán (*)";
            // 
            // lblPaymentClauseCode
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextVAlignAsString = "Middle";
            this.lblPaymentClauseCode.Appearance = appearance3;
            this.lblPaymentClauseCode.Location = new System.Drawing.Point(6, 26);
            this.lblPaymentClauseCode.Name = "lblPaymentClauseCode";
            this.lblPaymentClauseCode.Size = new System.Drawing.Size(170, 22);
            this.lblPaymentClauseCode.TabIndex = 43;
            this.lblPaymentClauseCode.Text = "Mã điều khoản thanh toán (*)";
            // 
            // btnSave
            // 
            appearance4.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance4;
            this.btnSave.Location = new System.Drawing.Point(269, 249);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 8;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            appearance5.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance5;
            this.btnClose.Location = new System.Drawing.Point(350, 250);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 9;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // chkIsActive
            // 
            appearance6.TextVAlignAsString = "Middle";
            this.chkIsActive.Appearance = appearance6;
            this.chkIsActive.BackColor = System.Drawing.Color.Transparent;
            this.chkIsActive.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsActive.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkIsActive.Checked = true;
            this.chkIsActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIsActive.Location = new System.Drawing.Point(18, 254);
            this.chkIsActive.Name = "chkIsActive";
            this.chkIsActive.Size = new System.Drawing.Size(110, 22);
            this.chkIsActive.TabIndex = 7;
            this.chkIsActive.Text = "Hoạt động";
            // 
            // ultraGroupBox2
            // 
            appearance7.FontData.BoldAsString = "True";
            this.ultraGroupBox2.Appearance = appearance7;
            this.ultraGroupBox2.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox2.Controls.Add(this.txtDiscountDate);
            this.ultraGroupBox2.Controls.Add(this.txtDueDate);
            this.ultraGroupBox2.Controls.Add(this.txtOverdueInterestPercent);
            this.ultraGroupBox2.Controls.Add(this.txtDiscountPercent);
            this.ultraGroupBox2.Controls.Add(this.lblDiscountDate);
            this.ultraGroupBox2.Controls.Add(this.lblOverdueInterestPercent);
            this.ultraGroupBox2.Controls.Add(this.lblDiscountPercent);
            this.ultraGroupBox2.Controls.Add(this.lblDate2);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox2.Controls.Add(this.lblPercent);
            this.ultraGroupBox2.Controls.Add(this.lblDate);
            this.ultraGroupBox2.Controls.Add(this.lblDuaDate);
            this.ultraGroupBox2.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox2.Location = new System.Drawing.Point(8, 95);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(420, 145);
            this.ultraGroupBox2.TabIndex = 1;
            this.ultraGroupBox2.Text = "Thông tin chiết khấu";
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtDiscountDate
            // 
            this.txtDiscountDate.AutoSize = false;
            this.txtDiscountDate.Location = new System.Drawing.Point(237, 110);
            this.txtDiscountDate.MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtDiscountDate.MaskInput = "nn,nnn,nnn";
            this.txtDiscountDate.MaxValue = 999999999;
            this.txtDiscountDate.MinValue = 0;
            this.txtDiscountDate.Name = "txtDiscountDate";
            this.txtDiscountDate.PromptChar = ' ';
            this.txtDiscountDate.Size = new System.Drawing.Size(100, 22);
            this.txtDiscountDate.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always;
            this.txtDiscountDate.TabIndex = 6;
            // 
            // txtDueDate
            // 
            this.txtDueDate.AutoSize = false;
            this.txtDueDate.Location = new System.Drawing.Point(237, 23);
            this.txtDueDate.MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtDueDate.MaskInput = "nnnnnnnnn";
            this.txtDueDate.MaxValue = 999999999;
            this.txtDueDate.MinValue = 0;
            this.txtDueDate.Name = "txtDueDate";
            this.txtDueDate.PromptChar = ' ';
            this.txtDueDate.Size = new System.Drawing.Size(100, 22);
            this.txtDueDate.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always;
            this.txtDueDate.TabIndex = 3;
            // 
            // txtOverdueInterestPercent
            // 
            this.txtOverdueInterestPercent.AutoSize = false;
            this.txtOverdueInterestPercent.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtOverdueInterestPercent.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.txtOverdueInterestPercent.InputMask = "{double:10.2}";
            this.txtOverdueInterestPercent.Location = new System.Drawing.Point(237, 81);
            this.txtOverdueInterestPercent.Name = "txtOverdueInterestPercent";
            this.txtOverdueInterestPercent.PromptChar = ' ';
            this.txtOverdueInterestPercent.Size = new System.Drawing.Size(100, 22);
            this.txtOverdueInterestPercent.TabIndex = 5;
            this.txtOverdueInterestPercent.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtOverdueInterestPercent_KeyPress);
            // 
            // txtDiscountPercent
            // 
            this.txtDiscountPercent.AutoSize = false;
            this.txtDiscountPercent.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtDiscountPercent.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.txtDiscountPercent.InputMask = "{double:10.2}";
            this.txtDiscountPercent.Location = new System.Drawing.Point(237, 52);
            this.txtDiscountPercent.Name = "txtDiscountPercent";
            this.txtDiscountPercent.PromptChar = ' ';
            this.txtDiscountPercent.Size = new System.Drawing.Size(100, 22);
            this.txtDiscountPercent.TabIndex = 4;
            // 
            // lblDiscountDate
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.TextVAlignAsString = "Middle";
            this.lblDiscountDate.Appearance = appearance8;
            this.lblDiscountDate.Location = new System.Drawing.Point(6, 110);
            this.lblDiscountDate.Name = "lblDiscountDate";
            this.lblDiscountDate.Size = new System.Drawing.Size(187, 22);
            this.lblDiscountDate.TabIndex = 47;
            this.lblDiscountDate.Text = "Hưởng chiết khấu nếu trả trong vòng";
            // 
            // lblOverdueInterestPercent
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.TextVAlignAsString = "Middle";
            this.lblOverdueInterestPercent.Appearance = appearance9;
            this.lblOverdueInterestPercent.Location = new System.Drawing.Point(6, 81);
            this.lblOverdueInterestPercent.Name = "lblOverdueInterestPercent";
            this.lblOverdueInterestPercent.Size = new System.Drawing.Size(105, 22);
            this.lblOverdueInterestPercent.TabIndex = 46;
            this.lblOverdueInterestPercent.Text = "Số % lãi nợ quá hạn";
            // 
            // lblDiscountPercent
            // 
            appearance10.BackColor = System.Drawing.Color.Transparent;
            appearance10.TextVAlignAsString = "Middle";
            this.lblDiscountPercent.Appearance = appearance10;
            this.lblDiscountPercent.Location = new System.Drawing.Point(6, 52);
            this.lblDiscountPercent.Name = "lblDiscountPercent";
            this.lblDiscountPercent.Size = new System.Drawing.Size(86, 22);
            this.lblDiscountPercent.TabIndex = 46;
            this.lblDiscountPercent.Text = "Số % chiết khấu";
            // 
            // lblDate2
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            appearance11.TextVAlignAsString = "Middle";
            this.lblDate2.Appearance = appearance11;
            this.lblDate2.Location = new System.Drawing.Point(343, 110);
            this.lblDate2.Name = "lblDate2";
            this.lblDate2.Size = new System.Drawing.Size(29, 22);
            this.lblDate2.TabIndex = 0;
            this.lblDate2.Text = "ngày";
            // 
            // ultraLabel1
            // 
            appearance12.BackColor = System.Drawing.Color.Transparent;
            appearance12.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance12;
            this.ultraLabel1.Location = new System.Drawing.Point(344, 81);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(14, 22);
            this.ultraLabel1.TabIndex = 0;
            this.ultraLabel1.Text = "%";
            // 
            // lblPercent
            // 
            appearance13.BackColor = System.Drawing.Color.Transparent;
            appearance13.TextVAlignAsString = "Middle";
            this.lblPercent.Appearance = appearance13;
            this.lblPercent.Location = new System.Drawing.Point(344, 52);
            this.lblPercent.Name = "lblPercent";
            this.lblPercent.Size = new System.Drawing.Size(14, 22);
            this.lblPercent.TabIndex = 0;
            this.lblPercent.Text = "%";
            // 
            // lblDate
            // 
            appearance14.BackColor = System.Drawing.Color.Transparent;
            appearance14.TextVAlignAsString = "Middle";
            this.lblDate.Appearance = appearance14;
            this.lblDate.Location = new System.Drawing.Point(344, 23);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(29, 22);
            this.lblDate.TabIndex = 0;
            this.lblDate.Text = "ngày";
            // 
            // lblDuaDate
            // 
            appearance15.BackColor = System.Drawing.Color.Transparent;
            appearance15.TextVAlignAsString = "Middle";
            this.lblDuaDate.Appearance = appearance15;
            this.lblDuaDate.Location = new System.Drawing.Point(6, 23);
            this.lblDuaDate.Name = "lblDuaDate";
            this.lblDuaDate.Size = new System.Drawing.Size(89, 22);
            this.lblDuaDate.TabIndex = 45;
            this.lblDuaDate.Text = "Số ngày được nợ";
            // 
            // FPaymentClauseDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(437, 287);
            this.Controls.Add(this.ultraGroupBox2);
            this.Controls.Add(this.chkIsActive);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.ultraGroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FPaymentClauseDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FPaymentClauseDetail_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkChiTiet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbChiTiet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentClauseName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentClauseCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtDiscountDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDueDate)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraLabel lblPaymentClauseCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtPaymentClauseName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtPaymentClauseCode;
        private Infragistics.Win.Misc.UltraLabel lblPaymentClauseName;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkChiTiet;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbChiTiet;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsActive;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.Misc.UltraLabel lblDiscountPercent;
        private Infragistics.Win.Misc.UltraLabel lblDuaDate;
        private Infragistics.Win.Misc.UltraLabel lblDiscountDate;
        private Infragistics.Win.Misc.UltraLabel lblDate2;
        private Infragistics.Win.Misc.UltraLabel lblPercent;
        private Infragistics.Win.Misc.UltraLabel lblDate;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtDiscountPercent;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor txtDueDate;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor txtDiscountDate;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtOverdueInterestPercent;
        private Infragistics.Win.Misc.UltraLabel lblOverdueInterestPercent;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
    }
}
