﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;

namespace Accounting
{
    public partial class FPaymentClauseDetail : DialogForm //UserControl
    {
        #region khai báo
        private readonly IPaymentClauseService _IPaymentClauseService;
        PaymentClause _Select = new PaymentClause();
        bool Them = true;
        public static bool isClose = true;
        #endregion

        #region khởi tạo
        public FPaymentClauseDetail()
        {//Thêm
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _IPaymentClauseService = IoC.Resolve<IPaymentClauseService>();
            chkIsActive.Visible = false;
            this.Text = "Thêm mới điều khoản thanh toán";
            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion
            //khởi tạo và hiển thị giá trị ban đầu cho các control
            #endregion
        }

        public FPaymentClauseDetail(PaymentClause temp)
        {//Sửa
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _Select = temp;
            Them = false;
            txtPaymentClauseCode.Enabled = false;
            _IPaymentClauseService = IoC.Resolve<IPaymentClauseService>();
            this.Text = "Sửa điều khoản thanh toán";
            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion
            #endregion

            #region Fill dữ liệu Obj vào control
            ObjandGUI(temp, false);
            #endregion
            txtDiscountPercent.FormatNumberic(ConstDatabase.Format_Coefficient);
            txtOverdueInterestPercent.FormatNumberic(ConstDatabase.Format_Coefficient);
        }

        #endregion

        #region nghiệp vụ
        #region Button Event
        /// <summary>
        /// Sự kiện nút Lưu lại
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion

            //Gui to object
            #region Fill dữ liệu control vào obj
            PaymentClause temp = Them ? new PaymentClause() : _IPaymentClauseService.Getbykey(_Select.ID);
            temp = ObjandGUI(temp, true);
            #endregion

            #region Thao tác CSDL
            _IPaymentClauseService.BeginTran();
            if (Them)
            {
                if (!CheckCode()) return;
                temp.IsActive = true;
                temp.IsSecurity = false;
                _IPaymentClauseService.CreateNew(temp);
            }
            else
            {
                if (!CheckError()) return;
                if (!chkIsActive.Checked) temp.IsActive = false;
                else temp.IsActive = true;
                _IPaymentClauseService.Update(temp);
            }
            _IPaymentClauseService.CommitTran();
            #endregion

            #region xử lý form, kết thúc form
            this.Close();
            isClose = false;
            #endregion
        }

        /// <summary>
        /// Sự kiện nút đóng
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            isClose = true;
            DialogResult = DialogResult.Cancel;
            this.Close();
        }
        #endregion

        private void txtOverdueInterestPercent_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar) || Char.IsControl(e.KeyChar) || e.KeyChar == 44)
            {
                e.Handled = false;
            }
            else
                e.Handled = true;
        }

        #endregion

        #region Utils
        /// <summary>
        /// get hoặc set một đối tượng vào giao diện xử lý
        /// </summary>
        /// <param name="input">đối tượng</param>
        /// <param name="isGet">True: get giá trị từ control gán vào đối tượng| False: set giá trị đối tượng vào control</param>
        /// <returns></returns>
        PaymentClause ObjandGUI(PaymentClause input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();  //Thêm mới
                input.PaymentClauseCode = txtPaymentClauseCode.Text;
                input.PaymentClauseName = txtPaymentClauseName.Text;
                input.DueDate = Int32.Parse(txtDueDate.Text);
                input.DiscountDate = Int32.Parse(txtDiscountDate.Text);
                if (txtOverdueInterestPercent.Text == null || txtOverdueInterestPercent.Text.Equals(""))
                {
                    input.OverdueInterestPercent = null;
                }
                else
                {
                    input.OverdueInterestPercent = decimal.Parse(txtOverdueInterestPercent.Text);
                }
                if (txtDiscountPercent.Text == null || txtDiscountPercent.Text.Equals("")) input.DiscountPercent = null;
                else input.DiscountPercent = decimal.Parse(txtDiscountPercent.Text);
                input.IsActive = chkIsActive.Checked;
            }
            else
            {
                txtPaymentClauseCode.Text = input.PaymentClauseCode;
                txtPaymentClauseName.Text = input.PaymentClauseName;
                txtDueDate.Text = input.DueDate.ToString();
                txtDiscountDate.Text = input.DiscountDate.ToString();
                txtDiscountPercent.Text = input.DiscountPercent.ToString();
                txtOverdueInterestPercent.Text = input.OverdueInterestPercent.ToString();
                chkIsActive.Checked = input.IsActive;
            }
            return input;
        }

        bool CheckError()
        {
            bool kq = true;
            if (string.IsNullOrEmpty(txtPaymentClauseCode.Text) || string.IsNullOrEmpty(txtPaymentClauseName.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }

            return kq;
        }

        bool CheckCode()
        {
            bool kq = true;
            if (string.IsNullOrEmpty(txtPaymentClauseCode.Text) || string.IsNullOrEmpty(txtPaymentClauseName.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }

            //check trùng code
            //List<string> lst = _IPaymentClauseService.Query.Select(c => c.PaymentClauseCode).ToList();
            List<string> lst = _IPaymentClauseService.GetCode();
            foreach (var x in lst)
            {
                if (txtPaymentClauseCode.Text.Equals(x))
                {
                    MSG.Warning(string.Format(resSystem.MSG_Catalog, "Mã", "Điều khoản thanh toán"));
                    return false;
                }
            }
            return kq;
        }
        #endregion

        private void FPaymentClauseDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
