﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;

using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FPaymentClause : CatalogBase //UserControl
    {
        #region khai báo
        private readonly IPaymentClauseService _IPaymentClauseService;
        private readonly IPPOrderService pPOrderService;
       
        
        #endregion

        #region khởi tạo
        public FPaymentClause()
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _IPaymentClauseService = IoC.Resolve<IPaymentClauseService>();
            this.uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            #endregion

            #region Tạo dữ liệu ban đầu
            LoadDuLieu();
            System.Windows.Forms.ToolTip ToolTip1 = new System.Windows.Forms.ToolTip();
            ToolTip1.SetToolTip(btnAdd, "Thêm (Ctrl + N)");
            System.Windows.Forms.ToolTip ToolTip2 = new System.Windows.Forms.ToolTip();
            ToolTip2.SetToolTip(btnEdit, "Xem (Ctrl + E)");
            System.Windows.Forms.ToolTip ToolTip3 = new System.Windows.Forms.ToolTip();
            ToolTip3.SetToolTip(btnDelete, "Xóa (Ctrl + D)");
            if (uGrid.Rows.Count > 0) uGrid.Rows[0].Selected = true;
            #endregion

        }

        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }
        private void LoadDuLieu(bool configGrid)
        {
            WaitingFrm.StartWaiting();
            #region Lấy dữ liệu từ CSDL
            //List<PaymentClause> list = _IPaymentClauseService.GetAll().OrderByDescending(c=>c.PaymentClauseCode).Reverse().ToList();
            List<PaymentClause> list = _IPaymentClauseService.OrderByCode();
            _IPaymentClauseService.UnbindSession(list);
            list = _IPaymentClauseService.OrderByCode();
            #endregion

            #region Xử lý dữ liệu
            #endregion

            #region hiển thị và xử lý hiển thị
            uGrid.DataSource = list.ToArray();

            if (configGrid) ConfigGrid(uGrid);
            uGrid.DisplayLayout.Bands[0].Columns["DiscountPercent"].FormatNumberic(ConstDatabase.Format_Rate);
            uGrid.DisplayLayout.Bands[0].Columns["DueDate"].CellAppearance.TextHAlign = HAlign.Center;
            uGrid.DisplayLayout.Bands[0].Columns["DueDate"].FormatNumberic(ConstDatabase.Format_Quantity);
            uGrid.DisplayLayout.Bands[0].Columns["DiscountDate"].CellAppearance.TextHAlign = HAlign.Center;
            #endregion
            WaitingFrm.StopWaiting();
        }
        #endregion

        #region Nghiệp vụ
        #region ContentMenu
        private void tsmAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void tsmEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            ResetFunction();
        }
        protected override void ResetFunction()
        {
            LoadDuLieu();
        }
        #endregion

        #region Button
        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }
        #endregion

        private void uGrid_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }

        private void uGrid_DoubleClick(object sender, EventArgs e)
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                EditFunction();
            }
        }

        /// Nghiệp vụ Add
        protected override void AddFunction()
        {
            new FPaymentClauseDetail().ShowDialog(this);
            if (!FPaymentClauseDetail.isClose) LoadDuLieu();
        }

        /// Nghiệp vụ Edit
        protected override void EditFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                PaymentClause temp = uGrid.Selected.Rows[0].ListObject as PaymentClause;
                new FPaymentClauseDetail(temp).ShowDialog(this);
                if (!FPaymentClauseDetail.isClose) LoadDuLieu();
            }
            else
                MSG.Warning(string.Format(resSystem.MSG_Catalog3, "một Điều khoản thanh toán"));
        }

        /// Nghiệp vụ Delete
        protected override void DeleteFunction()
        {
            #region check điều khoản thanh toán có trong các hóa đơn khác không
            if (uGrid.Selected.Rows.Count > 0)
            {
                PaymentClause temp = uGrid.Selected.Rows[0].ListObject as PaymentClause;
                //private readonly IPPOrderService pPOrderService;
               
                
                List<PPOrder> lst = new List<PPOrder>();
                List<SAQuote> lst1 = new List<SAQuote>();
                List<SAOrder> lst2 = new List<SAOrder>();
                List<SAInvoice> lst3 = new List<SAInvoice>();
                List<TIIncrement> lst4 = new List<TIIncrement>();
                List<FAIncrement> lst5 = new List<FAIncrement>();

                Utils.ClearCacheByType<PPOrder>();
                Utils.ClearCacheByType<SAOrder>();
                Utils.ClearCacheByType<SAQuote>();
                Utils.ClearCacheByType<SAInvoice>();
                Utils.ClearCacheByType<TIIncrement>();
                Utils.ClearCacheByType<FAIncrement>();

                lst4 = Utils.ListTIIncrement.Where(n => n.PaymentClauseID == temp.ID).ToList();
                lst2 = Utils.ListSAOrder.Where(n => n.PaymentClauseID == temp.ID).ToList();
                lst = Utils.ListPPOrder.Where(n=>n.PaymentClauseID==temp.ID).ToList();
                lst1 = Utils.ListSaQuote.Where(n => n.PaymentClauseID == temp.ID).ToList();
                lst3 = Utils.ListSAInvoice.Where(n => n.PaymentClauseID == temp.ID).ToList();
                lst5 = Utils.ListFAIncrement.Where(n => n.PaymentClauseID == temp.ID).ToList();

                #endregion
                if ((lst.Count!=0)||(lst1.Count!=0) || (lst2.Count != 0) || (lst3.Count != 0) || (lst4.Count != 0) || (lst5.Count != 0))
                {
                    MSG.Error("Cần xóa chứng từ kế toán có liên quan trước khi xóa dữ liệu danh mục");
                    return;
                }
                if (MSG.Question(string.Format(resSystem.MSG_System_05, "<" + temp.PaymentClauseCode + ">")) == System.Windows.Forms.DialogResult.Yes)
                {
                    if (temp.IsSecurity == true)
                    {
                        MSG.Warning(string.Format(resSystem.MSG_Catalog7, temp.PaymentClauseCode));
                    }
                    else
                    {
                        _IPaymentClauseService.BeginTran();
                        _IPaymentClauseService.Delete(temp);
                        _IPaymentClauseService.CommitTran();
                        Utils.ClearCacheByType<PaymentClause>();
                        LoadDuLieu();
                    }
                }
            }
            else
                MSG.Error(string.Format(resSystem.MSG_Catalog2, "một Điều khoản thanh toán"));
            
        }
        #endregion

        #region Utils
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, TextMessage.ConstDatabase.PaymentClause_TableName);
            uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.AlignWithDataRows;
            this.uGrid.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.uGrid.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGrid.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGrid.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGrid.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
        }
        #endregion

        private void FPaymentClause_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGrid.ResetText();
            uGrid.ResetUpdateMode();
            uGrid.ResetExitEditModeOnLeave();
            uGrid.ResetRowUpdateCancelAction();
            uGrid.DataSource = null;
            uGrid.Layouts.Clear();
            uGrid.ResetLayouts();
            uGrid.ResetDisplayLayout();
            uGrid.Refresh();
            uGrid.ClearUndoHistory();
            uGrid.ClearXsdConstraints();
        }

        private void FPaymentClause_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
