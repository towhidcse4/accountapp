﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Accounting.TextMessage;

namespace Accounting
{
    public partial class FMaterialGoodsSpecialTaxGroupDetail : DialogForm
    {
        #region khai báo
        private readonly IMaterialGoodsSpecialTaxGroupService _IMaterialGoodsSpecialTaxGroupService;
        MaterialGoodsSpecialTaxGroup _Select = new MaterialGoodsSpecialTaxGroup();
        bool IsAdd = true;
        public static bool isClose = true;
        List<MaterialGoodsSpecialTaxGroup> dsMaterialGoodsSpecialTaxGroup = new List<MaterialGoodsSpecialTaxGroup>();
        #endregion

        #region khởi tạo
        public FMaterialGoodsSpecialTaxGroupDetail()
        {
            //Thêm
            #region Khởi tạo giá trị mặc định Form
            InitializeComponent();
            this.Text = "Thêm mới nhóm HHDV chịu thuế TTĐB";
            this.CenterToParent();
            //  this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            // Set the MaximizeBox to false to remove the maximize box.
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            // Set the MinimizeBox to false to remove the minimize box.

            chkIsActive.Visible = false;
            #endregion

            #region Thiết lập ban đầu cho Form
            //Khai báo các webservices
            _IMaterialGoodsSpecialTaxGroupService = IoC.Resolve<IMaterialGoodsSpecialTaxGroupService>();

            //Lấy các giá trị const từ XML
            //<mặc định đôi tượng khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thi dữ liệu Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và thể hiện giá trị ban đầu cho các control
            InitializeGUI();
            #endregion
        }
        public FMaterialGoodsSpecialTaxGroupDetail(MaterialGoodsSpecialTaxGroup temp)
        {//Sửa
            #region Khởi tạo giá trị mặc định cho form
            InitializeComponent();
            this.Text = "Sửa nhóm HHDV chịu thuế TTĐB";
            this.CenterToParent();
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            #endregion

            #region Thiết lập ban đầu cho Form
            _Select = temp;
            IsAdd = false;
            txtFMaterialGoodsSpecialTaxGroupCode.Enabled = false;

            //Khai báo các webservices
            _IMaterialGoodsSpecialTaxGroupService = IoC.Resolve<IMaterialGoodsSpecialTaxGroupService>();


            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ  Database (xử lý giao diện mền dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion

            #region Fill dữ liệu Obj vào control
            ObjandGUI(temp, false);
            #endregion
        }

        private void InitializeGUI()
        {
            //khởi tạo cbb
            dsMaterialGoodsSpecialTaxGroup = _IMaterialGoodsSpecialTaxGroupService.GetAll();
            cbbParentID.DataSource = dsMaterialGoodsSpecialTaxGroup;
            cbbParentID.DisplayMember = "MaterialGoodsSpecialTaxGroupCode";
            Utils.ConfigGrid(cbbParentID, ConstDatabase.MaterialGoodsSpecialTaxGroup_TableName);
            txtTaxRate.FormatNumberic(ConstDatabase.Format_Coefficient);
        }
        #endregion

        #region Button Event
        private void btnClose_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click_1(object sender, EventArgs e)
        {
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion

            //Gui to object
            #region Fill dữ liệu control vào obj

            // thuc hien trong tung thao tac (them moi/ cap nhat)
            #endregion

            #region Thao tác CSDL

            try
            {
                _IMaterialGoodsSpecialTaxGroupService.BeginTran();
                if (IsAdd) // them moi
                {
                    MaterialGoodsSpecialTaxGroup temp = IsAdd ? new MaterialGoodsSpecialTaxGroup() : _IMaterialGoodsSpecialTaxGroupService.Getbykey(_Select.ID);
                    temp = ObjandGUI(temp, true);
                    // set orderfixcode
                    MaterialGoodsSpecialTaxGroup temp0 = (MaterialGoodsSpecialTaxGroup)Utils.getSelectCbbItem(cbbParentID);
                    //List<string> lstOrderFixCodeChild = _IMaterialGoodsSpecialTaxGroupService.Query.Where(a => a.ParentID == temp.ParentID).Select(a => a.OrderFixCode).ToList();
                    List<string> lstOrderFixCodeChild = _IMaterialGoodsSpecialTaxGroupService.GetOrderFixCode_ByParentID(temp.ParentID);//thành duy đã sửa chỗ này
                    string orderFixCodeParent = temp0 == null ? "" : temp0.OrderFixCode;
                    temp.OrderFixCode = Utils.GetOrderFixCode(lstOrderFixCodeChild, orderFixCodeParent);
                    temp.IsParentNode = false; // mac dinh cho insert
                    temp.IsSecurity = false; // mac dinh cho insert
                    temp.Unit = txtUnit.Text;
                    //    temp.TaxRate = decimal.Parse(txtTaxRate.Text.ToString());
                    temp.Grade = temp0 == null ? 1 : temp0.Grade + 1; // quy tac lay gia tri cho grade
                    //========> input.OrderFixCode chua xét

                    _IMaterialGoodsSpecialTaxGroupService.CreateNew(temp);
                    // thuc hien cap nhat lai isParentNode cho cha neu co
                    if (temp.ParentID != null)
                    {
                        MaterialGoodsSpecialTaxGroup parent = _IMaterialGoodsSpecialTaxGroupService.Getbykey((Guid)temp.ParentID);
                        parent.IsParentNode = true;
                        _IMaterialGoodsSpecialTaxGroupService.Update(parent);
                    }

                }
                else // cap nhat
                {
                    //L?y v? d?i tu?ng c?n s?a
                    MaterialGoodsSpecialTaxGroup temp = _IMaterialGoodsSpecialTaxGroupService.Getbykey(_Select.ID);
                    //Luu d?i tu?ng tru?c khi s?a
                    MaterialGoodsSpecialTaxGroup tempOriginal = (MaterialGoodsSpecialTaxGroup)Utils.CloneObject(temp);
                    //L?y v? d?i tu?ng ch?n trong combobox
                    MaterialGoodsSpecialTaxGroup objParentCombox = (MaterialGoodsSpecialTaxGroup)Utils.getSelectCbbItem(cbbParentID);
                    //Không thay d?i d?i tu?ng cha trong combobox
                    if (((temp.ParentID == null) && (objParentCombox == null)) || ((temp.ParentID != null) && (objParentCombox != null) && (temp.ParentID == objParentCombox.ID)))
                    {
                        temp = ObjandGUI(temp, true);
                        _IMaterialGoodsSpecialTaxGroupService.Update(temp);
                    }
                    //Thay d?i d?i tu?ng trong combobox
                    else
                    {
                        //Không cho phép chuy?n ngu?c
                        if (objParentCombox != null)
                        {
                            //List<MaterialGoodsSpecialTaxGroup> lstChildCheck =_IMaterialGoodsSpecialTaxGroupService.Query.Where(p => p.OrderFixCode.StartsWith(tempOriginal.OrderFixCode)).ToList();
                            List<MaterialGoodsSpecialTaxGroup> lstChildCheck = _IMaterialGoodsSpecialTaxGroupService.GetStartsWith_ByOrderFixCode(tempOriginal.OrderFixCode);//thành duy đã sửa chỗ này
                            foreach (var item in lstChildCheck)
                            {
                                if (objParentCombox.ID == item.ID)
                                {
                                    MSG.Warning(string.Format(resSystem.MSG_Catalog6, temp.MaterialGoodsSpecialTaxGroupCode));
                                    return;
                                }
                            }
                        }
                        //Tru?ng h?p có cha cu
                        //Ki?m tra cha cu có còn con ngoài con chuy?n di hay không
                        //Chuy?n tr?ng thái IsParentNode Cha cu n?u h?t con
                        if (temp.ParentID != null)
                        {
                            MaterialGoodsSpecialTaxGroup oldParent = _IMaterialGoodsSpecialTaxGroupService.Getbykey((Guid)temp.ParentID);
                            //int checkChildOldParent = _IMaterialGoodsSpecialTaxGroupService.Query.Count(p => p.ParentID == temp.ParentID);
                            int checkChildOldParent = _IMaterialGoodsSpecialTaxGroupService.CountByParentID(temp.ParentID);//thành duy đã sửa chỗ này
                            if (checkChildOldParent <= 1)
                            {
                                oldParent.IsParentNode = false;
                            }
                            _IMaterialGoodsSpecialTaxGroupService.Update(oldParent);
                        }
                        //Tru?ng h?p có cha m?i
                        //Chuy?n tr?ng IsParentNode cha m?i
                        if (objParentCombox != null)
                        {
                            MaterialGoodsSpecialTaxGroup newParent = _IMaterialGoodsSpecialTaxGroupService.Getbykey(objParentCombox.ID);
                            newParent.IsParentNode = true;
                            _IMaterialGoodsSpecialTaxGroupService.Update(newParent);

                            //List<MaterialGoodsSpecialTaxGroup> listChildNewParent = _IMaterialGoodsSpecialTaxGroupService.Query.Where( a => a.ParentID == objParentCombox.ID).ToList();
                            List<MaterialGoodsSpecialTaxGroup> listChildNewParent = _IMaterialGoodsSpecialTaxGroupService.GetAll_ByParentID(objParentCombox.ID);//thành duy đã sửa chỗ này
                            //Tính l?i OrderFixCode cho d?i tu?ng chính
                            List<string> lstOfcChildNewParent = new List<string>();
                            foreach (var item in listChildNewParent)
                            {
                                lstOfcChildNewParent.Add(item.OrderFixCode);
                            }
                            string orderFixCodeParent = newParent.OrderFixCode;
                            temp.OrderFixCode = Utils.GetOrderFixCode(lstOfcChildNewParent, orderFixCodeParent);
                            //Tính l?i b?c cho d?i tu?ng chính
                            temp.Grade = newParent.Grade + 1;
                        }
                        //Tru?ng h?p không có cha m?i t?c là chuy?n d?i tu?ng lên b?c 1
                        if (objParentCombox == null)
                        {
                            //L?y v? t?t c? d?i tu?ng b?c 1
                            List<int> lstOfcChildGradeNo1 = new List<int>();
                            List<MaterialGoodsSpecialTaxGroup> listChildGradeNo1 = _IMaterialGoodsSpecialTaxGroupService.Query.Where(
                                a => a.Grade == 1).ToList();
                            foreach (var item in listChildGradeNo1)
                            {
                                lstOfcChildGradeNo1.Add(Convert.ToInt32(item.OrderFixCode));
                            }
                            //Tính l?i OrderFixCode cho d?i tu?ng chính
                            temp.OrderFixCode = (lstOfcChildGradeNo1.Max() + 1).ToString(CultureInfo.InvariantCulture);
                            //Tính l?i b?c cho d?i tu?ng chính
                            temp.Grade = 1;
                            //temp.IsParentNode = false;//set mac dinh
                            //temp.IsSecurity = false;
                            //temp.IsTool = false;
                            temp = ObjandGUI(temp, true);
                            _IMaterialGoodsSpecialTaxGroupService.Update(temp);
                        }
                        //Xử lý các con của nó 
                        //Lấy về các con
                        //List<MaterialGoodsSpecialTaxGroup> lstChild =_IMaterialGoodsSpecialTaxGroupService.Query.Where(p => p.OrderFixCode.StartsWith(tempOriginal.OrderFixCode)).ToList();
                        List<MaterialGoodsSpecialTaxGroup> lstChild = _IMaterialGoodsSpecialTaxGroupService.GetStartsWith_ByOrderFixCode(tempOriginal.OrderFixCode);//thành duy đã sửa chỗ này
                        foreach (var item in lstChild)
                        {
                            string tempOldFixCode = tempOriginal.OrderFixCode;
                            item.OrderFixCode = temp.OrderFixCode + item.OrderFixCode.Substring(tempOldFixCode.Length);
                            item.Grade = temp.Grade - tempOriginal.Grade + item.Grade;
                            _IMaterialGoodsSpecialTaxGroupService.Update(item);
                        }
                        temp = ObjandGUI(temp, true);
                    }
                    //nếu cha ngừng theo dõi thì con cũng ngừng theo dõi
                    //List<MaterialGoodsSpecialTaxGroup> lstChild2 =_IMaterialGoodsSpecialTaxGroupService.Query.Where(p => p.OrderFixCode.StartsWith(tempOriginal.OrderFixCode)).ToList();
                    List<MaterialGoodsSpecialTaxGroup> lstChild2 = _IMaterialGoodsSpecialTaxGroupService.GetStartsWith_ByOrderFixCode(tempOriginal.OrderFixCode);//thành duy đã sửa chỗ này
                    if (!chkIsActive.Checked)
                    {
                        foreach (var item2 in lstChild2)
                        {
                            item2.IsActive = false;
                            _IMaterialGoodsSpecialTaxGroupService.Update(item2);
                        }

                    }
                    else
                    {
                        if (temp.IsParentNode)
                        {
                            if (MSG.Question(string.Format(resSystem.MSG_Catalog_Tree3,"nhóm HHDV")) == System.Windows.Forms.DialogResult.Yes)
                            {
                                foreach (var item2 in lstChild2)
                                {
                                    item2.IsActive = true;
                                    _IMaterialGoodsSpecialTaxGroupService.Update(item2);
                                }
                            }
                        }
                    }

                    //nếu cha isactive = false thì con ko được isactive = true,

                    if (temp.IsParentNode == false)
                    {
                        //lấy cha

                        if (temp.ParentID == null)
                        {
                            if (!CheckError()) return;
                            if (chkIsActive.Checked) temp.IsActive = true;
                            else temp.IsActive = false;
                            _IMaterialGoodsSpecialTaxGroupService.Update(temp);
                        }

                        else
                        {
                            MaterialGoodsSpecialTaxGroup Parent = _IMaterialGoodsSpecialTaxGroupService.Getbykey(objParentCombox.ID);
                            if (Parent.IsActive == true)
                            {
                                if (!CheckError()) return;
                                if (!chkIsActive.Checked) temp.IsActive = false;
                                else temp.IsActive = true;
                                _IMaterialGoodsSpecialTaxGroupService.Update(temp);
                            }
                            else
                            {
                                if (!chkIsActive.Checked)
                                    MSG.Warning(string.Format(resSystem.MSG_Catalog_Tree2, "nhóm HHDV"));
                                _IMaterialGoodsSpecialTaxGroupService.RolbackTran();
                            }
                        }

                    }
                    else
                    {
                        if (temp.ParentID != null)
                        {
                            MaterialGoodsSpecialTaxGroup Parent = _IMaterialGoodsSpecialTaxGroupService.Getbykey(objParentCombox.ID);
                            if (Parent.IsActive == true)
                            {
                                if (!CheckError()) return;
                                if (!chkIsActive.Checked) temp.IsActive = false;
                                else temp.IsActive = true;
                                _IMaterialGoodsSpecialTaxGroupService.Update(temp);
                            }
                            else
                            {
                                if (!chkIsActive.Checked)
                                    MSG.Warning(string.Format(resSystem.MSG_Catalog_Tree2, "nhóm HHDV"));
                                _IMaterialGoodsSpecialTaxGroupService.RolbackTran();
                            }
                        }
                        else
                        {
                            if (!CheckError()) return;
                            if (!chkIsActive.Checked) temp.IsActive = false;
                            else temp.IsActive = true;
                            _IMaterialGoodsSpecialTaxGroupService.Update(temp);
                        }
                    }
                }
                _IMaterialGoodsSpecialTaxGroupService.CommitTran();
            }
            catch (Exception ex)
            {
                _IMaterialGoodsSpecialTaxGroupService.RolbackTran();
            }
            #endregion

            #region xử lý form kết thúc form
            this.Close();
            isClose = false;
            #endregion
        }
        #endregion

        #region Utils
        /// <summary>
        /// get ho?c set m?t d?i tu?ng vào giao di?n x? lý
        /// </summary>
        /// <param name="input">d?i tu?ng</param>
        /// <param name="isGet">True: get giá tr? t? control gán vào d?i tu?ng| False: set giá tr? d?i tu?ng vào control</param>
        /// <returns></returns>
        MaterialGoodsSpecialTaxGroup ObjandGUI(MaterialGoodsSpecialTaxGroup input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();  //Thêm mới

                input.MaterialGoodsSpecialTaxGroupCode = txtFMaterialGoodsSpecialTaxGroupCode.Text;
                input.MaterialGoodsSpecialTaxGroupName = txtFMaterialGoodsSpecialTaxGroupName.Text;
                input.Unit = txtUnit.Text;
                //input.TaxRate = decimal.Parse(txtTaxRate.Value.ToString());
                decimal taxRate = 0;
                decimal.TryParse(txtTaxRate.Value.ToString(), out taxRate);
                input.TaxRate = taxRate;
                MaterialGoodsSpecialTaxGroup temp0 = (MaterialGoodsSpecialTaxGroup)Utils.getSelectCbbItem(cbbParentID);
                //Utils.CloneObject(temp0);
                if (temp0 == null) input.ParentID = null;
                else input.ParentID = temp0.ID;

                input.IsActive = chkIsActive.Checked;
                input.IsSecurity = false;
            }
            else
            {

                txtFMaterialGoodsSpecialTaxGroupCode.Text = input.MaterialGoodsSpecialTaxGroupCode;
                txtFMaterialGoodsSpecialTaxGroupName.Text = input.MaterialGoodsSpecialTaxGroupName;
                txtUnit.Text = input.Unit;
                txtTaxRate.Text = input.TaxRate.ToString();
                foreach (var item in cbbParentID.Rows)
                {
                    if ((item.ListObject as MaterialGoodsSpecialTaxGroup).ID == input.ParentID) cbbParentID.SelectedRow = item;
                }

                chkIsActive.Checked = input.IsActive;
            }
            return input;
        }

        bool CheckError()
        {
            bool kq = true;
            //Check Error Chung
            
            //Check Error Riêng t?ng Form
            if (string.IsNullOrEmpty(txtFMaterialGoodsSpecialTaxGroupCode.Text) || string.IsNullOrEmpty(txtFMaterialGoodsSpecialTaxGroupName.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }

            //Ki?m tra mã dã t?n t?i
            //if (dsMaterialGoodsCategory.Select(k => k.MaterialGoodsCode).Contains(txtMaterialGoodsCode.Text) && IsAdd)
            //{
            //    MSG.Error("D? li?u dã có trong CSDL");
            //    return false;
            //}
            //List<string> list = _IMaterialGoodsSpecialTaxGroupService.Query.Select(p => p.MaterialGoodsSpecialTaxGroupCode).ToList();
            List<string> list = _IMaterialGoodsSpecialTaxGroupService.GetMaterialGoodsSpecialTaxGroupCode();//thành duy đã sửa chỗ này

            foreach (var item in list)
            {
                if (item == txtFMaterialGoodsSpecialTaxGroupCode.Text && IsAdd)
                {
                    MSG.Warning(string.Format(resSystem.MSG_Catalog,"Mã",item));
                    return false;
                }
            }
            return kq;
        }
        #endregion

        private void cbbParentID_Leave(object sender, EventArgs e)
        {
            if(cbbParentID.Value == null || cbbParentID.SelectedRow == null)
            {
                cbbParentID.Text = null;
            }
        }

        private void FMaterialGoodsSpecialTaxGroupDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
