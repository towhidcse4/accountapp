﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using Accounting.TextMessage;
using System.Data;
using Infragistics.Win.UltraWinTree;

namespace Accounting
{
    public partial class FMaterialGoodsSpecialTaxGroup : CatalogBase //UserControl
    {
        #region khai báo
        private readonly IMaterialGoodsSpecialTaxGroupService _IMaterialGoodsSpecialTaxGroupService;
        List<MaterialGoodsSpecialTaxGroup> dsMaterialGoodsSpecialTaxGroup = new List<MaterialGoodsSpecialTaxGroup>();
        #endregion

        #region khởi tạo
        public FMaterialGoodsSpecialTaxGroup()
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            ToolTip ToolTip1 = new ToolTip();
            ToolTip1.SetToolTip(btnAdd, "Thêm (Ctrl + N)");
            ToolTip ToolTip2 = new ToolTip();
            ToolTip2.SetToolTip(btnEdit, "Sửa (Ctrl + E)");
            ToolTip ToolTip3 = new ToolTip();
            ToolTip3.SetToolTip(btnDelete, "Xóa (Ctrl + D)");
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            #endregion

            #region Thiết lập ban đầu cho Form
            _IMaterialGoodsSpecialTaxGroupService = IoC.Resolve<IMaterialGoodsSpecialTaxGroupService>();

            this.uTree.InitializeDataNode += new Infragistics.Win.UltraWinTree.InitializeDataNodeEventHandler(this.uTree_InitializeDataNode);
            this.uTree.AfterDataNodesCollectionPopulated += new Infragistics.Win.UltraWinTree.AfterDataNodesCollectionPopulatedEventHandler(Utils.uTree_AfterDataNodesCollectionPopulated);
            this.uTree.ColumnSetGenerated += new Infragistics.Win.UltraWinTree.ColumnSetGeneratedEventHandler(this.uTree_ColumnSetGenerated);
            this.uTree.MouseDown += new System.Windows.Forms.MouseEventHandler(this.uTree_MouseDown);
            this.uTree.DoubleClick += new System.EventHandler(this.uTree_DoubleClick);
            #endregion

            #region Tạo dữ liệu ban đầu
            LoadDuLieu();
            #endregion
        }

        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }
        private void LoadDuLieu(bool configTree)
        {
            WaitingFrm.StartWaiting();
            #region Lấy dữ liệu từ CSDL
            dsMaterialGoodsSpecialTaxGroup = _IMaterialGoodsSpecialTaxGroupService.GetAll_OrderBy();
            _IMaterialGoodsSpecialTaxGroupService.UnbindSession(dsMaterialGoodsSpecialTaxGroup);
            dsMaterialGoodsSpecialTaxGroup = _IMaterialGoodsSpecialTaxGroupService.GetAll_OrderBy();
            //dsMaterialGoodsSpecialTaxGroup = _IMaterialGoodsSpecialTaxGroupService.GetAll();
            #endregion

            #region Xử lý dữ liệu
            #endregion

            #region hiển thị và xử lý hiển thị
            DataSet ds = Utils.ToDataSet<MaterialGoodsSpecialTaxGroup>(dsMaterialGoodsSpecialTaxGroup, ConstDatabase.MaterialGoodsSpecialTaxGroup_TableName);
            uTree.SetDataBinding(ds, ConstDatabase.MaterialGoodsSpecialTaxGroup_TableName);
            if (uTree.SelectedNodes.Count == 0 && uTree.Nodes.Count > 0)
            {

                this.uTree.Nodes[0].Selected = true;
            }

            if (configTree) ConfigTree(uTree);
            #endregion
            WaitingFrm.StopWaiting();
        }
        #endregion

        #region Nghiệp vụ
        #region ContentMenu
        private void tsmAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void tsmEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            ResetFunction();
        }
        protected override void ResetFunction()
        {
            LoadDuLieu(false);
        }
        #endregion

        #region Button
        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
            LoadDuLieu();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
            LoadDuLieu();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
            LoadDuLieu();
        }
        #endregion

        /// <summary>
        /// Nghiệp vụ Add
        /// </summary>
        protected override void AddFunction()
        {
            new FMaterialGoodsSpecialTaxGroupDetail().ShowDialog(this);
            if (!FMaterialGoodsSpecialTaxGroupDetail.isClose) LoadDuLieu();
        }

        /// <summary>
        /// Nghiệp vụ Edit
        /// </summary>
        protected override void EditFunction()
        {
            if (uTree.SelectedNodes.Count > 0)
            {
                MaterialGoodsSpecialTaxGroup temp = dsMaterialGoodsSpecialTaxGroup.Single(k => k.ID == (new Guid(uTree.SelectedNodes[0].Text)));
                new FMaterialGoodsSpecialTaxGroupDetail(temp).ShowDialog(this);
                if (!FMaterialGoodsSpecialTaxGroupDetail.isClose) LoadDuLieu();
            }
            else
                Accounting.TextMessage.MSG.Error(string.Format(Accounting.TextMessage.resSystem.MSG_Catalog3,"một nhóm HHDV"));

        }

        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        protected override void DeleteFunction()
        {
            if (uTree.SelectedNodes.Count > 0)
            {
                MaterialGoodsSpecialTaxGroup temp = dsMaterialGoodsSpecialTaxGroup.Single(k => k.ID == (new Guid(uTree.SelectedNodes[0].Text)));

                if (temp.IsSecurity == true)
                {
                    MSG.Warning(string.Format(resSystem.MSG_Catalog7, "Nhóm HHDV"));
                    return;
                }



                if (MSG.Question(string.Format(resSystem.MSG_System_05, temp.MaterialGoodsSpecialTaxGroupCode)) == System.Windows.Forms.DialogResult.Yes)
                {
                    _IMaterialGoodsSpecialTaxGroupService.BeginTran();
                    //Kiểm tra xem đối tượng có con không nếu có con thì không được phép xóa
                    //List<MaterialGoodsSpecialTaxGroup> lstChild = _IMaterialGoodsSpecialTaxGroupService.Query.Where(p => p.ParentID == temp.ID).ToList();
                    List<MaterialGoodsSpecialTaxGroup> lstChild = _IMaterialGoodsSpecialTaxGroupService.GetAll_ByParentID(temp.ID);//thành duy đã sửa chỗ này
                    if (lstChild.Count > 0)
                    {
                        MSG.Warning(string.Format(resSystem.MSG_Catalog_Tree, "nhóm HHDV"));
                        return;
                    }
                    //Check xem cha còn có con khác không nếu không thì chuyển trạng thái IsParentNode = false
                    if (temp.ParentID != null)
                    {
                        MaterialGoodsSpecialTaxGroup parent = _IMaterialGoodsSpecialTaxGroupService.Getbykey((Guid)temp.ParentID);
                        //int checkChildOldParent = _IMaterialGoodsSpecialTaxGroupService.Query.Count(p => p.ParentID == temp.ParentID);
                        int checkChildOldParent = _IMaterialGoodsSpecialTaxGroupService.CountByParentID(temp.ParentID);//thành duy đã sửa chỗ này
                        if (checkChildOldParent <= 1)
                        {
                            parent.IsParentNode = false;
                        }
                        _IMaterialGoodsSpecialTaxGroupService.Update(parent);
                        _IMaterialGoodsSpecialTaxGroupService.Delete(temp);
                    }

                    if (temp.ParentID == null)
                    {
                        _IMaterialGoodsSpecialTaxGroupService.Delete(temp);
                    }
                    _IMaterialGoodsSpecialTaxGroupService.CommitTran();
                    Utils.ClearCacheByType<MaterialGoodsSpecialTaxGroup>();
                    LoadDuLieu();
                }
            }
            else
                MSG.Error(string.Format(resSystem.MSG_Catalog2,"một Nhóm HHDV"));
            
        }
        #endregion

        #region Event
        private void uTree_MouseDown(object sender, MouseEventArgs e)
        {//khi click chuột phải hiện content menu
            Utils.uGrid_MouseDown(sender, e, cms4Grid);
        }

        private void uTree_DoubleClick(object sender, EventArgs e)
        {//click đúp chuột để sửa
            EditFunction();
        }

        public void uTree_InitializeDataNode(object sender, InitializeDataNodeEventArgs e)
        {
            Utils.uTree_InitializeDataNode(sender, e, ConstDatabase.MaterialGoodsSpecialTaxGroup_TableName);
        }

        private void uTree_ColumnSetGenerated(object sender, ColumnSetGeneratedEventArgs e)
        {
            Utils.uTree_ColumnSetGenerated(sender, e, ConstDatabase.MaterialGoodsSpecialTaxGroup_TableName);
        }
        #endregion

        #region Utils
        void ConfigTree(Infragistics.Win.UltraWinTree.UltraTree ultraTree)
        {//hàm chung
            Utils.ConfigTree(ultraTree, TextMessage.ConstDatabase.MaterialGoodsSpecialTaxGroup_TableName);
        }
        #endregion

        private void uTree_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            // editFunction();
        }

        private void FMaterialGoodsSpecialTaxGroup_FormClosing(object sender, FormClosingEventArgs e)
        {
            uTree = null;
        }

        private void FMaterialGoodsSpecialTaxGroup_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
