﻿namespace Accounting
{
    partial class FMaterialGoodsSpecialTaxGroupDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtTaxRate = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.lblTaxRate = new Infragistics.Win.Misc.UltraLabel();
            this.txtUnit = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblUnit = new Infragistics.Win.Misc.UltraLabel();
            this.txtFMaterialGoodsSpecialTaxGroupName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblFMaterialGoodsSpecialTaxGroupName = new Infragistics.Win.Misc.UltraLabel();
            this.cbbParentID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.lblParentID = new Infragistics.Win.Misc.UltraLabel();
            this.txtFMaterialGoodsSpecialTaxGroupCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblFMaterialGoodsSpecialTaxGroupCode = new Infragistics.Win.Misc.UltraLabel();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.chkIsActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFMaterialGoodsSpecialTaxGroupName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbParentID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFMaterialGoodsSpecialTaxGroupCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox1.Controls.Add(this.txtTaxRate);
            this.ultraGroupBox1.Controls.Add(this.lblTaxRate);
            this.ultraGroupBox1.Controls.Add(this.txtUnit);
            this.ultraGroupBox1.Controls.Add(this.lblUnit);
            this.ultraGroupBox1.Controls.Add(this.txtFMaterialGoodsSpecialTaxGroupName);
            this.ultraGroupBox1.Controls.Add(this.lblFMaterialGoodsSpecialTaxGroupName);
            this.ultraGroupBox1.Controls.Add(this.cbbParentID);
            this.ultraGroupBox1.Controls.Add(this.lblParentID);
            this.ultraGroupBox1.Controls.Add(this.txtFMaterialGoodsSpecialTaxGroupCode);
            this.ultraGroupBox1.Controls.Add(this.lblFMaterialGoodsSpecialTaxGroupCode);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            appearance6.FontData.BoldAsString = "True";
            this.ultraGroupBox1.HeaderAppearance = appearance6;
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(375, 157);
            this.ultraGroupBox1.TabIndex = 34;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtTaxRate
            // 
            this.txtTaxRate.AutoSize = false;
            this.txtTaxRate.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.txtTaxRate.Location = new System.Drawing.Point(155, 127);
            this.txtTaxRate.Name = "txtTaxRate";
            this.txtTaxRate.Size = new System.Drawing.Size(216, 22);
            this.txtTaxRate.TabIndex = 27;
            // 
            // lblTaxRate
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.TextVAlignAsString = "Middle";
            this.lblTaxRate.Appearance = appearance1;
            this.lblTaxRate.Location = new System.Drawing.Point(9, 127);
            this.lblTaxRate.Name = "lblTaxRate";
            this.lblTaxRate.Size = new System.Drawing.Size(141, 22);
            this.lblTaxRate.TabIndex = 26;
            this.lblTaxRate.Text = "Thuế suất (%)";
            // 
            // txtUnit
            // 
            this.txtUnit.AutoSize = false;
            this.txtUnit.Location = new System.Drawing.Point(155, 102);
            this.txtUnit.MaxLength = 512;
            this.txtUnit.Name = "txtUnit";
            this.txtUnit.Size = new System.Drawing.Size(216, 22);
            this.txtUnit.TabIndex = 23;
            // 
            // lblUnit
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.TextVAlignAsString = "Middle";
            this.lblUnit.Appearance = appearance2;
            this.lblUnit.Location = new System.Drawing.Point(9, 102);
            this.lblUnit.Name = "lblUnit";
            this.lblUnit.Size = new System.Drawing.Size(141, 22);
            this.lblUnit.TabIndex = 24;
            this.lblUnit.Text = "Đơn vị tính";
            // 
            // txtFMaterialGoodsSpecialTaxGroupName
            // 
            this.txtFMaterialGoodsSpecialTaxGroupName.AutoSize = false;
            this.txtFMaterialGoodsSpecialTaxGroupName.Location = new System.Drawing.Point(155, 52);
            this.txtFMaterialGoodsSpecialTaxGroupName.MaxLength = 512;
            this.txtFMaterialGoodsSpecialTaxGroupName.Name = "txtFMaterialGoodsSpecialTaxGroupName";
            this.txtFMaterialGoodsSpecialTaxGroupName.Size = new System.Drawing.Size(216, 22);
            this.txtFMaterialGoodsSpecialTaxGroupName.TabIndex = 6;
            // 
            // lblFMaterialGoodsSpecialTaxGroupName
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextVAlignAsString = "Middle";
            this.lblFMaterialGoodsSpecialTaxGroupName.Appearance = appearance3;
            this.lblFMaterialGoodsSpecialTaxGroupName.Location = new System.Drawing.Point(9, 52);
            this.lblFMaterialGoodsSpecialTaxGroupName.Name = "lblFMaterialGoodsSpecialTaxGroupName";
            this.lblFMaterialGoodsSpecialTaxGroupName.Size = new System.Drawing.Size(141, 22);
            this.lblFMaterialGoodsSpecialTaxGroupName.TabIndex = 22;
            this.lblFMaterialGoodsSpecialTaxGroupName.Text = "Tên loại  (*)";
            // 
            // cbbParentID
            // 
            this.cbbParentID.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.Append;
            this.cbbParentID.AutoSize = false;
            this.cbbParentID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbParentID.Location = new System.Drawing.Point(155, 77);
            this.cbbParentID.Name = "cbbParentID";
            this.cbbParentID.Size = new System.Drawing.Size(216, 22);
            this.cbbParentID.TabIndex = 7;
            this.cbbParentID.Leave += new System.EventHandler(this.cbbParentID_Leave);
            // 
            // lblParentID
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.lblParentID.Appearance = appearance4;
            this.lblParentID.Location = new System.Drawing.Point(9, 77);
            this.lblParentID.Name = "lblParentID";
            this.lblParentID.Size = new System.Drawing.Size(92, 22);
            this.lblParentID.TabIndex = 16;
            this.lblParentID.Text = "Thuộc loại";
            // 
            // txtFMaterialGoodsSpecialTaxGroupCode
            // 
            this.txtFMaterialGoodsSpecialTaxGroupCode.AutoSize = false;
            this.txtFMaterialGoodsSpecialTaxGroupCode.Location = new System.Drawing.Point(155, 27);
            this.txtFMaterialGoodsSpecialTaxGroupCode.MaxLength = 25;
            this.txtFMaterialGoodsSpecialTaxGroupCode.Name = "txtFMaterialGoodsSpecialTaxGroupCode";
            this.txtFMaterialGoodsSpecialTaxGroupCode.Size = new System.Drawing.Size(216, 22);
            this.txtFMaterialGoodsSpecialTaxGroupCode.TabIndex = 5;
            // 
            // lblFMaterialGoodsSpecialTaxGroupCode
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.TextVAlignAsString = "Middle";
            this.lblFMaterialGoodsSpecialTaxGroupCode.Appearance = appearance5;
            this.lblFMaterialGoodsSpecialTaxGroupCode.Location = new System.Drawing.Point(9, 27);
            this.lblFMaterialGoodsSpecialTaxGroupCode.Name = "lblFMaterialGoodsSpecialTaxGroupCode";
            this.lblFMaterialGoodsSpecialTaxGroupCode.Size = new System.Drawing.Size(140, 22);
            this.lblFMaterialGoodsSpecialTaxGroupCode.TabIndex = 0;
            this.lblFMaterialGoodsSpecialTaxGroupCode.Text = "Mã loại (*)";
            // 
            // btnClose
            // 
            appearance7.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance7;
            this.btnClose.Location = new System.Drawing.Point(297, 163);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 33;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click_1);
            // 
            // chkIsActive
            // 
            appearance8.TextVAlignAsString = "Middle";
            this.chkIsActive.Appearance = appearance8;
            this.chkIsActive.BackColor = System.Drawing.Color.Transparent;
            this.chkIsActive.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsActive.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkIsActive.Checked = true;
            this.chkIsActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIsActive.Location = new System.Drawing.Point(3, 167);
            this.chkIsActive.Name = "chkIsActive";
            this.chkIsActive.Size = new System.Drawing.Size(119, 22);
            this.chkIsActive.TabIndex = 31;
            this.chkIsActive.Text = "Hoạt động";
            // 
            // btnSave
            // 
            appearance9.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance9;
            this.btnSave.Location = new System.Drawing.Point(210, 163);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 32;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click_1);
            // 
            // FMaterialGoodsSpecialTaxGroupDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(375, 198);
            this.Controls.Add(this.ultraGroupBox1);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.chkIsActive);
            this.Controls.Add(this.btnSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FMaterialGoodsSpecialTaxGroupDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FMaterialGoodsSpecialTaxGroupDetail";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FMaterialGoodsSpecialTaxGroupDetail_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFMaterialGoodsSpecialTaxGroupName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbParentID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFMaterialGoodsSpecialTaxGroupCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtFMaterialGoodsSpecialTaxGroupName;
        private Infragistics.Win.Misc.UltraLabel lblFMaterialGoodsSpecialTaxGroupName;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbParentID;
        private Infragistics.Win.Misc.UltraLabel lblParentID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtFMaterialGoodsSpecialTaxGroupCode;
        private Infragistics.Win.Misc.UltraLabel lblFMaterialGoodsSpecialTaxGroupCode;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsActive;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraLabel lblTaxRate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtUnit;
        private Infragistics.Win.Misc.UltraLabel lblUnit;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtTaxRate;
    }
}