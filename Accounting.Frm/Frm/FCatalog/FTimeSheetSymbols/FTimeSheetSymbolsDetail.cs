﻿using System;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Accounting.TextMessage;
using System.Collections.Generic;
using System.Linq;

namespace Accounting
{
    public partial class FTimeSheetSymbolsDetail : DialogForm //UserControl
    {
        #region khai báo
        private readonly ITimeSheetSymbolsService _ITimeSheetSymbolsService = IoC.Resolve<ITimeSheetSymbolsService>();
        private readonly IPSSalaryTaxInsuranceRegulationService _PSSalaryTaxInsuranceRegulation = IoC.Resolve<IPSSalaryTaxInsuranceRegulationService>();
        TimeSheetSymbols _Select = new TimeSheetSymbols();
        PSSalaryTaxInsuranceRegulation psstir = null;
        bool Them = true;
        public static bool isClose = true;
        #endregion

        #region khởi tạo
        public FTimeSheetSymbolsDetail()
        {//Thêm
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            this.Text = "Thêm mới ký hiệu chấm công";
            chkIsActive.Visible = false;
            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----
            psstir = _PSSalaryTaxInsuranceRegulation.FindByDate(DateTime.Now);
            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion
        }

        public FTimeSheetSymbolsDetail(TimeSheetSymbols temp)
        {//Sửa
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _Select = temp;
            Them = false;

            //Khai báo các webservices
            _ITimeSheetSymbolsService = IoC.Resolve<ITimeSheetSymbolsService>();
            this.Text = "Sửa ký hiệu chấm công";
            txtTimeSheetSymbolsCode.Enabled = false;
            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion

            #region Fill dữ liệu Obj vào control
            ObjandGUI(temp, false);
            #endregion
        }

        private void InitializeGUI()
        {
            //
            txtSalaryRate.FormatNumberic(ConstDatabase.Format_TienVND);
        }
        #endregion

        #region nghiệp vụ
        #region Button Event
        /// <summary>
        /// Sự kiện nút Lưu lại
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion

            //Gui to object
            #region Fill dữ liệu control vào obj
            TimeSheetSymbols temp = Them ? new TimeSheetSymbols() : _ITimeSheetSymbolsService.Getbykey(_Select.ID);
            temp = ObjandGUI(temp, true);
            #endregion

            #region Thao tác CSDL
            try
            {
                _ITimeSheetSymbolsService.BeginTran();
                if (Them)
                {
                    if (!CheckCode()) return;
                    if (!checkDefault()) return;
                    if (!checkHalfDefault()) return;
                    temp.IsSecurity = false;
                    temp.IsActive = true;
                    _ITimeSheetSymbolsService.CreateNew(temp);
                }
                else
                {
                    if (!checkDefault())
                    {
                        temp.IsDefault = false;
                        return;
                    }
                    if (!checkHalfDefault())
                    {
                        temp.IsHalfDayDefault = false;
                        return;
                    }
                    _ITimeSheetSymbolsService.Update(temp);

                }
                _ITimeSheetSymbolsService.CommitTran();
            }
            catch
            {
                _ITimeSheetSymbolsService.RolbackTran();
            }
            #endregion

            #region xử lý form, kết thúc form
            this.Close();
            isClose = false;
            #endregion
        }

        /// <summary>
        /// Sự kiện nút đóng
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            isClose = true;
            this.Close();
            
        }
        #endregion

        #endregion

        #region Utils
        /// <summary>
        /// get hoặc set một đối tượng vào giao diện xử lý
        /// </summary>
        /// <param name="input">đối tượng</param>
        /// <param name="isGet">True: get giá trị từ control gán vào đối tượng| False: set giá trị đối tượng vào control</param>
        /// <returns></returns>
        TimeSheetSymbols ObjandGUI(TimeSheetSymbols input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();  //Thêm mới

                //mã, tên, tỷ lệ hưởng lương, là ký hiệu mặc định
                input.TimeSheetSymbolsCode = txtTimeSheetSymbolsCode.Text;
                input.TimeSheetSymbolsName = txtTimeSheetSymbolsName.Text;
                if (txtSalaryRate.Text == "" || txtSalaryRate.Text == null) txtSalaryRate.Text = null;
                else input.SalaryRate = decimal.Parse(txtSalaryRate.Text);

                input.IsDefault = chkIsDefault.CheckState == CheckState.Checked ? true : false;
                input.IsHalfDay = chkIsHalfDay.CheckState == CheckState.Checked ? true : false;
                input.IsHalfDayDefault = ckhishalfdaydefault.CheckState == CheckState.Checked;                
                //Ngừng theo dõi
                input.IsActive = chkIsActive.Checked;
                input.OverTimeSymbol = cbbOverTimeSymbol.SelectedIndex - 1;
                if(input.OverTimeSymbol != null && input.OverTimeSymbol >= 0)
                {
                    input.IsOverTime = true;
                }
            }
            else
            {
                //mã, tên, tỷ lệ hưởng lương, là ký hiệu mặc định
                txtTimeSheetSymbolsCode.Text = input.TimeSheetSymbolsCode;
                txtTimeSheetSymbolsName.Text = input.TimeSheetSymbolsName;
                txtSalaryRate.Text = input.SalaryRate.ToString();
                //chkIsDefault.CheckState = input.IsDefault ? CheckState.Unchecked : CheckState.Checked;
                if (input.IsDefault == null || input.IsDefault == false) chkIsDefault.CheckState = CheckState.Unchecked;
                else chkIsDefault.CheckState = CheckState.Checked;
                if (input.IsHalfDay == null || input.IsHalfDay == false) chkIsHalfDay.CheckState = CheckState.Unchecked;
                else chkIsHalfDay.CheckState = CheckState.Checked;
                //Ngừng theo dõi
                ckhishalfdaydefault.Checked = input.IsHalfDayDefault;
                chkIsActive.Checked = input.IsActive;
                cbbOverTimeSymbol.SelectedIndex = (input.OverTimeSymbol ?? -1 ) + 1;

            }
            return input;
        }

        //check error
        bool CheckError()
        {
            bool kq = true;
            if (string.IsNullOrEmpty(txtTimeSheetSymbolsCode.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }
            if (ckhishalfdaydefault.CheckState == CheckState.Checked && chkIsDefault.CheckState == CheckState.Checked)
            {
                MSG.Warning("Không chọn đồng thời ký hiệu chấm công cả ngày và nửa ngày!");
                return false;
            }
            if (chkIsHalfDay.CheckState == CheckState.Checked && chkIsDefault.CheckState == CheckState.Checked)
            {
                MSG.Warning("Không chọn đồng thời ký hiệu chấm công cả ngày và nửa ngày!");
                return false;
            }
            return kq;
        }

        bool CheckCode()
        {
            bool kq = true;
            if (string.IsNullOrEmpty(txtTimeSheetSymbolsCode.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }
            //check trùng code
            //List<string> lst = _ITimeSheetSymbolsService.Query.Select(c => c.TimeSheetSymbolsCode).ToList();
            List<string> lst = _ITimeSheetSymbolsService.GetCode();
            foreach (var x in lst)
            {
                if (txtTimeSheetSymbolsCode.Text.Equals(x))
                {
                    MSG.Warning(string.Format(resSystem.MSG_Catalog_Account6, "Ký hiệu chấm công"));
                    return false;
                }
            }        
            
            //chỉ 1 ký hiệu được làm mặc định
            return kq;
        }

        bool checkDefault()
        {
            bool kq = true;
            //int x = _ITimeSheetSymbolsService.GetAll().Count(c => c.IsDefault == true);
            int x = _ITimeSheetSymbolsService.GetIsDefault(true).Count;
            if (x >= 2)
            {
                if (chkIsDefault.CheckState == CheckState.Checked)
                {
                    MSG.Warning(resSystem.MSG_Catalog_FTimeSheetSymbolsDetail);
                    return false;
                }
            }           
            return kq;
        }
        bool checkHalfDefault()
        {
            bool kq = true;
            if (ckhishalfdaydefault.CheckState == CheckState.Checked && Utils.ListTimeSheetSymbols.Where(c => c.IsHalfDayDefault).ToList().Count > 1)
            {
                MSG.Warning("Chỉ thiết lập được một ký hiệu chấm công nửa ngày mặc định!");
                return false;
            }

            return kq;
        }
        #endregion

        private void cbbOverTimeSymbol_ValueChanged(object sender, EventArgs e)
        {
            if (psstir == null) return;
            if (cbbOverTimeSymbol.SelectedIndex == 1)
            {
                txtSalaryRate.Value = psstir.OvertimeDailyPercent;
            }
            else if (cbbOverTimeSymbol.SelectedIndex == 2)
            {
                txtSalaryRate.Value = psstir.OvertimeWeekendPercent;
            }
            else if (cbbOverTimeSymbol.SelectedIndex == 3)
            {
                txtSalaryRate.Value = psstir.OvertimeHolidayPercent;
            }
            else if (cbbOverTimeSymbol.SelectedIndex == 4)
            {
                txtSalaryRate.Value = psstir.OvertimeWorkingDayNightPercent;
            }
            else if (cbbOverTimeSymbol.SelectedIndex == 5)
            {
                txtSalaryRate.Value = psstir.OvertimeHolidayNightPercent;
            }
            else if (cbbOverTimeSymbol.SelectedIndex == 6)
            {
                txtSalaryRate.Value = psstir.OvertimeHolidayNightPercent;
            }
        }

        private void txtSalaryRate_TextChanged(object sender, EventArgs e)
        {
            if (txtSalaryRate.Text == null || string.IsNullOrEmpty(txtSalaryRate.Text)) txtSalaryRate.Text = "0";
        }

        private void FTimeSheetSymbolsDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
