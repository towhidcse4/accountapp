﻿namespace Accounting
{
    partial class FTimeSheetSymbolsDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem7 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem8 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem4 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem5 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ckhishalfdaydefault = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.cbbOverTimeSymbol = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.chkIsHalfDay = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.txtSalaryRate = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.chkIsDefault = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.lblSalaryRate = new Infragistics.Win.Misc.UltraLabel();
            this.chkChiTiet = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.cbbChiTiet = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.txtTimeSheetSymbolsName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtTimeSheetSymbolsCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblTimeSheetSymbolsName = new Infragistics.Win.Misc.UltraLabel();
            this.lblTimeSheetSymbolsCode = new Infragistics.Win.Misc.UltraLabel();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.chkIsActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ckhishalfdaydefault)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbOverTimeSymbol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsHalfDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsDefault)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkChiTiet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbChiTiet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTimeSheetSymbolsName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTimeSheetSymbolsCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            appearance1.FontData.BoldAsString = "True";
            this.ultraGroupBox1.Appearance = appearance1;
            this.ultraGroupBox1.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox1.Controls.Add(this.ckhishalfdaydefault);
            this.ultraGroupBox1.Controls.Add(this.cbbOverTimeSymbol);
            this.ultraGroupBox1.Controls.Add(this.chkIsHalfDay);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox1.Controls.Add(this.txtSalaryRate);
            this.ultraGroupBox1.Controls.Add(this.chkIsDefault);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel4);
            this.ultraGroupBox1.Controls.Add(this.lblSalaryRate);
            this.ultraGroupBox1.Controls.Add(this.chkChiTiet);
            this.ultraGroupBox1.Controls.Add(this.cbbChiTiet);
            this.ultraGroupBox1.Controls.Add(this.txtTimeSheetSymbolsName);
            this.ultraGroupBox1.Controls.Add(this.txtTimeSheetSymbolsCode);
            this.ultraGroupBox1.Controls.Add(this.lblTimeSheetSymbolsName);
            this.ultraGroupBox1.Controls.Add(this.lblTimeSheetSymbolsCode);
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(1, 2);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(388, 220);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ckhishalfdaydefault
            // 
            appearance2.TextVAlignAsString = "Middle";
            this.ckhishalfdaydefault.Appearance = appearance2;
            this.ckhishalfdaydefault.BackColor = System.Drawing.Color.Transparent;
            this.ckhishalfdaydefault.BackColorInternal = System.Drawing.Color.Transparent;
            this.ckhishalfdaydefault.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.ckhishalfdaydefault.Location = new System.Drawing.Point(158, 191);
            this.ckhishalfdaydefault.Name = "ckhishalfdaydefault";
            this.ckhishalfdaydefault.Size = new System.Drawing.Size(216, 22);
            this.ckhishalfdaydefault.TabIndex = 27;
            this.ckhishalfdaydefault.Text = "Là ký hiệu nửa ngày mặc định";
            // 
            // cbbOverTimeSymbol
            // 
            this.cbbOverTimeSymbol.AutoSize = false;
            this.cbbOverTimeSymbol.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbOverTimeSymbol.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            valueListItem1.CheckState = System.Windows.Forms.CheckState.Checked;
            valueListItem1.DataValue = "";
            valueListItem2.DataValue = "0";
            valueListItem2.DisplayText = "Làm thêm ban ngày (ngày thường)";
            valueListItem7.DataValue = "1";
            valueListItem7.DisplayText = "Làm thêm ban ngày (ngày T7, CN)";
            valueListItem8.DataValue = "2";
            valueListItem8.DisplayText = "Làm thêm ban ngày (ngày lễ, Tết)";
            valueListItem3.DataValue = "3";
            valueListItem3.DisplayText = "Làm thêm ban đêm (ngày thường)";
            valueListItem4.DataValue = "4";
            valueListItem4.DisplayText = "Làm thêm ban đêm (ngày T7, CN)";
            valueListItem5.DataValue = "5";
            valueListItem5.DisplayText = "Làm thêm ban đêm (ngày lễ, Tết)";
            this.cbbOverTimeSymbol.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2,
            valueListItem7,
            valueListItem8,
            valueListItem3,
            valueListItem4,
            valueListItem5});
            this.cbbOverTimeSymbol.Location = new System.Drawing.Point(159, 81);
            this.cbbOverTimeSymbol.Name = "cbbOverTimeSymbol";
            this.cbbOverTimeSymbol.Size = new System.Drawing.Size(218, 22);
            this.cbbOverTimeSymbol.TabIndex = 26;
            this.cbbOverTimeSymbol.ValueChanged += new System.EventHandler(this.cbbOverTimeSymbol_ValueChanged);
            // 
            // chkIsHalfDay
            // 
            appearance3.TextVAlignAsString = "Middle";
            this.chkIsHalfDay.Appearance = appearance3;
            this.chkIsHalfDay.BackColor = System.Drawing.Color.Transparent;
            this.chkIsHalfDay.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsHalfDay.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkIsHalfDay.Location = new System.Drawing.Point(158, 163);
            this.chkIsHalfDay.Name = "chkIsHalfDay";
            this.chkIsHalfDay.Size = new System.Drawing.Size(216, 22);
            this.chkIsHalfDay.TabIndex = 25;
            this.chkIsHalfDay.Text = "Là ký hiệu nửa ngày";
            // 
            // ultraLabel1
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance4;
            this.ultraLabel1.Location = new System.Drawing.Point(8, 80);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(145, 22);
            this.ultraLabel1.TabIndex = 24;
            this.ultraLabel1.Text = "Là ký hiệu làm thêm";
            // 
            // txtSalaryRate
            // 
            this.txtSalaryRate.AutoSize = false;
            this.txtSalaryRate.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtSalaryRate.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.txtSalaryRate.InputMask = "{double:10.4}";
            this.txtSalaryRate.Location = new System.Drawing.Point(158, 109);
            this.txtSalaryRate.Name = "txtSalaryRate";
            this.txtSalaryRate.PromptChar = ' ';
            this.txtSalaryRate.Size = new System.Drawing.Size(174, 20);
            this.txtSalaryRate.TabIndex = 3;
            this.txtSalaryRate.TextChanged += new System.EventHandler(this.txtSalaryRate_TextChanged);
            // 
            // chkIsDefault
            // 
            appearance5.TextVAlignAsString = "Middle";
            this.chkIsDefault.Appearance = appearance5;
            this.chkIsDefault.BackColor = System.Drawing.Color.Transparent;
            this.chkIsDefault.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsDefault.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkIsDefault.Location = new System.Drawing.Point(158, 135);
            this.chkIsDefault.Name = "chkIsDefault";
            this.chkIsDefault.Size = new System.Drawing.Size(216, 22);
            this.chkIsDefault.TabIndex = 4;
            this.chkIsDefault.Text = "Là ký hiệu cả ngày mặc định";
            // 
            // ultraLabel4
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance6;
            this.ultraLabel4.Location = new System.Drawing.Point(347, 108);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(30, 22);
            this.ultraLabel4.TabIndex = 22;
            this.ultraLabel4.Text = "(%)";
            // 
            // lblSalaryRate
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.TextVAlignAsString = "Middle";
            this.lblSalaryRate.Appearance = appearance7;
            this.lblSalaryRate.Location = new System.Drawing.Point(8, 108);
            this.lblSalaryRate.Name = "lblSalaryRate";
            this.lblSalaryRate.Size = new System.Drawing.Size(120, 22);
            this.lblSalaryRate.TabIndex = 16;
            this.lblSalaryRate.Text = "Tỷ lệ hưởng lương";
            // 
            // chkChiTiet
            // 
            this.chkChiTiet.BackColor = System.Drawing.Color.Transparent;
            this.chkChiTiet.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkChiTiet.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkChiTiet.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkChiTiet.Location = new System.Drawing.Point(9, 275);
            this.chkChiTiet.Name = "chkChiTiet";
            this.chkChiTiet.Size = new System.Drawing.Size(101, 20);
            this.chkChiTiet.TabIndex = 11;
            this.chkChiTiet.Text = "Chi tiết theo";
            // 
            // cbbChiTiet
            // 
            this.cbbChiTiet.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbChiTiet.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbbChiTiet.Location = new System.Drawing.Point(129, 275);
            this.cbbChiTiet.Name = "cbbChiTiet";
            this.cbbChiTiet.Size = new System.Drawing.Size(245, 21);
            this.cbbChiTiet.TabIndex = 10;
            // 
            // txtTimeSheetSymbolsName
            // 
            this.txtTimeSheetSymbolsName.AutoSize = false;
            this.txtTimeSheetSymbolsName.Location = new System.Drawing.Point(158, 54);
            this.txtTimeSheetSymbolsName.MaxLength = 512;
            this.txtTimeSheetSymbolsName.Name = "txtTimeSheetSymbolsName";
            this.txtTimeSheetSymbolsName.Size = new System.Drawing.Size(219, 21);
            this.txtTimeSheetSymbolsName.TabIndex = 2;
            // 
            // txtTimeSheetSymbolsCode
            // 
            this.txtTimeSheetSymbolsCode.AutoSize = false;
            this.txtTimeSheetSymbolsCode.Location = new System.Drawing.Point(158, 27);
            this.txtTimeSheetSymbolsCode.MaxLength = 25;
            this.txtTimeSheetSymbolsCode.Name = "txtTimeSheetSymbolsCode";
            this.txtTimeSheetSymbolsCode.Size = new System.Drawing.Size(219, 21);
            this.txtTimeSheetSymbolsCode.TabIndex = 1;
            // 
            // lblTimeSheetSymbolsName
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.TextVAlignAsString = "Middle";
            this.lblTimeSheetSymbolsName.Appearance = appearance8;
            this.lblTimeSheetSymbolsName.Location = new System.Drawing.Point(8, 53);
            this.lblTimeSheetSymbolsName.Name = "lblTimeSheetSymbolsName";
            this.lblTimeSheetSymbolsName.Size = new System.Drawing.Size(145, 22);
            this.lblTimeSheetSymbolsName.TabIndex = 20;
            this.lblTimeSheetSymbolsName.Text = "Diễn giải";
            // 
            // lblTimeSheetSymbolsCode
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.TextVAlignAsString = "Middle";
            this.lblTimeSheetSymbolsCode.Appearance = appearance9;
            this.lblTimeSheetSymbolsCode.Location = new System.Drawing.Point(8, 26);
            this.lblTimeSheetSymbolsCode.Name = "lblTimeSheetSymbolsCode";
            this.lblTimeSheetSymbolsCode.Size = new System.Drawing.Size(144, 22);
            this.lblTimeSheetSymbolsCode.TabIndex = 21;
            this.lblTimeSheetSymbolsCode.Text = "Ký hiệu (*)";
            // 
            // btnSave
            // 
            appearance10.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance10;
            this.btnSave.Location = new System.Drawing.Point(216, 228);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            appearance11.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance11;
            this.btnClose.Location = new System.Drawing.Point(303, 228);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 7;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // chkIsActive
            // 
            appearance12.TextVAlignAsString = "Middle";
            this.chkIsActive.Appearance = appearance12;
            this.chkIsActive.BackColor = System.Drawing.Color.Transparent;
            this.chkIsActive.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsActive.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkIsActive.Checked = true;
            this.chkIsActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIsActive.Location = new System.Drawing.Point(5, 232);
            this.chkIsActive.Name = "chkIsActive";
            this.chkIsActive.Size = new System.Drawing.Size(110, 22);
            this.chkIsActive.TabIndex = 5;
            this.chkIsActive.Text = "Hoạt động";
            // 
            // FTimeSheetSymbolsDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(390, 265);
            this.Controls.Add(this.chkIsActive);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.ultraGroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FTimeSheetSymbolsDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FTimeSheetSymbolsDetail_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ckhishalfdaydefault)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbOverTimeSymbol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsHalfDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsDefault)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkChiTiet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbChiTiet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTimeSheetSymbolsName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTimeSheetSymbolsCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraLabel lblTimeSheetSymbolsCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTimeSheetSymbolsName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTimeSheetSymbolsCode;
        private Infragistics.Win.Misc.UltraLabel lblTimeSheetSymbolsName;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkChiTiet;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbChiTiet;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel lblSalaryRate;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsActive;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsDefault;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtSalaryRate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsHalfDay;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbOverTimeSymbol;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor ckhishalfdaydefault;
    }
}
