﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using Accounting.TextMessage;
using System.Linq;

namespace Accounting
{
    public partial class FType : CatalogBase
    {
        #region khai báo
        private readonly ITypeService _ITypeService;
        private readonly ITypeGroupService _ITypeGroupService;
        #endregion
        #region khởi tạo
        public FType()
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;

            _ITypeService = IoC.Resolve<ITypeService>();
            _ITypeGroupService = IoC.Resolve<ITypeGroupService>();
            //StyleManager.Load(Application.StartupPath + @"\..\Office2010Blue.isl");
            this.uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;

            LoadDuLieu();
            this.CenterToScreen();
        #endregion
        }
        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }
        private void LoadDuLieu(bool configGrid)
        {
            WaitingFrm.StartWaiting();
            #region Lấy dữ liệu từ CSDL
            List<Core.Domain.Type> list = _ITypeService.GetAll_OrderBy();
            _ITypeService.UnbindSession(list);
            list = _ITypeService.GetAll_OrderBy();
            List<TypeGroup> listGroup = _ITypeGroupService.GetAll();
            #endregion

            #region Xử lý dữ liệu
            #endregion

            #region hiển thị và xử lý hiển thị
            uGrid.DataSource = list.ToArray();
            if (uGrid.Rows.Count > 0)
            {
                uGrid.Rows[0].Selected = true;
            }
            if (configGrid) ConfigGrid(uGrid);

            foreach (var item in list)
            {
                if (item.TypeGroupID == null) { }
                else
                {
                    item.TypeGroupViewID = listGroup.Where(p => p.ID == item.TypeGroupID).SingleOrDefault().TypeGroupName;
                }
            }
            #endregion
            WaitingFrm.StopWaiting();
        }

        #region Utils
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, TextMessage.ConstDatabase.Type_TableName);
        }
        #endregion


        #region Nghiệp vụ
        /// Nghiệp vụ Delete
        /// </summary>
        protected override void DeleteFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                Accounting.Core.Domain.Type temp = uGrid.Selected.Rows[0].ListObject as Accounting.Core.Domain.Type;
                if (MSG.Question(string.Format(resSystem.MSG_System_05, temp.ID)) == System.Windows.Forms.DialogResult.Yes)
                {
                    _ITypeService.BeginTran();
                    _ITypeService.Delete(temp);
                    try
                    {
                        _ITypeService.CommitTran();
                    }
                    catch (Exception ex)
                    {
                        //if (temp.ID == 100)
                        //{
                        MSG.Error(string.Format(resSystem.MSG_Catalog_FType, temp.TypeGroupViewID));
                        return;
                        //}

                    }
                    Utils.ClearCacheByType<Accounting.Core.Domain.Type>();
                    LoadDuLieu();
                }
            }
            else
                MSG.Error(string.Format(resSystem.MSG_Catalog2,"một Loại chứng từ"));
            
        }

        /// Nghiệp vụ Them moi
        protected override void AddFunction()
        {
            new FdTypeGroupDetail().ShowDialog(this);
            if (!FdTypeGroupDetail.isClose) LoadDuLieu();
        }

        /// Nghiệp vụ Update
        protected override void EditFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                Accounting.Core.Domain.Type temp = uGrid.Selected.Rows[0].ListObject as Accounting.Core.Domain.Type;
                new FdTypeGroupDetail(temp).ShowDialog(this);
                if (!FdTypeGroupDetail.isClose) LoadDuLieu();

            }
            else
                Accounting.TextMessage.MSG.Error(string.Format(Accounting.TextMessage.resSystem.MSG_Catalog3,"một Loại chứng từ"));
        }
        #endregion

        #region Button
        private void btnThem_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }
        #endregion

        #region Event
        private void tsmAdd_Click(object sender, EventArgs e)
        {
            AddFunction();

        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void tsmEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            ResetFunction();
        }
        protected override void ResetFunction()
        {
            LoadDuLieu();
        }

        private void uGrid_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (e.Row.Index == -1) return;
            EditFunction();
        }
        #endregion

        private void FType_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGrid.ResetText();
            uGrid.ResetUpdateMode();
            uGrid.ResetExitEditModeOnLeave();
            uGrid.ResetRowUpdateCancelAction();
            uGrid.DataSource = null;
            uGrid.Layouts.Clear();
            uGrid.ResetLayouts();
            uGrid.ResetDisplayLayout();
            uGrid.Refresh();
            uGrid.ClearUndoHistory();
            uGrid.ClearXsdConstraints();
        }

        private void FType_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
