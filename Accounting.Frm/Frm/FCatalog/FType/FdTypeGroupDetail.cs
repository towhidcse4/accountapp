﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Accounting.TextMessage;
using System.Text.RegularExpressions;
namespace Accounting
{
    public partial class FdTypeGroupDetail : DialogForm
    {
        #region  khai báo
        private readonly ITypeService _ITypeService;
        private readonly ITypeGroupService _ITypeGroupService;
        public static bool isClose = true;
        bool Them = true;
        Accounting.Core.Domain.Type _Select = new Accounting.Core.Domain.Type();
        #endregion
        #region Khởi tạo
        public FdTypeGroupDetail()
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Text = "Thêm mới loại chứng từ";
            this.CenterToParent();
            #endregion
            _ITypeGroupService = IoC.Resolve<ITypeGroupService>();
            _ITypeService = IoC.Resolve<ITypeService>();
            displayTypeGroup();

            this.CenterToScreen();
        }

        public FdTypeGroupDetail(Accounting.Core.Domain.Type temp)
        {//Sửa
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            this.Text = "Sửa loại chứng từ";
            //this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.CenterToParent();
            txtID.Enabled = false;
            #endregion
            _ITypeGroupService = IoC.Resolve<ITypeGroupService>();
            #region Thiết lập ban đầu cho Form
            _Select = temp;
            Them = false;
            //Khai báo các webservices
            _ITypeService = IoC.Resolve<ITypeService>();
            #endregion

            displayTypeGroup();

            #region Fill dữ liệu Obj vào control
            Objandint(temp, false);
            #endregion
        }
        #endregion
        #region Khởi tạo Ham`
        public void displayTypeGroup()
        {
            cbbTypeGroup.DisplayMember = "TypeGroupName";
            cbbTypeGroup.DataSource = _ITypeGroupService.GetAll();
            Utils.ConfigGrid(cbbTypeGroup, ConstDatabase.TypeGroup_TableName);
        }
        #endregion
        #region Button
        private void btnluu_Click(object sender, EventArgs e)
        {
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion

            #region Kquantam
            //string ID = txtID.Text;
            //string Name = txtTypeName.Text;
            //string valCbbGroup = CbbGroup.SelectedItem.DataValue.ToString();
            //string GroupType=CbbGroup.SelectedIndex.ToString();
            //string recorda;
            //if (Rd_Recordable.CheckedItem.DisplayText.Equals("True"))
            //{
            //    recorda = 0.ToString();
            //}
            //else
            //{
            //    recorda = 1.ToString();
            //}
            //string seach;
            //if (rd_Serchchable.CheckedItem.DisplayText.Equals("True"))
            //{
            //    seach = 0.ToString();
            //}
            //else
            //{
            //    seach = 1.ToString();
            //}
            //string Posttype = txtPostType.Text;
            //string Oder = txtOrder.Text;

            //MessageBox.Show("ID: " + ID + "\n" + "Name:" + Name + "\n" + "Group ID:" + valCbbGroup + "\n" + "Posttype :" + Posttype + "\n" + "OrderPriority:" + Oder + "\n" + "Recordable:" + recorda + "\n" + "Searchable :" + seach);
            // Gui to object
            #endregion

            #region Fill dữ liệu control vào obj
            Accounting.Core.Domain.Type temp = Them ? new Accounting.Core.Domain.Type() : _ITypeService.Getbykey(_Select.ID);
            temp = Objandint(temp, true);
            #endregion

            #region Thao tác CSDL
            _ITypeService.BeginTran();
            if (Them) _ITypeService.CreateNew(temp);
            else _ITypeService.Update(temp);
            _ITypeService.CommitTran();
            #endregion

            #region xử lý form, kết thúc form
            this.Close();
            isClose = false;
            #endregion
        }

        private void btnthoat_Click(object sender, EventArgs e)
        {
            isClose = true;
            this.Close();
            
        }
        #endregion
        bool CheckError()
        {
            bool kq = true;
            //Check Error Chung

            //Check Error Riêng từng Form
            if (string.IsNullOrEmpty(txtID.Text) || string.IsNullOrEmpty(txtTypeName.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                txtTypeName.Focus();
                return false;
            }
            //Bắt kiếu số cho mã chứng từ
            string patentID = "^\\d+$";
            Regex objNotWholePatternID = new Regex(patentID);
            Match ma = objNotWholePatternID.Match(txtID.Text);

            if (!ma.Success)
            {
                MSG.Error(resSystem.MSG_Catalog_FdTypeGroupDetail);
                return false;
            }


            //List<int> list = _ITypeService.Query.Select(p => p.ID).ToList();
            List<int> list = _ITypeService.GetListId();

            foreach (var item in list)
            {
                int textID = 0;
                int.TryParse(txtID.Text, out textID);
                if (item == textID && Them)
                {
                    MSG.Warning(string.Format(resSystem.MSG_Catalog, "Mã", item));
                    return false;
                }

            }
            //  kiểm Phải chọn Loại sổ lưu
            //if (cbbTypeGroup.Value==null)
            //{
            //    MSG.Error("Phải chọn chứng từ ");
            //    return false;
            //}
            if (!txtPostType.Text.Equals(""))
            {
                string patent = "^\\d+$";
                Regex objNotWholePattern = new Regex(patent);
                // Bắt kiếu số cho loại sổ lưu
                Match m = objNotWholePattern.Match(txtPostType.Text);
                if (!m.Success)
                {
                    MSG.Warning(resSystem.MSG_Catalog_FdTypeGroupDetail1);
                    txtPostType.Focus();
                    return false;

                }
            }
            else
            {


            }

            return kq;

        }

        #region Utils
        /// <summary>
        /// get hoặc set một đối tượng vào giao diện xử lý
        /// </summary>
        /// <param name="input">đối tượng</param>
        /// <param name="isGet">True: get giá trị từ control gán vào đối tượng| False: set giá trị đối tượng vào control</param>
        /// <returns></returns>
        Accounting.Core.Domain.Type Objandint(Accounting.Core.Domain.Type input, bool isGet)
        {
            if (isGet)
            {
                #region SetGiá trị
                int textID = 0;
                int.TryParse(txtID.Text, out textID);
                input.ID = textID;
                input.TypeName = txtTypeName.Text;
                //input.TypeGroupID = int.Parse(cbbTypeGroup.Text);
                if (cbbTypeGroup.Value == null)
                {
                    input.TypeGroupID = null;
                }
                else
                {
                    input.TypeGroupID = ((Accounting.Core.Domain.TypeGroup)Utils.getSelectCbbItem(cbbTypeGroup)).ID;
                }
                string recorda;
                if (Rd_Recordable.CheckedItem.DisplayText.Trim().Equals("Có"))
                // if (Rd_Recordable.)
                {
                    recorda = "True";
                }
                else
                {
                    recorda = "False";
                }
                string seach;
                if (rd_Serchchable.CheckedItem.DisplayText.Trim().Equals("Có"))
                {
                    seach = "True";
                }
                else
                {
                    seach = "False";
                }
                input.Recordable = bool.Parse(recorda.ToString());
                input.Searchable = bool.Parse(seach.ToString());
                if (!txtPostType.Text.Equals(""))
                {
                    input.PostType = int.Parse(txtPostType.Text);
                }
                else
                {
                    input.PostType = null;
                }
                input.OrderPriority = 0;
                //input.Address = txtAddress.Text;
                #endregion
            }
            else
            {

                txtID.Text = input.ID.ToString();
                txtTypeName.Text = input.TypeName;
                cbbTypeGroup.Text = input.TypeGroupID.ToString();
                if (!txtPostType.Text.Equals(""))
                {
                    txtPostType.Text = input.PostType.ToString();
                }
                else
                {
                    input.PostType = null;
                }

                // txtOrder.Text = input.OrderPriority.ToString();
                Rd_Recordable.CheckedIndex = input.Recordable ? 1 : 0;
                rd_Serchchable.CheckedIndex = input.Searchable ? 1 : 0;
                //nhóm 

                foreach (var item in cbbTypeGroup.Rows)
                {
                    if ((item.ListObject as TypeGroup).ID == input.TypeGroupID) cbbTypeGroup.SelectedRow = item;

                }

                this.CenterToScreen();
            }
            return input;
        }

        #endregion

        private void FdTypeGroupDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
