﻿namespace Accounting
{
    partial class FdTypeGroupDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem4 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtPostType = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbTypeGroup = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.rd_Serchchable = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.Rd_Recordable = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.txtTypeName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPostType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbTypeGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rd_Serchchable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Rd_Recordable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTypeName)).BeginInit();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox1.Controls.Add(this.txtPostType);
            this.ultraGroupBox1.Controls.Add(this.txtID);
            this.ultraGroupBox1.Controls.Add(this.cbbTypeGroup);
            this.ultraGroupBox1.Controls.Add(this.ultraGroupBox3);
            this.ultraGroupBox1.Controls.Add(this.ultraGroupBox2);
            this.ultraGroupBox1.Controls.Add(this.txtTypeName);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel6);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel5);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel4);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel3);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            appearance24.FontData.BoldAsString = "True";
            this.ultraGroupBox1.HeaderAppearance = appearance24;
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(391, 203);
            this.ultraGroupBox1.TabIndex = 1;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtPostType
            // 
            this.txtPostType.AutoSize = false;
            this.txtPostType.Location = new System.Drawing.Point(149, 169);
            this.txtPostType.MaxLength = 25;
            this.txtPostType.Name = "txtPostType";
            this.txtPostType.Size = new System.Drawing.Size(230, 22);
            this.txtPostType.TabIndex = 5;
            // 
            // txtID
            // 
            this.txtID.AutoSize = false;
            this.txtID.Location = new System.Drawing.Point(149, 24);
            this.txtID.MaxLength = 10;
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(230, 22);
            this.txtID.TabIndex = 0;
            // 
            // cbbTypeGroup
            // 
            this.cbbTypeGroup.AutoSize = false;
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbTypeGroup.DisplayLayout.Appearance = appearance1;
            this.cbbTypeGroup.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbTypeGroup.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbTypeGroup.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbTypeGroup.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.cbbTypeGroup.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbTypeGroup.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.cbbTypeGroup.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbTypeGroup.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbTypeGroup.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbTypeGroup.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.cbbTypeGroup.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbTypeGroup.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.cbbTypeGroup.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbTypeGroup.DisplayLayout.Override.CellAppearance = appearance8;
            this.cbbTypeGroup.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbTypeGroup.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbTypeGroup.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.cbbTypeGroup.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.cbbTypeGroup.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbTypeGroup.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.cbbTypeGroup.DisplayLayout.Override.RowAppearance = appearance11;
            this.cbbTypeGroup.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbTypeGroup.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.cbbTypeGroup.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbTypeGroup.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbTypeGroup.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbTypeGroup.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbTypeGroup.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbTypeGroup.Location = new System.Drawing.Point(149, 76);
            this.cbbTypeGroup.Name = "cbbTypeGroup";
            this.cbbTypeGroup.NullText = "< Chọn dữ liệu >";
            this.cbbTypeGroup.Size = new System.Drawing.Size(230, 22);
            this.cbbTypeGroup.TabIndex = 2;
            // 
            // ultraGroupBox3
            // 
            appearance13.BackColor = System.Drawing.Color.Transparent;
            this.ultraGroupBox3.Appearance = appearance13;
            this.ultraGroupBox3.Controls.Add(this.rd_Serchchable);
            this.ultraGroupBox3.Location = new System.Drawing.Point(149, 134);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(230, 29);
            this.ultraGroupBox3.TabIndex = 4;
            this.ultraGroupBox3.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // rd_Serchchable
            // 
            appearance14.BackColor = System.Drawing.Color.Transparent;
            this.rd_Serchchable.Appearance = appearance14;
            this.rd_Serchchable.BackColor = System.Drawing.Color.Transparent;
            this.rd_Serchchable.BackColorInternal = System.Drawing.Color.Transparent;
            this.rd_Serchchable.CheckedIndex = 0;
            appearance15.BackColor = System.Drawing.Color.Transparent;
            valueListItem1.Appearance = appearance15;
            valueListItem1.DataValue = "0";
            valueListItem1.DisplayText = "Không";
            valueListItem2.DataValue = "1";
            valueListItem2.DisplayText = "Có";
            this.rd_Serchchable.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2});
            this.rd_Serchchable.Location = new System.Drawing.Point(6, 6);
            this.rd_Serchchable.Name = "rd_Serchchable";
            this.rd_Serchchable.Size = new System.Drawing.Size(230, 22);
            this.rd_Serchchable.TabIndex = 4;
            this.rd_Serchchable.Text = "Không";
            // 
            // ultraGroupBox2
            // 
            appearance16.BackColor = System.Drawing.Color.Transparent;
            this.ultraGroupBox2.Appearance = appearance16;
            this.ultraGroupBox2.Controls.Add(this.Rd_Recordable);
            this.ultraGroupBox2.Location = new System.Drawing.Point(149, 104);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(230, 29);
            this.ultraGroupBox2.TabIndex = 3;
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // Rd_Recordable
            // 
            appearance17.BackColor = System.Drawing.Color.Transparent;
            this.Rd_Recordable.Appearance = appearance17;
            this.Rd_Recordable.BackColor = System.Drawing.Color.Transparent;
            this.Rd_Recordable.BackColorInternal = System.Drawing.Color.Transparent;
            this.Rd_Recordable.CheckedIndex = 0;
            valueListItem3.DataValue = "1";
            valueListItem3.DisplayText = "Không";
            valueListItem4.DataValue = "0";
            valueListItem4.DisplayText = "Có";
            this.Rd_Recordable.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem3,
            valueListItem4});
            this.Rd_Recordable.Location = new System.Drawing.Point(6, 5);
            this.Rd_Recordable.Name = "Rd_Recordable";
            this.Rd_Recordable.Size = new System.Drawing.Size(230, 22);
            this.Rd_Recordable.TabIndex = 3;
            this.Rd_Recordable.Text = "Không";
            // 
            // txtTypeName
            // 
            this.txtTypeName.AutoSize = false;
            this.txtTypeName.Location = new System.Drawing.Point(149, 49);
            this.txtTypeName.MaxLength = 512;
            this.txtTypeName.Name = "txtTypeName";
            this.txtTypeName.Size = new System.Drawing.Size(230, 22);
            this.txtTypeName.TabIndex = 1;
            // 
            // ultraLabel6
            // 
            appearance18.BackColor = System.Drawing.Color.Transparent;
            appearance18.TextVAlignAsString = "Middle";
            this.ultraLabel6.Appearance = appearance18;
            this.ultraLabel6.Location = new System.Drawing.Point(11, 169);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel6.TabIndex = 5;
            this.ultraLabel6.Text = "Loại sổ lưu";
            // 
            // ultraLabel5
            // 
            appearance19.BackColor = System.Drawing.Color.Transparent;
            appearance19.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance19;
            this.ultraLabel5.Location = new System.Drawing.Point(11, 140);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(132, 22);
            this.ultraLabel5.TabIndex = 4;
            this.ultraLabel5.Text = "Cho xuất hiện / tìm kiếm";
            // 
            // ultraLabel4
            // 
            appearance20.BackColor = System.Drawing.Color.Transparent;
            appearance20.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance20;
            this.ultraLabel4.Location = new System.Drawing.Point(11, 111);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(111, 22);
            this.ultraLabel4.TabIndex = 3;
            this.ultraLabel4.Text = "Chức năng ghi sổ";
            // 
            // ultraLabel3
            // 
            appearance21.BackColor = System.Drawing.Color.Transparent;
            appearance21.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance21;
            this.ultraLabel3.Location = new System.Drawing.Point(11, 76);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel3.TabIndex = 2;
            this.ultraLabel3.Text = "Nhóm chứng từ";
            // 
            // ultraLabel2
            // 
            appearance22.BackColor = System.Drawing.Color.Transparent;
            appearance22.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance22;
            this.ultraLabel2.Location = new System.Drawing.Point(11, 49);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(111, 22);
            this.ultraLabel2.TabIndex = 1;
            this.ultraLabel2.Text = "Tên loại chứng từ (*)";
            // 
            // ultraLabel1
            // 
            appearance23.BackColor = System.Drawing.Color.Transparent;
            appearance23.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance23;
            this.ultraLabel1.Location = new System.Drawing.Point(11, 24);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel1.TabIndex = 0;
            this.ultraLabel1.Text = "Mã chứng từ (*)";
            // 
            // ultraPanel1
            // 
            appearance25.BackColor = System.Drawing.Color.Transparent;
            this.ultraPanel1.Appearance = appearance25;
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.btnClose);
            this.ultraPanel1.ClientArea.Controls.Add(this.btnSave);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 203);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(391, 41);
            this.ultraPanel1.TabIndex = 8;
            // 
            // btnClose
            // 
            appearance26.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance26;
            this.btnClose.Location = new System.Drawing.Point(304, 6);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 62;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnthoat_Click);
            // 
            // btnSave
            // 
            appearance27.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance27;
            this.btnSave.Location = new System.Drawing.Point(223, 6);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 61;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnluu_Click);
            // 
            // FdTypeGroupDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(391, 244);
            this.Controls.Add(this.ultraPanel1);
            this.Controls.Add(this.ultraGroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FdTypeGroupDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Chi tiết loại chứng từ";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FdTypeGroupDetail_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtPostType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbTypeGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rd_Serchchable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Rd_Recordable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTypeName)).EndInit();
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet rd_Serchchable;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet Rd_Recordable;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTypeName;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbTypeGroup;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtPostType;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnSave;
    }
}