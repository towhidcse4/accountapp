﻿namespace Accounting
{
    partial class FShareHolderGroupDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.chkIsActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtShareHolderGroupName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblShareHolderGroupName = new Infragistics.Win.Misc.UltraLabel();
            this.txtShareHolderGroupCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblShareHolderGroupCode = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtShareHolderGroupName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShareHolderGroupCode)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            appearance1.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance1;
            this.btnSave.Location = new System.Drawing.Point(210, 93);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            appearance2.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance2;
            this.btnClose.Location = new System.Drawing.Point(297, 93);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 5;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // chkIsActive
            // 
            this.chkIsActive.BackColor = System.Drawing.Color.Transparent;
            this.chkIsActive.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsActive.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkIsActive.Checked = true;
            this.chkIsActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIsActive.Location = new System.Drawing.Point(9, 98);
            this.chkIsActive.Name = "chkIsActive";
            this.chkIsActive.Size = new System.Drawing.Size(115, 22);
            this.chkIsActive.TabIndex = 3;
            this.chkIsActive.Text = "Hoạt động";
            // 
            // ultraGroupBox1
            // 
            appearance3.FontData.BoldAsString = "True";
            this.ultraGroupBox1.Appearance = appearance3;
            this.ultraGroupBox1.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox1.Controls.Add(this.txtShareHolderGroupName);
            this.ultraGroupBox1.Controls.Add(this.lblShareHolderGroupName);
            this.ultraGroupBox1.Controls.Add(this.txtShareHolderGroupCode);
            this.ultraGroupBox1.Controls.Add(this.lblShareHolderGroupCode);
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(1, 2);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(377, 85);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtShareHolderGroupName
            // 
            this.txtShareHolderGroupName.AutoSize = false;
            this.txtShareHolderGroupName.Location = new System.Drawing.Point(134, 54);
            this.txtShareHolderGroupName.MaxLength = 512;
            this.txtShareHolderGroupName.Name = "txtShareHolderGroupName";
            this.txtShareHolderGroupName.Size = new System.Drawing.Size(237, 22);
            this.txtShareHolderGroupName.TabIndex = 2;
            // 
            // lblShareHolderGroupName
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            this.lblShareHolderGroupName.Appearance = appearance4;
            this.lblShareHolderGroupName.Location = new System.Drawing.Point(9, 54);
            this.lblShareHolderGroupName.Name = "lblShareHolderGroupName";
            this.lblShareHolderGroupName.Size = new System.Drawing.Size(126, 22);
            this.lblShareHolderGroupName.TabIndex = 0;
            this.lblShareHolderGroupName.Text = "Tên nhóm cổ đông (*)";
            // 
            // txtShareHolderGroupCode
            // 
            this.txtShareHolderGroupCode.AutoSize = false;
            this.txtShareHolderGroupCode.Location = new System.Drawing.Point(134, 27);
            this.txtShareHolderGroupCode.MaxLength = 25;
            this.txtShareHolderGroupCode.Name = "txtShareHolderGroupCode";
            this.txtShareHolderGroupCode.Size = new System.Drawing.Size(237, 22);
            this.txtShareHolderGroupCode.TabIndex = 1;
            // 
            // lblShareHolderGroupCode
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            this.lblShareHolderGroupCode.Appearance = appearance5;
            this.lblShareHolderGroupCode.Location = new System.Drawing.Point(9, 27);
            this.lblShareHolderGroupCode.Name = "lblShareHolderGroupCode";
            this.lblShareHolderGroupCode.Size = new System.Drawing.Size(126, 22);
            this.lblShareHolderGroupCode.TabIndex = 0;
            this.lblShareHolderGroupCode.Text = "Mã nhóm cổ đông (*)";
            // 
            // FShareHolderGroupDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(378, 131);
            this.Controls.Add(this.ultraGroupBox1);
            this.Controls.Add(this.chkIsActive);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FShareHolderGroupDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FShareHolderGroupDetail_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtShareHolderGroupName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShareHolderGroupCode)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsActive;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtShareHolderGroupCode;
        private Infragistics.Win.Misc.UltraLabel lblShareHolderGroupCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtShareHolderGroupName;
        private Infragistics.Win.Misc.UltraLabel lblShareHolderGroupName;
    }
}
