﻿using System;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using System.Linq;
using System.Collections.Generic;

namespace Accounting
{
    public partial class FShareHolderGroupDetail : DialogForm //UserControl
    {
        #region khai báo
        private readonly IShareHolderGroupService _IShareHolderGroupService;

        ShareHolderGroup _Select = new ShareHolderGroup();

        bool Them = true;
        public static bool isClose = true;
        #endregion

        #region khởi tạo
        public FShareHolderGroupDetail()
        {//Thêm
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            //Khai báo các webservices
            _IShareHolderGroupService = IoC.Resolve<IShareHolderGroupService>();
            this.Text = "Thêm mới nhóm cổ đông";
            chkIsActive.Visible = false;
            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion
        }

        public FShareHolderGroupDetail(ShareHolderGroup temp)
        {//Sửa
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _Select = temp;
            Them = false;
            txtShareHolderGroupCode.Enabled = false;
            this.Text = "Sửa nhóm cổ đông";
            //Khai báo các webservices
            _IShareHolderGroupService = IoC.Resolve<IShareHolderGroupService>();

            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion

            #region Fill dữ liệu Obj vào control
            ObjandGUI(temp, false);
            #endregion
        }

        private void InitializeGUI()
        {
            //
        }
        #endregion

        #region Button Event
        /// <summary>
        /// Sự kiện nút Lưu lại
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion

            //Gui to object
            #region Fill dữ liệu control vào obj
            ShareHolderGroup temp = Them ? new ShareHolderGroup() : _IShareHolderGroupService.Getbykey(_Select.ID);
            temp = ObjandGUI(temp, true);
            #endregion

            #region Thao tác CSDL

            _IShareHolderGroupService.BeginTran();
            if (Them)
            {
                if (!CheckCode()) return;
                temp.IsSecurity = false;
                _IShareHolderGroupService.CreateNew(temp);
            }
            else
            {
                if (!CheckError()) return;
                _IShareHolderGroupService.Update(temp);
            }
            _IShareHolderGroupService.CommitTran();

            #endregion

            #region xử lý form, kết thúc form
            this.Close();
            isClose = false;
            #endregion
        }

        /// <summary>
        /// Sự kiện nút đóng
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            isClose = true;
            this.Close();
            
        }
        #endregion

        #region Utils
        /// <summary>
        /// get hoặc set một đối tượng vào giao diện xử lý
        /// </summary>
        /// <param name="input">đối tượng</param>
        /// <param name="isGet">True: get giá trị từ control gán vào đối tượng| False: set giá trị đối tượng vào control</param>
        /// <returns></returns>
        ShareHolderGroup ObjandGUI(ShareHolderGroup input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();  //Thêm mới

                input.ShareHolderGroupCode = txtShareHolderGroupCode.Text;
                input.ShareHolderGroupName = txtShareHolderGroupName.Text;
                input.IsActive = chkIsActive.Checked;
            }
            else
            {
                txtShareHolderGroupCode.Text = input.ShareHolderGroupCode;
                txtShareHolderGroupName.Text = input.ShareHolderGroupName;
                chkIsActive.Checked = input.IsActive;
            }
            return input;
        }

        //check error
        bool CheckError()
        {
            bool kq = true;
            if (string.IsNullOrEmpty(txtShareHolderGroupCode.Text) || string.IsNullOrEmpty(txtShareHolderGroupName.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }

            return kq;
        }

        bool CheckCode()
        {
            bool kq = true;
            if (string.IsNullOrEmpty(txtShareHolderGroupCode.Text) || string.IsNullOrEmpty(txtShareHolderGroupName.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }
            //check trùng code
            //List<string> lst = _IShareHolderGroupService.GetAll().Select(c => c.ShareHolderGroupCode).ToList();
            List<string> lst = _IShareHolderGroupService.GetShareHolderGroupCode(); //Duy đã sửa chỗ này.
            foreach (var x in lst)
            {
                if (txtShareHolderGroupCode.Text.Equals(x))
                {
                    MSG.Warning(string.Format(resSystem.MSG_Catalog, "Mã", "nhóm cổ đông"));
                    return false;
                }
            }

            return kq;
        }
        #endregion

        private void FShareHolderGroupDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
