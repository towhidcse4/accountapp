﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class CatalogBase : CustormForm
    {
        /// <summary>
        /// Đăng ký Shortcut Keys cho Form
        /// </summary>
        /// <param name="message"></param>
        /// <param name="keys"></param>
        /// <returns></returns>
        protected override bool ProcessCmdKey(ref Message message, Keys keys)
        {
            switch (keys)
            {
                case Keys.Control | Keys.N:
                    {//thêm mới
                        AddFunction();
                        return true;
                    }
                case Keys.Control | Keys.D:
                    {//xóa
                        DeleteFunction();
                        return true;
                    }
                case Keys.Control | Keys.E:
                    {//xem
                        EditFunction();
                        return true;
                    }
                case Keys.F1:
                    {//Giúp
                        HelpFunction();
                        return true;
                    }
                case Keys.Control | Keys.A:
                    {//Chọn hết các trường
                        //_notEvent = true;
                        //foreach (UltraGridRow ultraGridRow in uGrid.Rows)
                        //    uGrid.Selected.Rows.Add(ultraGridRow);
                        //_notEvent = false;
                        return true;
                    }
                case Keys.F5:
                    {//Làm mới
                        ResetFunction();
                        return true;
                    }
            }
            return base.ProcessCmdKey(ref message, keys);
        }

        protected virtual void DeleteFunction()
        {
            //throw new NotImplementedException();
        }

        protected virtual void EditFunction()
        {
            //throw new NotImplementedException();
        }

        protected virtual void HelpFunction()
        {
            //throw new NotImplementedException();
        }

        protected virtual void ResetFunction()
        {
            //throw new NotImplementedException();
        }

        protected virtual void AddFunction()
        {
            //throw new NotImplementedException();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Activate();
            Focus();
            FocusTree(this);
        }

        void FocusTree(Control @this)
        {
            foreach (Control control in @this.Controls)
            {
                if (control is Infragistics.Win.UltraWinTree.UltraTree)
                {
                    control.Focus();
                    this.ActiveControl = control;
                }
                else if (control.HasChildren)
                    FocusTree(control);
            }
        }
    }
}
