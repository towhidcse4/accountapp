﻿namespace Accounting
{
    partial class FFixedAssetDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem4 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem5 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem6 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem7 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem8 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem9 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem10 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance89 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance90 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance91 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance92 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance93 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance94 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance95 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance96 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance97 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance98 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance99 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance100 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance101 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance102 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance103 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance104 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance105 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance106 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance107 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance108 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance109 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance110 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance111 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance112 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance113 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance114 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance115 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance116 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance117 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance118 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance119 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance120 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance121 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance122 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance123 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance124 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance125 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance126 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance127 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance128 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance129 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance130 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance131 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance132 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance133 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance134 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance135 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance136 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance137 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance138 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance139 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance140 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance141 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance142 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance143 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance144 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance145 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance146 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance147 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance148 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance149 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance150 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance151 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance152 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance153 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance154 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance155 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance156 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance157 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance158 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab6 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab7 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance159 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance160 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance161 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance162 = new Infragistics.Win.Appearance();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.cbbWarrantyTime = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cldDeliveryRecordDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.cbbAccountingObject = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbbDepartmentID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbbFixedAssetCategoryID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel17 = new Infragistics.Win.Misc.UltraLabel();
            this.chkIsIrrationalCost = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.cbbCurrentState = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.txtSerialNumber = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.chkIsSecondHand = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccessories = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtFixedAssetCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.txtFixedAssetName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtDeliveryRecordNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.txtDescription = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.txtGuaranteeCodition = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.nmrProductionYear = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.txtMadeIn = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGroupBox5 = new Infragistics.Win.Misc.UltraGroupBox();
            this.nmrOriginalPrice = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.nmrRemainingAmount = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.nmrPurchasePrice = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.nmrAcDepreciationAmount = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.nmrMonthPeriodDepreciationAmount = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.nmrPeriodDepreciationAmount = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.nmrMonthDepreciationRate = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.nmrUsedTime = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.nmrDepreciationRate = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.btnOriginalPrice = new Infragistics.Win.Misc.UltraButton();
            this.ultraLabel37 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel38 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel36 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel35 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel34 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel33 = new Infragistics.Win.Misc.UltraLabel();
            this.CbbYearMonth = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel32 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbDepreciationMethod = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel31 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel30 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel29 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbCostSet = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel28 = new Infragistics.Win.Misc.UltraLabel();
            this.chkIsDepreciationAllocation = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cldDisposedDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.txtDisposedReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel24 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel22 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel23 = new Infragistics.Win.Misc.UltraLabel();
            this.nmrDisposedAmount = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbExpenditureAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbbDepreciationAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbbOriginalPriceAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel27 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel26 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel25 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cldDepreciationDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.cldUsedDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.cldIncrementDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.cldPurchasedDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel21 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel20 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel19 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel18 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl3 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.UGridDetails = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.tsmFixedAssetDetail = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmAdd1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDelete1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ultraTabPageControl4 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridFixedAssetAccessories = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.tsmFixedAssetAccessories = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.ultraTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.btnSaveContinue = new Infragistics.Win.Misc.UltraButton();
            this.chkActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbWarrantyTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cldDeliveryRecordDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDepartmentID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbFixedAssetCategoryID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsIrrationalCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCurrentState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSerialNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsSecondHand)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccessories)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFixedAssetCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFixedAssetName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeliveryRecordNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGuaranteeCodition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmrProductionYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMadeIn)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).BeginInit();
            this.ultraGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CbbYearMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDepreciationMethod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).BeginInit();
            this.ultraGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCostSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsDepreciationAllocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cldDisposedDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDisposedReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbExpenditureAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDepreciationAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbOriginalPriceAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cldDepreciationDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cldUsedDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cldIncrementDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cldPurchasedDate)).BeginInit();
            this.ultraTabPageControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UGridDetails)).BeginInit();
            this.tsmFixedAssetDetail.SuspendLayout();
            this.ultraTabPageControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridFixedAssetAccessories)).BeginInit();
            this.tsmFixedAssetAccessories.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).BeginInit();
            this.ultraTabControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkActive)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.cbbWarrantyTime);
            this.ultraTabPageControl1.Controls.Add(this.cldDeliveryRecordDate);
            this.ultraTabPageControl1.Controls.Add(this.cbbAccountingObject);
            this.ultraTabPageControl1.Controls.Add(this.cbbDepartmentID);
            this.ultraTabPageControl1.Controls.Add(this.cbbFixedAssetCategoryID);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel17);
            this.ultraTabPageControl1.Controls.Add(this.chkIsIrrationalCost);
            this.ultraTabPageControl1.Controls.Add(this.cbbCurrentState);
            this.ultraTabPageControl1.Controls.Add(this.txtSerialNumber);
            this.ultraTabPageControl1.Controls.Add(this.chkIsSecondHand);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel16);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel1);
            this.ultraTabPageControl1.Controls.Add(this.txtAccessories);
            this.ultraTabPageControl1.Controls.Add(this.txtFixedAssetCode);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel15);
            this.ultraTabPageControl1.Controls.Add(this.txtFixedAssetName);
            this.ultraTabPageControl1.Controls.Add(this.txtDeliveryRecordNo);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel2);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel14);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel3);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel13);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel12);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel5);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel11);
            this.ultraTabPageControl1.Controls.Add(this.txtDescription);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel6);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel10);
            this.ultraTabPageControl1.Controls.Add(this.txtGuaranteeCodition);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel7);
            this.ultraTabPageControl1.Controls.Add(this.nmrProductionYear);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel8);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel9);
            this.ultraTabPageControl1.Controls.Add(this.txtMadeIn);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 22);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(663, 403);
            // 
            // cbbWarrantyTime
            // 
            this.cbbWarrantyTime.AllowNull = Infragistics.Win.DefaultableBoolean.True;
            this.cbbWarrantyTime.AutoSize = false;
            this.cbbWarrantyTime.DisplayLayout.DefaultSelectedForeColor = System.Drawing.Color.Black;
            this.cbbWarrantyTime.Location = new System.Drawing.Point(113, 130);
            this.cbbWarrantyTime.Name = "cbbWarrantyTime";
            this.cbbWarrantyTime.Size = new System.Drawing.Size(537, 22);
            this.cbbWarrantyTime.TabIndex = 3021;
            // 
            // cldDeliveryRecordDate
            // 
            appearance1.TextVAlignAsString = "Middle";
            this.cldDeliveryRecordDate.Appearance = appearance1;
            this.cldDeliveryRecordDate.AutoSize = false;
            this.cldDeliveryRecordDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cldDeliveryRecordDate.Location = new System.Drawing.Point(507, 336);
            this.cldDeliveryRecordDate.MaskInput = "dd/mm/yyyy";
            this.cldDeliveryRecordDate.Name = "cldDeliveryRecordDate";
            this.cldDeliveryRecordDate.Size = new System.Drawing.Size(143, 22);
            this.cldDeliveryRecordDate.TabIndex = 3020;
            // 
            // cbbAccountingObject
            // 
            this.cbbAccountingObject.AllowNull = Infragistics.Win.DefaultableBoolean.True;
            this.cbbAccountingObject.AutoSize = false;
            appearance2.Image = global::Accounting.Properties.Resources.plus;
            editorButton1.Appearance = appearance2;
            appearance3.Image = global::Accounting.Properties.Resources.plus_press;
            editorButton1.PressedAppearance = appearance3;
            this.cbbAccountingObject.ButtonsRight.Add(editorButton1);
            appearance4.BackColor = System.Drawing.SystemColors.Window;
            appearance4.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbAccountingObject.DisplayLayout.Appearance = appearance4;
            this.cbbAccountingObject.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbAccountingObject.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.cbbAccountingObject.DisplayLayout.DefaultSelectedForeColor = System.Drawing.Color.Black;
            appearance5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance5.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance5.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObject.DisplayLayout.GroupByBox.Appearance = appearance5;
            appearance6.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountingObject.DisplayLayout.GroupByBox.BandLabelAppearance = appearance6;
            this.cbbAccountingObject.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance7.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance7.BackColor2 = System.Drawing.SystemColors.Control;
            appearance7.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountingObject.DisplayLayout.GroupByBox.PromptAppearance = appearance7;
            this.cbbAccountingObject.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbAccountingObject.DisplayLayout.MaxRowScrollRegions = 1;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            appearance8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbAccountingObject.DisplayLayout.Override.ActiveCellAppearance = appearance8;
            appearance9.BackColor = System.Drawing.SystemColors.Highlight;
            appearance9.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbAccountingObject.DisplayLayout.Override.ActiveRowAppearance = appearance9;
            this.cbbAccountingObject.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbAccountingObject.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObject.DisplayLayout.Override.CardAreaAppearance = appearance10;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            appearance11.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbAccountingObject.DisplayLayout.Override.CellAppearance = appearance11;
            this.cbbAccountingObject.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbAccountingObject.DisplayLayout.Override.CellPadding = 0;
            appearance12.BackColor = System.Drawing.SystemColors.Control;
            appearance12.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance12.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance12.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance12.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObject.DisplayLayout.Override.GroupByRowAppearance = appearance12;
            appearance13.TextHAlignAsString = "Left";
            this.cbbAccountingObject.DisplayLayout.Override.HeaderAppearance = appearance13;
            this.cbbAccountingObject.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbAccountingObject.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.BorderColor = System.Drawing.Color.Silver;
            this.cbbAccountingObject.DisplayLayout.Override.RowAppearance = appearance14;
            this.cbbAccountingObject.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance15.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbAccountingObject.DisplayLayout.Override.TemplateAddRowAppearance = appearance15;
            this.cbbAccountingObject.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccountingObject.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbAccountingObject.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbAccountingObject.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountingObject.LimitToList = true;
            this.cbbAccountingObject.Location = new System.Drawing.Point(113, 308);
            this.cbbAccountingObject.Name = "cbbAccountingObject";
            this.cbbAccountingObject.Size = new System.Drawing.Size(135, 22);
            this.cbbAccountingObject.TabIndex = 3014;
            this.cbbAccountingObject.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.cbbAccountingObject_EditorButtonClick);
            // 
            // cbbDepartmentID
            // 
            this.cbbDepartmentID.AutoSize = false;
            appearance16.Image = global::Accounting.Properties.Resources.plus;
            editorButton2.Appearance = appearance16;
            appearance17.Image = global::Accounting.Properties.Resources.plus;
            editorButton2.PressedAppearance = appearance17;
            this.cbbDepartmentID.ButtonsRight.Add(editorButton2);
            appearance18.BackColor = System.Drawing.SystemColors.Window;
            appearance18.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbDepartmentID.DisplayLayout.Appearance = appearance18;
            this.cbbDepartmentID.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbDepartmentID.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.cbbDepartmentID.DisplayLayout.DefaultSelectedForeColor = System.Drawing.Color.Black;
            appearance19.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance19.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance19.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance19.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbDepartmentID.DisplayLayout.GroupByBox.Appearance = appearance19;
            appearance20.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbDepartmentID.DisplayLayout.GroupByBox.BandLabelAppearance = appearance20;
            this.cbbDepartmentID.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance21.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance21.BackColor2 = System.Drawing.SystemColors.Control;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbDepartmentID.DisplayLayout.GroupByBox.PromptAppearance = appearance21;
            this.cbbDepartmentID.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbDepartmentID.DisplayLayout.MaxRowScrollRegions = 1;
            appearance22.BackColor = System.Drawing.SystemColors.Window;
            appearance22.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbDepartmentID.DisplayLayout.Override.ActiveCellAppearance = appearance22;
            appearance23.BackColor = System.Drawing.SystemColors.Highlight;
            appearance23.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbDepartmentID.DisplayLayout.Override.ActiveRowAppearance = appearance23;
            this.cbbDepartmentID.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbDepartmentID.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            this.cbbDepartmentID.DisplayLayout.Override.CardAreaAppearance = appearance24;
            appearance25.BorderColor = System.Drawing.Color.Silver;
            appearance25.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbDepartmentID.DisplayLayout.Override.CellAppearance = appearance25;
            this.cbbDepartmentID.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbDepartmentID.DisplayLayout.Override.CellPadding = 0;
            appearance26.BackColor = System.Drawing.SystemColors.Control;
            appearance26.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance26.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance26.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbDepartmentID.DisplayLayout.Override.GroupByRowAppearance = appearance26;
            appearance27.TextHAlignAsString = "Left";
            this.cbbDepartmentID.DisplayLayout.Override.HeaderAppearance = appearance27;
            this.cbbDepartmentID.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbDepartmentID.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance28.BackColor = System.Drawing.SystemColors.Window;
            appearance28.BorderColor = System.Drawing.Color.Silver;
            this.cbbDepartmentID.DisplayLayout.Override.RowAppearance = appearance28;
            this.cbbDepartmentID.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance29.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbDepartmentID.DisplayLayout.Override.TemplateAddRowAppearance = appearance29;
            this.cbbDepartmentID.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbDepartmentID.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbDepartmentID.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbDepartmentID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbDepartmentID.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbDepartmentID.Location = new System.Drawing.Point(342, 76);
            this.cbbDepartmentID.Name = "cbbDepartmentID";
            this.cbbDepartmentID.Size = new System.Drawing.Size(308, 22);
            this.cbbDepartmentID.TabIndex = 3005;
            this.cbbDepartmentID.ValueChanged += new System.EventHandler(this.cbbDepartmentID_ValueChanged);
            this.cbbDepartmentID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.cbbDepartmentID_EditorButtonClick);
            // 
            // cbbFixedAssetCategoryID
            // 
            this.cbbFixedAssetCategoryID.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.SuggestAppend;
            this.cbbFixedAssetCategoryID.AutoSize = false;
            appearance30.Image = global::Accounting.Properties.Resources.plus;
            editorButton3.Appearance = appearance30;
            appearance31.Image = global::Accounting.Properties.Resources.plus_press;
            editorButton3.PressedAppearance = appearance31;
            this.cbbFixedAssetCategoryID.ButtonsRight.Add(editorButton3);
            appearance32.BackColor = System.Drawing.SystemColors.Window;
            appearance32.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbFixedAssetCategoryID.DisplayLayout.Appearance = appearance32;
            this.cbbFixedAssetCategoryID.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbFixedAssetCategoryID.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.cbbFixedAssetCategoryID.DisplayLayout.DefaultSelectedForeColor = System.Drawing.Color.Black;
            appearance33.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance33.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance33.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbFixedAssetCategoryID.DisplayLayout.GroupByBox.Appearance = appearance33;
            appearance34.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbFixedAssetCategoryID.DisplayLayout.GroupByBox.BandLabelAppearance = appearance34;
            this.cbbFixedAssetCategoryID.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance35.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance35.BackColor2 = System.Drawing.SystemColors.Control;
            appearance35.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance35.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbFixedAssetCategoryID.DisplayLayout.GroupByBox.PromptAppearance = appearance35;
            this.cbbFixedAssetCategoryID.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbFixedAssetCategoryID.DisplayLayout.MaxRowScrollRegions = 1;
            appearance36.BackColor = System.Drawing.SystemColors.Window;
            appearance36.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbFixedAssetCategoryID.DisplayLayout.Override.ActiveCellAppearance = appearance36;
            appearance37.BackColor = System.Drawing.SystemColors.Highlight;
            appearance37.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbFixedAssetCategoryID.DisplayLayout.Override.ActiveRowAppearance = appearance37;
            this.cbbFixedAssetCategoryID.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbFixedAssetCategoryID.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance38.BackColor = System.Drawing.SystemColors.Window;
            this.cbbFixedAssetCategoryID.DisplayLayout.Override.CardAreaAppearance = appearance38;
            appearance39.BorderColor = System.Drawing.Color.Silver;
            appearance39.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbFixedAssetCategoryID.DisplayLayout.Override.CellAppearance = appearance39;
            this.cbbFixedAssetCategoryID.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbFixedAssetCategoryID.DisplayLayout.Override.CellPadding = 0;
            appearance40.BackColor = System.Drawing.SystemColors.Control;
            appearance40.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance40.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance40.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance40.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbFixedAssetCategoryID.DisplayLayout.Override.GroupByRowAppearance = appearance40;
            appearance41.TextHAlignAsString = "Left";
            this.cbbFixedAssetCategoryID.DisplayLayout.Override.HeaderAppearance = appearance41;
            this.cbbFixedAssetCategoryID.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbFixedAssetCategoryID.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance42.BackColor = System.Drawing.SystemColors.Window;
            appearance42.BorderColor = System.Drawing.Color.Silver;
            this.cbbFixedAssetCategoryID.DisplayLayout.Override.RowAppearance = appearance42;
            this.cbbFixedAssetCategoryID.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance43.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbFixedAssetCategoryID.DisplayLayout.Override.TemplateAddRowAppearance = appearance43;
            this.cbbFixedAssetCategoryID.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbFixedAssetCategoryID.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbFixedAssetCategoryID.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbFixedAssetCategoryID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbFixedAssetCategoryID.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbFixedAssetCategoryID.Location = new System.Drawing.Point(113, 36);
            this.cbbFixedAssetCategoryID.Name = "cbbFixedAssetCategoryID";
            this.cbbFixedAssetCategoryID.Size = new System.Drawing.Size(537, 22);
            this.cbbFixedAssetCategoryID.TabIndex = 3003;
            this.cbbFixedAssetCategoryID.ValueChanged += new System.EventHandler(this.cbbFixedAssetCategoryID_ValueChanged);
            this.cbbFixedAssetCategoryID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.cbbFixedAssetCategoryID_EditorButtonClick);
            // 
            // ultraLabel17
            // 
            appearance44.BackColor = System.Drawing.Color.Transparent;
            appearance44.TextVAlignAsString = "Middle";
            this.ultraLabel17.Appearance = appearance44;
            this.ultraLabel17.Location = new System.Drawing.Point(428, 308);
            this.ultraLabel17.Name = "ultraLabel17";
            this.ultraLabel17.Size = new System.Drawing.Size(59, 22);
            this.ultraLabel17.TabIndex = 35;
            this.ultraLabel17.Text = "Trạng thái";
            // 
            // chkIsIrrationalCost
            // 
            appearance45.BackColor = System.Drawing.Color.Transparent;
            appearance45.TextHAlignAsString = "Right";
            appearance45.TextVAlignAsString = "Middle";
            this.chkIsIrrationalCost.Appearance = appearance45;
            this.chkIsIrrationalCost.BackColor = System.Drawing.Color.Transparent;
            this.chkIsIrrationalCost.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsIrrationalCost.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkIsIrrationalCost.Location = new System.Drawing.Point(8, 367);
            this.chkIsIrrationalCost.Name = "chkIsIrrationalCost";
            this.chkIsIrrationalCost.Size = new System.Drawing.Size(126, 20);
            this.chkIsIrrationalCost.TabIndex = 3020;
            this.chkIsIrrationalCost.Text = "Chi phí không hợp lý";
            this.chkIsIrrationalCost.Visible = false;
            // 
            // cbbCurrentState
            // 
            this.cbbCurrentState.AutoSize = false;
            this.cbbCurrentState.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbCurrentState.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            valueListItem3.DataValue = "ValueListItem0";
            valueListItem3.DisplayText = "Không chọn";
            valueListItem4.DataValue = "ValueListItem1";
            valueListItem4.DisplayText = "Đã mua-chưa dùng";
            valueListItem5.DataValue = "ValueListItem2";
            valueListItem5.DisplayText = "Đang sử dụng";
            valueListItem6.DataValue = "ValueListItem3";
            valueListItem6.DisplayText = "Đã thanh lý, nhượng bán";
            valueListItem1.DataValue = "Đang sử dụng, ngừng tính khấu hao";
            this.cbbCurrentState.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem3,
            valueListItem4,
            valueListItem5,
            valueListItem6,
            valueListItem1});
            this.cbbCurrentState.Location = new System.Drawing.Point(507, 308);
            this.cbbCurrentState.Name = "cbbCurrentState";
            this.cbbCurrentState.Size = new System.Drawing.Size(143, 22);
            this.cbbCurrentState.TabIndex = 3016;
            // 
            // txtSerialNumber
            // 
            this.txtSerialNumber.AutoSize = false;
            this.txtSerialNumber.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.txtSerialNumber.Location = new System.Drawing.Point(113, 76);
            this.txtSerialNumber.MaxLength = 100;
            this.txtSerialNumber.Name = "txtSerialNumber";
            this.txtSerialNumber.Size = new System.Drawing.Size(135, 22);
            this.txtSerialNumber.TabIndex = 3006;
            // 
            // chkIsSecondHand
            // 
            appearance46.BackColor = System.Drawing.Color.Transparent;
            appearance46.TextVAlignAsString = "Middle";
            this.chkIsSecondHand.Appearance = appearance46;
            this.chkIsSecondHand.BackColor = System.Drawing.Color.Transparent;
            this.chkIsSecondHand.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsSecondHand.Location = new System.Drawing.Point(342, 308);
            this.chkIsSecondHand.Name = "chkIsSecondHand";
            this.chkIsSecondHand.Size = new System.Drawing.Size(80, 22);
            this.chkIsSecondHand.TabIndex = 3015;
            this.chkIsSecondHand.Text = "Mua cũ";
            // 
            // ultraLabel16
            // 
            appearance47.BackColor = System.Drawing.Color.Transparent;
            appearance47.TextVAlignAsString = "Middle";
            this.ultraLabel16.Appearance = appearance47;
            this.ultraLabel16.Location = new System.Drawing.Point(254, 308);
            this.ultraLabel16.Name = "ultraLabel16";
            this.ultraLabel16.Size = new System.Drawing.Size(82, 22);
            this.ultraLabel16.TabIndex = 32;
            this.ultraLabel16.Text = "Tình trạng";
            // 
            // ultraLabel1
            // 
            appearance48.BackColor = System.Drawing.Color.Transparent;
            appearance48.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance48;
            this.ultraLabel1.Location = new System.Drawing.Point(7, 8);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(82, 22);
            this.ultraLabel1.TabIndex = 31;
            this.ultraLabel1.Text = "Mã (*)";
            // 
            // txtAccessories
            // 
            this.txtAccessories.AutoSize = false;
            this.txtAccessories.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.txtAccessories.Location = new System.Drawing.Point(113, 281);
            this.txtAccessories.MaxLength = 512;
            this.txtAccessories.Name = "txtAccessories";
            this.txtAccessories.Size = new System.Drawing.Size(537, 22);
            this.txtAccessories.TabIndex = 3013;
            // 
            // txtFixedAssetCode
            // 
            this.txtFixedAssetCode.AutoSize = false;
            this.txtFixedAssetCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.txtFixedAssetCode.Location = new System.Drawing.Point(113, 8);
            this.txtFixedAssetCode.MaxLength = 25;
            this.txtFixedAssetCode.Name = "txtFixedAssetCode";
            this.txtFixedAssetCode.Size = new System.Drawing.Size(135, 22);
            this.txtFixedAssetCode.TabIndex = 3001;
            // 
            // ultraLabel15
            // 
            appearance49.BackColor = System.Drawing.Color.Transparent;
            appearance49.TextVAlignAsString = "Middle";
            this.ultraLabel15.Appearance = appearance49;
            this.ultraLabel15.Location = new System.Drawing.Point(428, 335);
            this.ultraLabel15.Name = "ultraLabel15";
            this.ultraLabel15.Size = new System.Drawing.Size(59, 22);
            this.ultraLabel15.TabIndex = 29;
            this.ultraLabel15.Text = "Ngày";
            // 
            // txtFixedAssetName
            // 
            this.txtFixedAssetName.AutoSize = false;
            this.txtFixedAssetName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.txtFixedAssetName.Location = new System.Drawing.Point(342, 8);
            this.txtFixedAssetName.MaxLength = 512;
            this.txtFixedAssetName.Name = "txtFixedAssetName";
            this.txtFixedAssetName.Size = new System.Drawing.Size(308, 22);
            this.txtFixedAssetName.TabIndex = 3002;
            // 
            // txtDeliveryRecordNo
            // 
            this.txtDeliveryRecordNo.AutoSize = false;
            this.txtDeliveryRecordNo.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.txtDeliveryRecordNo.Location = new System.Drawing.Point(113, 336);
            this.txtDeliveryRecordNo.MaxLength = 50;
            this.txtDeliveryRecordNo.Name = "txtDeliveryRecordNo";
            this.txtDeliveryRecordNo.Size = new System.Drawing.Size(309, 22);
            this.txtDeliveryRecordNo.TabIndex = 3017;
            // 
            // ultraLabel2
            // 
            appearance50.BackColor = System.Drawing.Color.Transparent;
            appearance50.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance50;
            this.ultraLabel2.Location = new System.Drawing.Point(254, 8);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(82, 22);
            this.ultraLabel2.TabIndex = 3;
            this.ultraLabel2.Text = "Tên(*)";
            // 
            // ultraLabel14
            // 
            appearance51.BackColor = System.Drawing.Color.Transparent;
            appearance51.TextVAlignAsString = "Middle";
            this.ultraLabel14.Appearance = appearance51;
            this.ultraLabel14.Location = new System.Drawing.Point(4, 336);
            this.ultraLabel14.Name = "ultraLabel14";
            this.ultraLabel14.Size = new System.Drawing.Size(103, 22);
            this.ultraLabel14.TabIndex = 27;
            this.ultraLabel14.Text = "BB giao nhận số";
            // 
            // ultraLabel3
            // 
            appearance52.BackColor = System.Drawing.Color.Transparent;
            appearance52.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance52;
            this.ultraLabel3.Location = new System.Drawing.Point(7, 36);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(82, 22);
            this.ultraLabel3.TabIndex = 4;
            this.ultraLabel3.Text = "Loại (*)";
            // 
            // ultraLabel13
            // 
            appearance53.BackColor = System.Drawing.Color.Transparent;
            appearance53.TextVAlignAsString = "Middle";
            this.ultraLabel13.Appearance = appearance53;
            this.ultraLabel13.Location = new System.Drawing.Point(4, 308);
            this.ultraLabel13.Name = "ultraLabel13";
            this.ultraLabel13.Size = new System.Drawing.Size(82, 22);
            this.ultraLabel13.TabIndex = 25;
            this.ultraLabel13.Text = "Nhà cung cấp";
            // 
            // ultraLabel12
            // 
            appearance54.BackColor = System.Drawing.Color.Transparent;
            appearance54.TextVAlignAsString = "Middle";
            this.ultraLabel12.Appearance = appearance54;
            this.ultraLabel12.Location = new System.Drawing.Point(4, 281);
            this.ultraLabel12.Name = "ultraLabel12";
            this.ultraLabel12.Size = new System.Drawing.Size(82, 22);
            this.ultraLabel12.TabIndex = 24;
            this.ultraLabel12.Text = "Phụ kiện";
            // 
            // ultraLabel5
            // 
            appearance55.BackColor = System.Drawing.Color.Transparent;
            appearance55.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance55;
            this.ultraLabel5.Location = new System.Drawing.Point(254, 76);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(82, 22);
            this.ultraLabel5.TabIndex = 8;
            this.ultraLabel5.Text = "Phòng ban (*)";
            // 
            // ultraLabel11
            // 
            appearance56.BackColor = System.Drawing.Color.Transparent;
            appearance56.TextVAlignAsString = "Middle";
            this.ultraLabel11.Appearance = appearance56;
            this.ultraLabel11.Location = new System.Drawing.Point(6, 215);
            this.ultraLabel11.Name = "ultraLabel11";
            this.ultraLabel11.Size = new System.Drawing.Size(82, 22);
            this.ultraLabel11.TabIndex = 22;
            this.ultraLabel11.Text = "Mô tả";
            // 
            // txtDescription
            // 
            this.txtDescription.AutoSize = false;
            this.txtDescription.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.txtDescription.Location = new System.Drawing.Point(113, 215);
            this.txtDescription.MaxLength = 512;
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(537, 61);
            this.txtDescription.TabIndex = 3012;
            // 
            // ultraLabel6
            // 
            appearance57.BackColor = System.Drawing.Color.Transparent;
            appearance57.TextVAlignAsString = "Middle";
            this.ultraLabel6.Appearance = appearance57;
            this.ultraLabel6.Location = new System.Drawing.Point(5, 76);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(85, 22);
            this.ultraLabel6.TabIndex = 10;
            this.ultraLabel6.Text = "Số hiệu";
            // 
            // ultraLabel10
            // 
            appearance58.BackColor = System.Drawing.Color.Transparent;
            appearance58.TextVAlignAsString = "Middle";
            this.ultraLabel10.Appearance = appearance58;
            this.ultraLabel10.Location = new System.Drawing.Point(4, 157);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(82, 22);
            this.ultraLabel10.TabIndex = 20;
            this.ultraLabel10.Text = "Điều kiện BH";
            // 
            // txtGuaranteeCodition
            // 
            this.txtGuaranteeCodition.AutoSize = false;
            this.txtGuaranteeCodition.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.txtGuaranteeCodition.Location = new System.Drawing.Point(113, 157);
            this.txtGuaranteeCodition.MaxLength = 512;
            this.txtGuaranteeCodition.Multiline = true;
            this.txtGuaranteeCodition.Name = "txtGuaranteeCodition";
            this.txtGuaranteeCodition.Size = new System.Drawing.Size(537, 22);
            this.txtGuaranteeCodition.TabIndex = 3011;
            // 
            // ultraLabel7
            // 
            appearance59.BackColor = System.Drawing.Color.Transparent;
            appearance59.TextVAlignAsString = "Middle";
            this.ultraLabel7.Appearance = appearance59;
            this.ultraLabel7.Location = new System.Drawing.Point(4, 104);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(86, 22);
            this.ultraLabel7.TabIndex = 12;
            this.ultraLabel7.Text = "Năm sản xuất";
            // 
            // nmrProductionYear
            // 
            this.nmrProductionYear.AutoSize = false;
            this.nmrProductionYear.Location = new System.Drawing.Point(113, 103);
            this.nmrProductionYear.MaxValue = 9999;
            this.nmrProductionYear.MinValue = 0;
            this.nmrProductionYear.Name = "nmrProductionYear";
            this.nmrProductionYear.PromptChar = ' ';
            this.nmrProductionYear.Size = new System.Drawing.Size(135, 21);
            this.nmrProductionYear.TabIndex = 3007;
            // 
            // ultraLabel8
            // 
            appearance60.BackColor = System.Drawing.Color.Transparent;
            appearance60.TextVAlignAsString = "Middle";
            this.ultraLabel8.Appearance = appearance60;
            this.ultraLabel8.Location = new System.Drawing.Point(254, 102);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(73, 22);
            this.ultraLabel8.TabIndex = 14;
            this.ultraLabel8.Text = "Nơi sản xuất";
            // 
            // ultraLabel9
            // 
            appearance61.BackColor = System.Drawing.Color.Transparent;
            appearance61.TextVAlignAsString = "Middle";
            this.ultraLabel9.Appearance = appearance61;
            this.ultraLabel9.Location = new System.Drawing.Point(4, 130);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(85, 22);
            this.ultraLabel9.TabIndex = 16;
            this.ultraLabel9.Text = "Thời gian BH";
            // 
            // txtMadeIn
            // 
            this.txtMadeIn.AutoSize = false;
            this.txtMadeIn.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.txtMadeIn.Location = new System.Drawing.Point(342, 103);
            this.txtMadeIn.Name = "txtMadeIn";
            this.txtMadeIn.Size = new System.Drawing.Size(308, 22);
            this.txtMadeIn.TabIndex = 3008;
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.ultraGroupBox5);
            this.ultraTabPageControl2.Controls.Add(this.ultraGroupBox4);
            this.ultraTabPageControl2.Controls.Add(this.ultraGroupBox3);
            this.ultraTabPageControl2.Controls.Add(this.ultraGroupBox2);
            this.ultraTabPageControl2.Controls.Add(this.ultraGroupBox1);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(663, 403);
            // 
            // ultraGroupBox5
            // 
            this.ultraGroupBox5.Controls.Add(this.nmrOriginalPrice);
            this.ultraGroupBox5.Controls.Add(this.nmrRemainingAmount);
            this.ultraGroupBox5.Controls.Add(this.nmrPurchasePrice);
            this.ultraGroupBox5.Controls.Add(this.nmrAcDepreciationAmount);
            this.ultraGroupBox5.Controls.Add(this.nmrMonthPeriodDepreciationAmount);
            this.ultraGroupBox5.Controls.Add(this.nmrPeriodDepreciationAmount);
            this.ultraGroupBox5.Controls.Add(this.nmrMonthDepreciationRate);
            this.ultraGroupBox5.Controls.Add(this.nmrUsedTime);
            this.ultraGroupBox5.Controls.Add(this.nmrDepreciationRate);
            this.ultraGroupBox5.Controls.Add(this.btnOriginalPrice);
            this.ultraGroupBox5.Controls.Add(this.ultraLabel37);
            this.ultraGroupBox5.Controls.Add(this.ultraLabel38);
            this.ultraGroupBox5.Controls.Add(this.ultraLabel36);
            this.ultraGroupBox5.Controls.Add(this.ultraLabel35);
            this.ultraGroupBox5.Controls.Add(this.ultraLabel34);
            this.ultraGroupBox5.Controls.Add(this.ultraLabel33);
            this.ultraGroupBox5.Controls.Add(this.CbbYearMonth);
            this.ultraGroupBox5.Controls.Add(this.ultraLabel32);
            this.ultraGroupBox5.Controls.Add(this.cbbDepreciationMethod);
            this.ultraGroupBox5.Controls.Add(this.ultraLabel31);
            this.ultraGroupBox5.Controls.Add(this.ultraLabel30);
            this.ultraGroupBox5.Controls.Add(this.ultraLabel29);
            this.ultraGroupBox5.Location = new System.Drawing.Point(11, 249);
            this.ultraGroupBox5.Name = "ultraGroupBox5";
            this.ultraGroupBox5.Size = new System.Drawing.Size(641, 145);
            this.ultraGroupBox5.TabIndex = 53;
            this.ultraGroupBox5.Text = "Nguyên giá và khấu hao";
            this.ultraGroupBox5.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // nmrOriginalPrice
            // 
            appearance62.TextHAlignAsString = "Right";
            this.nmrOriginalPrice.Appearance = appearance62;
            this.nmrOriginalPrice.AutoSize = false;
            this.nmrOriginalPrice.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.nmrOriginalPrice.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.nmrOriginalPrice.InputMask = "{LOC}nnnnnnn.nn";
            this.nmrOriginalPrice.Location = new System.Drawing.Point(171, 23);
            this.nmrOriginalPrice.MaxValue = 9999999999999D;
            this.nmrOriginalPrice.MinValue = 0D;
            this.nmrOriginalPrice.Name = "nmrOriginalPrice";
            this.nmrOriginalPrice.Size = new System.Drawing.Size(86, 22);
            this.nmrOriginalPrice.TabIndex = 3024;
            this.nmrOriginalPrice.Text = "0";
            // 
            // nmrRemainingAmount
            // 
            appearance63.TextHAlignAsString = "Right";
            this.nmrRemainingAmount.Appearance = appearance63;
            this.nmrRemainingAmount.AutoSize = false;
            this.nmrRemainingAmount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.nmrRemainingAmount.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.nmrRemainingAmount.Enabled = false;
            this.nmrRemainingAmount.InputMask = "{LOC}nnnnnnn.nn";
            this.nmrRemainingAmount.Location = new System.Drawing.Point(436, 116);
            this.nmrRemainingAmount.MaxValue = 9999999999999D;
            this.nmrRemainingAmount.MinValue = 0D;
            this.nmrRemainingAmount.Name = "nmrRemainingAmount";
            this.nmrRemainingAmount.Size = new System.Drawing.Size(188, 22);
            this.nmrRemainingAmount.TabIndex = 3023;
            this.nmrRemainingAmount.Text = "0";
            this.nmrRemainingAmount.Visible = false;
            // 
            // nmrPurchasePrice
            // 
            appearance64.TextHAlignAsString = "Right";
            this.nmrPurchasePrice.Appearance = appearance64;
            this.nmrPurchasePrice.AutoSize = false;
            this.nmrPurchasePrice.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.nmrPurchasePrice.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.nmrPurchasePrice.InputMask = "{LOC}nn,nnn,nnn.nn";
            this.nmrPurchasePrice.Location = new System.Drawing.Point(436, 24);
            this.nmrPurchasePrice.MaxValue = ((long)(99999999999999999));
            this.nmrPurchasePrice.Name = "nmrPurchasePrice";
            this.nmrPurchasePrice.Size = new System.Drawing.Size(188, 22);
            this.nmrPurchasePrice.TabIndex = 3021;
            this.nmrPurchasePrice.Validated += new System.EventHandler(this.nmrPurchasePrice_Validated);
            // 
            // nmrAcDepreciationAmount
            // 
            appearance65.TextHAlignAsString = "Right";
            this.nmrAcDepreciationAmount.Appearance = appearance65;
            this.nmrAcDepreciationAmount.AutoSize = false;
            this.nmrAcDepreciationAmount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.nmrAcDepreciationAmount.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.nmrAcDepreciationAmount.InputMask = "{LOC}nnnnnnn.nn";
            this.nmrAcDepreciationAmount.Location = new System.Drawing.Point(171, 115);
            this.nmrAcDepreciationAmount.MaxValue = 9999999999999D;
            this.nmrAcDepreciationAmount.MinValue = 0D;
            this.nmrAcDepreciationAmount.Name = "nmrAcDepreciationAmount";
            this.nmrAcDepreciationAmount.Size = new System.Drawing.Size(128, 22);
            this.nmrAcDepreciationAmount.TabIndex = 3018;
            this.nmrAcDepreciationAmount.Text = "0";
            this.nmrAcDepreciationAmount.Visible = false;
            this.nmrAcDepreciationAmount.ValueChanged += new System.EventHandler(this.nmrAcDepreciationAmount_ValueChanged);
            // 
            // nmrMonthPeriodDepreciationAmount
            // 
            appearance66.TextHAlignAsString = "Right";
            this.nmrMonthPeriodDepreciationAmount.Appearance = appearance66;
            this.nmrMonthPeriodDepreciationAmount.AutoSize = false;
            this.nmrMonthPeriodDepreciationAmount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.nmrMonthPeriodDepreciationAmount.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.nmrMonthPeriodDepreciationAmount.Enabled = false;
            this.nmrMonthPeriodDepreciationAmount.InputMask = "{LOC}nnnnnnn.nn";
            this.nmrMonthPeriodDepreciationAmount.Location = new System.Drawing.Point(436, 93);
            this.nmrMonthPeriodDepreciationAmount.MaxValue = 9999999999999D;
            this.nmrMonthPeriodDepreciationAmount.MinValue = 0D;
            this.nmrMonthPeriodDepreciationAmount.Name = "nmrMonthPeriodDepreciationAmount";
            this.nmrMonthPeriodDepreciationAmount.Size = new System.Drawing.Size(188, 22);
            this.nmrMonthPeriodDepreciationAmount.TabIndex = 69;
            this.nmrMonthPeriodDepreciationAmount.Text = "0";
            // 
            // nmrPeriodDepreciationAmount
            // 
            appearance67.TextHAlignAsString = "Right";
            this.nmrPeriodDepreciationAmount.Appearance = appearance67;
            this.nmrPeriodDepreciationAmount.AutoSize = false;
            this.nmrPeriodDepreciationAmount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.nmrPeriodDepreciationAmount.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.nmrPeriodDepreciationAmount.Enabled = false;
            this.nmrPeriodDepreciationAmount.InputMask = "{LOC}nnnnnnn.nn";
            this.nmrPeriodDepreciationAmount.Location = new System.Drawing.Point(171, 92);
            this.nmrPeriodDepreciationAmount.MaxValue = 999999999999D;
            this.nmrPeriodDepreciationAmount.MinValue = 0D;
            this.nmrPeriodDepreciationAmount.Name = "nmrPeriodDepreciationAmount";
            this.nmrPeriodDepreciationAmount.Size = new System.Drawing.Size(128, 22);
            this.nmrPeriodDepreciationAmount.TabIndex = 72;
            this.nmrPeriodDepreciationAmount.Text = "0";
            this.nmrPeriodDepreciationAmount.Validated += new System.EventHandler(this.nmrPeriodDepreciationAmount_Validated);
            // 
            // nmrMonthDepreciationRate
            // 
            appearance68.TextHAlignAsString = "Right";
            this.nmrMonthDepreciationRate.Appearance = appearance68;
            this.nmrMonthDepreciationRate.AutoSize = false;
            this.nmrMonthDepreciationRate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.nmrMonthDepreciationRate.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.nmrMonthDepreciationRate.Enabled = false;
            this.nmrMonthDepreciationRate.InputMask = "{LOC}nnnnnnn.nnnn";
            this.nmrMonthDepreciationRate.Location = new System.Drawing.Point(436, 70);
            this.nmrMonthDepreciationRate.MaxValue = 1000000000D;
            this.nmrMonthDepreciationRate.MinValue = 0D;
            this.nmrMonthDepreciationRate.Name = "nmrMonthDepreciationRate";
            this.nmrMonthDepreciationRate.Size = new System.Drawing.Size(188, 22);
            this.nmrMonthDepreciationRate.TabIndex = 68;
            this.nmrMonthDepreciationRate.Text = "00000";
            this.nmrMonthDepreciationRate.Validated += new System.EventHandler(this.nmrMonthDepreciationRate_Validated);
            // 
            // nmrUsedTime
            // 
            appearance69.TextHAlignAsString = "Right";
            this.nmrUsedTime.Appearance = appearance69;
            this.nmrUsedTime.AutoSize = false;
            this.nmrUsedTime.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.nmrUsedTime.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.nmrUsedTime.InputMask = "{LOC}nnnnnnn.nn";
            this.nmrUsedTime.Location = new System.Drawing.Point(436, 48);
            this.nmrUsedTime.MaxValue = 5000D;
            this.nmrUsedTime.MinValue = 0D;
            this.nmrUsedTime.Name = "nmrUsedTime";
            this.nmrUsedTime.Size = new System.Drawing.Size(107, 22);
            this.nmrUsedTime.TabIndex = 3016;
            this.nmrUsedTime.Validated += new System.EventHandler(this.nmrUsedTime_Validated);
            // 
            // nmrDepreciationRate
            // 
            appearance70.TextHAlignAsString = "Right";
            this.nmrDepreciationRate.Appearance = appearance70;
            this.nmrDepreciationRate.AutoSize = false;
            this.nmrDepreciationRate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.nmrDepreciationRate.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.nmrDepreciationRate.Enabled = false;
            this.nmrDepreciationRate.InputMask = "{LOC}nnnnnnn.nnnn";
            this.nmrDepreciationRate.Location = new System.Drawing.Point(171, 69);
            this.nmrDepreciationRate.MaxValue = 1000000D;
            this.nmrDepreciationRate.MinValue = 0D;
            this.nmrDepreciationRate.Name = "nmrDepreciationRate";
            this.nmrDepreciationRate.Size = new System.Drawing.Size(128, 22);
            this.nmrDepreciationRate.TabIndex = 71;
            this.nmrDepreciationRate.Text = "00000";
            this.nmrDepreciationRate.Validated += new System.EventHandler(this.nmrDepreciationRate_Validated);
            // 
            // btnOriginalPrice
            // 
            this.btnOriginalPrice.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.btnOriginalPrice.Location = new System.Drawing.Point(263, 23);
            this.btnOriginalPrice.Name = "btnOriginalPrice";
            this.btnOriginalPrice.Size = new System.Drawing.Size(35, 22);
            this.btnOriginalPrice.TabIndex = 3013;
            this.btnOriginalPrice.Text = "....";
            this.btnOriginalPrice.Click += new System.EventHandler(this.btnOriginalPrice_Click);
            // 
            // ultraLabel37
            // 
            appearance71.BackColor = System.Drawing.Color.Transparent;
            appearance71.TextVAlignAsString = "Middle";
            this.ultraLabel37.Appearance = appearance71;
            this.ultraLabel37.Location = new System.Drawing.Point(307, 116);
            this.ultraLabel37.Name = "ultraLabel37";
            this.ultraLabel37.Size = new System.Drawing.Size(121, 22);
            this.ultraLabel37.TabIndex = 65;
            this.ultraLabel37.Text = "Giá trị còn lại";
            this.ultraLabel37.Visible = false;
            // 
            // ultraLabel38
            // 
            appearance72.BackColor = System.Drawing.Color.Transparent;
            appearance72.TextVAlignAsString = "Middle";
            this.ultraLabel38.Appearance = appearance72;
            this.ultraLabel38.Location = new System.Drawing.Point(10, 115);
            this.ultraLabel38.Name = "ultraLabel38";
            this.ultraLabel38.Size = new System.Drawing.Size(153, 22);
            this.ultraLabel38.TabIndex = 63;
            this.ultraLabel38.Text = "Hao mòn lũy kế";
            this.ultraLabel38.Visible = false;
            // 
            // ultraLabel36
            // 
            appearance73.BackColor = System.Drawing.Color.Transparent;
            appearance73.TextVAlignAsString = "Middle";
            this.ultraLabel36.Appearance = appearance73;
            this.ultraLabel36.Location = new System.Drawing.Point(307, 93);
            this.ultraLabel36.Name = "ultraLabel36";
            this.ultraLabel36.Size = new System.Drawing.Size(123, 22);
            this.ultraLabel36.TabIndex = 61;
            this.ultraLabel36.Text = "Giá trị KH tháng";
            // 
            // ultraLabel35
            // 
            appearance74.BackColor = System.Drawing.Color.Transparent;
            appearance74.TextVAlignAsString = "Middle";
            this.ultraLabel35.Appearance = appearance74;
            this.ultraLabel35.Location = new System.Drawing.Point(10, 92);
            this.ultraLabel35.Name = "ultraLabel35";
            this.ultraLabel35.Size = new System.Drawing.Size(155, 22);
            this.ultraLabel35.TabIndex = 59;
            this.ultraLabel35.Text = "Giá trị KH năm";
            // 
            // ultraLabel34
            // 
            appearance75.BackColor = System.Drawing.Color.Transparent;
            appearance75.TextVAlignAsString = "Middle";
            this.ultraLabel34.Appearance = appearance75;
            this.ultraLabel34.Location = new System.Drawing.Point(307, 70);
            this.ultraLabel34.Name = "ultraLabel34";
            this.ultraLabel34.Size = new System.Drawing.Size(132, 22);
            this.ultraLabel34.TabIndex = 57;
            this.ultraLabel34.Text = "Tỷ lệ KH Tháng (%)";
            // 
            // ultraLabel33
            // 
            appearance76.BackColor = System.Drawing.Color.Transparent;
            appearance76.TextVAlignAsString = "Middle";
            this.ultraLabel33.Appearance = appearance76;
            this.ultraLabel33.Location = new System.Drawing.Point(10, 69);
            this.ultraLabel33.Name = "ultraLabel33";
            this.ultraLabel33.Size = new System.Drawing.Size(155, 22);
            this.ultraLabel33.TabIndex = 55;
            this.ultraLabel33.Text = "Tỷ lệ khấu hao năm (%)";
            // 
            // CbbYearMonth
            // 
            this.CbbYearMonth.AutoSize = false;
            valueListItem7.CheckState = System.Windows.Forms.CheckState.Checked;
            valueListItem7.DataValue = "ValueListItem0";
            valueListItem7.DisplayText = "Tháng";
            valueListItem8.DataValue = "ValueListItem1";
            valueListItem8.DisplayText = "Năm";
            this.CbbYearMonth.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem7,
            valueListItem8});
            this.CbbYearMonth.Location = new System.Drawing.Point(549, 48);
            this.CbbYearMonth.Name = "CbbYearMonth";
            this.CbbYearMonth.Size = new System.Drawing.Size(75, 22);
            this.CbbYearMonth.TabIndex = 3017;
            this.CbbYearMonth.Text = "Tháng";
            this.CbbYearMonth.SelectionChanged += new System.EventHandler(this.CbbYearMonth_SelectionChanged);
            // 
            // ultraLabel32
            // 
            appearance77.BackColor = System.Drawing.Color.Transparent;
            appearance77.TextVAlignAsString = "Middle";
            this.ultraLabel32.Appearance = appearance77;
            this.ultraLabel32.Location = new System.Drawing.Point(305, 48);
            this.ultraLabel32.Name = "ultraLabel32";
            this.ultraLabel32.Size = new System.Drawing.Size(125, 22);
            this.ultraLabel32.TabIndex = 52;
            this.ultraLabel32.Text = "Thời gian sử dụng ";
            // 
            // cbbDepreciationMethod
            // 
            this.cbbDepreciationMethod.AutoSize = false;
            valueListItem9.CheckState = System.Windows.Forms.CheckState.Checked;
            valueListItem9.DataValue = 0;
            valueListItem9.DisplayText = "Đường thẳng";
            valueListItem2.DataValue = 1;
            valueListItem2.DisplayText = "Khấu hao nhanh";
            valueListItem10.DataValue = 2;
            valueListItem10.DisplayText = "Sản lượng";
            this.cbbDepreciationMethod.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem9,
            valueListItem2,
            valueListItem10});
            this.cbbDepreciationMethod.Location = new System.Drawing.Point(171, 46);
            this.cbbDepreciationMethod.Name = "cbbDepreciationMethod";
            this.cbbDepreciationMethod.Size = new System.Drawing.Size(128, 22);
            this.cbbDepreciationMethod.TabIndex = 3015;
            // 
            // ultraLabel31
            // 
            appearance78.BackColor = System.Drawing.Color.Transparent;
            appearance78.TextVAlignAsString = "Middle";
            this.ultraLabel31.Appearance = appearance78;
            this.ultraLabel31.Location = new System.Drawing.Point(10, 46);
            this.ultraLabel31.Name = "ultraLabel31";
            this.ultraLabel31.Size = new System.Drawing.Size(155, 22);
            this.ultraLabel31.TabIndex = 12;
            this.ultraLabel31.Text = "Phương pháp tính khấu hao";
            // 
            // ultraLabel30
            // 
            appearance79.BackColor = System.Drawing.Color.Transparent;
            appearance79.TextVAlignAsString = "Middle";
            this.ultraLabel30.Appearance = appearance79;
            this.ultraLabel30.Location = new System.Drawing.Point(305, 24);
            this.ultraLabel30.Name = "ultraLabel30";
            this.ultraLabel30.Size = new System.Drawing.Size(125, 22);
            this.ultraLabel30.TabIndex = 10;
            this.ultraLabel30.Text = "Giá trị tính khấu hao ";
            // 
            // ultraLabel29
            // 
            appearance80.BackColor = System.Drawing.Color.Transparent;
            appearance80.TextVAlignAsString = "Middle";
            this.ultraLabel29.Appearance = appearance80;
            this.ultraLabel29.Location = new System.Drawing.Point(10, 23);
            this.ultraLabel29.Name = "ultraLabel29";
            this.ultraLabel29.Size = new System.Drawing.Size(87, 22);
            this.ultraLabel29.TabIndex = 8;
            this.ultraLabel29.Text = "Nguyên giá ";
            // 
            // ultraGroupBox4
            // 
            this.ultraGroupBox4.Controls.Add(this.cbbCostSet);
            this.ultraGroupBox4.Controls.Add(this.ultraLabel28);
            this.ultraGroupBox4.Controls.Add(this.chkIsDepreciationAllocation);
            this.ultraGroupBox4.Location = new System.Drawing.Point(11, 195);
            this.ultraGroupBox4.Name = "ultraGroupBox4";
            this.ultraGroupBox4.Size = new System.Drawing.Size(641, 48);
            this.ultraGroupBox4.TabIndex = 45;
            this.ultraGroupBox4.Text = "Phân bổ khấu hao";
            this.ultraGroupBox4.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbCostSet
            // 
            this.cbbCostSet.AutoSize = false;
            appearance81.BackColor = System.Drawing.SystemColors.Window;
            appearance81.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbCostSet.DisplayLayout.Appearance = appearance81;
            this.cbbCostSet.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbCostSet.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.cbbCostSet.DisplayLayout.DefaultSelectedForeColor = System.Drawing.Color.Black;
            appearance82.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance82.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance82.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance82.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbCostSet.DisplayLayout.GroupByBox.Appearance = appearance82;
            appearance83.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbCostSet.DisplayLayout.GroupByBox.BandLabelAppearance = appearance83;
            this.cbbCostSet.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance84.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance84.BackColor2 = System.Drawing.SystemColors.Control;
            appearance84.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance84.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbCostSet.DisplayLayout.GroupByBox.PromptAppearance = appearance84;
            this.cbbCostSet.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbCostSet.DisplayLayout.MaxRowScrollRegions = 1;
            appearance85.BackColor = System.Drawing.SystemColors.Window;
            appearance85.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbCostSet.DisplayLayout.Override.ActiveCellAppearance = appearance85;
            appearance86.BackColor = System.Drawing.SystemColors.Highlight;
            appearance86.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbCostSet.DisplayLayout.Override.ActiveRowAppearance = appearance86;
            this.cbbCostSet.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbCostSet.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance87.BackColor = System.Drawing.SystemColors.Window;
            this.cbbCostSet.DisplayLayout.Override.CardAreaAppearance = appearance87;
            appearance88.BorderColor = System.Drawing.Color.Silver;
            appearance88.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbCostSet.DisplayLayout.Override.CellAppearance = appearance88;
            this.cbbCostSet.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbCostSet.DisplayLayout.Override.CellPadding = 0;
            appearance89.BackColor = System.Drawing.SystemColors.Control;
            appearance89.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance89.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance89.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance89.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbCostSet.DisplayLayout.Override.GroupByRowAppearance = appearance89;
            appearance90.TextHAlignAsString = "Left";
            this.cbbCostSet.DisplayLayout.Override.HeaderAppearance = appearance90;
            this.cbbCostSet.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbCostSet.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance91.BackColor = System.Drawing.SystemColors.Window;
            appearance91.BorderColor = System.Drawing.Color.Silver;
            this.cbbCostSet.DisplayLayout.Override.RowAppearance = appearance91;
            this.cbbCostSet.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance92.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbCostSet.DisplayLayout.Override.TemplateAddRowAppearance = appearance92;
            this.cbbCostSet.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbCostSet.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbCostSet.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbCostSet.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbCostSet.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbCostSet.Location = new System.Drawing.Point(305, 18);
            this.cbbCostSet.Name = "cbbCostSet";
            this.cbbCostSet.Size = new System.Drawing.Size(319, 22);
            this.cbbCostSet.TabIndex = 3012;
            // 
            // ultraLabel28
            // 
            appearance93.BackColor = System.Drawing.Color.Transparent;
            appearance93.TextVAlignAsString = "Middle";
            this.ultraLabel28.Appearance = appearance93;
            this.ultraLabel28.Location = new System.Drawing.Point(210, 18);
            this.ultraLabel28.Name = "ultraLabel28";
            this.ultraLabel28.Size = new System.Drawing.Size(89, 22);
            this.ultraLabel28.TabIndex = 51;
            this.ultraLabel28.Text = "ĐT tập hợp CP ";
            // 
            // chkIsDepreciationAllocation
            // 
            appearance94.BackColor = System.Drawing.Color.Transparent;
            appearance94.TextHAlignAsString = "Right";
            appearance94.TextVAlignAsString = "Middle";
            this.chkIsDepreciationAllocation.Appearance = appearance94;
            this.chkIsDepreciationAllocation.BackColor = System.Drawing.Color.Transparent;
            this.chkIsDepreciationAllocation.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsDepreciationAllocation.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkIsDepreciationAllocation.Location = new System.Drawing.Point(10, 18);
            this.chkIsDepreciationAllocation.Name = "chkIsDepreciationAllocation";
            this.chkIsDepreciationAllocation.Size = new System.Drawing.Size(155, 22);
            this.chkIsDepreciationAllocation.TabIndex = 3011;
            this.chkIsDepreciationAllocation.Text = "Phân bổ chi phí khấu hao";
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Controls.Add(this.cldDisposedDate);
            this.ultraGroupBox3.Controls.Add(this.txtDisposedReason);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel24);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel22);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel23);
            this.ultraGroupBox3.Controls.Add(this.nmrDisposedAmount);
            this.ultraGroupBox3.Location = new System.Drawing.Point(11, 142);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(641, 47);
            this.ultraGroupBox3.TabIndex = 2;
            this.ultraGroupBox3.Text = "Thanh lý";
            this.ultraGroupBox3.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            this.ultraGroupBox3.Visible = false;
            // 
            // cldDisposedDate
            // 
            appearance95.TextVAlignAsString = "Middle";
            this.cldDisposedDate.Appearance = appearance95;
            this.cldDisposedDate.AutoSize = false;
            this.cldDisposedDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.cldDisposedDate.Location = new System.Drawing.Point(91, 16);
            this.cldDisposedDate.MaskInput = "dd/mm/yyyy";
            this.cldDisposedDate.Name = "cldDisposedDate";
            this.cldDisposedDate.Size = new System.Drawing.Size(99, 22);
            this.cldDisposedDate.TabIndex = 3012;
            this.cldDisposedDate.Value = null;
            // 
            // txtDisposedReason
            // 
            this.txtDisposedReason.AutoSize = false;
            this.txtDisposedReason.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.txtDisposedReason.Location = new System.Drawing.Point(488, 16);
            this.txtDisposedReason.MaxLength = 512;
            this.txtDisposedReason.Multiline = true;
            this.txtDisposedReason.Name = "txtDisposedReason";
            this.txtDisposedReason.Size = new System.Drawing.Size(136, 22);
            this.txtDisposedReason.TabIndex = 3010;
            // 
            // ultraLabel24
            // 
            appearance96.BackColor = System.Drawing.Color.Transparent;
            appearance96.TextVAlignAsString = "Middle";
            this.ultraLabel24.Appearance = appearance96;
            this.ultraLabel24.Location = new System.Drawing.Point(402, 16);
            this.ultraLabel24.Name = "ultraLabel24";
            this.ultraLabel24.Size = new System.Drawing.Size(88, 22);
            this.ultraLabel24.TabIndex = 43;
            this.ultraLabel24.Text = "Lý do thanh lý ";
            // 
            // ultraLabel22
            // 
            appearance97.BackColor = System.Drawing.Color.Transparent;
            appearance97.TextVAlignAsString = "Middle";
            this.ultraLabel22.Appearance = appearance97;
            this.ultraLabel22.Location = new System.Drawing.Point(8, 16);
            this.ultraLabel22.Name = "ultraLabel22";
            this.ultraLabel22.Size = new System.Drawing.Size(89, 22);
            this.ultraLabel22.TabIndex = 39;
            this.ultraLabel22.Text = "Ngày thanh lý ";
            // 
            // ultraLabel23
            // 
            appearance98.BackColor = System.Drawing.Color.Transparent;
            appearance98.TextVAlignAsString = "Middle";
            this.ultraLabel23.Appearance = appearance98;
            this.ultraLabel23.Location = new System.Drawing.Point(208, 16);
            this.ultraLabel23.Name = "ultraLabel23";
            this.ultraLabel23.Size = new System.Drawing.Size(91, 22);
            this.ultraLabel23.TabIndex = 42;
            this.ultraLabel23.Text = "Số tiền thanh lý ";
            // 
            // nmrDisposedAmount
            // 
            appearance99.TextHAlignAsString = "Right";
            this.nmrDisposedAmount.Appearance = appearance99;
            this.nmrDisposedAmount.AutoSize = false;
            this.nmrDisposedAmount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.nmrDisposedAmount.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.nmrDisposedAmount.InputMask = "{LOC}nn,nnn,nnn.nn";
            this.nmrDisposedAmount.Location = new System.Drawing.Point(305, 16);
            this.nmrDisposedAmount.Name = "nmrDisposedAmount";
            this.nmrDisposedAmount.PromptChar = ' ';
            this.nmrDisposedAmount.Size = new System.Drawing.Size(91, 22);
            this.nmrDisposedAmount.TabIndex = 3009;
            this.nmrDisposedAmount.Text = "000";
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.cbbExpenditureAccount);
            this.ultraGroupBox2.Controls.Add(this.cbbDepreciationAccount);
            this.ultraGroupBox2.Controls.Add(this.cbbOriginalPriceAccount);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel27);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel26);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel25);
            this.ultraGroupBox2.Location = new System.Drawing.Point(336, 3);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(316, 133);
            this.ultraGroupBox2.TabIndex = 1;
            this.ultraGroupBox2.Text = "Tài khoản";
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbExpenditureAccount
            // 
            this.cbbExpenditureAccount.AutoSize = false;
            appearance100.BackColor = System.Drawing.SystemColors.Window;
            appearance100.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbExpenditureAccount.DisplayLayout.Appearance = appearance100;
            this.cbbExpenditureAccount.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbExpenditureAccount.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.cbbExpenditureAccount.DisplayLayout.DefaultSelectedForeColor = System.Drawing.Color.Black;
            appearance101.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance101.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance101.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance101.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbExpenditureAccount.DisplayLayout.GroupByBox.Appearance = appearance101;
            appearance102.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbExpenditureAccount.DisplayLayout.GroupByBox.BandLabelAppearance = appearance102;
            this.cbbExpenditureAccount.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance103.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance103.BackColor2 = System.Drawing.SystemColors.Control;
            appearance103.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance103.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbExpenditureAccount.DisplayLayout.GroupByBox.PromptAppearance = appearance103;
            this.cbbExpenditureAccount.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbExpenditureAccount.DisplayLayout.MaxRowScrollRegions = 1;
            appearance104.BackColor = System.Drawing.SystemColors.Window;
            appearance104.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbExpenditureAccount.DisplayLayout.Override.ActiveCellAppearance = appearance104;
            appearance105.BackColor = System.Drawing.SystemColors.Highlight;
            appearance105.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbExpenditureAccount.DisplayLayout.Override.ActiveRowAppearance = appearance105;
            this.cbbExpenditureAccount.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbExpenditureAccount.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance106.BackColor = System.Drawing.SystemColors.Window;
            this.cbbExpenditureAccount.DisplayLayout.Override.CardAreaAppearance = appearance106;
            appearance107.BorderColor = System.Drawing.Color.Silver;
            appearance107.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbExpenditureAccount.DisplayLayout.Override.CellAppearance = appearance107;
            this.cbbExpenditureAccount.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbExpenditureAccount.DisplayLayout.Override.CellPadding = 0;
            appearance108.BackColor = System.Drawing.SystemColors.Control;
            appearance108.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance108.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance108.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance108.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbExpenditureAccount.DisplayLayout.Override.GroupByRowAppearance = appearance108;
            appearance109.TextHAlignAsString = "Left";
            this.cbbExpenditureAccount.DisplayLayout.Override.HeaderAppearance = appearance109;
            this.cbbExpenditureAccount.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbExpenditureAccount.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance110.BackColor = System.Drawing.SystemColors.Window;
            appearance110.BorderColor = System.Drawing.Color.Silver;
            this.cbbExpenditureAccount.DisplayLayout.Override.RowAppearance = appearance110;
            this.cbbExpenditureAccount.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance111.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbExpenditureAccount.DisplayLayout.Override.TemplateAddRowAppearance = appearance111;
            this.cbbExpenditureAccount.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbExpenditureAccount.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbExpenditureAccount.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbExpenditureAccount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbExpenditureAccount.Location = new System.Drawing.Point(111, 84);
            this.cbbExpenditureAccount.Name = "cbbExpenditureAccount";
            this.cbbExpenditureAccount.Size = new System.Drawing.Size(188, 22);
            this.cbbExpenditureAccount.TabIndex = 3007;
            this.cbbExpenditureAccount.ItemNotInList += new Infragistics.Win.UltraWinGrid.ItemNotInListEventHandler(this.cbbExpenditureAccount_ItemNotInList);
            // 
            // cbbDepreciationAccount
            // 
            this.cbbDepreciationAccount.AutoSize = false;
            appearance112.BackColor = System.Drawing.SystemColors.Window;
            appearance112.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbDepreciationAccount.DisplayLayout.Appearance = appearance112;
            this.cbbDepreciationAccount.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbDepreciationAccount.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.cbbDepreciationAccount.DisplayLayout.DefaultSelectedForeColor = System.Drawing.Color.Black;
            appearance113.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance113.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance113.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance113.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbDepreciationAccount.DisplayLayout.GroupByBox.Appearance = appearance113;
            appearance114.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbDepreciationAccount.DisplayLayout.GroupByBox.BandLabelAppearance = appearance114;
            this.cbbDepreciationAccount.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance115.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance115.BackColor2 = System.Drawing.SystemColors.Control;
            appearance115.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance115.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbDepreciationAccount.DisplayLayout.GroupByBox.PromptAppearance = appearance115;
            this.cbbDepreciationAccount.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbDepreciationAccount.DisplayLayout.MaxRowScrollRegions = 1;
            appearance116.BackColor = System.Drawing.SystemColors.Window;
            appearance116.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbDepreciationAccount.DisplayLayout.Override.ActiveCellAppearance = appearance116;
            appearance117.BackColor = System.Drawing.SystemColors.Highlight;
            appearance117.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbDepreciationAccount.DisplayLayout.Override.ActiveRowAppearance = appearance117;
            this.cbbDepreciationAccount.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbDepreciationAccount.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance118.BackColor = System.Drawing.SystemColors.Window;
            this.cbbDepreciationAccount.DisplayLayout.Override.CardAreaAppearance = appearance118;
            appearance119.BorderColor = System.Drawing.Color.Silver;
            appearance119.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbDepreciationAccount.DisplayLayout.Override.CellAppearance = appearance119;
            this.cbbDepreciationAccount.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbDepreciationAccount.DisplayLayout.Override.CellPadding = 0;
            appearance120.BackColor = System.Drawing.SystemColors.Control;
            appearance120.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance120.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance120.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance120.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbDepreciationAccount.DisplayLayout.Override.GroupByRowAppearance = appearance120;
            appearance121.TextHAlignAsString = "Left";
            this.cbbDepreciationAccount.DisplayLayout.Override.HeaderAppearance = appearance121;
            this.cbbDepreciationAccount.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbDepreciationAccount.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance122.BackColor = System.Drawing.SystemColors.Window;
            appearance122.BorderColor = System.Drawing.Color.Silver;
            this.cbbDepreciationAccount.DisplayLayout.Override.RowAppearance = appearance122;
            this.cbbDepreciationAccount.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance123.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbDepreciationAccount.DisplayLayout.Override.TemplateAddRowAppearance = appearance123;
            this.cbbDepreciationAccount.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbDepreciationAccount.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbDepreciationAccount.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbDepreciationAccount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbDepreciationAccount.Location = new System.Drawing.Point(111, 53);
            this.cbbDepreciationAccount.Name = "cbbDepreciationAccount";
            this.cbbDepreciationAccount.Size = new System.Drawing.Size(188, 22);
            this.cbbDepreciationAccount.TabIndex = 3006;
            this.cbbDepreciationAccount.ItemNotInList += new Infragistics.Win.UltraWinGrid.ItemNotInListEventHandler(this.cbbDepreciationAccount_ItemNotInList);
            // 
            // cbbOriginalPriceAccount
            // 
            this.cbbOriginalPriceAccount.AutoSize = false;
            appearance124.BackColor = System.Drawing.SystemColors.Window;
            appearance124.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbOriginalPriceAccount.DisplayLayout.Appearance = appearance124;
            this.cbbOriginalPriceAccount.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbOriginalPriceAccount.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.cbbOriginalPriceAccount.DisplayLayout.DefaultSelectedForeColor = System.Drawing.Color.Black;
            appearance125.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance125.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance125.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance125.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbOriginalPriceAccount.DisplayLayout.GroupByBox.Appearance = appearance125;
            appearance126.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbOriginalPriceAccount.DisplayLayout.GroupByBox.BandLabelAppearance = appearance126;
            this.cbbOriginalPriceAccount.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance127.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance127.BackColor2 = System.Drawing.SystemColors.Control;
            appearance127.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance127.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbOriginalPriceAccount.DisplayLayout.GroupByBox.PromptAppearance = appearance127;
            this.cbbOriginalPriceAccount.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbOriginalPriceAccount.DisplayLayout.MaxRowScrollRegions = 1;
            appearance128.BackColor = System.Drawing.SystemColors.Window;
            appearance128.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbOriginalPriceAccount.DisplayLayout.Override.ActiveCellAppearance = appearance128;
            appearance129.BackColor = System.Drawing.SystemColors.Highlight;
            appearance129.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbOriginalPriceAccount.DisplayLayout.Override.ActiveRowAppearance = appearance129;
            this.cbbOriginalPriceAccount.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbOriginalPriceAccount.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance130.BackColor = System.Drawing.SystemColors.Window;
            this.cbbOriginalPriceAccount.DisplayLayout.Override.CardAreaAppearance = appearance130;
            appearance131.BorderColor = System.Drawing.Color.Silver;
            appearance131.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbOriginalPriceAccount.DisplayLayout.Override.CellAppearance = appearance131;
            this.cbbOriginalPriceAccount.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbOriginalPriceAccount.DisplayLayout.Override.CellPadding = 0;
            appearance132.BackColor = System.Drawing.SystemColors.Control;
            appearance132.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance132.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance132.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance132.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbOriginalPriceAccount.DisplayLayout.Override.GroupByRowAppearance = appearance132;
            appearance133.TextHAlignAsString = "Left";
            this.cbbOriginalPriceAccount.DisplayLayout.Override.HeaderAppearance = appearance133;
            this.cbbOriginalPriceAccount.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbOriginalPriceAccount.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance134.BackColor = System.Drawing.SystemColors.Window;
            appearance134.BorderColor = System.Drawing.Color.Silver;
            this.cbbOriginalPriceAccount.DisplayLayout.Override.RowAppearance = appearance134;
            this.cbbOriginalPriceAccount.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance135.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbOriginalPriceAccount.DisplayLayout.Override.TemplateAddRowAppearance = appearance135;
            this.cbbOriginalPriceAccount.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbOriginalPriceAccount.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbOriginalPriceAccount.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbOriginalPriceAccount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbOriginalPriceAccount.Location = new System.Drawing.Point(111, 22);
            this.cbbOriginalPriceAccount.Name = "cbbOriginalPriceAccount";
            this.cbbOriginalPriceAccount.Size = new System.Drawing.Size(188, 22);
            this.cbbOriginalPriceAccount.TabIndex = 3005;
            this.cbbOriginalPriceAccount.ItemNotInList += new Infragistics.Win.UltraWinGrid.ItemNotInListEventHandler(this.cbbOriginalPriceAccount_ItemNotInList);
            // 
            // ultraLabel27
            // 
            appearance136.BackColor = System.Drawing.Color.Transparent;
            appearance136.TextVAlignAsString = "Middle";
            this.ultraLabel27.Appearance = appearance136;
            this.ultraLabel27.Location = new System.Drawing.Point(6, 84);
            this.ultraLabel27.Name = "ultraLabel27";
            this.ultraLabel27.Size = new System.Drawing.Size(99, 22);
            this.ultraLabel27.TabIndex = 49;
            this.ultraLabel27.Text = "TK chi phí (*)";
            // 
            // ultraLabel26
            // 
            appearance137.BackColor = System.Drawing.Color.Transparent;
            appearance137.TextVAlignAsString = "Middle";
            this.ultraLabel26.Appearance = appearance137;
            this.ultraLabel26.Location = new System.Drawing.Point(6, 53);
            this.ultraLabel26.Name = "ultraLabel26";
            this.ultraLabel26.Size = new System.Drawing.Size(99, 22);
            this.ultraLabel26.TabIndex = 47;
            this.ultraLabel26.Text = "TK khấu hao (*)";
            // 
            // ultraLabel25
            // 
            appearance138.BackColor = System.Drawing.Color.Transparent;
            appearance138.TextVAlignAsString = "Middle";
            this.ultraLabel25.Appearance = appearance138;
            this.ultraLabel25.Location = new System.Drawing.Point(6, 22);
            this.ultraLabel25.Name = "ultraLabel25";
            this.ultraLabel25.Size = new System.Drawing.Size(99, 22);
            this.ultraLabel25.TabIndex = 45;
            this.ultraLabel25.Text = "TK nguyên giá (*)";
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.cldDepreciationDate);
            this.ultraGroupBox1.Controls.Add(this.cldUsedDate);
            this.ultraGroupBox1.Controls.Add(this.cldIncrementDate);
            this.ultraGroupBox1.Controls.Add(this.cldPurchasedDate);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel21);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel20);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel19);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel18);
            this.ultraGroupBox1.Location = new System.Drawing.Point(11, 3);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(319, 133);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.Text = "Ngày tháng mua";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cldDepreciationDate
            // 
            appearance139.TextVAlignAsString = "Middle";
            this.cldDepreciationDate.Appearance = appearance139;
            this.cldDepreciationDate.AutoSize = false;
            this.cldDepreciationDate.Location = new System.Drawing.Point(171, 101);
            this.cldDepreciationDate.MaskInput = "dd/mm/yyyy";
            this.cldDepreciationDate.Name = "cldDepreciationDate";
            this.cldDepreciationDate.Size = new System.Drawing.Size(137, 22);
            this.cldDepreciationDate.TabIndex = 3011;
            this.cldDepreciationDate.Value = null;
            // 
            // cldUsedDate
            // 
            appearance140.TextVAlignAsString = "Middle";
            this.cldUsedDate.Appearance = appearance140;
            this.cldUsedDate.AutoSize = false;
            this.cldUsedDate.Location = new System.Drawing.Point(171, 75);
            this.cldUsedDate.MaskInput = "dd/mm/yyyy";
            this.cldUsedDate.Name = "cldUsedDate";
            this.cldUsedDate.Size = new System.Drawing.Size(137, 22);
            this.cldUsedDate.TabIndex = 3010;
            this.cldUsedDate.Value = null;
            // 
            // cldIncrementDate
            // 
            appearance141.TextVAlignAsString = "Middle";
            this.cldIncrementDate.Appearance = appearance141;
            this.cldIncrementDate.AutoSize = false;
            this.cldIncrementDate.Location = new System.Drawing.Point(171, 49);
            this.cldIncrementDate.MaskInput = "dd/mm/yyyy";
            this.cldIncrementDate.Name = "cldIncrementDate";
            this.cldIncrementDate.Size = new System.Drawing.Size(137, 22);
            this.cldIncrementDate.TabIndex = 3009;
            this.cldIncrementDate.Value = null;
            // 
            // cldPurchasedDate
            // 
            appearance142.TextVAlignAsString = "Middle";
            this.cldPurchasedDate.Appearance = appearance142;
            this.cldPurchasedDate.AutoSize = false;
            this.cldPurchasedDate.Location = new System.Drawing.Point(171, 23);
            this.cldPurchasedDate.MaskInput = "dd/mm/yyyy";
            this.cldPurchasedDate.Name = "cldPurchasedDate";
            this.cldPurchasedDate.Size = new System.Drawing.Size(137, 22);
            this.cldPurchasedDate.TabIndex = 3008;
            this.cldPurchasedDate.Value = null;
            // 
            // ultraLabel21
            // 
            appearance143.BackColor = System.Drawing.Color.Transparent;
            appearance143.TextVAlignAsString = "Middle";
            this.ultraLabel21.Appearance = appearance143;
            this.ultraLabel21.Location = new System.Drawing.Point(10, 101);
            this.ultraLabel21.Name = "ultraLabel21";
            this.ultraLabel21.Size = new System.Drawing.Size(155, 22);
            this.ultraLabel21.TabIndex = 37;
            this.ultraLabel21.Text = "Ngày BĐ tính khấu hao (*)";
            // 
            // ultraLabel20
            // 
            appearance144.BackColor = System.Drawing.Color.Transparent;
            appearance144.TextVAlignAsString = "Middle";
            this.ultraLabel20.Appearance = appearance144;
            this.ultraLabel20.Location = new System.Drawing.Point(10, 75);
            this.ultraLabel20.Name = "ultraLabel20";
            this.ultraLabel20.Size = new System.Drawing.Size(155, 22);
            this.ultraLabel20.TabIndex = 35;
            this.ultraLabel20.Text = "Ngày sử dụng ";
            // 
            // ultraLabel19
            // 
            appearance145.BackColor = System.Drawing.Color.Transparent;
            appearance145.TextVAlignAsString = "Middle";
            this.ultraLabel19.Appearance = appearance145;
            this.ultraLabel19.Location = new System.Drawing.Point(10, 49);
            this.ultraLabel19.Name = "ultraLabel19";
            this.ultraLabel19.Size = new System.Drawing.Size(155, 22);
            this.ultraLabel19.TabIndex = 33;
            this.ultraLabel19.Text = "Ngày ghi tăng (*)";
            // 
            // ultraLabel18
            // 
            appearance146.BackColor = System.Drawing.Color.Transparent;
            appearance146.TextVAlignAsString = "Middle";
            this.ultraLabel18.Appearance = appearance146;
            this.ultraLabel18.Location = new System.Drawing.Point(10, 23);
            this.ultraLabel18.Name = "ultraLabel18";
            this.ultraLabel18.Size = new System.Drawing.Size(155, 22);
            this.ultraLabel18.TabIndex = 31;
            this.ultraLabel18.Text = "Ngày mua ";
            // 
            // ultraTabPageControl3
            // 
            this.ultraTabPageControl3.Controls.Add(this.UGridDetails);
            this.ultraTabPageControl3.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl3.Name = "ultraTabPageControl3";
            this.ultraTabPageControl3.Size = new System.Drawing.Size(663, 403);
            // 
            // UGridDetails
            // 
            this.UGridDetails.ContextMenuStrip = this.tsmFixedAssetDetail;
            this.UGridDetails.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.UGridDetails.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.UGridDetails.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.UGridDetails.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.UGridDetails.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.AlignWithDataRows;
            this.UGridDetails.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.UGridDetails.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.UGridDetails.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.UGridDetails.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.UGridDetails.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.UGridDetails.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.UGridDetails.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.UGridDetails.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.UGridDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UGridDetails.Location = new System.Drawing.Point(0, 0);
            this.UGridDetails.Name = "UGridDetails";
            this.UGridDetails.Size = new System.Drawing.Size(663, 403);
            this.UGridDetails.TabIndex = 2;
            this.UGridDetails.Text = "uGrid";
            this.UGridDetails.BeforeRowInsert += new Infragistics.Win.UltraWinGrid.BeforeRowInsertEventHandler(this.UGridDetails_BeforeRowInsert);
            this.UGridDetails.BeforeCellUpdate += new Infragistics.Win.UltraWinGrid.BeforeCellUpdateEventHandler(this.UGridDetails_BeforeCellUpdate);
            this.UGridDetails.KeyUp += new System.Windows.Forms.KeyEventHandler(this.UGridDetails_KeyUp);
            // 
            // tsmFixedAssetDetail
            // 
            this.tsmFixedAssetDetail.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmAdd1,
            this.tsmDelete1});
            this.tsmFixedAssetDetail.Name = "tsmFixedAssetDetail";
            this.tsmFixedAssetDetail.Size = new System.Drawing.Size(228, 48);
            // 
            // tsmAdd1
            // 
            this.tsmAdd1.Name = "tsmAdd1";
            this.tsmAdd1.Size = new System.Drawing.Size(227, 22);
            this.tsmAdd1.Text = "Thêm mới một mô tả chi tiết";
            this.tsmAdd1.Click += new System.EventHandler(this.tsmAdd1_Click);
            // 
            // tsmDelete1
            // 
            this.tsmDelete1.Name = "tsmDelete1";
            this.tsmDelete1.Size = new System.Drawing.Size(227, 22);
            this.tsmDelete1.Text = "Xóa một dòng mô tả chi tiết";
            this.tsmDelete1.Click += new System.EventHandler(this.tsmDelete1_Click);
            // 
            // ultraTabPageControl4
            // 
            this.ultraTabPageControl4.Controls.Add(this.uGridFixedAssetAccessories);
            this.ultraTabPageControl4.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl4.Name = "ultraTabPageControl4";
            this.ultraTabPageControl4.Size = new System.Drawing.Size(663, 403);
            // 
            // uGridFixedAssetAccessories
            // 
            this.uGridFixedAssetAccessories.ContextMenuStrip = this.tsmFixedAssetAccessories;
            appearance147.BackColor = System.Drawing.SystemColors.Window;
            appearance147.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridFixedAssetAccessories.DisplayLayout.Appearance = appearance147;
            this.uGridFixedAssetAccessories.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGridFixedAssetAccessories.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridFixedAssetAccessories.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance148.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance148.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance148.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance148.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridFixedAssetAccessories.DisplayLayout.GroupByBox.Appearance = appearance148;
            appearance149.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridFixedAssetAccessories.DisplayLayout.GroupByBox.BandLabelAppearance = appearance149;
            this.uGridFixedAssetAccessories.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance150.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance150.BackColor2 = System.Drawing.SystemColors.Control;
            appearance150.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance150.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridFixedAssetAccessories.DisplayLayout.GroupByBox.PromptAppearance = appearance150;
            appearance151.BackColor = System.Drawing.SystemColors.Window;
            appearance151.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridFixedAssetAccessories.DisplayLayout.Override.ActiveCellAppearance = appearance151;
            appearance152.BackColor = System.Drawing.SystemColors.Highlight;
            appearance152.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridFixedAssetAccessories.DisplayLayout.Override.ActiveRowAppearance = appearance152;
            this.uGridFixedAssetAccessories.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridFixedAssetAccessories.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance153.BackColor = System.Drawing.SystemColors.Window;
            this.uGridFixedAssetAccessories.DisplayLayout.Override.CardAreaAppearance = appearance153;
            appearance154.BorderColor = System.Drawing.Color.Silver;
            appearance154.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridFixedAssetAccessories.DisplayLayout.Override.CellAppearance = appearance154;
            this.uGridFixedAssetAccessories.DisplayLayout.Override.CellPadding = 0;
            appearance155.BackColor = System.Drawing.SystemColors.Control;
            appearance155.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance155.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance155.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance155.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridFixedAssetAccessories.DisplayLayout.Override.GroupByRowAppearance = appearance155;
            appearance156.TextHAlignAsString = "Left";
            this.uGridFixedAssetAccessories.DisplayLayout.Override.HeaderAppearance = appearance156;
            this.uGridFixedAssetAccessories.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance157.BackColor = System.Drawing.SystemColors.Window;
            appearance157.BorderColor = System.Drawing.Color.Silver;
            this.uGridFixedAssetAccessories.DisplayLayout.Override.RowAppearance = appearance157;
            this.uGridFixedAssetAccessories.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance158.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridFixedAssetAccessories.DisplayLayout.Override.TemplateAddRowAppearance = appearance158;
            this.uGridFixedAssetAccessories.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridFixedAssetAccessories.Location = new System.Drawing.Point(0, 0);
            this.uGridFixedAssetAccessories.Name = "uGridFixedAssetAccessories";
            this.uGridFixedAssetAccessories.Size = new System.Drawing.Size(663, 403);
            this.uGridFixedAssetAccessories.TabIndex = 0;
            this.uGridFixedAssetAccessories.Text = "uGridFixedAssetAccessories";
            // 
            // tsmFixedAssetAccessories
            // 
            this.tsmFixedAssetAccessories.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmAdd,
            this.tsmDelete});
            this.tsmFixedAssetAccessories.Name = "contextMenuStrip1";
            this.tsmFixedAssetAccessories.Size = new System.Drawing.Size(263, 48);
            // 
            // tsmAdd
            // 
            this.tsmAdd.Name = "tsmAdd";
            this.tsmAdd.Size = new System.Drawing.Size(262, 22);
            this.tsmAdd.Text = "Thêm mới dòng phụ kiện kèm theo";
            this.tsmAdd.Click += new System.EventHandler(this.tsmAdd_Click);
            // 
            // tsmDelete
            // 
            this.tsmDelete.Name = "tsmDelete";
            this.tsmDelete.Size = new System.Drawing.Size(262, 22);
            this.tsmDelete.Text = "Xóa một dòng phụ kiện kèm theo";
            this.tsmDelete.Click += new System.EventHandler(this.tsmDelete_Click);
            // 
            // ultraTabControl1
            // 
            this.ultraTabControl1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl2);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl3);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl4);
            this.ultraTabControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.ultraTabControl1.Name = "ultraTabControl1";
            this.ultraTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.ultraTabControl1.Size = new System.Drawing.Size(665, 426);
            this.ultraTabControl1.TabIndex = 2;
            ultraTab6.TabPage = this.ultraTabPageControl1;
            ultraTab6.Text = "&1. Thông tin chung";
            ultraTab7.TabPage = this.ultraTabPageControl2;
            ultraTab7.Text = "&2. Thông tin khấu hao";
            ultraTab1.TabPage = this.ultraTabPageControl3;
            ultraTab1.Text = "&3. Thành phần";
            ultraTab2.TabPage = this.ultraTabPageControl4;
            ultraTab2.Text = "&4. Phụ kiện kèm theo";
            this.ultraTabControl1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab6,
            ultraTab7,
            ultraTab1,
            ultraTab2});
            this.ultraTabControl1.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007;
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(663, 403);
            // 
            // btnClose
            // 
            appearance159.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance159;
            this.btnClose.Location = new System.Drawing.Point(578, 435);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 55;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            appearance160.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance160;
            this.btnSave.Location = new System.Drawing.Point(497, 435);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 54;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnSaveContinue
            // 
            appearance161.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSaveContinue.Appearance = appearance161;
            this.btnSaveContinue.Location = new System.Drawing.Point(357, 435);
            this.btnSaveContinue.Name = "btnSaveContinue";
            this.btnSaveContinue.Size = new System.Drawing.Size(134, 30);
            this.btnSaveContinue.TabIndex = 3021;
            this.btnSaveContinue.Text = "Lưu và Thêm mới";
            this.btnSaveContinue.Click += new System.EventHandler(this.btnSaveContinue_Click);
            // 
            // chkActive
            // 
            appearance162.TextVAlignAsString = "Middle";
            this.chkActive.Appearance = appearance162;
            this.chkActive.BackColor = System.Drawing.Color.Transparent;
            this.chkActive.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkActive.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkActive.Location = new System.Drawing.Point(8, 439);
            this.chkActive.Name = "chkActive";
            this.chkActive.Size = new System.Drawing.Size(103, 22);
            this.chkActive.TabIndex = 3022;
            this.chkActive.Text = "Ngừng theo dõi";
            // 
            // FFixedAssetDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(665, 471);
            this.Controls.Add(this.chkActive);
            this.Controls.Add(this.btnSaveContinue);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.ultraTabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FFixedAssetDetail";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thông tin tài sản cố định";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FFixedAssetDetail_FormClosed);
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbWarrantyTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cldDeliveryRecordDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDepartmentID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbFixedAssetCategoryID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsIrrationalCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCurrentState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSerialNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsSecondHand)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccessories)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFixedAssetCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFixedAssetName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeliveryRecordNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGuaranteeCodition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmrProductionYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMadeIn)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).EndInit();
            this.ultraGroupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CbbYearMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDepreciationMethod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).EndInit();
            this.ultraGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbCostSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsDepreciationAllocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cldDisposedDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDisposedReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbExpenditureAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDepreciationAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbOriginalPriceAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cldDepreciationDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cldUsedDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cldIncrementDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cldPurchasedDate)).EndInit();
            this.ultraTabPageControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.UGridDetails)).EndInit();
            this.tsmFixedAssetDetail.ResumeLayout(false);
            this.ultraTabPageControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridFixedAssetAccessories)).EndInit();
            this.tsmFixedAssetAccessories.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).EndInit();
            this.ultraTabControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkActive)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtGuaranteeCodition;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMadeIn;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor nmrProductionYear;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtFixedAssetName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtFixedAssetCode;
        private Infragistics.Win.Misc.UltraLabel ultraLabel15;
        private Infragistics.Win.Misc.UltraLabel ultraLabel14;
        private Infragistics.Win.Misc.UltraLabel ultraLabel13;
        private Infragistics.Win.Misc.UltraLabel ultraLabel12;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccessories;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDescription;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsSecondHand;
        private Infragistics.Win.Misc.UltraLabel ultraLabel16;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSerialNumber;
        private Infragistics.Win.Misc.UltraLabel ultraLabel17;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbCurrentState;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel21;
        private Infragistics.Win.Misc.UltraLabel ultraLabel20;
        private Infragistics.Win.Misc.UltraLabel ultraLabel19;
        private Infragistics.Win.Misc.UltraLabel ultraLabel18;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel22;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit nmrDisposedAmount;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDisposedReason;
        private Infragistics.Win.Misc.UltraLabel ultraLabel24;
        private Infragistics.Win.Misc.UltraLabel ultraLabel23;
        private Infragistics.Win.Misc.UltraLabel ultraLabel25;
        private Infragistics.Win.Misc.UltraLabel ultraLabel27;
        private Infragistics.Win.Misc.UltraLabel ultraLabel26;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox4;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsDepreciationAllocation;
        private Infragistics.Win.Misc.UltraLabel ultraLabel28;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel29;
        private Infragistics.Win.Misc.UltraLabel ultraLabel31;
        private Infragistics.Win.Misc.UltraLabel ultraLabel30;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor CbbYearMonth;
        private Infragistics.Win.Misc.UltraLabel ultraLabel32;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbDepreciationMethod;
        private Infragistics.Win.Misc.UltraLabel ultraLabel33;
        private Infragistics.Win.Misc.UltraLabel ultraLabel34;
        private Infragistics.Win.Misc.UltraLabel ultraLabel35;
        private Infragistics.Win.Misc.UltraLabel ultraLabel36;
        private Infragistics.Win.Misc.UltraLabel ultraLabel37;
        private Infragistics.Win.Misc.UltraLabel ultraLabel38;
        private Infragistics.Win.Misc.UltraButton btnOriginalPrice;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsIrrationalCost;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Core.Domain.FixedAsset temp;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit nmrUsedTime;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit nmrMonthPeriodDepreciationAmount;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit nmrMonthDepreciationRate;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit nmrAcDepreciationAmount;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit nmrPeriodDepreciationAmount;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit nmrDepreciationRate;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbOriginalPriceAccount;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbExpenditureAccount;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbDepreciationAccount;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbCostSet;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbDepartmentID;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbFixedAssetCategoryID;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObject;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDeliveryRecordNo;
        private Infragistics.Win.UltraWinGrid.UltraGrid UGridDetails;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit nmrPurchasePrice;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit nmrOriginalPrice;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit nmrRemainingAmount;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl4;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridFixedAssetAccessories;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor cldDeliveryRecordDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor cldPurchasedDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor cldIncrementDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor cldUsedDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor cldDepreciationDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor cldDisposedDate;
        private System.Windows.Forms.ContextMenuStrip tsmFixedAssetAccessories;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripMenuItem tsmDelete;
        private System.Windows.Forms.ContextMenuStrip tsmFixedAssetDetail;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd1;
        private System.Windows.Forms.ToolStripMenuItem tsmDelete1;
        private Infragistics.Win.Misc.UltraButton btnSaveContinue;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkActive;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbWarrantyTime;
    }
}