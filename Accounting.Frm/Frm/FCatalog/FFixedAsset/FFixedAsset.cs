﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using Accounting.TextMessage;
using Accounting.Core;

namespace Accounting
{
    public partial class FFixedAsset : CatalogBase
    {
        #region khai báo
        private readonly IFixedAssetService _IFixedAssetService = Utils.IFixedAssetService;
        private readonly IFixedAssetCategoryService _IFixedAssetCategoryService = Utils.IFixedAssetCategoryService;
        private readonly IFixedAssetDetailService _IFixedAssetDetailService = Utils.IFixedAssetDetailService;
        private readonly IFAIncrementDetailService _IFAIncrementDetailService = Utils.IFAIncrementDetailService;
        List<VoucherFixedAsset> listVoucherFixedAsset = new List<VoucherFixedAsset>();
        private readonly List<Item> _lstItems = Utils.ObjConstValue.SelectTimes;
        private DateTime ngayHoachToan;
        private DateTime dtBegin;
        private DateTime dtEnd;
        private Guid selectedGuid;
        #endregion

        #region khởi tạo

        public FFixedAsset()
        {
            #region Khởi tạo giá trị mặc định của Form

            InitializeComponent();

            #endregion

            #region Tạo dữ liệu ban đầu
            
            LoadDuLieu();
            if (uGrid.Rows.Count > 0)
            {
                uGrid.Rows[0].Selected = true;
            }
            #endregion
            ToolTip ToolTip1 = new ToolTip();
            ToolTip1.SetToolTip(btnAdd, "Thêm (Ctrl + N)");
            ToolTip ToolTip2 = new ToolTip();
            ToolTip2.SetToolTip(btnEdit, "Xem (Ctrl + E)");
            ToolTip ToolTip3 = new ToolTip();
            ToolTip3.SetToolTip(btnDelete, "Xóa (Ctrl + D)");
            if (uGrid.Rows.Count > 0) uGrid.Rows[0].Selected = true;
           
            ngayHoachToan = (DateTime)Utils.StringToDateTime(ConstFrm.DbStartDate);
            //dtBegin = ngayHoachToan.AddDays(1 - ngayHoachToan.Day);
            //dtEnd = ngayHoachToan;
            //dteToDate.DateTime = dtEnd;
            //dteFromDate.DateTime = dtBegin;
            cbbAboutTime.DataSource = _lstItems;
            cbbAboutTime.DisplayMember = "Name";
            Utils.ConfigGrid(cbbAboutTime, ConstDatabase.ConfigXML_TableName);
            cbbAboutTime.SelectedRow = cbbAboutTime.Rows[7];
            cbbAboutTime.DisplayLayout.AutoFitStyle = AutoFitStyle.ExtendLastColumn;
        }

        private void LoadDuLieu()
        {
            LoadDuLieu(true);
        }

        private void LoadDuLieu(bool configGrid)
        {
            WaitingFrm.StartWaiting();
            #region Lấy dữ liệu từ CSDL

            List<FixedAsset> list = _IFixedAssetService.FindByNoIsNull(true);
            _IFixedAssetService.UnbindSession(list);
            list = _IFixedAssetService.FindByNoIsNull(true);
            Utils.ClearCacheByType<FAIncrementDetail>();
            Utils.ClearCacheByType<FixedAssetLedger>();
            foreach (FixedAsset fa in list)
            {
                //FAIncrementDetail detail = Utils.IFAIncrementDetailService.FindByFixedAssetID(fa.ID);
                FAIncrementDetail detail = Utils.ListFAIncrementDetail.FirstOrDefault(n => n.FixedAssetID == fa.ID);
                if (detail != null)
                {
                    //FixedAssetLedger fal = Utils.IFixedAssetLedgerService.findByRefIdAndFixedAssetId(detail.FAIncrementID, fa.ID);
                    FixedAssetLedger fal = Utils.ListFixedAssetLedger.FirstOrDefault(n => n.ReferenceID == detail.FAIncrementID && n.FixedAssetID == fa.ID);
                    if (fal != null)
                    {
                        fa.OriginalPrice = fal.DepreciationAmount;
                        fa.PurchasePrice = fal.DepreciationAmount;
                        fa.MonthPeriodDepreciationAmount = fal.MonthPeriodDepreciationAmount;
                        fa.PeriodDepreciationAmount = fal.MonthPeriodDepreciationAmount * 12;
                    }
                }
                
            }
            List<FixedAssetCategory> lst2 = _IFixedAssetCategoryService.GetAll();
            #endregion

            #region Xử lý dữ liệu

            //thiết lập tính chất
            foreach (var item in list)
            {
                foreach (var ls2 in lst2)
                {
                    if (item.FixedAssetCategoryID == ls2.ID)
                    {
                        item.FixedAssetCategory = ls2;
                        item.FixedAssetCategoryView = ls2.FixedAssetCategoryName;
                    }
                }
            }

            #endregion

            #region hiển thị và xử lý hiển thị

            uGrid.DataSource = list.OrderBy(x => x.FixedAssetCode).ToArray();

            if (configGrid) ConfigGrid(uGrid);
            uGrid.DisplayLayout.Bands[0].Columns["Quantity"].FormatNumberic(ConstDatabase.Format_Quantity);
            uGrid.DisplayLayout.Bands[0].Columns["OriginalPrice"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Extended;
            
            uGrid.DisplayLayout.Bands[0].Columns["FixedAssetCode"].Width = 100;
            uGrid.DisplayLayout.Bands[0].Columns["FixedAssetName"].Width = 150;
            uGrid.DisplayLayout.Bands[0].Columns["FixedAssetCategoryView"].Width = 600;
            uGrid.DisplayLayout.Bands[0].Columns["DepartmentID"].Width = 150;
            uGrid.DisplayLayout.Bands[0].Columns["UsedDate"].Width = 100;

            #endregion
            WaitingFrm.StopWaiting();
        }
        private void LoadDuLieuExport(bool configGrid)
        {
            WaitingFrm.StartWaiting();
            #region Lấy dữ liệu từ CSDL

            List<FixedAsset> list = _IFixedAssetService.FindByNoIsNull(true);
            _IFixedAssetService.UnbindSession(list);
            list = _IFixedAssetService.FindByNoIsNull(true);
            Utils.ClearCacheByType<FAIncrementDetail>();
            Utils.ClearCacheByType<FixedAssetLedger>();
            foreach (FixedAsset fa in list)
            {
                //FAIncrementDetail detail = Utils.IFAIncrementDetailService.FindByFixedAssetID(fa.ID);
                FAIncrementDetail detail = Utils.ListFAIncrementDetail.FirstOrDefault(n => n.FixedAssetID == fa.ID);
                if (detail != null)
                {
                    //FixedAssetLedger fal = Utils.IFixedAssetLedgerService.findByRefIdAndFixedAssetId(detail.FAIncrementID, fa.ID);
                    FixedAssetLedger fal = Utils.ListFixedAssetLedger.FirstOrDefault(n => n.ReferenceID == detail.FAIncrementID && n.FixedAssetID == fa.ID);
                    if (fal != null)
                    {
                        fa.OriginalPrice = fal.DepreciationAmount;
                        fa.PurchasePrice = fal.DepreciationAmount;
                        fa.MonthPeriodDepreciationAmount = fal.MonthPeriodDepreciationAmount;
                        fa.PeriodDepreciationAmount = fal.MonthPeriodDepreciationAmount * 12;
                    }
                }

            }
            List<FixedAssetCategory> lst2 = _IFixedAssetCategoryService.GetAll();
            #endregion

            #region Xử lý dữ liệu

            //thiết lập tính chất
            foreach (var item in list)
            {
                foreach (var ls2 in lst2)
                {
                    if (item.AccountingObjectID!=null)
                    {
                        AccountingObject accountingObject = Utils.IAccountingObjectService.Query.Where(n => n.ID == item.AccountingObjectID).FirstOrDefault();
                        if(accountingObject!=null)
                        { item.MaNhaCungCap = accountingObject.AccountingObjectCode; }
                    }
                    
                }
            }

            #endregion

            #region hiển thị và xử lý hiển thị

            uGridExport.DataSource = list.OrderBy(x => x.FixedAssetCode).ToArray();

            if (configGrid) { Utils.ConfigGrid(uGridExport, ConstDatabase.FixedAsset_TableNameExport); }
            uGrid.DisplayLayout.Bands[0].Columns["Quantity"].FormatNumberic(ConstDatabase.Format_Quantity);
            uGrid.DisplayLayout.Bands[0].Columns["OriginalPrice"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Extended;

            uGrid.DisplayLayout.Bands[0].Columns["FixedAssetCode"].Width = 100;
            uGrid.DisplayLayout.Bands[0].Columns["FixedAssetName"].Width = 150;
            uGrid.DisplayLayout.Bands[0].Columns["FixedAssetCategoryView"].Width = 600;
            uGrid.DisplayLayout.Bands[0].Columns["DepartmentID"].Width = 150;
            uGrid.DisplayLayout.Bands[0].Columns["UsedDate"].Width = 100;

            #endregion
            WaitingFrm.StopWaiting();
        }
        #endregion

        #region Nghiệp vụ

        #region ContentMenu

        private void tsmAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void tsmEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            ResetFunction();
        }
        protected override void ResetFunction()
        {
            LoadDuLieu(false);
        }

        #endregion

        #region Button

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        #endregion

        private void uGrid_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (e.Row.Index >= 0)
            {
                FixedAsset temp = e.Row.ListObject as FixedAsset;
                // Để sửa danh mục TSCĐ --> TSCĐ phải chưa ghi tăng
                //List<FAIncrementDetail> fAIncrementDetails = _IFAIncrementDetailService.FindByFixedAssetCode(temp.ID);
                //if (fAIncrementDetails.Count > 0)
                //{
                //    MSG.Warning("Không thể sửa thông tin của tài sản này tại đây vì đã phát sinh chứng từ liên quan. Vui lòng thực hiện việc sửa đổi tại nghiệp vụ Điều chỉnh Tài sản cố định!");
                //    return;
                //}

                new FFixedAssetDetail(temp).ShowDialog(this);
                if (!FFixedAssetDetail.isClose) { Utils.ClearCacheByType<FixedAsset>(); LoadDuLieu(); }
            }
        }

        /// <summary>
        /// Nghiệp vụ Add
        /// </summary>
        protected override void AddFunction()
        {
            new FFixedAssetDetail().ShowDialog(this);
            if (!FFixedAssetDetail.isClose) { Utils.ClearCacheByType<FixedAsset>(); LoadDuLieu(); Utils.IFAIncrementDetailService.RolbackTran(); }
        }

        /// <summary>
        /// Nghiệp vụ Edit
        /// </summary>
        protected override void EditFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                FixedAsset temp = uGrid.Selected.Rows[0].ListObject as FixedAsset;
                // Để sửa danh mục TSCĐ --> TSCĐ phải chưa ghi tăng
                //List<FAIncrementDetail> fAIncrementDetails = _IFAIncrementDetailService.FindByFixedAssetCode(temp.ID);
                //if (fAIncrementDetails.Count > 0)
                //{
                //    MSG.Warning("Không thể sửa thông tin của tài sản này tại đây vì đã phát sinh chứng từ liên quan. Vui lòng thực hiện việc sửa đổi tại nghiệp vụ Điều chỉnh Tài sản cố định!");
                //    return;
                //}

                new FFixedAssetDetail(temp).ShowDialog(this);
                if (!FFixedAssetDetail.isClose) { Utils.ClearCacheByType<FixedAsset>(); LoadDuLieu(); Utils.IFAIncrementDetailService.RolbackTran(); }
            }
            else
                Accounting.TextMessage.MSG.Error(string.Format(Accounting.TextMessage.resSystem.MSG_Catalog3, "một Tài sản cố định"));
        }

        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        protected override void DeleteFunction()
        {
            bool a = uGrid.Selected.Rows.Count > 0;
            bool b = uGrid.ActiveRow != null;
            if (a || b)
            {
                if (!(DialogResult.Yes == MessageBox.Show("Bạn có muốn xóa những dữ liệu này không ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question)))
                    return;
                SelectedRowsCollection lst = uGrid.Selected.Rows;
                var strResult = new List<Result>();
                foreach (UltraGridRow ultraGridRow in lst)
                {
                    var result = new Result();
                    FixedAsset temp = ultraGridRow.ListObject as FixedAsset;
                    FixedAsset data = Utils.IFixedAssetService.Getbykey(temp.ID);
                    try
                    {
                        if (!_IFixedAssetService.CheckDeleteFixedAsset(data.ID))
                        {
                            
                            Utils.IFixedAssetService.BeginTran();
                            Utils.IFixedAssetService.Delete(data);
                            Utils.IFixedAssetService.CommitTran();
                            Utils.ClearCacheByType<FixedAsset>();
                            result.Reason = "Xóa thành công";
                            result.Code = data.FixedAssetCode;
                            result.IsSucceed = true;
                            strResult.Add(result);


                        }
                        else
                        {
                            result.Reason = "Không thể xóa do dữ liệu đã phát sinh chứng từ liên quan";
                            result.Code = data.FixedAssetCode;
                            result.IsSucceed = false;
                            strResult.Add(result);
                        }
                    }
                    catch (Exception ex)
                    {
                        Utils.IFixedAssetService.RolbackTran();
                        MSG.Error("Xóa chứng từ thất bại");
                    }
                }
                if (lst.Count > 1 && strResult.Count > 0) new MsgResult(strResult, false).ShowDialog(this);
                else MSG.Information(strResult.First().Reason);
                LoadDuLieu();
            }
            //if (uGrid.Selected.Rows.Count > 0)
            //{
            //    FixedAsset temp = uGrid.Selected.Rows[0].ListObject as FixedAsset;

            //    // Để xóa danh mục TSCĐ --> TSCĐ phải chưa ghi tăng, hoặc không phải là khai báo
            //    if (_IFixedAssetService.CheckDeleteFixedAsset(temp.ID))
            //    {
            //        MSG.Warning("Không thể xóa tài sản này vì tài sản này đã có phát sinh chứng từ liên quan!");
            //        return;
            //    }

            //    if (MSG.Question(string.Format(resSystem.MSG_System_05, temp.FixedAssetName)) ==
            //        System.Windows.Forms.DialogResult.Yes)
            //    {
            //        _IFixedAssetService.BeginTran();
            //        _IFixedAssetService.Delete(temp);
            //        _IFixedAssetService.CommitTran();
            //        Utils.ClearCacheByType<FixedAsset>();
            //        LoadDuLieu();
            //    }
            //}
            //else
            //    MSG.Error(string.Format(resSystem.MSG_Catalog2, "một Tài sản cố định"));
        }

        #endregion

        #region Utils

        private void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {
            //hàm chung
            utralGrid.ConfigGrid(ConstDatabase.FixedAsset_TableName);
            //foreach (var col in utralGrid.DisplayLayout.Bands[0].Columns)
            //{
            //    this.ConfigEachColumn4Grid(0, col, utralGrid);
            //}
            var column = utralGrid.DisplayLayout.Bands[0].Columns["DepartmentID"];
            this.ConfigCbbToGrid(utralGrid, column.Key, Utils.ListDepartment, "ID", "DepartmentName",
                                ConstDatabase.Department_TableName, "ParentID", "IsParentNode");
            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownValidate;
            column.CellActivation = Activation.NoEdit;

        }

        #endregion

        private void uGrid_BeforeRowActivate(object sender, RowEventArgs e)
        {
            if (e.Row == null) return;
            var fixedAsset = e.Row.ListObject as FixedAsset;
            if (fixedAsset == null) return;
            lblFixedAssetCode.Text = fixedAsset.FixedAssetCode;
            lblFixedAssetName.Text = fixedAsset.FixedAssetName;
            if (fixedAsset.FixedAssetCategory != null)
            {
                lblFixedAssetCategory.Text = fixedAsset.FixedAssetCategoryView;
            }
            if (fixedAsset.Department != null && fixedAsset.Department.ID != Guid.Empty)
            {
                lblDepartmentID.Text = fixedAsset.Department.DepartmentName;
            }
            lblSerialNumber.Text = fixedAsset.SerialNumber;
            lblMadeIn.Text = fixedAsset.MadeIn;
            lblProductionYear.Text = fixedAsset.ProductionYear != null ? fixedAsset.ProductionYear.ToString() : "";
            
            lblOriginalPrice.Text = fixedAsset.OriginalPrice.ToStringNumbericFormat(ConstDatabase.Format_TienVND);
            lblPeriodDepreciationAmount.Text =
                fixedAsset.PeriodDepreciationAmount.ToStringNumbericFormat(ConstDatabase.Format_TienVND);
            lblPurchasePrice.Text = fixedAsset.MonthPeriodDepreciationAmount.ToStringNumbericFormat(ConstDatabase.Format_TienVND);
            //lblAcDepreciationAmount.Text =
            //    fixedAsset.AcDepreciationAmount.ToStringNumbericFormat(ConstDatabase.Format_TienVND);
            //lblRemainingAmount.Text = fixedAsset.RemainingAmount.ToStringNumbericFormat(ConstDatabase.Format_TienVND);
            //Hiển thị phần chứng từ liên quan
            var voucher = _IFixedAssetService.GetListVoucherFixedAssets(fixedAsset.ID).OrderByDescending(n => n.Date).ToList();
            foreach (var item in voucher)
            {
                var firstOrDefault = Utils.ListType.FirstOrDefault(x => x.ID == item.TypeID);
                if (firstOrDefault != null)
                    item.TypeName = firstOrDefault.TypeName;
            }
            uGridDetail.DataSource = voucher;
            uGridDetail.ConfigGrid(ConstDatabase.VoucherFixedAsset_KeyName);
            selectedGuid = fixedAsset.ID;
            foreach (var column in uGridDetail.DisplayLayout.Bands[0].Columns)
            {
                this.ConfigEachColumn4Grid(0, column, uGridDetail);
            }
        }

        private void btnViewVoucher_Click(object sender, EventArgs e)
        {
            if (uGridDetail.ActiveRow == null) return;
            var voucher = uGridDetail.ActiveRow.ListObject as VoucherFixedAsset;
            if (voucher == null) return;
            switch (voucher.TypeID)
            {
                case 510:
                    {
                        var fAIncrement = Utils.IFAIncrementService.Getbykey(voucher.ID);
                        new FFAIncrementDetail(fAIncrement, new List<FAIncrement> { fAIncrement },
                                               ConstFrm.optStatusForm.View).ShowDialog(this);
                    }
                    break;
                case 520:
                    {
                        var fADecrement = Utils.IFADecrementService.Getbykey(voucher.ID);
                        new FFADecrementDetail(fADecrement, new List<FADecrement> { fADecrement }, ConstFrm.optStatusForm.View).
                            ShowDialog(this);
                    }
                    break;
                case 530:
                    {
                        var fAAdjustment = Utils.IFAAdjustmentService.Getbykey(voucher.ID);
                        new FFAAdjustmentDetail(fAAdjustment, new List<FAAdjustment> { fAAdjustment }, ConstFrm.optStatusForm.View).
                            ShowDialog(this);
                    }
                    break;
                case 540:
                    {
                        var fAIncrement = Utils.IFADepreciationService.Getbykey(voucher.ID);
                        new FFADepreciationDetail(fAIncrement, new List<FADepreciation> { fAIncrement },
                                                  ConstFrm.optStatusForm.View).ShowDialog(this);
                    }
                    break;
                case 550:
                    {
                        var fAIncrement = Utils.IFATransferService.Getbykey(voucher.ID);
                        new FFATransferDetail(fAIncrement, new List<FATransfer> { fAIncrement },
                                                  ConstFrm.optStatusForm.View).ShowDialog(this);
                    }
                    break;
                case 560:
                    {
                        var fAIncrement = Utils.IFAAuditService.Getbykey(voucher.ID);
                        new FFAAuditDetail(fAIncrement, new List<FAAudit> { fAIncrement },
                                                  ConstFrm.optStatusForm.View).ShowDialog(this);
                    }
                    break;
            }
        }

        private void uGridDetail_BeforeRowActivate(object sender, RowEventArgs e)
        {
            if (e.Row == null) return;
            var voucher = e.Row.ListObject as VoucherFixedAsset;
            if (voucher == null) return;
            //btnIncrement.Enabled = btnDecrement.Enabled = btnAdjustment.Enabled = false;
            //if (voucher.TypeID == 510)
            //    btnIncrement.Enabled = true;
            //else if (voucher.TypeID == 520)
            //    btnDecrement.Enabled = true;
            //else if (voucher.TypeID == 530)
            //    btnAdjustment.Enabled = true;
        }

        private void btnIncrement_Click(object sender, EventArgs e)
        {
            if (uGridDetail.ActiveRow == null) return;
            var voucher = uGridDetail.ActiveRow.ListObject as VoucherFixedAsset;
            if (voucher == null) return;
            var fAIncrement = Utils.IFAIncrementService.Getbykey(voucher.ID);
            new FFAIncrementDetail(fAIncrement, new List<FAIncrement> { fAIncrement },
                                   ConstFrm.optStatusForm.View).ShowDialog(this);
        }

        private void btnAdjustment_Click(object sender, EventArgs e)
        {
            if (uGridDetail.ActiveRow == null) return;
            var voucher = uGridDetail.ActiveRow.ListObject as VoucherFixedAsset;
            if (voucher == null) return;
            var fAAdjustment = Utils.IFAAdjustmentService.Getbykey(voucher.ID);
            new FFAAdjustmentDetail(fAAdjustment, new List<FAAdjustment> { fAAdjustment }, ConstFrm.optStatusForm.View).
                ShowDialog(this);
        }

        private void btnDecrement_Click(object sender, EventArgs e)
        {
            if (uGridDetail.ActiveRow == null) return;
            var voucher = uGridDetail.ActiveRow.ListObject as VoucherFixedAsset;
            if (voucher == null) return;
            var fADecrement = Utils.IFADecrementService.Getbykey(voucher.ID);
            new FFADecrementDetail(fADecrement, new List<FADecrement> { fADecrement }, ConstFrm.optStatusForm.View).
                ShowDialog(this);
        }

        private void uGridDetail_DoubleClick(object sender, EventArgs e)
        {
            btnViewVoucher_Click(sender, e);
        }

        private void dteToDate_ValueChanged(object sender, EventArgs e)
        {
            if (dteFromDate.DateTime <= dteToDate.DateTime)
            {
                var voucher = _IFixedAssetService.GetListVoucherFixedAssets(selectedGuid).Where(p => p.Date >= dteFromDate.DateTime && p.Date <= dteToDate.DateTime).ToList();
                foreach (var item in voucher)
                {
                    var firstOrDefault = Utils.ListType.FirstOrDefault(x => x.ID == item.TypeID);
                    if (firstOrDefault != null)
                        item.TypeName = firstOrDefault.TypeName;
                }

                uGridDetail.DataSource = voucher;
                uGridDetail.ConfigGrid(ConstDatabase.VoucherAccountingObject_KeyName);
                uGridDetail.DisplayLayout.Bands[0].Columns["TotalAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            }
            else
            {
                // MSG.Warning("Ngày đến phải lớn hơn ngày bắt đầu !");
            }
        }

        private void dteFromDate_ValueChanged(object sender, EventArgs e)
        {
            if (dteFromDate.DateTime <= dteToDate.DateTime)
            {
                var voucher = _IFixedAssetService.GetListVoucherFixedAssets(selectedGuid).Where(p => p.Date >= dteFromDate.DateTime && p.Date <= dteToDate.DateTime).ToList();
                
                    foreach (var item in voucher)
                {
                    var firstOrDefault = Utils.ListType.FirstOrDefault(x => x.ID == item.TypeID);
                    if (firstOrDefault != null)
                        item.TypeName = firstOrDefault.TypeName;
                }

                uGridDetail.DataSource = voucher;
                uGridDetail.ConfigGrid(ConstDatabase.VoucherAccountingObject_KeyName);
                uGridDetail.DisplayLayout.Bands[0].Columns["TotalAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            }
            else
            {
                // MSG.Warning("Ngày đến phải lớn hơn ngày bắt đầu !");
            }
        }

        private void cbbAboutTime_ValueChanged(object sender, EventArgs e)
        {
            if (cbbAboutTime.SelectedRow != null)
            {
                var model = cbbAboutTime.SelectedRow.ListObject as Item;

                Utils.GetDateBeginDateEnd(ngayHoachToan.Year,ngayHoachToan, model, out dtBegin, out dtEnd);
                dteFromDate.DateTime = dtBegin;
                dteToDate.DateTime = dtEnd;
                //            List<VoucherFixedAsset> voucher = listVoucherFixedAsset.
                //Where(p => p.FixedAssetCode == selectedGuid && p.Date >= dtBegin && p.Date <= dtEnd).OrderByDescending(u => u.Date).ToList();
                var voucher = _IFixedAssetService.GetListVoucherFixedAssets(selectedGuid).Where(p =>  p.Date >= dteFromDate.DateTime && p.Date <= dteToDate.DateTime).ToList();
                foreach (var item in voucher)
                {
                    var firstOrDefault = Utils.ListType.FirstOrDefault(x => x.ID == item.TypeID);
                    if (firstOrDefault != null)
                        item.TypeName = firstOrDefault.TypeName;
                }

                uGridDetail.DataSource = voucher;
                uGridDetail.ConfigGrid(ConstDatabase.VoucherAccountingObject_KeyName);
                uGridDetail.DisplayLayout.Bands[0].Columns["TotalAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            }
        }

        private void FFixedAsset_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGrid.ResetText();
            uGrid.ResetUpdateMode();
            uGrid.ResetExitEditModeOnLeave();
            uGrid.ResetRowUpdateCancelAction();
            uGrid.DataSource = null;
            uGrid.Layouts.Clear();
            uGrid.ResetLayouts();
            uGrid.ResetDisplayLayout();
            uGrid.Refresh();
            uGrid.ClearUndoHistory();
            uGrid.ClearXsdConstraints();


            uGridDetail.ResetText();
            uGridDetail.ResetUpdateMode();
            uGridDetail.ResetExitEditModeOnLeave();
            uGridDetail.ResetRowUpdateCancelAction();
            uGridDetail.DataSource = null;
            uGridDetail.Layouts.Clear();
            uGridDetail.ResetLayouts();
            uGridDetail.ResetDisplayLayout();
            uGridDetail.Refresh();
            uGridDetail.ClearUndoHistory();
            uGridDetail.ClearXsdConstraints();
        }

        private void FFixedAsset_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            UltraGrid ultraGridExport = uGridExport;
            ultraGridExport.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            LoadDuLieuExport( true);
            //Utils.ConfigGrid(ultraGridExport, TextMessage.ConstDatabase.MaterialGoods_TableNameExportExcel);
            Utils.ExportExcel(ultraGridExport, "Export_DMTaiSanCoDinh");
        }
    }
}
