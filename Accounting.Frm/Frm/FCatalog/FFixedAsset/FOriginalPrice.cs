﻿using System;
using Accounting.Core.Domain;
using Accounting.TextMessage;
namespace Accounting
{
    public partial class FOriginalPrice : DialogForm
    {
        public static bool isClose = true;
        public static decimal Total = 0;
        public static decimal CostByBudget = 0;
        public static decimal CostByEquity = 0;
        public static decimal CostByLoan = 0;
        public static decimal CostByOther = 0;
        public static decimal CostByVenture = 0;
        public FOriginalPrice()
        {
            InitializeComponent();
            InitializeGUI();
            Total = 0;
        }

        private void InitializeGUI()
        {
            nmrCostByBudget.FormatNumberic(ConstDatabase.Format_TienVND);
            nmrCostByEquity.FormatNumberic(ConstDatabase.Format_TienVND);
            nmrCostByLoan.FormatNumberic(ConstDatabase.Format_TienVND);
            nmrCostByOther.FormatNumberic(ConstDatabase.Format_TienVND);
            nmrCostByVenture.FormatNumberic(ConstDatabase.Format_TienVND);
        }

        public FOriginalPrice(FixedAsset itemp)
        {
            InitializeComponent();
            InitializeGUI();
            SetValueText(itemp);
            Total = 0;
        }
        #region fill data to txt
        void SetValueText(FixedAsset input)
        {
            nmrCostByBudget.Value = input.CostByBudget != null ? (double)input.CostByBudget : 0;
            nmrCostByEquity.Value = input.CostByEquity != null ? (double)input.CostByEquity : 0;
            nmrCostByLoan.Value = input.CostByLoan != null ? (double)input.CostByLoan : 0;
            nmrCostByOther.Value = input.CostByOther != null ? (double)input.CostByOther : 0;
            nmrCostByVenture.Value = input.CostByVenture != null ? (double)input.CostByVenture : 0;
        }
        #endregion

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(nmrCostByBudget.Value.ToString()))
            {
                Total += decimal.Parse(nmrCostByBudget.Value.ToString());

                CostByBudget = decimal.Parse(nmrCostByBudget.Value.ToString());
                // nmrCostByBudget.Value = input.CostByBudget;
            } else
            {
                CostByBudget = 0;
            }
            if (!string.IsNullOrEmpty(nmrCostByEquity.Value.ToString()))
            {
                Total += decimal.Parse(nmrCostByEquity.Value.ToString());
                CostByEquity = decimal.Parse(nmrCostByEquity.Value.ToString());
            }
            else
            {
                CostByEquity = 0;
            }
            if (!string.IsNullOrEmpty(nmrCostByLoan.Value.ToString()))
            {
                Total += decimal.Parse(nmrCostByLoan.Value.ToString());
                CostByLoan = decimal.Parse(nmrCostByLoan.Value.ToString());
            }
            else
            {
                CostByLoan = 0;
            }
            if (!string.IsNullOrEmpty(nmrCostByOther.Value.ToString()))
            {
                Total += decimal.Parse(nmrCostByOther.Value.ToString());
                CostByOther = decimal.Parse(nmrCostByOther.Value.ToString());
            }
            else
            {
                CostByOther = 0;
            }
            if (!string.IsNullOrEmpty(nmrCostByVenture.Value.ToString()))
            {
                Total += decimal.Parse(nmrCostByVenture.Value.ToString());
                CostByVenture = decimal.Parse(nmrCostByVenture.Value.ToString());
            }
            else
            {
                CostByVenture = 0;
            }
            this.Close();
            isClose = false;
        }

        private void ultraButton1_Click(object sender, EventArgs e)
        {
            this.Close();
            isClose = true;
        }

        private void FOriginalPrice_FormClosed(object sender, System.Windows.Forms.FormClosedEventArgs e)
        {
            
        }
    }
}
