﻿namespace Accounting
{
    partial class FOriginalPrice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.nmrCostByOther = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.nmrCostByVenture = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.nmrCostByEquity = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.nmrCostByLoan = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.nmrCostByBudget = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel38 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel35 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel33 = new Infragistics.Win.Misc.UltraLabel();
            this.btnAdd = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton1 = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.Rectangular3D;
            this.ultraGroupBox1.Controls.Add(this.nmrCostByOther);
            this.ultraGroupBox1.Controls.Add(this.nmrCostByVenture);
            this.ultraGroupBox1.Controls.Add(this.nmrCostByEquity);
            this.ultraGroupBox1.Controls.Add(this.nmrCostByLoan);
            this.ultraGroupBox1.Controls.Add(this.nmrCostByBudget);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel38);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel35);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel33);
            this.ultraGroupBox1.Location = new System.Drawing.Point(3, 3);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(338, 140);
            this.ultraGroupBox1.TabIndex = 0;
            // 
            // nmrCostByOther
            // 
            appearance1.TextHAlignAsString = "Right";
            this.nmrCostByOther.Appearance = appearance1;
            this.nmrCostByOther.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.nmrCostByOther.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.nmrCostByOther.InputMask = "{LOC}nn,nnn,nnn.nn";
            this.nmrCostByOther.Location = new System.Drawing.Point(140, 105);
            this.nmrCostByOther.MaxValue = ((long)(999999999999999));
            this.nmrCostByOther.Name = "nmrCostByOther";
            this.nmrCostByOther.Size = new System.Drawing.Size(163, 20);
            this.nmrCostByOther.TabIndex = 79;
            // 
            // nmrCostByVenture
            // 
            appearance2.TextHAlignAsString = "Right";
            this.nmrCostByVenture.Appearance = appearance2;
            this.nmrCostByVenture.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.nmrCostByVenture.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.nmrCostByVenture.InputMask = "{LOC}nn,nnn,nnn.nn";
            this.nmrCostByVenture.Location = new System.Drawing.Point(140, 81);
            this.nmrCostByVenture.MaxValue = ((long)(999999999999999));
            this.nmrCostByVenture.Name = "nmrCostByVenture";
            this.nmrCostByVenture.Size = new System.Drawing.Size(163, 20);
            this.nmrCostByVenture.TabIndex = 78;
            // 
            // nmrCostByEquity
            // 
            appearance3.TextHAlignAsString = "Right";
            this.nmrCostByEquity.Appearance = appearance3;
            this.nmrCostByEquity.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.nmrCostByEquity.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.nmrCostByEquity.InputMask = "{LOC}nn,nnn,nnn.nn";
            this.nmrCostByEquity.Location = new System.Drawing.Point(140, 55);
            this.nmrCostByEquity.MaxValue = ((long)(9999999999999999));
            this.nmrCostByEquity.Name = "nmrCostByEquity";
            this.nmrCostByEquity.Size = new System.Drawing.Size(163, 20);
            this.nmrCostByEquity.TabIndex = 77;
            // 
            // nmrCostByLoan
            // 
            appearance4.TextHAlignAsString = "Right";
            this.nmrCostByLoan.Appearance = appearance4;
            this.nmrCostByLoan.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.nmrCostByLoan.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.nmrCostByLoan.InputMask = "{LOC}nn,nnn,nnn.nn";
            this.nmrCostByLoan.Location = new System.Drawing.Point(140, 29);
            this.nmrCostByLoan.MaxValue = ((long)(9999999999999999));
            this.nmrCostByLoan.Name = "nmrCostByLoan";
            this.nmrCostByLoan.Size = new System.Drawing.Size(163, 20);
            this.nmrCostByLoan.TabIndex = 76;
            // 
            // nmrCostByBudget
            // 
            appearance5.TextHAlignAsString = "Right";
            this.nmrCostByBudget.Appearance = appearance5;
            this.nmrCostByBudget.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.nmrCostByBudget.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.nmrCostByBudget.InputMask = "{LOC}nn,nnn,nnn.nn";
            this.nmrCostByBudget.Location = new System.Drawing.Point(140, 6);
            this.nmrCostByBudget.MaxValue = ((long)(999999999999999));
            this.nmrCostByBudget.Name = "nmrCostByBudget";
            this.nmrCostByBudget.Size = new System.Drawing.Size(163, 20);
            this.nmrCostByBudget.TabIndex = 75;
            // 
            // ultraLabel1
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel1.Appearance = appearance6;
            this.ultraLabel1.Location = new System.Drawing.Point(14, 108);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(103, 17);
            this.ultraLabel1.TabIndex = 73;
            this.ultraLabel1.Text = "Vốn khác:";
            // 
            // ultraLabel2
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel2.Appearance = appearance7;
            this.ultraLabel2.Location = new System.Drawing.Point(14, 85);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(105, 17);
            this.ultraLabel2.TabIndex = 71;
            this.ultraLabel2.Text = "Vốn liên doanh:";
            // 
            // ultraLabel38
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel38.Appearance = appearance8;
            this.ultraLabel38.Location = new System.Drawing.Point(14, 59);
            this.ultraLabel38.Name = "ultraLabel38";
            this.ultraLabel38.Size = new System.Drawing.Size(103, 17);
            this.ultraLabel38.TabIndex = 69;
            this.ultraLabel38.Text = "Vốn chủ sở hữu:";
            // 
            // ultraLabel35
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel35.Appearance = appearance9;
            this.ultraLabel35.Location = new System.Drawing.Point(14, 36);
            this.ultraLabel35.Name = "ultraLabel35";
            this.ultraLabel35.Size = new System.Drawing.Size(105, 17);
            this.ultraLabel35.TabIndex = 67;
            this.ultraLabel35.Text = "Vốn vay:";
            // 
            // ultraLabel33
            // 
            appearance10.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel33.Appearance = appearance10;
            this.ultraLabel33.Location = new System.Drawing.Point(14, 13);
            this.ultraLabel33.Name = "ultraLabel33";
            this.ultraLabel33.Size = new System.Drawing.Size(105, 17);
            this.ultraLabel33.TabIndex = 65;
            this.ultraLabel33.Text = "Vốn ngân sách:";
            // 
            // btnAdd
            // 
            appearance11.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnAdd.Appearance = appearance11;
            this.btnAdd.Location = new System.Drawing.Point(197, 149);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(69, 30);
            this.btnAdd.TabIndex = 3;
            this.btnAdd.Text = "Lưu";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // ultraButton1
            // 
            appearance12.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.ultraButton1.Appearance = appearance12;
            this.ultraButton1.Location = new System.Drawing.Point(272, 149);
            this.ultraButton1.Name = "ultraButton1";
            this.ultraButton1.Size = new System.Drawing.Size(69, 30);
            this.ultraButton1.TabIndex = 4;
            this.ultraButton1.Text = "Đóng";
            this.ultraButton1.Click += new System.EventHandler(this.ultraButton1_Click);
            // 
            // FOriginalPrice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(343, 180);
            this.Controls.Add(this.ultraButton1);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.ultraGroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FOriginalPrice";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Nguyên giá bao gồm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FOriginalPrice_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraButton btnAdd;
        private Infragistics.Win.Misc.UltraButton ultraButton1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel38;
        private Infragistics.Win.Misc.UltraLabel ultraLabel35;
        private Infragistics.Win.Misc.UltraLabel ultraLabel33;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit nmrCostByVenture;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit nmrCostByEquity;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit nmrCostByLoan;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit nmrCostByBudget;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit nmrCostByOther;
    }
}