﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinEditors;

namespace Accounting
{
    public partial class FFixedAssetDetail : DialogForm
    {
        #region khai báo
        //thieu truong Cbb chi nhanh doanh nghiep de NULL
        private readonly IFixedAssetService _IFixedAssetService = Utils.IFixedAssetService;
        private readonly IFixedAssetCategoryService _IFixedAssetCategoryService = Utils.IFixedAssetCategoryService;
        private readonly IDepartmentService _IDepartmentService = Utils.IDepartmentService;
        private readonly IAccountService _IAccountService = Utils.IAccountService;
        private readonly ICostSetService _ICostSetService = Utils.ICostSetService;
        private readonly IFixedAssetDetailService _IFixedAssetDetailService = Utils.IFixedAssetDetailService;
        private readonly IAccountingObjectService _IAccountingObjectService = Utils.IAccountingObjectService;
        private readonly IWarrantyService _IWarrantyService = Utils.IWarrantyService;
        private readonly ISystemOptionService _SystemOptionService = Utils.ISystemOptionService;

        BindingList<FixedAssetAccessories> lstFixedAssetAccessories = new BindingList<FixedAssetAccessories>();
        BindingList<FixedAssetDetail> lstFixedAssetDetail = new BindingList<FixedAssetDetail>();

        private bool isDisplayMounth = true;
        bool OldIsActive = false;
        FixedAsset _Select = new FixedAsset();
        private Guid _id;
        public Guid Id
        {
            get { return _id; }
            set { _id = value; }
        }
        bool isDataMounth = true;
        decimal userTime;
        decimal userTimeText;
        bool Them = true;
        bool firstCost = true;
        FixedAsset GetCostBy = new FixedAsset();
        public static bool isClose = true;
        private bool _forceReload = false;
        private DateTime ApplicationStartDate = DateTime.Now;
        private List<Warranty> listWarranty = new List<Warranty>();
        #endregion

        #region khởi tạo
        public FFixedAssetDetail()
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion
            this.Text = @"Thêm mới Tài sản cố định";
            nmrOriginalPrice.Enabled = false;
            cbbDepreciationMethod.Enabled = false;
            nmrDepreciationRate.Enabled = true;
            nmrPeriodDepreciationAmount.Enabled = true;
            nmrAcDepreciationAmount.Enabled = true;
            nmrMonthDepreciationRate.Enabled = true;
            nmrMonthPeriodDepreciationAmount.Enabled = true;
            nmrRemainingAmount.Enabled = true;

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI(_Select);
            InitCld();
            txtAccessories.Visible = false;
            ultraLabel12.Visible = false;
            txtDescription.Height = txtDescription.Height + txtAccessories.Height+ 5;


        }

        public FFixedAssetDetail(FixedAsset temp)
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion
            //FixedAsset temp = temp2.CloneObject();
            FAIncrementDetail detail = Utils.IFAIncrementDetailService.FindByFixedAssetID(temp.ID);
            if (detail != null && Utils.IFAIncrementService.Getbykey(detail.FAIncrementID).Recorded)
            {
                btnOriginalPrice.Enabled = false;
            }
            this.Text = @"Sửa Tài sản cố định";
            #region Thiết lập ban đầu cho Form
            _Select = temp;
            GetCostBy = temp;
            OldIsActive = temp.CloneObject().IsActive;
            Them = false;
            nmrOriginalPrice.Enabled = false;
            cbbDepreciationMethod.Enabled = false;
            nmrDepreciationRate.Enabled = true;
            nmrPeriodDepreciationAmount.Enabled = true;
            nmrAcDepreciationAmount.Enabled = true;
            nmrMonthDepreciationRate.Enabled = true;
            nmrMonthPeriodDepreciationAmount.Enabled = true;
            nmrRemainingAmount.Enabled = true;



            lstFixedAssetAccessories = Them ? new BindingList<FixedAssetAccessories>() : new BindingList<FixedAssetAccessories>(temp.FixedAssetAccessoriess);
            lstFixedAssetDetail = Them ? new BindingList<FixedAssetDetail>() : new BindingList<FixedAssetDetail>(temp.FixedAssetDetails);

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI(temp);

            #endregion

            #region Fill dữ liệu Obj vào control
            ObjandGUI(temp, false);
            #endregion
        }
        private void InitializeGUI(FixedAsset Input)
        {
            string ss = _SystemOptionService.Getbykey(82).Data;
            if (ss != null && !string.IsNullOrEmpty(ss))
                ApplicationStartDate = DateTime.ParseExact(ss, "dd/MM/yyyy", null);
            //khoi tao cho Combobox
            cbbDepreciationMethod.SelectedItem = cbbDepreciationMethod.Items[0];

            cbbDepreciationAccount.DataSource = _IAccountService.GetListAccountAccountNumberIsActiveWithUpperLever("214", true, 0);
            cbbDepreciationAccount.DisplayMember = "AccountNumber";
            Utils.ConfigGrid(cbbDepreciationAccount, ConstDatabase.Account_TableName);

            cbbExpenditureAccount.DataSource = _IAccountService.GetListAccountAccountNumberIsActiveWithUpperLever("154;241;631;642;632;811", true, 1);
            cbbExpenditureAccount.DisplayMember = "AccountNumber";
            Utils.ConfigGrid(cbbExpenditureAccount, ConstDatabase.Account_TableName);

            cbbOriginalPriceAccount.DataSource = _IAccountService.GetListAccountAccountNumberIsActiveWithUpperLever("211;217", true, 1);
            cbbOriginalPriceAccount.DisplayMember = "AccountNumber";
            Utils.ConfigGrid(cbbOriginalPriceAccount, ConstDatabase.Account_TableName);

            cbbAccountingObject.DataSource = _IAccountingObjectService.GetAccountingObjects(1, true);
            cbbAccountingObject.DisplayMember = "AccountingObjectName";
            Utils.ConfigGrid(cbbAccountingObject, ConstDatabase.AccountingObject_TableName);

            cbbCostSet.DataSource = _ICostSetService.GetListCostSetIsActive(true);
            cbbCostSet.DisplayMember = "CostSetName";
            Utils.ConfigGrid(cbbCostSet, ConstDatabase.CostSet_TableName);

            cbbDepartmentID.DataSource = _IDepartmentService.GetListDepartmentIsActive(true);
            cbbDepartmentID.DisplayMember = "DepartmentName";
            Utils.ConfigGrid<Department>(this, cbbDepartmentID, ConstDatabase.Department_TableName, "ParentID", "IsParentNode");
            listWarranty = _IWarrantyService.getActive();
            cbbWarrantyTime.DataSource = listWarranty;
            cbbWarrantyTime.DisplayMember = "WarrantyName";
            cbbWarrantyTime.ValueMember = "WarrantyName";
            //cbbWarrantyTime.DataBindings.Add("Value", _Select, "GuaranteeDuration", true, DataSourceUpdateMode.OnPropertyChanged);
            Utils.ConfigGrid(cbbWarrantyTime, ConstDatabase.Warranty_TableName);

            this.ConfigCombo(Utils.ListFixedAssetCategory, cbbFixedAssetCategoryID, "FixedAssetCategoryName", "ID");
            this.ConfigCombo(Utils.ListDepartment, cbbDepartmentID, "DepartmentName", "ID");


            #region Grid Fixedset detail

            UGridDetails.DataSource = lstFixedAssetDetail;
            //UGridDetails.SetDataBinding(lstFixedAssetDetail, "");
            Utils.ConfigGrid(UGridDetails, ConstDatabase.FixedAssetDetail_TableName, new List<TemplateColumn>(), isBusiness: true);
            foreach (var column in UGridDetails.DisplayLayout.Bands[0].Columns)
            {
                this.ConfigEachColumn4Grid(0, column, UGridDetails);
            }
            this.ConfigCbbToGrid(UGridDetails, UGridDetails.DisplayLayout.Bands[0].Columns["WarrantyTime"].Key, _IWarrantyService.getActive(), "WarrantyName", "WarrantyName", ConstDatabase.Warranty_TableName);
            uGridFixedAssetAccessories.DataSource = lstFixedAssetAccessories;
            //uGridFixedAssetAccessories.SetDataBinding(lstFixedAssetAccessories, "");
            Utils.ConfigGrid(uGridFixedAssetAccessories, ConstDatabase.FixedAssetAccessories_TableName, new List<TemplateColumn>(), isBusiness: true);
            foreach (var column in uGridFixedAssetAccessories.DisplayLayout.Bands[0].Columns)
            {
                this.ConfigEachColumn4Grid(0, column, uGridFixedAssetAccessories);
            }

            #endregion
            nmrDisposedAmount.FormatNumberic(ConstDatabase.Format_TienVND);
            nmrOriginalPrice.FormatNumberic(ConstDatabase.Format_TienVND);
            nmrPeriodDepreciationAmount.FormatNumberic(ConstDatabase.Format_TienVND);
            nmrAcDepreciationAmount.FormatNumberic(ConstDatabase.Format_TienVND);
            nmrPurchasePrice.FormatNumberic(ConstDatabase.Format_TienVND);
            nmrMonthPeriodDepreciationAmount.FormatNumberic(ConstDatabase.Format_TienVND);
            nmrRemainingAmount.FormatNumberic(ConstDatabase.Format_TienVND);
            nmrDepreciationRate.FormatNumberic(ConstDatabase.Format_Coefficient);
            nmrMonthDepreciationRate.FormatNumberic(ConstDatabase.Format_Coefficient);
            nmrUsedTime.FormatNumberic(ConstDatabase.Format_Quantity);
            nmrProductionYear.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(nmrProductionYear_EditorSpinButtonClick);
            nmrProductionYear.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always;
            nmrProductionYear.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Integer;
            Utils.FormatNumberic(uGridFixedAssetAccessories.DisplayLayout.Bands[0].Columns["FixedAssetAccessoriesQuantity"], ConstDatabase.Format_Quantity);
            Utils.FormatNumberic(uGridFixedAssetAccessories.DisplayLayout.Bands[0].Columns["FixedAssetAccessoriesAmount"], ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(UGridDetails.DisplayLayout.Bands[0].Columns["Quantity"], ConstDatabase.Format_Quantity);
        }

        private void InitCld()
        {
            cldDeliveryRecordDate.Value = null;
            cldDepreciationDate.Value = Utils.StringToDateTime(Utils.GetDbStartDate());
            cldDisposedDate.Value = null;
            cldIncrementDate.Value = Utils.StringToDateTime(Utils.GetDbStartDate());
            cldPurchasedDate.Value = Utils.StringToDateTime(Utils.GetDbStartDate());
            cldUsedDate.Value = Utils.StringToDateTime(Utils.GetDbStartDate());
            CbbYearMonth.SelectedIndex = 1;
        }
        #endregion

        #region Button Event
        /// <summary>
        /// Sự kiện nút Lưu lại
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!Them)
            {
                FAIncrementDetail fAIncrementDetails = Utils.IFAIncrementDetailService.FindByFixedAssetID(_Select.ID);
                if (fAIncrementDetails != null)
                {
                    if (OldIsActive != chkActive.Checked)
                    {

                        var model = _IFixedAssetService.Getbykey(_Select.ID);
                        model.IsActive = chkActive.Checked;                       
                        _IFixedAssetService.BeginTran();
                        try
                        {
                            _IFixedAssetService.Update(model);
                            _IFixedAssetService.CommitTran();
                        }
                        catch (Exception ex)
                        {
                            _IFixedAssetService.RolbackTran();
                        }
                        isClose = false;
                        this.Close();
                        return;
                    }
                    else
                    {
                        MSG.Warning("Không thể sửa thông tin của tài sản này tại đây vì đã phát sinh chứng từ liên quan. Vui lòng thực hiện việc sửa đổi tại nghiệp vụ Điều chỉnh Tài sản cố định!");
                        return;
                    }
                }
            }

            #region Check Lỗi
            if (!CheckError()) return;
            #endregion

            //Gui to object
            #region Fill dữ liệu control vào obj
            FixedAsset temp = Them ? new FixedAsset() : _IFixedAssetService.Getbykey(_Select.ID);
            temp = ObjandGUI(temp, true);
            _Select = temp;

            #endregion
            //Utils.IFAIncrementDetailService.RolbackTran();
            #region Thao tác CSDL // Hautv Sửa
            //var session = _IFixedAssetService.GetSessionStateless();

            try
            {
                //_IFixedAssetService.BeginTran();
                uGridFixedAssetAccessories.Update();
                UGridDetails.Update();
                var session = _IFixedAssetService.GetSessionState();
                session.Clear();
                session = _IFixedAssetService.GetSessionState();
                using (var trans = session.BeginTransaction())
                {
                    try
                    {
                        if (Them)
                        {

                            foreach (FixedAssetAccessories item in lstFixedAssetAccessories)
                            {
                                item.ID = Guid.NewGuid();
                                item.FixedAssetID = temp.ID;

                            }
                            temp.FixedAssetAccessoriess = lstFixedAssetAccessories;
                            foreach (FixedAssetDetail item in lstFixedAssetDetail)
                            {
                                item.ID = Guid.NewGuid();
                                item.FixedAssetID = temp.ID;
                            }
                            temp.FixedAssetDetails = lstFixedAssetDetail;
                            session.Save(temp);
                        }
                        else
                        {
                            foreach (FixedAssetAccessories item in lstFixedAssetAccessories)
                            {
                                if (item.ID == Guid.Empty)
                                {
                                    item.ID = Guid.NewGuid();
                                    item.FixedAssetID = temp.ID;
                                }
                            }
                            foreach (FixedAssetDetail item in lstFixedAssetDetail)
                            {
                                if (item.ID == Guid.Empty)
                                {
                                    item.ID = Guid.NewGuid();
                                    item.FixedAssetID = temp.ID;
                                }
                            }
                            session.Update(temp.CloneObject());
                        }
                        trans.Commit();
                        _id = temp.ID;

                        //Utils.IFAIncrementDetailService.GetSession().Clear();
                        Utils.ClearCacheByType<FixedAsset>();
                        BindingList<FixedAsset> lst = Utils.ListFixedAsset;
                    }
                    catch (Exception ex)
                    {
                        //_IFixedAssetService.RolbackTran();
                        MSG.Error(ex.Message);
                    }
                }
                
                //_IFixedAssetService.CommitTran();
                
            }
            catch (Exception ex)
            {
                //_IFixedAssetService.RolbackTran();
                //MSG.Error(ex.Message);
            }
            #endregion

            #region xử lý form, kết thúc form
            //Utils.ListFixedAsset.Clear();
            this.Close();
            isClose = false;
            #endregion
        }
        /// <summary>
        /// Lưu và tiếp tục nhập
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSaveContinue_Click(object sender, EventArgs e)
        {
            if (!Them)
            {
                FAIncrementDetail fAIncrementDetails = Utils.IFAIncrementDetailService.FindByFixedAssetID(_Select.ID);
                if (fAIncrementDetails != null)
                {
                    if (OldIsActive != chkActive.Checked)
                    {

                        var model = _IFixedAssetService.Getbykey(_Select.ID);
                        model.IsActive = chkActive.Checked;
                        _IFixedAssetService.BeginTran();
                        try
                        {
                            _IFixedAssetService.Update(model);
                            _IFixedAssetService.CommitTran();
                        }
                        catch (Exception ex)
                        {
                            _IFixedAssetService.RolbackTran();
                        }
                        isClose = false;
                        _forceReload = true;
                        _Select = new FixedAsset();
                        ObjandGUI(_Select, false);
                        SetDefaultValueControls();
                        cbbFixedAssetCategoryID.ToggleDropdown();
                        return;
                    }
                    else
                    {
                        MSG.Warning("Không thể sửa thông tin của tài sản này tại đây vì đã phát sinh chứng từ liên quan. Vui lòng thực hiện việc sửa đổi tại nghiệp vụ Điều chỉnh Tài sản cố định!");
                        return;
                    }
                }
            }
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion

            //Gui to object
            #region Fill dữ liệu control vào obj
            FixedAsset temp = Them ? new FixedAsset() : _IFixedAssetService.Getbykey(_Select.ID);
            temp = ObjandGUI(temp, true);
            _Select = temp;

            #endregion
            Utils.IFAIncrementDetailService.RolbackTran();
            #region Thao tác CSDL
            uGridFixedAssetAccessories.Update();
            UGridDetails.Update();
            _IFixedAssetService.BeginTran();
            if (Them)
            {

                foreach (FixedAssetAccessories item in lstFixedAssetAccessories)
                {
                    item.ID = Guid.NewGuid();
                    item.FixedAssetID = temp.ID;
                }
                temp.FixedAssetAccessoriess = lstFixedAssetAccessories;
                foreach (FixedAssetDetail item in lstFixedAssetDetail)
                {
                    item.ID = Guid.NewGuid();
                    item.FixedAssetID = temp.ID;
                }
                temp.FixedAssetDetails = lstFixedAssetDetail;
                _IFixedAssetService.CreateNew(temp);
            }
            else
            {
                foreach (FixedAssetAccessories item in lstFixedAssetAccessories)
                {
                    if (item.ID == Guid.Empty)
                    {
                        item.ID = Guid.NewGuid();
                        item.FixedAssetID = temp.ID;
                    }
                }
                foreach (FixedAssetDetail item in lstFixedAssetDetail)
                {
                    if (item.ID == Guid.Empty)
                    {
                        item.ID = Guid.NewGuid();
                        item.FixedAssetID = temp.ID;
                    }
                }
                _IFixedAssetService.Update(temp.CloneObject());
            }
            _IFixedAssetService.CommitTran();

            _id = temp.ID;
            Utils.ClearCacheByType<FixedAsset>();
            BindingList<FixedAsset> lst = Utils.ListFixedAsset;
            #endregion

            #region xử lý form, reset form
            isClose = false;
            _forceReload = true;
            // reset form
            _Select = new FixedAsset();
            ObjandGUI(_Select, false);
            SetDefaultValueControls();
            cbbFixedAssetCategoryID.ToggleDropdown();
            #endregion
        }

        private void SetDefaultValueControls()
        {
            cbbFixedAssetCategoryID.Text = cbbFixedAssetCategoryID.NullText;
            cbbDepartmentID.Text = cbbDepartmentID.NullText;
            cbbWarrantyTime.Text = cbbWarrantyTime.NullText;
            cbbAccountingObject.Text = cbbAccountingObject.NullText;
            cbbCurrentState.Text = cbbCurrentState.NullText;
            txtFixedAssetCode.Enabled = true;
            cbbOriginalPriceAccount.Text = cbbOriginalPriceAccount.NullText;
            cbbDepreciationAccount.Text = cbbDepreciationAccount.NullText;
            cbbExpenditureAccount.Text = cbbExpenditureAccount.NullText;
            cbbCostSet.Text = cbbCostSet.NullText;
            cbbDepreciationMethod.Text = cbbDepreciationMethod.NullText;
            CbbYearMonth.Text = CbbYearMonth.NullText;

            nmrOriginalPrice.Value = 0;
            cbbDepreciationMethod.Value = cbbDepreciationMethod.NullText;
            nmrAcDepreciationAmount.Value = 0;
            nmrDepreciationRate.Value = 0;
            nmrDisposedAmount.Value = 0;
            nmrMonthDepreciationRate.Value = 0;
            nmrMonthPeriodDepreciationAmount.Value = 0;
            nmrPeriodDepreciationAmount.Value = 0;
            nmrPurchasePrice.Value = 0;
            nmrRemainingAmount.Value = 0;
            nmrUsedTime.Value = 0;
            CbbYearMonth.Value = CbbYearMonth.NullText;

            uGridFixedAssetAccessories.DataSource = new BindingList<FixedAssetAccessories>();
            UGridDetails.DataSource = new BindingList<FixedAssetDetail>();
        }

        /// <summary>
        /// Sự kiện nút đóng
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            isClose = false;
            DialogResult = DialogResult.Cancel;
            FOriginalPrice.Total = 0;
            FOriginalPrice.CostByBudget = 0;
            FOriginalPrice.CostByEquity = 0;
            FOriginalPrice.CostByLoan = 0;
            FOriginalPrice.CostByOther = 0;
            FOriginalPrice.CostByVenture = 0;
            this.Close();
        }

        private void btnOriginalPrice_Click(object sender, EventArgs e)
        {

            if (GetCostBy == null)
            {
                getCostByValue();
                new FOriginalPrice(null).ShowDialog(this);
            }
            else
            {
                new FOriginalPrice(GetCostBy).ShowDialog(this);
            }

            if (!FOriginalPrice.isClose)
            {
                XuLyDuLieu();
                getCostByValue();
            }

        }
        #region set value to Gia tri con lai

        private void nmrAcDepreciationAmount_ValueChanged(object sender, EventArgs e)
        {
            SetValueForRemainingAmount();
        }



        #endregion

        private void CbbYearMonth_SelectionChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(nmrUsedTime.Value.ToString()))
            {

                if (CbbYearMonth.SelectedIndex == 0 && !isDisplayMounth)
                {
                    if (userTimeText == decimal.Parse(nmrUsedTime.Value.ToString()))
                    {
                        if (isDataMounth)
                        {
                            isDisplayMounth = true;
                            nmrUsedTime.Value = userTime;
                            userTimeText = decimal.Parse(nmrUsedTime.Value.ToString());
                        }
                        else
                        {
                            isDisplayMounth = true;
                            nmrUsedTime.Value = userTime * 12;
                            userTimeText = decimal.Parse(nmrUsedTime.Value.ToString());
                        }
                    }
                    else
                    {
                        isDisplayMounth = true;
                        userTime = decimal.Parse(nmrUsedTime.Value.ToString());
                        nmrUsedTime.Value = decimal.Parse(nmrUsedTime.Value.ToString()) * 12;
                        isDataMounth = false;

                        userTimeText = decimal.Parse(nmrUsedTime.Value.ToString());
                    }
                }
                else if (CbbYearMonth.SelectedIndex == 1 && isDisplayMounth)
                {
                    if (userTimeText == decimal.Parse(nmrUsedTime.Value.ToString()))
                    {
                        if (isDataMounth)
                        {
                            isDisplayMounth = false;
                            nmrUsedTime.Value = userTime / 12;
                            userTimeText = decimal.Parse(nmrUsedTime.Value.ToString());
                        }
                        else
                        {
                            isDisplayMounth = false;
                            nmrUsedTime.Value = userTime;
                            userTimeText = decimal.Parse(nmrUsedTime.Value.ToString());
                        }
                    }
                    else
                    {
                        isDisplayMounth = false;
                        userTime = decimal.Parse(nmrUsedTime.Value.ToString());
                        nmrUsedTime.Value = decimal.Parse(nmrUsedTime.Value.ToString()) / 12;
                        isDataMounth = true;

                        userTimeText = decimal.Parse(nmrUsedTime.Value.ToString());
                    }

                }
            }
            if (CbbYearMonth.SelectedIndex == 0) isDisplayMounth = true;
            else isDisplayMounth = false;
        }
        #endregion

        FixedAsset ObjandGUI(FixedAsset input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();  //Thêm mới

                if (!string.IsNullOrEmpty(cbbFixedAssetCategoryID.Text))
                {
                    input.FixedAssetCategoryID = ((FixedAssetCategory)Utils.getSelectCbbItem(cbbFixedAssetCategoryID)).ID;
                    input.FixedAssetCategoryView = ((FixedAssetCategory)Utils.getSelectCbbItem(cbbFixedAssetCategoryID)).FixedAssetCategoryName;
                }

                if (!string.IsNullOrEmpty(cbbDepartmentID.Text))
                {
                    input.DepartmentID = ((Department)Utils.getSelectCbbItem(cbbDepartmentID)).ID;
                }
                input.FixedAssetCode = txtFixedAssetCode.Text;
                input.FixedAssetName = txtFixedAssetName.Text;
                //input.Quantity = Utils.decimalTryParse(nmrQuantity.Value != null ? nmrQuantity.Value.ToString() : "");
                input.Description = txtDescription.Text;
                if (nmrProductionYear.Value != null)
                {
                    input.ProductionYear = (int)nmrProductionYear.Value;
                }
                input.SerialNumber = txtSerialNumber.Text;
                input.MadeIn = txtMadeIn.Text;
                input.Accessories = txtAccessories.Text;
                if (!string.IsNullOrEmpty(cbbAccountingObject.Text))
                {
                    input.AccountingObjectID = ((AccountingObject)Utils.getSelectCbbItem(cbbAccountingObject)).ID;
                    input.AccountingObjectName = ((AccountingObject)Utils.getSelectCbbItem(cbbAccountingObject)).AccountingObjectName;
                    input.AccountingObjectAddress = ((AccountingObject)Utils.getSelectCbbItem(cbbAccountingObject)).Address;

                }
                input.GuaranteeDuration = cbbWarrantyTime.Text;

                input.GuaranteeCodition = txtGuaranteeCodition.Text;
                input.IsSecondHand = chkIsSecondHand.CheckState == CheckState.Checked ? true : false;
                input.CurrentState = cbbCurrentState.SelectedIndex;
                if (GetBoolDateTime(cldDisposedDate, false))
                {
                    input.DisposedDate = DateTime.Parse(cldDisposedDate.Value.ToString());
                }
                else
                {
                    input.DisposedDate = null;
                }
                input.DisposedAmount = Utils.decimalTryParse(nmrDisposedAmount.Value != null ? nmrDisposedAmount.Value.ToString() : "");
                input.DisposedReason = txtDisposedReason.Text;
                if (GetBoolDateTime(cldPurchasedDate, false))
                {
                    input.PurchasedDate = DateTime.Parse(cldPurchasedDate.Value.ToString());
                }
                else
                {
                    input.PurchasedDate = null;
                }
                if (GetBoolDateTime(cldUsedDate, false))
                {
                    input.UsedDate = DateTime.Parse(cldUsedDate.Value.ToString());
                }
                else
                {
                    input.UsedDate = null;
                }
                if (GetBoolDateTime(cldIncrementDate, false))
                {
                    input.IncrementDate = DateTime.Parse(cldIncrementDate.Value.ToString());
                }
                else
                {
                    input.IncrementDate = null;
                }
                if (GetBoolDateTime(cldDepreciationDate, false))
                {
                    input.DepreciationDate = DateTime.Parse(cldDepreciationDate.Value.ToString());
                }
                else
                {
                    input.DepreciationDate = null;
                }
                if ((nmrPurchasePrice.Value != null) && (nmrPurchasePrice.Text.Trim() != ""))
                {
                    input.PurchasePrice = Utils.decimalTryParse(nmrPurchasePrice.Value != null ? nmrPurchasePrice.Value.ToString() : "");
                }
                else
                {
                    input.PurchasePrice = null;
                }
                if (nmrOriginalPrice.Value != null)
                {
                    input.OriginalPrice = Utils.decimalTryParse(nmrOriginalPrice.Value.ToString());
                }
                if ((nmrAcDepreciationAmount.Value != null) && (nmrAcDepreciationAmount.Text.Trim() != ""))
                {
                    input.AcDepreciationAmount = Utils.decimalTryParse(nmrAcDepreciationAmount.Value.ToString());
                }
                else
                {
                    input.AcDepreciationAmount = null;
                }
                if ((nmrRemainingAmount.Value != null) && (nmrRemainingAmount.Text.Trim() != ""))
                {
                    input.RemainingAmount = Utils.decimalTryParse(nmrRemainingAmount.Value.ToString());
                }
                else
                {
                    input.RemainingAmount = null;
                }
                if (nmrUsedTime.Value != null)
                {
                    try
                    {
                        input.UsedTime = Utils.decimalTryParse(nmrUsedTime.Value.ToString());
                    }
                    catch (Exception)
                    {


                    }

                }
                if (!string.IsNullOrEmpty(cbbDepreciationAccount.Text))
                {
                    input.DepreciationAccount = ((Account)Utils.getSelectCbbItem(cbbDepreciationAccount)).AccountNumber;
                }
                if (!string.IsNullOrEmpty(cbbOriginalPriceAccount.Text))
                {
                    input.OriginalPriceAccount = ((Account)Utils.getSelectCbbItem(cbbOriginalPriceAccount)).AccountNumber;
                }
                if (!string.IsNullOrEmpty(cbbExpenditureAccount.Text))
                {
                    input.ExpenditureAccount = ((Account)Utils.getSelectCbbItem(cbbExpenditureAccount)).AccountNumber;
                }
                if ((nmrPeriodDepreciationAmount.Value != null) && (nmrPeriodDepreciationAmount.Text.Trim() != ""))
                {
                    input.PeriodDepreciationAmount = Utils.decimalTryParse(nmrPeriodDepreciationAmount.Value.ToString());
                }
                else
                {
                    input.PeriodDepreciationAmount = null;
                }
                if ((nmrDepreciationRate.Value != null) && (nmrDepreciationRate.Text.Trim() != ""))
                {
                    input.DepreciationRate = Utils.decimalTryParse(nmrDepreciationRate.Value.ToString());
                }
                else
                {
                    input.DepreciationRate = null;
                }
                if (cbbDepreciationMethod.Value != null)
                {
                    input.DepreciationMethod = (int)cbbDepreciationMethod.SelectedIndex;
                }
                input.CostByBudget = GetCostBy.CostByBudget;
                input.CostByEquity = GetCostBy.CostByEquity;
                input.CostByLoan = GetCostBy.CostByLoan;
                input.CostByOther = GetCostBy.CostByOther;
                input.CostByVenture = GetCostBy.CostByVenture;
                if (!string.IsNullOrEmpty(cbbCostSet.Text))
                {
                    input.CostSetID = ((CostSet)Utils.getSelectCbbItem(cbbCostSet)).ID;
                }
                input.IsDepreciationAllocation = chkIsDepreciationAllocation.CheckState == CheckState.Checked ? true : false;
                if ((nmrMonthDepreciationRate.Value != null) && (nmrMonthDepreciationRate.Text.Trim() != ""))
                {
                    input.MonthDepreciationRate = Utils.decimalTryParse(nmrMonthDepreciationRate.Value.ToString());
                }
                else
                {
                    input.MonthDepreciationRate = null;
                }
                if ((nmrMonthPeriodDepreciationAmount.Value != null) && (nmrMonthPeriodDepreciationAmount.Text.Trim() != ""))
                {
                    input.MonthPeriodDepreciationAmount = Utils.decimalTryParse(nmrMonthPeriodDepreciationAmount.Value.ToString());
                }
                else
                {
                    input.MonthPeriodDepreciationAmount = null;
                }
                input.DeliveryRecordNo = txtDeliveryRecordNo.Text;
                if (GetBoolDateTime(cldDeliveryRecordDate, false))
                {

                    input.DeliveryRecordDate = DateTime.Parse(cldDeliveryRecordDate.Value.ToString());
                }
                else
                {
                    input.DeliveryRecordDate = null;
                }
                input.IsIrrationalCost = chkIsIrrationalCost.CheckState == CheckState.Checked ? true : false;
                if (CbbYearMonth.SelectedIndex == 1)
                {
                    input.IsDisplayMonth = false;
                }
                else
                {
                    input.IsDisplayMonth = true;
                }
                input.IsCreateFromOldDatabase = false;
                input.IsActive = chkActive.Checked;
            }
            else
            {
                chkActive.Checked = input.IsActive;
                if (input.FixedAssetCategoryID != null)
                {
                    foreach (var item in cbbFixedAssetCategoryID.Rows)
                    {

                        if (((item.ListObject as FixedAssetCategory).ID == input.FixedAssetCategoryID))
                        {
                            cbbFixedAssetCategoryID.SelectedRow = item;
                        }
                    }

                }

                if (input.DepartmentID != null)
                {
                    foreach (var item in cbbDepartmentID.Rows)
                    {
                        if ((item.ListObject as Department).ID == input.DepartmentID)
                        {
                            cbbDepartmentID.SelectedRow = item;
                        }
                    }

                }
                txtFixedAssetCode.Text = input.FixedAssetCode;
                txtFixedAssetCode.Enabled = false;
                txtFixedAssetName.Text = input.FixedAssetName;
                txtDescription.Text = input.Description;
                nmrProductionYear.Value = input.ProductionYear;
                txtSerialNumber.Text = input.SerialNumber;
                txtMadeIn.Text = input.MadeIn;
                txtAccessories.Text = input.Accessories;
                if (input.AccountingObjectID != null)
                {
                    foreach (var item in cbbAccountingObject.Rows)
                    {
                        if ((item.ListObject as AccountingObject).ID == input.AccountingObjectID)
                        {
                            cbbAccountingObject.SelectedRow = item;
                        }
                    }

                }
                if (!input.GuaranteeDuration.IsNullOrEmpty())
                {
                    cbbWarrantyTime.Text = input.GuaranteeDuration;
                    if (listWarranty.Any(n => n.WarrantyTime.ToString() == input.GuaranteeDuration))
                    {
                        cbbWarrantyTime.Text = listWarranty.FirstOrDefault(n => n.WarrantyTime.ToString() == input.GuaranteeDuration).WarrantyName;
                    }
                }
                txtGuaranteeCodition.Text = input.GuaranteeCodition;
                chkIsSecondHand.Checked = (bool)input.IsSecondHand;
                if (input.CurrentState != null)
                {
                    cbbCurrentState.SelectedIndex = (int)input.CurrentState;
                }
                if (input.DisposedDate != null)
                {
                    cldDisposedDate.Value = input.DisposedDate;
                }
                nmrDisposedAmount.Value = input.DisposedAmount;
                txtDisposedReason.Text = input.DisposedReason;
                if (input.PurchasedDate != null)
                {
                    cldPurchasedDate.Value = input.PurchasedDate;
                }
                if (input.UsedDate != null)
                {
                    cldUsedDate.Value = input.UsedDate;
                }
                if (input.IncrementDate != null)
                {
                    cldIncrementDate.Value = input.IncrementDate;
                }
                if (input.DepreciationDate != null)
                {
                    cldDepreciationDate.Value = input.DepreciationDate;
                }



                if (input.PurchasePrice != null)
                {
                    nmrPurchasePrice.Value = input.PurchasePrice;

                }

                if (input.OriginalPrice != null)
                {
                    nmrOriginalPrice.Value = input.OriginalPrice;
                }
                if (input.AcDepreciationAmount != null)
                {
                    nmrAcDepreciationAmount.Value = input.AcDepreciationAmount;

                }
                if (input.RemainingAmount != null)
                {
                    nmrRemainingAmount.Value = input.RemainingAmount;

                }
                if (input.OriginalPriceAccount != null)
                {
                    foreach (var item in cbbOriginalPriceAccount.Rows)
                    {
                        if ((item.ListObject as Account).AccountNumber == input.OriginalPriceAccount)
                        {
                            cbbOriginalPriceAccount.SelectedRow = item;
                            break;
                        }
                    }

                }
                if (input.DepreciationAccount != null)
                {
                    foreach (var item in cbbDepreciationAccount.Rows)
                    {
                        if ((item.ListObject as Account).AccountNumber == input.DepreciationAccount)
                        {
                            cbbDepreciationAccount.SelectedRow = item;
                            break;
                        }
                    }

                }
                if (input.ExpenditureAccount != null)
                {
                    foreach (var item in cbbExpenditureAccount.Rows)
                    {
                        if ((item.ListObject as Account).AccountNumber == input.ExpenditureAccount)
                        {
                            cbbExpenditureAccount.SelectedRow = item;
                            break;
                        }
                    }

                }


                if (input.PeriodDepreciationAmount != null)
                {
                    nmrPeriodDepreciationAmount.Value = input.PeriodDepreciationAmount;

                }
                if (input.DepreciationRate != null)
                {
                    nmrDepreciationRate.Value = input.DepreciationRate;

                }
                if (input.DepreciationMethod != null)
                {
                    cbbDepreciationMethod.SelectedIndex = (int)input.DepreciationMethod;
                }
                GetCostBy.CostByBudget = input.CostByBudget;
                GetCostBy.CostByEquity = input.CostByEquity;
                GetCostBy.CostByLoan = input.CostByLoan;
                GetCostBy.CostByOther = input.CostByOther;
                GetCostBy.CostByVenture = input.CostByVenture;
                if (input.CostSetID != null)
                {
                    foreach (var item in cbbCostSet.Rows)
                    {
                        if ((item.ListObject as CostSet).ID == input.CostSetID)
                        {
                            cbbCostSet.SelectedRow = item;
                        }
                    }

                }
                chkIsDepreciationAllocation.Checked = input.IsDepreciationAllocation;


                if (input.MonthDepreciationRate != null)
                {
                    nmrMonthDepreciationRate.Value = input.MonthDepreciationRate;

                }
                if (input.MonthPeriodDepreciationAmount != null)
                {
                    nmrMonthPeriodDepreciationAmount.Value = input.MonthPeriodDepreciationAmount;

                }
                txtDeliveryRecordNo.Text = input.DeliveryRecordNo;

                if (input.DeliveryRecordDate != null)
                {
                    cldDeliveryRecordDate.Value = input.DeliveryRecordDate;

                }
                else
                {
                    cldDeliveryRecordDate.Value = null;
                }
                chkIsIrrationalCost.Checked = (bool)(input.IsIrrationalCost ?? false);

                if (!input.IsDisplayMonth)
                {
                    CbbYearMonth.SelectedIndex = 1;
                }
                if (input.UsedTime != null)
                {
                    nmrUsedTime.Value = input.UsedTime;

                }
            }
            return input;
        }

        bool GetBoolDateTime(UltraDateTimeEditor cld, bool dk)
        {
            DateTime dt;
            if (cld.Value != null)
            {
                try
                {
                    dt = DateTime.Parse(cld.Value.ToString());
                    return true;
                }
                catch (Exception)
                {
                    if (dk)
                        MSG.Error(resSystem.MSG_Catalog_FCostSetDetail6);
                    return false;
                }
            }

            return dk;


        }
        #region gan gia tri cho cac truong Cost
        void getCostByValue()
        {
            FixedAsset temp = Them ? new FixedAsset() : _IFixedAssetService.Getbykey(_Select.ID);
            temp.CostByBudget = FOriginalPrice.CostByBudget;
            temp.CostByEquity = FOriginalPrice.CostByEquity;
            temp.CostByLoan = FOriginalPrice.CostByLoan;
            temp.CostByOther = FOriginalPrice.CostByOther;
            temp.CostByVenture = FOriginalPrice.CostByVenture;
            FOriginalPrice.isClose = true;
            GetCostBy = temp;
            SetValueForRemainingAmount();
            SetValueCaculator();
        }

        #endregion

        //check du lieu
        bool CheckError()
        {
            bool kq = true;
            //Check Error Chung

            //Check Error Riêng từng Form

            if (string.IsNullOrEmpty(txtFixedAssetCode.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                ultraTabControl1.SelectedTab = ultraTabControl1.Tabs[0];
                txtFixedAssetCode.Focus();
                return false;
            }
            //else if (Them && _IFixedAssetService.Query.Where(c => c.FixedAssetCode.Equals(txtFixedAssetCode.Text)).Count() > 0)
            else if (Them && _IFixedAssetService.CheckExistFA(txtFixedAssetCode.Text, _Select.ID) > 0)
            {
                MSG.Error(string.Format(resSystem.MSG_Catalog_Account6, "Tài sản cố định"));
                ultraTabControl1.SelectedTab = ultraTabControl1.Tabs[0];
                txtFixedAssetCode.Focus();
                return false;
            }
            else if (string.IsNullOrEmpty(txtFixedAssetName.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                ultraTabControl1.SelectedTab = ultraTabControl1.Tabs[0];
                txtFixedAssetName.Focus();
                return false;
            }
            else if (cbbFixedAssetCategoryID.SelectedRow == null)
            {
                MSG.Error(resSystem.MSG_System_03);
                ultraTabControl1.SelectedTab = ultraTabControl1.Tabs[0];
                cbbFixedAssetCategoryID.Focus();
                return false;
            }
            if (cbbDepartmentID.Text.Trim() == "")
            {
                MSG.Warning(resSystem.MSG_Catalog_FFixedAssetDetail8);
                ultraTabControl1.SelectedTab = ultraTabControl1.Tabs[0];
                cbbDepartmentID.Focus();
                return false;
            }
            if (!(GetBoolDateTime(cldDeliveryRecordDate, true) && GetBoolDateTime(cldDepreciationDate, true) && GetBoolDateTime(cldDisposedDate, true) &&
                GetBoolDateTime(cldIncrementDate, true) && GetBoolDateTime(cldPurchasedDate, true) && GetBoolDateTime(cldUsedDate, true)))
            {
                return false;
            }
            if (string.IsNullOrEmpty(cbbOriginalPriceAccount.Text))
            {
                MSG.Warning(resSystem.MSG_Catalog_FFixedAssetDetail9);
                ultraTabControl1.SelectedTab = ultraTabControl1.Tabs[1];
                cbbOriginalPriceAccount.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(cbbDepreciationAccount.Text))
            {
                MSG.Warning(resSystem.MSG_Catalog_FFixedAssetDetail10);
                ultraTabControl1.SelectedTab = ultraTabControl1.Tabs[1];
                cbbDepreciationAccount.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(cbbExpenditureAccount.Text))
            {
                MSG.Warning(resSystem.MSG_Catalog_FFixedAssetDetail11);
                ultraTabControl1.SelectedTab = ultraTabControl1.Tabs[1];
                cbbExpenditureAccount.Focus();
                return false;
            }
            if (cldIncrementDate.Value == null)
            {
                MSG.Warning(resSystem.MSG_Catalog_FFixedAssetDetail22);
                cldIncrementDate.Focus();
                return false;
            }
            if (cldDepreciationDate.Value == null)
            {
                MSG.Warning(resSystem.MSG_Catalog_FFixedAssetDetail23);
                cldDepreciationDate.Focus();
                return false;
            }
            if (cldIncrementDate.Value != null && cldPurchasedDate.Value != null)
            {
                DateTime ghiTang = DateTime.Parse(cldIncrementDate.Value.ToString());
                DateTime mua = DateTime.Parse(cldPurchasedDate.Value.ToString());
                if (DateTime.Compare(ghiTang, mua) < 0)
                {
                    MSG.Error(resSystem.MSG_Catalog_FFixedAssetDetail);
                    ultraTabControl1.SelectedTab = ultraTabControl1.Tabs[1];
                    cldIncrementDate.Focus();
                    return false;
                }
                if (cldUsedDate.Value != null)
                {
                    DateTime Sudung = DateTime.Parse(cldUsedDate.Value.ToString());
                    if (DateTime.Compare(Sudung, ghiTang) < 0)
                    {
                        MSG.Error(resSystem.MSG_Catalog_FFixedAssetDetail1);
                        ultraTabControl1.SelectedTab = ultraTabControl1.Tabs[1];
                        cldUsedDate.Focus();
                        return false;
                    }
                }
                if (cldDepreciationDate.Value != null)
                {
                    DateTime khauHao = DateTime.Parse(cldDepreciationDate.Value.ToString());
                    if (DateTime.Compare(khauHao, ghiTang) < 0)
                    {
                        MSG.Error(resSystem.MSG_Catalog_FFixedAssetDetail2);
                        ultraTabControl1.SelectedTab = ultraTabControl1.Tabs[1];
                        cldDepreciationDate.Focus();
                        return false;
                    }
                }

                // Ngày ghi tăng phải lớn hơn ngày sử dụng phần mềm
                if (DateTime.Compare(ApplicationStartDate, ghiTang) > 0)
                {
                    MSG.Error("Ngày ghi tăng nhỏ hơn ngày sử dụng phần mềm. Vui lòng thực hiện tại nghiệp vụ Khai báo TSCĐ đầu kỳ");
                    cldIncrementDate.Focus();
                    return false;
                }
            }
            if (cldIncrementDate.Value != null && cldPurchasedDate.Value == null
                || cldIncrementDate.NullText != "Chọn ngày tháng" && cldDepreciationDate.NullText == "Chọn ngày tháng")
            {
                DateTime ghiTang = DateTime.Parse(cldIncrementDate.Value.ToString());
                if (cldUsedDate.Value != null)
                {
                    DateTime Sudung = DateTime.Parse(cldUsedDate.Value.ToString());
                    if (DateTime.Compare(Sudung, ghiTang) < 0)
                    {
                        MSG.Error(resSystem.MSG_Catalog_FFixedAssetDetail1);
                        ultraTabControl1.SelectedTab = ultraTabControl1.Tabs[1];
                        cldUsedDate.Focus();
                        return false;
                    }
                }
                if (cldDepreciationDate.Value != null)
                {
                    DateTime khauHao = DateTime.Parse(cldDepreciationDate.Value.ToString());
                    if (DateTime.Compare(khauHao, ghiTang) < 0)
                    {
                        MSG.Error(resSystem.MSG_Catalog_FFixedAssetDetail2);
                        ultraTabControl1.SelectedTab = ultraTabControl1.Tabs[1];
                        cldDepreciationDate.Focus();
                        return false;
                    }
                }
            }
            if (cldIncrementDate.Value == null && cldPurchasedDate.Value != null
                || cldIncrementDate.NullText == "Chọn ngày tháng" && cldDepreciationDate.NullText != "Chọn ngày tháng")
            {
                // DateTime ghiTang = DateTime.Parse(cldIncrementDate.Value.ToString());
                DateTime mua = DateTime.Parse(cldPurchasedDate.Value.ToString());

                if (cldUsedDate.Value != null)
                {
                    DateTime Sudung = DateTime.Parse(cldUsedDate.Value.ToString());
                    if (DateTime.Compare(Sudung, mua) < 0)
                    {
                        MSG.Error(resSystem.MSG_Catalog_FFixedAssetDetail3);
                        ultraTabControl1.SelectedTab = ultraTabControl1.Tabs[1];
                        cldUsedDate.Focus();
                        return false;
                    }
                }
                if (cldDepreciationDate.Value != null)
                {
                    DateTime khauHao = DateTime.Parse(cldDepreciationDate.Value.ToString());
                    if (DateTime.Compare(khauHao, mua) < 0)
                    {
                        MSG.Error(resSystem.MSG_Catalog_FFixedAssetDetail4);
                        ultraTabControl1.SelectedTab = ultraTabControl1.Tabs[1];
                        cldDepreciationDate.Focus();
                        return false;
                    }
                }
            }
            if (cldDisposedDate.Value != null && cldPurchasedDate.Value != null)
            {
                if (DateTime.Compare(DateTime.Parse(cldDisposedDate.Value.ToString()), DateTime.Parse(cldPurchasedDate.Value.ToString())) < 0)
                {
                    MSG.Error(resSystem.MSG_Catalog_FFixedAssetDetail5);
                    cldDisposedDate.Focus();
                    return false;
                }
            }
            else if (cldDisposedDate.Value != null && cldIncrementDate.Value != null)
            {
                if (DateTime.Compare(DateTime.Parse(cldDisposedDate.Value.ToString()), DateTime.Parse(cldIncrementDate.Value.ToString())) < 0)
                {
                    MSG.Error(resSystem.MSG_Catalog_FFixedAssetDetail6);
                    ultraTabControl1.SelectedTab = ultraTabControl1.Tabs[1];
                    cldDisposedDate.Focus();
                    return false;
                }
            }
            if (!string.IsNullOrEmpty(nmrRemainingAmount.Text))
            {
                try
                {
                    decimal.Parse(nmrRemainingAmount.Text);
                }
                catch (Exception)
                {
                    MSG.Error(resSystem.MSG_Catalog_FFixedAssetDetail7);
                    ultraTabControl1.SelectedTab = ultraTabControl1.Tabs[1];
                    nmrRemainingAmount.Focus();
                    return false;
                }
            }
            if ((nmrUsedTime.Value == null) || (nmrUsedTime.Text.Trim() == "") || (nmrUsedTime.Text == "0") || (nmrUsedTime.Value.ToInt() == 0))
            {
                if (MSG.Question("Thời gian sử dụng chưa được nhập, bạn có muốn tiếp tục lưu không?") == DialogResult.Yes)
                {
                    nmrPeriodDepreciationAmount.Value = 0;
                    nmrDepreciationRate.Value = 0;
                    nmrMonthPeriodDepreciationAmount.Value = 0;
                    nmrMonthDepreciationRate.Value = 0;
                    nmrAcDepreciationAmount.Value = 0;
                    return true;
                }
                else
                {
                    ultraTabControl1.SelectedTab = ultraTabControl1.Tabs[1];
                    nmrUsedTime.Focus();
                    return false;
                }
            }
            if (!nmrPeriodDepreciationAmount.Text.IsNullOrEmpty() && !nmrPurchasePrice.Text.IsNullOrEmpty())
            {
                if (Decimal.Parse(nmrPeriodDepreciationAmount.Text.ToString()) > Decimal.Parse(nmrPurchasePrice.Text.ToString()))
                {
                    MSG.Error("Giá trị khấu hao năm không được lớn hơn Giá trị tính khấu hao");
                    nmrPeriodDepreciationAmount.Focus();
                    return false;
                }

            }

            return kq;
        }

        void XuLyDuLieu()
        {
            nmrOriginalPrice.Value = FOriginalPrice.Total;
            nmrPurchasePrice.Value = FOriginalPrice.Total;

        }

        void SetValueForRemainingAmount()
        {
            try
            {
                var _nmrPurchasePrice = string.IsNullOrEmpty(nmrPurchasePrice.Value.ToString()) || nmrPurchasePrice.Value == DBNull.Value ? 0 : nmrPurchasePrice.Value;
                var _nmrAcDepreciationAmount = string.IsNullOrEmpty(nmrAcDepreciationAmount.Value.ToString()) || nmrAcDepreciationAmount.Value == DBNull.Value ? 0 : nmrAcDepreciationAmount.Value;
                nmrRemainingAmount.Value = Convert.ToDecimal(_nmrPurchasePrice) - Convert.ToDecimal(_nmrAcDepreciationAmount);
            }
            catch (Exception ex)
            {
                nmrRemainingAmount.Value = default(decimal);
            }
        }

        void SetValueCaculator()
        {
            decimal giatriThang = 0;
            if (!string.IsNullOrEmpty(nmrUsedTime.Value.ToString()) && !string.IsNullOrEmpty(nmrPurchasePrice.Value.ToString()))
            {
                decimal soThang = isDisplayMounth ? decimal.Parse(nmrUsedTime.Value.ToString()) : decimal.Parse(nmrUsedTime.Value.ToString()) * 12;

                giatriThang = decimal.Parse(nmrPurchasePrice.Value.ToString()) / (soThang == 0 ? 1 : soThang);

                nmrMonthPeriodDepreciationAmount.Value = giatriThang;
                nmrMonthDepreciationRate.Value = 100 / (soThang == 0 ? 1 : soThang);

                nmrPeriodDepreciationAmount.Value = giatriThang * 12;
                nmrDepreciationRate.Value = 1200 / (soThang == 0 ? 1 : soThang);
            }
        }

        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, ConstDatabase.FixedAssetDetail_TableName, new List<TemplateColumn>(), 0);

        }

        private void UGridDetails_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (UGridDetails.ActiveCell.Column.DataType == typeof(Guid))
                {
                    return;
                }
                else if (UGridDetails.ActiveCell.Column.DataType == typeof(string))
                {
                    UGridDetails.ActiveCell.SetValue(string.Empty, true);
                }
            }
        }

        private void UGridDetails_BeforeRowInsert(object sender, Infragistics.Win.UltraWinGrid.BeforeRowInsertEventArgs e)
        {
            if (UGridDetails.Rows.TemplateAddRow.IsTemplateAddRow)
            {//nếu dòng thêm mới được TemplateAddRow thêm vào
                int lastRow = UGridDetails.Rows.Count - 1;
                if (lastRow >= 0)
                {//copy row cuối vào row mới
                    UGridDetails.Rows[lastRow].Selected = true;
                    this.UGridDetails.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.Copy);
                    UGridDetails.Rows[lastRow].Selected = false;
                    //uGridPosted.Rows.TemplateAddRow.Selected = true;
                    //this.uGridPosted.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.Paste);
                }
            }
        }

        private void UGridDetails_BeforeCellUpdate(object sender, Infragistics.Win.UltraWinGrid.BeforeCellUpdateEventArgs e)
        {
            if (e.Cell.Column.Key.Equals("Desciption"))
            {//trước khi cell AmountOriginal cập nhật thì kiểm tra nếu giá trị mới rỗng thì mặc định quay trở về giá trị cũ
                decimal temp = 0;
                string value = e.NewValue.ToString();
                if (string.IsNullOrEmpty(value))    // || decimal.TryParse(value, out temp)
                {
                    e.Cancel = true;
                }
            }
        }
        void loadFixedAssetCategory()
        {
            this.ConfigCombo(Utils.ListFixedAssetCategory, cbbFixedAssetCategoryID, "FixedAssetCategoryName", "ID");
            this.ConfigCombo(Utils.ListDepartment, cbbDepartmentID, "DepartmentName", "ID");
        }
        private void cbbFixedAssetCategoryID_EditorButtonClick(object sender, EditorButtonEventArgs e)
        {
            new FFixedAssetCategoryDetail().ShowDialog(this);

            Utils.ClearCacheByType<FixedAssetCategory>();
            loadFixedAssetCategory();
            if (FFixedAssetCategoryDetail.FixedCategoryCode != null)
            {
                cbbFixedAssetCategoryID.Text = FFixedAssetCategoryDetail.FixedCategoryCode;
                cbbOriginalPriceAccount.Text = FFixedAssetCategoryDetail.OriginalPriceAccount;
                cbbExpenditureAccount.Text = FFixedAssetCategoryDetail.ExpenditureAccount;
                cbbDepreciationAccount.Text = FFixedAssetCategoryDetail.DepreciationAccount;
                FFixedAssetCategoryDetail.FixedCategoryCode = null;
                FFixedAssetCategoryDetail.OriginalPriceAccount = null;
                FFixedAssetCategoryDetail.ExpenditureAccount = null;
                FFixedAssetCategoryDetail.DepreciationAccount = null;
            }
            if (cbbFixedAssetCategoryID.IsDroppedDown)
            {
                cbbFixedAssetCategoryID.ToggleDropdown();
            }
            if (cbbOriginalPriceAccount.IsDroppedDown)
            {
                cbbOriginalPriceAccount.ToggleDropdown();
            }
            if (cbbExpenditureAccount.IsDroppedDown)
            {
                cbbExpenditureAccount.ToggleDropdown();
            }
            if (cbbDepreciationAccount.IsDroppedDown)
            {
                cbbDepreciationAccount.ToggleDropdown();
            }
        }

        private void cbbAccountingObject_EditorButtonClick(object sender, EditorButtonEventArgs e)
        {
            new FAccountingObjectCustomersDetail().ShowDialog(this);
            this.ConfigCombo(Utils.ListAccountingObject, cbbAccountingObject, "AccountingObjectCode", "ID", accountingObjectType: 1);
            Utils.ClearCacheByType<AccountingObject>();
            cbbAccountingObject.Text = FAccountingObjectCustomersDetail.AccounttingObjectCode;
        }

        private void nmrProductionYear_EditorSpinButtonClick(object sender, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        {

        }

        private void cbbFixedAssetCategoryID_ValueChanged(object sender, EventArgs e)
        {
            if (e == null || cbbFixedAssetCategoryID.SelectedRow == null) return;
            FixedAssetCategory temp = (FixedAssetCategory)cbbFixedAssetCategoryID.SelectedRow.ListObject;
            cbbOriginalPriceAccount.SelectedRow = null;
            cbbDepreciationAccount.SelectedRow = null;
            cbbExpenditureAccount.SelectedRow = null;
            foreach (var item in cbbOriginalPriceAccount.Rows)
            {
                if (temp.OriginalPriceAccount != null)
                {
                    if ((item.ListObject as Account).AccountNumber == temp.OriginalPriceAccount)
                    {
                        cbbOriginalPriceAccount.SelectedRow = item;
                        break;
                    }
                }
            }
            foreach (var item in cbbDepreciationAccount.Rows)
            {
                if (temp.DepreciationAccount != null)
                {
                    if ((item.ListObject as Account).AccountNumber == temp.DepreciationAccount)
                    {
                        cbbDepreciationAccount.SelectedRow = item;
                        break;
                    }
                }
            }
            foreach (var item in cbbExpenditureAccount.Rows)
            {
                if (temp.ExpenditureAccount != null)
                {
                    if ((item.ListObject as Account).AccountNumber == temp.ExpenditureAccount)
                    {
                        cbbExpenditureAccount.SelectedRow = item;
                        break;
                    }
                }
            }
        }

        private void tsmAdd_Click(object sender, EventArgs e)
        {
            uGridFixedAssetAccessories.AddNewRow4Grid();
        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            if (uGridFixedAssetAccessories.ActiveCell != null)
            {
                this.RemoveRow4Grid<FFixedAssetDetail>(uGridFixedAssetAccessories);
                uGridFixedAssetAccessories.Update();
            }
        }

        private void tsmAdd1_Click(object sender, EventArgs e)
        {
            UGridDetails.AddNewRow4Grid();
        }

        private void tsmDelete1_Click(object sender, EventArgs e)
        {
            if (UGridDetails.ActiveCell != null)
            {
                this.RemoveRow4Grid<FFixedAssetDetail>(UGridDetails);
                UGridDetails.Update();
            }
        }

        private void nmrUsedTime_Validated(object sender, EventArgs e)
        {
            //    decimal giatriThang = 0;
            //    decimal tyLeThang = 0;
            //    if (!string.IsNullOrEmpty(nmrUsedTime.Value.ToString()) && !string.IsNullOrEmpty(nmrPurchasePrice.Value.ToString()))
            //    {
            //        decimal soThang = 1;
            //        if (isDisplayMounth)
            //        {
            //            soThang = decimal.Parse(nmrUsedTime.Value.ToString());
            //        }
            //        else
            //        {
            //            soThang = decimal.Parse(nmrUsedTime.Value.ToString()) * 12;
            //        }
            //        giatriThang = soThang == 0 ? 0 : decimal.Parse(nmrPurchasePrice.Value.ToString()) / soThang;

            //        tyLeThang = (decimal.Parse(nmrPurchasePrice.Value.ToString()) == 0) ? ((giatriThang / decimal.Parse(nmrPurchasePrice.Value.ToString())) * 100) : 0;
            //        //tyLeThang = (giatriThang / decimal.Parse(nmrPurchasePrice.Value.ToString())) * 100;
            //        nmrMonthPeriodDepreciationAmount.Value = giatriThang.ToString();
            //        nmrMonthDepreciationRate.Value = tyLeThang.ToString();
            //        nmrPeriodDepreciationAmount.Value = (giatriThang * 12).ToString();
            //        nmrDepreciationRate.Value = (tyLeThang * 12).ToString();
            //    }
            SetValueForRemainingAmount();
            SetValueCaculator();
        }

        private void nmrPurchasePrice_Validated(object sender, EventArgs e)
        {
            SetValueForRemainingAmount();
            SetValueCaculator();
        }

        private void nmrDepreciationRate_Validated(object sender, EventArgs e)
        {
            if ((nmrPurchasePrice.Value != null) && (nmrPurchasePrice.Text.Trim() != "") && (nmrPurchasePrice.Text != "0,00") && (nmrDepreciationRate.Value != null) && (nmrDepreciationRate.Text.Trim() != "") && (nmrDepreciationRate.Text != "0,00"))
            {
                nmrPeriodDepreciationAmount.Value = (Convert.ToDecimal(nmrDepreciationRate.Value) * Convert.ToDecimal(nmrPurchasePrice.Value)) / 100;
                nmrMonthDepreciationRate.Value = (Convert.ToDecimal(nmrDepreciationRate.Value)) / 12;
                nmrMonthPeriodDepreciationAmount.Value = (Convert.ToDecimal(nmrMonthDepreciationRate.Value) * Convert.ToDecimal(nmrPurchasePrice.Value)) / 100;
            }
            else if ((nmrDepreciationRate.Value != null) && (nmrDepreciationRate.Text.Trim() != "") && (nmrDepreciationRate.Text != "0,00") && ((nmrPurchasePrice.Value == null) || (nmrPurchasePrice.Text.Trim() == "") || (nmrPurchasePrice.Text == "0,00")))
            {
                nmrMonthDepreciationRate.Value = (Convert.ToDecimal(nmrDepreciationRate.Value)) / 12;
            }
            else if (nmrDepreciationRate.Text.Trim() == "")
            {
                nmrPeriodDepreciationAmount.ResetText();
                nmrMonthPeriodDepreciationAmount.ResetText();
                nmrMonthDepreciationRate.ResetText();
            }
        }

        private void nmrMonthDepreciationRate_Validated(object sender, EventArgs e)
        {
            if (nmrMonthDepreciationRate.Text.Trim() == "")
            {
                nmrPeriodDepreciationAmount.ResetText();
                nmrDepreciationRate.ResetText();
                nmrMonthPeriodDepreciationAmount.ResetText();
            }
        }

        private void nmrPeriodDepreciationAmount_Validated(object sender, EventArgs e)
        {
            if ((nmrPeriodDepreciationAmount.Value != null) && (nmrPeriodDepreciationAmount.Text.Trim() != "") && (nmrPeriodDepreciationAmount.Text != "0,00"))
            {
                nmrMonthPeriodDepreciationAmount.Value = (Convert.ToDecimal(nmrPeriodDepreciationAmount.Value)) / 12;
            }
            else if (nmrPeriodDepreciationAmount.Text.Trim() == "")
            {
                nmrMonthPeriodDepreciationAmount.ResetText();
            }
        }

        private void cbbDepartmentID_ValueChanged(object sender, EventArgs e)
        {
            var row = cbbDepartmentID.SelectedRow;
            if (row == null) return;
            if (row.Cells["IsParentNode"].Value != null && (bool)row.Cells["IsParentNode"].Value == true)
            {
                cbbDepartmentID.Value = null;
                cbbDepartmentID.ToggleDropdown();
                MSG.Warning("Trường phòng ban đang là chỉ tiêu tổng hợp. Vui lòng thử lại!");
            }
        }

        private void cbbOriginalPriceAccount_ItemNotInList(object sender, ValidationErrorEventArgs e)
        {
            Account b = new Account();
            if (cbbOriginalPriceAccount.Text != b.AccountNumber && cbbOriginalPriceAccount.Text != "")
            {
                MSG.Warning("Dữ liệu không có trong danh mục!");
                cbbOriginalPriceAccount.Focus();
                return;
            }
        }

        private void cbbDepreciationAccount_ItemNotInList(object sender, ValidationErrorEventArgs e)
        {
            Account b = new Account();
            if (cbbDepreciationAccount.Text != b.AccountNumber && cbbDepreciationAccount.Text != "")
            {
                MSG.Warning("Dữ liệu không có trong danh mục!");
                cbbDepreciationAccount.Focus();
                return;
            }
        }

        private void cbbExpenditureAccount_ItemNotInList(object sender, ValidationErrorEventArgs e)
        {
            Account b = new Account();
            if (cbbExpenditureAccount.Text != b.AccountNumber && cbbExpenditureAccount.Text != "")
            {
                MSG.Warning("Dữ liệu không có trong danh mục!");
                cbbExpenditureAccount.Focus();
                return;
            }
        }

        private void cbbDepartmentID_EditorButtonClick(object sender, EditorButtonEventArgs e)
        {
            new FDepartmentDetail().ShowDialog(this);

            Utils.ClearCacheByType<Department>();
            loadFixedAssetCategory();
            if (FDepartmentDetail.DepartmentName != null)
            {
                cbbDepartmentID.Text = FDepartmentDetail.DepartmentName;
                FDepartmentDetail.DepartmentName = null;
            }
            if (cbbDepartmentID.IsDroppedDown)
            {
                cbbDepartmentID.ToggleDropdown();
            }
        }

        private void FFixedAssetDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
