﻿namespace Accounting
{
    partial class FAutoPrincipleDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbTypeID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbbCreditAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbbDebitAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.lblCreditAccount = new Infragistics.Win.Misc.UltraLabel();
            this.lblDebitAccount = new Infragistics.Win.Misc.UltraLabel();
            this.txtDescription = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.labelDescription = new Infragistics.Win.Misc.UltraLabel();
            this.txtAutoPrincipleName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblAutoPrincipleName = new Infragistics.Win.Misc.UltraLabel();
            this.lblTypeID = new Infragistics.Win.Misc.UltraLabel();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.chkIsActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCreditAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDebitAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAutoPrincipleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox1.Controls.Add(this.cbbTypeID);
            this.ultraGroupBox1.Controls.Add(this.cbbCreditAccount);
            this.ultraGroupBox1.Controls.Add(this.cbbDebitAccount);
            this.ultraGroupBox1.Controls.Add(this.lblCreditAccount);
            this.ultraGroupBox1.Controls.Add(this.lblDebitAccount);
            this.ultraGroupBox1.Controls.Add(this.txtDescription);
            this.ultraGroupBox1.Controls.Add(this.labelDescription);
            this.ultraGroupBox1.Controls.Add(this.txtAutoPrincipleName);
            this.ultraGroupBox1.Controls.Add(this.lblAutoPrincipleName);
            this.ultraGroupBox1.Controls.Add(this.lblTypeID);
            appearance6.FontData.BoldAsString = "True";
            this.ultraGroupBox1.HeaderAppearance = appearance6;
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(3, 3);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(310, 184);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbTypeID
            // 
            this.cbbTypeID.AutoSize = false;
            this.cbbTypeID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbTypeID.Location = new System.Drawing.Point(108, 25);
            this.cbbTypeID.Name = "cbbTypeID";
            this.cbbTypeID.NullText = "<Chọn dữ liệu>";
            this.cbbTypeID.Size = new System.Drawing.Size(192, 22);
            this.cbbTypeID.TabIndex = 27;
            // 
            // cbbCreditAccount
            // 
            this.cbbCreditAccount.AutoSize = false;
            this.cbbCreditAccount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbCreditAccount.Location = new System.Drawing.Point(108, 109);
            this.cbbCreditAccount.Name = "cbbCreditAccount";
            this.cbbCreditAccount.NullText = "<Chọn dữ liệu>";
            this.cbbCreditAccount.Size = new System.Drawing.Size(192, 22);
            this.cbbCreditAccount.TabIndex = 26;
            // 
            // cbbDebitAccount
            // 
            this.cbbDebitAccount.AutoSize = false;
            this.cbbDebitAccount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbDebitAccount.Location = new System.Drawing.Point(108, 81);
            this.cbbDebitAccount.Name = "cbbDebitAccount";
            this.cbbDebitAccount.NullText = "<Chọn dữ liệu>";
            this.cbbDebitAccount.Size = new System.Drawing.Size(192, 22);
            this.cbbDebitAccount.TabIndex = 25;
            // 
            // lblCreditAccount
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.TextVAlignAsString = "Middle";
            this.lblCreditAccount.Appearance = appearance1;
            this.lblCreditAccount.Location = new System.Drawing.Point(10, 109);
            this.lblCreditAccount.Name = "lblCreditAccount";
            this.lblCreditAccount.Size = new System.Drawing.Size(94, 22);
            this.lblCreditAccount.TabIndex = 22;
            this.lblCreditAccount.Text = "Tài khoản Có";
            // 
            // lblDebitAccount
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.TextVAlignAsString = "Middle";
            this.lblDebitAccount.Appearance = appearance2;
            this.lblDebitAccount.Location = new System.Drawing.Point(10, 81);
            this.lblDebitAccount.Name = "lblDebitAccount";
            this.lblDebitAccount.Size = new System.Drawing.Size(94, 22);
            this.lblDebitAccount.TabIndex = 20;
            this.lblDebitAccount.Text = "Tài khoản Nợ";
            // 
            // txtDescription
            // 
            this.txtDescription.AutoSize = false;
            this.txtDescription.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.txtDescription.Location = new System.Drawing.Point(108, 137);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(192, 41);
            this.txtDescription.TabIndex = 6;
            // 
            // labelDescription
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextVAlignAsString = "Middle";
            this.labelDescription.Appearance = appearance3;
            this.labelDescription.Location = new System.Drawing.Point(10, 137);
            this.labelDescription.Name = "labelDescription";
            this.labelDescription.Size = new System.Drawing.Size(104, 22);
            this.labelDescription.TabIndex = 1;
            this.labelDescription.Text = "Mô tả";
            // 
            // txtAutoPrincipleName
            // 
            this.txtAutoPrincipleName.AutoSize = false;
            this.txtAutoPrincipleName.Location = new System.Drawing.Point(108, 53);
            this.txtAutoPrincipleName.Name = "txtAutoPrincipleName";
            this.txtAutoPrincipleName.Size = new System.Drawing.Size(192, 22);
            this.txtAutoPrincipleName.TabIndex = 6;
            // 
            // lblAutoPrincipleName
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.lblAutoPrincipleName.Appearance = appearance4;
            this.lblAutoPrincipleName.Location = new System.Drawing.Point(10, 53);
            this.lblAutoPrincipleName.Name = "lblAutoPrincipleName";
            this.lblAutoPrincipleName.Size = new System.Drawing.Size(104, 22);
            this.lblAutoPrincipleName.TabIndex = 1;
            this.lblAutoPrincipleName.Text = "Tên định khoản (*)";
            // 
            // lblTypeID
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.TextVAlignAsString = "Middle";
            this.lblTypeID.Appearance = appearance5;
            this.lblTypeID.Location = new System.Drawing.Point(10, 25);
            this.lblTypeID.Name = "lblTypeID";
            this.lblTypeID.Size = new System.Drawing.Size(92, 22);
            this.lblTypeID.TabIndex = 0;
            this.lblTypeID.Text = "Loại chứng từ (*)";
            // 
            // btnSave
            // 
            appearance7.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance7;
            this.btnSave.Location = new System.Drawing.Point(138, 193);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 11;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            appearance8.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance8;
            this.btnClose.Location = new System.Drawing.Point(225, 193);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 12;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // chkIsActive
            // 
            appearance9.TextVAlignAsString = "Middle";
            this.chkIsActive.Appearance = appearance9;
            this.chkIsActive.BackColor = System.Drawing.Color.Transparent;
            this.chkIsActive.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsActive.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkIsActive.Checked = true;
            this.chkIsActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIsActive.Location = new System.Drawing.Point(4, 197);
            this.chkIsActive.Name = "chkIsActive";
            this.chkIsActive.Size = new System.Drawing.Size(103, 22);
            this.chkIsActive.TabIndex = 16;
            this.chkIsActive.Text = "Hoạt động";
            // 
            // FAutoPrincipleDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(316, 230);
            this.Controls.Add(this.chkIsActive);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.ultraGroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FAutoPrincipleDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FAutoPrincipleDetail_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCreditAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDebitAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAutoPrincipleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraLabel lblTypeID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAutoPrincipleName;
        private Infragistics.Win.Misc.UltraLabel lblAutoPrincipleName;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraLabel lblCreditAccount;
        private Infragistics.Win.Misc.UltraLabel lblDebitAccount;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsActive;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbTypeID;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbCreditAccount;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbDebitAccount;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDescription;
        private Infragistics.Win.Misc.UltraLabel labelDescription;
    }
}
