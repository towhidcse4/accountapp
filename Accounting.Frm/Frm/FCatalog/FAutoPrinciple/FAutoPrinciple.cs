﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using Accounting.TextMessage;
using System.Linq;

namespace Accounting
{
    public partial class FAutoPrinciple : CatalogBase //UserControl
    {
        #region khai báo
        private readonly IAutoPrincipleService _IAutoPrincipleService;
        #endregion

        #region khởi tạo
        public FAutoPrinciple()
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _IAutoPrincipleService = IoC.Resolve<IAutoPrincipleService>();
            this.uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            #endregion

            #region Tạo dữ liệu ban đầu
            LoadDuLieu();
            #endregion
        }

        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }
        private void LoadDuLieu(bool configGrid)
        {
            WaitingFrm.StartWaiting();
            #region Lấy dữ liệu từ CSDL
            List<AutoPrinciple> list = _IAutoPrincipleService.GetAll_OrderBy();
            _IAutoPrincipleService.UnbindSession(list);
            list = _IAutoPrincipleService.GetAll_OrderBy();
            foreach (var item in list)
            {
                var firstOrDefault = Utils.ListType.FirstOrDefault(x => x.ID == item.TypeID);
                //if (firstOrDefault != null)
                //    item.TypeName = firstOrDefault.TypeName;
            }
            #endregion

            #region Xử lý dữ liệu
            #endregion

            #region hiển thị và xử lý hiển thị
            uGrid.DataSource = list.ToArray();

            if (configGrid) ConfigGrid(uGrid);
            if ((uGrid.Selected.Rows.Count == 0) && (list.Count > 0))
            {
                uGrid.Rows[0].Selected = true;
            }
            #endregion
            WaitingFrm.StopWaiting();
        }
        #endregion

        #region Nghiệp vụ
        #region ContentMenu
        private void tsmAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void tsmEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            ResetFunction();
        }
        protected override void ResetFunction()
        {
            LoadDuLieu(false);
        }
        #endregion

        #region Button
        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }
        #endregion

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (e.Row.Index >= 0)
            {
                AutoPrinciple temp = e.Row.ListObject as AutoPrinciple;
                new FAutoPrincipleDetail(temp).ShowDialog(this);
                if (!FAutoPrincipleDetail.isClose) LoadDuLieu();
            }
        }

        /// <summary>
        /// Nghiệp vụ Add
        /// </summary>
        protected override void AddFunction()
        {
            new FAutoPrincipleDetail().ShowDialog(this);
            if (!FAutoPrincipleDetail.isClose) LoadDuLieu();
        }

        /// <summary>
        /// Nghiệp vụ Edit
        /// </summary>
        protected override void EditFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                AutoPrinciple temp = uGrid.Selected.Rows[0].ListObject as AutoPrinciple;
                new FAutoPrincipleDetail(temp).ShowDialog(this);
                if (!FAutoPrincipleDetail.isClose) LoadDuLieu();
            }
            else
                MSG.Error(string.Format(resSystem.MSG_System_04, "định khoản"));
        }

        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        protected override void DeleteFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                AutoPrinciple temp = uGrid.Selected.Rows[0].ListObject as AutoPrinciple;
                if (MSG.Question(string.Format(resSystem.MSG_System_05, temp.AutoPrincipleName)) == System.Windows.Forms.DialogResult.Yes)
                {
                    try
                    {
                        _IAutoPrincipleService.BeginTran();
                        _IAutoPrincipleService.Delete(temp);
                        _IAutoPrincipleService.CommitTran();
                        Utils.ClearCacheByType<AutoPrinciple>();
                        LoadDuLieu();
                    }
                    catch
                    {
                        _IAutoPrincipleService.RolbackTran();
                    }
                }
            }
            else
                MSG.Error(string.Format(resSystem.MSG_Catalog2, "định khoản"));
        }
        #endregion

        #region Utils
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, TextMessage.ConstDatabase.AutoPrinciple_TableName);
        }
        #endregion

        private void uGrid_MouseDown_1(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }

        private void FAutoPrinciple_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }

        private void FAutoPrinciple_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGrid.ResetText();
            uGrid.ResetUpdateMode();
            uGrid.ResetExitEditModeOnLeave();
            uGrid.ResetRowUpdateCancelAction();
            uGrid.DataSource = null;
            uGrid.Layouts.Clear();
            uGrid.ResetLayouts();
            uGrid.ResetDisplayLayout();
            uGrid.Refresh();
            uGrid.ClearUndoHistory();
            uGrid.ClearXsdConstraints();
        }
    }
}
