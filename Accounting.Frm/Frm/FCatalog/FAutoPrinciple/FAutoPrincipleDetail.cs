﻿using System;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Accounting.TextMessage;

namespace Accounting
{
    public partial class FAutoPrincipleDetail : DialogForm //UserControl
    {
        #region khai báo
        private readonly IAutoPrincipleService _IAutoPrincipleService;
        private readonly IAccountService _IAccountService;
        private readonly ITypeService _ITypeService;
        AutoPrinciple _Select = new AutoPrinciple();
        bool Them = true;
        public static bool isClose = true;
        #endregion

        #region khởi tạo
        public FAutoPrincipleDetail()
        {//Thêm
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            //Khai báo các webservices
            _IAutoPrincipleService = IoC.Resolve<IAutoPrincipleService>();
            _IAccountService = IoC.Resolve<IAccountService>();
            _ITypeService = IoC.Resolve<ITypeService>();

            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            this.Text = "Thêm Định Khoản Tự Động";
            chkIsActive.Visible = false;
            InitializeGUI();
            #endregion
        }

        public FAutoPrincipleDetail(AutoPrinciple temp)
        {//Sửa
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _Select = temp;
            Them = false;

            //Khai báo các webservices
            _IAutoPrincipleService = IoC.Resolve<IAutoPrincipleService>();
            _IAccountService = IoC.Resolve<IAccountService>();
            _ITypeService = IoC.Resolve<ITypeService>();

            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion

            #region Fill dữ liệu Obj vào control
            this.Text = "Sửa Định Khoản tự Động";
            ObjandGUI(temp, false);
            chkIsActive.Visible = true;
            #endregion
        }

        private void InitializeGUI()
        {
            //Khởi tạo loại chứng từ
            cbbTypeID.DataSource = _ITypeService.GetAll();
            cbbTypeID.DisplayMember = "TypeName";
            Utils.ConfigGrid(cbbTypeID, ConstDatabase.Type_TableName);
            foreach (var item in cbbTypeID.Rows) { cbbTypeID.SelectedRow = item; break; }
            //Khởi tạo TK nợ, TK có
            //cbbDebitAccount.DataSource = _IAccountService.GetAll().Where(p=>p.IsActive==true).OrderByDescending(p=>p.AccountNumber).Reverse().ToList();
            cbbDebitAccount.DataSource = _IAccountService.GetAllChildrenAccount().OrderBy(p=>p.AccountNumber).ToList();
            cbbDebitAccount.DisplayMember = "AccountNumber";
            Utils.ConfigGrid(cbbDebitAccount, ConstDatabase.Account_TableName);
            //cbbCreditAccount.DataSource = _IAccountService.GetAll().Where(p => p.IsActive == true).OrderByDescending(p => p.AccountNumber).Reverse().ToList();
            cbbCreditAccount.DataSource = _IAccountService.GetAllChildrenAccount().OrderBy(p => p.AccountNumber).ToList();
            cbbCreditAccount.DisplayMember = "AccountNumber";
            Utils.ConfigGrid(cbbCreditAccount, ConstDatabase.Account_TableName);
        }
        #endregion

        #region Button Event
        /// <summary>
        /// Sự kiện nút Lưu lại
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion
            if (!check()) return;

            //Gui to object
            #region Fill dữ liệu control vào obj
            AutoPrinciple temp = Them ? new AutoPrinciple() : _IAutoPrincipleService.Getbykey(_Select.ID);
            temp = ObjandGUI(temp, true);
            #endregion

            #region Thao tác CSDL
            try
            {
                _IAutoPrincipleService.BeginTran();
                if (Them) _IAutoPrincipleService.CreateNew(temp);
                else _IAutoPrincipleService.Update(temp);
                _IAutoPrincipleService.CommitTran();
            }
            catch
            {
                _IAutoPrincipleService.RolbackTran();
            }

            #endregion

            #region xử lý form, kết thúc form
            this.Close();
            isClose = false;
            #endregion
        }

        /// <summary>
        /// Sự kiện nút đóng
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            isClose = true;
            this.Close();
        }
        #endregion

        #region Utils
        /// <summary>
        /// get hoặc set một đối tượng vào giao diện xử lý
        /// </summary>
        /// <param name="input">đối tượng</param>
        /// <param name="isGet">True: get giá trị từ control gán vào đối tượng| False: set giá trị đối tượng vào control</param>
        /// <returns></returns>
        AutoPrinciple ObjandGUI(AutoPrinciple input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();  //Thêm mới

                //loại chứng từ
                Accounting.Core.Domain.Type _type = (Accounting.Core.Domain.Type)Utils.getSelectCbbItem(cbbTypeID);
                if (_type == null) input.TypeID = -1;
                else input.TypeID = _type.ID;
                //Tên định khoản
                input.AutoPrincipleName = txtAutoPrincipleName.Text;
                //TK nợ
                Account _tkno = (Account)Utils.getSelectCbbItem(cbbDebitAccount);
                if (_tkno == null) input.DebitAccount = string.Empty;
                else input.DebitAccount = _tkno.AccountNumber;
                //TK có
                Account _tkco = (Account)Utils.getSelectCbbItem(cbbCreditAccount);
                if (_tkco == null) input.CreditAccount = string.Empty;
                else input.CreditAccount = _tkco.AccountNumber;
                input.Description = txtDescription.Text;
                //Ngừng theo dõi
                input.IsActive = chkIsActive.Checked;
            }
            else
            {
                //loại chứng từ
                foreach (var item in cbbTypeID.Rows)
                {
                    if ((item.ListObject as Accounting.Core.Domain.Type).ID == input.TypeID) cbbTypeID.SelectedRow = item;
                }
                //tên định khoản
                txtAutoPrincipleName.Text = input.AutoPrincipleName;
                //TK nợ
                foreach (var item in cbbDebitAccount.Rows)
                {
                    if ((item.ListObject as Account).AccountNumber == input.DebitAccount) cbbDebitAccount.SelectedRow = item;
                }
                //TK có
                foreach (var item in cbbCreditAccount.Rows)
                {
                    if ((item.ListObject as Account).AccountNumber == input.CreditAccount) cbbCreditAccount.SelectedRow = item;
                }
                //Ngừng theo dõi
                chkIsActive.Checked = input.IsActive;
                txtDescription.Text = input.Description;
            }
            return input;
        }
        bool check()
        {
            bool kq = true;
            //if (cbbDebitAccount.SelectedRow==null || cbbCreditAccount.SelectedRow ==null)
            //    {
            //        MessageBox.Show("Bạn chưa chọn định khoản","Thông Báo",MessageBoxButtons.OK, MessageBoxIcon.Error);
            //        return false;
            //    }
            if (cbbDebitAccount.SelectedRow == null && cbbCreditAccount.SelectedRow != null)
            {
                return true;
            }
            else if (cbbDebitAccount.SelectedRow != null && cbbCreditAccount.SelectedRow == null)
            {
                return true;
            }
            else if (cbbDebitAccount.SelectedRow == null && cbbCreditAccount.SelectedRow == null)
            {
                MessageBox.Show("Bạn chưa chọn định khoản", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return kq;
        }
        bool CheckError()
        {
            bool kq = true;
            //Check Error Chung

            //Check Error Riêng từng Form
            if (string.IsNullOrEmpty(cbbTypeID.Text) || string.IsNullOrEmpty(txtAutoPrincipleName.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }
            return kq;
        }
        #endregion

        private void FAutoPrincipleDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
