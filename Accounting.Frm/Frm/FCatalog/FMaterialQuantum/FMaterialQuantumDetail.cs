﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using static Accounting.AssemblyInfoTool;

namespace Accounting
{
    public partial class FMaterialQuantumDetail : DialogForm //UserControl
    {
        #region khai báo
        private readonly IMaterialQuantumService _IMaterialQuantumService;
        private readonly IMaterialGoodsService _IMaterialGoodsService;
        private readonly ICostSetService _ICostSetService;
        private readonly IMaterialQuantumDetailService _IMaterialQuantumDetailService;
        MaterialQuantum _Select = new MaterialQuantum();
        List<ObjectQuantum> lstCS = new List<ObjectQuantum>();
        bool Them = true;
        public static bool isClose = true;
        bool first = true;
        #endregion

        #region khởi tạo
        public FMaterialQuantumDetail()
        {//Thêm
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            //Khai báo các webservices
            this.Text = "Thêm mới Định mức nguyên vật liệu";
            _IMaterialQuantumService = IoC.Resolve<IMaterialQuantumService>();
            _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
            _ICostSetService = IoC.Resolve<ICostSetService>();
            _IMaterialQuantumDetailService = IoC.Resolve<IMaterialQuantumDetailService>();
            txtCostSetName.Enabled = false;
            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            LoadDuLieu();
            lstCS = ListCoset((DateTime)dteFromDate.Value, (DateTime)dteToDate.Value).CloneObject();
            cbbCostSetID.DataSource = lstCS.OrderBy(d => d.ObjectCode).ToList();
            #endregion
        }

        public FMaterialQuantumDetail(MaterialQuantum temp)
        {//Sửa
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            this.Text = "Sửa Định mức nguyên vật liệu";
            _Select = temp;
            Them = false;
            cbbCostSetID.Enabled = false;

            //Khai báo các webservices
            _IMaterialQuantumService = IoC.Resolve<IMaterialQuantumService>();
            _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
            _ICostSetService = IoC.Resolve<ICostSetService>();
            _IMaterialQuantumDetailService = IoC.Resolve<IMaterialQuantumDetailService>();
            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            LoadDuLieu();
            #endregion

            #region Fill dữ liệu Obj vào control
            lstCS = ListCoset((DateTime)dteFromDate.Value, (DateTime)dteToDate.Value).CloneObject();
            if (Utils.ListCostSet.Any(x => x.ID == temp.CostSetID))
            {
                var x = Utils.ListCostSet.FirstOrDefault(c => c.ID == temp.CostSetID);
                var model = new ObjectQuantum();
                model.ObjectID = x.ID;
                model.ObjectCode = x.CostSetCode;
                model.ObjectName = x.CostSetName;
                model.ObjectType = "DTTHCP";
                lstCS.Add(model);
            }
            else if (Utils.ListMaterialGoods.Any(x => x.ID == temp.CostSetID))
            {
                var x = Utils.ListMaterialGoods.FirstOrDefault(c => c.ID == temp.CostSetID);
                var model = new ObjectQuantum();
                model.ObjectID = x.ID;
                model.ObjectCode = x.MaterialGoodsCode;
                model.ObjectName = x.MaterialGoodsName;
                model.ObjectType = "VTHH";
                lstCS.Add(model);
            }
            cbbCostSetID.DataSource = lstCS.OrderBy(d => d.ObjectCode).ToList();
            ObjandGUI(temp, false);
            #endregion
        }

        void LoadDuLieu()
        {
            LoadDuLieu(true);
            WaitingFrm.StopWaiting();
        }

        private void LoadDuLieu(bool configGrid)
        {
            #region lay du lieu tu db do ra list
            List<MaterialQuantumDetail> lst = new List<MaterialQuantumDetail>();
            BindingList<MaterialQuantumDetail> bdlMaterialQuantumDetail = new BindingList<MaterialQuantumDetail>(lst);
            #endregion

            #region load
            uGrid.DataSource = bdlMaterialQuantumDetail;//.Where(c=>c.MaterialQuantumID == temp.ID);           
            CreaterColumsStyle(uGrid);
            if (configGrid) ConfigGrid(uGrid);
            #endregion
        }

        private void InitializeGUI()
        {
            //lay ma thanh pham
            cbbCostSetID.DataSource = new List<ObjectQuantum>();
            cbbCostSetID.ValueMember = "ObjectID";
            cbbCostSetID.DisplayMember = "ObjectCode";
            Utils.ConfigGrid(cbbCostSetID, ConstDatabase.ObjectQuantum_TableName);
            //this.ConfigCombo(new List<ObjectQuantum>(), cbbCostSetID, "ObjectCode", "ObjectID");
        }
        #endregion

        #region nghiệp vụ
        #region Button Event
        /// <summary>
        /// Sự kiện nút Lưu lại
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion

            //Gui to object
            #region Fill dữ liệu control vào obj
            MaterialQuantum temp = Them ? new MaterialQuantum() : _IMaterialQuantumService.Getbykey(_Select.ID);
            temp = ObjandGUI(temp, true);
            #endregion
            //if (!Them)
            //{
            //    var lstcheck = Utils.ListMaterialQuantum.Where(n => n.CostSetID == temp.CostSetID && n.ID != temp.ID).ToList();
            //    foreach(var item in lstcheck)
            //    {
            //        if (dteFromDate.DateTime <= item.FromDate && dteToDate.DateTime >= item.ToDate || dteFromDate.DateTime >= item.FromDate && dteToDate.DateTime <= item.ToDate )
            //        {
            //            MSG.Error("Khoảng thời gian này đã tồn tại Định mức NVL");
            //            return;

            //        }
            //        else if (dteFromDate.DateTime <= item.FromDate && dteToDate.DateTime <= item.ToDate || dteFromDate.DateTime >= item.FromDate && dteToDate.DateTime >= item.ToDate)
            //        {
            //            MSG.Error("Khoảng thời gian này đã tồn tại Định mức NVL");
            //            return;
            //        }

            //    }
                
            //}
            //else
            {
                var lstcheck = Utils.ListMaterialQuantum.Where(n => n.CostSetID == temp.CostSetID && n.ID != temp.ID).ToList();
                foreach (var item in lstcheck)
                {
                    if (dteFromDate.DateTime <= item.FromDate && dteToDate.DateTime >= item.ToDate || dteFromDate.DateTime >= item.FromDate && dteToDate.DateTime <= item.ToDate)
                    {
                        MSG.Error("Khoảng thời gian này đã tồn tại Định mức NVL");
                        return;

                    }
                    else if (dteFromDate.DateTime <= item.FromDate && (item.FromDate <= dteToDate.DateTime && dteToDate.DateTime <= item.ToDate))
                    {
                        MSG.Error("Khoảng thời gian này đã tồn tại Định mức NVL");
                        return;
                    }
                    else if ((item.FromDate <= dteFromDate.DateTime && dteFromDate.DateTime <= item.ToDate) && dteToDate.DateTime >= item.ToDate )
                    {
                        MSG.Error("Khoảng thời gian này đã tồn tại Định mức NVL");
                        return;
                    }

                }
            }

            #region Thao tác CSDL
            try
            {
                _IMaterialQuantumService.BeginTran();
                if (Them)
                {
                    if ((DateTime)dteFromDate.Value > (DateTime)dteToDate.Value)
                    {
                        MSG.Error("Ngày bắt đầu phải nhỏ hơn ngày kết thúc!");
                        return;
                    }
                   
                    if (!CheckCode()) return;
                    if (uGrid.Rows.Count < 1)
                    {
                        MSG.Warning(resSystem.MSG_Catalog_FMaterialQuantumDetail);
                        return;
                    }
                    else
                    {
                        temp.IsSecurity = false;
                        _IMaterialQuantumService.CreateNew(temp);
                        //foreach (MaterialQuantumDetail item in temp.MaterialQuantumDetails) _IMaterialQuantumDetailService.CreateNew(item);
                    }
                }
                else
                {
                    if ((DateTime)dteFromDate.Value > (DateTime)dteToDate.Value)
                    {
                        MSG.Error("Ngày bắt đầu phải nhỏ hơn ngày kết thúc!");
                        return;
                    }
                    if (uGrid.Rows.Count < 1)
                    {
                        MSG.Warning(resSystem.MSG_Catalog_FMaterialQuantumDetail);
                        return;
                    }
                    else
                        _IMaterialQuantumService.Update(temp);
                }
                _IMaterialQuantumService.CommitTran();
            }
            catch (Exception ex)
            {
                _IMaterialQuantumService.RolbackTran();
            }
            #endregion
            #region xử lý form, kết thúc form
            DialogResult = DialogResult.OK;
            Close();
            isClose = false;
            #endregion
        }

        /// <summary>
        /// Sự kiện nút đóng
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            isClose = true;
            DialogResult = DialogResult.OK;
            Close();
        }
        #endregion

        #endregion

        #region Utils
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung            
            uGrid.DisplayLayout.Override.AllowAddNew = AllowAddNew.TemplateOnBottom;
            uGrid.DisplayLayout.Override.TemplateAddRowPrompt = resSystem.MSG_System_07;
            uGrid.DisplayLayout.Override.SpecialRowSeparator = SpecialRowSeparator.None;
            //hiển thị 1 band
            uGrid.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            //tắt lọc cột
            uGrid.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.FilterUIType = FilterUIType.Default;
            //tự thay đổi kích thước cột
            uGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            //tắt tiêu đề
            uGrid.DisplayLayout.CaptionVisible = DefaultableBoolean.False;
            //select cả hàng hay ko?
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            //Hiển thị SupportDataErrorInfo
            uGrid.DisplayLayout.Override.SupportDataErrorInfo = SupportDataErrorInfo.CellsOnly;
            uGrid.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            uGrid.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.ExtendFirstCell;
            uGrid.DisplayLayout.UseFixedHeaders = true;
            uGrid.DisplayLayout.Override.AllowMultiCellOperations = Infragistics.Win.UltraWinGrid.AllowMultiCellOperation.All;
        }
        /// <summary>
        /// get hoặc set một đối tượng vào giao diện xử lý
        /// </summary>
        /// <param name="input">đối tượng</param>
        /// <param name="isGet">True: get giá trị từ control gán vào đối tượng| False: set giá trị đối tượng vào control</param>
        /// <returns></returns>
        MaterialQuantum ObjandGUI(MaterialQuantum input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();//them moi id
                ObjectQuantum temp0 = (ObjectQuantum)Utils.getSelectCbbItem(cbbCostSetID);
                if (temp0 == null) input.CostSetID = null;
                else input.CostSetID = temp0.ObjectID;
                input.MaterialQuantumCode = temp0.ObjectCode;
                input.MaterialQuantumName = temp0.ObjectName;
                //input.CostSetID = (Guid?)cbbCostSetID.Value;
                input.CostSetName = txtCostSetName.Text;
                input.FromDate = dteFromDate.DateTime;
                input.ToDate = dteToDate.DateTime;
                input.IsActive = true;

                #region MaterialQuantumDetail
                input.MaterialQuantumDetails.Clear();
                List<MaterialQuantumDetail> uGridQuantumDetail = ((BindingList<MaterialQuantumDetail>)uGrid.DataSource).ToList();
                foreach (MaterialQuantumDetail item in uGridQuantumDetail)
                {
                    //if (item.ID == Guid.Empty) item.ID = Guid.NewGuid();
                    item.MaterialQuantumID = input.ID;
                    input.MaterialQuantumDetails.Add(item);
                }
                //input.MaterialQuantumDetails = uGridQuantumDetail;
                #endregion
            }
            else
            {
                foreach (var item in cbbCostSetID.Rows)
                {
                    if ((item.ListObject as ObjectQuantum).ObjectID == input.CostSetID) cbbCostSetID.SelectedRow = item;
                }
                txtCostSetName.Text = input.CostSetName;
                dteFromDate.DateTime = DateTime.Parse(input.FromDate.ToString());
                dteToDate.DateTime = DateTime.Parse(input.ToDate.ToString());
                chkIsActive.Checked = input.IsActive;
                var lst = _IMaterialQuantumDetailService.GetAll_ByMaterialQuantumID(input.ID);
                _IMaterialQuantumDetailService.UnbindSession(lst);
                lst = _IMaterialQuantumDetailService.GetAll_ByMaterialQuantumID(input.ID);
                BindingList<MaterialQuantumDetail> bdMaterialQuantumDetail = new BindingList<MaterialQuantumDetail>(lst);//thành duy đã sửa chỗ này
                uGrid.DataSource = bdMaterialQuantumDetail;
                ConfigGrid(uGrid);
            }
            return input;
        }

        /// <summary>
        /// set giá trị DetailType từ thuộc tính của đối tượng xuống các control checkbox DetailType
        /// </summary>

        bool CheckError()
        {
            bool kq = true;
            if (string.IsNullOrEmpty(cbbCostSetID.Text) || dteFromDate.Value == null || dteToDate.Value == null)
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }
            if(dteFromDate.DateTime > dteToDate.DateTime)
            {
                MSG.Error("Từ ngày không được lớn hơn đến ngày");
                return false;
            }
            return kq;
        }

        bool CheckCode()
        {
            bool kq = true;
            //check trùng code
            List<string> lst = _IMaterialQuantumService.GetMaterialQuantumCode();//thành duy đã sửa chỗ này
            //foreach (var x in lst)
            //{
            //    if (txtMaterialQuantumName.Text.Equals(x))
            //    {
            //        MSG.Warning(string.Format(resSystem.MSG_Catalog, "Mã", "Định mức nguyên vật liệu"));
            //        return false;
            //    }
            //}

            return kq;
        }

        #endregion

        void CreaterColumsStyle(Infragistics.Win.UltraWinGrid.UltraGrid uGrid)
        {
            #region grid dong
            Utils.ConfigGrid(uGrid, TextMessage.ConstDatabase.MaterialQuantumDetail_TableName, new List<TemplateColumn>(), 0);
            Infragistics.Win.UltraWinGrid.UltraGridBand band = uGrid.DisplayLayout.Bands[0];
            foreach (var item in band.Columns)
            {
                //ma vat tu hang hoa
                if (item.Key.Equals("MaterialGoodsID"))
                {
                    this.ConfigCbbMaterialGoodsToGrid(uGrid, item.Key, Utils.ListMaterialGoods, "ID", "MaterialGoodsCode", ConstDatabase.MaterialGoodsCustom_TableName);
                }
                //mo ta 
                else if (item.Key.Equals("Description"))
                {
                    item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Edit;
                }
                //don vi
                else if (item.Key.Equals("Unit"))
                {
                    item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Edit;
                }
                //ti le chuyen doi
                else if (item.Key.Equals("UnitPrice"))
                {
                    item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Edit;
                    item.FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                }
                //don vi chuyen doi
                else if (item.Key.Equals("Amount"))
                {
                    item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Edit;
                    item.FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                    item.CellActivation = Activation.NoEdit;
                }
                //so luong
                else if (item.Key.Equals("Quantity"))
                {
                    item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Edit;
                    item.FormatNumberic(ConstDatabase.Format_Quantity);
                }
                //so luong chuyen doi
                //else if (item.Key.Equals("ConvertQuantity"))
                //{
                //    item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Edit;
                //    item.MaskInput = "-nn,nnn,nnn,nnn.nn";
                //}
                //doi tuong tap hop chi phi
                //else if (item.Key.Equals("CostSetID"))
                //{
                //    item.Editor = GetEmbeddableEditorBase(Controls, _ICostSetService.GetAll(),
                //                                                               "ID", "CostSetCode",
                //                                                               ConstDatabase.CostSet_TableName);
                //}
            }
            #endregion
        }
        private static EmbeddableEditorBase GetEmbeddableEditorBase<T>(Control.ControlCollection _this, List<T> inputItem, string valueMember, string displayMember, string nameTable)
        {

            Infragistics.Win.UltraWinGrid.UltraCombo cbb = new Infragistics.Win.UltraWinGrid.UltraCombo
            {
                Name = "dropDown1",
                Visible = false,
                DataSource = inputItem,
                ValueMember = valueMember,
                DisplayMember = displayMember
            };
            _this.Add(cbb);
            Utils.ConfigGrid(cbb, nameTable);
            EmbeddableEditorBase editor = new EditorWithCombo(new DefaultEditorOwner(new DefaultEditorOwnerSettings { ValueList = cbb }));
            return editor;
        }
        #region sự kiện khi thay đổi dữ liệu trên Grid con
        private void uGrid_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            if (e.Cell.Column.Key.Equals("MaterialGoodsID"))
            {
                if (!e.Cell.Row.Cells["MaterialGoodsID"].IsFilterRowCell)
                {
                    //Infragistics.Win.UltraWinGrid.UltraCombo test = (Infragistics.Win.UltraWinGrid.UltraCombo)Controls["dropDown1"];
                    //MaterialGoods select = (MaterialGoods)(test.SelectedRow == null ? test.SelectedRow : test.SelectedRow.ListObject);
                    //materialgoods select = utils.listmaterialgoods.firstordefault(x => x.id == (guid)e.cell.row.cells["description"].value);
                    var select = _IMaterialGoodsService.Getbykey(((Guid)e.Cell.Row.Cells["MaterialGoodsID"].Value)); /*edit*/
                    e.Cell.Row.Cells["Description"].Value = select.MaterialGoodsName;
                    e.Cell.Row.Cells["Unit"].Value = select.Unit;
                    e.Cell.Row.Cells["ConvertRate"].Value = select.ConvertRate;
                    e.Cell.Row.Cells["Quantity"].Value = select.Quantity;
                    e.Cell.Row.Cells["ConvertUnit"].Value = select.ConvertUnit;
                }
            }
            if (e.Cell.Column.Key.Equals("UnitPrice") || e.Cell.Column.Key.Equals("Quantity"))
            {
                UltraGridRow row = uGrid.ActiveRow;
                if (!e.Cell.Row.Cells["Quantity"].IsFilterRowCell && !e.Cell.Row.Cells["UnitPrice"].IsFilterRowCell)
                {
                    row.Cells["Amount"].Value = (decimal)row.Cells["UnitPrice"].Value * (decimal)row.Cells["Quantity"].Value;
                }
            }
        }
        #endregion
        private void uGrid_Error(object sender, Infragistics.Win.UltraWinGrid.ErrorEventArgs e)
        {
            UltraGridCell cell = uGrid.ActiveCell;
            if (uGrid.ActiveCell == null) return;
            if (cell.Column.Key.Equals("MaterialGoodsID"))
            {
                int count = Utils.ListMaterialGoods.Count(x => x.MaterialGoodsCode == cell.Text);
                if (count == 0)
                {
                    MSG.Error("Dữ liệu không có trong danh mục");
                }
            }
            e.Cancel = true;
        }
        #region cbb

        private void cbbCostSetID_ValueChanged(object sender, EventArgs e)
        {
            if (cbbCostSetID.Value.IsNullOrEmpty()) return;
            if (!Guid.TryParse(cbbCostSetID.Value.ToString(), out Guid guid)) return;
            if (((List<ObjectQuantum>)cbbCostSetID.DataSource).Any(x => x.ObjectID == (Guid)cbbCostSetID.Value))
            {
                var model = ((List<ObjectQuantum>)cbbCostSetID.DataSource).FirstOrDefault(x => x.ObjectID == (Guid)cbbCostSetID.Value);
                txtCostSetName.Text = model.ObjectName;

                if (Utils.ListMaterialGoods.Any(x => x.ID == model.ObjectID))
                {
                    uGrid.DataSource = new BindingList<MaterialQuantumDetail>();
                    foreach (var x in Utils.ListMaterialGoods.FirstOrDefault(x => x.ID == model.ObjectID).MaterialGoodsAssemblys)
                    {
                        uGrid.AddNewRow4Grid();
                        var row = uGrid.Rows[uGrid.Rows.Count - 1];
                        row.Cells["MaterialGoodsID"].Value = x.MaterialAssemblyID;
                        row.Cells["Description"].Value = x.MaterialAssemblyDescription;
                        row.Cells["Unit"].Value = x.Unit;
                        row.Cells["Quantity"].Value = x.Quantity;
                        row.Cells["ConvertUnit"].Value = x.UnitConvert;
                        row.Cells["ConvertRate"].Value = x.ConvertRate;
                        row.Cells["UnitPrice"].Value = x.UnitPrice;
                        row.Cells["ConvertQuantity"].Value = x.QuantityConvert;
                        row.Cells["Amount"].Value = x.UnitPrice * (x.Quantity ?? 0);
                        row.UpdateData();
                        uGrid.UpdateData();
                    }
                }
            }

        }
        private void cbbMaterialGoodsID_TextChanged(object sender, EventArgs e)
        {
            if (cbbCostSetID.Value == null)
            {
                txtCostSetName.Value = null;
            }
        }
        #endregion

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            UltraGridCell cell = uGrid.ActiveCell;
            if (uGrid.ActiveCell != null)
            {
                UltraGridRow row = null;
                if (uGrid.ActiveCell != null || uGrid.ActiveRow != null)
                {
                    row = uGrid.ActiveCell != null ? uGrid.ActiveCell.Row : uGrid.ActiveRow;
                }
                else
                {
                    if (uGrid.Rows.Count > 0) row = uGrid.Rows[uGrid.Rows.Count - 1];
                }
                if (row != null) row.Delete(false);
                uGrid.Update();
            }
        }

        private void tsmAdd_Click(object sender, EventArgs e)
        {
            uGrid.AddNewRow4Grid();
        }
        #region check DTTHCP đã tính kỳ giá thành hiện tại chưa
        List<ObjectQuantum> ListCoset(DateTime fromDate, DateTime toDate)
        {
            var lst = new List<ObjectQuantum>();
            var listcpp = Utils.ListMaterialQuantum.Where(x => !(x.FromDate > toDate || x.ToDate < fromDate)).ToList();
            foreach (var x in Utils.ListCostSet.ToList().Where(x => x.CostSetType == 1).ToList())
            {
                var model = new ObjectQuantum();
                model.ObjectID = x.ID;
                model.ObjectCode = x.CostSetCode;
                model.ObjectName = x.CostSetName;
                model.ObjectType = "DTTHCP";
                lst.Add(model);
            }
            foreach (var x in Utils.ListMaterialGoods.Where(x => x.MaterialGoodsType == 3 || x.MaterialGoodsType == 1).ToList())
            {
                var model = new ObjectQuantum();
                model.ObjectID = x.ID;
                model.ObjectCode = x.MaterialGoodsCode;
                model.ObjectName = x.MaterialGoodsName;
                model.ObjectType = "VTHH";
                lst.Add(model);
            }
            //for (int i = 0; i < lst.Count; i++)
            //{
            //    if (listcpp.Any(d => d.CostSetID == lst[i].ObjectID))
            //    {
            //        lst.RemoveAt(i);
            //        i--;
            //    }
            //}
            return lst;
        }

        private void dteToDate_ValueChanged(object sender, EventArgs e)
        {
            if (dteFromDate.Value != null && dteToDate.Value != null)
            {
                List<ObjectQuantum> list1 = ListCoset((DateTime)dteFromDate.Value, (DateTime)dteToDate.Value);
                if (!Them)
                {
                    if (Utils.ListCostSet.Any(x => x.ID == _Select.CostSetID))
                    {
                        var x = Utils.ListCostSet.FirstOrDefault(c => c.ID == _Select.CostSetID);
                        var model = new ObjectQuantum();
                        model.ObjectID = x.ID;
                        model.ObjectCode = x.CostSetCode;
                        model.ObjectName = x.CostSetName;
                        model.ObjectType = "DTTHCP";
                        list1.Add(model);
                    }
                    else if (Utils.ListMaterialGoods.Any(x => x.ID == _Select.CostSetID))
                    {
                        var x = Utils.ListMaterialGoods.FirstOrDefault(c => c.ID == _Select.CostSetID);
                        var model = new ObjectQuantum();
                        model.ObjectID = x.ID;
                        model.ObjectCode = x.MaterialGoodsCode;
                        model.ObjectName = x.MaterialGoodsName;
                        model.ObjectType = "VTHH";
                        list1.Add(model);
                    }
                }
                cbbCostSetID.DataSource = list1.OrderBy(d => d.ObjectCode).ToList();
            }
        }

        private void dteFromDate_ValueChanged(object sender, EventArgs e)
        {
            if (dteFromDate.Value != null && dteToDate.Value != null)
            {
                List<ObjectQuantum> list1 = ListCoset((DateTime)dteFromDate.Value, (DateTime)dteToDate.Value);
                if (!Them)
                {
                    if (Utils.ListCostSet.Any(x => x.ID == _Select.CostSetID))
                    {
                        var x = Utils.ListCostSet.FirstOrDefault(c => c.ID == _Select.CostSetID);
                        var model = new ObjectQuantum();
                        model.ObjectID = x.ID;
                        model.ObjectCode = x.CostSetCode;
                        model.ObjectName = x.CostSetName;
                        model.ObjectType = "DTTHCP";
                        list1.Add(model);
                    }
                    else if (Utils.ListMaterialGoods.Any(x => x.ID == _Select.CostSetID))
                    {
                        var x = Utils.ListMaterialGoods.FirstOrDefault(c => c.ID == _Select.CostSetID);
                        var model = new ObjectQuantum();
                        model.ObjectID = x.ID;
                        model.ObjectCode = x.MaterialGoodsCode;
                        model.ObjectName = x.MaterialGoodsName;
                        model.ObjectType = "VTHH";
                        list1.Add(model);
                    }
                }
                cbbCostSetID.DataSource = list1.OrderBy(d => d.ObjectCode).ToList();
            }
        }

        #endregion

        private void FMaterialQuantumDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
