﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using Accounting.TextMessage;
using System.Linq;

namespace Accounting
{
    public partial class FMaterialQuantum : CatalogBase //UserControl
    {
        #region khai báo
        private readonly IMaterialQuantumService _IMaterialQuantumService;
        #endregion

        #region khởi tạo
        public FMaterialQuantum()
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _IMaterialQuantumService = IoC.Resolve<IMaterialQuantumService>();
            this.uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            #endregion

            #region Tạo dữ liệu ban đầu
            LoadDuLieu();
            if (uGrid.Rows.Count > 0)
            {
                uGrid.Rows[0].Selected = true;
            }
            #endregion
            ToolTip ToolTip1 = new ToolTip();
            ToolTip1.SetToolTip(btnAdd, "Thêm (Ctrl + N)");
            ToolTip ToolTip2 = new ToolTip();
            ToolTip2.SetToolTip(btnEdit, "Xem (Ctrl + E)");
            ToolTip ToolTip3 = new ToolTip();
            ToolTip3.SetToolTip(btnDelete, "Xóa (Ctrl + D)");
            if (uGrid.Rows.Count > 0) uGrid.Rows[0].Selected = true;
        }

        private void LoadDuLieu()
        {
            WaitingFrm.StartWaiting();
            #region Lấy dữ liệu từ CSDL
            //List<MaterialQuantum> list = _IMaterialQuantumService.GetAll().OrderByDescending(c=>c.MaterialQuantumCode).Reverse().ToList();
            List<MaterialQuantum> list = _IMaterialQuantumService.GetOrderBy_MaterialQuantumCode();//thành duy đã sửa chỗ này
            _IMaterialQuantumService.UnbindSession(list);
            list = _IMaterialQuantumService.GetOrderBy_MaterialQuantumCode();
            #endregion

            #region Xử lý dữ liệu
            //thiết lập tính chất

            #endregion

            #region hiển thị và xử lý hiển thị            
            uGrid.DataSource = list.OrderByDescending(n => n.FromDate).ThenByDescending(n => n.ToDate).ToList();
            ConfigGrid(uGrid);
            #endregion
            WaitingFrm.StopWaiting();
        }
        #endregion

        #region Nghiệp vụ
        #region ContentMenu
        private void tsmAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void tsmEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            ResetFunction();
        }
        protected override void ResetFunction()
        {
            LoadDuLieu();
        }
        #endregion

        #region Button
        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }
        #endregion

        private void uGrid_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                EditFunction();
            }
        }

        /// <summary>
        /// Nghiệp vụ Add
        /// </summary>
        protected override void AddFunction()
        {
            var f = new FMaterialQuantumDetail();
            f.FormClosed += new FormClosedEventHandler(FMaterialQuantumDetail_FormClosed);
            //f.ShowDialog(this);
            //HUYPD Edit Show Form
            f.ShowDialogHD(this);
            //if (!FMaterialQuantumDetail.isClose) { Utils.ClearCacheByType<MaterialQuantum>(); LoadDuLieu(); }
        }

        private void FMaterialQuantumDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            Utils.ClearCacheByType<MaterialQuantum>();
            //HUYPD Edit Show Form
            LoadDuLieu();
            List<MaterialQuantum> list = _IMaterialQuantumService.GetOrderBy_MaterialQuantumCode();//thành duy đã sửa chỗ này
            _IMaterialQuantumService.UnbindSession(list);
            list = _IMaterialQuantumService.GetOrderBy_MaterialQuantumCode();
            uGrid.DataSource = list.OrderByDescending(n => n.FromDate).ThenByDescending(n => n.ToDate).ToList();
        }

        /// <summary>
        /// Nghiệp vụ Edit
        /// </summary>
        protected override void EditFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                WaitingFrm.StartWaiting();
                MaterialQuantum temp = uGrid.Selected.Rows[0].ListObject as MaterialQuantum;
                var f = new FMaterialQuantumDetail(temp);
                f.FormClosed += new FormClosedEventHandler(FMaterialQuantumDetail_FormClosed);
                //f.ShowDialog(this);
                //HUYPD Edit Show Form
                f.ShowDialogHD(this);              
                //if (!FMaterialQuantumDetail.isClose) { Utils.ClearCacheByType<MaterialQuantum>(); LoadDuLieu(); } 
                WaitingFrm.StopWaiting();
            }
            else
                Accounting.TextMessage.MSG.Error(string.Format(Accounting.TextMessage.resSystem.MSG_Catalog3,"một Định mức nguyên vật liệu"));
        }

        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        protected override void DeleteFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                MaterialQuantum temp = uGrid.Selected.Rows[0].ListObject as MaterialQuantum;
                if (MSG.Question(string.Format(resSystem.MSG_System_05, "Định mức nguyên vật liệu")) == System.Windows.Forms.DialogResult.Yes)
                {
                    if (Utils.IRSInwardOutwardDetailService.Query.Any(n => n.MaterialQuantumID == temp.ID))
                    {
                        MSG.Warning("Không thể xóa dữ liệu vì đã phát sinh dữ liệu liên quan");
                    }
                    else
                    {
                        if (temp.IsSecurity == true)
                        {
                            MSG.Warning(string.Format(resSystem.MSG_Catalog7, "Định mức nguyên vật liệu"));
                        }
                        else
                        {
                            _IMaterialQuantumService.BeginTran();
                            _IMaterialQuantumService.Delete(temp);
                            _IMaterialQuantumService.CommitTran();
                            Utils.ClearCacheByType<MaterialQuantum>();
                            LoadDuLieu();
                        }
                    }
                }
            }
            else
                MSG.Error(string.Format(resSystem.MSG_Catalog2,"một Định mức nguyên vật liệu"));
            
        }
        #endregion

        #region Utils
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, TextMessage.ConstDatabase.MaterialQuantum_TableName);

            uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.AlignWithDataRows;
            this.uGrid.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.uGrid.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGrid.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGrid.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGrid.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
        }
        #endregion

        private void FMaterialQuantum_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGrid.ResetText();
            uGrid.ResetUpdateMode();
            uGrid.ResetExitEditModeOnLeave();
            uGrid.ResetRowUpdateCancelAction();
            uGrid.DataSource = null;
            uGrid.Layouts.Clear();
            uGrid.ResetLayouts();
            uGrid.ResetDisplayLayout();
            uGrid.Refresh();
            uGrid.ClearUndoHistory();
            uGrid.ClearXsdConstraints();
        }

        private void FMaterialQuantum_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
