﻿namespace Accounting
{
    partial class FMaterialQuantumDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbCostSetID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.lblToDate = new Infragistics.Win.Misc.UltraLabel();
            this.dteToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.lblFromDate = new Infragistics.Win.Misc.UltraLabel();
            this.dteFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.txtCostSetName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblMaterialGoodsName = new Infragistics.Win.Misc.UltraLabel();
            this.lblMaterialGoodsID = new Infragistics.Win.Misc.UltraLabel();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.chkIsActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCostSetID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCostSetName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            appearance1.FontData.BoldAsString = "True";
            this.ultraGroupBox1.Appearance = appearance1;
            this.ultraGroupBox1.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox1.Controls.Add(this.cbbCostSetID);
            this.ultraGroupBox1.Controls.Add(this.lblToDate);
            this.ultraGroupBox1.Controls.Add(this.dteToDate);
            this.ultraGroupBox1.Controls.Add(this.lblFromDate);
            this.ultraGroupBox1.Controls.Add(this.dteFromDate);
            this.ultraGroupBox1.Controls.Add(this.txtCostSetName);
            this.ultraGroupBox1.Controls.Add(this.lblMaterialGoodsName);
            this.ultraGroupBox1.Controls.Add(this.lblMaterialGoodsID);
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(3, 3);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(788, 114);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbCostSetID
            // 
            this.cbbCostSetID.AllowDrop = true;
            this.cbbCostSetID.AllowNull = Infragistics.Win.DefaultableBoolean.True;
            this.cbbCostSetID.AlwaysInEditMode = true;
            this.cbbCostSetID.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.SuggestAppend;
            this.cbbCostSetID.AutoSize = false;
            this.cbbCostSetID.DisplayLayout.DefaultSelectedForeColor = System.Drawing.Color.Black;
            this.cbbCostSetID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbCostSetID.Location = new System.Drawing.Point(106, 32);
            this.cbbCostSetID.Name = "cbbCostSetID";
            this.cbbCostSetID.Size = new System.Drawing.Size(250, 22);
            this.cbbCostSetID.TabIndex = 208;
            this.cbbCostSetID.ValueChanged += new System.EventHandler(this.cbbCostSetID_ValueChanged);
            this.cbbCostSetID.TextChanged += new System.EventHandler(this.cbbMaterialGoodsID_TextChanged);
            // 
            // lblToDate
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.TextVAlignAsString = "Middle";
            this.lblToDate.Appearance = appearance2;
            this.lblToDate.Location = new System.Drawing.Point(410, 68);
            this.lblToDate.Name = "lblToDate";
            this.lblToDate.Size = new System.Drawing.Size(86, 22);
            this.lblToDate.TabIndex = 25;
            this.lblToDate.Text = "Đến ngày (*)";
            // 
            // dteToDate
            // 
            this.dteToDate.AutoSize = false;
            this.dteToDate.FormatString = "dd/MM/yyyy";
            this.dteToDate.Location = new System.Drawing.Point(502, 68);
            this.dteToDate.Name = "dteToDate";
            this.dteToDate.Size = new System.Drawing.Size(98, 22);
            this.dteToDate.TabIndex = 10;
            this.dteToDate.ValueChanged += new System.EventHandler(this.dteToDate_ValueChanged);
            // 
            // lblFromDate
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextVAlignAsString = "Middle";
            this.lblFromDate.Appearance = appearance3;
            this.lblFromDate.Location = new System.Drawing.Point(8, 68);
            this.lblFromDate.Name = "lblFromDate";
            this.lblFromDate.Size = new System.Drawing.Size(91, 22);
            this.lblFromDate.TabIndex = 23;
            this.lblFromDate.Text = "Từ ngày (*)";
            // 
            // dteFromDate
            // 
            this.dteFromDate.AutoSize = false;
            this.dteFromDate.FormatString = "dd/MM/yyyy";
            this.dteFromDate.Location = new System.Drawing.Point(106, 68);
            this.dteFromDate.Name = "dteFromDate";
            this.dteFromDate.Size = new System.Drawing.Size(98, 22);
            this.dteFromDate.TabIndex = 9;
            this.dteFromDate.ValueChanged += new System.EventHandler(this.dteFromDate_ValueChanged);
            // 
            // txtCostSetName
            // 
            this.txtCostSetName.AutoSize = false;
            this.txtCostSetName.Location = new System.Drawing.Point(502, 32);
            this.txtCostSetName.Name = "txtCostSetName";
            this.txtCostSetName.Size = new System.Drawing.Size(239, 22);
            this.txtCostSetName.TabIndex = 8;
            // 
            // lblMaterialGoodsName
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.lblMaterialGoodsName.Appearance = appearance4;
            this.lblMaterialGoodsName.Location = new System.Drawing.Point(410, 32);
            this.lblMaterialGoodsName.Name = "lblMaterialGoodsName";
            this.lblMaterialGoodsName.Size = new System.Drawing.Size(118, 22);
            this.lblMaterialGoodsName.TabIndex = 20;
            this.lblMaterialGoodsName.Text = "Tên đối tượng";
            // 
            // lblMaterialGoodsID
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.TextVAlignAsString = "Middle";
            this.lblMaterialGoodsID.Appearance = appearance5;
            this.lblMaterialGoodsID.Location = new System.Drawing.Point(8, 34);
            this.lblMaterialGoodsID.Name = "lblMaterialGoodsID";
            this.lblMaterialGoodsID.Size = new System.Drawing.Size(91, 22);
            this.lblMaterialGoodsID.TabIndex = 14;
            this.lblMaterialGoodsID.Text = "Mã đối tượng(*)";
            // 
            // btnSave
            // 
            appearance6.Image = global::Accounting.Properties.Resources.ubtnSave1;
            this.btnSave.Appearance = appearance6;
            this.btnSave.Location = new System.Drawing.Point(625, 432);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 12;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            appearance7.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance7;
            this.btnClose.Location = new System.Drawing.Point(709, 432);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 13;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox2.Controls.Add(this.uGrid);
            appearance8.FontData.BoldAsString = "True";
            this.ultraGroupBox2.HeaderAppearance = appearance8;
            this.ultraGroupBox2.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox2.Location = new System.Drawing.Point(3, 114);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(788, 312);
            this.ultraGroupBox2.TabIndex = 26;
            this.ultraGroupBox2.Text = "Nguyên vật liệu định mức";
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // uGrid
            // 
            this.uGrid.ContextMenuStrip = this.contextMenuStrip1;
            this.uGrid.Location = new System.Drawing.Point(6, 19);
            this.uGrid.Name = "uGrid";
            this.uGrid.Size = new System.Drawing.Size(776, 260);
            this.uGrid.TabIndex = 0;
            this.uGrid.Text = "ultraGrid1";
            this.uGrid.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid_AfterCellUpdate);
            this.uGrid.Error += new Infragistics.Win.UltraWinGrid.ErrorEventHandler(this.uGrid_Error);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmAdd,
            this.tsmDelete});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(205, 48);
            // 
            // tsmAdd
            // 
            this.tsmAdd.Name = "tsmAdd";
            this.tsmAdd.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.tsmAdd.Size = new System.Drawing.Size(204, 22);
            this.tsmAdd.Text = "Thêm một dòng";
            this.tsmAdd.Click += new System.EventHandler(this.tsmAdd_Click);
            // 
            // tsmDelete
            // 
            this.tsmDelete.Name = "tsmDelete";
            this.tsmDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.tsmDelete.Size = new System.Drawing.Size(204, 22);
            this.tsmDelete.Text = "Xóa một dòng";
            this.tsmDelete.Click += new System.EventHandler(this.tsmDelete_Click);
            // 
            // chkIsActive
            // 
            appearance9.TextVAlignAsString = "Middle";
            this.chkIsActive.Appearance = appearance9;
            this.chkIsActive.BackColor = System.Drawing.Color.Transparent;
            this.chkIsActive.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsActive.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkIsActive.Checked = true;
            this.chkIsActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIsActive.Location = new System.Drawing.Point(9, 442);
            this.chkIsActive.Name = "chkIsActive";
            this.chkIsActive.Size = new System.Drawing.Size(116, 22);
            this.chkIsActive.TabIndex = 11;
            this.chkIsActive.Text = "Hoạt động";
            this.chkIsActive.Visible = false;
            // 
            // FMaterialQuantumDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(796, 474);
            this.Controls.Add(this.chkIsActive);
            this.Controls.Add(this.ultraGroupBox2);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.ultraGroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FMaterialQuantumDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FMaterialQuantumDetail_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbCostSetID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCostSetName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraLabel lblMaterialGoodsID;
        private Infragistics.Win.Misc.UltraLabel lblToDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteToDate;
        private Infragistics.Win.Misc.UltraLabel lblFromDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteFromDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCostSetName;
        private Infragistics.Win.Misc.UltraLabel lblMaterialGoodsName;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsActive;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripMenuItem tsmDelete;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbCostSetID;
    }
}
