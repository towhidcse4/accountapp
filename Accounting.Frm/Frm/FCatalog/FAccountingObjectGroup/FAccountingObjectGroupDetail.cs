﻿using System;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Accounting.TextMessage;
using System.Collections.Generic;
using System.Globalization;

namespace Accounting
{
    public partial class FAccountingObjectGroupDetail : DialogForm //UserControl
    {
        #region khai báo
        private IAccountingObjectGroupService _IAccountingObjectGroupService;
        AccountingObjectGroup _Select = new AccountingObjectGroup();
        bool Them = true;
        public static bool isClose = true;
        #endregion

        #region khởi tạo
        public FAccountingObjectGroupDetail()
        {//Thêm
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion
            Add();
        }

        private void Add()
        {
            #region Thiết lập ban đầu cho Form
            //Khai báo các webservices
            _IAccountingObjectGroupService = IoC.Resolve<IAccountingObjectGroupService>();
            chkIsActive.Visible = false;
            this.Text = "Thêm mới nhóm khách hàng, nhà cung cấp";
            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion
        }

        private void Edit(AccountingObjectGroup temp)
        {
            #region Thiết lập ban đầu cho Form
            _Select = temp;
            Them = false;
            txtAccountingObjectGroupCode.Enabled = false;
            this.Text = "Sửa nhóm khách hàng, nhà cung cấp";
            //Khai báo các webservices
            _IAccountingObjectGroupService = IoC.Resolve<IAccountingObjectGroupService>();

            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion

            #region Fill dữ liệu Obj vào control
            ObjandGUI(temp, false);
            #endregion
        }

        public FAccountingObjectGroupDetail(AccountingObjectGroup temp)
        {//Sửa
            
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion
            Edit(temp);
        }

        public FAccountingObjectGroupDetail(AccountingObjectGroup temp, bool Them)
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            if (Them)
            {
                Add();
                foreach (var item in cbbParentID.Rows)
                {
                    if ((item.ListObject as AccountingObjectGroup).ID == temp.ID) cbbParentID.SelectedRow = item;
                }
            }
            else
            {
                Edit(temp);
            }
        }

        private void InitializeGUI()
        {
            //Khởi tạo loại chứng từ
            //cbbParentID.DataSource = _IAccountingObjectGroupService.GetAll().OrderByDescending(c=>c.AccountingObjectGroupName).Reverse().ToList();
            cbbParentID.DataSource = _IAccountingObjectGroupService.GetAll_OrderByAccountingObjectGroupName();
            cbbParentID.DisplayMember = "AccountingObjectGroupName";
            Utils.ConfigGrid(cbbParentID, ConstDatabase.AccountingObjectGroup_TableName);
        }
        #endregion

        #region Button Event
        /// <summary>
        /// Sự kiện nút Lưu lại
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion

            #region Thao tác CSDL
            try
            {
                _IAccountingObjectGroupService.BeginTran();

                if (Them)//them moi
                {
                    AccountingObjectGroup temp = Them ? new AccountingObjectGroup() : _IAccountingObjectGroupService.Getbykey(_Select.ID);
                    temp = ObjandGUI(temp, true);

                    //set order fixcode
                    AccountingObjectGroup temp0 = (AccountingObjectGroup)Utils.getSelectCbbItem(cbbParentID);
                    //List<string> lstOrderFixCodeChild = _IAccountingObjectGroupService.Query.Where(a => a.ParentID == temp.ParentID)
                    //.Select(a => a.OrderFixCode).ToList();
                    List<string> lstOrderFixCodeChild = _IAccountingObjectGroupService.GetOrderFixCode_ByParentID(temp.ParentID);
                    string orderFixCodeParent = temp0 == null ? "" : temp0.OrderFixCode;
                    temp.OrderFixCode = Utils.GetOrderFixCode(lstOrderFixCodeChild, orderFixCodeParent);
                    temp.IsParentNode = false; // mac dinh cho insert
                    temp.Grade = temp0 == null ? 1 : temp0.Grade + 1; // quy tac lay gia tri cho grade

                    //nếu cha isactive =false thì khi thêm mới con cũng là false

                    if (cbbParentID.Text.Equals(""))
                    {
                        if (!CheckCode()) return;
                        temp.IsActive = true;
                        _IAccountingObjectGroupService.CreateNew(temp);
                    }
                    else
                    {
                        AccountingObjectGroup objParentCombox = (AccountingObjectGroup)Utils.getSelectCbbItem(cbbParentID);
                        //lay cha
                        AccountingObjectGroup Parent = _IAccountingObjectGroupService.Getbykey(objParentCombox.ID);
                        if (Parent.IsActive == false)
                        {
                            temp.IsActive = false;
                        }
                        else
                            temp.IsActive = true;

                        //check loi
                        if (!CheckCode()) return;
                        //temp.IsActive = true;
                        _IAccountingObjectGroupService.CreateNew(temp);
                    }

                    //update lai isparentnode neu co
                    if (temp.ParentID != null)
                    {
                        AccountingObjectGroup parent = _IAccountingObjectGroupService.Getbykey((Guid)temp.ParentID);
                        parent.IsParentNode = true;
                        _IAccountingObjectGroupService.Update(parent);
                    }
                }
                else //cap nhat
                {
                    AccountingObjectGroup temp = _IAccountingObjectGroupService.Getbykey(_Select.ID);
                    //Lưu đối tượng trước khi sửa
                    AccountingObjectGroup tempOriginal = (AccountingObjectGroup)Utils.CloneObject(temp);
                    //Lấy về đối tượng chọn trong combobox
                    AccountingObjectGroup objParentCombox = (AccountingObjectGroup)Utils.getSelectCbbItem(cbbParentID);
                    //Không thay đổi đối tượng cha trong combobox
                    if (((temp.ParentID == null) && (objParentCombox == null)) || ((temp.ParentID != null) && (objParentCombox != null) && (temp.ParentID == objParentCombox.ID)))
                    {
                        temp = ObjandGUI(temp, true);
                        _IAccountingObjectGroupService.Update(temp);
                    }
                    else
                    {
                        //không cho phép chuyển ngược
                        if (objParentCombox != null)
                        {
                            //List<AccountingObjectGroup> lstChildCheck =
                            //    _IAccountingObjectGroupService.Query.Where(p => p.OrderFixCode.StartsWith(tempOriginal.OrderFixCode)).ToList();
                            List<AccountingObjectGroup> lstChildCheck = _IAccountingObjectGroupService.GetStartsWith_OrderFixCode(tempOriginal.OrderFixCode);
                            foreach (var item in lstChildCheck)
                            {
                                if (objParentCombox.ID == item.ID)
                                {
                                    MessageBox.Show("danh mục <<" + temp.AccountingObjectGroupCode + ">> không được phép chọn chi tiết của nó làm danh mục tổng hợp!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    return;
                                }
                            }
                        }
                        //Trường hợp có cha cũ
                        //Kiểm tra cha cũ có còn con ngoài con chuyển đi hay không
                        //Chuyển trạng thái IsParentNode Cha cũ nếu hết con
                        if (temp.ParentID != null)
                        {
                            AccountingObjectGroup oldParent = _IAccountingObjectGroupService.Getbykey((Guid)temp.ParentID);
                            //int checkChildOldParent = _IAccountingObjectGroupService.Query.Count(p => p.ParentID == temp.ParentID);
                            int checkChildOldParent = _IAccountingObjectGroupService.CountByParentID(temp.ParentID);
                            if (checkChildOldParent <= 1)
                            {
                                oldParent.IsParentNode = false;
                            }
                            _IAccountingObjectGroupService.Update(oldParent);
                        }
                        //Trường hợp có cha mới
                        //Chuyển trạng IsParentNode cha mới
                        if (objParentCombox != null)
                        {
                            AccountingObjectGroup newParent = _IAccountingObjectGroupService.Getbykey(objParentCombox.ID);
                            newParent.IsParentNode = true;
                            _IAccountingObjectGroupService.Update(newParent);

                            //List<AccountingObjectGroup> listChildNewParent = _IAccountingObjectGroupService.Query.Where(
                            //    a => a.ParentID == objParentCombox.ID).ToList();
                            List<AccountingObjectGroup> listChildNewParent = _IAccountingObjectGroupService.GetAll_ByParentID(objParentCombox.ID);
                            //Tính lại OrderFixCode cho đối tượng chính
                            List<string> lstOfcChildNewParent = new List<string>();
                            foreach (var item in listChildNewParent)
                            {
                                lstOfcChildNewParent.Add(item.OrderFixCode);
                            }
                            string orderFixCodeParent = newParent.OrderFixCode;
                            temp.OrderFixCode = Utils.GetOrderFixCode(lstOfcChildNewParent, orderFixCodeParent);
                            //Tính lại bậc cho đối tượng chính
                            temp.Grade = newParent.Grade + 1;
                        }
                        //Trường hợp không có cha mới tức là chuyển đối tượng lên bậc 1
                        if (objParentCombox == null)
                        {
                            //Lấy về tất cả đối tượng bậc 1
                            List<int> lstOfcChildGradeNo1 = new List<int>();
                            //List<AccountingObjectGroup> listChildGradeNo1 = _IAccountingObjectGroupService.Query.Where(
                            //    a => a.Grade == 1).ToList();
                            List<AccountingObjectGroup> listChildGradeNo1 = _IAccountingObjectGroupService.GetAll_ByGrade(1);
                            foreach (var item in listChildGradeNo1)
                            {
                                lstOfcChildGradeNo1.Add(Convert.ToInt32(item.OrderFixCode));
                            }
                            //Tính lại OrderFixCode cho đối tượng chính
                            temp.OrderFixCode = (lstOfcChildGradeNo1.Max() + 1).ToString(CultureInfo.InvariantCulture);
                            //Tính lại bậc cho đối tượng chính
                            temp.Grade = 1;
                            temp = ObjandGUI(temp, true);
                            _IAccountingObjectGroupService.Update(temp);
                        }
                        //Xử lý các con của nó 
                        //Lấy về các con
                        //List<AccountingObjectGroup> lstChild =
                        //    _IAccountingObjectGroupService.Query.Where(p => p.OrderFixCode.StartsWith(tempOriginal.OrderFixCode)).ToList();
                        List<AccountingObjectGroup> lstChild = _IAccountingObjectGroupService.GetStartsWith_OrderFixCode(tempOriginal.OrderFixCode);
                        foreach (var item in lstChild)
                        {
                            string tempOldFixCode = tempOriginal.OrderFixCode;
                            item.OrderFixCode = temp.OrderFixCode + item.OrderFixCode.Substring(tempOldFixCode.Length);
                            item.Grade = temp.Grade - tempOriginal.Grade + item.Grade;
                            _IAccountingObjectGroupService.Update(item);
                        }
                        temp = ObjandGUI(temp, true);
                    }
                    //nếu cha ngừng theo dõi thì con cũng ngừng theo dõi
                    //Lấy về danh sách các con
                    //List<AccountingObjectGroup> lstChild2 =
                    //        _IAccountingObjectGroupService.Query.Where(p => p.OrderFixCode.StartsWith(tempOriginal.OrderFixCode) && p.ID != tempOriginal.ID).ToList();
                    List<AccountingObjectGroup> lstChild2 = _IAccountingObjectGroupService.GetStartsWith_OrderFixCode_NotValueByID(tempOriginal.OrderFixCode, tempOriginal.ID);
                    //Chuyen True -> False
                    if (chkIsActive.CheckState == CheckState.Unchecked && CheckError())
                    {
                        temp.IsActive = false;
                        //neu la cha
                        if (lstChild2.Count > 0)
                        {
                            foreach (var itemChild in lstChild2)
                            {
                                itemChild.IsActive = false;
                                _IAccountingObjectGroupService.Update(itemChild);
                            }
                        }
                        //neu ko phai la cha
                        _IAccountingObjectGroupService.Update(temp);
                    }

                    //Chuyen False -> True
                    else if (chkIsActive.CheckState == CheckState.Checked && CheckError())
                    {
                        //Co Cha
                        if (temp.ParentID != null)
                        {
                            AccountingObjectGroup Parent = _IAccountingObjectGroupService.Getbykey(objParentCombox.ID);
                            //Cha la false khong cho chuyen
                            if (Parent.IsActive == false)
                            {
                                MessageBox.Show("Đối tượng con không thể hoạt động khi đối tượng cha đang bị ngừng hoạt đông!");
                                return;
                            }
                            //Cha la True duoc phep chuyen
                            temp.IsActive = true;
                            if (lstChild2.Count > 0)
                            {

                                if (MSG.Question("Bạn có muốn thiết lập cho tất cả các danh mục con của danh mục này sang trạng thái <<Hoạt động>> không?") == System.Windows.Forms.DialogResult.Yes)
                                {
                                    foreach (var item2 in lstChild2)
                                    {
                                        item2.IsActive = true;
                                        _IAccountingObjectGroupService.Update(item2);
                                    }
                                }

                            }
                            _IAccountingObjectGroupService.Update(temp);
                        }
                        //Khong Co cha
                        else
                        {
                            temp.IsActive = true;
                            if (lstChild2.Count > 0)
                            {

                                if (MSG.Question("Bạn có muốn thiết lập cho tất cả các danh mục con của danh mục này sang trạng thái <<Hoạt động>> không?") == System.Windows.Forms.DialogResult.Yes)
                                {
                                    foreach (var item2 in lstChild2)
                                    {
                                        item2.IsActive = true;
                                        _IAccountingObjectGroupService.Update(item2);
                                    }
                                }

                            }

                            _IAccountingObjectGroupService.Update(temp);
                        }
                    }
                }
                _IAccountingObjectGroupService.CommitTran();
            }
            catch
            {
                _IAccountingObjectGroupService.RolbackTran();
            }

            #endregion

            #region xử lý form, kết thúc form
            this.Close();
            isClose = false;
            #endregion
        }

        /// <summary>
        /// Sự kiện nút đóng
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            isClose = true;
            this.Close();
        }
        #endregion

        #region Utils
        /// <summary>
        /// get hoặc set một đối tượng vào giao diện xử lý
        /// </summary>
        /// <param name="input">đối tượng</param>
        /// <param name="isGet">True: get giá trị từ control gán vào đối tượng| False: set giá trị đối tượng vào control</param>
        /// <returns></returns>
        AccountingObjectGroup ObjandGUI(AccountingObjectGroup input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();  //Thêm mới

                //mã, tên, diễn giải
                input.AccountingObjectGroupCode = txtAccountingObjectGroupCode.Text;
                input.AccountingObjectGroupName = txtAccountingObjectGroupName.Text;
                input.Description = txtDescription.Text;
                //thuộc nhóm
                AccountingObjectGroup _tkno = (AccountingObjectGroup)Utils.getSelectCbbItem(cbbParentID);
                if (_tkno == null) input.ParentID = null;
                else input.ParentID = _tkno.ID;
                //Ngừng theo dõi
                input.IsActive = chkIsActive.Checked;
            }
            else
            {
                //mã, tên, diễn giải
                txtAccountingObjectGroupCode.Text = input.AccountingObjectGroupCode;
                txtAccountingObjectGroupName.Text = input.AccountingObjectGroupName;
                txtDescription.Text = input.Description;
                //TK có
                foreach (var item in cbbParentID.Rows)
                {
                    if ((item.ListObject as AccountingObjectGroup).ID == input.ParentID) cbbParentID.SelectedRow = item;
                }
                //Ngừng theo dõi
                chkIsActive.Checked = input.IsActive;
            }
            return input;
        }

        bool CheckError()
        {
            bool kq = true;
            //Check Error Chung

            //Check Error Riêng từng Form
            if (string.IsNullOrEmpty(txtAccountingObjectGroupCode.Text) || string.IsNullOrEmpty(txtAccountingObjectGroupName.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }
            return kq;
        }

        bool CheckCode()
        {
            bool kq = true;
            if (string.IsNullOrEmpty(txtAccountingObjectGroupCode.Text) || string.IsNullOrEmpty(txtAccountingObjectGroupName.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }

            //check trùng code
            //List<string> lst = _IAccountingObjectGroupService.Query.Select(c => c.AccountingObjectGroupCode).ToList();
            List<string> lst = _IAccountingObjectGroupService.GetAccountingObjectGroupCode();
            foreach (var x in lst)
            {
                if (txtAccountingObjectGroupCode.Text.Equals(x))
                {
                    MessageBox.Show("Mã " + x + " đã bị trùng trong danh sách", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }
            return kq;
        }
        #endregion

        private void FAccountingObjectGroupDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
