﻿namespace Accounting
{
    partial class FAccountingObjectGroupDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbParentID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtDescription = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblDescription = new Infragistics.Win.Misc.UltraLabel();
            this.lblParentID = new Infragistics.Win.Misc.UltraLabel();
            this.chkChiTiet = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.cbbChiTiet = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.txtAccountingObjectGroupName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtAccountingObjectGroupCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblAccountingObjectGroupName = new Infragistics.Win.Misc.UltraLabel();
            this.lblAccountingObjectGroupCode = new Infragistics.Win.Misc.UltraLabel();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.chkIsActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbParentID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkChiTiet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbChiTiet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectGroupName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectGroupCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            appearance1.FontData.BoldAsString = "True";
            this.ultraGroupBox1.Appearance = appearance1;
            this.ultraGroupBox1.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox1.Controls.Add(this.cbbParentID);
            this.ultraGroupBox1.Controls.Add(this.txtDescription);
            this.ultraGroupBox1.Controls.Add(this.lblDescription);
            this.ultraGroupBox1.Controls.Add(this.lblParentID);
            this.ultraGroupBox1.Controls.Add(this.chkChiTiet);
            this.ultraGroupBox1.Controls.Add(this.cbbChiTiet);
            this.ultraGroupBox1.Controls.Add(this.txtAccountingObjectGroupName);
            this.ultraGroupBox1.Controls.Add(this.txtAccountingObjectGroupCode);
            this.ultraGroupBox1.Controls.Add(this.lblAccountingObjectGroupName);
            this.ultraGroupBox1.Controls.Add(this.lblAccountingObjectGroupCode);
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(3, 3);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(388, 173);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbParentID
            // 
            this.cbbParentID.AutoSize = false;
            this.cbbParentID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbParentID.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbParentID.Location = new System.Drawing.Point(129, 81);
            this.cbbParentID.Name = "cbbParentID";
            this.cbbParentID.NullText = "<chọn dữ liệu>";
            this.cbbParentID.Size = new System.Drawing.Size(245, 22);
            this.cbbParentID.TabIndex = 3;
            // 
            // txtDescription
            // 
            this.txtDescription.AutoSize = false;
            this.txtDescription.Location = new System.Drawing.Point(129, 108);
            this.txtDescription.MaxLength = 512;
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(245, 58);
            this.txtDescription.TabIndex = 4;
            // 
            // lblDescription
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.TextVAlignAsString = "Middle";
            this.lblDescription.Appearance = appearance2;
            this.lblDescription.Location = new System.Drawing.Point(8, 108);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(68, 22);
            this.lblDescription.TabIndex = 3;
            this.lblDescription.Text = "Mô tả chi tiết";
            // 
            // lblParentID
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextVAlignAsString = "Middle";
            this.lblParentID.Appearance = appearance3;
            this.lblParentID.Location = new System.Drawing.Point(8, 81);
            this.lblParentID.Name = "lblParentID";
            this.lblParentID.Size = new System.Drawing.Size(69, 22);
            this.lblParentID.TabIndex = 2;
            this.lblParentID.Text = "Thuộc nhóm";
            // 
            // chkChiTiet
            // 
            this.chkChiTiet.BackColor = System.Drawing.Color.Transparent;
            this.chkChiTiet.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkChiTiet.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkChiTiet.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkChiTiet.Location = new System.Drawing.Point(9, 275);
            this.chkChiTiet.Name = "chkChiTiet";
            this.chkChiTiet.Size = new System.Drawing.Size(101, 20);
            this.chkChiTiet.TabIndex = 11;
            this.chkChiTiet.Text = "Chi tiết theo";
            // 
            // cbbChiTiet
            // 
            this.cbbChiTiet.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbChiTiet.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbbChiTiet.Location = new System.Drawing.Point(129, 275);
            this.cbbChiTiet.Name = "cbbChiTiet";
            this.cbbChiTiet.Size = new System.Drawing.Size(245, 21);
            this.cbbChiTiet.TabIndex = 10;
            // 
            // txtAccountingObjectGroupName
            // 
            this.txtAccountingObjectGroupName.AutoSize = false;
            this.txtAccountingObjectGroupName.Location = new System.Drawing.Point(129, 54);
            this.txtAccountingObjectGroupName.MaxLength = 512;
            this.txtAccountingObjectGroupName.Name = "txtAccountingObjectGroupName";
            this.txtAccountingObjectGroupName.Size = new System.Drawing.Size(245, 22);
            this.txtAccountingObjectGroupName.TabIndex = 2;
            // 
            // txtAccountingObjectGroupCode
            // 
            this.txtAccountingObjectGroupCode.AutoSize = false;
            this.txtAccountingObjectGroupCode.Location = new System.Drawing.Point(129, 27);
            this.txtAccountingObjectGroupCode.MaxLength = 25;
            this.txtAccountingObjectGroupCode.Name = "txtAccountingObjectGroupCode";
            this.txtAccountingObjectGroupCode.Size = new System.Drawing.Size(245, 22);
            this.txtAccountingObjectGroupCode.TabIndex = 1;
            // 
            // lblAccountingObjectGroupName
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.lblAccountingObjectGroupName.Appearance = appearance4;
            this.lblAccountingObjectGroupName.Location = new System.Drawing.Point(8, 54);
            this.lblAccountingObjectGroupName.Name = "lblAccountingObjectGroupName";
            this.lblAccountingObjectGroupName.Size = new System.Drawing.Size(120, 22);
            this.lblAccountingObjectGroupName.TabIndex = 1;
            this.lblAccountingObjectGroupName.Text = "Tên nhóm KH,NCC (*)";
            // 
            // lblAccountingObjectGroupCode
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.TextVAlignAsString = "Middle";
            this.lblAccountingObjectGroupCode.Appearance = appearance5;
            this.lblAccountingObjectGroupCode.Location = new System.Drawing.Point(8, 27);
            this.lblAccountingObjectGroupCode.Name = "lblAccountingObjectGroupCode";
            this.lblAccountingObjectGroupCode.Size = new System.Drawing.Size(119, 22);
            this.lblAccountingObjectGroupCode.TabIndex = 0;
            this.lblAccountingObjectGroupCode.Text = "Mã nhóm KH,NCC (*)";
            // 
            // btnSave
            // 
            appearance6.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance6;
            this.btnSave.Location = new System.Drawing.Point(218, 182);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            appearance7.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance7;
            this.btnClose.Location = new System.Drawing.Point(299, 182);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 7;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // chkIsActive
            // 
            this.chkIsActive.BackColor = System.Drawing.Color.Transparent;
            this.chkIsActive.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsActive.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkIsActive.Checked = true;
            this.chkIsActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIsActive.Location = new System.Drawing.Point(8, 187);
            this.chkIsActive.Name = "chkIsActive";
            this.chkIsActive.Size = new System.Drawing.Size(121, 20);
            this.chkIsActive.TabIndex = 5;
            this.chkIsActive.Text = "Hoạt động";
            // 
            // FAccountingObjectGroupDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(394, 217);
            this.Controls.Add(this.chkIsActive);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.ultraGroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FAccountingObjectGroupDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FAccountingObjectGroupDetail_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbParentID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkChiTiet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbChiTiet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectGroupName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectGroupCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObjectGroupCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectGroupName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectGroupCode;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObjectGroupName;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkChiTiet;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbChiTiet;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDescription;
        private Infragistics.Win.Misc.UltraLabel lblDescription;
        private Infragistics.Win.Misc.UltraLabel lblParentID;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbParentID;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsActive;
    }
}
