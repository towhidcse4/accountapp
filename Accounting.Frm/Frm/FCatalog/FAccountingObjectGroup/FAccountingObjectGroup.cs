﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FAccountingObjectGroup : CatalogBase //UserControl
    {
        #region khai báo
        private readonly IAccountingObjectGroupService _IAccountingObjectGroupService;
        List<AccountingObjectGroup> dsAccountingObjectGroup = new List<AccountingObjectGroup>();
        #endregion

        #region khởi tạo
        public FAccountingObjectGroup()
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            ToolTip ToolTip1 = new ToolTip();
            ToolTip1.SetToolTip(btnAdd, "Thêm (Ctrl + N)");
            ToolTip ToolTip2 = new ToolTip();
            ToolTip2.SetToolTip(btnEdit, "Sửa (Ctrl + E)");
            ToolTip ToolTip3 = new ToolTip();
            ToolTip3.SetToolTip(btnDelete, "Xóa (Ctrl + D)");
            #endregion

            #region Thiết lập ban đầu cho Form
            _IAccountingObjectGroupService = IoC.Resolve<IAccountingObjectGroupService>();

            this.uTree.InitializeDataNode += new Infragistics.Win.UltraWinTree.InitializeDataNodeEventHandler(Utils.uTree_InitializeDataNode);
            this.uTree.AfterDataNodesCollectionPopulated += new Infragistics.Win.UltraWinTree.AfterDataNodesCollectionPopulatedEventHandler(Utils.uTree_AfterDataNodesCollectionPopulated);
            this.uTree.ColumnSetGenerated += new Infragistics.Win.UltraWinTree.ColumnSetGeneratedEventHandler(this.uTree_ColumnSetGenerated);
            this.uTree.MouseDown += new System.Windows.Forms.MouseEventHandler(this.uTree_MouseDown);
            this.uTree.DoubleClick += new System.EventHandler(this.uTree_DoubleClick);
            #endregion

            #region Tạo dữ liệu ban đầu
            LoadDuLieu();
            #endregion
        }

        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }
        private void LoadDuLieu(bool configTree)
        {
            WaitingFrm.StartWaiting();
            #region Lấy dữ liệu từ CSDL
            //dsAccountingObjectGroup = _IAccountingObjectGroupService.GetAll().OrderByDescending(c=>c.AccountingObjectGroupCode).Reverse().ToList();
            dsAccountingObjectGroup = _IAccountingObjectGroupService.GetAll_OrderByAccountingObjectGroupCode();
            _IAccountingObjectGroupService.UnbindSession(dsAccountingObjectGroup);
            dsAccountingObjectGroup = _IAccountingObjectGroupService.GetAll_OrderByAccountingObjectGroupCode();
            #endregion

            #region Xử lý dữ liệu
            #endregion

            #region hiển thị và xử lý hiển thị
            DataSet ds = Utils.ToDataSet<AccountingObjectGroup>(dsAccountingObjectGroup, ConstDatabase.AccountingObjectGroup_TableName);
            uTree.SetDataBinding(ds, ConstDatabase.AccountingObjectGroup_TableName);

            if (configTree) ConfigTree(uTree);
            #endregion
            WaitingFrm.StopWaiting();
        }
        #endregion

        #region Nghiệp vụ
        #region ContentMenu
        private void tsmAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void tsmEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            ResetFunction();
        }
        protected override void ResetFunction()
        {
            LoadDuLieu(false);
        }
        #endregion

        #region Button
        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }
        #endregion

        /// <summary>
        /// Nghiệp vụ Add
        /// </summary>
        protected override void AddFunction()
        {
            if (uTree.SelectedNodes.Count > 0)
            {
                AccountingObjectGroup temp = dsAccountingObjectGroup.Single(k => k.ID == (new Guid(uTree.SelectedNodes[0].Text)));
                new FAccountingObjectGroupDetail(temp, true).ShowDialog(this);
                if (!FAccountingObjectGroupDetail.isClose) LoadDuLieu();
            }
            else
            {
                new FAccountingObjectGroupDetail().ShowDialog(this);
                if (!FAccountingObjectGroupDetail.isClose) LoadDuLieu();
            }
        }

        /// <summary>
        /// Nghiệp vụ Edit
        /// </summary>
        protected override void EditFunction()
        {
            if (uTree.SelectedNodes.Count == 0 && uTree.ActiveNode != null)
                uTree.ActiveNode.Selected = true;
            if (uTree.SelectedNodes.Count > 0)
            {
                AccountingObjectGroup temp = dsAccountingObjectGroup.Single(k => k.ID == (new Guid(uTree.SelectedNodes[0].Text)));
                new FAccountingObjectGroupDetail(temp).ShowDialog(this);
                if (!FAccountingObjectGroupDetail.isClose) LoadDuLieu();
            }
            else
                MSG.Error(string.Format(resSystem.MSG_Catalog3,"một nhóm khách hàng nhà cung cấp"));
        }

        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        protected override void DeleteFunction()
        {
            if (uTree.SelectedNodes.Count == 0 && uTree.ActiveNode != null)
            {
                uTree.ActiveNode.Selected = true;
            }
            if (uTree.SelectedNodes.Count > 0)
            {
                AccountingObjectGroup temp = dsAccountingObjectGroup.Single(k => k.ID == (new Guid(uTree.SelectedNodes[0].Text)));
                if (MSG.Question(string.Format(resSystem.MSG_System_05, "<" + temp.AccountingObjectGroupCode + ">")) == System.Windows.Forms.DialogResult.Yes)
                {
                    _IAccountingObjectGroupService.BeginTran();
                    //Kiểm tra xem đối tượng có con không nếu có con thì không được phép xóa
                    //List<AccountingObjectGroup> lstChild =_IAccountingObjectGroupService.Query.Where(p => p.ParentID == temp.ID).ToList();
                    List<AccountingObjectGroup> lstChild = _IAccountingObjectGroupService.GetAll_ByParentID(temp.ID); //thành duy đã sửa chỗ này
                    if (lstChild.Count > 0)
                    {
                        MSG.Warning(string.Format(resSystem.MSG_Catalog_Tree,"nhóm Khách hàng/Nhà cung cấp"));
                        return;
                    }
                    //Check xem cha còn có con khác không nếu không thì chuyển trạng thái IsParentNode = false
                    if (temp.ParentID != null)
                    {
                        AccountingObjectGroup parent = _IAccountingObjectGroupService.Getbykey((Guid)temp.ParentID);
                        //int checkChildOldParent = _IAccountingObjectGroupService.Query.Count(p => p.ParentID == temp.ParentID);
                        int checkChildOldParent = _IAccountingObjectGroupService.CountByParentID(temp.ParentID);  //thành duy đã sửa chỗ này
                        if (checkChildOldParent <= 1)
                        {
                            parent.IsParentNode = false;
                        }
                        _IAccountingObjectGroupService.Update(parent);
                        _IAccountingObjectGroupService.Delete(temp);
                    }
                    if (temp.ParentID == null)
                    {
                        _IAccountingObjectGroupService.Delete(temp);
                    }
                    _IAccountingObjectGroupService.CommitTran();
                    Utils.ClearCacheByType<AccountingObjectGroup>();
                    LoadDuLieu();
                }
            }
            else
                MSG.Error(string.Format(resSystem.MSG_Catalog2,"một nhóm khách hàng, nhà cung cấp"));
        }

        private void uTree_DoubleClick(object sender, EventArgs e)
        {
            if (uTree.SelectedNodes.Count > 0)
                EditFunction();
        }

        private void uTree_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, cms4Grid);
        }

        private void uTree_ColumnSetGenerated(object sender, Infragistics.Win.UltraWinTree.ColumnSetGeneratedEventArgs e)
        {
            Utils.uTree_ColumnSetGenerated(sender, e, ConstDatabase.AccountingObjectGroup_TableName);
        }
        #endregion

        #region Utils
        void ConfigTree(Infragistics.Win.UltraWinTree.UltraTree ultraTree)
        {//hàm chung
            Utils.ConfigTree(ultraTree, TextMessage.ConstDatabase.Department_TableName);
        }


        #endregion

        private void FAccountingObjectGroup_FormClosing(object sender, FormClosingEventArgs e)
        {
            uTree = null;
        }

        private void FAccountingObjectGroup_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
