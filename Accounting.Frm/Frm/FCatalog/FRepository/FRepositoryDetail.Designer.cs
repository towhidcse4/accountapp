﻿namespace Accounting
{
    partial class FRepositoryDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbDefaultAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.lblDefaultAccount = new Infragistics.Win.Misc.UltraLabel();
            this.txtDescription = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblDescription = new Infragistics.Win.Misc.UltraLabel();
            this.txtRepositoryName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtRepositoryCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblRepositoryName = new Infragistics.Win.Misc.UltraLabel();
            this.lblRepositoryCode = new Infragistics.Win.Misc.UltraLabel();
            this.chkIsActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDefaultAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRepositoryName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRepositoryCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox1.Controls.Add(this.cbbDefaultAccount);
            this.ultraGroupBox1.Controls.Add(this.lblDefaultAccount);
            this.ultraGroupBox1.Controls.Add(this.txtDescription);
            this.ultraGroupBox1.Controls.Add(this.lblDescription);
            this.ultraGroupBox1.Controls.Add(this.txtRepositoryName);
            this.ultraGroupBox1.Controls.Add(this.txtRepositoryCode);
            this.ultraGroupBox1.Controls.Add(this.lblRepositoryName);
            this.ultraGroupBox1.Controls.Add(this.lblRepositoryCode);
            appearance5.FontData.BoldAsString = "True";
            this.ultraGroupBox1.HeaderAppearance = appearance5;
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(3, 3);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(387, 195);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbDefaultAccount
            // 
            this.cbbDefaultAccount.AutoSize = false;
            this.cbbDefaultAccount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbDefaultAccount.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbDefaultAccount.Location = new System.Drawing.Point(114, 161);
            this.cbbDefaultAccount.Name = "cbbDefaultAccount";
            this.cbbDefaultAccount.NullText = "<Chọn dữ liệu>";
            this.cbbDefaultAccount.Size = new System.Drawing.Size(260, 22);
            this.cbbDefaultAccount.TabIndex = 26;
            // 
            // lblDefaultAccount
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.TextVAlignAsString = "Middle";
            this.lblDefaultAccount.Appearance = appearance1;
            this.lblDefaultAccount.Location = new System.Drawing.Point(8, 161);
            this.lblDefaultAccount.Name = "lblDefaultAccount";
            this.lblDefaultAccount.Size = new System.Drawing.Size(99, 22);
            this.lblDefaultAccount.TabIndex = 25;
            this.lblDefaultAccount.Text = "TK kho mặc định";
            // 
            // txtDescription
            // 
            this.txtDescription.AutoSize = false;
            this.txtDescription.Location = new System.Drawing.Point(114, 81);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(260, 75);
            this.txtDescription.TabIndex = 24;
            // 
            // lblDescription
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.TextVAlignAsString = "Middle";
            this.lblDescription.Appearance = appearance2;
            this.lblDescription.Location = new System.Drawing.Point(8, 81);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(101, 22);
            this.lblDescription.TabIndex = 23;
            this.lblDescription.Text = "Mô tả chi tiết";
            // 
            // txtRepositoryName
            // 
            this.txtRepositoryName.AutoSize = false;
            this.txtRepositoryName.Location = new System.Drawing.Point(114, 54);
            this.txtRepositoryName.Name = "txtRepositoryName";
            this.txtRepositoryName.Size = new System.Drawing.Size(260, 22);
            this.txtRepositoryName.TabIndex = 6;
            // 
            // txtRepositoryCode
            // 
            this.txtRepositoryCode.AutoSize = false;
            this.txtRepositoryCode.Location = new System.Drawing.Point(114, 27);
            this.txtRepositoryCode.MaxLength = 30;
            this.txtRepositoryCode.Name = "txtRepositoryCode";
            this.txtRepositoryCode.Size = new System.Drawing.Size(260, 22);
            this.txtRepositoryCode.TabIndex = 5;
            // 
            // lblRepositoryName
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextVAlignAsString = "Middle";
            this.lblRepositoryName.Appearance = appearance3;
            this.lblRepositoryName.Location = new System.Drawing.Point(8, 54);
            this.lblRepositoryName.Name = "lblRepositoryName";
            this.lblRepositoryName.Size = new System.Drawing.Size(101, 22);
            this.lblRepositoryName.TabIndex = 1;
            this.lblRepositoryName.Text = "Tên kho (*)";
            // 
            // lblRepositoryCode
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.lblRepositoryCode.Appearance = appearance4;
            this.lblRepositoryCode.Location = new System.Drawing.Point(8, 27);
            this.lblRepositoryCode.Name = "lblRepositoryCode";
            this.lblRepositoryCode.Size = new System.Drawing.Size(100, 22);
            this.lblRepositoryCode.TabIndex = 0;
            this.lblRepositoryCode.Text = "Mã kho (*)";
            // 
            // chkIsActive
            // 
            appearance6.TextVAlignAsString = "Middle";
            this.chkIsActive.Appearance = appearance6;
            this.chkIsActive.BackColor = System.Drawing.Color.Transparent;
            this.chkIsActive.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsActive.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkIsActive.Checked = true;
            this.chkIsActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIsActive.Location = new System.Drawing.Point(8, 204);
            this.chkIsActive.Name = "chkIsActive";
            this.chkIsActive.Size = new System.Drawing.Size(103, 22);
            this.chkIsActive.TabIndex = 14;
            this.chkIsActive.Text = "Hoạt động";
            // 
            // btnClose
            // 
            appearance7.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance7;
            this.btnClose.Location = new System.Drawing.Point(299, 204);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 62;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            appearance8.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance8;
            this.btnSave.Location = new System.Drawing.Point(218, 204);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 61;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // FRepositoryDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(396, 238);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.chkIsActive);
            this.Controls.Add(this.ultraGroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FRepositoryDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Chi tiết kho";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FRepositoryDetail_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbDefaultAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRepositoryName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRepositoryCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraLabel lblRepositoryCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtRepositoryName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtRepositoryCode;
        private Infragistics.Win.Misc.UltraLabel lblRepositoryName;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsActive;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDescription;
        private Infragistics.Win.Misc.UltraLabel lblDescription;
        private Infragistics.Win.Misc.UltraLabel lblDefaultAccount;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbDefaultAccount;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnSave;
    }
}
