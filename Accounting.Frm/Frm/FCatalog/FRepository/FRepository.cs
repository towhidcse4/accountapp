﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using Accounting.TextMessage;

namespace Accounting
{
    public partial class FRepository : CatalogBase //UserControl
    {
        #region khai báo
        private readonly IRepositoryService _IRepositoryService;
        #endregion

        #region khởi tạo
        public FRepository()
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _IRepositoryService = IoC.Resolve<IRepositoryService>();
            this.uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            #endregion

            #region Tạo dữ liệu ban đầu
            LoadDuLieu();
            System.Windows.Forms.ToolTip ToolTip1 = new System.Windows.Forms.ToolTip();
            ToolTip1.SetToolTip(btnAdd, "Thêm (Ctrl + N)");
            System.Windows.Forms.ToolTip ToolTip2 = new System.Windows.Forms.ToolTip();
            ToolTip2.SetToolTip(btnEdit, "Xem (Ctrl + E)");
            System.Windows.Forms.ToolTip ToolTip3 = new System.Windows.Forms.ToolTip();
            ToolTip3.SetToolTip(btnDelete, "Xóa (Ctrl + D)");
            if (uGrid.Rows.Count > 0) uGrid.Rows[0].Selected = true;
            #endregion
        }

        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }
        private void LoadDuLieu(bool configGrid)
        {
            WaitingFrm.StartWaiting();
            #region Lấy dữ liệu từ CSDL
            List<Repository> list = _IRepositoryService.GetAll_OrderBy();
            _IRepositoryService.UnbindSession(list);
            list = _IRepositoryService.GetAll_OrderBy();
            #endregion

            #region Xử lý dữ liệu
            #endregion

            #region hiển thị và xử lý hiển thị
            uGrid.DataSource = list.ToArray();
            if (uGrid.Rows.Count > 0)
            {
                uGrid.Rows[0].Selected = true;
            }
            if (configGrid) ConfigGrid(uGrid);
            uGrid.DisplayLayout.Bands[0].Columns["RepositoryCode"].Width = 150;
            uGrid.DisplayLayout.Bands[0].Columns["RepositoryName"].Width = 400;
            uGrid.DisplayLayout.Bands[0].Columns["IsActive"].Width = 50;

            
            #endregion
            WaitingFrm.StopWaiting();
        }
        private void LoadDuLieuExport(bool configGrid)
        {
            WaitingFrm.StartWaiting();
            #region Lấy dữ liệu từ CSDL
            List<Repository> list = _IRepositoryService.GetAll_OrderBy();
            _IRepositoryService.UnbindSession(list);
            list = _IRepositoryService.GetAll_OrderBy();
            #endregion

            #region Xử lý dữ liệu
            #endregion
            if(list!=null)
            {
                foreach(var item in list)
                {
                if(item.IsActive==true)
                    {
                        item.ActiveName = "Đang hoạt động";
                    }
                else if (item.IsActive == false)
                    {
                        item.ActiveName = "Ngừng hoạt động";
                    }

                }
            }
            #region hiển thị và xử lý hiển thị
            uGridExport.DataSource = list.ToArray();
            
            if (configGrid) { Utils.ConfigGrid(uGridExport, ConstDatabase.Repository_TableNameExport); }
            uGrid.DisplayLayout.Bands[0].Columns["RepositoryCode"].Width = 150;
            uGrid.DisplayLayout.Bands[0].Columns["RepositoryName"].Width = 400;
            uGrid.DisplayLayout.Bands[0].Columns["IsActive"].Width = 50;


            #endregion
            WaitingFrm.StopWaiting();
        }
        #endregion

        #region Nghiệp vụ
        #region ContentMenu
        private void tsmAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void tsmEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            ResetFunction();
        }
        protected override void ResetFunction()
        {
            LoadDuLieu(false);
        }

        private void uGrid_MouseDown_1(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }

        #endregion

        #region Button
        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }
        #endregion

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (e.Row.Index == -1)
                return;
            EditFunction();
        }

        /// <summary>
        /// Nghiệp vụ Add
        /// </summary>
        protected override void AddFunction()
        {
            new FRepositoryDetail().ShowDialog(this);
            if (!FRepositoryDetail.isClose) LoadDuLieu();
        }

        /// <summary>
        /// Nghiệp vụ Edit
        /// </summary>
        protected override void EditFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                Repository temp = uGrid.Selected.Rows[0].ListObject as Repository;
                new FRepositoryDetail(temp).ShowDialog(this);
                if (!FRepositoryDetail.isClose) LoadDuLieu();
            }
            else
                MSG.Error(string.Format(resSystem.MSG_Catalog3,"một Kho"));
        }

        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        protected override void DeleteFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                Repository temp = uGrid.Selected.Rows[0].ListObject as Repository;
                if (MSG.Question(string.Format(resSystem.MSG_System_05, temp.RepositoryName)) == System.Windows.Forms.DialogResult.Yes)
                {
                    try
                    {
                        if (!Utils.checkRelationVoucher(temp))
                        {
                            _IRepositoryService.BeginTran();
                            _IRepositoryService.Delete(temp);
                            _IRepositoryService.CommitTran();
                            Utils.ClearCacheByType<Repository>();
                            LoadDuLieu();
                        }
                        else
                        {
                            MSG.Error("Đã khai báo vật tư trong kho, không thể xóa kho");
                        }
                    }
                    catch
                    {
                        _IRepositoryService.RolbackTran();
                    }
                }
            }
            else
                MSG.Error(string.Format(resSystem.MSG_Catalog2,"một Kho"));
            
        }
        #endregion

        #region Utils
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, TextMessage.ConstDatabase.Repository_TableName);
        }


        #endregion

        private void FRepository_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGrid.ResetText();
            uGrid.ResetUpdateMode();
            uGrid.ResetExitEditModeOnLeave();
            uGrid.ResetRowUpdateCancelAction();
            uGrid.DataSource = null;
            uGrid.Layouts.Clear();
            uGrid.ResetLayouts();
            uGrid.ResetDisplayLayout();
            uGrid.Refresh();
            uGrid.ClearUndoHistory();
            uGrid.ClearXsdConstraints();
        }

        private void FRepository_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            UltraGrid ultraGridExport = uGridExport;
            ultraGridExport.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            LoadDuLieuExport(true);
            //Utils.ConfigGrid(ultraGridExport, TextMessage.ConstDatabase.MaterialGoods_TableNameExportExcel);
            Utils.ExportExcel(uGridExport, "Export_DMKho");
        }
    }
}
