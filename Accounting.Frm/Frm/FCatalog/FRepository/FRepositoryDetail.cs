﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Accounting.TextMessage;
using System.ComponentModel;

namespace Accounting
{
    public partial class FRepositoryDetail : DialogForm //UserControl
    {
        #region khai báo
        private readonly IRepositoryService _IRepositoryService;
        Repository _Select = new Repository();
        bool Them = true;
        public static bool isClose = true;
        public static string MaKho;
        #endregion
        public Guid Id { get; set; }
        #region khởi tạo
        public FRepositoryDetail()
        {//Thêm
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion
            this.Text = "Thêm mới chi tiết kho";
            chkIsActive.Visible = false;
            #region Thiết lập ban đầu cho Form
            //Khai báo các webservices
            _IRepositoryService = IoC.Resolve<IRepositoryService>();

            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion
        }

        public FRepositoryDetail(Repository temp)
        {//Sửa
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion
            this.Text = "Sửa chi tiết kho";
            #region Thiết lập ban đầu cho Form
            _Select = temp;
            Them = false;
            txtRepositoryCode.Enabled = false;
            //Khai báo các webservices
            _IRepositoryService = IoC.Resolve<IRepositoryService>();

            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion

            #region Fill dữ liệu Obj vào control
            ObjandGUI(temp, false);
            #endregion
        }

        private void InitializeGUI()
        {
            //Khởi tạo TK nợ, TK có
            List<string> temp = new List<string>() { "152", "153","154", "155","156","157" };
            List<Account> dsAccount = new List<Account>();
            foreach (string item in temp)
            {
                dsAccount.AddRange(Utils.GetChildrenAccount(new Account(item)));
            }

            cbbDefaultAccount.DataSource = dsAccount.Where(p=>p.IsActive==true).ToList();
            cbbDefaultAccount.DisplayMember = "AccountNumber";
            Utils.ConfigGrid(cbbDefaultAccount, ConstDatabase.Account_TableName);
        }
        #endregion

        #region Button Event
        /// <summary>
        /// Sự kiện nút Lưu lại
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion

            //Gui to object
            #region Fill dữ liệu control vào obj
            Repository temp = Them ? new Repository() : _IRepositoryService.Getbykey(_Select.ID);
            temp = ObjandGUI(temp, true);
            #endregion

            #region Thao tác CSDL
            try
            {
                _IRepositoryService.BeginTran();
                if (Them) _IRepositoryService.CreateNew(temp);
                else _IRepositoryService.Update(temp);
                _IRepositoryService.CommitTran();
            }
            catch
            {
                _IRepositoryService.RolbackTran();
            }
            Id = temp.ID;
            #endregion

            #region xử lý form, kết thúc form

            Utils.ListRepository.Clear();
            Utils.ListRepository.AddToBindingList(new BindingList<Repository>(_IRepositoryService.GetAll_ByOrderbyIsActive(true)));
            MaKho = txtRepositoryCode.Text;
            this.Close();
            isClose = false;
            #endregion
        }

        /// <summary>
        /// Sự kiện nút đóng
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            isClose = true;
            DialogResult = DialogResult.Cancel;
            this.Close();
        }
        #endregion

        #region Utils
        /// <summary>
        /// get hoặc set một đối tượng vào giao diện xử lý
        /// </summary>
        /// <param name="input">đối tượng</param>
        /// <param name="isGet">True: get giá trị từ control gán vào đối tượng| False: set giá trị đối tượng vào control</param>
        /// <returns></returns>
        Repository ObjandGUI(Repository input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();  //Thêm mới

                //mã, tên, mô tả chi tiết
                input.RepositoryCode = txtRepositoryCode.Text;
                input.RepositoryName = txtRepositoryName.Text;
                input.Description = txtDescription.Text;
                //TK kho mặc định
                Account _account = (Account)Utils.getSelectCbbItem(cbbDefaultAccount);
                if (_account == null) input.DefaultAccount = string.Empty;
                else input.DefaultAccount = _account.AccountNumber;
                //Ngừng theo dõi
                input.IsActive = chkIsActive.Checked;
            }
            else
            {
                //mã, tên, mô tả chi tiết
                txtRepositoryCode.Text = input.RepositoryCode;
                txtRepositoryName.Text = input.RepositoryName;
                txtDescription.Text = input.Description;
                //TK kho mặc định
                foreach (var item in cbbDefaultAccount.Rows)
                {
                    if ((item.ListObject as Account).AccountNumber == input.DefaultAccount) cbbDefaultAccount.SelectedRow = item;
                }
                //Ngừng theo dõi
                chkIsActive.Checked = input.IsActive;
            }
            return input;
        }

        bool CheckError()
        {
            bool kq = true;
            //Check Error Chung

            //Check Error Riêng từng Form
            if (string.IsNullOrEmpty(txtRepositoryCode.Text) || string.IsNullOrEmpty(txtRepositoryName.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }
            // chek mã kho không c trùng nhau trong db
            // List<string> listRepositoryCode = _IRepositoryService.Query.Select(p => p.RepositoryCode).ToList();
            List<string> listRepositoryCode = _IRepositoryService.GetRepositoryCode();
            foreach (var item in listRepositoryCode)
            {
                if (item == txtRepositoryCode.Text && Them)
                {
                    MSG.Error(string.Format(resSystem.MSG_Catalog,"Mã",item));
                    return false;
                }

            }
            return kq;
        }
        #endregion

        private void FRepositoryDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}