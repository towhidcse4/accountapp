﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using Accounting.TextMessage;

namespace Accounting
{
    public partial class FPSSalaryTaxInsuranceRegulation : CatalogBase
    {
        #region khai báo

        private readonly IPSSalaryTaxInsuranceRegulationService _IPSSalaryTaxInsuranceRegulationService = Utils.IPSSalaryTaxInsuranceRegulationService;
        #endregion

        #region khởi tạo

        public FPSSalaryTaxInsuranceRegulation()
        {
            #region Khởi tạo giá trị mặc định của Form

            InitializeComponent();

            #endregion

            #region Thiết lập ban đầu cho Form

            uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;

            #endregion

            #region Tạo dữ liệu ban đầu
            
            LoadDuLieu();
            System.Windows.Forms.ToolTip ToolTip1 = new System.Windows.Forms.ToolTip();
            ToolTip1.SetToolTip(btnAdd, "Thêm (Ctrl + N)");
            System.Windows.Forms.ToolTip ToolTip2 = new System.Windows.Forms.ToolTip();
            ToolTip2.SetToolTip(btnEdit, "Xem (Ctrl + E)");
            System.Windows.Forms.ToolTip ToolTip3 = new System.Windows.Forms.ToolTip();
            ToolTip3.SetToolTip(btnDelete, "Xóa (Ctrl + D)");
            if (uGrid.Rows.Count > 0) uGrid.Rows[0].Selected = true;
            #endregion
        }

        private void LoadDuLieu()
        {
            LoadDuLieu(true);
        }

        private void LoadDuLieu(bool configGrid)
        {
            WaitingFrm.StartWaiting();
            #region Lấy dữ liệu từ CSDL

            List<PSSalaryTaxInsuranceRegulation> list = _IPSSalaryTaxInsuranceRegulationService.GetAll();
            _IPSSalaryTaxInsuranceRegulationService.UnbindSession(list);
            list = _IPSSalaryTaxInsuranceRegulationService.GetAll();
            #endregion

            #region hiển thị và xử lý hiển thị

            uGrid.DataSource = list.OrderByDescending(x => x.FromDate).ToList();

            if (configGrid) ConfigGrid(uGrid);
            foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
            {
                this.ConfigEachColumn4Grid(0, column, uGrid);
            }
            #endregion
            WaitingFrm.StopWaiting();
        }

        #endregion

        #region Nghiệp vụ

        #region ContentMenu

        private void tsmAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void tsmEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            ResetFunction();
        }
        protected override void ResetFunction()
        {
            LoadDuLieu(false);
        }

        #endregion

        #region Button

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        #endregion

        private void uGrid_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (e.Row.Index >= 0)
            {
                PSSalaryTaxInsuranceRegulation temp = e.Row.ListObject as PSSalaryTaxInsuranceRegulation;
                new FPSSalaryTaxInsuranceRegulationDetail(temp).ShowDialog(this);
                if (!FPSSalaryTaxInsuranceRegulationDetail.isClose) LoadDuLieu();
            }
        }

        /// <summary>
        /// Nghiệp vụ Add
        /// </summary>
        protected override void AddFunction()
        {
            new FPSSalaryTaxInsuranceRegulationDetail().ShowDialog(this);
            if (!FPSSalaryTaxInsuranceRegulationDetail.isClose) LoadDuLieu();
        }

        /// <summary>
        /// Nghiệp vụ Edit
        /// </summary>
        protected override void EditFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                PSSalaryTaxInsuranceRegulation temp = uGrid.Selected.Rows[0].ListObject as PSSalaryTaxInsuranceRegulation;
                new FPSSalaryTaxInsuranceRegulationDetail(temp).ShowDialog(this);
                if (!FPSSalaryTaxInsuranceRegulationDetail.isClose) LoadDuLieu();
            }
            else
                Accounting.TextMessage.MSG.Error(string.Format(Accounting.TextMessage.resSystem.MSG_Catalog3, "một quy định"));
        }

        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        protected override void DeleteFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                PSSalaryTaxInsuranceRegulation temp = uGrid.Selected.Rows[0].ListObject as PSSalaryTaxInsuranceRegulation;
                if (MSG.Question("Bạn có chắc chắn muốn xóa quy định lương, thuế, bảo hiểm này không") ==
                    System.Windows.Forms.DialogResult.Yes)
                {
                    try
                    {
                        _IPSSalaryTaxInsuranceRegulationService.BeginTran();
                        _IPSSalaryTaxInsuranceRegulationService.Delete(temp);
                        _IPSSalaryTaxInsuranceRegulationService.CommitTran();
                        LoadDuLieu();
                    }
                    catch
                    {
                        MSG.Warning("Có lỗi xảy ra khi xóa quy định này!");
                        _IPSSalaryTaxInsuranceRegulationService.RolbackTran();
                    }
                    
                }
            }
            else
            {
                MSG.Warning("Bạn cần chọn quy định để xóa!");
            }
            
        }

        #endregion

        #region Utils

        private void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {
            utralGrid.ConfigGrid(ConstDatabase.PSSalaryTaxInsuranceRegulation_TableName);
            var band = utralGrid.DisplayLayout.Bands[0];
            var columnAmout = new List<string>();
            foreach (var column in band.Columns)
            {
                column.ConfigColumnByNumberic();
            }
            Utils.FormatNumberic(utralGrid.DisplayLayout.Bands[0].Columns["BasicWage"], ConstDatabase.Format_TienVND);
        }
        #endregion

        private void FPSSalaryTaxInsuranceRegulation_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGrid.ResetText();
            uGrid.ResetUpdateMode();
            uGrid.ResetExitEditModeOnLeave();
            uGrid.ResetRowUpdateCancelAction();
            uGrid.DataSource = null;
            uGrid.Layouts.Clear();
            uGrid.ResetLayouts();
            uGrid.ResetDisplayLayout();
            uGrid.Refresh();
            uGrid.ClearUndoHistory();
            uGrid.ClearXsdConstraints();
        }

        private void FPSSalaryTaxInsuranceRegulation_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
