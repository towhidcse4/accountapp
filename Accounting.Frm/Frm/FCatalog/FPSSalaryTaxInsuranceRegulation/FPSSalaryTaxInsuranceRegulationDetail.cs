﻿using System;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinEditors;
using Itenso.TimePeriod;

namespace Accounting
{
    public partial class FPSSalaryTaxInsuranceRegulationDetail : DialogForm
    {
        #region khai báo

        private readonly IPSSalaryTaxInsuranceRegulationService _IPSSalaryTaxInsuranceRegulationService = Utils.IPSSalaryTaxInsuranceRegulationService;
        PSSalaryTaxInsuranceRegulation _Select = new PSSalaryTaxInsuranceRegulation();
        private PSSalaryTaxInsuranceRegulation oldTemp = new PSSalaryTaxInsuranceRegulation();
        public static bool isClose = true;
        private bool _forceReload = false;
        private bool Them = true;

        #endregion

        #region khởi tạo
        public FPSSalaryTaxInsuranceRegulationDetail()
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion
            #region  config cho các control
            InitializeGUI();
            ConfigTextBox();
            #endregion
        }
        #endregion

        public FPSSalaryTaxInsuranceRegulationDetail(PSSalaryTaxInsuranceRegulation temp)
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _Select = temp;
            Them = false;
            #endregion

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            oldTemp = temp.CloneObject();

            #region Fill dữ liệu Obj vào control
            ObjandGUI(temp, false);
            ConfigTextBox();
            #endregion
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion

            //Gui to object
            #region Fill dữ liệu control vào obj
            PSSalaryTaxInsuranceRegulation temp = Them ? new PSSalaryTaxInsuranceRegulation() : _IPSSalaryTaxInsuranceRegulationService.Getbykey(_Select.ID);
            temp = ObjandGUI(temp, true);
            _Select = temp;

            #endregion
            try
            {
                _IPSSalaryTaxInsuranceRegulationService.BeginTran();
                if (Them)
                    _IPSSalaryTaxInsuranceRegulationService.Save(temp);
                else
                    _IPSSalaryTaxInsuranceRegulationService.Update(temp);
                _IPSSalaryTaxInsuranceRegulationService.CommitTran();

                #region xử lý form, reset form
                isClose = false;
                _forceReload = true;
                // reset form
                DialogResult = DialogResult.OK;
                this.Close();
                #endregion
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                _IPSSalaryTaxInsuranceRegulationService.RolbackTran();
            }
        }



        private void btnClose_Click(object sender, EventArgs e)
        {
            isClose = _forceReload ? false : true;
            DialogResult = DialogResult.Cancel;
            this.Close();
            
        }

        private void InitializeGUI()
        {




        }
        private void ConfigTextBox()
        {
            txtBasicWage.FormatNumberic(ConstDatabase.Format_ForeignCurrency);
            txtInsuaranceMaximumizeSalary.FormatNumberic(ConstDatabase.Format_ForeignCurrency);
            txtReduceSelfTaxAmount.FormatNumberic(ConstDatabase.Format_ForeignCurrency);
            txtReduceDependTaxAmount.FormatNumberic(ConstDatabase.Format_ForeignCurrency);
            txtOvertimeHolidayPercent.FormatNumberic(ConstDatabase.Format_Rate);
            txtOvertimeWeekendPercent.FormatNumberic(ConstDatabase.Format_Rate);
            txtOvertimeDailyPercent.FormatNumberic(ConstDatabase.Format_Rate);
            txtOvertimeHolidayNightPercent.FormatNumberic(ConstDatabase.Format_Rate);
            txtOvertimeWeekendDayNightPercent.FormatNumberic(ConstDatabase.Format_Rate);
            txtOvertimeWorkingDayNightPercent.FormatNumberic(ConstDatabase.Format_Rate);
            txtEmployeeTradeUnionInsurancePercent.FormatNumberic(ConstDatabase.Format_Rate);
            txtEmployeeAccidentInsurancePercent.FormatNumberic(ConstDatabase.Format_Rate);
            txtEmployeeUnEmployeeInsurancePercent.FormatNumberic(ConstDatabase.Format_Rate);
            txtEmployeeMedicalInsurancePercent.FormatNumberic(ConstDatabase.Format_Rate);
            txtEmployeeSocityInsurancePercent.FormatNumberic(ConstDatabase.Format_Rate);
            txtCompanyTradeUnionInsurancePercent.FormatNumberic(ConstDatabase.Format_Rate);
            txtCompanytAccidentInsurancePercent.FormatNumberic(ConstDatabase.Format_Rate);
            txtCompanyUnEmployeeInsurancePercent.FormatNumberic(ConstDatabase.Format_Rate);
            txtCompanyMedicalInsurancePercent.FormatNumberic(ConstDatabase.Format_Rate);
            txtCompanySocityInsurancePercent.FormatNumberic(ConstDatabase.Format_Rate);
            txtWorkDayInMonth.FormatNumberic(ConstDatabase.Format_Quantity);
            txtWorkHoursInDay.FormatNumberic(ConstDatabase.Format_Quantity);
        }

        //check du lieu
        bool CheckError()
        {
            if (!CheckDateTime(dteDateFrom, true))
            {
                return false;
            }
            if (!CheckDateTime(dteDateTo, true))
            {
                return false;
            }
            if (DateTime.Parse(dteDateFrom.Value.ToString()).CompareTo(DateTime.Parse(dteDateTo.Value.ToString())) > 0)
            {
                MSG.Warning("Từ ngày áp dụng phải nhỏ hơn hoặc bằng đến ngày áp dụng");
                return false;
            }
            // Kiểm tra xem từ ngày đến ngày đã có khoảng thời gian nào trùng chưa
            if (_IPSSalaryTaxInsuranceRegulationService.checkDate(_Select.ID, DateTime.Parse(dteDateFrom.Value.ToString()), DateTime.Parse(dteDateTo.Value.ToString())))
            {
                MSG.Warning("Khoảng thời gian này đã bị trùng");
                return false;
            }
            return true;
        }

        PSSalaryTaxInsuranceRegulation ObjandGUI(PSSalaryTaxInsuranceRegulation input, bool isGet)
        {
            if (isGet)
            {
                // từ ngày áp dụng
                if (CheckDateTime(dteDateFrom, false))
                {
                    input.FromDate = DateTime.Parse(dteDateFrom.Value.ToString());
                }
                else
                {
                    input.FromDate = null;
                }

                // từ ngày áp dụng
                if (CheckDateTime(dteDateTo, false))
                {
                    input.ToDate = DateTime.Parse(dteDateTo.Value.ToString());
                }
                else
                {
                    input.ToDate = null;
                }

                // Lương cơ bản
                input.BasicWage = txtBasicWage.Text.IsNullOrEmpty() ? 0 : decimal.Parse(txtBasicWage.Value.ToString());

                input.InsuaranceMaximumizeSalary = txtInsuaranceMaximumizeSalary.Text.IsNullOrEmpty() ? 0 : decimal.Parse(txtInsuaranceMaximumizeSalary.Value.ToString());

                input.ReduceSelfTaxAmount = txtReduceSelfTaxAmount.Text.IsNullOrEmpty() ? 0 : decimal.Parse(txtReduceSelfTaxAmount.Value.ToString());

                input.ReduceDependTaxAmount = txtReduceDependTaxAmount.Text.IsNullOrEmpty() ? 0 : decimal.Parse(txtReduceDependTaxAmount.Value.ToString());

                input.OvertimeHolidayPercent = txtOvertimeHolidayPercent.Text.IsNullOrEmpty() ? 0 : decimal.Parse(txtOvertimeHolidayPercent.Value.ToString());

                input.OvertimeWeekendPercent = txtOvertimeWeekendPercent.Text.IsNullOrEmpty() ? 0 : decimal.Parse(txtOvertimeWeekendPercent.Value.ToString());

                input.OvertimeDailyPercent = txtOvertimeDailyPercent.Text.IsNullOrEmpty() ? 0 : decimal.Parse(txtOvertimeDailyPercent.Value.ToString());

                input.OvertimeHolidayNightPercent = txtOvertimeHolidayNightPercent.Text.IsNullOrEmpty() ? 0 : decimal.Parse(txtOvertimeHolidayNightPercent.Value.ToString());

                input.OvertimeWeekendDayNightPercent = txtOvertimeWeekendDayNightPercent.Text.IsNullOrEmpty() ? 0 : decimal.Parse(txtOvertimeWeekendDayNightPercent.Value.ToString());

                input.OvertimeWorkingDayNightPercent = txtOvertimeWorkingDayNightPercent.Text.IsNullOrEmpty() ? 0 : decimal.Parse(txtOvertimeWorkingDayNightPercent.Value.ToString());

                input.EmployeeTradeUnionInsurancePercent = txtEmployeeTradeUnionInsurancePercent.Text.IsNullOrEmpty() ? 0 : decimal.Parse(txtEmployeeTradeUnionInsurancePercent.Value.ToString());

                input.EmployeeAccidentInsurancePercent = txtEmployeeAccidentInsurancePercent.Text.IsNullOrEmpty() ? 0 : decimal.Parse(txtEmployeeAccidentInsurancePercent.Value.ToString());

                input.EmployeeUnEmployeeInsurancePercent = txtEmployeeUnEmployeeInsurancePercent.Text.IsNullOrEmpty() ? 0 : decimal.Parse(txtEmployeeUnEmployeeInsurancePercent.Value.ToString());

                input.EmployeeMedicalInsurancePercent = txtEmployeeMedicalInsurancePercent.Text.IsNullOrEmpty() ? 0 : decimal.Parse(txtEmployeeMedicalInsurancePercent.Value.ToString());

                input.EmployeeSocityInsurancePercent = txtEmployeeSocityInsurancePercent.Text.IsNullOrEmpty() ? 0 : decimal.Parse(txtEmployeeSocityInsurancePercent.Value.ToString());

                input.CompanyTradeUnionInsurancePercent = txtCompanyTradeUnionInsurancePercent.Text.IsNullOrEmpty() ? 0 : decimal.Parse(txtCompanyTradeUnionInsurancePercent.Value.ToString());

                input.CompanytAccidentInsurancePercent = txtCompanytAccidentInsurancePercent.Text.IsNullOrEmpty() ? 0 : decimal.Parse(txtCompanytAccidentInsurancePercent.Value.ToString());

                input.CompanyUnEmployeeInsurancePercent = txtCompanyUnEmployeeInsurancePercent.Text.IsNullOrEmpty() ? 0 : decimal.Parse(txtCompanyUnEmployeeInsurancePercent.Value.ToString());

                input.CompanyMedicalInsurancePercent = txtCompanyMedicalInsurancePercent.Text.IsNullOrEmpty() ? 0 : decimal.Parse(txtCompanyMedicalInsurancePercent.Value.ToString());

                input.CompanySocityInsurancePercent = txtCompanySocityInsurancePercent.Text.IsNullOrEmpty() ? 0 : decimal.Parse(txtCompanySocityInsurancePercent.Value.ToString());

                input.WorkDayInMonth = txtWorkDayInMonth.Text.IsNullOrEmpty() ? 0 : decimal.Parse(txtWorkDayInMonth.Value.ToString());

                input.WorkingHoursInDay = txtWorkHoursInDay.Text.IsNullOrEmpty() ? 0 : decimal.Parse(txtWorkHoursInDay.Value.ToString());
                input.IsWorkingOnSaturday = chkT7.Checked;
                input.IsWorkingOnSunday = chkCn.Checked;
                input.IsWorkingOnSaturdayNoon = chkT7c.Checked;
                input.IsWorkingOnSundayNoon = chkCnc.Checked;
            }
            else
            {
                // từ ngày áp dụng
                if (input.FromDate != null)
                {
                    dteDateFrom.Value = input.FromDate;
                }

                // từ ngày áp dụng
                if (input.ToDate != null)
                {
                    dteDateTo.Value = input.ToDate;
                }

                // Lương cơ bản
                txtBasicWage.Value = input.BasicWage;
                txtInsuaranceMaximumizeSalary.Value = input.InsuaranceMaximumizeSalary;
                txtReduceSelfTaxAmount.Value = input.ReduceSelfTaxAmount;
                txtReduceDependTaxAmount.Value = input.ReduceDependTaxAmount;
                txtOvertimeHolidayPercent.Value = input.OvertimeHolidayPercent;
                txtOvertimeWeekendPercent.Value = input.OvertimeWeekendPercent;
                txtOvertimeDailyPercent.Value = input.OvertimeDailyPercent;
                txtOvertimeHolidayNightPercent.Value = input.OvertimeHolidayNightPercent;
                txtOvertimeWeekendDayNightPercent.Value = input.OvertimeWeekendDayNightPercent;
                txtOvertimeWorkingDayNightPercent.Value = input.OvertimeWorkingDayNightPercent;
                txtEmployeeTradeUnionInsurancePercent.Value = input.EmployeeTradeUnionInsurancePercent;
                txtEmployeeAccidentInsurancePercent.Value = input.EmployeeAccidentInsurancePercent;
                txtEmployeeUnEmployeeInsurancePercent.Value = input.EmployeeUnEmployeeInsurancePercent;
                txtEmployeeMedicalInsurancePercent.Value = input.EmployeeMedicalInsurancePercent;
                txtEmployeeSocityInsurancePercent.Value = input.EmployeeSocityInsurancePercent;
                txtCompanyTradeUnionInsurancePercent.Value = input.CompanyTradeUnionInsurancePercent;
                txtCompanytAccidentInsurancePercent.Value = input.CompanytAccidentInsurancePercent;
                txtCompanyUnEmployeeInsurancePercent.Value = input.CompanyUnEmployeeInsurancePercent;
                txtCompanyMedicalInsurancePercent.Value = input.CompanyMedicalInsurancePercent;
                txtCompanySocityInsurancePercent.Value = input.CompanySocityInsurancePercent;
                txtWorkDayInMonth.Value = input.WorkDayInMonth;
                txtWorkHoursInDay.Value = input.WorkingHoursInDay;
                chkT7.Checked = input.IsWorkingOnSaturday;
                chkCn.Checked = input.IsWorkingOnSunday;
                chkT7c.Checked = input.IsWorkingOnSaturdayNoon;
                chkCnc.Checked = input.IsWorkingOnSundayNoon;
            }
            return input;
        }

        bool CheckDateTime(UltraDateTimeEditor cld, bool dk)
        {
            DateTime dt;
            if (cld.Value != null)
            {
                try
                {
                    dt = DateTime.Parse(cld.Value.ToString());
                    return true;
                }
                catch (Exception)
                {
                    if (dk)
                        MSG.Error(resSystem.MSG_Catalog_FCostSetDetail6);
                    return false;
                }
            }
            else
            {
                if (dk)
                    MSG.Error(resSystem.MSG_System_03);
                return false;
            }
        }

        private void FPSSalaryTaxInsuranceRegulationDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}