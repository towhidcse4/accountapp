﻿namespace Accounting
{
    partial class FPSSalaryTaxInsuranceRegulationDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            this.tsmFixedAssetDetail = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmAdd1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDelete1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmFixedAssetAccessories = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.ultraGroupBox15 = new Infragistics.Win.Misc.UltraGroupBox();
            this.dteDateTo = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.dteDateFrom = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox16 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtReduceDependTaxAmount = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.txtInsuaranceMaximumizeSalary = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.txtReduceSelfTaxAmount = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.txtBasicWage = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox17 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel40 = new Infragistics.Win.Misc.UltraLabel();
            this.txtOvertimeHolidayPercent = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtOvertimeWeekendPercent = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtOvertimeDailyPercent = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel62 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel60 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel57 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox18 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel38 = new Infragistics.Win.Misc.UltraLabel();
            this.chkCnc = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraLabel39 = new Infragistics.Win.Misc.UltraLabel();
            this.chkT7c = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.chkCn = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraLabel67 = new Infragistics.Win.Misc.UltraLabel();
            this.chkT7 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.txtWorkHoursInDay = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.txtWorkDayInMonth = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.txtOvertimeHolidayNightPercent = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtOvertimeWeekendDayNightPercent = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtOvertimeWorkingDayNightPercent = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel17 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel34 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel18 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel35 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel19 = new Infragistics.Win.Misc.UltraLabel();
            this.txtEmployeeTradeUnionInsurancePercent = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel36 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel20 = new Infragistics.Win.Misc.UltraLabel();
            this.txtEmployeeAccidentInsurancePercent = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtEmployeeUnEmployeeInsurancePercent = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel37 = new Infragistics.Win.Misc.UltraLabel();
            this.txtEmployeeMedicalInsurancePercent = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtEmployeeSocityInsurancePercent = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel21 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel22 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel23 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel33 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel32 = new Infragistics.Win.Misc.UltraLabel();
            this.txtCompanyTradeUnionInsurancePercent = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel31 = new Infragistics.Win.Misc.UltraLabel();
            this.txtCompanytAccidentInsurancePercent = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel30 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel24 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel25 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel26 = new Infragistics.Win.Misc.UltraLabel();
            this.txtCompanyUnEmployeeInsurancePercent = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtCompanyMedicalInsurancePercent = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtCompanySocityInsurancePercent = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel27 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel28 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel29 = new Infragistics.Win.Misc.UltraLabel();
            this.tsmFixedAssetDetail.SuspendLayout();
            this.tsmFixedAssetAccessories.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox15)).BeginInit();
            this.ultraGroupBox15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dteDateTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDateFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox16)).BeginInit();
            this.ultraGroupBox16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox17)).BeginInit();
            this.ultraGroupBox17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox18)).BeginInit();
            this.ultraGroupBox18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkCnc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkT7c)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkT7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tsmFixedAssetDetail
            // 
            this.tsmFixedAssetDetail.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmAdd1,
            this.tsmDelete1});
            this.tsmFixedAssetDetail.Name = "tsmFixedAssetDetail";
            this.tsmFixedAssetDetail.Size = new System.Drawing.Size(68, 48);
            // 
            // tsmAdd1
            // 
            this.tsmAdd1.Name = "tsmAdd1";
            this.tsmAdd1.Size = new System.Drawing.Size(67, 22);
            // 
            // tsmDelete1
            // 
            this.tsmDelete1.Name = "tsmDelete1";
            this.tsmDelete1.Size = new System.Drawing.Size(67, 22);
            // 
            // tsmFixedAssetAccessories
            // 
            this.tsmFixedAssetAccessories.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmAdd,
            this.tsmDelete});
            this.tsmFixedAssetAccessories.Name = "contextMenuStrip1";
            this.tsmFixedAssetAccessories.Size = new System.Drawing.Size(68, 48);
            // 
            // tsmAdd
            // 
            this.tsmAdd.Name = "tsmAdd";
            this.tsmAdd.Size = new System.Drawing.Size(67, 22);
            // 
            // tsmDelete
            // 
            this.tsmDelete.Name = "tsmDelete";
            this.tsmDelete.Size = new System.Drawing.Size(67, 22);
            // 
            // btnClose
            // 
            appearance1.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance1;
            this.btnClose.Location = new System.Drawing.Point(602, 547);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 55;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            appearance2.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance2;
            this.btnSave.Location = new System.Drawing.Point(521, 547);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 54;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // ultraGroupBox15
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            this.ultraGroupBox15.Appearance = appearance3;
            this.ultraGroupBox15.ContentPadding.Top = 10;
            this.ultraGroupBox15.Controls.Add(this.dteDateTo);
            this.ultraGroupBox15.Controls.Add(this.ultraLabel4);
            this.ultraGroupBox15.Controls.Add(this.dteDateFrom);
            this.ultraGroupBox15.Controls.Add(this.ultraLabel3);
            this.ultraGroupBox15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            appearance6.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Centered;
            this.ultraGroupBox15.HeaderAppearance = appearance6;
            this.ultraGroupBox15.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox15.Name = "ultraGroupBox15";
            this.ultraGroupBox15.Size = new System.Drawing.Size(689, 70);
            this.ultraGroupBox15.TabIndex = 61;
            this.ultraGroupBox15.Text = "Thời gian áp dụng quy định";
            this.ultraGroupBox15.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // dteDateTo
            // 
            this.dteDateTo.AutoSize = false;
            this.dteDateTo.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.dteDateTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dteDateTo.Location = new System.Drawing.Point(512, 29);
            this.dteDateTo.MaskInput = "dd/mm/yyyy";
            this.dteDateTo.Name = "dteDateTo";
            this.dteDateTo.Size = new System.Drawing.Size(115, 22);
            this.dteDateTo.TabIndex = 63;
            // 
            // ultraLabel4
            // 
            appearance4.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance4;
            this.ultraLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel4.Location = new System.Drawing.Point(378, 31);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(128, 20);
            this.ultraLabel4.TabIndex = 62;
            this.ultraLabel4.Text = "Áp dụng đến ngày (*):";
            // 
            // dteDateFrom
            // 
            this.dteDateFrom.AutoSize = false;
            this.dteDateFrom.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.dteDateFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dteDateFrom.Location = new System.Drawing.Point(122, 31);
            this.dteDateFrom.MaskInput = "dd/mm/yyyy";
            this.dteDateFrom.Name = "dteDateFrom";
            this.dteDateFrom.Size = new System.Drawing.Size(115, 22);
            this.dteDateFrom.TabIndex = 61;
            // 
            // ultraLabel3
            // 
            appearance5.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance5;
            this.ultraLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel3.Location = new System.Drawing.Point(12, 31);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(110, 20);
            this.ultraLabel3.TabIndex = 60;
            this.ultraLabel3.Text = "Áp dụng từ ngày(*):";
            // 
            // ultraGroupBox16
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            this.ultraGroupBox16.Appearance = appearance7;
            this.ultraGroupBox16.ContentPadding.Top = 10;
            this.ultraGroupBox16.Controls.Add(this.txtReduceDependTaxAmount);
            this.ultraGroupBox16.Controls.Add(this.ultraLabel6);
            this.ultraGroupBox16.Controls.Add(this.txtInsuaranceMaximumizeSalary);
            this.ultraGroupBox16.Controls.Add(this.ultraLabel7);
            this.ultraGroupBox16.Controls.Add(this.txtReduceSelfTaxAmount);
            this.ultraGroupBox16.Controls.Add(this.ultraLabel5);
            this.ultraGroupBox16.Controls.Add(this.txtBasicWage);
            this.ultraGroupBox16.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            appearance16.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Centered;
            this.ultraGroupBox16.HeaderAppearance = appearance16;
            this.ultraGroupBox16.Location = new System.Drawing.Point(0, 70);
            this.ultraGroupBox16.Name = "ultraGroupBox16";
            this.ultraGroupBox16.Size = new System.Drawing.Size(689, 89);
            this.ultraGroupBox16.TabIndex = 62;
            this.ultraGroupBox16.Text = "Tiền lương";
            this.ultraGroupBox16.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtReduceDependTaxAmount
            // 
            appearance8.FontData.BoldAsString = "False";
            appearance8.TextHAlignAsString = "Right";
            this.txtReduceDependTaxAmount.Appearance = appearance8;
            this.txtReduceDependTaxAmount.Location = new System.Drawing.Point(512, 53);
            this.txtReduceDependTaxAmount.MaxValue = new decimal(new int[] {
            -1486618625,
            232830643,
            0,
            0});
            this.txtReduceDependTaxAmount.Name = "txtReduceDependTaxAmount";
            this.txtReduceDependTaxAmount.PromptChar = ' ';
            this.txtReduceDependTaxAmount.Size = new System.Drawing.Size(115, 21);
            this.txtReduceDependTaxAmount.TabIndex = 16;
            // 
            // ultraLabel6
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.TextHAlignAsString = "Left";
            appearance9.TextVAlignAsString = "Middle";
            this.ultraLabel6.Appearance = appearance9;
            this.ultraLabel6.AutoSize = true;
            this.ultraLabel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel6.Location = new System.Drawing.Point(245, 58);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(134, 14);
            this.ultraLabel6.TabIndex = 15;
            this.ultraLabel6.Text = "Giảm trừ người phụ thuộc: ";
            // 
            // txtInsuaranceMaximumizeSalary
            // 
            appearance10.FontData.BoldAsString = "False";
            appearance10.TextHAlignAsString = "Right";
            this.txtInsuaranceMaximumizeSalary.Appearance = appearance10;
            this.txtInsuaranceMaximumizeSalary.Location = new System.Drawing.Point(512, 26);
            this.txtInsuaranceMaximumizeSalary.MaxValue = new decimal(new int[] {
            -1486618625,
            232830643,
            0,
            0});
            this.txtInsuaranceMaximumizeSalary.Name = "txtInsuaranceMaximumizeSalary";
            this.txtInsuaranceMaximumizeSalary.PromptChar = ' ';
            this.txtInsuaranceMaximumizeSalary.Size = new System.Drawing.Size(115, 21);
            this.txtInsuaranceMaximumizeSalary.TabIndex = 14;
            // 
            // ultraLabel7
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            appearance11.TextHAlignAsString = "Left";
            appearance11.TextVAlignAsString = "Middle";
            this.ultraLabel7.Appearance = appearance11;
            this.ultraLabel7.AutoSize = true;
            this.ultraLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel7.Location = new System.Drawing.Point(245, 31);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(264, 14);
            this.ultraLabel7.TabIndex = 13;
            this.ultraLabel7.Text = "Mức lương tối đa đóng BHXH, BHYT, KPCĐ, BHTN:";
            // 
            // txtReduceSelfTaxAmount
            // 
            appearance12.FontData.BoldAsString = "False";
            appearance12.TextHAlignAsString = "Right";
            this.txtReduceSelfTaxAmount.Appearance = appearance12;
            this.txtReduceSelfTaxAmount.Location = new System.Drawing.Point(122, 53);
            this.txtReduceSelfTaxAmount.MaxValue = new decimal(new int[] {
            -1486618625,
            232830643,
            0,
            0});
            this.txtReduceSelfTaxAmount.Name = "txtReduceSelfTaxAmount";
            this.txtReduceSelfTaxAmount.PromptChar = ' ';
            this.txtReduceSelfTaxAmount.Size = new System.Drawing.Size(115, 21);
            this.txtReduceSelfTaxAmount.TabIndex = 12;
            // 
            // ultraLabel5
            // 
            appearance13.BackColor = System.Drawing.Color.Transparent;
            appearance13.TextHAlignAsString = "Left";
            appearance13.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance13;
            this.ultraLabel5.AutoSize = true;
            this.ultraLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel5.Location = new System.Drawing.Point(12, 58);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(98, 14);
            this.ultraLabel5.TabIndex = 11;
            this.ultraLabel5.Text = "Giảm trừ bản thân:";
            // 
            // txtBasicWage
            // 
            appearance14.FontData.BoldAsString = "False";
            appearance14.TextHAlignAsString = "Right";
            this.txtBasicWage.Appearance = appearance14;
            this.txtBasicWage.Location = new System.Drawing.Point(122, 26);
            this.txtBasicWage.MaxValue = new decimal(new int[] {
            -1486618625,
            232830643,
            0,
            0});
            this.txtBasicWage.Name = "txtBasicWage";
            this.txtBasicWage.PromptChar = ' ';
            this.txtBasicWage.Size = new System.Drawing.Size(115, 21);
            this.txtBasicWage.TabIndex = 10;
            // 
            // ultraLabel2
            // 
            appearance15.BackColor = System.Drawing.Color.Transparent;
            appearance15.TextHAlignAsString = "Left";
            appearance15.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance15;
            this.ultraLabel2.AutoSize = true;
            this.ultraLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel2.Location = new System.Drawing.Point(12, 31);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(89, 14);
            this.ultraLabel2.TabIndex = 9;
            this.ultraLabel2.Text = "Mức lương cơ sở:";
            // 
            // ultraGroupBox17
            // 
            appearance17.BackColor = System.Drawing.Color.Transparent;
            this.ultraGroupBox17.Appearance = appearance17;
            this.ultraGroupBox17.ContentPadding.Top = 10;
            this.ultraGroupBox17.Controls.Add(this.ultraLabel11);
            this.ultraGroupBox17.Controls.Add(this.ultraLabel10);
            this.ultraGroupBox17.Controls.Add(this.ultraLabel40);
            this.ultraGroupBox17.Controls.Add(this.txtOvertimeHolidayPercent);
            this.ultraGroupBox17.Controls.Add(this.txtOvertimeWeekendPercent);
            this.ultraGroupBox17.Controls.Add(this.txtOvertimeDailyPercent);
            this.ultraGroupBox17.Controls.Add(this.ultraLabel62);
            this.ultraGroupBox17.Controls.Add(this.ultraLabel60);
            this.ultraGroupBox17.Controls.Add(this.ultraLabel57);
            this.ultraGroupBox17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            appearance27.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Centered;
            this.ultraGroupBox17.HeaderAppearance = appearance27;
            this.ultraGroupBox17.Location = new System.Drawing.Point(0, 158);
            this.ultraGroupBox17.Name = "ultraGroupBox17";
            this.ultraGroupBox17.Size = new System.Drawing.Size(339, 126);
            this.ultraGroupBox17.TabIndex = 63;
            this.ultraGroupBox17.Text = "Tỷ lệ hưởng lương làm thêm ban ngày";
            this.ultraGroupBox17.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel11
            // 
            appearance18.BackColor = System.Drawing.Color.Transparent;
            appearance18.TextHAlignAsString = "Left";
            appearance18.TextVAlignAsString = "Middle";
            this.ultraLabel11.Appearance = appearance18;
            this.ultraLabel11.AutoSize = true;
            this.ultraLabel11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel11.Location = new System.Drawing.Point(300, 93);
            this.ultraLabel11.Name = "ultraLabel11";
            this.ultraLabel11.Size = new System.Drawing.Size(14, 14);
            this.ultraLabel11.TabIndex = 11;
            this.ultraLabel11.Text = "%";
            // 
            // ultraLabel10
            // 
            appearance19.BackColor = System.Drawing.Color.Transparent;
            appearance19.TextHAlignAsString = "Left";
            appearance19.TextVAlignAsString = "Middle";
            this.ultraLabel10.Appearance = appearance19;
            this.ultraLabel10.AutoSize = true;
            this.ultraLabel10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel10.Location = new System.Drawing.Point(300, 66);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(14, 14);
            this.ultraLabel10.TabIndex = 10;
            this.ultraLabel10.Text = "%";
            // 
            // ultraLabel40
            // 
            appearance20.BackColor = System.Drawing.Color.Transparent;
            appearance20.TextHAlignAsString = "Left";
            appearance20.TextVAlignAsString = "Middle";
            this.ultraLabel40.Appearance = appearance20;
            this.ultraLabel40.AutoSize = true;
            this.ultraLabel40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel40.Location = new System.Drawing.Point(300, 39);
            this.ultraLabel40.Name = "ultraLabel40";
            this.ultraLabel40.Size = new System.Drawing.Size(14, 14);
            this.ultraLabel40.TabIndex = 9;
            this.ultraLabel40.Text = "%";
            // 
            // txtOvertimeHolidayPercent
            // 
            appearance21.FontData.BoldAsString = "False";
            appearance21.TextHAlignAsString = "Right";
            this.txtOvertimeHolidayPercent.Appearance = appearance21;
            this.txtOvertimeHolidayPercent.Location = new System.Drawing.Point(168, 88);
            this.txtOvertimeHolidayPercent.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.txtOvertimeHolidayPercent.Name = "txtOvertimeHolidayPercent";
            this.txtOvertimeHolidayPercent.PromptChar = ' ';
            this.txtOvertimeHolidayPercent.Size = new System.Drawing.Size(126, 21);
            this.txtOvertimeHolidayPercent.TabIndex = 8;
            // 
            // txtOvertimeWeekendPercent
            // 
            appearance22.FontData.BoldAsString = "False";
            appearance22.TextHAlignAsString = "Right";
            this.txtOvertimeWeekendPercent.Appearance = appearance22;
            this.txtOvertimeWeekendPercent.Location = new System.Drawing.Point(168, 61);
            this.txtOvertimeWeekendPercent.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.txtOvertimeWeekendPercent.Name = "txtOvertimeWeekendPercent";
            this.txtOvertimeWeekendPercent.PromptChar = ' ';
            this.txtOvertimeWeekendPercent.Size = new System.Drawing.Size(126, 21);
            this.txtOvertimeWeekendPercent.TabIndex = 8;
            // 
            // txtOvertimeDailyPercent
            // 
            appearance23.FontData.BoldAsString = "False";
            appearance23.TextHAlignAsString = "Right";
            this.txtOvertimeDailyPercent.Appearance = appearance23;
            this.txtOvertimeDailyPercent.Location = new System.Drawing.Point(168, 34);
            this.txtOvertimeDailyPercent.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.txtOvertimeDailyPercent.Name = "txtOvertimeDailyPercent";
            this.txtOvertimeDailyPercent.PromptChar = ' ';
            this.txtOvertimeDailyPercent.Size = new System.Drawing.Size(126, 21);
            this.txtOvertimeDailyPercent.TabIndex = 8;
            // 
            // ultraLabel62
            // 
            appearance24.BackColor = System.Drawing.Color.Transparent;
            appearance24.TextHAlignAsString = "Left";
            appearance24.TextVAlignAsString = "Middle";
            this.ultraLabel62.Appearance = appearance24;
            this.ultraLabel62.AutoSize = true;
            this.ultraLabel62.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel62.Location = new System.Drawing.Point(9, 93);
            this.ultraLabel62.Name = "ultraLabel62";
            this.ultraLabel62.Size = new System.Drawing.Size(84, 14);
            this.ultraLabel62.TabIndex = 3;
            this.ultraLabel62.Text = "Làm ngày lễ tết:";
            // 
            // ultraLabel60
            // 
            appearance25.BackColor = System.Drawing.Color.Transparent;
            appearance25.TextHAlignAsString = "Left";
            appearance25.TextVAlignAsString = "Middle";
            this.ultraLabel60.Appearance = appearance25;
            this.ultraLabel60.AutoSize = true;
            this.ultraLabel60.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel60.Location = new System.Drawing.Point(9, 39);
            this.ultraLabel60.Name = "ultraLabel60";
            this.ultraLabel60.Size = new System.Drawing.Size(95, 14);
            this.ultraLabel60.TabIndex = 3;
            this.ultraLabel60.Text = "Làm ngày thường: ";
            // 
            // ultraLabel57
            // 
            appearance26.BackColor = System.Drawing.Color.Transparent;
            appearance26.TextHAlignAsString = "Left";
            appearance26.TextVAlignAsString = "Middle";
            this.ultraLabel57.Appearance = appearance26;
            this.ultraLabel57.AutoSize = true;
            this.ultraLabel57.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel57.Location = new System.Drawing.Point(9, 66);
            this.ultraLabel57.Name = "ultraLabel57";
            this.ultraLabel57.Size = new System.Drawing.Size(96, 14);
            this.ultraLabel57.TabIndex = 4;
            this.ultraLabel57.Text = "Làm ngày T7, CN:";
            // 
            // ultraGroupBox18
            // 
            appearance28.BackColor = System.Drawing.Color.Transparent;
            this.ultraGroupBox18.Appearance = appearance28;
            this.ultraGroupBox18.ContentPadding.Top = 10;
            this.ultraGroupBox18.Controls.Add(this.ultraLabel38);
            this.ultraGroupBox18.Controls.Add(this.chkCnc);
            this.ultraGroupBox18.Controls.Add(this.ultraLabel39);
            this.ultraGroupBox18.Controls.Add(this.chkT7c);
            this.ultraGroupBox18.Controls.Add(this.ultraLabel9);
            this.ultraGroupBox18.Controls.Add(this.chkCn);
            this.ultraGroupBox18.Controls.Add(this.ultraLabel67);
            this.ultraGroupBox18.Controls.Add(this.chkT7);
            this.ultraGroupBox18.Controls.Add(this.txtWorkHoursInDay);
            this.ultraGroupBox18.Controls.Add(this.ultraLabel8);
            this.ultraGroupBox18.Controls.Add(this.txtWorkDayInMonth);
            this.ultraGroupBox18.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            appearance41.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Centered;
            this.ultraGroupBox18.HeaderAppearance = appearance41;
            this.ultraGroupBox18.Location = new System.Drawing.Point(0, 283);
            this.ultraGroupBox18.Name = "ultraGroupBox18";
            this.ultraGroupBox18.Size = new System.Drawing.Size(689, 89);
            this.ultraGroupBox18.TabIndex = 64;
            this.ultraGroupBox18.Text = "Chấm công";
            this.ultraGroupBox18.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel38
            // 
            appearance29.BackColor = System.Drawing.Color.Transparent;
            appearance29.TextHAlignAsString = "Left";
            appearance29.TextVAlignAsString = "Middle";
            this.ultraLabel38.Appearance = appearance29;
            this.ultraLabel38.AutoSize = true;
            this.ultraLabel38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel38.Location = new System.Drawing.Point(522, 63);
            this.ultraLabel38.Name = "ultraLabel38";
            this.ultraLabel38.Size = new System.Drawing.Size(103, 14);
            this.ultraLabel38.TabIndex = 20;
            this.ultraLabel38.Text = "Làm chiều chủ nhật";
            // 
            // chkCnc
            // 
            appearance30.BackColor = System.Drawing.Color.Transparent;
            this.chkCnc.Appearance = appearance30;
            this.chkCnc.BackColor = System.Drawing.Color.Transparent;
            this.chkCnc.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkCnc.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
            this.chkCnc.Location = new System.Drawing.Point(499, 61);
            this.chkCnc.Name = "chkCnc";
            this.chkCnc.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkCnc.Size = new System.Drawing.Size(17, 16);
            this.chkCnc.TabIndex = 19;
            this.chkCnc.Text = "Làm thêm thứ 7 và chủ nhật .";
            // 
            // ultraLabel39
            // 
            appearance31.BackColor = System.Drawing.Color.Transparent;
            appearance31.TextHAlignAsString = "Left";
            appearance31.TextVAlignAsString = "Middle";
            this.ultraLabel39.Appearance = appearance31;
            this.ultraLabel39.AutoSize = true;
            this.ultraLabel39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel39.Location = new System.Drawing.Point(522, 36);
            this.ultraLabel39.Name = "ultraLabel39";
            this.ultraLabel39.Size = new System.Drawing.Size(84, 14);
            this.ultraLabel39.TabIndex = 18;
            this.ultraLabel39.Text = "Làm chiều thứ 7";
            // 
            // chkT7c
            // 
            appearance32.BackColor = System.Drawing.Color.Transparent;
            this.chkT7c.Appearance = appearance32;
            this.chkT7c.BackColor = System.Drawing.Color.Transparent;
            this.chkT7c.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkT7c.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
            this.chkT7c.Location = new System.Drawing.Point(499, 34);
            this.chkT7c.Name = "chkT7c";
            this.chkT7c.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkT7c.Size = new System.Drawing.Size(17, 16);
            this.chkT7c.TabIndex = 17;
            this.chkT7c.Text = "Làm thêm thứ 7 và chủ nhật .";
            // 
            // ultraLabel9
            // 
            appearance33.BackColor = System.Drawing.Color.Transparent;
            appearance33.TextHAlignAsString = "Left";
            appearance33.TextVAlignAsString = "Middle";
            this.ultraLabel9.Appearance = appearance33;
            this.ultraLabel9.AutoSize = true;
            this.ultraLabel9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel9.Location = new System.Drawing.Point(359, 63);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(100, 14);
            this.ultraLabel9.TabIndex = 16;
            this.ultraLabel9.Text = "Làm sáng chủ nhật";
            // 
            // chkCn
            // 
            appearance34.BackColor = System.Drawing.Color.Transparent;
            this.chkCn.Appearance = appearance34;
            this.chkCn.BackColor = System.Drawing.Color.Transparent;
            this.chkCn.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkCn.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
            this.chkCn.Location = new System.Drawing.Point(336, 61);
            this.chkCn.Name = "chkCn";
            this.chkCn.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkCn.Size = new System.Drawing.Size(17, 16);
            this.chkCn.TabIndex = 15;
            this.chkCn.Text = "Làm thêm thứ 7 và chủ nhật .";
            // 
            // ultraLabel67
            // 
            appearance35.BackColor = System.Drawing.Color.Transparent;
            appearance35.TextHAlignAsString = "Left";
            appearance35.TextVAlignAsString = "Middle";
            this.ultraLabel67.Appearance = appearance35;
            this.ultraLabel67.AutoSize = true;
            this.ultraLabel67.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel67.Location = new System.Drawing.Point(359, 36);
            this.ultraLabel67.Name = "ultraLabel67";
            this.ultraLabel67.Size = new System.Drawing.Size(82, 14);
            this.ultraLabel67.TabIndex = 14;
            this.ultraLabel67.Text = "Làm sáng thứ 7 ";
            // 
            // chkT7
            // 
            appearance36.BackColor = System.Drawing.Color.Transparent;
            this.chkT7.Appearance = appearance36;
            this.chkT7.BackColor = System.Drawing.Color.Transparent;
            this.chkT7.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkT7.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
            this.chkT7.Location = new System.Drawing.Point(336, 34);
            this.chkT7.Name = "chkT7";
            this.chkT7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkT7.Size = new System.Drawing.Size(17, 16);
            this.chkT7.TabIndex = 13;
            this.chkT7.Text = "Làm thêm thứ 7 và chủ nhật .";
            // 
            // txtWorkHoursInDay
            // 
            appearance37.FontData.BoldAsString = "False";
            appearance37.TextHAlignAsString = "Right";
            this.txtWorkHoursInDay.Appearance = appearance37;
            this.txtWorkHoursInDay.Location = new System.Drawing.Point(168, 31);
            this.txtWorkHoursInDay.MaxValue = new decimal(new int[] {
            24,
            0,
            0,
            0});
            this.txtWorkHoursInDay.Name = "txtWorkHoursInDay";
            this.txtWorkHoursInDay.PromptChar = ' ';
            this.txtWorkHoursInDay.Size = new System.Drawing.Size(126, 21);
            this.txtWorkHoursInDay.TabIndex = 12;
            // 
            // ultraLabel8
            // 
            appearance38.BackColor = System.Drawing.Color.Transparent;
            appearance38.TextHAlignAsString = "Left";
            appearance38.TextVAlignAsString = "Middle";
            this.ultraLabel8.Appearance = appearance38;
            this.ultraLabel8.AutoSize = true;
            this.ultraLabel8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel8.Location = new System.Drawing.Point(9, 36);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(137, 14);
            this.ultraLabel8.TabIndex = 11;
            this.ultraLabel8.Text = "Số giờ làm việc trong ngày";
            // 
            // txtWorkDayInMonth
            // 
            appearance39.FontData.BoldAsString = "False";
            appearance39.TextHAlignAsString = "Right";
            this.txtWorkDayInMonth.Appearance = appearance39;
            this.txtWorkDayInMonth.Location = new System.Drawing.Point(168, 31);
            this.txtWorkDayInMonth.MaxValue = new decimal(new int[] {
            31,
            0,
            0,
            0});
            this.txtWorkDayInMonth.Name = "txtWorkDayInMonth";
            this.txtWorkDayInMonth.PromptChar = ' ';
            this.txtWorkDayInMonth.Size = new System.Drawing.Size(126, 21);
            this.txtWorkDayInMonth.TabIndex = 10;
            this.txtWorkDayInMonth.Visible = false;
            // 
            // ultraLabel1
            // 
            appearance40.BackColor = System.Drawing.Color.Transparent;
            appearance40.TextHAlignAsString = "Left";
            appearance40.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance40;
            this.ultraLabel1.AutoSize = true;
            this.ultraLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel1.Location = new System.Drawing.Point(9, 36);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(153, 14);
            this.ultraLabel1.TabIndex = 9;
            this.ultraLabel1.Text = "Số ngày làm việc trong tháng:";
            this.ultraLabel1.Visible = false;
            // 
            // ultraGroupBox1
            // 
            appearance42.BackColor = System.Drawing.Color.Transparent;
            this.ultraGroupBox1.Appearance = appearance42;
            this.ultraGroupBox1.ContentPadding.Top = 10;
            this.ultraGroupBox1.Controls.Add(this.ultraLabel12);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel13);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel14);
            this.ultraGroupBox1.Controls.Add(this.txtOvertimeHolidayNightPercent);
            this.ultraGroupBox1.Controls.Add(this.txtOvertimeWeekendDayNightPercent);
            this.ultraGroupBox1.Controls.Add(this.txtOvertimeWorkingDayNightPercent);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel15);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel16);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel17);
            this.ultraGroupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            appearance52.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Centered;
            this.ultraGroupBox1.HeaderAppearance = appearance52;
            this.ultraGroupBox1.Location = new System.Drawing.Point(336, 158);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(353, 126);
            this.ultraGroupBox1.TabIndex = 64;
            this.ultraGroupBox1.Text = "Tỷ lệ hưởng lương làm thêm ban đêm";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel12
            // 
            appearance43.BackColor = System.Drawing.Color.Transparent;
            appearance43.TextHAlignAsString = "Left";
            appearance43.TextVAlignAsString = "Middle";
            this.ultraLabel12.Appearance = appearance43;
            this.ultraLabel12.AutoSize = true;
            this.ultraLabel12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel12.Location = new System.Drawing.Point(300, 93);
            this.ultraLabel12.Name = "ultraLabel12";
            this.ultraLabel12.Size = new System.Drawing.Size(14, 14);
            this.ultraLabel12.TabIndex = 11;
            this.ultraLabel12.Text = "%";
            // 
            // ultraLabel13
            // 
            appearance44.BackColor = System.Drawing.Color.Transparent;
            appearance44.TextHAlignAsString = "Left";
            appearance44.TextVAlignAsString = "Middle";
            this.ultraLabel13.Appearance = appearance44;
            this.ultraLabel13.AutoSize = true;
            this.ultraLabel13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel13.Location = new System.Drawing.Point(300, 66);
            this.ultraLabel13.Name = "ultraLabel13";
            this.ultraLabel13.Size = new System.Drawing.Size(14, 14);
            this.ultraLabel13.TabIndex = 10;
            this.ultraLabel13.Text = "%";
            // 
            // ultraLabel14
            // 
            appearance45.BackColor = System.Drawing.Color.Transparent;
            appearance45.TextHAlignAsString = "Left";
            appearance45.TextVAlignAsString = "Middle";
            this.ultraLabel14.Appearance = appearance45;
            this.ultraLabel14.AutoSize = true;
            this.ultraLabel14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel14.Location = new System.Drawing.Point(300, 39);
            this.ultraLabel14.Name = "ultraLabel14";
            this.ultraLabel14.Size = new System.Drawing.Size(14, 14);
            this.ultraLabel14.TabIndex = 9;
            this.ultraLabel14.Text = "%";
            // 
            // txtOvertimeHolidayNightPercent
            // 
            appearance46.FontData.BoldAsString = "False";
            appearance46.TextHAlignAsString = "Right";
            this.txtOvertimeHolidayNightPercent.Appearance = appearance46;
            this.txtOvertimeHolidayNightPercent.Location = new System.Drawing.Point(168, 88);
            this.txtOvertimeHolidayNightPercent.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.txtOvertimeHolidayNightPercent.Name = "txtOvertimeHolidayNightPercent";
            this.txtOvertimeHolidayNightPercent.PromptChar = ' ';
            this.txtOvertimeHolidayNightPercent.Size = new System.Drawing.Size(126, 21);
            this.txtOvertimeHolidayNightPercent.TabIndex = 8;
            // 
            // txtOvertimeWeekendDayNightPercent
            // 
            appearance47.FontData.BoldAsString = "False";
            appearance47.TextHAlignAsString = "Right";
            this.txtOvertimeWeekendDayNightPercent.Appearance = appearance47;
            this.txtOvertimeWeekendDayNightPercent.Location = new System.Drawing.Point(168, 61);
            this.txtOvertimeWeekendDayNightPercent.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.txtOvertimeWeekendDayNightPercent.Name = "txtOvertimeWeekendDayNightPercent";
            this.txtOvertimeWeekendDayNightPercent.PromptChar = ' ';
            this.txtOvertimeWeekendDayNightPercent.Size = new System.Drawing.Size(126, 21);
            this.txtOvertimeWeekendDayNightPercent.TabIndex = 8;
            // 
            // txtOvertimeWorkingDayNightPercent
            // 
            appearance48.FontData.BoldAsString = "False";
            appearance48.TextHAlignAsString = "Right";
            this.txtOvertimeWorkingDayNightPercent.Appearance = appearance48;
            this.txtOvertimeWorkingDayNightPercent.Location = new System.Drawing.Point(168, 34);
            this.txtOvertimeWorkingDayNightPercent.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.txtOvertimeWorkingDayNightPercent.Name = "txtOvertimeWorkingDayNightPercent";
            this.txtOvertimeWorkingDayNightPercent.PromptChar = ' ';
            this.txtOvertimeWorkingDayNightPercent.Size = new System.Drawing.Size(126, 21);
            this.txtOvertimeWorkingDayNightPercent.TabIndex = 8;
            // 
            // ultraLabel15
            // 
            appearance49.BackColor = System.Drawing.Color.Transparent;
            appearance49.TextHAlignAsString = "Left";
            appearance49.TextVAlignAsString = "Middle";
            this.ultraLabel15.Appearance = appearance49;
            this.ultraLabel15.AutoSize = true;
            this.ultraLabel15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel15.Location = new System.Drawing.Point(9, 93);
            this.ultraLabel15.Name = "ultraLabel15";
            this.ultraLabel15.Size = new System.Drawing.Size(84, 14);
            this.ultraLabel15.TabIndex = 3;
            this.ultraLabel15.Text = "Làm ngày lễ tết:";
            // 
            // ultraLabel16
            // 
            appearance50.BackColor = System.Drawing.Color.Transparent;
            appearance50.TextHAlignAsString = "Left";
            appearance50.TextVAlignAsString = "Middle";
            this.ultraLabel16.Appearance = appearance50;
            this.ultraLabel16.AutoSize = true;
            this.ultraLabel16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel16.Location = new System.Drawing.Point(9, 39);
            this.ultraLabel16.Name = "ultraLabel16";
            this.ultraLabel16.Size = new System.Drawing.Size(95, 14);
            this.ultraLabel16.TabIndex = 3;
            this.ultraLabel16.Text = "Làm ngày thường: ";
            // 
            // ultraLabel17
            // 
            appearance51.BackColor = System.Drawing.Color.Transparent;
            appearance51.TextHAlignAsString = "Left";
            appearance51.TextVAlignAsString = "Middle";
            this.ultraLabel17.Appearance = appearance51;
            this.ultraLabel17.AutoSize = true;
            this.ultraLabel17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel17.Location = new System.Drawing.Point(9, 66);
            this.ultraLabel17.Name = "ultraLabel17";
            this.ultraLabel17.Size = new System.Drawing.Size(96, 14);
            this.ultraLabel17.TabIndex = 4;
            this.ultraLabel17.Text = "Làm ngày T7, CN:";
            // 
            // ultraGroupBox2
            // 
            appearance53.BackColor = System.Drawing.Color.Transparent;
            this.ultraGroupBox2.Appearance = appearance53;
            this.ultraGroupBox2.ContentPadding.Top = 10;
            this.ultraGroupBox2.Controls.Add(this.ultraLabel34);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel18);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel35);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel19);
            this.ultraGroupBox2.Controls.Add(this.txtEmployeeTradeUnionInsurancePercent);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel36);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel20);
            this.ultraGroupBox2.Controls.Add(this.txtEmployeeAccidentInsurancePercent);
            this.ultraGroupBox2.Controls.Add(this.txtEmployeeUnEmployeeInsurancePercent);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel37);
            this.ultraGroupBox2.Controls.Add(this.txtEmployeeMedicalInsurancePercent);
            this.ultraGroupBox2.Controls.Add(this.txtEmployeeSocityInsurancePercent);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel21);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel22);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel23);
            this.ultraGroupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            appearance69.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Centered;
            this.ultraGroupBox2.HeaderAppearance = appearance69;
            this.ultraGroupBox2.Location = new System.Drawing.Point(336, 373);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(353, 168);
            this.ultraGroupBox2.TabIndex = 66;
            this.ultraGroupBox2.Text = "Bảo hiểm người lao động đóng";
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel34
            // 
            appearance54.BackColor = System.Drawing.Color.Transparent;
            appearance54.TextHAlignAsString = "Left";
            appearance54.TextVAlignAsString = "Middle";
            this.ultraLabel34.Appearance = appearance54;
            this.ultraLabel34.AutoSize = true;
            this.ultraLabel34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel34.Location = new System.Drawing.Point(300, 144);
            this.ultraLabel34.Name = "ultraLabel34";
            this.ultraLabel34.Size = new System.Drawing.Size(14, 14);
            this.ultraLabel34.TabIndex = 23;
            this.ultraLabel34.Text = "%";
            // 
            // ultraLabel18
            // 
            appearance55.BackColor = System.Drawing.Color.Transparent;
            appearance55.TextHAlignAsString = "Left";
            appearance55.TextVAlignAsString = "Middle";
            this.ultraLabel18.Appearance = appearance55;
            this.ultraLabel18.AutoSize = true;
            this.ultraLabel18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel18.Location = new System.Drawing.Point(300, 93);
            this.ultraLabel18.Name = "ultraLabel18";
            this.ultraLabel18.Size = new System.Drawing.Size(14, 14);
            this.ultraLabel18.TabIndex = 11;
            this.ultraLabel18.Text = "%";
            // 
            // ultraLabel35
            // 
            appearance56.BackColor = System.Drawing.Color.Transparent;
            appearance56.TextHAlignAsString = "Left";
            appearance56.TextVAlignAsString = "Middle";
            this.ultraLabel35.Appearance = appearance56;
            this.ultraLabel35.AutoSize = true;
            this.ultraLabel35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel35.Location = new System.Drawing.Point(300, 118);
            this.ultraLabel35.Name = "ultraLabel35";
            this.ultraLabel35.Size = new System.Drawing.Size(14, 14);
            this.ultraLabel35.TabIndex = 22;
            this.ultraLabel35.Text = "%";
            // 
            // ultraLabel19
            // 
            appearance57.BackColor = System.Drawing.Color.Transparent;
            appearance57.TextHAlignAsString = "Left";
            appearance57.TextVAlignAsString = "Middle";
            this.ultraLabel19.Appearance = appearance57;
            this.ultraLabel19.AutoSize = true;
            this.ultraLabel19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel19.Location = new System.Drawing.Point(300, 66);
            this.ultraLabel19.Name = "ultraLabel19";
            this.ultraLabel19.Size = new System.Drawing.Size(14, 14);
            this.ultraLabel19.TabIndex = 10;
            this.ultraLabel19.Text = "%";
            // 
            // txtEmployeeTradeUnionInsurancePercent
            // 
            appearance58.FontData.BoldAsString = "False";
            appearance58.TextHAlignAsString = "Right";
            this.txtEmployeeTradeUnionInsurancePercent.Appearance = appearance58;
            this.txtEmployeeTradeUnionInsurancePercent.Location = new System.Drawing.Point(168, 139);
            this.txtEmployeeTradeUnionInsurancePercent.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.txtEmployeeTradeUnionInsurancePercent.Name = "txtEmployeeTradeUnionInsurancePercent";
            this.txtEmployeeTradeUnionInsurancePercent.PromptChar = ' ';
            this.txtEmployeeTradeUnionInsurancePercent.Size = new System.Drawing.Size(126, 21);
            this.txtEmployeeTradeUnionInsurancePercent.TabIndex = 21;
            // 
            // ultraLabel36
            // 
            appearance59.BackColor = System.Drawing.Color.Transparent;
            appearance59.TextHAlignAsString = "Left";
            appearance59.TextVAlignAsString = "Middle";
            this.ultraLabel36.Appearance = appearance59;
            this.ultraLabel36.AutoSize = true;
            this.ultraLabel36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel36.Location = new System.Drawing.Point(9, 144);
            this.ultraLabel36.Name = "ultraLabel36";
            this.ultraLabel36.Size = new System.Drawing.Size(104, 14);
            this.ultraLabel36.TabIndex = 20;
            this.ultraLabel36.Text = "Kinh phí công đoàn:";
            // 
            // ultraLabel20
            // 
            appearance60.BackColor = System.Drawing.Color.Transparent;
            appearance60.TextHAlignAsString = "Left";
            appearance60.TextVAlignAsString = "Middle";
            this.ultraLabel20.Appearance = appearance60;
            this.ultraLabel20.AutoSize = true;
            this.ultraLabel20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel20.Location = new System.Drawing.Point(300, 39);
            this.ultraLabel20.Name = "ultraLabel20";
            this.ultraLabel20.Size = new System.Drawing.Size(14, 14);
            this.ultraLabel20.TabIndex = 9;
            this.ultraLabel20.Text = "%";
            // 
            // txtEmployeeAccidentInsurancePercent
            // 
            appearance61.FontData.BoldAsString = "False";
            appearance61.TextHAlignAsString = "Right";
            this.txtEmployeeAccidentInsurancePercent.Appearance = appearance61;
            this.txtEmployeeAccidentInsurancePercent.Location = new System.Drawing.Point(168, 113);
            this.txtEmployeeAccidentInsurancePercent.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.txtEmployeeAccidentInsurancePercent.Name = "txtEmployeeAccidentInsurancePercent";
            this.txtEmployeeAccidentInsurancePercent.PromptChar = ' ';
            this.txtEmployeeAccidentInsurancePercent.Size = new System.Drawing.Size(126, 21);
            this.txtEmployeeAccidentInsurancePercent.TabIndex = 19;
            // 
            // txtEmployeeUnEmployeeInsurancePercent
            // 
            appearance62.FontData.BoldAsString = "False";
            appearance62.TextHAlignAsString = "Right";
            this.txtEmployeeUnEmployeeInsurancePercent.Appearance = appearance62;
            this.txtEmployeeUnEmployeeInsurancePercent.Location = new System.Drawing.Point(168, 88);
            this.txtEmployeeUnEmployeeInsurancePercent.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.txtEmployeeUnEmployeeInsurancePercent.Name = "txtEmployeeUnEmployeeInsurancePercent";
            this.txtEmployeeUnEmployeeInsurancePercent.PromptChar = ' ';
            this.txtEmployeeUnEmployeeInsurancePercent.Size = new System.Drawing.Size(126, 21);
            this.txtEmployeeUnEmployeeInsurancePercent.TabIndex = 8;
            // 
            // ultraLabel37
            // 
            appearance63.BackColor = System.Drawing.Color.Transparent;
            appearance63.TextHAlignAsString = "Left";
            appearance63.TextVAlignAsString = "Middle";
            this.ultraLabel37.Appearance = appearance63;
            this.ultraLabel37.AutoSize = true;
            this.ultraLabel37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel37.Location = new System.Drawing.Point(9, 118);
            this.ultraLabel37.Name = "ultraLabel37";
            this.ultraLabel37.Size = new System.Drawing.Size(138, 14);
            this.ultraLabel37.TabIndex = 18;
            this.ultraLabel37.Text = "Bảo hiểm tại nạn lao động:";
            // 
            // txtEmployeeMedicalInsurancePercent
            // 
            appearance64.FontData.BoldAsString = "False";
            appearance64.TextHAlignAsString = "Right";
            this.txtEmployeeMedicalInsurancePercent.Appearance = appearance64;
            this.txtEmployeeMedicalInsurancePercent.Location = new System.Drawing.Point(168, 61);
            this.txtEmployeeMedicalInsurancePercent.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.txtEmployeeMedicalInsurancePercent.Name = "txtEmployeeMedicalInsurancePercent";
            this.txtEmployeeMedicalInsurancePercent.PromptChar = ' ';
            this.txtEmployeeMedicalInsurancePercent.Size = new System.Drawing.Size(126, 21);
            this.txtEmployeeMedicalInsurancePercent.TabIndex = 8;
            // 
            // txtEmployeeSocityInsurancePercent
            // 
            appearance65.FontData.BoldAsString = "False";
            appearance65.TextHAlignAsString = "Right";
            this.txtEmployeeSocityInsurancePercent.Appearance = appearance65;
            this.txtEmployeeSocityInsurancePercent.Location = new System.Drawing.Point(168, 34);
            this.txtEmployeeSocityInsurancePercent.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.txtEmployeeSocityInsurancePercent.Name = "txtEmployeeSocityInsurancePercent";
            this.txtEmployeeSocityInsurancePercent.PromptChar = ' ';
            this.txtEmployeeSocityInsurancePercent.Size = new System.Drawing.Size(126, 21);
            this.txtEmployeeSocityInsurancePercent.TabIndex = 8;
            // 
            // ultraLabel21
            // 
            appearance66.BackColor = System.Drawing.Color.Transparent;
            appearance66.TextHAlignAsString = "Left";
            appearance66.TextVAlignAsString = "Middle";
            this.ultraLabel21.Appearance = appearance66;
            this.ultraLabel21.AutoSize = true;
            this.ultraLabel21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel21.Location = new System.Drawing.Point(9, 93);
            this.ultraLabel21.Name = "ultraLabel21";
            this.ultraLabel21.Size = new System.Drawing.Size(114, 14);
            this.ultraLabel21.TabIndex = 3;
            this.ultraLabel21.Text = "Bảo hiểm thất nghiệp:";
            // 
            // ultraLabel22
            // 
            appearance67.BackColor = System.Drawing.Color.Transparent;
            appearance67.TextHAlignAsString = "Left";
            appearance67.TextVAlignAsString = "Middle";
            this.ultraLabel22.Appearance = appearance67;
            this.ultraLabel22.AutoSize = true;
            this.ultraLabel22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel22.Location = new System.Drawing.Point(9, 39);
            this.ultraLabel22.Name = "ultraLabel22";
            this.ultraLabel22.Size = new System.Drawing.Size(88, 14);
            this.ultraLabel22.TabIndex = 3;
            this.ultraLabel22.Text = "Bảo hiểm xã hội:";
            // 
            // ultraLabel23
            // 
            appearance68.BackColor = System.Drawing.Color.Transparent;
            appearance68.TextHAlignAsString = "Left";
            appearance68.TextVAlignAsString = "Middle";
            this.ultraLabel23.Appearance = appearance68;
            this.ultraLabel23.AutoSize = true;
            this.ultraLabel23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel23.Location = new System.Drawing.Point(9, 66);
            this.ultraLabel23.Name = "ultraLabel23";
            this.ultraLabel23.Size = new System.Drawing.Size(76, 14);
            this.ultraLabel23.TabIndex = 4;
            this.ultraLabel23.Text = "Bảo hiểm y tế:";
            // 
            // ultraGroupBox3
            // 
            appearance70.BackColor = System.Drawing.Color.Transparent;
            this.ultraGroupBox3.Appearance = appearance70;
            this.ultraGroupBox3.ContentPadding.Top = 10;
            this.ultraGroupBox3.Controls.Add(this.ultraLabel33);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel32);
            this.ultraGroupBox3.Controls.Add(this.txtCompanyTradeUnionInsurancePercent);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel31);
            this.ultraGroupBox3.Controls.Add(this.txtCompanytAccidentInsurancePercent);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel30);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel24);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel25);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel26);
            this.ultraGroupBox3.Controls.Add(this.txtCompanyUnEmployeeInsurancePercent);
            this.ultraGroupBox3.Controls.Add(this.txtCompanyMedicalInsurancePercent);
            this.ultraGroupBox3.Controls.Add(this.txtCompanySocityInsurancePercent);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel27);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel28);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel29);
            this.ultraGroupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            appearance86.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Centered;
            this.ultraGroupBox3.HeaderAppearance = appearance86;
            this.ultraGroupBox3.Location = new System.Drawing.Point(0, 373);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(339, 168);
            this.ultraGroupBox3.TabIndex = 65;
            this.ultraGroupBox3.Text = "Bảo hiểm công ty đóng";
            this.ultraGroupBox3.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel33
            // 
            appearance71.BackColor = System.Drawing.Color.Transparent;
            appearance71.TextHAlignAsString = "Left";
            appearance71.TextVAlignAsString = "Middle";
            this.ultraLabel33.Appearance = appearance71;
            this.ultraLabel33.AutoSize = true;
            this.ultraLabel33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel33.Location = new System.Drawing.Point(300, 146);
            this.ultraLabel33.Name = "ultraLabel33";
            this.ultraLabel33.Size = new System.Drawing.Size(14, 14);
            this.ultraLabel33.TabIndex = 17;
            this.ultraLabel33.Text = "%";
            // 
            // ultraLabel32
            // 
            appearance72.BackColor = System.Drawing.Color.Transparent;
            appearance72.TextHAlignAsString = "Left";
            appearance72.TextVAlignAsString = "Middle";
            this.ultraLabel32.Appearance = appearance72;
            this.ultraLabel32.AutoSize = true;
            this.ultraLabel32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel32.Location = new System.Drawing.Point(300, 120);
            this.ultraLabel32.Name = "ultraLabel32";
            this.ultraLabel32.Size = new System.Drawing.Size(14, 14);
            this.ultraLabel32.TabIndex = 16;
            this.ultraLabel32.Text = "%";
            // 
            // txtCompanyTradeUnionInsurancePercent
            // 
            appearance73.FontData.BoldAsString = "False";
            appearance73.TextHAlignAsString = "Right";
            this.txtCompanyTradeUnionInsurancePercent.Appearance = appearance73;
            this.txtCompanyTradeUnionInsurancePercent.Location = new System.Drawing.Point(168, 141);
            this.txtCompanyTradeUnionInsurancePercent.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.txtCompanyTradeUnionInsurancePercent.Name = "txtCompanyTradeUnionInsurancePercent";
            this.txtCompanyTradeUnionInsurancePercent.PromptChar = ' ';
            this.txtCompanyTradeUnionInsurancePercent.Size = new System.Drawing.Size(126, 21);
            this.txtCompanyTradeUnionInsurancePercent.TabIndex = 15;
            // 
            // ultraLabel31
            // 
            appearance74.BackColor = System.Drawing.Color.Transparent;
            appearance74.TextHAlignAsString = "Left";
            appearance74.TextVAlignAsString = "Middle";
            this.ultraLabel31.Appearance = appearance74;
            this.ultraLabel31.AutoSize = true;
            this.ultraLabel31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel31.Location = new System.Drawing.Point(9, 146);
            this.ultraLabel31.Name = "ultraLabel31";
            this.ultraLabel31.Size = new System.Drawing.Size(104, 14);
            this.ultraLabel31.TabIndex = 14;
            this.ultraLabel31.Text = "Kinh phí công đoàn:";
            // 
            // txtCompanytAccidentInsurancePercent
            // 
            appearance75.FontData.BoldAsString = "False";
            appearance75.TextHAlignAsString = "Right";
            this.txtCompanytAccidentInsurancePercent.Appearance = appearance75;
            this.txtCompanytAccidentInsurancePercent.Location = new System.Drawing.Point(168, 115);
            this.txtCompanytAccidentInsurancePercent.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.txtCompanytAccidentInsurancePercent.Name = "txtCompanytAccidentInsurancePercent";
            this.txtCompanytAccidentInsurancePercent.PromptChar = ' ';
            this.txtCompanytAccidentInsurancePercent.Size = new System.Drawing.Size(126, 21);
            this.txtCompanytAccidentInsurancePercent.TabIndex = 13;
            // 
            // ultraLabel30
            // 
            appearance76.BackColor = System.Drawing.Color.Transparent;
            appearance76.TextHAlignAsString = "Left";
            appearance76.TextVAlignAsString = "Middle";
            this.ultraLabel30.Appearance = appearance76;
            this.ultraLabel30.AutoSize = true;
            this.ultraLabel30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel30.Location = new System.Drawing.Point(9, 120);
            this.ultraLabel30.Name = "ultraLabel30";
            this.ultraLabel30.Size = new System.Drawing.Size(138, 14);
            this.ultraLabel30.TabIndex = 12;
            this.ultraLabel30.Text = "Bảo hiểm tại nạn lao động:";
            // 
            // ultraLabel24
            // 
            appearance77.BackColor = System.Drawing.Color.Transparent;
            appearance77.TextHAlignAsString = "Left";
            appearance77.TextVAlignAsString = "Middle";
            this.ultraLabel24.Appearance = appearance77;
            this.ultraLabel24.AutoSize = true;
            this.ultraLabel24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel24.Location = new System.Drawing.Point(300, 93);
            this.ultraLabel24.Name = "ultraLabel24";
            this.ultraLabel24.Size = new System.Drawing.Size(14, 14);
            this.ultraLabel24.TabIndex = 11;
            this.ultraLabel24.Text = "%";
            // 
            // ultraLabel25
            // 
            appearance78.BackColor = System.Drawing.Color.Transparent;
            appearance78.TextHAlignAsString = "Left";
            appearance78.TextVAlignAsString = "Middle";
            this.ultraLabel25.Appearance = appearance78;
            this.ultraLabel25.AutoSize = true;
            this.ultraLabel25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel25.Location = new System.Drawing.Point(300, 66);
            this.ultraLabel25.Name = "ultraLabel25";
            this.ultraLabel25.Size = new System.Drawing.Size(14, 14);
            this.ultraLabel25.TabIndex = 10;
            this.ultraLabel25.Text = "%";
            // 
            // ultraLabel26
            // 
            appearance79.BackColor = System.Drawing.Color.Transparent;
            appearance79.TextHAlignAsString = "Left";
            appearance79.TextVAlignAsString = "Middle";
            this.ultraLabel26.Appearance = appearance79;
            this.ultraLabel26.AutoSize = true;
            this.ultraLabel26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel26.Location = new System.Drawing.Point(300, 39);
            this.ultraLabel26.Name = "ultraLabel26";
            this.ultraLabel26.Size = new System.Drawing.Size(14, 14);
            this.ultraLabel26.TabIndex = 9;
            this.ultraLabel26.Text = "%";
            // 
            // txtCompanyUnEmployeeInsurancePercent
            // 
            appearance80.FontData.BoldAsString = "False";
            appearance80.TextHAlignAsString = "Right";
            this.txtCompanyUnEmployeeInsurancePercent.Appearance = appearance80;
            this.txtCompanyUnEmployeeInsurancePercent.Location = new System.Drawing.Point(168, 88);
            this.txtCompanyUnEmployeeInsurancePercent.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.txtCompanyUnEmployeeInsurancePercent.Name = "txtCompanyUnEmployeeInsurancePercent";
            this.txtCompanyUnEmployeeInsurancePercent.PromptChar = ' ';
            this.txtCompanyUnEmployeeInsurancePercent.Size = new System.Drawing.Size(126, 21);
            this.txtCompanyUnEmployeeInsurancePercent.TabIndex = 8;
            // 
            // txtCompanyMedicalInsurancePercent
            // 
            appearance81.FontData.BoldAsString = "False";
            appearance81.TextHAlignAsString = "Right";
            this.txtCompanyMedicalInsurancePercent.Appearance = appearance81;
            this.txtCompanyMedicalInsurancePercent.Location = new System.Drawing.Point(168, 61);
            this.txtCompanyMedicalInsurancePercent.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.txtCompanyMedicalInsurancePercent.Name = "txtCompanyMedicalInsurancePercent";
            this.txtCompanyMedicalInsurancePercent.PromptChar = ' ';
            this.txtCompanyMedicalInsurancePercent.Size = new System.Drawing.Size(126, 21);
            this.txtCompanyMedicalInsurancePercent.TabIndex = 8;
            // 
            // txtCompanySocityInsurancePercent
            // 
            appearance82.FontData.BoldAsString = "False";
            appearance82.TextHAlignAsString = "Right";
            this.txtCompanySocityInsurancePercent.Appearance = appearance82;
            this.txtCompanySocityInsurancePercent.Location = new System.Drawing.Point(168, 34);
            this.txtCompanySocityInsurancePercent.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.txtCompanySocityInsurancePercent.Name = "txtCompanySocityInsurancePercent";
            this.txtCompanySocityInsurancePercent.PromptChar = ' ';
            this.txtCompanySocityInsurancePercent.Size = new System.Drawing.Size(126, 21);
            this.txtCompanySocityInsurancePercent.TabIndex = 8;
            // 
            // ultraLabel27
            // 
            appearance83.BackColor = System.Drawing.Color.Transparent;
            appearance83.TextHAlignAsString = "Left";
            appearance83.TextVAlignAsString = "Middle";
            this.ultraLabel27.Appearance = appearance83;
            this.ultraLabel27.AutoSize = true;
            this.ultraLabel27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel27.Location = new System.Drawing.Point(9, 93);
            this.ultraLabel27.Name = "ultraLabel27";
            this.ultraLabel27.Size = new System.Drawing.Size(114, 14);
            this.ultraLabel27.TabIndex = 3;
            this.ultraLabel27.Text = "Bảo hiểm thất nghiệp:";
            // 
            // ultraLabel28
            // 
            appearance84.BackColor = System.Drawing.Color.Transparent;
            appearance84.TextHAlignAsString = "Left";
            appearance84.TextVAlignAsString = "Middle";
            this.ultraLabel28.Appearance = appearance84;
            this.ultraLabel28.AutoSize = true;
            this.ultraLabel28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel28.Location = new System.Drawing.Point(9, 39);
            this.ultraLabel28.Name = "ultraLabel28";
            this.ultraLabel28.Size = new System.Drawing.Size(88, 14);
            this.ultraLabel28.TabIndex = 3;
            this.ultraLabel28.Text = "Bảo hiểm xã hội:";
            // 
            // ultraLabel29
            // 
            appearance85.BackColor = System.Drawing.Color.Transparent;
            appearance85.TextHAlignAsString = "Left";
            appearance85.TextVAlignAsString = "Middle";
            this.ultraLabel29.Appearance = appearance85;
            this.ultraLabel29.AutoSize = true;
            this.ultraLabel29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel29.Location = new System.Drawing.Point(9, 66);
            this.ultraLabel29.Name = "ultraLabel29";
            this.ultraLabel29.Size = new System.Drawing.Size(76, 14);
            this.ultraLabel29.TabIndex = 4;
            this.ultraLabel29.Text = "Bảo hiểm y tế:";
            // 
            // FPSSalaryTaxInsuranceRegulationDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(689, 585);
            this.Controls.Add(this.ultraGroupBox2);
            this.Controls.Add(this.ultraGroupBox3);
            this.Controls.Add(this.ultraGroupBox1);
            this.Controls.Add(this.ultraGroupBox18);
            this.Controls.Add(this.ultraGroupBox17);
            this.Controls.Add(this.ultraGroupBox16);
            this.Controls.Add(this.ultraGroupBox15);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FPSSalaryTaxInsuranceRegulationDetail";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quy định lương, thuế, bảo hiểm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FPSSalaryTaxInsuranceRegulationDetail_FormClosed);
            this.tsmFixedAssetDetail.ResumeLayout(false);
            this.tsmFixedAssetAccessories.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox15)).EndInit();
            this.ultraGroupBox15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dteDateTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDateFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox16)).EndInit();
            this.ultraGroupBox16.ResumeLayout(false);
            this.ultraGroupBox16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox17)).EndInit();
            this.ultraGroupBox17.ResumeLayout(false);
            this.ultraGroupBox17.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox18)).EndInit();
            this.ultraGroupBox18.ResumeLayout(false);
            this.ultraGroupBox18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkCnc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkT7c)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkT7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ultraGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            this.ultraGroupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Core.Domain.FixedAsset temp;
        private System.Windows.Forms.ContextMenuStrip tsmFixedAssetAccessories;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripMenuItem tsmDelete;
        private System.Windows.Forms.ContextMenuStrip tsmFixedAssetDetail;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd1;
        private System.Windows.Forms.ToolStripMenuItem tsmDelete1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox15;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox16;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteDateTo;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteDateFrom;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox17;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtOvertimeHolidayPercent;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtOvertimeWeekendPercent;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtOvertimeDailyPercent;
        private Infragistics.Win.Misc.UltraLabel ultraLabel62;
        private Infragistics.Win.Misc.UltraLabel ultraLabel60;
        private Infragistics.Win.Misc.UltraLabel ultraLabel57;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtReduceDependTaxAmount;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtInsuaranceMaximumizeSalary;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtReduceSelfTaxAmount;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtBasicWage;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox18;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtWorkDayInMonth;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtWorkHoursInDay;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkT7;
        private Infragistics.Win.Misc.UltraLabel ultraLabel67;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkCn;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.Misc.UltraLabel ultraLabel40;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel12;
        private Infragistics.Win.Misc.UltraLabel ultraLabel13;
        private Infragistics.Win.Misc.UltraLabel ultraLabel14;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtOvertimeHolidayNightPercent;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtOvertimeWeekendDayNightPercent;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtOvertimeWorkingDayNightPercent;
        private Infragistics.Win.Misc.UltraLabel ultraLabel15;
        private Infragistics.Win.Misc.UltraLabel ultraLabel16;
        private Infragistics.Win.Misc.UltraLabel ultraLabel17;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel34;
        private Infragistics.Win.Misc.UltraLabel ultraLabel18;
        private Infragistics.Win.Misc.UltraLabel ultraLabel35;
        private Infragistics.Win.Misc.UltraLabel ultraLabel19;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtEmployeeTradeUnionInsurancePercent;
        private Infragistics.Win.Misc.UltraLabel ultraLabel36;
        private Infragistics.Win.Misc.UltraLabel ultraLabel20;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtEmployeeAccidentInsurancePercent;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtEmployeeUnEmployeeInsurancePercent;
        private Infragistics.Win.Misc.UltraLabel ultraLabel37;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtEmployeeMedicalInsurancePercent;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtEmployeeSocityInsurancePercent;
        private Infragistics.Win.Misc.UltraLabel ultraLabel21;
        private Infragistics.Win.Misc.UltraLabel ultraLabel22;
        private Infragistics.Win.Misc.UltraLabel ultraLabel23;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel33;
        private Infragistics.Win.Misc.UltraLabel ultraLabel32;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtCompanyTradeUnionInsurancePercent;
        private Infragistics.Win.Misc.UltraLabel ultraLabel31;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtCompanytAccidentInsurancePercent;
        private Infragistics.Win.Misc.UltraLabel ultraLabel30;
        private Infragistics.Win.Misc.UltraLabel ultraLabel24;
        private Infragistics.Win.Misc.UltraLabel ultraLabel25;
        private Infragistics.Win.Misc.UltraLabel ultraLabel26;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtCompanyUnEmployeeInsurancePercent;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtCompanyMedicalInsurancePercent;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtCompanySocityInsurancePercent;
        private Infragistics.Win.Misc.UltraLabel ultraLabel27;
        private Infragistics.Win.Misc.UltraLabel ultraLabel28;
        private Infragistics.Win.Misc.UltraLabel ultraLabel29;
        private Infragistics.Win.Misc.UltraLabel ultraLabel38;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkCnc;
        private Infragistics.Win.Misc.UltraLabel ultraLabel39;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkT7c;
    }
}