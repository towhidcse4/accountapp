﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using Accounting.TextMessage;
using System.Linq;
using System.Globalization;

namespace Accounting
{
    public partial class FInvestor : CatalogBase
    {
        #region khai báo
        private readonly IInvestorService _IInvestorService;
        private readonly IInvestorGroupService _IInvestorGroupService;
        #endregion

        #region khởi tạo
        public FInvestor()
        {

            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _IInvestorService = IoC.Resolve<IInvestorService>();
            _IInvestorGroupService = IoC.Resolve<IInvestorGroupService>();
            this.uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            #endregion

            #region Tạo dữ liệu ban đầu
            LoadDuLieu();
            #endregion
            //ultraLabel14.Hide();
            //ultraLabel15.Hide();
            //ultraLabel16.Hide();
            //ultraLabel17.Hide();

        }

        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }
        private void LoadDuLieu(bool configGrid)
        {
            WaitingFrm.StartWaiting();
            #region Lấy dữ liệu từ CSDL
            List<Investor> list = _IInvestorService.GetAll_OrderBy();
            _IInvestorGroupService.UnbindSession(list);
            list = _IInvestorService.GetAll_OrderBy();
            List<InvestorGroup> list1 = _IInvestorGroupService.GetAll();
            #endregion

            #region Xử lý dữ liệu
            #endregion

            #region hiển thị và xử lý hiển thị
            uGrid.DataSource = list.ToArray();

            //Investor temp = uGrid.Selected.Rows[0].ListObject as Investor;
            foreach (var item in list)
            {
                InvestorGroup temp = list1.Where(p => p.ID == item.InvestorGroupID).SingleOrDefault();
                item.InvestorCategoryIDVIEW = temp != null ? temp.InvestorGroupName : string.Empty;
            }
            if ((uGrid.Selected.Rows.Count == 0) && (list.Count > 0))
            {
                uGrid.Rows[0].Selected = true;
            }
            //foreach (var item in list) item.InvestorCategoryIDVIEW = list1.Where(k => k.ID == item.InvestorCategoryID).SingleOrDefault().InvestorGroupName;
            //foreach (var item in list) item.InvestorCategoryIDVIEW = _IInvestorGroupService.Getbykey((Guid)(item.InvestorCategoryID)).InvestorGroupName;
            if (configGrid) ConfigGrid(uGrid);
            #endregion
            WaitingFrm.StopWaiting();
        }
        #endregion

        #region Nghiệp vụ
        #region ContentMenu
        #endregion

        #region Button
        private void btnAdd_Click_1(object sender, EventArgs e)
        {
            AddFunction();
            //Investor temp = uGrid.Selected.Rows[0].ListObject as Investor;
        }

        private void btnEdit_Click_1(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void btnDelete_Click_1(object sender, EventArgs e)
        {
            DeleteFunction();
        }
        #endregion

        /// <summary>
        /// Nghiệp vụ Add
        /// </summary>
        protected override void AddFunction()
        {
            new FInvestorDetail().ShowDialog(this);
            if (!FInvestorDetail.isClose) LoadDuLieu();
        }

        /// <summary>
        /// Nghiệp vụ Edit
        /// </summary>
        protected override void EditFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                Investor temp = uGrid.Selected.Rows[0].ListObject as Investor;
                //Investor Node = _IInvestorService.Getbykey((Guid)temp.ID);
                //ultraLabel1.Text = "Nhà đầu tư\t " + Node.InvestorCode;

                new FInvestorDetail(temp).ShowDialog(this);
                if (!FInvestorDetail.isClose) LoadDuLieu();
            }
            else
                Accounting.TextMessage.MSG.Error(string.Format(Accounting.TextMessage.resSystem.MSG_Catalog3, "một Nhà đầu tư"));
        }

        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        protected override void DeleteFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                Investor temp = uGrid.Selected.Rows[0].ListObject as Investor;
                if (MSG.Question(string.Format(resSystem.MSG_System_05, temp.InvestorCode)) == System.Windows.Forms.DialogResult.Yes)
                {
                    _IInvestorService.Delete(temp);
                    _IInvestorService.CommitChanges();
                    Utils.ClearCacheByType<Investor>();
                    LoadDuLieu();
                }
            }
            else
                MSG.Error(string.Format(resSystem.MSG_Catalog2, "một Nhà đầu tư"));
        }
        #endregion

        #region Utils
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, TextMessage.ConstDatabase.Investor_TableName);
        }
        #endregion
        private void uGrid_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            if (uGrid.Selected.Rows.Count > 0)
            {

                Investor Node = uGrid.Selected.Rows[0].ListObject as Investor;
                //Investor Node = _IInvestorService.Getbykey((Guid)(uGrid.Selected.Rows[0].ListObject as Investor).ID);
                if (Node.InvestorType == 0)
                {
                    ultraLabel14.Hide();
                    ultraLabel15.Hide();
                    ultraLabel16.Hide();
                    ultraLabel17.Hide();
                    ultraLabel4.Show();
                    //ultraLabel4.Location = new Point(13, 469);
                    ultraLabel8.Show();
                    //ultraLabel8.Location = new Point(13, 524);
                    //ultraLabel1.Text = "Nhà đầu tư : " + Node.InvestorCode;
                    //ultraLabel2.Text = "Tên nhà đầu tư : " + Node.InvestorName;
                    //ultraLabel3.Text = "Tài Khoản Ngân Hàng : " + Node.BankAccount;
                    //ultraLabel4.Text = "Số CMND/HC : " + Node.IdenticationNumber;
                    //ultraLabel8.Text = "ĐT Cơ Quan : " + Node.ContactOfficeTel;
                    //ultraLabel7.Text = "Mã Số Thuế : " + Node.TaxCode;
                    //ultraLabel6.Text = "Địa Chỉ : " + Node.Address;
                    //ultraLabel12.Text = "Ngày Cấp : " + Node.ContactIssueDate;
                    //ultraLabel11.Text = "Mở tại NH : " + Node.BankName;
                    //ultraLabel9.Text = "Email : " + Node.Email;
                    //ultraLabel13.Text = "Nơi cấp : " + Node.ContactIssueBy;
                    //ultraLabel5.Text = "ĐT di động : " + Node.ContactMobile;
                    //ultraLabel10.Location = new Point(273, 518);
                    ultraLabel5.Show();
                    mndt.Text = ": " + Node.InvestorCode;
                    tndd.Text = ": " + Node.InvestorName;
                    tknh.Text = ": " + Node.BankAccount;
                    dkkdcmnd.Text = ": " + Node.IdenticationNumber;
                    dtcq.Text = ": " + Node.ContactOfficeTel;
                    mst.Text = ": " + Node.TaxCode;
                    dc.Text = ": " + Node.Address;
                    if (Node.IssueDate != null)
                    {
                        DateTime dateValue = DateTime.Parse(Node.IssueDate.ToString());
                        nc.Text = ": " + dateValue.ToShortDateString();
                    }
                    nh.Text = ": " + Node.BankName;
                    email.Text = ": " + Node.Email;
                    noicap.Text = ": " + Node.ContactIssueBy;
                    faxdtdd.Text = ": " + Node.ContactMobile;
                    web.Visible = false;
                    //ultraLabel5.Location = new Point(507, 518);
                }
                if (Node.InvestorType == 1)
                {
                    ultraLabel14.Show();
                    ultraLabel15.Show();
                    ultraLabel16.Show();
                    ultraLabel17.Show();
                    //ultraLabel1.Text = "Nhà đầu tư : " + Node.InvestorCode;
                    //ultraLabel2.Text = "Tên nhà đầu tư : " + Node.InvestorName;
                    //ultraLabel3.Text = "Tài Khoản Ngân Hàng : " + Node.BankAccount;
                    //ultraLabel14.Text = "Số ĐKKĐ : " + Node.BusinessRegistrationNumber;
                    //ultraLabel16.Text = "Điện thoại : " + Node.Tel;
                    //ultraLabel7.Text = "Mã Số Thuế : " + Node.TaxCode;
                    //ultraLabel6.Text = "Địa Chỉ : " + Node.Address;
                    //if (Node.IssueDate != null)
                    //{
                    //    ultraLabel12.Text = "Ngày Cấp : " + (DateTime)Node.IssueDate;
                    //}
                    //ultraLabel11.Text = "Mở tại NH : " + Node.BankName;
                    //ultraLabel15.Text = "Fax : " + Node.Fax;
                    //ultraLabel9.Text = "Email : " + Node.Email;
                    //ultraLabel13.Text = "Nơi cấp : " + Node.IssueBy;
                    //ultraLabel17.Text = "Website : " + Node.Website;
                    ultraLabel4.Hide();
                    //ultraLabel14.Location = new Point(13, 469);
                    ultraLabel8.Hide();
                    //ultraLabel16.Location = new Point(13, 524);
                    //ultraLabel15.Location = new Point(273, 518);
                    ultraLabel5.Hide();
                    //ultraLabel17.Location = new Point(507, 518);
                    mndt.Text = ": " + Node.InvestorCode;
                    tndd.Text = ": " + Node.InvestorName;
                    tknh.Text = ": " + Node.BankAccount;
                    dkkdcmnd.Text = ": " + Node.BusinessRegistrationNumber;
                    dtcq.Text = ": " + Node.Tel;
                    mst.Text = ": " + Node.TaxCode;
                    dc.Text = ": " + Node.Address;
                    if (Node.IssueDate != null)
                    {
                        DateTime dateValue = DateTime.Parse(Node.IssueDate.ToString());
                        nc.Text = ": " + dateValue.ToShortDateString();
                    }
                    nh.Text = ": " + Node.BankName;
                    faxdtdd.Text = ": " + Node.Fax;
                    email.Text = ": " + Node.Email;
                    noicap.Text = ": " + Node.IssueBy;
                    web.Text = ":  " + Node.Website;
                }
            }

        }

        private void uGrid_MouseDown_1(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }

        private void tsmAdd_Click_1(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void tsmEdit_Click_1(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tsmDelete_Click_1(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void tmsReLoad_Click_1(object sender, EventArgs e)
        {
            ResetFunction();
        }
        protected override void ResetFunction()
        {
            LoadDuLieu(false);
        }
        private void uGrid_DoubleClick(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void FInvestor_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGrid.ResetText();
            uGrid.ResetUpdateMode();
            uGrid.ResetExitEditModeOnLeave();
            uGrid.ResetRowUpdateCancelAction();
            uGrid.DataSource = null;
            uGrid.Layouts.Clear();
            uGrid.ResetLayouts();
            uGrid.ResetDisplayLayout();
            uGrid.Refresh();
            uGrid.ClearUndoHistory();
            uGrid.ClearXsdConstraints();
        }

        private void FInvestor_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
