﻿namespace Accounting
{
    partial class FInvestor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnAdd = new Infragistics.Win.Misc.UltraButton();
            this.btnDelete = new Infragistics.Win.Misc.UltraButton();
            this.btnEdit = new Infragistics.Win.Misc.UltraButton();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.cms4Grid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.tmsReLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.ultraSplitter1 = new Infragistics.Win.Misc.UltraSplitter();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.tndd = new Infragistics.Win.Misc.UltraLabel();
            this.mndt = new Infragistics.Win.Misc.UltraLabel();
            this.dc = new Infragistics.Win.Misc.UltraLabel();
            this.mst = new Infragistics.Win.Misc.UltraLabel();
            this.dtcq = new Infragistics.Win.Misc.UltraLabel();
            this.tknh = new Infragistics.Win.Misc.UltraLabel();
            this.email = new Infragistics.Win.Misc.UltraLabel();
            this.faxdtdd = new Infragistics.Win.Misc.UltraLabel();
            this.nh = new Infragistics.Win.Misc.UltraLabel();
            this.web = new Infragistics.Win.Misc.UltraLabel();
            this.noicap = new Infragistics.Win.Misc.UltraLabel();
            this.nc = new Infragistics.Win.Misc.UltraLabel();
            this.dkkdcmnd = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel17 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            this.cms4Grid.SuspendLayout();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnAdd);
            this.panel1.Controls.Add(this.btnDelete);
            this.panel1.Controls.Add(this.btnEdit);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(878, 52);
            this.panel1.TabIndex = 3;
            // 
            // btnAdd
            // 
            appearance17.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.btnAdd.Appearance = appearance17;
            this.btnAdd.Location = new System.Drawing.Point(10, 12);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 30);
            this.btnAdd.TabIndex = 2;
            this.btnAdd.Text = "Thêm";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click_1);
            // 
            // btnDelete
            // 
            appearance18.Image = global::Accounting.Properties.Resources.xoa1_xoa_du_lieu_ke_toan_khoi_database_;
            this.btnDelete.Appearance = appearance18;
            this.btnDelete.Location = new System.Drawing.Point(172, 12);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 30);
            this.btnDelete.TabIndex = 1;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click_1);
            // 
            // btnEdit
            // 
            appearance19.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            this.btnEdit.Appearance = appearance19;
            this.btnEdit.Location = new System.Drawing.Point(91, 12);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(75, 30);
            this.btnEdit.TabIndex = 0;
            this.btnEdit.Text = "Sửa";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click_1);
            // 
            // uGrid
            // 
            this.uGrid.ContextMenuStrip = this.cms4Grid;
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.uGrid.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGrid.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGrid.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGrid.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.uGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid.Location = new System.Drawing.Point(0, 52);
            this.uGrid.Name = "uGrid";
            this.uGrid.Size = new System.Drawing.Size(878, 319);
            this.uGrid.TabIndex = 4;
            this.uGrid.Text = "uGrid";
            this.uGrid.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.uGrid_AfterSelectChange);
            this.uGrid.DoubleClick += new System.EventHandler(this.uGrid_DoubleClick);
            this.uGrid.MouseDown += new System.Windows.Forms.MouseEventHandler(this.uGrid_MouseDown_1);
            // 
            // cms4Grid
            // 
            this.cms4Grid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.tsmAdd,
            this.tsmEdit,
            this.tsmDelete,
            this.tmsReLoad});
            this.cms4Grid.Name = "cms4Grid";
            this.cms4Grid.Size = new System.Drawing.Size(149, 98);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(145, 6);
            // 
            // tsmAdd
            // 
            this.tsmAdd.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.tsmAdd.Name = "tsmAdd";
            this.tsmAdd.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.tsmAdd.Size = new System.Drawing.Size(148, 22);
            this.tsmAdd.Text = "Thêm";
            this.tsmAdd.Click += new System.EventHandler(this.tsmAdd_Click_1);
            // 
            // tsmEdit
            // 
            this.tsmEdit.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            this.tsmEdit.Name = "tsmEdit";
            this.tsmEdit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.tsmEdit.Size = new System.Drawing.Size(148, 22);
            this.tsmEdit.Text = "Sửa";
            this.tsmEdit.Click += new System.EventHandler(this.tsmEdit_Click_1);
            // 
            // tsmDelete
            // 
            this.tsmDelete.Image = global::Accounting.Properties.Resources.xoa1_xoa_du_lieu_ke_toan_khoi_database_;
            this.tsmDelete.Name = "tsmDelete";
            this.tsmDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.tsmDelete.Size = new System.Drawing.Size(148, 22);
            this.tsmDelete.Text = "Xóa";
            this.tsmDelete.Click += new System.EventHandler(this.tsmDelete_Click_1);
            // 
            // tmsReLoad
            // 
            this.tmsReLoad.Image = global::Accounting.Properties.Resources.ubtnReset;
            this.tmsReLoad.Name = "tmsReLoad";
            this.tmsReLoad.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.tmsReLoad.Size = new System.Drawing.Size(148, 22);
            this.tmsReLoad.Text = "Tải Lại";
            this.tmsReLoad.Click += new System.EventHandler(this.tmsReLoad_Click_1);
            // 
            // ultraSplitter1
            // 
            this.ultraSplitter1.BackColor = System.Drawing.SystemColors.Control;
            this.ultraSplitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraSplitter1.Location = new System.Drawing.Point(0, 371);
            this.ultraSplitter1.Name = "ultraSplitter1";
            this.ultraSplitter1.RestoreExtent = 236;
            this.ultraSplitter1.Size = new System.Drawing.Size(878, 10);
            this.ultraSplitter1.TabIndex = 36;
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.tndd);
            this.ultraPanel1.ClientArea.Controls.Add(this.mndt);
            this.ultraPanel1.ClientArea.Controls.Add(this.dc);
            this.ultraPanel1.ClientArea.Controls.Add(this.mst);
            this.ultraPanel1.ClientArea.Controls.Add(this.dtcq);
            this.ultraPanel1.ClientArea.Controls.Add(this.tknh);
            this.ultraPanel1.ClientArea.Controls.Add(this.email);
            this.ultraPanel1.ClientArea.Controls.Add(this.faxdtdd);
            this.ultraPanel1.ClientArea.Controls.Add(this.nh);
            this.ultraPanel1.ClientArea.Controls.Add(this.web);
            this.ultraPanel1.ClientArea.Controls.Add(this.noicap);
            this.ultraPanel1.ClientArea.Controls.Add(this.nc);
            this.ultraPanel1.ClientArea.Controls.Add(this.dkkdcmnd);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraLabel17);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraLabel16);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraLabel15);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraLabel14);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraLabel5);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraLabel13);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraLabel9);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraLabel11);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraLabel12);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraLabel6);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraLabel7);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraLabel8);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraLabel3);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraLabel4);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraLabel2);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraLabel1);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 381);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(878, 236);
            this.ultraPanel1.TabIndex = 37;
            // 
            // tndd
            // 
            appearance20.BackColor = System.Drawing.Color.Transparent;
            this.tndd.Appearance = appearance20;
            this.tndd.Location = new System.Drawing.Point(103, 36);
            this.tndd.Name = "tndd";
            this.tndd.Size = new System.Drawing.Size(227, 14);
            this.tndd.TabIndex = 41;
            // 
            // mndt
            // 
            appearance21.BackColor = System.Drawing.Color.Transparent;
            this.mndt.Appearance = appearance21;
            this.mndt.Location = new System.Drawing.Point(103, 10);
            this.mndt.Name = "mndt";
            this.mndt.Size = new System.Drawing.Size(144, 14);
            this.mndt.TabIndex = 41;
            // 
            // dc
            // 
            appearance22.BackColor = System.Drawing.Color.Transparent;
            this.dc.Appearance = appearance22;
            this.dc.Location = new System.Drawing.Point(103, 166);
            this.dc.Name = "dc";
            this.dc.Size = new System.Drawing.Size(171, 30);
            this.dc.TabIndex = 40;
            // 
            // mst
            // 
            appearance23.BackColor = System.Drawing.Color.Transparent;
            this.mst.Appearance = appearance23;
            this.mst.Location = new System.Drawing.Point(103, 140);
            this.mst.Name = "mst";
            this.mst.Size = new System.Drawing.Size(171, 14);
            this.mst.TabIndex = 40;
            // 
            // dtcq
            // 
            appearance24.BackColor = System.Drawing.Color.Transparent;
            this.dtcq.Appearance = appearance24;
            this.dtcq.Location = new System.Drawing.Point(103, 114);
            this.dtcq.Name = "dtcq";
            this.dtcq.Size = new System.Drawing.Size(171, 14);
            this.dtcq.TabIndex = 40;
            // 
            // tknh
            // 
            appearance25.BackColor = System.Drawing.Color.Transparent;
            this.tknh.Appearance = appearance25;
            this.tknh.Location = new System.Drawing.Point(103, 88);
            this.tknh.Name = "tknh";
            this.tknh.Size = new System.Drawing.Size(171, 14);
            this.tknh.TabIndex = 40;
            // 
            // email
            // 
            appearance26.BackColor = System.Drawing.Color.Transparent;
            this.email.Appearance = appearance26;
            this.email.Location = new System.Drawing.Point(428, 140);
            this.email.Name = "email";
            this.email.Size = new System.Drawing.Size(149, 14);
            this.email.TabIndex = 40;
            // 
            // faxdtdd
            // 
            appearance27.BackColor = System.Drawing.Color.Transparent;
            this.faxdtdd.Appearance = appearance27;
            this.faxdtdd.Location = new System.Drawing.Point(428, 114);
            this.faxdtdd.Name = "faxdtdd";
            this.faxdtdd.Size = new System.Drawing.Size(149, 14);
            this.faxdtdd.TabIndex = 40;
            // 
            // nh
            // 
            appearance28.BackColor = System.Drawing.Color.Transparent;
            this.nh.Appearance = appearance28;
            this.nh.Location = new System.Drawing.Point(428, 88);
            this.nh.Name = "nh";
            this.nh.Size = new System.Drawing.Size(149, 14);
            this.nh.TabIndex = 40;
            // 
            // web
            // 
            appearance29.BackColor = System.Drawing.Color.Transparent;
            this.web.Appearance = appearance29;
            this.web.Location = new System.Drawing.Point(691, 114);
            this.web.Name = "web";
            this.web.Size = new System.Drawing.Size(134, 28);
            this.web.TabIndex = 40;
            // 
            // noicap
            // 
            appearance30.BackColor = System.Drawing.Color.Transparent;
            this.noicap.Appearance = appearance30;
            this.noicap.Location = new System.Drawing.Point(691, 62);
            this.noicap.Name = "noicap";
            this.noicap.Size = new System.Drawing.Size(134, 40);
            this.noicap.TabIndex = 40;
            // 
            // nc
            // 
            appearance31.BackColor = System.Drawing.Color.Transparent;
            this.nc.Appearance = appearance31;
            this.nc.Location = new System.Drawing.Point(428, 62);
            this.nc.Name = "nc";
            this.nc.Size = new System.Drawing.Size(149, 14);
            this.nc.TabIndex = 40;
            // 
            // dkkdcmnd
            // 
            appearance32.BackColor = System.Drawing.Color.Transparent;
            this.dkkdcmnd.Appearance = appearance32;
            this.dkkdcmnd.Location = new System.Drawing.Point(103, 62);
            this.dkkdcmnd.Name = "dkkdcmnd";
            this.dkkdcmnd.Size = new System.Drawing.Size(171, 14);
            this.dkkdcmnd.TabIndex = 40;
            // 
            // ultraLabel17
            // 
            this.ultraLabel17.Location = new System.Drawing.Point(640, 114);
            this.ultraLabel17.Name = "ultraLabel17";
            this.ultraLabel17.Size = new System.Drawing.Size(45, 14);
            this.ultraLabel17.TabIndex = 39;
            this.ultraLabel17.Text = "Website";
            // 
            // ultraLabel16
            // 
            this.ultraLabel16.Location = new System.Drawing.Point(10, 114);
            this.ultraLabel16.Name = "ultraLabel16";
            this.ultraLabel16.Size = new System.Drawing.Size(55, 14);
            this.ultraLabel16.TabIndex = 38;
            this.ultraLabel16.Text = "Điện thoại";
            // 
            // ultraLabel15
            // 
            this.ultraLabel15.Location = new System.Drawing.Point(358, 114);
            this.ultraLabel15.Name = "ultraLabel15";
            this.ultraLabel15.Size = new System.Drawing.Size(23, 14);
            this.ultraLabel15.TabIndex = 37;
            this.ultraLabel15.Text = "Fax";
            // 
            // ultraLabel14
            // 
            this.ultraLabel14.Location = new System.Drawing.Point(10, 62);
            this.ultraLabel14.Name = "ultraLabel14";
            this.ultraLabel14.Size = new System.Drawing.Size(53, 14);
            this.ultraLabel14.TabIndex = 36;
            this.ultraLabel14.Text = "Số ĐKKĐ";
            // 
            // ultraLabel5
            // 
            this.ultraLabel5.Location = new System.Drawing.Point(358, 114);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(59, 14);
            this.ultraLabel5.TabIndex = 35;
            this.ultraLabel5.Text = "ĐT di động";
            // 
            // ultraLabel13
            // 
            this.ultraLabel13.Location = new System.Drawing.Point(640, 62);
            this.ultraLabel13.Name = "ultraLabel13";
            this.ultraLabel13.Size = new System.Drawing.Size(45, 14);
            this.ultraLabel13.TabIndex = 34;
            this.ultraLabel13.Text = "Nơi Cấp";
            // 
            // ultraLabel9
            // 
            this.ultraLabel9.Location = new System.Drawing.Point(358, 140);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(33, 14);
            this.ultraLabel9.TabIndex = 33;
            this.ultraLabel9.Text = "Email";
            // 
            // ultraLabel11
            // 
            this.ultraLabel11.Location = new System.Drawing.Point(358, 88);
            this.ultraLabel11.Name = "ultraLabel11";
            this.ultraLabel11.Size = new System.Drawing.Size(54, 14);
            this.ultraLabel11.TabIndex = 31;
            this.ultraLabel11.Text = "Mở tại NH";
            // 
            // ultraLabel12
            // 
            this.ultraLabel12.Location = new System.Drawing.Point(358, 62);
            this.ultraLabel12.Name = "ultraLabel12";
            this.ultraLabel12.Size = new System.Drawing.Size(54, 14);
            this.ultraLabel12.TabIndex = 30;
            this.ultraLabel12.Text = "Ngày Cấp";
            // 
            // ultraLabel6
            // 
            this.ultraLabel6.Location = new System.Drawing.Point(10, 166);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(41, 14);
            this.ultraLabel6.TabIndex = 29;
            this.ultraLabel6.Text = "Địa Chỉ";
            // 
            // ultraLabel7
            // 
            this.ultraLabel7.Location = new System.Drawing.Point(10, 140);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(66, 14);
            this.ultraLabel7.TabIndex = 28;
            this.ultraLabel7.Text = "Mã Số Thuế";
            // 
            // ultraLabel8
            // 
            this.ultraLabel8.Location = new System.Drawing.Point(10, 114);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(67, 14);
            this.ultraLabel8.TabIndex = 27;
            this.ultraLabel8.Text = "ĐT Cơ Quan";
            // 
            // ultraLabel3
            // 
            this.ultraLabel3.Location = new System.Drawing.Point(10, 88);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(87, 14);
            this.ultraLabel3.TabIndex = 26;
            this.ultraLabel3.Text = "TK Ngân Hàng";
            // 
            // ultraLabel4
            // 
            this.ultraLabel4.Location = new System.Drawing.Point(10, 62);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(75, 14);
            this.ultraLabel4.TabIndex = 25;
            this.ultraLabel4.Text = "Số CMND/HC";
            // 
            // ultraLabel2
            // 
            this.ultraLabel2.Location = new System.Drawing.Point(10, 36);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(88, 14);
            this.ultraLabel2.TabIndex = 24;
            this.ultraLabel2.Text = "Tên nhà đầu tư";
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(10, 10);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(88, 14);
            this.ultraLabel1.TabIndex = 23;
            this.ultraLabel1.Text = "Mã nhà đầu tư";
            // 
            // FInvestor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(878, 617);
            this.Controls.Add(this.uGrid);
            this.Controls.Add(this.ultraSplitter1);
            this.Controls.Add(this.ultraPanel1);
            this.Controls.Add(this.panel1);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FInvestor";
            this.Text = "Nhà đầu tư";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FInvestor_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FInvestor_FormClosed);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            this.cms4Grid.ResumeLayout(false);
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private Infragistics.Win.Misc.UltraButton btnAdd;
        private Infragistics.Win.Misc.UltraButton btnDelete;
        private Infragistics.Win.Misc.UltraButton btnEdit;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
        private System.Windows.Forms.ContextMenuStrip cms4Grid;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripMenuItem tsmEdit;
        private System.Windows.Forms.ToolStripMenuItem tsmDelete;
        private System.Windows.Forms.ToolStripMenuItem tmsReLoad;
        private Infragistics.Win.Misc.UltraSplitter ultraSplitter1;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel17;
        private Infragistics.Win.Misc.UltraLabel ultraLabel16;
        private Infragistics.Win.Misc.UltraLabel ultraLabel15;
        private Infragistics.Win.Misc.UltraLabel ultraLabel14;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel13;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
        private Infragistics.Win.Misc.UltraLabel ultraLabel12;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel dkkdcmnd;
        private Infragistics.Win.Misc.UltraLabel dc;
        private Infragistics.Win.Misc.UltraLabel mst;
        private Infragistics.Win.Misc.UltraLabel dtcq;
        private Infragistics.Win.Misc.UltraLabel tknh;
        private Infragistics.Win.Misc.UltraLabel email;
        private Infragistics.Win.Misc.UltraLabel faxdtdd;
        private Infragistics.Win.Misc.UltraLabel nh;
        private Infragistics.Win.Misc.UltraLabel nc;
        private Infragistics.Win.Misc.UltraLabel web;
        private Infragistics.Win.Misc.UltraLabel noicap;
        private Infragistics.Win.Misc.UltraLabel mndt;
        private Infragistics.Win.Misc.UltraLabel tndd;
    }
}