﻿namespace Accounting
{
    partial class FInvestorDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.panel27 = new System.Windows.Forms.Panel();
            this.ultraLabel25 = new Infragistics.Win.Misc.UltraLabel();
            this.txtIssueBy = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel26 = new System.Windows.Forms.Panel();
            this.dtePostedDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel24 = new Infragistics.Win.Misc.UltraLabel();
            this.panel22 = new System.Windows.Forms.Panel();
            this.ultraLabel20 = new Infragistics.Win.Misc.UltraLabel();
            this.txtWebsite = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel21 = new System.Windows.Forms.Panel();
            this.ultraLabel19 = new Infragistics.Win.Misc.UltraLabel();
            this.txtFax = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel20 = new System.Windows.Forms.Panel();
            this.ultraLabel18 = new Infragistics.Win.Misc.UltraLabel();
            this.txtEmail1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel19 = new System.Windows.Forms.Panel();
            this.ultraLabel17 = new Infragistics.Win.Misc.UltraLabel();
            this.txtTel = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel18 = new System.Windows.Forms.Panel();
            this.ultraLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.txtBusinessRegistrationNumber = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel11 = new System.Windows.Forms.Panel();
            this.cbbInvestorCategoryID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.ultraLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.txtBankName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel9 = new System.Windows.Forms.Panel();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.txtTaxCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel6 = new System.Windows.Forms.Panel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAddress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel10 = new System.Windows.Forms.Panel();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.txtBankAcount = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel3 = new System.Windows.Forms.Panel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.txtInvestorName1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblInvestorCode = new Infragistics.Win.Misc.UltraLabel();
            this.txtInvestorCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.chkIsActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraOptionSet1 = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.panel25 = new System.Windows.Forms.Panel();
            this.ultraLabel23 = new Infragistics.Win.Misc.UltraLabel();
            this.txtContactAddress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel24 = new System.Windows.Forms.Panel();
            this.ultraLabel22 = new Infragistics.Win.Misc.UltraLabel();
            this.txtContactTitle = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel23 = new System.Windows.Forms.Panel();
            this.ultraLabel21 = new Infragistics.Win.Misc.UltraLabel();
            this.txtInvestorName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel7 = new System.Windows.Forms.Panel();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.txtContactMobile = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel16 = new System.Windows.Forms.Panel();
            this.ultraLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.txtContactIssueBy = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel12 = new System.Windows.Forms.Panel();
            this.ultraDateTimeEditor1 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.txtIndentication = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel14 = new System.Windows.Forms.Panel();
            this.ultraLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.txtEmail = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel8 = new System.Windows.Forms.Panel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.txtDTCoQuan = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.panel4 = new System.Windows.Forms.Panel();
            this.cbbContactPrefix = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            this.panel27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtIssueBy)).BeginInit();
            this.panel26.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtePostedDate)).BeginInit();
            this.panel22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtWebsite)).BeginInit();
            this.panel21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtFax)).BeginInit();
            this.panel20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail1)).BeginInit();
            this.panel19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTel)).BeginInit();
            this.panel18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBusinessRegistrationNumber)).BeginInit();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbInvestorCategoryID)).BeginInit();
            this.panel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankName)).BeginInit();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxCode)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAcount)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvestorName1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvestorCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraOptionSet1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            this.panel25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactAddress)).BeginInit();
            this.panel24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactTitle)).BeginInit();
            this.panel23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvestorName)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactMobile)).BeginInit();
            this.panel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactIssueBy)).BeginInit();
            this.panel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor1)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtIndentication)).BeginInit();
            this.panel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail)).BeginInit();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDTCoQuan)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbContactPrefix)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox1.Controls.Add(this.panel27);
            this.ultraGroupBox1.Controls.Add(this.panel26);
            this.ultraGroupBox1.Controls.Add(this.panel22);
            this.ultraGroupBox1.Controls.Add(this.panel21);
            this.ultraGroupBox1.Controls.Add(this.panel20);
            this.ultraGroupBox1.Controls.Add(this.panel19);
            this.ultraGroupBox1.Controls.Add(this.panel18);
            this.ultraGroupBox1.Controls.Add(this.panel11);
            this.ultraGroupBox1.Controls.Add(this.panel15);
            this.ultraGroupBox1.Controls.Add(this.panel9);
            this.ultraGroupBox1.Controls.Add(this.panel6);
            this.ultraGroupBox1.Controls.Add(this.panel10);
            this.ultraGroupBox1.Controls.Add(this.panel3);
            this.ultraGroupBox1.Controls.Add(this.panel2);
            appearance18.FontData.BoldAsString = "True";
            this.ultraGroupBox1.HeaderAppearance = appearance18;
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(12, 27);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(585, 241);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // panel27
            // 
            this.panel27.Controls.Add(this.ultraLabel25);
            this.panel27.Controls.Add(this.txtIssueBy);
            this.panel27.Location = new System.Drawing.Point(402, 112);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(166, 31);
            this.panel27.TabIndex = 8;
            // 
            // ultraLabel25
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.TextVAlignAsString = "Middle";
            this.ultraLabel25.Appearance = appearance1;
            this.ultraLabel25.Location = new System.Drawing.Point(1, 3);
            this.ultraLabel25.Name = "ultraLabel25";
            this.ultraLabel25.Size = new System.Drawing.Size(49, 22);
            this.ultraLabel25.TabIndex = 6;
            this.ultraLabel25.Text = "Nơi cấp";
            // 
            // txtIssueBy
            // 
            this.txtIssueBy.AutoSize = false;
            this.txtIssueBy.Location = new System.Drawing.Point(59, 3);
            this.txtIssueBy.Name = "txtIssueBy";
            this.txtIssueBy.Size = new System.Drawing.Size(104, 22);
            this.txtIssueBy.TabIndex = 8;
            // 
            // panel26
            // 
            this.panel26.Controls.Add(this.dtePostedDate);
            this.panel26.Controls.Add(this.ultraLabel24);
            this.panel26.Location = new System.Drawing.Point(216, 112);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(179, 31);
            this.panel26.TabIndex = 7;
            // 
            // dtePostedDate
            // 
            appearance2.TextHAlignAsString = "Center";
            appearance2.TextVAlignAsString = "Middle";
            this.dtePostedDate.Appearance = appearance2;
            this.dtePostedDate.AutoSize = false;
            this.dtePostedDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtePostedDate.Location = new System.Drawing.Point(70, 3);
            this.dtePostedDate.MaskInput = "dd/mm/yyyy";
            this.dtePostedDate.Name = "dtePostedDate";
            this.dtePostedDate.Size = new System.Drawing.Size(104, 22);
            this.dtePostedDate.TabIndex = 7;
            this.dtePostedDate.Value = null;
            // 
            // ultraLabel24
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextVAlignAsString = "Middle";
            this.ultraLabel24.Appearance = appearance3;
            this.ultraLabel24.Location = new System.Drawing.Point(1, 2);
            this.ultraLabel24.Name = "ultraLabel24";
            this.ultraLabel24.Size = new System.Drawing.Size(65, 22);
            this.ultraLabel24.TabIndex = 6;
            this.ultraLabel24.Text = "Ngày cấp";
            // 
            // panel22
            // 
            this.panel22.Controls.Add(this.ultraLabel20);
            this.panel22.Controls.Add(this.txtWebsite);
            this.panel22.Location = new System.Drawing.Point(216, 204);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(352, 31);
            this.panel22.TabIndex = 14;
            // 
            // ultraLabel20
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.ultraLabel20.Appearance = appearance4;
            this.ultraLabel20.Location = new System.Drawing.Point(1, 6);
            this.ultraLabel20.Name = "ultraLabel20";
            this.ultraLabel20.Size = new System.Drawing.Size(65, 22);
            this.ultraLabel20.TabIndex = 6;
            this.ultraLabel20.Text = "Website";
            // 
            // txtWebsite
            // 
            this.txtWebsite.AutoSize = false;
            this.txtWebsite.Location = new System.Drawing.Point(70, 6);
            this.txtWebsite.Name = "txtWebsite";
            this.txtWebsite.Size = new System.Drawing.Size(279, 22);
            this.txtWebsite.TabIndex = 14;
            // 
            // panel21
            // 
            this.panel21.Controls.Add(this.ultraLabel19);
            this.panel21.Controls.Add(this.txtFax);
            this.panel21.Location = new System.Drawing.Point(216, 173);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(352, 31);
            this.panel21.TabIndex = 12;
            // 
            // ultraLabel19
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.TextVAlignAsString = "Middle";
            this.ultraLabel19.Appearance = appearance5;
            this.ultraLabel19.Location = new System.Drawing.Point(1, 6);
            this.ultraLabel19.Name = "ultraLabel19";
            this.ultraLabel19.Size = new System.Drawing.Size(65, 22);
            this.ultraLabel19.TabIndex = 6;
            this.ultraLabel19.Text = "Fax";
            // 
            // txtFax
            // 
            this.txtFax.AutoSize = false;
            this.txtFax.Location = new System.Drawing.Point(70, 6);
            this.txtFax.Name = "txtFax";
            this.txtFax.Size = new System.Drawing.Size(279, 22);
            this.txtFax.TabIndex = 12;
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.ultraLabel18);
            this.panel20.Controls.Add(this.txtEmail1);
            this.panel20.Location = new System.Drawing.Point(10, 204);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(197, 31);
            this.panel20.TabIndex = 13;
            // 
            // ultraLabel18
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextVAlignAsString = "Middle";
            this.ultraLabel18.Appearance = appearance6;
            this.ultraLabel18.Location = new System.Drawing.Point(1, 4);
            this.ultraLabel18.Name = "ultraLabel18";
            this.ultraLabel18.Size = new System.Drawing.Size(88, 22);
            this.ultraLabel18.TabIndex = 6;
            this.ultraLabel18.Text = "Email";
            // 
            // txtEmail1
            // 
            this.txtEmail1.AutoSize = false;
            this.txtEmail1.Location = new System.Drawing.Point(95, 4);
            this.txtEmail1.Multiline = true;
            this.txtEmail1.Name = "txtEmail1";
            this.txtEmail1.Size = new System.Drawing.Size(99, 22);
            this.txtEmail1.TabIndex = 13;
            // 
            // panel19
            // 
            this.panel19.Controls.Add(this.ultraLabel17);
            this.panel19.Controls.Add(this.txtTel);
            this.panel19.Location = new System.Drawing.Point(10, 173);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(197, 31);
            this.panel19.TabIndex = 11;
            // 
            // ultraLabel17
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.TextVAlignAsString = "Middle";
            this.ultraLabel17.Appearance = appearance7;
            this.ultraLabel17.Location = new System.Drawing.Point(1, 4);
            this.ultraLabel17.Name = "ultraLabel17";
            this.ultraLabel17.Size = new System.Drawing.Size(88, 22);
            this.ultraLabel17.TabIndex = 6;
            this.ultraLabel17.Text = "Điện thoại";
            // 
            // txtTel
            // 
            this.txtTel.AutoSize = false;
            this.txtTel.Location = new System.Drawing.Point(95, 4);
            this.txtTel.Name = "txtTel";
            this.txtTel.Size = new System.Drawing.Size(99, 22);
            this.txtTel.TabIndex = 11;
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.ultraLabel16);
            this.panel18.Controls.Add(this.txtBusinessRegistrationNumber);
            this.panel18.Location = new System.Drawing.Point(10, 112);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(197, 31);
            this.panel18.TabIndex = 6;
            // 
            // ultraLabel16
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.TextVAlignAsString = "Middle";
            this.ultraLabel16.Appearance = appearance8;
            this.ultraLabel16.Location = new System.Drawing.Point(1, 3);
            this.ultraLabel16.Name = "ultraLabel16";
            this.ultraLabel16.Size = new System.Drawing.Size(72, 22);
            this.ultraLabel16.TabIndex = 6;
            this.ultraLabel16.Text = "Mã ĐKKD";
            // 
            // txtBusinessRegistrationNumber
            // 
            this.txtBusinessRegistrationNumber.AutoSize = false;
            this.txtBusinessRegistrationNumber.Location = new System.Drawing.Point(95, 3);
            this.txtBusinessRegistrationNumber.Name = "txtBusinessRegistrationNumber";
            this.txtBusinessRegistrationNumber.Size = new System.Drawing.Size(99, 22);
            this.txtBusinessRegistrationNumber.TabIndex = 6;
            this.txtBusinessRegistrationNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBusinessRegistrationNumber_KeyPress);
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.cbbInvestorCategoryID);
            this.panel11.Controls.Add(this.ultraLabel9);
            this.panel11.Location = new System.Drawing.Point(216, 50);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(352, 31);
            this.panel11.TabIndex = 4;
            // 
            // cbbInvestorCategoryID
            // 
            this.cbbInvestorCategoryID.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.SuggestAppend;
            this.cbbInvestorCategoryID.AutoSize = false;
            appearance9.Image = global::Accounting.Properties.Resources.plus;
            editorButton1.Appearance = appearance9;
            appearance10.Image = global::Accounting.Properties.Resources.plus_press;
            editorButton1.PressedAppearance = appearance10;
            editorButton1.Text = "";
            this.cbbInvestorCategoryID.ButtonsRight.Add(editorButton1);
            this.cbbInvestorCategoryID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbInvestorCategoryID.Location = new System.Drawing.Point(70, 3);
            this.cbbInvestorCategoryID.Name = "cbbInvestorCategoryID";
            this.cbbInvestorCategoryID.Size = new System.Drawing.Size(279, 22);
            this.cbbInvestorCategoryID.TabIndex = 1003;
            this.cbbInvestorCategoryID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.cbbInvestorCategoryID_EditorButtonClick);
            // 
            // ultraLabel9
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            appearance11.TextVAlignAsString = "Middle";
            this.ultraLabel9.Appearance = appearance11;
            this.ultraLabel9.Location = new System.Drawing.Point(1, 3);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(51, 22);
            this.ultraLabel9.TabIndex = 6;
            this.ultraLabel9.Text = "Nhóm(*)";
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.ultraLabel13);
            this.panel15.Controls.Add(this.txtBankName);
            this.panel15.Location = new System.Drawing.Point(216, 142);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(352, 31);
            this.panel15.TabIndex = 10;
            // 
            // ultraLabel13
            // 
            appearance12.BackColor = System.Drawing.Color.Transparent;
            appearance12.TextVAlignAsString = "Middle";
            this.ultraLabel13.Appearance = appearance12;
            this.ultraLabel13.Location = new System.Drawing.Point(1, 6);
            this.ultraLabel13.Name = "ultraLabel13";
            this.ultraLabel13.Size = new System.Drawing.Size(65, 22);
            this.ultraLabel13.TabIndex = 6;
            this.ultraLabel13.Text = "Mở tại NH";
            // 
            // txtBankName
            // 
            this.txtBankName.AutoSize = false;
            this.txtBankName.Location = new System.Drawing.Point(70, 6);
            this.txtBankName.Name = "txtBankName";
            this.txtBankName.Size = new System.Drawing.Size(279, 22);
            this.txtBankName.TabIndex = 10;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.ultraLabel7);
            this.panel9.Controls.Add(this.txtTaxCode);
            this.panel9.Location = new System.Drawing.Point(10, 50);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(197, 31);
            this.panel9.TabIndex = 3;
            // 
            // ultraLabel7
            // 
            appearance13.BackColor = System.Drawing.Color.Transparent;
            appearance13.TextVAlignAsString = "Middle";
            this.ultraLabel7.Appearance = appearance13;
            this.ultraLabel7.Location = new System.Drawing.Point(1, 3);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(77, 22);
            this.ultraLabel7.TabIndex = 6;
            this.ultraLabel7.Text = "Mã số thuế";
            // 
            // txtTaxCode
            // 
            this.txtTaxCode.AutoSize = false;
            this.txtTaxCode.Location = new System.Drawing.Point(95, 3);
            this.txtTaxCode.MaxLength = 14;
            this.txtTaxCode.Name = "txtTaxCode";
            this.txtTaxCode.Size = new System.Drawing.Size(99, 22);
            this.txtTaxCode.TabIndex = 1002;
            this.txtTaxCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTaxCode_KeyPress);
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.ultraLabel4);
            this.panel6.Controls.Add(this.txtAddress);
            this.panel6.Location = new System.Drawing.Point(10, 82);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(558, 31);
            this.panel6.TabIndex = 5;
            // 
            // ultraLabel4
            // 
            appearance14.BackColor = System.Drawing.Color.Transparent;
            appearance14.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance14;
            this.ultraLabel4.Location = new System.Drawing.Point(1, 3);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(49, 22);
            this.ultraLabel4.TabIndex = 6;
            this.ultraLabel4.Text = "Địa chỉ";
            // 
            // txtAddress
            // 
            this.txtAddress.AutoSize = false;
            this.txtAddress.Location = new System.Drawing.Point(95, 3);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(460, 22);
            this.txtAddress.TabIndex = 5;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.ultraLabel8);
            this.panel10.Controls.Add(this.txtBankAcount);
            this.panel10.Location = new System.Drawing.Point(10, 142);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(197, 31);
            this.panel10.TabIndex = 9;
            // 
            // ultraLabel8
            // 
            appearance15.BackColor = System.Drawing.Color.Transparent;
            appearance15.TextVAlignAsString = "Middle";
            this.ultraLabel8.Appearance = appearance15;
            this.ultraLabel8.Location = new System.Drawing.Point(1, 4);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(88, 22);
            this.ultraLabel8.TabIndex = 6;
            this.ultraLabel8.Text = "TK ngân hàng";
            // 
            // txtBankAcount
            // 
            this.txtBankAcount.AutoSize = false;
            this.txtBankAcount.Location = new System.Drawing.Point(95, 4);
            this.txtBankAcount.Name = "txtBankAcount";
            this.txtBankAcount.Size = new System.Drawing.Size(99, 22);
            this.txtBankAcount.TabIndex = 9;
            this.txtBankAcount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBankAcount_KeyPress);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.ultraLabel1);
            this.panel3.Controls.Add(this.txtInvestorName1);
            this.panel3.Location = new System.Drawing.Point(216, 19);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(352, 31);
            this.panel3.TabIndex = 2;
            // 
            // ultraLabel1
            // 
            appearance16.BackColor = System.Drawing.Color.Transparent;
            appearance16.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance16;
            this.ultraLabel1.Location = new System.Drawing.Point(1, 4);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(50, 22);
            this.ultraLabel1.TabIndex = 6;
            this.ultraLabel1.Text = "Tên(*)";
            // 
            // txtInvestorName1
            // 
            this.txtInvestorName1.AutoSize = false;
            this.txtInvestorName1.Location = new System.Drawing.Point(70, 4);
            this.txtInvestorName1.Name = "txtInvestorName1";
            this.txtInvestorName1.Size = new System.Drawing.Size(279, 22);
            this.txtInvestorName1.TabIndex = 1001;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lblInvestorCode);
            this.panel2.Controls.Add(this.txtInvestorCode);
            this.panel2.Location = new System.Drawing.Point(10, 20);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(197, 31);
            this.panel2.TabIndex = 1;
            // 
            // lblInvestorCode
            // 
            appearance17.BackColor = System.Drawing.Color.Transparent;
            appearance17.TextVAlignAsString = "Middle";
            this.lblInvestorCode.Appearance = appearance17;
            this.lblInvestorCode.Location = new System.Drawing.Point(1, 4);
            this.lblInvestorCode.Name = "lblInvestorCode";
            this.lblInvestorCode.Size = new System.Drawing.Size(49, 22);
            this.lblInvestorCode.TabIndex = 6;
            this.lblInvestorCode.Text = "Mã";
            // 
            // txtInvestorCode
            // 
            this.txtInvestorCode.AutoSize = false;
            this.txtInvestorCode.Location = new System.Drawing.Point(95, 4);
            this.txtInvestorCode.Name = "txtInvestorCode";
            this.txtInvestorCode.Size = new System.Drawing.Size(99, 22);
            this.txtInvestorCode.TabIndex = 1000;
            this.txtInvestorCode.ValueChanged += new System.EventHandler(this.txtInvestorCode_ValueChanged);
            // 
            // btnSave
            // 
            appearance19.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance19;
            this.btnSave.Location = new System.Drawing.Point(434, 495);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 337;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click_1);
            // 
            // btnClose
            // 
            appearance20.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance20;
            this.btnClose.Location = new System.Drawing.Point(519, 495);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 338;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click_1);
            // 
            // chkIsActive
            // 
            appearance21.TextVAlignAsString = "Middle";
            this.chkIsActive.Appearance = appearance21;
            this.chkIsActive.BackColor = System.Drawing.Color.Transparent;
            this.chkIsActive.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsActive.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkIsActive.Checked = true;
            this.chkIsActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIsActive.Location = new System.Drawing.Point(7, 500);
            this.chkIsActive.Name = "chkIsActive";
            this.chkIsActive.Size = new System.Drawing.Size(103, 20);
            this.chkIsActive.TabIndex = 69;
            this.chkIsActive.Text = "Hoạt động";
            // 
            // ultraOptionSet1
            // 
            this.ultraOptionSet1.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            valueListItem1.DataValue = "Default Item";
            valueListItem1.DisplayText = "1.Cá Nhân";
            valueListItem2.DataValue = "ValueListItem1";
            valueListItem2.DisplayText = "2. Tổ Chức";
            this.ultraOptionSet1.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2});
            this.ultraOptionSet1.Location = new System.Drawing.Point(22, 3);
            this.ultraOptionSet1.Name = "ultraOptionSet1";
            this.ultraOptionSet1.Size = new System.Drawing.Size(161, 18);
            this.ultraOptionSet1.TabIndex = 888;
            this.ultraOptionSet1.ValueChanged += new System.EventHandler(this.ultraOptionSet1_ValueChanged_1);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.ultraGroupBox2);
            this.panel1.Controls.Add(this.ultraOptionSet1);
            this.panel1.Controls.Add(this.chkIsActive);
            this.panel1.Controls.Add(this.btnClose);
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Controls.Add(this.ultraGroupBox1);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(606, 532);
            this.panel1.TabIndex = 0;
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox2.Controls.Add(this.panel25);
            this.ultraGroupBox2.Controls.Add(this.panel24);
            this.ultraGroupBox2.Controls.Add(this.panel23);
            this.ultraGroupBox2.Controls.Add(this.panel7);
            this.ultraGroupBox2.Controls.Add(this.panel16);
            this.ultraGroupBox2.Controls.Add(this.panel12);
            this.ultraGroupBox2.Controls.Add(this.panel5);
            this.ultraGroupBox2.Controls.Add(this.panel14);
            this.ultraGroupBox2.Controls.Add(this.panel8);
            this.ultraGroupBox2.Controls.Add(this.panel4);
            appearance33.FontData.BoldAsString = "True";
            this.ultraGroupBox2.HeaderAppearance = appearance33;
            this.ultraGroupBox2.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
            this.ultraGroupBox2.Location = new System.Drawing.Point(12, 268);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(585, 226);
            this.ultraGroupBox2.TabIndex = 33;
            this.ultraGroupBox2.Text = "Thông tin cá nhân";
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // panel25
            // 
            this.panel25.Controls.Add(this.ultraLabel23);
            this.panel25.Controls.Add(this.txtContactAddress);
            this.panel25.Location = new System.Drawing.Point(10, 85);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(558, 31);
            this.panel25.TabIndex = 185;
            // 
            // ultraLabel23
            // 
            appearance22.BackColor = System.Drawing.Color.Transparent;
            appearance22.TextVAlignAsString = "Middle";
            this.ultraLabel23.Appearance = appearance22;
            this.ultraLabel23.Location = new System.Drawing.Point(1, 6);
            this.ultraLabel23.Name = "ultraLabel23";
            this.ultraLabel23.Size = new System.Drawing.Size(65, 22);
            this.ultraLabel23.TabIndex = 6;
            this.ultraLabel23.Text = "Địa chỉ";
            // 
            // txtContactAddress
            // 
            this.txtContactAddress.AutoSize = false;
            this.txtContactAddress.Location = new System.Drawing.Point(94, 6);
            this.txtContactAddress.Multiline = true;
            this.txtContactAddress.Name = "txtContactAddress";
            this.txtContactAddress.Size = new System.Drawing.Size(460, 22);
            this.txtContactAddress.TabIndex = 18;
            // 
            // panel24
            // 
            this.panel24.Controls.Add(this.ultraLabel22);
            this.panel24.Controls.Add(this.txtContactTitle);
            this.panel24.Location = new System.Drawing.Point(10, 52);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(558, 31);
            this.panel24.TabIndex = 175;
            // 
            // ultraLabel22
            // 
            appearance23.BackColor = System.Drawing.Color.Transparent;
            appearance23.TextVAlignAsString = "Middle";
            this.ultraLabel22.Appearance = appearance23;
            this.ultraLabel22.Location = new System.Drawing.Point(1, 6);
            this.ultraLabel22.Name = "ultraLabel22";
            this.ultraLabel22.Size = new System.Drawing.Size(65, 22);
            this.ultraLabel22.TabIndex = 6;
            this.ultraLabel22.Text = "Chức danh";
            // 
            // txtContactTitle
            // 
            this.txtContactTitle.AutoSize = false;
            this.txtContactTitle.Location = new System.Drawing.Point(95, 6);
            this.txtContactTitle.Name = "txtContactTitle";
            this.txtContactTitle.Size = new System.Drawing.Size(460, 22);
            this.txtContactTitle.TabIndex = 17;
            // 
            // panel23
            // 
            this.panel23.Controls.Add(this.ultraLabel21);
            this.panel23.Controls.Add(this.txtInvestorName);
            this.panel23.Location = new System.Drawing.Point(216, 19);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(352, 31);
            this.panel23.TabIndex = 165;
            // 
            // ultraLabel21
            // 
            appearance24.BackColor = System.Drawing.Color.Transparent;
            appearance24.TextVAlignAsString = "Middle";
            this.ultraLabel21.Appearance = appearance24;
            this.ultraLabel21.Location = new System.Drawing.Point(1, 6);
            this.ultraLabel21.Name = "ultraLabel21";
            this.ultraLabel21.Size = new System.Drawing.Size(65, 22);
            this.ultraLabel21.TabIndex = 6;
            this.ultraLabel21.Text = "Họ và tên";
            // 
            // txtInvestorName
            // 
            this.txtInvestorName.AutoSize = false;
            this.txtInvestorName.Location = new System.Drawing.Point(70, 6);
            this.txtInvestorName.Name = "txtInvestorName";
            this.txtInvestorName.Size = new System.Drawing.Size(279, 22);
            this.txtInvestorName.TabIndex = 16;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.ultraLabel5);
            this.panel7.Controls.Add(this.txtContactMobile);
            this.panel7.Location = new System.Drawing.Point(216, 148);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(179, 31);
            this.panel7.TabIndex = 335;
            // 
            // ultraLabel5
            // 
            appearance25.BackColor = System.Drawing.Color.Transparent;
            appearance25.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance25;
            this.ultraLabel5.Location = new System.Drawing.Point(1, 3);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(49, 22);
            this.ultraLabel5.TabIndex = 6;
            this.ultraLabel5.Text = "ĐTDĐ";
            // 
            // txtContactMobile
            // 
            this.txtContactMobile.AutoSize = false;
            this.txtContactMobile.Location = new System.Drawing.Point(70, 3);
            this.txtContactMobile.Name = "txtContactMobile";
            this.txtContactMobile.Size = new System.Drawing.Size(106, 22);
            this.txtContactMobile.TabIndex = 24;
            this.txtContactMobile.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtContactMobile_KeyPress);
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.ultraLabel14);
            this.panel16.Controls.Add(this.txtContactIssueBy);
            this.panel16.Location = new System.Drawing.Point(402, 118);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(166, 31);
            this.panel16.TabIndex = 215;
            // 
            // ultraLabel14
            // 
            appearance26.BackColor = System.Drawing.Color.Transparent;
            appearance26.TextVAlignAsString = "Middle";
            this.ultraLabel14.Appearance = appearance26;
            this.ultraLabel14.Location = new System.Drawing.Point(1, 3);
            this.ultraLabel14.Name = "ultraLabel14";
            this.ultraLabel14.Size = new System.Drawing.Size(49, 22);
            this.ultraLabel14.TabIndex = 6;
            this.ultraLabel14.Text = "Nơi cấp";
            // 
            // txtContactIssueBy
            // 
            this.txtContactIssueBy.AutoSize = false;
            this.txtContactIssueBy.Location = new System.Drawing.Point(59, 3);
            this.txtContactIssueBy.Name = "txtContactIssueBy";
            this.txtContactIssueBy.Size = new System.Drawing.Size(104, 22);
            this.txtContactIssueBy.TabIndex = 21;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.ultraDateTimeEditor1);
            this.panel12.Controls.Add(this.ultraLabel10);
            this.panel12.Location = new System.Drawing.Point(216, 118);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(179, 31);
            this.panel12.TabIndex = 205;
            this.panel12.Paint += new System.Windows.Forms.PaintEventHandler(this.panel12_Paint);
            // 
            // ultraDateTimeEditor1
            // 
            appearance27.TextHAlignAsString = "Center";
            appearance27.TextVAlignAsString = "Middle";
            this.ultraDateTimeEditor1.Appearance = appearance27;
            this.ultraDateTimeEditor1.AutoSize = false;
            this.ultraDateTimeEditor1.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.ultraDateTimeEditor1.Location = new System.Drawing.Point(73, 3);
            this.ultraDateTimeEditor1.MaskInput = "dd/mm/yyyy";
            this.ultraDateTimeEditor1.Name = "ultraDateTimeEditor1";
            this.ultraDateTimeEditor1.Size = new System.Drawing.Size(103, 22);
            this.ultraDateTimeEditor1.TabIndex = 20;
            this.ultraDateTimeEditor1.Value = null;
            // 
            // ultraLabel10
            // 
            appearance28.BackColor = System.Drawing.Color.Transparent;
            appearance28.TextVAlignAsString = "Middle";
            this.ultraLabel10.Appearance = appearance28;
            this.ultraLabel10.Location = new System.Drawing.Point(1, 3);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(65, 22);
            this.ultraLabel10.TabIndex = 6;
            this.ultraLabel10.Text = "Ngày cấp";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.ultraLabel3);
            this.panel5.Controls.Add(this.txtIndentication);
            this.panel5.Location = new System.Drawing.Point(10, 118);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(197, 31);
            this.panel5.TabIndex = 195;
            // 
            // ultraLabel3
            // 
            appearance29.BackColor = System.Drawing.Color.Transparent;
            appearance29.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance29;
            this.ultraLabel3.Location = new System.Drawing.Point(1, 3);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(77, 22);
            this.ultraLabel3.TabIndex = 6;
            this.ultraLabel3.Text = "Số CMND/HC";
            // 
            // txtIndentication
            // 
            this.txtIndentication.AutoSize = false;
            this.txtIndentication.Location = new System.Drawing.Point(95, 3);
            this.txtIndentication.Name = "txtIndentication";
            this.txtIndentication.Size = new System.Drawing.Size(99, 22);
            this.txtIndentication.TabIndex = 19;
            this.txtIndentication.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtIndentication_KeyPress);
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.ultraLabel12);
            this.panel14.Controls.Add(this.txtEmail);
            this.panel14.Location = new System.Drawing.Point(10, 185);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(558, 34);
            this.panel14.TabIndex = 705;
            // 
            // ultraLabel12
            // 
            appearance30.BackColor = System.Drawing.Color.Transparent;
            appearance30.TextVAlignAsString = "Middle";
            this.ultraLabel12.Appearance = appearance30;
            this.ultraLabel12.Location = new System.Drawing.Point(1, 6);
            this.ultraLabel12.Name = "ultraLabel12";
            this.ultraLabel12.Size = new System.Drawing.Size(49, 22);
            this.ultraLabel12.TabIndex = 6;
            this.ultraLabel12.Text = "Email";
            // 
            // txtEmail
            // 
            this.txtEmail.AutoSize = false;
            this.txtEmail.Location = new System.Drawing.Point(95, 6);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(460, 22);
            this.txtEmail.TabIndex = 25;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.ultraLabel6);
            this.panel8.Controls.Add(this.txtDTCoQuan);
            this.panel8.Location = new System.Drawing.Point(10, 148);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(197, 31);
            this.panel8.TabIndex = 245;
            // 
            // ultraLabel6
            // 
            appearance31.BackColor = System.Drawing.Color.Transparent;
            appearance31.TextVAlignAsString = "Middle";
            this.ultraLabel6.Appearance = appearance31;
            this.ultraLabel6.Location = new System.Drawing.Point(1, 3);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(77, 22);
            this.ultraLabel6.TabIndex = 6;
            this.ultraLabel6.Text = "ĐT cơ quan";
            // 
            // txtDTCoQuan
            // 
            this.txtDTCoQuan.AutoSize = false;
            this.txtDTCoQuan.Location = new System.Drawing.Point(95, 3);
            this.txtDTCoQuan.Name = "txtDTCoQuan";
            this.txtDTCoQuan.Size = new System.Drawing.Size(99, 22);
            this.txtDTCoQuan.TabIndex = 22;
            this.txtDTCoQuan.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDTCoQuan_KeyPress);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.cbbContactPrefix);
            this.panel4.Controls.Add(this.ultraLabel2);
            this.panel4.Location = new System.Drawing.Point(10, 19);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(197, 31);
            this.panel4.TabIndex = 155;
            // 
            // cbbContactPrefix
            // 
            this.cbbContactPrefix.AutoSize = false;
            this.cbbContactPrefix.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbContactPrefix.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbbContactPrefix.Location = new System.Drawing.Point(95, 4);
            this.cbbContactPrefix.Name = "cbbContactPrefix";
            this.cbbContactPrefix.Size = new System.Drawing.Size(99, 22);
            this.cbbContactPrefix.TabIndex = 15;
            // 
            // ultraLabel2
            // 
            appearance32.BackColor = System.Drawing.Color.Transparent;
            appearance32.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance32;
            this.ultraLabel2.Location = new System.Drawing.Point(1, 4);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(49, 22);
            this.ultraLabel2.TabIndex = 6;
            this.ultraLabel2.Text = "Xưng hô";
            // 
            // FInvestorDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(607, 533);
            this.Controls.Add(this.panel1);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FInvestorDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FInvestorDetail";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FInvestorDetail_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.panel27.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtIssueBy)).EndInit();
            this.panel26.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtePostedDate)).EndInit();
            this.panel22.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtWebsite)).EndInit();
            this.panel21.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtFax)).EndInit();
            this.panel20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail1)).EndInit();
            this.panel19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTel)).EndInit();
            this.panel18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtBusinessRegistrationNumber)).EndInit();
            this.panel11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbInvestorCategoryID)).EndInit();
            this.panel15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtBankName)).EndInit();
            this.panel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxCode)).EndInit();
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
            this.panel10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAcount)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtInvestorName1)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtInvestorCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraOptionSet1)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.panel25.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtContactAddress)).EndInit();
            this.panel24.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtContactTitle)).EndInit();
            this.panel23.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtInvestorName)).EndInit();
            this.panel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtContactMobile)).EndInit();
            this.panel16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtContactIssueBy)).EndInit();
            this.panel12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor1)).EndInit();
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtIndentication)).EndInit();
            this.panel14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail)).EndInit();
            this.panel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtDTCoQuan)).EndInit();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbContactPrefix)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private System.Windows.Forms.Panel panel2;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsActive;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet ultraOptionSet1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel15;
        private Infragistics.Win.Misc.UltraLabel ultraLabel13;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtBankName;
        private System.Windows.Forms.Panel panel9;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTaxCode;
        private System.Windows.Forms.Panel panel6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAddress;
        private System.Windows.Forms.Panel panel10;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtBankAcount;
        private System.Windows.Forms.Panel panel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtInvestorName1;
        private Infragistics.Win.Misc.UltraLabel lblInvestorCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtInvestorCode;
        private System.Windows.Forms.Panel panel11;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbInvestorCategoryID;
        private System.Windows.Forms.Panel panel22;
        private Infragistics.Win.Misc.UltraLabel ultraLabel20;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtWebsite;
        private System.Windows.Forms.Panel panel20;
        private Infragistics.Win.Misc.UltraLabel ultraLabel18;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtEmail1;
        private System.Windows.Forms.Panel panel19;
        private Infragistics.Win.Misc.UltraLabel ultraLabel17;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTel;
        private System.Windows.Forms.Panel panel18;
        private Infragistics.Win.Misc.UltraLabel ultraLabel16;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtBusinessRegistrationNumber;
        private System.Windows.Forms.Panel panel27;
        private Infragistics.Win.Misc.UltraLabel ultraLabel25;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtIssueBy;
        private System.Windows.Forms.Panel panel26;
        private Infragistics.Win.Misc.UltraLabel ultraLabel24;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private System.Windows.Forms.Panel panel25;
        private Infragistics.Win.Misc.UltraLabel ultraLabel23;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtContactAddress;
        private System.Windows.Forms.Panel panel24;
        private Infragistics.Win.Misc.UltraLabel ultraLabel22;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtContactTitle;
        private System.Windows.Forms.Panel panel23;
        private Infragistics.Win.Misc.UltraLabel ultraLabel21;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtInvestorName;
        private System.Windows.Forms.Panel panel16;
        private Infragistics.Win.Misc.UltraLabel ultraLabel14;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtContactIssueBy;
        private System.Windows.Forms.Panel panel12;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private System.Windows.Forms.Panel panel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtIndentication;
        private System.Windows.Forms.Panel panel14;
        private Infragistics.Win.Misc.UltraLabel ultraLabel12;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtEmail;
        private System.Windows.Forms.Panel panel8;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDTCoQuan;
        private System.Windows.Forms.Panel panel4;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbContactPrefix;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtePostedDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ultraDateTimeEditor1;
        private System.Windows.Forms.Panel panel7;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtContactMobile;
        private System.Windows.Forms.Panel panel21;
        private Infragistics.Win.Misc.UltraLabel ultraLabel19;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtFax;


    }
}