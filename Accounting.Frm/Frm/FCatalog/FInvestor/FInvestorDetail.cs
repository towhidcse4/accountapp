﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Accounting.TextMessage;
using System.Text.RegularExpressions;

namespace Accounting
{
    public partial class FInvestorDetail : CustormForm
    {

        #region khai báo
        private readonly IInvestorService _IInvestorService;
        private readonly IInvestorGroupService _IInvestorGroupService;
        private readonly IGenCodeService _IGenCodeService;

        Investor _Select = new Investor();
        List<string> ltemp = new List<string>() { "Ông", "Bà" };
        bool Them = true;
        public static bool isClose = true;

        #endregion

        #region khởi tạo
        public FInvestorDetail()
        {//Thêm

            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            //Khai báo các webservices
            _IInvestorService = IoC.Resolve<IInvestorService>();
            _IInvestorGroupService = IoC.Resolve<IInvestorGroupService>();
            _IGenCodeService = IoC.Resolve<IGenCodeService>();
            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            dtePostedDate.DateTime = DateTime.Now;
            ultraDateTimeEditor1.DateTime = DateTime.Now;
            this.Text = "Thêm mới nhà đầu tư";
            chkIsActive.Visible = false;
            #endregion
        }

        public FInvestorDetail(Investor temp)
        {//Sửa
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _Select = temp;
            Them = false;
            txtInvestorCode.Enabled = false;

            //Khai báo các webservices
            _IInvestorService = IoC.Resolve<IInvestorService>();
            _IInvestorGroupService = IoC.Resolve<IInvestorGroupService>();
            _IGenCodeService = IoC.Resolve<IGenCodeService>();
            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            //
            #endregion

            //Chỉnh sửa dữ liệu
            //----

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion

            #region Fill dữ liệu Obj vào control
            ObjandGUI(temp, false);
            this.Text = "Sửa nhà đầu tư";

            #endregion
        }
        void loadcbb()
        {
            //cbbInvestorCategoryID.DataSource = _IInvestorGroupService.GetAll();
            //cbbInvestorCategoryID.DisplayMember = "InvestorGroupName";
            //Utils.ConfigGrid(cbbInvestorCategoryID, ConstDatabase.InvestorGroup_TableName);
            this.ConfigCombo(Utils.ListInvestorGroup, cbbInvestorCategoryID, "InvestorGroupCode", "ID");
        }
        private void InitializeGUI()
        {
            //Tự sinh số chứng từ
            GenCode gencode = _IGenCodeService.getGenCode(12);
            string curValue = string.Empty;
            for (int i = 0; i < gencode.Length - gencode.CurrentValue.ToString().Length; i++) curValue += "0";
            txtInvestorCode.Text = string.Format("{0}{1}{2}{3}", gencode.Prefix, curValue, gencode.CurrentValue, gencode.Suffix);
            //khởi tạo cbb
            loadcbb();

            cbbContactPrefix.DataSource = ltemp;
            cbbContactPrefix.SelectedIndex = -1;
            ultraOptionSet1.CheckedIndex = 0;
        }
        #endregion

        #region Button Event
        /// <summary>
        /// Sự kiện nút Lưu lại
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click_1(object sender, EventArgs e)
        {
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion
            if (txtEmail.Text != "")
            {
                if (!isValidEmail(txtEmail.Text)) return;
            }

            if (txtEmail1.Text != "")
            {
                if (!isValidEmail(txtEmail1.Text)) return;
            }

            if (txtTel.Text != "")
            {
                if (!isPhone(txtTel.Text)) return;
            }
            if (txtContactMobile.Text != "")
            {
                if (!isPhone(txtContactMobile.Text)) return;
            }
            //if (txtTaxCode.Text != "")
            //{
            //    if (!isMST(txtTaxCode.Text)) return;
            //}
            //Gui to object
            #region Fill dữ liệu control vào obj
            Investor temp = Them ? new Investor() : _IInvestorService.Getbykey(_Select.ID);
            temp = ObjandGUI(temp, true);
            #endregion

            #region Thao tác CSDL
            try
            {
                _IInvestorService.BeginTran();

                if (Them)
                {
                    if (!CheckCode()) return;
                    _IInvestorService.CreateNew(temp);

                    Dictionary<string, string> kq = Utils.CheckNo(txtInvestorCode.Text);
                    if (kq != null)
                    {
                        GenCode gencode = _IGenCodeService.getGenCode(12);
                        gencode.Prefix = kq["Prefix"];
                        gencode.Suffix = kq["Suffix"];
                        gencode.CurrentValue = decimal.Parse(kq["Value"]) + 1;
                        _IGenCodeService.Update(gencode);
                    }

                }
                else _IInvestorService.Update(temp);
                //Cập nhật lại gencode

                _IInvestorService.CommitTran();
            }
            catch (Exception)
            {
                _IInvestorService.RolbackTran();
            }
            #endregion

            #region xử lý form, kết thúc form
            this.Close();
            isClose = false;
            #endregion
        }
        #endregion

        #region Utils
        /// <summary>
        /// get hoặc set một đối tượng vào giao diện xử lý
        /// </summary>
        /// <param name="input">đối tượng</param>
        /// <param name="isGet">True: get giá trị từ control gán vào đối tượng| False: set giá trị đối tượng vào control</param>
        /// <returns></returns>
        Investor ObjandGUI(Investor input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();  //Thêm mới

                input.InvestorCode = txtInvestorCode.Text;
                input.InvestorName = txtInvestorName1.Text;
                input.Address = txtAddress.Text;
                input.BankAccount = txtBankAcount.Text;
                input.ContactOfficeTel = txtDTCoQuan.Text;
                input.InvestorType = ultraOptionSet1.CheckedIndex;
                input.IdenticationNumber = txtIndentication.Text;
                input.BusinessRegistrationNumber = txtBusinessRegistrationNumber.Text;
                input.Tel = txtTel.Text;
                input.TaxCode = txtTaxCode.Text;
                input.ContactOfficeTel = txtDTCoQuan.Text;
                input.BankName = txtBankName.Text;
                input.Fax = txtFax.Text;
                input.ContactIssueBy = txtContactIssueBy.Text;
                input.IssueBy = txtIssueBy.Text;
                input.ContactMobile = txtContactMobile.Text;
                input.Website = txtWebsite.Text;
                input.Email = txtEmail.Text;

                input.Email = txtEmail1.Text;

                input.ContactTitle = txtContactTitle.Text;
                input.ContactAddress = txtContactAddress.Text;
                if (cbbContactPrefix.SelectedItem != null)
                    input.ContactPrefix = cbbContactPrefix.SelectedItem.ToString();
                input.ContactIssueDate = ultraDateTimeEditor1.DateTime;
                //input.IssueDate = (DateTime)ultraCalendarCombo2.Value;
                input.IssueDate = dtePostedDate.DateTime;
                //từ tài khoản
                InvestorGroup temp0 = (InvestorGroup)Utils.getSelectCbbItem(cbbInvestorCategoryID);
                if (temp0 == null) input.InvestorGroupID = null;
                else input.InvestorGroupID = temp0.ID;
                //========> input.OrderFixCode chưa xét
                input.IsActive = chkIsActive.Checked;
            }
            else
            {
                txtInvestorCode.Text = input.InvestorCode;
                txtInvestorName1.Text = input.InvestorName;
                ultraOptionSet1.CheckedIndex = input.InvestorType;
                txtIndentication.Text = input.IdenticationNumber;
                cbbContactPrefix.SelectedText = input.ContactPrefix;
                txtBusinessRegistrationNumber.Text = input.BusinessRegistrationNumber;
                txtAddress.Text = input.Address;
                txtBankAcount.Text = input.BankAccount;
                txtBankName.Text = input.BankName;
                txtDTCoQuan.Text = input.ContactOfficeTel;
                txtTel.Text = input.Tel;
                //ultraCalendarCombo1.Value = input.ContactIssueDate;
                //ultraCalendarCombo2.Value = input.IssueDate;
                if (input.IssueDate != null)
                {
                    dtePostedDate.DateTime = (DateTime)input.IssueDate;
                }
                if (input.ContactIssueDate != null)
                {
                    ultraDateTimeEditor1.DateTime = (DateTime)input.ContactIssueDate;
                }
                txtTaxCode.Text = input.TaxCode;
                txtBankName.Text = input.BankName;
                txtFax.Text = input.Fax;
                txtIssueBy.Text = input.IssueBy;
                txtContactIssueBy.Text = input.ContactIssueBy;
                txtContactMobile.Text = input.ContactMobile;
                txtWebsite.Text = input.Website;
                txtEmail.Text = input.Email;

                txtEmail1.Text = input.Email;

                txtContactTitle.Text = input.ContactTitle;
                txtContactAddress.Text = input.ContactAddress;

                //từ tài khoản
                foreach (var item in cbbInvestorCategoryID.Rows)
                {
                    if ((item.ListObject as InvestorGroup).ID == input.InvestorGroupID) cbbInvestorCategoryID.SelectedRow = item;
                }
                chkIsActive.Checked = input.IsActive;
            }
            return input;
        }

        bool CheckCode()
        {
            bool kq = true;
            //Check Error Chung
            //List<string> hh = _IInvestorService.Query.Select(a => a.InvestorCode).ToList();
            List<string> hh = _IInvestorService.GetCode();
            foreach (var item in hh)
            {
                if (item.Equals(txtInvestorCode.Text))
                {
                    MSG.Error(string.Format(resSystem.MSG_Catalog_Account6, "Nhà đầu tư"));
                    return false;
                }
            }

            return kq;
        }

        bool CheckError()
        {
            bool kq = true;
            //Check Error Chung

            //Check Error Riêng từng Form
            if (string.IsNullOrEmpty(txtInvestorCode.Text) || string.IsNullOrEmpty(txtInvestorName1.Text) || cbbInvestorCategoryID.SelectedRow == null)
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }

            return kq;
        }
        bool isPhone(string inputPhone)
        {
            string strRegex = @"0\d{9,10}";
            Regex re = new Regex(strRegex);
            if (re.IsMatch(inputPhone))
                return (true);
            else
                MSG.Warning(resSystem.MSG_Catalog_FInvestorDetail);
            return (false);
        }
        bool isValidEmail(string inputEmail)
        {
            string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                  @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                  @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            Regex re = new Regex(strRegex);
            if (re.IsMatch(inputEmail))
                return (true);
            else
                MSG.Warning(string.Format(resSystem.MSG_System_58, "này"));
            return (false);
        }
        #endregion


        private void ultraOptionSet1_ValueChanged_1(object sender, EventArgs e)
        {
            if (ultraOptionSet1.CheckedIndex == 0)
            {
                this.Size = new Size(623, 367);
                //panel1.Size = new Size(606, 327);
                chkIsActive.Location = new Point(14, 294);
                btnSave.Location = new Point(434, 289);
                btnClose.Location = new Point(524, 289);
                //ultraGroupBox1.Size = new Size(585, 248);
                ultraGroupBox2.Hide();
                panel2.TabIndex = 660;
                panel3.TabIndex = 661;
                panel4.TabIndex = 662;
                panel11.TabIndex = 663;
                panel6.TabIndex = 664;
                panel5.TabIndex = 665;
                panel12.TabIndex = 666;
                panel16.TabIndex = 667;
                panel10.TabIndex = 668;
                panel15.TabIndex = 669;
                panel8.TabIndex = 700;
                panel7.TabIndex = 702;
                panel9.TabIndex = 703;
                panel14.TabIndex = 705;
                chkIsActive.TabIndex = 706;
                btnSave.TabIndex = 707;
                btnClose.TabIndex = 708;
                panel16.Location = new Point(402, 112);
                panel15.Location = new Point(216, 142);
                panel14.Location = new Point(214, 206);
                panel12.Location = new Point(216, 112);
                panel11.Location = new Point(216, 50);
                panel10.Location = new Point(10, 142);
                panel9.Location = new Point(10, 204);
                panel8.Location = new Point(10, 173);
                panel7.Location = new Point(389, 173);
                panel6.Location = new Point(10, 82);
                panel5.Location = new Point(10, 112);
                panel4.Location = new Point(10, 50);
                ultraGroupBox1.Controls.Add(panel4); //173,31
                panel3.Location = new Point(216, 19);
                panel2.Location = new Point(10, 20);
                panel18.Hide();
                panel19.Hide();
                panel20.Hide();
                panel21.Hide();
                panel22.Hide();
                panel23.Hide();
                panel24.Hide();
                panel25.Hide();
                panel26.Hide();
                panel27.Hide();
                panel14.Size = new Size(352, 31);
                panel14.Location = new Point(216,204);
                txtEmail.Location = new Point(69, 3);
                txtEmail.Size = new Size(280, 22);
                ultraGroupBox1.Controls.Add(panel16);
                ultraGroupBox1.Controls.Add(panel14);
                ultraGroupBox1.Controls.Add(panel7);
                ultraGroupBox1.Controls.Add(panel8);
                ultraGroupBox1.Controls.Add(panel5);
                ultraGroupBox1.Controls.Add(panel12);
            }
            if (ultraOptionSet1.CheckedIndex == 1)
            {
                this.Size = new Size(623, 571);
                panel1.Size = new Size(606, 532);
                chkIsActive.Location = new Point(7, 500);
                btnSave.Location = new Point(434, 495);
                btnClose.Location = new Point(519, 495);
                ultraGroupBox1.Size = new Size(585, 241);
                ultraGroupBox2.Size = new Size(585, 226);
                ultraGroupBox2.Show();
                panel16.Location = new Point(402, 118);
                ultraGroupBox2.Controls.Add(panel16);
                panel15.Location = new Point(216, 142);
                panel14.Location = new Point(10, 185);
                ultraGroupBox2.Controls.Add(panel14);
                panel2.TabIndex = 1000;
                panel3.TabIndex = 1220;
                panel9.TabIndex = 3220;
                panel11.TabIndex = 4220;
                panel6.TabIndex = 5220;
                panel8.TabIndex = 6220;
                panel26.TabIndex = 7220;
                panel27.TabIndex = 8220;
                panel10.TabIndex = 9220;
                panel15.TabIndex = 10220;
                panel18.TabIndex = 5221;
                panel19.TabIndex = 11220;
                panel21.TabIndex = 12220;
                panel20.TabIndex = 13220;
                panel22.TabIndex = 14220;
                panel4.TabIndex = 16552;
                panel23.TabIndex = 16562;
                panel24.TabIndex = 16572;
                panel25.TabIndex = 16582;
                panel5.TabIndex = 16592;
                panel12.TabIndex = 16602;
                panel16.TabIndex = 16612;
                panel8.TabIndex = 16622;
                panel7.TabIndex = 16642;
                panel14.TabIndex = 16652;
                chkIsActive.TabIndex = 16662;
                btnSave.TabIndex = 16672;
                btnClose.TabIndex = 16682;
                panel12.Location = new Point(219, 118);
                ultraGroupBox2.Controls.Add(panel12);
                panel11.Location = new Point(216, 50);
                panel10.Location = new Point(10, 142);
                panel9.Location = new Point(10, 53);
                panel8.Location = new Point(10, 148);
                ultraGroupBox2.Controls.Add(panel8);
                panel7.Location = new Point(402, 146);
                txtContactMobile.Location = new Point(59,5);
                txtContactMobile.Size = new Size(103, 22);
                ultraGroupBox2.Controls.Add(panel7);
                panel6.Location = new Point(10, 82);
                panel5.Show();
                panel5.Location = new Point(10, 118);
                ultraGroupBox2.Controls.Add(panel5);
                panel4.Show();
                panel4.Location = new Point(10, 19);

                ultraGroupBox2.Controls.Add(panel4);
                panel3.Location = new Point(216, 19);
                panel2.Location = new Point(10, 20);
                panel18.Show();
                panel19.Show();
                panel20.Show();
                panel21.Show();
                panel22.Show();
                panel23.Show();
                panel24.Show();
                panel25.Show();
                panel26.Show();
                panel27.Show();
                panel14.Size = new Size(563, 34);
                txtEmail.Location = new Point(95, 3);
                txtEmail.Size = new Size(460, 22);
                txtInvestorName.Text = txtInvestorName1.Text;
            }
        }

        private void cbbInvestorCategoryID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            new FInvestorGroupDetail().ShowDialog(this);
            loadcbb();
            Utils.ClearCacheByType<InvestorGroup>();
            cbbInvestorCategoryID.SelectedText = FInvestorGroupDetail.InvestorGroupCode;
        }

        private void btnClose_Click_1(object sender, EventArgs e)
        {
            isClose = true;
            DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void txtInvestorCode_ValueChanged(object sender, EventArgs e)
        {

        }

        private void panel12_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtTaxCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar) || Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtDTCoQuan_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar) || Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void ultraTextEditor11_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar) || Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtContactMobile_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar) || Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtBankAcount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar) || Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtBusinessRegistrationNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar) || Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtIndentication_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar) || Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void FInvestorDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
