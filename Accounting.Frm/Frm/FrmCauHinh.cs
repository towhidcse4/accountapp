﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm
{
    public partial class FrmCauHinh : Form
    {
        public bool Exit = false;
        public FrmCauHinh()
        {
            InitializeComponent();
        }

        private void FrmCauHinh_Load(object sender, EventArgs e)
        {
            var lgAu = System.Configuration.ConfigurationManager.AppSettings["isUpdate"];
            var UpdateUrl = System.Configuration.ConfigurationManager.AppSettings["UpdateUrl"];
            //if (string.IsNullOrEmpty(lgAu)) return;
            string UpdateFile = "xxx.exe";
            //get version server
            Version versionLastest = GetLatestVersion(UpdateUrl);
            Version versionCurrent = GetLocalVersionNumber();
            if (ShallIDownloadTheLatest(versionCurrent, versionLastest) && lgAu == "true")
            {
                Process secondProc = new Process();
                if (File.Exists(UpdateFile))
                {
                    secondProc.StartInfo.FileName = UpdateFile;
                    secondProc.Start();
                    Exit = true;
                    Application.Exit();
                }
                else
                {
                    MessageBox.Show("Không có file" + UpdateFile + " trong thư mục");
                    Application.Exit();
                }
            }
        }
        #region Update - AC
        public class UpdateApp
        {
            public List<VersionApp> item;
        }
        public class VersionApp
        {
            public string number;
            public string linkUrl;
            public List<string> fileName;
            public string dateModified;
        }
        public class UpdateAppLocal
        {
            public List<VersionAppLocal> item;
        }
        public class VersionAppLocal
        {
            public string number;
            public string dateModified;
        }
        public static Version GetLocalVersionNumber()
        {
            try
            {
                System.Xml.Serialization.XmlSerializer reader = new System.Xml.Serialization.XmlSerializer(typeof(UpdateAppLocal));
                FileInfo fi = new FileInfo(Path.GetDirectoryName(Environment.GetCommandLineArgs()[0]) + "\\" + "vrs.xml");
                if (fi.Exists)
                {
                    using (System.IO.StreamReader file = new System.IO.StreamReader(fi.FullName))
                    {
                        UpdateAppLocal overview = (UpdateAppLocal)reader.Deserialize(file);
                        if (overview != null && overview.item.Count > 0)
                            return new Version(overview.item.Last().number);
                    }

                }
                return new Version("99.0.0.0");
            }
            catch (Exception ex)
            {
                return new Version("99.0.0.0");
            }
        }
        public static Version GetLatestVersion(string linkXML)
        {
            try
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(UpdateApp));
                WebClient webClient = new WebClient();
                UpdateApp overview = null;
                string receivedData = string.Empty;
                receivedData = webClient.DownloadString(linkXML).Trim();

                using (TextReader reader = new StringReader(receivedData))
                {
                    overview = (UpdateApp)serializer.Deserialize(reader);
                }
                if (overview != null && overview.item.Count > 0)
                    return new Version(overview.item.Last().number);
                else return new Version("0.0.0.0");
            }
            catch (Exception ex)
            {
                //ZMessageNotify.OkWarning("Cảnh báo: k update phiên bản!", AlertResource.Error_Title);
                //ex.ShowException();
                return new Version("0.0.0.0");
            }
        }
        public static void AddOrUpdateAppSettings(string key, string value)
        {
            try
            {
                var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var settings = configFile.AppSettings.Settings;
                if (settings[key] == null)
                {
                    settings.Add(key, value);
                }
                else
                {
                    settings[key].Value = value;
                }
                configFile.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
            }
            catch (ConfigurationErrorsException)
            {
                Console.WriteLine("Error writing app settings");
            }
        }
        public static bool ShallIDownloadTheLatest(Version localVersion, Version latestVersion)
        {
            if (localVersion.CompareTo(latestVersion) < 0)
            {
                return true;
            }
            return false;

        }
        #endregion
    }
}
