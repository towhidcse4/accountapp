﻿using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting
{
    public partial class FCatSysUsers : CatalogBase
    {
        #region khai báo
        ////private readonly ISaleDiscountPolicyService _ISaleDiscountPolicyService;
        private readonly ISysUserService _ISysUserService = Utils.ISysUserService;
        #endregion
        protected override void AddFunction()
        {
            WaitingFrm.StartWaiting();
            new FSysUser().ShowDialog(this);
            if (!FSysUser.isClose) LoadDuLieu();
            
        }
        protected override void EditFunction()
        {
            WaitingFrm.StartWaiting();
            if (uGrid.Selected.Rows.Count > 0)
            {
                SysUser temp = uGrid.Selected.Rows[0].ListObject as SysUser;
                //Investor Node = _IInvestorService.Getbykey((Guid)temp.ID);
                //ultraLabel1.Text = "Nhà đầu tư\t " + Node.InvestorCode;

                new FSysUser(temp).ShowDialog(this);
                if (!FSysUser.isClose) LoadDuLieu();
            }
            else
                MSG.Error(string.Format(resSystem.MSG_Catalog3, "một tài khoản người dùng"));
            
        }
        public FCatSysUsers()
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            this.uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            #endregion

            #region Tạo dữ liệu ban đầu
            LoadDuLieu();
            #endregion
        }
        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }
        private void LoadDuLieu(bool configGrid)
        {
            WaitingFrm.StartWaiting();
            #region Lấy dữ liệu từ CSDL
            List<SysUser> list1 = _ISysUserService.GetAll().OrderBy(p => p.username).ToList();
            _ISysUserService.UnbindSession(list1);
            list1 = _ISysUserService.GetAll().OrderBy(p => p.username).ToList();
            uGrid.DataSource = list1.ToArray();
            #endregion
            ConfigChildGrid(new List<SysRole>());

            if (uGrid.Selected.Rows.Count == 0)
            {
                uGrid.Rows[0].Selected = true;
            }
            if (configGrid) ConfigGrid(uGrid);
            WaitingFrm.StopWaiting();
        }

        #region Utils
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(uGrid, TextMessage.ConstDatabase.SysUser_TableName);
        }
        #endregion

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void uGrid_DoubleClick(object sender, EventArgs e)
        {
            EditFunction();
        }

        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        protected override void DeleteFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                SysUser temp = uGrid.Selected.Rows[0].ListObject as SysUser;
                if (temp.IsSystem)
                {
                    MSG.Warning("Đây là tài khoản hệ thống. \r\nBạn không có quyền xóa tài khoản này!");
                    return;
                }
                if (temp.userid == Authenticate.User.userid)
                {
                    MSG.Warning(string.Format("Bạn không thể xóa tài khoản hiện đang đăng nhập. \r\nVui lòng chuyển sang tài khoản quản trị khác để xóa tài khoản <{0}>", temp.username));
                    return;
                }
                if (MSG.Question(string.Format(resSystem.MSG_System_05, string.Format("người dùng \"{0}\"", temp.username))) == System.Windows.Forms.DialogResult.Yes)
                {
                    WaitingFrm.StartWaiting();
                    string username = temp.username/*.CloneObject()*/;
                    Guid userid = temp.userid/*.CloneObject()*/;
                    _ISysUserService.BeginTran();
                    _ISysUserService.Delete(temp);
                    _ISysUserService.CommitTran();
                    Logs.Save(0, resSystem.Logs_Title_00, Logs.Action.Delete, username, userid, string.Format("Xóa tài khoản người dùng <{0}>", username));
                    LoadDuLieu();
                }
            }
            else
                MSG.Error(string.Format(resSystem.MSG_Catalog2, "một tài khoản người dùng"));
            
        }

        protected override void ResetFunction()
        {
            LoadDuLieu(false);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void uGrid_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                SysUser Node = uGrid.Selected.Rows[0].ListObject as SysUser;
                txtFullName.Text = Node.FullName;
                txtJob.Text = Node.Job;
                txtAddress.Text = Node.Address;
                txtDescription.Text = Node.Description;
                txtEmail.Text = Node.Email;
                txtFax.Text = Node.Fax;
                txtHomePhone.Text = Node.HomePhone;
                txtMobilePhone.Text = Node.MobilePhone;
                txtWebsite.Text = Node.Website;
                txtWorkPhone.Text = Node.WorkPhone;
                txtBirthDay.Text = Node.BirthDay == null ? "" : Node.BirthDay.Value.Date.ToString(Constants.DdMMyyyy);
                txtIDCard.Text = Node.IDCard;
                List<country> countries = Utils.DicCounties.Values.ToList();
                var country = countries.FirstOrDefault(c => c.countryCode == Node.Country);
                if (country != null)
                    txtCountry.Text = country.countryName;
                txtProvince.Text = Node.Province;
                if (Node.Photo != null)
                {
                    pixPhoto.Image = Node.Photo.ByteArrayToImage();
                }
                btnDelete.Enabled = !Node.IsSystem;
                var roles = Node.Roles.ToList();

                btnAddRole.Enabled = btnRemoveRole.Enabled = false;
                if (Node.username != "ADMIN" && Node.userid != Authenticate.User.userid)
                    btnAddRole.Enabled = true;
                ConfigChildGrid(roles.Count > 0 ? roles : new List<SysRole>());
            }
        }

        void ConfigChildGrid(List<SysRole> roles, bool config = true)
        {
            uGrid0.DataSource = roles;
            if (config)
            {
                uGrid0.ConfigGrid(ConstDatabase.SysUserRole_TableName, true, false);
                UltraGridBand band = uGrid0.DisplayLayout.Bands[0];
                band.Summaries.Clear();
                foreach (var column in uGrid0.DisplayLayout.Bands[0].Columns)
                {
                    column.CellActivation = Activation.NoEdit;
                }
            }
            if (uGrid0.Rows.Count > 0)
                uGrid0.Rows[0].Selected = true;
        }


        private void uGrid_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }

        private void tsmAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void tsmEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            ResetFunction();
        }

        private void uGrid0_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                SysUser Node = uGrid.Selected.Rows[0].ListObject as SysUser;
                btnRemoveRole.Enabled = false;
                if (Node.username != "ADMIN" && Node.userid != Authenticate.User.userid)
                {
                    if (uGrid0.Selected.Rows.Count > 0)
                        btnRemoveRole.Enabled = true;
                }
            }
        }

        private void btnAddRole_Click(object sender, EventArgs e)
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                var user = uGrid.Selected.Rows[0].ListObject as SysUser;
                var f = new FChoiceRole(user.Roles.ToList());
                if (f.ShowDialog(this) == DialogResult.OK)
                {
                    Utils.ISysUserroleService.BeginTran();
                    try
                    {
                        //user.Roles.Add(f.Role);
                        var temp = new SysUserrole
                        {
                            UserID = user.userid,
                            RoleID = f.Role.RoleID
                        };
                        Utils.ISysUserroleService.CreateNew(temp);
                        Utils.ISysUserroleService.CommitTran();
                        var userid = user.userid;
                        List<SysUser> list1 = _ISysUserService.GetAll().OrderBy(p => p.username).ToList();
                        _ISysUserService.UnbindSession(list1);
                        list1 = _ISysUserService.GetAll().OrderBy(p => p.username).ToList();
                        uGrid.DataSource = list1.ToArray();
                        var row = uGrid.Rows.FirstOrDefault(r => (r.ListObject as SysUser).userid == userid);
                        if (row != null)
                            row.Selected = true;
                        //ConfigChildGrid(Utils.ISysUserService.Getbykey(user.userid).Roles.ToList(), false);
                    }
                    catch (Exception)
                    {
                        Utils.ISysUserroleService.RolbackTran();
                    }
                }
            }
        }

        private void btnRemoveRole_Click(object sender, EventArgs e)
        {
            if (uGrid.Selected.Rows.Count > 0 && uGrid0.Selected.Rows.Count > 0)
            {
                var user = uGrid.Selected.Rows[0].ListObject as SysUser;
                var role = uGrid0.Selected.Rows[0].ListObject as SysRole;
                var ques = string.Format("Bạn có thực sự muốn loại bỏ vai trò \"{0}\" khỏi người dùng \"{1}\" không ?", role.RoleName, user.FullName);
                if (MSG.MessageBoxStand(ques, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
                {
                    var userrole = Utils.ISysUserroleService.Query.FirstOrDefault(ur => ur.UserID == user.userid && ur.RoleID == role.RoleID);
                    Utils.ISysUserroleService.BeginTran();
                    try
                    {
                        Utils.ISysUserroleService.Delete(userrole);
                        Utils.ISysUserroleService.CommitTran();
                        var userid = user.userid;
                        List<SysUser> list1 = _ISysUserService.GetAll().OrderBy(p => p.username).ToList();
                        _ISysUserService.UnbindSession(list1);
                        list1 = _ISysUserService.GetAll().OrderBy(p => p.username).ToList();
                        uGrid.DataSource = list1.ToArray();
                        var row = uGrid.Rows.FirstOrDefault(r => (r.ListObject as SysUser).userid == userid);
                        if (row != null)
                            row.Selected = true;
                        //ConfigChildGrid(Utils.ISysUserService.Getbykey(user.userid).Roles.ToList(), false);
                    }
                    catch (Exception)
                    {
                        Utils.ISysUserroleService.RolbackTran();
                    }
                }
            }
        }

        private void FCatSysUsers_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGrid.ResetText();
            uGrid.ResetUpdateMode();
            uGrid.ResetExitEditModeOnLeave();
            uGrid.ResetRowUpdateCancelAction();
            uGrid.DataSource = null;
            uGrid.Layouts.Clear();
            uGrid.ResetLayouts();
            uGrid.ResetDisplayLayout();
            uGrid.Refresh();
            uGrid.ClearUndoHistory();
            uGrid.ClearXsdConstraints();

            uGrid0.ResetText();
            uGrid0.ResetUpdateMode();
            uGrid0.ResetExitEditModeOnLeave();
            uGrid0.ResetRowUpdateCancelAction();
            uGrid0.DataSource = null;
            uGrid0.Layouts.Clear();
            uGrid0.ResetLayouts();
            uGrid0.ResetDisplayLayout();
            uGrid0.Refresh();
            uGrid0.ClearUndoHistory();
            uGrid0.ClearXsdConstraints();
        }

        private void FCatSysUsers_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
