﻿using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting
{
    public partial class FCatSysRoles : CustormForm
    {
        public FCatSysRoles()
        {
            InitializeComponent();
            LoadData();
        }

        private void LoadData(bool configGrid = true)
        {
            WaitingFrm.StartWaiting();
            var input = Utils.ISysRoleService.Query.OrderBy(r => r.RoleCode).ToList();
            Utils.ISysRoleService.UnbindSession(input);
            input = Utils.ISysRoleService.Query.OrderBy(r => r.RoleCode).ToList();
            uGrid.DataSource = input;
            if (configGrid) ConfigGrid(uGrid);
            ConfigChildGrid(new List<SysUser>());
            if (uGrid.Selected.Rows.Count == 0)
            {
                uGrid.Rows[0].Selected = true;
            }
            WaitingFrm.StopWaiting();
        }

        private void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid uGrid)
        {
            Utils.ConfigGrid(uGrid, TextMessage.ConstDatabase.SysUserRole_TableName);
        }

        void ConfigChildGrid(List<SysUser> users)
        {
            uGrid0.DataSource = users;
            uGrid0.ConfigGrid(ConstDatabase.SysRoleUser_TableName, true, false);
            UltraGridBand band = uGrid0.DisplayLayout.Bands[0];
            band.Summaries.Clear();
            foreach (var column in uGrid0.DisplayLayout.Bands[0].Columns)
            {
                column.CellActivation = Activation.NoEdit;
            }
        }

        private void uGrid_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            btnEdit.Enabled = btnDelete.Enabled = btnConfigPermission.Enabled = false;

            if (uGrid.Selected.Rows.Count > 0)
            {
                SysRole Node = uGrid.Selected.Rows[0].ListObject as SysRole;
                btnEdit.Enabled = true;
                txtRoleCode.Text = Node.RoleCode;
                txtRoleName.Text = Node.RoleName;
                txtDescription.Text = Node.Description;
                var users = Node.Users.ToList();

                ConfigChildGrid(users.Count > 0 ? users : new List<SysUser>());
                if (Node.RoleCode != "ADMIN" && !Authenticate.User.Roles.Any(r => r.RoleID == Node.RoleID))
                {
                    btnConfigPermission.Enabled = true;
                    if (!Node.IsSystem)
                        btnDelete.Enabled = true;
                }
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (new FSysRole().ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
                LoadData(false);
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                SysRole Node = uGrid.Selected.Rows[0].ListObject as SysRole;
                if (new FSysRole(Node).ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
                    LoadData(false);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                SysRole Node = uGrid.Selected.Rows[0].ListObject as SysRole;
                if (MSG.MessageBoxStand(string.Format("Bạn có thực sự muốn xóa vai trò \"{0}\" không?", Node.RoleName), MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
                {
                    Utils.ISysRoleService.BeginTran();
                    try
                    {
                        Utils.ISysRoleService.Delete(Node.RoleID);
                        Utils.ISysRoleService.CommitTran();
                    }
                    catch (Exception)
                    {
                        Utils.ISysRoleService.RolbackTran();
                        MSG.Warning(string.Format("Có lỗi xảy ra khi xóa vai trò \"{0}\".", Node.RoleName));
                    }
                    LoadData();
                }
            }
            
        }

        private void btnConfigPermission_Click(object sender, EventArgs e)
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                SysRole Node = uGrid.Selected.Rows[0].ListObject as SysRole;
                var f = new FSysPermissions(Node);
                if (f.ShowDialog(this) == DialogResult.OK)
                {
                    LoadData(false);
                }
            }
        }

        private void FCatSysRoles_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGrid.ResetText();
            uGrid.ResetUpdateMode();
            uGrid.ResetExitEditModeOnLeave();
            uGrid.ResetRowUpdateCancelAction();
            uGrid.DataSource = null;
            uGrid.Layouts.Clear();
            uGrid.ResetLayouts();
            uGrid.ResetDisplayLayout();
            uGrid.Refresh();
            uGrid.ClearUndoHistory();
            uGrid.ClearXsdConstraints();

            uGrid0.ResetText();
            uGrid0.ResetUpdateMode();
            uGrid0.ResetExitEditModeOnLeave();
            uGrid0.ResetRowUpdateCancelAction();
            uGrid0.DataSource = null;
            uGrid0.Layouts.Clear();
            uGrid0.ResetLayouts();
            uGrid0.ResetDisplayLayout();
            uGrid0.Refresh();
            uGrid0.ClearUndoHistory();
            uGrid0.ClearXsdConstraints();
        }

        private void FCatSysRoles_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
