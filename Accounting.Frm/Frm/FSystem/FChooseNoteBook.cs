﻿using Accounting.TextMessage;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core;

namespace Accounting.Frm.FReport
{
    public partial class FChooseNoteBook : Form
    {
        string[] accountingBook = new string[] { "Sổ tài chính", "Số tài chính và sổ quản trị" };
        string[] manageBook = new string[] { "Sổ quản trị", "Sổ tài chính và sổ quản trị" };
        public FChooseNoteBook()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void optChooseNoteBook_ValueChanged(object sender, EventArgs e)
        {
            if(optChooseNoteBook.CheckedIndex == 0)
            {
                cbbNoteBook.DataSource = accountingBook;                
            }
            else if(optChooseNoteBook.CheckedIndex == 1)
            {
                cbbNoteBook.DataSource = manageBook;
            }
            Utils.ConfigGrid(cbbNoteBook, "");
        }
    }
}
