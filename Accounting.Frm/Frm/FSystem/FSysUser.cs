﻿using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting
{
    public partial class FSysUser : DialogForm
    {
        public static bool isClose = true;
        private bool isAdd = true;
        private SysUser sysUser;
        private SysUserrole sysUserRole;
        bool checkWP = true;
        bool checkMP = true;
        bool checkHP = true;
        bool checkF = true;
        ISysRoleService _ISysRoleService { get { return IoC.Resolve<ISysRoleService>(); } }
        ISysUserroleService _ISysUserroleService { get { return IoC.Resolve<ISysUserroleService>(); } }
        public FSysUser()
        {
            InitializeComponent();
            Text = "Thêm mới người dùng";
            isAdd = true;
            sysUser = new SysUser();
            sysUserRole = new SysUserrole();
            chkIsActive.CheckState = CheckState.Checked;
            chkIsActive.Visible = false;
            txtUsername.Enabled = true;
            ConfigControl();
            WaitingFrm.StopWaiting();
        }
        public FSysUser(SysUser user)
        {
            InitializeComponent();
            Text = "Thay đổi thông tin người dùng";
            isAdd = false;
            sysUser = user;
            sysUserRole = new SysUserrole();
            chkIsActive.Visible = true;
            chkIsActive.Enabled = !user.IsSystem;
            txtUsername.Enabled = false;
            ConfigControl();
            WaitingFrm.StopWaiting();
        }

        private void ConfigControl()
        {
            List<country> countries = Utils.DicCounties.Values.ToList();
            foreach (var item in countries)
            {
                cbbCountry.Items.Add(item.countryCode, item.countryName);
            }

            cbbSystemRole.DataSource = _ISysRoleService.GetAll();
            Utils.ConfigGrid(cbbSystemRole, ConstDatabase.SystemRole_Table);
            UltraGridColumn c = cbbSystemRole.DisplayLayout.Bands[0].Columns.Add();
            if (c.Key == "")
                c.Key = "Selected";
            c.Header.Caption = string.Empty;
            c.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.Always;
            c.DataType = typeof(bool);
            c.Header.VisiblePosition = 0;
            cbbSystemRole.CheckedListSettings.ListSeparator = "/";
            cbbSystemRole.CheckedListSettings.ItemCheckArea = ItemCheckArea.Item;
            cbbSystemRole.CheckedListSettings.CheckStateMember = "Selected";
            cbbSystemRole.CheckedListSettings.EditorValueSource = EditorWithComboValueSource.CheckedItems;
            cbbSystemRole.DisplayMember = "RoleCode";
            cbbSystemRole.ValueMember = "RoleID";

            if (!isAdd)
            {
                cbbCountry.Text = sysUser.Country;
                txtCity.Value = sysUser.City;
                txtDescription.Value = sysUser.Description;
                txtFullName.Value = sysUser.FullName;
                txtIDCard.Value = sysUser.IDCard;
                txtEmail.Value = sysUser.Email;
                txtAdress.Value = sysUser.Address;
                txtFax.Value = sysUser.Fax;
                txtHomePhone.Value = sysUser.HomePhone;
                txtJob.Value = sysUser.Job;
                txtMobilePhone.Value = sysUser.MobilePhone;
                txtPassword.Value = sysUser.password;
                txtRepassword.Value = sysUser.password;
                txtUsername.Value = sysUser.username;
                txtWebsite.Value = sysUser.Website;
                txtWorkPhone.Value = sysUser.WorkPhone;
                txtProvince.Value = sysUser.Province;
                chkIsActive.CheckState = sysUser.IsActive ? CheckState.Checked : CheckState.Unchecked;
                if (sysUser.BirthDay.HasValue)
                    dteBirthDay.Value = sysUser.BirthDay.Value;
                else dteBirthDay.Value = "";
                if (sysUser.Photo != null)
                    picPhoto.Image = sysUser.Photo.ByteArrayToImage();

                List<Guid> lst = _ISysRoleService.GetRoleByUserID(sysUser.userid);
                if (lst.Count > 0)
                {
                    foreach (var item in cbbSystemRole.Rows)
                    {
                        SysRole sysrole = item.ListObject as SysRole;
                        for (int i = 0; i < lst.Count; i++)
                        {
                            if (sysrole.RoleID == lst[i])
                                item.Cells["Selected"].Value = true;
                        }
                    }
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

            if (!checkWP)
            {
                MSG.Warning("Số điện thoại cơ quan nhập sai định dạng !");
                return;
            }
            if (!checkMP)
            {
                MSG.Warning("Số điện thoại di động nhập sai định dạng !");
                return;
            }
            if (!checkHP)
            {
                MSG.Warning("Số điện thoại nhà riêng nhập sai định dạng !");
                return;
            }
            if (!checkF)
            {
                MSG.Warning("Số fax nhập sai định dạng !");
                return;
            }
            if (string.IsNullOrEmpty(txtUsername.Text.Trim()))
            {
                MSG.Warning("Họ và tên không được để trống!");
                tabControl.Tabs[0].Selected = true;
                txtUsername.Focus();
                return;
            }
            if (string.IsNullOrEmpty(txtPassword.Text.Trim()))
            {
                MSG.Warning("Mật khẩu không được để trống!");
                tabControl.Tabs[0].Selected = true;
                txtPassword.Focus();
                return;
            }
            if (string.IsNullOrEmpty(txtRepassword.Text.Trim()))
            {
                MSG.Warning("Hãy xác nhận lại mật khẩu!");
                tabControl.Tabs[0].Selected = true;
                txtRepassword.Focus();
                return;
            }
            if (txtRepassword.Text.Trim() != txtPassword.Text.Trim())
            {
                MSG.Warning("Xác nhận lại mật khẩu không chính xác!");
                tabControl.Tabs[0].Selected = true;
                txtRepassword.Focus();
                return;
            }
            if (cbbSystemRole.Value == null)
            {
                MSG.Warning("Vai trò không được để trống");
                tabControl.Tabs[0].Selected = true;
                cbbSystemRole.Focus();
                return;
            }
            if (string.IsNullOrEmpty(txtFullName.Text.Trim()))
            {
                MSG.Warning("Họ và tên không được để trống!");
                tabControl.Tabs[0].Selected = true;
                txtFullName.Focus();
                return;
            }
            if (!string.IsNullOrEmpty(txtEmail.Text.Trim()) && !txtEmail.Text.IsValidEmail())
            {
                MSG.Warning("Định dạng email không chính xác!");
                tabControl.Tabs[1].Selected = true;
                txtEmail.Focus();
                return;
            }
            if (MSG.MessageBoxStand("Bạn có thực sự muốn lưu thay đổi này không?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if (!isAdd)
                {
                    sysUser = Utils.ISysUserService.Getbykey(sysUser.userid);
                    Utils.ISysUserService.UnbindSession(sysUser);
                    sysUser = Utils.ISysUserService.Getbykey(sysUser.userid);
                }
                sysUser.Country = cbbCountry.Text;
                sysUser.City = txtCity.Text.Trim();
                sysUser.Description = txtDescription.Text.Trim();
                sysUser.FullName = txtFullName.Text.Trim();
                sysUser.IDCard = txtIDCard.Text.Trim();
                sysUser.Email = txtEmail.Text.Trim();
                sysUser.Address = txtAdress.Text.Trim();

                sysUser.Job = txtJob.Text.Trim();
                sysUser.Province = txtProvince.Text.Trim();
                if (!isAdd && txtPassword.Value.ToString() == sysUser.password) { }
                else
                    sysUser.password = FX.Utils.Encryption.StringToMD5Hash(txtPassword.Value.ToString());
                if (isAdd)
                {
                    sysUser.PasswordFormat = 1;
                    sysUser.PasswordSalt = "MD5";
                }
                sysUser.Fax = txtFax.Text.Trim();
                sysUser.WorkPhone = txtWorkPhone.Text.Trim();
                sysUser.HomePhone = txtHomePhone.Text.Trim();
                sysUser.MobilePhone = txtMobilePhone.Text.Trim();
                sysUser.username = txtUsername.Text.Trim();
                sysUser.Website = txtWebsite.Text.Trim();
                sysUser.IsActive = chkIsActive.CheckState == CheckState.Checked;
                if (dteBirthDay.Value != null)
                    sysUser.BirthDay = dteBirthDay.DateTime;
                else sysUser.BirthDay = null;
                if (picPhoto.Image != null)
                    sysUser.Photo = Utils.converterDemo(picPhoto.Image as Bitmap);
                Utils.ISysUserService.BeginTran();
                try
                {
                    if (isAdd)
                    {
                        Utils.ISysUserService.CreateNew(sysUser);
                        if (cbbSystemRole.Text != "")
                        {
                            foreach (var item1 in cbbSystemRole.CheckedRows)
                            {
                                SysRole sysrole = (SysRole)item1.ListObject;
                                SysUserrole sur = new SysUserrole();
                                sur.ID = Guid.NewGuid();
                                sur.RoleID = sysrole.RoleID;
                                sur.UserID = sysUser.userid;
                                _ISysUserroleService.CreateNew(sur);
                            }
                        }
                    }
                    else
                    {
                        Utils.ISysUserService.Update(sysUser);
                        List<SysUserrole> lstUserRole = _ISysUserroleService.GetUserroleByUserID(sysUser.userid);
                        if (lstUserRole.Count > 0)
                        {
                            foreach (var item in lstUserRole)
                            {
                                _ISysUserroleService.Delete(item);
                            }
                        }

                        if (cbbSystemRole.Text != "")
                        {
                            foreach (var item1 in cbbSystemRole.CheckedRows)
                            {
                                SysRole sysrole = (SysRole)item1.ListObject;
                                SysUserrole sur = new SysUserrole();
                                sur.ID = Guid.NewGuid();
                                sur.RoleID = sysrole.RoleID;
                                sur.UserID = sysUser.userid;
                                _ISysUserroleService.CreateNew(sur);
                            }
                        }
                    }

                    Utils.ISysUserService.CommitTran();
                    Logs.Save(0, resSystem.Logs_Title_00, isAdd ? Logs.Action.Add : Logs.Action.Edit, sysUser.username, sysUser.userid, string.Format("{0} tài khoản người dùng <{1}>", isAdd ? "Thêm mới" : "Thay đổi", sysUser.username));
                }
                catch (Exception ex)
                {
                    Utils.ISysUserService.RolbackTran();
                    MSG.Warning(string.Format("Lỗi {0} tài khoản người dùng! \r\nVui lòng thực hiện lại.", isAdd ? "khởi tạo" : "cập nhật"));
                    return;
                }
                isClose = false;
                Close();
            }
        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Filter = @"All Images|*.BMP;*.DIB;*.RLE;*.JPG;*.JPEG;*.JPE;*.JFIF;*.GIF;*.TIF;*.TIFF;*.PNG|BMP Files: (*.BMP;*.DIB;*.RLE)|*.BMP;*.DIB;*.RLE|JPEG Files: (*.JPG;*.JPEG;*.JPE;*.JFIF)|*.JPG;*.JPEG;*.JPE;*.JFIF|GIF Files: (*.GIF)|*.GIF|TIFF Files: (*.TIF;*.TIFF)|*.TIF;*.TIFF|PNG Files: (*.PNG)|*.PNG";
            fileDialog.FilterIndex = 1;
            //fileDialog.ShowDialog(this);
            var result = fileDialog.ShowDialog(this);

            if (result == DialogResult.OK || result == DialogResult.Yes)
            {
                //giới hạn dung lượng ảnh cho phép là 50 kb
                double fileSize = (double)new System.IO.FileInfo(fileDialog.FileName).Length / 1024;
                if (fileSize > 50)
                {
                    MSG.Error(resSystem.MSG_Catalog_FBankAccountDetail);
                    return;
                }
                //Kích thước ảnh cho phép không lớn hơn 128x128
                try
                {
                    //Image temp = System.Drawing.Image.FromFile(fileDialog.FileName);
                    //if (temp.Width > 128 || temp.Height > 128)
                    //{
                    //    MSG.Error(resSystem.MSG_Catalog_FBankAccountDetail1);
                    //    return;
                    //}
                    picPhoto.Image = System.Drawing.Image.FromFile(fileDialog.FileName);
                }
                catch (Exception)
                {
                    MSG.Error(resSystem.MSG_Catalog_FBank);
                    return;
                }

            }
        }

        private void txtWorkPhone_ValueChanged(object sender, EventArgs e)
        {
            checkWP = true;
            foreach (Char c in txtWorkPhone.Text.Trim())
            {
                if (!Char.IsDigit(c))
                {
                    checkWP = false;
                }
            }
            return;
        }

        private void txtFax_ValueChanged(object sender, EventArgs e)
        {
            checkF = true;
            foreach (Char c in txtFax.Text.Trim())
            {
                if (!Char.IsDigit(c))
                {
                    checkF = false;
                }
            }
            return;
        }

        private void txtMobilePhone_ValueChanged(object sender, EventArgs e)
        {
            checkMP = true;
            foreach (Char c in txtMobilePhone.Text.Trim())
            {
                if (!Char.IsDigit(c))
                {
                    checkMP = false;
                }
            }
            return;
        }

        private void txtHomePhone_ValueChanged(object sender, EventArgs e)
        {
            checkHP = true;
            foreach (Char c in txtHomePhone.Text.Trim())
            {
                if (!Char.IsDigit(c))
                {
                    checkHP = false;
                }
            }
            return;
        }

        private void txtWorkPhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= 48 && e.KeyChar <= 57 || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtMobilePhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= 48 && e.KeyChar <= 57 || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtHomePhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= 48 && e.KeyChar <= 57 || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtFax_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= 48 && e.KeyChar <= 57 || e.KeyChar == 8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void FSysUser_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
