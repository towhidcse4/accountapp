﻿using Accounting.Core.Domain;
using Accounting.TextMessage;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting
{
    public partial class FViewSysLog : DialogForm
    {
        public FViewSysLog(List<SysLog> input)
        {
            InitializeComponent();
            uGrid.DataSource = input;

            if (uGrid.Selected.Rows.Count == 0)
            {
                if (uGrid.Rows.Count > 0)
                    uGrid.Rows[0].Selected = true;
            }
            ConfigGrid(uGrid);
        }
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            utralGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            Utils.ConfigGrid(utralGrid, TextMessage.ConstDatabase.SysLog_TableName);
            foreach (var column in utralGrid.DisplayLayout.Bands[0].Columns)
            {
                column.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            }
            utralGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.ExtendedAutoDrag;
            utralGrid.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {

            try
            {
                var fileDialog = new OpenFileDialog();
                fileDialog.Title = "Chọn tệp nhật ký truy cập";
                fileDialog.Filter = @"XML Files (*.xml)|*.xml";
                if (fileDialog.ShowDialog() == DialogResult.OK)
                {
                    System.Xml.Serialization.XmlSerializer reader =
                        new System.Xml.Serialization.XmlSerializer(typeof(List<SysLog>));
                    System.IO.StreamReader file = new
                       System.IO.StreamReader(fileDialog.FileName);
                    List<SysLog> overview = new List<SysLog>();
                    try
                    {
                        overview = (List<SysLog>)reader.Deserialize(file);
                    }
                    catch (Exception)
                    {
                        MSG.Warning("Lỗi định dạng tệp nhật ký.");
                        goto end;
                    }
                    uGrid.DataSource = overview;
                    if (uGrid.Selected.Rows.Count == 0)
                    {
                        if (uGrid.Rows.Count > 0)
                            uGrid.Rows[0].Selected = true;
                    }
                end:
                    file.Close();
                }
            }
            catch (Exception)
            {
                MSG.Warning("Không thể mở tệp dữ liệu!");
            }
        }

        private void tmsImport_Click(object sender, EventArgs e)
        {
            btnOpen_Click(btnOpen, null);
        }
    }
}
