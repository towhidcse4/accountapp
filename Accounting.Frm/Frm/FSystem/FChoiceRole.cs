﻿using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting
{
    public partial class FChoiceRole : DialogForm
    {
        private SysRole _role;

        public SysRole Role
        {
            get { return _role; }
            private set { _role = value; }
        }
        public FChoiceRole(List<SysRole> roles)
        {
            InitializeComponent();
            var source = Utils.ISysRoleService.Query.Where(r => !roles.Contains(r)).ToList();
            uGrid.DataSource = source;
            uGrid.ConfigGrid(ConstDatabase.SysUserRole_TableName, true, false);
            UltraGridBand band = uGrid.DisplayLayout.Bands[0];
            band.Summaries.Clear();
            foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
            {
                column.CellActivation = Activation.NoEdit;
            }
            if (uGrid.Rows.Count > 0)
                uGrid.Rows[0].Selected = true;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void uGrid_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            btnApply.Enabled = uGrid.Selected.Rows.Count > 0;
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                _role = uGrid.Selected.Rows[0].ListObject as SysRole;
                DialogResult = DialogResult.OK;
                Close();
            }
        }
    }
}
