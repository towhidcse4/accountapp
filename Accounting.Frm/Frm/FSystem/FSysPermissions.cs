﻿using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinTree;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting
{
    public partial class FSysPermissions : CustormForm
    {
        private Guid roleId;
        private List<SysRolepermission> lstRolePermiss = new List<SysRolepermission>();
        public FSysPermissions(SysRole role)
        {
            InitializeComponent();
            #region Thiết lập ban đầu cho Form
            this.roleId = role.RoleID;
            lstRolePermiss = Utils.ISysRolepermissionService.Query.Where(rp => rp.RoleID == roleId).ToList();
            this.uTree.InitializeDataNode += new Infragistics.Win.UltraWinTree.InitializeDataNodeEventHandler(Utils.uTree_InitializeDataNode);
            this.uTree.AfterDataNodesCollectionPopulated += new Infragistics.Win.UltraWinTree.AfterDataNodesCollectionPopulatedEventHandler(Utils.uTree_AfterDataNodesCollectionPopulated);
            this.uTree.ColumnSetGenerated += new Infragistics.Win.UltraWinTree.ColumnSetGeneratedEventHandler(this.uTree_ColumnSetGenerated);
            this.uTree.AfterSelect += uTree_AfterSelect;
            #endregion
            LoadData();
        }

        void uTree_AfterSelect(object sender, SelectEventArgs e)
        {
            var subSystemCode = ((UltraTreeNode)e.NewSelections.All[0]).Text;
            if (subSystemCode.StartsWith("Parent_"))
            {
                uGrid.DataSource = new List<SysPermission>();
                uGrid.DataBind();
                return;
            }
            List<SysPermission> lst = Utils.ISysPermissionService.Query.Where(x=> !new[] { "PIS", "PIO", "EXD", "EXR" }.Contains(x.PermissionCode)).OrderBy(p => p.OrderPriority).ToList().CloneObject();
            if (subSystemCode.StartsWith("CAT_"))
                lst = lst.Where(p => !new[] { "BO", "PR", "IN", "PIS", "GIV", "EXR", "PIO", "GH", "CCO", "ELP" }.Contains(p.PermissionCode)).ToList();
            else if (subSystemCode.StartsWith("Rpt_"))
                lst = lst.Where(p => !new[] { "BO", "IN", "PIS", "EXD", "GIV", "PIO", "GH", "CCO", "ELP" }.Contains(p.PermissionCode)).ToList();
            uGrid.DataSource = lst;
            uGrid.DataBind();
            var T = true;
            foreach(var item in lst)
            {
                if (item.Check)
                {
                    T = false;
                    break;
                }
            }
            if(T) uGrid.DisplayLayout.Bands[0].Columns[0].SetHeaderCheckedState(uGrid.Rows, CheckState.Unchecked);
            var lstRP = lstRolePermiss.Where(rp => rp.SubSystemCode == subSystemCode).ToList();
            foreach (var row in uGrid.Rows)
            {
                if (lstRP.Any(rp => rp.PermissionID == (row.ListObject as SysPermission).PermissionID))
                    row.Cells["Check"].Value = true;
            }
        }

        private void LoadData(bool configTree = true)
        {
            var parent = new FMain();
            var lstItems = new List<SubSystem>();
            lstItems.Add(new SubSystem { ID = "Parent_Cat", Name = "Danh mục", IsParent = true, ParentID = null });
            foreach (Infragistics.Win.UltraWinToolbars.RibbonGroup group in parent.uToolbarsManager.Ribbon.Tabs["tabCatalog"].Groups)
            {
                var parentCode = string.Format("Parent_{0}", group.Key);
                lstItems.Add(new SubSystem { ID = parentCode, Name = group.Caption, IsParent = true, ParentID = "Parent_Cat" });
                foreach (var item in group.Tools)
                {
                    if (item is Infragistics.Win.UltraWinToolbars.PopupMenuTool)
                    {
                        foreach (var iitem in ((Infragistics.Win.UltraWinToolbars.PopupMenuTool)item).Tools)
                        {
                            lstItems.Add(new SubSystem { ID = iitem.Key, Name = iitem.SharedProps.Caption, IsParent = false, ParentID = parentCode });
                        }
                    }
                    else
                        lstItems.Add(new SubSystem { ID = item.Key, Name = item.SharedProps.Caption, IsParent = false, ParentID = parentCode });
                }
            }
            lstItems.Add(new SubSystem { ID = "Parent_Business", Name = "Nghiệp vụ", IsParent = true, ParentID = null });
            foreach (var group in parent.uExBar.Groups)
            {
                if (group.Key.StartsWith("GroupCustom_") || group.Key == "Desktop") continue;
                var parentCode = string.Format("Parent_{0}", group.Index);
                lstItems.Add(new SubSystem { ID = parentCode, Name = group.Text, IsParent = true, ParentID = "Parent_Business" });
                foreach (var item in group.Items)
                {
                    lstItems.Add(new SubSystem { ID = item.Key, Name = item.Text, IsParent = false, ParentID = parentCode });
                }
            }
            DataSet ds = Utils.ToDataSet<SubSystem>(lstItems, ConstDatabase.SubSystem_KeyName);
            uTree.SetDataBinding(ds, ConstDatabase.SubSystem_KeyName);

            if (configTree) ConfigTree(uTree);
            uGrid.DataSource = new List<SysPermission>();
            ConfigGrid();
            //this.uTree.Nodes.Override.ColumnSet.Columns[0].LayoutInfo.LabelPosition = Infragistics.Win.UltraWinTree.NodeLayoutLabelPosition.None;
        }

        private void ConfigGrid()
        {
            uGrid.ConfigGrid(ConstDatabase.SysPermission_TableName, null, true, false);
            uGrid.DisplayLayout.Bands[0].Summaries.Clear();
            uGrid.DisplayLayout.Override.HeaderAppearance.TextHAlign = Infragistics.Win.HAlign.Center;
            UltraGridColumn ugc = uGrid.DisplayLayout.Bands[0].Columns["Check"];
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
        }
        void ConfigTree(Infragistics.Win.UltraWinTree.UltraTree ultraTree)
        {//hàm chung
            Utils.ConfigTree(ultraTree, TextMessage.ConstDatabase.SubSystem_KeyName);
        }
        private void uTree_ColumnSetGenerated(object sender, ColumnSetGeneratedEventArgs e)
        {
            Utils.uTree_ColumnSetGenerated(sender, e, ConstDatabase.SubSystem_KeyName);
            e.ColumnSet.Columns["Name"].LayoutInfo.LabelPosition = NodeLayoutLabelPosition.None;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void uGrid_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            if (e.Cell.Column.Key == "Check")
            {
                if ((bool?)e.Cell.Value == true)
                {
                    var per = e.Cell.Row.ListObject as SysPermission;
                    var temp = lstRolePermiss.FirstOrDefault(k => k.RoleID == roleId && k.PermissionID == per.PermissionID && k.SubSystemCode == ((UltraTreeNode)uTree.SelectedNodes.All[0]).Text);
                    if (temp == null)
                        lstRolePermiss.Add(new SysRolepermission
                        {
                            RoleID = roleId,
                            PermissionID = per.PermissionID,
                            SubSystemCode = ((UltraTreeNode)uTree.SelectedNodes.All[0]).Text
                        });
                }
                else
                {
                    var per = e.Cell.Row.ListObject as SysPermission;
                    var temp = lstRolePermiss.FirstOrDefault(k => k.RoleID == roleId && k.PermissionID == per.PermissionID && k.SubSystemCode == ((UltraTreeNode)uTree.SelectedNodes.All[0]).Text);
                    if (temp != null)
                        lstRolePermiss.Remove(temp);
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            var lst = Utils.ISysRolepermissionService.Query.Where(rp => rp.RoleID == roleId).ToList();
            Utils.ISysRolepermissionService.BeginTran();
            try
            {
                foreach (var item in lst)
                {
                    Utils.ISysRolepermissionService.Delete(item);
                }
                foreach (var item in lstRolePermiss)
                {
                    item.ID = Guid.Empty;
                    Utils.ISysRolepermissionService.CreateNew(item);
                }
                Utils.ISysRolepermissionService.CommitTran();
                DialogResult = System.Windows.Forms.DialogResult.OK;
                Close();
            }
            catch (Exception)
            {
                Utils.ISysRolepermissionService.RolbackTran();
                MSG.Warning("Thiết lập quyền bị lỗi. Vui lòng thực hiện lại.");
            }
        }

        private void btnCloseAllNode_Click(object sender, EventArgs e)
        {
            this.uTree.Override.ShowExpansionIndicator = Infragistics.Win.UltraWinTree.ShowExpansionIndicator.Never;
            this.uTree.Override.TipStyleNode = TipStyleNode.Hide;
            var nodes = this.uTree.NodeLevelOverrides;
            foreach (Infragistics.Win.UltraWinTree.Override item in nodes)
            {
                item.TipStyleNode = TipStyleNode.Hide;
                item.ShowExpansionIndicator = Infragistics.Win.UltraWinTree.ShowExpansionIndicator.CheckOnExpand;
            }
        }

        private void uGrid_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            if (e.Cell.Column.Key == "Check")
            {
                e.Cell.Row.UpdateData();
                if ((bool?)e.Cell.Value == true)
                {
                    var row = uGrid.Rows.FirstOrDefault(r => r.Cells["PermissionCode"].Text == "SD");
                    if (row != null)
                    {
                        if ((bool?)row.Cells["Check"].Value != true)
                            row.Cells["Check"].Value = true;
                    }
                }
                else
                {
                    var per = e.Cell.Row.ListObject as SysPermission;
                    if (per.PermissionCode.Equals("SD"))
                        foreach (var row in uGrid.Rows)
                        {
                            if (row == e.Cell.Row) continue;
                            row.Cells["Check"].Value = false;
                        }
                }
            }
        }

        private void FSysPermissions_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
