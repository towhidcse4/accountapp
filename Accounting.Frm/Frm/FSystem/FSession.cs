﻿using Accounting.TextMessage;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core;

namespace Accounting.Frm.FReport
{
    public partial class FSession : Form
    {
        private List<Company> companies;
        public Company NextSession;
        public Company CurrentSession;
        public FSession()
        {
            InitializeComponent();
            LoadData();
        }

        private void LoadData()
        {
            string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            string CustomerFilePath = string.Format("{0}\\companies.xml", path).Replace("file:\\", "");
            companies = XmlToObject.DeserializeFromFile<Company>(CustomerFilePath) ?? new List<Company>();
            CurrentSession = companies.FirstOrDefault(o => o.ServerName == Authenticate.Server && o.Database == Authenticate.Database);

            cbbCompany.DataSource = companies;
            cbbCompany.DisplayMember = "CompanyName";
            cbbCompany.ValueMember = "ID";
            //hautv edit
            if (!Utils.isDuLieuMau)
                cbbCompany.Value = CurrentSession.ID;
            Utils.ConfigGrid(cbbCompany, ConstDatabase.Company_TableName);
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            NextSession = companies.FirstOrDefault(o => o.ID == (Guid)cbbCompany.Value);
            
            Close();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
