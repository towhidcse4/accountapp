﻿using Accounting.Core.Domain;
using Accounting.TextMessage;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting
{
    public partial class FSysUserProfile : DialogForm
    {
        public static bool isClose = true;
        private bool isAdd = true;
        private SysUser sysUser;
        public FSysUserProfile(SysUser user)
        {
            InitializeComponent();
            isAdd = false;
            sysUser = Utils.ISysUserService.Getbykey(user.userid);
            Utils.ISysUserService.UnbindSession(sysUser);
            sysUser = Utils.ISysUserService.Getbykey(user.userid);
            ConfigControl();
            WaitingFrm.StopWaiting();
        }

        private void ConfigControl()
        {
            List<country> countries = Utils.DicCounties.Values.ToList();
            foreach (var item in countries)
            {
                cbbCountry.Items.Add(item.countryCode, item.countryName);
            }
            if (!isAdd)
            {
                cbbCountry.Value = sysUser.Country;
                txtCity.Value = sysUser.City;
                txtDescription.Value = sysUser.Description;
                txtFullName.Value = sysUser.FullName;
                txtIDCard.Value = sysUser.IDCard;
                txtEmail.Value = sysUser.Email;
                txtAdress.Value = sysUser.Address;
                txtFax.Value = sysUser.Fax;
                txtHomePhone.Value = sysUser.HomePhone;
                txtJob.Value = sysUser.Job;
                txtMobilePhone.Value = sysUser.MobilePhone;
                txtWebsite.Value = sysUser.Website;
                txtWorkPhone.Value = sysUser.WorkPhone;
                txtProvince.Value = sysUser.Province;
                if (sysUser.BirthDay.HasValue)
                    dteBirthDay.Value = sysUser.BirthDay.Value;
                if (sysUser.Photo != null)
                    picPhoto.Image = sysUser.Photo.ByteArrayToImage();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtFullName.Text.Trim()))
            {
                MSG.Warning("Họ và tên không được để trống!");
                txtFullName.Focus();
                return;
            }
            if (MSG.MessageBoxStand("Bạn có thực sự muốn lưu thay đổi này không?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if (!isAdd)
                {
                    sysUser = Utils.ISysUserService.Getbykey(sysUser.userid);
                    Utils.ISysUserService.UnbindSession(sysUser);
                    sysUser = Utils.ISysUserService.Getbykey(sysUser.userid);
                }
                sysUser.Country = cbbCountry.Value.ToString();
                sysUser.City = txtCity.Text.Trim();
                sysUser.Description = txtDescription.Text.Trim();
                sysUser.FullName = txtFullName.Text.Trim();
                sysUser.IDCard = txtIDCard.Text.Trim();
                sysUser.Email = txtEmail.Text.Trim();
                sysUser.Address = txtAdress.Text.Trim();
                sysUser.Fax = txtFax.Value.ToString();
                sysUser.WorkPhone = txtWorkPhone.Value.ToString();
                sysUser.HomePhone = txtHomePhone.Value.ToString();
                sysUser.MobilePhone = txtMobilePhone.Value.ToString();
                sysUser.Job = txtJob.Text.Trim();
                sysUser.Website = txtWebsite.Text.Trim();
                sysUser.Province = txtProvince.Text.Trim();
                if (dteBirthDay.Value != null)
                    sysUser.BirthDay = dteBirthDay.DateTime;
                if (picPhoto.Image != null)
                    sysUser.Photo = (picPhoto.Image as Bitmap).ImageToByteArray();
                Utils.ISysUserService.BeginTran();
                try
                {
                    if (isAdd) Utils.ISysUserService.CreateNew(sysUser);
                    else Utils.ISysUserService.Update(sysUser);
                    Utils.ISysUserService.CommitTran();
                    Logs.Save(0, resSystem.Logs_Title_00, Logs.Action.Edit, sysUser.username, sysUser.userid, "Thay đổi thông tin cá nhân");
                }
                catch (Exception ex)
                {
                    Utils.ISysUserService.RolbackTran();
                    MSG.Warning(string.Format("Lỗi {0} tài khoản người dùng! \r\nVui lòng thực hiện lại.", isAdd ? "khởi tạo" : "cập nhật"));
                    return;
                }
                isClose = false;
                Close();
            }
        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Filter = @"All Images|*.BMP;*.DIB;*.RLE;*.JPG;*.JPEG;*.JPE;*.JFIF;*.GIF;*.TIF;*.TIFF;*.PNG|BMP Files: (*.BMP;*.DIB;*.RLE)|*.BMP;*.DIB;*.RLE|JPEG Files: (*.JPG;*.JPEG;*.JPE;*.JFIF)|*.JPG;*.JPEG;*.JPE;*.JFIF|GIF Files: (*.GIF)|*.GIF|TIFF Files: (*.TIF;*.TIFF)|*.TIF;*.TIFF|PNG Files: (*.PNG)|*.PNG";
            fileDialog.FilterIndex = 1;
            fileDialog.ShowDialog(this);
            if (!string.IsNullOrEmpty(fileDialog.FileName))
            {
                picPhoto.Image = new Bitmap(fileDialog.FileName);
            }
        }

    }
}
