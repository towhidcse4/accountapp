﻿namespace Accounting
{
    partial class FSysUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab(true);
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FSysUser));
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbCountry = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.dteBirthDay = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.txtDescription = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtIDCard = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtCity = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtProvince = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtJob = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtFullName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.lbl1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbSystemRole = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel22 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel21 = new Infragistics.Win.Misc.UltraLabel();
            this.txtRepassword = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtPassword = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.txtUsername = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl3 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.txtMobilePhone = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtHomePhone = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtFax = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtWorkPhone = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.btnUpload = new Infragistics.Win.Misc.UltraButton();
            this.picPhoto = new Infragistics.Win.UltraWinEditors.UltraPictureBox();
            this.ultraLabel19 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel17 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAdress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtWebsite = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtEmail = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel18 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel20 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel23 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel24 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel25 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.chkIsActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.tabControl = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCountry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteBirthDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIDCard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProvince)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJob)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFullName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbSystemRole)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRepassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUsername)).BeginInit();
            this.ultraTabPageControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMobilePhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHomePhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWorkPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWebsite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).BeginInit();
            this.tabControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.ultraGroupBox2);
            this.ultraTabPageControl1.Controls.Add(this.ultraGroupBox1);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(471, 462);
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.cbbCountry);
            this.ultraGroupBox2.Controls.Add(this.dteBirthDay);
            this.ultraGroupBox2.Controls.Add(this.txtDescription);
            this.ultraGroupBox2.Controls.Add(this.txtIDCard);
            this.ultraGroupBox2.Controls.Add(this.txtCity);
            this.ultraGroupBox2.Controls.Add(this.txtProvince);
            this.ultraGroupBox2.Controls.Add(this.txtJob);
            this.ultraGroupBox2.Controls.Add(this.txtFullName);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel10);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel3);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel13);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel15);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel11);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel12);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel6);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox2.Controls.Add(this.lbl1);
            this.ultraGroupBox2.Location = new System.Drawing.Point(4, 149);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(464, 297);
            this.ultraGroupBox2.TabIndex = 56;
            this.ultraGroupBox2.Text = "Thông tin cá nhân";
            // 
            // cbbCountry
            // 
            this.cbbCountry.AutoSize = false;
            this.cbbCountry.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbbCountry.Location = new System.Drawing.Point(159, 105);
            this.cbbCountry.Name = "cbbCountry";
            this.cbbCountry.Size = new System.Drawing.Size(233, 22);
            this.cbbCountry.TabIndex = 72;
            // 
            // dteBirthDay
            // 
            appearance1.TextHAlignAsString = "Right";
            this.dteBirthDay.Appearance = appearance1;
            this.dteBirthDay.AutoSize = false;
            this.dteBirthDay.Location = new System.Drawing.Point(159, 78);
            this.dteBirthDay.MaskInput = "dd/mm/yyyy";
            this.dteBirthDay.Name = "dteBirthDay";
            this.dteBirthDay.Size = new System.Drawing.Size(106, 22);
            this.dteBirthDay.TabIndex = 71;
            // 
            // txtDescription
            // 
            this.txtDescription.AutoSize = false;
            this.txtDescription.Location = new System.Drawing.Point(159, 213);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Scrollbars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDescription.Size = new System.Drawing.Size(233, 73);
            this.txtDescription.TabIndex = 70;
            // 
            // txtIDCard
            // 
            this.txtIDCard.AutoSize = false;
            this.txtIDCard.Location = new System.Drawing.Point(159, 186);
            this.txtIDCard.Name = "txtIDCard";
            this.txtIDCard.Size = new System.Drawing.Size(233, 22);
            this.txtIDCard.TabIndex = 69;
            // 
            // txtCity
            // 
            this.txtCity.AutoSize = false;
            this.txtCity.Location = new System.Drawing.Point(159, 159);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(233, 22);
            this.txtCity.TabIndex = 68;
            // 
            // txtProvince
            // 
            this.txtProvince.AutoSize = false;
            this.txtProvince.Location = new System.Drawing.Point(159, 132);
            this.txtProvince.Name = "txtProvince";
            this.txtProvince.Size = new System.Drawing.Size(233, 22);
            this.txtProvince.TabIndex = 67;
            // 
            // txtJob
            // 
            this.txtJob.AutoSize = false;
            this.txtJob.Location = new System.Drawing.Point(159, 51);
            this.txtJob.Name = "txtJob";
            this.txtJob.Size = new System.Drawing.Size(233, 22);
            this.txtJob.TabIndex = 66;
            // 
            // txtFullName
            // 
            this.txtFullName.AutoSize = false;
            this.txtFullName.Location = new System.Drawing.Point(159, 24);
            this.txtFullName.Name = "txtFullName";
            this.txtFullName.Size = new System.Drawing.Size(233, 22);
            this.txtFullName.TabIndex = 65;
            // 
            // ultraLabel10
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.ForeColor = System.Drawing.Color.Red;
            appearance2.TextHAlignAsString = "Center";
            appearance2.TextVAlignAsString = "Middle";
            this.ultraLabel10.Appearance = appearance2;
            this.ultraLabel10.Location = new System.Drawing.Point(131, 24);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(16, 22);
            this.ultraLabel10.TabIndex = 63;
            this.ultraLabel10.Text = "(*)";
            this.ultraLabel10.WrapText = false;
            // 
            // ultraLabel3
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextHAlignAsString = "Right";
            appearance3.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance3;
            this.ultraLabel3.Location = new System.Drawing.Point(47, 159);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel3.TabIndex = 62;
            this.ultraLabel3.Text = "Quận/huyện";
            // 
            // ultraLabel13
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextHAlignAsString = "Right";
            appearance4.TextVAlignAsString = "Middle";
            this.ultraLabel13.Appearance = appearance4;
            this.ultraLabel13.Location = new System.Drawing.Point(47, 105);
            this.ultraLabel13.Name = "ultraLabel13";
            this.ultraLabel13.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel13.TabIndex = 61;
            this.ultraLabel13.Text = "Quốc gia";
            // 
            // ultraLabel15
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.TextHAlignAsString = "Right";
            appearance5.TextVAlignAsString = "Middle";
            this.ultraLabel15.Appearance = appearance5;
            this.ultraLabel15.Location = new System.Drawing.Point(47, 132);
            this.ultraLabel15.Name = "ultraLabel15";
            this.ultraLabel15.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel15.TabIndex = 60;
            this.ultraLabel15.Text = "Tỉnh/Thành phố";
            // 
            // ultraLabel11
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextHAlignAsString = "Right";
            appearance6.TextVAlignAsString = "Middle";
            this.ultraLabel11.Appearance = appearance6;
            this.ultraLabel11.Location = new System.Drawing.Point(47, 78);
            this.ultraLabel11.Name = "ultraLabel11";
            this.ultraLabel11.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel11.TabIndex = 59;
            this.ultraLabel11.Text = "Ngày sinh";
            // 
            // ultraLabel12
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.TextHAlignAsString = "Right";
            appearance7.TextVAlignAsString = "Middle";
            this.ultraLabel12.Appearance = appearance7;
            this.ultraLabel12.Location = new System.Drawing.Point(47, 186);
            this.ultraLabel12.Name = "ultraLabel12";
            this.ultraLabel12.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel12.TabIndex = 58;
            this.ultraLabel12.Text = "CMTND/Passport";
            // 
            // ultraLabel6
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.TextHAlignAsString = "Right";
            appearance8.TextVAlignAsString = "Middle";
            this.ultraLabel6.Appearance = appearance8;
            this.ultraLabel6.Location = new System.Drawing.Point(47, 213);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel6.TabIndex = 57;
            this.ultraLabel6.Text = "Mô tả";
            // 
            // ultraLabel2
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.TextHAlignAsString = "Right";
            appearance9.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance9;
            this.ultraLabel2.Location = new System.Drawing.Point(47, 51);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel2.TabIndex = 56;
            this.ultraLabel2.Text = "Vị trí công tác";
            // 
            // lbl1
            // 
            appearance10.BackColor = System.Drawing.Color.Transparent;
            appearance10.TextHAlignAsString = "Right";
            appearance10.TextVAlignAsString = "Middle";
            this.lbl1.Appearance = appearance10;
            this.lbl1.Location = new System.Drawing.Point(33, 24);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(100, 22);
            this.lbl1.TabIndex = 55;
            this.lbl1.Text = "Họ và tên";
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraGroupBox1.Controls.Add(this.cbbSystemRole);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel22);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel21);
            this.ultraGroupBox1.Controls.Add(this.txtRepassword);
            this.ultraGroupBox1.Controls.Add(this.txtPassword);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel9);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel8);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel7);
            this.ultraGroupBox1.Controls.Add(this.txtUsername);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel5);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel4);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox1.Location = new System.Drawing.Point(3, 7);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(465, 136);
            this.ultraGroupBox1.TabIndex = 55;
            this.ultraGroupBox1.Text = "Thông tin đăng nhập";
            // 
            // cbbSystemRole
            // 
            appearance11.TextVAlignAsString = "Middle";
            this.cbbSystemRole.Appearance = appearance11;
            this.cbbSystemRole.AutoSize = false;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbSystemRole.DisplayLayout.Appearance = appearance12;
            this.cbbSystemRole.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbSystemRole.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance13.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance13.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbSystemRole.DisplayLayout.GroupByBox.Appearance = appearance13;
            appearance14.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbSystemRole.DisplayLayout.GroupByBox.BandLabelAppearance = appearance14;
            this.cbbSystemRole.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance15.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance15.BackColor2 = System.Drawing.SystemColors.Control;
            appearance15.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbSystemRole.DisplayLayout.GroupByBox.PromptAppearance = appearance15;
            this.cbbSystemRole.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbSystemRole.DisplayLayout.MaxRowScrollRegions = 1;
            appearance16.BackColor = System.Drawing.SystemColors.Window;
            appearance16.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbSystemRole.DisplayLayout.Override.ActiveCellAppearance = appearance16;
            appearance17.BackColor = System.Drawing.SystemColors.Highlight;
            appearance17.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbSystemRole.DisplayLayout.Override.ActiveRowAppearance = appearance17;
            this.cbbSystemRole.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbSystemRole.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance18.BackColor = System.Drawing.SystemColors.Window;
            this.cbbSystemRole.DisplayLayout.Override.CardAreaAppearance = appearance18;
            appearance19.BorderColor = System.Drawing.Color.Silver;
            appearance19.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbSystemRole.DisplayLayout.Override.CellAppearance = appearance19;
            this.cbbSystemRole.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbSystemRole.DisplayLayout.Override.CellPadding = 0;
            appearance20.BackColor = System.Drawing.SystemColors.Control;
            appearance20.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance20.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance20.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance20.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbSystemRole.DisplayLayout.Override.GroupByRowAppearance = appearance20;
            appearance21.TextHAlignAsString = "Left";
            this.cbbSystemRole.DisplayLayout.Override.HeaderAppearance = appearance21;
            this.cbbSystemRole.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbSystemRole.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance22.BackColor = System.Drawing.SystemColors.Window;
            appearance22.BorderColor = System.Drawing.Color.Silver;
            this.cbbSystemRole.DisplayLayout.Override.RowAppearance = appearance22;
            this.cbbSystemRole.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance23.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbSystemRole.DisplayLayout.Override.TemplateAddRowAppearance = appearance23;
            this.cbbSystemRole.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbSystemRole.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbSystemRole.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbSystemRole.Location = new System.Drawing.Point(160, 105);
            this.cbbSystemRole.Name = "cbbSystemRole";
            this.cbbSystemRole.Size = new System.Drawing.Size(233, 22);
            this.cbbSystemRole.TabIndex = 58;
            // 
            // ultraLabel22
            // 
            appearance24.BackColor = System.Drawing.Color.Transparent;
            appearance24.ForeColor = System.Drawing.Color.Red;
            appearance24.TextHAlignAsString = "Center";
            appearance24.TextVAlignAsString = "Middle";
            this.ultraLabel22.Appearance = appearance24;
            this.ultraLabel22.Location = new System.Drawing.Point(132, 106);
            this.ultraLabel22.Name = "ultraLabel22";
            this.ultraLabel22.Size = new System.Drawing.Size(16, 22);
            this.ultraLabel22.TabIndex = 57;
            this.ultraLabel22.Text = "(*)";
            this.ultraLabel22.WrapText = false;
            // 
            // ultraLabel21
            // 
            appearance25.BackColor = System.Drawing.Color.Transparent;
            appearance25.TextHAlignAsString = "Right";
            appearance25.TextVAlignAsString = "Middle";
            this.ultraLabel21.Appearance = appearance25;
            this.ultraLabel21.Location = new System.Drawing.Point(39, 106);
            this.ultraLabel21.Name = "ultraLabel21";
            this.ultraLabel21.Size = new System.Drawing.Size(95, 22);
            this.ultraLabel21.TabIndex = 56;
            this.ultraLabel21.Text = "Vai trò";
            // 
            // txtRepassword
            // 
            this.txtRepassword.AutoSize = false;
            this.txtRepassword.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRepassword.Location = new System.Drawing.Point(160, 77);
            this.txtRepassword.Name = "txtRepassword";
            this.txtRepassword.PasswordChar = '.';
            this.txtRepassword.Size = new System.Drawing.Size(233, 22);
            this.txtRepassword.TabIndex = 55;
            // 
            // txtPassword
            // 
            this.txtPassword.AutoSize = false;
            this.txtPassword.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPassword.Location = new System.Drawing.Point(160, 49);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '.';
            this.txtPassword.Size = new System.Drawing.Size(233, 22);
            this.txtPassword.TabIndex = 54;
            // 
            // ultraLabel9
            // 
            appearance26.BackColor = System.Drawing.Color.Transparent;
            appearance26.ForeColor = System.Drawing.Color.Red;
            appearance26.TextHAlignAsString = "Center";
            appearance26.TextVAlignAsString = "Middle";
            this.ultraLabel9.Appearance = appearance26;
            this.ultraLabel9.Location = new System.Drawing.Point(132, 78);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(16, 22);
            this.ultraLabel9.TabIndex = 53;
            this.ultraLabel9.Text = "(*)";
            this.ultraLabel9.WrapText = false;
            // 
            // ultraLabel8
            // 
            appearance27.BackColor = System.Drawing.Color.Transparent;
            appearance27.ForeColor = System.Drawing.Color.Red;
            appearance27.TextHAlignAsString = "Center";
            appearance27.TextVAlignAsString = "Middle";
            this.ultraLabel8.Appearance = appearance27;
            this.ultraLabel8.Location = new System.Drawing.Point(132, 50);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(16, 22);
            this.ultraLabel8.TabIndex = 52;
            this.ultraLabel8.Text = "(*)";
            this.ultraLabel8.WrapText = false;
            // 
            // ultraLabel7
            // 
            appearance28.BackColor = System.Drawing.Color.Transparent;
            appearance28.ForeColor = System.Drawing.Color.Red;
            appearance28.TextHAlignAsString = "Center";
            appearance28.TextVAlignAsString = "Middle";
            this.ultraLabel7.Appearance = appearance28;
            this.ultraLabel7.Location = new System.Drawing.Point(132, 22);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(16, 22);
            this.ultraLabel7.TabIndex = 51;
            this.ultraLabel7.Text = "(*)";
            this.ultraLabel7.WrapText = false;
            // 
            // txtUsername
            // 
            this.txtUsername.AutoSize = false;
            this.txtUsername.Location = new System.Drawing.Point(160, 22);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(233, 22);
            this.txtUsername.TabIndex = 50;
            // 
            // ultraLabel5
            // 
            appearance29.BackColor = System.Drawing.Color.Transparent;
            appearance29.TextHAlignAsString = "Right";
            appearance29.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance29;
            this.ultraLabel5.Location = new System.Drawing.Point(39, 78);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(95, 22);
            this.ultraLabel5.TabIndex = 49;
            this.ultraLabel5.Text = "Xác nhận lại MK";
            // 
            // ultraLabel4
            // 
            appearance30.BackColor = System.Drawing.Color.Transparent;
            appearance30.TextHAlignAsString = "Right";
            appearance30.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance30;
            this.ultraLabel4.Location = new System.Drawing.Point(39, 50);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(95, 22);
            this.ultraLabel4.TabIndex = 48;
            this.ultraLabel4.Text = "Mật khẩu";
            // 
            // ultraLabel1
            // 
            appearance31.BackColor = System.Drawing.Color.Transparent;
            appearance31.TextHAlignAsString = "Right";
            appearance31.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance31;
            this.ultraLabel1.Location = new System.Drawing.Point(39, 22);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(95, 22);
            this.ultraLabel1.TabIndex = 47;
            this.ultraLabel1.Text = "Tên đăng nhập";
            // 
            // ultraTabPageControl3
            // 
            this.ultraTabPageControl3.Controls.Add(this.txtMobilePhone);
            this.ultraTabPageControl3.Controls.Add(this.txtHomePhone);
            this.ultraTabPageControl3.Controls.Add(this.txtFax);
            this.ultraTabPageControl3.Controls.Add(this.txtWorkPhone);
            this.ultraTabPageControl3.Controls.Add(this.btnUpload);
            this.ultraTabPageControl3.Controls.Add(this.picPhoto);
            this.ultraTabPageControl3.Controls.Add(this.ultraLabel19);
            this.ultraTabPageControl3.Controls.Add(this.ultraLabel17);
            this.ultraTabPageControl3.Controls.Add(this.ultraLabel16);
            this.ultraTabPageControl3.Controls.Add(this.txtAdress);
            this.ultraTabPageControl3.Controls.Add(this.txtWebsite);
            this.ultraTabPageControl3.Controls.Add(this.txtEmail);
            this.ultraTabPageControl3.Controls.Add(this.ultraLabel18);
            this.ultraTabPageControl3.Controls.Add(this.ultraLabel20);
            this.ultraTabPageControl3.Controls.Add(this.ultraLabel23);
            this.ultraTabPageControl3.Controls.Add(this.ultraLabel24);
            this.ultraTabPageControl3.Controls.Add(this.ultraLabel25);
            this.ultraTabPageControl3.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl3.Name = "ultraTabPageControl3";
            this.ultraTabPageControl3.Size = new System.Drawing.Size(471, 462);
            // 
            // txtMobilePhone
            // 
            this.txtMobilePhone.AutoSize = false;
            this.txtMobilePhone.Location = new System.Drawing.Point(152, 100);
            this.txtMobilePhone.Name = "txtMobilePhone";
            this.txtMobilePhone.Size = new System.Drawing.Size(233, 22);
            this.txtMobilePhone.TabIndex = 92;
            this.txtMobilePhone.ValueChanged += new System.EventHandler(this.txtMobilePhone_ValueChanged);
            this.txtMobilePhone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMobilePhone_KeyPress);
            // 
            // txtHomePhone
            // 
            this.txtHomePhone.AutoSize = false;
            this.txtHomePhone.Location = new System.Drawing.Point(152, 128);
            this.txtHomePhone.Name = "txtHomePhone";
            this.txtHomePhone.Size = new System.Drawing.Size(233, 22);
            this.txtHomePhone.TabIndex = 91;
            this.txtHomePhone.ValueChanged += new System.EventHandler(this.txtHomePhone_ValueChanged);
            this.txtHomePhone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtHomePhone_KeyPress);
            // 
            // txtFax
            // 
            this.txtFax.AutoSize = false;
            this.txtFax.Location = new System.Drawing.Point(152, 152);
            this.txtFax.Name = "txtFax";
            this.txtFax.Size = new System.Drawing.Size(233, 22);
            this.txtFax.TabIndex = 90;
            this.txtFax.ValueChanged += new System.EventHandler(this.txtFax_ValueChanged);
            this.txtFax.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFax_KeyPress);
            // 
            // txtWorkPhone
            // 
            this.txtWorkPhone.AutoSize = false;
            this.txtWorkPhone.Location = new System.Drawing.Point(152, 71);
            this.txtWorkPhone.Name = "txtWorkPhone";
            this.txtWorkPhone.Size = new System.Drawing.Size(233, 22);
            this.txtWorkPhone.TabIndex = 89;
            this.txtWorkPhone.ValueChanged += new System.EventHandler(this.txtWorkPhone_ValueChanged);
            this.txtWorkPhone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtWorkPhone_KeyPress);
            // 
            // btnUpload
            // 
            appearance32.Image = global::Accounting.Properties.Resources.folder;
            this.btnUpload.Appearance = appearance32;
            this.btnUpload.Location = new System.Drawing.Point(152, 381);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Size = new System.Drawing.Size(27, 26);
            this.btnUpload.TabIndex = 80;
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // picPhoto
            // 
            appearance33.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance33.ImageVAlign = Infragistics.Win.VAlign.Bottom;
            this.picPhoto.Appearance = appearance33;
            this.picPhoto.BackColor = System.Drawing.Color.White;
            this.picPhoto.BorderShadowColor = System.Drawing.Color.Empty;
            this.picPhoto.ImageTransparentColor = System.Drawing.Color.White;
            this.picPhoto.Location = new System.Drawing.Point(152, 258);
            this.picPhoto.Name = "picPhoto";
            this.picPhoto.Size = new System.Drawing.Size(100, 117);
            this.picPhoto.TabIndex = 78;
            // 
            // ultraLabel19
            // 
            appearance34.BackColor = System.Drawing.Color.Transparent;
            appearance34.TextHAlignAsString = "Right";
            appearance34.TextVAlignAsString = "Middle";
            this.ultraLabel19.Appearance = appearance34;
            this.ultraLabel19.Location = new System.Drawing.Point(40, 258);
            this.ultraLabel19.Name = "ultraLabel19";
            this.ultraLabel19.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel19.TabIndex = 77;
            this.ultraLabel19.Text = "Hình đại diện";
            // 
            // ultraLabel17
            // 
            appearance35.BackColor = System.Drawing.Color.Transparent;
            appearance35.TextHAlignAsString = "Right";
            appearance35.TextVAlignAsString = "Middle";
            this.ultraLabel17.Appearance = appearance35;
            this.ultraLabel17.Location = new System.Drawing.Point(40, 99);
            this.ultraLabel17.Name = "ultraLabel17";
            this.ultraLabel17.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel17.TabIndex = 72;
            this.ultraLabel17.Text = "ĐT di động";
            // 
            // ultraLabel16
            // 
            appearance36.BackColor = System.Drawing.Color.Transparent;
            appearance36.TextHAlignAsString = "Right";
            appearance36.TextVAlignAsString = "Middle";
            this.ultraLabel16.Appearance = appearance36;
            this.ultraLabel16.Location = new System.Drawing.Point(40, 72);
            this.ultraLabel16.Name = "ultraLabel16";
            this.ultraLabel16.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel16.TabIndex = 71;
            this.ultraLabel16.Text = "ĐT cơ quan";
            // 
            // txtAdress
            // 
            this.txtAdress.AutoSize = false;
            this.txtAdress.Location = new System.Drawing.Point(152, 181);
            this.txtAdress.Multiline = true;
            this.txtAdress.Name = "txtAdress";
            this.txtAdress.Scrollbars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtAdress.Size = new System.Drawing.Size(233, 73);
            this.txtAdress.TabIndex = 70;
            // 
            // txtWebsite
            // 
            this.txtWebsite.AutoSize = false;
            this.txtWebsite.Location = new System.Drawing.Point(152, 45);
            this.txtWebsite.Name = "txtWebsite";
            this.txtWebsite.Size = new System.Drawing.Size(233, 22);
            this.txtWebsite.TabIndex = 66;
            // 
            // txtEmail
            // 
            this.txtEmail.AutoSize = false;
            this.txtEmail.Location = new System.Drawing.Point(152, 18);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(233, 22);
            this.txtEmail.TabIndex = 65;
            // 
            // ultraLabel18
            // 
            appearance37.BackColor = System.Drawing.Color.Transparent;
            appearance37.TextHAlignAsString = "Right";
            appearance37.TextVAlignAsString = "Middle";
            this.ultraLabel18.Appearance = appearance37;
            this.ultraLabel18.Location = new System.Drawing.Point(40, 153);
            this.ultraLabel18.Name = "ultraLabel18";
            this.ultraLabel18.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel18.TabIndex = 62;
            this.ultraLabel18.Text = "Fax";
            // 
            // ultraLabel20
            // 
            appearance38.BackColor = System.Drawing.Color.Transparent;
            appearance38.TextHAlignAsString = "Right";
            appearance38.TextVAlignAsString = "Middle";
            this.ultraLabel20.Appearance = appearance38;
            this.ultraLabel20.Location = new System.Drawing.Point(40, 126);
            this.ultraLabel20.Name = "ultraLabel20";
            this.ultraLabel20.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel20.TabIndex = 60;
            this.ultraLabel20.Text = "ĐT nhà riêng";
            // 
            // ultraLabel23
            // 
            appearance39.BackColor = System.Drawing.Color.Transparent;
            appearance39.TextHAlignAsString = "Right";
            appearance39.TextVAlignAsString = "Middle";
            this.ultraLabel23.Appearance = appearance39;
            this.ultraLabel23.Location = new System.Drawing.Point(40, 180);
            this.ultraLabel23.Name = "ultraLabel23";
            this.ultraLabel23.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel23.TabIndex = 57;
            this.ultraLabel23.Text = "Địa chỉ";
            // 
            // ultraLabel24
            // 
            appearance40.BackColor = System.Drawing.Color.Transparent;
            appearance40.TextHAlignAsString = "Right";
            appearance40.TextVAlignAsString = "Middle";
            this.ultraLabel24.Appearance = appearance40;
            this.ultraLabel24.Location = new System.Drawing.Point(40, 45);
            this.ultraLabel24.Name = "ultraLabel24";
            this.ultraLabel24.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel24.TabIndex = 56;
            this.ultraLabel24.Text = "Website";
            // 
            // ultraLabel25
            // 
            appearance41.BackColor = System.Drawing.Color.Transparent;
            appearance41.TextHAlignAsString = "Right";
            appearance41.TextVAlignAsString = "Middle";
            this.ultraLabel25.Appearance = appearance41;
            this.ultraLabel25.Location = new System.Drawing.Point(40, 18);
            this.ultraLabel25.Name = "ultraLabel25";
            this.ultraLabel25.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel25.TabIndex = 55;
            this.ultraLabel25.Text = "Email";
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(471, 335);
            // 
            // chkIsActive
            // 
            this.chkIsActive.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkIsActive.Checked = true;
            this.chkIsActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIsActive.Location = new System.Drawing.Point(12, 508);
            this.chkIsActive.Name = "chkIsActive";
            this.chkIsActive.Size = new System.Drawing.Size(94, 20);
            this.chkIsActive.TabIndex = 3;
            this.chkIsActive.Text = "Hoạt động";
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance42.Image = global::Accounting.Properties.Resources.apply_32;
            this.btnSave.Appearance = appearance42;
            this.btnSave.Location = new System.Drawing.Point(290, 509);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(107, 30);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Lưu thay đổi";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance43.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance43;
            this.btnClose.Location = new System.Drawing.Point(403, 509);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(84, 30);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Hủy bỏ";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.ultraTabSharedControlsPage1);
            this.tabControl.Controls.Add(this.ultraTabPageControl1);
            this.tabControl.Controls.Add(this.ultraTabPageControl3);
            this.tabControl.Location = new System.Drawing.Point(12, 12);
            this.tabControl.Name = "tabControl";
            this.tabControl.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.tabControl.Size = new System.Drawing.Size(475, 488);
            this.tabControl.TabIndex = 0;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "1. Cá nhân";
            ultraTab2.TabPage = this.ultraTabPageControl3;
            ultraTab2.Text = "2. Liên hệ";
            this.tabControl.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            this.tabControl.TextOrientation = Infragistics.Win.UltraWinTabs.TextOrientation.Horizontal;
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(471, 462);
            // 
            // FSysUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(499, 548);
            this.Controls.Add(this.chkIsActive);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.tabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FSysUser";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FSysUser";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FSysUser_FormClosed);
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbCountry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteBirthDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIDCard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProvince)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJob)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFullName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbSystemRole)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRepassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUsername)).EndInit();
            this.ultraTabPageControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtMobilePhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHomePhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWorkPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWebsite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinTabControl.UltraTabControl tabControl;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl3;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsActive;
        private Infragistics.Win.UltraWinEditors.UltraPictureBox picPhoto;
        private Infragistics.Win.Misc.UltraLabel ultraLabel19;
        private Infragistics.Win.Misc.UltraLabel ultraLabel17;
        private Infragistics.Win.Misc.UltraLabel ultraLabel16;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAdress;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtWebsite;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtEmail;
        private Infragistics.Win.Misc.UltraLabel ultraLabel18;
        private Infragistics.Win.Misc.UltraLabel ultraLabel20;
        private Infragistics.Win.Misc.UltraLabel ultraLabel23;
        private Infragistics.Win.Misc.UltraLabel ultraLabel24;
        private Infragistics.Win.Misc.UltraLabel ultraLabel25;
        private Infragistics.Win.Misc.UltraButton btnUpload;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbCountry;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteBirthDay;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDescription;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtIDCard;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCity;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtProvince;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtJob;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtFullName;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel13;
        private Infragistics.Win.Misc.UltraLabel ultraLabel15;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
        private Infragistics.Win.Misc.UltraLabel ultraLabel12;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel lbl1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtRepassword;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtPassword;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtUsername;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel22;
        private Infragistics.Win.Misc.UltraLabel ultraLabel21;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbSystemRole;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtWorkPhone;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMobilePhone;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtHomePhone;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtFax;
    }
}