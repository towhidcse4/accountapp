﻿namespace Accounting
{
    partial class FSysLog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            this.cms4Grid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tmsImport = new System.Windows.Forms.ToolStripMenuItem();
            this.tmsExport = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.tmsDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tmsReLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.tmsGetData = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tmsSearch = new System.Windows.Forms.ToolStripMenuItem();
            this.tmsSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.popPanel = new Infragistics.Win.Misc.UltraPopupControlContainer(this.components);
            this.palPopRefresh = new Infragistics.Win.Misc.UltraPanel();
            this.btnApplyGetData = new Infragistics.Win.Misc.UltraButton();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtLine = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.dteTo = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.dteFrom = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupNgayThang = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbDateTime = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.dtEndDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.dtBeginDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.btnOk = new Infragistics.Win.Misc.UltraButton();
            this.cms4Grid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            this.palPopRefresh.ClientArea.SuspendLayout();
            this.palPopRefresh.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupNgayThang)).BeginInit();
            this.uGroupNgayThang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtBeginDate)).BeginInit();
            this.SuspendLayout();
            // 
            // cms4Grid
            // 
            this.cms4Grid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tmsImport,
            this.tmsExport,
            this.toolStripMenuItem1,
            this.tmsDelete,
            this.toolStripSeparator1,
            this.tmsReLoad,
            this.toolStripSeparator2,
            this.tmsSearch,
            this.tmsSelectAll});
            this.cms4Grid.Name = "cms4Grid";
            this.cms4Grid.Size = new System.Drawing.Size(178, 154);
            this.cms4Grid.Closing += new System.Windows.Forms.ToolStripDropDownClosingEventHandler(this.cms4Grid_Closing);
            this.cms4Grid.Opening += new System.ComponentModel.CancelEventHandler(this.cms4Grid_Opening);
            // 
            // tmsImport
            // 
            this.tmsImport.Image = global::Accounting.Properties.Resources.open_file;
            this.tmsImport.Name = "tmsImport";
            this.tmsImport.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.tmsImport.Size = new System.Drawing.Size(177, 22);
            this.tmsImport.Text = "Mở tệp";
            this.tmsImport.Click += new System.EventHandler(this.tmsImport_Click);
            // 
            // tmsExport
            // 
            this.tmsExport.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.tmsExport.Name = "tmsExport";
            this.tmsExport.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.tmsExport.Size = new System.Drawing.Size(177, 22);
            this.tmsExport.Text = "Kết xuất tệp";
            this.tmsExport.Click += new System.EventHandler(this.tmsExport_Click);
            this.tmsExport.MouseHover += new System.EventHandler(this.tmsExport_MouseHover);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(174, 6);
            // 
            // tmsDelete
            // 
            this.tmsDelete.Image = global::Accounting.Properties.Resources.xoa1_xoa_du_lieu_ke_toan_khoi_database_;
            this.tmsDelete.Name = "tmsDelete";
            this.tmsDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.tmsDelete.Size = new System.Drawing.Size(177, 22);
            this.tmsDelete.Text = "Xóa";
            this.tmsDelete.Click += new System.EventHandler(this.tmsDelete_Click);
            this.tmsDelete.MouseHover += new System.EventHandler(this.tmsDelete_MouseHover);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(174, 6);
            // 
            // tmsReLoad
            // 
            this.tmsReLoad.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tmsGetData});
            this.tmsReLoad.Image = global::Accounting.Properties.Resources.ubtnReset;
            this.tmsReLoad.Name = "tmsReLoad";
            this.tmsReLoad.ShortcutKeyDisplayString = "F5";
            this.tmsReLoad.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.tmsReLoad.Size = new System.Drawing.Size(177, 22);
            this.tmsReLoad.Text = "Tải Lại";
            this.tmsReLoad.Click += new System.EventHandler(this.tmsReLoad_Click);
            // 
            // tmsGetData
            // 
            this.tmsGetData.Name = "tmsGetData";
            this.tmsGetData.Size = new System.Drawing.Size(67, 22);
            this.tmsGetData.Paint += new System.Windows.Forms.PaintEventHandler(this.tmsGetData_Paint);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(174, 6);
            // 
            // tmsSearch
            // 
            this.tmsSearch.Image = global::Accounting.Properties.Resources.btnsearch;
            this.tmsSearch.Name = "tmsSearch";
            this.tmsSearch.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.tmsSearch.Size = new System.Drawing.Size(177, 22);
            this.tmsSearch.Text = "Tìm kiếm";
            this.tmsSearch.Visible = false;
            this.tmsSearch.MouseHover += new System.EventHandler(this.tmsSearch_MouseHover);
            // 
            // tmsSelectAll
            // 
            this.tmsSelectAll.Image = global::Accounting.Properties.Resources.select;
            this.tmsSelectAll.Name = "tmsSelectAll";
            this.tmsSelectAll.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.tmsSelectAll.Size = new System.Drawing.Size(177, 22);
            this.tmsSelectAll.Text = "Chọn tất cả";
            this.tmsSelectAll.Click += new System.EventHandler(this.tmsSelectAll_Click);
            // 
            // uGrid
            // 
            this.uGrid.ContextMenuStrip = this.cms4Grid;
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.uGrid.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGrid.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGrid.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGrid.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.uGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid.Location = new System.Drawing.Point(0, 61);
            this.uGrid.Name = "uGrid";
            this.uGrid.Size = new System.Drawing.Size(851, 383);
            this.uGrid.TabIndex = 40;
            this.uGrid.Text = "uGrid";
            this.uGrid.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGrid_DoubleClickRow);
            // 
            // popPanel
            // 
            this.popPanel.PopupControl = this.palPopRefresh;
            this.popPanel.Closed += new System.EventHandler(this.popPanel_Closed);
            // 
            // palPopRefresh
            // 
            this.palPopRefresh.BorderStyle = Infragistics.Win.UIElementBorderStyle.RaisedSoft;
            // 
            // palPopRefresh.ClientArea
            // 
            this.palPopRefresh.ClientArea.Controls.Add(this.btnApplyGetData);
            this.palPopRefresh.ClientArea.Controls.Add(this.ultraGroupBox1);
            this.palPopRefresh.Location = new System.Drawing.Point(418, 250);
            this.palPopRefresh.Name = "palPopRefresh";
            this.palPopRefresh.Size = new System.Drawing.Size(236, 154);
            this.palPopRefresh.TabIndex = 41;
            this.palPopRefresh.Visible = false;
            // 
            // btnApplyGetData
            // 
            this.btnApplyGetData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance11.Image = global::Accounting.Properties.Resources.apply_16;
            this.btnApplyGetData.Appearance = appearance11;
            this.btnApplyGetData.Location = new System.Drawing.Point(136, 115);
            this.btnApplyGetData.MaximumSize = new System.Drawing.Size(83, 30);
            this.btnApplyGetData.Name = "btnApplyGetData";
            this.btnApplyGetData.Size = new System.Drawing.Size(83, 30);
            this.btnApplyGetData.TabIndex = 1;
            this.btnApplyGetData.Text = "Đồng ý";
            this.btnApplyGetData.Click += new System.EventHandler(this.btnApplyGetData_Click);
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraGroupBox1.Controls.Add(this.txtLine);
            this.ultraGroupBox1.Controls.Add(this.dteTo);
            this.ultraGroupBox1.Controls.Add(this.dteFrom);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel3);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox1.Location = new System.Drawing.Point(14, 13);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(205, 97);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.UseAppStyling = false;
            // 
            // txtLine
            // 
            this.txtLine.AutoSize = false;
            this.txtLine.Location = new System.Drawing.Point(103, 66);
            this.txtLine.Name = "txtLine";
            this.txtLine.Size = new System.Drawing.Size(89, 22);
            this.txtLine.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always;
            this.txtLine.TabIndex = 5;
            // 
            // dteTo
            // 
            this.dteTo.AutoSize = false;
            this.dteTo.Location = new System.Drawing.Point(103, 38);
            this.dteTo.Name = "dteTo";
            this.dteTo.Size = new System.Drawing.Size(89, 22);
            this.dteTo.TabIndex = 4;
            // 
            // dteFrom
            // 
            this.dteFrom.AutoSize = false;
            this.dteFrom.Location = new System.Drawing.Point(103, 10);
            this.dteFrom.Name = "dteFrom";
            this.dteFrom.Size = new System.Drawing.Size(89, 22);
            this.dteFrom.TabIndex = 3;
            // 
            // ultraLabel3
            // 
            appearance12.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance12;
            this.ultraLabel3.Location = new System.Drawing.Point(7, 66);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(89, 22);
            this.ultraLabel3.TabIndex = 2;
            this.ultraLabel3.Text = "Số dòng hiển thị";
            // 
            // ultraLabel2
            // 
            appearance13.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance13;
            this.ultraLabel2.Location = new System.Drawing.Point(7, 38);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(89, 22);
            this.ultraLabel2.TabIndex = 1;
            this.ultraLabel2.Text = "Đến ngày";
            // 
            // ultraLabel1
            // 
            appearance14.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance14;
            this.ultraLabel1.Location = new System.Drawing.Point(7, 10);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(89, 22);
            this.ultraLabel1.TabIndex = 0;
            this.ultraLabel1.Text = "Từ ngày";
            // 
            // uGroupNgayThang
            // 
            this.uGroupNgayThang.Controls.Add(this.cbbDateTime);
            this.uGroupNgayThang.Controls.Add(this.dtEndDate);
            this.uGroupNgayThang.Controls.Add(this.dtBeginDate);
            this.uGroupNgayThang.Controls.Add(this.ultraLabel4);
            this.uGroupNgayThang.Controls.Add(this.ultraLabel5);
            this.uGroupNgayThang.Controls.Add(this.ultraLabel6);
            this.uGroupNgayThang.Controls.Add(this.btnOk);
            this.uGroupNgayThang.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGroupNgayThang.Location = new System.Drawing.Point(0, 0);
            this.uGroupNgayThang.Name = "uGroupNgayThang";
            this.uGroupNgayThang.Size = new System.Drawing.Size(851, 61);
            this.uGroupNgayThang.TabIndex = 42;
            // 
            // cbbDateTime
            // 
            this.cbbDateTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbDateTime.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbDateTime.Location = new System.Drawing.Point(156, 28);
            this.cbbDateTime.Name = "cbbDateTime";
            this.cbbDateTime.NullText = "<chọn dữ liệu>";
            this.cbbDateTime.Size = new System.Drawing.Size(272, 22);
            this.cbbDateTime.TabIndex = 56;
            // 
            // dtEndDate
            // 
            this.dtEndDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance15.TextHAlignAsString = "Center";
            appearance15.TextVAlignAsString = "Middle";
            this.dtEndDate.Appearance = appearance15;
            this.dtEndDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtEndDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtEndDate.Location = new System.Drawing.Point(585, 30);
            this.dtEndDate.MaskInput = "";
            this.dtEndDate.Name = "dtEndDate";
            this.dtEndDate.Size = new System.Drawing.Size(130, 21);
            this.dtEndDate.TabIndex = 58;
            this.dtEndDate.Value = null;
            // 
            // dtBeginDate
            // 
            this.dtBeginDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance16.TextHAlignAsString = "Center";
            appearance16.TextVAlignAsString = "Middle";
            this.dtBeginDate.Appearance = appearance16;
            this.dtBeginDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtBeginDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtBeginDate.Location = new System.Drawing.Point(443, 30);
            this.dtBeginDate.MaskInput = "";
            this.dtBeginDate.Name = "dtBeginDate";
            this.dtBeginDate.Size = new System.Drawing.Size(136, 21);
            this.dtBeginDate.TabIndex = 57;
            this.dtBeginDate.Value = null;
            // 
            // ultraLabel4
            // 
            this.ultraLabel4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance17.BackColor = System.Drawing.Color.Transparent;
            appearance17.TextHAlignAsString = "Left";
            appearance17.TextVAlignAsString = "Bottom";
            this.ultraLabel4.Appearance = appearance17;
            this.ultraLabel4.Location = new System.Drawing.Point(64, 29);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(89, 17);
            this.ultraLabel4.TabIndex = 59;
            this.ultraLabel4.Text = "Khoảng thời gian";
            // 
            // ultraLabel5
            // 
            this.ultraLabel5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance18.BackColor = System.Drawing.Color.Transparent;
            appearance18.TextHAlignAsString = "Left";
            appearance18.TextVAlignAsString = "Bottom";
            this.ultraLabel5.Appearance = appearance18;
            this.ultraLabel5.Location = new System.Drawing.Point(585, 7);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(89, 17);
            this.ultraLabel5.TabIndex = 61;
            this.ultraLabel5.Text = "Đến ngày";
            // 
            // ultraLabel6
            // 
            this.ultraLabel6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance19.BackColor = System.Drawing.Color.Transparent;
            appearance19.TextHAlignAsString = "Left";
            appearance19.TextVAlignAsString = "Bottom";
            this.ultraLabel6.Appearance = appearance19;
            this.ultraLabel6.Location = new System.Drawing.Point(443, 7);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(89, 17);
            this.ultraLabel6.TabIndex = 60;
            this.ultraLabel6.Text = "Từ ngày";
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance20.Image = global::Accounting.Properties.Resources.apply_16;
            this.btnOk.Appearance = appearance20;
            this.btnOk.Location = new System.Drawing.Point(726, 27);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(112, 25);
            this.btnOk.TabIndex = 62;
            this.btnOk.Text = "Đồng ý";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // FSysLog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(851, 444);
            this.Controls.Add(this.uGrid);
            this.Controls.Add(this.uGroupNgayThang);
            this.Controls.Add(this.palPopRefresh);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FSysLog";
            this.Text = "Nhật ký truy cập";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FSysLog_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FSysLog_FormClosed);
            this.cms4Grid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            this.palPopRefresh.ClientArea.ResumeLayout(false);
            this.palPopRefresh.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtLine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupNgayThang)).EndInit();
            this.uGroupNgayThang.ResumeLayout(false);
            this.uGroupNgayThang.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtBeginDate)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
        private System.Windows.Forms.ContextMenuStrip cms4Grid;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem tmsReLoad;
        private System.Windows.Forms.ToolStripMenuItem tmsExport;
        private System.Windows.Forms.ToolStripMenuItem tmsDelete;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem tmsSearch;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private Infragistics.Win.Misc.UltraPopupControlContainer popPanel;
        private Infragistics.Win.Misc.UltraPanel palPopRefresh;
        private Infragistics.Win.Misc.UltraButton btnApplyGetData;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor txtLine;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteTo;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteFrom;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private System.Windows.Forms.ToolStripMenuItem tmsGetData;
        private System.Windows.Forms.ToolStripMenuItem tmsSelectAll;
        private System.Windows.Forms.ToolStripMenuItem tmsImport;
        private Infragistics.Win.Misc.UltraGroupBox uGroupNgayThang;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbDateTime;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtEndDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtBeginDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraButton btnOk;
    }
}