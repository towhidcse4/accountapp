﻿namespace Accounting
{
    partial class FSysUserProfile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraPanel2 = new Infragistics.Win.Misc.UltraPanel();
            this.btnUpload = new Infragistics.Win.Misc.UltraButton();
            this.picPhoto = new Infragistics.Win.UltraWinEditors.UltraPictureBox();
            this.txtFax = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel19 = new Infragistics.Win.Misc.UltraLabel();
            this.txtHomePhone = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtMobilePhone = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtWorkPhone = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel17 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAdress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtWebsite = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtEmail = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel18 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel20 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel23 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel24 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel25 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbCountry = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.dteBirthDay = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.txtDescription = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtIDCard = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtCity = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtProvince = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtJob = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtFullName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.lbl1 = new Infragistics.Win.Misc.UltraLabel();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            this.ultraPanel2.ClientArea.SuspendLayout();
            this.ultraPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWebsite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCountry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteBirthDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIDCard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProvince)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJob)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFullName)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(471, 335);
            // 
            // ultraPanel1
            // 
            this.ultraPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraPanel1.AutoScroll = true;
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraPanel2);
            this.ultraPanel1.Location = new System.Drawing.Point(12, 13);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(475, 465);
            this.ultraPanel1.TabIndex = 4;
            // 
            // ultraPanel2
            // 
            // 
            // ultraPanel2.ClientArea
            // 
            this.ultraPanel2.ClientArea.Controls.Add(this.btnUpload);
            this.ultraPanel2.ClientArea.Controls.Add(this.picPhoto);
            this.ultraPanel2.ClientArea.Controls.Add(this.txtFax);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel19);
            this.ultraPanel2.ClientArea.Controls.Add(this.txtHomePhone);
            this.ultraPanel2.ClientArea.Controls.Add(this.txtMobilePhone);
            this.ultraPanel2.ClientArea.Controls.Add(this.txtWorkPhone);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel17);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel16);
            this.ultraPanel2.ClientArea.Controls.Add(this.txtAdress);
            this.ultraPanel2.ClientArea.Controls.Add(this.txtWebsite);
            this.ultraPanel2.ClientArea.Controls.Add(this.txtEmail);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel18);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel20);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel23);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel24);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel25);
            this.ultraPanel2.ClientArea.Controls.Add(this.cbbCountry);
            this.ultraPanel2.ClientArea.Controls.Add(this.dteBirthDay);
            this.ultraPanel2.ClientArea.Controls.Add(this.txtDescription);
            this.ultraPanel2.ClientArea.Controls.Add(this.txtIDCard);
            this.ultraPanel2.ClientArea.Controls.Add(this.txtCity);
            this.ultraPanel2.ClientArea.Controls.Add(this.txtProvince);
            this.ultraPanel2.ClientArea.Controls.Add(this.txtJob);
            this.ultraPanel2.ClientArea.Controls.Add(this.txtFullName);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel10);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel3);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel13);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel15);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel11);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel12);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel6);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel2);
            this.ultraPanel2.ClientArea.Controls.Add(this.lbl1);
            this.ultraPanel2.Location = new System.Drawing.Point(0, 1);
            this.ultraPanel2.Name = "ultraPanel2";
            this.ultraPanel2.Size = new System.Drawing.Size(458, 698);
            this.ultraPanel2.TabIndex = 5;
            // 
            // btnUpload
            // 
            appearance1.Image = global::Accounting.Properties.Resources.folder;
            this.btnUpload.Appearance = appearance1;
            this.btnUpload.Location = new System.Drawing.Point(144, 403);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Size = new System.Drawing.Size(27, 26);
            this.btnUpload.TabIndex = 142;
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // picPhoto
            // 
            appearance2.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance2.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.picPhoto.Appearance = appearance2;
            this.picPhoto.BackColor = System.Drawing.Color.White;
            this.picPhoto.BorderShadowColor = System.Drawing.Color.Empty;
            this.picPhoto.DefaultImage = global::Accounting.Properties.Resources.technology;
            this.picPhoto.ImageTransparentColor = System.Drawing.Color.White;
            this.picPhoto.Location = new System.Drawing.Point(144, 280);
            this.picPhoto.Name = "picPhoto";
            this.picPhoto.Size = new System.Drawing.Size(100, 117);
            this.picPhoto.TabIndex = 141;
            // 
            // txtFax
            // 
            this.txtFax.AutoSize = false;
            this.txtFax.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.txtFax.InputMask = "(nn) nnnnnnnnnnnn";
            this.txtFax.Location = new System.Drawing.Point(144, 570);
            this.txtFax.Name = "txtFax";
            this.txtFax.Size = new System.Drawing.Size(233, 22);
            this.txtFax.TabIndex = 139;
            this.txtFax.Text = "() ";
            // 
            // ultraLabel19
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextHAlignAsString = "Right";
            appearance3.TextVAlignAsString = "Middle";
            this.ultraLabel19.Appearance = appearance3;
            this.ultraLabel19.Location = new System.Drawing.Point(32, 280);
            this.ultraLabel19.Name = "ultraLabel19";
            this.ultraLabel19.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel19.TabIndex = 140;
            this.ultraLabel19.Text = "Hình đại diện";
            // 
            // txtHomePhone
            // 
            this.txtHomePhone.AutoSize = false;
            this.txtHomePhone.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.txtHomePhone.InputMask = "(nn) nnnnnnnnnnnn";
            this.txtHomePhone.Location = new System.Drawing.Point(144, 543);
            this.txtHomePhone.Name = "txtHomePhone";
            this.txtHomePhone.Size = new System.Drawing.Size(233, 22);
            this.txtHomePhone.TabIndex = 138;
            this.txtHomePhone.Text = "() ";
            // 
            // txtMobilePhone
            // 
            this.txtMobilePhone.AutoSize = false;
            this.txtMobilePhone.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.txtMobilePhone.InputMask = "(nn) nnnnnnnnnnnn";
            this.txtMobilePhone.Location = new System.Drawing.Point(144, 516);
            this.txtMobilePhone.Name = "txtMobilePhone";
            this.txtMobilePhone.Size = new System.Drawing.Size(233, 22);
            this.txtMobilePhone.TabIndex = 137;
            this.txtMobilePhone.Text = "() ";
            // 
            // txtWorkPhone
            // 
            this.txtWorkPhone.AutoSize = false;
            this.txtWorkPhone.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.txtWorkPhone.InputMask = "(nn) nnnnnnnnnnnn";
            this.txtWorkPhone.Location = new System.Drawing.Point(144, 489);
            this.txtWorkPhone.Name = "txtWorkPhone";
            this.txtWorkPhone.Size = new System.Drawing.Size(233, 22);
            this.txtWorkPhone.TabIndex = 136;
            this.txtWorkPhone.Text = "() ";
            // 
            // ultraLabel17
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextHAlignAsString = "Right";
            appearance4.TextVAlignAsString = "Middle";
            this.ultraLabel17.Appearance = appearance4;
            this.ultraLabel17.Location = new System.Drawing.Point(32, 516);
            this.ultraLabel17.Name = "ultraLabel17";
            this.ultraLabel17.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel17.TabIndex = 135;
            this.ultraLabel17.Text = "ĐT di động";
            // 
            // ultraLabel16
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.TextHAlignAsString = "Right";
            appearance5.TextVAlignAsString = "Middle";
            this.ultraLabel16.Appearance = appearance5;
            this.ultraLabel16.Location = new System.Drawing.Point(32, 489);
            this.ultraLabel16.Name = "ultraLabel16";
            this.ultraLabel16.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel16.TabIndex = 134;
            this.ultraLabel16.Text = "ĐT cơ quan";
            // 
            // txtAdress
            // 
            this.txtAdress.AutoSize = false;
            this.txtAdress.Location = new System.Drawing.Point(144, 597);
            this.txtAdress.Multiline = true;
            this.txtAdress.Name = "txtAdress";
            this.txtAdress.Scrollbars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtAdress.Size = new System.Drawing.Size(233, 73);
            this.txtAdress.TabIndex = 133;
            // 
            // txtWebsite
            // 
            this.txtWebsite.AutoSize = false;
            this.txtWebsite.Location = new System.Drawing.Point(144, 462);
            this.txtWebsite.Name = "txtWebsite";
            this.txtWebsite.Size = new System.Drawing.Size(233, 22);
            this.txtWebsite.TabIndex = 132;
            // 
            // txtEmail
            // 
            this.txtEmail.AutoSize = false;
            this.txtEmail.Location = new System.Drawing.Point(144, 435);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(233, 22);
            this.txtEmail.TabIndex = 131;
            // 
            // ultraLabel18
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextHAlignAsString = "Right";
            appearance6.TextVAlignAsString = "Middle";
            this.ultraLabel18.Appearance = appearance6;
            this.ultraLabel18.Location = new System.Drawing.Point(32, 570);
            this.ultraLabel18.Name = "ultraLabel18";
            this.ultraLabel18.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel18.TabIndex = 130;
            this.ultraLabel18.Text = "Fax";
            // 
            // ultraLabel20
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.TextHAlignAsString = "Right";
            appearance7.TextVAlignAsString = "Middle";
            this.ultraLabel20.Appearance = appearance7;
            this.ultraLabel20.Location = new System.Drawing.Point(32, 543);
            this.ultraLabel20.Name = "ultraLabel20";
            this.ultraLabel20.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel20.TabIndex = 129;
            this.ultraLabel20.Text = "ĐT nhà riêng";
            // 
            // ultraLabel23
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.TextHAlignAsString = "Right";
            appearance8.TextVAlignAsString = "Middle";
            this.ultraLabel23.Appearance = appearance8;
            this.ultraLabel23.Location = new System.Drawing.Point(32, 597);
            this.ultraLabel23.Name = "ultraLabel23";
            this.ultraLabel23.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel23.TabIndex = 128;
            this.ultraLabel23.Text = "Địa chỉ";
            // 
            // ultraLabel24
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.TextHAlignAsString = "Right";
            appearance9.TextVAlignAsString = "Middle";
            this.ultraLabel24.Appearance = appearance9;
            this.ultraLabel24.Location = new System.Drawing.Point(32, 462);
            this.ultraLabel24.Name = "ultraLabel24";
            this.ultraLabel24.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel24.TabIndex = 127;
            this.ultraLabel24.Text = "Website";
            // 
            // ultraLabel25
            // 
            appearance10.BackColor = System.Drawing.Color.Transparent;
            appearance10.TextHAlignAsString = "Right";
            appearance10.TextVAlignAsString = "Middle";
            this.ultraLabel25.Appearance = appearance10;
            this.ultraLabel25.Location = new System.Drawing.Point(32, 435);
            this.ultraLabel25.Name = "ultraLabel25";
            this.ultraLabel25.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel25.TabIndex = 126;
            this.ultraLabel25.Text = "Email";
            // 
            // cbbCountry
            // 
            this.cbbCountry.AutoSize = false;
            this.cbbCountry.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbbCountry.Location = new System.Drawing.Point(144, 94);
            this.cbbCountry.Name = "cbbCountry";
            this.cbbCountry.Size = new System.Drawing.Size(233, 22);
            this.cbbCountry.TabIndex = 125;
            // 
            // dteBirthDay
            // 
            appearance11.TextHAlignAsString = "Right";
            this.dteBirthDay.Appearance = appearance11;
            this.dteBirthDay.AutoSize = false;
            this.dteBirthDay.Location = new System.Drawing.Point(144, 67);
            this.dteBirthDay.MaskInput = "dd/mm/yyyy";
            this.dteBirthDay.Name = "dteBirthDay";
            this.dteBirthDay.Size = new System.Drawing.Size(106, 22);
            this.dteBirthDay.TabIndex = 124;
            // 
            // txtDescription
            // 
            this.txtDescription.AutoSize = false;
            this.txtDescription.Location = new System.Drawing.Point(144, 202);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Scrollbars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDescription.Size = new System.Drawing.Size(233, 73);
            this.txtDescription.TabIndex = 123;
            // 
            // txtIDCard
            // 
            this.txtIDCard.AutoSize = false;
            this.txtIDCard.Location = new System.Drawing.Point(144, 175);
            this.txtIDCard.Name = "txtIDCard";
            this.txtIDCard.Size = new System.Drawing.Size(233, 22);
            this.txtIDCard.TabIndex = 122;
            // 
            // txtCity
            // 
            this.txtCity.AutoSize = false;
            this.txtCity.Location = new System.Drawing.Point(144, 148);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(233, 22);
            this.txtCity.TabIndex = 121;
            // 
            // txtProvince
            // 
            this.txtProvince.AutoSize = false;
            this.txtProvince.Location = new System.Drawing.Point(144, 121);
            this.txtProvince.Name = "txtProvince";
            this.txtProvince.Size = new System.Drawing.Size(233, 22);
            this.txtProvince.TabIndex = 120;
            // 
            // txtJob
            // 
            this.txtJob.AutoSize = false;
            this.txtJob.Location = new System.Drawing.Point(144, 40);
            this.txtJob.Name = "txtJob";
            this.txtJob.Size = new System.Drawing.Size(233, 22);
            this.txtJob.TabIndex = 119;
            // 
            // txtFullName
            // 
            this.txtFullName.AutoSize = false;
            this.txtFullName.Location = new System.Drawing.Point(144, 13);
            this.txtFullName.Name = "txtFullName";
            this.txtFullName.Size = new System.Drawing.Size(233, 22);
            this.txtFullName.TabIndex = 118;
            // 
            // ultraLabel10
            // 
            appearance12.BackColor = System.Drawing.Color.Transparent;
            appearance12.ForeColor = System.Drawing.Color.Red;
            appearance12.TextHAlignAsString = "Center";
            appearance12.TextVAlignAsString = "Middle";
            this.ultraLabel10.Appearance = appearance12;
            this.ultraLabel10.Location = new System.Drawing.Point(116, 13);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(16, 22);
            this.ultraLabel10.TabIndex = 116;
            this.ultraLabel10.Text = "(*)";
            this.ultraLabel10.WrapText = false;
            // 
            // ultraLabel3
            // 
            appearance13.BackColor = System.Drawing.Color.Transparent;
            appearance13.TextHAlignAsString = "Right";
            appearance13.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance13;
            this.ultraLabel3.Location = new System.Drawing.Point(32, 148);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel3.TabIndex = 115;
            this.ultraLabel3.Text = "Quận/huyện";
            // 
            // ultraLabel13
            // 
            appearance14.BackColor = System.Drawing.Color.Transparent;
            appearance14.TextHAlignAsString = "Right";
            appearance14.TextVAlignAsString = "Middle";
            this.ultraLabel13.Appearance = appearance14;
            this.ultraLabel13.Location = new System.Drawing.Point(32, 95);
            this.ultraLabel13.Name = "ultraLabel13";
            this.ultraLabel13.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel13.TabIndex = 114;
            this.ultraLabel13.Text = "Quốc gia";
            // 
            // ultraLabel15
            // 
            appearance15.BackColor = System.Drawing.Color.Transparent;
            appearance15.TextHAlignAsString = "Right";
            appearance15.TextVAlignAsString = "Middle";
            this.ultraLabel15.Appearance = appearance15;
            this.ultraLabel15.Location = new System.Drawing.Point(32, 121);
            this.ultraLabel15.Name = "ultraLabel15";
            this.ultraLabel15.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel15.TabIndex = 113;
            this.ultraLabel15.Text = "Tỉnh/Thành phố";
            // 
            // ultraLabel11
            // 
            appearance16.BackColor = System.Drawing.Color.Transparent;
            appearance16.TextHAlignAsString = "Right";
            appearance16.TextVAlignAsString = "Middle";
            this.ultraLabel11.Appearance = appearance16;
            this.ultraLabel11.Location = new System.Drawing.Point(32, 67);
            this.ultraLabel11.Name = "ultraLabel11";
            this.ultraLabel11.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel11.TabIndex = 112;
            this.ultraLabel11.Text = "Ngày sinh";
            // 
            // ultraLabel12
            // 
            appearance17.BackColor = System.Drawing.Color.Transparent;
            appearance17.TextHAlignAsString = "Right";
            appearance17.TextVAlignAsString = "Middle";
            this.ultraLabel12.Appearance = appearance17;
            this.ultraLabel12.Location = new System.Drawing.Point(32, 175);
            this.ultraLabel12.Name = "ultraLabel12";
            this.ultraLabel12.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel12.TabIndex = 111;
            this.ultraLabel12.Text = "CMTND/Passport";
            // 
            // ultraLabel6
            // 
            appearance18.BackColor = System.Drawing.Color.Transparent;
            appearance18.TextHAlignAsString = "Right";
            appearance18.TextVAlignAsString = "Middle";
            this.ultraLabel6.Appearance = appearance18;
            this.ultraLabel6.Location = new System.Drawing.Point(32, 202);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel6.TabIndex = 110;
            this.ultraLabel6.Text = "Mô tả";
            // 
            // ultraLabel2
            // 
            appearance19.BackColor = System.Drawing.Color.Transparent;
            appearance19.TextHAlignAsString = "Right";
            appearance19.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance19;
            this.ultraLabel2.Location = new System.Drawing.Point(32, 40);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel2.TabIndex = 109;
            this.ultraLabel2.Text = "Vị trí công tác";
            // 
            // lbl1
            // 
            appearance20.BackColor = System.Drawing.Color.Transparent;
            appearance20.TextHAlignAsString = "Right";
            appearance20.TextVAlignAsString = "Middle";
            this.lbl1.Appearance = appearance20;
            this.lbl1.Location = new System.Drawing.Point(18, 13);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(100, 22);
            this.lbl1.TabIndex = 108;
            this.lbl1.Text = "Họ và tên";
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance21.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance21;
            this.btnSave.Location = new System.Drawing.Point(290, 488);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(107, 30);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Lưu thay đổi";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance22.Image = global::Accounting.Properties.Resources.cancel_16;
            this.btnClose.Appearance = appearance22;
            this.btnClose.Location = new System.Drawing.Point(403, 488);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(84, 30);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Hủy bỏ";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // FSysUserProfile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(499, 526);
            this.Controls.Add(this.ultraPanel1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnClose);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FSysUserProfile";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thay đổi thông tin cá nhân";
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            this.ultraPanel2.ClientArea.ResumeLayout(false);
            this.ultraPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtAdress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWebsite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCountry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteBirthDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIDCard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProvince)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJob)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFullName)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraPanel ultraPanel2;
        private Infragistics.Win.Misc.UltraButton btnUpload;
        private Infragistics.Win.UltraWinEditors.UltraPictureBox picPhoto;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtFax;
        private Infragistics.Win.Misc.UltraLabel ultraLabel19;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtHomePhone;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtMobilePhone;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtWorkPhone;
        private Infragistics.Win.Misc.UltraLabel ultraLabel17;
        private Infragistics.Win.Misc.UltraLabel ultraLabel16;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAdress;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtWebsite;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtEmail;
        private Infragistics.Win.Misc.UltraLabel ultraLabel18;
        private Infragistics.Win.Misc.UltraLabel ultraLabel20;
        private Infragistics.Win.Misc.UltraLabel ultraLabel23;
        private Infragistics.Win.Misc.UltraLabel ultraLabel24;
        private Infragistics.Win.Misc.UltraLabel ultraLabel25;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbCountry;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteBirthDay;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDescription;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtIDCard;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCity;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtProvince;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtJob;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtFullName;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel13;
        private Infragistics.Win.Misc.UltraLabel ultraLabel15;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
        private Infragistics.Win.Misc.UltraLabel ultraLabel12;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel lbl1;

    }
}