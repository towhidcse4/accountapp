﻿namespace Accounting
{
    partial class FCatSysUsers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FCatSysUsers));
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance89 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance90 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance91 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance92 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance93 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance94 = new Infragistics.Win.Appearance();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraLabel27 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel26 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel25 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel24 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel23 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel22 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel21 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel20 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel19 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel18 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel17 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.txtProvince = new Infragistics.Win.Misc.UltraLabel();
            this.txtCountry = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.txtIDCard = new Infragistics.Win.Misc.UltraLabel();
            this.txtBirthDay = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.pixPhoto = new Infragistics.Win.UltraWinEditors.UltraPictureBox();
            this.txtEmail = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.txtMobilePhone = new Infragistics.Win.Misc.UltraLabel();
            this.txtFax = new Infragistics.Win.Misc.UltraLabel();
            this.txtDescription = new Infragistics.Win.Misc.UltraLabel();
            this.txtAddress = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.txtWebsite = new Infragistics.Win.Misc.UltraLabel();
            this.txtHomePhone = new Infragistics.Win.Misc.UltraLabel();
            this.txtWorkPhone = new Infragistics.Win.Misc.UltraLabel();
            this.txtJob = new Infragistics.Win.Misc.UltraLabel();
            this.txtFullName = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.lbl1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGrid0 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.btnAddRole = new Infragistics.Win.Misc.UltraButton();
            this.btnRemoveRole = new Infragistics.Win.Misc.UltraButton();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.cms4Grid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.tmsReLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.ultraSplitter1 = new Infragistics.Win.Misc.UltraSplitter();
            this.ultraTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.btnAdd = new Infragistics.Win.Misc.UltraButton();
            this.btnDelete = new Infragistics.Win.Misc.UltraButton();
            this.btnEdit = new Infragistics.Win.Misc.UltraButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ultraTabPageControl1.SuspendLayout();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid0)).BeginInit();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            this.cms4Grid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).BeginInit();
            this.ultraTabControl1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel27);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel26);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel25);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel24);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel23);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel22);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel21);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel20);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel19);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel18);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel17);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel16);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel10);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel9);
            this.ultraTabPageControl1.Controls.Add(this.txtProvince);
            this.ultraTabPageControl1.Controls.Add(this.txtCountry);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel13);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel15);
            this.ultraTabPageControl1.Controls.Add(this.txtIDCard);
            this.ultraTabPageControl1.Controls.Add(this.txtBirthDay);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel11);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel12);
            this.ultraTabPageControl1.Controls.Add(this.pixPhoto);
            this.ultraTabPageControl1.Controls.Add(this.txtEmail);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel14);
            this.ultraTabPageControl1.Controls.Add(this.txtMobilePhone);
            this.ultraTabPageControl1.Controls.Add(this.txtFax);
            this.ultraTabPageControl1.Controls.Add(this.txtDescription);
            this.ultraTabPageControl1.Controls.Add(this.txtAddress);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel8);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel7);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel6);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel1);
            this.ultraTabPageControl1.Controls.Add(this.txtWebsite);
            this.ultraTabPageControl1.Controls.Add(this.txtHomePhone);
            this.ultraTabPageControl1.Controls.Add(this.txtWorkPhone);
            this.ultraTabPageControl1.Controls.Add(this.txtJob);
            this.ultraTabPageControl1.Controls.Add(this.txtFullName);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel5);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel4);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel3);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel2);
            this.ultraTabPageControl1.Controls.Add(this.lbl1);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(701, 252);
            // 
            // ultraLabel27
            // 
            appearance48.BackColor = System.Drawing.Color.Transparent;
            appearance48.TextVAlignAsString = "Middle";
            this.ultraLabel27.Appearance = appearance48;
            this.ultraLabel27.Location = new System.Drawing.Point(401, 74);
            this.ultraLabel27.Name = "ultraLabel27";
            this.ultraLabel27.Size = new System.Drawing.Size(10, 22);
            this.ultraLabel27.TabIndex = 43;
            this.ultraLabel27.Text = ":";
            // 
            // ultraLabel26
            // 
            appearance49.BackColor = System.Drawing.Color.Transparent;
            appearance49.TextVAlignAsString = "Middle";
            this.ultraLabel26.Appearance = appearance49;
            this.ultraLabel26.Location = new System.Drawing.Point(401, 51);
            this.ultraLabel26.Name = "ultraLabel26";
            this.ultraLabel26.Size = new System.Drawing.Size(10, 22);
            this.ultraLabel26.TabIndex = 42;
            this.ultraLabel26.Text = ":";
            // 
            // ultraLabel25
            // 
            appearance50.BackColor = System.Drawing.Color.Transparent;
            appearance50.TextVAlignAsString = "Middle";
            this.ultraLabel25.Appearance = appearance50;
            this.ultraLabel25.Location = new System.Drawing.Point(401, 28);
            this.ultraLabel25.Name = "ultraLabel25";
            this.ultraLabel25.Size = new System.Drawing.Size(10, 22);
            this.ultraLabel25.TabIndex = 41;
            this.ultraLabel25.Text = ":";
            // 
            // ultraLabel24
            // 
            appearance51.BackColor = System.Drawing.Color.Transparent;
            appearance51.TextVAlignAsString = "Middle";
            this.ultraLabel24.Appearance = appearance51;
            this.ultraLabel24.Location = new System.Drawing.Point(401, 97);
            this.ultraLabel24.Name = "ultraLabel24";
            this.ultraLabel24.Size = new System.Drawing.Size(10, 22);
            this.ultraLabel24.TabIndex = 40;
            this.ultraLabel24.Text = ":";
            // 
            // ultraLabel23
            // 
            appearance52.BackColor = System.Drawing.Color.Transparent;
            appearance52.TextVAlignAsString = "Middle";
            this.ultraLabel23.Appearance = appearance52;
            this.ultraLabel23.Location = new System.Drawing.Point(401, 120);
            this.ultraLabel23.Name = "ultraLabel23";
            this.ultraLabel23.Size = new System.Drawing.Size(10, 22);
            this.ultraLabel23.TabIndex = 39;
            this.ultraLabel23.Text = ":";
            // 
            // ultraLabel22
            // 
            appearance53.BackColor = System.Drawing.Color.Transparent;
            appearance53.TextVAlignAsString = "Middle";
            this.ultraLabel22.Appearance = appearance53;
            this.ultraLabel22.Location = new System.Drawing.Point(401, 3);
            this.ultraLabel22.Name = "ultraLabel22";
            this.ultraLabel22.Size = new System.Drawing.Size(10, 22);
            this.ultraLabel22.TabIndex = 38;
            this.ultraLabel22.Text = ":";
            // 
            // ultraLabel21
            // 
            appearance54.BackColor = System.Drawing.Color.Transparent;
            appearance54.TextVAlignAsString = "Middle";
            this.ultraLabel21.Appearance = appearance54;
            this.ultraLabel21.Location = new System.Drawing.Point(90, 166);
            this.ultraLabel21.Name = "ultraLabel21";
            this.ultraLabel21.Size = new System.Drawing.Size(10, 22);
            this.ultraLabel21.TabIndex = 37;
            this.ultraLabel21.Text = ":";
            // 
            // ultraLabel20
            // 
            appearance55.BackColor = System.Drawing.Color.Transparent;
            appearance55.TextVAlignAsString = "Middle";
            this.ultraLabel20.Appearance = appearance55;
            this.ultraLabel20.Location = new System.Drawing.Point(90, 143);
            this.ultraLabel20.Name = "ultraLabel20";
            this.ultraLabel20.Size = new System.Drawing.Size(10, 22);
            this.ultraLabel20.TabIndex = 36;
            this.ultraLabel20.Text = ":";
            // 
            // ultraLabel19
            // 
            appearance56.BackColor = System.Drawing.Color.Transparent;
            appearance56.TextVAlignAsString = "Middle";
            this.ultraLabel19.Appearance = appearance56;
            this.ultraLabel19.Location = new System.Drawing.Point(90, 120);
            this.ultraLabel19.Name = "ultraLabel19";
            this.ultraLabel19.Size = new System.Drawing.Size(10, 22);
            this.ultraLabel19.TabIndex = 35;
            this.ultraLabel19.Text = ":";
            // 
            // ultraLabel18
            // 
            appearance57.BackColor = System.Drawing.Color.Transparent;
            appearance57.TextVAlignAsString = "Middle";
            this.ultraLabel18.Appearance = appearance57;
            this.ultraLabel18.Location = new System.Drawing.Point(90, 51);
            this.ultraLabel18.Name = "ultraLabel18";
            this.ultraLabel18.Size = new System.Drawing.Size(10, 22);
            this.ultraLabel18.TabIndex = 34;
            this.ultraLabel18.Text = ":";
            // 
            // ultraLabel17
            // 
            appearance58.BackColor = System.Drawing.Color.Transparent;
            appearance58.TextVAlignAsString = "Middle";
            this.ultraLabel17.Appearance = appearance58;
            this.ultraLabel17.Location = new System.Drawing.Point(90, 74);
            this.ultraLabel17.Name = "ultraLabel17";
            this.ultraLabel17.Size = new System.Drawing.Size(10, 22);
            this.ultraLabel17.TabIndex = 33;
            this.ultraLabel17.Text = ":";
            // 
            // ultraLabel16
            // 
            appearance59.BackColor = System.Drawing.Color.Transparent;
            appearance59.TextVAlignAsString = "Middle";
            this.ultraLabel16.Appearance = appearance59;
            this.ultraLabel16.Location = new System.Drawing.Point(90, 97);
            this.ultraLabel16.Name = "ultraLabel16";
            this.ultraLabel16.Size = new System.Drawing.Size(10, 22);
            this.ultraLabel16.TabIndex = 32;
            this.ultraLabel16.Text = ":";
            // 
            // ultraLabel10
            // 
            appearance60.BackColor = System.Drawing.Color.Transparent;
            appearance60.TextVAlignAsString = "Middle";
            this.ultraLabel10.Appearance = appearance60;
            this.ultraLabel10.Location = new System.Drawing.Point(90, 28);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(10, 22);
            this.ultraLabel10.TabIndex = 31;
            this.ultraLabel10.Text = ":";
            // 
            // ultraLabel9
            // 
            appearance61.BackColor = System.Drawing.Color.Transparent;
            appearance61.TextVAlignAsString = "Middle";
            this.ultraLabel9.Appearance = appearance61;
            this.ultraLabel9.Location = new System.Drawing.Point(90, 5);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(10, 22);
            this.ultraLabel9.TabIndex = 30;
            this.ultraLabel9.Text = ":";
            // 
            // txtProvince
            // 
            this.txtProvince.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance62.BackColor = System.Drawing.Color.Transparent;
            appearance62.TextVAlignAsString = "Middle";
            this.txtProvince.Appearance = appearance62;
            this.txtProvince.Location = new System.Drawing.Point(417, 120);
            this.txtProvince.Name = "txtProvince";
            this.txtProvince.Size = new System.Drawing.Size(140, 22);
            this.txtProvince.TabIndex = 29;
            // 
            // txtCountry
            // 
            this.txtCountry.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance63.BackColor = System.Drawing.Color.Transparent;
            appearance63.TextVAlignAsString = "Middle";
            this.txtCountry.Appearance = appearance63;
            this.txtCountry.Location = new System.Drawing.Point(417, 97);
            this.txtCountry.Name = "txtCountry";
            this.txtCountry.Size = new System.Drawing.Size(140, 22);
            this.txtCountry.TabIndex = 28;
            // 
            // ultraLabel13
            // 
            appearance64.BackColor = System.Drawing.Color.Transparent;
            appearance64.TextVAlignAsString = "Middle";
            this.ultraLabel13.Appearance = appearance64;
            this.ultraLabel13.Location = new System.Drawing.Point(301, 97);
            this.ultraLabel13.Name = "ultraLabel13";
            this.ultraLabel13.Size = new System.Drawing.Size(94, 22);
            this.ultraLabel13.TabIndex = 27;
            this.ultraLabel13.Text = "Quốc gia       ";
            // 
            // ultraLabel15
            // 
            appearance65.BackColor = System.Drawing.Color.Transparent;
            appearance65.TextVAlignAsString = "Middle";
            this.ultraLabel15.Appearance = appearance65;
            this.ultraLabel15.Location = new System.Drawing.Point(301, 120);
            this.ultraLabel15.Name = "ultraLabel15";
            this.ultraLabel15.Size = new System.Drawing.Size(94, 22);
            this.ultraLabel15.TabIndex = 26;
            this.ultraLabel15.Text = "Tỉnh/Thành phố      ";
            // 
            // txtIDCard
            // 
            this.txtIDCard.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance66.BackColor = System.Drawing.Color.Transparent;
            appearance66.TextVAlignAsString = "Middle";
            this.txtIDCard.Appearance = appearance66;
            this.txtIDCard.Location = new System.Drawing.Point(417, 28);
            this.txtIDCard.Name = "txtIDCard";
            this.txtIDCard.Size = new System.Drawing.Size(140, 22);
            this.txtIDCard.TabIndex = 25;
            // 
            // txtBirthDay
            // 
            this.txtBirthDay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance67.BackColor = System.Drawing.Color.Transparent;
            appearance67.TextVAlignAsString = "Middle";
            this.txtBirthDay.Appearance = appearance67;
            this.txtBirthDay.Location = new System.Drawing.Point(417, 5);
            this.txtBirthDay.Name = "txtBirthDay";
            this.txtBirthDay.Size = new System.Drawing.Size(140, 22);
            this.txtBirthDay.TabIndex = 24;
            // 
            // ultraLabel11
            // 
            appearance68.BackColor = System.Drawing.Color.Transparent;
            appearance68.TextVAlignAsString = "Middle";
            this.ultraLabel11.Appearance = appearance68;
            this.ultraLabel11.Location = new System.Drawing.Point(301, 5);
            this.ultraLabel11.Name = "ultraLabel11";
            this.ultraLabel11.Size = new System.Drawing.Size(94, 22);
            this.ultraLabel11.TabIndex = 23;
            this.ultraLabel11.Text = "Ngày sinh          ";
            // 
            // ultraLabel12
            // 
            appearance69.BackColor = System.Drawing.Color.Transparent;
            appearance69.TextVAlignAsString = "Middle";
            this.ultraLabel12.Appearance = appearance69;
            this.ultraLabel12.Location = new System.Drawing.Point(301, 28);
            this.ultraLabel12.Name = "ultraLabel12";
            this.ultraLabel12.Size = new System.Drawing.Size(94, 22);
            this.ultraLabel12.TabIndex = 22;
            this.ultraLabel12.Text = "CMTND/Passport    ";
            // 
            // pixPhoto
            // 
            this.pixPhoto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pixPhoto.BackColor = System.Drawing.Color.White;
            this.pixPhoto.BorderShadowColor = System.Drawing.Color.Empty;
            this.pixPhoto.Image = ((object)(resources.GetObject("pixPhoto.Image")));
            this.pixPhoto.ImageTransparentColor = System.Drawing.Color.White;
            this.pixPhoto.Location = new System.Drawing.Point(590, 14);
            this.pixPhoto.Name = "pixPhoto";
            this.pixPhoto.Size = new System.Drawing.Size(100, 117);
            this.pixPhoto.TabIndex = 21;
            // 
            // txtEmail
            // 
            appearance70.BackColor = System.Drawing.Color.Transparent;
            appearance70.TextVAlignAsString = "Middle";
            this.txtEmail.Appearance = appearance70;
            this.txtEmail.Location = new System.Drawing.Point(100, 97);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(191, 22);
            this.txtEmail.TabIndex = 20;
            // 
            // ultraLabel14
            // 
            appearance71.BackColor = System.Drawing.Color.Transparent;
            appearance71.TextVAlignAsString = "Middle";
            this.ultraLabel14.Appearance = appearance71;
            this.ultraLabel14.Location = new System.Drawing.Point(5, 97);
            this.ultraLabel14.Name = "ultraLabel14";
            this.ultraLabel14.Size = new System.Drawing.Size(79, 22);
            this.ultraLabel14.TabIndex = 19;
            this.ultraLabel14.Text = "Email         ";
            // 
            // txtMobilePhone
            // 
            this.txtMobilePhone.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance72.BackColor = System.Drawing.Color.Transparent;
            appearance72.TextVAlignAsString = "Middle";
            this.txtMobilePhone.Appearance = appearance72;
            this.txtMobilePhone.Location = new System.Drawing.Point(417, 74);
            this.txtMobilePhone.Name = "txtMobilePhone";
            this.txtMobilePhone.Size = new System.Drawing.Size(140, 22);
            this.txtMobilePhone.TabIndex = 18;
            // 
            // txtFax
            // 
            this.txtFax.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance73.BackColor = System.Drawing.Color.Transparent;
            appearance73.TextVAlignAsString = "Middle";
            this.txtFax.Appearance = appearance73;
            this.txtFax.Location = new System.Drawing.Point(417, 51);
            this.txtFax.Name = "txtFax";
            this.txtFax.Size = new System.Drawing.Size(140, 22);
            this.txtFax.TabIndex = 16;
            // 
            // txtDescription
            // 
            this.txtDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance74.BackColor = System.Drawing.Color.Transparent;
            this.txtDescription.Appearance = appearance74;
            this.txtDescription.Location = new System.Drawing.Point(100, 170);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(590, 75);
            this.txtDescription.TabIndex = 15;
            // 
            // txtAddress
            // 
            this.txtAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance75.BackColor = System.Drawing.Color.Transparent;
            appearance75.TextVAlignAsString = "Middle";
            this.txtAddress.Appearance = appearance75;
            this.txtAddress.Location = new System.Drawing.Point(100, 143);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(590, 22);
            this.txtAddress.TabIndex = 14;
            // 
            // ultraLabel8
            // 
            appearance76.BackColor = System.Drawing.Color.Transparent;
            appearance76.TextVAlignAsString = "Middle";
            this.ultraLabel8.Appearance = appearance76;
            this.ultraLabel8.Location = new System.Drawing.Point(301, 51);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(94, 22);
            this.ultraLabel8.TabIndex = 13;
            this.ultraLabel8.Text = "Fax     ";
            // 
            // ultraLabel7
            // 
            appearance77.BackColor = System.Drawing.Color.Transparent;
            appearance77.TextVAlignAsString = "Middle";
            this.ultraLabel7.Appearance = appearance77;
            this.ultraLabel7.Location = new System.Drawing.Point(301, 74);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(94, 22);
            this.ultraLabel7.TabIndex = 12;
            this.ultraLabel7.Text = "ĐT di động        ";
            // 
            // ultraLabel6
            // 
            appearance78.BackColor = System.Drawing.Color.Transparent;
            appearance78.TextVAlignAsString = "Middle";
            this.ultraLabel6.Appearance = appearance78;
            this.ultraLabel6.Location = new System.Drawing.Point(5, 166);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(79, 22);
            this.ultraLabel6.TabIndex = 11;
            this.ultraLabel6.Text = "Mô tả        ";
            // 
            // ultraLabel1
            // 
            appearance79.BackColor = System.Drawing.Color.Transparent;
            appearance79.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance79;
            this.ultraLabel1.Location = new System.Drawing.Point(5, 143);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(79, 22);
            this.ultraLabel1.TabIndex = 10;
            this.ultraLabel1.Text = "Địa chỉ       ";
            // 
            // txtWebsite
            // 
            appearance80.BackColor = System.Drawing.Color.Transparent;
            appearance80.TextVAlignAsString = "Middle";
            this.txtWebsite.Appearance = appearance80;
            this.txtWebsite.Location = new System.Drawing.Point(100, 120);
            this.txtWebsite.Name = "txtWebsite";
            this.txtWebsite.Size = new System.Drawing.Size(191, 22);
            this.txtWebsite.TabIndex = 9;
            // 
            // txtHomePhone
            // 
            appearance81.BackColor = System.Drawing.Color.Transparent;
            appearance81.TextVAlignAsString = "Middle";
            this.txtHomePhone.Appearance = appearance81;
            this.txtHomePhone.Location = new System.Drawing.Point(100, 74);
            this.txtHomePhone.Name = "txtHomePhone";
            this.txtHomePhone.Size = new System.Drawing.Size(151, 22);
            this.txtHomePhone.TabIndex = 8;
            this.txtHomePhone.Text = " ";
            // 
            // txtWorkPhone
            // 
            appearance82.BackColor = System.Drawing.Color.Transparent;
            appearance82.TextVAlignAsString = "Middle";
            this.txtWorkPhone.Appearance = appearance82;
            this.txtWorkPhone.Location = new System.Drawing.Point(100, 51);
            this.txtWorkPhone.Name = "txtWorkPhone";
            this.txtWorkPhone.Size = new System.Drawing.Size(151, 22);
            this.txtWorkPhone.TabIndex = 7;
            // 
            // txtJob
            // 
            appearance83.BackColor = System.Drawing.Color.Transparent;
            appearance83.TextVAlignAsString = "Middle";
            this.txtJob.Appearance = appearance83;
            this.txtJob.Location = new System.Drawing.Point(100, 28);
            this.txtJob.Name = "txtJob";
            this.txtJob.Size = new System.Drawing.Size(191, 22);
            this.txtJob.TabIndex = 6;
            // 
            // txtFullName
            // 
            appearance84.BackColor = System.Drawing.Color.Transparent;
            appearance84.TextVAlignAsString = "Middle";
            this.txtFullName.Appearance = appearance84;
            this.txtFullName.Location = new System.Drawing.Point(100, 5);
            this.txtFullName.Name = "txtFullName";
            this.txtFullName.Size = new System.Drawing.Size(191, 22);
            this.txtFullName.TabIndex = 5;
            // 
            // ultraLabel5
            // 
            appearance85.BackColor = System.Drawing.Color.Transparent;
            appearance85.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance85;
            this.ultraLabel5.Location = new System.Drawing.Point(5, 120);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(79, 22);
            this.ultraLabel5.TabIndex = 4;
            this.ultraLabel5.Text = "Website   ";
            // 
            // ultraLabel4
            // 
            appearance86.BackColor = System.Drawing.Color.Transparent;
            appearance86.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance86;
            this.ultraLabel4.Location = new System.Drawing.Point(5, 74);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(79, 22);
            this.ultraLabel4.TabIndex = 3;
            this.ultraLabel4.Text = "ĐT nhà riêng   ";
            // 
            // ultraLabel3
            // 
            appearance87.BackColor = System.Drawing.Color.Transparent;
            appearance87.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance87;
            this.ultraLabel3.Location = new System.Drawing.Point(5, 51);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(79, 22);
            this.ultraLabel3.TabIndex = 2;
            this.ultraLabel3.Text = "ĐT cơ quan  ";
            // 
            // ultraLabel2
            // 
            appearance88.BackColor = System.Drawing.Color.Transparent;
            appearance88.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance88;
            this.ultraLabel2.Location = new System.Drawing.Point(5, 28);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(79, 22);
            this.ultraLabel2.TabIndex = 1;
            this.ultraLabel2.Text = "Vị trí công tác ";
            // 
            // lbl1
            // 
            appearance89.TextVAlignAsString = "Middle";
            this.lbl1.Appearance = appearance89;
            this.lbl1.Location = new System.Drawing.Point(5, 5);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(79, 22);
            this.lbl1.TabIndex = 0;
            this.lbl1.Text = "Họ và tên ";
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.uGrid0);
            this.ultraTabPageControl2.Controls.Add(this.ultraPanel1);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(1, 22);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(701, 252);
            // 
            // uGrid0
            // 
            this.uGrid0.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGrid0.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGrid0.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid0.Location = new System.Drawing.Point(0, 0);
            this.uGrid0.Name = "uGrid0";
            this.uGrid0.Size = new System.Drawing.Size(701, 218);
            this.uGrid0.TabIndex = 0;
            this.uGrid0.Text = "ultraGrid1";
            this.uGrid0.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.uGrid0_AfterSelectChange);
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.btnAddRole);
            this.ultraPanel1.ClientArea.Controls.Add(this.btnRemoveRole);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 218);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(701, 34);
            this.ultraPanel1.TabIndex = 1;
            // 
            // btnAddRole
            // 
            this.btnAddRole.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance90.Image = global::Accounting.Properties.Resources.plus;
            this.btnAddRole.Appearance = appearance90;
            this.btnAddRole.Enabled = false;
            this.btnAddRole.ImageSize = new System.Drawing.Size(14, 14);
            this.btnAddRole.Location = new System.Drawing.Point(503, 6);
            this.btnAddRole.Name = "btnAddRole";
            this.btnAddRole.Size = new System.Drawing.Size(97, 23);
            this.btnAddRole.TabIndex = 1;
            this.btnAddRole.Text = "Thêm vai trò";
            this.btnAddRole.Click += new System.EventHandler(this.btnAddRole_Click);
            // 
            // btnRemoveRole
            // 
            this.btnRemoveRole.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance91.Image = global::Accounting.Properties.Resources._1437136435_14;
            this.btnRemoveRole.Appearance = appearance91;
            this.btnRemoveRole.Enabled = false;
            this.btnRemoveRole.ImageSize = new System.Drawing.Size(14, 14);
            this.btnRemoveRole.Location = new System.Drawing.Point(615, 6);
            this.btnRemoveRole.Name = "btnRemoveRole";
            this.btnRemoveRole.Size = new System.Drawing.Size(75, 23);
            this.btnRemoveRole.TabIndex = 0;
            this.btnRemoveRole.Text = "Loại bỏ";
            this.btnRemoveRole.Click += new System.EventHandler(this.btnRemoveRole_Click);
            // 
            // uGrid
            // 
            this.uGrid.ContextMenuStrip = this.cms4Grid;
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.uGrid.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGrid.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGrid.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGrid.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.uGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid.Location = new System.Drawing.Point(0, 52);
            this.uGrid.Name = "uGrid";
            this.uGrid.Size = new System.Drawing.Size(703, 170);
            this.uGrid.TabIndex = 39;
            this.uGrid.Text = "uGrid";
            this.uGrid.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.uGrid_AfterSelectChange);
            this.uGrid.DoubleClick += new System.EventHandler(this.uGrid_DoubleClick);
            // 
            // cms4Grid
            // 
            this.cms4Grid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.tsmAdd,
            this.tsmEdit,
            this.tsmDelete,
            this.tmsReLoad});
            this.cms4Grid.Name = "cms4Grid";
            this.cms4Grid.Size = new System.Drawing.Size(149, 98);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(145, 6);
            // 
            // tsmAdd
            // 
            this.tsmAdd.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.tsmAdd.Name = "tsmAdd";
            this.tsmAdd.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.tsmAdd.Size = new System.Drawing.Size(148, 22);
            this.tsmAdd.Text = "Thêm";
            this.tsmAdd.Click += new System.EventHandler(this.tsmAdd_Click);
            // 
            // tsmEdit
            // 
            this.tsmEdit.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            this.tsmEdit.Name = "tsmEdit";
            this.tsmEdit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.tsmEdit.Size = new System.Drawing.Size(148, 22);
            this.tsmEdit.Text = "Sửa";
            this.tsmEdit.Click += new System.EventHandler(this.tsmEdit_Click);
            // 
            // tsmDelete
            // 
            this.tsmDelete.Image = global::Accounting.Properties.Resources.xoa1_xoa_du_lieu_ke_toan_khoi_database_;
            this.tsmDelete.Name = "tsmDelete";
            this.tsmDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.tsmDelete.Size = new System.Drawing.Size(148, 22);
            this.tsmDelete.Text = "Xóa";
            this.tsmDelete.Click += new System.EventHandler(this.tsmDelete_Click);
            // 
            // tmsReLoad
            // 
            this.tmsReLoad.Image = global::Accounting.Properties.Resources.ubtnReset;
            this.tmsReLoad.Name = "tmsReLoad";
            this.tmsReLoad.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.tmsReLoad.Size = new System.Drawing.Size(148, 22);
            this.tmsReLoad.Text = "Tải Lại";
            this.tmsReLoad.Click += new System.EventHandler(this.tmsReLoad_Click);
            // 
            // ultraSplitter1
            // 
            this.ultraSplitter1.BackColor = System.Drawing.SystemColors.Control;
            this.ultraSplitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraSplitter1.Location = new System.Drawing.Point(0, 222);
            this.ultraSplitter1.Name = "ultraSplitter1";
            this.ultraSplitter1.RestoreExtent = 178;
            this.ultraSplitter1.Size = new System.Drawing.Size(703, 10);
            this.ultraSplitter1.TabIndex = 41;
            // 
            // ultraTabControl1
            // 
            this.ultraTabControl1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl2);
            this.ultraTabControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraTabControl1.Location = new System.Drawing.Point(0, 232);
            this.ultraTabControl1.Name = "ultraTabControl1";
            this.ultraTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.ultraTabControl1.Size = new System.Drawing.Size(703, 275);
            this.ultraTabControl1.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Office2007Ribbon;
            this.ultraTabControl1.TabIndex = 40;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "1. Thông tin chung";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "2. Vai trò";
            this.ultraTabControl1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            this.ultraTabControl1.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007;
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(701, 252);
            // 
            // btnAdd
            // 
            appearance92.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.btnAdd.Appearance = appearance92;
            this.btnAdd.Location = new System.Drawing.Point(10, 12);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 30);
            this.btnAdd.TabIndex = 2;
            this.btnAdd.Text = "Thêm";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnDelete
            // 
            appearance93.Image = global::Accounting.Properties.Resources.xoa1_xoa_du_lieu_ke_toan_khoi_database_;
            this.btnDelete.Appearance = appearance93;
            this.btnDelete.Location = new System.Drawing.Point(172, 12);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 30);
            this.btnDelete.TabIndex = 1;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnEdit
            // 
            appearance94.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            this.btnEdit.Appearance = appearance94;
            this.btnEdit.Location = new System.Drawing.Point(91, 12);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(75, 30);
            this.btnEdit.TabIndex = 0;
            this.btnEdit.Text = "Sửa";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnAdd);
            this.panel1.Controls.Add(this.btnDelete);
            this.panel1.Controls.Add(this.btnEdit);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(703, 52);
            this.panel1.TabIndex = 38;
            // 
            // FCatSysUsers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(703, 507);
            this.Controls.Add(this.uGrid);
            this.Controls.Add(this.ultraSplitter1);
            this.Controls.Add(this.ultraTabControl1);
            this.Controls.Add(this.panel1);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FCatSysUsers";
            this.Text = "Quản lý người dùng";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FCatSysUsers_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FCatSysUsers_FormClosed);
            this.ultraTabPageControl1.ResumeLayout(false);
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid0)).EndInit();
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            this.cms4Grid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).EndInit();
            this.ultraTabControl1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
        private Infragistics.Win.Misc.UltraSplitter ultraSplitter1;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.Misc.UltraLabel txtWebsite;
        private Infragistics.Win.Misc.UltraLabel txtHomePhone;
        private Infragistics.Win.Misc.UltraLabel txtWorkPhone;
        private Infragistics.Win.Misc.UltraLabel txtJob;
        private Infragistics.Win.Misc.UltraLabel txtFullName;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel lbl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid0;
        private System.Windows.Forms.ToolStripMenuItem tmsReLoad;
        private System.Windows.Forms.ToolStripMenuItem tsmDelete;
        private System.Windows.Forms.ToolStripMenuItem tsmEdit;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ContextMenuStrip cms4Grid;
        private Infragistics.Win.Misc.UltraButton btnAdd;
        private Infragistics.Win.Misc.UltraButton btnDelete;
        private Infragistics.Win.Misc.UltraButton btnEdit;
        private System.Windows.Forms.Panel panel1;
        private Infragistics.Win.Misc.UltraLabel txtEmail;
        private Infragistics.Win.Misc.UltraLabel ultraLabel14;
        private Infragistics.Win.Misc.UltraLabel txtMobilePhone;
        private Infragistics.Win.Misc.UltraLabel txtFax;
        private Infragistics.Win.Misc.UltraLabel txtDescription;
        private Infragistics.Win.Misc.UltraLabel txtAddress;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraPictureBox pixPhoto;
        private Infragistics.Win.Misc.UltraLabel txtProvince;
        private Infragistics.Win.Misc.UltraLabel txtCountry;
        private Infragistics.Win.Misc.UltraLabel ultraLabel13;
        private Infragistics.Win.Misc.UltraLabel ultraLabel15;
        private Infragistics.Win.Misc.UltraLabel txtIDCard;
        private Infragistics.Win.Misc.UltraLabel txtBirthDay;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
        private Infragistics.Win.Misc.UltraLabel ultraLabel12;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraButton btnAddRole;
        private Infragistics.Win.Misc.UltraButton btnRemoveRole;
        private Infragistics.Win.Misc.UltraLabel ultraLabel27;
        private Infragistics.Win.Misc.UltraLabel ultraLabel26;
        private Infragistics.Win.Misc.UltraLabel ultraLabel25;
        private Infragistics.Win.Misc.UltraLabel ultraLabel24;
        private Infragistics.Win.Misc.UltraLabel ultraLabel23;
        private Infragistics.Win.Misc.UltraLabel ultraLabel22;
        private Infragistics.Win.Misc.UltraLabel ultraLabel21;
        private Infragistics.Win.Misc.UltraLabel ultraLabel20;
        private Infragistics.Win.Misc.UltraLabel ultraLabel19;
        private Infragistics.Win.Misc.UltraLabel ultraLabel18;
        private Infragistics.Win.Misc.UltraLabel ultraLabel17;
        private Infragistics.Win.Misc.UltraLabel ultraLabel16;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
    }
}