﻿using Accounting.Core.Domain;
using Accounting.TextMessage;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting
{
    public partial class FSysRole : DialogForm
    {
        private bool isAdd = true;
        public FSysRole(SysRole role = null)
        {
            InitializeComponent();
            if (role != null)
            {
                isAdd = false;
                Text = "Sửa vai trò";
                txtRoleCode.Enabled = false;
                txtRoleCode.Value = role.RoleCode;
                txtRoleName.Value = role.RoleName;
                txtDesxription.Value = role.Description;
            }
            else
                Text = "Thêm mới vai trò";
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            var role = new SysRole();
            if (!isAdd)
                role = Utils.ISysRoleService.Query.FirstOrDefault(r => r.RoleCode == txtRoleCode.Text);
            role.RoleCode = txtRoleCode.Text.Trim();
            role.RoleName = txtRoleName.Text.Trim();
            role.Description = txtDesxription.Text.Trim();

            if (role.RoleCode == null || role.RoleCode == string.Empty)
            {
                MSG.Warning(string.Format("Mã vai trò không được để trống.\r\nVui lòng nhập lại."));
                txtRoleCode.Focus();
                return;
            }

            if (role.RoleName == null || role.RoleName == string.Empty)
            {
                MSG.Warning(string.Format("Tên vai trò không được để trống.\r\nVui lòng nhập lại."));
                txtRoleName.Focus();
                return;
            }

            Utils.ISysRoleService.BeginTran();
            try
            {
                if (isAdd)
                {
                    var temp = Utils.ISysRoleService.Query.FirstOrDefault(r => r.RoleCode == txtRoleCode.Text);
                    Utils.ISysRoleService.UnbindSession(temp);
                    temp = Utils.ISysRoleService.Query.FirstOrDefault(r => r.RoleCode == txtRoleCode.Text);
                    if (temp == null)
                        Utils.ISysRoleService.CreateNew(role);
                    else
                    {
                        MSG.Warning(string.Format("Mã vai trò [{0}] đã tồn tại.\r\nVui lòng thay đổi mã vai trò khác."));
                        txtRoleCode.Focus();
                        return;
                    }
                }
                else
                    Utils.ISysRoleService.Update(role);
                Utils.ISysRoleService.CommitChanges();
                Utils.ISysRoleService.CommitTran();
                Utils.ClearCacheByType<SysRole>();
                DialogResult = System.Windows.Forms.DialogResult.OK;
                Close();
            }
            catch (Exception)
            {
                Utils.ISysRoleService.RolbackTran();
                MSG.Warning("Có lỗi xảy ra khi lưu vai trò!");
            }
        }
    }
}
