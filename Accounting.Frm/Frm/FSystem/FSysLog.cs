﻿using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Accounting
{
    public partial class FSysLog : CustormForm
    {
        private bool _notevent = false;
        public FSysLog()
        {
            InitializeComponent();
            txtLine.Value = 100;
            dteFrom.Value = Utils.GetApplicationStartDate().StringToDateTime();
            LoadDuLieu(true);
            ReportUtils.ProcessControls(this);
            this.Activate();
            uGrid.Focus();
        }
        private void LoadDuLieu(bool configGrid)
        {
            WaitingFrm.StartWaiting();
            #region Lấy dữ liệu từ CSDL
            DateTime toDate = dteTo.DateTime.AddDays(1);
            List<SysLog> list1 = Utils.ISysLogService.Query.Where(s => s.Time >= dteFrom.DateTime && s.Time <= toDate).OrderByDescending(s => s.Time).Take(Convert.ToInt32(txtLine.Value)).ToList();
            Utils.ISysLogService.UnbindSession(list1);
            //list1 = Utils.ISysLogService.Query.Where(s => s.Time >= dteFrom.DateTime && s.Time <= toDate).OrderByDescending(s => s.Time).Take(Convert.ToInt32(txtLine.Value)).ToList();
            DateTime dateTime1 = DateTime.Now.Date;
            DateTime dateTime2 = DateTime.Now.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
            list1 = Utils.ISysLogService.Query.Where(s => s.Time >= dateTime1 && s.Time <= dateTime2).OrderByDescending(s => s.Time).ToList();
            uGrid.DataSource = list1;
            #endregion

            if (uGrid.Selected.Rows.Count == 0)
            {
                if (uGrid.Rows.Count > 0)
                    uGrid.Rows[0].Selected = true;
            }
            if (configGrid) ConfigGrid(uGrid);
            WaitingFrm.StopWaiting();
        }

        #region Utils
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            utralGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            Utils.ConfigGrid(utralGrid, TextMessage.ConstDatabase.SysLog_TableName);
            foreach (var column in utralGrid.DisplayLayout.Bands[0].Columns)
            {
                column.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            }
            utralGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.ExtendedAutoDrag;
            utralGrid.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
        }
        #endregion

        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            LoadDuLieu(false);
        }

        private void tmsExport_Click(object sender, EventArgs e)
        {
            try
            {
                List<SysLog> overview = uGrid.DataSource as List<SysLog>;
                //create the serialiser to create the xml
                XmlSerializer serialiser = new XmlSerializer(typeof(List<SysLog>));

                SaveFileDialog saveFileDialog = new SaveFileDialog();

                saveFileDialog.Filter = "XML Files (*.xml)|*.xml";
                saveFileDialog.FilterIndex = 0;
                saveFileDialog.RestoreDirectory = true;
                string path = "";
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    path = saveFileDialog.FileName.CloneObject();
                    saveFileDialog.Dispose();
                }
                if (string.IsNullOrEmpty(path)) return;
                TextWriter Filestream = new StreamWriter(path);
                //write to the file
                serialiser.Serialize(Filestream, overview);
                // Close the file
                Filestream.Close();
                MSG.MessageBoxStand("Kết xuất nhật ký truy cập thành công!");
            }
            catch (Exception)
            {
                MSG.Warning("Lỗi kết xuất nhật ký truy cập!");
            }

        }

        private void tmsDelete_Click(object sender, EventArgs e)
        {
            bool a = uGrid.Selected.Rows.Count > 0;
            bool b = uGrid.ActiveRow != null;
            if (a || b)
            {
                SelectedRowsCollection lst = uGrid.Selected.Rows;
                if (!a) { lst.Add(uGrid.ActiveRow); }
                if (lst.Count > 0)
                {
                    try
                    {
                        Utils.ISysLogService.BeginTran();
                        foreach (var row in lst)
                        {
                            var temp = row.ListObject as SysLog;
                            if (temp != null)
                                Utils.ISysLogService.Delete(temp.ID);
                        }
                        Utils.ISysLogService.CommitTran();
                        LoadDuLieu(false);
                    }
                    catch (Exception)
                    {
                        Utils.ISysLogService.RolbackTran();
                    }
                }
            }
        }

        private void cms4Grid_Opening(object sender, CancelEventArgs e)
        {
            tmsDelete.Visible = Authenticate.Roles("ADMIN");
            tmsDelete.Enabled = uGrid.Selected.Rows.Count > 0;
        }

        private void cms4Grid_Closing(object sender, ToolStripDropDownClosingEventArgs e)
        {
            if (popPanel.IsDisplayed)
                e.Cancel = true;
        }

        private void popPanel_Closed(object sender, EventArgs e)
        {
            if (!_notevent)
            {
                if (!cms4Grid.IsDisposed)
                {
                    cms4Grid.AutoClose = true;
                    cms4Grid.Close();
                }
            }
            else
            {
                cms4Grid.AutoClose = true;
                cms4Grid.Refresh();
            }
            _notevent = false;
        }

        private void tmsSearch_MouseHover(object sender, EventArgs e)
        {
            _notevent = true;
            if (popPanel.IsDisplayed)
                popPanel.Close();
        }

        private void tmsDelete_MouseHover(object sender, EventArgs e)
        {
            _notevent = true;
            if (popPanel.IsDisplayed)
                popPanel.Close();
        }

        private void tmsExport_MouseHover(object sender, EventArgs e)
        {
            _notevent = true;
            if (popPanel.IsDisplayed)
                popPanel.Close();
        }

        private void tmsGetData_Paint(object sender, PaintEventArgs e)
        {
            //cms4Grid.AutoClose = false;
            if (!popPanel.IsDisplayed)
            {
                popPanel.Show(cms4Grid, tmsReLoad.DropDown.Location);
                tmsReLoad.DropDown.Visible = false;
            }
        }

        private void btnApplyGetData_Click(object sender, EventArgs e)
        {
            popPanel.Close();
            cms4Grid.Hide();
            LoadDuLieu(false);
        }

        private void tmsSelectAll_Click(object sender, EventArgs e)
        {
            foreach (var row in uGrid.Rows)
            {
                row.Selected = true;
            }
        }

        private void tmsImport_Click(object sender, EventArgs e)
        {
            try
            {
                var fileDialog = new OpenFileDialog();
                fileDialog.Title = "Chọn tệp nhật ký truy cập";
                fileDialog.Filter = @"XML Files (*.xml)|*.xml";
                if (fileDialog.ShowDialog() == DialogResult.OK)
                {
                    System.Xml.Serialization.XmlSerializer reader =
                        new System.Xml.Serialization.XmlSerializer(typeof(List<SysLog>));
                    System.IO.StreamReader file = new
                       System.IO.StreamReader(fileDialog.FileName);
                    List<SysLog> overview = new List<SysLog>();
                    try
                    {
                        overview = (List<SysLog>)reader.Deserialize(file);
                    }
                    catch (Exception)
                    {
                        MSG.Warning("Lỗi định dạng tệp nhật ký.");
                        file.Close();
                        return;
                    }
                    file.Close();
                    new FViewSysLog(overview).ShowDialog(this);
                }
            }
            catch (Exception)
            {
                MSG.Warning("Không thể mở tệp dữ liệu!");
            }
        }
        /// <summary>
        /// Đăng ký Shortcut Keys cho Form
        /// </summary>
        /// <param name="message"></param>
        /// <param name="keys"></param>
        /// <returns></returns>
        protected override bool ProcessCmdKey(ref Message message, Keys keys)
        {
            //switch (keys)
            //{
            //    case Keys.Control | Keys.O:
            //        {
            //            tmsImport_Click(tmsImport, null);
            //            return true;
            //        }
            //    case Keys.Control | Keys.S:
            //        {
            //            tmsExport_Click(tmsExport, null);
            //            return true;
            //        }
            //    case Keys.Control | Keys.D:
            //        {//xóa
            //            tmsDelete_Click(tmsDelete, null);
            //            return true;
            //        }
            //    case Keys.Control | Keys.A:
            //        {//Chọn hết các trường
            //            tmsSelectAll_Click(tmsSelectAll, null);
            //            return true;
            //        }
            //    case Keys.F5:
            //        {//Làm mới
            //            tmsReLoad_Click(tmsReLoad, null);
            //            return true;
            //        }
            //}
            return base.ProcessCmdKey(ref message, keys);
        }

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (e.Row.ListObject.HasProperty("TypeID"))
            {
                if (!e.Row.ListObject.GetProperty("TypeID").IsNullOrEmpty())
                {
                    if (e.Row.ListObject.GetProperty("TypeID").ToInt()!=0)
                    {
                        int typeid = e.Row.ListObject.GetProperty("TypeID").ToInt();

                        if (e.Row.ListObject.HasProperty("RefID"))
                        {
                            if (!e.Row.ListObject.GetProperty("RefID").IsNullOrEmpty())
                            {
                                Guid id = (Guid)e.Row.ListObject.GetProperty("RefID");
                                var f = Utils.ViewVoucherSelected1(id, typeid);

                            }
                        }
                    }
                }

            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            //string a = dtBeginDate.DateTime.tos
            DateTime dateTimea = dtBeginDate.DateTime.Date;
            DateTime dateTimeb = dtEndDate.DateTime.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
            List<SysLog> list2 = Utils.ISysLogService.Query.Where(s => s.Time >= dateTimea && s.Time <= dateTimeb).OrderByDescending(s => s.Time).ToList();
            uGrid.DataSource = list2;
        }

        private void FSysLog_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGrid.ResetText();
            uGrid.ResetUpdateMode();
            uGrid.ResetExitEditModeOnLeave();
            uGrid.ResetRowUpdateCancelAction();
            uGrid.DataSource = null;
            uGrid.Layouts.Clear();
            uGrid.ResetLayouts();
            uGrid.ResetDisplayLayout();
            uGrid.Refresh();
            uGrid.ClearUndoHistory();
            uGrid.ClearXsdConstraints();
        }

        private void FSysLog_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
