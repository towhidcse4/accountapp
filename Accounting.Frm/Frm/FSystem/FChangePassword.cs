﻿using Accounting.Core.Domain;
using Accounting.TextMessage;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting
{
    public partial class FChangePassword : DialogForm
    {
        public static bool isClose = true;
        private SysUser sysUser;
        public FChangePassword(SysUser user)
        {
            InitializeComponent();
            sysUser = Utils.ISysUserService.Getbykey(user.userid);
            Utils.ISysUserService.UnbindSession(sysUser);
            sysUser = Utils.ISysUserService.Getbykey(user.userid);
            ConfigControl();
            WaitingFrm.StopWaiting();
        }

        private void ConfigControl()
        {
            txtPassword.Value = sysUser.password;
            txtRepassword.Value = sysUser.password;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtPassword.Text.Trim()))
            {
                MSG.Warning("Mật khẩu không được để trống!");
                txtPassword.Focus();
                return;
            }
            if (string.IsNullOrEmpty(txtRepassword.Text.Trim()))
            {
                MSG.Warning("Hãy xác nhận lại mật khẩu!");
                txtRepassword.Focus();
                return;
            }
            if (txtRepassword.Text.Trim() != txtPassword.Text.Trim())
            {
                MSG.Warning("Xác nhận lại mật khẩu không chính xác!");
                txtRepassword.Focus();
                return;
            }
            if (MSG.MessageBoxStand("Bạn có thực sự muốn lưu thay đổi này không?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                sysUser = Utils.ISysUserService.Getbykey(sysUser.userid);
                Utils.ISysUserService.UnbindSession(sysUser);
                sysUser = Utils.ISysUserService.Getbykey(sysUser.userid);
                if (txtPassword.Value.ToString() == sysUser.password) { }
                else
                    sysUser.password = FX.Utils.Encryption.StringToMD5Hash(txtPassword.Value.ToString());
                Utils.ISysUserService.BeginTran();
                try
                {
                    Utils.ISysUserService.Update(sysUser);
                    Utils.ISysUserService.CommitTran();
                    Logs.Save(0, resSystem.Logs_Title_00, Logs.Action.Edit, sysUser.username, sysUser.userid, "Thay đổi mật khẩu đăng nhập");
                }
                catch (Exception ex)
                {
                    Utils.ISysUserService.RolbackTran();
                    MSG.Warning("Lỗi thay đổi mật khẩu! \r\nVui lòng thực hiện lại.");
                    return;
                }
                isClose = false;
                Close();
            }
        }

    }
}
