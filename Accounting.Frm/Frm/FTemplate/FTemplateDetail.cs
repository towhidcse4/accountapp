﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinTabControl;
using ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle;

namespace Accounting
{
    public partial class FTemplateDetail : CustormForm
    {
        #region khai báo
        public ITemplateService ItemplateService { get { return IoC.Resolve<ITemplateService>(); } }
        public ITemplateDetailService ItemplateDetailService { get { return IoC.Resolve<ITemplateDetailService>(); } }
        public ITemplateColumnService ItemplateColumnService { get { return IoC.Resolve<ITemplateColumnService>(); } }
        private int _typeId;

        public Template Template
        {
            get { return _template; }
        }

        public int Status
        {
            get
            {
                if (_isAdd)
                    return ConstFrm.optStatusForm.Add;
                else
                    return ConstFrm.optStatusForm.Edit;
            }
        }

        readonly Template _inputDefault;
        Template _input;
        private bool _isAdd;
        private Template _template;

        #endregion

        #region khởi tạo
        public FTemplateDetail(Template input, Template inputDefault, bool isAdd = true)
        {
            InitializeComponent();

            _input = input == null ? new Template() : (Template)Utils.CloneObject(input);
            _inputDefault = inputDefault == null ? new Template() : (Template)Utils.CloneObject(inputDefault);
            _isAdd = isAdd;
            _input.IsDefault = _isAdd ? false : _input.IsDefault;
            _input.IsSecurity = _isAdd ? false : _input.IsSecurity;
            txtNameOfTemplate.Text = _isAdd ? string.Empty : _input.TemplateName;
            txtNameOfTemplate.Enabled = !_input.IsSecurity;
            if (input != null)
            {
                _typeId = input.TypeID;
                InitializeForm(input);
            }
        }

        private void InitializeForm(Template input)
        {
            //config tab hiển thị tùy theo loại chứng từ (tabHachToan, tabThue, tabThongKe, tabChungTuChiPhi)
            //Template mauGiaoDien = Utils.GetMauGiaoDien(input.TypeID, input.ID, Utils.DsTemplate);
            #region Cấu hình TAB Giao diện
            ConfigGridView(input.TypeID, input);
            ConfigGridFirstView(input.TypeID, input);
            #endregion

            #region Cấu hình TAB Phần đầu
            ConfigGridFirst(input);
            #endregion

            #region Cấu hình TAB Chi tiết

            ConfigGridDetail(input);

            #endregion
        }

        #endregion

        #region Nghiệp vụ
        private void BtnHelpClick(object sender, EventArgs e)
        {//Bật trợ giúp
            //kiểm tra input với uGrid

        }

        private void BtnTemplateClick(object sender, EventArgs e)
        {
            //lấy mẫu ngầm định
            //Template backupTemplate = (Template)Utils.CloneObject(_input);
            //_input.TemplateDetails = _inputDefault.TemplateDetails;
            //IList<TemplateDetail> templateDetails = _input.TemplateDetails;
            //int countDetailsDefault = _inputDefault.TemplateDetails.Count;
            //int countDetails = _input.TemplateDetails.Count;
            foreach (TemplateDetail templateDetail in _input.TemplateDetails)
            {
                int tabIndex = templateDetail.TabIndex;
                int index =
                    _inputDefault.TemplateDetails.IndexOf(
                        _inputDefault.TemplateDetails.FirstOrDefault(p => p.TabIndex == tabIndex));
                templateDetail.GridType = _inputDefault.TemplateDetails[index].GridType;
                templateDetail.TabCaption = _inputDefault.TemplateDetails[index].TabCaption;
                templateDetail.TabIndex = _inputDefault.TemplateDetails[index].TabIndex;
                int countColumns = templateDetail.TemplateColumns.Count;
                int countColumnsDefault = _inputDefault.TemplateDetails[index].TemplateColumns.Count;
                for (int i = 0; i < countColumnsDefault; i++)
                {
                    var columnName = (string)templateDetail.TemplateColumns[i].ColumnName.Trim();
                    var newTemplateColumn = _inputDefault.TemplateDetails[index].TemplateColumns.FirstOrDefault(t => t.ColumnName.Trim() == columnName);
                    if (newTemplateColumn == null) continue;
                    //if (i >= countColumns)
                    //{
                    //    templateDetail.TemplateColumns.Add(new TemplateColumn());
                    //    templateDetail.TemplateColumns[i].ID = Guid.NewGuid();
                    //    templateDetail.TemplateColumns[i].TemplateDetailID = templateDetail.ID;
                    //}
                    PropertyInfo[] propInfo = templateDetail.TemplateColumns[i].GetType().GetProperties();
                    foreach (PropertyInfo info in propInfo)
                    {
                        if (info != null && info.CanWrite)
                        {
                            if (!info.Name.Equals("ID") && !info.Name.Equals("TemplateDetailID"))
                            {
                                PropertyInfo propInfoDefault = newTemplateColumn.GetType().GetProperty(info.Name);
                                if (propInfoDefault != null && propInfoDefault.CanWrite)
                                {
                                    info.SetValue(templateDetail.TemplateColumns[i],
                                                  propInfoDefault.GetValue(newTemplateColumn, null), null);
                                }
                            }
                        }
                    }
                    //_input.TemplateDetails[i].TemplateColumns[j].ColumnCaption =
                    //    _inputDefault.TemplateDetails[i].TemplateColumns[j].ColumnCaption;
                    //_input.TemplateDetails[i].TemplateColumns[j].ColumnMaxWidth =
                    //    _inputDefault.TemplateDetails[i].TemplateColumns[j].ColumnMaxWidth;
                    //_input.TemplateDetails[i].TemplateColumns[j].ColumnMinWidth =
                    //    _inputDefault.TemplateDetails[i].TemplateColumns[j].ColumnMinWidth;
                    //_input.TemplateDetails[i].TemplateColumns[j].ColumnName =
                    //    _inputDefault.TemplateDetails[i].TemplateColumns[j].ColumnName;
                    //_input.TemplateDetails[i].TemplateColumns[j].ColumnToolTip =
                    //    _inputDefault.TemplateDetails[i].TemplateColumns[j].ColumnToolTip;
                    //_input.TemplateDetails[i].TemplateColumns[j].ColumnWidth =
                    //    _inputDefault.TemplateDetails[i].TemplateColumns[j].ColumnWidth;
                    //_input.TemplateDetails[i].TemplateColumns[j].IsColumnHeader =
                    //    _inputDefault.TemplateDetails[i].TemplateColumns[j].IsColumnHeader;
                    //_input.TemplateDetails[i].TemplateColumns[j].IsReadOnly =
                    //    _inputDefault.TemplateDetails[i].TemplateColumns[j].IsReadOnly;
                    //_input.TemplateDetails[i].TemplateColumns[j].IsVisible=_inputDefault.TemplateDetails[i].TemplateColumns[j]
                }
            }
            InitializeForm(_input);
        }

        private void BtnSaveClick(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtNameOfTemplate.Text.Trim()))
            {
                MSG.Warning(resSystem.MSG_System_10);
                return;
            }
            Template template = new Template();
            if (_isAdd)
            {
                template = _input.CloneObject();
                template.ID = Guid.NewGuid();
                template.IsDefault = false;
                template.IsSecurity = false;
                template.IsActive = true;
            }
            else
                template = ItemplateService.Getbykey(_input.ID);
            template.TemplateName = txtNameOfTemplate.Text;
            foreach (var templateDetail in template.TemplateDetails)
            {
                if (_isAdd)
                {
                    templateDetail.ID = Guid.NewGuid();
                }
                templateDetail.TemplateID = template.ID;
                var templateColumns = new List<TemplateColumn>();
                if (templateDetail.TabIndex == 100)
                {
                    uGridFirst.UpdateData();
                    templateColumns = ((BindingList<TemplateColumn>)uGridFirst.DataSource).ToList();
                }
                else
                {
                    var uGrid = (UltraGrid)Controls.Find("uGridTempleteDetail" + templateDetail.TabIndex, true).FirstOrDefault();
                    if (uGrid != null)
                    {
                        uGrid.UpdateData();
                        templateColumns = ((BindingList<TemplateColumn>)uGrid.DataSource).ToList();
                    }
                }
                if (_isAdd)
                    templateDetail.TemplateColumns = templateColumns;
                foreach (TemplateColumn templateColumn in templateDetail.TemplateColumns)
                {
                    if (_isAdd)
                    {
                        templateColumn.ID = Guid.NewGuid();
                    }
                    else
                    {
                        int index = templateDetail.TemplateColumns.IndexOf(templateColumn);
                        templateColumn.ID = templateColumns[index].ID;
                        templateColumn.VisiblePosition = templateColumns[index].VisiblePosition;
                        templateColumn.IsColumnHeader = templateColumns[index].IsColumnHeader;
                        templateColumn.IsReadOnly = templateColumns[index].IsReadOnly;
                        templateColumn.IsVisible = templateColumns[index].IsVisible;
                        templateColumn.ColumnCaption = templateColumns[index].ColumnCaption;
                        templateColumn.ColumnWidth = templateColumns[index].ColumnWidth;
                        templateColumn.ColumnToolTip = templateColumns[index].ColumnToolTip;
                        templateColumn.ColumnName = templateColumns[index].ColumnName;
                        templateColumn.ColumnMaxWidth = templateColumns[index].ColumnMaxWidth;
                        templateColumn.ColumnMinWidth = templateColumns[index].ColumnMinWidth;
                    }
                    templateColumn.TemplateDetailID = templateDetail.ID;
                }
                if (templateColumns.Count > templateDetail.TemplateColumns.Count)
                {
                    for (int i = templateDetail.TemplateColumns.Count; i < templateColumns.Count; i++)
                    {
                        templateColumns[i].ID = Guid.NewGuid();
                        templateDetail.TemplateColumns.Add(templateColumns[i]);
                    }
                }
            }
            ItemplateService.BeginTran();
            try
            {
                
                if (_isAdd)
                    ItemplateService.CreateNew(template);
                else
                {
                    _template = template;
                    ItemplateService.Update(template);
                    
                }
                ItemplateService.CommitTran();
                DialogResult = DialogResult.OK;
                if (!_isAdd)
                {
                    //string keyofdsTemplate = string.Format("{0}@{1}", template.TypeID, template.ID);
                    //if (Utils.ListTemplate.Any(p => p.Key.Equals(keyofdsTemplate)))
                    //    Utils.ListTemplate.Remove(keyofdsTemplate);
                    //Utils.ListTemplate.Add(keyofdsTemplate, template);
                    Utils.ListTemplate.Clear();
                }
                this.Close();
            }
            catch (Exception ex)
            {
                ItemplateService.RolbackTran();
                MSG.Warning(resSystem.MSG_System_11);
            }
        }

        private void BtnCloseClick(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
        #endregion

        #region Utils

        void ConfigGridView(int typeId, Template mauGiaoDien)
        {
            palView.ClientArea.Controls.Clear();
            Utils.ConfigGridByTemplete_General(palView, mauGiaoDien);
            this.ConfigGridByTemplete(typeId, mauGiaoDien, false);
        }

        void ConfigGridFirstView(int typeID, Template mauGiaoDien)
        {
            this.ConfigGridCurrencyByTemplate(typeID, null, uGridFirstView, mauGiaoDien);
            uGridFirstView.DisplayLayout.AutoFitStyle = AutoFitStyle.ExtendLastColumn;
        }

        void ConfigGridFirst(Template mauGiaoDien)
        {
            TemplateDetail templateDetail =
                mauGiaoDien.TemplateDetails.FirstOrDefault(p => p.TabIndex.Equals(100));
            if (templateDetail != null)
            {
                uTabMain.Tabs[1].Visible = true;
                ConfigGrid(_input.TemplateDetails.IndexOf(_input.TemplateDetails.FirstOrDefault(p => p.TabIndex == 100)), uGridFirst);
                //uGridFirst.DisplayLayout.Bands[0].Columns["VisiblePosition"].SortIndicator = SortIndicator.Ascending;
                //uGridFirst.DisplayLayout.Bands[0].Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.Edit;
            }
            else uTabMain.Tabs[1].Visible = false;
        }

        void ConfigGridDetail(Template mauGiaoDien)
        {
            UltraTabControl ultraTabControl;
            List<TemplateDetail> tabs =
                mauGiaoDien.TemplateDetails.Where(p => p.TabIndex != 100).OrderBy(p => p.TabIndex).ToList();
            if (tabs.Count > 1)
            {
                //ultraTabControlGrid.Visible = false;
                ultraTabControl = new UltraTabControl();
                ultraTabControl.SuspendLayout();
                foreach (var detail in tabs)
                {
                    var ultraTab = ultraTabControl.Tabs.Add();
                    var uGrid = new UltraGrid { Name = "uGridTempleteDetail" + detail.TabIndex };
                    uGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ExtendLastColumn;
                    uGrid.Dock = DockStyle.Fill;
                    uGrid.Text = @"uGridTempleteDetail" + detail.TabIndex;
                    uGrid.TabIndex = detail.TabIndex;
                    ultraTab.TabPage.Controls.Add(uGrid);
                    ultraTab.Text = detail.TabCaption;
                    ultraTab.Key = "uTab" + detail.TabIndex;
                }

                ultraTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
                ultraTabControl.Name = "ultraTabControlTempleteDetail";
                ultraTabControl.TabIndex = 0;
                ultraTabControl.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Office2007Ribbon;
                ultraTabControl.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007;

                palDetail.ClientArea.Controls.Add(ultraTabControl);
                ((System.ComponentModel.ISupportInitialize)(ultraTabControl)).EndInit();
            }
            else if (tabs.Count == 1)
            {
                var uGrid = new UltraGrid();
                uGrid.Name = "uGridTempleteDetail0";
                uGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ExtendLastColumn;
                uGrid.Dock = DockStyle.Fill;
                uGrid.Text = @"uGrid";
                uGrid.TabIndex = 0;
                palDetail.ClientArea.Controls.Add(uGrid);
            }

            for (int i = 0; i < tabs.Count; i++)
            {
                var uGrid = (UltraGrid)this.Controls.Find("uGridTempleteDetail" + i, true).FirstOrDefault();
                ConfigGrid(_input.TemplateDetails.IndexOf(_input.TemplateDetails.FirstOrDefault(p => p.TabIndex == tabs[i].TabIndex)), uGrid);
            }
        }

        private void ConfigGrid(int index, UltraGrid uGridTemplate)
        {
            BindingList<TemplateColumn> @TemplateColumns = new BindingList<TemplateColumn>();

            //List<TemplateColumn> dsTemp =
            //    (_input.TemplateDetails.ToList().FirstOrDefault(k => k.TabIndex == tabIndex) as TemplateDetail).TemplateColumns.ToList();
            @TemplateColumns = new BindingList<TemplateColumn>(_input.TemplateDetails[index].TemplateColumns);
            uGridTemplate.DataSource = @TemplateColumns;
            Utils.ConfigGrid(uGridTemplate, ConstDatabase.Template_TableName);
            //tắt lọc cột
            uGridTemplate.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.True;
            uGridTemplate.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            //tự thay đổi kích thước cột
            uGridTemplate.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            //tắt tiêu đề
            uGridTemplate.DisplayLayout.CaptionVisible = DefaultableBoolean.False;
            //select cả hàng hay ko?
            //uGridTemplate.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            //uGridTemplate.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            //Hiển thị SupportDataErrorInfo
            uGridTemplate.DisplayLayout.Override.SupportDataErrorInfo = SupportDataErrorInfo.CellsOnly;
            //Hiện những dòng trống?
            uGridTemplate.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            uGridTemplate.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.ExtendFirstCell;
            //Sort
            uGridTemplate.DisplayLayout.Override.HeaderClickAction = HeaderClickAction.SortSingle;
            uGridTemplate.DisplayLayout.Bands[0].Columns["VisiblePosition"].SortIndicator = SortIndicator.Ascending;

            UltraGridBand band = uGridTemplate.DisplayLayout.Bands[0];
            band.Summaries.Clear();
            SummarySettings summary = band.Summaries.Add("Count", SummaryType.Count, band.Columns["VisiblePosition"]);
            summary.DisplayFormat = "Số dòng = {0:N0}";
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed | SummaryDisplayAreas.GroupByRowsFooter;
            uGridTemplate.DisplayLayout.Override.TemplateAddRowPrompt = resSystem.MSG_System_07;
            uGridTemplate.DisplayLayout.Override.SpecialRowSeparator = SpecialRowSeparator.None;
            //Fix Header
            uGridTemplate.DisplayLayout.UseFixedHeaders = false;
            // Turn on all of the Cut, Copy, and Paste functionality. 
            uGridTemplate.DisplayLayout.Override.AllowMultiCellOperations = AllowMultiCellOperation.All;
            uGridTemplate.DisplayLayout.Bands[0].Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.Edit;
            //Khóa 1 số TH đặc biệt
            foreach (var row in uGridTemplate.Rows)
            {
                if (row.Cells["ColumnName"].Value.ToString().Contains("Currency") ||
                    row.Cells["ColumnName"].Value.ToString().Contains("ExchangeRate") ||
                    (row.Cells["ColumnName"].Value.ToString().Contains("Amount") && !row.Cells["ColumnName"].Value.ToString().Contains("Original")) || row.Cells["ColumnName"].Value.ToString().Equals("VATRate"))
                {
                    row.Cells["IsVisible"].Activation = Activation.Disabled;
                    row.Cells["IsVisible"].Appearance.BackColorDisabled = Color.Gainsboro;
                }
                else if (row.Cells["ColumnName"].Value.ToString().Contains("AccountingObjectName") && (bool)row.Cells["IsReadOnly"].Value)
                {
                    row.Cells["IsReadOnly"].Activation = Activation.Disabled;
                    row.Cells["IsReadOnly"].Appearance.BackColorDisabled = Color.Gainsboro;
                }
                else if (row.Cells["ColumnName"].Value.ToString().Equals("RepositoryID"))
                {
                    //Không cho phép sửa phần hiển thị cột [Kho] của các Form Mua hàng
                    if (new int[] { 210, 260, 261, 262, 263, 264, 117, 127, 131, 141, 171 }.Contains(_typeId))
                    {
                        row.Cells["IsVisible"].Activation = Activation.Disabled;
                        row.Cells["IsVisible"].Appearance.BackColorDisabled = Color.Gainsboro;
                    }
                    else if (new int[] { }.Contains(_typeId))
                    {

                    }
                }
                else if (row.Cells["ColumnName"].Value.ToString().Equals("InwardAmountOriginal") || row.Cells["ColumnName"].Value.ToString().Equals("InwardAmount"))
                {
                    //Không cho phép sửa phần hiển thị cột [Giá trị nhập kho] của các Form Mua hàng
                    if (new int[] { 210, 260, 261, 262, 263, 264, 117, 127, 131, 141, 171 }.Contains(_typeId))
                    {
                        row.Cells["IsVisible"].Activation = Activation.Disabled;
                        row.Cells["IsVisible"].Appearance.BackColorDisabled = Color.Gainsboro;
                    }
                    else if (new int[] { }.Contains(_typeId))
                    {

                    }
                }
            }
        }
        #endregion

        public static void ChangeIndexTemplateColum(int current, int next, BindingList<TemplateColumn> templateColumns, UltraGrid uGrid)
        {
            //TH1: di chuyển E từ currentIndex đến newIndex mà currentIndex < newIndex
            //  + lọc các phần tử > currentIndex và <= newIndex
            //  + giảm vị trí các phần tử đi 1
            //  + sét lại vị trí currentIndex = newIndex của E
            //TH2: di chuyển E từ currentIndex đến newIndex mà currentIndex > newIndex
            //  + lọc các phần tử < currentIndex và >= newIndex
            //  + tăng vị trí các phần tử đi 1
            //  + sét lại vị trí currentIndex = newIndex của E
            bool runFlow = current < next; //di chuyển tiến
            if (!runFlow)
            {
                if (next < 0) return;
            }
            else if (next > (uGrid.Rows.Count - 1)) return;
            int currentIndex = ((TemplateColumn)uGrid.Rows[current].ListObject).VisiblePosition;
            int newIndex = ((TemplateColumn)uGrid.Rows[next].ListObject).VisiblePosition;
            var currentTemplateColumn = templateColumns.SingleOrDefault(k => k.VisiblePosition == currentIndex);
            var newTemplateColumn = templateColumns.SingleOrDefault(k => k.VisiblePosition == newIndex);
            templateColumns[templateColumns.IndexOf(currentTemplateColumn)].VisiblePosition = newIndex;
            if (newTemplateColumn != null)
                templateColumns[templateColumns.IndexOf(newTemplateColumn)].VisiblePosition = currentIndex;
            uGrid.DataBind();
            uGrid.DisplayLayout.Bands[0].Columns["VisiblePosition"].SortIndicator = SortIndicator.Ascending;
            uGrid.ResetLayouts();
        }

        private void btnMoveUp_Click(object sender, EventArgs e)
        {
            if (uTabMain.ActiveTab.Index.Equals(1))
            {
                var templateColumns = (BindingList<TemplateColumn>)uGridFirst.DataSource;
                ChangeIndexTemplateColum(uGridFirst.ActiveRow.Index, uGridFirst.ActiveRow.Index - 1, templateColumns, uGridFirst);
            }
            else
            {
                var tabControl =
                           (UltraTabControl)this.Controls.Find("ultraTabControlTempleteDetail", true).FirstOrDefault();
                if (tabControl != null)
                {
                    var uGrid =
                        (UltraGrid)Controls.Find("uGridTempleteDetail" + tabControl.ActiveTab.Index,
                                                                    true).FirstOrDefault();
                    if (uGrid != null)
                    {
                        var dsTemplateColumn = (BindingList<TemplateColumn>)uGrid.DataSource;
                        ChangeIndexTemplateColum(uGrid.ActiveRow.Index, uGrid.ActiveRow.Index - 1, dsTemplateColumn, uGrid);
                    }
                }
                else
                {
                    var uGrid = (UltraGrid)this.Controls.Find("uGridTempleteDetail0", true).FirstOrDefault();
                    if (uGrid != null)
                    {
                        var dsTemplateColumn = (BindingList<TemplateColumn>)uGrid.DataSource;
                        ChangeIndexTemplateColum(uGrid.ActiveRow.Index, uGrid.ActiveRow.Index - 1, dsTemplateColumn, uGrid);
                    }
                }
            }
        }

        private void btnMoveDown_Click(object sender, EventArgs e)
        {
            if (uTabMain.ActiveTab.Index.Equals(1))
            {
                var dsTemplateColumn = (BindingList<TemplateColumn>)uGridFirst.DataSource;
                ChangeIndexTemplateColum(uGridFirst.ActiveRow.Index, uGridFirst.ActiveRow.Index + 1, dsTemplateColumn, uGridFirst);
            }
            else
            {
                var tabControl =
                           (UltraTabControl)this.Controls.Find("ultraTabControlTempleteDetail", true).FirstOrDefault();
                if (tabControl != null)
                {
                    var uGrid =
                        (UltraGrid)
                        tabControl.ActiveTab.TabPage.Controls.Find("uGridTempleteDetail" + tabControl.ActiveTab.Index,
                                                                    true).FirstOrDefault();
                    if (uGrid != null)
                    {
                        var dsTemplateColumn = (BindingList<TemplateColumn>)uGrid.DataSource;
                        ChangeIndexTemplateColum(uGrid.ActiveRow.Index, uGrid.ActiveRow.Index + 1, dsTemplateColumn, uGrid);
                    }
                }
                else
                {
                    var uGrid = (UltraGrid)this.Controls.Find("uGridTempleteDetail0", true).FirstOrDefault();
                    if (uGrid != null)
                    {
                        var dsTemplateColumn = (BindingList<TemplateColumn>)uGrid.DataSource;
                        ChangeIndexTemplateColum(uGrid.ActiveRow.Index, uGrid.ActiveRow.Index + 1, dsTemplateColumn, uGrid);
                    }
                }
            }
        }

        private void ultraTabControl_SelectedTabChanged(object sender, SelectedTabChangedEventArgs e)
        {
            int index = 0;
            if (e.PreviousSelectedTab != null)
                index = e.PreviousSelectedTab.Index;
            if (!index.Equals(0))
            {
                if (index.Equals(2))
                    ConfigGridView(_input.TypeID, _input);
                else
                    ConfigGridFirstView(_input.TypeID, _input);
            }
        }
    }
}
