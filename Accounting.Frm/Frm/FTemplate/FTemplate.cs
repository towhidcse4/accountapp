﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FTemplate : CustormForm
    {
        #region khai báo
        static ITemplateService TemplateService { get { return Utils.ITemplateService; } }
        readonly Template _templateDefault = new Template();
        readonly int _typeID;
        private Guid _templateID;
        public Guid TemplateID { get { return _templateID; } }
        #endregion

        #region khởi tạo
        public FTemplate()
        {
            InitializeComponent();
        }

        public FTemplate(int typeID, Guid? templateID = null)
        {
            InitializeComponent();
            _typeID = typeID;
            //Lấy danh sách các Template của một loại chứng từ
            List<Template> listTemplate = TemplateService.getTemplateinTypeForm(typeID);
            _templateDefault = Utils.CloneObject(listTemplate.FirstOrDefault(k => k.IsDefault == true));
            listTemplate = (List<Template>)Utils.CloneObject(listTemplate.Where(k => k.IsDefault == false).ToList());

            uGrid.DataSource = listTemplate;
            Utils.ConfigGrid(uGrid, "Template", GetTemplateColums("Template"), true, false);
            uGrid.DisplayLayout.Bands[0].Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            foreach (UltraGridRow row in uGrid.Rows)
            {
                if (row.Cells["ID"].Value.Equals(templateID))
                {
                    row.Selected = true;
                    row.Activated = true;
                }
            }
        }
        #endregion

        #region nghiệp vụ
        void EditFunction()
        {
            if (uGrid.Rows.Count > 0)
            {
                Template temp = new Template();
                //if (uGrid.Selected != null)
                //    temp = uGrid.Selected.Rows.Count <= 0 ? uGrid.Rows[0].ListObject as Template : uGrid.Selected.Rows[0].ListObject as Template;
                //else
                temp = uGrid.ActiveRow.ListObject as Template;
                FTemplateDetail FTemplateDetail = new FTemplateDetail(temp, _templateDefault, false);
                FTemplateDetail.FormClosed += new FormClosedEventHandler(FTemplateDetail_FormClosed);
                FTemplateDetail.ShowDialog(this);
            }
        }

        void FTemplateDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            var frm = (FTemplateDetail)sender;
            if (e.CloseReason == CloseReason.UserClosing)
                if (frm.DialogResult == DialogResult.OK)
                {
                    List<Template> listTemplate =
                        TemplateService.Query.Where(k => k.TypeID == _typeID && k.IsDefault == false).ToList();
                    int index = 0;
                    if(frm.Status.Equals(ConstFrm.optStatusForm.Edit))
                    {
                        int current = listTemplate.IndexOf(frm.Template);
                        if (current > -1)
                        {
                            index = current;
                        }
                    }
                    else
                    {
                        if (listTemplate.Count > 0)
                        {
                            var query = from u in listTemplate
                                        join g in (List<Template>) uGrid.DataSource on u.ID equals g.ID into j1
                                        from j2 in j1.DefaultIfEmpty().Where(p => p == null)
                                        group j2 by u into joined
                                        select new {joined};
                            Template temp = query.ToList().FirstOrDefault().joined.Key;
                            if(temp!=null)
                            {
                                int current = listTemplate.IndexOf(temp);
                                if (current > -1)
                                {
                                    index = current;
                                }
                            }
                        }
                    }
                    uGrid.DataSource = listTemplate;
                    uGrid.DataBind();
                    uGrid.Rows[index].Activate();
                    uGrid.Rows[index].Selected = true;
                }
        }

        private void BtnCloseClick(object sender, EventArgs e)
        {//Đóng
            Close();
        }

        private void UGridDoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            EditFunction();
        }

        private void BtnDeleteClick(object sender, EventArgs e)
        {//Xóa
            Template temp = uGrid.Rows.Count <= 0 ? null : uGrid.ActiveRow.ListObject as Template;
            if (temp == null) return;
            Template tmpTemplate = new Template();
            if (temp == null) return;
            tmpTemplate = TemplateService.Getbykey(temp.ID);
            if (tmpTemplate.IsSecurity)
            {
                MSG.Warning(resSystem.MSG_System_13);
                return;
            }
            else
            {
                if (MessageBox.Show(resSystem.MSG_System_14, resSystem.String_Title, MessageBoxButtons.YesNo,
                                MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1) == DialogResult.No) return;
            }
            try
            {
                TemplateService.BeginTran();
                TemplateService.Delete(tmpTemplate);
                TemplateService.CommitTran();

                List<Template> listTemplate =
                    TemplateService.Query.Where(k => k.TypeID == _typeID).ToList();
                listTemplate = (List<Template>)Utils.CloneObject(listTemplate.Where(k => k.IsDefault == false).ToList());

                uGrid.DataSource = listTemplate;
            }
            catch (Exception)
            {
                TemplateService.RolbackTran();
            }
        }

        private void BtnEditClick(object sender, EventArgs e)
        {//Sửa
            EditFunction();
        }

        private void BtnAddClick(object sender, EventArgs e)
        {//Thêm
            //Template temp = uGrid.Selected.Rows.Count <= 0 ? new Template() : uGrid.Selected.Rows[0].ListObject as Template;

            FTemplateDetail FTemplateDetail = new FTemplateDetail(_templateDefault, _templateDefault);
            FTemplateDetail.FormClosed += new FormClosedEventHandler(FTemplateDetail_FormClosed);
            FTemplateDetail.ShowDialog(this);
        }

        private void BtnSaveClick(object sender, EventArgs e)
        {//Áp dụng
            Template temp = uGrid.Rows.Count <= 0 ? null : uGrid.ActiveRow.ListObject as Template;
            if (temp == null) return;
            _templateID = temp.ID;
            this.DialogResult = DialogResult.OK;
    	    this.Close();
        }
        #endregion

        #region Utils
        public static List<TemplateColumn> GetTemplateColums(string select)
        {
            List<string> strColumnToolTip;

            #region TemplateColumn
            List<string> strColumnName = new List<string> { "IsVisibleCbb", "ID", "TemplateDetailID", "ColumnCaption", "IsReadOnly", "ColumnWidth", "ColumnMaxWidth", "ColumnMinWidth", "IsVisible", "VisiblePosition", "ColumnToolTip", "ColumnName", };
            List<string> strColumnCaption = strColumnToolTip = new List<string> { "IsVisibleCbb", "ID", "TemplateDetailID", "Tiêu đề cột", "Chỉ đọc", "Độ rộng cột", "Độ rộng cột tối đa", "Độ rộng cột tối thiểu", "Hiện/Ẩn", "Vị trí xuất hiện", "Ghi chú", "Tên cột", };
            List<bool> bolIsReadOnly = new List<bool>();
            List<bool> bolIsVisible = new List<bool>();
            List<bool> bolIsVisibleCbb = new List<bool>();
            List<int> intColumnWidth = new List<int>();
            List<int> intColumnMaxWidth = new List<int>();
            List<int> intColumnMinWidth = new List<int>();
            List<int> intVisiblePosition = new List<int> { 11, 11, 11, 1, 3, 4, 5, 6, 7, 8, 2, 0 };
            List<int> vTbolIsVisible = new List<int> { 3, 4, 5, 6, 7, 8, 9, 10, 11 };
            List<int> vTbolIsVisibleCbb = new List<int> { 1, 2 };
            for (int i = 0; i < 12; i++)
            {
                bolIsVisible.Add(vTbolIsVisible.Contains(i));
                bolIsVisibleCbb.Add(vTbolIsVisibleCbb.Contains(i));
                bolIsReadOnly.Add(false);
                intColumnWidth.Add(-1);
                intColumnMaxWidth.Add(-1);
                intColumnMinWidth.Add(-1);
                //intVisiblePosition.Add(-1);
            }
            List<TemplateColumn> dsTemplateColumn = ConstDatabase.CreateData(strColumnName, strColumnCaption, strColumnToolTip, bolIsReadOnly, bolIsVisible, bolIsVisibleCbb, intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition).Values.ToList();
            #endregion

            #region TemplateDetail
            strColumnName = new List<string> { "TemplateColumns", "ID", "TemplateID", "GridType", "TabIndex", "TabCaption", };
            strColumnCaption = strColumnToolTip = new List<string> { "TemplateColumns", "ID", "TemplateID", "GridType", "TabIndex", "TabCaption", };
            bolIsReadOnly = new List<bool>();
            bolIsVisible = new List<bool>();
            bolIsVisibleCbb = new List<bool>();
            intColumnWidth = new List<int>();
            intColumnMaxWidth = new List<int>();
            intColumnMinWidth = new List<int>();
            intVisiblePosition = new List<int>();
            vTbolIsVisible = new List<int> { 1, 2 };    //vị trí có giá trị bằng true
            vTbolIsVisibleCbb = new List<int> { 5, 4 };
            for (int i = 0; i < 6; i++)
            {
                bolIsVisible.Add(vTbolIsVisible.Contains(i));
                bolIsVisibleCbb.Add(vTbolIsVisibleCbb.Contains(i));
                bolIsReadOnly.Add(false);
                intColumnWidth.Add(-1);
                intColumnMaxWidth.Add(-1);
                intColumnMinWidth.Add(-1);
                intVisiblePosition.Add(-1);
            }
            List<TemplateColumn> dsTemplateDetail = ConstDatabase.CreateData(strColumnName, strColumnCaption, strColumnToolTip, bolIsReadOnly, bolIsVisible, bolIsVisibleCbb, intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition).Values.ToList();
            #endregion

            #region Template
            strColumnName = new List<string> { "ID", "TemplateName", "TypeID", "IsActive", "IsSecurity", "OrderPriority", "IsDefault", "TemplateDetails", "TemplateDetailsBinding" };
            strColumnCaption = strColumnToolTip = new List<string> { "ID", "Mẫu giao diện", "TypeID", "IsActive", "IsSecurity", "OrderPriority", "IsDefault", "TemplateDetails", "TemplateDetailsBinding" };
            bolIsReadOnly = new List<bool>();
            bolIsVisible = new List<bool>();
            bolIsVisibleCbb = new List<bool>();
            intColumnWidth = new List<int>();
            intColumnMaxWidth = new List<int>();
            intColumnMinWidth = new List<int>();
            intVisiblePosition = new List<int>();
            vTbolIsVisible = new List<int> { 1, };    //vị trí có giá trị bằng true
            vTbolIsVisibleCbb = new List<int> { 1 };
            for (int i = 0; i < 9; i++)
            {
                bolIsVisible.Add(vTbolIsVisible.Contains(i));
                bolIsVisibleCbb.Add(vTbolIsVisibleCbb.Contains(i));
                bolIsReadOnly.Add(false);
                intColumnWidth.Add(-1);
                intColumnMaxWidth.Add(-1);
                intColumnMinWidth.Add(-1);
                intVisiblePosition.Add(-1);
            }
            List<TemplateColumn> dsTemplate = ConstDatabase.CreateData(strColumnName, strColumnCaption, strColumnToolTip, bolIsReadOnly, bolIsVisible, bolIsVisibleCbb, intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition).Values.ToList();
            #endregion

            if (select.Equals("Template")) return dsTemplate;
            return @select.Equals("TemplateDetail") ? dsTemplateDetail : dsTemplateColumn;
        }
        #endregion
    }
}
