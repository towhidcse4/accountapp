﻿namespace Accounting
{
    partial class FTemplateDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            this.pageView = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.palView = new Infragistics.Win.Misc.UltraPanel();
            this.palFisrt = new Infragistics.Win.Misc.UltraPanel();
            this.uGridFirstView = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridFirst = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnFirstUp = new Infragistics.Win.Misc.UltraButton();
            this.btnFirstDown = new Infragistics.Win.Misc.UltraButton();
            this.pageDetail = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.palDetail = new Infragistics.Win.Misc.UltraPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnMoveUp = new Infragistics.Win.Misc.UltraButton();
            this.btnMoveDown = new Infragistics.Win.Misc.UltraButton();
            this.palTop = new System.Windows.Forms.Panel();
            this.txtNameOfTemplate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblMau = new Infragistics.Win.Misc.UltraLabel();
            this.palFill = new System.Windows.Forms.Panel();
            this.uTabMain = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.pageFirst = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.palBottom = new System.Windows.Forms.Panel();
            this.btnHelp = new Infragistics.Win.Misc.UltraButton();
            this.btnTemplate = new Infragistics.Win.Misc.UltraButton();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.ultraTabSharedControlsPage2 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraTabPageControl4 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl5 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGrid2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.pageView.SuspendLayout();
            this.palView.SuspendLayout();
            this.palFisrt.ClientArea.SuspendLayout();
            this.palFisrt.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridFirstView)).BeginInit();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridFirst)).BeginInit();
            this.panel2.SuspendLayout();
            this.pageDetail.SuspendLayout();
            this.palDetail.SuspendLayout();
            this.panel1.SuspendLayout();
            this.palTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNameOfTemplate)).BeginInit();
            this.palFill.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTabMain)).BeginInit();
            this.uTabMain.SuspendLayout();
            this.palBottom.SuspendLayout();
            this.ultraTabPageControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).BeginInit();
            this.ultraTabPageControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid2)).BeginInit();
            this.SuspendLayout();
            // 
            // pageView
            // 
            this.pageView.Controls.Add(this.palView);
            this.pageView.Controls.Add(this.palFisrt);
            this.pageView.Location = new System.Drawing.Point(1, 22);
            this.pageView.Name = "pageView";
            this.pageView.Padding = new System.Windows.Forms.Padding(10);
            this.pageView.Size = new System.Drawing.Size(801, 372);
            // 
            // palView
            // 
            this.palView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palView.Location = new System.Drawing.Point(10, 75);
            this.palView.Name = "palView";
            this.palView.Size = new System.Drawing.Size(781, 287);
            this.palView.TabIndex = 0;
            // 
            // palFisrt
            // 
            appearance1.BackColor = System.Drawing.Color.White;
            appearance1.BackColor2 = System.Drawing.Color.White;
            appearance1.BorderColor3DBase = System.Drawing.SystemColors.ButtonFace;
            this.palFisrt.Appearance = appearance1;
            // 
            // palFisrt.ClientArea
            // 
            this.palFisrt.ClientArea.Controls.Add(this.uGridFirstView);
            this.palFisrt.Dock = System.Windows.Forms.DockStyle.Top;
            this.palFisrt.Location = new System.Drawing.Point(10, 10);
            this.palFisrt.Name = "palFisrt";
            this.palFisrt.Size = new System.Drawing.Size(781, 65);
            this.palFisrt.TabIndex = 0;
            // 
            // uGridFirstView
            // 
            this.uGridFirstView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridFirstView.Location = new System.Drawing.Point(0, 0);
            this.uGridFirstView.Name = "uGridFirstView";
            this.uGridFirstView.Size = new System.Drawing.Size(781, 65);
            this.uGridFirstView.TabIndex = 0;
            this.uGridFirstView.Visible = false;
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.uGridFirst);
            this.ultraTabPageControl1.Controls.Add(this.panel2);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Padding = new System.Windows.Forms.Padding(10);
            this.ultraTabPageControl1.Size = new System.Drawing.Size(809, 383);
            // 
            // uGridFirst
            // 
            this.uGridFirst.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridFirst.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridFirst.Location = new System.Drawing.Point(10, 10);
            this.uGridFirst.Name = "uGridFirst";
            this.uGridFirst.Size = new System.Drawing.Size(752, 363);
            this.uGridFirst.TabIndex = 0;
            this.uGridFirst.Text = "ultraGrid3";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.btnFirstUp);
            this.panel2.Controls.Add(this.btnFirstDown);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(762, 10);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(37, 363);
            this.panel2.TabIndex = 5;
            // 
            // btnFirstUp
            // 
            appearance2.Image = global::Accounting.Properties.Resources.up;
            appearance2.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance2.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.btnFirstUp.Appearance = appearance2;
            this.btnFirstUp.Location = new System.Drawing.Point(6, 15);
            this.btnFirstUp.Name = "btnFirstUp";
            this.btnFirstUp.Size = new System.Drawing.Size(30, 30);
            this.btnFirstUp.TabIndex = 2;
            this.btnFirstUp.Tag = "Up";
            this.btnFirstUp.Click += new System.EventHandler(this.btnMoveUp_Click);
            // 
            // btnFirstDown
            // 
            appearance3.Image = global::Accounting.Properties.Resources.down;
            appearance3.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance3.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.btnFirstDown.Appearance = appearance3;
            this.btnFirstDown.Location = new System.Drawing.Point(6, 51);
            this.btnFirstDown.Name = "btnFirstDown";
            this.btnFirstDown.Size = new System.Drawing.Size(30, 30);
            this.btnFirstDown.TabIndex = 3;
            this.btnFirstDown.Tag = "Down";
            this.btnFirstDown.Click += new System.EventHandler(this.btnMoveDown_Click);
            // 
            // pageDetail
            // 
            this.pageDetail.Controls.Add(this.palDetail);
            this.pageDetail.Controls.Add(this.panel1);
            this.pageDetail.Location = new System.Drawing.Point(-10000, -10000);
            this.pageDetail.Name = "pageDetail";
            this.pageDetail.Padding = new System.Windows.Forms.Padding(10);
            this.pageDetail.Size = new System.Drawing.Size(809, 383);
            // 
            // palDetail
            // 
            this.palDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palDetail.Location = new System.Drawing.Point(10, 10);
            this.palDetail.Name = "palDetail";
            this.palDetail.Size = new System.Drawing.Size(752, 363);
            this.palDetail.TabIndex = 5;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.btnMoveUp);
            this.panel1.Controls.Add(this.btnMoveDown);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(762, 10);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(37, 363);
            this.panel1.TabIndex = 4;
            // 
            // btnMoveUp
            // 
            appearance4.Image = global::Accounting.Properties.Resources.up;
            appearance4.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance4.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.btnMoveUp.Appearance = appearance4;
            this.btnMoveUp.Location = new System.Drawing.Point(6, 15);
            this.btnMoveUp.Name = "btnMoveUp";
            this.btnMoveUp.Size = new System.Drawing.Size(30, 30);
            this.btnMoveUp.TabIndex = 2;
            this.btnMoveUp.Tag = "Up";
            this.btnMoveUp.Click += new System.EventHandler(this.btnMoveUp_Click);
            // 
            // btnMoveDown
            // 
            appearance5.Image = global::Accounting.Properties.Resources.down;
            appearance5.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance5.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.btnMoveDown.Appearance = appearance5;
            this.btnMoveDown.Location = new System.Drawing.Point(6, 51);
            this.btnMoveDown.Name = "btnMoveDown";
            this.btnMoveDown.Size = new System.Drawing.Size(30, 30);
            this.btnMoveDown.TabIndex = 3;
            this.btnMoveDown.Tag = "Down";
            this.btnMoveDown.Click += new System.EventHandler(this.btnMoveDown_Click);
            // 
            // palTop
            // 
            this.palTop.Controls.Add(this.txtNameOfTemplate);
            this.palTop.Controls.Add(this.lblMau);
            this.palTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.palTop.Location = new System.Drawing.Point(10, 10);
            this.palTop.Name = "palTop";
            this.palTop.Size = new System.Drawing.Size(803, 45);
            this.palTop.TabIndex = 0;
            // 
            // txtNameOfTemplate
            // 
            this.txtNameOfTemplate.Location = new System.Drawing.Point(89, 12);
            this.txtNameOfTemplate.Name = "txtNameOfTemplate";
            this.txtNameOfTemplate.Size = new System.Drawing.Size(254, 21);
            this.txtNameOfTemplate.TabIndex = 41;
            // 
            // lblMau
            // 
            this.lblMau.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextHAlignAsString = "Center";
            appearance6.TextVAlignAsString = "Middle";
            this.lblMau.Appearance = appearance6;
            this.lblMau.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblMau.Location = new System.Drawing.Point(11, 12);
            this.lblMau.Name = "lblMau";
            this.lblMau.Size = new System.Drawing.Size(72, 21);
            this.lblMau.TabIndex = 40;
            this.lblMau.Text = "Tên mẫu (*)";
            // 
            // palFill
            // 
            this.palFill.Controls.Add(this.uTabMain);
            this.palFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palFill.Location = new System.Drawing.Point(10, 55);
            this.palFill.Name = "palFill";
            this.palFill.Size = new System.Drawing.Size(803, 395);
            this.palFill.TabIndex = 1;
            // 
            // uTabMain
            // 
            appearance7.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.uTabMain.Appearance = appearance7;
            this.uTabMain.Controls.Add(this.pageFirst);
            this.uTabMain.Controls.Add(this.pageView);
            this.uTabMain.Controls.Add(this.pageDetail);
            this.uTabMain.Controls.Add(this.ultraTabPageControl1);
            this.uTabMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uTabMain.Location = new System.Drawing.Point(0, 0);
            this.uTabMain.Name = "uTabMain";
            this.uTabMain.SharedControlsPage = this.pageFirst;
            this.uTabMain.Size = new System.Drawing.Size(803, 395);
            this.uTabMain.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Office2007Ribbon;
            this.uTabMain.TabIndex = 6;
            ultraTab2.TabPage = this.pageView;
            ultraTab2.Text = "Giao diện";
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "Phần đầu";
            ultraTab1.Visible = false;
            ultraTab3.TabPage = this.pageDetail;
            ultraTab3.Text = "Chi tiết";
            this.uTabMain.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab2,
            ultraTab1,
            ultraTab3});
            this.uTabMain.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007;
            this.uTabMain.SelectedTabChanged += new Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventHandler(this.ultraTabControl_SelectedTabChanged);
            // 
            // pageFirst
            // 
            this.pageFirst.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pageFirst.Location = new System.Drawing.Point(-10000, -10000);
            this.pageFirst.Name = "pageFirst";
            this.pageFirst.Size = new System.Drawing.Size(801, 372);
            // 
            // palBottom
            // 
            this.palBottom.Controls.Add(this.btnHelp);
            this.palBottom.Controls.Add(this.btnTemplate);
            this.palBottom.Controls.Add(this.btnClose);
            this.palBottom.Controls.Add(this.btnSave);
            this.palBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.palBottom.Location = new System.Drawing.Point(10, 450);
            this.palBottom.Name = "palBottom";
            this.palBottom.Size = new System.Drawing.Size(803, 43);
            this.palBottom.TabIndex = 2;
            // 
            // btnHelp
            // 
            appearance8.Image = global::Accounting.Properties.Resources.ubtnHelp;
            this.btnHelp.Appearance = appearance8;
            this.btnHelp.Location = new System.Drawing.Point(1, 10);
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(70, 30);
            this.btnHelp.TabIndex = 18;
            this.btnHelp.Text = "&Giúp";
            this.btnHelp.Click += new System.EventHandler(this.BtnHelpClick);
            // 
            // btnTemplate
            // 
            this.btnTemplate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance9.Image = global::Accounting.Properties.Resources.Designs_Folder_icon;
            this.btnTemplate.Appearance = appearance9;
            this.btnTemplate.Location = new System.Drawing.Point(499, 10);
            this.btnTemplate.Name = "btnTemplate";
            this.btnTemplate.Size = new System.Drawing.Size(143, 30);
            this.btnTemplate.TabIndex = 17;
            this.btnTemplate.Text = "&Lấy mẫu ngầm định";
            this.btnTemplate.Click += new System.EventHandler(this.BtnTemplateClick);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance10.Image = global::Accounting.Properties.Resources.cancel_16;
            this.btnClose.Appearance = appearance10;
            this.btnClose.Location = new System.Drawing.Point(728, 10);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(74, 30);
            this.btnClose.TabIndex = 16;
            this.btnClose.Text = "&Hủy bỏ";
            this.btnClose.Click += new System.EventHandler(this.BtnCloseClick);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance11.Image = global::Accounting.Properties.Resources.apply_16;
            this.btnSave.Appearance = appearance11;
            this.btnSave.Location = new System.Drawing.Point(648, 10);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(74, 30);
            this.btnSave.TabIndex = 15;
            this.btnSave.Text = "Đồ&ng ý";
            this.btnSave.Click += new System.EventHandler(this.BtnSaveClick);
            // 
            // ultraTabSharedControlsPage2
            // 
            this.ultraTabSharedControlsPage2.Location = new System.Drawing.Point(1, 20);
            this.ultraTabSharedControlsPage2.Name = "ultraTabSharedControlsPage2";
            this.ultraTabSharedControlsPage2.Size = new System.Drawing.Size(196, 77);
            // 
            // ultraTabPageControl4
            // 
            this.ultraTabPageControl4.Controls.Add(this.ultraGrid1);
            this.ultraTabPageControl4.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl4.Name = "ultraTabPageControl4";
            this.ultraTabPageControl4.Size = new System.Drawing.Size(821, 386);
            // 
            // ultraGrid1
            // 
            this.ultraGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid1.Location = new System.Drawing.Point(0, 0);
            this.ultraGrid1.Name = "ultraGrid1";
            this.ultraGrid1.Size = new System.Drawing.Size(821, 386);
            this.ultraGrid1.TabIndex = 0;
            this.ultraGrid1.Text = "ultraGrid1";
            this.ultraGrid1.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            // 
            // ultraTabPageControl5
            // 
            this.ultraTabPageControl5.Controls.Add(this.ultraGrid2);
            this.ultraTabPageControl5.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl5.Name = "ultraTabPageControl5";
            this.ultraTabPageControl5.Size = new System.Drawing.Size(821, 386);
            // 
            // ultraGrid2
            // 
            this.ultraGrid2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGrid2.Location = new System.Drawing.Point(0, 0);
            this.ultraGrid2.Name = "ultraGrid2";
            this.ultraGrid2.Size = new System.Drawing.Size(821, 386);
            this.ultraGrid2.TabIndex = 0;
            this.ultraGrid2.Text = "ultraGrid1";
            // 
            // FTemplateDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(823, 503);
            this.Controls.Add(this.palFill);
            this.Controls.Add(this.palBottom);
            this.Controls.Add(this.palTop);
            this.Name = "FTemplateDetail";
            this.Padding = new System.Windows.Forms.Padding(10);
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thay đổi mẫu";
            this.pageView.ResumeLayout(false);
            this.palView.ResumeLayout(false);
            this.palFisrt.ClientArea.ResumeLayout(false);
            this.palFisrt.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridFirstView)).EndInit();
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridFirst)).EndInit();
            this.panel2.ResumeLayout(false);
            this.pageDetail.ResumeLayout(false);
            this.palDetail.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.palTop.ResumeLayout(false);
            this.palTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNameOfTemplate)).EndInit();
            this.palFill.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uTabMain)).EndInit();
            this.uTabMain.ResumeLayout(false);
            this.palBottom.ResumeLayout(false);
            this.ultraTabPageControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).EndInit();
            this.ultraTabPageControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel palTop;
        private System.Windows.Forms.Panel palFill;
        private System.Windows.Forms.Panel palBottom;
        private Infragistics.Win.Misc.UltraLabel lblMau;
        private Infragistics.Win.Misc.UltraButton btnTemplate;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraButton btnHelp;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage2;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl4;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl5;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtNameOfTemplate;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl uTabMain;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage pageFirst;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl pageView;
        private Infragistics.Win.Misc.UltraPanel palView;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl pageDetail;
        private Infragistics.Win.Misc.UltraPanel palDetail;
        private System.Windows.Forms.Panel panel1;
        private Infragistics.Win.Misc.UltraButton btnMoveDown;
        private Infragistics.Win.Misc.UltraButton btnMoveUp;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.Misc.UltraPanel palFisrt;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridFirst;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridFirstView;
        private System.Windows.Forms.Panel panel2;
        private Infragistics.Win.Misc.UltraButton btnFirstUp;
        private Infragistics.Win.Misc.UltraButton btnFirstDown;
    }
}