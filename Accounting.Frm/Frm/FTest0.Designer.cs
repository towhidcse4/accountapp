﻿namespace Accounting
{
    partial class FTest0
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab5 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab6 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.txtstrColumnCaption = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraButton9 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton8 = new Infragistics.Win.Misc.UltraButton();
            this.btnUp = new Infragistics.Win.Misc.UltraButton();
            this.btnDown = new Infragistics.Win.Misc.UltraButton();
            this.ultraGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraButton7 = new Infragistics.Win.Misc.UltraButton();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.txtVTbolIsReadOnly = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.txtVTintVisiblePosition = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.txtVTbolIsVisible = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.txtNameObj = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtKq = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraButton6 = new Infragistics.Win.Misc.UltraButton();
            this.ultraTabPageControl5 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.btnDeleteTemplate = new System.Windows.Forms.Button();
            this.btnMoveUp = new Infragistics.Win.Misc.UltraButton();
            this.btnMoveDown = new Infragistics.Win.Misc.UltraButton();
            this.uGridTemplateColumnWrong = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.btnDelete = new Infragistics.Win.Misc.UltraButton();
            this.txtTypeID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.btnShowTemplateEditorForm = new Infragistics.Win.Misc.UltraButton();
            this.button2 = new System.Windows.Forms.Button();
            this.btnUpdateData = new System.Windows.Forms.Button();
            this.cbbTemplateDetail = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbbTemplate = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbbTypeGroup = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbbType = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.uGridTemplate = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.btnGetData = new System.Windows.Forms.Button();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraButton3 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton5 = new Infragistics.Win.Misc.UltraButton();
            this.ultraCombo1 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraButton4 = new Infragistics.Win.Misc.UltraButton();
            this.ultraCombo2 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraCombo3 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraButton2 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton1 = new Infragistics.Win.Misc.UltraButton();
            this.ultraTabPageControl3 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridTemplateColumnProperty = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.btnAccept = new Infragistics.Win.Misc.UltraButton();
            this.ultraTabPageControl4 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.button3 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.FTest0_Fill_Panel = new Infragistics.Win.Misc.UltraPanel();
            this.ultraTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraTabPageControl6 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridTestView = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtstrColumnCaption)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVTbolIsReadOnly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVTintVisiblePosition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVTbolIsVisible)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNameObj)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKq)).BeginInit();
            this.ultraTabPageControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridTemplateColumnWrong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbTemplateDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbTypeGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridTemplate)).BeginInit();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCombo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCombo2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCombo3)).BeginInit();
            this.ultraTabPageControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridTemplateColumnProperty)).BeginInit();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            this.ultraTabPageControl4.SuspendLayout();
            this.FTest0_Fill_Panel.ClientArea.SuspendLayout();
            this.FTest0_Fill_Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).BeginInit();
            this.ultraTabControl1.SuspendLayout();
            this.ultraTabPageControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridTestView)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.txtstrColumnCaption);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel5);
            this.ultraTabPageControl2.Controls.Add(this.ultraButton9);
            this.ultraTabPageControl2.Controls.Add(this.ultraButton8);
            this.ultraTabPageControl2.Controls.Add(this.btnUp);
            this.ultraTabPageControl2.Controls.Add(this.btnDown);
            this.ultraTabPageControl2.Controls.Add(this.ultraGrid1);
            this.ultraTabPageControl2.Controls.Add(this.ultraButton7);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel4);
            this.ultraTabPageControl2.Controls.Add(this.txtVTbolIsReadOnly);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel3);
            this.ultraTabPageControl2.Controls.Add(this.txtVTintVisiblePosition);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel2);
            this.ultraTabPageControl2.Controls.Add(this.txtVTbolIsVisible);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel1);
            this.ultraTabPageControl2.Controls.Add(this.txtNameObj);
            this.ultraTabPageControl2.Controls.Add(this.txtKq);
            this.ultraTabPageControl2.Controls.Add(this.ultraButton6);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(1049, 491);
            // 
            // txtstrColumnCaption
            // 
            this.txtstrColumnCaption.Location = new System.Drawing.Point(162, 459);
            this.txtstrColumnCaption.Name = "txtstrColumnCaption";
            this.txtstrColumnCaption.Size = new System.Drawing.Size(878, 21);
            this.txtstrColumnCaption.TabIndex = 21;
            // 
            // ultraLabel5
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel5.Appearance = appearance1;
            this.ultraLabel5.Location = new System.Drawing.Point(11, 460);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(143, 17);
            this.ultraLabel5.TabIndex = 20;
            this.ultraLabel5.Text = "(3) strColumnCaption";
            // 
            // ultraButton9
            // 
            this.ultraButton9.Location = new System.Drawing.Point(328, 41);
            this.ultraButton9.Name = "ultraButton9";
            this.ultraButton9.Size = new System.Drawing.Size(342, 23);
            this.ultraButton9.TabIndex = 19;
            this.ultraButton9.Text = "2. Chỉnh grid xong bấm cái này (sinh chuỗi config string)";
            this.ultraButton9.Click += new System.EventHandler(this.ultraButton9_Click);
            // 
            // ultraButton8
            // 
            this.ultraButton8.Location = new System.Drawing.Point(19, 41);
            this.ultraButton8.Name = "ultraButton8";
            this.ultraButton8.Size = new System.Drawing.Size(303, 23);
            this.ultraButton8.TabIndex = 18;
            this.ultraButton8.Text = "1. <bấm cái này đầu tiên =)) Fill Data to Grid>";
            this.ultraButton8.Click += new System.EventHandler(this.ultraButton8_Click);
            // 
            // btnUp
            // 
            appearance2.Image = global::Accounting.Properties.Resources.up;
            appearance2.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance2.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.btnUp.Appearance = appearance2;
            this.btnUp.Location = new System.Drawing.Point(676, 70);
            this.btnUp.Name = "btnUp";
            this.btnUp.Size = new System.Drawing.Size(30, 23);
            this.btnUp.TabIndex = 16;
            this.btnUp.Tag = "Up";
            this.btnUp.Click += new System.EventHandler(this.btnUp_Click);
            // 
            // btnDown
            // 
            appearance3.Image = global::Accounting.Properties.Resources.down;
            appearance3.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance3.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.btnDown.Appearance = appearance3;
            this.btnDown.Location = new System.Drawing.Point(676, 98);
            this.btnDown.Name = "btnDown";
            this.btnDown.Size = new System.Drawing.Size(30, 22);
            this.btnDown.TabIndex = 17;
            this.btnDown.Tag = "Down";
            this.btnDown.Click += new System.EventHandler(this.btnUp_Click);
            // 
            // ultraGrid1
            // 
            appearance4.BackColor = System.Drawing.SystemColors.Window;
            appearance4.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid1.DisplayLayout.Appearance = appearance4;
            this.ultraGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance5.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance5.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.GroupByBox.Appearance = appearance5;
            appearance6.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance6;
            this.ultraGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance7.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance7.BackColor2 = System.Drawing.SystemColors.Control;
            appearance7.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance7;
            this.ultraGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            appearance8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance8;
            appearance9.BackColor = System.Drawing.SystemColors.Highlight;
            appearance9.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance9;
            this.ultraGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.Override.CardAreaAppearance = appearance10;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            appearance11.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid1.DisplayLayout.Override.CellAppearance = appearance11;
            this.ultraGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance12.BackColor = System.Drawing.SystemColors.Control;
            appearance12.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance12.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance12.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance12.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance12;
            appearance13.TextHAlignAsString = "Left";
            this.ultraGrid1.DisplayLayout.Override.HeaderAppearance = appearance13;
            this.ultraGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid1.DisplayLayout.Override.RowAppearance = appearance14;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance15.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance15;
            this.ultraGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ultraGrid1.Location = new System.Drawing.Point(11, 70);
            this.ultraGrid1.Name = "ultraGrid1";
            this.ultraGrid1.Size = new System.Drawing.Size(659, 297);
            this.ultraGrid1.TabIndex = 11;
            this.ultraGrid1.Text = "ultraGrid1";
            // 
            // ultraButton7
            // 
            this.ultraButton7.Location = new System.Drawing.Point(819, 41);
            this.ultraButton7.Name = "ultraButton7";
            this.ultraButton7.Size = new System.Drawing.Size(169, 23);
            this.ultraButton7.TabIndex = 10;
            this.ultraButton7.Text = "4. SelectAll and Copy Script";
            this.ultraButton7.Click += new System.EventHandler(this.ultraButton7_Click_1);
            // 
            // ultraLabel4
            // 
            appearance16.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel4.Appearance = appearance16;
            this.ultraLabel4.Location = new System.Drawing.Point(13, 406);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(143, 17);
            this.ultraLabel4.TabIndex = 9;
            this.ultraLabel4.Text = "(1) VTintVisiblePosition";
            // 
            // txtVTbolIsReadOnly
            // 
            this.txtVTbolIsReadOnly.Location = new System.Drawing.Point(162, 432);
            this.txtVTbolIsReadOnly.Name = "txtVTbolIsReadOnly";
            this.txtVTbolIsReadOnly.Size = new System.Drawing.Size(878, 21);
            this.txtVTbolIsReadOnly.TabIndex = 8;
            // 
            // ultraLabel3
            // 
            appearance17.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel3.Appearance = appearance17;
            this.ultraLabel3.Location = new System.Drawing.Point(11, 433);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(143, 17);
            this.ultraLabel3.TabIndex = 7;
            this.ultraLabel3.Text = "(2) VTbolIsReadOnly";
            // 
            // txtVTintVisiblePosition
            // 
            this.txtVTintVisiblePosition.Location = new System.Drawing.Point(162, 404);
            this.txtVTintVisiblePosition.Name = "txtVTintVisiblePosition";
            this.txtVTintVisiblePosition.Size = new System.Drawing.Size(878, 21);
            this.txtVTintVisiblePosition.TabIndex = 6;
            // 
            // ultraLabel2
            // 
            appearance18.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel2.Appearance = appearance18;
            this.ultraLabel2.Location = new System.Drawing.Point(11, 380);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(143, 17);
            this.ultraLabel2.TabIndex = 5;
            this.ultraLabel2.Text = "(1) VTbolIsVisible";
            // 
            // txtVTbolIsVisible
            // 
            this.txtVTbolIsVisible.Location = new System.Drawing.Point(162, 378);
            this.txtVTbolIsVisible.Name = "txtVTbolIsVisible";
            this.txtVTbolIsVisible.Size = new System.Drawing.Size(878, 21);
            this.txtVTbolIsVisible.TabIndex = 4;
            // 
            // ultraLabel1
            // 
            appearance19.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel1.Appearance = appearance19;
            this.ultraLabel1.Location = new System.Drawing.Point(19, 13);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(67, 15);
            this.ultraLabel1.TabIndex = 3;
            this.ultraLabel1.Text = "Tên bảng";
            // 
            // txtNameObj
            // 
            this.txtNameObj.Location = new System.Drawing.Point(92, 11);
            this.txtNameObj.Name = "txtNameObj";
            this.txtNameObj.Size = new System.Drawing.Size(162, 21);
            this.txtNameObj.TabIndex = 2;
            this.txtNameObj.Text = "MCPayment";
            // 
            // txtKq
            // 
            this.txtKq.Location = new System.Drawing.Point(712, 70);
            this.txtKq.Multiline = true;
            this.txtKq.Name = "txtKq";
            this.txtKq.Size = new System.Drawing.Size(328, 297);
            this.txtKq.TabIndex = 1;
            // 
            // ultraButton6
            // 
            this.ultraButton6.Location = new System.Drawing.Point(712, 41);
            this.ultraButton6.Name = "ultraButton6";
            this.ultraButton6.Size = new System.Drawing.Size(101, 23);
            this.ultraButton6.TabIndex = 0;
            this.ultraButton6.Text = "3. Gen Script";
            this.ultraButton6.Click += new System.EventHandler(this.ultraButton6_Click);
            // 
            // ultraTabPageControl5
            // 
            this.ultraTabPageControl5.Controls.Add(this.btnDeleteTemplate);
            this.ultraTabPageControl5.Controls.Add(this.btnMoveUp);
            this.ultraTabPageControl5.Controls.Add(this.btnMoveDown);
            this.ultraTabPageControl5.Controls.Add(this.uGridTemplateColumnWrong);
            this.ultraTabPageControl5.Controls.Add(this.btnDelete);
            this.ultraTabPageControl5.Controls.Add(this.txtTypeID);
            this.ultraTabPageControl5.Controls.Add(this.btnShowTemplateEditorForm);
            this.ultraTabPageControl5.Controls.Add(this.button2);
            this.ultraTabPageControl5.Controls.Add(this.btnUpdateData);
            this.ultraTabPageControl5.Controls.Add(this.cbbTemplateDetail);
            this.ultraTabPageControl5.Controls.Add(this.cbbTemplate);
            this.ultraTabPageControl5.Controls.Add(this.cbbTypeGroup);
            this.ultraTabPageControl5.Controls.Add(this.cbbType);
            this.ultraTabPageControl5.Controls.Add(this.uGridTemplate);
            this.ultraTabPageControl5.Controls.Add(this.btnGetData);
            this.ultraTabPageControl5.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl5.Name = "ultraTabPageControl5";
            this.ultraTabPageControl5.Size = new System.Drawing.Size(1049, 491);
            // 
            // btnDeleteTemplate
            // 
            this.btnDeleteTemplate.Location = new System.Drawing.Point(360, 6);
            this.btnDeleteTemplate.Name = "btnDeleteTemplate";
            this.btnDeleteTemplate.Size = new System.Drawing.Size(105, 23);
            this.btnDeleteTemplate.TabIndex = 16;
            this.btnDeleteTemplate.Text = "Xóa Template";
            this.btnDeleteTemplate.UseVisualStyleBackColor = true;
            this.btnDeleteTemplate.Click += new System.EventHandler(this.btnDeleteTemplate_Click);
            // 
            // btnMoveUp
            // 
            appearance20.Image = global::Accounting.Properties.Resources.up;
            appearance20.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance20.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.btnMoveUp.Appearance = appearance20;
            this.btnMoveUp.Location = new System.Drawing.Point(734, 5);
            this.btnMoveUp.Name = "btnMoveUp";
            this.btnMoveUp.Size = new System.Drawing.Size(30, 23);
            this.btnMoveUp.TabIndex = 14;
            this.btnMoveUp.Tag = "Up";
            this.btnMoveUp.Click += new System.EventHandler(this.btnMoveUp_Click);
            // 
            // btnMoveDown
            // 
            appearance21.Image = global::Accounting.Properties.Resources.down;
            appearance21.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance21.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.btnMoveDown.Appearance = appearance21;
            this.btnMoveDown.Location = new System.Drawing.Point(734, 33);
            this.btnMoveDown.Name = "btnMoveDown";
            this.btnMoveDown.Size = new System.Drawing.Size(30, 22);
            this.btnMoveDown.TabIndex = 15;
            this.btnMoveDown.Tag = "Down";
            this.btnMoveDown.Click += new System.EventHandler(this.btnMoveUp_Click);
            // 
            // uGridTemplateColumnWrong
            // 
            this.uGridTemplateColumnWrong.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.uGridTemplateColumnWrong.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGridTemplateColumnWrong.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            this.uGridTemplateColumnWrong.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.PrefixWithEmptyCell;
            this.uGridTemplateColumnWrong.DisplayLayout.Override.FilterOperatorDefaultValue = Infragistics.Win.UltraWinGrid.FilterOperatorDefaultValue.Equals;
            this.uGridTemplateColumnWrong.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGridTemplateColumnWrong.DisplayLayout.Override.RowFilterMode = Infragistics.Win.UltraWinGrid.RowFilterMode.AllRowsInBand;
            this.uGridTemplateColumnWrong.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGridTemplateColumnWrong.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridTemplateColumnWrong.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridTemplateColumnWrong.Location = new System.Drawing.Point(744, 61);
            this.uGridTemplateColumnWrong.Name = "uGridTemplateColumnWrong";
            this.uGridTemplateColumnWrong.Size = new System.Drawing.Size(296, 421);
            this.uGridTemplateColumnWrong.TabIndex = 13;
            this.uGridTemplateColumnWrong.Text = "Danh sách Column sai";
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(649, 5);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(70, 23);
            this.btnDelete.TabIndex = 11;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // txtTypeID
            // 
            this.txtTypeID.Location = new System.Drawing.Point(185, 6);
            this.txtTypeID.Name = "txtTypeID";
            this.txtTypeID.NullText = "<Nhập TypeID ở đây>";
            this.txtTypeID.Size = new System.Drawing.Size(125, 21);
            this.txtTypeID.TabIndex = 10;
            this.txtTypeID.Text = "100";
            // 
            // btnShowTemplateEditorForm
            // 
            this.btnShowTemplateEditorForm.Location = new System.Drawing.Point(12, 5);
            this.btnShowTemplateEditorForm.Name = "btnShowTemplateEditorForm";
            this.btnShowTemplateEditorForm.Size = new System.Drawing.Size(166, 23);
            this.btnShowTemplateEditorForm.TabIndex = 9;
            this.btnShowTemplateEditorForm.Text = "Show Template Editor Form";
            this.btnShowTemplateEditorForm.Click += new System.EventHandler(this.BtnShowTemplateEditorFormClick);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(472, 5);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(64, 23);
            this.button2.TabIndex = 8;
            this.button2.Text = "UnCheck";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnUpdateData
            // 
            this.btnUpdateData.Location = new System.Drawing.Point(542, 6);
            this.btnUpdateData.Name = "btnUpdateData";
            this.btnUpdateData.Size = new System.Drawing.Size(101, 23);
            this.btnUpdateData.TabIndex = 7;
            this.btnUpdateData.Text = "Update Data";
            this.btnUpdateData.UseVisualStyleBackColor = true;
            this.btnUpdateData.Click += new System.EventHandler(this.BtnUpdateDataClick);
            // 
            // cbbTemplateDetail
            // 
            this.cbbTemplateDetail.Location = new System.Drawing.Point(436, 34);
            this.cbbTemplateDetail.Name = "cbbTemplateDetail";
            this.cbbTemplateDetail.Size = new System.Drawing.Size(100, 22);
            this.cbbTemplateDetail.TabIndex = 6;
            this.cbbTemplateDetail.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.CbbTemplateDetailRowSelected);
            // 
            // cbbTemplate
            // 
            this.cbbTemplate.Location = new System.Drawing.Point(332, 34);
            this.cbbTemplate.Name = "cbbTemplate";
            this.cbbTemplate.Size = new System.Drawing.Size(100, 22);
            this.cbbTemplate.TabIndex = 5;
            this.cbbTemplate.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.CbbTemplateRowSelected);
            // 
            // cbbTypeGroup
            // 
            this.cbbTypeGroup.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbTypeGroup.Location = new System.Drawing.Point(12, 33);
            this.cbbTypeGroup.Name = "cbbTypeGroup";
            this.cbbTypeGroup.Size = new System.Drawing.Size(100, 22);
            this.cbbTypeGroup.TabIndex = 4;
            this.cbbTypeGroup.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.CbbTypeGroupRowSelected);
            // 
            // cbbType
            // 
            this.cbbType.Location = new System.Drawing.Point(119, 33);
            this.cbbType.Name = "cbbType";
            this.cbbType.Size = new System.Drawing.Size(100, 22);
            this.cbbType.TabIndex = 3;
            // 
            // uGridTemplate
            // 
            this.uGridTemplate.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.uGridTemplate.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.Yes;
            this.uGridTemplate.DisplayLayout.Override.RowSelectorHeaderStyle = Infragistics.Win.UltraWinGrid.RowSelectorHeaderStyle.None;
            this.uGridTemplate.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.uGridTemplate.Location = new System.Drawing.Point(11, 61);
            this.uGridTemplate.Name = "uGridTemplate";
            this.uGridTemplate.Size = new System.Drawing.Size(727, 421);
            this.uGridTemplate.TabIndex = 0;
            this.uGridTemplate.Text = "ultraGrid2";
            // 
            // btnGetData
            // 
            this.btnGetData.Location = new System.Drawing.Point(225, 33);
            this.btnGetData.Name = "btnGetData";
            this.btnGetData.Size = new System.Drawing.Size(101, 23);
            this.btnGetData.TabIndex = 2;
            this.btnGetData.Text = "Get data";
            this.btnGetData.UseVisualStyleBackColor = true;
            this.btnGetData.Click += new System.EventHandler(this.BtnGetDataClick);
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.ultraButton3);
            this.ultraTabPageControl1.Controls.Add(this.ultraButton5);
            this.ultraTabPageControl1.Controls.Add(this.ultraCombo1);
            this.ultraTabPageControl1.Controls.Add(this.ultraButton4);
            this.ultraTabPageControl1.Controls.Add(this.ultraCombo2);
            this.ultraTabPageControl1.Controls.Add(this.ultraCombo3);
            this.ultraTabPageControl1.Controls.Add(this.ultraButton2);
            this.ultraTabPageControl1.Controls.Add(this.ultraButton1);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(1049, 491);
            // 
            // ultraButton3
            // 
            this.ultraButton3.Location = new System.Drawing.Point(16, 13);
            this.ultraButton3.Name = "ultraButton3";
            this.ultraButton3.Size = new System.Drawing.Size(198, 32);
            this.ultraButton3.TabIndex = 5;
            this.ultraButton3.Text = "Call FTest1 Form (Test Config Grid)";
            this.ultraButton3.Click += new System.EventHandler(this.ultraButton3_Click);
            // 
            // ultraButton5
            // 
            this.ultraButton5.Location = new System.Drawing.Point(16, 51);
            this.ultraButton5.Name = "ultraButton5";
            this.ultraButton5.Size = new System.Drawing.Size(198, 32);
            this.ultraButton5.TabIndex = 7;
            this.ultraButton5.Text = "Call FTest2 Form (Test Style)";
            this.ultraButton5.Click += new System.EventHandler(this.ultraButton5_Click);
            // 
            // ultraCombo1
            // 
            this.ultraCombo1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.ultraCombo1.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.ultraCombo1.Location = new System.Drawing.Point(504, 13);
            this.ultraCombo1.Name = "ultraCombo1";
            this.ultraCombo1.NullText = "<vui lòng chọn dữ liệu>";
            this.ultraCombo1.Size = new System.Drawing.Size(207, 22);
            this.ultraCombo1.TabIndex = 0;
            // 
            // ultraButton4
            // 
            this.ultraButton4.Location = new System.Drawing.Point(636, 98);
            this.ultraButton4.Name = "ultraButton4";
            this.ultraButton4.Size = new System.Drawing.Size(75, 23);
            this.ultraButton4.TabIndex = 6;
            this.ultraButton4.Text = "ultraButton4";
            this.ultraButton4.Click += new System.EventHandler(this.ultraButton4_Click);
            // 
            // ultraCombo2
            // 
            this.ultraCombo2.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.ultraCombo2.Location = new System.Drawing.Point(504, 70);
            this.ultraCombo2.Name = "ultraCombo2";
            this.ultraCombo2.NullText = "<vui lòng chọn dữ liệu>";
            this.ultraCombo2.Size = new System.Drawing.Size(99, 22);
            this.ultraCombo2.TabIndex = 1;
            // 
            // ultraCombo3
            // 
            this.ultraCombo3.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.ultraCombo3.Location = new System.Drawing.Point(612, 70);
            this.ultraCombo3.Name = "ultraCombo3";
            this.ultraCombo3.NullText = "<vui lòng chọn dữ liệu>";
            this.ultraCombo3.Size = new System.Drawing.Size(99, 22);
            this.ultraCombo3.TabIndex = 2;
            // 
            // ultraButton2
            // 
            this.ultraButton2.Location = new System.Drawing.Point(636, 41);
            this.ultraButton2.Name = "ultraButton2";
            this.ultraButton2.Size = new System.Drawing.Size(75, 23);
            this.ultraButton2.TabIndex = 4;
            this.ultraButton2.Text = "ultraButton2";
            this.ultraButton2.Click += new System.EventHandler(this.ultraButton2_Click);
            // 
            // ultraButton1
            // 
            this.ultraButton1.Location = new System.Drawing.Point(504, 41);
            this.ultraButton1.Name = "ultraButton1";
            this.ultraButton1.Size = new System.Drawing.Size(75, 23);
            this.ultraButton1.TabIndex = 3;
            this.ultraButton1.Text = "ultraButton1";
            this.ultraButton1.Click += new System.EventHandler(this.ultraButton1_Click);
            // 
            // ultraTabPageControl3
            // 
            this.ultraTabPageControl3.Controls.Add(this.uGridTemplateColumnProperty);
            this.ultraTabPageControl3.Controls.Add(this.ultraPanel1);
            this.ultraTabPageControl3.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl3.Name = "ultraTabPageControl3";
            this.ultraTabPageControl3.Size = new System.Drawing.Size(1049, 491);
            // 
            // uGridTemplateColumnProperty
            // 
            this.uGridTemplateColumnProperty.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridTemplateColumnProperty.Location = new System.Drawing.Point(0, 0);
            this.uGridTemplateColumnProperty.Name = "uGridTemplateColumnProperty";
            this.uGridTemplateColumnProperty.Size = new System.Drawing.Size(1049, 443);
            this.uGridTemplateColumnProperty.TabIndex = 0;
            this.uGridTemplateColumnProperty.Text = "uGridTemplateColumnProperty";
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.btnAccept);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 443);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(1049, 48);
            this.ultraPanel1.TabIndex = 1;
            // 
            // btnAccept
            // 
            this.btnAccept.Location = new System.Drawing.Point(560, 7);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Size = new System.Drawing.Size(137, 38);
            this.btnAccept.TabIndex = 0;
            this.btnAccept.Text = "Chuyển đổi";
            this.btnAccept.Click += new System.EventHandler(this.btnAccept_Click);
            // 
            // ultraTabPageControl4
            // 
            this.ultraTabPageControl4.Controls.Add(this.button3);
            this.ultraTabPageControl4.Controls.Add(this.button1);
            this.ultraTabPageControl4.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl4.Name = "ultraTabPageControl4";
            this.ultraTabPageControl4.Size = new System.Drawing.Size(1049, 491);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(22, 23);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(108, 23);
            this.button3.TabIndex = 1;
            this.button3.Text = "DANH MỤC";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(212, 101);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FTest0_Fill_Panel
            // 
            // 
            // FTest0_Fill_Panel.ClientArea
            // 
            this.FTest0_Fill_Panel.ClientArea.Controls.Add(this.ultraTabControl1);
            this.FTest0_Fill_Panel.Cursor = System.Windows.Forms.Cursors.Default;
            this.FTest0_Fill_Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FTest0_Fill_Panel.Location = new System.Drawing.Point(0, 0);
            this.FTest0_Fill_Panel.Name = "FTest0_Fill_Panel";
            this.FTest0_Fill_Panel.Size = new System.Drawing.Size(1053, 517);
            this.FTest0_Fill_Panel.TabIndex = 0;
            // 
            // ultraTabControl1
            // 
            this.ultraTabControl1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl2);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl3);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl4);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl5);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl6);
            this.ultraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.ultraTabControl1.Name = "ultraTabControl1";
            this.ultraTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.ultraTabControl1.Size = new System.Drawing.Size(1053, 517);
            this.ultraTabControl1.TabIndex = 8;
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "AutoGen ConstDatabase";
            ultraTab5.TabPage = this.ultraTabPageControl5;
            ultraTab5.Text = "Template Editor";
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "tab1";
            ultraTab3.TabPage = this.ultraTabPageControl3;
            ultraTab3.Text = "tab3";
            ultraTab4.TabPage = this.ultraTabPageControl4;
            ultraTab4.Text = "tab4";
            ultraTab6.TabPage = this.ultraTabPageControl6;
            ultraTab6.Text = "tab2";
            this.ultraTabControl1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab2,
            ultraTab5,
            ultraTab1,
            ultraTab3,
            ultraTab4,
            ultraTab6});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(1049, 491);
            // 
            // ultraTabPageControl6
            // 
            this.ultraTabPageControl6.Controls.Add(this.uGridTestView);
            this.ultraTabPageControl6.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl6.Name = "ultraTabPageControl6";
            this.ultraTabPageControl6.Size = new System.Drawing.Size(1049, 491);
            // 
            // uGridTestView
            // 
            appearance22.BackColor = System.Drawing.SystemColors.Window;
            appearance22.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridTestView.DisplayLayout.Appearance = appearance22;
            this.uGridTestView.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridTestView.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance23.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance23.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance23.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridTestView.DisplayLayout.GroupByBox.Appearance = appearance23;
            appearance24.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridTestView.DisplayLayout.GroupByBox.BandLabelAppearance = appearance24;
            this.uGridTestView.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance25.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance25.BackColor2 = System.Drawing.SystemColors.Control;
            appearance25.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance25.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridTestView.DisplayLayout.GroupByBox.PromptAppearance = appearance25;
            this.uGridTestView.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridTestView.DisplayLayout.MaxRowScrollRegions = 1;
            appearance26.BackColor = System.Drawing.SystemColors.Window;
            appearance26.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridTestView.DisplayLayout.Override.ActiveCellAppearance = appearance26;
            appearance27.BackColor = System.Drawing.SystemColors.Highlight;
            appearance27.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridTestView.DisplayLayout.Override.ActiveRowAppearance = appearance27;
            this.uGridTestView.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridTestView.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance28.BackColor = System.Drawing.SystemColors.Window;
            this.uGridTestView.DisplayLayout.Override.CardAreaAppearance = appearance28;
            appearance29.BorderColor = System.Drawing.Color.Silver;
            appearance29.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridTestView.DisplayLayout.Override.CellAppearance = appearance29;
            this.uGridTestView.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridTestView.DisplayLayout.Override.CellPadding = 0;
            appearance30.BackColor = System.Drawing.SystemColors.Control;
            appearance30.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance30.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance30.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance30.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridTestView.DisplayLayout.Override.GroupByRowAppearance = appearance30;
            appearance31.TextHAlignAsString = "Left";
            this.uGridTestView.DisplayLayout.Override.HeaderAppearance = appearance31;
            this.uGridTestView.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridTestView.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance32.BackColor = System.Drawing.SystemColors.Window;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            this.uGridTestView.DisplayLayout.Override.RowAppearance = appearance32;
            this.uGridTestView.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance33.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridTestView.DisplayLayout.Override.TemplateAddRowAppearance = appearance33;
            this.uGridTestView.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridTestView.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridTestView.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridTestView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridTestView.Location = new System.Drawing.Point(0, 0);
            this.uGridTestView.Name = "uGridTestView";
            this.uGridTestView.Size = new System.Drawing.Size(1049, 491);
            this.uGridTestView.TabIndex = 0;
            this.uGridTestView.Text = "ultraGrid2";
            // 
            // FTest0
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1053, 517);
            this.Controls.Add(this.FTest0_Fill_Panel);
            this.Name = "FTest0";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Frm test";
            this.ultraTabPageControl2.ResumeLayout(false);
            this.ultraTabPageControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtstrColumnCaption)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVTbolIsReadOnly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVTintVisiblePosition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVTbolIsVisible)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNameObj)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKq)).EndInit();
            this.ultraTabPageControl5.ResumeLayout(false);
            this.ultraTabPageControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridTemplateColumnWrong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbTemplateDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbTypeGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridTemplate)).EndInit();
            this.ultraTabPageControl1.ResumeLayout(false);
            this.ultraTabPageControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCombo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCombo2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCombo3)).EndInit();
            this.ultraTabPageControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridTemplateColumnProperty)).EndInit();
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            this.ultraTabPageControl4.ResumeLayout(false);
            this.FTest0_Fill_Panel.ClientArea.ResumeLayout(false);
            this.FTest0_Fill_Panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).EndInit();
            this.ultraTabControl1.ResumeLayout(false);
            this.ultraTabPageControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridTestView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinGrid.UltraCombo ultraCombo1;
        private Infragistics.Win.Misc.UltraPanel FTest0_Fill_Panel;
        private Infragistics.Win.UltraWinGrid.UltraCombo ultraCombo3;
        private Infragistics.Win.UltraWinGrid.UltraCombo ultraCombo2;
        private Infragistics.Win.Misc.UltraButton ultraButton1;
        private Infragistics.Win.Misc.UltraButton ultraButton2;
        private Infragistics.Win.Misc.UltraButton ultraButton3;
        private Infragistics.Win.Misc.UltraButton ultraButton4;
        private Infragistics.Win.Misc.UltraButton ultraButton5;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtKq;
        private Infragistics.Win.Misc.UltraButton ultraButton6;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtNameObj;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl3;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridTemplateColumnProperty;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl4;
        private System.Windows.Forms.Button button1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl5;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridTemplate;
        private System.Windows.Forms.Button btnGetData;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbType;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbTypeGroup;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbTemplate;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbTemplateDetail;
        private System.Windows.Forms.Button btnUpdateData;
        private System.Windows.Forms.Button button2;
        private Infragistics.Win.Misc.UltraButton btnShowTemplateEditorForm;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTypeID;
        private System.Windows.Forms.Button button3;
        private Infragistics.Win.Misc.UltraButton btnDelete;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraButton btnAccept;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridTemplateColumnWrong;
        private Infragistics.Win.Misc.UltraButton btnMoveUp;
        private Infragistics.Win.Misc.UltraButton btnMoveDown;
        private Infragistics.Win.Misc.UltraButton ultraButton7;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid1;
        private Infragistics.Win.Misc.UltraButton btnUp;
        private Infragistics.Win.Misc.UltraButton btnDown;
        private Infragistics.Win.Misc.UltraButton ultraButton8;
        private Infragistics.Win.Misc.UltraButton ultraButton9;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtVTbolIsReadOnly;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtVTintVisiblePosition;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtVTbolIsVisible;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtstrColumnCaption;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private System.Windows.Forms.Button btnDeleteTemplate;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl6;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridTestView;
    }
}