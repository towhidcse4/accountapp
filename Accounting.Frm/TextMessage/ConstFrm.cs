﻿using System.Collections.Generic;

namespace Accounting.TextMessage
{
    public class ConstFrm
    {
        #region MỘT SỐ THIẾT LẬP OPTION
        public static string DbStartDate { get; set; }  //Ngày hoạch toán (DBStartDate) dd/MM/yyyy
        public static string TTH { get; set; }
        #endregion

        #region TypeGroup
        public static int TypeGroupSaQuote = 30;
        public static int TypeGroupSaOrder = 31;
        public static int TypeGroupSaInvoice = 32;
        public static int TypeGroupSaReturnHBTL = 33;
        public static int TypeGroupSaReturnGghb = 34;
        public static int TypeGroup_MBDepositDetail = 16;
        public static int TypeGroup_MCReceipt = 10;
        public static int TypeGroup_RSInwardOutward = 40;
        public static int TypeGroup_RSInwardOutwardOutput = 41;
        public static int TypeGroup_RSItransfer = 42;
        public static int TypeGroupMCPayment = 11;
        public static int TypeGroupMBDeposit = 16;
        public static int TypeGroup_PPOrder = 20;
        public static int TypeGroup_PPInvoice = 21;
        public static int TypeGroup_PPReturnDetail = 22;
        public static int TypeGroup_PPDiscountDetail = 23;
        public static int TypeGroup_PPService = 24;

        public static int TypeGroup_GOtherVoucher = 60;
        public static int TypeGruop_MBTellerPaper = 12;
        public static int TypeGroup_MBCreditCard = 17;
        public static int TypeGroup_FADecrement = 52;
        public static int TypeGroup_FAAdjustmet = 53;

        public static int TypeGruop_MBInteralTransfer = 15;
        #endregion

        /// <summary>
        /// thiết lập luồng chạy nghiệp vụ hỗ trợ GHI SỔ hoặc không.
        /// </summary>
        public class optBizFollowConst
        {
            public static int status = SavelegedSave;

            /// <summary>
            /// Không có chế độ ghi sổ
            /// </summary>
            public const int NotSaveleged = 1;         //Không có chế độ ghi sổ
            /// <summary>
            /// Chương trình tự ghi sổ
            /// </summary>
            public const int SavelegedSave = 2;        //Chương trình tự ghi sổ
            /// <summary>
            /// Người dùng tự ghi sổ
            /// </summary>
            public const int SavelegedNotsave = 3;     //Người dùng tự ghi sổ
        }

        /// <summary>
        /// Status Form
        /// </summary>
        public class optStatusForm
        {
            public static int status = View;

            /// <summary>
            /// Xem
            /// </summary>
            public const int View = 1;
            /// <summary>
            /// Thêm
            /// </summary>
            public const int Add = 2;
            /// <summary>
            /// Sửa
            /// </summary>
            public const int Edit = 3;
            /// <summary>
            /// Xóa
            /// </summary>
            public const int Delete = 4;
        }
        public class Paper
        {
            /// <summary>
            /// Khổ giấy A4
            /// </summary>
            public const int PAPER_SIZE_A4 = 0;
            /// <summary>
            /// Khổ giấy A5
            /// </summary>
            public const int PAPER_SIZE_A5 = 1;
            public static System.Drawing.Printing.PaperSize PaperSize(int i)
            {
                switch (i)
                {
                    case 1: return new System.Drawing.Printing.PaperSize { RawKind = (int)System.Drawing.Printing.PaperKind.A5 };
                    default: return new System.Drawing.Printing.PaperSize { RawKind = (int)System.Drawing.Printing.PaperKind.A4 };
                }
            }
        }
    }
}
