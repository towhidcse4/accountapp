﻿using System.Windows.Forms;

namespace Accounting.TextMessage
{
    public static class Constants
    {
        #region Patterns DateTime
        public const string DdMMyyyy = "dd/MM/yyyy";
        public const string MMddyyyy = "MM/dd/yyyy";
        #endregion
    }

    public class MSG
    {
        public static string MSGErrorForm = "Có lỗi xảy ra. \r\nChi tiết lỗi: \r\n{0}";
        public static string TieuDe = resSystem.MSG_System_00;

        public static DialogResult Question(string msg)
        {
            return MessageBoxStand(msg, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }
        public static DialogResult Error(string msg)
        {
            return MessageBoxStand(msg, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        public static DialogResult Warning(string msg)
        {
            return MessageBoxStand(msg, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
        public static DialogResult Information(string msg)
        {
            return MessageBoxStand(msg, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        public static DialogResult MessageBoxStand(string msg, MessageBoxButtons msgbuton = MessageBoxButtons.OK, MessageBoxIcon msgIcon = MessageBoxIcon.None)
        {
            return MessageBox.Show(msg, TieuDe, msgbuton, msgIcon);
        }
    }
}
