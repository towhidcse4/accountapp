﻿namespace Accounting
{
    partial class MsgRefDateTime
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MsgRefDateTime));
            this.lblImage = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.btnApply = new Infragistics.Win.Misc.UltraButton();
            this.btnCancel = new Infragistics.Win.Misc.UltraButton();
            this.txtTime = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.SuspendLayout();
            // 
            // lblImage
            // 
            appearance1.Image = global::Accounting.Properties.Resources.Calendar;
            appearance1.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance1.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance1.TextVAlignAsString = "Middle";
            this.lblImage.Appearance = appearance1;
            this.lblImage.AutoSize = true;
            this.lblImage.ImageSize = new System.Drawing.Size(48, 48);
            this.lblImage.Location = new System.Drawing.Point(12, 12);
            this.lblImage.Name = "lblImage";
            this.lblImage.Size = new System.Drawing.Size(48, 48);
            this.lblImage.TabIndex = 11;
            // 
            // ultraLabel1
            // 
            appearance2.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance2;
            this.ultraLabel1.Location = new System.Drawing.Point(62, 30);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(130, 21);
            this.ultraLabel1.TabIndex = 14;
            this.ultraLabel1.Text = "Thời gian (giờ/phút/giây)";
            // 
            // btnApply
            // 
            appearance3.Image = global::Accounting.Properties.Resources.apply_16;
            this.btnApply.Appearance = appearance3;
            this.btnApply.Location = new System.Drawing.Point(116, 79);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(85, 26);
            this.btnApply.TabIndex = 13;
            this.btnApply.Text = "Đồng ý";
            // 
            // btnCancel
            // 
            appearance4.Image = ((object)(resources.GetObject("appearance4.Image")));
            this.btnCancel.Appearance = appearance4;
            this.btnCancel.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2010Button;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(207, 79);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 27);
            this.btnCancel.TabIndex = 12;
            this.btnCancel.Text = "Hủy bỏ";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // txtTime
            // 
            this.txtTime.AutoSize = false;
            this.txtTime.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtTime.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.txtTime.InputMask = "{LOC}hh:mm:ss";
            this.txtTime.Location = new System.Drawing.Point(192, 30);
            this.txtTime.Name = "txtTime";
            this.txtTime.NullText = "00:00:00";
            this.txtTime.Size = new System.Drawing.Size(83, 21);
            this.txtTime.SpinButtonDisplayStyle = Infragistics.Win.SpinButtonDisplayStyle.OnRight;
            this.txtTime.SpinWrap = true;
            this.txtTime.TabIndex = 16;
            this.txtTime.Text = "::";
            // 
            // MsgRefDateTime
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(289, 114);
            this.Controls.Add(this.txtTime);
            this.Controls.Add(this.ultraLabel1);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lblImage);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MsgRefDateTime";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thiết lập thời gian ";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.Misc.UltraLabel lblImage;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraButton btnApply;
        private Infragistics.Win.Misc.UltraButton btnCancel;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtTime;

    }
}