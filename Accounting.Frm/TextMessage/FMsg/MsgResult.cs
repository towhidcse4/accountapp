﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class MsgResult : DialogForm
    {
        public MsgResult(List<Result> results, bool isVoucher, bool? isRecordedResult = null)
        {
            InitializeComponent();
            Text = !isRecordedResult.HasValue ? "Hiển thị kết quả khi xóa chứng từ" : (isRecordedResult.Value ? "Hiển thị kết quả khi ghi sổ chứng từ" : "Hiển thị kết quả khi bỏ ghi sổ chứng từ");
            if (isVoucher)
            {
                if (isRecordedResult.HasValue && isRecordedResult.Value)
                {
                    ultraLabel2.Text = "Số chứng từ ghi sổ thành công";
                    ultraLabel3.Text = "Số chứng từ ghi sổ không thành công";
                }
                if (isRecordedResult.HasValue && !isRecordedResult.Value)
                {
                    ultraLabel2.Text = "Số chứng từ bỏ ghi sổ thành công";
                    ultraLabel3.Text = "Số chứng từ bỏ ghi sổ không thành công";
                }
            }
            else
            {
                ultraLabel1.Text = "Số dòng được xử lý";
                ultraLabel2.Text = "Số dòng xóa thành công";
                ultraLabel3.Text = "Số dòng xóa không thành công";
            }
            lblTotal.Text = string.Format(": {0}", results.Count);
            lblDone.Text = string.Format(": {0}", results.Count(x => x.IsSucceed));
            lblError.Text = string.Format(": {0}", results.Count(x => !x.IsSucceed));
            gridResult.DataSource = results.OrderByDescending(x => x.IsSucceed).ThenByDescending(x => x.Date).ThenBy(x => x.No).ToList();
            gridResult.ConfigGrid(ConstDatabase.DeleteResults_TableName);
            var band = gridResult.DisplayLayout.Bands[0];
            if (!isVoucher)
            {
                band.Columns["Date"].Hidden = true;
                band.Columns["No"].Hidden = true;
                band.Columns["TypeName"].Hidden = true;
            }
            else
                band.Columns["Code"].Hidden = true;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void gridResult_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                Result result = (Result)grid.ActiveRow.ListObject;
                if (result.refID != null && result.TypeID != null)
                {
                    ViewVoucherInvisible viewVoucherInvisible = new ViewVoucherInvisible();
                    viewVoucherInvisible.RefID = result.refID ?? Guid.Empty;
                    viewVoucherInvisible.TypeID = result.TypeID ?? 0;
                    Utils.OpenVoucher(viewVoucherInvisible);
                }
            }
        }
    }
}
