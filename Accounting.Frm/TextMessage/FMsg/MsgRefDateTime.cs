﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting
{
    public partial class MsgRefDateTime : DialogForm
    {
        public DateTime RefDateTime;
        public MsgRefDateTime(DateTime refTime)
        {
            InitializeComponent();
            btnApply.Click += btnApply_Click;
            txtTime.Value = refTime;
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            RefDateTime = Convert.ToDateTime(txtTime.Value != null && txtTime.Value != DBNull.Value ? txtTime.Value : "00:00:00");
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
