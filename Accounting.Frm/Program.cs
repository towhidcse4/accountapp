﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting;
using Accounting.Model;
using Accounting.TextMessage;
using FX.Core;
using log4net.Config;
using log4net;
using Accounting.Frm;
using System.Data.SqlClient;
using System.Data;
using System.Xml;
using Accounting.Core.Common;
using System.Linq;
using System.Globalization;
using System.Windows.Forms;
namespace Accounting
{
    class Program
    {
        //public static bool isUpdate = false;
        //[assembly: log4net.Config.XmlConfigurator(Watch = false)]
        // private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
            ("Accounting.Program");
        //public static ProgramConfig SysCofig;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                log4net.Config.DOMConfigurator.Configure();

                log.Error("Start Application");
                SplashScreen.ShowSplashScreen();
                SplashScreen.SetStatus("Loading logging.config");
                #region Thiết lập NHibernate
                Bootstrapper.InitializeContainer();

                // log4net old
                //const string str = "\\Config\\logging.config";
                //XmlConfigurator.ConfigureAndWatch(new FileInfo(str));



                #endregion

                #region Custom Message Box
                //Reference https://www.codeproject.com/Articles/18399/Localizing-System-MessageBox
                MessageBoxManager.Yes = "Có";
                MessageBoxManager.No = "Không";
                MessageBoxManager.OK = "Đồng ý";
                MessageBoxManager.Cancel = "Bỏ Qua";
                MessageBoxManager.Register();
                #endregion

                #region Application.Run
                
                SplashScreen.SetStatus("Loading Style");
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                var UpdateUrl = System.Configuration.ConfigurationManager.AppSettings["UpdateUrl"];
                Version versionLastest = FrmCauHinh.GetLatestVersion(UpdateUrl);
                Version versionCurrent = FrmCauHinh.GetLocalVersionNumber();

                #region ThoHD: Overwrite AutoUpdate
                string appPath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\","") ;
                string easybooksPath = string.Format("{0}\\HDSAccounting", appPath);
                if (Directory.Exists(easybooksPath))
                {
                    try
                    {
                        Copy.CopyAllFile(easybooksPath, appPath, true);
                        Directory.Delete(easybooksPath, true);
                    }
                    catch (IOException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
                #endregion


                
                var setup = new FrmCauHinh();
                if (FrmCauHinh.ShallIDownloadTheLatest(versionCurrent, versionLastest))
                {
                    Application.Run(setup);
                }
                
                if (!setup.Exit)
                {
                    SplashScreen.SetStatus("Loading template");
                    string islFile = Properties.Settings.Default.Style;
                    using (System.IO.Stream stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(string.Format("Accounting.Styles.{0}", islFile)))
                        Infragistics.Win.AppStyling.StyleManager.Load(stream);
                    SplashScreen.SetStatus("Loading StartDate");
                    Application.DoEvents();
                    SplashScreen.SetStatus("Loading Application");
                    FStartApp app = new FStartApp(args);
                    Application.Run(app);
                    MessageBoxManager.Unregister();
                    Application.ExitThread();
                    SplashScreen.CloseForm();
                }
                #endregion
            }
            catch (Exception ex)
            {
                MSG.Warning(ex.ToString());
            }
        }
    }

}
