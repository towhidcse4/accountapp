﻿using Accounting.Core.DAO;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.Report;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinTree;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRTongHopCPTheoKhoanMucCP : Form
    {
        private readonly List<AccountTongHopCP> _lstnew = ReportUtils.LstAccountCP;
        private readonly List<ExpenseItemTongHopCP> _lstnew2 = ReportUtils.LstExpenseCP;
        private List<AccountTongHopCP> _lstAccountReport = new List<AccountTongHopCP>();
        private List<ExpenseItemTongHopCP> dsExpenseItem = new List<ExpenseItemTongHopCP>();
        private IExpenseItemService _IExpenseItemService
        {
            get { return IoC.Resolve<IExpenseItemService>(); }
        }
        string _subSystemCode;
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        public FRTongHopCPTheoKhoanMucCP()
        {
            InitializeComponent();
            this.uTreeCP.InitializeDataNode += new Infragistics.Win.UltraWinTree.InitializeDataNodeEventHandler(this.uTree_InitializeDataNode);
            this.uTreeCP.AfterDataNodesCollectionPopulated += new Infragistics.Win.UltraWinTree.AfterDataNodesCollectionPopulatedEventHandler(Utils.uTree_AfterDataNodesCollectionPopulated);
            this.uTreeCP.ColumnSetGenerated += new Infragistics.Win.UltraWinTree.ColumnSetGeneratedEventHandler(this.uTree_ColumnSetGenerated);
            //Gridivew vật tư
            _lstAccountReport = _lstnew;
            ugridAccountCP.SetDataBinding(_lstAccountReport.OrderBy(n => n.AccountNumber).ToList(), "");
            ReportUtils.ProcessControls(this);

            dsExpenseItem = _lstnew2;
            DataSet ds = Utils.ToDataSet<ExpenseItemTongHopCP>(dsExpenseItem, ConstDatabase.ExpenseItemTongHopCP_TableName);
            uTreeCP.SetDataBinding(ds, ConstDatabase.ExpenseItemTongHopCP_TableName);
            ConfigTree(uTreeCP);
            foreach (var node in uTreeCP.Nodes)
            {
                node.Cells["Check"].Value = false;
            }
            foreach (var row in ugridAccountCP.Rows)
            {
                row.Cells["Check"].Value = false;
            }
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dtBeginDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }
        
        private void btnOk_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }

            if (dtBeginDate.DateTime.Date > dtEndDate.DateTime.Date)
            {
                //MessageBox.Show("Ngày bắt đầu không thể lớn hơn ngày kết thúc", "Cảnh báo");
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            var lstAcc = ((List<AccountTongHopCP>)ugridAccountCP.DataSource).Where(c => c.Check).Select(x => x.AccountNumber).ToList();
            if (lstAcc.Count == 0)
            {
                MSG.Warning("Bạn chưa chọn tài khoản!");
                return;
            }
            var lst = _lstnew2.Where(x => x.Check).ToList();
            if (lst.Count == 0)
            {
                MSG.Warning("Bạn chưa chọn KMCP!");
                return;
            }         
            FRTongHopCPTheoKhoanMucCPex fm = new FRTongHopCPTheoKhoanMucCPex((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, lstAcc, lst);
            fm.Show(this);
            

        }

        private void uTree_ColumnSetGenerated(object sender, ColumnSetGeneratedEventArgs e)
        {
            Utils.uTree_ColumnSetGenerated(sender, e, ConstDatabase.ExpenseItemTongHopCP_TableName);
        }
        private void uTree_InitializeDataNode(object sender, InitializeDataNodeEventArgs e)
        {
            Utils.uTree_InitializeDataNode(sender, e, ConstDatabase.ExpenseItemTongHopCP_TableName);
        }
        void ConfigTree(Infragistics.Win.UltraWinTree.UltraTree ultraTree)
        {//hàm chung
            Utils.ConfigTree(ultraTree, TextMessage.ConstDatabase.ExpenseItemTongHopCP_TableName);
        }
        private void uTreeCP_Click(object sender, EventArgs e)
        {
            if (uTreeCP.SelectedNodes.Count == 0 && uTreeCP.ActiveNode != null)
            {
                uTreeCP.ActiveNode.Selected = true;
            }
            if (uTreeCP.SelectedNodes.Count > 0)
            {
                if ((bool)uTreeCP.SelectedNodes[0].Cells["Check"].Value == false)
                {
                    uTreeCP.SelectedNodes[0].Cells["Check"].Value = true;
                }
                else
                {
                    uTreeCP.SelectedNodes[0].Cells["Check"].Value = false;
                }
                var id = (Guid)uTreeCP.SelectedNodes[0].Cells["ID"].Value;
                _lstnew2.FirstOrDefault(x => x.ID == id).Check = (bool)uTreeCP.SelectedNodes[0].Cells["Check"].Value;
                var lstID = new List<Guid>();
                getListID(lstID, id);
                foreach (var node in uTreeCP.Nodes)
                {
                    if (lstID.Any(d => d == (Guid)node.Cells["ID"].Value))
                    {
                        _lstnew2.FirstOrDefault(x => x.ID == (Guid)node.Cells["ID"].Value).Check = (bool)uTreeCP.SelectedNodes[0].Cells["Check"].Value;
                        node.Cells["Check"].Value = uTreeCP.SelectedNodes[0].Cells["Check"].Value;
                    }
                }
            }
        }
        void getListID(List<Guid> lstID, Guid parent)
        {
            foreach (var x in _lstnew2)
            {
                if (x.ParentID == parent)
                {
                    lstID.Add(x.ID);
                    if (x.IsParentNode) getListID(lstID, x.ID);
                }
            }
        }
    }
}
