﻿using Accounting.Core.DAO;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.Report;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm.FReport
{
    public partial class FRSOCHITIETMUAHANG : Form
    {
        private readonly IMaterialGoodsService _IMaterialGoodsService;
        private List<MaterialGoods> _lstMaterialGoods = new List<MaterialGoods>();
        private List<MaterialGoods> _lstMaterialGoodsInitial = new List<MaterialGoods>();
        private readonly List<AccountingObjectReport> _lstnew = ReportUtils.LstProvider;
        List<AccountingObjectReport> _lstAccountingObjectReport = new List<AccountingObjectReport>();
        int _soDVT = 1; //check so luong dvt neu = 1 thi co tong so luong, con lai thi ko co tong so luong
        string _subSystemCode;
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        public FRSOCHITIETMUAHANG()
        {
            InitializeComponent();
            _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
            //string path = System.IO.Path.GetDirectoryName(
            //    System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            //fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\SoChiTietMuaHang.rst", path);
            //Gridivew vật tư
            _lstMaterialGoodsInitial = _IMaterialGoodsService.Query.Where(c => c.IsActive).OrderBy(n => n.MaterialGoodsCode).ToList();
            _lstMaterialGoods = Utils.CloneObject(_lstMaterialGoodsInitial);
            ugridProduce.SetDataBinding(_lstMaterialGoods.ToList(), "");
            string columnKey = "Status";
            List<TemplateColumn> list = new List<TemplateColumn>();
            TemplateColumn item1 = new TemplateColumn();
            item1.ColumnName = "MaterialGoodsCode";
            item1.ColumnCaption = "Mã hàng";
            TemplateColumn item2 = new TemplateColumn();
            item2.ColumnName = "MaterialGoodsName";
            item2.ColumnCaption = "Tên hàng";
            TemplateColumn item3 = new TemplateColumn();
            item3.ColumnName = "Status";
            list.Add(item1);
            list.Add(item2);
            list.Add(item3);
            SetConfigGV_Account(ugridProduce, _lstMaterialGoods.Cast<object>().ToList(), list, ConstDatabase.MaterialGoods_TableName, columnKey);

            //Gridivew nhà cung cấp
            _lstAccountingObjectReport = _lstnew;
            ugridNhaCC.SetDataBinding(_lstAccountingObjectReport.OrderBy(n => n.AccountingObjectCode).ToList(), "");
            string columnKey2 = "Check";
            List<TemplateColumn> list2 = new List<TemplateColumn>();
            TemplateColumn item_1 = new TemplateColumn();
            item_1.ColumnName = "AccountingObjectCode";
            item_1.ColumnCaption = "Mã NCC";
            TemplateColumn item_2 = new TemplateColumn();
            item_2.ColumnName = "AccountingObjectName";
            item_2.ColumnCaption = "Tên NCC";
            TemplateColumn item_3 = new TemplateColumn();
            item_3.ColumnName = "Check";
            list2.Add(item_1);
            list2.Add(item_2);
            list2.Add(item_3);
            SetConfigGV_Account(ugridNhaCC, _lstAccountingObjectReport.Cast<object>().ToList(), list2, ConstDatabase.AccountingObjectGroup_TableName, columnKey2);
            ReportUtils.ProcessControls(this);
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dtBeginDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }

        }
        private void SetConfigGV_Account(UltraGrid ultraGrid, List<object> data, List<TemplateColumn> list, string constantKey, string columnKey = "Check")
        {
            if (data.Count > 0) ultraGrid.SetDataBinding(data, "");
            Utils.ConfigGrid(ultraGrid, constantKey, list);

            ultraGrid.DisplayLayout.Bands[0].Summaries.Clear();
            ultraGrid.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
            ultraGrid.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.True;
            ultraGrid.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            ultraGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            ultraGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            for (int i = 0; i < ultraGrid.DisplayLayout.Bands[0].Columns.Count; i++)
            {
                if (ultraGrid.DisplayLayout.Bands[0].Columns[i].Key == columnKey)
                    ultraGrid.DisplayLayout.Bands[0].Columns[i].CellActivation = Activation.AllowEdit;
                else
                    ultraGrid.DisplayLayout.Bands[0].Columns[i].CellActivation = Activation.NoEdit;
            }
            UltraGridBand band = ultraGrid.DisplayLayout.Bands[0];

            if (band != null && band.Columns.Exists(columnKey))
            {
                UltraGridColumn ugc = band.Columns[columnKey];
                ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
                ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
                ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;

                //ultraGrid.DisplayLayout.Bands[0].Columns[columnKey].CellActivation = Activation.NoEdit;

                band.Columns[columnKey].Header.VisiblePosition = 0;
                band.Columns[columnKey].Width = 40;
                band.Columns[columnKey].Header.Fixed = true;
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }

            if((DateTime)dtBeginDate.Value > (DateTime)dtEndDate.Value)
            {
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }


            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(this.Name, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = this.Name;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            var dsnhacc = _lstAccountingObjectReport.Where(t => t.Check).Select(c => c.ID.ToString());
            var dsvattu = _lstMaterialGoods.Where(t => t.Status).Select(c => c.ID.ToString());

            if(dsnhacc.Count() == 0 && dsvattu.Count() == 0)
            {
                MSG.Warning("Bạn chưa chọn mã hàng và nhà cung cấp, xin kiểm tra lại");
                return;
            }

            var dsAccountID = dsnhacc.Count() > 0 ? "," + string.Join(",", dsnhacc.ToArray()) + "," : ""; //edit by cuongpv dsvattu.Count() -> dsnhacc.Count()
            var dsVatTuID = dsvattu.Count() > 0 ? "," + string.Join(",", dsvattu.ToArray()) + "," : "";
            FSOCHITIETMUAHANGex fm = new FSOCHITIETMUAHANGex((DateTime)dtBeginDate.Value, (DateTime)dtEndDate.Value, dsAccountID, dsVatTuID);
            fm.Show(this);           
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            foreach (var item in ugridNhaCC.Rows)
            {
                item.Cells["Check"].Value = false;
            }
            foreach (var item in ugridProduce.Rows)
            {
                item.Cells["Status"].Value = false;
            }
            Close();
        }      

        private void Form_FormClosed(object sender, FormClosedEventArgs e)
        {
            foreach (var item in ugridNhaCC.Rows)
            {
                item.Cells["Check"].Value = false;
            }
            foreach (var item in ugridProduce.Rows)
            {
                item.Cells["Status"].Value = false;
            }
        }

        private void reportManager1_GetReportParameter(object sender, PerpetuumSoft.Reporting.Components.GetReportParameterEventArgs e)
        {
            e.Parameters["Sodvt"].Value = _soDVT.ToString();
        }
    }
}
