﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinTabControl;
using Type = Accounting.Core.Domain.Type;
using FX.Core;
using Itenso.TimePeriod;
using Infragistics.Win;
using Infragistics.Win.Misc;

namespace Accounting
{
    public partial class UTopVouchers<T> : UserControl
    {
        private string _noBindName;
        private string _postedDateBindName;
        private string _dateBindName;
        private int? _typeGroup;
        private bool _haveType;
        private string _typeID;
        private object _objectDataSource;
        private int typeid; //add by namnh
        private T select;
        private int statusForm;
        Stopwatch _stopWatch = new Stopwatch();
        private IGenCodeService _iGenCodeService;

        public string No
        {
            get { return txtNo.Text; }
            set
            {
                txtNo.Text = value;
                txtNo.Update();
                txtNo.Refresh();
            }
        }
        public DateTime PostedDate
        {
            get { return dtePostedDate.DateTime; }
            set { dtePostedDate.DateTime = value; }
        }
        public DateTime Date
        {
            get { return dteDate.DateTime; }
            set { dteDate.DateTime = value; }
        }

        public bool PostedDateVisible
        {
            get { return dtePostedDate.Visible; }
            set
            {
                dtePostedDate.Visible = value;
                lblPostedDate.Visible = value;
                //if (!value)
                //{
                //    dtePostedDate.Location = new System.Drawing.Point(198, 24);
                //    lblPostedDate.Location = new System.Drawing.Point(198, 0);
                //}
                //else
                //{
                //    dtePostedDate.Location = new System.Drawing.Point(99, 24);
                //    lblPostedDate.Location = new System.Drawing.Point(99, 0);
                //}
            }
        }

        public bool DateVisible
        {
            get { return dteDate.Visible; }
            set
            {
                dteDate.Visible = value;
                lblDate.Visible = value;
            }
        }

        public string NoBindName
        {
            get { return _noBindName; }
        }

        public string PostedDateBindName
        {
            get { return _postedDateBindName; }
        }

        public string DateBindName
        {
            get { return _dateBindName; }
        }

        public int? TypeGroup
        {
            get { return _typeGroup; }
        }

        public bool HaveType
        {
            get { return _haveType; }
        }

        public string TypeId
        {
            get { return _typeID; }
        }

        public object ObjectDataSource
        {
            get { return _objectDataSource; }
        }

        public void FocusDate()
        {
            dteDate.Focus();
        }

        public void FocusPostedDate()
        {
            dtePostedDate.Focus();
        }

        public void FocusNo()
        {
            txtNo.Focus();
        }

        /// <summary>
        /// Vùng chứng từ
        /// </summary>
        /// <param name="this">Form hiện tại</param>
        /// <param name="no">Tên thuộc tính [No] sẽ được Binding của _select</param>
        /// <param name="postedDate">Tên thuộc tính [PostedDate] sẽ được Binding của _select</param>
        /// <param name="date">Tên thuộc tính [Date] sẽ được Binding của _select</param>
        /// <param name="typeGroup">TypeGroup của _select, mặc định = null sẽ tự động lấy TypeGroup theo TypeID</param>
        /// <param name="haveType">Có cột [Loại chứng từ] hay không. [Default = false]</param>
        /// <param name="typeID"> </param>
        /// <param name="objectDataSource">Source to Binding</param>
        /// <param name="resetNo">Có làm mới số chứng từ không </param>
        /// <param name="fieldNo">Nhãn ô Số chứng từ mới</param>
        /// <param name="fieldDate">Nhãn ô Ngày chứng từ mới</param>
        public UTopVouchers(Form @this, string no = null, string postedDate = null, string date = null, int? typeGroup = null, bool haveType = false, string typeID = null, object objectDataSource = null, bool resetNo = false, string fieldNo = null, string fieldDate = null, DateTime? dateTime = null)
        {
            InitializeComponent();
            _objectDataSource = objectDataSource = objectDataSource ?? ((DetailBase<T>)@this)._select;
            dtePostedDate.Leave += (s, e) => dtePostedDate_Leave(s, e, @this);
            dteDate.Leave += (s, e) => dteDate_Leave(s, e, @this);
            //select = (T)objectDataSource;
            cbbType.RowSelected += (sender, e) => cbbType_RowSelected(sender, e, objectDataSource, @this);

            statusForm = ((DetailBase<T>)@this)._statusForm;
            txtNo.DataBindings.Clear();
            txtNo.DataBindings.Add("Text", new BindingSource { DataSource = objectDataSource }, string.IsNullOrEmpty(no) ? "No" : no, true, DataSourceUpdateMode.OnPropertyChanged);
            if (!string.IsNullOrEmpty(postedDate))
            {
                dtePostedDate.TabIndex = 3;
                dtePostedDate.DataBindings.Clear();
                dtePostedDate.DataBindings.Add("Value", new BindingSource { DataSource = objectDataSource }, string.IsNullOrEmpty(postedDate) ? "PostedDate" : postedDate, true, DataSourceUpdateMode.OnPropertyChanged);
                dtePostedDate.ValueChanged += new EventHandler((s, e) => dtePostedDate_ValueChanged(s, e, @this));
            }
            if (!string.IsNullOrEmpty(date))
            {
                dteDate.TabIndex = 2;
                dteDate.DataBindings.Clear();
                dteDate.DataBindings.Add("Value", new BindingSource { DataSource = objectDataSource }, string.IsNullOrEmpty(date) ? "Date" : date, true, DataSourceUpdateMode.OnPropertyChanged);
            }
            
            
            if (statusForm.Equals(ConstFrm.optStatusForm.Add))
            {
                string _no = string.Empty;
                DateTime? _date = DateTime.Now;
                DateTime? _postedDate = DateTime.Now;
                PropertyInfo propNo = objectDataSource.GetType().GetProperty(string.IsNullOrEmpty(no) ? "No" : no);
                if (propNo != null && propNo.CanWrite)
                    _no = (string)propNo.GetValue(objectDataSource, null);
                PropertyInfo propDate = objectDataSource.GetType().GetProperty(string.IsNullOrEmpty(date) ? "Date" : date);
                if (propDate != null && propDate.CanWrite)
                    _date = (DateTime?)propDate.GetValue(objectDataSource, null);
                PropertyInfo propPostedDate = objectDataSource.GetType().GetProperty(string.IsNullOrEmpty(postedDate) ? "PostedDate" : postedDate);
                if (propPostedDate != null && propPostedDate.CanWrite)
                    _postedDate = (DateTime?)propPostedDate.GetValue(objectDataSource, null);
                txtNo.Focus();
                if (resetNo)
                    objectDataSource.SetProperty(string.IsNullOrEmpty(no) ? "No" : no, "");
                
                #region THOHD - Tạo mới số chứng từ và cập nhật số chứng từ 
                // (xử lý việc trùng lặp số chứng từ khi thực hiện theo tác trên 2 máy)
                typeid = ((DetailBase<T>)@this).TypeID == 840 ? 600 : ((DetailBase<T>)@this).TypeID;
                var TypeGroupID = typeGroup == null ? (int)Utils.ListType.FirstOrDefault(p => p.ID == typeid).TypeGroupID : (int)typeGroup;
                //_iGenCodeService.BeginTran();
                try
                {
                    if (((DetailBase<T>)@this)._select.GetProperty<T, String>("ID") == null && typeid != 120)
                    {
                        txtNo.Text = (!string.IsNullOrEmpty(_no) && !resetNo) ? _no : Utils.TaoMaChungTu(Utils.IGenCodeService.getGenCode(TypeGroupID));
                        txtNo.Update();
                        //Utils.IGenCodeService.UpdateGenCodeForm(TypeGroupID, txtNo.Text);
                        //Utils.IGenCodeService.CommitChanges();
                    }
                }
                catch (Exception ex)
                {
                    MSG.Error("Lỗi tạo số chứng từ " + txtNo.Text + "\r\n" + ex.ToString());
                }

                #endregion

                if (dateTime == null)
                {
                    dteDate.DateTime = (_date == null || _date == Utils.GetDateTimeDefault() || _date == Utils.GetDateTimeEmpty()) ? (Utils.StringToDateTime(ConstFrm.DbStartDate) ?? DateTime.Now) : (DateTime)_date;
                    dteDate.Update();
                    dtePostedDate.DateTime = (_postedDate == null || _postedDate == Utils.GetDateTimeDefault() || _postedDate == Utils.GetDateTimeEmpty()) ? (Utils.StringToDateTime(ConstFrm.DbStartDate) ?? DateTime.Now) : (DateTime)_postedDate;
                    dtePostedDate.Update();
                }
                else
                {
                    dteDate.DateTime = dateTime ?? DateTime.Now;
                    dteDate.Update();
                    dtePostedDate.DateTime = dateTime ?? DateTime.Now;
                    dtePostedDate.Update();
                }

            }
            if (statusForm.Equals(ConstFrm.optStatusForm.Edit) && ((ObjectDataSource.HasProperty("TypeID") ? ObjectDataSource.GetProperty("TypeID").ToString().StartsWith("26") : false) || (ObjectDataSource.HasProperty("TypeID") ? ObjectDataSource.GetProperty("TypeID").ToString().StartsWith("22") : false) || (ObjectDataSource.HasProperty("TypeID") ? ObjectDataSource.GetProperty("TypeID").ToString().StartsWith("33") : false) || (ObjectDataSource.HasProperty("TypeID") ? new List<int>{ 320,321,322,323,324,325,260}.Contains(ObjectDataSource.GetProperty("TypeID").ToInt()) : false)))
            {
                string _no = string.Empty;
                DateTime? _date = DateTime.Now;
                DateTime? _postedDate = DateTime.Now;
                PropertyInfo propNo = objectDataSource.GetType().GetProperty(string.IsNullOrEmpty(no) ? "No" : no);
                if (propNo != null && propNo.CanWrite)
                    _no = (string)propNo.GetValue(objectDataSource, null);
                PropertyInfo propDate = objectDataSource.GetType().GetProperty(string.IsNullOrEmpty(date) ? "Date" : date);
                if (propDate != null && propDate.CanWrite)
                    _date = (DateTime?)propDate.GetValue(objectDataSource, null);
                PropertyInfo propPostedDate = objectDataSource.GetType().GetProperty(string.IsNullOrEmpty(postedDate) ? "PostedDate" : postedDate);
                if (propPostedDate != null && propPostedDate.CanWrite)
                    _postedDate = (DateTime?)propPostedDate.GetValue(objectDataSource, null);
                txtNo.Focus();
                if (resetNo)
                    objectDataSource.SetProperty(string.IsNullOrEmpty(no) ? "No" : no, "");

                #region THOHD - Tạo mới số chứng từ và cập nhật số chứng từ 
                // (xử lý việc trùng lặp số chứng từ khi thực hiện theo tác trên 2 máy)
                typeid = ((DetailBase<T>)@this).TypeID == 840 ? 600 : ((DetailBase<T>)@this).TypeID;
                var TypeGroupID = typeGroup == null ? (int)Utils.ListType.FirstOrDefault(p => p.ID == typeid).TypeGroupID : (int)typeGroup;
                //_iGenCodeService.BeginTran();
                try
                {
                    if (((DetailBase<T>)@this)._select.GetProperty<T, String>("ID") == null && typeid != 120)
                    {
                        txtNo.Text = (!string.IsNullOrEmpty(_no) && !resetNo) ? _no : Utils.TaoMaChungTu(Utils.IGenCodeService.getGenCode(TypeGroupID));
                        txtNo.Update();
                        //Utils.IGenCodeService.UpdateGenCodeForm(TypeGroupID, txtNo.Text);
                        //Utils.IGenCodeService.CommitChanges();
                    }
                }
                catch (Exception ex)
                {
                    MSG.Error("Lỗi tạo số chứng từ " + txtNo.Text + "\r\n" + ex.ToString());
                }

                #endregion

                if (dateTime == null)
                {
                    dteDate.DateTime = (_date == null || _date == Utils.GetDateTimeDefault() || _date == Utils.GetDateTimeEmpty()) ? (Utils.StringToDateTime(ConstFrm.DbStartDate) ?? DateTime.Now) : (DateTime)_date;
                    dteDate.Update();
                    dtePostedDate.DateTime = (_postedDate == null || _postedDate == Utils.GetDateTimeDefault() || _postedDate == Utils.GetDateTimeEmpty()) ? (Utils.StringToDateTime(ConstFrm.DbStartDate) ?? DateTime.Now) : (DateTime)_postedDate;
                    dtePostedDate.Update();
                }
                else
                {
                    dteDate.DateTime = dateTime ?? DateTime.Now;
                    dteDate.Update();
                    dtePostedDate.DateTime = dateTime ?? DateTime.Now;
                    dtePostedDate.Update();
                }

            }
            cbbType.DataBindings.Clear();
            if (!haveType)
            {
                palType.Visible = false;
                palVouchers.Location = new Point(0, 0);
            }
            else
            {
                palType.Visible = true;
                palVouchers.Location = new Point(117, 0);
                List<int> lstId = new List<int>(new int[] { 120, 130, 140 });
                List<Accounting.Core.Domain.Type> lstType = Utils.ListType.Where(c => lstId.Contains(c.ID)).ToList();
                cbbType.DataSource = lstType;
                cbbType.DisplayMember = "TypeName";
                cbbType.ValueMember = "ID";
                Utils.ConfigGrid(cbbType, ConstDatabase.Type_TableName);
                cbbType.DataBindings.Add("Value", new BindingSource { DataSource = ((DetailBase<T>)@this)._select },
                                         string.IsNullOrEmpty(typeID) ? "TypeID" : typeID, true, DataSourceUpdateMode.OnPropertyChanged);
                cbbType.DisplayLayout.Bands[0].Columns["ID"].Hidden = true;
                cbbType.DisplayLayout.Bands[0].ColHeadersVisible = false;
                //PropertyInfo propTypeId = (((DetailBase<T>) @this)._select).GetType().GetProperty("TypeID");
                //if (propTypeId != null && propTypeId.CanWrite)
                //{
                //    cbbType.Value = propTypeId.GetValue(((DetailBase<T>) @this)._select, null);
                //}
            }
            
            if (statusForm.Equals(ConstFrm.optStatusForm.Add)&& typeid == 120)// add by namnh
                cbbType.SelectedRow = cbbType.Rows[0];
            _noBindName = no;
            _postedDateBindName = postedDate;
            _dateBindName = date;
            _typeGroup = typeGroup;
            _haveType = haveType;
            _typeID = typeID;
            _objectDataSource = objectDataSource;
            int typeId = 0;
            if (objectDataSource.HasProperty("TypeID"))
                typeId = (int)objectDataSource.GetProperty("TypeID");
            if (typeId == 540 && ((DetailBase<T>)@this)._statusForm == ConstFrm.optStatusForm.Add)
            {
                dteDate.DateTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                dteDate.Update();
            }
            dtePostedDate.ButtonsRight.Clear();
            if (typeId.ToString(CultureInfo.InvariantCulture).StartsWith("40") || typeId.ToString(CultureInfo.InvariantCulture).StartsWith("41") || new[] { 900, 901, 420 }.Any(x => x == typeId) || new[] { "IWNo", "OWNo", "InwardNo", "OutwardNo" }.Any(x => x == no))
            {
                var btnRefDateTime = new EditorButton("btnRefDateTime");
                var appearance = new Infragistics.Win.Appearance
                {
                    Image = Properties.Resources.clock,
                    ImageHAlign = Infragistics.Win.HAlign.Center,
                    ImageVAlign = Infragistics.Win.VAlign.Middle
                };
                btnRefDateTime.Appearance = appearance;
                btnRefDateTime.Key = "btnRefDateTime";
                btnRefDateTime.Click += (s, e) => btnRefDateTime_Click(ref objectDataSource, s, e);
                dtePostedDate.ButtonsRight.Add(btnRefDateTime);
                dtePostedDate.CreationFilter = new CreationFilterForRefDateTime();
                dtePostedDate.TextChanged += new EventHandler((s, e) => dtePostedDate_TextChanged(s, e, @this));
            }
            if (new[] { 180 }.Any(x => x == typeId))
            {
                var btnRefDateTime = new EditorButton("btnRefDateTime");
                var appearance = new Infragistics.Win.Appearance
                {
                    Image = Properties.Resources.clock,
                    ImageHAlign = Infragistics.Win.HAlign.Center,
                    ImageVAlign = Infragistics.Win.VAlign.Middle,
                };
                btnRefDateTime.Appearance = appearance;
                btnRefDateTime.Key = "btnRefDateTime";
                btnRefDateTime.Click += (s, e) => btnRefDateTime_Click(ref objectDataSource, s, e);
                dteDate.ButtonsRight.Add(btnRefDateTime);
                dteDate.CreationFilter = new CreationFilterForRefDateTime();
                dteDate.TextChanged += new EventHandler((s, e) => dtePostedDate_TextChanged(s, e, @this));
            }
            if (new[] { 900, 901 }.Any(x => x == typeId))
            {
                if (no == "IWNo")
                    AddRefDateTimeByKey("RefDateTimeIn");
                else if (no == "OWNo")
                    AddRefDateTimeByKey("RefDateTimeOut");
            }
            else if (_objectDataSource.HasProperty("TypeID") && new[] { 320, 321, 322, 323, 324, 325 }.Any(x => x == (int?)_objectDataSource.GetProperty("TypeID")))
            {
                //AddRefDateTimeByKey("ModifiedDate", getText: true);
                _objectDataSource.SetProperty("ModifiedDate", DateTime.Now);
            }
            else
                AddRefDateTimeByKey("RefDateTime");
            if (fieldNo != null) lblNo.Text = fieldNo;
            if (fieldDate != null) lblDate.Text = fieldDate;
        }

        void AddRefDateTimeByKey(string key, DateTime? refDateTime = null, bool getText = false)
        {
            if (_objectDataSource.HasProperty(key))
            {
                if (refDateTime == null && key != "ModifiedDate")
                    refDateTime = (DateTime?)_objectDataSource.GetProperty(key);
                dtePostedDate.Update();
                _objectDataSource.SetProperty(key, GetRefDateTime((refDateTime == null || refDateTime == Utils.GetDateTimeDefault() || refDateTime == Utils.GetDateTimeEmpty()) ? DateTime.Now : (DateTime)refDateTime, !getText ? dtePostedDate.DateTime : (Utils.StringToDateTime(dtePostedDate.Text) ?? DateTime.Now)));
            }
        }

        private void btnRefDateTime_Click(ref object input, object sender, EditorButtonEventArgs e)
        {
            try
            {
                var refTime = DateTime.Now;
                if (!_objectDataSource.HasProperty("TypeID")) return;
                var TypeId = (int?)_objectDataSource.GetProperty("TypeID");
                if (new[] { 900, 901 }.Any(x => x == TypeId))
                {
                    if (_noBindName == "IWNo")
                    {
                        if (input.HasProperty("RefDateTimeIn"))
                            refTime = (DateTime?)input.GetProperty("RefDateTimeIn") ?? refTime;
                    }
                    else if (_noBindName == "OWNo")
                    {
                        if (input.HasProperty("RefDateTimeOut"))
                            refTime = (DateTime?)input.GetProperty("RefDateTimeOut") ?? refTime;
                    }
                }
                else if (input.HasProperty("RefDateTime"))
                    refTime = (DateTime?)input.GetProperty("RefDateTime") ?? refTime;

                var f = new MsgRefDateTime(refTime);
                f.FormClosed += new FormClosedEventHandler(MsgRefDateTime_FormClosed);
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }

        private void MsgRefDateTime_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (e.CloseReason != CloseReason.UserClosing) return;
            var frm = (MsgRefDateTime)sender;
            if (frm.DialogResult != DialogResult.OK) return;
            var refTime = frm.RefDateTime;
            var refDateTime = GetRefDateTime(refTime, Utils.StringToDateTime(dtePostedDate.Text) ?? DateTime.Now);
            if (_objectDataSource.HasProperty("TypeID") && new[] { 900, 901 }.Any(x => x == (int?)_objectDataSource.GetProperty("TypeID")))
            {
                if (_noBindName == "IWNo")
                    AddRefDateTimeByKey("RefDateTimeIn", refDateTime);
                else if (_noBindName == "OWNo")
                    AddRefDateTimeByKey("RefDateTimeOut", refDateTime);
            }
            else if (_objectDataSource.HasProperty("TypeID") && new[] { 180 }.Any(x => x == (int?)_objectDataSource.GetProperty("TypeID")))
            {
                AddRefDateTimeByKey("Date", refDateTime);
            }
            else
                AddRefDateTimeByKey("RefDateTime", refDateTime);
        }

        DateTime GetRefDateTime(DateTime refTime, DateTime postedDate)
        {
            return new DateTime(postedDate.Year, postedDate.Month, postedDate.Day, refTime.Hour, refTime.Minute, refTime.Second, refTime.Millisecond);
        }

        public void Reset(Form @this)
        {
            string nameNo = txtNo.DataBindings[0].BindingMemberInfo.BindingMember;
            string nameDate = dteDate.DataBindings[0].BindingMemberInfo.BindingMember;
            string namePostedDate = dtePostedDate.DataBindings[0].BindingMemberInfo.BindingMember;
            PropertyInfo propNo = _objectDataSource.GetType().GetProperty(nameNo);
            if (propNo != null && propNo.CanWrite)
                txtNo.Text = propNo.GetValue(_objectDataSource, null).ToString();
            PropertyInfo propDate = _objectDataSource.GetType().GetProperty(nameDate);
            if (propDate != null && propDate.CanWrite)
                dteDate.Value = propDate.GetValue(_objectDataSource, null).ToString();
            PropertyInfo propPostedDate = _objectDataSource.GetType().GetProperty(namePostedDate);
            if (propPostedDate != null && propPostedDate.CanWrite)
                dtePostedDate.Value = propPostedDate.GetValue(_objectDataSource, null).ToString();
        }
        public Infragistics.Win.UltraWinGrid.RowSelectedEventHandler CbbType_RowSelected;
        private void cbbType_RowSelected(object sender, Infragistics.Win.UltraWinGrid.RowSelectedEventArgs e, object objectDataSource, Form @this)
        {
            if (palType.Visible && !statusForm.Equals(ConstFrm.optStatusForm.View))
            {
                int _id = (int)e.Row.Cells["ID"].Value;
                PropertyInfo propTypeId = objectDataSource.GetType().GetProperty("TypeID");
                if (propTypeId != null && propTypeId.CanWrite)
                {
                    propTypeId.SetValue(objectDataSource, _id, null);
                }
                cbbType.Update();
                string _no = txtNo.Text;
                Type type = Utils.ListType.FirstOrDefault(p => p.ID == _id);
                if (type != null)
                    if (type.TypeGroupID != null)
                        _no =
                            Utils.TaoMaChungTu(
                                Utils.IGenCodeService.getGenCode((int)type.TypeGroupID));
                txtNo.Text = _no;
                txtNo.Update();
                txtNo.Refresh();
                if (_id.ToString().StartsWith("12") || _id.ToString().StartsWith("13") || _id.ToString().StartsWith("14"))
                {
                    UltraGroupBox gr = (UltraGroupBox)@this.Controls.Find("grpTop", true).FirstOrDefault();
                    if (gr != null) gr.Text = cbbType.Text.ToUpper();
                }
            }
            if (CbbType_RowSelected != null)
            {
                CbbType_RowSelected(sender, e);
            }
        }

        private void dteDate_AfterExitEditMode(object sender, EventArgs e)
        {
            ConfigDate();
        }

        private void dtePostedDate_AfterExitEditMode(object sender, EventArgs e)
        {
            ConfigPostedDate();
        }

        private void ConfigPostedDate()
        {
            try
            {
                BindingSource bSource = dteDate.DataBindings[0].DataSource as BindingSource;
                T temp = (T)(bSource == null ? dteDate.DataBindings[0].DataSource : bSource.DataSource);
                foreach (var property in temp.GetType().GetProperties())
                {
                    if (property.Name.Equals(dteDate.DataBindings[0].BindingMemberInfo.BindingMember)) continue;
                    if (property.Name.Contains("PostedDate") && !property.Name.Equals("FinalDate"))
                    {
                        if (property.CanRead && property.CanWrite)
                        {
                            property.SetValue(temp, dtePostedDate.DateTime, null);
                        }
                    }
                }
                if (bSource != null)
                {
                    bSource.EndEdit();
                    bSource.ResetBindings(false);
                    //SendKeys.Send("{TAB}");
                    //SendKeys.Send("+{TAB}");
                }
            }
            catch
            {

            }
        }

        private void ConfigDate()
        {
            var bSource = dteDate.DataBindings[0].DataSource as BindingSource;
            T temp = (T)(bSource == null ? dteDate.DataBindings[0].DataSource : bSource.DataSource);
            foreach (var property in temp.GetType().GetProperties())
            {
                if (property.Name.Equals(dteDate.DataBindings[0].BindingMemberInfo.BindingMember)) continue;
                if (property.Name.Contains("Date") && !property.Name.Contains("PostedDate") && !property.Name.Equals("FinalDate"))
                {
                    if (property.CanRead && property.CanWrite)
                    {
                        property.SetValue(temp, dteDate.DateTime, null);
                    }
                }
            }
            if (bSource != null)
            {
                bSource.EndEdit();
                bSource.ResetBindings(false);
                //SendKeys.Send("{TAB}");
                //SendKeys.Send("+{TAB}");
            }
        }
        /// <summary>
        /// Các thuộc tính ngày tháng được thay đổi theo ngày hạch toán
        /// </summary>
        /// <param name="this"></param>
        void ConfigDateByPostedDate(Form @this)
        {
            //Thay đổi trong phần chính
            if (((DetailBase<T>)@this)._select.HasProperty("DueDate") &&
                ((DetailBase<T>)@this)._select.GetProperty("DueDate") != null)
            {
                var uGridControl = @this.Controls.Find("uGridControl", true).FirstOrDefault() as UltraGrid;
                if (uGridControl != null && uGridControl.Rows.Count > 0)
                {
                    uGridControl.Rows[0].Cells["DueDate"].Value = dtePostedDate.Value;
                    uGridControl.Rows[0].Refresh(RefreshRow.ReloadData);
                    uGridControl.Rows[0].Refresh(RefreshRow.RefreshDisplay);
                    return;
                }
                ((DetailBase<T>)@this)._select.SetProperty("DueDate", dteDate.Value);
            }
            //Thay đổi trong phần chi tiết
            var keys = new[] { "VATPostedDate" };
            var uTabControl = @this.Controls.Find("ultraTabControl", true).FirstOrDefault() as UltraTabControl;
            if (uTabControl != null)
            {
                foreach (var tab in uTabControl.Tabs)
                {
                    var uGrid = @this.Controls.Find(string.Format("uGrid{0}", tab.Index), true).FirstOrDefault() as UltraGrid;
                    if (uGrid != null)
                    {
                        var newKeys = keys.Where(key => uGrid.DisplayLayout.Bands[0].Columns.Exists(key)).ToList();
                        foreach (var row in uGrid.Rows)
                        {
                            foreach (var key in newKeys)
                            {
                                row.Cells[key].Value = dtePostedDate.Value;
                                row.Refresh(RefreshRow.RefreshDisplay);
                                row.Refresh(RefreshRow.ReloadData);
                            }
                        }
                    }
                }
            }
            else
            {
                var uGrid = @this.Controls.Find("uGrid0", true).FirstOrDefault() as UltraGrid;
                if (uGrid != null)
                {
                    var newKeys = keys.Where(key => uGrid.DisplayLayout.Bands[0].Columns.Exists(key)).ToList();
                    foreach (var row in uGrid.Rows)
                    {
                        foreach (var key in newKeys)
                        {
                            row.Cells[key].Value = dtePostedDate.Value;
                            row.Refresh(RefreshRow.RefreshDisplay);
                            row.Refresh(RefreshRow.ReloadData);
                        }
                    }
                }
            }
        }

        private void dtePostedDate_Leave(object sender, EventArgs e, Form @this)
        {
            if (checkDate((UltraDateTimeEditor)sender) == true)
            {
                ConfigDateByPostedDate(@this);
            }
        }

        private void dteDate_Leave(object sender, EventArgs e, Form @this)
        {
            if (checkDate((UltraDateTimeEditor)sender) == true)
            {
                dtePostedDate.Value = dteDate.Value.CloneObject();
                ConfigDateByPostedDate(@this);
                ConfigPostedDate();
                @this.ReloadControlByDataBinding<T>();
                //if (((DetailBase<T>)@this)._select.HasProperty("DueDate") && ((DetailBase<T>)@this)._select.GetProperty("DueDate") != null)
                //{
                //    var uGridControl = @this.Controls.Find("uGridControl", true).FirstOrDefault() as UltraGrid;
                //    if (uGridControl != null)
                //    {
                //        uGridControl.Rows[0].Cells["DueDate"].Value = dteDate.Value;
                //        return;
                //    }
                //    ((DetailBase<T>)@this)._select.SetProperty("DueDate", dteDate.Value);
                //}
            }
        }

        private bool? checkDate(UltraDateTimeEditor date)
        {
            if (_stopWatch.IsRunning)
            {
                _stopWatch.Stop();
                return null;
            }
            _stopWatch.Reset();
            _stopWatch.Start();
            List<ErrorObj> lstError = new List<ErrorObj>();     //danh sách lỗi
            List<string> lstIdProperty;                         //key cần check
            string locationProperty = string.Empty;             //control bị lỗi (để focus đến control đó)
            List<string> lstMsgProperty;                        //thông báo lỗi tiếng việt
            lstIdProperty = new List<string> { "Date", "PostedDate", "ODate", "OPostedDate", "IWDate", "IWPostedDate", "MDate", "MPostedDate", "SDate", "SPostedDate" };
            lstMsgProperty = new List<string> { resSystem.MSG_Error_01, resSystem.MSG_Error_02 };

            if (date.DataBindings.Count > 0)
            {
                string key = date.DataBindings[0].BindingMemberInfo.BindingMember;
                int i = lstIdProperty.IndexOf(key);
                if (string.IsNullOrEmpty(date.Text) || date.DateTime.Date == Utils.GetDateTimeDefault().Date || date.DateTime.Date == new DateTime().Date)
                    lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = lstMsgProperty[i] });
                if (lstError.Count > 0)
                {
                    date.Focus();
                    MSG.Warning(lstError[0].Msg);
                    return false;
                }
            }

            return true;
        }

        private void dtePostedDate_Validated(object sender, EventArgs e)
        {
            if (_stopWatch.IsRunning)
                _stopWatch.Stop();
        }

        private void dteDate_Validated(object sender, EventArgs e)
        {
            if (_stopWatch.IsRunning)
                _stopWatch.Stop();
        }

        private void dtePostedDate_TextChanged(object sender, EventArgs e, Form @this)
        {
            if (_objectDataSource.HasProperty("TypeID") && new[] { 900, 901 }.Any(x => x == (int?)_objectDataSource.GetProperty("TypeID")))
            {
                if (_noBindName == "IWNo")
                    AddRefDateTimeByKey("RefDateTimeIn", getText: true);
                else if (_noBindName == "OWNo")
                    AddRefDateTimeByKey("RefDateTimeOut", getText: true);
            }
            else if (_objectDataSource.HasProperty("TypeID") && new[] { 180 }.Any(x => x == (int?)_objectDataSource.GetProperty("TypeID")))
            {
                AddRefDateTimeByKey("Date", getText: true);
            }
            else if (_objectDataSource.HasProperty("TypeID") && new[] { 320, 321, 322, 323, 324, 325 }.Any(x => x == (int?)_objectDataSource.GetProperty("TypeID")))
            {
                AddRefDateTimeByKey("ModifiedDate", getText: true);
            }
            else
                AddRefDateTimeByKey("RefDateTime", getText: true);
            if (_objectDataSource.HasProperty("TypeID") && (int)_objectDataSource.GetProperty("TypeID") != 210)
            {
                UltraDateTimeEditor ultraDateTimeEditor = (UltraDateTimeEditor)sender;

                UltraCombo CbbFixedAssetID = @this.Controls.Find("cbbFixedAssetID", true).FirstOrDefault() as UltraCombo;
                Utils.ConfigComboFixedAsset(CbbFixedAssetID, 0, null, DateTime.Parse(ultraDateTimeEditor.Value.ToString()));
            }
        }

        private void dtePostedDate_ValueChanged(object sender, EventArgs e, Form @this)
        {
            if (_objectDataSource.HasProperty("TypeID"))
            {
                var typeID = (int)_objectDataSource.GetProperty("TypeID");
                UltraDateTimeEditor ultraDateTimeEditor = (UltraDateTimeEditor)sender;
                if (typeID == 530)
                {
                    UltraCombo CbbFixedAssetID = @this.Controls.Find("cbbFixedAssetID", true).FirstOrDefault() as UltraCombo;
                    Utils.ConfigComboFixedAsset(CbbFixedAssetID, 0, null, DateTime.Parse(ultraDateTimeEditor.Value.ToString()));
                }
                else if (new int[] { 500, 520, 550 }.Contains(typeID))
                {
                    UltraGrid uGridControl = (UltraGrid)@this.Controls.Find("uGrid0", true).FirstOrDefault();
                    if (uGridControl != null)
                    {
                        //var cbbValueList = uGridControl.Rows[0].Cells["FixedAssetID"].ValueListResolved as UltraCombo;
                        //cbbValueList.ConfigComboFixedAsset(0, null, DateTime.Parse(ultraDateTimeEditor.Value.ToString()));
                        var band = uGridControl.DisplayLayout.Bands[0];
                        foreach (var @column in band.Columns)
                        {
                            if (@column.Key.Equals("FixedAssetID"))
                            {
                                int? type = typeID == 540 ? 3 : typeID == 500 ? 1 : (typeID == 520 || typeID == 550) ? 0 : (int?)null;
                                Utils.ConfigCbbFixedAssetToGrid(@this, uGridControl, @column.Key, Utils.ListFixedAsset, "ID", "FixedAssetCode",
                                    ConstDatabase.FixedAsset_TableName, type, false, null, false, null, DateTime.Parse(ultraDateTimeEditor.Value.ToString()));
                                //uGridControl.DataSource = new List<FADecrementDetail>();
                            }
                        }
                    }
                }
                else if (new int[] { 540 }.Contains(typeID))
                {
                    UltraGrid uGridControl = (UltraGrid)@this.Controls.Find("uGrid1", true).FirstOrDefault(); //
                    if (uGridControl != null)
                    {
                        //var cbbValueList = uGridControl.Rows[0].Cells["FixedAssetID"].ValueListResolved as UltraCombo;
                        //cbbValueList.ConfigComboFixedAsset(0, null, DateTime.Parse(ultraDateTimeEditor.Value.ToString()));
                        var band = uGridControl.DisplayLayout.Bands[0];
                        foreach (var @column in band.Columns)
                        {
                            if (@column.Key.Equals("FixedAssetID"))
                            {
                                int? type = 3;
                                Utils.ConfigCbbFixedAssetToGrid(@this, uGridControl, @column.Key, Utils.ListFixedAsset, "ID", "FixedAssetCode",
                                    ConstDatabase.FixedAsset_TableName, type, false, null, false, null, DateTime.Parse(ultraDateTimeEditor.Value.ToString()));
                                //uGridControl.DataSource = new List<FADecrementDetail>();
                            }
                        }
                        BindingList<FADepreciationDetail> dsDepreciationDetails = new BindingList<FADepreciationDetail>();
                        BindingList<FADepreciationAllocation> dsFADepreciationAllocations = new BindingList<FADepreciationAllocation>();
                        BindingList<FADepreciationPost> dsFADepreciationPosts = new BindingList<FADepreciationPost>();

                        var postedDate = DateTime.Parse(ultraDateTimeEditor.Value.ToString());
                        var lst = new List<FADepreciationDetail>();
                        var lstallo = new List<FADepreciationAllocation>();
                        var lstPost = new List<FADepreciationPost>();
                        foreach (var fixedAsset in Utils.IFixedAssetService.GetFAForDepreciation(postedDate))
                        {
                            FixedAssetLedger FixedAssetLedger = Utils.IGeneralLedgerService.GetLastFixedAssetLedger(fixedAsset.ID, postedDate);
                            var detail = new FADepreciationDetail
                            {
                                FixedAssetID = fixedAsset.ID,
                                FixedAssetName = fixedAsset.FixedAssetName,
                                ObjectID = FixedAssetLedger.DepartmentID,
                                CurrencyID = "VND",
                                DebitAccount = fixedAsset.ExpenditureAccount,
                                CreditAccount = fixedAsset.DepreciationAccount,
                                Reason = string.Format("Khấu hao TSCĐ tháng {0} năm {1}", postedDate.Month, postedDate.Year)
                            };
                            var allo = new FADepreciationAllocation
                            {
                                FixedAssetID = fixedAsset.ID,
                                ObjectID = FixedAssetLedger.DepartmentID,
                                Rate = 100,
                                AccountNumber = fixedAsset.ExpenditureAccount
                            };
                            var post = new FADepreciationPost
                            {
                                FixedAssetID = fixedAsset.ID,
                                FixedAssetName = fixedAsset.FixedAssetName,
                                ObjectID = FixedAssetLedger.DepartmentID,
                                DebitAccount = fixedAsset.ExpenditureAccount,
                                CreditAccount = fixedAsset.DepreciationAccount,
                                Description = string.Format("Khấu hao TSCĐ tháng {0} năm {1}", postedDate.Month, postedDate.Year)
                            };
                            var faFixedAssetCategory = Utils.IFixedAssetCategoryService.Query.Where(fads => fads.ID == fixedAsset.FixedAssetCategoryID).ToList().LastOrDefault();
                            if (faFixedAssetCategory != null)
                            {
                                detail.FixedAssetCategory = faFixedAssetCategory;
                                detail.FixedAssetCategoryID = faFixedAssetCategory.ID;
                            }

                            detail.OrgPrice = FixedAssetLedger.OriginalPrice ?? 0;
                            detail.OrgPriceOriginal = FixedAssetLedger.OriginalPrice ?? 0;
                            detail.EachYearDepreciationAmount = FixedAssetLedger.MonthPeriodDepreciationAmount * 12 ?? 0;
                            detail.EachYearDepreciationAmountOriginal = FixedAssetLedger.MonthPeriodDepreciationAmount * 12 ?? 0;
                            detail.EachMonthDepreciationRate = FixedAssetLedger.MonthDepreciationRate ?? 0;
                            detail.Amount = FixedAssetLedger.DepreciationAmount ?? 0;
                            detail.AmountOriginal = FixedAssetLedger.DepreciationAmount ?? 0;

                            var usedtime = FixedAssetLedger.UsedTime.GetValueOrDefault() > FixedAssetLedger.UsedTimeRemain.GetValueOrDefault() ? FixedAssetLedger.UsedTime.GetValueOrDefault() : FixedAssetLedger.UsedTimeRemain.GetValueOrDefault();
                            post.Amount = post.AmountOriginal = detail.TotalOrgPrice = detail.TotalOrgPriceOriginal = detail.MonthlyDepreciationAmount = detail.MonthlyDepreciationAmountOriginal = Math.Round(this.GetMonthlyDepreciationAmount(fixedAsset.ID, fixedAsset.DepreciationDate.GetValueOrDefault(), FixedAssetLedger.DepreciationAmount.GetValueOrDefault(), FixedAssetLedger.MonthPeriodDepreciationAmount.GetValueOrDefault(), FixedAssetLedger.RemainingAmount.GetValueOrDefault(), FixedAssetLedger.UsedTimeRemain.GetValueOrDefault(), postedDate));
                            allo.Amount = detail.AllocationAmountOriginal = detail.AllocationAmount = detail.MonthlyDepreciationAmountOriginal;

                            lst.Add(detail);
                            lstallo.Add(allo);
                            lstPost.Add(post);
                        }
                        UltraGrid uGrid0 = (UltraGrid)@this.Controls.Find("uGrid0", true).FirstOrDefault();
                        UltraGrid uGrid1 = (UltraGrid)@this.Controls.Find("uGrid1", true).FirstOrDefault();
                        UltraGrid uGrid2 = (UltraGrid)@this.Controls.Find("uGrid2", true).FirstOrDefault();
                        uGrid0.DataSource = lst;
                        uGrid1.DataSource = lstallo;
                        uGrid2.DataSource = lstPost;

                    }
                }
                else if (new int[] { 620 }.Contains(typeID))
                {
                    UltraGrid uGrid0 = (UltraGrid)@this.Controls.Find("uGrid0", true).FirstOrDefault();
                    if (uGrid0 != null)
                    {
                        uGrid0.DataSource = Utils.IGeneralLedgerService.getILTranferDetail(PostedDate);
                    }
                    ((DetailBase<T>)@this)._select.SetProperty("Reason", string.Format("Kết chuyển lãi lỗ đến ngày {0}", PostedDate.ToString("dd/MM/yyyy")));
                }
            }
        }

        public decimal GetMonthlyDepreciationAmount(Guid ID, DateTime startDate, decimal DepreciationAmount, decimal MonthDepreciationAmount, decimal remainAmount, decimal usedTimeRemain, DateTime postedDate)
        {
            if (remainAmount == 0)
                return remainAmount;
            if (usedTimeRemain == 1)
            {
                return remainAmount;
            }
            // Kiểm tra xem đã ghi giảm trong tháng này chưa, nếu đã ghi giảm, chỉ tính số tiền khấu hao từ đầu tháng
            // đến lúc ghi giảm TSCD
            DateTime? decrementDate = Utils.IFADecrementDetailService.FindDecrementDate(ID, PostedDate);
            DateTime date = decrementDate ?? PostedDate;
            if (startDate.Month == PostedDate.Month && startDate.Year == PostedDate.Year)
            {
                // Nếu không phải ngày đầu của tháng
                if (startDate.Day != 1)
                {
                    var dteDiff = new DateDiff(startDate, PostedDate);
                    return MonthDepreciationAmount * (dteDiff.Days + 1) / DateTime.DaysInMonth(PostedDate.Year, PostedDate.Month);
                }
            }
            if (decrementDate != null)
            {
                return MonthDepreciationAmount * (date.Day - 1) / DateTime.DaysInMonth(date.Year, date.Month);
            }
            return MonthDepreciationAmount;
        }

        private void dteDate_ValueChanged(object sender, EventArgs e)
        {

        }

    }
}