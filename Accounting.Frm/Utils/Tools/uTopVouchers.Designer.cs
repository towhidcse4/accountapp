﻿namespace Accounting
{
    partial class UTopVouchers<T>
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            this.palVouchers = new System.Windows.Forms.Panel();
            this.lblPostedDate = new Infragistics.Win.Misc.UltraLabel();
            this.lblNo = new Infragistics.Win.Misc.UltraLabel();
            this.txtNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblDate = new Infragistics.Win.Misc.UltraLabel();
            this.dteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.dtePostedDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.cbbType = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.palType = new Infragistics.Win.Misc.UltraPanel();
            this.toolTip = new Infragistics.Win.UltraWinToolTip.UltraToolTipManager(this.components);
            this.palVouchers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtePostedDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbType)).BeginInit();
            this.palType.ClientArea.SuspendLayout();
            this.palType.SuspendLayout();
            this.SuspendLayout();
            // 
            // palVouchers
            // 
            this.palVouchers.BackColor = System.Drawing.Color.Transparent;
            this.palVouchers.Controls.Add(this.lblPostedDate);
            this.palVouchers.Controls.Add(this.lblNo);
            this.palVouchers.Controls.Add(this.txtNo);
            this.palVouchers.Controls.Add(this.lblDate);
            this.palVouchers.Controls.Add(this.dteDate);
            this.palVouchers.Controls.Add(this.dtePostedDate);
            this.palVouchers.Location = new System.Drawing.Point(117, 0);
            this.palVouchers.Name = "palVouchers";
            this.palVouchers.Size = new System.Drawing.Size(299, 45);
            this.palVouchers.TabIndex = 38;
            // 
            // lblPostedDate
            // 
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(104)))), ((int)(((byte)(189)))));
            appearance1.BackColor2 = System.Drawing.Color.Transparent;
            appearance1.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance1.ForeColor = System.Drawing.Color.White;
            appearance1.TextHAlignAsString = "Center";
            appearance1.TextVAlignAsString = "Middle";
            this.lblPostedDate.Appearance = appearance1;
            this.lblPostedDate.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.lblPostedDate.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.lblPostedDate.Location = new System.Drawing.Point(198, 0);
            this.lblPostedDate.Name = "lblPostedDate";
            this.lblPostedDate.Size = new System.Drawing.Size(100, 24);
            this.lblPostedDate.TabIndex = 28;
            this.lblPostedDate.Text = "Ngày hạch toán";
            // 
            // lblNo
            // 
            appearance2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(104)))), ((int)(((byte)(189)))));
            appearance2.BackColor2 = System.Drawing.Color.Transparent;
            appearance2.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance2.ForeColor = System.Drawing.Color.White;
            appearance2.TextHAlignAsString = "Center";
            appearance2.TextVAlignAsString = "Middle";
            this.lblNo.Appearance = appearance2;
            this.lblNo.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.lblNo.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.lblNo.Location = new System.Drawing.Point(0, 0);
            this.lblNo.Name = "lblNo";
            this.lblNo.Size = new System.Drawing.Size(100, 24);
            this.lblNo.TabIndex = 0;
            this.lblNo.Text = "Số chứng từ";
            this.lblNo.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            // 
            // txtNo
            // 
            appearance3.TextHAlignAsString = "Center";
            this.txtNo.Appearance = appearance3;
            this.txtNo.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.txtNo.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtNo.Location = new System.Drawing.Point(0, 24);
            this.txtNo.MaxLength = 25;
            this.txtNo.Name = "txtNo";
            this.txtNo.Size = new System.Drawing.Size(100, 19);
            this.txtNo.TabIndex = 1;
            // 
            // lblDate
            // 
            appearance4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(104)))), ((int)(((byte)(189)))));
            appearance4.BackColor2 = System.Drawing.Color.Transparent;
            appearance4.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance4.ForeColor = System.Drawing.Color.White;
            appearance4.TextHAlignAsString = "Center";
            appearance4.TextVAlignAsString = "Middle";
            this.lblDate.Appearance = appearance4;
            this.lblDate.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.lblDate.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.lblDate.Location = new System.Drawing.Point(99, 0);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(100, 24);
            this.lblDate.TabIndex = 24;
            this.lblDate.Text = "Ngày chứng từ";
            this.lblDate.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            // 
            // dteDate
            // 
            appearance5.TextHAlignAsString = "Center";
            appearance5.TextVAlignAsString = "Middle";
            this.dteDate.Appearance = appearance5;
            this.dteDate.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.dteDate.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2010Button;
            this.dteDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.dteDate.FormatProvider = new System.Globalization.CultureInfo("");
            this.dteDate.Location = new System.Drawing.Point(99, 24);
            this.dteDate.MaskInput = "dd/mm/yyyy";
            this.dteDate.Name = "dteDate";
            this.dteDate.Size = new System.Drawing.Size(100, 19);
            this.dteDate.TabIndex = 3;
            this.dteDate.Value = null;
            this.dteDate.ValueChanged += new System.EventHandler(this.dteDate_ValueChanged);
            this.dteDate.AfterExitEditMode += new System.EventHandler(this.dteDate_AfterExitEditMode);
            this.dteDate.Validated += new System.EventHandler(this.dteDate_Validated);
            // 
            // dtePostedDate
            // 
            appearance6.TextHAlignAsString = "Center";
            appearance6.TextVAlignAsString = "Middle";
            this.dtePostedDate.Appearance = appearance6;
            this.dtePostedDate.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.dtePostedDate.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2010Button;
            this.dtePostedDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtePostedDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.dtePostedDate.FormatProvider = new System.Globalization.CultureInfo("");
            this.dtePostedDate.Location = new System.Drawing.Point(198, 24);
            this.dtePostedDate.MaskInput = "dd/mm/yyyy";
            this.dtePostedDate.Name = "dtePostedDate";
            this.dtePostedDate.Size = new System.Drawing.Size(100, 19);
            this.dtePostedDate.TabIndex = 2;
            this.dtePostedDate.Value = null;
            this.dtePostedDate.AfterExitEditMode += new System.EventHandler(this.dtePostedDate_AfterExitEditMode);
            this.dtePostedDate.Validated += new System.EventHandler(this.dtePostedDate_Validated);
            // 
            // cbbType
            // 
            this.cbbType.AutoSize = false;
            this.cbbType.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.cbbType.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbType.Location = new System.Drawing.Point(0, 24);
            this.cbbType.Name = "cbbType";
            this.cbbType.NullText = "<chọn dữ liệu>";
            this.cbbType.Size = new System.Drawing.Size(119, 19);
            this.cbbType.TabIndex = 0;
            // 
            // ultraLabel1
            // 
            appearance7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(104)))), ((int)(((byte)(189)))));
            appearance7.BackColor2 = System.Drawing.Color.Transparent;
            appearance7.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance7.ForeColor = System.Drawing.Color.White;
            appearance7.TextHAlignAsString = "Center";
            appearance7.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance7;
            this.ultraLabel1.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel1.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.ultraLabel1.Location = new System.Drawing.Point(0, 0);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(119, 24);
            this.ultraLabel1.TabIndex = 34;
            this.ultraLabel1.Text = "Loại giấy báo nợ";
            // 
            // palType
            // 
            // 
            // palType.ClientArea
            // 
            this.palType.ClientArea.Controls.Add(this.cbbType);
            this.palType.ClientArea.Controls.Add(this.ultraLabel1);
            this.palType.Location = new System.Drawing.Point(0, 0);
            this.palType.Name = "palType";
            this.palType.Size = new System.Drawing.Size(119, 43);
            this.palType.TabIndex = 30;
            // 
            // toolTip
            // 
            this.toolTip.ContainingControl = this;
            // 
            // UTopVouchers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.palType);
            this.Controls.Add(this.palVouchers);
            this.Name = "UTopVouchers";
            this.Size = new System.Drawing.Size(419, 45);
            this.palVouchers.ResumeLayout(false);
            this.palVouchers.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtePostedDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbType)).EndInit();
            this.palType.ClientArea.ResumeLayout(false);
            this.palType.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel palVouchers;
        private Infragistics.Win.Misc.UltraLabel lblPostedDate;
        private Infragistics.Win.Misc.UltraLabel lblNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtNo;
        private Infragistics.Win.Misc.UltraLabel lblDate;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbType;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraPanel palType;
        public Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteDate;
        public Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtePostedDate;
        private Infragistics.Win.UltraWinToolTip.UltraToolTipManager toolTip;
    }
}
