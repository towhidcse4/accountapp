﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting
{
    public class RegistryUtils
    {
        public class HKEY_CURRENT_USER
        {
            static readonly string subKey = "SOFTWARE\\EASY\\Accounting";
            public static void SetValue(string key, object value)
            {
                RegistryKey root = Registry.CurrentUser;
                RegistryKey regKey =
                    //root.OpenSubKey(subKey);
                    //if (regKey == null)
                    //regKey = 
                    root.CreateSubKey(subKey);
                regKey.SetValue(key.ToLower(), value);
                regKey.Close();
                root.Close();
            }
            public static T GetValue<T>(string key)
            {
                T result = default(T);
                RegistryKey root = Registry.CurrentUser;
                RegistryKey regKey = root.OpenSubKey(subKey);
                if (regKey == null)
                    goto end;
                result = (T)regKey.GetValue(key.ToLower(), result);
                regKey.Close();
            end:
                root.Close();
                return result;
            }
        }
        public class HKEY_LOCAL_MACHINE
        {
            static readonly string subKey = "SOFTWARE\\EASY\\Accounting";
            public static void SetValue(string key, object value)
            {
                RegistryKey root = Registry.LocalMachine;
                RegistryKey regKey = root.CreateSubKey(subKey, RegistryKeyPermissionCheck.ReadWriteSubTree);
                    //root.OpenSubKey(subKey);
                    //if (regKey == null)
                    //regKey = 
                regKey.SetValue(key.ToLower(), value);
                regKey.Close();
                root.Close();
            }
            public static T GetValue<T>(string key)
            {
                T result = default(T);
                RegistryKey root = Registry.LocalMachine;
                RegistryKey regKey = root.OpenSubKey(subKey);
                if (regKey == null)
                    goto end;
                result = (T)regKey.GetValue(key.ToLower(), result);
                regKey.Close();
            end:
                root.Close();
                return result;
            }
        }
    }
}
