﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System.Drawing;
using Accounting.TextMessage;
using Accounting.Core.Domain;

namespace Accounting
{
    class CustomCreationFilter<T> : IUIElementCreationFilter
    {
        UltraGridRow _hotTrackedRow = null;
        //ImageAndTextButtonUIElement button;
        public UltraGridRow HotTrackedRow
        {
            get { return _hotTrackedRow; }
            set { _hotTrackedRow = value; }
        }

        private bool _haveAddButton = false;
        /// <summary>
        /// Có nút Thêm trên phần header của ultracombo
        /// </summary>
        public bool HaveAddButton
        {
            get { return _haveAddButton; }
            set { _haveAddButton = value; }
        }

        private int _count = 1;

        public int Count
        {
            get { return _count; }
            set { _count = value; }
        }

        private UltraGrid _ultraGrid;

        public UltraGrid UltraGrid
        {
            get { return _ultraGrid; }
            set { _ultraGrid = value; }
        }
        private UltraCombo _Ucombo;

        public UltraCombo Ucombo
        {
            get { return _Ucombo; }
            set { _Ucombo = value; }
        }
        private int? _typeID;

        public int? typeID
        {
            get { return _typeID; }
            set { _typeID = value; }
        }


        private bool _isDrop = false;
        private UltraCombo _ultraCombo = new UltraCombo();
        private Form _form;
        public CustomCreationFilter(Form @this)
        {
            _form = @this;
        }
        public void AfterCreateChildElements(UIElement parent)
        {
            if (parent is HeaderUIElement)
            {
                HeaderUIElement header = parent as HeaderUIElement;
                if (HaveAddButton) Count = 2;
                if (header.Header.Column.Key == Key)
                {
                    //Rectangle thisRect = new Rectangle();
                    UltraGridUIElement gridUiElement = (UltraGridUIElement)header.Parent.Parent.Parent.Parent;
                    if (!_isDrop)
                        gridUiElement.Grid.DisplayLayout.Bands[0].Columns[Key].Width += (header.Rect.Height * Count);
                    if (_ultraGrid != null && _ultraGrid.ActiveCell.EditorResolved.IsDroppedDown) _isDrop = true;
                    else if (gridUiElement.Grid.GetType() == typeof(UltraCombo))
                    {
                        _ultraCombo = (UltraCombo)gridUiElement.Grid;
                        _isDrop = _ultraCombo.IsDroppedDown;
                    }
                    for (int i = 0; i < Count; i++)
                    {
                        ImageAndTextButtonUIElement button = new ImageAndTextButtonUIElement(header);
                        button.Rect =
                            new System.Drawing.Rectangle(
                                new Point(
                                    header.Rect.Location.X + (header.Rect.Width - (header.Rect.Height * (Count - i))),
                                    header.Rect.Location.Y),
                                new System.Drawing.Size(header.Rect.Height, header.Rect.Height));
                        header.ChildElements.Add(button);
                        button.Style = UIElementButtonStyle.Office2010Button;
                        button.HotkeyPrefix = HotkeyPrefix.Show;
                        if (HaveAddButton && i == 0)
                            button.ElementClick += editorButton_ElementClick;
                        else button.ElementClick += ButtonFilter_ElementClick;
                        button.Image = HaveAddButton && i == 0 ? (Image ?? Properties.Resources.plus_press) : Properties.Resources.filter;
                        button.Appearance = new Infragistics.Win.Appearance
                        {
                            ImageHAlign = HAlign.Center,
                            ImageVAlign = VAlign.Middle
                        };
                        button.HotTracking = false;
                    }
                }
            }
            if (parent is CellUIElement)
            {
                if (IsTree)
                {
                    var cell = ((CellUIElement)parent).Cell;
                    if (!cellsWithPadding.ContainsKey(cell) || cell.Row.Equals(_hotTrackedRow))
                        return;

                    var el = parent.ChildElements.FirstOrDefault(e => e.GetType().Name.Contains("Editor"));

                    if (el == null)
                        return;

                    if (cellsWithPadding[cell] <= 0 || cellsWithPadding[cell] >= el.Rect.Width)
                        return;

                    el.Rect = new Rectangle(el.Rect.X + cellsWithPadding[cell], el.Rect.Y,
                            el.Rect.Width - cellsWithPadding[cell], el.Rect.Height);
                }
            }
        }

        private void ButtonFilter_ElementClick(object sender, UIElementEventArgs e)
        {
            if (_ultraGrid != null)
            {
                if (_form != null) _form.ShowQuickFilter<T>(_ultraGrid);
            }
            else if (_ultraCombo != new UltraCombo())
                if (_form != null) _form.ShowQuickFilter<T>(_ultraCombo);
        }

        private string _key;

        /// <summary>
        /// Tên Column được gắn Button
        /// </summary>
        public string Key
        {
            get { return _key; }
            set { _key = value; }
        }
        private Image _image;
        /// <summary>
        /// Icon của Button
        /// </summary>
        public Image Image
        {
            get { return _image; }
            set { _image = value; }
        }

        private UIElementEventHandler _eventInput;
        /// <summary>
        /// Sự kiện thực thi khi click button Add trên Header của UltraCombo [void EventButtonAdd(object sender, UIElementEventArgs e)]
        /// </summary>
        public UIElementEventHandler EventButtonAdd
        {
            get { return _eventInput; }
            set { _eventInput = value; }
        }
        void editorButton_ElementClick(object sender, UIElementEventArgs e)
        {
            try
            {
                _ultraGrid.ActiveCell.EditorResolved.CloseUp();
                EventButtonAdd(sender, e);
                FFixedAssetDetail frm;
                FMaterialGoodsDetail frm2;
                FMaterialToolsDetail frm3;
                FExpenseItemDetail frm4;
                FCostSetDetail frm5;
                //FEMContractSaleDetail frm6;
                //FEMContractBuyDetail frm7;
                FBudgetItemDetail frm8;
                FStatisticsCodeDetail frm9;
                FRepositoryDetail frm10;
                FAccountingObjectCustomersDetail frm11;
                FAccountingObjectEmployeeDetail frm12;
                FDepartmentDetail frm13;
                System.Reflection.PropertyInfo propId = null;
                object id = null;
                string namecolumn = _ultraGrid.ActiveCell.Column.ToString();
                if (namecolumn.Contains("AccountingObject")) namecolumn = "AccountingObject";
                if (namecolumn.Contains("BudgetItemCode")) namecolumn = "BudgetItemID";
                if (namecolumn.Contains("ExpenseItemCode")) namecolumn = "ExpenseItemID";
                if (namecolumn.Contains("Department")) namecolumn = "DepartmentID";
                if (namecolumn.Contains("CostSet")) namecolumn = "CostSetID";
                //if (namecolumn.Contains("Contract")) namecolumn = "ContractID";
                if (namecolumn.Contains("StatisticsCode")) namecolumn = "StatisticsCodeID";
                new string[] { "FixedAssetID", "MaterialGoodsID", "ToolsID", "ExpenseItemID", "CostSetID", /*"ContractID",*/ "BudgetItemID", "StatisticsCodeID", "RepositoryID", "AccountingObject", "EmployeeID", "DepartmentID" }.Contains(namecolumn);
                switch (namecolumn)
                {
                    case "FixedAssetID":
                        frm = new FFixedAssetDetail();
                        frm.ShowDialog(_form);
                        if (frm.DialogResult == DialogResult.Ignore) return;
                        Utils.ClearCacheByType<T>();
                        _form.ReloadComboInGridByName("FixedAssetID");
                        propId = frm.GetType().GetProperty("Id");
                        if (propId != null && propId.CanWrite)
                            id = propId.GetValue(frm, null);
                        break;
                    case "MaterialGoodsID":
                        frm2 = new FMaterialGoodsDetail();
                        frm2.ShowDialog(_form);
                        if (frm2.DialogResult == DialogResult.Ignore) return;
                        Utils.ClearCacheByType<MaterialGoods>();
                        Utils.ClearCacheByType<MaterialGoodsCustom>();
                        _form.ReloadComboInGridByName("MaterialGoodsID");
                        propId = frm2.GetType().GetProperty("Id");
                        if (propId != null && propId.CanWrite)
                            id = propId.GetValue(frm2, null);
                        break;
                    case "ToolsID":
                        frm3 = new FMaterialToolsDetail(false);
                        frm3.ShowDialog(_form);
                        if (frm3.DialogResult == DialogResult.Ignore) return;
                        Utils.ClearCacheByType<T>();
                        _form.ReloadComboInGridByName("ToolsID");
                        propId = frm3.GetType().GetProperty("ID");
                        if (propId != null && propId.CanWrite)
                            id = propId.GetValue(frm3, null);
                        break;
                    case "ExpenseItemID":
                        frm4 = new FExpenseItemDetail();
                        frm4.ShowDialog(_form);
                        if (frm4.DialogResult == DialogResult.Ignore) return;
                        Utils.ClearCacheByType<T>();
                        _Ucombo.DataSource = Utils.ListExpenseItem.ToList();
                        ConfigGrid(_Ucombo, "ExpenseItem", "ParentID", "IsParentNode");
                        propId = frm4.GetType().GetProperty("Id");
                        if (propId != null && propId.CanWrite)
                            id = propId.GetValue(frm4, null);
                        break;
                    case "CostSetID":
                        frm5 = new FCostSetDetail();
                        frm5.ShowDialog(_form);
                        if (frm5.DialogResult == DialogResult.Ignore) return;
                        Utils.ClearCacheByType<T>();
                        _Ucombo.DataSource = Utils.ListCostSet.ToList();
                        ConfigGrid(_Ucombo, "CostSet", "ParentID", "IsParentNode");
                        propId = frm5.GetType().GetProperty("Id");
                        if (propId != null && propId.CanWrite)
                            id = propId.GetValue(frm5, null);
                        break;
                    //case "ContractID":
                    //    if (new int?[] { 300, 310, 320, 321, 322, 323, 324, 325, 330, 340, 160, 161, 162, 163, 100, 101, 102, 103 }.Contains(typeID))
                    //    {
                    //        frm6 = new FEMContractSaleDetail();
                    //        frm6.ShowDialog(_form);
                    //        if (frm6.DialogResult == DialogResult.Ignore) return;
                    //        Utils.ClearCacheByType<T>();
                    //        _Ucombo.DataSource = new BindingList<EMContract>(Utils.ListEmContract.Where(n => n.TypeID == 860).ToList());
                    //        propId = frm6.GetType().GetProperty("Id");
                    //        if (propId != null && propId.CanWrite)
                    //            id = propId.GetValue(frm6, null);
                    //    }
                    //    else if (new int?[] { 200, 210, 220, 230, 240, 250, 260, 261, 262, 263, 264, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 140, 141, 142, 143, 144, /*160, 161, 162, 163, */170, 171, 172, 173, 174, 500 }.Contains(typeID))
                    //    {
                    //        frm7 = new FEMContractBuyDetail();
                    //        frm7.ShowDialog(_form);
                    //        if (frm7.DialogResult == DialogResult.Ignore) return;
                    //        Utils.ClearCacheByType<T>();
                    //        _Ucombo.DataSource = new BindingList<EMContract>(Utils.ListEmContract.Where(n => n.TypeID == 850).ToList());
                    //        propId = frm7.GetType().GetProperty("Id");
                    //        if (propId != null && propId.CanWrite)
                    //            id = propId.GetValue(frm7, null);
                    //    }
                    //    break;
                    case "BudgetItemID":
                        frm8 = new FBudgetItemDetail();
                        frm8.ShowDialog(_form);
                        if (frm8.DialogResult == DialogResult.Ignore) return;
                        Utils.ClearCacheByType<T>();
                        _Ucombo.DataSource = Utils.ListBudgetItem.ToList();
                        ConfigGrid(_Ucombo, "BudgetItem", "ParentID", "IsParentNode");
                        propId = frm8.GetType().GetProperty("Id");
                        if (propId != null && propId.CanWrite)
                            id = propId.GetValue(frm8, null);
                        break;
                    case "StatisticsCodeID":
                        frm9 = new FStatisticsCodeDetail();
                        frm9.ShowDialog(_form);
                        if (frm9.DialogResult == DialogResult.Ignore) return;
                        Utils.ClearCacheByType<T>();
                        _Ucombo.DataSource = Utils.ListStatisticsCode.ToList();
                        ConfigGrid(_Ucombo, "StatisticsCode", "ParentID", "IsParentNode");
                        propId = frm9.GetType().GetProperty("Id");
                        if (propId != null && propId.CanWrite)
                            id = propId.GetValue(frm9, null);
                        break;
                    case "RepositoryID":
                        frm10 = new FRepositoryDetail();
                        frm10.ShowDialog(_form);
                        if (frm10.DialogResult == DialogResult.Ignore) return;
                        Utils.ClearCacheByType<T>();
                        propId = frm10.GetType().GetProperty("Id");
                        if (propId != null && propId.CanWrite)
                            id = propId.GetValue(frm10, null);
                        break;
                    case "AccountingObject":
                        if (new int?[] { 435, 180, 560 }.Contains(typeID))
                        {
                            frm12 = new FAccountingObjectEmployeeDetail();
                            frm12.ShowDialog(_form);
                            if (frm12.DialogResult == DialogResult.Ignore) return;
                            Utils.ClearCacheByType<T>();
                            _form.ReloadComboInGridByName("AccountingObjectID");
                            _Ucombo.DataSource = Utils.IAccountingObjectService.GetListAccountingObjectByEmployee(true).ToList();
                            propId = frm12.GetType().GetProperty("Id");
                            if (propId != null && propId.CanWrite)
                                id = propId.GetValue(frm12, null);
                        }
                        else
                        {
                            frm11 = new FAccountingObjectCustomersDetail();
                            frm11.ShowDialog(_form);
                            if (frm11.DialogResult == DialogResult.Ignore) return;
                            Utils.ClearCacheByType<T>();
                            //_form.ReloadComboInGridByName("AccountingObjectID");
                            _Ucombo.DataSource = Utils.ListAccountingObject.ToList();
                            propId = frm11.GetType().GetProperty("Id");
                            if (propId != null && propId.CanWrite)
                                id = propId.GetValue(frm11, null);
                        }
                        break;
                    case "EmployeeID":
                        frm12 = new FAccountingObjectEmployeeDetail();
                        frm12.ShowDialog(_form);
                        if (frm12.DialogResult == DialogResult.Ignore) return;
                        Utils.ClearCacheByType<T>();
                        _form.ReloadComboInGridByName("EmployeeID");
                        _Ucombo.DataSource = Utils.IAccountingObjectService.GetListAccountingObjectByEmployee(true).ToList();
                        propId = frm12.GetType().GetProperty("Id");
                        if (propId != null && propId.CanWrite)
                            id = propId.GetValue(frm12, null);
                        break;
                    //case "AccountingObjectTaxID":
                    //    frm11 = new FAccountingObjectCustomersDetail();
                    //    frm11.ShowDialog(_form);
                    //    if (frm11.DialogResult == DialogResult.Ignore) return;
                    //    Utils.ClearCacheByType<T>();
                    //    _form.ReloadComboInGridByName("AccountingObjectID");
                    //    _Ucombo.DataSource = Utils.ListAccountingObject.ToList();
                    //    propId = frm11.GetType().GetProperty("Id");
                    //    if (propId != null && propId.CanWrite)
                    //        id = propId.GetValue(frm11, null);
                    //    break;
                    case "DepartmentID":
                        frm13 = new FDepartmentDetail();
                        frm13.ShowDialog(_form);
                        if (frm13.DialogResult == DialogResult.Ignore) return;
                        Utils.ClearCacheByType<T>();
                        _Ucombo.DataSource = Utils.ListDepartment.ToList();
                        ConfigGrid(_Ucombo, "Department", "ParentID", "IsParentNode");
                        propId = frm13.GetType().GetProperty("Id");
                        if (propId != null && propId.CanWrite)
                            id = propId.GetValue(frm13, null);
                        break;

                }

                UltraGrid uGrid = _form.FindGridIsActived();
                if (uGrid != null)
                {
                    if (uGrid.ActiveCell != null)
                    {
                        uGrid.ActiveCell.ValueListResolved.CloseUp();
                        //_isKeyPress = false;
                        (uGrid.ActiveCell.ValueListResolved as UltraCombo).SetValueForTag("KeyPress", false);
                        //System.Reflection.PropertyInfo propId = null;

                        if (propId != null && propId.CanWrite)
                        {
                            if (id == null || (Guid)id == Guid.Empty || id.ToString() == "00000000-0000-0000-0000-000000000000")
                            {

                            }
                            else
                            {
                                uGrid.ActiveCell.Value = id;
                                uGrid.ActiveCell.Row.UpdateData();
                            }
                        }
                        uGrid.ActiveCell.Activated = false;
                    }
                }

            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }

        private bool _isTree = false;
        /// <summary>
        /// Có cho phép hiển thị dạng cây cha con không
        /// </summary>
        public bool IsTree
        {
            get { return _isTree; }
            set { _isTree = value; }
        }

        public bool BeforeCreateChildElements(UIElement parent)
        {
            return false;
        }

        Dictionary<UltraGridCell, int> cellsWithPadding = new Dictionary<UltraGridCell, int>();

        public Dictionary<UltraGridCell, int> CellsWithPadding
        {
            get { return cellsWithPadding; }
            set { cellsWithPadding = value; }
        }
        public void ConfigGrid(UltraCombo ultraCombo, string nameTable, string nameParentId = null, string nameParentNode = null)
        {
            UltraGridBand band = ultraCombo.DisplayLayout.Bands[0];
            ultraCombo.BorderStyle = UIElementBorderStyle.Solid;
            ultraCombo.DisplayLayout.BorderStyle = UIElementBorderStyle.Solid;
            ultraCombo.DisplayStyle = EmbeddableElementDisplayStyle.Default;
            ultraCombo.DisplayLayout.AutoFitStyle = AutoFitStyle.ExtendLastColumn;
            //Cau hinh kieu Scroll
            ultraCombo.DisplayLayout.ScrollStyle = ScrollStyle.Immediate;
            ultraCombo.DisplayLayout.ScrollBounds = ScrollBounds.ScrollToFill;
            ultraCombo.DisplayLayout.Scrollbars = Scrollbars.Automatic;

            //
            band.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            band.Override.ActiveRowAppearance.ForeColor = Color.Black;
            band.Override.BorderStyleCell = UIElementBorderStyle.None;
            band.Override.BorderStyleRow = UIElementBorderStyle.None;
            //Tiêu đề cột
            string headerCaptionFirst = string.Empty;
            if (!ConstDatabase.TablesInDatabase.Any(x => x.Key.Equals(nameTable))) return;
            foreach (var item in band.Columns)
            {
                if (ConstDatabase.TablesInDatabase[nameTable].ContainsKey(item.Key))
                {
                    item.Header.Appearance.BorderAlpha = Alpha.Transparent;
                    TemplateColumn temp = ConstDatabase.TablesInDatabase[nameTable][item.Key];
                    if (string.IsNullOrEmpty(headerCaptionFirst) && temp.IsVisible) headerCaptionFirst = item.Key;   //Lấy dòng Header Caption đầu tiên
                    item.Header.Caption = temp.ColumnCaption;
                    item.Header.ToolTipText = temp.ColumnToolTip;
                    item.CellActivation = temp.IsReadOnly ? Activation.NoEdit : Activation.AllowEdit;
                    //Set màu header
                    item.Header.Appearance.BorderAlpha = Alpha.Transparent;
                    item.Header.Appearance.TextHAlign = HAlign.Left;
                    item.Width = temp.ColumnWidth == -1 ? item.Width : temp.ColumnWidth;
                    item.MaxWidth = temp.ColumnMaxWidth == -1 ? item.MaxWidth : temp.ColumnMaxWidth;
                    item.MinWidth = temp.ColumnMinWidth == -1 ? item.MinWidth : temp.ColumnMinWidth;
                    item.Hidden = !temp.IsVisibleCbb;
                    item.Header.VisiblePosition = temp.VisiblePosition == -1 ? item.Header.VisiblePosition : temp.VisiblePosition;
                    if (item.Hidden == false && item.Key.Contains("Quantity"))
                        Utils.FormatNumberic(item, ConstDatabase.Format_Quantity);
                    if (item.Key.Contains("SalePrice"))
                        Utils.FormatNumberic(item, ConstDatabase.Format_DonGiaQuyDoi);
                }

                else if (item.Key != "Select")
                {
                    item.Hidden = true;
                }
            }
            List<TemplateColumn> templateColumns = ConstDatabase.TablesInDatabase[nameTable].Values.ToList();
            var firstOrDefault = templateColumns.Where(p => p.IsVisibleCbb).OrderBy(p => p.VisiblePosition).FirstOrDefault();
            if (firstOrDefault != null)
                headerCaptionFirst =
                    firstOrDefault.ColumnName;
            var filter = new CustomCreationFilter<T>(_form) { Key = headerCaptionFirst };
            if (ultraCombo.CreationFilter == null)
            {
                ultraCombo.CreationFilter = filter;
            }
            else
            {
                filter = (CustomCreationFilter<T>)ultraCombo.CreationFilter;
                filter.Key = headerCaptionFirst;
            }
            if (!string.IsNullOrEmpty(nameParentId) && !string.IsNullOrEmpty(nameParentNode))
            {
                filter.IsTree = true;
                foreach (var item in ultraCombo.Rows)
                {
                    if (!(bool)item.Cells[nameParentNode].Value && item.Cells[nameParentId].Value != null)
                    {
                        filter.CellsWithPadding.Add(ultraCombo.Rows[item.Index].Cells[ultraCombo.DisplayMember], 20);
                    }
                    else
                    {
                        if ((bool)item.Cells[nameParentNode].Value)
                            item.Appearance.FontData.Bold = DefaultableBoolean.True;
                        filter.CellsWithPadding.Add(ultraCombo.Rows[item.Index].Cells[ultraCombo.DisplayMember], 0);
                    }
                }
                ultraCombo.MouseEnterElement += new Infragistics.Win.UIElementEventHandler(Combo_MouseEnterElement);
                ultraCombo.MouseLeaveElement += new Infragistics.Win.UIElementEventHandler(Combo_MouseLeaveElement);
            }
        }
        private static void Combo_MouseEnterElement(object sender, UIElementEventArgs e)
        {
            if (e.Element is RowUIElement)
            {
                UltraCombo cbb = (UltraCombo)sender;
                CustomCreationFilter<T> filter = (CustomCreationFilter<T>)cbb.CreationFilter;
                filter.HotTrackedRow = ((RowUIElement)e.Element).Row;
            }
        }

        private static void Combo_MouseLeaveElement(object sender, UIElementEventArgs e)
        {
            if (e.Element is RowUIElement)
            {
                UltraCombo cbb = (UltraCombo)sender;
                CustomCreationFilter<T> filter = (CustomCreationFilter<T>)cbb.CreationFilter;
                filter.HotTrackedRow = null;
            }
        }
    }
}
