﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Frm
{
    public static class NumberToWord
    {
        private static readonly string[] ChuSo = new string[10] { " không", " một", " hai", " ba", " bốn", " năm", " sáu", " bảy", " tám", " chín" };
        private static readonly string[] Tien = new string[6] { "", " nghìn", " triệu", " tỷ", " nghìn tỷ", " triệu tỷ" };
        // Hàm đọc số thành chữ
        public static string GetAmountInWords(decimal amount, string currency)
        {
            if (!string.Equals(currency, "VND", StringComparison.CurrentCultureIgnoreCase))
            {
                long dol = (long)amount;
                amount = decimal.Round(amount, 2, MidpointRounding.AwayFromZero);
                var ret = string.Empty;
                var cent = amount - dol;
                cent *= 100;
                cent = decimal.Round(cent, 0, MidpointRounding.AwayFromZero);
                if (string.Equals(currency, "USD", StringComparison.CurrentCultureIgnoreCase))
                {
                    if (dol > 0)
                    {
                        ret = GetAmountInWords(dol, " đô la Mỹ", /*" chẵn",*/ false);
                    }
                }
                if(string.Equals(currency, "EUR", StringComparison.CurrentCultureIgnoreCase))
                {
                    if (dol > 0)
                    {
                        ret = GetAmountInWords(dol, " Euro", /*" chẵn",*/ false);
                    }
                }
                if (string.Equals(currency, "JPY", StringComparison.CurrentCultureIgnoreCase))
                {
                    if (dol > 0)
                    {
                        ret = GetAmountInWords(dol, " Yên", /*" chẵn",*/ false);
                    }
                }
                if (string.Equals(currency, "SGD", StringComparison.CurrentCultureIgnoreCase))
                {
                    if (dol > 0)
                    {
                        ret = GetAmountInWords(dol, " đô la Sing", /*" chẵn",*/ false);
                    }
                }
                if (string.Equals(currency, "GBP", StringComparison.CurrentCultureIgnoreCase))
                {
                    if (dol > 0)
                    {
                        ret = GetAmountInWords(dol, " bảng Anh", /*" chẵn",*/ false);
                    }
                }
                if (cent > 0)
                {
                    var centString = cent.ToString(CultureInfo.InvariantCulture);
                    var array = centString.Select(t => int.Parse(t.ToString())).Reverse().ToArray();
                    var tem = centString.Length == 1 ? array[0] : array[1] * 10 + array[0];
                    if (ret.Length > 0)
                        ret += " và";
                    ret += ReadGroupOfThree(tem, false);
                    if (string.Equals(currency, "USD", StringComparison.CurrentCultureIgnoreCase) || string.Equals(currency, "EUR", StringComparison.CurrentCultureIgnoreCase) || string.Equals(currency, "SGD", StringComparison.CurrentCultureIgnoreCase))
                    {
                        ret += " cents";
                    }
                    if (string.Equals(currency, "GBP", StringComparison.CurrentCultureIgnoreCase))
                    {
                        ret += " pence";
                    }
                }
                if (string.IsNullOrEmpty(ret))
                {
                    if (string.Equals(currency, "USD", StringComparison.CurrentCultureIgnoreCase))
                        ret = "Không đô la Mỹ";
                    if (string.Equals(currency, "EUR", StringComparison.CurrentCultureIgnoreCase))
                        ret = "Không Euro";
                    if (string.Equals(currency, "JPY", StringComparison.CurrentCultureIgnoreCase))
                        ret = "Không Yên";
                    if (string.Equals(currency, "SGD", StringComparison.CurrentCultureIgnoreCase))
                        ret = "Không đô la Sing";
                    if (string.Equals(currency, "GBP", StringComparison.CurrentCultureIgnoreCase))
                        ret = "Không bảng Anh";
                }
                ret = ret.Substring(0, 1).ToUpper() + ret.Substring(1) + ".";
                return ret;
            }
            return GetAmountInWords((long)amount, " đồng"/*, " chẵn"*/);
        }
        private static string GetAmountInWords(long soTien, string currency, /*string evenText,*/ bool includeDot = true)
        {
            int lan, i;
            long so;
            string ketQua = "";
            int[] viTri = new int[6];
            if (soTien < 0) return "Số tiền âm!";
            if (soTien == 0) return "Không đồng!";
            if (soTien > 0)
            {
                so = soTien;
            }
            else
            {
                so = -soTien;
            }
            //Kiểm tra số quá lớn
            if (soTien > 8999999999999999)
            {
                soTien = 0;
                return "";
            }
            viTri[5] = (int)(so / 1000000000000000);
            so = so - long.Parse(viTri[5].ToString()) * 1000000000000000;
            viTri[4] = (int)(so / 1000000000000);
            so = so - long.Parse(viTri[4].ToString()) * +1000000000000;
            viTri[3] = (int)(so / 1000000000);
            so = so - long.Parse(viTri[3].ToString()) * 1000000000;
            viTri[2] = (int)(so / 1000000);
            viTri[1] = (int)((so % 1000000) / 1000);
            viTri[0] = (int)(so % 1000);
            if (viTri[5] > 0)
            {
                lan = 5;
            }
            else if (viTri[4] > 0)
            {
                lan = 4;
            }
            else if (viTri[3] > 0)
            {
                lan = 3;
            }
            else if (viTri[2] > 0)
            {
                lan = 2;
            }
            else if (viTri[1] > 0)
            {
                lan = 1;
            }
            else
            {
                lan = 0;
            }
            for (i = lan; i >= 0; i--)
            {
                bool isDoc = false;
                if (viTri[i].ToString().Length < 3 && i < lan)
                {
                    string testing = viTri[i].ToString();
                    isDoc = true;
                    //ViTri[i] = AddZero(ViTri[i]);
                }
                var tmp = ReadGroupOfThree(viTri[i], isDoc);
                isDoc = false;
                ketQua += tmp;
                if (viTri[i] != 0) ketQua += Tien[i];
                if ((i > 0) && (!string.IsNullOrEmpty(tmp))) ketQua += "";//&& (!string.IsNullOrEmpty(tmp))
            }
            if (ketQua.Substring(ketQua.Length - 1, 1) == ",") ketQua = ketQua.Substring(0, ketQua.Length - 1);
            ketQua = ketQua.Trim();
            //if (soTien.ToString()[soTien.ToString().Length - 1] == '0')
            //    currency = currency + evenText;
            return ketQua.Substring(0, 1).ToUpper() + ketQua.Substring(1) + currency + (includeDot ? "." : "");
        }

        private static string AddZero(string str)
        {
            if (str.Length == 2)
                str = "0" + str;
            else if (str.Length == 1)
                str = "00" + str;
            return str;

        }

        // Hàm đọc số có 3 chữ số
        private static string ReadGroupOfThree(int baso, bool isDoc0)
        {
            string ketQua = "";
            var tram = (int)(baso / 100);
            var chuc = (int)((baso % 100) / 10);
            var donvi = baso % 10;
            if ((tram == 0) && (chuc == 0) && (donvi == 0)) return "";
            if (tram != 0 || isDoc0)
            {
                ketQua += ChuSo[tram] + " trăm";
                if ((chuc == 0) && (donvi != 0)) ketQua += " linh";
            }
            if ((chuc != 0) && (chuc != 1))
            {
                ketQua += ChuSo[chuc] + " mươi";
                if ((chuc == 0) && (donvi != 0)) ketQua = ketQua + " linh";
            }
            if (chuc == 1) ketQua += " mười";
            switch (donvi)
            {
                case 1:
                    if ((chuc != 0) && (chuc != 1))
                    {
                        ketQua += " mốt";
                    }
                    else
                    {
                        ketQua += ChuSo[donvi];
                    }
                    break;
                case 5:
                    if (chuc == 0)
                    {
                        ketQua += ChuSo[donvi];
                    }
                    else
                    {
                        ketQua += " lăm";
                    }
                    break;
                default:
                    if (donvi != 0)
                    {
                        ketQua += ChuSo[donvi];
                    }
                    break;
            }
            return ketQua;
        }
    }
}
