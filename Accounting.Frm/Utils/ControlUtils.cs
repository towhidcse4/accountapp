﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinEditors;
using Accounting.TextMessage;
using Infragistics.Win;
using System.Windows.Forms;

namespace Accounting
{
    public static class ControlUtils
    {
        public static EmbeddableEditorBase CreateCombobox(this Form parent, object datasource, string tablename, string valueMember, string displayMember)
        {
            DefaultEditorOwnerSettings editorSettings = new DefaultEditorOwnerSettings();
            Infragistics.Win.UltraWinGrid.UltraDropDown dropDown = new Infragistics.Win.UltraWinGrid.UltraDropDown();
            dropDown.Visible = false;

            parent.Controls.Add(dropDown);

            dropDown.DataSource = datasource;
            dropDown.ValueMember = valueMember;
            dropDown.DisplayMember = displayMember;

            Utils.ConfigGrid(dropDown, tablename);

            editorSettings.ValueList = dropDown;
            editorSettings.DataType = typeof(int);
            EmbeddableEditorBase editor = new EditorWithCombo(new DefaultEditorOwner(editorSettings));
            return editor;
        }

        public static EmbeddableEditorBase CreateComboboxTree(this Form parent, object datasource, string valueMember, string displayMember)
        {
            DefaultEditorOwnerSettings editorSettings = new DefaultEditorOwnerSettings();
            Infragistics.Win.UltraWinGrid.UltraDropDown dropDown_Currencys = new Infragistics.Win.UltraWinGrid.UltraDropDown();
            dropDown_Currencys.Visible = false;

            parent.Controls.Add(dropDown_Currencys);

            dropDown_Currencys.DataSource = datasource;
            dropDown_Currencys.ValueMember = valueMember;
            dropDown_Currencys.DisplayMember = displayMember;
            dropDown_Currencys.DisplayLayout.Override.CellAppearance.BackColor = System.Drawing.Color.LightYellow;
            dropDown_Currencys.DisplayLayout.Override.CellAppearance.BackColor2 = System.Drawing.Color.Yellow;
            dropDown_Currencys.DisplayLayout.Override.CellAppearance.BackGradientStyle = GradientStyle.ForwardDiagonal;

            editorSettings.ValueList = dropDown_Currencys;
            editorSettings.DataType = typeof(int);
            EmbeddableEditorBase editor = new EditorWithCombo(new DefaultEditorOwner(editorSettings));
            return editor;
        }
        public static void CreateSummary(this UltraGridBand band, string sumkey, string sumCol)
        {

            Infragistics.Win.UltraWinGrid.SummarySettings summary = band.Summaries.Add(sumkey, Infragistics.Win.UltraWinGrid.SummaryType.Sum, band.Columns[sumCol]);
            summary.SummaryPosition = Infragistics.Win.UltraWinGrid.SummaryPosition.UseSummaryPositionColumn;
            summary.DisplayFormat = "{0:N0}";
            summary.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.BottomFixed;
        }
    }
}
