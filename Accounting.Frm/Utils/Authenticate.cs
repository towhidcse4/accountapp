﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using System.Net.NetworkInformation;

namespace Accounting
{
    public static class Authenticate
    {
        public static SysUser User { get; set; }
        public static string Server { get; set; }
        public static string Database { get; set; }
        public static string UserName { get { return User != null ? User.username : null; } }
        public static bool CheckUser(string usr, string pwd)
        {
            string pwdEncryp = FX.Utils.Encryption.StringToMD5Hash(pwd);
            var users = Utils.ISysUserService.Query.Where(u => u.username == usr && u.PasswordSalt == "MD5").ToList();
            if (users.Any(u => u.password == pwdEncryp))
            {
                User = users.FirstOrDefault(u => u.password == pwdEncryp);
                //Ktra quyền hoặc tình trạng hoạt động
                if (!User.IsActive)
                {
                    MSG.Warning("Tài khoản của bạn đang bị khóa, hoặc chưa được cấp quyền truy cập vào chương trình. \r\nVui lòng liên hệ quản trị viên để biết thêm thông tin chi tiết!");
                    goto End;
                }
                User.IsOnline = true;
                Utils.ISysUserService.Update(User);
                var log = new SysLog
                {
                    TypeID = 0,
                    UserID = User.userid,
                    UserName = User.username,
                    ComputerName = Environment.MachineName,
                    ComputerMAC = Logs.GetMACAddress(),
                    ComputerIP = Logs.GetLocalIPv4(),
                    WorkName = "Bảo mật",
                    Action = Logs.Action.Logon,
                    Reference = string.Empty,
                    Time = DateTime.Now,
                    Description = string.Empty
                };
                Logs.Write(log);
                return true;
            }
        End:
            User = null;
            return false;
        }
        public static bool Roles(string role)
        {
            var status = false;
            if(User == null) return status;

            //var user_role = Utils.ISysUserroleService.Query.Where(h => h.UserID == User.userid);
            var user_role = Utils.ListSysUserrole.Where(h => h.UserID == User.userid);

            if (user_role != null)
            {
                foreach (var item in user_role)
                {
                    //var sysRole = Utils.ISysRoleService.Getbykey(item.RoleID);
                    var sysRole = Utils.ListSysRole.FirstOrDefault(o => o.RoleID == item.RoleID);
                    if (sysRole != null && sysRole.RoleCode == role)
                    {
                        status = true;
                        break;
                    }
                }
            }
            
            return status;
        }
        /// <summary>
        /// Kiểm tra quyền
        /// </summary>
        /// <param name="permiss">Mã quyền cần kiểm tra</param>
        /// <param name="subSystemCode">Mã Danh mục/Nghiệp vụ đang thực hiện kiểm tra</param>
        /// <returns></returns>
        public static bool Permissions(string permiss, string subSystemCode)
        {
            var status = false;
            if (Roles("ADMIN")) return true;
            if (User == null) return status;
            //var user_role = Utils.ISysUserroleService.Query.Where(h => h.UserID == User.userid);
            var user_role = Utils.ListSysUserrole.Where(h => h.UserID == User.userid);

            if (user_role != null)
            {
                foreach (var user in user_role)
                {
                    //var user_per = Utils.ISysRolepermissionService.Query.Where(g => g.RoleID == user.RoleID && g.SubSystemCode == subSystemCode);
                    var user_per = Utils.ListSysRolePermission.Where(g => g.RoleID == user.RoleID && g.SubSystemCode == subSystemCode);
                    if (user_per != null)
                    {
                        foreach (var item in user_per)
                        {
                            //var per = Utils.ISysPermissionService.Getbykey(item.PermissionID);
                            var per = Utils.ListSysPermission.FirstOrDefault(o => o.PermissionID == item.PermissionID);
                            if (per != null & per.PermissionCode == permiss)
                            {
                                status = true;
                                goto End;
                            }
                        }
                    }
                }
            }
            
        End:
            return status;
        }
        public static bool? LogOn
        {
            get { return Accounting.FLogon.LogOn; }
            set { Accounting.FLogon.LogOn = value; }
        }
    }
    public static class Logs
    {
        public static void SaveBusiness<T>(T @select, int TypeID, string action)
        {
            var type = Utils.ListType.FirstOrDefault(t => t.ID == TypeID);
            string proNo = "No,ONo";
            string no = "";
            foreach (var item in proNo.Split(',').ToList())
            {
                if (@select.HasProperty(item))
                {
                    var tempNo = @select.GetProperty<T, string>(item);
                    if (string.IsNullOrEmpty(tempNo)) continue;
                    no = tempNo;
                    goto Step1;
                }
            }
        Step1:
            string proDes = "Description,Reason";
            string des = "";
            foreach (var item in proDes.Split(',').ToList())
            {
                if (@select.HasProperty(item))
                {
                    var tempDes = @select.GetProperty<T, string>(item);
                    if (string.IsNullOrEmpty(tempDes)) continue;
                    des = tempDes;
                    goto Step2;
                }
            }
        Step2:
            Logs.Save(TypeID, type != null ? type.TypeName : "", action, no, @select.GetProperty<T, Guid>("ID"), des);
        }
        public static void Save(int typeId, string workName, string action, string reference, Guid? refId, string description)
        {
            SysLog log = new SysLog
            {
                TypeID = typeId,
                UserID = Authenticate.User.userid,
                UserName = Authenticate.User.username,
                ComputerName = Environment.MachineName,
                ComputerMAC = Logs.GetMACAddress(),
                ComputerIP = Logs.GetLocalIPv4(),
                WorkName = workName,
                Action = action,
                Reference = reference,
                RefID = refId,
                Time = DateTime.Now,
                Description = description
            };
            Logs.Write(log);
        }
        public static void Write(this SysLog log)
        {
            Utils.ISysLogService.BeginTran();
            try
            {
                Utils.ISysLogService.CreateNew(log);
                Utils.ISysLogService.CommitTran();
            }
            catch (Exception ex)
            {
                Utils.ISysLogService.RolbackTran();
                MSG.Warning("Có lỗi xảy ra khi lưu lịch sử công việc này.");
            }
        }
        private static string _fileLog = null;
        public static void WriteTxt(string text)
        {
            text = string.Format(">>>>line: {0}     {1}", new System.Diagnostics.StackTrace(1).GetFrame(1).GetFileLineNumber(), text);
            string path = string.Format("{0}\\Logs", System.IO.Path.GetDirectoryName(
            System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase)).Replace("file:\\", "");
            if (_fileLog == null)
                _fileLog = string.Format("{0}\\{1}.txt", path, DateTime.Now.ToString("dd-MM-yyyy_h-mm-ss-tt")).Replace("file:\\", "");
            if (!System.IO.Directory.Exists(path))
                System.IO.Directory.CreateDirectory(path);
            if (!System.IO.File.Exists(_fileLog))
            {
                var f = System.IO.File.Create(_fileLog);
                f.Close();
                System.IO.File.SetAttributes(_fileLog, System.IO.FileAttributes.Normal);
                System.Security.AccessControl.DirectorySecurity sec = System.IO.Directory.GetAccessControl(_fileLog);
                System.Security.Principal.SecurityIdentifier everyone = new System.Security.Principal.SecurityIdentifier(System.Security.Principal.WellKnownSidType.WorldSid, null);
                sec.AddAccessRule(new System.Security.AccessControl.FileSystemAccessRule(everyone, System.Security.AccessControl.FileSystemRights.Modify | System.Security.AccessControl.FileSystemRights.Synchronize, System.Security.AccessControl.InheritanceFlags.ContainerInherit | System.Security.AccessControl.InheritanceFlags.ObjectInherit, System.Security.AccessControl.PropagationFlags.None, System.Security.AccessControl.AccessControlType.Allow));
                System.IO.Directory.SetAccessControl(_fileLog, sec);
                System.Security.AccessControl.FileSecurity security = System.IO.File.GetAccessControl(_fileLog);
                System.Security.Principal.SecurityIdentifier everybody = new System.Security.Principal.SecurityIdentifier(System.Security.Principal.WellKnownSidType.WorldSid, null);
                security.AddAccessRule(new System.Security.AccessControl.FileSystemAccessRule(everybody, System.Security.AccessControl.FileSystemRights.FullControl, System.Security.AccessControl.AccessControlType.Allow));
                System.IO.File.SetAccessControl(_fileLog, security);
                System.IO.TextWriter tw = new System.IO.StreamWriter(_fileLog);
                tw.WriteLine(text);
                tw.Close();
            }
            else if (System.IO.File.Exists(_fileLog))
            {
                //System.IO.File.SetAttributes(_fileLog, System.IO.FileAttributes.Normal);
                //System.Security.AccessControl.DirectorySecurity sec = System.IO.Directory.GetAccessControl(_fileLog);
                //System.Security.Principal.SecurityIdentifier everyone = new System.Security.Principal.SecurityIdentifier(System.Security.Principal.WellKnownSidType.WorldSid, null);
                //sec.AddAccessRule(new System.Security.AccessControl.FileSystemAccessRule(everyone, System.Security.AccessControl.FileSystemRights.Modify | System.Security.AccessControl.FileSystemRights.Synchronize, System.Security.AccessControl.InheritanceFlags.ContainerInherit | System.Security.AccessControl.InheritanceFlags.ObjectInherit, System.Security.AccessControl.PropagationFlags.None, System.Security.AccessControl.AccessControlType.Allow));
                //System.IO.Directory.SetAccessControl(_fileLog, sec);
                //System.Security.AccessControl.FileSecurity security = System.IO.File.GetAccessControl(_fileLog);
                //System.Security.Principal.SecurityIdentifier everybody = new System.Security.Principal.SecurityIdentifier(System.Security.Principal.WellKnownSidType.WorldSid, null);
                //security.AddAccessRule(new System.Security.AccessControl.FileSystemAccessRule(everybody, System.Security.AccessControl.FileSystemRights.FullControl, System.Security.AccessControl.AccessControlType.Allow));
                //System.IO.File.SetAccessControl(_fileLog, security);
                //System.IO.TextWriter tw = new System.IO.StreamWriter(_fileLog);
                //tw.WriteLine(text);
                //tw.Close();
                System.IO.File.AppendAllText(_fileLog, string.Format("{0}{1}", text, Environment.NewLine));
            }
        }

        public class Action
        {
            public const string Logon = "Đăng nhập";
            public const string Logout = "Đăng xuất";
            public const string Add = "Thêm";
            public const string Edit = "Sửa";
            public const string Delete = "Xóa";
            public const string Recorded = "Ghi sổ";
            public const string UnRecorded = "Bỏ ghi";
            public const string ImportData = "Nhập dữ liệu";
            public const string ExportData = "Kết xuất dữ liệu";
            public const string ImportExcel = "Nhập dữ liệu từ Excel";
            public const string ExportExcel = "Kết xuất dữ liệu ra Excel";
            public const string LedgerClosed = "Khóa sổ";
            public const string LedgerOpened = "Bỏ khóa sổ";
        }
        public static string GetLocalIPv4()
        {
            string output = "";
            foreach (NetworkInterface item in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (item.OperationalStatus == OperationalStatus.Up)
                {
                    IPInterfaceProperties adapterProperties = item.GetIPProperties();

                    if (adapterProperties.GatewayAddresses.FirstOrDefault() != null)
                    {
                        foreach (UnicastIPAddressInformation ip in adapterProperties.UnicastAddresses)
                        {
                            if (ip.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                            {
                                if (string.IsNullOrEmpty(ip.Address.ToString()))
                                    continue;
                                output = ip.Address.ToString();
                            }
                        }
                    }
                }
            }

            return output;
        }
        public static string GetMACAddress()
        {
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
            String sMacAddress = string.Empty;
            foreach (NetworkInterface adapter in nics)
            {
                if (sMacAddress == String.Empty)// only return MAC Address from first card  
                {
                    //IPInterfaceProperties properties = adapter.GetIPProperties(); Line is not required
                    sMacAddress = adapter.GetPhysicalAddress().ToString();
                }
            } return sMacAddress;
        }
    }
}
