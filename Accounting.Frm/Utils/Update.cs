﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using System.Data.SqlClient;
using Accounting.TextMessage;
using Accounting.Core.Domain;
using Accounting.Core.Common;

namespace Accounting
{
    class Update
    {
        private string _ConnectionString;
        private string serverName;
        private string database;
        private string user;
        private string pass;

        public Update (string host, string username, string password, string dbname)
        {
            serverName = host ?? Properties.Settings.Default.ServerName;
            database = dbname ?? Properties.Settings.Default.DatabaseName;
            user = username ?? Properties.Settings.Default.UserName;
            pass = password ?? Properties.Settings.Default.Password;

            _ConnectionString = "Data Source=" + serverName + ";Initial Catalog=" + database + ";User ID=" + user + ";Password=" + pass + "";
        }
        
        public bool Exec()
        {
            if (serverName == null || database == null || user == null || pass == null)
            {
                return false;
            }

            SqlConnection sqlConnection = new SqlConnection("");
            if (!string.IsNullOrEmpty(serverName) && !string.IsNullOrEmpty(database) && !string.IsNullOrEmpty(pass) && !serverName.Equals("172.16.10.66") && !serverName.Equals("14.225.3.218"))
            {
                sqlConnection = new SqlConnection(_ConnectionString);

                SqlTransaction transaction;
                try
                {
                    sqlConnection.Open();
                }
                catch (Exception)
                {
                    WaitingFrm.StopWaiting();
                    MSG.Warning("Máy chủ SQL không tồn tại. Vui lòng chọn máy chủ SQL khác.");
                    return false;
                }

                var versions = checkVersionInDb(sqlConnection);

                if (versions != null && versions.Count > 0)
                {
                    foreach(var version in versions)
                    {
                        string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                        var filePath = string.Format("{0}\\Data\\Updates\\ACCOUNTING_{1}.sql", path, version).Replace("file:\\", "");
                        if(File.Exists(filePath))
                        {
                            transaction = sqlConnection.BeginTransaction();
                            try
                            {
                                string data = File.ReadAllText(filePath);
                                data = data.Replace("\r\n", " ");
                                var sqlqueries = data.Split(new[] { " GO " }, StringSplitOptions.RemoveEmptyEntries);
                                foreach (var query in sqlqueries)
                                {
                                    new SqlCommand(query, sqlConnection, transaction).ExecuteNonQuery();
                                }
                                
                                string update_version = String.Format("UPDATE dbo.SystemOption SET Data = '{0}' WHERE Code = 'DB_Version'", version);
                                new SqlCommand(update_version, sqlConnection, transaction).ExecuteNonQuery();

                                transaction.Commit();
                            }
                            catch (Exception ex)
                            {
                                transaction.Rollback();
                                MSG.Error(ex.ToString());
                            }
                        }
                    }
                }

                sqlConnection.Close();
            }
            return true;
        }

        private static List<string> checkVersionInDb(SqlConnection sqlConnection)
        {
            List<SystemOption> records = new List<SystemOption>();
            string query_version = "SELECT * FROM dbo.SystemOption WHERE Code = 'DB_Version'";
            List<string> versions = new List<string>();
            using (SqlCommand dbCmd = new SqlCommand(query_version, sqlConnection))
            {

                try
                {
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            records = Helpers.GetPOBaseTListFromReader<SystemOption>(reader);
                            versions = Utils.getTargetVersion(records.FirstOrDefault().Data);
                        }

                    }

                }
                catch (Exception ex)
                {
                    MSG.Warning(ex.ToString());
                }
            }
            return versions;
        }
        private static bool validateVersion(string source, string target)
        {
            var src_split = source.Split('.');
            var tar_split = target.Split('.');
            int length = src_split.Length > tar_split.Length ? tar_split.Length : src_split.Length;
            for (int i = 0; i < length; i++)
            {
                if (Utils.decimalTryParse(src_split[i]) > Utils.decimalTryParse(tar_split[i]))
                {
                    return true;
                }

                if (i == length && src_split.Length > tar_split.Length)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
