﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.AllOPAccount;
using Accounting.Core.IService;
using Accounting.Core.ServiceImp;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public static class OPNUntil
    {
        public static ICurrencyService CurrencyService { get { return IoC.Resolve<ICurrencyService>(); } }
        public static IAccountService AccountService { get { return IoC.Resolve<IAccountService>(); } }
        public static IMaterialGoodsService MaterialGoodsService
        {
            get { return IoC.Resolve<IMaterialGoodsService>(); }
        }
        public static IAccountingObjectService AccountingObjectService
        {
            get { return IoC.Resolve<IAccountingObjectService>(); }
        }
        //ngày tháng bắt đầu chương trình - thực hiện ghi sổ cho opn -1 ngày
        public static DateTime ApplicationStartDateOPN
        {
            get { return (Utils.StringToDateTime(GetApplicationStartDate()) ?? DateTime.Now).AddDays(-1); }
            //set { dt = value; }
        }
        public static DateTime StartDateOPN
        {
            get { return (Utils.StringToDateTime(GetStartDate()) ?? DateTime.Now); }
            //set { dt = value; }
        }
        public static DateTime StringToDateTime(string dateString)
        {
            //return dateString;
            string[] formats = new[] {
                    @"dd/MM/yyyy", @"dd-MM-yyyy", @"dd.MM.yyyy", @"dd\MM\yyyy",
                    @"dd/M/yyyy", @"dd-M-yyyy", @"d.M.yyyy", @"d\M\yyyy",  
                    @"dd/MM/yy", @"dd-MM-yy", @"dd.MM.yy", @"dd\MM\yy",
                    @"dd/M/yy", @"dd-M-yy", @"dd.M.yy", @"dd\M\yy",

                    @"MM/dd/yyyy", @"MM-dd-yyyy", @"MM.dd.yyyy", @"MM\dd\yyyy",
                    @"M/dd/yyyy", @"M-dd-yyyy", @"M.d.yyyy", @"M\d\yyyy",  
                    @"MM/dd/yy", @"MM-dd-yy", @"MM.dd.yy", @"MM\dd\yy",
                    @"M/dd/yy", @"M-dd-yy", @"M.dd.yy", @"M\dd\yy",
                    @"MM/yyyy", @"MM-yyyy", @"MM.yyyy", @"MM\yyyy",
                    @"MM/yy", @"MM-yy", @"MM.yy", @"MM\yy",
                    @"M/yy", @"M-yy", @"M.YY", @"M\YY",
            };

            //string[] formats = {"M/d/yyyy h:mm:ss tt", "M/d/yyyy h:mm tt", 
            //       "MM/dd/yyyy hh:mm:ss", "M/d/yyyy h:mm:ss", 
            //       "M/d/yyyy hh:mm tt", "M/d/yyyy hh tt", 
            //       "M/d/yyyy h:mm", "M/d/yyyy h:mm", 
            //       "MM/dd/yyyy hh:mm", "M/dd/yyyy hh:mm"};

            DateTime dfg = DateTime.Now;
            try
            {
                dfg = DateTime.ParseExact(dateString, formats, CultureInfo.InvariantCulture,
                   DateTimeStyles.None);
            }
            catch (Exception)
            {
                MessageBox.Show("Có lỗi khi chuyển định dạng ngày tháng");
            }
            return dfg;
        }
        public static ISystemOptionService ISystemOptionService
        {
            get { return IoC.Resolve<ISystemOptionService>(); }
        }
        private static string GetApplicationStartDate()
        {
            SystemOption firstOrDefault = new SystemOption();
            try
            {
                firstOrDefault = ISystemOptionService.GetByCode("ApplicationStartDate");
            }
            catch (Exception)
            {
                MessageBox.Show("Có lỗi xảy ra với ngày tháng bắt đầu chương trình");
            }
            if (firstOrDefault != null) return firstOrDefault.Data;
            return "1/1/" + DateTime.Now.Year;
        }
        private static string GetStartDate()
        {
            SystemOption firstOrDefault = new SystemOption();
            try
            {
                firstOrDefault = ISystemOptionService.GetByCode("NgayHachToan");
            }
            catch (Exception)
            {
                MessageBox.Show("Có lỗi xảy ra với ngày hạch toán chương trình");
            }
            if (firstOrDefault != null) return firstOrDefault.Data;
            return DateTime.Now.ToString(CultureInfo.InvariantCulture);
        }
        static List<Account> _lstAccountFollowCurrency = new List<Account>();

        public static List<Account> ListAccountFollowCurrency(bool refresh = false)
        {
            if (_lstAccountFollowCurrency.Count == 0 || refresh)
            {
                return _lstAccountFollowCurrency = AccountService.GetAccounts_ByDetailTypeFollowCurrency();
            }
            else
            {
                return _lstAccountFollowCurrency;
            }
        }
        static List<Currency> _listCurrency = new List<Currency>();
        public static List<Currency> ListCurrency(bool refresh = false)
        {
            if (_listCurrency.Count == 0 || refresh)
            {
                return _listCurrency = CurrencyService.GetIsActive(true);
            }
            else
            {
                return _listCurrency;
            }
        }

        public static void AddSumChoCot(UltraGrid uGridView, string keyCot)
        {
            string keySum = "Sum" + keyCot;
            SummarySettings summary = uGridView.DisplayLayout.Bands[0].Summaries.Add(keySum, SummaryType.Sum,
                uGridView.DisplayLayout.Bands[0].Columns[keyCot]);
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary.DisplayFormat = "{0:N0}";
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed | SummaryDisplayAreas.GroupByRowsFooter;
            summary.Appearance.TextHAlign = HAlign.Right;
        }
        public static void AllowEditRowGrid(UltraGridRow ultraGridRow, IEnumerable<string> keyColumns)
        {
            foreach (string keyColumn in keyColumns)
            {
                ultraGridRow.Cells[keyColumn].Activation = Activation.AllowEdit;
            }

        }
        public static void NoEditRowGrid(UltraGridRow ultraGridRow, IEnumerable<string> keyColumns)
        {
            foreach (string keyColumn in keyColumns)
            {
                ultraGridRow.Cells[keyColumn].Appearance.BackColor = Color.FromArgb(218, 220, 221);
                ultraGridRow.Cells[keyColumn].Activation = Activation.NoEdit;
            }

        }
        public static void HiddenColumnGrid(UltraGrid ultraGrid, IEnumerable<string> keyColumns, bool anHien)
        {
            foreach (string keyColumn in keyColumns)
            {
                ultraGrid.DisplayLayout.Bands[0].Columns[keyColumn].Hidden = anHien;
            }

        }

        public static bool CheckDateTime(string t)
        {
            DateTime dt;
            if (!DateTime.TryParse(t, out dt))
            {
                //lỗi
                return false;
            }
            else
            {
                return true;
            }
            
        }

        public static void uGrid_KeyDown(object sender, KeyEventArgs e)
        {
            UltraGrid uGrid = (UltraGrid) sender;
            if (uGrid.ActiveCell != null)
            {
                UltraGridCell oldIndex = null;
                if (uGrid.ActiveCell.IsInEditMode)
                {
                    switch (e.KeyCode)
                    {
                        case Keys.Enter:
                            uGrid.PerformAction(UltraGridAction.ExitEditMode, false, false);
                            SendKeys.Send("{TAB}");
                            break;
                        case Keys.Tab:
                            List<UltraGridColumn> Columns = new List<UltraGridColumn>();
                            foreach (UltraGridColumn column in uGrid.DisplayLayout.Bands[0].Columns)
                            {
                                Columns.Add(column);
                            }
                            UltraGridColumn lastCol = Columns.OrderByDescending(p => p.Header.VisiblePosition).FirstOrDefault(p => !p.Hidden);
                            if (lastCol != null && uGrid.ActiveCell.Column.Key.Equals(lastCol.Key))
                            {
                                uGrid.PerformAction(UltraGridAction.ExitEditMode, false, false);
                                SendKeys.Send("{ESC}");
                                uGrid.ActiveCell = null;
                                SendKeys.Send("{TAB}");
                            }
                            break;
                    }
                }
                else
                {
                    switch (e.KeyCode)
                    {
                        case Keys.Enter:
                            uGrid.ActiveCell.Selected = true;
                            uGrid.PerformAction(UltraGridAction.EnterEditMode, false, false);
                            break;
                    }
                }
            }
        }
    }
}
