﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System.Web;
using System.Net;

namespace Accounting
{
    public static class XmlToObject
    {
        public static T Deserialize<T>(XmlNode node) where T : class
        {
            MemoryStream stm = new MemoryStream();

            StreamWriter stw = new StreamWriter(stm);
            stw.Write(node.OuterXml);
            stw.Flush();

            stm.Position = 0;

            XmlSerializer ser = new XmlSerializer(typeof(T));
            T result = (ser.Deserialize(stm) as T);

            return result;
        }

        public static List<T> DeserializeFromFile<T> (string filepath) where T: class
        {
            var objectName = typeof(T).Name;
            var ListObject = new List<T>();

            try
            {
                XmlDocument xmlDoc = Utils.readXmlConfig(filepath);
                XmlNode root = xmlDoc.DocumentElement;
                XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);

                XmlNodeList nodeList = root.SelectNodes(string.Format("descendant::{0}", objectName));

                if (nodeList != null)
                {
                    foreach (XmlNode item in nodeList)
                    {
                        T objectT = Deserialize<T>(item);

                        ListObject.Add(objectT);
                    }
                }
            }
            catch(Exception ex)
            {
                ListObject = null;
            }
            return ListObject;
        }

        public static string Serialize<T>(T ObjectToSerialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(ObjectToSerialize.GetType());

            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, ObjectToSerialize);
                return textWriter.ToString();
            }
        }

        public static string SerializeObjectToXmlNode(Object obj)
        {
            if (obj == null)
                throw new ArgumentNullException("Argument cannot be null");

            XmlNode resultNode = null;
            XmlSerializer xmlSerializer = new XmlSerializer(obj.GetType());
            using (MemoryStream memoryStream = new MemoryStream())
            {
                try
                {
                    xmlSerializer.Serialize(memoryStream, obj);
                }
                catch (InvalidOperationException)
                {
                    return null;
                }
                memoryStream.Position = 0;
                XmlDocument doc = new XmlDocument();
                doc.Load(memoryStream);
                resultNode = doc.DocumentElement;
            }
            return resultNode.InnerXml.ToString();
        }

        public static bool SaveToFile<T>(string filepath, T objectT, string parents = null) where T: class
        {
            if(parents.IsNullOrEmpty())
            {
                parents = string.Format("{0}s", typeof(T).Name);
            }

            try
            {
                XmlDocument xmlDoc = Utils.readXmlConfig(filepath);
                XmlNode root = xmlDoc.DocumentElement;
                XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);

                XmlElement parentsNode = (XmlElement)root.SelectSingleNode(parents);

                XmlNode node = xmlDoc.CreateElement(typeof(T).Name);
                node.InnerText = SerializeObjectToXmlNode(objectT);

                parentsNode.AppendChild(node);

                xmlDoc.Save(filepath);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool RewriteFile<T>(string filepath, List<T> data, string parents = null) where T: class
        {
            if (parents.IsNullOrEmpty())
            {
                parents = string.Format("{0}s", typeof(T).Name);
            }

            try
            {
                XmlDocument xmlDoc = Utils.readXmlConfig(filepath);
                XmlNode root = xmlDoc.DocumentElement;
                XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);

                XmlElement parentsNode = (XmlElement)root.SelectSingleNode(parents);

                parentsNode.RemoveAll();

                foreach(var objectT in data)
                {
                    XmlNode node = xmlDoc.CreateElement(typeof(T).Name);
                    node.InnerXml = SerializeObjectToXmlNode(objectT);

                    parentsNode.AppendChild(node);
                }

                xmlDoc.Save(filepath);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            } 
        }
    }
}
