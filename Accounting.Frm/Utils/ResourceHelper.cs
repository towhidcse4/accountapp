﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Accounting.TextMessage;
using Infragistics.Win;

namespace Accounting
{
    public class ResourceHelper
    {
        public static void MakeFileOutOfAStream(string stream, string filePath)
        {
            try
            {
                Stream file = new FileStream(stream, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                using (var fs = new FileStream(filePath, FileMode.Create, FileAccess.ReadWrite))
                {
                    CopyStream(file, fs);
                }
                file.Close();
            } catch(Exception ex)
            {
                MSG.Warning("Không thể ghi đè lên file đang mở");
            }
        }

        static void CopyStream(Stream input, Stream output)
        {
            byte[] buffer = new byte[32768];
            int read;
            while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, read);
            }
        }
        static Stream GetStream(string stream)
        {
            return Assembly.GetExecutingAssembly().GetManifestResourceStream(stream);

        }
    }
}