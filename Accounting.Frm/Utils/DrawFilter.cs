﻿using Infragistics.Win;
using Infragistics.Win.UltraWinExplorerBar;
using Infragistics.Win.UltraWinTabs;

namespace Accounting
{
    class DrawFilter : IUIElementDrawFilter
    {
        public bool DrawElement(DrawPhase drawPhase, ref UIElementDrawParams drawParams)
        {
            return true;
        }

        public DrawPhase GetPhasesToFilter(ref UIElementDrawParams drawParams)
        {
            if (drawParams.Element is TabItemUIElement || drawParams.Element is ButtonUIElementBase || drawParams.Element is ItemUIElementBase || drawParams.Element is GroupUIElement || drawParams.Element is TabGroupUIElement
                || drawParams.Element is NavigationAreaUIElement || drawParams.Element is NavigationOverflowButtonUIElement || drawParams.Element is NavigationOverflowButtonImageUIElement || drawParams.Element is NavigationOverflowButtonAreaUIElement ||
                drawParams.Element is NavigationGroupHeaderAreaUIElement || drawParams.Element is BetweenTextAndImageUIElement || drawParams.Element is GroupAreaUIElement || drawParams.Element is UltraExplorerBarUIElement||
                drawParams.Element is NavigationPaneExpansionButtonUIElement)
            {
                return DrawPhase.BeforeDrawFocus;
            }
            return DrawPhase.None;
        }
    }
}