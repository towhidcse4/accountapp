using System;
using System.Drawing;
using Infragistics.Win.UltraWinTabControl;
using Infragistics.Win.UltraWinTabs;
using Infragistics.Win;

namespace Accounting
{
    public class HideTabHeaders : IUIElementCreationFilter
    {
        private int height;
        private static int firstHeight;
        public HideTabHeaders()
        {

        }
        #region IUIElementCreationFilter Members

        public void AfterCreateChildElements(UIElement parent)
        {
            // Do nothing
        }

        public bool BeforeCreateChildElements(UIElement parent)
        {
            if (parent is TabHeaderAreaUIElement)
            {
                /*	By returning True, we will be preventing the further creation of child elements
                    in the TabHeaderArea, including each tab's header.  This will also remove the
                    line you see along the top of each Tab page.  A line can be created and added
                    in here to replace it.  The measurements are set up to place the line at the
                    bottom of the TabHeaderArea's bounds.  This line will make it look like all four
                    sides of the tab pages have a border line.	*/
                TabHeaderAreaUIElement headerArea = parent as TabHeaderAreaUIElement;
                //TabLineUIElement line = new TabLineUIElement(parent,headerArea.TabManager);
                //Rectangle rect = new Rectangle(headerArea.Rect.X, headerArea.Rect.Y + headerArea.Rect.Height - 2,headerArea.Rect.Width,0);
                //line.Rect = rect;
                //headerArea.ChildElements.Add(line);
                height = headerArea.Rect.Height;
                if (firstHeight != height)
                    firstHeight = height;
                else
                    height = 0;
                headerArea.Rect = new Rectangle(headerArea.Rect.X, headerArea.Rect.Y, headerArea.Rect.Width, 0);
                UltraTabControlUIElement uTabControlUI = headerArea.Parent as UltraTabControlUIElement;
                if (uTabControlUI != null)
                {
                    uTabControlUI.Rect = new Rectangle(uTabControlUI.Rect.X, uTabControlUI.Rect.Y,
                                                       uTabControlUI.Rect.Width, uTabControlUI.Rect.Height - height);
                    //uTabControlUI.Control.Size = new Size(uTabControlUI.Control.Size.Width, uTabControlUI.Control.Size.Height - height);
                    //foreach (var childElement in uTabControlUI.ChildElements)
                    //{
                    //    if (childElement is TabPageAreaUIElement)
                    //        childElement.Rect = new Rectangle(childElement.Rect.X, 0, childElement.Rect.Width, childElement.Rect.Height);
                    //}
                }
                return true;
            }
            //else if(parent is UltraTabControlUIElement)
            //{
            //    var uTabControlUI = (UltraTabControlUIElement) parent;
            //    height = uTabControlUI.Rect.Height - uTabControlUI.TabControl.TabPageSize.Height;
            //    return true;
            //}
            else if (parent is TabPageAreaUIElement)
            {
                //var tabPage = (TabPageAreaUIElement)parent;
                parent.Rect = new Rectangle(parent.Rect.X, 0, parent.Rect.Width,
                                             parent.Rect.Height);
                return true;
            }
            else
            {
                //Otherwise continue to allow processing of the elements.
                return false;
            }
        }

        #endregion
    }
}
