﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Infragistics.Win.UltraWinGrid;

namespace Accounting.Frm
{
    public class UtilsNoStatic
    {

        /// <summary>
        /// Hàm add combo vào Grid có Autosearch
        /// [Huy Anh]
        /// </summary>    
        /// <typeparam name="TK">Đối tượng của Form</typeparam>
        /// <typeparam name="T"></typeparam>
        /// <param name="this"></param>
        /// <param name="uGrid">UltraGrid chứa combo</param>
        /// <param name="columnName">Tên Cột gắn combo</param> 
        /// <param name="inputItem">DS truyền vào combo</param>
        /// <param name="valueMember">ValueMember</param>
        /// <param name="displayMember">DisplayMember</param>
        /// <param name="nameTable">Tên bảng</param>
        /// <param name="nameParentId">Tên cột chứa parentID</param>
        /// <param name="nameParentNode">Tên cột xác định có phải là cha hay ko</param>
        /// <param name="accountingObjectType">Loại đối tượng AccountingObject: 0-Khách hàng. 1-Nhà cung cấp. 2-Nhân viên. 3-KH và NCC. [Mặc định là lấy đầy đủ ds]</param>
        public void ConfigCbbToGrid<TK, T>(Form @this, UltraGrid @uGrid, string @columnName, IList<T> @inputItem, string @valueMember,
                                              string @displayMember, string @nameTable, string nameParentId = null,
                                              string @nameParentNode = null, int? accountingObjectType = null, bool isThread = false, Thread thread = null, bool returnMethod = false)
        {
            if (!isThread || returnMethod)
            {
                UltraGridColumn @ugc = @uGrid.DisplayLayout.Bands[0].Columns[@columnName];
                @ugc.ValueList = @this.GetValueList<TK, T>(@uGrid, @inputItem, @valueMember, @displayMember, @nameTable,
                                                           nameParentId,
                                                           @nameParentNode, accountingObjectType);
                @ugc.Tag = typeof(T);
                if (typeof(T) == typeof(AccountingObject) || typeof(T) == typeof(MaterialGoodsCustom))
                {
                    @ugc.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.None;
                }
                else
                {
                    @ugc.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.Default;
                    @ugc.AutoSuggestFilterMode = Infragistics.Win.AutoSuggestFilterMode.Contains;
                }
                if (returnMethod)
                {
                    thread.StopThread();
                }
            }
            else
            {
                Utils.CreateThread(ref thread, () => @this.Invoke(() => ConfigCbbToGrid<TK, T>(@this, @uGrid, @columnName, @inputItem,
                                                      @valueMember,
                                                      @displayMember, @nameTable, nameParentId, @nameParentNode, accountingObjectType, true, thread, true)));
                thread.StartThread();
            }
        }

        /// <summary>
        /// Hàm add combo fixedAsset vào Grid có Autosearch
        /// [Huy Anh]
        /// </summary>    
        /// <typeparam name="T"></typeparam>
        /// <param name="this"></param>
        /// <param name="uGrid">UltraGrid chứa combo</param>
        /// <param name="columnName">Tên Cột gắn combo</param> 
        /// <param name="inputItem">DS truyền vào combo</param>
        /// <param name="valueMember">ValueMember</param>
        /// <param name="displayMember">DisplayMember</param>
        /// <param name="nameTable">Tên bảng</param>
        /// <param name="fixedAssetType">Loại ds Mã TS: 0-Nằm trong FAIncrement. 1-Không nằm trong FAIncrement. null-Tất cả.</param>
        public void ConfigCbbFixedAssetToGrid<T>(Form @this, UltraGrid @uGrid, string @columnName, IList<T> @inputItem, string @valueMember,
                                              string @displayMember, string @nameTable, int? fixedAssetType = null, bool isThread = false, Thread thread = null, bool returnMethod = false)
        {
            if (!isThread || returnMethod)
            {
                UltraGridColumn @ugc = @uGrid.DisplayLayout.Bands[0].Columns[@columnName];
                @ugc.ValueList = @this.GetValueList(@uGrid, @inputItem, @valueMember, @displayMember, @nameTable, fixedAssetType);
                @ugc.Tag = typeof(T);
                @ugc.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.None;
                if (returnMethod)
                {
                    thread.StopThread();
                }
            }
            else
            {
                Utils.CreateThread(ref thread, () => @this.Invoke(() => ConfigCbbFixedAssetToGrid<T>(@this, @uGrid, @columnName, @inputItem,
                                                      @valueMember,
                                                      @displayMember, @nameTable, fixedAssetType, true, thread, true)));
                thread.StartThread();
            }
        }
    }
}
