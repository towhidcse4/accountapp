﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinExplorerBar;
using Infragistics.Win.UltraWinTabControl;

namespace Accounting
{
    public partial class CustormForm : Form
    {
        public Control LastControl;
        public bool IsCloseNonStatic { get { return IsClose; } }
        public bool IsClose;
        public Dictionary<Control, Keys> PessKeys = new Dictionary<Control, Keys>();
        /// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
        public new DialogResult ShowDialog()
        {
            this.ShowInTaskbar = false;
            this.TopLevel = true;
            return base.ShowDialog();
        }

        public new DialogResult ShowDialog(IWin32Window owner)
        {
            this.ShowInTaskbar = true;
            this.TopLevel = true;
            return base.ShowDialog(owner);
        }
        public new void ShowDialogHD(IWin32Window owner)
        {
            this.ShowInTaskbar = true;
            this.TopLevel = true;
            base.Show(owner);
            //return base.ShowDialog(owner);
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (!(this is FMain))
                this.Icon = global::Accounting.Properties.Resources.icon;
            ConfigControl(this, this);
            this.Load += new EventHandler(CustormForm_Load);
            this.Layout += new LayoutEventHandler(CustormForm_Layout);
            this.SortFormControls();
            this.Activate();
        }
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.Enter))
            {
                SendKeys.Send("{TAB}");
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }
        void CustormForm_Layout(object sender, LayoutEventArgs e)
        {
            this.BringToFront();
            this.Activate();
        }

        void CustormForm_Load(object sender, EventArgs e)
        {
            this.Activate();
        }

        private static void ConfigControl(CustormForm @this, Control container)
        {
            foreach (Control control in container.Controls)
            {
                if (control is UltraTabControl || control is UltraExplorerBar || control is UltraButton)
                {
                    if (control.HasProperty("DrawFilter"))
                        control.SetProperty("DrawFilter", new DrawFilter());
                }
                if (control is UltraTabControl)
                {
                    ((UltraTabControl)control).Style = UltraTabControlStyle.Flat;
                }
                if (control is UltraButton)
                {
                    control.TabStop = false;
                    if (control.HasProperty("Cursor"))
                        control.SetProperty("Cursor", System.Windows.Forms.Cursors.Hand);
                    if (control.HasProperty("ButtonStyle"))
                        control.SetProperty("ButtonStyle", Infragistics.Win.UIElementButtonStyle.Default);
                }
                if (control is Infragistics.Win.UltraWinEditors.UltraDateTimeEditor)
                {
                    ((Infragistics.Win.UltraWinEditors.UltraDateTimeEditor)control).MaskInput = "dd/mm/yyyy";
                    ((Infragistics.Win.UltraWinEditors.UltraDateTimeEditor)control).FormatProvider = System.Globalization.CultureInfo.InvariantCulture;
                    ((Infragistics.Win.UltraWinEditors.UltraDateTimeEditor)control).MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeBoth;
                    ((Infragistics.Win.UltraWinEditors.UltraDateTimeEditor)control).MaskClipMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                    ((Infragistics.Win.UltraWinEditors.UltraDateTimeEditor)control).Appearance.TextVAlign = VAlign.Middle;
                }
                if (control is Infragistics.Win.UltraWinEditors.UltraTextEditor)
                {
                    Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor = control as Infragistics.Win.UltraWinEditors.UltraTextEditor;
                    EditorWithText editor = ultraTextEditor.Editor as EditorWithText;
                    TextBox textBox = editor.TextBox;
                    //Gán sự kiện click cho Textbox
                    textBox.Click += new EventHandler((s, e) => controlTextBox_Click(s, e, @this));
                    textBox.DoubleClick += new EventHandler((s, e) => control_DoubleClick(s, e, @this));
                    textBox.LostFocus += new EventHandler((s, e) => control_LostFocus(s, e, @this));
                    if (control.Name.StartsWith("txtSoHD"))
                    {
                        textBox.KeyPress += txtInvoiceNo_KeyPress;
                        textBox.TextChanged += new EventHandler((s, e) => TextBox_Changed(s, e, @this));
                    }
                }
                control.Click += new EventHandler((s, e) => control_Click(s, e, @this));
                control.KeyDown += new KeyEventHandler((s, e) => control_KeyDown(s, e, @this));
                if (control.HasChildren)
                    ConfigControl(@this, control);
            }
        }
        public static string LastContrlNameClick;
        private static void controlTextBox_Click(object s, EventArgs e, CustormForm @this)
        {
            try
            {
                string controlName = (s as Control).Name;
                if (controlName != LastContrlNameClick) //Check xem textbox hiện tại có focus không, để không lặp lại SelectAll
                {
                    LastContrlNameClick = controlName;
                    var diengiai = (s as TextBox).Text;
                    if (!String.IsNullOrEmpty(diengiai))
                        (s as TextBox).SelectAll();
                    Clipboard.SetText(diengiai);
                }

                //}
            }
            catch (Exception ex)
            { }
        }
        private static void TextBox_Changed(object s, EventArgs e, CustormForm @this)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch((s as TextBox).Text, "[^0-9]"))
            {
                (s as TextBox).Text = "";
            }
        }
        private static void txtInvoiceNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }
        private static void control_DoubleClick(object s, EventArgs e, CustormForm @this)
        {
            try
            {
                var diengiai = (s as TextBox).Text;
                if (!String.IsNullOrEmpty(diengiai))
                    (s as TextBox).SelectAll();
                Clipboard.SetText(diengiai);
            }
            catch (Exception ex)
            { }
        }
        private static void control_LostFocus(object s, EventArgs e, CustormForm @this)
        {
            LastContrlNameClick = null;
        }
        static void control_KeyDown(object sender, KeyEventArgs e, CustormForm @this)
        {
            var control = (Control)sender;
            if (@this.PessKeys.Any(x => x.Key == control))
            {
                @this.PessKeys.Remove(control);
            }
            //@this.PessKeys.Add(control, e.KeyCode);
            if (e.KeyCode == Keys.Tab)
                @this.LastControl = control;
        }

        private static void control_Click(object sender, EventArgs e, CustormForm @this)
        {
            if (@this.PessKeys.Any(x => x.Key == (Control)sender))
            {
                @this.PessKeys.Remove((Control)sender);
            }
        }

        //protected override bool ProcessCmdKey(ref Message message, Keys keys)
        //{
        //    if (ActiveControl == null) goto end;
        //    if (PessKeys.Any(x => x.Key == ActiveControl))
        //    {
        //        PessKeys.Remove(ActiveControl);
        //    }
        //    PessKeys.Add(ActiveControl, keys);
        //end:
        //    return base.ProcessCmdKey(ref message, keys);
        //}


        /// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }
    }
}
