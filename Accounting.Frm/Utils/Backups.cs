﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.SqlClient;
using Accounting.TextMessage;
using Accounting.Core;
using System.Data;

namespace Accounting
{
    class Backups
    {
        // <summary>
        /// SQL server connection string
        /// </summary>
        static string serverName = Properties.Settings.Default.ServerName;
        static string database = Properties.Settings.Default.DatabaseName;
        static string user = Properties.Settings.Default.UserName;
        static string pass = Properties.Settings.Default.Password;
        static string strConStr = "Data Source=" + serverName + ";Initial Catalog=" + database + ";User ID=" + user + ";Password=" + pass + "";
        /// <summary>
        /// Global SQL server connection
        /// </summary>
        public static SqlConnection connection;
        public Backups()
        {
            //Hautv edit
            if (user.Equals("ADMIN") && string.IsNullOrEmpty(pass))
                strConStr = string.Format("Data Source={0};initial catalog=" + database + ";Integrated Security=SSPI;", serverName);
            if (connection == null) { connection = new SqlConnection(strConStr); }
            //if (connection.State != ConnectionState.Open) { connection.Open(); }
        }
        public void Create()
        {
            WaitingFrm.StartWaiting();
            try
            {
                var backup_path = Utils.ListSystemOption.FirstOrDefault(o => o.Code == "SLUU_TMSaoLuu").Data;
                string database = Properties.Settings.Default.DatabaseName;

                SqlDataProvider sqlClient = new SqlDataProvider();

                using (System.IO.Stream stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("Accounting.Data.backup.sql"))
                {
                    if (stream != null)
                    {
                        using (StreamReader sr = new StreamReader(stream))
                        {
                            var fileContent = sr.ReadToEnd();
                            string default_path = "C:\\Backup";
                            // set backup path
                            if (!backup_path.IsNullOrEmpty())
                            {
                                fileContent = fileContent.Replace(@default_path, @backup_path);
                            }                            

                            // set database name
                            fileContent = fileContent.Replace("ACCOUNTING", database).Replace("\r\n", " ");
                            var sqlqueries = fileContent.Split(new[] { " GO " }, StringSplitOptions.RemoveEmptyEntries);

                            foreach (var query in sqlqueries)
                            {
                                try
                                {
                                    new SqlCommand(query, GetConnection()).ExecuteNonQuery();
                                }
                                catch (Exception ex)
                                {
                                    WaitingFrm.StopWaiting();
                                    MSG.Warning("Chưa tạo được file backup!" + "\r\n" + ex.ToString());
                                    return;
                                }

                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                WaitingFrm.StopWaiting();
                //MSG.Warning(ex.ToString());
            }
        }

        public static SqlConnection GetConnection()
        {
            try
            {
                if (connection == null) { connection = new SqlConnection(strConStr); }
                if (connection.State == ConnectionState.Closed)
                {
                    //connection.Close();
                    connection.Open();
                    return connection;
                }
                else
                    return connection;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
