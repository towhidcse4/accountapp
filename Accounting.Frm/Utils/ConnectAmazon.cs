﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.SqlClient;
using Accounting.TextMessage;
using Accounting.Core;
using System.Data;
using Amazon.S3.Transfer;
using Amazon.S3;
using Amazon.S3.Model;
using System.Configuration;
using System.Collections.Specialized;

namespace Accounting
{
    public class ConnectAmazon
    {
        private const string BUCKET_NAME = "my-bucket-name";
        private const string S3_KEY = "s3_key";
        private const int TIMEOUT = 900;
        private const int ERRORTRY = 3;
        private TransferUtility _transferUtility;
        public bool Connet()
        {
            AmazonS3Client s3Client = GetS3Client();
            if (s3Client != null)
            {
                Console.Out.WriteLine("Ket noi thanh cong");
                CreateBucket(s3Client);
                //CreateNewFile(s3Client);
                //CreateNewFolder(s3Client);
                //CreateNewFileInFolder(s3Client);
                return true;
            }
            else
            {
                Console.Out.WriteLine("Ket noi k thanh cong");
                return false;
            }
        }

        public AmazonS3Client GetS3Client()
        {
            NameValueCollection appConfig = ConfigurationManager.AppSettings;
            AmazonS3Config config = new AmazonS3Config();
            config.Timeout = TimeSpan.FromSeconds(TIMEOUT);
            config.MaxErrorRetry = ERRORTRY;

            AmazonS3Client s3Client = new AmazonS3Client(
                    appConfig["AWSAccessKey"],
                    appConfig["AWSSecretKey"],
                    appConfig["AWSRegion"]
                    //config
                    );
            return s3Client;
        }

        /// <summary>
        /// Creates bucket if it is not exists.
        /// </summary>
        /// <param name="client"></param>
        private void CreateBucket(AmazonS3Client client)
        {
            Console.Out.WriteLine("Checking S3 bucket with name " + BUCKET_NAME);

            ListBucketsResponse response = client.ListBuckets();

            bool found = false;
            foreach (S3Bucket bucket in response.Buckets)
            {
                if (bucket.BucketName == BUCKET_NAME)
                {
                    Console.Out.WriteLine("Bucket does exists");
                    found = true;
                    break;
                }
            }

            if (found == false)
            {
                Console.Out.WriteLine("Bucket not found will create it.");

                PutBucketRequest request = new PutBucketRequest();
                request.BucketName = BUCKET_NAME;
                client.PutBucket(request);

                Console.Out.WriteLine("Created S3 bucket with name " + BUCKET_NAME);
            }
        }

        public void CreateNewFile(AmazonS3Client client)
        {
            String S3_KEY = "Demo Create File.txt";
            PutObjectRequest request = new PutObjectRequest();
            request.BucketName = BUCKET_NAME;
            request.Key = S3_KEY;
            request.ContentBody = "This is body of S3 object.";
            client.PutObject(request);
        }

        public void CreateNewFolder(AmazonS3Client client)
        {
            String S3_KEY = "Demo Create Folder/";
            PutObjectRequest request = new PutObjectRequest();
            request.BucketName = BUCKET_NAME;
            request.Key = S3_KEY;
            request.ContentBody = "";
            client.PutObject(request);
        }

        public void CreateNewFileInFolder(AmazonS3Client client)
        {
            String S3_KEY = "Demo Create Folder/" + "Demo Create File.txt";
            PutObjectRequest request = new PutObjectRequest();
            request.BucketName = BUCKET_NAME;
            request.Key = S3_KEY;
            request.ContentBody = "This is body of S3 object.";
            client.PutObject(request);
        }

        public void UploadFile(AmazonS3Client client)
        {
            PutObjectRequest request = new PutObjectRequest();
            request.BucketName = BUCKET_NAME;
            request.Key = S3_KEY;
            request.FilePath = "";
            client.PutObject(request);
        }

        public MemoryStream GetFile(AmazonS3Client s3Client)
        {
            using (s3Client)
            {
                MemoryStream file = new MemoryStream();
                try
                {
                    GetObjectResponse r = s3Client.GetObject(new GetObjectRequest()
                    {
                        BucketName = BUCKET_NAME,
                        Key = S3_KEY
                    });
                    try
                    {
                        long transferred = 0L;
                        BufferedStream stream2 = new BufferedStream(r.ResponseStream);
                        byte[] buffer = new byte[0x2000];
                        int count = 0;
                        while ((count = stream2.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            file.Write(buffer, 0, count);
                        }
                    }
                    finally
                    {
                    }
                    return file;
                }
                catch (AmazonS3Exception)
                {
                    //Show exception
                }
            }
            return null;
        }

        //public static void DeleteFile(AmazonS3Client Client)
        //{
        //    DeleteObjectRequest request = new DeleteObjectRequest()
        //    {
        //        BucketName = BUCKET_NAME,
        //        Key = S3_KEY
        //    };
        //    S3Response response = Client.DeleteObject(request);
        //}

        public void CopyFile(AmazonS3Client s3Client)
        {
            String destinationPath = "";
            CopyObjectRequest request = new CopyObjectRequest()
            {
                SourceBucket = BUCKET_NAME,
                SourceKey = S3_KEY,
                DestinationBucket = BUCKET_NAME,
                DestinationKey = destinationPath
            };
            CopyObjectResponse response = s3Client.CopyObject(request);
        }

        //public static void ShareFile(AmazonS3Client s3Client)
        //{
        //    S3Response response1 = s3Client.SetACL(new SetACLRequest()
        //    {
        //        CannedACL = S3CannedACL.PublicReadWritại
        //        BucketName = BUCKET_NAME,
        //        Key = S3_KEY
        //    });
        //}

        public String MakeUrl(AmazonS3Client s3Client)
        {
            string preSignedURL = s3Client.GetPreSignedURL(new GetPreSignedUrlRequest()
            {
                BucketName = BUCKET_NAME,
                Key = S3_KEY,
                Expires = System.DateTime.Now.AddMinutes(30)

            });

            return preSignedURL;
        }

        public void DownloadFile(AmazonS3Client client, string path)
        {
            _transferUtility = new TransferUtility(client);
            var request = new TransferUtilityDownloadRequest()
            {
                BucketName = BUCKET_NAME,
                FilePath = path,
                Key = S3_KEY
            };

            _transferUtility.Download(request);
        }
    }
}
