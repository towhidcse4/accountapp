﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting
{
    public partial class DialogForm : CustormForm
    {
        protected override void OnLoad(EventArgs e)
        {
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.StartPosition = FormStartPosition.CenterScreen;
            base.OnLoad(e);
            SplashScreen.SetStatus("Loading CustormForm");
            this.Activate();
            this.SortFormControls();
        }
    }
}
