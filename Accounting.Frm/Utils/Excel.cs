﻿using Accounting.Core.Domain;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Accounting
{
    class Excel
    {
        public readonly List<String> HEADER = new List<String> { "MÃ NV", "TÊN NV", "SỐ CÔNG CẢ NGÀY", "SỐ CÔNG NỬA NGÀY", "TỔNG CÔNG LÀM THÊM", "PHÒNG BAN" };
        string path = "";
        _Application excel = new Application();
        Workbook wb;
        Worksheet ws;
        Range xlRange;

        public void ChooseSheet(int sheet)
        {
            ws = wb.Worksheets[sheet];
            xlRange = ws.UsedRange;
        }

        public Excel(string path)
        {
            this.path = path;
            wb = excel.Workbooks.Open(path);
        }

        public string ReadCell(int i, int j)
        {
            i++;
            j++;
            Range x = (Range)ws.Cells[i, j];
            if (x.Value2 != null) return (string)x.Value2;
            else return "";
        }

        public Dictionary<string, int> ReadHeader()
        {
            int y = xlRange.Columns.Count;
            Dictionary<string, int> headers = new Dictionary<string, int>();
            for (int i = 1; i <= y; i++)
            {
                Range x = (Range)ws.Cells[3, i];
                if (x.Value2 != null)
                {
                    string header = "";
                    try
                    {
                        header = ((string)x.Value2).ToUpper();
                    }
                    catch
                    {
                        header = ((double)x.Value2).ToString().ToUpper();
                    }

                    if (HEADER.Contains(header) && !headers.ContainsKey(header))
                    {
                        headers.Add(header, i);
                    }
                };
            }
            return headers;
        }

        public List<PSTimeSheetSummaryDetail> GetObject(Dictionary<string, int> headers, int typeid)
        {
            List<PSTimeSheetSummaryDetail> lst = new List<PSTimeSheetSummaryDetail>();
            int x = xlRange.Rows.Count;
            int y = xlRange.Columns.Count;
            for (int i = 4; i <= x; i++)
            {
                // Lấy mã nv
                string maNV = "";
                if (headers.ContainsKey(HEADER[0]))
                {
                    Range manvr = (Range)ws.Cells[i, headers[HEADER[0]]];
                    try
                    {
                        maNV = ((string)manvr.Value2).ToUpper();
                    }
                    catch
                    {
                        
                    }
                }
                if (maNV.IsNullOrEmpty()) continue;

                // Lấy tên nv
                string tenNV = "";
                if (headers.ContainsKey(HEADER[1]))
                {
                    Range tennvr = (Range)ws.Cells[i, headers[HEADER[1]]];
                    try
                    {
                        tenNV = ((string)tennvr.Value2).ToUpper();
                    }
                    catch { }
                }

                // Lấy mã pb
                string maPb = "";
                if (headers.ContainsKey(HEADER[5]))
                {
                    Range maPbr = (Range)ws.Cells[i, headers[HEADER[5]]];
                    try
                    {
                        maPb = ((string)maPbr.Value2).ToUpper();
                    }
                    catch { }
                }

                // số ngày công cả ngày
                double snccn = 0;
                if (headers.ContainsKey(HEADER[2]))
                {
                    Range snccnr = (Range)ws.Cells[i, headers[HEADER[2]]];

                    try
                    {
                        snccn = (double)snccnr.Value2;
                    }
                    catch { }
                }


                // số ngày công nửa ngày
                double sncnn = 0;
                if (headers.ContainsKey(HEADER[3]))
                {
                    Range sncnnr = (Range)ws.Cells[i, headers[HEADER[3]]];

                    try
                    {
                        sncnn = (double)sncnnr.Value2;
                    }
                    catch { }
                }


                // tổng công làm thêm
                double tclt = 0;
                if (headers.ContainsKey(HEADER[4]))
                {
                    Range tcltr = (Range)ws.Cells[i, headers[HEADER[4]]];
                    try
                    {
                        tclt = (double)tcltr.Value2;
                    }
                    catch { }
                }

                AccountingObject ao = Utils.ListAccountingObject.Where(acc => acc.AccountingObjectCode == maNV).FirstOrDefault();
                Department department = Utils.ListDepartment.Where(dep => dep.DepartmentCode == maPb).FirstOrDefault();
                String mess = "";
                if (ao == null)
                    mess += "Nhân viên không tồn tại,";
                else
                {
                    if (tenNV.IsNullOrEmpty() || ao.AccountingObjectName.ToLower() != tenNV.ToLower())
                        mess += "Sai tên nhân viên,";
                    if (ao.Department != null && ao.Department.DepartmentCode != maPb)
                        mess += "Sai mã phòng ban";
                    if (ao.Department == null && !maPb.IsNullOrEmpty())
                        mess += "Sai mã phòng ban";
                }

                if (ao == null)
                    lst.Add(new PSTimeSheetSummaryDetail
                    {
                        WorkAllDay = (decimal)snccn,
                        WorkHalfADay = (decimal)sncnn,
                        TotalOverTime = (decimal)tclt,
                        Total = (decimal)(snccn + sncnn + tclt),
                        DepartmentCode = maPb,
                        EmployeeCode = maNV,
                        EmployeeName = tenNV,
                        Message = mess,
                    });
                else
                    lst.Add(new PSTimeSheetSummaryDetail
                    {
                        EmployeeID = ao.ID,
                        DepartmentID = ao.DepartmentID,
                        AccountingObjectName = ao.AccountingObjectName,
                        WorkAllDay = (decimal)snccn,
                        WorkHalfADay = (decimal)sncnn,
                        TotalOverTime = (decimal)tclt,
                        Total = (decimal)(snccn + sncnn + tclt),
                        DepartmentCode = maPb,
                        EmployeeCode = maNV,
                        EmployeeName = tenNV,
                        Message = mess,
                    });

            }
            return lst;
        }

        public List<string> ReadSheetName()
        {
            List<string> result = new List<string>();
            for (int i = 1; i <= wb.Worksheets.Count; i++)
            {
                result.Add(wb.Worksheets[i].Name);
            }
            return result;
        }
    }
}
