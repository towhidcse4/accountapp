﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting
{
    public class StartApplication : CustormForm
    {
        public NotifyIcon ncIcon;
        private ContextMenuStrip mnContext;
        private ToolStripMenuItem mniExit;
        private IContainer components;
        private bool _isClose = false;

        public StartApplication()
        {
            InitializeComponent();
            SplashScreen.SetStatus("Loading StartApplication");
            Application.ApplicationExit += new EventHandler(Application_ApplicationExit);
            this.FormClosed += new FormClosedEventHandler(StartApplication_FormClosed);
        }

        void StartApplication_FormClosed(object sender, FormClosedEventArgs e)
        {
            _isClose = true;
        }

        protected void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StartApplication));
            this.ncIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.mnContext = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mniExit = new System.Windows.Forms.ToolStripMenuItem();
            this.mnContext.SuspendLayout();
            this.SuspendLayout();
            // 
            // ncIcon
            // 
            this.ncIcon.ContextMenuStrip = this.mnContext;
            this.ncIcon.Icon = global::Accounting.Properties.Resources.icon;
            this.ncIcon.Text = "Phần mềm kế toán";
            this.ncIcon.Visible = true;
            this.ncIcon.DoubleClick += new System.EventHandler(this.ncIcon_DoubleClick);
            // 
            // mnContext
            // 
            this.mnContext.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mniExit});
            this.mnContext.Name = "mnContext";
            this.mnContext.Size = new System.Drawing.Size(106, 26);
            // 
            // mniExit
            // 
            this.mniExit.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.mniExit.Name = "mniExit";
            this.mniExit.Size = new System.Drawing.Size(105, 22);
            this.mniExit.Text = "Thoát";
            this.mniExit.Click += new System.EventHandler(this.mniExit_Click);
            // 
            // StartApplication
            // 
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "StartApplication";
            this.TransparencyKey = System.Drawing.Color.Fuchsia;
            this.Resize += new System.EventHandler(this.StartApplication_Resize);
            this.mnContext.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        void Application_ApplicationExit(object sender, EventArgs e)
        {
            if (_isClose)
                ncIcon.Icon = null;
        }

        private void mniExit_Click(object sender, EventArgs e)
        {
            _isClose = true;
            Application.Exit();
        }

        private void ncIcon_DoubleClick(object sender, EventArgs e)
        {
            //if (Visible)
            //{
            //    Hide();
            //    return;
            //}
            //Show();
            //WindowState = FormWindowState.Maximized;
        }

        private void StartApplication_Resize(object sender, EventArgs e)
        {
            //if (FormWindowState.Minimized == WindowState)
            //{
            //    Hide();
            //}
        }
    }
}
