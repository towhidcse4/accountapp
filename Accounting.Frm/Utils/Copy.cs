﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using log4net.Config;
using log4net;
using System.Data.SqlClient;
using System.Data;
using System.Xml;
using System.Linq;
using System.Globalization;

namespace Accounting
{
    public static class Copy
    {
        public static void CopyAllFile(string sourceDir, string targetDir, bool overwrite = false)
        {
            Directory.CreateDirectory(targetDir);

            foreach (var file in Directory.GetFiles(sourceDir))
                File.Copy(file, Path.Combine(targetDir, Path.GetFileName(file)), overwrite);

            foreach (var directory in Directory.GetDirectories(sourceDir))
                CopyAllFile(directory, Path.Combine(targetDir, Path.GetFileName(directory)));
        }
    }
}
