﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Infragistics.Win.UltraWinEditors;

namespace Accounting
{
    public partial class UltraOptionSet_Ex : UltraOptionSet
    {


        #region Properties
        private bool CancelPaint;
        private PaintEventArgs grap;
        private bool readOnly;

        [Category("Layout")]
        [Description("Determinate if the control is ReadOnly")]
        public bool ReadOnly
        {
            get { return readOnly; }
            set
            {
                readOnly = value;
                if (!readOnly) CancelPaint = false;
            }
        }

        #endregion Properties



        public UltraOptionSet_Ex()
        {
            InitializeComponent();
        }

        #region Overrides
        protected override void OnPaint(PaintEventArgs pe)
        {
            //if (readOnly && CancelPaint) pe = grap;
            //grap = pe;
            base.OnPaint(pe);
        }

        protected override void OnClick(EventArgs e)
        {
            if (readOnly)
            {
                this.Invalidate(); return;
            }
            base.OnClick(e);
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            if (readOnly) CancelPaint = true;
            base.OnMouseDown(e);
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            if (CancelPaint) CancelPaint = false;
            base.OnMouseUp(e);
        }

        protected override void OnDoubleClick(EventArgs e)
        {
            if (readOnly) { this.Invalidate(); return; }
            base.OnDoubleClick(e);
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (readOnly) { this.Invalidate(); return; }
            base.OnKeyDown(e);
        }

        protected override void OnMouseEnter(EventArgs e)
        {
            //if (readOnly) CancelPaint = true;
            base.OnMouseEnter(e);
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);
        }
        #endregion Overrides
    }
}
