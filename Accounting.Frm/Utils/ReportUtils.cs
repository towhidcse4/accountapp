﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using PerpetuumSoft.Framework.Drawing;
using Accounting.TextMessage;
using Margins = PerpetuumSoft.Framework.Drawing.Margins;
using TextBox = PerpetuumSoft.Reporting.DOM.TextBox;
using System.Globalization;
using System.Drawing;
using Infragistics.Win.UltraWinTree;
using PerpetuumSoft.Framework.Text;
using NHibernate.AdoNet.Util;
using System.Runtime.Serialization;
using Accounting.Core.DAO;
//using Accounting.Core.Domain.obj.Report;
//using Accounting.Frm.FReport;

namespace Accounting
{
    public static class ReportUtils
    {
        #region [HuyAnh]
        #region khai báo


        #endregion
        /// <summary>
        /// Mở form theo Hyperlink
        /// </summary>
        /// <param name="hyperlink"></param>
        public static void Hyperlink(string hyperlink)
        {
            // add by tungnt: view form
            string[] resultStr = hyperlink.Split(';');
            Guid _RefID = new Guid(resultStr[0]);
            int _TypeID = int.Parse(resultStr[1]);
            Utils.ViewVoucherSelected1(_RefID, _TypeID);

            #region code cũ
            //try
            //{
            //    IMCReceiptService IMCReceiptService = IoC.Resolve<IMCReceiptService>();
            //    IMCPaymentService IMCPaymentService = IoC.Resolve<IMCPaymentService>();
            //    IMBDepositService IMBDepositService = IoC.Resolve<IMBDepositService>();
            //    IRSTransferService IRSTransferService = IoC.Resolve<IRSTransferService>();
            //    string[] resultStr = hyperlink.Split(';');
            //    if (resultStr[0].StartsWith("10"))
            //    {
            //        //Phiếu Thu
            //        MCReceipt mcReceipt = IMCReceiptService.Getbykey(new Guid(resultStr[1]));
            //        new FMCReceiptDetail(mcReceipt, IMCReceiptService.GetAll(),
            //                                             ConstFrm.optStatusForm.View).ShowDialog();
            //    }
            //    else if (resultStr[0].StartsWith("11"))
            //    {    //Phiếu chi
            //        MCPayment mcPayment = IMCPaymentService.Getbykey(new Guid(resultStr[1]));
            //        new FMCPaymentDetail(mcPayment, IMCPaymentService.GetAll(), ConstFrm.optStatusForm.View);
            //    }
            //    else if (resultStr[0].Equals(121) || resultStr[0].Equals(121) || resultStr[0].Equals(121))
            //    {    //Trả lương
            //        //
            //    }
            //    else if (resultStr[0].Equals(122) || resultStr[0].Equals(123) || resultStr[0].Equals(124))
            //    {    //Nộp thuế
            //        //
            //    }
            //    else if (resultStr[0].Equals(128) || resultStr[0].Equals(134) || resultStr[0].Equals(144) || resultStr[0].Equals(174) || resultStr[0].Equals(250))
            //    {    //Trả tiền NCC

            //    }
            //    else if (resultStr[0].StartsWith("16"))
            //    {    //Nộp tiền vào TK
            //        MBDeposit mbDeposit = IMBDepositService.Getbykey(new Guid(resultStr[1]));
            //        new FMBDepositDetail(mbDeposit, IMBDepositService.GetAll(), ConstFrm.optStatusForm.View).ShowDialog();
            //    }
            //    else
            //        switch (resultStr[0])
            //        {
            //            case "120":	//	Ủy nhiệm chi    
            //                break;
            //            case "121":	//	Trả lương bằng Ủy nhiệm chi 

            //                break;
            //            case "125":	//	Nộp bảo hiểm bằng Ủy nhiệm chi 
            //                break;
            //            case "126":	//	Mua dịch vụ bằng Ủy nhiệm chi  
            //                break;
            //            case "127":	//	Mua hàng bằng Ủy nhiệm chi  
            //                break;
            //            case "128":	//	Trả tiền NCC bằng Ủy nhiệm chi 
            //                break;
            //            case "129":	//	Mua TSCĐ bằng Ủy nhiệm chi 
            //                break;
            //            case "130":	//	Séc chuyển khoản   
            //                break;
            //            case "131":	//	Mua hàng bằng Séc chuyển khoản
            //                break;
            //            case "132":	//	Mua TSCĐ bằng Séc chuyển khoản
            //                break;
            //            case "133":	//	Mua dịch vụ bằng Séc chuyển khoản 
            //                break;
            //            case "134":	//	Trả tiền NCC bằng Séc chuyển khoản
            //                break;
            //            case "140":	//	Séc tiền mặt 
            //                break;
            //            case "141":	//	Mua hàng bằng Séc TM  
            //                break;
            //            case "142":	//	Mua TSCĐ bằng Séc TM  
            //                break;
            //            case "143":	//	Mua dịch vụ bằng Séc TM 
            //                break;
            //            case "144":	//	Trả tiền NCC bằng Séc TM
            //                break;
            //            case "150":	//	Chuyển tiền nội bộ 
            //                break;
            //            case "170":	//	Trả tiền bằng thẻ tín dụng
            //                break;
            //            case "171":	//	Mua hàng bằng thẻ tín dụng 
            //                break;
            //            case "172":	//	Mua TSCĐ bằng thẻ tín dụng
            //                break;
            //            case "173":	//	Mua dịch vụ bằng thẻ tín dụng 
            //                break;
            //            case "174":	//	Trả tiền NCC bằng thẻ tín dụng 
            //                break;
            //            case "200":	//	Đơn mua hàng   
            //                break;
            //            case "210":	//	Mua hàng chưa thanh toán     
            //                break;
            //            case "220":	//	Hàng mua trả lại    
            //                break;
            //            case "230":	//	Hàng mua giảm giá     
            //                break;
            //            case "240":	//	Mua dịch vụ chưa thanh toán 
            //                break;
            //            case "250":	//	Trả tiền nhà cung cấp 
            //                break;
            //            case "300":	//	Báo giá    
            //                break;
            //            case "310":	//	Đơn đặt hàng   
            //                break;
            //            case "320":	//	Hóa đơn bán hàng   
            //                break;
            //            case "321":	//	Hóa đơn bán hàng thu tiền ngay-Tiền mặt 
            //                break;
            //            case "322":	//	Hóa đơn bán hàng thu tiền ngay-Chuyển khoản   
            //                break;
            //            case "323":	//	Hóa đơn bán hàng đại lý bán đúng giá, nhận ủy thác  XNK    
            //                break;
            //            case "324":	//	Hóa đơn bán hàng đại lý bán đúng giá, nhận ủy thác  XNK-TM  
            //                break;
            //            case "325":	//	Hóa đơn bán hàng đại lý bán đúng giá, nhận ủy thác  XNK-CK 
            //                break;
            //            case "330":	//	Hàng bán trả lại                                
            //                break;
            //            case "340":	//	Giảm giá hàng bán                                
            //                break;
            //            case "350":	//	Tổng hợp hóa đơn bán lẻ    
            //                break;
            //            case "360":	//	Thu tiền khách hàng      
            //                break;
            //            case "400":	//	Nhập kho              
            //                break;
            //            case "401":	//	Nhập kho từ điều chỉnh    
            //                break;
            //            case "402":	//	Nhập kho từ mua hàng   
            //                break;
            //            case "403":	//	Nhập kho từ hàng bán trả lại   
            //                break;
            //            case "404":	//	Nhập kho từ lắp ráp       
            //                break;
            //            case "407":	//	Chứng từ ghi sổ           
            //                break;
            //            case "410":	//	Xuất kho          
            //                break;
            //            case "411":	//	Xuất kho từ tháo dỡ    
            //                break;
            //            case "412":	//	Xuất kho từ bán hàng     
            //                break;
            //            case "413":	//	Xuất kho từ hàng mua trả lại   
            //                break;
            //            case "414":	//	Xuất kho từ điều chỉnh   
            //                break;
            //            case "415":	//	Xuất kho từ bán hàng đại lý bán đúng giá, nhận ủy thác XNK 
            //                break;
            //            case "420":	//	Chuyển kho  
            //                RSTransfer rsTransfer = IRSTransferService.Getbykey(new Guid(resultStr[1]));
            //                new FRSTransferDetail(rsTransfer, IRSTransferService.GetAll(), ConstFrm.optStatusForm.View).ShowDialog();

            //                break;
            //            case "500":	//	Mua TSCĐ      
            //                break;
            //            case "510":	//	Ghi tăng TSCĐ   
            //                break;
            //            case "520":	//	Ghi giảm TSCĐ    
            //                break;
            //            case "530":	//	Điều chỉnh TSCĐ  
            //                break;
            //            case "540":	//	Tính khấu hao TSCĐ 
            //                break;
            //            case "550":	//	Điều chuyển TSCĐ   
            //                break;
            //            case "600":	//	Chứng từ nghiệp vụ khác     
            //                break;
            //            case "610":	//	Kết chuyển chi phí  
            //                break;
            //            case "620":	//	Kết chuyển lãi, lỗ    
            //                break;
            //            case "630":	//	Nghiệm thu công trình, vụ việc  
            //                break;
            //            case "640":	//	Hạch toán chi phí khấu hao TSCĐ 
            //                break;
            //            case "650":	//	Hạch toán chi phí phân bổ CCDC 
            //                break;
            //            case "660":	//	Xử lý chênh lệch tỷ giá từ tính giá xuất quỹ  
            //                break;
            //            case "670":	//	Xử lý chênh lệch tỷ giá
            //                break;
            //            case "900":	//	Đăng ký mua cổ phần 
            //                break;
            //            case "910":	//	Sổ cổ đông         
            //                break;
            //            case "920":	//	Chuyển nhượng cổ phần   
            //                break;
            //            case "930":	//	Nhà đầu tư          
            //                break;
            //        }
            //    return;
            //}
            //catch (Exception ex)
            //{
            //    MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            //    return;
            //} 
            #endregion
        }

        public static string GetAccountingObjectBankAccount(string guid)
        {
            string s = string.Empty;
            var gId = new Guid(guid);
            var acc = Utils.ListAccountingObject.FirstOrDefault(a => a.ID == gId);
            if (acc != null)
            {
                s = acc.BankAccount;
            }
            return s;
        }
        public static bool CheckIsEmployee(string AccountingObjectID)
        {
            bool i = false;
            var guid = new Guid(AccountingObjectID);
            var ob = Utils.ListAccountingObject.FirstOrDefault(b => b.ID == guid);
            i = ob.IsEmployee;
            return i;
        }
        public static string GetAccountingObjectDepartment(string guid)
        {
            string s = string.Empty;
            var gId = new Guid(guid);
            var acc = Utils.ListAccountingObject.FirstOrDefault(a => a.ID == gId);
            if (acc != null)
            {
                var dp = Utils.ListDepartment.FirstOrDefault(d => d.ID == acc.DepartmentID);
                if (dp != null)
                    s = dp.DepartmentName;
            }
            return s;
        }
        public static string GetAccountingObjectDepartment1(string guid)
        {
            string s = string.Empty;
            var gId = new Guid(guid);
            var acc = Utils.ListDepartment.FirstOrDefault(a => a.ID == gId);
            if (acc != null)
            {
                var dp = Utils.ListDepartment.FirstOrDefault(d => d.ID == acc.ID);
                if (dp != null)
                    s = dp.DepartmentName;
            }
            return s;
        }
        public static string GetAccountingObjectCode(string guid)
        {
            string s = string.Empty;
            var gId = new Guid(guid);
            var acc = Utils.ListAccountingObject.FirstOrDefault(a => a.ID == gId);
            if (acc != null)
            {
                var dp = Utils.ListAccountingObject.FirstOrDefault(d => d.ID == acc.ID);
                if (dp != null)
                    s = dp.AccountingObjectCode;
            }
            return s;
        }
        #endregion
        #region [Luongvt]
        public static string GetPeriod(DateTime toDate)
        {
            return string.Format("Thông báo công nợ đến ngày {0}", toDate.ToString("dd/MM/yyyy"));
        }
        public static string GetPeriod(DateTime fromDate, DateTime toDate)
        {
            if (fromDate.Day == 1 && fromDate.Year == toDate.Year)
            {
                // Năm
                if (fromDate.Month == 1 && toDate.Day == 31 && toDate.Month == 12)
                    return string.Format("Năm {0}", fromDate.Year);
                // Quý
                // List<int> lstMonth = new int[] {1,4,7,10};
                if ((new int[] { 1, 4, 7, 10 }).Contains(fromDate.Month))
                {
                    List<string> chars = new List<string> { "I", "II", "III", "IV" };
                    if (fromDate.AddMonths(3).AddDays(-1).Date == toDate.Date)
                        return string.Format("Quý {0} năm {1}", chars[((fromDate.Month - 1) / 3)], fromDate.Year);
                }
                // tháng
                if (fromDate.AddMonths(1).AddDays(-1) == toDate)
                    return string.Format("Tháng {0} năm {1}", fromDate.Month, fromDate.Year);
            }
            return string.Format("Từ ngày {0} đến ngày {1}", fromDate.ToString("dd/MM/yyyy"), toDate.ToString("dd/MM/yyyy"));
        }
        private static Core.Domain.ReportStyle GetStyle()
        {

            Accounting.Core.Domain.ReportStyle reportStyle = new ReportStyle();
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            System.IO.StreamReader file = new System.IO.StreamReader(string.Format("{0}\\Frm\\FReport\\Template\\ReportStyle.xml", path));
            System.Xml.Serialization.XmlSerializer reader = new System.Xml.Serialization.XmlSerializer(typeof(Accounting.Core.Domain.ReportStyle));
            reportStyle = (Accounting.Core.Domain.ReportStyle)reader.Deserialize(file);
            file.Close();
            return reportStyle;
        }
        public static Margins PageMargins()
        {
            ReportStyle reportStyle = GetStyle();
            int n = 100;
            PerpetuumSoft.Framework.Drawing.Margins m = new PerpetuumSoft.Framework.Drawing.Margins(reportStyle.AlignTop * n, reportStyle.AlignBottom * n, reportStyle.AlignLeft * n, 0);
            return m;
        }
        public static Border Border(string style)
        {
            BorderLine dotLine = new BorderLine(LineStyle.Dot, System.Drawing.Color.Black, 0.5);
            BorderLine solidLine = new BorderLine(LineStyle.Solid, System.Drawing.Color.Black, 0.5);
            switch (style)
            {
                case "dot":
                    return new Border(dotLine, dotLine, solidLine, solidLine);
                default:
                    return new Border(solidLine);
            }

        }
        public static string GetProductInfo()
        {
            ReportStyle reportStyle = GetStyle();
            if (reportStyle.ShowProductInfo)
                return "HDSAccoungting";
            return "";
        }
        public static string GetCompanyName()
        {
            return String.Format(Utils.GetCompanyName());
        }
        public static string GetCompanyAddress()
        {
            return String.Format(Utils.GetCompanyAddress());
        }

        public static string GetFormatCurrency()
        {
            return String.Format(Utils.GetFormatCurrency());
        }
        public static string GetCompanyTaxCode()
        {
            return String.Format(Utils.GetCompanyTaxCode());
        }
        public static string GetCompanyTaxCodeForInvoice()
        {
            return String.Format(Utils.GetCompanyTaxCodeForInvoice());
        }
        public static string GetNameOfTheManagingUnit()
        {
            return String.Format(Utils.GetNameOfTheManagingUnit());
        }
        public static string GetTaxCodeOfTheManagingUnit()
        {
            return String.Format(Utils.GetTaxCodeOfTheManagingUnit());
        }
        public static string GetCompanyPhoneNumber()
        {
            return String.Format(Utils.GetCompanyPhoneNumber());
        }
        public static string GetCompanyDistrict()
        {
            return String.Format(Utils.GetDLTDistrict());
        }
        public static string GetCompanyCity()
        {
            return String.Format(Utils.GetDLTCity());
        }
        public static string GetCompanyFax()
        {
            return String.Format(Utils.GetCompanyFax());
        }
        public static string GetCompanyEmail()
        {
            return String.Format(Utils.GetCompanyEmail());
        }
        public static string GetSigner()
        {
            return String.Format(Utils.GetSigner());
        }
        public static string GetCompanyCashier()
        {
            return Utils.GetCompanyCashier();
        }
        public static string GetCompanyChiefAccountant()
        {
            return Utils.GetCompanyChiefAccountant();
        }
        public static string GetCompanyDirector()
        {
            return Utils.GetCompanyDirector();
        }
        public static string GetCompanyReportPreparer()
        {
            return Utils.GetCompanyReportPreparer();
        }
        public static string GetCompanyStoreKeeper()
        {
            return Utils.GetCompanyStoreKeeper();
        }
        public static string GetBankAccount()
        {
            return Utils.GetBankAccount();
        }
        public static string GetBankName()
        {
            return Utils.GetBankName();
        }

        #region set font báo cáo
        public static FontDescriptor GetFontCompanyName()
        {
            string s = Utils.GetFontCompanyName();
            if (!string.IsNullOrEmpty(s))
            {
                List<string> a = s.Split(' ', ';').Where(n => n != "").ToList();
                string b = "";
                foreach (string item in a.Where(n => a.IndexOf(n) < a.Count - 2))
                {
                    b += item + " ";
                }
                b = b.Substring(0, b.Length - 1);
                Font f = new Font(b, float.Parse(a[a.Count - 1]), (FontStyle)Enum.Parse(typeof(FontStyle), a[a.Count - 2], true));
                var font = new FontDescriptor(f);
                return font;
            }
            else
                return null;

        }
        public static FontDescriptor GetFontCompanyAdress()
        {
            string s = Utils.GetFontCompanyAdress();
            if (!string.IsNullOrEmpty(s))
            {
                List<string> a = s.Split(' ', ';').Where(n => n != "").ToList();
                string b = "";
                foreach (string item in a.Where(n => a.IndexOf(n) < a.Count - 2))
                {
                    b += item + " ";
                }
                b = b.Substring(0, b.Length - 1);
                Font f = new Font(b, float.Parse(a[a.Count - 1]), (FontStyle)Enum.Parse(typeof(FontStyle), a[a.Count - 2], true));
                var font = new FontDescriptor(f);
                return font;
            }
            else
            {
                return null;
            }

        }

        public static FontDescriptor GetFontCompanyTaxCode()
        {
            string s = Utils.GetFontCompany();
            if (!string.IsNullOrEmpty(s))
            {
                List<string> a = s.Split(' ', ';').Where(n => n != "").ToList();
                string b = "";
                foreach (string item in a.Where(n => a.IndexOf(n) < a.Count - 2))
                {
                    b += item + " ";
                }
                b = b.Substring(0, b.Length - 1);
                Font f = new Font(b, float.Parse(a[a.Count - 1]), (FontStyle)Enum.Parse(typeof(FontStyle), a[a.Count - 2], true));
                var font = new FontDescriptor(f);
                return font;
            }
            else
            {
                return null;
            }

        }
        #endregion
        //public static Font GetCurrentFontCompany()
        //{
        //    //Font conv = new Font(Utils.GetCurrentFontCompany(), FontStyle.Bold);
        //    //return (Utils.GetCurrentFontCompany());
        //}
        public static string GetCompanyCashPayer() { return "Tên người nộp tiền"; }
        public static System.Drawing.Image GetCompanyLogo()
        {
            //System.Drawing.Image logo = System.Drawing.Image.FromFile(@"F:\VDC CA\Accounting\Source\04_Code\Accounting\Accounting\Resources\1 (10).png");
            System.Drawing.Image logo = global::Accounting.Properties.Resources.logox48;
            return logo;
        }
        public static string GetTextMessage(string resName)
        {
            return global::Accounting.TextMessage.resSystem.ResourceManager.GetString(resName);
        }
        public static PerpetuumSoft.Framework.Text.TextFormat GetTextFormat(PerpetuumSoft.Framework.Text.TextFormatStyle style)
        {
            //TextFormat
            PerpetuumSoft.Framework.Text.TextFormat tf = new PerpetuumSoft.Framework.Text.TextFormat();
            tf.FormatStyle = style;
            return tf;

        }
        public enum DataType
        {
            String = 0,
            Number = 1,
            Currency = 2,
            Date = 3,
            Bool = 4,
            Percentage = 5,
        }
        static PerpetuumSoft.Framework.Text.TextFormat CCYForrmat = GetTextFormat(PerpetuumSoft.Framework.Text.TextFormatStyle.Currency);
        static PerpetuumSoft.Framework.Text.TextFormat NumberFormat = GetTextFormat(PerpetuumSoft.Framework.Text.TextFormatStyle.Number);
        static PerpetuumSoft.Framework.Text.TextFormat Percentage = GetTextFormat(PerpetuumSoft.Framework.Text.TextFormatStyle.Percentage);
        public static void BindData(TextBox control, object val, DataType type)
        {
            try
            {
                switch (type)
                {
                    case DataType.String: // string
                        if (val == null || val.ToString() == "") control.Value = "";
                        else control.Value = val;
                        break;
                    case DataType.Number: // Number
                        if (val == null || val.ToString() == "") val = 0;
                        if (Convert.ToDecimal(val) == 0)
                        {
                            control.Value = "";
                        }
                        else
                        {
                            control.Value = val;
                        }
                        control.TextFormat = NumberFormat;
                        break;
                    case DataType.Currency: // money
                        if (val == null || val.ToString() == "") val = 0;
                        if (Convert.ToDecimal(val) == 0) control.Value = "";
                        else control.Value = val;
                        control.TextFormat = CCYForrmat;
                        break;
                    case DataType.Date:  // DateTime
                        if (val == null || val.ToString() == "") control.Value = "";
                        else
                        {
                            DateTime dateVal;
                            DateTime.TryParse(val.ToString(), out dateVal);
                            if (dateVal <= DateTime.MinValue) control.Value = "";
                            //else control.Value = ((DateTime)val).ToShortDateString();
                            else control.Value = ((DateTime)val).ToString("dd/MM/yyyy");
                        }
                        break;
                    case DataType.Percentage:
                        if (val == null || val.ToString() == "") control.Value = "";
                        else
                        {
                            control.Value = val;
                            control.TextFormat = Percentage;
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                MSG.Warning(ex.ToString());
            }
        }
        public static void BindData2(TextBox control, object val)
        {
            if (val == null || val.ToString() == "") val = 0;
            if (Convert.ToDecimal(val) == 0)
            {
                control.Value = "";
            }
            else
            {

                //   TextFormat textFormat = new TextFormat();


                //textFormat.FormatMask = "n,nnn,nnn.nn;(n,nnn,nnn.nn)";
                //textFormat.DecimalSeparator = ",";
                //control.TextFormat = textFormat;
                //control.TextFormat.FormatStyle;
                control.Value = val;
                if (Convert.ToDecimal(val) < 0)
                {
                    control.TextFill = new PerpetuumSoft.Framework.Drawing.SolidFill(Utils.GetColorTienAm());
                    control.Text = String.Format("{0:" + Utils.GetTypeFormatNumberic(ConstDatabase.Format_TienVND) + ";" + Utils.GetFormatNegative().Replace("n", Utils.GetTypeFormatNumberic(ConstDatabase.Format_TienVND)) + "}", Convert.ToDecimal(val));
                }
                else
                {
                    string s = "n";
                    control.TextFill = new PerpetuumSoft.Framework.Drawing.SolidFill(System.Drawing.Color.Black);
                    control.Text = String.Format("{0:" + Utils.GetTypeFormatNumberic(ConstDatabase.Format_TienVND) + ";" + s.Replace("n", Utils.GetTypeFormatNumberic(ConstDatabase.Format_TienVND)) + "}", Convert.ToDecimal(val));
                }
                //control.TextFormat = NumberFormat;
            }
        }

        // format tien viet
        public static void BindData3(TextBox control, object val)
        {
            if (val == null || val.ToString() == "") val = 0;
            if (Convert.ToDecimal(val) == 0)
            {
                control.Value = "";
            }
            else
            {
                control.Value = val;
                if (Convert.ToDecimal(val) < 0)
                {
                    control.TextFill = new PerpetuumSoft.Framework.Drawing.SolidFill(Utils.GetColorTienAm());
                    control.Text = String.Format("{0:" + Utils.GetTypeFormatNumberic(ConstDatabase.Format_TienVND) + ";" + Utils.GetFormatNegative().Replace("n", Utils.GetTypeFormatNumberic(ConstDatabase.Format_TienVND)) + "}", Convert.ToDecimal(val));

                }
                else
                {
                    string s = "n";
                    control.TextFill = new PerpetuumSoft.Framework.Drawing.SolidFill(System.Drawing.Color.Black);
                    control.Text = String.Format("{0:" + Utils.GetTypeFormatNumberic(ConstDatabase.Format_TienVND) + ";" + s.Replace("n", Utils.GetTypeFormatNumberic(ConstDatabase.Format_TienVND)) + "}", Convert.ToDecimal(val));
                }
                //control.TextFormat = CCYForrmat;
            }

        }

        // format ngoai te
        public static void BindData6(TextBox control, object val)
        {
            if (val == null || val.ToString() == "") val = 0;
            if (Convert.ToDecimal(val) == 0)
            {
                control.Value = "";
            }
            else
            {
                control.Value = val;
                if (Convert.ToDecimal(val) < 0)
                {
                    control.TextFill = new PerpetuumSoft.Framework.Drawing.SolidFill(Utils.GetColorTienAm());
                    control.Text = String.Format("{0:" + Utils.GetTypeFormatNumberic(ConstDatabase.Format_ForeignCurrency) + ";" + Utils.GetFormatNegative().Replace("n", Utils.GetTypeFormatNumberic(ConstDatabase.Format_ForeignCurrency)) + "}", Convert.ToDecimal(val));

                }
                else
                {
                    string s = "n";
                    control.TextFill = new PerpetuumSoft.Framework.Drawing.SolidFill(System.Drawing.Color.Black);
                    control.Text = String.Format("{0:" + Utils.GetTypeFormatNumberic(ConstDatabase.Format_ForeignCurrency) + ";" + s.Replace("n", Utils.GetTypeFormatNumberic(ConstDatabase.Format_ForeignCurrency)) + "}", Convert.ToDecimal(val));
                }
                //control.TextFormat = CCYForrmat;
            }

        }

        // format so luong
        public static void BindData4(TextBox control, object val)
        {
            if (val == null || val.ToString() == "") val = 0;
            if (Convert.ToDecimal(val) == 0)
            {
                control.Value = "";
            }
            else
            {
                control.Value = val;
                if (Convert.ToDecimal(val) < 0)
                {
                    control.TextFill = new PerpetuumSoft.Framework.Drawing.SolidFill(Utils.GetColorTienAm());
                    control.Text = String.Format("{0:" + Utils.GetTypeFormatNumberic(ConstDatabase.Format_Quantity) + ";" + Utils.GetFormatNegative().Replace("n", Utils.GetTypeFormatNumberic(ConstDatabase.Format_Quantity)) + "}", Convert.ToDecimal(val));

                }
                else
                {
                    string s = "n";
                    control.TextFill = new PerpetuumSoft.Framework.Drawing.SolidFill(System.Drawing.Color.Black);
                    control.Text = String.Format("{0:" + Utils.GetTypeFormatNumberic(ConstDatabase.Format_Quantity) + ";" + s.Replace("n", Utils.GetTypeFormatNumberic(ConstDatabase.Format_Quantity)) + "}", Convert.ToDecimal(val));
                }
                //control.TextFormat = NumberFormat;
            }

        }

        // format don gia tien viet
        public static void BindData5(TextBox control, object val)
        {
            if (val == null || val.ToString() == "") val = 0;
            if (Convert.ToDecimal(val) == 0)
            {
                control.Value = "";
            }
            else
            {
                control.Value = val;
                if (Convert.ToDecimal(val) < 0)
                {
                    control.TextFill = new PerpetuumSoft.Framework.Drawing.SolidFill(Utils.GetColorTienAm());
                    control.Text = String.Format("{0:" + Utils.GetTypeFormatNumberic(ConstDatabase.Format_DonGiaQuyDoi) + ";" + Utils.GetFormatNegative().Replace("n", Utils.GetTypeFormatNumberic(ConstDatabase.Format_DonGiaQuyDoi)) + "}", Convert.ToDecimal(val));

                }
                else
                {
                    string s = "n";
                    control.TextFill = new PerpetuumSoft.Framework.Drawing.SolidFill(System.Drawing.Color.Black);
                    control.Text = String.Format("{0:" + Utils.GetTypeFormatNumberic(ConstDatabase.Format_DonGiaQuyDoi) + ";" + s.Replace("n", Utils.GetTypeFormatNumberic(ConstDatabase.Format_DonGiaQuyDoi)) + "}", Convert.ToDecimal(val));
                }
                //control.TextFormat = NumberFormat;
            }

        }
        public static void ConvertDate(TextBox control, object val)
        {
            if (val == null || val.ToString() == "") control.Value = "";
            else
            {
                DateTime dateVal;
                DateTime.TryParse(val.ToString(), out dateVal);
                if (dateVal <= DateTime.MinValue) control.Value = "";
                else
                    control.Value = dateVal.ToString("dd/MM/yyyy");
            }
        }

        internal static List<ReportFilter> CreateListRFilter(string fName)
        {
            List<ReportFilter> lst = new List<ReportFilter>();
            lst.Add(new ReportFilter("AccountNumber", "Description1", false, "asc"));
            lst.Add(new ReportFilter("OrderType", "Description2", false, "asc"));
            // throw new NotImplementedException();
            return lst;
        }
        #endregion
        #region [DuyNT]
        #region các hàm service, list sử dụng
        private static IAccountService AccountService
        {
            get { return IoC.Resolve<IAccountService>(); }
        }
        private static IExpenseItemService ExpenseItemService
        {
            get { return IoC.Resolve<IExpenseItemService>(); }
        }

        private static IAccountingObjectService AccountingObjectService
        {
            get { return IoC.Resolve<IAccountingObjectService>(); }
        }
        private static IAccountingObjectCategoryService AccountingObjectCategoryService
        {
            get { return IoC.Resolve<IAccountingObjectCategoryService>(); }
        }
        private static IMaterialGoodsService MaterialGoodsService
        {
            get { return IoC.Resolve<IMaterialGoodsService>(); }
        }
        private static IStatisticsCodeService StatisticsCodeService
        {
            get { return IoC.Resolve<IStatisticsCodeService>(); }
        }
        private static ICostSetService CostSetService
        {
            get { return IoC.Resolve<ICostSetService>(); }
        }
        private static IGVoucherListService GVoucherListService
        {
            get { return IoC.Resolve<IGVoucherListService>(); }
        }
        private static IRepositoryService RepositoryService
        {
            get { return IoC.Resolve<IRepositoryService>(); }
        }
        private static IMaterialGoodsCategoryService MaterialGoodsCategoryService
        {
            get { return IoC.Resolve<IMaterialGoodsCategoryService>(); }
        }
        private static IFixedAssetService FixedAssetService
        {
            get { return IoC.Resolve<IFixedAssetService>(); }
        }
        private static ITIInitService ITIInitService
        {
            get { return IoC.Resolve<ITIInitService>(); }
        }
        private static IBankAccountDetailService BankAccountDetailService
        {
            get { return IoC.Resolve<IBankAccountDetailService>(); }
        }
        private static IDepartmentService DepartmentService
        {
            get { return IoC.Resolve<IDepartmentService>(); }
        }

        private static IEMContractService EMContractService
        {
            get { return IoC.Resolve<IEMContractService>(); }
        }
        /// <summary>
        /// Lấy ra danh sách các tài khoản AccountReport
        /// </summary>
        /// <param name="accountNumber">
        /// DetailType 0=Nhà cung cấp,
        /// DetailType 1=Khách hàng,
        /// DetailType 2=Nhân viên,
        /// DetailType 3=Đối tượng tập hợp chi phí,
        /// DetailType 4=Hợp đồng,
        /// DetailType 5=Vật tư hàng hóa công cụ dụng cụ,
        /// DetailType 6=Tài khoản ngân hàng,
        /// DetailType 7=Khoản mục chi phí,
        /// DetailType 8=Chi tiết theo ngoại tệ,
        /// DetailType 9=Theo dõi phát sinh theo phòng ban,
        /// DetailType 10=Theo dõi phát sinh theo mục thu/chi
        /// tài khoản sổ quỹ - accountNumber bắt đầu bằng 111
        /// tài khoản ngân hàng - accountNumber bắt đầu bằng 112</param>
        /// <param name="select">
        /// select = 0 : lấy theo AccountNumber bắt đầu với, hoặc chuỗi AccountNumber mỗi AccountNumber được ngăn cách với nhau bằng dấu ;
        /// select = 1 : lấy theo DetailType truyền vào một số</param>
        /// <returns></returns>
        private static List<AccountReport> AccountReports(string accountNumber, int select)
        {
            if (select == 0)
            {
                return
                    AccountService.GetListAccountAccountNumberIsActive(accountNumber, true)
                        .Select(c => new AccountReport()
                        {
                            AccountNumber = c.AccountNumber,
                            AccountGroupID = c.AccountGroupID,
                            AccountGroupKindView = c.AccountGroupKindView,
                            AccountGroupKind = c.AccountGroupKind,
                            AccountName = c.AccountName,
                            AccountNameGlobal = c.AccountNameGlobal,
                            IsActive = c.IsActive,
                            Description = c.Description,
                            DetailType = c.DetailType,
                            ID = c.ID,
                            ParentID = c.ParentID,
                            IsParentNode = c.IsParentNode,
                            Grade = c.Grade
                        }).OrderBy(c => c.AccountNumber).ToList();
            }
            if (select == 1)
            {
                return
                    AccountService.GetByDetailType(int.Parse(accountNumber))
                        .Select(c => new AccountReport()
                        {
                            AccountNumber = c.AccountNumber,
                            AccountGroupID = c.AccountGroupID,
                            AccountGroupKindView = c.AccountGroupKindView,
                            AccountGroupKind = c.AccountGroupKind,
                            AccountName = c.AccountName,
                            AccountNameGlobal = c.AccountNameGlobal,
                            IsActive = c.IsActive,
                            Description = c.Description,
                            DetailType = c.DetailType,
                            ID = c.ID,
                            ParentID = c.ParentID,
                            IsParentNode = c.IsParentNode,
                            Grade = c.Grade
                        }).OrderBy(c => c.AccountNumber).ToList();
            }
            return new List<AccountReport>();
        }

        #region ds tài khoản theo nhà cung cấp - phải trả(BC)
        private static List<AccountReport> _lstAccountReceivablesReport = new List<AccountReport>();
        public static List<AccountReport> LstAccountReceivablesReport
        {
            get
            {
                if (_lstAccountReceivablesReport.Count == 0)
                    _lstAccountReceivablesReport = AccountReports("0", 1);
                return _lstAccountReceivablesReport;
            }
            set { _lstAccountReceivablesReport = value; }
        }
        #endregion

        #region ds tài khoản công nợ phải trả
        private static List<AccountReport> _lstAccountReceivablesReportCongNo = new List<AccountReport>();
        public static List<AccountReport> LstAccountReceivablesReportCongNo
        {
            get
            {
                if (_lstAccountReceivablesReportCongNo.Count == 0)
                    _lstAccountReceivablesReportCongNo = AccountReports("33", 0);
                return _lstAccountReceivablesReportCongNo;
            }
            set { _lstAccountReceivablesReportCongNo = value; }
        }
        #endregion

        #region ds tài khoản theo khách hàng - phải thu(BC)
        private static List<AccountReport> _lstAccountPayReport = new List<AccountReport>();
        public static List<AccountReport> LstAccountPayReport
        {
            get
            {
                //if (_lstAccountPayReport.Count == 0)
                _lstAccountPayReport = AccountReports("1", 1); //ds tài khoản trả
                return _lstAccountPayReport;
            }
            set { _lstAccountPayReport = value; }
        }
        #endregion
        #region ds tài khoản theo nhan vien - phải thu(BC)
        private static List<AccountReport> _lstAccountEmployeesReport = new List<AccountReport>();
        public static List<AccountReport> LstAccountEmployeesReport
        {
            get
            {
                //if (_lstAccountEmployeesReport.Count == 0)
                _lstAccountEmployeesReport = AccountReports("2", 1); //ds tài khoản trả
                return _lstAccountEmployeesReport;
            }
            set { _lstAccountEmployeesReport = value; }
        }
        #endregion

        #region ds tài khoản sổ quỹ tiền mặt đầu 111(BC)
        private static List<AccountReport> _lstAccountCashrReport = new List<AccountReport>();
        public static List<AccountReport> LstAccountCashrReport
        {
            get
            {
                if (_lstAccountCashrReport.Count == 0)
                    _lstAccountCashrReport = AccountReports("111", 0);
                return _lstAccountCashrReport;
            }
            set { _lstAccountCashrReport = value; }

        }
        #endregion

        #region ds TK tài khoản ngân hàng(BC)
        private static List<AccountReport> _lstAccountBankReport = new List<AccountReport>();
        public static List<AccountReport> LstAccountBankReport
        {
            get
            {
                if (_lstAccountBankReport.Count == 0)
                    _lstAccountBankReport = AccountReports("112", 0);
                return _lstAccountBankReport;
            }
            set { _lstAccountBankReport = value; }
        }
        #endregion

        #region ds tài khoản tiền vay đầu 341
        private static List<AccountReport> _lstAccountTienVay = new List<AccountReport>();
        public static List<AccountReport> LstAccountTienVay
        {
            get
            {
                if (_lstAccountTienVay.Count == 0)
                    _lstAccountTienVay = AccountReports("341", 0);
                return _lstAccountTienVay;
            }
            set { _lstAccountTienVay = value; }

        }
        #endregion

        #region ds Hop Dong Ban
        private static List<EMContractReport> _lstEMContractReportSale = new List<EMContractReport>();
        public static List<EMContractReport> lstEMContractReportSale
        {
            get
            {
                if (_lstEMContractReportSale.Count == 0)
                    _lstEMContractReportSale = (EMContractService.GetAllContractSale().Select(m => new EMContractReport()
                    {
                        ID = m.ID,
                        ContractCode = m.Code,
                        ContractName = m.Name
                    })).ToList();
                return _lstEMContractReportSale;
            }
            set { _lstEMContractReportSale = value; }
        }
        #endregion

        #region ds Hop Dong Mua
        private static List<EMContractReport> _lstEMContractReportBuy = new List<EMContractReport>();
        public static List<EMContractReport> lstEMContractReportBuy
        {
            get
            {
                if (_lstEMContractReportBuy.Count == 0)
                    _lstEMContractReportBuy = (EMContractService.GetAllContractBuy().Select(m => new EMContractReport()
                    {
                        ID = m.ID,
                        ContractCode = m.Code,
                        ContractName = m.Name
                    })).ToList();
                return _lstEMContractReportBuy;
            }
            set { _lstEMContractReportBuy = value; }
        }
        #endregion

        #region ds vật tư hàng hóa, công cụ dụng cụ  đang hoạt động(BC)
        private static List<MaterialGoodsReport> _lstMaterialGoodsReport = new List<MaterialGoodsReport>();
        public static List<MaterialGoodsReport> LstMaterialGoodsReport
        {
            get
            {
                //if (_lstMaterialGoodsReport.Count == 0)
                _lstMaterialGoodsReport = (MaterialGoodsService.GetByMateriaGoodCode(true).Select(c => new MaterialGoodsReport()
                {
                    ID = c.ID,
                    MaterialGoodsCode = c.MaterialGoodsCode,
                    MaterialGoodsName = c.MaterialGoodsName,
                    MaterialGoodsGSTID = c.MaterialGoodsGSTID ?? Guid.Empty,
                    MaterialGoodsCategoryID = c.MaterialGoodsCategoryID ?? Guid.Empty,
                })).ToList();
                return _lstMaterialGoodsReport;
            }
            set { _lstMaterialGoodsReport = value; }
        }// vật tư hàng hóa, công cụ dụng cụ
        #endregion
        #region danh sách kho
        private static List<RepositoryReport> _lstRepositoryReport = new List<RepositoryReport>();
        public static List<RepositoryReport> LstRepositoryReport
        {
            get
            {
                //if (_lstRepositoryReport.Count == 0)
                _lstRepositoryReport = (RepositoryService.GetByRepositoryCode(true).Select(c => new RepositoryReport()
                {
                    ID = c.ID,
                    RepositoryCode = c.RepositoryCode,
                    RepositoryName = c.RepositoryName,

                })).ToList();
                return _lstRepositoryReport;
            }
            set { _lstRepositoryReport = value; }
        }
        #endregion
        private static List<AccountTongHopCP> _lstAccount = new List<AccountTongHopCP>();
        public static List<AccountTongHopCP> LstAccount
        {
            get
            {
                if (_lstAccount.Count == 0)
                    _lstAccount = (AccountService.GetListAccountIsActive(true).Select(c => new AccountTongHopCP()
                    {
                        ID = c.ID,
                        AccountName = c.AccountName,
                        AccountNumber = c.AccountNumber,
                    })).ToList();
                return _lstAccount;
            }
            set { _lstAccount = value; }
        }
        #region Tổng hợp CP
        private static List<AccountTongHopCP> _lstAccountCP = new List<AccountTongHopCP>();
        public static List<AccountTongHopCP> LstAccountCP
        {
            get
            {
                if (_lstAccountCP.Count == 0)
                    _lstAccountCP = (AccountService.GetListAccountAccountNumberIsActive("154;632;635;642;811", true).Select(c => new AccountTongHopCP()
                    {
                        ID = c.ID,
                        AccountName = c.AccountName,
                        AccountNumber = c.AccountNumber,

                    })).ToList();
                return _lstAccountCP;
            }
            set { _lstAccountCP = value; }
        }
        private static List<ExpenseItemTongHopCP> _lstExpenseCP = new List<ExpenseItemTongHopCP>();
        public static List<ExpenseItemTongHopCP> LstExpenseCP
        {
            get
            {
                if (_lstExpenseCP.Count == 0)
                    _lstExpenseCP = (ExpenseItemService.GetAll_OrderBy().Select(c => new ExpenseItemTongHopCP()
                    {
                        ID = c.ID,
                        ExpenseItemCode = c.ExpenseItemCode,
                        ExpenseItemName = c.ExpenseItemName,
                        ExpenseType = c.ExpenseType,
                        ExpenseItemTypeView = c.ExpenseItemTypeView,
                        Grade = c.Grade,
                        IsActive = c.IsActive,
                        IsSecurity = c.IsSecurity,
                        Description = c.Description,
                        ParentID = c.ParentID,
                        OrderFixCode = c.OrderFixCode,
                        IsParentNode = c.IsParentNode
                    })).ToList();
                return _lstExpenseCP;
            }
            set { _lstExpenseCP = value; }
        }
        #endregion
        #region Sổ theo dõi tổng hợp theo mã thống kê

        private static List<StatisticsCodeCT> _lstStatisticsCode = new List<StatisticsCodeCT>();
        public static List<StatisticsCodeCT> LstStatisticsCode
        {
            get
            {
                //if (_lstStatisticsCode.Count == 0)
                _lstStatisticsCode = (StatisticsCodeService.OrderByCode().Select(c => new StatisticsCodeCT()
                {
                    ID = c.ID,
                    StatisticsCode1 = c.StatisticsCode_,
                    StatisticsCodeName1 = c.StatisticsCodeName,
                    Grade = c.Grade,
                    IsActive = c.IsActive,
                    IsSecurity = c.IsSecurity,
                    Description = c.Description,
                    ParentID = c.ParentID,
                    OrderFixCode = c.OrderFixCode,
                    IsParentNode = c.IsParentNode
                })).ToList();
                return _lstStatisticsCode;
            }
            set { _lstStatisticsCode = value; }
        }
        #endregion

        #region Sổ theo dõi chi tiết đối tượng tập hợp chi phí
        // Sửa
        private static List<CostSetTongHopCP> _lstCostSetTongHopCP = new List<CostSetTongHopCP>();
        public static List<CostSetTongHopCP> LstCostSetTongHopCP
        {
            get
            {
                //if (_lstCostSetTongHopCP.Count == 0)
                _lstCostSetTongHopCP = (CostSetService.GetAll_OrderBy().Select(c => new CostSetTongHopCP()
                {

                    iD = c.ID,
                    costSetCode = c.CostSetCode,
                    costSetName = c.CostSetName,
                    TypeId = c.CostSetType,
                })).ToList();
                return _lstCostSetTongHopCP;
            }
            set { _lstCostSetTongHopCP = value; }
        }
        #endregion

        //#region Sổ chi tiết doanh thu theo nhân viên
        /// Sửa
        //private static List<AccountingObjectEmployees> _lstAccountingObjectEmployees = new List<AccountingObjectEmployees>();
        //public static List<AccountingObjectEmployees> LstAccountingObjectEmployees
        //{
        //    get
        //    {
        //        if (_lstAccountingObjectEmployees.Count == 0)
        //            _lstAccountingObjectEmployees = (AccountingObjectService.GetAll().Select(c => new AccountingObjectEmployees()
        //            {
        //                AccountingObjectCode = 

        //                costSetCode = c.CostSetCode,
        //                costSetName = c.CostSetName,
        //            })).ToList();
        //        return _lstAccountingObjectEmployees;
        //    }
        //    set { _lstAccountingObjectEmployees = value; }
        //}
        //#endregion


        #region Sổ đăng ký chứng từ ghi sổ - S02B - DNN
        // Sửa
        private static List<GVoucherList> _lstGvoucherList = new List<GVoucherList>();
        public static List<GVoucherList> LstGvoucherList
        {
            get
            {
                if (_lstGvoucherList.Count == 0)
                    _lstGvoucherList = (GVoucherListService.GetAll_OrderBy().Select(c => new GVoucherList()
                    {
                        ID = c.ID,
                        No = c.No,
                        Date = c.Date,
                        TotalAmount = c.TotalAmount,

                    })).ToList();
                return _lstGvoucherList;
            }
            set { _lstGvoucherList = value; }
        }
        #endregion

        #region ds nhóm vật tư hàng hóa 
        private static List<MaterialGoodsCategoryReport> _lstMaterialGoodsCategoryReport = new List<MaterialGoodsCategoryReport>();
        public static List<MaterialGoodsCategoryReport> LstMaterialGoodsCategoryReport
        {
            get
            {
                if (_lstMaterialGoodsCategoryReport.Count == 0)
                    _lstMaterialGoodsCategoryReport = (MaterialGoodsCategoryService.GetAll_OrderBy().Select(c => new MaterialGoodsCategoryReport()
                    {
                        ID = c.ID,
                        MaterialGoodsCategoryCode = c.MaterialGoodsCategoryCode,
                        MaterialGoodsCategoryName = c.MaterialGoodsCategoryName
                    })).ToList();
                return _lstMaterialGoodsCategoryReport;
            }
            set { _lstMaterialGoodsCategoryReport = value; }
        }
        #endregion

        #region ds tài khoản ngân hàng(BC)
        private static List<BankAccountDetailReport> _lstBankAccountDetailReport = new List<BankAccountDetailReport>();
        public static List<BankAccountDetailReport> LstBankAccountDetailReport
        {
            get
            {
                if (_lstBankAccountDetailReport.Count == 0)
                    _lstBankAccountDetailReport = (BankAccountDetailService.GetAll().Select(c => new BankAccountDetailReport()
                    {
                        ID = c.ID,
                        BankID = c.BankID,
                        BankAccount = c.BankAccount,
                        BankName = c.BankName,
                        Description = c.Description,
                    })).ToList();
                return _lstBankAccountDetailReport;
            }
            set { _lstBankAccountDetailReport = value; }
        }// vật tư hàng hóa, công cụ dụng cụ
        #endregion

        #region ds tài sản cố định(BC)
        private static List<FixedAssetReport> _lstFixedAssetReport = new List<FixedAssetReport>();
        public static List<FixedAssetReport> LstFixedAssetReport
        {
            get
            {
                if (_lstFixedAssetReport.Count == 0)
                    _lstFixedAssetReport = (FixedAssetService.GetAll().Select(c => new FixedAssetReport()
                    {
                        ID = c.ID,
                        FixedAssetCode = c.FixedAssetCode,
                        FixedAssetName = c.FixedAssetName,
                        FixedAssetCategoryID = c.FixedAssetCategoryID
                    })).ToList();
                return _lstFixedAssetReport;
            }
            set { _lstFixedAssetReport = value; }
        }// tài sản cố định
        #endregion
        #region ds ccdc(BC)
        //private static List<ToolsReport> _lstToolsReport = new List<ToolsReport>();
        //public static List<ToolsReport> LstToolsReport
        //{
        //    get
        //    {
        //        if (_lstToolsReport.Count == 0)
        //            _lstToolsReport = (ITIInitService.GetAll().Select(c => new ToolsReport()
        //            {
        //                ID = c.ID,
        //                ToolsCode = c.ToolsCode,
        //                ToolsName = c.ToolsName
        //            })).ToList();
        //        return _lstToolsReport;
        //    }
        //    set { _lstToolsReport = value; }
        //}// ccdc
        #endregion

        #region ds Phòng ban(BC)
        private static List<DepartmentReport> _lstDepartmentReport = new List<DepartmentReport>();
        public static List<DepartmentReport> LstDepartmentReport
        {
            get
            {
                _lstDepartmentReport = (Utils.ListDepartment.Select(c => new DepartmentReport()
                {
                    ID = c.ID,
                    DepartmentCode = c.DepartmentCode,
                    DepartmentName = c.DepartmentName,
                })).ToList();
                return _lstDepartmentReport;
            }
            set { _lstDepartmentReport = value; }
        }
        #endregion

        /// <summary>
        /// Lấy danh sách các đối tượng nhân viên, khách hàng, nhà cung cấp
        /// </summary>
        /// <param name="select">0: khách hàng, 1: nhà cung cấp, 2: nhân viên</param>
        /// <returns>tập các AccountingObject, null nếu bị lỗi</returns>
        private static List<AccountingObjectReport> AccountingObjectReport(int select)
        {
            Utils.ClearCacheByType<AccountingObject>();
            Utils.ClearCacheByType<AccountingObjectReport>();
            //Utils.ClearCacheByType<AccountingObject>();
            return AccountingObjectService.GetAccountingObjects(select, true).Select(c => new AccountingObjectReport()
            {
                Check = false,
                ID = c.ID,
                AccountingObjectCode = c.AccountingObjectCode,
                AccountingObjectName = c.AccountingObjectName,
                Address = c.Address,
                ObjectType = c.ObjectType ?? 0,
                AccountingObjectCategory = c.AccountingObjectCategory,
                AccountObjectGroupID = c.AccountObjectGroupID ?? Guid.Empty,
                DepartmentName = c.DepartmentName
            }).ToList();
        }
        private static List<AccountingObjectReport> AccountingObjectReportNCC()
        {
            return AccountingObjectService.GetAll().Where(x => (x.ObjectType == 1 || x.ObjectType == 2) && x.IsActive).ToList().Select(c => new AccountingObjectReport()
            {
                Check = false,
                ID = c.ID,
                AccountingObjectCode = c.AccountingObjectCode,
                AccountingObjectName = c.AccountingObjectName,
                Address = c.Address,
                ObjectType = c.ObjectType ?? 0,
                AccountingObjectCategory = c.AccountingObjectCategory,
                AccountObjectGroupID = c.AccountObjectGroupID ?? Guid.Empty

            }).ToList();
        }
        private static List<AccountingObjectReport> AccountingObjectReportKH()
        {
            return AccountingObjectService.GetAll().Where(x => (x.ObjectType == 0 || x.ObjectType == 2) && x.IsActive).ToList().Select(c => new AccountingObjectReport()
            {
                Check = false,
                ID = c.ID,
                AccountingObjectCode = c.AccountingObjectCode,
                AccountingObjectName = c.AccountingObjectName,
                Address = c.Address,
                ObjectType = c.ObjectType ?? 0,
                AccountingObjectCategory = c.AccountingObjectCategory,
                AccountObjectGroupID = c.AccountObjectGroupID ?? Guid.Empty

            }).ToList();
        }
        private static List<AccountingObjectReport> AccountingObjectReportKH_NCC()
        {
            return AccountingObjectService.GetAll().Where(x => (x.ObjectType == 0 || x.ObjectType == 1 || x.ObjectType == 2) && x.IsActive).ToList().Select(c => new AccountingObjectReport()
            {
                Check = false,
                ID = c.ID,
                AccountingObjectCode = c.AccountingObjectCode,
                AccountingObjectName = c.AccountingObjectName,
                Address = c.Address,
                ObjectType = c.ObjectType ?? 0,
                AccountingObjectCategory = c.AccountingObjectCategory,
                AccountObjectGroupID = c.AccountObjectGroupID ?? Guid.Empty

            }).ToList();
        }
        private static List<AccountingObjectReport> AccountingObjectReportKH_NCC_NV_Khac()
        {
            return AccountingObjectService.GetAll().Where(x => x.IsActive).ToList().Select(c => new AccountingObjectReport()
            {
                Check = false,
                ID = c.ID,
                AccountingObjectCode = c.AccountingObjectCode,
                AccountingObjectName = c.AccountingObjectName,
                Address = c.Address,
                ObjectType = c.ObjectType ?? 0,
                AccountingObjectCategory = c.AccountingObjectCategory,
                AccountObjectGroupID = c.AccountObjectGroupID ?? Guid.Empty

            }).ToList();
        }
        private static List<AccountingObjectReport> _lstCustomers = new List<AccountingObjectReport>();//khách hàng
        private static List<AccountingObjectReport> _lstProvider = new List<AccountingObjectReport>();//nhà cung cấp
        private static List<AccountingObjectReport> _lstProviderkh = new List<AccountingObjectReport>();//nhà cung cấp,kh-ncc
        private static List<AccountingObjectReport> _lstProviderncc = new List<AccountingObjectReport>();//nhà cung cấp,kh-ncc
        private static List<AccountingObjectReport> _lstEmployee = new List<AccountingObjectReport>();//nhân viên
        public static List<AccountingObjectReport> LstCustomers
        {
            get
            {
                //if (_lstCustomers.Count == 0)
                _lstCustomers = AccountingObjectReport(0);//khách hàng
                return _lstCustomers;
            }
            set { _lstCustomers = value; }
        }//khách hàng
        public static List<AccountingObjectReport> LstProvider
        {
            get
            {
                //if (_lstProvider.Count == 0)
                _lstProvider = AccountingObjectReport(1);//nhà cung cấp
                return _lstProvider;
            }
            set { _lstProvider = value; }
        }//nhà cung cấp
        public static List<AccountingObjectReport> LstProviderKH
        {
            get
            {
                if (_lstProviderkh.Count == 0)
                    _lstProviderkh = AccountingObjectReportKH();//nhà cung cấp
                return _lstProviderkh;
            }
            set { _lstProviderkh = value; }
        }//kh, kh-ncc
        public static List<AccountingObjectReport> LstProviderNCC
        {
            get
            {
                if (_lstProviderncc.Count == 0)
                    _lstProviderncc = AccountingObjectReportNCC();//nhà cung cấp
                return _lstProviderncc;
            }
            set { _lstProviderncc = value; }
        }//ncc, kh-ncc
        public static List<AccountingObjectReport> AccountingOJReportKH_NCC
        {
            get
            {
                if (_lstProviderncc.Count == 0)
                    _lstProviderncc = AccountingObjectReportKH_NCC();//nhà cung cấp
                return _lstProviderncc;
            }
            set { _lstProviderncc = value; }
        }
        public static List<AccountingObjectReport> AccountingOJReportKH_NCC_NV_Khac
        {
            get
            {
                if (_lstProviderncc.Count == 0)
                    _lstProviderncc = AccountingObjectReportKH_NCC_NV_Khac();//lấy tất cả accoutingobject khách hàng, nhà cung cấp, khác , nhân viên
                return _lstProviderncc;
            }
            set { _lstProviderncc = value; }
        }
        public static List<AccountingObjectReport> LstEmployee
        {
            get
            {
                //if (_lstEmployee.Count == 0)
                _lstEmployee = AccountingObjectReport(2);//nhân viên
                return _lstEmployee;
            }
            set { _lstEmployee = value; }
        }//nhân viên

        #region ds nhóm khách hàng
        private static List<AccountingObjectCategoryReport> _lstAccountingObjectCategoryReport = new List<AccountingObjectCategoryReport>();
        public static List<AccountingObjectCategoryReport> LstAccountingObjectCategoryReport
        {
            get
            {
                if (_lstAccountingObjectCategoryReport.Count == 0)
                    _lstAccountingObjectCategoryReport = (AccountingObjectCategoryService.GetAll_OrderBy().Select(c => new AccountingObjectCategoryReport()
                    {
                        ID = c.ID,
                        AccountingObjectCategoryCode = c.AccountingObjectCategoryCode,
                        AccountingObjectCategoryName = c.AccountingObjectCategoryName
                    })).ToList();
                return _lstAccountingObjectCategoryReport;
            }
            set { _lstAccountingObjectCategoryReport = value; }
        }
        #endregion 

        public static List<BankAccountDetail> LstBankAccountDetailDb = new List<BankAccountDetail>();
        #endregion

        static UltraDateTimeEditor _dtFromDate = new UltraDateTimeEditor();
        static UltraDateTimeEditor _dtToDate = new UltraDateTimeEditor();

        public static void ProcessControls(Control ctrlContainer)
        {
            foreach (Control ctrl in ctrlContainer.Controls)
            {
                if (ctrl.GetType().Name.Contains(typeof(UltraDateTimeEditor).Name))
                {
                    if (ctrl.Name == "dtBeginDate")
                    {
                        _dtFromDate = (UltraDateTimeEditor)ctrl;
                        _dtFromDate.DateTime = DateTime.Now;
                        _dtFromDate.Enter += Utils.UltraDateTimeEditor_Enter;
                    }
                    if (ctrl.Name == "dtEndDate")
                    {
                        _dtToDate = (UltraDateTimeEditor)ctrl;
                        _dtToDate.DateTime = DateTime.Now;
                        _dtToDate.Enter += Utils.UltraDateTimeEditor_Enter;
                    }
                }
                else if (ctrl.GetType().Name.Contains(typeof(UltraCombo).Name))
                {
                    switch (ctrl.Name)
                    {
                        case "cbbDateTime":
                            ConfigComboDate((UltraCombo)ctrl);

                            break;
                        case "cbbCurrency":
                            ConfigComboCurrency((UltraCombo)ctrl);
                            break;
                        case "cbbAccountingObjectGroupID":
                            ConfigComboAccountingObjectGroup((UltraCombo)ctrl);
                            break;
                        case "cbbAccountingObjectCategoryID":
                            ConfigComboAccountingObjectCategory((UltraCombo)ctrl);
                            break;
                        case "cbbBankAccountDetail":
                            ConfigComboBankAccountDetail((UltraCombo)ctrl);
                            break;
                        case "cbbMaterialGoodsCategory":
                            ConfigComboMaterialGoodsCategory((UltraCombo)ctrl);
                            break;
                        case "cbbMaterialGoodsCategory1":
                            ConfigComboMaterialGoodsCategory1((UltraCombo)ctrl, false);
                            break;
                        case "cbbRepository":
                            ConfigComboRepository((UltraCombo)ctrl);
                            break;
                    }
                }
                else if (ctrl.GetType().Name.Contains(typeof(UltraGrid).Name))
                {
                    switch (ctrl.Name)
                    {
                        case "ugridAccount":
                            ConfigGridAccountReport((UltraGrid)ctrl);
                            break;
                        case "ugridAccountingObject":
                            ConfigGridAccountingObjectReport((UltraGrid)ctrl);
                            break;
                        case "ugridAccountingObject2":
                            ConfigGridAccountingObjectReport2((UltraGrid)ctrl);
                            break;
                        case "ugridMaterialGoodsCategory":
                            ConfigGridMaterialGoodsReport((UltraGrid)ctrl);
                            break;
                        case "ugridFixedAsset":
                            ConfigGridFixedAsset((UltraGrid)ctrl);
                            break;
                        case "ugridDepartment":
                            ConfigGridDepartment((UltraGrid)ctrl);
                            break;
                        case "ugridBank":
                            ConfigGridBank((UltraGrid)ctrl);
                            break;
                        case "ugridRepository":
                            ConfigGridRepositoryReport((UltraGrid)ctrl);
                            break;
                        case "ugridAccountCP":
                            ConfigGridAccountCP((UltraGrid)ctrl);
                            break;
                        case "ugridTools":
                            ConfigGridTools((UltraGrid)ctrl);
                            break;
                        case "ugridEmployee":
                            ConfigGridEmployee((UltraGrid)ctrl);
                            break;
                        case "ugridCostSetCP":
                            ConfigGridCostSetCP((UltraGrid)ctrl);
                            break;
                    }
                }
                if (ctrl.HasChildren && !ctrl.GetType().Name.Contains(typeof(UltraGrid).Name) && !ctrl.GetType().Name.Contains(typeof(UltraCombo).Name)) ProcessControls(ctrl);
            }
        }


        public static void cbbDateTime_ValueChanged(object sender, EventArgs e)//cuongpv fix private -> public
        {
            var cbb = (UltraCombo)sender;
            if (cbb.SelectedRow != null)
            {
                var model = cbb.SelectedRow.ListObject as Item;
                DateTime dtBegin;
                DateTime dtEnd;
                Utils.GetDateBeginDateEnd(DateTime.Now.Year, DateTime.Now, model, out dtBegin, out dtEnd);
                _dtFromDate.DateTime = dtBegin;
                _dtToDate.DateTime = dtEnd;
            }
        }
        private static void ConfigComboDate(UltraCombo cbb)
        {
            List<Item> lstItems = Utils.ObjConstValue.SelectTimes;
            // load dữ liệu cho Combobox Chọn thời gian
            cbb.DataSource = lstItems;
            cbb.DisplayMember = "Name";
            Utils.ConfigGrid(cbb, ConstDatabase.ConfigXML_TableName);
            cbb.ValueChanged += cbbDateTime_ValueChanged;
            cbb.SelectedRow = cbb.Rows[0];
        }
        private static void ConfigComboAccountingObjectCategory(UltraCombo cbb)
        {
            var accountingObjectCategoryService = IoC.Resolve<IAccountingObjectCategoryService>();
            var accountingObjectCategorys = new List<AccountingObjectCategory>();
            var accountingObjectCategory = new AccountingObjectCategory
            {
                AccountingObjectCategoryCode = "TC",
                AccountingObjectCategoryName = "Tất cả",
                ID = Guid.Empty,
                IsActive = true,
                IsSecurity = false
            };
            accountingObjectCategorys.Add(accountingObjectCategory);
            accountingObjectCategorys.AddRange(accountingObjectCategoryService.GetByIsActiveOrderByCode(true));
            cbb.DataSource = accountingObjectCategorys;
            cbb.DisplayMember = "AccountingObjectCategoryName";
            Utils.ConfigGrid(cbb, ConstDatabase.AccountingObjectCategory_TableName);
            cbb.SelectedRow = cbb.Rows[0];
        }
        private static void ConfigComboAccountingObjectGroup(UltraCombo cbb)
        {
            var accountingObjectGroupService = IoC.Resolve<IAccountingObjectGroupService>();
            var accountingObjectGroups = new List<AccountingObjectGroup>();
            //var accountingObjectGroup = new AccountingObjectGroup
            //{
            //    AccountingObjectGroupCode = "TC",
            //    AccountingObjectGroupName = "Tất cả",
            //    ID = Guid.Empty,
            //    IsActive = true,
            //    IsSecurity = false
            //};
            //accountingObjectGroups.Add(accountingObjectGroup);
            accountingObjectGroups.AddRange(accountingObjectGroupService.GetListByIsActiveOrderCode(true));
            cbb.DataSource = accountingObjectGroups;
            cbb.DisplayMember = "AccountingObjectGroupName";
            Utils.ConfigGrid(cbb, ConstDatabase.AccountingObjectGroup_TableName);
            //cbb.SelectedRow = cbb.Rows[0];
        }
        private static void ConfigComboMaterialGoodsCategory(UltraCombo cbb, bool kt = true)
        {
            var materialGoodsCategoryService = IoC.Resolve<IMaterialGoodsCategoryService>();
            var materialGoodsCategorys = new List<MaterialGoodsCategory>();
            var materialGoodsCategory = new MaterialGoodsCategory
            {
                MaterialGoodsCategoryCode = "TC",
                MaterialGoodsCategoryName = "Tất cả",
                ID = Guid.Empty,
                IsActive = true,
                IsSecurity = false
            };
            materialGoodsCategorys.Add(materialGoodsCategory);
            materialGoodsCategorys.AddRange(materialGoodsCategoryService.GetAll_ByIsActive(true));
            if (kt == false)
            {
                materialGoodsCategorys = materialGoodsCategorys.Where(c => c.MaterialGoodsCategoryCode != "DV").ToList();
            }
            cbb.DataSource = materialGoodsCategorys;
            cbb.DisplayMember = "MaterialGoodsCategoryName";
            Utils.ConfigGrid(cbb, ConstDatabase.MaterialGoodsCategory_TableName);
            try
            {
                if (materialGoodsCategorys.Count > 0) cbb.SelectedRow = cbb.Rows[0];
            }
            catch (Exception ex)
            {
                string s = ex.ToString();
            }

        }
        //add by cuongpv 20190419
        private static void ConfigComboMaterialGoodsCategory1(UltraCombo cbb, bool kt = true)
        {
            var materialGoodsCategoryService = IoC.Resolve<IMaterialGoodsCategoryService>();
            var materialGoodsCategorys = new List<MaterialGoodsCategory>();
            var materialGoodsCategory = new MaterialGoodsCategory
            {
                MaterialGoodsCategoryCode = "Tất cả",//edit by cuongpv 20190419 TC->Tất cả
                MaterialGoodsCategoryName = "Tất cả",
                ID = Guid.Empty,
                IsActive = true,
                IsSecurity = false
            };
            materialGoodsCategorys.Add(materialGoodsCategory);
            materialGoodsCategorys.AddRange(materialGoodsCategoryService.GetAll_ByIsActive(true));
            if (kt == false)
            {
                materialGoodsCategorys = materialGoodsCategorys.Where(c => c.MaterialGoodsCategoryCode != "DV").ToList();
            }
            cbb.DataSource = materialGoodsCategorys;
            cbb.DisplayMember = "MaterialGoodsCategoryName";
            Utils.ConfigGrid(cbb, ConstDatabase.MaterialGoodsCategory_TableName);
            try
            {
                if (materialGoodsCategorys.Count > 0) cbb.SelectedRow = cbb.Rows[0];
            }
            catch (Exception ex)
            {
                string s = ex.ToString();
            }

        }
        //end add by cuongpv
        private static void ConfigComboRepository(UltraCombo cbb)
        {
            var repositoryService = IoC.Resolve<IRepositoryService>();
            var repositorys = new List<Repository>();
            var repository = new Repository
            {
                RepositoryCode = "Tất cả",//edit by cuongpv 20190419 TC->Tất cả
                RepositoryName = "Tất cả",
                ID = Guid.Empty,
                IsActive = true
            };
            repositorys.Add(repository);
            repositorys.AddRange(repositoryService.GetAll_ByIsActive(true));
            cbb.DataSource = repositorys;
            cbb.DisplayMember = "RepositoryName";
            Utils.ConfigGrid(cbb, ConstDatabase.Repository_TableName);
            cbb.SelectedRow = cbb.Rows[0];
        }
        private static void ConfigComboBankAccountDetail(UltraCombo cbb)
        {
            var bankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            var bankAccountDetails = new List<BankAccountDetail>();
            var bankAccountDetail = new BankAccountDetail
            {
                BankAccount = "Tất cả",
                BankName = "Tất cả",
                ID = Guid.Empty,
                IsActive = true
            };
            bankAccountDetails.Add(bankAccountDetail);
            bankAccountDetails.AddRange(bankAccountDetailService.GetIsActive(true));
            LstBankAccountDetailDb.AddRange(bankAccountDetailService.GetIsActive(true));
            cbb.DataSource = bankAccountDetails;
            cbb.DisplayMember = "BankAccount";
            Utils.ConfigGrid(cbb, ConstDatabase.BankAccountDetail_TableName);
            cbb.SelectedRow = cbb.Rows[0];
        }
        private static void ConfigComboCurrency(UltraCombo cbb)
        {
            var currencyService = IoC.Resolve<ICurrencyService>();
            cbb.DataSource = currencyService.GetIsActive(true);
            Utils.ConfigGrid(cbb, ConstDatabase.Currency_TableName);
            foreach (var item in cbb.Rows)
            {
                if (((Currency)item.ListObject).ID == "VND")
                {
                    cbb.SelectedRow = item;
                    break;
                }
                cbb.SelectedRow = cbb.Rows[0];
            }
        }
        public static void ConfigGridColumnCheck(UltraGrid ultraGrid, string columnKey = "Check")
        {
            ultraGrid.DisplayLayout.Bands[0].Summaries.Clear();
            ultraGrid.DisplayLayout.Bands[0].Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.Default;
            ultraGrid.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.True;
            ultraGrid.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
            ultraGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            UltraGridBand band = ultraGrid.DisplayLayout.Bands[0];
            UltraGridColumn ugc = band.Columns[columnKey];
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            ultraGrid.DisplayLayout.Bands[0].Columns[columnKey].CellActivation = Activation.AllowEdit;
            band.Columns[columnKey].Header.Fixed = true;
            foreach (var c in band.Columns)
            {
                if (c.Key != columnKey) c.CellActivation = Activation.NoEdit;
            }
        }
        private static void ConfigGridAccountReport(UltraGrid ulg)
        {
            Utils.ConfigGrid(ulg, ConstDatabase.AccountReport_TableName);
            ConfigGridColumnCheck(ulg);
        }
        private static void ConfigGridAccountingObjectReport(UltraGrid ulg)
        {
            Utils.ConfigGrid(ulg, ConstDatabase.AccountingObjectReport_TableName);
            ConfigGridColumnCheck(ulg);
        }
        private static void ConfigGridAccountingObjectReport2(UltraGrid ulg)
        {
            Utils.ConfigGrid(ulg, ConstDatabase.AccountingObjectReport2_TableName);
            ConfigGridColumnCheck(ulg);
        }
        private static void ConfigGridMaterialGoodsReport(UltraGrid ulg)
        {
            Utils.ConfigGrid(ulg, ConstDatabase.MaterialGoodsReport_TableName);
            ConfigGridColumnCheck(ulg);
        }
        private static void ConfigGridRepositoryReport(UltraGrid ulg)
        {
            Utils.ConfigGrid(ulg, ConstDatabase.RepositoryReport_TableName);
            ConfigGridColumnCheck(ulg);
        }
        private static void ConfigGridAccountCP(UltraGrid ulg)
        {
            Utils.ConfigGrid(ulg, ConstDatabase.AccountTongHopCP_TableName);
            ConfigGridColumnCheck(ulg);
        }
        private static void ConfigGridCostSetCP(UltraGrid ulg)
        {
            Utils.ConfigGrid(ulg, ConstDatabase.CostSetTongHopCP_TableName);
            ConfigGridColumnCheck(ulg);
        }
        private static void ConfigGridAccountingObject(UltraGrid ulg)
        {
            Utils.ConfigGrid(ulg, ConstDatabase.CostSetTongHopCP_TableName);
            ConfigGridColumnCheck(ulg);
        }
        private static void ConfigGridFixedAsset(UltraGrid ulg)
        {
            Utils.ConfigGrid(ulg, ConstDatabase.FixedAssetReport_TableName);
            ConfigGridColumnCheck(ulg);
        }
        private static void ConfigGridTools(UltraGrid ulg)
        {
            Utils.ConfigGrid(ulg, ConstDatabase.ToolsReport_TableName);
            ConfigGridColumnCheck(ulg);
        }
        private static void ConfigGridEmployee(UltraGrid ulg)
        {
            Utils.ConfigGrid(ulg, ConstDatabase.EmployeeReport_TableName);
            ConfigGridColumnCheck(ulg);
        }
        private static void ConfigGridDepartment(UltraGrid ulg)
        {
            Utils.ConfigGrid(ulg, ConstDatabase.DepartmentReport_TableName);
            ConfigGridColumnCheck(ulg);
        }
        private static void ConfigGridBank(UltraGrid ulg)
        {
            Utils.ConfigGrid(ulg, ConstDatabase.BankAccountDetailReport_TableName);
            ConfigGridColumnCheck(ulg);
        }
        #endregion

        #region [Yên_SPKT_HY]
        public static string GetBankAccount(string bankAccountDetailId)
        {
            string s = string.Empty;
            var guid = new Guid(bankAccountDetailId);
            var bank = Utils.ListBankAccountDetail.FirstOrDefault(b => b.ID == guid);
            if (bank != null)
            {
                s = bank.BankAccount;
            }

            return s;
        }
        public static string GetBankAccountName(string bankAccountDetailId)
        {
            string s = string.Empty;
            var guid = new Guid(bankAccountDetailId);
            var bank = Utils.ListBankAccountDetail.FirstOrDefault(b => b.ID == guid);
            if (bank != null)
            {
                s = bank.BankName;
            }

            return s;
        }
        public static string GetBankAccountid(string bankAccountDetailId)
        {
            string s = string.Empty;
            var guid = new Guid(bankAccountDetailId);
            var bank = Utils.ListBankAccountDetail.FirstOrDefault(b => b.ID == guid);
            if (bank != null)
            {
                s = bank.BankIDView;
            }

            return s;
        }

        public static string GetAccountingObjectName(string AccountingObjectID)
        {
            string s = string.Empty;
            var guid = new Guid(AccountingObjectID);
            var ob = Utils.ListAccountingObject.FirstOrDefault(b => b.ID == guid);
            if (ob != null)
            {
                s = ob.AccountingObjectName;
            }

            return s;
        }
        public static string GetAccountingObjectBankAcount(string AccountingObjectID)
        {
            string s = string.Empty;
            var guid = new Guid(AccountingObjectID);
            var ob = Utils.ListAccountingObject.FirstOrDefault(b => b.ID == guid);
            if (ob != null)
            {
                s = ob.BankAccount;
            }

            return s;
        }
        public static string GetAccountingObjectBankName(string AccountingObjectID)
        {
            string s = string.Empty;
            var guid = new Guid(AccountingObjectID);
            var ob = Utils.ListAccountingObject.FirstOrDefault(b => b.ID == guid);
            if (ob != null)
            {
                s = ob.BankName;
            }

            return s;
        }
        public static string GetAccountingObjectAddress(string AccountingObjectID)
        {
            string s = string.Empty;
            var guid = new Guid(AccountingObjectID);
            var ob = Utils.ListAccountingObject.FirstOrDefault(b => b.ID == guid);
            if (ob != null)
            {
                s = ob.Address;
            }

            return s;
        }
        public static string GetPaymentClauseName(string PaymentClauseID)
        {
            string s = string.Empty;
            var guid = new Guid(PaymentClauseID);
            var ob = Utils.ListPaymentClause.FirstOrDefault(b => b.ID == guid);
            if (ob != null)
            {
                s = ob.PaymentClauseName;
            }

            return s;
        }

        public static string GetAccountingObjectTel(string AccountingObjectID)
        {
            string s = string.Empty;
            var guid = new Guid(AccountingObjectID);
            var ob = Utils.ListAccountingObject.FirstOrDefault(b => b.ID == guid);
            if (ob != null)
            {
                s = ob.Tel;
            }
            return s;
        }
        public static string GetMaterialGoodsCode(string MaterialGoodsID)
        {
            string s = string.Empty;
            var guid = new Guid(MaterialGoodsID);
            var ob = Utils.ListMaterialGoods.FirstOrDefault(b => b.ID == guid);
            if (ob != null)
            {
                s = ob.MaterialGoodsCode;
            }
            return s;
        }
        public static string GetMaterialGoodsName(string MaterialGoodsID)
        {
            string s = string.Empty;
            var guid = new Guid(MaterialGoodsID);
            var ob = Utils.ListMaterialGoods.FirstOrDefault(b => b.ID == guid);
            if (ob != null)
            {
                s = ob.MaterialGoodsName;
            }
            return s;
        }
        public static string GetFixedAssetCode(string FixedAssetID)
        {
            string s = string.Empty;
            var guid = new Guid(FixedAssetID);
            var ob = Utils.ListFixedAsset.FirstOrDefault(b => b.ID == guid);
            if (ob != null)
            {
                s = ob.FixedAssetCode;
            }
            return s;
        }

        public static string GetToolCode(string ToolsID)
        {
            string s = string.Empty;
            var guid = new Guid(ToolsID);
            var ob = Utils.ListTools.FirstOrDefault(b => b.ID == guid);
            if (ob != null)
            {
                s = ob.ToolsCode;
            }
            return s;
        }

        public static string GetToolName(string ToolsID)
        {
            string s = string.Empty;
            var guid = new Guid(ToolsID);
            var ob = Utils.ListTools.FirstOrDefault(b => b.ID == guid);
            if (ob != null)
            {
                s = ob.ToolsName;
            }
            return s;
        }
        public static string GetSerialNumber(string FixedAssetID)
        {
            string s = string.Empty;
            var guid = new Guid(FixedAssetID);
            var ob = Utils.ListFixedAsset.FirstOrDefault(b => b.ID == guid);
            if (ob != null)
            {
                s = ob.SerialNumber;
            }
            return s;
        }
        public static string GetFixedAssetName(string FixedAssetID)
        {
            string s = string.Empty;
            var guid = new Guid(FixedAssetID);
            var ob = Utils.ListFixedAsset.FirstOrDefault(b => b.ID == guid);
            if (ob != null)
            {
                s = ob.FixedAssetName;
            }
            return s;
        }
        public static string GetDepartmentName(string FixedAssetID)
        {
            if (FixedAssetID == null || FixedAssetID == "") return "";
            string s = string.Empty;
            var guid = new Guid(FixedAssetID);
            var ob = Utils.ListDepartment.FirstOrDefault(b => b.ID == guid);
            if (ob != null)
            {
                s = ob.DepartmentName;
            }
            return s;
        }
        public static string GetObjectName(string ID)
        {
            if (ID == null || ID == "") return "";
            string s = string.Empty;
            var guid = new Guid(ID);
            var ob = Utils.ListObjects.FirstOrDefault(b => b.ID == guid);
            if (ob != null)
            {
                s = ob.ObjectName;
            }
            return s;
        }

        public static string GetVNDate(DateTime dateTime)
        {
            if (dateTime != null && dateTime != DateTime.MinValue)
            {
                return dateTime.Day + "/" + dateTime.Month + "/" + dateTime.Year;
            }
            return "";
        }

        public static string GetRepositoryName(string RepositoryID)
        {
            string s = string.Empty;
            if (!string.IsNullOrEmpty(RepositoryID))
            {
                var guid = new Guid(RepositoryID);
                var ob = Utils.ListRepository.FirstOrDefault(b => b.ID == guid);
                if (ob != null)
                {
                    s = ob.RepositoryName;
                }
            }
            return s;

        }
        public static string GetTransportMethodName(string TransportMethodID)
        {
            string s = string.Empty;
            if (!string.IsNullOrEmpty(TransportMethodID))
            {
                var guid = new Guid(TransportMethodID);
                var ob = Utils.ListTransportMethod.FirstOrDefault(b => b.ID == guid);
                if (ob != null)
                {
                    s = ob.TransportMethodName;
                }
            }
            return s;

        }
        public static string GetContractCode(string ContractID)
        {
            string s = string.Empty;
            if (!string.IsNullOrEmpty(ContractID))
            {
                var guid = new Guid(ContractID);
                var ob = Utils.ListEmContract.FirstOrDefault(b => b.ID == guid);
                if (ob != null)
                {
                    s = ob.Code;
                }
            }
            return s;

        }
        public static string GetRepositoryCode(string RepositoryID)
        {
            string s = string.Empty;
            var guid = new Guid(RepositoryID);
            var ob = Utils.ListRepository.FirstOrDefault(b => b.ID == guid);
            if (ob != null)
            {
                s = ob.RepositoryCode;
            }
            return s;

        }
        public static string GetInvoiceTypeCode(string InvoiceTypeID)
        {
            string s = string.Empty;
            var guid = new Guid(InvoiceTypeID);
            var ob = Utils.ListInvoiceType.FirstOrDefault(b => b.ID == guid);
            if (ob != null)
            {
                s = ob.InvoiceTypeCode;
            }
            return s;
        }

        public static string FormatUnitPrice(object t)
        {
            string ss = Accounting.Utils.FormatNumberic(t, Accounting.TextMessage.ConstDatabase.Format_DonGiaQuyDoi);
            return ss;
        }
        public static string FormatUnitPrice(object t, string currencyId)
        {
            string ss = Accounting.Utils.FormatNumberic(t, currencyId == "VND" ? Accounting.TextMessage.ConstDatabase.Format_DonGiaQuyDoi : Accounting.TextMessage.ConstDatabase.Format_DonGiaNgoaiTe);
            return ss;
        }
        public static string FormatVatRate(object t)
        {
            string ss = Convert.ToDecimal(t).ToString("0.##");
            return ss;
        }
        public static string FormatOWUnitPrice(object t)
        {
            //var info = System.Globalization.CultureInfo.GetCultureInfo("vi-VN");
            string ss = Convert.ToDecimal(t).ToString("0.##");
            return ss;
        }
        /// <summary>
        /// Hàm chuyển định dạng tiền VND hoặc ngoại tệ
        /// </summary>
        /// <param name="t"> string</param>
        /// <param name="currencyId"> loại tiền</param>
        /// <returns> định dạng chuẩn công ty</returns>
        public static string FormatTiente(object t, string currencyId)
        {
            string ss = Accounting.Utils.FormatNumberic(t, currencyId == "VND" ? Accounting.TextMessage.ConstDatabase.Format_TienVND : Accounting.TextMessage.ConstDatabase.Format_ForeignCurrency);
            return ss;
        }

        /// <summary>
        /// Hàm chuyển định dạng tỷ lệ chiết khấu
        /// </summary>
        /// <param name="t"> string</param>
        /// <returns> định dạng chuẩn công ty</returns>
        public static string Dinhdangtyle(object t)
        {
            string ss = Accounting.Utils.FormatNumberic(t, Accounting.TextMessage.ConstDatabase.Format_Coefficient);
            return ss;
        }
        public static string FormatSoLuong(object t)
        {
            string ss = Accounting.Utils.FormatNumberic(t, Accounting.TextMessage.ConstDatabase.Format_Quantity);
            return ss;
        }

        public static string GetDaiLyThue()
        {
            return Utils.GetTenDLT();
        }

        /// <summary>
        ///  Hàm căn chỉnh dữ liệu (yên) tiêu đề.
        /// 0- Trái Trên
        /// 1- Phải Trên
        /// 2- Giữa Trên
        /// </summary>
        /// <returns></returns>
        public static void GetStyle1(TextBox txtCompanyInfoName, TextBox txtCompanyInfoAddress, TextBox txtCompanyInfoTaxCode, TextBox txtCompanyInfoPhone, TextBox txtDonVi)
        {
            int a = Accounting.Utils.GetCompanyInfoType();
            int b = Accounting.Utils.GetCompanyInfoLocation();

            txtCompanyInfoName.Size = new Vector(2195.811024, 45.82);
            txtCompanyInfoAddress.Size = new Vector(2195.811024, 45.82);
            txtCompanyInfoTaxCode.Size = new Vector(2195.811024, 45.82);
            txtCompanyInfoPhone.Size = new Vector(2195.811024, 45.82);
            txtDonVi.Size = new Vector(2195.811024, 45.82);

            txtCompanyInfoName.Font.FamilyName = Accounting.Utils.GetFontCompanyName();
            txtCompanyInfoAddress.Font.FamilyName = Accounting.Utils.GetFontCompanyAdress();
            txtDonVi.Font.FamilyName = Accounting.Utils.GetFontCompany();
            txtCompanyInfoTaxCode.Font.FamilyName = Accounting.Utils.GetFontCompanyName();
            txtCompanyInfoPhone.Font.FamilyName = Accounting.Utils.GetFontCompany();

            if (a == 0)
            {
                txtCompanyInfoPhone.Visible = false;
                txtDonVi.Visible = false;

                txtCompanyInfoName.Location = new Vector(171.811024, 0);
                txtCompanyInfoAddress.Location = new Vector(171.811024, 45);
                txtCompanyInfoTaxCode.Location = new Vector(171.811024, 90);
                if (b == 0)
                {
                    txtCompanyInfoName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
                    txtCompanyInfoAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
                    txtCompanyInfoTaxCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;

                }
                if (b == 1)
                {
                    txtCompanyInfoName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
                    txtCompanyInfoAddress.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
                    txtCompanyInfoTaxCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;

                }
                if (b == 2)
                {
                    txtCompanyInfoName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
                    txtCompanyInfoAddress.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
                    txtCompanyInfoTaxCode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;

                }

            }
            if (a == 1)
            {
                txtCompanyInfoPhone.Visible = false;
                txtCompanyInfoTaxCode.Visible = false;
                txtDonVi.Visible = false;

                txtCompanyInfoName.Location = new Vector(171.811024, 0);
                txtCompanyInfoAddress.Location = new Vector(171.811024, 45);

                if (b == 0)
                {
                    txtCompanyInfoName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
                    txtCompanyInfoAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;

                }
                if (b == 1)
                {
                    txtCompanyInfoName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
                    txtCompanyInfoAddress.TextAlign = System.Drawing.ContentAlignment.MiddleRight;

                }
                if (b == 2)
                {
                    txtCompanyInfoName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
                    txtCompanyInfoAddress.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;

                }
            }
            if (a == 2)
            {
                txtCompanyInfoPhone.Visible = false;
                txtCompanyInfoTaxCode.Visible = false;

                txtCompanyInfoName.Location = new Vector(171.811024, 0);
                txtCompanyInfoAddress.Location = new Vector(171.811024, 45);
                txtDonVi.Location = new Vector(171.811024, 90);

                if (b == 0)
                {
                    txtCompanyInfoName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
                    txtCompanyInfoAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
                    txtDonVi.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
                }
                if (b == 1)
                {
                    txtCompanyInfoName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
                    txtCompanyInfoAddress.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
                    txtDonVi.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
                }
                if (b == 2)
                {
                    txtCompanyInfoName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
                    txtCompanyInfoAddress.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
                    txtDonVi.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
                }
            }
            else
            {
                txtCompanyInfoPhone.Visible = false;

                txtCompanyInfoName.Location = new Vector(171.811024, 0);
                txtCompanyInfoAddress.Location = new Vector(171.811024, 45);
                txtCompanyInfoTaxCode.Location = new Vector(171.811024, 90);
                txtDonVi.Location = new Vector(171.811024, 135);

                txtCompanyInfoName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
                txtCompanyInfoAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
                txtCompanyInfoTaxCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
                txtDonVi.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            }
        }
        //dung cho .rst co mau qui dinh
        public static void GetStyle2(TextBox txtCompanyInfoName, TextBox txtCompanyInfoAddress, TextBox txtCompanyInfoTaxCode, TextBox txtCompanyInfoPhone, TextBox txtDonVi)
        {
            int a = Accounting.Utils.GetCompanyInfoType();
            int b = Accounting.Utils.GetCompanyInfoLocation();

            //txtCompanyInfoName.Size = new Vector(1095.811024, 45.82);
            //txtCompanyInfoAddress.Size = new Vector(1095.811024, 45.82);
            //txtCompanyInfoTaxCode.Size = new Vector(1095.811024, 45.82);
            //txtCompanyInfoPhone.Size = new Vector(1095.811024, 45.82);
            //txtDonVi.Size = new Vector(1095.811024, 45.82);
            txtCompanyInfoName.Font.FamilyName = Accounting.Utils.GetFontCompanyName();
            txtCompanyInfoAddress.Font.FamilyName = Accounting.Utils.GetFontCompanyAdress();
            txtDonVi.Font.FamilyName = Accounting.Utils.GetFontCompany();
            txtCompanyInfoTaxCode.Font.FamilyName = Accounting.Utils.GetFontCompanyName();
            txtCompanyInfoPhone.Font.FamilyName = Accounting.Utils.GetFontCompany();

            if (a == 0)
            {
                txtCompanyInfoPhone.Visible = false;
                txtDonVi.Visible = false;

                //txtCompanyInfoName.Location = new Vector(171.811024, 0);
                //txtCompanyInfoAddress.Location = new Vector(171.811024, 45);
                //txtCompanyInfoTaxCode.Location = new Vector(171.811024, 90);
                if (b == 0)
                {
                    txtCompanyInfoName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
                    txtCompanyInfoAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
                    txtCompanyInfoTaxCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;

                }
                if (b == 1)
                {
                    txtCompanyInfoName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
                    txtCompanyInfoAddress.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
                    txtCompanyInfoTaxCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;

                }
                if (b == 2)
                {
                    txtCompanyInfoName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
                    txtCompanyInfoAddress.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
                    txtCompanyInfoTaxCode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;

                }

            }
            if (a == 1)
            {
                txtCompanyInfoPhone.Visible = false;
                txtCompanyInfoTaxCode.Visible = false;
                txtDonVi.Visible = false;

                //txtCompanyInfoName.Location = new Vector(171.811024, 0);
                //txtCompanyInfoAddress.Location = new Vector(171.811024, 45);

                if (b == 0)
                {
                    txtCompanyInfoName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
                    txtCompanyInfoAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;

                }
                if (b == 1)
                {
                    txtCompanyInfoName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
                    txtCompanyInfoAddress.TextAlign = System.Drawing.ContentAlignment.MiddleRight;

                }
                if (b == 2)
                {
                    txtCompanyInfoName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
                    txtCompanyInfoAddress.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;

                }
            }
            if (a == 2)
            {
                txtCompanyInfoPhone.Visible = false;
                txtCompanyInfoTaxCode.Visible = false;

                //txtCompanyInfoName.Location = new Vector(171.811024, 0);
                //txtCompanyInfoAddress.Location = new Vector(171.811024, 45);
                //txtDonVi.Location = new Vector(171.811024, 90);

                if (b == 0)
                {
                    txtCompanyInfoName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
                    txtCompanyInfoAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
                    txtDonVi.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
                }
                if (b == 1)
                {
                    txtCompanyInfoName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
                    txtCompanyInfoAddress.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
                    txtDonVi.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
                }
                if (b == 2)
                {
                    txtCompanyInfoName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
                    txtCompanyInfoAddress.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
                    txtDonVi.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
                }
            }
            else
            {
                txtCompanyInfoPhone.Visible = false;

                //txtCompanyInfoName.Location = new Vector(171.811024, 0);
                //txtCompanyInfoAddress.Location = new Vector(171.811024, 45);
                //txtCompanyInfoTaxCode.Location = new Vector(171.811024, 90);
                //txtDonVi.Location = new Vector(171.811024, 135);

                txtCompanyInfoName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
                txtCompanyInfoAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
                txtCompanyInfoTaxCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
                txtDonVi.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            }

        }

        #endregion

        #region THOHD
        public static string FormatDateTime(object val, string currentFormat, string targetFormat)
        {
            string convert = "";

            try
            {
                DateTime dateVal;
                DateTime.TryParse(val.ToString(), out dateVal);
                if (dateVal <= DateTime.MinValue)
                    convert = val.ToString();
                else
                    convert = dateVal.ToString("dd/MM/yyyy");
            }
            catch (Exception ex)
            {
                convert = val.ToString();
            }

            return convert;
        }
        #endregion

        #region clear check box
        // [HieuGie]
        public static void ClearCheckBox(UltraGrid uGrid)
        {
            foreach (var row in uGrid.Rows)
            {
                row.Cells["check"].Value = false;
            }

        }
        #endregion

        #region Quân add
        public static void CallFormBC(string column_name, string gtri, DateTime FromDate1, DateTime ToDate1)
        {
            if (column_name == "AccountNumber")
                CallSoChiTietCacTK(FromDate1, ToDate1, gtri);
        }
        private static void CallSoChiTietCacTK(DateTime FromDate1, DateTime ToDate1, string gtri)
        {
            ReportProcedureSDS sp = new ReportProcedureSDS();
            List<Core.Domain.obj.Report.GL_GetBookDetailPaymentByAccountNumber> data = sp.GetSoChiTietCacTaiKhoan1("", "", FromDate1, ToDate1, gtri, 0);
            if (data.Count == 0)
                MSG.Warning("Không có dữ liệu");
            else
            {
                var list_accountnumber = data.Select(t => t.AccountNumber).Distinct().ToList();
                foreach (var item in list_accountnumber)
                {
                    var list1 = data.Where(t => t.AccountNumber == item).ToList();
                    list1[list1.Count() - 1].Sum_DuNo = list1[list1.Count() - 1].ClosingDebitAmount;
                    list1[list1.Count() - 1].Sum_DuCo = list1[list1.Count() - 1].ClosingCreditAmount;
                }
                var rD = new Core.Domain.obj.Report.GL_GetBookDetailPaymentByAccountNumber_Detail();
                rD.Period = ReportUtils.GetPeriod(FromDate1, ToDate1);
                //if (dsaccount.Count() > 1) comment by cuongpv
                //    rD.LoaiTien = "Loại tiền: " + loaitien;
                //else
                //    rD.LoaiTien = "Loại tiền: " + loaitien + "; Tài khoản: " + listaccount;
                var f = new Frm.FReport.FRSOCHITIETCACTAIKHOANTruocIn(data, FromDate1, ToDate1, 0, gtri);
                f.Show();
            }
        }
        #endregion
    }
}
