﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using Accounting.Core;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.Frm.TextMessage;
using Castle.MicroKernel.Registration;
using FX.Core;
using FX.Data;
using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinMaskedEdit;
using Infragistics.Win.UltraWinTabControl;
using Infragistics.Win.UltraWinToolbars;
using Infragistics.Win.UltraWinToolTip;
using Infragistics.Win.UltraWinTree;
using Itenso.TimePeriod;
//using UltraTabControlNoBorder;
using AutoCompleteMode = Infragistics.Win.AutoCompleteMode;
using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;
using ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds;
using Type = Accounting.Core.Domain.Type;
using ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle;
using System.Linq.Expressions;
using System.Threading;
using ThreadState = System.Threading.ThreadState;

namespace Accounting.Frm
{
    public static class Utils
    {
        ////////////////////////////////////////////////////////////////////////////////
        // UltraGridRow.Tag:  Dictionary<string, object>:   - IsAdjustmentFixedAsset  //
        ////////////////////////////////////////////////////////////////////////////////

        //////////////////////////////////////////////////////////////////
        // UltraGridCell.Tag:  Dictionary<string, object>:  - KeyPress  //
        //                                                  - Error     //
        //                                                  - Dropdown  //
        //////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////
        // UltraCombo.Tag:   Dictionary<string, object>: - AccountingObjectType  //
        //                                               - KeyPress              //
        //                                               - CreditCardControl     //
        //                                               - FixedAssetType (Loại ds Mã TS: 0-Nằm trong FAIncrement. 1-Không nằm trong FAIncrement. null-Tất cả.)      //
        //                                               - FixedAssetIdJoinFAIncrement (DS FixedAssetId của các FixedAsset có trong FAIncrement.)//
        ///////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////
        // UltraCombo.Row.Tag:   bool = true  //
        ////////////////////////////////////////

        #region khai báo
        static Thread threadConfig;
        public static bool DisableAllEvent = false;
        private static readonly string PathConfigXml = Application.StartupPath + "\\ConfigSystem\\ConfigXML.xml";
        static ConstValue _objConstValue; public static ConstValue ObjConstValue { get { _objConstValue = _objConstValue ?? (ConstValue)BuildConfig(0); return _objConstValue; } }
        static Dictionary<string, Item> _dicAccountKind; public static Dictionary<string, Item> DicAccountKind { get { _dicAccountKind = _dicAccountKind ?? (Dictionary<string, Item>)BuildConfig(1); return _dicAccountKind; } }
        static Dictionary<string, Item> _dicDetailType; public static Dictionary<string, Item> DicDetailType { get { _dicDetailType = _dicDetailType ?? (Dictionary<string, Item>)BuildConfig(2); return _dicDetailType; } }
        static Dictionary<string, Item> _dicSelectTimes; public static Dictionary<string, Item> DicSelectTimes { get { _dicSelectTimes = _dicSelectTimes ?? (Dictionary<string, Item>)BuildConfig(3); return _dicSelectTimes; } }
        static Dictionary<string, Item> _dicUtilities; public static Dictionary<string, Item> DicUtilities { get { _dicUtilities = _dicUtilities ?? (Dictionary<string, Item>)BuildConfig(4); return _dicUtilities; } }
        static Dictionary<string, Item> _dicUtilitiesCaption; public static Dictionary<string, Item> DicUtilitiesCaption { get { _dicUtilitiesCaption = _dicUtilitiesCaption ?? (Dictionary<string, Item>)BuildConfig(5); return _dicUtilitiesCaption; } }
        static Dictionary<string, Item> _dicBrowser; public static Dictionary<string, Item> DicBrowser { get { _dicBrowser = _dicBrowser ?? (Dictionary<string, Item>)BuildConfig(6); return _dicBrowser; } }
        private static readonly BindingList<object> SourceUnitPrice = new BindingList<object>() { "", "", "" };
        public static Dictionary<string, Template> ListTemplate = new Dictionary<string, Template>();
        static readonly int[] PurchaseTypeGroup = new int[] { 12, 13, 14, 17, 20, 22, 23 };
        static readonly int[] SaleTypeGroup = new int[] { 30, 31, 32, 33, 34 };
        public static Stopwatch StopWatch = new Stopwatch();
        public static BindingList<IList> Cache = new BindingList<IList>();
        //public static Form @Form;
        //public static Dictionary<string, Form> @Forms = new Dictionary<string, Form>();
        private static bool _isKeyPress = false;
        //public static bool Checked = true;
        public static readonly List<int> ListTypeByCreditAccount = new List<int>() { 100, 160 };//Ds Type chỉ kiểm tra theo cột TK có
        public static readonly List<int> ListTypeByDebitAccount = new List<int>() { 110, 120, 130, 140, 150, 170, 150 };//Ds Type chỉ kiểm tra theo cột TK nợ
        public static readonly List<int> ListTypeByBothAccount = new List<int>() { 600, 610, 620, 630, 640, 650, 660, 670 };//Ds Type kiểm tra theo cả 2 cột TK nợ và TK có

        public static List<int> ListTypeInward = new List<int>() { 400, 401, 402, 403, 404, 210, 260, 261, 262, 263, 264 };//Ds Type có sinh chứng từ Nhập kho
        public static List<int> ListTypeOutward = new List<int>() { 410, 411, 412, 413, 414, 415, 220, 320, 321, 322 };//Ds Type có sinh chứng từ Xuất kho

        private static int[] typesUpdateRepositoryLedger = new[] { 400, 401, 402, 403, 404, 200, 410, 411, 412, 413, 414, 415 };//DS Type có cập nhật sổ kho

        public static Color DefaultRowSelectedColor { get { return Color.Black; } }
        public static Color DefaultRowUnRecordedColor { get { return Color.Green; } }

        #region Service
        #region Danh mục
        public static IMaterialGoodsService IMaterialGoodsService
        {
            get { return IoC.Resolve<IMaterialGoodsService>(); }
        }
        public static IPaymentClauseService IPaymentClauseService
        {
            get { return IoC.Resolve<IPaymentClauseService>(); }
        }
        public static ITransportMethodService ITransportMethodService
        {
            get { return IoC.Resolve<ITransportMethodService>(); }
        }
        public static IRepositoryService IRepositoryService
        {
            get { return IoC.Resolve<IRepositoryService>(); }
        }
        public static IAccountService IAccountService
        {
            get { return IoC.Resolve<IAccountService>(); }
        }
        public static IInvoiceTypeService IInvoiceTypeService
        {
            get { return IoC.Resolve<IInvoiceTypeService>(); }
        }
        public static IRepositoryLedgerService IRepositoryLedgerService
        {
            get { return IoC.Resolve<IRepositoryLedgerService>(); }
        }
        public static IAccountingObjectService IAccountingObjectService
        {
            get { return IoC.Resolve<IAccountingObjectService>(); }
        }
        public static IBankAccountDetailService IBankAccountDetailService
        {
            get { return IoC.Resolve<IBankAccountDetailService>(); }
        }
        public static IExpenseItemService IExpenseItemService
        {
            get { return IoC.Resolve<IExpenseItemService>(); }
        }
        public static IDepartmentService IDepartmentService
        {
            get { return IoC.Resolve<IDepartmentService>(); }
        }
        public static IBudgetItemService IBudgetItemService
        {
            get { return IoC.Resolve<IBudgetItemService>(); }
        }
        public static IStatisticsCodeService IStatisticsCodeService
        {
            get { return IoC.Resolve<IStatisticsCodeService>(); }
        }
        public static ICostSetService ICostSetService
        {
            get { return IoC.Resolve<ICostSetService>(); }
        }
        public static ICurrencyService ICurrencyService
        {
            get { return IoC.Resolve<ICurrencyService>(); }
        }
        public static IGenCodeService IGenCodeService
        {
            get { return IoC.Resolve<IGenCodeService>(); }
        }
        public static IGeneralLedgerService IGeneralLedgerService
        {
            get { return IoC.Resolve<IGeneralLedgerService>(); }
        }
        public static IFixedAssetLedgerService IFixedAssetLedgerService
        {
            get { return IoC.Resolve<IFixedAssetLedgerService>(); }
        }
        public static IAccountingObjectBankAccountService IAccountingObjectBankAccountService
        {
            get { return IoC.Resolve<IAccountingObjectBankAccountService>(); }
        }
        public static ICreditCardService ICreditCardService
        {
            get { return IoC.Resolve<ICreditCardService>(); }
        }
        public static ITypeService ITypeService
        {
            get { return IoC.Resolve<ITypeService>(); }
        }
        public static ITemplateService ITemplateService
        {
            get { return IoC.Resolve<ITemplateService>(); }
        }
        public static IAccountDefaultService IAccountDefaultService
        {
            get { return IoC.Resolve<IAccountDefaultService>(); }
        }
        public static IAccountTransferService IAccountTransferService
        {
            get { return IoC.Resolve<IAccountTransferService>(); }
        }
        public static IAccountGroupService IAccountGroupService
        {
            get { return IoC.Resolve<IAccountGroupService>(); }
        }
        public static IAutoPrincipleService IAutoPrincipleService
        {
            get { return IoC.Resolve<IAutoPrincipleService>(); }
        }
        public static IAccountingObjectCategoryService IAccountingObjectCategoryService
        {
            get { return IoC.Resolve<IAccountingObjectCategoryService>(); }
        }
        public static IAccountingObjectGroupService IAccountingObjectGroupService
        {
            get { return IoC.Resolve<IAccountingObjectGroupService>(); }
        }
        public static IPersonalSalaryTaxService IPersonalSalaryTaxService
        {
            get { return IoC.Resolve<IPersonalSalaryTaxService>(); }
        }
        public static ITimeSheetSymbolsService ITimeSheetSymbolsService
        {
            get { return IoC.Resolve<ITimeSheetSymbolsService>(); }
        }
        public static IMaterialGoodsCategoryService IMaterialGoodsCategoryService
        {
            get { return IoC.Resolve<IMaterialGoodsCategoryService>(); }
        }
        public static IMaterialQuantumService IMaterialQuantumService
        {
            get { return IoC.Resolve<IMaterialQuantumService>(); }
        }
        public static IMaterialQuantumDetailService IMaterialQuantumDetailService
        {
            get { return IoC.Resolve<IMaterialQuantumDetailService>(); }
        }
        public static IFixedAssetCategoryService IFixedAssetCategoryService
        {
            get { return IoC.Resolve<IFixedAssetCategoryService>(); }
        }
        public static IFixedAssetService IFixedAssetService
        {
            get { return IoC.Resolve<IFixedAssetService>(); }
        }
        public static IFixedAssetDetailService IFixedAssetDetailService
        {
            get { return IoC.Resolve<IFixedAssetDetailService>(); }
        }
        public static IStockCategoryService IStockCategoryService
        {
            get { return IoC.Resolve<IStockCategoryService>(); }
        }
        public static IInvestorGroupService IInvestorGroupService
        {
            get { return IoC.Resolve<IInvestorGroupService>(); }
        }
        public static IInvestorService IInvestorService
        {
            get { return IoC.Resolve<IInvestorService>(); }
        }
        public static IRegistrationGroupService IRegistrationGroupService
        {
            get { return IoC.Resolve<IRegistrationGroupService>(); }
        }
        public static IShareHolderGroupService IShareHolderGroupService
        {
            get { return IoC.Resolve<IShareHolderGroupService>(); }
        }
        public static IBankService IBankService
        {
            get { return IoC.Resolve<IBankService>(); }
        }
        public static IContractStateService IContractStateService
        {
            get { return IoC.Resolve<IContractStateService>(); }
        }

        public static IGoodsServicePurchaseService IGoodsServicePurchaseService
        {
            get { return IoC.Resolve<IGoodsServicePurchaseService>(); }
        }
        #endregion

        #region MC Module
        public static IMCPaymentService IMCPaymentService
        {
            get { return IoC.Resolve<IMCPaymentService>(); }
        }
        public static IMCPaymentDetailVendorService IMCPaymentDetailVendorService
        {
            get { return IoC.Resolve<IMCPaymentDetailVendorService>(); }
        }
        public static IMCPaymentDetailService IMCPaymentDetailService
        {
            get { return IoC.Resolve<IMCPaymentDetailService>(); }
        }
        public static IMCPaymentDetailTaxService IMCPaymentDetailTaxService
        {
            get { return IoC.Resolve<IMCPaymentDetailTaxService>(); }
        }
        public static IMCReceiptService IMCReceiptService
        {
            get { return IoC.Resolve<IMCReceiptService>(); }
        }
        public static IMCReceiptDetailService IMCReceiptDetailService
        {
            get { return IoC.Resolve<IMCReceiptDetailService>(); }
        }
        public static IMCReceiptDetailCustomerService IMCReceiptDetailCustomerService
        {
            get { return IoC.Resolve<IMCReceiptDetailCustomerService>(); }
        }
        public static IMCReceiptDetailTaxService IMCReceiptDetailTaxService
        {
            get { return IoC.Resolve<IMCReceiptDetailTaxService>(); }
        }
        #endregion

        #region MB Module
        public static IMBCreditCardService IMBCreditCardService
        {
            get { return IoC.Resolve<IMBCreditCardService>(); }
        }
        public static IMBCreditCardDetailTaxService IMBCreditCardDetailTaxService
        {
            get { return IoC.Resolve<IMBCreditCardDetailTaxService>(); }
        }
        public static IMBCreditCardDetailService IMBCreditCardDetailService
        {
            get { return IoC.Resolve<IMBCreditCardDetailService>(); }
        }
        public static IMBTellerPaperDetailVendorService IMBTellerPaperDetailVendorService
        {
            get { return IoC.Resolve<IMBTellerPaperDetailVendorService>(); }
        }
        public static IMBTellerPaperDetailService IMBTellerPaperDetailService
        {
            get { return IoC.Resolve<IMBTellerPaperDetailService>(); }
        }
        public static IMBTellerPaperDetailTaxService IMBTellerPaperDetailTaxService
        {
            get { return IoC.Resolve<IMBTellerPaperDetailTaxService>(); }
        }
        public static IMBTellerPaperService IMBTellerPaperService
        {
            get { return IoC.Resolve<IMBTellerPaperService>(); }
        }
        public static IMBTellerPaperDetailService IMBTellerPaperDetailSerivce
        {
            get { return IoC.Resolve<IMBTellerPaperDetailService>(); }
        }
        public static IMBDepositService IMBDepositService
        {
            get { return IoC.Resolve<IMBDepositService>(); }
        }
        public static IMBDepositDetailTaxService IMBDepositDetailTaxService
        {
            get { return IoC.Resolve<IMBDepositDetailTaxService>(); }
        }
        public static IMBInternalTransferService IMBInternalTransferService
        {
            get { return IoC.Resolve<IMBInternalTransferService>(); }
        }
        public static IMBInternalTransferDetailService IMBInternalTransferDetailService
        {
            get { return IoC.Resolve<IMBInternalTransferDetailService>(); }
        }
        public static IMBInternalTransferTaxService IMBInternalTransferTaxService
        {
            get { return IoC.Resolve<IMBInternalTransferTaxService>(); }
        }


        #endregion

        #region PP Module
        public static IPPDiscountReturnService IPPDiscountReturnService
        {
            get { return IoC.Resolve<IPPDiscountReturnService>(); }
        }
        public static IPPDiscountReturnDetailService IPPDiscountReturnDetailService
        {
            get { return IoC.Resolve<IPPDiscountReturnDetailService>(); }
        }
        public static IPPServiceService IPPServiceService
        {
            get { return IoC.Resolve<IPPServiceService>(); }
        }
        public static IPPServiceDetailService IPPServiceDetailService
        {
            get { return IoC.Resolve<IPPServiceDetailService>(); }
        }
        public static IPPOrderService IPPOrderService
        {
            get { return IoC.Resolve<IPPOrderService>(); }
        }
        public static IPPOrderDetailService IPPOrderDetailService
        {
            get { return IoC.Resolve<IPPOrderDetailService>(); }
        }
        public static IPPInvoiceDetailService IPPInvoiceDetailService
        {
            get { return IoC.Resolve<IPPInvoiceDetailService>(); }
        }
        public static IPPInvoiceService IPPInvoiceService
        {
            get { return IoC.Resolve<IPPInvoiceService>(); }
        }
        #endregion

        #region RS Module
        public static IRSInwardOutwardService IRSInwardOutwardService
        {
            get { return IoC.Resolve<IRSInwardOutwardService>(); }
        }
        public static IRSInwardOutwardDetailService IRSInwardOutwardDetailService
        {
            get { return IoC.Resolve<IRSInwardOutwardDetailService>(); }
        }
        public static IRSTransferService IRSTransferService
        {
            get { return IoC.Resolve<IRSTransferService>(); }
        }
        public static IRSTransferDetailService IRSTransferDetailService
        {
            get { return IoC.Resolve<IRSTransferDetailService>(); }
        }
        public static IRSAssemblyDismantlementService IRSAssemblyDismantlementService
        {
            get { return IoC.Resolve<IRSAssemblyDismantlementService>(); }
        }
        public static IRSAssemblyDismantlementDetailService IRSAssemblyDismantlementDetailService
        {
            get { return IoC.Resolve<IRSAssemblyDismantlementDetailService>(); }
        }
        #endregion

        #region EM Module
        public static IEMContractService IEMContractService
        {
            get { return IoC.Resolve<IEMContractService>(); }
        }
        #endregion

        #region SA Module
        public static ISAQuoteService ISAQuoteService
        {
            get { return IoC.Resolve<ISAQuoteService>(); }
        }
        public static ISAQuoteDetailService ISAQuoteDetailService
        {
            get { return IoC.Resolve<ISAQuoteDetailService>(); }
        }
        public static ISAOrderService ISAOrderService
        {
            get { return IoC.Resolve<ISAOrderService>(); }
        }
        public static ISAOrderDetailService ISAOrderDetailService
        {
            get { return IoC.Resolve<ISAOrderDetailService>(); }
        }
        public static ISAReturnService ISAReturnService
        {
            get { return IoC.Resolve<ISAReturnService>(); }
        }
        public static ISAReturnDetailService ISAReturnDetailService
        {
            get { return IoC.Resolve<ISAReturnDetailService>(); }
        }
        public static ISAReturnDetailCustomerService ISAReturnDetailCustomerService
        {
            get { return IoC.Resolve<ISAReturnDetailCustomerService>(); }
        }
        public static ISAInvoiceService ISAInvoiceService
        {
            get { return IoC.Resolve<ISAInvoiceService>(); }
        }
        public static ISAInvoiceDetailService ISAInvoiceDetailService
        {
            get { return IoC.Resolve<ISAInvoiceDetailService>(); }
        }
        #endregion

        #region FA Module
        public static IFADecrementService IFADecrementService
        {
            get { return IoC.Resolve<IFADecrementService>(); }
        }
        public static IFAIncrementService IFAIncrementService
        {
            get { return IoC.Resolve<IFAIncrementService>(); }
        }
        public static IFAIncrementDetailService IFAIncrementDetailService
        {
            get { return IoC.Resolve<IFAIncrementDetailService>(); }
        }
        public static IFADecrementDetailService IFADecrementDetailService
        {
            get { return IoC.Resolve<IFADecrementDetailService>(); }
        }
        public static IFAAdjustmentService IFAAdjustmentService
        {
            get { return IoC.Resolve<IFAAdjustmentService>(); }
        }
        public static IFAAdjustmentDetailService IFAAdjustmentDetailService
        {
            get { return IoC.Resolve<IFAAdjustmentDetailService>(); }
        }

        public static IFATransferService IFATransferService
        {
            get { return IoC.Resolve<IFATransferService>(); }
        }
        public static IFATransferDetailService IFATransferDetailService
        {
            get { return IoC.Resolve<IFATransferDetailService>(); }
        }
        public static IFADepreciationService IFADepreciationService
        {
            get { return IoC.Resolve<IFADepreciationService>(); }
        }
        public static IFADepreciationDetailService IFADepreciationDetailService
        {
            get { return IoC.Resolve<IFADepreciationDetailService>(); }
        }
        #endregion

        #region GO Module
        public static IGOtherVoucherService IGOtherVoucherService
        {
            get { return IoC.Resolve<IGOtherVoucherService>(); }
        }
        #endregion

        #region SystemOption
        public static ISystemOptionService ISystemOptionService
        {
            get { return IoC.Resolve<ISystemOptionService>(); }
        }
        #endregion
        #endregion

        #region List object
        private static IList<AccountingObject> _cacheAccountingObjects = new List<AccountingObject>();
        //private static BindingList<AccountingObject> _listAccountingObject = new BindingList<AccountingObject>();
        public static BindingList<AccountingObject> ListAccountingObject
        {
            get
            {
                var listAccountingObject = typeof(BindingList<AccountingObject>).GetBindingListFromCache<AccountingObject>();
                if (listAccountingObject == null || listAccountingObject.Count == 0)
                {
                    _cacheAccountingObjects = IAccountingObjectService.GetOderByAll();
                    listAccountingObject = listAccountingObject ?? new BindingList<AccountingObject>();
                    _cacheAccountingObjects.AddToBindingList(listAccountingObject);
                    listAccountingObject.ManagerCache();
                }
                return listAccountingObject;
            }
            //set { _listAccountingObject = valueí }
        }

        static IList<AccountDefault> _cacheAccountDefaults = new List<AccountDefault>();
        public static BindingList<AccountDefault> ListAccountDefault
        {
            get
            {
                var listAccountDefault = typeof(BindingList<AccountDefault>).GetBindingListFromCache<AccountDefault>();
                if (listAccountDefault == null || listAccountDefault.Count == 0)
                {
                    _cacheAccountDefaults = IAccountDefaultService.GetAll();
                    listAccountDefault = listAccountDefault ?? new BindingList<AccountDefault>();
                    _cacheAccountDefaults.AddToBindingList(listAccountDefault);
                    listAccountDefault.ManagerCache();
                }
                return listAccountDefault;
            }
            //set { _listAccountDefault = value; }
        }
        static IList<Currency> _cacheCurrencys = new List<Currency>();
        public static BindingList<Currency> ListCurrency   //Bảng Currency
        {
            get
            {
                var listCurrency = typeof(BindingList<Currency>).GetBindingListFromCache<Currency>();
                if (listCurrency == null || listCurrency.Count == 0)
                {
                    _cacheCurrencys = ICurrencyService.OrderByID();
                    listCurrency = listCurrency ?? new BindingList<Currency>();
                    _cacheCurrencys.AddToBindingList(listCurrency);
                    listCurrency.ManagerCache();
                }
                return listCurrency;
            }
            //set { _listCurrency = value; }
        }
        static IList<BankAccountDetail> _cacheBankAccountDetails = new List<BankAccountDetail>();
        public static BindingList<BankAccountDetail> ListBankAccountDetail  //Bảng BankAccountDetail
        {
            get
            {
                var listBankAccountDetail = typeof(BindingList<BankAccountDetail>).GetBindingListFromCache<BankAccountDetail>();
                if (listBankAccountDetail == null || listBankAccountDetail.Count == 0)
                {
                    _cacheBankAccountDetails = IBankAccountDetailService.GetAllOrderBy();
                    listBankAccountDetail = listBankAccountDetail ?? new BindingList<BankAccountDetail>();
                    _cacheBankAccountDetails.AddToBindingList(listBankAccountDetail);
                    listBankAccountDetail.ManagerCache();
                }
                return listBankAccountDetail;
            }

            //set { _listBankAccountDetail = value; }
        }
        static IList<PaymentClause> _cachePaymentClauses = new List<PaymentClause>();
        public static BindingList<PaymentClause> ListPaymentClause  //Bảng PaymentClause
        {
            get
            {
                var listPaymentClause = typeof(BindingList<PaymentClause>).GetBindingListFromCache<PaymentClause>();
                if (listPaymentClause == null || listPaymentClause.Count == 0)
                {
                    _cachePaymentClauses = IPaymentClauseService.OrderByCode();
                    listPaymentClause = listPaymentClause ?? new BindingList<PaymentClause>();
                    _cachePaymentClauses.AddToBindingList(listPaymentClause);
                    listPaymentClause.ManagerCache();
                }
                return listPaymentClause;
            }
            //set { _listPaymentClause = value; }
        }
        static IList<MaterialGoodsCustom> _cacheMaterialGoodsCustoms = new List<MaterialGoodsCustom>();
        public static BindingList<MaterialGoodsCustom> ListMaterialGoodsCustom  //Bảng MaterialGoods
        {
            get
            {
                var listMaterialGoodsCustom = typeof(BindingList<MaterialGoodsCustom>).GetBindingListFromCache<MaterialGoodsCustom>();
                if (listMaterialGoodsCustom == null || listMaterialGoodsCustom.Count == 0)
                {
                    DateTime postedDate = StringToDateTime(ConstFrm.DbStartDate) ?? DateTime.Now;
                    _cacheMaterialGoodsCustoms = IMaterialGoodsService.GetMateriaGoodByAndRepositoryLedger(
                        postedDate, null);
                    listMaterialGoodsCustom = listMaterialGoodsCustom ?? new BindingList<MaterialGoodsCustom>();
                    _cacheMaterialGoodsCustoms.AddToBindingList(listMaterialGoodsCustom);
                    listMaterialGoodsCustom.ManagerCache();
                }
                return listMaterialGoodsCustom;
            }
            //set { _listMaterialGoodsCustom = value; }
        }
        static IList<Repository> _cacheRepositorys = new List<Repository>();
        public static BindingList<Repository> ListRepository    //Bảng Repository  
        {
            get
            {
                var listRepository = typeof(BindingList<Repository>).GetBindingListFromCache<Repository>();
                if (listRepository == null || listRepository.Count == 0)
                {
                    _cacheRepositorys = IRepositoryService.GetOrderby();
                    listRepository = listRepository ?? new BindingList<Repository>();
                    _cacheRepositorys.AddToBindingList(listRepository);
                    listRepository.ManagerCache();
                }
                return listRepository;
            }
            //set { _listRepository = value; }
        }
        static IList<BudgetItem> _cacheBudgetItems = new List<BudgetItem>();
        public static BindingList<BudgetItem> ListBudgetItem //Bảng BudgetItem
        {
            get
            {
                var listBudgetItem = typeof(BindingList<BudgetItem>).GetBindingListFromCache<BudgetItem>();
                if (listBudgetItem == null || listBudgetItem.Count == 0)
                {
                    _cacheBudgetItems = IBudgetItemService.GetAll_OrderByTreeIsParentNode();
                    listBudgetItem = listBudgetItem ?? new BindingList<BudgetItem>();
                    _cacheBudgetItems.AddToBindingList(listBudgetItem);
                    listBudgetItem.ManagerCache();
                }
                return listBudgetItem;
            }
            //set { _listBudgetItem = value; }
        }
        static IList<ExpenseItem> _cacheExpenseItems = new List<ExpenseItem>();
        public static BindingList<ExpenseItem> ListExpenseItem   //Bảng ExpenseItem
        {
            get
            {
                var listExpenseItem = typeof(BindingList<ExpenseItem>).GetBindingListFromCache<ExpenseItem>();
                if (listExpenseItem == null || listExpenseItem.Count == 0)
                {
                    _cacheExpenseItems = IExpenseItemService.GetAll_OrderByTreeIsParentNode();
                    listExpenseItem = listExpenseItem ?? new BindingList<ExpenseItem>();
                    _cacheExpenseItems.AddToBindingList(listExpenseItem);
                    listExpenseItem.ManagerCache();
                }
                return listExpenseItem;
            }
            //set { _listExpenseItem = value; }
        }
        static IList<Department> _cacheDepartments = new List<Department>();
        public static BindingList<Department> ListDepartment  //Bảng Department
        {
            get
            {
                var listDepartment = typeof(BindingList<Department>).GetBindingListFromCache<Department>();
                if (listDepartment == null || listDepartment.Count == 0)
                {
                    _cacheDepartments = IDepartmentService.GetAll_OrderByTreeIsParentNode();
                    listDepartment = listDepartment ?? new BindingList<Department>();
                    _cacheDepartments.AddToBindingList(listDepartment);
                    listDepartment.ManagerCache();
                }
                return listDepartment;
            }
            //set { _listDepartment = value; }
        }
        static IList<CostSet> _cacheCostSets = new List<CostSet>();
        public static BindingList<CostSet> ListCostSet  //Bảng CostSet
        {
            get
            {
                var listCostSet = typeof(BindingList<CostSet>).GetBindingListFromCache<CostSet>();
                if (listCostSet == null || listCostSet.Count == 0)
                {
                    _cacheCostSets = ICostSetService.GetAll_OrderByTreeIsParentNode();
                    listCostSet = listCostSet ?? new BindingList<CostSet>();
                    _cacheCostSets.AddToBindingList(listCostSet);
                    listCostSet.ManagerCache();
                }
                return listCostSet;
            }
            //set { _listCostSet = value; }
        }
        static IList<StatisticsCode> _cacheStatisticsCodes = new List<StatisticsCode>();
        public static BindingList<StatisticsCode> ListStatisticsCode  //Bảng StatisticsCode
        {
            get
            {
                var listStatisticsCode = typeof(BindingList<StatisticsCode>).GetBindingListFromCache<StatisticsCode>();
                if (listStatisticsCode == null || listStatisticsCode.Count == 0)
                {
                    _cacheStatisticsCodes = IStatisticsCodeService.GetAll_OrderByTreeIsParentNode();
                    listStatisticsCode = listStatisticsCode ?? new BindingList<StatisticsCode>();
                    _cacheStatisticsCodes.AddToBindingList(listStatisticsCode);
                    listStatisticsCode.ManagerCache();
                }
                return listStatisticsCode;
            }
            //set { _listStatisticsCode = value; }
        }
        static IList<EMContract> _cacheEmContracts = new List<EMContract>();
        public static BindingList<EMContract> ListEmContract  //Bảng EMContract
        {
            get
            {
                var listEmContract = typeof(BindingList<EMContract>).GetBindingListFromCache<EMContract>();
                if (listEmContract == null || listEmContract.Count == 0)
                {
                    _cacheEmContracts = IEMContractService.GetAll_OrderBy();
                    listEmContract = listEmContract ?? new BindingList<EMContract>();
                    _cacheEmContracts.AddToBindingList(listEmContract);
                    listEmContract.ManagerCache();
                }
                return listEmContract;
            }
            //set { _listEMContract = value; }
        }
        static IList<GoodsServicePurchase> _cacheGoodsServicePurchases = new List<GoodsServicePurchase>();
        public static BindingList<GoodsServicePurchase> ListGoodsServicePurchase //Bảng GoodsServicePurchase
        {
            get
            {
                var listGoodsServicePurchase = typeof(BindingList<GoodsServicePurchase>).GetBindingListFromCache<GoodsServicePurchase>();
                if (listGoodsServicePurchase == null || listGoodsServicePurchase.Count == 0)
                {
                    _cacheGoodsServicePurchases = IGoodsServicePurchaseService.GetAll_OrderBy();
                    listGoodsServicePurchase = listGoodsServicePurchase ?? new BindingList<GoodsServicePurchase>();
                    _cacheGoodsServicePurchases.AddToBindingList(listGoodsServicePurchase);
                    listGoodsServicePurchase.ManagerCache();
                }
                return listGoodsServicePurchase;
            }
            //set { _listGoodsServicePurchase = value; }
        }
        static IList<InvoiceType> _cacheInvoiceTypes = new List<InvoiceType>();
        public static BindingList<InvoiceType> ListInvoiceType //Bảng InvoiceType 
        {
            get
            {
                var listInvoiceType = typeof(BindingList<InvoiceType>).GetBindingListFromCache<InvoiceType>();
                if (listInvoiceType == null || listInvoiceType.Count == 0)
                {
                    _cacheInvoiceTypes = IInvoiceTypeService.GetAll_OrderBy();
                    listInvoiceType = listInvoiceType ?? new BindingList<InvoiceType>();
                    _cacheInvoiceTypes.AddToBindingList(listInvoiceType);
                    listInvoiceType.ManagerCache();
                }
                return listInvoiceType;
            }
            //set { _listInvoiceType = value; }
        }
        private static IList<AccountingObjectBankAccount> _cacheAccountingObjectBanks = new List<AccountingObjectBankAccount>();
        public static BindingList<AccountingObjectBankAccount> ListAccountingObjectBank
        {
            get
            {
                var listAccountingObjectBank = typeof(BindingList<AccountingObjectBankAccount>).GetBindingListFromCache<AccountingObjectBankAccount>();
                if (listAccountingObjectBank == null || listAccountingObjectBank.Count == 0)
                {
                    _cacheAccountingObjectBanks = IAccountingObjectBankAccountService.GetAll_OrderBy();
                    listAccountingObjectBank = listAccountingObjectBank ??
                                               new BindingList<AccountingObjectBankAccount>();
                    _cacheAccountingObjectBanks.AddToBindingList(listAccountingObjectBank);
                    listAccountingObjectBank.ManagerCache();
                }
                return listAccountingObjectBank;
            }
            //set { _listAccountingObjectBank = value; }
        }
        private static IList<FixedAsset> _cacheFixedAssets = new List<FixedAsset>();
        public static BindingList<FixedAsset> ListFixedAsset
        {
            get
            {
                var listFixedAsset = typeof(BindingList<FixedAsset>).GetBindingListFromCache<FixedAsset>();
                if (listFixedAsset == null || listFixedAsset.Count == 0)
                {
                    _cacheFixedAssets = IFixedAssetService.GetAll_OrderBy();
                    listFixedAsset = listFixedAsset ?? new BindingList<FixedAsset>();
                    _cacheFixedAssets.AddToBindingList(listFixedAsset);
                    listFixedAsset.ManagerCache();
                }
                return listFixedAsset;
            }
            //set { _listFixedAsset = value; }
        }
        private static IList<CreditCard> _cacheCreditCards = new List<CreditCard>();
        public static BindingList<CreditCard> ListCreditCard
        {
            get
            {
                var listCreditCard = typeof(BindingList<CreditCard>).GetBindingListFromCache<CreditCard>();
                if (listCreditCard == null || listCreditCard.Count == 0)
                {
                    _cacheCreditCards = ICreditCardService.GetAll_OrderBy();
                    listCreditCard = listCreditCard ?? new BindingList<CreditCard>();
                    _cacheCreditCards.AddToBindingList(listCreditCard);
                    listCreditCard.ManagerCache();
                }
                return listCreditCard;
            }
            //set { _listCreditCard = value; }
        }
        private static IList<SystemOption> _cacheSystemOptions = new List<SystemOption>();
        public static BindingList<SystemOption> ListSystemOption
        {
            get
            {
                var listSystemOption = typeof(BindingList<SystemOption>).GetBindingListFromCache<SystemOption>();
                if (listSystemOption == null || listSystemOption.Count == 0)
                {
                    _cacheSystemOptions = ISystemOptionService.GetAll();
                    listSystemOption = listSystemOption ?? new BindingList<SystemOption>();
                    _cacheSystemOptions.AddToBindingList(listSystemOption);
                    listSystemOption.ManagerCache();
                }
                return listSystemOption;
            }
            //set { _listCreditCard = value; }
        }
        static List<Account> _listAccount = new List<Account>();
        public static List<Account> ListAccount //Bảng Account
        {
            get
            {
                if (_listAccount.Count == 0)
                {
                    _listAccount.AddRange(IAccountService.GetListOrderBy());
                }
                return _listAccount;
            }
        }
        private static List<Type> _listType = new List<Type>();
        public static List<Type> ListType
        {
            get
            {
                if (_listType.Count == 0)
                {
                    _listType.AddRange(ITypeService.GetAll());
                }
                return _listType;
            }
        }
        static IList<SAQuote> _cacheSaQuotes = new List<SAQuote>();
        public static BindingList<SAQuote> ListSaQuote //Bảng SAQuote
        {
            get
            {
                var listSAQuote = typeof(BindingList<SAQuote>).GetBindingListFromCache<SAQuote>();
                if (listSAQuote == null || listSAQuote.Count == 0)
                {
                    _cacheSaQuotes = ISAQuoteService.OrderByCode();
                    listSAQuote = listSAQuote ?? new BindingList<SAQuote>();
                    _cacheSaQuotes.AddToBindingList(listSAQuote);
                    listSAQuote.ManagerCache();
                }
                return listSAQuote;
            }
            //set { _listInvoiceType = value; }
        }
        #endregion
        #endregion

        #region Xử lý string
        #region ham chuyen so thanh chu
        /* Copy from TVAN*/

        //hàm chữ 
        private static string Chu(string gNumber)
        {
            string result = "";
            switch (gNumber)
            {
                case "0":
                    result = "không";
                    break;
                case "1":
                    result = "một";
                    break;
                case "2":
                    result = "hai";
                    break;
                case "3":
                    result = "ba";
                    break;
                case "4":
                    result = "bốn";
                    break;
                case "5":
                    result = "năm";
                    break;
                case "6":
                    result = "sáu";
                    break;
                case "7":
                    result = "bảy";
                    break;
                case "8":
                    result = "tám";
                    break;
                case "9":
                    result = "chín";
                    break;
            }
            return result;
        }
        //ham don vi
        private static string Donvi(string so)
        {
            string Kdonvi = "";

            if (so.Equals("1"))
                Kdonvi = "";
            if (so.Equals("2"))
                Kdonvi = "nghìn";
            if (so.Equals("3"))
                Kdonvi = "triệu";
            if (so.Equals("4"))
                Kdonvi = "tỷ";
            if (so.Equals("5"))
                Kdonvi = "nghìn tỷ";
            if (so.Equals("6"))
                Kdonvi = "triệu tỷ";
            if (so.Equals("7"))
                Kdonvi = "tỷ tỷ";
            if (so.Equals("8"))
                Kdonvi = "nghìn tỷ tỷ";
            if (so.Equals("9"))
                Kdonvi = "triệu tỷ tỷ";

            return Kdonvi;
        }
        //ham tach
        private static string Tach(string tach3)
        {
            string Ktach = "";
            if (tach3.Equals("000"))
                return "";
            if (tach3.Length == 3)
            {
                string tr = tach3.Trim().Substring(0, 1).ToString().Trim();
                string ch = tach3.Trim().Substring(1, 1).ToString().Trim();
                string dv = tach3.Trim().Substring(2, 1).ToString().Trim();
                if (tr.Equals("0") && ch.Equals("0"))
                    Ktach = " không trăm lẻ " + Chu(dv.ToString().Trim()) + " ";
                if (!tr.Equals("0") && ch.Equals("0") && dv.Equals("0"))
                    Ktach = Chu(tr.ToString().Trim()).Trim() + " trăm ";
                if (!tr.Equals("0") && ch.Equals("0") && !dv.Equals("0"))
                    Ktach = Chu(tr.ToString().Trim()).Trim() + " trăm lẻ " + Chu(dv.Trim()).Trim() + " ";
                if (tr.Equals("0") && Convert.ToInt32(ch) > 1 && Convert.ToInt32(dv) > 0 && !dv.Equals("5"))
                    Ktach = " không trăm " + Chu(ch.Trim()).Trim() + " mươi " + Chu(dv.Trim()).Trim() + " ";
                if (tr.Equals("0") && Convert.ToInt32(ch) > 1 && dv.Equals("0"))
                    Ktach = " không trăm " + Chu(ch.Trim()).Trim() + " mươi ";
                if (tr.Equals("0") && Convert.ToInt32(ch) > 1 && dv.Equals("5"))
                    Ktach = " không trăm " + Chu(ch.Trim()).Trim() + " mươi lăm ";
                if (tr.Equals("0") && ch.Equals("1") && Convert.ToInt32(dv) > 0 && !dv.Equals("5"))
                    Ktach = " không trăm mười " + Chu(dv.Trim()).Trim() + " ";
                if (tr.Equals("0") && ch.Equals("1") && dv.Equals("0"))
                    Ktach = " không trăm mười ";
                if (tr.Equals("0") && ch.Equals("1") && dv.Equals("5"))
                    Ktach = " không trăm mười lăm ";
                if (Convert.ToInt32(tr) > 0 && Convert.ToInt32(ch) > 1 && Convert.ToInt32(dv) > 0 && !dv.Equals("5"))
                    Ktach = Chu(tr.Trim()).Trim() + " trăm " + Chu(ch.Trim()).Trim() + " mươi " + Chu(dv.Trim()).Trim() + " ";
                if (Convert.ToInt32(tr) > 0 && Convert.ToInt32(ch) > 1 && dv.Equals("0"))
                    Ktach = Chu(tr.Trim()).Trim() + " trăm " + Chu(ch.Trim()).Trim() + " mươi ";
                if (Convert.ToInt32(tr) > 0 && Convert.ToInt32(ch) > 1 && dv.Equals("5"))
                    Ktach = Chu(tr.Trim()).Trim() + " trăm " + Chu(ch.Trim()).Trim() + " mươi lăm ";
                if (Convert.ToInt32(tr) > 0 && ch.Equals("1") && Convert.ToInt32(dv) > 0 && !dv.Equals("5"))
                    Ktach = Chu(tr.Trim()).Trim() + " trăm mười " + Chu(dv.Trim()).Trim() + " ";

                if (Convert.ToInt32(tr) > 0 && ch.Equals("1") && dv.Equals("0"))
                    Ktach = Chu(tr.Trim()).Trim() + " trăm mười ";
                if (Convert.ToInt32(tr) > 0 && ch.Equals("1") && dv.Equals("5"))
                    Ktach = Chu(tr.Trim()).Trim() + " trăm mười lăm ";

            }


            return Ktach;

        }
        //ham su dung
        public static string So_chu(decimal gNum, string currencyID)
        {
            if (string.IsNullOrEmpty(currencyID)) currencyID = "VND";
            string sauDauPhay = "";
            ICurrencyService curSrv = IoC.Resolve<ICurrencyService>();
            string strUnit = curSrv.Getbykey(currencyID).CurrencyName;
            if (gNum == 0)
                return "Không " + strUnit;

            string stringNum = gNum.ToString().Replace("-", "");
            if (!stringNum.Contains(CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator))
            { strUnit += " chẵn"; }
            else
            {

                string[] strArrTemp = stringNum.Split(new string[] { CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator }, StringSplitOptions.None);
                string strTemp = strArrTemp[strArrTemp.Count() - 1];
                if (string.IsNullOrEmpty(strTemp.Replace("0", ""))) { strUnit += " chẵn"; }
                else
                {
                    sauDauPhay = "phảy";
                    for (int ijk = 0; ijk < strTemp.Count(); ijk++)
                    {
                        var v = sauDauPhay[ijk];
                        sauDauPhay += " " + Chu(strTemp[ijk].ToString());
                    }
                }
            }
            string lso_chu = "";
            string tach_mod = "";
            string tach_conlai = "";
            decimal Num = Math.Round(gNum, 0);
            string gN = Convert.ToString(Num).Replace("-", "");
            int m = Convert.ToInt32(gN.Length / 3);
            int mod = gN.Length - m * 3;
            string dau = "";

            // Dau [+ , - ]
            if (gNum < 0)
            {
                dau = "Âm";
                Num = -Num;
            }
            //dau = "";

            // Tach hang lon nhat
            if (mod.Equals(1))
                tach_mod = "00" + Convert.ToString(Num.ToString().Trim().Substring(0, 1)).Trim();
            if (mod.Equals(2))
                tach_mod = "0" + Convert.ToString(Num.ToString().Trim().Substring(0, 2)).Trim();
            if (mod.Equals(0))
                tach_mod = "000";
            // Tach hang con lai sau mod :
            if (Num.ToString().Length > 2)
                tach_conlai = Convert.ToString(Num.ToString().Trim().Substring(mod, Num.ToString().Length - mod)).Trim();

            ///don vi hang mod
            int im = m + 1;
            if (mod > 0)
                lso_chu = Tach(tach_mod).ToString().Trim() + " " + Donvi(im.ToString().Trim());
            /// Tach 3 trong tach_conlai

            int i = m;
            int _m = m;
            int j = 1;
            string tach3 = "";
            string tach3_ = "";

            while (i > 0)
            {
                tach3 = tach_conlai.Trim().Substring(0, 3).Trim();
                tach3_ = tach3;
                lso_chu = lso_chu.Trim() + " " + Tach(tach3.Trim()).Trim();
                m = _m + 1 - j;
                if (!tach3_.Equals("000"))
                    lso_chu = lso_chu.Trim() + " " + Donvi(m.ToString().Trim()).Trim();
                tach_conlai = tach_conlai.Trim().Substring(3, tach_conlai.Trim().Length - 3);

                i = i - 1;
                j = j + 1;
            }
            if (lso_chu.Trim().Substring(0, 1).Equals("k"))
                lso_chu = lso_chu.Trim().Substring(10, lso_chu.Trim().Length - 10).Trim();
            if (lso_chu.Trim().Substring(0, 1).Equals("l"))
                lso_chu = lso_chu.Trim().Substring(2, lso_chu.Trim().Length - 2).Trim();
            if (lso_chu.Trim().Length > 0)
                lso_chu = dau.Trim() + " " + lso_chu.Trim().Substring(0, 1).Trim().ToUpper() + lso_chu.Trim().Substring(1, lso_chu.Trim().Length - 1).Trim();
            //if (gNum < 0)
            //{
            //    lso_chu = "Âm " + lso_chu.ToString().Trim();
            //}
            //else
            //    lso_chu = lso_chu.ToString().Trim();
            lso_chu += " " + sauDauPhay + " " + strUnit;
            return lso_chu;
        }

        //public static string doc_so(string number)
        //{
        //    int num = Convert.ToInt32(number);
        //}
        #endregion
        #endregion

        #region Tiện ích chương trình
        #region tạo mã chứng từ tự động
        /// <summary>
        /// Check số chứng từ
        /// 1. dic trả về null số chứng từ không hợp lệ.
        /// 2. dic trả về giá trị số chứng từ hợp lệ và cho phép insert DB 
        /// </summary>
        /// <param name="no"></param>
        /// <returns></returns>
        public static Dictionary<string, string> CheckNo(string no)
        {
            //if (string.IsNullOrEmpty(no)) return null;
            const string noPattern = @"^([^\d]{0,10})(\d{6})([^\d]{0,10})$";
            System.Text.RegularExpressions.Match m = new System.Text.RegularExpressions.Regex(noPattern).Match(no);
            return m.Groups.Count > 1
                       ? new Dictionary<string, string>
                                 {
                                     {"Prefix", m.Groups[1].Value},
                                     {"Value", m.Groups[2].Value},
                                     {"Suffix", m.Groups[3].Value}
                                 }
                       : null;
        }

        /// <summary>
        /// Tạo mã chứng từ với Gencode
        /// </summary>
        /// <param name="gencode">Đối tượng Gencode</param>
        /// <returns>Mã chứng từ</returns>
        public static string TaoMaChungTu(GenCode gencode)
        {
            if (gencode == null) return string.Empty;
            GenCode genCodeTemp = gencode.CloneObject();
            string strNumber = string.Empty;
            if (genCodeTemp.Length > genCodeTemp.Length - genCodeTemp.CurrentValue.ToString().Length)
            {
                for (int i = 0; i < genCodeTemp.Length - genCodeTemp.CurrentValue.ToString().Length; i++) strNumber += "0";
            }
            strNumber += genCodeTemp.CurrentValue.ToString();
            return string.Format("{0}{1}{2}", genCodeTemp.Prefix ?? string.Empty, strNumber, genCodeTemp.Suffix ?? string.Empty);
        }
        #endregion

        /// <summary>
        /// lấy danh sách các tài khoản con, cháu chắt của một tài khoản cha (điều kiện: tài khoản cuối cùng là tài khoản ko có con)
        /// </summary>
        /// <param name="accParent">tài khoản cha cần tìm</param>
        /// <returns>danh sách tài khoản con, cháu, chắt .....</returns>
        public static List<Account> GetChildrenAccount(Account accParent)
        {
            List<Account> listAcc = IoC.Resolve<IAccountService>().GetChildrenAccount(accParent);
            return listAcc;
        }

        /// <summary>
        /// lấy tập tài khoản mặc định cho chứng từ
        /// </summary>
        /// <param name="typeId">chứng từ cần lấy các tài khoản mặc định</param>
        /// <returns>tập các tài khoản mặc định của chứng từ đó</returns>
        public static Dictionary<string, List<Account>> GetAccountDefaultByTypeID(int typeId)
        {
            Dictionary<string, List<Account>> kq = new Dictionary<string, List<Account>>();

            //lấy danh sách các tài khoản mặc định của chứng từ.
            List<AccountDefault> accountDefaults =
                IoC.Resolve<IAccountDefaultService>().Query.Where(k => k.TypeID == typeId).ToList();

            //run
            foreach (AccountDefault accountDefaultitem in accountDefaults)
            {
                string key = accountDefaultitem.ColumnName;
                List<Account> value = new List<Account>();

                #region tạo value (string FilterAccount => List<Account>)
                //tập các tài khoản FilterAccount
                string[] filterAccounts = accountDefaultitem.FilterAccount == null ? new string[0] : accountDefaultitem.FilterAccount.Split(';');
                //phân rã các tài khoản FilterAccount thành các tài khoản nguyên tử (vì nguyên tắc không hoạch toán vào tài khoản cha)
                foreach (string filterAccountitem in filterAccounts)
                {
                    Account temp = IoC.Resolve<IAccountService>().GetByAccountNumber(filterAccountitem);
                    if (temp == null) continue;
                    //kiểm tra tài khoản này có phải là cha hay không?
                    if (temp.IsParentNode) value.AddRange(GetChildrenAccount(temp));   //là tài khoản cha thì tìm các con của nó và gán tập các con của nó vào tập value
                    else value.Add(temp);    //không phải là tài khoản cha thì add luôn vào tập value
                }
                #endregion

                kq.Add(key, value.OrderBy(a => a.AccountNumber).ToList());
            }

            return kq;
        }

        #region lấy vị trí của một đối tượng trong một danh sách đối tượng
        public static int GetIndexOfList<T>(T item, IList<T> dsItem)
        {
            return GetIndexOfList(item, dsItem, "ID");
        }
        public static int GetIndexOfList<T>(T item, IList<T> dsItem, string key)
        {
            #region cách 1
            return dsItem.Select(k => GetValueOfPropertyObjectByNameType(k, key)).ToList().IndexOf(GetValueOfPropertyObjectByNameType(item, key));
            #endregion

            #region cách 2
            //for (int i = 0; i < dsItem.Count; i++)
            //{
            //    Guid guid1 = new Guid(item.GetType().GetProperty(key).GetValue(item, null).ToString());
            //    Guid guid2 = new Guid(dsItem[i].GetType().GetProperty(key).GetValue(dsItem[i], null).ToString());
            //    if (guid1 == guid2) return i;
            //}
            //return -0x1;
            #endregion
        }
        static object GetValueOfPropertyObjectByNameType<T>(T item, string nameType)
        {
            return item.GetType().GetProperty(nameType).GetValue(item, null);
        }
        //static object GetValueOfPropertyObjectByIndexType<T>(T item, int indexType)
        //{
        //    return item.GetType().GetProperties()[indexType].GetValue(item, null);
        //}
        #endregion
        #endregion

        #region Xử lý ảnh
        /// <summary>
        /// Hàm chuyển đổi một Image thành mảng byte[]
        /// [kiendd]
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static byte[] ImageTobyteArray(Image input)
        {
            using (System.IO.MemoryStream fileStream = new System.IO.MemoryStream())
            {
                input.Save(fileStream, System.Drawing.Imaging.ImageFormat.Png);
                return fileStream.ToArray();
            }
        }
        /// <summary>
        /// Hàm chuyển mảng byte[] thành file ảnh
        /// [kiendd]
        /// </summary>
        /// <param name="fileBytes"></param>
        /// <returns></returns>
        public static Image ByteArrayToImage(byte[] fileBytes)
        {
            using (System.IO.MemoryStream fileStream = new System.IO.MemoryStream(fileBytes))
            {
                return Image.FromStream(fileStream);
            }
        }
        #endregion

        #region Thời gian
        public static DateTime GetDateTimeDefault()
        {
            return new DateTime(1753, 1, 1);
        }

        public static DateTime GetDateTimeEmpty()
        {
            return new DateTime(0001, 1, 1);
        }

        public static DateTime StringToDateTime(string input, out bool kq)
        {
            return StringToDateTime(input, out kq, "dd/MM/yyyy");
        }
        /// <summary>
        /// Hàm hỗ trợ chuyển String to Datetime
        ///     - phục vụ cho xử lý thời gian Datetime với máy client có thời gian tiếng việt và tiếng anh....
        /// </summary>
        /// <param name="input"></param>
        /// <param name="kq"></param>
        /// <param name="patterns"></param>
        /// <returns></returns>
        public static DateTime StringToDateTime(string input, out bool kq, string patterns)
        {
            kq = true;
            try
            {
                DateTime dt = new DateTime(1753, 1, 1);
                switch (patterns)
                {
                    case Constants.DdMMyyyy:    //Mẫu ngày tháng năm
                        {
                            string[] arr = input.Split('/');
                            dt = new DateTime(int.Parse(arr[2]), int.Parse(arr[1]), int.Parse(arr[0]));
                        }
                        break;
                    case Constants.MMddyyyy:    //Mẫu tháng ngày năm
                        {
                            string[] arr = input.Split('/');
                            dt = new DateTime(int.Parse(arr[2]), int.Parse(arr[0]), int.Parse(arr[1]));
                        }
                        break;
                }
                return dt;
            }
            catch
            {
                kq = false;
                return new DateTime(1753, 1, 1);
            }
        }

        public static DateTime? StringToDateTime(string dateString)
        {
            //return dateString;
            string[] formats = new[] {
                    @"dd/MM/yyyy", @"dd-MM-yyyy", @"dd.MM.yyyy", @"dd\MM\yyyy",
                    @"dd/M/yyyy", @"dd-M-yyyy", @"d.M.yyyy", @"d\M\yyyy",  
                    @"dd/MM/yy", @"dd-MM-yy", @"dd.MM.yy", @"dd\MM\yy",
                    @"dd/M/yy", @"dd-M-yy", @"dd.M.yy", @"dd\M\yy",

                    @"MM/dd/yyyy", @"MM-dd-yyyy", @"MM.dd.yyyy", @"MM\dd\yyyy",
                    @"M/dd/yyyy", @"M-dd-yyyy", @"M.d.yyyy", @"M\d\yyyy",  
                    @"MM/dd/yy", @"MM-dd-yy", @"MM.dd.yy", @"MM\dd\yy",
                    @"M/dd/yy", @"M-dd-yy", @"M.dd.yy", @"M\dd\yy",
                    @"MM/yyyy", @"MM-yyyy", @"MM.yyyy", @"MM\yyyy",
                    @"MM/yy", @"MM-yy", @"MM.yy", @"MM\yy",
                    @"M/yy", @"M-yy", @"M.YY", @"M\YY",
            };

            //string[] formats = {"M/d/yyyy h:mm:ss tt", "M/d/yyyy h:mm tt", 
            //       "MM/dd/yyyy hh:mm:ss", "M/d/yyyy h:mm:ss", 
            //       "M/d/yyyy hh:mm tt", "M/d/yyyy hh tt", 
            //       "M/d/yyyy h:mm", "M/d/yyyy h:mm", 
            //       "MM/dd/yyyy hh:mm", "M/dd/yyyy hh:mm"};

            DateTime dateValue;
            DateTime? dfg = DateTime.TryParseExact(dateString, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out dateValue)
                       ? (DateTime?)dateValue
                       : null;
            return dfg;
        }

        public static void GetDateBeginDateEnd(DateTime ngayHoachToan, Item item, out DateTime dtBegin, out DateTime dtEnd)
        {
            dtBegin = DateTime.Now;
            dtEnd = DateTime.Now;
            int month = ngayHoachToan.Month;
            int year = ngayHoachToan.Year;
            // số ngày từ ngày đầu tuần tới ngày hiện tại
            int alpha = DayOfWeek.Monday - ngayHoachToan.DayOfWeek;
            switch (item.Value)
            {
                case "0": // hôm nay
                    dtBegin = dtEnd = ngayHoachToan;
                    break;
                case "1": // tuần ngày
                    dtBegin = ngayHoachToan.AddDays(alpha);
                    dtEnd = dtBegin.AddDays(6);
                    break;
                case "2": // đầu tuần đến hiện tại
                    dtBegin = ngayHoachToan.AddDays(alpha);
                    dtEnd = ngayHoachToan;
                    break;
                case "3": // tháng này
                    dtBegin = new DateTime(year, month, 1);
                    dtEnd = new DateTime(year, month, DateTime.DaysInMonth(year, month));
                    break;
                case "4": // đầu tháng tới hiện tại
                    dtBegin = new DateTime(year, month, 1);
                    dtEnd = ngayHoachToan;
                    break;
                case "5": // quý này
                    // quý I
                    if (month >= 1 && month <= 3)
                    {
                        dtBegin = new DateTime(year, 1, 1);
                        dtEnd = new DateTime(year, 3, 31);
                    }
                    // quý II
                    if (month >= 4 && month <= 6)
                    {
                        dtBegin = new DateTime(year, 4, 1);
                        dtEnd = new DateTime(year, 6, 30);
                    }
                    // quý III
                    if (month >= 7 && month <= 9)
                    {
                        dtBegin = new DateTime(year, 7, 1);
                        dtEnd = new DateTime(year, 9, 30);
                    }
                    // quý IV
                    if (month >= 10 && month <= 12)
                    {
                        dtBegin = new DateTime(year, 10, 1);
                        dtEnd = new DateTime(year, 12, 31);
                    }
                    break;
                case "6": // đầu quý đến hiện tại
                    // quý I
                    if (month >= 1 && month <= 3)
                    {
                        dtBegin = new DateTime(year, 1, 1);
                        dtEnd = ngayHoachToan;
                    }
                    // quý II
                    if (month >= 4 && month <= 6)
                    {
                        dtBegin = new DateTime(year, 4, 1);
                        dtEnd = ngayHoachToan;
                    }
                    // quý III
                    if (month >= 7 && month <= 9)
                    {
                        dtBegin = new DateTime(year, 7, 1);
                        dtEnd = ngayHoachToan;
                    }
                    // quý IV
                    if (month >= 10 && month <= 12)
                    {
                        dtBegin = new DateTime(year, 10, 1);
                        dtEnd = ngayHoachToan;
                    }
                    break;
                case "7": // năm nay
                    dtBegin = new DateTime(year, 1, 1);
                    dtEnd = new DateTime(year, 12, 31);
                    break;
                case "8": // đầu năm tới hiện tại
                    dtBegin = new DateTime(year, 1, 1);
                    dtEnd = ngayHoachToan;
                    break;
                case "9": // tháng 1
                    dtBegin = new DateTime(year, 1, 1);
                    dtEnd = new DateTime(year, 1, 31);
                    break;
                case "10": // tháng 2
                    dtBegin = new DateTime(year, 2, 1);
                    dtEnd = new DateTime(year, 2, DateTime.DaysInMonth(year, 2));
                    break;
                case "11": // tháng 3
                    dtBegin = new DateTime(year, 3, 1);
                    dtEnd = new DateTime(year, 3, 31);
                    break;
                case "12": // tháng 4
                    dtBegin = new DateTime(year, 4, 1);
                    dtEnd = new DateTime(year, 4, 30);
                    break;
                case "13": // tháng 5
                    dtBegin = new DateTime(year, 5, 1);
                    dtEnd = new DateTime(year, 5, 31);
                    break;
                case "14": // tháng 6
                    dtBegin = new DateTime(year, 6, 1);
                    dtEnd = new DateTime(year, 6, 30);
                    break;
                case "15": // tháng 7
                    dtBegin = new DateTime(year, 7, 1);
                    dtEnd = new DateTime(year, 7, 31);
                    break;
                case "16": // tháng 8
                    dtBegin = new DateTime(year, 8, 1);
                    dtEnd = new DateTime(year, 8, 31);
                    break;
                case "17": // tháng 9
                    dtBegin = new DateTime(year, 9, 1);
                    dtEnd = new DateTime(year, 9, 30);
                    break;
                case "18": // tháng 10
                    dtBegin = new DateTime(year, 10, 1);
                    dtEnd = new DateTime(year, 10, 31);
                    break;
                case "19": // tháng 11
                    dtBegin = new DateTime(year, 11, 1);
                    dtEnd = new DateTime(year, 11, 30);
                    break;
                case "20": // tháng 12
                    dtBegin = new DateTime(year, 12, 1);
                    dtEnd = new DateTime(year, 12, 31);
                    break;
                case "21": // quý I
                    dtBegin = new DateTime(year, 1, 1);
                    dtEnd = new DateTime(year, 3, 31);
                    break;
                case "22": // quý II
                    dtBegin = new DateTime(year, 4, 1);
                    dtEnd = new DateTime(year, 6, 30);
                    break;
                case "23": // quý III
                    dtBegin = new DateTime(year, 7, 1);
                    dtEnd = new DateTime(year, 9, 30);
                    break;
                case "24": // quý IV
                    dtBegin = new DateTime(year, 10, 1);
                    dtEnd = new DateTime(year, 12, 31);
                    break;
                case "25": // tuần trước
                    dtBegin = ngayHoachToan.AddDays(alpha - 7);
                    dtEnd = dtBegin.AddDays(6);
                    break;
                case "26": // tháng trước
                    int lastMonth, lastYear;
                    if (month == 1)
                    {
                        lastMonth = 12;
                        lastYear = year - 1;
                    }
                    else
                    {
                        lastMonth = month - 1;
                        lastYear = year;
                    }
                    dtBegin = new DateTime(lastYear, lastMonth, 1);
                    dtEnd = new DateTime(lastYear, lastMonth, DateTime.DaysInMonth(year, month));
                    break;
                case "27": // quý trước
                    // quý I
                    if (month >= 1 && month <= 3)
                    {
                        dtBegin = new DateTime(year - 1, 10, 1);
                        dtEnd = new DateTime(year - 1, 12, 31);
                    }
                    // quý II
                    if (month >= 4 && month <= 6)
                    {
                        dtBegin = new DateTime(year, 1, 1);
                        dtEnd = new DateTime(year, 3, 31);
                    }
                    // quý III
                    if (month >= 7 && month <= 9)
                    {
                        dtBegin = new DateTime(year, 4, 1);
                        dtEnd = new DateTime(year, 6, 30);
                    }
                    // quý IV
                    if (month >= 10 && month <= 12)
                    {
                        dtBegin = new DateTime(year, 7, 1);
                        dtEnd = new DateTime(year, 9, 30);
                    }
                    break;
                case "28": // năm trước
                    dtBegin = new DateTime(year - 1, 1, 1);
                    dtEnd = new DateTime(year - 1, 12, 31);
                    break;
                case "29": // tuần sau
                    dtBegin = ngayHoachToan.AddDays(alpha + 7);
                    dtEnd = dtBegin.AddDays(6);
                    break;
                case "30": // bốn tuần tới
                    dtBegin = ngayHoachToan;
                    dtEnd = ngayHoachToan.AddDays(28);
                    break;
                case "31": // tháng sau
                    int afterMonth, afterYear;
                    if (month == 12)
                    {
                        afterMonth = 1;
                        afterYear = year + 1;
                    }
                    else
                    {
                        afterMonth = month + 1;
                        afterYear = year;
                    }
                    dtBegin = new DateTime(afterYear, afterMonth, 1);
                    dtEnd = new DateTime(afterYear, afterMonth, DateTime.DaysInMonth(afterYear, afterMonth));
                    break;
                case "32": // quý sau
                    // quý I
                    if (month >= 1 && month <= 3)
                    {
                        dtBegin = new DateTime(year, 4, 1);
                        dtEnd = new DateTime(year, 6, 30);
                    }
                    // quý II
                    if (month >= 4 && month <= 6)
                    {
                        dtBegin = new DateTime(year, 7, 1);
                        dtEnd = new DateTime(year, 9, 30);
                    }
                    // quý III
                    if (month >= 7 && month <= 9)
                    {
                        dtBegin = new DateTime(year, 10, 1);
                        dtEnd = new DateTime(year, 12, 31);
                    }
                    // quý IV
                    if (month >= 10 && month <= 12)
                    {
                        dtBegin = new DateTime(year + 1, 1, 1);
                        dtEnd = new DateTime(year + 1, 3, 31);
                    }
                    break;
                case "33": // năm sau
                    dtBegin = new DateTime(year + 1, 1, 1);
                    dtEnd = new DateTime(year + 1, 12, 31);
                    break;
                case "34": // Tùy chọn
                    dtBegin = dtEnd = ngayHoachToan;
                    break;
            }
        }

        /// <summary>
        /// Cấu hình tiền tệ cho toàn chương trình
        /// </summary>
        /// <returns></returns>
        public static CultureInfo Config()
        {
            //số chữ số sau dấu phẩy, ký hiệu ngăn cách, ký hiệu tiền tệ, giao diện nhập liệu, màn hình hiển thị.....
            // Creates a CultureInfo for US in en.
            return new CultureInfo("en-US")
            {
                DateTimeFormat = { AMDesignator = "AM", DateSeparator = "/" },
                NumberFormat =
                {
                    CurrencyGroupSeparator = GetFormatThousands(),
                    NumberGroupSeparator = GetFormatThousands(),
                    PercentGroupSeparator = GetFormatThousands(),
                    CurrencyDecimalSeparator = GetFormatUnit(),
                    NumberDecimalSeparator = GetFormatUnit(),
                    PercentDecimalSeparator = GetFormatUnit(),
                    NumberDecimalDigits = 2,
                    CurrencySymbol = GetFormatSymbol(),
                    NegativeInfinitySymbol = GetFormatNegative()
                }
            };
        }
        #endregion

        #region Hệ thống
        /// <summary>
        /// Clone một đối tượng bất kỳ để loại bỏ hoàn toàn khi gán 1 đối tượng (yêu cầu đối tượng có thuộc tính Serializable)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static T CloneObject<T>(this T obj)
        {
            try
            {
                if (Object.ReferenceEquals(obj, null))
                {
                    return default(T);
                }
                if (obj.GetType().Name.Contains("List"))
                    if (((IList)obj).Count == 0)
                    {
                        obj = (T)Activator.CreateInstance(obj.GetType());
                        return obj;
                    }
                PropertyInfo propCount = obj.GetType().GetProperty("Count");
                if (propCount != null && propCount.CanRead)
                    if ((int)propCount.GetValue(obj, null) == 0)
                    {
                        obj = (T)Activator.CreateInstance(obj.GetType());
                        return obj;
                    }
                using (System.IO.MemoryStream memStream = new System.IO.MemoryStream())
                {
                    System.Runtime.Serialization.Formatters.Binary.BinaryFormatter binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter(null,
                         new System.Runtime.Serialization.StreamingContext(System.Runtime.Serialization.StreamingContextStates.Clone));
                    binaryFormatter.Serialize(memStream, obj);
                    memStream.Seek(0, System.IO.SeekOrigin.Begin);
                    return (T)binaryFormatter.Deserialize(memStream);
                }
            }
            catch (Exception)
            {
                return default(T);
            }
        }

        public static System.Data.DataSet ToDataSet<T>(IList<T> list, string nameTable)
        {
            const string colParent = "ID";
            const string colChild = "ParentID";
            return ToDataSet(list, nameTable, colParent, colChild);
        }
        /// <summary>
        /// Chuyển một tập các đối tượng thành Dataset (trong chỉ có 1 bảng)
        /// </summary>
        /// <typeparam name="T">kiểu dữ liệu</typeparam>
        /// <param name="list">tập đối tượng</param>
        /// <param name="nameTable">tên bảng</param>
        /// <param name="colParent">tên cột cha</param>
        /// <param name="colChild">tên cột con</param>
        /// <returns>trả về 1 dataset trong chứa 1 datatable</returns>
        public static System.Data.DataSet ToDataSet<T>(IList<T> list, string nameTable, string colParent, string colChild)
        {
            System.Type elementType = typeof(T);
            System.Data.DataSet ds = new System.Data.DataSet();
            System.Data.DataTable t = new System.Data.DataTable(nameTable);
            ds.Tables.Add(t);

            foreach (var propInfo in elementType.GetProperties())
            {
                System.Type colType = System.Nullable.GetUnderlyingType(propInfo.PropertyType) ?? propInfo.PropertyType;

                t.Columns.Add(propInfo.Name, colType);
            }

            foreach (T item in list)
            {
                System.Data.DataRow row = t.NewRow();

                foreach (var propInfo in elementType.GetProperties())
                {
                    row[propInfo.Name] = propInfo.GetValue(item, null) ?? DBNull.Value;
                }

                t.Rows.Add(row);
            }

            ds.Relations.Add("RelationsName", ds.Tables[0].Columns[colParent], ds.Tables[0].Columns[colChild], false);
            return ds;
        }

        public static void Copy(System.Type fromType, object from, System.Type toType, object to)
        {
            if (fromType == null)
                throw new ArgumentNullException("fromType", "The type that you are copying from cannot be null");

            if (from == null)
                throw new ArgumentNullException("from", "The object you are copying from cannot be null");

            if (toType == null)
                throw new ArgumentNullException("toType", "The type that you are copying to cannot be null");

            if (to == null)
                throw new ArgumentNullException("to", "The object you are copying to cannot be null");

            // Don't copy if they are the same object  
            if (!ReferenceEquals(from, to))
            {
                BindingFlags flags = BindingFlags.Public | BindingFlags.Instance | BindingFlags.GetProperty | BindingFlags.SetProperty;
                // Get all of the public properties in the toType with getters and setters  
                Dictionary<string, PropertyInfo> toProperties = new Dictionary<string, PropertyInfo>();
                PropertyInfo[] properties = toType.GetProperties(flags);
                foreach (PropertyInfo property in properties)
                {
                    toProperties.Add(property.Name, property);
                }

                // Now get all of the public properties in the fromType with getters and setters  
                properties = fromType.GetProperties(flags);
                foreach (PropertyInfo fromProperty in properties)
                {
                    // If a property matches in name and type, copy across  
                    if (toProperties.ContainsKey(fromProperty.Name))
                    {
                        PropertyInfo toProperty = toProperties[fromProperty.Name];
                        if (toProperty.PropertyType == fromProperty.PropertyType)
                        {
                            object value = fromProperty.GetValue(from, null);
                            toProperty.SetValue(to, value, null);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Ham so snah 2 doi tuong bat ky
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="object1"></param>
        /// <param name="object2"></param>
        /// <returns></returns>
        public static bool Compare<T>(T object1, T object2)
        {
            bool check = true;
            var fullName = object1.GetType().FullName;
            if (fullName != null && ((object1 as IList) == null))
            {
                var propObjects = object1.GetType().GetProperties();
                foreach (var info in propObjects)
                {
                    if (!info.PropertyType.Name.Contains("IList"))
                    {
                        bool checkItem = true;
                        var propObject2 = object2.GetType().GetProperty(info.Name);
                        if (propObject2 != null && propObject2.CanWrite && propObject2.CanRead)
                        {
                            var valueObject1 = info.GetValue(object1, null);
                            var valueObject2 = propObject2.GetValue(object2, null);
                            if (valueObject1 != null && valueObject2 != null)
                            {
                                try
                                {
                                    checkItem = valueObject1.ToString().Equals(valueObject2.ToString());
                                }
                                catch (Exception)
                                {
                                    if (valueObject1.GetType().GetProperties().Length != 0)
                                    {
                                        try
                                        {
                                            checkItem = Compare(valueObject1, valueObject2);
                                        }
                                        catch
                                        {
                                            checkItem = true;
                                        }
                                    }
                                }
                            }
                            else if (!((valueObject1 == null && valueObject2 == null) || (valueObject1 != null && valueObject2 != null && valueObject1.Equals(valueObject2))))
                                checkItem = false;
                        }
                        else if (propObject2 == null || (info.CanWrite && !propObject2.CanWrite))
                            checkItem = false;
                        if (!checkItem)
                            check = false;
                    }
                }
            }
            else
            {
                IList listObject1 = (IList)object1;
                IList listObject2 = (IList)object2;
                if (listObject1.Count == listObject2.Count)
                {
                    if (listObject1.Count > 0)
                        if (listObject1[0].GetType().Name.Contains("List"))
                        {
                            if (!Compare(listObject1[0], listObject2[0]))
                                check = false;
                        }
                        else
                        {
                            for (int i = 0; i < listObject1.Count; i++)
                            {
                                if (!Compare(listObject1[i], listObject2[i]))
                                    check = false;
                            }
                        }
                }
                else
                    check = false;
            }
            return check;
        }

        //xử lý XML ConstValue
        static object BuildConfig(int obj)
        {
            switch (obj)
            {
                case 1: return ObjConstValue.AccountKinds.ToDictionary(k => k.Value, k => k);   //AccountKinds
                case 2: return ObjConstValue.DetailTypes.ToDictionary(k => k.Value, k => k);    //DetailTypes
                case 3: return ObjConstValue.SelectTimes.ToDictionary(k => k.Value, k => k);
                case 4: return ObjConstValue.Utilities.ToDictionary(k => k.ID, k => k);
                case 5: return ObjConstValue.UtilitiesCaption.ToDictionary(k => k.Name, k => k);
                case 6: return ObjConstValue.Browser.ToDictionary(k => k.ID, k => k);
                default: return ObjectXMLSerializer<ConstValue>.Load(PathConfigXml);
            }
        }
        #endregion

        #region Hàm chung Frm
        #region GRID
        #region Config Grid
        /// <summary>
        /// Hàm cấu hình combobox 
        /// </summary>
        /// <param name="ultraCombo">Ultra1Combobox</param>
        /// <param name="nameTable">Tên bảng</param>
        public static void ConfigGrid(UltraCombo ultraCombo, string nameTable)
        {
            UltraGridBand band = ultraCombo.DisplayLayout.Bands[0];
            ultraCombo.BorderStyle = UIElementBorderStyle.Solid;
            ultraCombo.DisplayStyle = EmbeddableElementDisplayStyle.Default;
            ultraCombo.DisplayLayout.BorderStyle = UIElementBorderStyle.Solid;
            ultraCombo.DisplayLayout.AutoFitStyle = AutoFitStyle.ExtendLastColumn;
            //Cau hinh kieu Scroll
            ultraCombo.DisplayLayout.ScrollStyle = ScrollStyle.Immediate;
            ultraCombo.DisplayLayout.ScrollBounds = ScrollBounds.ScrollToFill;
            ultraCombo.DisplayLayout.Scrollbars = Scrollbars.Automatic;
            //
            band.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            band.Override.SelectedRowAppearance.ForeColor = Color.Black;
            band.Override.ActiveRowAppearance.ForeColor = Color.Black;
            //band.Override.HotTrackRowAppearance.ForeColor = Color.White;
            //band.Override.HotTrackRowSelectorAppearance.BackColor = Color.FromArgb(51, 153, 255);
            //band.Override.HotTrackRowCellAppearance.BackColor = Color.FromArgb(51, 153, 255);
            band.Override.BorderStyleCell = UIElementBorderStyle.None;
            band.Override.BorderStyleRow = UIElementBorderStyle.None;
            //Tiêu đề cột
            string headerCaptionFirst = string.Empty;
            foreach (var item in band.Columns)
            {
                if (ConstDatabase.TablesInDatabase[nameTable].ContainsKey(item.Key))
                {
                    ultraCombo.DisplayLayout.ScrollBounds = ScrollBounds.ScrollToFill;
                    TemplateColumn temp = ConstDatabase.TablesInDatabase[nameTable][item.Key];
                    if (string.IsNullOrEmpty(headerCaptionFirst) && temp.IsVisible) headerCaptionFirst = item.Key;   //Lấy dòng Header Caption đầu tiên
                    item.Header.Caption = temp.ColumnCaption;
                    item.Header.ToolTipText = temp.ColumnToolTip;
                    //item.Header.Enabled = !temp.IsReadOnly;
                    item.CellActivation = temp.IsReadOnly ? Activation.NoEdit : Activation.AllowEdit;
                    //Set màu header
                    item.Header.Appearance.BorderAlpha = Alpha.Transparent;
                    item.Header.Appearance.TextHAlign = HAlign.Left;
                    //item.Header.Appearance.BackColor = Color.FromArgb(50, 104, 189);
                    //item.Header.Appearance.ForeColor = Color.White;
                    item.Width = temp.ColumnWidth == -1 ? item.Width : temp.ColumnWidth;
                    item.MaxWidth = temp.ColumnMaxWidth == -1 ? item.MaxWidth : temp.ColumnMaxWidth;
                    item.MinWidth = temp.ColumnMinWidth == -1 ? item.MinWidth : temp.ColumnMinWidth;
                    item.Hidden = !temp.IsVisibleCbb;
                    item.Header.VisiblePosition = temp.VisiblePosition == -1 ? item.Header.VisiblePosition : temp.VisiblePosition;
                    if (item.Hidden == false && item.Key.Contains("Quantity"))
                        FormatNumberic(item, ConstDatabase.Format_Quantity);
                }
                else
                {
                    item.Hidden = true;
                }
            }
            //ultraCombo.DropDownWidth = 0;// countVisible.Equals(2) ? : 0;
        }

        /// <summary>
        /// Hàm cấu hình combobox 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="this"> </param>
        /// <param name="ultraCombo">Ultra1Combobox</param>
        /// <param name="nameTable">Tên bảng</param>
        /// <param name="nameParentId">Tên cột chứa parentID</param>
        /// <param name="nameParentNode">Tên cột xác định có phải là cha hay ko</param>
        public static void ConfigGrid<T>(this Form @this, UltraCombo ultraCombo, string nameTable, string nameParentId = null, string nameParentNode = null)
        {
            UltraGridBand band = ultraCombo.DisplayLayout.Bands[0];
            ultraCombo.BorderStyle = UIElementBorderStyle.Solid;
            ultraCombo.DisplayLayout.BorderStyle = UIElementBorderStyle.Solid;
            ultraCombo.DisplayStyle = EmbeddableElementDisplayStyle.Default;
            //ultraCombo.DisplayLayout.Appearance.BorderColor = Color.FromArgb(229, 195, 101);
            ultraCombo.DisplayLayout.AutoFitStyle = AutoFitStyle.ExtendLastColumn;
            //Cau hinh kieu Scroll
            ultraCombo.DisplayLayout.ScrollStyle = ScrollStyle.Immediate;
            ultraCombo.DisplayLayout.ScrollBounds = ScrollBounds.ScrollToFill;
            ultraCombo.DisplayLayout.Scrollbars = Scrollbars.Automatic;
            //
            band.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            band.Override.ActiveRowAppearance.ForeColor = DefaultRowSelectedColor;
            //band.Override.HotTrackRowAppearance.ForeColor = Color.White;
            //band.Override.HotTrackRowSelectorAppearance.BackColor = Color.FromArgb(51, 153, 255);
            //band.Override.HotTrackRowCellAppearance.BackColor = Color.FromArgb(51, 153, 255);
            band.Override.BorderStyleCell = UIElementBorderStyle.None;
            band.Override.BorderStyleRow = UIElementBorderStyle.None;
            //Tiêu đề cột
            string headerCaptionFirst = string.Empty;
            foreach (var item in band.Columns)
            {
                if (ConstDatabase.TablesInDatabase[nameTable].ContainsKey(item.Key))
                {
                    item.Header.Appearance.BorderAlpha = Alpha.Transparent;
                    TemplateColumn temp = ConstDatabase.TablesInDatabase[nameTable][item.Key];
                    if (string.IsNullOrEmpty(headerCaptionFirst) && temp.IsVisible) headerCaptionFirst = item.Key;   //Lấy dòng Header Caption đầu tiên
                    item.Header.Caption = temp.ColumnCaption;
                    item.Header.ToolTipText = temp.ColumnToolTip;
                    //item.Header.Enabled = !temp.IsReadOnly;
                    item.CellActivation = temp.IsReadOnly ? Activation.NoEdit : Activation.AllowEdit;
                    //Set màu header
                    item.Header.Appearance.BorderAlpha = Alpha.Transparent;
                    item.Header.Appearance.TextHAlign = HAlign.Left;
                    //item.Header.Appearance.BackColor = Color.FromArgb(50, 104, 189);
                    //item.Header.Appearance.ForeColor = Color.White;
                    item.Width = temp.ColumnWidth == -1 ? item.Width : temp.ColumnWidth;
                    item.MaxWidth = temp.ColumnMaxWidth == -1 ? item.MaxWidth : temp.ColumnMaxWidth;
                    item.MinWidth = temp.ColumnMinWidth == -1 ? item.MinWidth : temp.ColumnMinWidth;
                    item.Hidden = !temp.IsVisibleCbb;
                    item.Header.VisiblePosition = temp.VisiblePosition == -1 ? item.Header.VisiblePosition : temp.VisiblePosition;
                    if (item.Hidden == false && item.Key.Contains("Quantity"))
                        FormatNumberic(item, ConstDatabase.Format_Quantity);
                }
                else
                {
                    item.Hidden = true;
                }
            }
            List<TemplateColumn> templateColumns = ConstDatabase.TablesInDatabase[nameTable].Values.ToList();
            var firstOrDefault = templateColumns.Where(p => p.IsVisibleCbb).OrderBy(p => p.VisiblePosition).FirstOrDefault();
            if (firstOrDefault != null)
                headerCaptionFirst =
                    firstOrDefault.ColumnName;
            var filter = new CustomCreationFilter<T>(@this) { Key = headerCaptionFirst };
            if (ultraCombo.CreationFilter == null)
            {
                ultraCombo.CreationFilter = filter;
            }
            else
            {
                filter = (CustomCreationFilter<T>)ultraCombo.CreationFilter;
                filter.Key = headerCaptionFirst;
            }
            if (!string.IsNullOrEmpty(nameParentId) && !string.IsNullOrEmpty(nameParentNode))
            {
                filter.IsTree = true;
                foreach (var item in ultraCombo.Rows)
                {
                    if (!(bool)item.Cells[nameParentNode].Value && item.Cells[nameParentId].Value != null)
                    {
                        filter.CellsWithPadding.Add(ultraCombo.Rows[item.Index].Cells[ultraCombo.DisplayMember], 20);
                    }
                    else
                    {
                        if ((bool)item.Cells[nameParentNode].Value)
                            item.Appearance.FontData.Bold = DefaultableBoolean.True;
                        filter.CellsWithPadding.Add(ultraCombo.Rows[item.Index].Cells[ultraCombo.DisplayMember], 0);
                    }
                }
                ultraCombo.MouseEnterElement += new Infragistics.Win.UIElementEventHandler(Combo_MouseEnterElement<T>);
                ultraCombo.MouseLeaveElement += new Infragistics.Win.UIElementEventHandler(Combo_MouseLeaveElement<T>);
            }
        }

        public static void ConfigGrid(UltraDropDown ultraDropDown, string nameTable)
        {
            UltraGridBand band = ultraDropDown.DisplayLayout.Bands[0];
            ultraDropDown.DisplayLayout.BorderStyle = UIElementBorderStyle.WindowsVista;
            //Cau hinh kieu Scroll
            ultraDropDown.DisplayLayout.ScrollStyle = ScrollStyle.Immediate;
            ultraDropDown.DisplayLayout.ScrollBounds = ScrollBounds.ScrollToFill;
            //ultraDropDown.DisplayLayout.AutoFitStyle = AutoFitStyle.ExtendLastColumn;
            band.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            //band.Override.SelectedRowAppearance.BackColor = System.Drawing.Color.FromArgb(255, 192, 128);
            //band.Override.HotTrackRowAppearance.ForeColor = Color.White;
            //band.Override.HotTrackRowSelectorAppearance.BackColor = Color.FromArgb(51, 153, 255);
            //band.Override.HotTrackRowCellAppearance.BackColor = Color.FromArgb(51, 153, 255);
            band.Override.BorderStyleCell = UIElementBorderStyle.None;
            band.Override.BorderStyleRow = UIElementBorderStyle.None;

            //Tiêu đề cột
            string headerCaptionFirst = string.Empty;
            foreach (var item in band.Columns)
            {
                if (!ConstDatabase.TablesInDatabase[nameTable].ContainsKey(item.Key)) continue;
                TemplateColumn temp = ConstDatabase.TablesInDatabase[nameTable][item.Key];
                if (string.IsNullOrEmpty(headerCaptionFirst) && temp.IsVisible) headerCaptionFirst = item.Key;   //Lấy dòng Header Caption đầu tiên
                item.Header.Caption = temp.ColumnCaption;
                item.Header.ToolTipText = temp.ColumnToolTip;
                item.Header.Enabled = !temp.IsReadOnly;
                //Set màu header
                item.Header.Appearance.BorderAlpha = Alpha.Transparent;
                //item.Header.Appearance.BackColor = Color.FromArgb(227, 239, 255);
                //item.Header.Appearance.ForeColor = Color.Black;
                item.Width = temp.ColumnWidth == -1 ? item.Width : temp.ColumnWidth;
                item.MaxWidth = temp.ColumnMaxWidth == -1 ? item.MaxWidth : temp.ColumnMaxWidth;
                item.MinWidth = temp.ColumnMinWidth == -1 ? item.MinWidth : temp.ColumnMinWidth;
                item.Hidden = !temp.IsVisibleCbb;
                item.Header.VisiblePosition = temp.VisiblePosition == -1 ? item.Header.VisiblePosition : temp.VisiblePosition;
                if (item.Hidden == false && item.Key.Contains("Quantity"))
                    FormatNumberic(item, ConstDatabase.Format_Quantity);
            }
        }

        /// <summary>
        /// Config DropDown dạng Tree
        /// </summary>
        /// <param name="this"> </param>
        /// <param name="ultraDropDown"></param>
        /// <param name="nameTable"></param>
        /// <param name="nameParentId">Tên cột chứa parentID</param>
        /// <param name="nameParentNode">Tên cột xác định có phải là cha hay ko</param>
        public static void ConfigGrid<T>(this Form @this, UltraDropDown ultraDropDown, string nameTable, string nameParentId, string nameParentNode)
        {
            UltraGridBand band = ultraDropDown.DisplayLayout.Bands[0];
            var filter = new CustomCreationFilter<T>(@this) { IsTree = true };
            ultraDropDown.CreationFilter = filter;
            ultraDropDown.DisplayLayout.BorderStyle = UIElementBorderStyle.WindowsVista;
            //Cau hinh kieu Scroll
            ultraDropDown.DisplayLayout.ScrollStyle = ScrollStyle.Immediate;
            //ultraDropDown.DisplayLayout.AutoFitStyle = AutoFitStyle.ExtendLastColumn;
            band.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            //band.Override.SelectedRowAppearance.BackColor = System.Drawing.Color.FromArgb(255, 192, 128);
            //band.Override.HotTrackRowAppearance.ForeColor = Color.White;
            //band.Override.HotTrackRowSelectorAppearance.BackColor = Color.FromArgb(51, 153, 255);
            //band.Override.HotTrackRowCellAppearance.BackColor = Color.FromArgb(51, 153, 255);
            band.Override.BorderStyleCell = UIElementBorderStyle.None;
            band.Override.BorderStyleRow = UIElementBorderStyle.None;

            //Tiêu đề cột
            string headerCaptionFirst = string.Empty;
            foreach (var item in band.Columns)
            {
                if (!ConstDatabase.TablesInDatabase[nameTable].ContainsKey(item.Key)) continue;
                TemplateColumn temp = ConstDatabase.TablesInDatabase[nameTable][item.Key];
                if (string.IsNullOrEmpty(headerCaptionFirst) && temp.IsVisible) headerCaptionFirst = item.Key;   //Lấy dòng Header Caption đầu tiên
                item.Header.Caption = temp.ColumnCaption;
                item.Header.ToolTipText = temp.ColumnToolTip;
                item.Header.Enabled = !temp.IsReadOnly;
                //Set màu header
                item.Header.Appearance.BorderAlpha = Alpha.Transparent;
                //item.Header.Appearance.BackColor = Color.FromArgb(227, 239, 255);
                //item.Header.Appearance.ForeColor = Color.Black;
                item.Width = temp.ColumnWidth == -1 ? item.Width : temp.ColumnWidth;
                item.MaxWidth = temp.ColumnMaxWidth == -1 ? item.MaxWidth : temp.ColumnMaxWidth;
                item.MinWidth = temp.ColumnMinWidth == -1 ? item.MinWidth : temp.ColumnMinWidth;
                item.Hidden = !temp.IsVisibleCbb;
                item.Header.VisiblePosition = temp.VisiblePosition == -1 ? item.Header.VisiblePosition : temp.VisiblePosition;
                if (item.Hidden == false && item.Key.Contains("Quantity"))
                    FormatNumberic(item, ConstDatabase.Format_Quantity);
            }
            foreach (var item in ultraDropDown.Rows)
            {
                if (item.Cells[nameParentId].Value == null)
                    filter.CellsWithPadding.Add(ultraDropDown.Rows[item.Index].Cells[ultraDropDown.DisplayMember], 0);
                else
                {
                    if ((bool)item.Cells[nameParentNode].Value)
                    {
                        item.Appearance.FontData.Bold = DefaultableBoolean.True;
                        filter.CellsWithPadding.Add(ultraDropDown.Rows[item.Index].Cells[ultraDropDown.DisplayMember], 0);
                    }
                    else
                        filter.CellsWithPadding.Add(ultraDropDown.Rows[item.Index].Cells[ultraDropDown.DisplayMember], 20);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="utralGrid"></param>
        /// <param name="nameTable"></param>
        /// <param name="isRowSelect">Nếu không phải là Grid danh sách thì set = false</param>
        /// <param name="isFilterForm"></param>
        public static void ConfigGrid(this UltraGrid utralGrid, string nameTable, bool isRowSelect = true, bool isFilterForm = false)
        {
            ConfigGrid(utralGrid, nameTable, new List<TemplateColumn>(), false, true, isFilterForm);

            if (!isRowSelect) return;
            utralGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            utralGrid.DisplayLayout.UseFixedHeaders = false;
            utralGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            UltraGridBand band = utralGrid.DisplayLayout.Bands[0];
            band.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            //band.Override.SelectedRowAppearance.BackColor = Color.FromArgb(255, 192, 128);
            //band.Override.SelectedRowAppearance.ForeColor = Color.Black;
            foreach (UltraGridColumn column in band.Columns)
            {
                if (column.Key.Contains("Date"))
                    column.CellActivation = Activation.NoEdit;
                if (column.DataType.IsNumericType())
                    column.FilterOperatorDefaultValue = FilterOperatorDefaultValue.Equals;
                else
                    column.FilterOperatorDefaultValue = FilterOperatorDefaultValue.Contains;
            }
            if (isFilterForm) return;
            if (utralGrid.DisplayLayout.Bands[0].Columns.Exists("Recorded"))
            {
                foreach (var item in utralGrid.Rows)
                {
                    if ((bool)item.Cells["Recorded"].Value)
                        item.Appearance.ForeColor = DefaultRowSelectedColor;
                    else if (!(bool)item.Cells["Recorded"].Value)
                        item.Appearance.ForeColor = GetColorNotRecorded();
                }
            }
        }

        /// <summary>
        /// Cấu hình hiển thị cho UltraGrid
        /// </summary>
        /// <param name="utralGrid">Grid đang thao tác</param>
        /// <param name="nameTable">Tên bảng đang thao tác</param>
        /// <param name="configGui">List các TemplateColumn load từ cơ sở dữ liệu</param>
        /// <param name="loaiGrid">0: loại Grid động| khác 0: các loại Grid khác (do việc config Grid động và các Grid khác nó khác nhau)</param>
        public static void ConfigGrid(this UltraGrid utralGrid, string nameTable, IList<TemplateColumn> configGui, int loaiGrid)
        {
            ConfigGrid(utralGrid, nameTable, configGui);
        }

        /// <summary>
        /// Cấu hình hiển thị cho UltraGrid
        /// </summary>
        /// <param name="utralGrid">Grid đang thao tác</param>
        /// <param name="nameTable">Tên bảng đang thao tác</param>
        /// <param name="configGui">List các TemplateColumn load từ cơ sở dữ liệu</param>
        /// <param name="isStandard"> </param>
        /// <param name="isFilterForm"> </param>
        /// <param name="isConfigGuiManually">True: IList-TemplateColumn configGui Tự sinh truyền vào, False: mặc định configGui từ database</param>
        /// <param name="isBusiness"> </param>
        /// <param name="isHaveSum">Có dòng tổng số dòng không</param>
        public static void ConfigGrid(this UltraGrid utralGrid, string nameTable, IList<TemplateColumn> configGui, bool isBusiness = false, bool isStandard = true, bool isFilterForm = false, bool isConfigGuiManually = false, bool isHaveSum = true)  //, bool isRowSelect = false
        {
            configGui = configGui ?? new List<TemplateColumn>();

            //utralGrid.DisplayLayout.Bands[0].Override.SelectedRowAppearance.ForeColor = Color.Black;
            //Hiện những dòng trống?
            utralGrid.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            utralGrid.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.ExtendFirstCell;
            utralGrid.DisplayLayout.Override.ActiveRowAppearance.ForeColor = DefaultRowSelectedColor;
            utralGrid.DisplayLayout.Override.SelectedRowAppearance.ForeColor = DefaultRowSelectedColor;
            //tắt tiêu đề
            utralGrid.DisplayLayout.CaptionVisible = DefaultableBoolean.False;
            utralGrid.DisplayLayout.BorderStyle = UIElementBorderStyle.None;

            utralGrid.DisplayLayout.Override.AllowAddNew = AllowAddNew.No;
            #region config nếu là Grid nghiệp vụ
            if (isBusiness)
            {
                //hiển thị 1 band
                utralGrid.DisplayLayout.ViewStyle = ViewStyle.SingleBand;
                //tắt lọc cột
                utralGrid.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.False;
                utralGrid.DisplayLayout.Override.FilterUIType = FilterUIType.Default;
                //tự thay đổi kích thước cột
                //utralGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
                //select cả hàng hay ko?
                utralGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
                utralGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
                //Hiển thị SupportDataErrorInfo
                utralGrid.DisplayLayout.Override.SupportDataErrorInfo = SupportDataErrorInfo.CellsOnly;

                utralGrid.DisplayLayout.Bands[0].Summaries.Clear();

                if (isStandard)
                {
                    utralGrid.DisplayLayout.Override.AllowAddNew = AllowAddNew.TemplateOnBottom;
                    utralGrid.DisplayLayout.Override.TemplateAddRowPrompt = resSystem.MSG_System_07;
                    utralGrid.DisplayLayout.Override.SpecialRowSeparator = SpecialRowSeparator.None;
                    //Fix Header
                    utralGrid.DisplayLayout.UseFixedHeaders = true;
                    // Turn on all of the Cut, Copy, and Paste functionality. 
                    utralGrid.DisplayLayout.Override.AllowMultiCellOperations = AllowMultiCellOperation.All;
                }
            }
            #endregion

            //Convert configGUI to dic
            var dicconfigGui = new Dictionary<string, TemplateColumn>();
            if (configGui.Count > 0)
            {
                foreach (var item in configGui)
                {
                    if (!dicconfigGui.ContainsKey(item.ColumnName)) dicconfigGui.Add(item.ColumnName, item);
                    else
                    {
                        Clipboard.SetText(item.ColumnName);
                        throw new Exception(string.Format("Tên cột {0} bị trùng khi cấu hình [ {1} ] Đề nghị xem lại", item.ColumnName, utralGrid.Name));
                    }
                }
            }

            #region setting ResourceCustomizer
            Infragistics.Shared.ResourceCustomizer rc = Infragistics.Win.UltraWinGrid.Resources.Customizer;
            rc.SetCustomizedString("RowFilterDropDownCustomItem", "(Tùy chọn)");
            rc.SetCustomizedString("RowFilterDropDownBlanksItem", "(Trống)");
            rc.SetCustomizedString("RowFilterDropDownNonBlanksItem", "(Không trống)");
            rc.SetCustomizedString("RowFilterDropDownEquals", "Bằng");
            rc.SetCustomizedString("RowFilterDropDownNotEquals", "Khác");
            rc.SetCustomizedString("RowFilterDropDownLessThan", "Nhỏ hơn");
            rc.SetCustomizedString("RowFilterDropDownLessThanOrEqualTo", "Nhỏ hơn hoặc bằng");
            rc.SetCustomizedString("RowFilterDropDownGreaterThan", "Lớn hơn");
            rc.SetCustomizedString("RowFilterDropDownGreaterThanOrEqualTo", "Lớn hơn hoặc bằng");
            rc.SetCustomizedString("RowFilterDropDownMatch", "Thỏa mãn biểu thức");
            rc.SetCustomizedString("RowFilterDropDownLike", "Giống");
            rc.SetCustomizedString("RowFilterDropDown_Operator_StartsWith", "Bắt đầu với");
            rc.SetCustomizedString("RowFilterDropDown_Operator_Contains", "Chứa");
            rc.SetCustomizedString("RowFilterDropDown_Operator_EndsWith", "Kết thúc với");
            rc.SetCustomizedString("RowFilterDropDown_Operator_DoesNotStartWith", "Không bắt đầu với");
            rc.SetCustomizedString("RowFilterDropDown_Operator_DoesNotContain", "Không chứa");
            rc.SetCustomizedString("RowFilterDropDown_Operator_DoesNotEndWith", "Không kết thúc với");
            rc.SetCustomizedString("RowFilterDropDown_Operator_DoesNotMatch", "Không chứa(chuỗi)");
            rc.SetCustomizedString("RowFilterDropDown_Operator_NotLike", "Không giống");
            #endregion

            //Tiêu đề
            utralGrid.Text = nameTable;
            utralGrid.DisplayLayout.UseFixedHeaders = true;
            utralGrid.DisplayLayout.ScrollStyle = ScrollStyle.Immediate;
            utralGrid.DisplayLayout.Scrollbars = Scrollbars.Automatic;
            utralGrid.DisplayLayout.MaxColScrollRegions = 1;
            utralGrid.DisplayLayout.MaxRowScrollRegions = 1;
            utralGrid.DisplayLayout.ViewStyle = ViewStyle.SingleBand;
            UltraGridBand band = utralGrid.DisplayLayout.Bands[0];

            //Border các ô các dòng
            //band.Override.BorderStyleRow = UIElementBorderStyle.WindowsVista;
            //band.Override.BorderStyleCell = UIElementBorderStyle.WindowsVista;


            ////Tiêu đề cột
            string headerCaptionFirst = string.Empty;
            var dicVisiblePosition = new Dictionary<int, int>();   //[vị trí trong list band.Columns] - [vị trí VisiblePosition]
            var templateColumns = new List<TemplateColumn>();
            foreach (var item in band.Columns)
            {
                //Set màu Header
                item.Header.Appearance.BorderAlpha = Alpha.Transparent;
                if (dicconfigGui.Count > 0 ? dicconfigGui.ContainsKey(item.Key) : ConstDatabase.TablesInDatabase[nameTable].ContainsKey(item.Key))
                {
                    TemplateColumn temp = dicconfigGui.Count > 0 ? dicconfigGui[item.Key] : ConstDatabase.TablesInDatabase[nameTable][item.Key];
                    templateColumns.Add(temp);
                    item.Header.Caption = temp.ColumnCaption;
                    item.Header.ToolTipText = temp.ColumnToolTip;
                    //item.Header.Enabled = !temp.IsReadOnly;
                    item.Header.Fixed = temp.IsColumnHeader;
                    item.CellActivation = temp.IsReadOnly ? Activation.NoEdit : Activation.AllowEdit;
                    item.CellAppearance.BackColor = temp.IsReadOnly && isStandard ? Color.FromArgb(225, 225, 225) : Color.Transparent;
                    item.CellAppearance.BorderColor = temp.IsReadOnly && isStandard ? Color.White : Color.FromArgb(218, 220, 221);
                    item.Width = temp.ColumnWidth == -1 ? item.Width : temp.ColumnWidth;
                    item.MaxWidth = temp.ColumnMaxWidth == -1 ? item.MaxWidth : temp.ColumnMaxWidth;
                    item.MinWidth = temp.ColumnMinWidth == -1 ? item.MinWidth : temp.ColumnMinWidth;
                    item.Hidden = !isFilterForm ? !temp.IsVisible : !temp.IsVisibleCbb;
                    if (item.Hidden && (isConfigGuiManually || configGui.Count == 0)) continue; //(đến đoạn xét vị trí thì bỏ qua nếu colum đó là colum ẩn => chỉ xét vị trí cho những colum hiển thị)
                    int vitri = temp.VisiblePosition == -1 ? item.Header.VisiblePosition : temp.VisiblePosition;
                    dicVisiblePosition.Add(vitri, item.Index);
                }
                else
                    item.Hidden = true;
                if (item.Key.Contains("Date") && !item.Key.Equals("FinalDate"))
                {
                    item.Format = Constants.DdMMyyyy;
                    item.MaskInput = @"{LOC}dd/mm/yyyy";
                    item.CellAppearance.TextHAlign = HAlign.Center;
                    item.CellAppearance.TextVAlign = VAlign.Middle;
                }
            }
            //xét vị trí xuất hiện cho các cột hiển thị (Việc gán vị trí phải được lần lượt từ vị trí đầu tiên đến vị trí cuối cùng do đó ta phải sắp xếp lại vị trí hiển thị rồi mới set lần lượt)
            foreach (var item in (from pair in dicVisiblePosition orderby pair.Key ascending select pair)) band.Columns[item.Value].Header.VisiblePosition = item.Key;


            //Tính tổng hàng
            //List<TemplateColumn> templateColumns = dicconfigGui.Count > 0 ? dicconfigGui.Values.ToList() : ConstDatabase.TablesInDatabase[nameTable].Values.ToList();
            var list = templateColumns.Where(p => p.IsVisible).OrderBy(p => p.VisiblePosition).ToList();
            if (list.Count > 0)
            {
                var firstOrDefault = list.FirstOrDefault();
                if (firstOrDefault != null) headerCaptionFirst = firstOrDefault.ColumnName;
                utralGrid.DisplayLayout.Override.AllowRowSummaries = AllowRowSummaries.Default; //Ẩn ký hiệu tổng trên Header Caption
                if (band.Summaries.Count != 0) band.Summaries.Clear();
                if (isHaveSum)
                {
                    SummarySettings summary = band.Summaries.Add("Count", SummaryType.Count, band.Columns[headerCaptionFirst]);
                    summary.DisplayFormat = "Số dòng = {0:N0}";
                    summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
                    summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed | SummaryDisplayAreas.GroupByRowsFooter;
                }
            }

            utralGrid.DisplayLayout.Override.GroupBySummaryDisplayStyle = GroupBySummaryDisplayStyle.SummaryCells;
            utralGrid.DisplayLayout.Bands[0].SummaryFooterCaption = @"Số dòng dữ liệu: ";
            utralGrid.DisplayLayout.Override.SummaryFooterAppearance.FontData.Bold = DefaultableBoolean.True;
            utralGrid.DisplayLayout.Override.SummaryFooterCaptionVisible = DefaultableBoolean.False;   //ẩn
            utralGrid.DisplayLayout.Override.CellAppearance.TextTrimming = TextTrimming.EllipsisWord;
        }

        /// <summary>
        /// Hàm add combo vào Grid có Autosearch cho Form thường
        /// [Huy Anh]
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="this"> </param>
        /// <param name="uGrid">UltraGrid chứa combo</param>
        /// <param name="columnName">Tên Cột gắn combo</param>  
        /// <param name="inputItem">DS truyền vào combo</param>
        /// <param name="valueMember">ValueMember</param>
        /// <param name="displayMember">DisplayMember</param>
        /// <param name="nameTable">Tên bảng</param>
        /// <param name="nameParentId">Tên cột chứa parentID</param>
        /// <param name="nameParentNode">Tên cột xác định có phải là cha hay ko</param>
        /// <param name="accountingObjectType">Loại đối tượng AccountingObject: 0-Khách hàng. 1-Nhà cung cấp. 2-Nhân viên. 3-KH và NCC. [Mặc định là lấy đầy đủ ds]</param>
        public static void ConfigCbbToGrid<T>(this Form @this, UltraGrid @uGrid, string @columnName, IList<T> @inputItem, string @valueMember,
                                              string @displayMember, string @nameTable, string nameParentId = null,
                                              string @nameParentNode = null, int? accountingObjectType = null)
        {
            UltraGridColumn @ugc = @uGrid.DisplayLayout.Bands[0].Columns[@columnName];
            @ugc.ValueList = @this.GetValueList(@uGrid, @inputItem, @valueMember, @displayMember, @nameTable, nameParentId,
                                                @nameParentNode, accountingObjectType);
            if (typeof(T) == typeof(AccountingObject) || typeof(T) == typeof(MaterialGoodsCustom))
            {
                @ugc.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.None;
            }
            else
            {
                @ugc.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.Default;
                @ugc.AutoSuggestFilterMode = Infragistics.Win.AutoSuggestFilterMode.Contains;
            }
        }
        /// <summary>
        /// Hàm add combo vào Grid có Autosearch
        /// [Huy Anh]
        /// </summary>    
        /// <typeparam name="TK">Đối tượng của Form</typeparam>
        /// <typeparam name="T"></typeparam>
        /// <param name="this"></param>
        /// <param name="uGrid">UltraGrid chứa combo</param>
        /// <param name="columnName">Tên Cột gắn combo</param> 
        /// <param name="inputItem">DS truyền vào combo</param>
        /// <param name="valueMember">ValueMember</param>
        /// <param name="displayMember">DisplayMember</param>
        /// <param name="nameTable">Tên bảng</param>
        /// <param name="nameParentId">Tên cột chứa parentID</param>
        /// <param name="nameParentNode">Tên cột xác định có phải là cha hay ko</param>
        /// <param name="accountingObjectType">Loại đối tượng AccountingObject: 0-Khách hàng. 1-Nhà cung cấp. 2-Nhân viên. 3-KH và NCC. [Mặc định là lấy đầy đủ ds]</param>
        public static void ConfigCbbToGrid<TK, T>(this Form @this, UltraGrid @uGrid, string @columnName, IList<T> @inputItem, string @valueMember,
                                              string @displayMember, string @nameTable, string nameParentId = null,
                                              string @nameParentNode = null, int? accountingObjectType = null)
        {
            UltraGridColumn @ugc = @uGrid.DisplayLayout.Bands[0].Columns[@columnName];
            @ugc.ValueList = @this.GetValueList<TK, T>(@uGrid, @inputItem, @valueMember, @displayMember, @nameTable,
                                                       nameParentId,
                                                       @nameParentNode, accountingObjectType);
            @ugc.Tag = typeof(T);
            if (typeof(T) == typeof(AccountingObject) || typeof(T) == typeof(MaterialGoodsCustom))
            {
                @ugc.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.None;
            }
            else
            {
                @ugc.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.Default;
                @ugc.AutoSuggestFilterMode = Infragistics.Win.AutoSuggestFilterMode.Contains;
            }
        }


        /// <summary>
        /// Thêm cột tổng vào một được chỉ định trong grid
        /// </summary>
        /// <param name="uGrid">UGrid cần thêm</param>
        /// <param name="keyColumn"></param>
        /// <param name="left">bên trái = true, bên phải = flase</param>
        /// <param name="displayFormat">kết quả cần gán</param>
        /// <param name="constDatabaseFormat">mặc định = 0, chọn kiểu hiển thị tương ứng trong ConstDatabase </param>
        /// <param name="isMinus">mặc định true - hiển thị kiểu số dương, false hiển thị kiểu số âm</param>
        /// <returns>nếu keyColumn có tồn tại trả về true, không tồn tại trả về false </returns>
        public static bool AddSumColumn(UltraGrid uGrid, string keyColumn, bool left = true, string displayFormat = "", int constDatabaseFormat = 0, bool isMinus = true)
        {
            string keySum = keyColumn;
            do
            {
                keySum += "Sum";
            } while (uGrid.DisplayLayout.Bands[0].Summaries.Exists(keySum));
            if (uGrid.DisplayLayout.Bands[0].Columns.Exists(keyColumn))
            {
                SummarySettings summary = uGrid.DisplayLayout.Bands[0].Summaries.Add(keySum, SummaryType.Sum, uGrid.DisplayLayout.Bands[0].Columns[keyColumn]);
                summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;

                if (!string.IsNullOrEmpty(displayFormat))
                {
                    summary.DisplayFormat = displayFormat;
                }
                else if (constDatabaseFormat == 0)
                {
                    summary.DisplayFormat = "{0:N0}";
                }
                else if (constDatabaseFormat != 0)
                {
                    string format = GetTypeFormatNumberic(constDatabaseFormat);
                    string s = "{0:" + format;
                    if (!isMinus) s += ";(" + format + ")}";
                    else s += "}";
                    summary.DisplayFormat = s;
                }

                summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed | SummaryDisplayAreas.GroupByRowsFooter;
                summary.Appearance.TextHAlign = left ? HAlign.Left : HAlign.Right;
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Cấu hình Grid chính theo Template
        /// <summary>
        /// Cấu hình giao diện demo cho phần Mẫu giao diện
        /// </summary>
        /// <param name="panel"></param>
        /// <param name="template"></param>
        /// <param name="isCatalog">Có phải Form Catalog không</param>
        public static void ConfigGridByTemplete_General(UltraPanel panel, Template template, bool isCatalog = false)
        {
            #region Cấu hình chung
            if (template != null)
            {
                List<TemplateDetail> listTabs = template.TemplateDetails.Where(p => p.TabIndex != 100).OrderBy(p => p.TabIndex).ToList();
                if (listTabs.Count > 1 && !isCatalog)
                {
                    //ultraTabControlGrid.Visible = false;
                    var tabControl = new UltraTabControl();
                    tabControl.SuspendLayout();
                    foreach (TemplateDetail detail in listTabs)
                    {
                        UltraTab ultraTab = tabControl.Tabs.Add();
                        var uGrid = new UltraGrid { Name = "uGrid" + detail.TabIndex };
                        uGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ExtendLastColumn;
                        uGrid.Dock = DockStyle.Fill;
                        uGrid.Text = string.Format(@"uGrid{0}", detail.TabIndex);
                        uGrid.TabIndex = 0;
                        ultraTab.TabPage.Controls.Add(uGrid);
                        ultraTab.Text = detail.TabCaption;
                        ultraTab.Key = "uTab" + detail.TabIndex;
                    }

                    tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
                    tabControl.Name = "ultraTabControl";
                    tabControl.TabIndex = 0;
                    tabControl.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Flat;
                    tabControl.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007;
                    tabControl.DrawFilter = new DrawFilter();

                    panel.ClientArea.Controls.Add(tabControl);
                    ((System.ComponentModel.ISupportInitialize)(tabControl)).EndInit();
                }
                else if (listTabs.Count == 1 || isCatalog)
                {
                    var uGrid = new UltraGrid { Name = "uGrid0" };
                    uGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ExtendLastColumn;
                    uGrid.Dock = DockStyle.Fill;
                    uGrid.Text = @"uGrid";
                    uGrid.TabIndex = 0;
                    panel.ClientArea.Controls.Add(uGrid);
                }
            }
            #endregion
        }

        /// <summary>
        /// Cấu hình tự động các Grid theo mẫu giao diện và typeID. 
        /// </summary>
        /// <typeparam name="TK">Đối tượng Form</typeparam>
        /// <param name="this"> </param>
        /// <param name="panel"></param>
        /// <param name="template"></param>
        public static void ConfigGridByTemplete_General<TK>(this Form @this, UltraPanel panel, Template template, System.Type[] blackStandList = null)
        {
            #region Cấu hình chung
            if (template == null) return;
            var tabs =
                template.TemplateDetails.Where(p => p.TabIndex != 100).OrderBy(p => p.TabIndex).ToList();
            if (tabs.Count > 1)
            {
                //ultraTabControlGrid.Visible = false;
                var ultraTabControl = new UltraTabControl();
                ultraTabControl.SuspendLayout();
                foreach (TemplateDetail detail in tabs)
                {
                    UltraTab ultraTab = ultraTabControl.Tabs.Add();
                    var uGrid = new UltraGrid { Name = "uGrid" + detail.TabIndex };
                    uGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ExtendLastColumn;
                    #region Event
                    uGrid.BeforeRowActivate += new RowEventHandler((s, e) => @this.uGrid_BeforeRowActivate<TK>(s, e, blackStandList));
                    uGrid.CellChange += new CellEventHandler(@this.uGrid_CellChange<TK>);
                    uGrid.AfterCellUpdate += new CellEventHandler(@this.uGrid_AfterCellUpdate<TK>);
                    uGrid.KeyPress += new KeyPressEventHandler(@this.uGrid_KeyPress<TK>);
                    uGrid.AfterExitEditMode += new EventHandler(@this.uGrid_AfterExitEditMode<TK>);
                    uGrid.CellDataError += new CellDataErrorEventHandler(uGrid_CellDataError);
                    uGrid.InitializeLayout += new InitializeLayoutEventHandler(uGrid_InitializeLayout);
                    uGrid.InitializeRow += new InitializeRowEventHandler(@this.uGrid_InitializeRow<TK>);
                    uGrid.AfterCellActivate += new EventHandler(@this.uGrid_AfterCellActivate<TK>);
                    uGrid.KeyDown += new KeyEventHandler(@this.uGrid_KeyDown<TK>);
                    uGrid.Validated += new EventHandler(uGrid_Validated);
                    uGrid.SummaryValueChanged += new SummaryValueChangedEventHandler(@this.uGrid_SummaryValueChanged<TK>);
                    #endregion
                    uGrid.Dock = DockStyle.Fill;
                    uGrid.Text = string.Format(@"uGrid{0}", detail.TabIndex);
                    var @base = @this as DetailBase<TK>;
                    if (@base != null) uGrid.ContextMenuStrip = @base.cms4Grid;
                    uGrid.TabIndex = detail.TabIndex;
                    ultraTab.TabPage.Controls.Add(uGrid);
                    ultraTab.Text = detail.TabCaption;
                    ultraTab.ToolTipText = detail.TabCaption;
                    ultraTab.TabPage.Padding = new Padding { Top = 10 };
                    ultraTab.Key = "uTab" + detail.TabIndex;
                }

                ultraTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
                ultraTabControl.Name = "ultraTabControl";
                ultraTabControl.TabIndex = 0;
                ultraTabControl.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Flat;
                //ultraTabControl.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007;
                //ultraTabControl.Appearance.BorderColor = Color.DodgerBlue;
                //ultraTabControl.Appearance.FontData.Bold = DefaultableBoolean.True;
                ultraTabControl.DrawFilter = new DrawFilter();
                ultraTabControl.KeyUp += new KeyEventHandler(@this.ultraTabControl_KeyUp);
                panel.ClientArea.Controls.Add(ultraTabControl);
                ((System.ComponentModel.ISupportInitialize)(ultraTabControl)).EndInit();
            }
            else if (tabs.Count == 1)
            {
                var uGrid = new UltraGrid { Name = "uGrid0" };
                uGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ExtendLastColumn;
                #region Event
                uGrid.BeforeRowActivate += new RowEventHandler((s, e) => @this.uGrid_BeforeRowActivate<TK>(s, e, blackStandList));
                uGrid.CellChange += new CellEventHandler(@this.uGrid_CellChange<TK>);
                uGrid.AfterCellUpdate += new CellEventHandler(@this.uGrid_AfterCellUpdate<TK>);
                uGrid.KeyPress += new KeyPressEventHandler(@this.uGrid_KeyPress<TK>);
                uGrid.AfterExitEditMode += new EventHandler(@this.uGrid_AfterExitEditMode<TK>);
                uGrid.CellDataError += new CellDataErrorEventHandler(uGrid_CellDataError);
                uGrid.InitializeLayout += new InitializeLayoutEventHandler(uGrid_InitializeLayout);
                uGrid.InitializeRow += new InitializeRowEventHandler(@this.uGrid_InitializeRow<TK>);
                uGrid.AfterCellActivate += new EventHandler(@this.uGrid_AfterCellActivate<TK>);
                uGrid.KeyDown += new KeyEventHandler(@this.uGrid_KeyDown<TK>);
                uGrid.KeyUp += uGrid_KeyUp;
                uGrid.Validated += new EventHandler(uGrid_Validated);
                uGrid.SummaryValueChanged += new SummaryValueChangedEventHandler(@this.uGrid_SummaryValueChanged<TK>);
                #endregion
                uGrid.Dock = DockStyle.Fill;
                uGrid.Text = @"uGrid";
                var @base = @this as DetailBase<TK>;
                if (@base != null) uGrid.ContextMenuStrip = @base.cms4Grid;
                uGrid.TabIndex = 0;
                panel.ClientArea.Controls.Add(uGrid);
            }

            #endregion
        }

        static void ultraTabControl_KeyUp(this Form @this, object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Tab || e.KeyData == Keys.Menu)
            {
                var uTabControl = (UltraTabControl)sender;
                var uGrid = (UltraGrid)uTabControl.Tabs.TabControl.Controls.Find("uGrid" + uTabControl.ActiveTab.Index, true).FirstOrDefault();
                if (uGrid != null)
                {
                    if (uGrid.Rows.Count == 0)
                        uGrid.DisplayLayout.Bands[0].AddNew();
                    var columns = uGrid.DisplayLayout.Bands[0].Columns.Cast<UltraGridColumn>().ToList();
                    var fisrtCol = columns.OrderBy(p => p.Header.VisiblePosition).FirstOrDefault(p => !p.Hidden);
                    if (fisrtCol != null)
                    {
                        uGrid.ActiveCell = uGrid.Rows[0].Cells[fisrtCol.Key];
                        uGrid.PerformAction(UltraGridAction.EnterEditMode, false, false);
                    }
                }
            }
        }

        static void uGrid_InitializeRow<TK>(this Form @this, object sender, InitializeRowEventArgs e)
        {
            var uGrid = (UltraGrid)sender;
            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("UnitPriceOriginal"))
            {
                var @base = @this as DetailBase<TK>;
                if (SaleTypeGroup.Any(p => @base != null && p.Equals(@base.TypeGroup)))
                {
                    UltraGridCell cell = e.Row.Cells["UnitPriceOriginal"];
                    var mask = new EditorWithMask();
                    SetDropDownWithMaskInput(mask, cell);
                    cell.Editor = mask;
                }
            }
        }

        public static void ConfigGridByTempleteList(this Form @this, int typeId, Template template, bool isBusiness = true, List<IList> listBindingObject = null, bool isStandard = true, bool isCatalog = false)
        {
            if (template == null) return;
            var dsTab = template.TemplateDetails.Where(p => p.TabIndex != 100).OrderBy(p => p.TabIndex).ToList();
            for (int i = 0; i < dsTab.Count; i++)
            {
                var uGrid = (UltraGrid)@this.Controls.Find("uGrid" + i, true).FirstOrDefault();
                var templateColumns =
                    template.TemplateDetails.Single(k => k.TabIndex == i).TemplateColumns;
                @this.ConfigGridData(typeId, uGrid, listBindingObject, templateColumns);
                ConfigGrid(uGrid, ConstDatabase.GOtherVoucherDetail_TableName, templateColumns, isBusiness, isStandard);
                if (isCatalog && i == 0) return;
            }
        }

        /// <summary>
        /// Cấu hình theo Template cho giao diện demo cho phần Mẫu giao diện
        /// </summary>
        /// <param name="this"></param>
        /// <param name="typeId"></param>
        /// <param name="template"></param>
        /// <param name="isBusiness"></param>
        /// <param name="listBindingObject"></param>
        /// <param name="isStandard"></param>
        /// <param name="isCatalog">Có phải Form Catalog không</param>
        public static void ConfigGridByTemplete(this Form @this, int typeId, Template template, bool isBusiness = true, List<IList> listBindingObject = null, bool isStandard = true, bool isCatalog = false)
        {
            if (template == null) return;
            List<TemplateDetail> dsTab =
                template.TemplateDetails.Where(p => p.TabIndex != 100).OrderBy(p => p.TabIndex).ToList();
            for (int i = 0; i < dsTab.Count; i++)
            {
                var uGrid = (UltraGrid)@this.Controls.Find("uGrid" + i, true).FirstOrDefault();
                if (uGrid == null) continue;
                var templateColumns =
                    template.TemplateDetails.Single(k => k.TabIndex == i).TemplateColumns;
                @this.ConfigGridData(typeId, uGrid, listBindingObject, templateColumns);
                ConfigGrid(uGrid, "", templateColumns, isBusiness, isStandard);
                if (!isCatalog || i != 0) continue;
                foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
                {
                    column.CellActivation = Activation.NoEdit;
                }
                return;
            }
        }

        /// <summary>
        /// Cấu hình Grid theo Template
        /// </summary>
        /// <typeparam name="TK">Đối tượng của Form</typeparam>
        /// <param name="this"></param>
        /// <param name="typeId"></param>
        /// <param name="template"></param>
        /// <param name="isBusiness"></param>
        /// <param name="listBindingObject"></param>
        /// <param name="isStandard"></param>
        /// <param name="isCatalog">Có phải Form Catalog không</param>
        /// <param name="isForeignCurrency"> </param>
        /// <param name="blackStandList">DS type cua DataSource của grid không phải là grid cho thêm dòng </param>
        public static void ConfigGridByTemplete<TK>(this Form @this, int typeId, Template template, bool isBusiness = true, List<IList> listBindingObject = null, bool isStandard = true, bool isCatalog = false, bool isForeignCurrency = false, System.Type[] blackStandList = null)
        {
            if (template != null)
            {
                List<TemplateDetail> tabs =
                    template.TemplateDetails.Where(p => p.TabIndex != 100).OrderBy(p => p.TabIndex).ToList();
                if (!isCatalog)
                {
                    var @base = @this as DetailBase<TK>;
                    if (@base != null) @base._isStandardGrid.Clear();
                }
                decimal totalVat133 = 0;
                decimal totalVat3331 = 0;
                decimal totalVat = 0;
                for (int i = 0; i < tabs.Count; i++)
                {
                    var uGrid = (UltraGrid)@this.Controls.Find("uGrid" + i, true).FirstOrDefault();
                    if (uGrid == null) continue;
                    var band = uGrid.DisplayLayout.Bands[0];
                    var templateColumns =
                        template.TemplateDetails.Single(k => k.TabIndex == i).TemplateColumns;
                    #region Lấy DataSource cho vào Grid
                    @this.ConfigGridData(typeId, uGrid, listBindingObject, templateColumns);
                    #endregion
                    #region Cấu hình giao diện cho Grid
                    if (blackStandList != null && blackStandList.Any(x => x == uGrid.DataSource.GetType()))
                    {
                        ConfigGrid(uGrid, string.Format("{0}Detail", typeof(TK).Name), templateColumns, isBusiness, false);
                        uGrid.DisplayLayout.Override.AllowAddNew = AllowAddNew.No;
                    }
                    else
                        ConfigGrid(uGrid, string.Format("{0}Detail", typeof(TK).Name), templateColumns, isBusiness, isStandard);
                    //Kiểm tra có cho phép Theo dõi các khoản chi phí không hợp lý hay không
                    if (band.Columns.Exists("IsIrrationalCost"))
                        if (GetIsTrackIrrationalCost())
                        {
                            if (!band.Columns["IsIrrationalCost"].Hidden)
                                band.Columns["IsIrrationalCost"].Hidden = false;
                        }
                        else
                            if (!band.Columns["IsIrrationalCost"].Hidden)
                                band.Columns["IsIrrationalCost"].Hidden = true;

                    #endregion
                    #region Cấu hình chi tiết trong Grid
                    @this.ConfigGridDetail<TK>(typeId, uGrid, isCatalog, isForeignCurrency);
                    if (isCatalog && i == 0)
                    {
                        foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
                        {
                            column.CellActivation = Activation.NoEdit;
                        }
                        return;
                    }
                    var @base = @this as DetailBase<TK>;
                    @base._isStandardGrid.Add(uGrid.Name, isStandard);
                    if (@base._statusForm.Equals(ConstFrm.optStatusForm.Add)) continue;
                    if (!band.Columns.Exists("MaterialGoodsID") && (ListTypeByCreditAccount.Any(p => p.Equals(typeId)) || ListTypeByDebitAccount.Any(p => p.Equals(typeId)) || ListTypeByBothAccount.Any(p => p.Equals(typeId))))
                    {
                        foreach (var row in uGrid.Rows)
                        {
                            if (band.Columns.Exists("DiscountRate") && band.Columns.Exists("DiscountAmount"))
                            {
                                if (ListTypeByCreditAccount.Any(p => p.Equals(typeId)))
                                {
                                    if (band.Columns.Exists("CreditAccount") && row.Cells["CreditAccount"].Value != null)
                                    {
                                        if (row.Cells["CreditAccount"].Value.ToString().StartsWith("133"))
                                        {
                                            totalVat133 += (row.Cells["Amount"].Value == null
                                                                ? 0
                                                                : (decimal)row.Cells["Amount"].Value) -
                                                           (row.Cells["DiscountAmount"].Value == null
                                                                ? 0
                                                                : (decimal)row.Cells["DiscountAmount"].Value);
                                        }
                                        else if (row.Cells["CreditAccount"].Value.ToString().StartsWith("3331"))
                                        {
                                            totalVat3331 += row.Cells["Amount"].Value == null
                                                                ? 1
                                                                : (decimal)row.Cells["Amount"].Value;
                                        }
                                    }
                                }
                                else if (ListTypeByDebitAccount.Any(p => p.Equals(typeId)))
                                {
                                    if (band.Columns.Exists("DebitAccount") && row.Cells["DebitAccount"].Value != null)
                                    {
                                        if (row.Cells["DebitAccount"].Value.ToString().StartsWith("133"))
                                        {
                                            totalVat133 += (row.Cells["Amount"].Value == null
                                                                ? 0
                                                                : (decimal)row.Cells["Amount"].Value) -
                                                           (row.Cells["DiscountAmount"].Value == null
                                                                ? 0
                                                                : (decimal)row.Cells["DiscountAmount"].Value);
                                        }
                                        else if (row.Cells["DebitAccount"].Value.ToString().StartsWith("3331"))
                                        {
                                            totalVat3331 += row.Cells["Amount"].Value == null
                                                                ? 1
                                                                : (decimal)row.Cells["Amount"].Value;
                                        }
                                    }
                                }
                                else if (ListTypeByBothAccount.Any(p => p.Equals(typeId)))
                                {
                                    if ((band.Columns.Exists("DebitAccount") && band.Columns.Exists("CreditAccount") && (row.Cells["DebitAccount"].Value != null || row.Cells["CreditAccount"].Value != null)))
                                    {
                                        if ((row.Cells["DebitAccount"].Value != null && (row.Cells["DebitAccount"].Value.ToString().StartsWith("133") ||
                                                                                         row.Cells["DebitAccount"].Value.ToString().StartsWith("3331"))) || (row.Cells["CreditAccount"].Value != null && (row.Cells["CreditAccount"].Value.ToString().StartsWith("133") ||
                                                                                                                                                                                                          row.Cells["CreditAccount"].Value.ToString().StartsWith("3331"))))
                                        {
                                            totalVat += (row.Cells["Amount"].Value == null
                                                             ? 0
                                                             : (decimal)row.Cells["Amount"].Value) -
                                                        (row.Cells["DiscountAmount"].Value == null
                                                             ? 0
                                                             : (decimal)row.Cells["DiscountAmount"].Value);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (ListTypeByCreditAccount.Any(p => p.Equals(typeId)))
                                {
                                    if (band.Columns.Exists("CreditAccount") && row.Cells["CreditAccount"].Value != null)
                                    {
                                        if (row.Cells["CreditAccount"].Value.ToString().StartsWith("133"))
                                        {
                                            totalVat133 += row.Cells["Amount"].Value == null
                                                               ? 1
                                                               : (decimal)row.Cells["Amount"].Value;
                                        }
                                        else if (row.Cells["CreditAccount"].Value.ToString().StartsWith("3331"))
                                        {
                                            totalVat3331 += row.Cells["Amount"].Value == null
                                                                ? 1
                                                                : (decimal)row.Cells["Amount"].Value;
                                        }
                                    }
                                }
                                else if (ListTypeByDebitAccount.Any(p => p.Equals(typeId)))
                                {
                                    if (band.Columns.Exists("DebitAccount") && row.Cells["DebitAccount"].Value != null)
                                    {
                                        if (row.Cells["DebitAccount"].Value.ToString().StartsWith("133"))
                                        {
                                            totalVat133 += row.Cells["Amount"].Value == null
                                                               ? 1
                                                               : (decimal)row.Cells["Amount"].Value;
                                        }
                                        else if (row.Cells["DebitAccount"].Value.ToString().StartsWith("3331"))
                                        {
                                            totalVat3331 += row.Cells["Amount"].Value == null
                                                                ? 1
                                                                : (decimal)row.Cells["Amount"].Value;
                                        }
                                    }
                                }
                                else if (ListTypeByBothAccount.Any(p => p.Equals(typeId)))
                                {
                                    if ((band.Columns.Exists("DebitAccount") && band.Columns.Exists("CreditAccount") && (row.Cells["DebitAccount"].Value != null || row.Cells["CreditAccount"].Value != null)))
                                    {
                                        if ((row.Cells["DebitAccount"].Value != null && (row.Cells["DebitAccount"].Value.ToString().StartsWith("133") ||
                                                                                         row.Cells["DebitAccount"].Value.ToString().StartsWith("3331"))) || (row.Cells["CreditAccount"].Value != null && (row.Cells["CreditAccount"].Value.ToString().StartsWith("133") ||
                                                                                                                                                                                                          row.Cells["CreditAccount"].Value.ToString().StartsWith("3331"))))
                                        {
                                            totalVat += row.Cells["Amount"].Value == null
                                                            ? 1
                                                            : (decimal)row.Cells["Amount"].Value;
                                        }
                                    }
                                }
                            }
                            if (band.Columns.Exists("VATAmount"))
                            {
                                if (ListTypeByCreditAccount.Any(p => p.Equals(typeId)) || ListTypeByDebitAccount.Any(p => p.Equals(typeId)))
                                {
                                    if ((decimal)row.Cells["VATAmount"].Value == totalVat133)
                                        row.Tag = "Vat133";
                                    else if ((decimal)row.Cells["VATAmount"].Value == totalVat3331)
                                        row.Tag = "Vat3331";
                                }
                                else if (ListTypeByBothAccount.Any(p => p.Equals(typeId)))
                                {
                                    if ((decimal)row.Cells["VATAmount"].Value == totalVat)
                                        row.Tag = "Vat";
                                }
                            }
                        }
                    }


                    #region TH là Nhập khẩu
                    if (@base._select.HasProperty("IsImportPurchase"))
                    {
                        if (@base._select.GetProperty<TK, bool?>("IsImportPurchase") == true)
                        {
                            uGrid.ConfigColumnByImportPurchase(true);
                        }
                    }
                    #endregion
                    #endregion
                }
            }
        }

        public static void ConfigGridData(this Form @this, int typeId, UltraGrid @uGridInput, List<IList> listBindingObject, IList<TemplateColumn> templateColumns)
        {
            var correctType = new List<string>();
            IList result = new BindingList<string>() { "Lỗi dữ liệu..." };
            //string nameType = string.Empty;
            var dicObjectDb = ConstDatabase.DicObjectForTemplete();
            var dicObject = new Dictionary<System.Type, IList>();
            foreach (var item in dicObjectDb.Where(item => item.Key.Equals(typeId)))
            {
                dicObject = item.Value;
                break;
            }
            foreach (var temp in dicObject)
            {
                var grid = new UltraGrid();
                @this.Controls.Add(grid);
                grid.DataSource = temp.Value;
                var listColumns = (from UltraGridColumn column in grid.DisplayLayout.Bands[0].Columns select column.Key).ToList();
                var configGui = templateColumns;
                var listColumnTemplete = new List<string>();
                if (configGui.Count > 0) foreach (var item in configGui) listColumnTemplete.Add(item.ColumnName);
                var check = listColumnTemplete.Where(item => !item.StartsWith("Panel") && !item.Contains("Width") && !item.Contains("Height")).All(item => listColumns.Count(p => p.Contains(item)) != 0);
                @this.Controls.Remove(grid);
                if (!check) continue;
                result = temp.Value;
                var typeCurrent = temp.Key;
                @uGridInput.Tag = typeCurrent;
                correctType.Add(typeCurrent.Name);
                //nameType = typeCurrent.Name;
            }
            if (listBindingObject != null)
                foreach (IList o in listBindingObject)
                {
                    string nameObject = o.GetType().FullName.GetObjectNameFromBindingName();
                    if (correctType.Any(x => x == nameObject))
                        result = o;
                }
            @uGridInput.DataSource = result;
        }

        /// <summary>
        /// HÀm Convert từ List sang BindingList
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="bindingList"></param>
        /// <param name="list"></param>
        public static void AddToBindingList<T>(this IList<T> list, BindingList<T> bindingList)
        {
            foreach (T aItem in list)
            {
                bindingList.Add(aItem);
                bindingList.ListChanged += new ListChangedEventHandler(BindingList_ListChanged);
            }
        }

        private static void BindingList_ListChanged(object sender, ListChangedEventArgs e)
        {
            // MessageBox.Show(e.ListChangedType.ToString());
        }

        /// <summary>
        /// Hàm config chung cho từng Grid
        /// </summary>
        /// <typeparam name="TK">đối tượng của Form</typeparam>
        /// <param name="this"></param>
        /// <param name="typeId"></param>
        /// <param name="uGridInput"></param>
        /// <param name="isCatalog">Có phải Form Catalog không</param>
        /// <param name="isForeignCurrency"> </param>
        static void ConfigGridDetail<TK>(this Form @this, int typeId, UltraGrid @uGridInput, bool isCatalog = false, bool isForeignCurrency = false)
        {
            #region Phần chung
            var band = @uGridInput.DisplayLayout.Bands[0];
            var columnAmout = new List<string>();
            foreach (var @column in band.Columns)
            {
                @this.ConfigEachColumn4Grid<TK>(typeId, @column, @uGridInput);
                if ((column.Key.Contains("Amount") && !column.Key.Contains("Original")) || column.Key.Equals("UnitPrice"))
                    if (band.Columns.Exists(column.Key + "Original"))
                        if (!band.Columns[column.Key + "Original"].Hidden)
                            columnAmout.Add(column.Key);
            }
            //Cấu hình các Summary
            ConfigSummaryOfGrid(@uGridInput);
            //Add sự kiện KeyDown cho control trong Grid
            @this.AddKeyDownEventForControls<TK>(@uGridInput);
            if (@uGridInput.DataSource.GetType().Name.Equals(typeof(BindingList<string>)))
            {
                band.HeaderVisible = false;
                band.ColHeadersVisible = false;
                @uGridInput.DisplayLayout.Override.CellAppearance.TextHAlign = HAlign.Center;
            }
            if (isCatalog)
            {
                if (isForeignCurrency || (band.Columns.Exists("CurrencyID") && band.Columns.Exists("ExchangeRate")))
                {
                    if (isForeignCurrency)
                    {
                        foreach (var s in columnAmout.Where(s => !s.Contains("Pretax")))
                        {
                            if (s.Contains("VAT"))
                                if (uGridInput.DisplayLayout.Bands[0].Columns.Exists(s + "Original"))
                                    if (uGridInput.DisplayLayout.Bands[0].Columns[s + "Original"].Hidden)
                                        continue;
                            band.Columns[s].Hidden = false;
                        }
                    }
                    else
                    {
                        if (@uGridInput.Rows.Any(row => !row.Cells["CurrencyID"].Value.ToString().Equals("VND")))
                        {
                            band.Columns["ExchangeRate"].Hidden = false;
                            foreach (var s in columnAmout.Where(s => !s.Contains("Pretax")))
                            {
                                if (s.Contains("VAT"))
                                    if (uGridInput.DisplayLayout.Bands[0].Columns.Exists(s + "Original"))
                                        if (uGridInput.DisplayLayout.Bands[0].Columns[s + "Original"].Hidden)
                                            continue;
                                band.Columns[s].Hidden = false;
                            }
                        }
                    }
                }
                return;
            }
            var @base = @this as DetailBase<TK>;
            if (@base != null && !@base._statusForm.Equals(ConstFrm.optStatusForm.Add))
            {
                if (band.Columns.Exists("CurrencyID") && band.Columns.Exists("ExchangeRate"))
                {
                    if (@uGridInput.Name.Equals("uGridControl"))
                    {

                        if (@uGridInput.Rows[0].Cells["CurrencyID"].Value != null && !@uGridInput.Rows[0].Cells["CurrencyID"].Value.ToString().Equals("VND"))
                        {
                            if (band.Columns.Exists("TotalAmountOriginal") && band.Columns.Exists("TotalAmount") &&
                                band.Columns["TotalAmountOriginal"].Hidden.Equals(false))
                            {
                                band.Columns["TotalAmount"].Hidden = false;
                            }
                            band.Columns["ExchangeRate"].Hidden = false;
                        }
                        if (@uGridInput.Rows[0].Cells["ExchangeRate"].Value == null)
                        {
                            var currency = ListCurrency.FirstOrDefault(p => p.ID == @uGridInput.Rows[0].Cells["CurrencyID"].Value.ToString());
                            if (
                                currency != null)
                                @uGridInput.Rows[0].Cells["ExchangeRate"].Value =
                                    currency.ExchangeRate ?? (decimal)1;
                            @uGridInput.Rows[0].Update();
                        }
                    }
                    else
                    {
                        foreach (UltraGridRow row in @uGridInput.Rows)
                        {
                            if (row.Cells["CurrencyID"].Value != null && !row.Cells["CurrencyID"].Value.ToString().Equals("VND"))
                            {
                                band.Columns["ExchangeRate"].Hidden = false;
                                foreach (var s in columnAmout.Where(s => !s.Contains("Pretax")))
                                {
                                    if (s.Contains("VAT"))
                                        if (uGridInput.DisplayLayout.Bands[0].Columns.Exists(s + "Original"))
                                            if (uGridInput.DisplayLayout.Bands[0].Columns[s + "Original"].Hidden)
                                                continue;
                                    band.Columns[s].Hidden = false;
                                }
                                break;
                            }
                        }
                    }
                }
                else
                {
                    if (columnAmout.Count > 0)
                    {
                        if (band.Columns.Exists(columnAmout[0] + "Original"))
                        {
                            if (@uGridInput.Rows.Count > 0)
                            {
                                var select = @base._select;
                                var propCurrency = select.GetType().GetProperty("CurrencyID");
                                if (propCurrency != null && propCurrency.CanWrite && propCurrency.CanRead)
                                {
                                    string currency = (string)propCurrency.GetValue(select, null);
                                    foreach (var s in columnAmout.Where(s => !s.Contains("Pretax")))
                                    {
                                        if (s.Contains("VAT"))
                                            if (uGridInput.DisplayLayout.Bands[0].Columns.Exists(s + "Original"))
                                                if (uGridInput.DisplayLayout.Bands[0].Columns[s + "Original"].Hidden)
                                                    continue;
                                        @uGridInput.DisplayLayout.Bands[0].Columns[s].Hidden =
                                            currency.Equals("VND");
                                    }
                                }
                                else if (
                                    !@uGridInput.Rows[0].Cells[columnAmout[0]].Value.Equals(
                                        @uGridInput.Rows[0].Cells[columnAmout[0] + "Original"].Value))
                                {
                                    foreach (var s in columnAmout.Where(s => !s.Contains("Pretax")))
                                    {
                                        if (s.Contains("VAT"))
                                            if (uGridInput.DisplayLayout.Bands[0].Columns.Exists(s + "Original"))
                                                if (uGridInput.DisplayLayout.Bands[0].Columns[s + "Original"].Hidden)
                                                    continue;
                                        @uGridInput.DisplayLayout.Bands[0].Columns[s].Hidden = false;
                                    }
                                }
                            }
                        }
                    }
                }
                if (uGridInput.DisplayLayout.Bands[0].Columns.Exists("AccountingObjectID"))
                {
                    if (!uGridInput.Name.Equals("uGridControl"))
                    {
                        foreach (var row in uGridInput.Rows)
                        {
                            var accountingObjectId = (Guid?)row.Cells["AccountingObjectID"].Value;
                            if (accountingObjectId != null && accountingObjectId != Guid.Empty)
                            {
                                var model = ListAccountingObject.FirstOrDefault(p => p.ID == accountingObjectId);
                                uGridInput.AutoFillAccoutingObject(model, row.Cells["AccountingObjectID"], true);
                            }
                        }
                    }
                }
            }
            else
            {
                if (band.Columns.Exists("CurrencyID") && band.Columns.Exists("ExchangeRate"))
                {
                    if (@uGridInput.Name.Equals("uGridControl"))
                    {
                        if (@uGridInput.Rows[0].Cells["ExchangeRate"].Value == null)
                        {
                            Currency currency = ListCurrency.FirstOrDefault(p => p.ID == @uGridInput.Rows[0].Cells["CurrencyID"].Value.ToString());
                            if (currency != null)
                                @uGridInput.Rows[0].Cells["ExchangeRate"].Value =
                                    currency.ExchangeRate ?? (decimal)1;
                            @uGridInput.Rows[0].Update();
                        }
                    }
                }
            }


            #endregion
        }
        /// <summary>
        /// Hàm cấu hình từng Column trong Grid cho Form thường
        /// </summary>
        /// <param name="this"></param>
        /// <param name="typeId"></param>
        /// <param name="column"></param>
        /// <param name="uGridInput"></param>
        public static void ConfigEachColumn4Grid(this Form @this, int typeId, UltraGridColumn @column, UltraGrid @uGridInput)
        {
            if (@column.Key.Contains("AccountingObjectName") || @column.Key.Contains("AccountingObjectAddress"))
                @column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            else if (@column.Key.Contains("AccountingObject") && @column.Key.Contains("ID"))
            {
                ConfigCbbToGrid(@this, @uGridInput, @column.Key, ListAccountingObject,
                                                      "ID",
                                                      "AccountingObjectCode", ConstDatabase.AccountingObject_TableName);
                // @column.MaxLength = 25;
            }
            else if (@column.Key.Equals("AccountingObjectAddress"))
            {
                @column.MaxLength = 512;
            }
            else if (@column.Key.Contains("Account") && !@column.Key.Contains("BankAccount") &&
                     !@column.Key.Contains("AccountingObject")) //tài khoản nợ, tài khoản có
            {
                IList<Account> lstTemp =
                    IAccountDefaultService.GetAccountDefaultByTypeId(typeId, @column.Key, ListAccountDefault,
                                                                     ListAccount);
                ConfigCbbToGrid(@this, @uGridInput, @column.Key, lstTemp, "AccountNumber", "AccountNumber",
                                ConstDatabase.Account_TableName);
                @column.CellAppearance.TextHAlign = HAlign.Center;
                //@column.MaxLength = 25;
            }
            else if (@column.Key.Contains("BankAccount") && !@column.Key.Contains("BankAccountingObject"))
            {
                ConfigCbbToGrid(@this, @uGridInput, @column.Key, ListBankAccountDetail,
                                "ID",
                                "BankAccount", ConstDatabase.BankAccountDetail_TableName);
                @column.Nullable = Infragistics.Win.UltraWinGrid.Nullable.Nothing;
                @column.NullText = string.Empty;
                //@column.MaxLength = 25;
            }
            else if (@column.Key.Contains("Rate") || @column.Key.Contains("Amount") || (@column.Key.Contains("UnitPrice")) ||
                     @column.Key.Contains("Quantity"))
            {
                @column.Nullable = Infragistics.Win.UltraWinGrid.Nullable.Nothing;

                var typeNonNegative = new List<int>() { 200, 300, 310 };//DS Type ko cho nhập âm

                if (typeNonNegative.Any(p => p.Equals(typeId)) && (@column.Key.Equals("Amount") || @column.Key.Equals("AmountOriginal") || @column.Key.Contains("UnitPrice") || @column.Key.Equals("UnitPriceOriginal")))
                    @column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DoubleNonNegative;
                else
                    @column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
                if (@column.Key.Contains("Rate"))
                {
                    @column.MaxLength = 36;
                }
                else if (@column.Key.Contains("Quantity"))
                {
                    @column.MaxLength = 36;
                }
                else if (@column.Key.Contains("DiscountRate"))
                {
                    @column.MaxLength = 36;
                }
                if (@column.Key.Contains("Amount") || @column.Key.Contains("UnitPrice") || @column.Key.Contains("OrgPrice"))
                {
                    @column.MaxValue = 1000000000000000;
                    @column.MaxValueExclusive = 1000000000000000.00;
                    if (typeNonNegative.Any(p => p.Equals(typeId)) && (@column.Key.Equals("Amount") || @column.Key.Equals("AmountOriginal") || @column.Key.Contains("UnitPrice") || @column.Key.Equals("UnitPriceOriginal") || @column.Key.Contains("OrgPrice")))
                    {
                        @column.MinValue = 0;
                        @column.MinValueExclusive = 0.00;
                    }
                    else
                    {
                        @column.MinValue = -1000000000000000;
                        @column.MinValueExclusive = -1000000000000000.00;
                    }
                    @column.MaxLength = 15;
                    if (@column.Key.Equals("UnitPriceOriginal"))
                    {
                        SourceUnitPrice[0] = "";
                        SourceUnitPrice[1] = "";
                        SourceUnitPrice[2] = "";
                    }
                }
            }
            else if (@column.Key.Contains("MaterialGoods"))
            {
                ConfigCbbToGrid(@this, @uGridInput, @column.Key, ListMaterialGoodsCustom, "ID",
                                "MaterialGoodsCode", ConstDatabase.MaterialGoodsCustom_TableName);
                //@column.MaxLength = 25;
            }
            else if (@column.Key.Equals("SAQuoteID"))
            {
                ConfigCbbToGrid(@this, @uGridInput, @column.Key, ListSaQuote, "ID",
                                "No", ConstDatabase.SAQuote_TableName);
                //@column.MaxLength = 25;
            }
            else if (@column.Key.Contains("CompanyTaxCode"))
            {
                @column.Nullable = Infragistics.Win.UltraWinGrid.Nullable.Nothing;
                @column.NullText = string.Empty;
                @column.MaxLength = 50;
            }
            //Loại tiền
            else if (@column.Key.Contains("CurrencyID"))
            {
                ConfigCbbToGrid(@this, @uGridInput, @column.Key, ListCurrency, "ID", "ID",
                                ConstDatabase.Currency_TableName);
                @column.MaxLength = 3;
            }
            else if (@column.Key.Contains("InvoiceTypeID"))
            {
                ConfigCbbToGrid(@this, @uGridInput, @column.Key, ListInvoiceType, "ID", "InvoiceTypeCode",
                                ConstDatabase.InvoiceType_TableName);
                //@column.MaxLength = 25;
            }
            //Nhóm HHDV
            else if (@column.Key.Equals("GoodsServicePurchaseID"))
            {
                ConfigCbbToGrid(@this, @uGridInput, @column.Key, ListGoodsServicePurchase, "ID",
                                "GoodsServicePurchaseCode", ConstDatabase.GoodsServicePurchase_TableName_ForCombo);
                //@column.MaxLength = 25;
            }
            //điều khoản thanh toán
            if (@column.Key.Contains("PaymentClause"))
            {
                ConfigCbbToGrid(@this, @uGridInput, @column.Key, ListPaymentClause, "ID",
                                "PaymentClauseCode", ConstDatabase.PaymentClause_TableName);
            }
            //Loại hóa đơn
            else if (@column.Key.Equals("InvoiceType"))
            {
                var cbbInvoiceType = new UltraComboEditor();
                cbbInvoiceType.Items.Add(0, "Không có hóa đơn");
                cbbInvoiceType.Items.Add(1, "Hóa đơn thông thường");
                cbbInvoiceType.Items.Add(2, "Hóa đơn GTGT");
                @column.EditorComponent = cbbInvoiceType;
                @column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
            }
            else if (@column.Key.Equals("InvoiceNo"))
            {
                @column.MaxLength = 7;
            }
            else if (@column.Key.Contains("Date") && !@column.Key.Equals("FinalDate"))
            {
                @column.MinValue = GetDateTimeDefault();
                @column.MinValueExclusive = GetDateTimeDefault();
                @column.Nullable = Infragistics.Win.UltraWinGrid.Nullable.Nothing;
                @column.Style = ColumnStyle.Date;
            }
            else if (@column.Key.Equals("FinalDate"))
            {
                @column.Style = ColumnStyle.IntegerNonNegative;
                @column.MinValue = 0;
                @column.Nullable = Infragistics.Win.UltraWinGrid.Nullable.Nothing;
                @column.CellAppearance.TextHAlign = HAlign.Right;
            }
            else if (@column.Key.Equals("IsIrrationalCost"))
            {
                column.Style = ColumnStyle.CheckBox;
                column.CellAppearance.TextHAlign = HAlign.Center;
            }
            else if (@column.Key.Contains("CustomProperty"))
                @column.MaxLength = 512;
            //Nhân viên
            if (@column.Key.Equals("EmployeeID"))
            {
                ConfigCbbToGrid(@this, @uGridInput, @column.Key, ListAccountingObject, "ID",
                                                      "AccountingObjectCode", ConstDatabase.AccountingObject_TableName,
                                                      null, null, 2);
            }
            //Mục thu/chi
            if (@column.Key.Equals("BudgetItemID"))
            {
                ConfigCbbToGrid(@this, @uGridInput, @column.Key, ListBudgetItem, "ID", "BudgetItemCode",
                                ConstDatabase.BudgetItem_TableName, "ParentID", "IsParentNode");
                @column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownValidate;
            }
            //Khoản mục CP
            if (@column.Key.Equals("ExpenseItemID"))
            {
                ConfigCbbToGrid(@this, @uGridInput, @column.Key, ListExpenseItem, "ID", "ExpenseItemCode",
                                ConstDatabase.ExpenseItem_TableName, "ParentID", "IsParentNode");
                @column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownValidate;
            }
            //Phòng ban
            if (@column.Key.Contains("Department"))
            {
                ConfigCbbToGrid(@this, @uGridInput, @column.Key, ListDepartment, "ID", "DepartmentCode",
                                ConstDatabase.Department_TableName, "ParentID", "IsParentNode");
                @column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownValidate;
            }
            //Đối tượng tập hợp CP
            if (@column.Key.Contains("CostSet"))
            {
                ConfigCbbToGrid(@this, @uGridInput, @column.Key, ListCostSet, "ID", "CostSetCode",
                                ConstDatabase.CostSet_TableName, "ParentID", "IsParentNode");
                @column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownValidate;
            }
            //Hợp đồng
            if (@column.Key.Contains("Contract"))
            {
                ConfigCbbToGrid(@this, @uGridInput, @column.Key, ListEmContract, "ID", "Code",
                                ConstDatabase.EMContract_TableName);
                @column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownValidate;
            }
            //Mã thống kê
            if (@column.Key.Contains("StatisticsCode"))
            {
                ConfigCbbToGrid(@this, @uGridInput, @column.Key, ListStatisticsCode, "ID", "StatisticsCode_",
                                ConstDatabase.StatisticsCode_TableName, "ParentID", "IsParentNode");
                @column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownValidate;
            }
            //Kho
            if (@column.Key.Contains("RepositoryID"))
            {
                ConfigCbbToGrid(@this, @uGridInput, @column.Key, ListRepository, "ID", "RepositoryCode",
                                ConstDatabase.Repository_TableName);
                @column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownValidate;
            }
            //Mã TSCĐ
            if (@column.Key.Equals("FixedAssetID"))
            {
                ConfigCbbToGrid(@this, @uGridInput, @column.Key, ListFixedAsset, "ID", "FixedAssetCode",
                                ConstDatabase.FixedAsset_TableName);
                @column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownValidate;
            }
            //
            if (@column.Key.Equals("VATRate"))
            {
                var cbbVatRate = new UltraComboEditor();
                cbbVatRate.Items.Add((decimal)-1, "Không có thuế");
                cbbVatRate.Items.Add((decimal)0, "0%");
                cbbVatRate.Items.Add((decimal)5, "5%");
                cbbVatRate.Items.Add((decimal)10, "10%");
                @column.EditorComponent = cbbVatRate;
                @column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
            }
            @column.ConfigColumnByNumberic();
        }
        /// <summary>
        /// Hàm cấu hình từng Column trong Grid cho Form nghiệp vụ
        /// </summary>
        /// <param name="this"></param>
        /// <param name="typeId"></param>
        /// <param name="column"></param>
        /// <param name="uGridInput"></param>
        public static void ConfigEachColumn4Grid<TK>(this Form @this, int typeId, UltraGridColumn @column, UltraGrid @uGridInput)
        {
            var u = new UtilsNoStatic();
            if (@column.Key.Contains("AccountingObjectName") || @column.Key.Contains("AccountingObjectAddress"))
                @column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Default;
            else if (@column.Key.Contains("AccountingObject") && @column.Key.Contains("ID"))
            {
                Thread th = null;
                u.ConfigCbbToGrid<TK, AccountingObject>(@this, @uGridInput, @column.Key, ListAccountingObject,
                                                      "ID",
                                                      "AccountingObjectCode", ConstDatabase.AccountingObject_TableName, isThread: true, thread: th);
                // @column.MaxLength = 25;
            }
            else if (@column.Key.Equals("AccountingObjectAddress"))
            {
                @column.MaxLength = 512;
            }
            else if (@column.Key.Contains("Account") && !@column.Key.Contains("BankAccount") &&
                     !@column.Key.Contains("AccountingObject")) //tài khoản nợ, tài khoản có
            {
                @this.ConfigAccountColumnInGrid<TK>(typeId, @uGridInput, column);
            }
            else if (@column.Key.Contains("BankAccount") && !@column.Key.Contains("BankAccountingObject"))
            {
                Thread th = null;
                u.ConfigCbbToGrid<TK, BankAccountDetail>(@this, @uGridInput, @column.Key, ListBankAccountDetail,
                                "ID",
                                "BankAccount", ConstDatabase.BankAccountDetail_TableName, isThread: true, thread: th);
                @column.Nullable = Infragistics.Win.UltraWinGrid.Nullable.Nothing;
                @column.NullText = string.Empty;
                //@column.MaxLength = 25;
            }
            else if (@column.Key.Contains("Rate") || @column.Key.Contains("Amount") || (@column.Key.Contains("UnitPrice")) ||
                     @column.Key.Contains("Quantity") || column.Key.Contains("OrgPrice"))
            {
                @column.Nullable = Infragistics.Win.UltraWinGrid.Nullable.Nothing;

                var typeNonNegative = new List<int>() { 200, 300, 310 };//DS Type ko cho nhập âm

                if (typeNonNegative.Any(p => p.Equals(typeId)) && (@column.Key.Equals("Amount") || @column.Key.Equals("AmountOriginal") || @column.Key.Contains("UnitPrice") || @column.Key.Equals("UnitPriceOriginal") || @column.Key.Contains("OrgPrice")))
                    @column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DoubleNonNegative;
                else
                    @column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
                if (@column.Key.Contains("Rate"))
                {
                    @column.MaxLength = 36;
                }
                else if (@column.Key.Contains("Quantity"))
                {
                    @column.MaxLength = 36;
                }
                else if (@column.Key.Contains("DiscountRate"))
                {
                    @column.MaxLength = 36;
                }
                if (@column.Key.Contains("Amount") || @column.Key.Contains("UnitPrice") || column.Key.Contains("OrgPrice"))
                {
                    @column.MaxValue = 1000000000000000;
                    @column.MaxValueExclusive = 1000000000000000.00;
                    if (typeNonNegative.Any(p => p.Equals(typeId)) && (@column.Key.Equals("Amount") || @column.Key.Equals("AmountOriginal") || @column.Key.Contains("UnitPrice") || @column.Key.Equals("UnitPriceOriginal") || @column.Key.Equals("OrgPrice") || @column.Key.Equals("OrgPriceOriginal")))
                    {
                        @column.MinValue = 0;
                        @column.MinValueExclusive = 0.00;
                    }
                    else
                    {
                        @column.MinValue = -1000000000000000;
                        @column.MinValueExclusive = -1000000000000000.00;
                    }
                    @column.MaxLength = 15;
                    if (@column.Key.Equals("UnitPriceOriginal"))
                    {
                        SourceUnitPrice[0] = "";
                        SourceUnitPrice[1] = "";
                        SourceUnitPrice[2] = "";
                    }
                }
            }
            else if (@column.Key.Contains("MaterialGoods"))
            {
                Thread th = null;
                u.ConfigCbbToGrid<TK, MaterialGoodsCustom>(@this, @uGridInput, @column.Key, ListMaterialGoodsCustom, "ID",
                                "MaterialGoodsCode", ConstDatabase.MaterialGoodsCustom_TableName, isThread: true, thread: th);
                //@column.MaxLength = 25;
            }
            else if (@column.Key.Equals("SAQuoteID"))
            {
                Thread th = null;
                u.ConfigCbbToGrid<TK, SAQuote>(@this, @uGridInput, @column.Key, ListSaQuote, "ID",
                                "No", ConstDatabase.SAQuote_TableName, isThread: true, thread: th);
                //@column.MaxLength = 25;
            }
            else if (@column.Key.Contains("CompanyTaxCode"))
            {
                @column.Nullable = Infragistics.Win.UltraWinGrid.Nullable.Nothing;
                @column.NullText = string.Empty;
                @column.MaxLength = 50;
            }
            //Loại tiền
            else if (@column.Key.Contains("CurrencyID"))
            {
                Thread th = null;
                u.ConfigCbbToGrid<TK, Currency>(@this, @uGridInput, @column.Key, ListCurrency, "ID", "ID",
                                ConstDatabase.Currency_TableName, isThread: true, thread: th);
                @column.MaxLength = 3;
            }
            else if (@column.Key.Contains("InvoiceTypeID"))
            {
                Thread th = null;
                u.ConfigCbbToGrid<TK, InvoiceType>(@this, @uGridInput, @column.Key, ListInvoiceType, "ID", "InvoiceTypeCode",
                                ConstDatabase.InvoiceType_TableName, isThread: true, thread: th);
                //@column.MaxLength = 25;
            }
            //Nhóm HHDV
            else if (@column.Key.Equals("GoodsServicePurchaseID"))
            {
                Thread th = null;
                u.ConfigCbbToGrid<TK, GoodsServicePurchase>(@this, @uGridInput, @column.Key, ListGoodsServicePurchase, "ID",
                                "GoodsServicePurchaseCode", ConstDatabase.GoodsServicePurchase_TableName_ForCombo, isThread: true, thread: th);
                //@column.MaxLength = 25;
            }
            //điều khoản thanh toán
            if (@column.Key.Contains("PaymentClause"))
            {
                Thread th = null;
                u.ConfigCbbToGrid<TK, PaymentClause>(@this, @uGridInput, @column.Key, ListPaymentClause, "ID",
                                "PaymentClauseCode", ConstDatabase.PaymentClause_TableName, isThread: true, thread: th);
            }
            //Loại hóa đơn
            else if (@column.Key.Equals("InvoiceType"))
            {
                var cbbInvoiceType = new UltraComboEditor();
                cbbInvoiceType.Items.Add(0, "Không có hóa đơn");
                cbbInvoiceType.Items.Add(1, "Hóa đơn thông thường");
                cbbInvoiceType.Items.Add(2, "Hóa đơn GTGT");
                @column.EditorComponent = cbbInvoiceType;
                @column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
            }
            else if (@column.Key.Equals("InvoiceNo"))
            {
                @column.MaxLength = 7;
            }
            else if (@column.Key.Contains("Date") && !@column.Key.Equals("FinalDate"))
            {
                @column.MinValue = GetDateTimeDefault();
                @column.MinValueExclusive = GetDateTimeDefault();
                @column.Nullable = Infragistics.Win.UltraWinGrid.Nullable.Nothing;
                @column.Style = ColumnStyle.Date;
            }
            else if (@column.Key.Equals("FinalDate"))
            {
                @column.Style = ColumnStyle.IntegerNonNegative;
                @column.MinValue = 0;
                @column.Nullable = Infragistics.Win.UltraWinGrid.Nullable.Nothing;
                @column.CellAppearance.TextHAlign = HAlign.Right;
            }
            else if (@column.Key.Equals("IsIrrationalCost"))
            {
                column.Style = ColumnStyle.CheckBox;
                column.CellAppearance.TextHAlign = HAlign.Center;
            }
            else if (@column.Key.Contains("CustomProperty"))
                @column.MaxLength = 512;
            //Nhân viên
            if (@column.Key.Equals("EmployeeID"))
            {
                Thread th = null;
                u.ConfigCbbToGrid<TK, AccountingObject>(@this, @uGridInput, @column.Key, ListAccountingObject, "ID",
                                                      "AccountingObjectCode", ConstDatabase.AccountingObject_TableName,
                                                      null, null, 2, isThread: true, thread: th);
            }
            //Mục thu/chi
            if (@column.Key.Equals("BudgetItemID"))
            {
                Thread th = null;
                u.ConfigCbbToGrid<TK, BudgetItem>(@this, @uGridInput, @column.Key, ListBudgetItem, "ID", "BudgetItemCode",
                                ConstDatabase.BudgetItem_TableName, "ParentID", "IsParentNode", isThread: true, thread: th);
                @column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownValidate;
            }
            //Khoản mục CP
            if (@column.Key.Equals("ExpenseItemID"))
            {
                Thread th = null;
                u.ConfigCbbToGrid<TK, ExpenseItem>(@this, @uGridInput, @column.Key, ListExpenseItem, "ID", "ExpenseItemCode",
                                ConstDatabase.ExpenseItem_TableName, "ParentID", "IsParentNode", isThread: true, thread: th);
                @column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownValidate;
            }
            //Phòng ban
            if (@column.Key.Contains("Department"))
            {
                Thread th = null;
                u.ConfigCbbToGrid<TK, Department>(@this, @uGridInput, @column.Key, ListDepartment, "ID", "DepartmentCode",
                                ConstDatabase.Department_TableName, "ParentID", "IsParentNode", isThread: true, thread: th);
                @column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownValidate;
            }
            //Đối tượng tập hợp CP
            if (@column.Key.Contains("CostSet"))
            {
                Thread th = null;
                u.ConfigCbbToGrid<TK, CostSet>(@this, @uGridInput, @column.Key, ListCostSet, "ID", "CostSetCode",
                                ConstDatabase.CostSet_TableName, "ParentID", "IsParentNode", isThread: true, thread: th);
                @column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownValidate;
            }
            //Hợp đồng
            if (@column.Key.Contains("Contract"))
            {
                Thread th = null;
                u.ConfigCbbToGrid<TK, EMContract>(@this, @uGridInput, @column.Key, ListEmContract, "ID", "Code",
                                ConstDatabase.EMContract_TableName, isThread: true, thread: th);
                @column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownValidate;
            }
            //Mã thống kê
            if (@column.Key.Contains("StatisticsCode"))
            {
                Thread th = null;
                u.ConfigCbbToGrid<TK, StatisticsCode>(@this, @uGridInput, @column.Key, ListStatisticsCode, "ID", "StatisticsCode_",
                                ConstDatabase.StatisticsCode_TableName, "ParentID", "IsParentNode", isThread: true, thread: th);
                @column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownValidate;
            }
            //Kho
            if (@column.Key.Contains("RepositoryID"))
            {
                Thread th = null;
                u.ConfigCbbToGrid<TK, Repository>(@this, @uGridInput, @column.Key, ListRepository, "ID", "RepositoryCode",
                                ConstDatabase.Repository_TableName, isThread: true, thread: th);
                @column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownValidate;
            }
            //Mã TSCĐ
            if (@column.Key.Equals("FixedAssetID"))
            {
                Thread th = null;
                u.ConfigCbbFixedAssetToGrid(@this, @uGridInput, @column.Key, ListFixedAsset, "ID", "FixedAssetCode",
                    ConstDatabase.FixedAsset_TableName, (typeof(TK) == typeof(FAIncrement) ? 1 : (typeof(TK) == typeof(FADecrement) ? 0 : (int?)null)), isThread: true, thread: th);
                @column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownValidate;
            }
            //
            if (@column.Key.Equals("VATRate"))
            {
                var cbbVatRate = new UltraComboEditor();
                cbbVatRate.Items.Add((decimal)-1, "Không có thuế");
                cbbVatRate.Items.Add((decimal)0, "0%");
                cbbVatRate.Items.Add((decimal)5, "5%");
                cbbVatRate.Items.Add((decimal)10, "10%");
                @column.EditorComponent = cbbVatRate;
                @column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
            }
            if (@column.Key.Equals("IsImportPurchase"))
            {
                var cbbIsImportPurchase = new UltraComboEditor();
                cbbIsImportPurchase.Items.Add(false, "Trong nước");
                cbbIsImportPurchase.Items.Add(true, "Nhập khẩu");
                @column.EditorComponent = cbbIsImportPurchase;
                @column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
            }
            @column.ConfigColumnByNumberic();
        }

        /// <summary>
        /// Format định dạng số cho column
        /// </summary>
        /// <param name="column"></param>
        public static void ConfigColumnByNumberic(this UltraGridColumn column)
        {
            if (column.Key.Contains("Rate") || column.Key.Contains("Amount") || (column.Key.Contains("UnitPrice")) ||
                     column.Key.Contains("Quantity") || column.Key.Contains("OrgPrice"))
            {
                if (column.Key.Contains("Rate"))
                {
                    FormatNumberic(column, ConstDatabase.Format_Rate);
                }
                else if (column.Key.Contains("QuantityConvert"))
                    FormatNumberic(column, ConstDatabase.Format_QuantityConversions);
                else if (column.Key.Contains("Quantity"))
                {
                    FormatNumberic(column, ConstDatabase.Format_Quantity);
                }
                else if (column.Key.Contains("DiscountRate"))
                {
                    FormatNumberic(column, ConstDatabase.Format_Coefficient);
                }
                else
                    FormatNumberic(column, ConstDatabase.Format_TienVND);
            }
        }

        #region Bộ code cấu hình Đơn giá
        private static void SetDropDownWithMaskInput(EditorWithMask mask, UltraGridCell cell)
        {
            //binding source for the DropDownWithMaskInput
            var dropDownBtn = new DropDownEditorButton("ddButton");
            var grid = new UltraGrid();
            dropDownBtn.Control = grid;
            dropDownBtn.BeforeDropDown += new BeforeEditorButtonDropDownEventHandler(dropDownBtn_BeforeDropDown);
            dropDownBtn.ButtonStyle = UIElementButtonStyle.Office2010Button;

            grid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            grid.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            grid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            grid.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            grid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            grid.AutoSize = true;
            UltraGridBand band = grid.DisplayLayout.Bands[0];
            band.HeaderVisible = false;
            band.ColHeadersVisible = false;
            grid.DisplayLayout.BorderStyle = UIElementBorderStyle.WindowsVista;
            grid.DisplayLayout.AutoFitStyle = AutoFitStyle.ExtendLastColumn;
            //Cau hinh kieu Scroll
            grid.DisplayLayout.ScrollStyle = ScrollStyle.Immediate;
            grid.DisplayLayout.ScrollBounds = ScrollBounds.ScrollToFill;
            //
            band.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            band.Override.SelectedRowAppearance.BackColor = Color.White;
            band.Override.HotTrackRowAppearance.ForeColor = Color.White;
            band.Override.HotTrackRowSelectorAppearance.BackColor = Color.FromArgb(51, 153, 255);
            band.Override.HotTrackRowCellAppearance.BackColor = Color.FromArgb(51, 153, 255);
            band.Override.BorderStyleCell = UIElementBorderStyle.None;
            band.Override.BorderStyleRow = UIElementBorderStyle.None;
            grid.DataSource = SourceUnitPrice;
            grid.Tag = cell;

            mask.ButtonsRight.Add(dropDownBtn);

            grid.InitializeLayout += new InitializeLayoutEventHandler(grid_InitializeLayout);
            grid.DoubleClick += new EventHandler(grid_DoubleClick);
            grid.KeyDown += new KeyEventHandler(grid_KeyDown);

        }
        static void grid_KeyDown(object sender, KeyEventArgs e)
        {
            var ug = (UltraGrid)sender;
            var cell = (UltraGridCell)ug.Tag;
            if (e.KeyCode == Keys.Escape)
            {
                if (ug.Selected.Rows.Count > 0)
                    ug.Selected.Rows.Clear();
                else
                {
                    var f = ug.FindForm();
                    cell.Value = null;
                    if (f != null) f.Hide();
                }
                return;
            }
            if (e.KeyCode == Keys.Enter)
            {
                var f = ug.FindForm();
                switch (ug.Selected.Rows.Count)
                {
                    case 0:
                        cell.Value = null;
                        break;
                    case 1:
                        cell.Value = ug.Selected.Rows[0].Cells[0].Value;
                        break;
                }
                if (f != null) f.Hide();
            }
        }

        static void grid_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            FormatNumberic(e.Layout.Bands[0].Columns[0], ConstDatabase.Format_DonGiaQuyDoi);
        }

        static void grid_DoubleClick(object sender, EventArgs e)
        {
            var ug = (UltraGrid)sender;
            var f = (DropDownManager.DropDownForm)ug.FindForm();
            var cell = (UltraGridCell)ug.Tag;
            switch (ug.Selected.Rows.Count)
            {
                case 0:
                    cell.Value = null;
                    break;
                case 1:
                    if (!string.IsNullOrEmpty(ug.Selected.Rows[0].Cells[0].Value.ToString()))
                        cell.Value = Convert.ToDecimal(ug.Selected.Rows[0].Cells[0].Value);
                    break;
            }

            if (f != null) f.Hide();
        }
        static void dropDownBtn_BeforeDropDown(object sender, BeforeEditorButtonDropDownEventArgs e)
        {
            ((DropDownEditorButton)sender).Control.Size =
                new Size(((UltraGridCell)((DropDownEditorButton)sender).Control.Tag).Width, 140);
        }
        #endregion

        /// <summary>
        /// Cấu hình vùng số chứng từ
        /// </summary>
        /// <typeparam name="TK">Đối tượng của Form</typeparam>
        /// <param name="this">this</param>
        /// <param name="panel">UltraPanel chứa Control Số chứng từ</param>
        /// <param name="no">Tên thuộc tính "Số chứng từ" trên đối tượng sẽ được DataBinding</param>
        /// <param name="postedDate">Tên thuộc tính "Ngày hạch toán" trên đối tượng sẽ được DataBinding</param>
        /// <param name="date">Tên thuộc tính "Ngày chứng từ" trên đối tượng sẽ được DataBinding</param>
        /// <param name="typeGroup">TypeGroup (mặc định để null sẽ tự động lấy TypeGroup theo TypeID trong DetailBase)</param>
        /// <param name="postedDateVisible">Hiện/Ẩn Ngày hạch toán</param>
        /// <param name="dateVisible">Hiện/Ẩn Ngày chứng từ</param>        
        /// <param name="haveType">Có cột [Loại giấy báo nợ] hay không. [Default = false]</param>
        /// <param name="typeId">Tên thuộc tính "TypeID" trên đối tượng sẽ được DataBinding</param>
        /// <param name="cbbTypeRowSelected">Sự kiện RowSelected của Combo [Loại giấy báo nợ]</param>
        /// <param name="objectDataSource"> </param>
        /// <param name="resetNo">Có làm mới Số chứng từ không</param>
        public static void ConfigTopVouchersNo<TK>(this Form @this, UltraPanel @panel, string no, string postedDate, string date, int? typeGroup = null, bool postedDateVisible = true, bool dateVisible = true, bool haveType = false, string typeId = null, RowSelectedEventHandler cbbTypeRowSelected = null, object objectDataSource = null, bool resetNo = false)
        {
            var topVouchers = new UTopVouchers<TK>(@this, no, postedDate, date, typeGroup, haveType, typeId, objectDataSource, resetNo);
            topVouchers.Visible = true;
            topVouchers.Dock = DockStyle.Top;
            topVouchers.DateVisible = dateVisible;
            topVouchers.PostedDateVisible = postedDateVisible;
            topVouchers.Refresh();
            topVouchers.Name = "TopVouchersControl";
            topVouchers.CbbType_RowSelected = cbbTypeRowSelected;
            @panel.ClientArea.Controls.Clear();
            @panel.ClientArea.Controls.Add(topVouchers);
            @panel.AutoSize = true;
        }
        public static void FocusDate<TK>(this UltraPanel panel)
        {
            var topVouchers =
                (UTopVouchers<TK>)panel.Controls.Find("TopVouchersControl", true).FirstOrDefault();
            if (topVouchers != null)
                topVouchers.FocusDate();
        }
        public static void FocusPostedDate<TK>(this UltraPanel panel)
        {
            var topVouchers =
                (UTopVouchers<TK>)panel.Controls.Find("TopVouchersControl", true).FirstOrDefault();
            if (topVouchers != null)
                topVouchers.FocusPostedDate();
        }
        public static void FocusNo<TK>(this UltraPanel panel)
        {
            var topVouchers =
                (UTopVouchers<TK>)panel.Controls.Find("TopVouchersControl", true).FirstOrDefault();
            if (topVouchers != null)
                topVouchers.FocusNo();
        }
        public static UTopVouchers<TK> GetTopVouchers<TK>(this UltraPanel panel)
        {
            var topVouchers =
                (UTopVouchers<TK>)panel.Controls.Find("TopVouchersControl", true).FirstOrDefault();
            if (topVouchers != null)
                return topVouchers;
            return null;
        }
        /// <summary>
        /// Add sự kiện KeyDown vào tất cả Control
        /// </summary>
        /// <param name="this"> </param>
        /// <param name="controlContainer">Control chứa</param>
        private static void AddKeyDownEventForControls<TK>(this Form @this, Control controlContainer)
        {
            foreach (Control @control in controlContainer.Controls)
            {
                var eventInfo = @control.GetType().GetEvent("KeyDown");
                if (eventInfo != null)
                {
                    var handler = new KeyEventHandler(@this.Control_KeyDown<TK>);
                    eventInfo.AddEventHandler(@control, handler);
                }
                if (@control.HasChildren)
                    @this.AddKeyDownEventForControls<TK>(@control);
            }
        }
        private static void Control_KeyDown<TK>(this Form @this, object sender, KeyEventArgs e)
        {
            @this.CheckShortCut4Grid<TK>(sender, e);
        }

        /// <summary>
        /// Kiểm tra phím tắt (Ctrl+Insert) và (Ctrl+Del)
        /// </summary>
        /// <param name="this"> </param>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void CheckShortCut4Grid<TK>(this Form @this, object sender, KeyEventArgs e)
        {
            var uGrid = @this.FindGridIsActived();
            if (uGrid == null) return;
            if (e.Control && e.KeyCode.Equals(Keys.Insert))
            {
                AddNewRow4Grid(uGrid);
            }
            else if (e.Control && e.KeyCode.Equals(Keys.Delete))
            {
                @this.RemoveRow4Grid<TK>(uGrid);
            }
        }
        public static void AddNewRow4Grid(this UltraGrid uGrid)
        {
            uGrid.DisplayLayout.Bands[0].AddNew();
            uGrid.Rows[uGrid.Rows.Count - 1].Update();
        }
        public static void RemoveRow4Grid<TK>(this Form @this, UltraGrid uGrid)
        {
            UltraGridRow row = null;
            if (uGrid.ActiveCell != null || uGrid.ActiveRow != null)
            {
                row = uGrid.ActiveCell != null ? uGrid.ActiveCell.Row : uGrid.ActiveRow;
            }
            else
            {
                if (uGrid.Rows.Count > 0) row = uGrid.Rows[uGrid.Rows.Count - 1];
            }
            if (row != null)
            {
                row.Delete(false);
                if (uGrid.DisplayLayout.Bands[0].Columns.Exists("DebitAccount") && uGrid.DisplayLayout.Bands[0].Columns.Exists("CreditAccount") && uGrid.DisplayLayout.Bands[0].Columns.Exists("AmountOriginal"))
                {
                    var @base = @this as DetailBase<TK>;
                    if (@base != null)
                    {
                        foreach (var rw in uGrid.Rows)
                        {
                            @this.SetVatAmount<TK>(uGrid, rw.Cells["AmountOriginal"], @base.TypeID);
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Hàm cấu hình các summary mặc định
        /// </summary>
        /// <param name="uGridInput"></param>
        public static void ConfigSummaryOfGrid(this UltraGrid uGridInput)
        {
            UltraGridBand band = uGridInput.DisplayLayout.Bands[0];
            foreach (UltraGridColumn @column in band.Columns)
            {
                if (@column.Key.Contains("Amount") || @column.Key.Contains("OrgPrice"))
                {
                    //Thêm tổng số ở cột các cột "Số tiền"

                    SummarySettings summary = band.Summaries.Add("Sum" + @column.Key, SummaryType.Sum, band.Columns[@column.Key]);
                    summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
                    string format = GetTypeFormatNumberic(ConstDatabase.Format_TienVND);
                    summary.DisplayFormat = "{0:" + format + ";(" + format + ")}";
                    summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
                    summary.Appearance.TextHAlign = HAlign.Right;
                }
                else if (@column.Key.Contains("Quantity"))
                {
                    SummarySettings summary = uGridInput.DisplayLayout.Bands[0].Summaries.Add("Sum" + @column.Key, SummaryType.Sum, band.Columns[@column.Key]);
                    summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
                    summary.DisplayFormat = "{0:" + GetTypeFormatNumberic(ConstDatabase.Format_Quantity) + "}";
                    summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
                    summary.Appearance.TextHAlign = HAlign.Right;
                }
                else if (@column.Key.Equals("FreightRate"))
                {
                    SummarySettings summary = uGridInput.DisplayLayout.Bands[0].Summaries.Add("Sum" + @column.Key, SummaryType.Sum, band.Columns[@column.Key]);
                    summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
                    summary.DisplayFormat = "{0:" + GetTypeFormatNumberic(ConstDatabase.Format_Coefficient) + "}";
                    summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
                    summary.Appearance.TextHAlign = HAlign.Right;
                }
            }
        }
        #endregion

        #region Cấu hình Grid Tiền tệ theo Template
        /// <summary>
        /// Cấu hình cho Grid Tiền tệ
        /// </summary>
        /// <param name="this"></param>
        /// <param name="typeId"></param>
        /// <param name="listObject"></param>
        /// <param name="uGridInput"></param>
        /// <param name="template"></param>
        public static void ConfigGridCurrencyByTemplate(this Form @this, int typeId, List<IList> listObject, UltraGrid @uGridInput, Template template)
        {
            var templateDetail = template.TemplateDetails.FirstOrDefault(k => k.TabIndex == 100);
            if (templateDetail == null)
            {
                @uGridInput.Visible = false;
                return;
            }
            ConfigGridData(@this, typeId, @uGridInput, listObject, templateDetail.TemplateColumns);
            ConfigGrid(@uGridInput, null, templateDetail.TemplateColumns, true, false);
            @uGridInput.Name = "uGridControl";
            @uGridInput.Visible = true;
            @uGridInput.DisplayLayout.Bands[0].Summaries.Clear();
        }

        /// <summary>
        /// Hàm cấu hình lại size uGrid theo các cột đang hiện
        /// </summary>
        /// <param name="uGrid"></param>
        /// <param name="this"> </param>
        public static void ConfigSizeGridCurrency(this UltraGrid uGrid, Form @this)
        {
            int total = 0;
            int height = 0;
            var point = new Point(uGrid.Location.X + uGrid.Width, uGrid.Location.Y);
            foreach (var col in uGrid.DisplayLayout.Bands[0].Columns)
            {
                if (!col.Hidden)
                    total += col.Width;
                height = col.CellSizeResolved.Height;
            }
            height += uGrid.DisplayLayout.Bands[0].Header.Height;
            uGrid.DisplayLayout.Override.CellSpacing = 0;
            uGrid.DisplayLayout.BorderStyle = UIElementBorderStyle.WindowsVista;
            //uGrid.Anchor = AnchorStyles.Right;
            uGrid.MaximumSize = new Size(@this.Width, height);
            uGrid.Width = total + 2;
            point.X = point.X - uGrid.Width;
            uGrid.DisplayLayout.Scrollbars = (total + 2) >= @this.Width ? Scrollbars.Automatic : Scrollbars.None;
            uGrid.Location = new Point(point.X, uGrid.Location.Y);
            uGrid.DisplayLayout.Override.BorderStyleRow = UIElementBorderStyle.None;
            uGrid.DisplayLayout.Override.BorderStyleCell = UIElementBorderStyle.None;
            uGrid.DisplayLayout.Override.BorderStyleHeader = UIElementBorderStyle.None;
            uGrid.DisplayLayout.Override.RowSizingAutoMaxLines = 1;
            uGrid.DisplayLayout.Override.RowSizing = RowSizing.AutoFree;
            uGrid.DisplayLayout.Override.RowSizingArea = RowSizingArea.EntireRow;
            //uGrid.DisplayLayout.Override.DefaultRowHeight = 18;
            uGrid.Height = height;
            uGrid.DisplayLayout.UseFixedHeaders = false;
            var panel = (Panel)uGrid.GetParentControl<Panel>();
            panel.Height = uGrid.Parent.Height;
        }

        static Control GetParentControl<T>(this Control control)
        {
            if (control.Parent is T)
            {
                return control.Parent;
            }
            return control.Parent.GetParentControl<T>();
        }
        /// <summary>
        /// Cấu hình Grid Tiền tệ theo Template
        /// </summary>
        /// <typeparam name="TK">Đối tượng của Form</typeparam>
        /// <param name="this"></param>
        /// <param name="typeId"></param>
        /// <param name="listObject"></param>
        /// <param name="uGrid"></param>
        /// <param name="template"></param>
        public static void ConfigGridCurrencyByTemplate<TK>(this Form @this, int typeId, List<IList> listObject, UltraGrid uGrid, Template template)
        {
            ConfigGridCurrencyByTemplate(@this, typeId, listObject, uGrid, template);
            if (uGrid.DataSource == null)
            {
                @this.Controls.Remove(uGrid);
                return;
            }
            @this.ConfigGridDetail<TK>(typeId, uGrid);
            uGrid.DisplayLayout.Bands[0].Summaries.Clear();
            uGrid.ConfigSizeGridCurrency(@this);
            uGrid.DisplayLayout.Override.ActiveRowAppearance.BackColor = Color.White;
            uGrid.DisplayLayout.Override.ActiveRowCellAppearance.BackColor = Color.White;
            uGrid.DisplayLayout.Override.RowAppearance.BackColor = Color.White;
            var detailBase = (DetailBase<TK>)@this;
            detailBase.uGridControl = uGrid;
            uGrid.AfterCellActivate += @this.uGrid_AfterCellActivate<TK>;
            uGrid.KeyDown += @this.uGrid_KeyDown<TK>;
            uGrid.KeyUp += uGrid_KeyUp;
            uGrid.AfterCellUpdate += @this.uGridControl_AfterCellUpdate<TK>;
            uGrid.AfterExitEditMode += @this.uGrid_AfterExitEditMode<TK>;
            uGrid.CellDataError += uGrid_CellDataError;
            uGrid.KeyPress += @this.uGrid_KeyPress<TK>;
            uGrid.CellChange += @this.uGrid_CellChange<TK>;
            uGrid.Validated += uGrid_Validated;
        }

        public static void uGrid_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Tab)
            {
                var uGrid = (UltraGrid)sender;
                if (uGrid.ActiveCell == null)
                {
                    if (uGrid.Rows.Count == 0)
                    {
                        uGrid.DisplayLayout.Bands[0].AddNew();
                    }
                    int index = 0;
                    if (uGrid.ActiveRow != null) index = uGrid.ActiveRow.Index;
                    var columns = uGrid.DisplayLayout.Bands[0].Columns.Cast<UltraGridColumn>().ToList();
                    var fisrtCol = columns.OrderBy(p => p.Header.VisiblePosition).FirstOrDefault(p => !p.Hidden);
                    if (fisrtCol != null)
                    {
                        uGrid.ActiveCell = uGrid.Rows[index].Cells[fisrtCol.Key];
                        uGrid.PerformAction(UltraGridAction.EnterEditMode, false, false);
                    }
                }
            }
        }

        #region Xử lý phần Tìm Nhanh

        /// <summary>
        /// Hàm hiện form Tìm nhanh
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="this"> </param>
        /// <param name="control"></param>
        public static void ShowQuickFilter<T>(this Form @this, Control @control)
        {
            var ultraCombo = new UltraCombo();
            string title = string.Empty;
            if (@control.GetType() == typeof(UltraGrid))
            {
                var ultraGrid = (UltraGrid)@control;
                UltraGridCell cell = ultraGrid.ActiveCell;
                cell.EditorResolved.CloseUp();
                title = String.IsNullOrEmpty(cell.Column.Header.ToolTipText)
                                   ? cell.Column.Header.Caption
                                   : cell.Column.Header.ToolTipText;
                ultraCombo = (UltraCombo)ultraGrid.ActiveCell.ValueListResolved;
            }
            else if (@control.GetType() == typeof(UltraCombo))
            {
                ultraCombo = (UltraCombo)@control;
                if (ultraCombo.IsDroppedDown) ultraCombo.ToggleDropdown();
            }
            var listInput = CloneObject(ultraCombo.DataSource.GetType() == typeof(BindingList<T>) ? ((BindingList<T>)ultraCombo.DataSource).ToList() : (List<T>)ultraCombo.DataSource);
            int i = 0;
            while (i < listInput.Count)
            {
                var propIsActive = listInput[i].GetType().GetProperty("IsActive");
                if (propIsActive == null || !propIsActive.CanWrite) { i++; continue; }
                if (!(bool)propIsActive.GetValue(listInput[i], null))
                    listInput.Remove(listInput[i]);
                else
                    i++;
            }
            int? accountingObjectType = null;
            if (ultraCombo.Tag != null)
            {
                var dicTag = (Dictionary<string, object>)ultraCombo.Tag;
                if (dicTag.Any(p => p.Key.Equals("AccountingObjectType")))
                    accountingObjectType = (int?)dicTag.FirstOrDefault(p => p.Key.Equals("AccountingObjectType")).Value;
            }
            var frmFilter = new FFilters<T>(listInput, typeof(T).Name, ultraCombo.ValueMember, title, accountingObjectType);
            frmFilter.FormClosed += @this.frmFilter_FormClosed<T>;
            frmFilter.ShowDialog();
        }
        static void frmFilter_FormClosed<T>(this Form @this, object sender, FormClosedEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                var fFilters = (FFilters<T>)sender;
                if (fFilters.DialogResult == DialogResult.OK)
                {
                    UltraGrid uGrid = FindGridIsActived(@this);
                    if (uGrid != null)
                    {
                        if (uGrid.ActiveCell != null)
                        {
                            uGrid.ActiveCell.Value = fFilters.Output;
                            if (typeof(T) == typeof(Currency))
                            {
                                var fFilterCurrency = (FFilters<Currency>)sender;
                                @this.AutoExchangeByCurrencySelected(fFilterCurrency.Selected);
                                fFilterCurrency.Dispose();
                            }
                            uGrid.ActiveCell.ValueListResolved.CloseUp();

                            //_isKeyPress = false;
                            var combo = (UltraCombo)uGrid.ActiveCell.ValueListResolved;
                            //combo.SetValueForTag("KeyPress", false);
                            combo.Refresh();
                        }
                    }
                    else
                    {
                        var combo = FindComboIsActived(@this);
                        if (combo != null)
                        {
                            PropertyInfo propValue = fFilters.Selected.GetType().GetProperty(combo.ValueMember);
                            if (propValue != null && propValue.CanRead)
                            {
                                combo.Value = propValue.GetValue(fFilters.Selected, null);
                            }
                            else
                                combo.Value = fFilters.Output;
                            fFilters.Selected.ExcuteRowSelectedEventOfcbbAutoFilter(@this, combo, true);
                            combo.ToggleDropdown();
                            combo.Refresh();
                            if (combo.DataBindings.Count > 0)
                                combo.DataBindings[0].WriteValue();
                            //combo.SetValueForTag("KeyPress", false);
                            //_isKeyPress = false;
                        }
                    }
                }
                else
                {
                }
                fFilters.Dispose();
                if (StopWatch.IsRunning)
                {
                    //_stopWatch.Stop();
                    StopWatch.Reset();
                    //_stopWatch.Start();
                }
                else
                    StopWatch.Start();
            }
        }

        public static List<UltraGrid> GetAllGridInForm(this Control @this)
        {
            var lstGrid = new List<UltraGrid>();
            foreach (Control control in @this.Controls)
            {
                if ((control is UltraGrid) && !String.IsNullOrEmpty(control.Name))
                    lstGrid.Add(control as UltraGrid);
                else if (control.HasChildren)
                    lstGrid.AddRange(control.GetAllGridInForm());
            }
            return lstGrid;
        }
        public static List<UltraCombo> GetAllComboInForm(this Control @this)
        {
            var lstCombo = new List<UltraCombo>();
            foreach (Control control in @this.Controls)
            {
                if ((control is UltraCombo) && !String.IsNullOrEmpty(control.Name))
                    lstCombo.Add(control as UltraCombo);
                else if (control.HasChildren)
                    lstCombo.AddRange(control.GetAllComboInForm());
            }
            return lstCombo;
        }
        /// <summary>
        /// Lấy về Grid đang được Active trong form
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static UltraGrid FindGridIsActived(this Form @this)
        {
            UltraGrid uGrid = null;
            if (@this.ActiveControl != null && @this.ActiveControl.GetType() == typeof(UltraGrid))
                uGrid = (UltraGrid)@this.ActiveControl;
            else if (@this.ActiveControl != null && @this.ActiveControl.Parent != null && @this.ActiveControl.Parent.GetType() == typeof(UltraGrid))
                uGrid = (UltraGrid)@this.ActiveControl.Parent;
            else if (@this.ActiveControl != null && @this.ActiveControl.Parent != null && @this.ActiveControl.Parent.Parent != null && @this.ActiveControl.Parent.Parent.GetType() == typeof(UltraGrid))
                uGrid = (UltraGrid)@this.ActiveControl.Parent.Parent;
            else if (@this.ActiveControl != null && @this.ActiveControl.Parent != null && @this.ActiveControl.Parent.Parent != null && @this.ActiveControl.Parent.Parent.Parent != null && @this.ActiveControl.Parent.Parent.Parent.GetType() == typeof(UltraGrid))
                uGrid = (UltraGrid)@this.ActiveControl.Parent.Parent.Parent;
            else if (@this.ActiveControl != null && @this.ActiveControl.Parent != null && @this.ActiveControl.Parent.Parent != null && @this.ActiveControl.Parent.Parent.Parent != null && @this.ActiveControl.Parent.Parent.Parent.Parent != null && @this.ActiveControl.Parent.Parent.Parent.Parent.GetType() == typeof(UltraGrid))
                uGrid = (UltraGrid)@this.ActiveControl.Parent.Parent.Parent.Parent;
            return uGrid;
        }
        /// <summary>
        /// Lấy về Combo đang được active trong form
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static UltraCombo FindComboIsActived(this Form @this)
        {
            UltraCombo ultraCombo = null;
            if (@this.ActiveControl != null && @this.ActiveControl.GetType() == typeof(UltraCombo))
                ultraCombo = (UltraCombo)@this.ActiveControl;
            else if (@this.ActiveControl != null && @this.ActiveControl.Parent != null && @this.ActiveControl.Parent.GetType() == typeof(UltraCombo))
                ultraCombo = (UltraCombo)@this.ActiveControl.Parent;
            else if (@this.ActiveControl != null && @this.ActiveControl.Parent != null && @this.ActiveControl.Parent.Parent != null && @this.ActiveControl.Parent.Parent.GetType() == typeof(UltraCombo))
                ultraCombo = (UltraCombo)@this.ActiveControl.Parent.Parent;
            else if (@this.ActiveControl != null && @this.ActiveControl.Parent != null && @this.ActiveControl.Parent.Parent != null && @this.ActiveControl.Parent.Parent.Parent != null && @this.ActiveControl.Parent.Parent.Parent.GetType() == typeof(UltraCombo))
                ultraCombo = (UltraCombo)@this.ActiveControl.Parent.Parent.Parent;
            else if (@this.ActiveControl != null && @this.ActiveControl.Parent != null && @this.ActiveControl.Parent.Parent != null && @this.ActiveControl.Parent.Parent.Parent != null && @this.ActiveControl.Parent.Parent.Parent.Parent != null && @this.ActiveControl.Parent.Parent.Parent.Parent.GetType() == typeof(UltraCombo))
                ultraCombo = (UltraCombo)@this.ActiveControl.Parent.Parent.Parent.Parent;
            return ultraCombo;
        }
        #endregion
        #endregion

        #region Event
        #region Event chung
        /// <summary>
        /// Hàm sự kiện RowSelected khi có sử dụng GetValueList để add Combo vào Grid
        /// </summary>
        private static void cbbAutoFilter_RowSelected(this Form @this, object sender, RowSelectedEventArgs e)
        {
            if (e.Row == null) return;
            //if (e.Row.Selected) _isKeyPress = false;
            //if (_isKeyPress) _isKeyPress = false;
            var ultraCombo = (UltraCombo)sender;
            //if (e.Row.Selected) ultraCombo.SetValueForTag("KeyPress", false);
            //if (ultraCombo.GetValueFromTag<bool>("KeyPress") == true) ultraCombo.SetValueForTag("KeyPress", false);
            if (ultraCombo.DataSource.GetType() == typeof(BindingList<AccountingObject>) || ultraCombo.DataSource.GetType() == typeof(BindingList<MaterialGoodsCustom>))
            {
                if (ultraCombo.IsDroppedDown)
                    if (e.Row != null)
                    {
                        if (ultraCombo.DataSource.GetType() == typeof(BindingList<AccountingObject>))
                        {
                            var model = e.Row.ListObject as AccountingObject;
                            UltraGrid uGrid = FindGridIsActived(@this);//_tabControl.Tabs[tabIndex].TabPage.Controls.Find("uGrid" + tabIndex, true).FirstOrDefault();
                            if (uGrid != null)
                            {
                                uGrid.AutoFillAccoutingObject(model);
                            }
                        }
                        (sender as UltraCombo).ClearAutoComplete();
                    }
            }
        }
        /// <summary>
        /// Tự động điền thông tin liên quan của AccountingObject trong UltraGrid
        /// </summary>
        /// <param name="uGrid">UltraGrid</param>
        /// <param name="model">Đối tượng AccoutingObject</param>
        /// <param name="cell"></param>
        /// <param name="isFirst"></param>
        public static void AutoFillAccoutingObject(this UltraGrid uGrid, AccountingObject model, UltraGridCell cell = null, bool isFirst = false)
        {
            if (model == null) return;
            if (cell == null) cell = uGrid.ActiveCell;
            if (cell == null) return;
            int index = cell.Row.Index;
            var nameCol = cell.Column.Key.Split(new string[] { "AccountingObject", "ID" },
                                                StringSplitOptions
                                                    .RemoveEmptyEntries);
            if (cell.Column.Key.Contains("AccountingObject") && cell.Column.Key.Contains("ID"))
            {
                if (
                    cell.Column.Key.Split(new string[] { "AccountingObject" },
                                          StringSplitOptions.RemoveEmptyEntries).Length > 1)
                {
                    if (uGrid.DisplayLayout.Bands[0].Columns.Exists(nameCol[0] + "AccountingObject" + (nameCol.Length > 1 ? nameCol[1] : string.Empty) + "Name"))
                    {
                        if ((string)uGrid.Rows[index].Cells[nameCol[0] + "AccountingObject" + (nameCol.Length > 1 ? nameCol[1] : string.Empty) + "Name"].Value == string.Empty && isFirst) { }
                        else
                            uGrid.Rows[index].Cells[nameCol[0] + "AccountingObject" + (nameCol.Length > 1 ? nameCol[1] : string.Empty) + "Name"].Value = model.AccountingObjectName;
                    }
                    if (
                        uGrid.DisplayLayout.Bands[0].Columns.Exists(nameCol[0] + "AccountingObject" +
                                                                    (nameCol.Length > 1 ? nameCol[1] : string.Empty) +
                                                                    "Address") && !isFirst)
                        uGrid.Rows[index].Cells[
                            nameCol[0] + "AccountingObject" + (nameCol.Length > 1 ? nameCol[1] : string.Empty) +
                            "Address"].Value = model.Address;
                }
                else
                {
                    if (uGrid.DisplayLayout.Bands[0].Columns.Exists("AccountingObject" + (nameCol.Length > 0 ? nameCol[0] : string.Empty) + "Name"))
                    {
                        if ((string)uGrid.Rows[index].Cells["AccountingObject" + (nameCol.Length > 0 ? nameCol[0] : string.Empty) + "Name"].Value == string.Empty && isFirst) { }
                        else
                            uGrid.Rows[index].Cells["AccountingObject" + (nameCol.Length > 0 ? nameCol[0] : string.Empty) + "Name"].Value = model.AccountingObjectName;
                    }
                    if (
                        uGrid.DisplayLayout.Bands[0].Columns.Exists("AccountingObject" +
                                                                    (nameCol.Length > 0 ? nameCol[0] : string.Empty) +
                                                                    "Address") && !isFirst)
                        uGrid.Rows[index].Cells[
                            "AccountingObject" + (nameCol.Length > 0 ? nameCol[0] : string.Empty) + "Address"].Value =
                            model.Address;
                }
                if (uGrid.DisplayLayout.Bands[0].Columns.Exists("CompanyTaxCode"))
                {
                    if (!isFirst)
                        uGrid.Rows[index].Cells["CompanyTaxCode"].Value = model.TaxCode;
                    bool isError = false;
                    if (uGrid.Rows[index].Cells["CompanyTaxCode"].Value != null && !string.IsNullOrEmpty(uGrid.Rows[index].Cells["CompanyTaxCode"].Value.ToString()))
                    {
                        isError = !Accounting.Core.Utils.CheckMST(uGrid.Rows[index].Cells["CompanyTaxCode"].Value.ToString());
                        if (isError)
                            NotificationCell(uGrid, uGrid.Rows[index].Cells["CompanyTaxCode"], resSystem.MSG_System_44);
                        else
                            RemoveNotificationCell(uGrid, uGrid.Rows[index].Cells["CompanyTaxCode"]);
                    }
                    uGrid.Rows[index].Cells["CompanyTaxCode"].SetErrorForCell(isError);
                }
            }
        }

        /// <summary>
        /// Sự kiên RowSelected cho cbbAutoFilter trong thường
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void cbbAutoFilter_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (e.Row == null) return;
            var ultraCombo = (UltraCombo)sender;
            if (ultraCombo.DataSource.GetType() == typeof(BindingList<AccountingObject>) || ultraCombo.DataSource.GetType() == typeof(BindingList<MaterialGoodsCustom>))
            {
                if (ultraCombo.IsDroppedDown)
                    if (e.Row != null)
                    {
                        (sender as UltraCombo).ClearAutoComplete();
                    }
            }
        }
        /// <summary>
        /// Sự kiên RowSelected cho cbbAutoFilter bình thường
        /// </summary>
        /// <typeparam name="T"></typeparam> 
        /// <typeparam name="TK"></typeparam>
        /// <param name="this"></param>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void cbbAutoFilter_RowSelected<T, TK>(this Form @this, object sender, RowSelectedEventArgs e)
        {
            if (e.Row == null) return;
            T model = (T)e.Row.ListObject;
            var @base = @this as DetailBase<TK>;
            if (@base != null && !@base._statusForm.Equals(ConstFrm.optStatusForm.View))
            {
                var combo = (UltraCombo)sender;
                if (combo.IsDroppedDown)
                {
                    model.ExcuteRowSelectedEventOfcbbAutoFilter(@this, combo, e.Row.Selected);
                    combo.ClearAutoComplete();
                }
                if (@base.ComboAutoFilterRowSelected != null) @base.ComboAutoFilterRowSelected(sender, e);
            }
        }

        /// <summary>
        /// Sự kiên RowSelected cho Combo AccountingObject có kèm Combo AccountingObjectBank
        /// </summary>
        /// <typeparam name="TK"></typeparam>
        /// <param name="this"> </param>
        /// <param name="combo">Combo AccountingObjectBank</param>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void cbbAutoFilter_RowSelected<TK>(this Form @this, UltraCombo @combo, object sender, RowSelectedEventArgs e)
        {
            if (e.Row == null) return;
            var ultraCombo = (UltraCombo)sender;
            if (!ultraCombo.IsDroppedDown) return;
            var model = (AccountingObject)e.Row.ListObject;
            var @base = @this as DetailBase<TK>;
            if (@base != null && !@base._statusForm.Equals(ConstFrm.optStatusForm.View))
            {
                model.ExcuteRowSelectedEventOfcbbAutoFilter(@this, (UltraCombo)sender, e.Row.Selected);
            }
            new BindingList<AccountingObjectBankAccount>();
            var listAccountingObjectBank = new BindingList<AccountingObjectBankAccount>(IAccountingObjectBankAccountService.GetByAccountingObjectID(model.ID));
            IAccountingObjectBankAccountService.UnbindSession(listAccountingObjectBank);
            listAccountingObjectBank = new BindingList<AccountingObjectBankAccount>(IAccountingObjectBankAccountService.GetByAccountingObjectID(model.ID));
            var dataBinding = (TK)@combo.DataBindings[0].DataSource;
            string nameBindingAccountingObjectBank = @combo.DataBindings[0].BindingMemberInfo.BindingMember;
            var dataSourceUpdateMode = @combo.DataBindings[0].DataSourceUpdateMode;
            //@combo.Text = string.Empty;
            @this.ConfigCombo(listAccountingObjectBank, @combo, "BankAccount", "ID",
                              dataBinding, nameBindingAccountingObjectBank, dataSourceUpdateMode, null, false);
            if (@base != null && !@base._statusForm.Equals(ConstFrm.optStatusForm.View))
            {
                PropertyInfo propId = dataBinding.GetType().GetProperty(nameBindingAccountingObjectBank);
                if (propId != null && propId.CanWrite)
                    propId.SetValue(dataBinding, null, null);
                PropertyInfo propName = dataBinding.GetType().GetProperty("AccountingObjectBankName");
                if (propName != null && propName.CanWrite)
                    propName.SetValue(dataBinding, string.Empty, null);
            }
            ultraCombo.ClearAutoComplete();
        }

        /// <summary>
        /// Sự kiên RowSelected cho Combo CreditCard
        /// </summary>
        /// <param name="this"> </param>
        /// <param name="controls"></param>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void
            cbbAutoFilter_RowSelected(this Form @this, List<Control> controls, object sender, RowSelectedEventArgs e)
        {
            if (e.Row == null) return;
            if (!((UltraCombo)sender).IsDroppedDown) return;
            var model = (CreditCard)e.Row.ListObject;
            model.ExcuteRowSelectedEventOfcbbAutoFilter(@this, (UltraCombo)sender, e.Row.Selected, controls);
        }

        /// <summary>
        /// Thực thi sự kiện RowSelected của cá Combo
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="model"></param>
        /// <param name="this"> </param>
        /// <param name="ultraCombo"></param>
        /// <param name="isSelected"></param>
        /// <param name="controls"> </param>
        private static void ExcuteRowSelectedEventOfcbbAutoFilter<T>(this T @model, Form @this, UltraCombo ultraCombo, bool isSelected, List<Control> controls = null)
        {
            if (model == null) return;
            //if (isSelected) _isKeyPress = false;
            //if (_isKeyPress) _isKeyPress = false;
            var comboActive = @this.FindComboIsActived();
            //if (isSelected) ultraCombo.SetValueForTag("KeyPress", false);
            //if (ultraCombo.GetValueFromTag<bool>("KeyPress") == true) ultraCombo.SetValueForTag("KeyPress", false);
            if (comboActive != null && comboActive.Name.Equals(ultraCombo.Name))// && ultraCombo.Focused
            {
                if (ultraCombo.DataBindings.Count > 0)
                {
                    string nameDataBinding = ultraCombo.DataBindings[0].BindingMemberInfo.BindingMember;
                    var input = ultraCombo.DataBindings[0].DataSource;
                    var propInfo = input.GetType().GetProperties();
                    foreach (var propItem in propInfo)
                    {
                        if (propItem != null && propItem.CanWrite)
                        {
                            if (propItem.Name.Equals(nameDataBinding.Replace("ID", "Name")))
                            {
                                if (model.GetType() == typeof(AccountingObject))
                                {
                                    var acc = (AccountingObject)Convert.ChangeType(model, typeof(AccountingObject));
                                    if (acc != null) propItem.SetValue(input, acc.AccountingObjectName, null);
                                }
                                else if (model.GetType() == typeof(MaterialGoodsCustom))
                                {
                                    var materialGoods = (MaterialGoodsCustom)Convert.ChangeType(model, typeof(MaterialGoodsCustom));
                                    if (materialGoods != null)
                                        propItem.SetValue(input, materialGoods.MaterialGoodsName, null);
                                }
                            }
                            else if (propItem.Name.Equals(nameDataBinding.Replace("ID", "Code")))
                            {
                                if (model.GetType() == typeof(AccountingObject))
                                {
                                    var acc = (AccountingObject)Convert.ChangeType(model, typeof(AccountingObject));
                                    if (acc != null) propItem.SetValue(input, acc.AccountingObjectCode, null);
                                }
                            }
                            else if (propItem.Name.Equals(nameDataBinding.Replace("ID", "Address")))
                            {
                                if (model.GetType() == typeof(AccountingObject))
                                {
                                    var acc = (AccountingObject)Convert.ChangeType(model, typeof(AccountingObject));
                                    if (acc != null) propItem.SetValue(input, acc.Address, null);
                                }
                            }
                            else if (propItem.Name.Contains("TaxCode"))
                            {
                                if (model.GetType() == typeof(AccountingObject))
                                {
                                    var acc = (AccountingObject)Convert.ChangeType(model, typeof(AccountingObject));
                                    if (acc != null) propItem.SetValue(input, acc.TaxCode, null);
                                }
                            }
                            else if (propItem.Name.Contains("BankName"))
                            {
                                if (model.GetType().Name.Equals(typeof(AccountingObjectBankAccount).Name))
                                {
                                    if (propItem.Name.Contains("AccountingObject"))
                                    {
                                        var bank = (AccountingObjectBankAccount)Convert.ChangeType(model, typeof(AccountingObjectBankAccount));
                                        if (bank != null) propItem.SetValue(input, bank.BankName, null);
                                    }
                                }
                                else if (model.GetType().Name.Equals(typeof(BankAccountDetail).Name))
                                {
                                    if (!propItem.Name.Contains("AccountingObject"))
                                    {
                                        var bank = (BankAccountDetail)Convert.ChangeType(model, typeof(BankAccountDetail));
                                        if (bank != null) propItem.SetValue(input, bank.BankName, null);
                                    }
                                }
                            }
                            else if (propItem.Name.Contains("Payer"))
                            {
                                if (model.GetType() == typeof(AccountingObject))
                                {
                                    var acc = (AccountingObject)Convert.ChangeType(model, typeof(AccountingObject));
                                    if (acc != null) propItem.SetValue(input, acc.ContactName, null);
                                }
                            }
                            else if (propItem.Name.Contains("Department"))
                            {
                                if (model.GetType() == typeof(AccountingObject))
                                {
                                    if (propItem.Name.Equals(nameDataBinding.Replace("AccountingObjectID", "Department")))
                                    {
                                        var acc = (AccountingObject)Convert.ChangeType(model, typeof(AccountingObject));
                                        propItem.SetValue(input, acc != null && acc.Department != null ? acc.Department.DepartmentName : "", null);
                                    }
                                }
                            }
                            else if (propItem.Name.Contains("RepositoryID"))
                            {
                                if (model.GetType() == typeof(MaterialGoodsCustom))
                                {
                                    var materialGoods = (MaterialGoodsCustom)Convert.ChangeType(model, typeof(MaterialGoodsCustom));
                                    if (materialGoods != null)
                                        propItem.SetValue(input, materialGoods.RepositoryID, null);
                                }
                            }
                            else if (propItem.Name.Contains("MaterialGoodsName"))
                            {
                                if (model.GetType() == typeof(MaterialGoodsCustom))
                                {
                                    var materialGoods = (MaterialGoodsCustom)Convert.ChangeType(model, typeof(MaterialGoodsCustom));
                                    if (materialGoods != null)
                                        propItem.SetValue(input, materialGoods.MaterialGoodsName, null);
                                }
                            }
                            else if (propItem.Name.Equals("Unit"))
                            {
                                if (model.GetType() == typeof(MaterialGoodsCustom))
                                {
                                    var materialGoods = (MaterialGoodsCustom)Convert.ChangeType(model, typeof(MaterialGoodsCustom));
                                    if (materialGoods != null) propItem.SetValue(input, materialGoods.Unit, null);
                                }
                            }
                        }
                    }
                }
            }
            if (controls != null)
            {
                if (typeof(T) != typeof(CreditCard)) return;
                var creditCard = (CreditCard)Convert.ChangeType(model, typeof(CreditCard));
                if (creditCard != null)
                {
                    if (controls[0] is UltraPictureBox)
                    {
                        Size sizeImg = new Size(0, 0);
                        controls[0].Visible = true;
                        ((UltraPictureBox)controls[0]).BorderStyle = UIElementBorderStyle.None;
                        if (creditCard.CreditCardType.Contains("VisaCard"))
                        {
                            ((UltraPictureBox)controls[0]).Image = Properties.Resources.visacard;
                            sizeImg = Properties.Resources.visacard.Size;
                        }
                        else if (creditCard.CreditCardType.Contains("MasterCard"))
                        {
                            ((UltraPictureBox)controls[0]).Image = Properties.Resources.mastercard;
                            sizeImg = Properties.Resources.mastercard.Size;
                        }
                        else if (creditCard.CreditCardType.Contains("Discover"))
                        {
                            ((UltraPictureBox)controls[0]).Image = Properties.Resources.discover_card;
                            sizeImg = Properties.Resources.discover_card.Size;
                        }
                        else if (creditCard.CreditCardType.Contains("Diners Club"))
                        {
                            ((UltraPictureBox)controls[0]).Image = Properties.Resources.DinersClub;
                            sizeImg = Properties.Resources.DinersClub.Size;
                        }
                        else if (creditCard.CreditCardType.Contains("American Express"))
                        {
                            ((UltraPictureBox)controls[0]).Image = Properties.Resources.american_express;
                            sizeImg = Properties.Resources.american_express.Size;
                        }
                        controls[0].BackColor = Color.Transparent;
                        ((UltraPictureBox)controls[0]).ImageTransparentColor = Color.Transparent;
                        controls[0].Width = sizeImg.Width / (sizeImg.Height / controls[0].Height);
                    }
                    controls[1].Text = creditCard.CreditCardType;
                    controls[1].Location = new Point(controls[0].Location.X + controls[0].Width + 5, controls[1].Location.Y);
                    controls[2].Text = creditCard.OwnerCard;
                }
            }
        }
        private static void editorButton_ElementClick(object sender, UIElementEventArgs e)
        {

        }

        /// <summary>
        /// Sự kiện của combo MaterialGoods, truyền dữ liệu sang các Cell khác cùng dòng trong Grid
        /// </summary>
        /// <typeparam name="TK">Đối tượng của Form</typeparam> 
        /// <param name="this">Form hiện thời</param>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void cbbMaterialGoods_RowSelected<TK>(this Form @this, object sender, RowSelectedEventArgs e)
        {
            if (e.Row == null) return;
            MaterialGoodsCustom model = (MaterialGoodsCustom)e.Row.ListObject;
            //UltraGrid uGrid = @this.FindGridIsActived();
            //if (uGrid != null)
            //{
            //    if (uGrid.ActiveCell != null && uGrid.DisplayLayout.Bands[0].Columns.Exists("Description") && uGrid.DisplayLayout.Bands[0].Columns.Exists("UnitPrice"))
            //    {
            //        UltraGridCell cell = uGrid.ActiveCell;
            //        cell.Row.Cells["Description"].Value = model.MaterialGoodsName;
            //        if (@this.GetType().BaseType != null &&
            //            @this.GetType().BaseType.BaseType.Name.Contains(typeof(DetailBase<TK>).Name))
            //        {
            //            int? typeGroup = ((DetailBase<TK>)@this).TypeGroup;
            //            if (typeGroup != null)
            //            {
            //                if (PurchaseTypeGroup.Any(p => p.Equals(typeGroup)))
            //                {
            //                    cell.Row.Cells["UnitPriceOriginal"].Value = model.PurchasePrice;
            //                }
            //                else if (SaleTypeGroup.Any(p => p.Equals(typeGroup)))
            //                {
            //                    cell.Row.Cells["UnitPriceOriginal"].Value = model.SalePrice;
            //                    sourceUnitPrice[0] = sourceUnitPrice[0] ?? model.SalePrice;
            //                    sourceUnitPrice[1] = sourceUnitPrice[1] ?? model.SalePrice2;
            //                    sourceUnitPrice[2] = sourceUnitPrice[2] ?? model.SalePrice3;
            //                }
            //                cell.Row.UpdateData();
            //                uGrid.PerformAction(UltraGridAction.ExitEditMode, false, false);
            //                @this.CalculateAmount<TK>(uGrid, cell.Row.Cells["UnitPriceOriginal"]);
            //            }
            //            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("RepositoryID"))
            //                cell.Row.Cells["RepositoryID"].Value = model.RepositoryID;
            //            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("Unit"))
            //                cell.Row.Cells["Unit"].Value = model.Unit;
            //        }
            //    }
            //}
        }
        private static void cbbMaterialGoods_RowSelected(object sender, RowSelectedEventArgs e)
        {

        }


        private static void cbbSAQuote_RowSelected<TK>(this Form @this, object sender, RowSelectedEventArgs e)
        {
            if (e.Row != null)
            {
                SAQuote sa = (SAQuote)e.Row.ListObject;
                var @base = @this as DetailBase<TK>;
                if (@base != null)
                {
                    List<IList> listObjectInput = @base._listObjectInput;
                    UltraGrid uGrid = (UltraGrid)@this.Controls.Find("uGrid0", true).FirstOrDefault();
                    FchoseSaQuote frm = new FchoseSaQuote(listObjectInput, sa, uGrid);
                    frm.FormClosed += new FormClosedEventHandler(FchoseSaQuote_FormClosed);
                    frm.ShowDialog();
                }
            }
        }

        private static void FchoseSaQuote_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private static void cbbCurrency_RowSelected(this Form @this, object sender, RowSelectedEventArgs e)
        {
            if (e.Row != null)
            {
                var model = (Currency)e.Row.ListObject;
                if (((UltraCombo)sender).IsDroppedDown)
                    @this.AutoExchangeByCurrencySelected(model);
            }
        }

        /// <summary>
        /// Hàm tự động tính quy đổi khi thay đổi cột loại tiền tệ cho các Grid con
        /// </summary>
        /// <param name="this"> </param>
        /// <param name="currency"></param>
        /// <param name="exchangeRate"> </param>
        public static void AutoExchangeByCurrencySelected(this Form @this, Currency currency, decimal? exchangeRate = null)
        {
            if (currency == null) return;
            if (exchangeRate == null) exchangeRate = currency.ExchangeRate == null ? 1 : (decimal)currency.ExchangeRate;

            var tabControl =
                (UltraTabControl)@this.Controls.Find("ultraTabControl", true).FirstOrDefault();
            if (tabControl != null)
            {
                foreach (UltraTab tab in tabControl.Tabs)
                {
                    int index = tab.Index;
                    var uGrid = (UltraGrid)tabControl.Tabs[index].TabPage.Controls.Find("uGrid" + index, true).FirstOrDefault();
                    //uGrid.UpdateData();
                    if (uGrid == null) continue;
                    uGrid.UpdateDataGrid();
                    bool haveCrurrency = uGrid.DisplayLayout.Bands[0].Columns.Exists("CurrencyID");
                    var lstNameColumn = new List<string>();
                    //bool isHidden = false;
                    foreach (UltraGridColumn column in uGrid.DisplayLayout.Bands[0].Columns)
                    {
                        if (column.Key.Equals("ExchangeRate"))
                        {
                            lstNameColumn.Add(column.Key);
                        }
                        else if ((column.Key.Contains("Amount") || column.Key.Contains("UnitPrice") || column.Key.Contains("OrgPrice")) && !column.Key.Contains("Original"))
                        {
                            if (uGrid.DisplayLayout.Bands[0].Columns.Exists(column.Key + "Original") && !uGrid.DisplayLayout.Bands[0].Columns[column.Key + "Original"].Hidden)
                                lstNameColumn.Add(column.Key);
                            else if (uGrid.DisplayLayout.Bands[0].Columns.Exists(column.Key + "Original") && (column.Key.Contains("VAT") || column.Key.Contains("Pretax")))
                                lstNameColumn.Add(column.Key);
                        }
                    }
                    foreach (string columnName in lstNameColumn)
                    {
                        ConfigColumnByCurrency(uGrid, columnName, currency.ID);
                    }
                    @this.ExchangeByCurrency(uGrid, haveCrurrency, lstNameColumn, (decimal)exchangeRate, currency.ID);
                }
            }
            else
            {
                UltraGrid uGrid = (UltraGrid)@this.Controls.Find("uGrid0", true).FirstOrDefault();
                if (uGrid == null) return;
                bool haveCrurrency = uGrid.DisplayLayout.Bands[0].Columns.Exists("CurrencyID");
                List<string> lstNameColumn = new List<string>();
                foreach (UltraGridColumn column in uGrid.DisplayLayout.Bands[0].Columns)
                {
                    if (column.Key.Equals("ExchangeRate"))
                    {
                        ConfigColumnByCurrency(uGrid, column.Key, currency.ID);
                    }
                    else if (column.Key.Contains("Amount") && !column.Key.Contains("AmountOriginal") && !column.Key.Contains("AutoOWAmountCal"))
                    {
                        ConfigColumnByCurrency(uGrid, column.Key, currency.ID);
                        lstNameColumn.Add(column.Key);
                    }
                }
                @this.ExchangeByCurrency(uGrid, haveCrurrency, lstNameColumn, (decimal)exchangeRate, currency.ID);
            }
        }

        /// <summary>
        /// Hàm Update dữ liệu Grid xuống đối tượng
        /// </summary>
        /// <param name="uGrid"></param>
        public static void UpdateDataGrid(this UltraGrid uGrid)
        {
            try
            {
                foreach (UltraGridRow row in uGrid.Rows)
                {
                    row.UpdateData();
                }
            }
            catch (Exception) { }
        }

        /// <summary>
        /// Hàm tính quy đổi theo loại tiền tệ [Hàm chính]
        /// </summary>
        /// <param name="this"> </param>
        /// <param name="uGrid"></param>
        /// <param name="haveCurrency"></param>
        /// <param name="lstNameColumn"></param>
        /// <param name="exchangeRate"></param>
        /// <param name="currency"></param>
        /// <param name="currentRow"> </param>
        private static void ExchangeByCurrency(this Form @this, UltraGrid uGrid, bool haveCurrency, IEnumerable<string> lstNameColumn, decimal exchangeRate, string currency, UltraGridRow currentRow = null)
        {
            if (haveCurrency)
            {
                UltraGridRow row = currentRow;
                if (row == null)
                {
                    if (uGrid.ActiveRow != null) row = uGrid.ActiveRow;
                    else if (uGrid.ActiveCell != null) row = uGrid.ActiveCell.Row;
                }
                if (row != null)
                {
                    foreach (string columnName in lstNameColumn.Where(p => !p.Contains("ExchangeRate")))
                    {
                        row.Cells[columnName].Value =
                            row.Cells[columnName + "Original"].Value == null
                                ? row.Cells[columnName].Value
                                : (decimal)row.Cells[columnName + "Original"].Value * exchangeRate;
                        row.Cells[columnName].CheckMaxValueByMoney();
                        UltraGridBand band = uGrid.DisplayLayout.Bands[0];
                        band.Columns[columnName].FormatNumberic(ConstDatabase.Format_TienVND);
                        band.Columns[columnName + "Original"].FormatNumberic(currency.Equals("VND")
                                    ? ConstDatabase.Format_TienVND
                                    : ConstDatabase.Format_ForeignCurrency);
                        if (band.Summaries.Exists("Sum" + columnName))
                        {
                            SummarySettings summary = band.Summaries["Sum" + columnName];
                            string format = GetTypeFormatNumberic(ConstDatabase.Format_TienVND);
                            summary.DisplayFormat = "{0:" + format + ";(" + format + ")}";
                        }
                        if (band.Summaries.Exists("Sum" + columnName + "Original"))
                        {
                            SummarySettings summary = band.Summaries["Sum" + columnName + "Original"];
                            string format = GetTypeFormatNumberic(currency.Equals("VND")
                                    ? ConstDatabase.Format_TienVND
                                    : ConstDatabase.Format_ForeignCurrency);
                            summary.DisplayFormat = "{0:" + format + ";(" + format + ")}";
                        }
                    }
                    if (uGrid.DisplayLayout.Bands[0].Columns.Exists("ExchangeRate"))
                        row.Cells["ExchangeRate"].Value = exchangeRate;
                    row.UpdateData();
                }
            }
            else
            {
                foreach (string columnName in lstNameColumn)
                {
                    if (columnName != "ExchangeRate")
                        foreach (UltraGridRow row in uGrid.Rows)
                        {
                            row.Cells[columnName].Value =
                            row.Cells[columnName + "Original"].Value == null
                                ? row.Cells[columnName].Value
                                : (decimal)row.Cells[columnName + "Original"].Value * exchangeRate;
                            row.Cells[columnName].CheckMaxValueByMoney();
                            row.UpdateData();
                        }
                    UltraGridBand band = uGrid.DisplayLayout.Bands[0];
                    band.Columns[columnName].FormatNumberic(ConstDatabase.Format_TienVND);
                    band.Columns[columnName + "Original"].FormatNumberic(currency.Equals("VND")
                                    ? ConstDatabase.Format_TienVND
                                    : ConstDatabase.Format_ForeignCurrency);
                    if (band.Summaries.Exists("Sum" + columnName))
                    {
                        SummarySettings summary = band.Summaries["Sum" + columnName];
                        string format = GetTypeFormatNumberic(ConstDatabase.Format_TienVND);
                        summary.DisplayFormat = "{0:" + format + ";(" + format + ")}";
                    }
                    if (band.Summaries.Exists("Sum" + columnName + "Original"))
                    {
                        SummarySettings summary = band.Summaries["Sum" + columnName + "Original"];
                        string format = GetTypeFormatNumberic(currency.Equals("VND")
                                ? ConstDatabase.Format_TienVND
                                : ConstDatabase.Format_ForeignCurrency);
                        summary.DisplayFormat = "{0:" + format + ";(" + format + ")}";
                    }
                }
                UltraGrid uGridControl = FindGridIsActived(@this);
                if (uGridControl != null && uGridControl.DisplayLayout.Bands[0].Columns.Exists("CurrencyID"))
                {
                    if (uGridControl.DisplayLayout.Bands[0].Columns.Exists("TotalAmountOriginal") &&
                        uGridControl.DisplayLayout.Bands[0].Columns.Exists("ExchangeRate"))
                    {
                        uGridControl.Rows[0].Cells["ExchangeRate"].Column.Hidden = currency.Equals("VND");   //Tỷ giá  
                        uGridControl.Rows[0].Cells["ExchangeRate"].Value = exchangeRate;
                        if (!uGridControl.DisplayLayout.Bands[0].Columns["TotalAmountOriginal"].Hidden)
                        {
                            uGridControl.Rows[0].Cells["TotalAmount"].Column.Hidden = currency.Equals("VND");   //Quy đổi
                            uGridControl.Rows[0].Cells["TotalAmount"].Value =
                            uGridControl.Rows[0].Cells["TotalAmountOriginal"].Value == null
                                ? 0
                                : (decimal)uGridControl.Rows[0].Cells["TotalAmountOriginal"].Value * exchangeRate;
                            uGridControl.Rows[0].Cells["TotalAmount"].CheckMaxValueByMoney();
                            uGridControl.DisplayLayout.Bands[0].Columns["TotalAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                            uGridControl.DisplayLayout.Bands[0].Columns["TotalAmountOriginal"].FormatNumberic(
                                currency.Equals("VND")
                                    ? ConstDatabase.Format_TienVND
                                    : ConstDatabase.Format_ForeignCurrency);
                        }
                        uGridControl.ConfigSizeGridCurrency(@this);
                    }
                }
            }
            uGrid.UpdateDataGrid();
        }

        /// <summary>
        /// Tính quy đổi cho 1 Grid xác định
        /// </summary>
        /// <param name="uGrid"></param>
        /// <param name="this"> </param>
        /// <param name="currency"></param>
        /// <param name="currentRow"> </param>
        public static void ExchangeCurrencyInSingleGrid(this UltraGrid uGrid, Form @this, Currency currency = null, UltraGridRow currentRow = null)
        {
            List<string> lstNameColumn = new List<string>();
            decimal exchangeRate = 1;
            bool haveRate = false;
            foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
            {
                if (column.Key.Contains("Amount") && !column.Key.Contains("AmountOriginal") && !column.Key.Contains("AutoOWAmountCal"))
                    lstNameColumn.Add(column.Key);
                else if (column.Key.Equals("ExchangeRate"))
                    haveRate = true;
            }
            if (haveRate)
            {
                currentRow = currentRow ?? ((uGrid.ActiveCell != null)
                                       ? uGrid.ActiveCell.Row
                                       : uGrid.ActiveRow);
                if (currentRow != null)
                    exchangeRate = currentRow.Cells["ExchangeRate"].Value == null
                                   ? 1
                                   : (decimal)currentRow.Cells["ExchangeRate"].Value;
                else if (uGrid.Name != "uGridControl")
                {
                    foreach (var row in uGrid.Rows)
                    {
                        uGrid.ExchangeCurrencyInSingleGrid(@this, currentRow: row);
                        lstNameColumn.Add("ExchangeRate");
                        foreach (var columnName in lstNameColumn.Where(columnName => uGrid.DisplayLayout.Bands[0].Columns.Exists("CurrencyID")))
                        {
                            uGrid.ConfigColumnByCurrency(columnName, (string)row.Cells["CurrencyID"].Value);
                        }
                    }
                    return;
                }
            }
            else
            {
                currency = currency ?? ListCurrency.FirstOrDefault(
                    p => p.ID == uGrid.Rows[0].Cells["CurrencyID"].Value.ToString());
                exchangeRate = currency == null
                                   ? 1
                                   : (currency.ExchangeRate == null ? 1 : (decimal)currency.ExchangeRate);
            }
            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("CurrencyID"))
                @this.ExchangeByCurrency(uGrid, true, lstNameColumn, exchangeRate,
                                   uGrid.Rows[0].Cells["CurrencyID"].Value == null
                                       ? "VND"
                                       : uGrid.Rows[0].Cells["CurrencyID"].Value.ToString());
        }
        #endregion

        /// <summary>
        /// Kiểm tra giới hạn kiểu tiền tệ cho 1 Cell
        /// </summary>
        /// <param name="cell"></param>
        /// <returns></returns>
        public static bool CheckMaxValueByMoney(this UltraGridCell cell)
        {
            bool isError = false;
            var uGrid = (UltraGrid)cell.Row.Band.Layout.Grid;
            if (cell.Value != null && ((decimal)cell.Value > (decimal)(Math.Pow(2, 63) - 1) / 10000 || (decimal)cell.Value < (decimal)-Math.Pow(2, 63) / 10000))
                isError = true;
            if (isError)
                NotificationCell(uGrid, cell, resSystem.MSG_System_50);
            else
                RemoveNotificationCell(uGrid, cell);
            cell.SetErrorForCell(isError);
            return isError;
        }

        #region Event Grid

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TK"></typeparam>
        /// <param name="this"></param>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static void uGrid_AfterCellUpdate<TK>(this Form @this, object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            if (e.Cell == null) return;
            if (e.Cell.ValueListResolved != null)
            {
                var combo = (UltraCombo)e.Cell.ValueListResolved;
                if (combo.DataSource.GetType().FullName.GetObjectNameFromBindingName().Equals(typeof(MaterialGoodsCustom).Name))
                {
                    if (combo.SelectedRow != null)
                    {
                        var model = (MaterialGoodsCustom)combo.SelectedRow.ListObject;
                        var uGrid = (UltraGrid)sender;
                        if (uGrid != null)
                        {
                            UltraGridBand band = uGrid.DisplayLayout.Bands[0];
                            if (uGrid.ActiveCell != null && uGrid.DisplayLayout.Bands[0].Columns.Exists("Description"))
                            {
                                UltraGridCell cell = uGrid.ActiveCell;
                                cell.Row.Cells["Description"].Value = model.MaterialGoodsName;
                                if (!uGrid.DisplayLayout.Bands[0].Columns.Exists("UnitPrice")) return;
                                var baseType = @this.GetType().BaseType;
                                if (baseType != null && (baseType.BaseType != null && baseType.BaseType.Name.Contains(typeof(DetailBase<TK>).Name)))
                                {
                                    var @base = @this as DetailBase<TK>;
                                    if (@base != null)
                                    {
                                        var typeGroup = @base.TypeGroup;
                                        if (typeGroup != null)
                                        {
                                            if (PurchaseTypeGroup.Any(p => p.Equals(typeGroup)))
                                            {
                                                cell.Row.Cells["UnitPriceOriginal"].Value = model.PurchasePrice;
                                            }
                                            else if (SaleTypeGroup.Any(p => p.Equals(typeGroup)))
                                            {
                                                cell.Row.Cells["UnitPriceOriginal"].Value = model.SalePrice;
                                                SourceUnitPrice[0] = SourceUnitPrice[0] ?? model.SalePrice;
                                                SourceUnitPrice[1] = SourceUnitPrice[1] ?? model.SalePrice2;
                                                SourceUnitPrice[2] = SourceUnitPrice[2] ?? model.SalePrice3;
                                            }
                                            cell.Row.UpdateData();
                                            uGrid.PerformAction(UltraGridAction.ExitEditMode, false, false);
                                            @this.CalculateAmount<TK>(uGrid, cell.Row.Cells["UnitPriceOriginal"]);
                                        }
                                    }
                                    if (band.Columns.Exists("RepositoryID"))
                                        cell.Row.Cells["RepositoryID"].Value = model.RepositoryID;
                                    if (band.Columns.Exists("FromRepositoryID"))
                                        cell.Row.Cells["FromRepositoryID"].Value = model.RepositoryID;
                                    if (band.Columns.Exists("Unit"))
                                        cell.Row.Cells["Unit"].Value = model.Unit;
                                    if (band.Columns.Exists("VATRate"))
                                        cell.Row.Cells["VATRate"].Value = model.TaxRate;

                                    #region Tính Vat cho Tab Thuế
                                    int index = cell.Row.Index;
                                    if (index < 0 && index >= uGrid.Rows.Count) return;
                                    decimal totalVat, totalVatOriginal = 0;
                                    if (band.Columns.Exists("DiscountRate") && band.Columns.Exists("DiscountAmount"))
                                    {
                                        totalVat = (cell.Row.Cells["Amount"].Value == null
                                                        ? 0
                                                        : (decimal)cell.Row.Cells["Amount"].Value) -
                                                   (cell.Row.Cells["DiscountAmount"].Value == null
                                                        ? 0
                                                        : (decimal)cell.Row.Cells["DiscountAmount"].Value);
                                        totalVatOriginal = (cell.Row.Cells["AmountOriginal"].Value == null
                                                                ? 0
                                                                : (decimal)cell.Row.Cells["AmountOriginal"].Value) -
                                                           (cell.Row.Cells["DiscountAmountOriginal"].Value == null
                                                                ? 0
                                                                : (decimal)cell.Row.Cells["DiscountAmountOriginal"].Value);
                                    }
                                    else
                                    {
                                        totalVat = cell.Row.Cells["Amount"].Value == null
                                                       ? 0
                                                       : (decimal)cell.Row.Cells["Amount"].Value;
                                        totalVatOriginal = cell.Row.Cells["AmountOriginal"].Value == null
                                                               ? 0
                                                               : (decimal)cell.Row.Cells["AmountOriginal"].Value;
                                    }
                                    if (@base != null)
                                    {
                                        Template template = GetMauGiaoDien(@base.TypeID, null);
                                        int countTabs = template.TemplateDetails.Count(p => p.TabIndex != 100);
                                        for (int i = 0; i < countTabs; i++)
                                        {
                                            UltraGrid ultraGrid =
                                                (UltraGrid)@this.Controls.Find("uGrid" + i, true).FirstOrDefault();
                                            if (ultraGrid != null && ultraGrid.DisplayLayout.Bands[0].Columns.Exists("VATAmount") && ultraGrid.DisplayLayout.Bands[0].Columns.Exists("VATRate"))
                                            {
                                                decimal vatRate = uGrid.Rows[index].Cells["VATRate"].Value != null
                                                                      ? (decimal)uGrid.Rows[index].Cells["VATRate"].Value / 100
                                                                      : 0;
                                                ultraGrid.Rows[index].Cells["VATAmount"].Value = totalVat * vatRate;
                                                ultraGrid.Rows[index].Cells["VATAmountOriginal"].Value = totalVatOriginal * vatRate;
                                                ultraGrid.Rows[index].Update();
                                                ultraGrid.CalculationVat(index);
                                            }
                                        }
                                    }

                                    #endregion
                                }
                            }
                        }
                    }
                }
                else if (combo.DataSource.GetType().FullName.GetObjectNameFromBindingName().Equals(typeof(FixedAsset).Name))
                {
                    if (combo.SelectedRow != null)
                    {
                        var model = (FixedAsset)combo.SelectedRow.ListObject;
                        var uGrid = (UltraGrid)sender;
                        if (uGrid != null)
                        {
                            UltraGridBand band = uGrid.DisplayLayout.Bands[0];
                            if (uGrid.ActiveCell != null && uGrid.DisplayLayout.Bands[0].Columns.Exists("Description"))
                            {
                                UltraGridCell cell = uGrid.ActiveCell;
                                cell.Row.Cells["Description"].Value = model.FixedAssetName;
                                //var baseType = @this.GetType().BaseType;
                                //if (baseType != null && (baseType.BaseType != null && baseType.BaseType.Name.Contains(typeof(DetailBase<TK>).Name)))
                                //{
                                var @base = @this as DetailBase<TK>;
                                if (@base != null)
                                {
                                    if (band.Columns.Exists("DepartmentID"))
                                        cell.Row.Cells["DepartmentID"].Value = model.DepartmentID;
                                    if (band.Columns.Exists("DebitAccount"))
                                    {
                                        cell.Row.Cells["DebitAccount"].Value = typeof(TK) == typeof(FADecrement) ? model.DepreciationAccount : model.OriginalPriceAccount;
                                        cell.Row.Cells["DebitAccount"].SetErrorForCell(uGrid.CheckAccount(cell.Row.Cells["DebitAccount"]));
                                    }
                                    if (band.Columns.Exists("CreditAccount"))
                                    {
                                        if (typeof(TK) == typeof(FADecrement))
                                        {
                                            cell.Row.Cells["CreditAccount"].Value = model.FAIncrementDebitAccount;
                                            cell.Row.Cells["CreditAccount"].SetErrorForCell(uGrid.CheckAccount(cell.Row.Cells["CreditAccount"]));
                                        }
                                    }
                                    if (band.Columns.Exists("AmountOriginal"))
                                        cell.Row.Cells["AmountOriginal"].Value = model.OriginalPrice;
                                    if (@base._select.HasProperty("ExchangeRate"))
                                    {
                                        var exRate = @base._select.GetProperty<TK, decimal?>("ExchangeRate");
                                        cell.Row.Cells["Amount"].Value = exRate != null ? model.OriginalPrice * exRate : model.OriginalPrice;
                                    }
                                    else
                                        cell.Row.Cells["Amount"].Value = model.OriginalPrice;
                                    cell.Row.CalculationOrgPrice(@base);
                                    cell.Row.CalculationImportTaxAmount(@base);
                                    cell.Row.CalculationSpecialConsumeTaxAmount(@base);
                                    cell.Row.CalculationVatAmount(@base);
                                    #region Tính Vat cho Tab Thuế
                                    int index = cell.Row.Index;
                                    if (index < 0 && index >= uGrid.Rows.Count) return;
                                    decimal totalVat, totalVatOriginal = 0;
                                    if (band.Columns.Exists("DiscountRate") && band.Columns.Exists("DiscountAmount"))
                                    {
                                        totalVat = (cell.Row.Cells["Amount"].Value == null
                                                                           ? 0
                                                                           : (decimal)cell.Row.Cells["Amount"].Value) -
                                                                      (cell.Row.Cells["DiscountAmount"].Value == null
                                                                           ? 0
                                                                           : (decimal)cell.Row.Cells["DiscountAmount"].Value);
                                        totalVatOriginal = (cell.Row.Cells["AmountOriginal"].Value == null
                                                                 ? 0
                                                                 : (decimal)cell.Row.Cells["AmountOriginal"].Value) -
                                                            (cell.Row.Cells["DiscountAmountOriginal"].Value == null
                                                                 ? 0
                                                                 : (decimal)cell.Row.Cells["DiscountAmountOriginal"].Value);
                                    }
                                    else
                                    {
                                        totalVat = cell.Row.Cells["Amount"].Value == null
                                                        ? 0
                                                        : (decimal)cell.Row.Cells["Amount"].Value;
                                        totalVatOriginal = cell.Row.Cells["AmountOriginal"].Value == null
                                                                ? 0
                                                                : (decimal)cell.Row.Cells["AmountOriginal"].Value;
                                    }
                                    //{
                                    Template template = GetMauGiaoDien(@base.TypeID, null);
                                    int countTabs = template.TemplateDetails.Count(p => p.TabIndex != 100);
                                    for (int i = 0; i < countTabs; i++)
                                    {
                                        UltraGrid ultraGrid =
                                            (UltraGrid)@this.Controls.Find("uGrid" + i, true).FirstOrDefault();
                                        if (ultraGrid != null && ultraGrid.DisplayLayout.Bands[0].Columns.Exists("VATAmount") && ultraGrid.DisplayLayout.Bands[0].Columns.Exists("VATRate"))
                                        {
                                            decimal vatRate = uGrid.Rows[index].Cells["VATRate"].Value != null
                                                                  ? (decimal)uGrid.Rows[index].Cells["VATRate"].Value / 100
                                                                  : 0;
                                            ultraGrid.Rows[index].Cells["VATAmount"].Value = totalVat * vatRate;
                                            ultraGrid.Rows[index].Cells["VATAmountOriginal"].Value = totalVatOriginal * vatRate;
                                            ultraGrid.Rows[index].Update();
                                            ultraGrid.CalculationVat(index);
                                        }
                                    }
                                    //}
                                    //Nếu là Ghi giảm TSCĐ thì nhân đôi dòng hiện tại
                                    if (typeof(TK) == typeof(FADecrement))
                                    {
                                        uGrid.AddNewRow4Grid();
                                        var newRow = uGrid.Rows[uGrid.Rows.Count - 1];
                                        foreach (var newCell in newRow.Cells.Cast<UltraGridCell>().Where(newCell => newCell.Column.Key != "FixedAssetID"))
                                        {
                                            newCell.Value = cell.Row.Cells[newCell.Column.Key].Value;
                                        }
                                        if (newRow.Band.Columns.Exists("DebitAccount"))
                                        {
                                            newRow.Cells["DebitAccount"].Value = 811;
                                            newRow.Cells["DebitAccount"].SetErrorForCell(uGrid.CheckAccount(newRow.Cells["DebitAccount"]));
                                        }
                                        if (newRow.Band.Columns.Exists("CreditAccount"))
                                        {
                                            newRow.Cells["CreditAccount"].Value = model.FAIncrementDebitAccount;
                                            newRow.Cells["CreditAccount"].SetErrorForCell(uGrid.CheckAccount(newRow.Cells["CreditAccount"]));
                                        }
                                    }

                                    #endregion
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TK">Đối tượng của Form</typeparam>
        /// <param name="this"></param>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void uGridControl_AfterCellUpdate<TK>(this Form @this, object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            var uGrid = (UltraGrid)sender;
            var @base = @this as DetailBase<TK>;
            if (@base != null)
            {
                var propertyManager = (PropertyManager)@this.BindingContext[@base._select];
                propertyManager.Position = uGrid.Rows[0].Index;
            }
        }

        static void uGrid_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {

        }

        static void uGrid_Layout(object sender, LayoutEventArgs e)
        {

        }

        public static void uGrid_CellDataError(object sender, CellDataErrorEventArgs e)
        {
            CellDataError((UltraGrid)sender);
            e.RaiseErrorEvent = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T">Đối tượng của Form</typeparam>
        /// <param name="this"> </param>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <param name="blackStandList"> </param>
        public static void uGrid_BeforeRowActivate<T>(this Form @this, object sender, RowEventArgs e, System.Type[] blackStandList = null)
        {
            if (e.Row.IsUnmodifiedTemplateAddRow)
            {
                if (blackStandList != null && blackStandList.Any(x => x == ((UltraGrid)sender).DataSource.GetType()))
                {
                    e.Row.Delete(false);
                    return;
                }
                BindingListAddNew<T>(@this, (UltraGrid)sender, e.Row.Index, ((DetailBase<T>)@this).TypeID);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T">ĐỐi tượng của Form</typeparam>
        /// <param name="this"> </param>
        /// <param name="uGrid"></param>
        /// <param name="index"></param>
        /// <param name="typeId"> </param>
        private static void BindingListAddNew<T>(this Form @this, UltraGrid uGrid, int index, int typeId)
        {
            if (index >= 0)
            {
                foreach (var cell in uGrid.Rows[index].Cells)
                {
                    if (cell.Column.Key.Contains("Rate") || cell.Column.Key.Contains("Amount") || cell.Column.Key.Contains("UnitPrice") || cell.Column.Key.Contains("OrgPrice"))
                        cell.Value = (decimal)0;
                    else if (cell.Column.Key.Contains("CurrencyID"))
                        cell.Value = "VND";
                    else if (cell.Column.Key.Contains("Date") && !cell.Column.Key.Equals("FinalDate"))
                        cell.Value = StringToDateTime(ConstFrm.DbStartDate);
                    else if (cell.Column.Key.Equals("InvoiceType"))
                        cell.Value = 2;
                    if (cell.Column.Key.Equals("UnitPrice") ||
                        cell.Column.Key.Equals("UnitPriceOriginal") || cell.Column.Key.Equals("Amount") ||
                        cell.Column.Key.Equals("AmountOriginal") || cell.Column.Key.Equals("VATAmount") ||
                        cell.Column.Key.Equals("VATAmountOriginal"))
                        cell.Value = (decimal)0;
                    else if (cell.Column.Key.Contains("Account") && !cell.Column.Key.Contains("BankAccount") && !cell.Column.Key.Contains("AccountingObject"))
                    {
                        bool isImportPurchase = false;
                        T select = ((DetailBase<T>)@this)._select;
                        if (select.HasProperty("IsImportPurchase"))
                            isImportPurchase = select.GetProperty<T, bool?>("IsImportPurchase") ?? false;
                        var dicDefaultAccount = IAccountDefaultService.DefaultAccount();
                        if (dicDefaultAccount.Any(p => p.Key == string.Format("{0};{1};{2}", typeId, cell.Column.Key, isImportPurchase)))
                        {
                            var keyDefaultAccount =
                                dicDefaultAccount.FirstOrDefault(p => p.Key == string.Format("{0};{1};{2}", typeId, cell.Column.Key, isImportPurchase));
                            if (keyDefaultAccount.Value != null && !string.IsNullOrEmpty(keyDefaultAccount.Value))
                            {
                                cell.Value = keyDefaultAccount.Value;
                            }
                        }
                        cell.SetErrorForCell(uGrid.CheckAccount(cell));
                    }
                    else if (cell.Column.Key.Equals("Quantity"))
                    {
                        cell.Value = index.Equals(0) ? (decimal)1 : (decimal)0;
                    }
                    else if (cell.Column.Key.Equals("Description"))
                    {
                        if (!uGrid.DisplayLayout.Bands[0].Columns.Exists("MaterialGoodsID") && !uGrid.DisplayLayout.Bands[0].Columns.Exists("FixedAssetID"))
                        {
                            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("VATAmount"))
                            {
                                cell.Value = resSystem.String_01;
                            }
                            else
                            {
                                if (index == 0)
                                {
                                    T select = ((DetailBase<T>)@this)._select;
                                    PropertyInfo propInfo = select.GetType().GetProperty("Reason");
                                    if (propInfo != null && propInfo.CanWrite)
                                        cell.Value = Convert.ChangeType(propInfo.GetValue(@select, null) ?? "", propInfo.PropertyType).ToString();
                                }
                                else
                                {
                                    if (GetIsCopyRow())
                                        cell.Value = uGrid.Rows[index - 1].Cells[cell.Column.Key].Value;
                                }
                            }
                        }
                    }
                    else if (cell.Column.Key.Equals("AccountingObjectID") && index == 0)
                    {
                        T select = ((DetailBase<T>)@this)._select;
                        PropertyInfo propInfo = select.GetType().GetProperty("AccountingObjectID");
                        if (propInfo != null && propInfo.CanWrite && propInfo.GetValue(select, null) != null)
                            cell.Value =
                                (Guid)propInfo.GetValue(select, null);
                    }
                    else if (cell.Column.Key.Equals("AccountingObjectName") && index == 0)
                    {
                        T select = ((DetailBase<T>)@this)._select;
                        PropertyInfo propInfo = select.GetType().GetProperty("AccountingObjectName");
                        if (propInfo != null && propInfo.CanWrite)
                            cell.Value = Convert.ChangeType(propInfo.GetValue(@select, null) ?? "", propInfo.PropertyType).ToString();
                    }
                    else if (cell.Column.Key.Equals("AccountingObjectAddress") && index == 0)
                    {
                        T select = ((DetailBase<T>)@this)._select;
                        PropertyInfo propInfo = select.GetType().GetProperty("AccountingObjectAddress");
                        if (propInfo != null && propInfo.CanWrite)
                            cell.Value = Convert.ChangeType(propInfo.GetValue(@select, null) ?? "", propInfo.PropertyType).ToString();
                    }
                    else if (cell.Column.Key.Equals("CompanyTaxCode") && index == 0)
                    {
                        T select = ((DetailBase<T>)@this)._select;
                        PropertyInfo propInfo = select.GetType().GetProperty("CompanyTaxCode");
                        if (propInfo != null && propInfo.CanWrite)
                            cell.Value = Convert.ChangeType(propInfo.GetValue(@select, null) ?? "", propInfo.PropertyType).ToString();
                    }
                    else if (cell.Column.Key.Equals("VATDescription"))
                    {
                        cell.Value = resSystem.String_01;
                    }
                    else if (cell.Column.Key.Equals("VATRate"))
                    {
                        cell.Value = (decimal)10;
                    }
                    else if (cell.Column.Key.Equals("VATAccount"))
                    {
                        cell.Value = IAccountDefaultService.DefaultVatAccount() ?? cell.Value;
                    }
                    else if (cell.Column.Key.Equals("CurrencyID"))
                    {
                        cell.Value = "VND";
                    }
                    else if (cell.Column.Key.Equals("ExchangeRate"))
                    {
                        cell.Value = (decimal)1;
                    }
                    else if (cell.Column.Key.Contains("Amount"))
                    {
                        if (cell.Value == null) cell.Value = (decimal)0;
                    }
                    else if (cell.Column.Key.Equals("GoodsServicePurchaseID"))
                    {
                        if (cell.ValueListResolved != null)
                        {
                            var goodsServicePurchase =
                                ListGoodsServicePurchase.FirstOrDefault(
                                    x => x.GoodsServicePurchaseCode == GetGoodsServicePurchaseDefault());
                            if (goodsServicePurchase != null)
                                cell.Value = goodsServicePurchase.ID;
                            //UltraCombo combo = (UltraCombo)cell.ValueListResolved;
                            //if (combo.Rows.Count > 0)
                            //{
                            //    var temp = combo.Rows[0].ListObject;
                            //    PropertyInfo propID = temp.GetType().GetProperty("ID");
                            //    if (propID != null && propID.CanWrite)
                            //        cell.Value = propID.GetValue(temp, null);
                            //}
                        }
                    }
                    else if (cell.Column.Key.Equals("IsIrrationalCost"))
                    {
                        cell.Value = false;
                    }
                    else if (cell.Column.Key.Equals("InvoiceDate"))
                    {
                        T select = ((DetailBase<T>)@this)._select;
                        PropertyInfo propInfo = select.GetType().GetProperty("Date");
                        if (propInfo != null && propInfo.CanWrite)
                            cell.Value = Convert.ChangeType(propInfo.GetValue(@select, null) ?? "", propInfo.PropertyType).ToString();
                    }
                    else if (index > 0 && !cell.Column.Key.Equals("ID"))
                        if (GetIsCopyRow())
                            cell.Value = uGrid.Rows[index - 1].Cells[cell.Column.Key].Value;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TK">Đối tượng của Form</typeparam>
        /// <param name="this"></param>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void uGrid_AfterExitEditMode<TK>(this Form @this, object sender, EventArgs e)
        {
            var @base = @this as DetailBase<TK>;
            if (@base != null && !@base._statusForm.Equals(ConstFrm.optStatusForm.View))
            {
                var uGrid = (UltraGrid)sender;
                @this.DoEverythingInGrid<TK>(uGrid);
                @base.Checked = true;
            }
        }
        /// <summary>
        /// Làm mọi thứ trong Grid
        /// </summary>
        /// <typeparam name="TK"></typeparam>
        /// <param name="this"></param>
        /// <param name="uGrid"></param>
        public static void DoEverythingInGrid<TK>(this Form @this, UltraGrid uGrid)
        {
            UltraGridCell cell = uGrid.ActiveCell;
            uGrid.UpdateDataGrid();
            if (cell != null)
            {
                var @base = @this as DetailBase<TK>;
                if ((cell.Column.Key.Contains("Rate") || cell.Column.Key.Contains("Amount") || (cell.Column.Key.Contains("UnitPrice")) ||
                     cell.Column.Key.Contains("Quantity") || cell.Column.Key.Contains("OrgPrice")) && cell.Value == null)
                    cell.Value = (decimal)0;
                else if (cell.Column.Key.Contains("Date") && !cell.Column.Key.Equals("FinalDate") && (cell.Value == null || (DateTime)cell.Value == GetDateTimeDefault()))
                    cell.Value = StringToDateTime(ConstFrm.DbStartDate);

                #region Tính CHiết khấu
                if (uGrid.DisplayLayout.Bands[0].Columns.Exists("DiscountRate"))
                {
                    if (cell.Column.Key.Equals("DiscountRate") || cell.Column.Key.Equals("DiscountAmountOriginal") ||
                        cell.Column.Key.Equals("AmountOriginal"))
                    {
                        decimal rate = GetValueFromText(cell.Row.Cells["DiscountRate"]);
                        if (rate <= 100)
                            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("DiscountAmountOriginal") &&
                                uGrid.DisplayLayout.Bands[0].Columns.Exists("AmountOriginal"))
                            {
                                var a = Convert.ToDecimal(cell.Row.Cells["AmountOriginal"].Value.ToString());
                                var b = a * rate / 100;
                                cell.Row.Cells["DiscountAmountOriginal"].Value = b;
                                cell.Row.UpdateData();
                            }
                    }
                }
                #endregion

                #region Tự động tính quy đổi nếu có
                if (!(cell.Column.Key.Contains("Amount") && !cell.Column.Key.Contains("AmountOriginal") && !cell.Column.Key.Contains("AutoOWAmountCal") && !cell.Column.Key.Contains("OrgPrice")) || cell.Column.Key.Contains("Currency") ||
                    cell.Column.Key.Equals("ExchangeRate"))
                {
                    if (uGrid.Name.Equals("uGridControl"))
                    {
                        if (uGrid.DisplayLayout.Bands[0].Columns.Exists("CurrencyID"))
                        {
                            Currency currency = ListCurrency.FirstOrDefault(
                                p => p.ID == uGrid.Rows[0].Cells["CurrencyID"].Value.ToString());
                            if (currency == null && cell.Column.Key.Contains("Currency")) goto ErrorCurrency;
                            uGrid.ExchangeCurrencyInSingleGrid(@this, currency);
                            @this.AutoExchangeByCurrencySelected(currency, (decimal?)cell.Row.Cells["ExchangeRate"].Value);
                        }
                    }
                    else
                    {
                        #region Cấu hình cho TH ko có cột Currency trong Grid con
                        if (!uGrid.DisplayLayout.Bands[0].Columns.Exists("CurrencyID"))
                        {
                            var uGridControl = (UltraGrid)@this.Controls.Find("uGridControl", true).FirstOrDefault();
                            //TH có Grid tiền tệ
                            if (uGridControl != null)
                            {
                                var currency = new Currency();
                                if (uGridControl.DisplayLayout.Bands[0].Columns.Exists("CurrencyID"))
                                {
                                    currency = ListCurrency.FirstOrDefault(
                                        p => p.ID == uGridControl.Rows[0].Cells["CurrencyID"].Value.ToString());
                                    if (uGridControl.DisplayLayout.Bands[0].Columns.Exists("ExchangeRate"))
                                    {
                                        if (currency != null)
                                        {
                                            if (@base != null)
                                            {
                                                @base.ExchangeRate = (decimal?)uGridControl.Rows[0].Cells["ExchangeRate"].Value != null ? (decimal)uGridControl.Rows[0].Cells["ExchangeRate"].Value : currency.ExchangeRate ?? 1;
                                                @this.AutoExchangeByCurrencySelected(currency, @base.ExchangeRate);
                                            }
                                        }
                                    }
                                }
                                foreach (UltraGridCell ultraGridCell in uGridControl.Rows[0].Cells)
                                {
                                    if ((ultraGridCell.Column.Key.Contains("Amount") &&
                                        !ultraGridCell.Column.Key.Contains("AmountOriginal") && !ultraGridCell.Column.Key.Contains("AutoOWAmountCal")) || (ultraGridCell.Column.Key.Contains("OrgPrice") && !ultraGridCell.Column.Key.Contains("OrgPriceOriginal")))
                                    {
                                        if (@base == null) continue;
                                        if (!uGridControl.DisplayLayout.Bands[0].Columns.Exists(ultraGridCell.Column.Key + "Original")) continue;
                                        ultraGridCell.Value = currency == null
                                                                ? 1
                                                                : ((decimal?)
                                                                    ultraGridCell.Row.Cells[
                                                                        ultraGridCell.Column.Key + "Original"].Value ?? 0 *
                                                                    @base.ExchangeRate);
                                        ultraGridCell.CheckMaxValueByMoney();
                                    }
                                }
                            }
                            //TH Ko có Grid tiền tệ nhưng có thuộc tính CurrencyID trong đối tượng chính
                            else if (@base != null && @base._select.HasProperty("CurrencyID") && @base._select.HasProperty("ExchangeRate"))
                            {
                                var currency = ListCurrency.FirstOrDefault(
                                        p => p.ID == @base._select.GetProperty<TK, string>("CurrencyID"));
                                if (currency != null)
                                {
                                    @base.ExchangeRate = currency.ExchangeRate ?? 1;
                                    @this.AutoExchangeByCurrencySelected(currency, @base.ExchangeRate);
                                }
                                foreach (var prop in @base._select.GetType().GetProperties())
                                {
                                    if ((prop.Name.Contains("Amount") &&
                                        !prop.Name.Contains("AmountOriginal") && !prop.Name.Contains("AutoOWAmountCal")) || (prop.Name.Contains("OrgPrice") && !prop.Name.Contains("OrgPriceOriginal")))
                                    {
                                        if (!@base._select.HasProperty(prop.Name + "Original")) continue;
                                        prop.SetValue(@base._select, currency == null
                                                                ? 1
                                                                : ((decimal?)@base._select.GetProperty(prop.Name + "Original") ?? 0 *
                                                                    @base.ExchangeRate), null);
                                    }
                                }
                            }
                        }
                        #endregion
                        #region Cấu hình cho TH Có cột Currency trong Grid con
                        else
                        {
                            if (string.IsNullOrEmpty(uGrid.Rows[0].Cells["CurrencyID"].Text.Trim()))
                                goto ErrorCurrency;
                            uGrid.ExchangeCurrencyInSingleGrid(@this);
                        }
                        #endregion
                    }
                }
                #endregion

                #region Check lai TH dao nguoc khi sua QUi doi tiền thuế
                else if (cell.Column.Key.Equals("VATAmount"))
                {
                    if (!uGrid.Name.Equals("uGridControl"))
                    {
                        //Chỉ xử lý cho TH có Grid Tiền tệ bên ngoài
                        var uGridControl = (UltraGrid)@this.Controls.Find("uGridControl", true).FirstOrDefault();
                        if (uGridControl != null && uGridControl.DisplayLayout.Bands[0].Columns.Exists("CurrencyID") && uGridControl.Rows.Count > 0)
                        {
                            decimal rate = 1;
                            if (uGridControl.Rows[0].Cells["ExchangeRate"].Value != null)
                                rate = (decimal)uGridControl.Rows[0].Cells["ExchangeRate"].Value;
                            cell.Row.Cells["VATAmountOriginal"].Value = (cell.Value == null ? 0 : (decimal)cell.Value) / rate;
                        }
                    }
                }
                #endregion

                #region Check lai TH dao nguoc khi sua QUi doi đơn giá
                if (cell.Column.Key.Equals("UnitPrice"))
                {
                    if (!uGrid.Name.Equals("uGridControl"))
                    {
                        //Chỉ xử lý cho TH có Grid Tiền tệ bên ngoài
                        var uGridControl = (UltraGrid)@this.Controls.Find("uGridControl", true).FirstOrDefault();
                        if (uGridControl != null && uGridControl.DisplayLayout.Bands[0].Columns.Exists("CurrencyID") && uGridControl.Rows.Count > 0)
                        {
                            decimal rate = 1;
                            if (uGridControl.Rows[0].Cells["ExchangeRate"].Value != null)
                                rate = (decimal)uGridControl.Rows[0].Cells["ExchangeRate"].Value;
                            cell.Row.Cells["UnitPriceOriginal"].Value = (cell.Value == null ? 0 : (decimal)cell.Value) / rate;
                        }
                        else if ((((DetailBase<TK>)@this)._select).HasProperty("ExchangeRate"))
                        {
                            decimal? rate = (((DetailBase<TK>)@this)._select).GetProperty<TK, Decimal?>("ExchangeRate");
                            if (rate != null && rate != 0)
                                cell.Row.Cells["UnitPriceOriginal"].Value = (cell.Value == null ? 0 : (decimal)cell.Value) / rate;
                        }
                    }
                }
                #endregion

                #region Tính tiền theo số lượng và đơn giá
                if (cell.Column.Key.Contains("Quantity") || cell.Column.Key.Contains("UnitPrice"))
                {
                    @this.CalculateAmount<TK>(uGrid, cell);
                }
                #endregion

                if (cell.Column.Key.Equals("DebitAccount") || cell.Column.Key.Equals("CreditAccount") || cell.Column.Key.Equals("Amount") || cell.Column.Key.Equals("AmountOriginal"))
                {
                    if (@base != null)
                    {
                        var @group = @base.TypeGroup;
                        if (@group != null)
                        {
                            var typeGroup = (int)@group;
                            if (typeGroup.Equals(11) && uGrid.DisplayLayout.Bands[0].Columns.Exists("CreditAccount") && (cell.Column.Key.Equals("CreditAccount") || cell.Column.Key.Equals("Amount") || cell.Column.Key.Equals("AmountOriginal")))
                            {
                                decimal totalAmount = 0;
                                decimal totalAmountOriginal = 0;
                                foreach (var row in uGrid.Rows)
                                {
                                    if (row.Cells["CreditAccount"].Value != null && row.Cells["CreditAccount"].Value.ToString().StartsWith("111"))
                                    {
                                        totalAmountOriginal += (decimal)row.Cells["AmountOriginal"].Value;
                                        totalAmount += (decimal)row.Cells["Amount"].Value;
                                    }
                                }
                                var uGridControl =
                                    (UltraGrid)@this.Controls.Find("uGridControl", true).FirstOrDefault();
                                if (uGridControl != null)
                                {
                                    uGridControl.Rows[0].Cells["TotalAmount"].Value = totalAmount;
                                    uGridControl.Rows[0].Cells["TotalAmount"].CheckMaxValueByMoney();
                                    uGridControl.Rows[0].Cells["TotalAmountOriginal"].Value = totalAmountOriginal;
                                    uGridControl.Rows[0].Cells["TotalAmountOriginal"].CheckMaxValueByMoney();
                                }
                            }
                            else if (typeGroup.Equals(10) && uGrid.DisplayLayout.Bands[0].Columns.Exists("DebitAccount") && (cell.Column.Key.Equals("DebitAccount") || cell.Column.Key.Equals("Amount") || cell.Column.Key.Equals("AmountOriginal")))
                            {
                                decimal totalAmount = 0;
                                decimal totalAmountOriginal = 0;
                                foreach (var row in uGrid.Rows.Where(row => row.Cells["DebitAccount"].Value != null && row.Cells["DebitAccount"].Value.ToString().StartsWith("111")))
                                {
                                    totalAmountOriginal += (decimal)row.Cells["AmountOriginal"].Value;
                                    totalAmount += (decimal)row.Cells["Amount"].Value;
                                }
                                var uGridControl =
                                    (UltraGrid)@this.Controls.Find("uGridControl", true).FirstOrDefault();
                                if (uGridControl != null)
                                {
                                    uGridControl.Rows[0].Cells["TotalAmount"].Value = totalAmount;
                                    uGridControl.Rows[0].Cells["TotalAmount"].CheckMaxValueByMoney();
                                    uGridControl.Rows[0].Cells["TotalAmountOriginal"].Value = totalAmountOriginal;
                                    uGridControl.Rows[0].Cells["TotalAmountOriginal"].CheckMaxValueByMoney();
                                }
                            }
                            else if (typeGroup.Equals(12) && uGrid.DisplayLayout.Bands[0].Columns.Exists("CreditAccount") && (cell.Column.Key.Equals("CreditAccount") || cell.Column.Key.Equals("Amount") || cell.Column.Key.Equals("AmountOriginal")))
                            {
                                decimal totalAmount = 0;
                                decimal totalAmountOriginal = 0;
                                foreach (var row in uGrid.Rows.Where(row => row.Cells["CreditAccount"].Value != null && row.Cells["CreditAccount"].Value.ToString().StartsWith("112")))
                                {
                                    totalAmountOriginal += (decimal)row.Cells["AmountOriginal"].Value;
                                    totalAmount += (decimal)row.Cells["Amount"].Value;
                                }
                                var uGridControl =
                                    (UltraGrid)@this.Controls.Find("uGridControl", true).FirstOrDefault();
                                if (uGridControl != null)
                                {
                                    uGridControl.Rows[0].Cells["TotalAmount"].Value = totalAmount;
                                    uGridControl.Rows[0].Cells["TotalAmountOriginal"].Value = totalAmountOriginal;
                                }
                            }
                            else if (typeGroup.Equals(16) && uGrid.DisplayLayout.Bands[0].Columns.Exists("DebitAccount") && (cell.Column.Key.Equals("DebitAccount") || cell.Column.Key.Equals("Amount") || cell.Column.Key.Equals("AmountOriginal")))
                            {
                                decimal totalAmount = 0;
                                decimal totalAmountOriginal = 0;
                                foreach (var row in uGrid.Rows.Where(row => row.Cells["DebitAccount"].Value != null && row.Cells["DebitAccount"].Value.ToString().StartsWith("112")))
                                {
                                    totalAmountOriginal += (decimal)row.Cells["AmountOriginal"].Value;
                                    totalAmount += (decimal)row.Cells["Amount"].Value;
                                }
                                var uGridControl =
                                    (UltraGrid)@this.Controls.Find("uGridControl", true).FirstOrDefault();
                                if (uGridControl != null)
                                {
                                    uGridControl.Rows[0].Cells["TotalAmount"].Value = totalAmount;
                                    uGridControl.Rows[0].Cells["TotalAmount"].CheckMaxValueByMoney();
                                    uGridControl.Rows[0].Cells["TotalAmountOriginal"].Value = totalAmountOriginal;
                                    uGridControl.Rows[0].Cells["TotalAmountOriginal"].CheckMaxValueByMoney();
                                }
                            }
                        }
                    }
                }
                #region TH Thay đổi cột Nhân viên của Form Chuyển tiền nội bộ
                if (cell.Column.Key.Equals("EmployeeID") && (typeof(TK) == typeof(MBInternalTransfer)))
                {
                    //var id = (Guid?) cell.Value;
                    //var accObj = ListAccountingObject.FirstOrDefault(x => x.ID == id);
                    //var uGridTax = (UltraGrid)@this.Controls.Find("uGrid1", true).FirstOrDefault();
                    //if (uGridTax != null && uGridTax.DisplayLayout.Bands[0].Columns.Exists("AccountingObjectID"))
                    //{
                    //    uGridTax.AutoFillAccoutingObject(accObj, uGridTax.Rows[cell.Row.Index].Cells["AccountingObjectID"]);
                    //}
                }
                #endregion

                #region Tính Nguyên giá

                if (new[] { "AmountOriginal", "VATRate", "ImportTaxRate", "FreightAmountOriginal", "ImportTaxAmountOriginal", "SpecialConsumeTaxAmountOriginal", "SpecialConsumeTaxRate", "DiscountAmountOriginal" }.Contains(cell.Column.Key))
                {
                    if (cell.Row.CalculationImportTaxAmount(@base))
                        @this.ReUpdateValueCell(new[] { "ImportTaxAmount" }, cell, uGrid);
                    if (cell.Row.CalculationSpecialConsumeTaxAmount(@base))
                        @this.ReUpdateValueCell(new[] { "SpecialConsumeTaxAmount" }, cell, uGrid);
                    if (cell.Row.CalculationVatAmount(@base))
                        @this.ReUpdateValueCell(new[] { "VATAmount" }, cell, uGrid);
                    if (cell.Row.CalculationInwardAmount(@base))
                        @this.ReUpdateValueCell(new[] { "InwardAmount" }, cell, uGrid);
                    if (cell.Row.CalculationOrgPrice(@base))
                        @this.ReUpdateValueCell(new[] { "OrgPrice" }, cell, uGrid);
                }
                //if (new[] { "AmountOriginal", "ImportTaxRate", "DiscountAmountOriginal" }.Contains(cell.Column.Key))
                //{
                //}
                //if (new[] { "AmountOriginal", "ImportTaxAmountOriginal", "DiscountAmountOriginal", "SpecialConsumeTaxRate" }.Contains(cell.Column.Key))
                //{
                //}
                //if (new[] { "AmountOriginal", "ImportTaxAmountOriginal", "DiscountAmountOriginal", "SpecialConsumeTaxAmountOriginal", "VATRate" }.Contains(cell.Column.Key))
                //{
                //}
                //if (new[] { "AmountOriginal", "DiscountAmountOriginal", "ImportTaxAmountOriginal", "SpecialConsumeTaxAmountOriginal" }.Contains(cell.Column.Key))
                //{
                //}
                #endregion

                if (cell.Column.Key.Contains("AccountingObject") && cell.Column.Key.Contains("ID") && string.IsNullOrEmpty(cell.Text))
                    ClearCellAccountingObject(uGrid);
                if (!uGrid.Name.Equals("uGridControl"))
                    @this.SetVatAmount<TK>(uGrid, cell, @base.TypeID);
                @base.ExchangeRate = 1;
            }
            goto End;
        ErrorCurrency:
            MSG.Warning(resSystem.MSG_System_49);//[H.A] Sửa sang thông báo về Loại tiền
            if (cell != null)
            {
                cell.Value = "VND";
                uGrid.UpdateData();
                @this.DoEverythingInGrid<TK>(uGrid);
            }
        End:
            #region Check lỗi
            @this.CheckActiveCellError<TK>(uGrid);
            #endregion
            return;
        }

        /// <summary>
        /// Cập nhật lại giá trị 1 cell cho các Grid khác 
        /// khi dòng Summary không tự động cộng tổng của column đó trên các Grid khác
        /// </summary>
        /// <param name="this"></param>
        /// <param name="keys">Danh sách tên các cột qui đổi cần cập nhật lại (Sẽ tự động cập nhật cả cột Nguyên tệ)</param>
        /// <param name="thisCell"></param>
        /// <param name="thisGrid"></param>
        private static void ReUpdateValueCell(this Form @this, string[] keys, UltraGridCell thisCell, UltraGrid thisGrid = null)
        {
            var uTabControl = @this.Controls.Find("ultraTabControl", true).FirstOrDefault() as UltraTabControl;
            if (uTabControl != null)
            {
                for (int i = 0; i < uTabControl.Tabs.Count; i++)
                {
                    var uGridOther =
                        @this.Controls.Find(string.Format("uGrid{0}", i), true).Cast<UltraGrid>().FirstOrDefault(
                            x => x != thisGrid);
                    if (uGridOther == null) continue;
                    foreach (var key in keys)
                    {
                        var orgKey = string.Format("{0}Original", key);
                        if (uGridOther.DisplayLayout.Bands[0].Columns.Exists(orgKey))
                        {
                            var a = uGridOther.Rows[thisCell.Row.Index].Cells[orgKey].Value;
                            uGridOther.Rows[thisCell.Row.Index].Cells[orgKey].Value = uGridOther.Rows[thisCell.Row.Index].Cells[orgKey].Value is decimal ? (decimal)0 : 0;
                            uGridOther.Rows[thisCell.Row.Index].UpdateData();
                            uGridOther.Rows[thisCell.Row.Index].Cells[orgKey].Value = a;
                        }

                        if (uGridOther.DisplayLayout.Bands[0].Columns.Exists(key))
                        {
                            var a = uGridOther.Rows[thisCell.Row.Index].Cells[key].Value;
                            uGridOther.Rows[thisCell.Row.Index].Cells[key].Value = uGridOther.Rows[thisCell.Row.Index].Cells[key].Value is decimal ? (decimal)0 : 0;
                            uGridOther.Rows[thisCell.Row.Index].UpdateData();
                            uGridOther.Rows[thisCell.Row.Index].Cells[key].Value = a;
                        }
                    }
                    uGridOther.RefreshRowGrid();
                }
            }
            else
            {
                var uGridOther =
                        @this.Controls.Find("uGrid0", true).Cast<UltraGrid>().FirstOrDefault(
                            x => x != thisGrid);
                if (uGridOther == null) return;
                foreach (var key in keys)
                {
                    var orgKey = string.Format("{0}Original", key);
                    if (uGridOther.DisplayLayout.Bands[0].Columns.Exists(orgKey))
                    {
                        var a = uGridOther.Rows[thisCell.Row.Index].Cells[orgKey].Value;
                        uGridOther.Rows[thisCell.Row.Index].Cells[orgKey].Value = uGridOther.Rows[thisCell.Row.Index].Cells[orgKey].Value is decimal ? (decimal)0 : 0;
                        uGridOther.Rows[thisCell.Row.Index].UpdateData();
                        uGridOther.Rows[thisCell.Row.Index].Cells[orgKey].Value = a;
                    }

                    if (uGridOther.DisplayLayout.Bands[0].Columns.Exists(key))
                    {
                        var a = uGridOther.Rows[thisCell.Row.Index].Cells[key].Value;
                        uGridOther.Rows[thisCell.Row.Index].Cells[key].Value = uGridOther.Rows[thisCell.Row.Index].Cells[key].Value is decimal ? (decimal)0 : 0;
                        uGridOther.Rows[thisCell.Row.Index].UpdateData();
                        uGridOther.Rows[thisCell.Row.Index].Cells[key].Value = a;
                    }
                }
                uGridOther.RefreshRowGrid();
            }
        }

        /// <summary>
        /// Tính nguyên giá
        /// </summary>
        /// <typeparam name="TK"></typeparam>
        /// <param name="row"></param>
        /// <param name="base"></param>
        private static bool CalculationOrgPrice<TK>(this UltraGridRow row, DetailBase<TK> @base)
        {
            //Nguyên giá = Giá mua + Chi phí mua + thuế nhập khẩu + thuế tiêu thụ đặc biệt - chiết khấu
            if (@base == null) return false;
            var uGrid = row.Band.Layout.Grid;
            var columns = uGrid.DisplayLayout.Bands[0].Columns;
            if (!(columns.Exists("AmountOriginal") && columns.Exists("FreightAmountOriginal") && columns.Exists("ImportTaxAmountOriginal") && columns.Exists("SpecialConsumeTaxAmountOriginal") && columns.Exists("DiscountAmountOriginal")))
                return false;
            var a = (decimal?)row.Cells["AmountOriginal"].Value ?? 0;
            var b = (decimal?)row.Cells["FreightAmountOriginal"].Value ?? 0;
            var c = (decimal?)row.Cells["ImportTaxAmountOriginal"].Value ?? 0;
            var d = (decimal?)row.Cells["SpecialConsumeTaxAmountOriginal"].Value ?? 0;
            var e = (decimal?)row.Cells["DiscountAmountOriginal"].Value ?? 0;
            var f = a + b + c + d - e;
            if (row.Band.Columns.Exists("OrgPriceOriginal"))
                row.Cells["OrgPriceOriginal"].Value = f;
            if (@base._select.HasProperty("ExchangeRate"))
            {
                if (row.Band.Columns.Exists("OrgPrice"))
                {
                    if (row.Band.Columns.Exists("OrgPriceOriginal"))
                        row.Cells["OrgPrice"].Value = (decimal)row.Cells["OrgPriceOriginal"].Value *
                                                           (@base._select.GetProperty<TK, decimal?>("ExchangeRate") ?? 1);
                    else
                        row.Cells["OrgPrice"].Value = f * (@base._select.GetProperty<TK, decimal?>("ExchangeRate") ?? 1);
                }
            }
            return true;
        }
        /// <summary>
        /// Tính tiền thuế nhập khẩu
        /// </summary>
        /// <typeparam name="TK"></typeparam>
        /// <param name="row"></param>
        /// <param name="base"></param>
        private static bool CalculationImportTaxAmount<TK>(this UltraGridRow row, DetailBase<TK> @base)
        {
            //Tiền thuế nhập khẩu = (Giá mua - chiết khấu)* thuế suất thuế nhập khẩu
            if (@base == null) return false;
            var uGrid = row.Band.Layout.Grid;
            var columns = uGrid.DisplayLayout.Bands[0].Columns;
            if (!(columns.Exists("AmountOriginal") && columns.Exists("ImportTaxRate") && columns.Exists("DiscountAmountOriginal")))
                return false;
            var a = (decimal?)row.Cells["AmountOriginal"].Value ?? 0;
            var b = (decimal?)row.Cells["DiscountAmountOriginal"].Value ?? 0;
            var c = ((decimal?)row.Cells["ImportTaxRate"].Value == null || (decimal?)row.Cells["ImportTaxRate"].Value == -1) ? 0 : (decimal)row.Cells["ImportTaxRate"].Value;
            var d = (a - b) * c;
            if (columns.Exists("ImportTaxAmountOriginal"))
                row.Cells["ImportTaxAmountOriginal"].Value = d;
            if (@base._select.HasProperty("ExchangeRate") && columns.Exists("ImportTaxAmount"))
            {
                if (columns.Exists("ImportTaxAmountOriginal"))
                    row.Cells["ImportTaxAmount"].Value = (decimal)row.Cells["ImportTaxAmountOriginal"].Value *
                                                       (@base._select.GetProperty<TK, decimal?>("ExchangeRate") ?? 1);
                else
                    row.Cells["ImportTaxAmount"].Value = d * (@base._select.GetProperty<TK, decimal?>("ExchangeRate") ?? 1);
            }
            return true;
        }
        /// <summary>
        /// Tính tiền thuế TTĐB
        /// </summary>
        /// <typeparam name="TK"></typeparam>
        /// <param name="row"></param>
        /// <param name="base"></param>
        private static bool CalculationSpecialConsumeTaxAmount<TK>(this UltraGridRow row, DetailBase<TK> @base)
        {
            //Tiền thuế TTĐB = (Giá mua - chiết khấu + tiền thuế nhập khẩu )* thuế suất thuế TTDB
            if (@base == null) return false;
            var uGrid = row.Band.Layout.Grid;
            var columns = uGrid.DisplayLayout.Bands[0].Columns;
            if (!(columns.Exists("AmountOriginal") && columns.Exists("ImportTaxAmountOriginal") && columns.Exists("SpecialConsumeTaxRate") && columns.Exists("DiscountAmountOriginal")))
                return false;
            var a = (decimal?)row.Cells["AmountOriginal"].Value ?? 0;
            var b = (decimal?)row.Cells["DiscountAmountOriginal"].Value ?? 0;
            var c = (decimal?)row.Cells["ImportTaxAmountOriginal"].Value ?? 0;
            var d = ((decimal?)row.Cells["SpecialConsumeTaxRate"].Value == null || (decimal?)row.Cells["SpecialConsumeTaxRate"].Value == -1) ? 0 : (decimal)row.Cells["SpecialConsumeTaxRate"].Value;
            var e = (a - b + c) * d;
            if (columns.Exists("SpecialConsumeTaxAmountOriginal"))
                row.Cells["SpecialConsumeTaxAmountOriginal"].Value = e;
            if (@base._select.HasProperty("ExchangeRate") && columns.Exists("SpecialConsumeTaxAmount"))
            {
                if (columns.Exists("SpecialConsumeTaxAmountOriginal"))
                    row.Cells["SpecialConsumeTaxAmount"].Value = (decimal)row.Cells["SpecialConsumeTaxAmountOriginal"].Value *
                                                       (@base._select.GetProperty<TK, decimal?>("ExchangeRate") ?? 1);
                else
                    row.Cells["SpecialConsumeTaxAmount"].Value = e * (@base._select.GetProperty<TK, decimal?>("ExchangeRate") ?? 1);
            }
            return true;
        }
        /// <summary>
        /// Tính tiền thuế GTGT
        /// </summary>
        /// <typeparam name="TK"></typeparam>
        /// <param name="row"></param>
        /// <param name="base"></param>
        private static bool CalculationVatAmount<TK>(this UltraGridRow row, DetailBase<TK> @base)
        {
            //Tiền thuế GTGT = (Giá mua - chiết khấu + tiền thuế nhập khẩu + tiền thuế TTĐB)* thuế suất thuế GTGT
            if (@base == null) return false;
            var uGrid = row.Band.Layout.Grid;
            var columns = uGrid.DisplayLayout.Bands[0].Columns;
            if (!(columns.Exists("AmountOriginal") && columns.Exists("ImportTaxAmountOriginal") && columns.Exists("SpecialConsumeTaxAmountOriginal") && columns.Exists("VATRate") && columns.Exists("DiscountAmountOriginal")))
                return false;
            var a = (decimal?)row.Cells["AmountOriginal"].Value ?? 0;
            var b = (decimal?)row.Cells["DiscountAmountOriginal"].Value ?? 0;
            var c = (decimal?)row.Cells["ImportTaxAmountOriginal"].Value ?? 0;
            var d = (decimal?)row.Cells["SpecialConsumeTaxAmountOriginal"].Value ?? 0;
            var e = ((decimal?)row.Cells["VATRate"].Value == null || (decimal?)row.Cells["VATRate"].Value == -1) ? 0 : (decimal)row.Cells["VATRate"].Value;
            var f = (a - b + c + d) * e / 100;
            if (columns.Exists("VATAmountOriginal"))
                row.Cells["VATAmountOriginal"].Value = f;
            if (@base._select.HasProperty("ExchangeRate") && columns.Exists("VATAmount"))
            {
                if (columns.Exists("VATAmountOriginal"))
                    row.Cells["VATAmount"].Value = (decimal)row.Cells["VATAmountOriginal"].Value *
                                                       (@base._select.GetProperty<TK, decimal?>("ExchangeRate") ?? 1);
                else
                    row.Cells["VATAmount"].Value = f * (@base._select.GetProperty<TK, decimal?>("ExchangeRate") ?? 1);
            }
            return true;
        }
        /// <summary>
        /// Tính giá trị nhập kho
        /// </summary>
        /// <typeparam name="TK"></typeparam>
        /// <param name="row"></param>
        /// <param name="base"></param>
        public static bool CalculationInwardAmount<TK>(this UltraGridRow row, DetailBase<TK> @base)
        {
            //Giá trị nhập kho = gia mua - chiet khau + thue NK + thue TTĐB
            if (@base == null) return false;
            var uGrid = row.Band.Layout.Grid;
            var columns = uGrid.DisplayLayout.Bands[0].Columns;
            if (!(columns.Exists("AmountOriginal") && columns.Exists("DiscountAmountOriginal") && columns.Exists("ImportTaxAmountOriginal") && columns.Exists("SpecialConsumeTaxAmountOriginal")))
                return false;
            var a = (decimal?)row.Cells["AmountOriginal"].Value ?? 0;
            var b = (decimal?)row.Cells["DiscountAmountOriginal"].Value ?? 0;
            var c = (decimal?)row.Cells["ImportTaxAmountOriginal"].Value ?? 0;
            var d = (decimal?)row.Cells["SpecialConsumeTaxAmountOriginal"].Value ?? 0;
            var e = a - b + c + d;
            if (columns.Exists("InwardAmountOriginal"))
                row.Cells["InwardAmountOriginal"].Value = e;
            if (@base._select.HasProperty("ExchangeRate") && columns.Exists("InwardAmount"))
            {
                if (columns.Exists("InwardAmountOriginal"))
                    row.Cells["InwardAmount"].Value = (decimal)row.Cells["InwardAmountOriginal"].Value *
                                                       (@base._select.GetProperty<TK, decimal?>("ExchangeRate") ?? 1);
                else
                    row.Cells["InwardAmount"].Value = e * (@base._select.GetProperty<TK, decimal?>("ExchangeRate") ?? 1);
            }
            return true;
        }


        public static bool CheckGridForm(this Control @this)
        {
            bool isError = false;
            foreach (Control ctrl in @this.Controls)
            {
                if (ctrl.GetType().Name.Equals(typeof(UltraGrid).Name))
                {
                    if (((UltraGrid)ctrl).CheckErrorGrid())
                        isError = true;
                }
                else if (ctrl.HasChildren)
                {
                    if (CheckGridForm(ctrl))
                        isError = true;
                }
            }
            return isError;
        }

        static bool CheckErrorGrid(this UltraGrid uGrid)
        {
            bool isError = false;
            foreach (UltraGridRow row in uGrid.Rows)
            {
                foreach (UltraGridCell cell in row.Cells)
                {
                    if (cell.Tag != null)
                    {
                        Dictionary<string, object> dicTag = (Dictionary<string, object>)cell.Tag;
                        KeyValuePair<string, object>? item = dicTag.FirstOrDefault(p => p.Key.Equals("Error"));
                        if (item != null)
                        {
                            if (item.Value.Value != null && item.Value.Value.Equals(true))
                                isError = true;
                        }
                    }
                }
            }
            return isError;
        }

        /// <summary>
        /// Check Error Cell
        /// </summary>
        /// <param name="this"></param>
        /// <param name="uGrid"></param>
        public static void CheckActiveCellError<TK>(this Form @this, UltraGrid uGrid)
        {
            UltraGridCell cell = uGrid.ActiveCell;
            if (cell == null) return;
            if (cell.Column.ValueList != null)
            {
                //_isKeyPress = false;
                var ultraCombo = (UltraCombo)cell.Column.ValueList;
                //ultraCombo.SetValueForTag("KeyPress", false);
                ultraCombo.ClearAutoComplete();

            }
            bool isAccountError = false;
            if (cell.Column.Key.Equals("DebitAccount") || cell.Column.Key.Equals("CreditAccount") ||
                cell.Column.Key.Equals("VATAccount") || cell.Column.Key.Equals("AmountOriginal") || cell.Column.Key.Equals("ExchangeRate"))
            {
                isAccountError = uGrid.CheckAccount();
                cell.SetErrorForCell(isAccountError);
            }
            bool isError = false;
            if (cell.Column.Key.Equals("EmployeeID"))
            {
                var combo = (UltraCombo)cell.ValueListResolved;
                if (combo != null)
                {
                    if (!string.IsNullOrEmpty(cell.Text) &&
                        (cell.Value == null || !combo.Rows.Any(row => (Guid?)row.Cells["ID"].Value == (Guid?)cell.Value && !row.Hidden)))
                        isError = true;
                    if (isError)
                    {
                        NotificationCell(uGrid, cell, resSystem.MSG_System_34, true);
                        uGrid.Focus();
                        uGrid.ActiveCell = cell;
                        uGrid.PerformAction(UltraGridAction.EnterEditMode, false, false);
                        //cell.Activate();
                        //cell.Selected = true;
                    }
                    else
                        RemoveNotificationCell(uGrid, cell);
                }
                cell.SetErrorForCell(isError);
            }
            else if (cell.Column.Key.Contains("BankAccountDetail"))
            {
                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                {
                    isError = ListBankAccountDetail.Count(x => x.ID == (Guid?)cell.Value && x.IsActive) == 0;
                    if (isError)
                        NotificationCell(uGrid, cell, resSystem.MSG_System_30);
                    else
                        RemoveNotificationCell(uGrid, cell);
                }
                cell.SetErrorForCell(isError);
            }
            else if (cell.Column.Key.Contains("PaymentClause"))
            {
                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                {
                    isError = ListPaymentClause.Count(x => x.ID == (Guid?)cell.Value && x.IsActive) == 0;
                    if (isError)
                        NotificationCell(uGrid, cell, resSystem.MSG_System_30);
                    else
                        RemoveNotificationCell(uGrid, cell);
                }
                cell.SetErrorForCell(isError);
            }
            else if (cell.Column.Key.Contains("Repository"))
            {
                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                {
                    isError = ListRepository.Count(x => x.ID == (Guid?)cell.Value && x.IsActive) == 0;
                    if (isError)
                        NotificationCell(uGrid, cell, resSystem.MSG_System_30);
                    else
                        RemoveNotificationCell(uGrid, cell);
                }
                cell.SetErrorForCell(isError);
            }
            else if (cell.Column.Key.Contains("Contract"))
            {
                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                {
                    isError = ListEmContract.Count(x => x.ID == (Guid?)cell.Value && x.IsActive) == 0;
                    if (isError)
                        NotificationCell(uGrid, cell, resSystem.MSG_System_30);
                    else
                        RemoveNotificationCell(uGrid, cell);
                }
                cell.SetErrorForCell(isError);
            }
            else if (cell.Column.Key.Equals("GoodsServicePurchaseID"))
            {
                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                {
                    isError = ListGoodsServicePurchase.Count(x => x.ID == (Guid?)cell.Value && x.IsActive) == 0;
                    if (isError)
                        NotificationCell(uGrid, cell, resSystem.MSG_System_30);
                    else
                        RemoveNotificationCell(uGrid, cell);
                }
                cell.SetErrorForCell(isError);
            }
            else if (cell.Column.Key.Equals("InvoiceTypeID"))
            {
                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                {
                    isError = ListInvoiceType.Count(x => x.ID == (Guid?)cell.Value && x.IsActive) == 0;
                    if (isError)
                        NotificationCell(uGrid, cell, resSystem.MSG_System_30);
                    else
                        RemoveNotificationCell(uGrid, cell);
                }
                cell.SetErrorForCell(isError);
            }
            else if (cell.Column.Key.Contains("FixedAssetID"))
            {
                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                {
                    isError = ListFixedAsset.Count(x => x.ID == (Guid?)cell.Value) == 0;
                    if (isError)
                        NotificationCell(uGrid, cell, resSystem.MSG_System_30);
                    else
                        RemoveNotificationCell(uGrid, cell);
                }
                cell.SetErrorForCell(isError);
            }
            else if (cell.Column.Key.Equals("BudgetItemID"))
            {
                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                {
                    isError = ListBudgetItem.Count(x => x.ID == (Guid?)cell.Value && x.IsActive) == 0;
                    if (isError)
                        NotificationCell(uGrid, cell, resSystem.MSG_System_30);
                    else
                        RemoveNotificationCell(uGrid, cell);
                }
                if (!isError)
                {
                    isError =
                        ListBudgetItem.Where(x => x.ID == (Guid?)cell.Value && x.IsActive).Select(
                            x => x.IsParentNode).FirstOrDefault();
                    ShowErrorOnCellHaveTree(isError, uGrid, cell);
                }
                cell.SetErrorForCell(isError);
            }
            else if (cell.Column.Key.Equals("ExpenseItemID"))
            {
                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                {
                    isError = ListExpenseItem.Count(x => x.ID == (Guid?)cell.Value && x.IsActive) == 0;
                    if (isError)
                        NotificationCell(uGrid, cell, resSystem.MSG_System_30);
                    else
                        RemoveNotificationCell(uGrid, cell);
                }
                if (!isError)
                {
                    isError = ListExpenseItem.Where(x => x.ID == (Guid?)cell.Value && x.IsActive).Select(x => x.IsParentNode).FirstOrDefault();
                    ShowErrorOnCellHaveTree(isError, uGrid, cell);
                }
                cell.SetErrorForCell(isError);
            }
            else if (cell.Column.Key.Contains("Department"))
            {
                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                {
                    isError = ListDepartment.Count(x => x.ID == (Guid?)cell.Value && x.IsActive) == 0;
                    if (isError)
                        NotificationCell(uGrid, cell, resSystem.MSG_System_30);
                    else
                        RemoveNotificationCell(uGrid, cell);
                }
                if (!isError)
                {
                    isError = ListDepartment.Where(x => x.ID == (Guid?)cell.Value && x.IsActive).Select(x => x.IsParentNode).FirstOrDefault();
                    ShowErrorOnCellHaveTree(isError, uGrid, cell);
                }
                cell.SetErrorForCell(isError);
            }
            else if (cell.Column.Key.Equals("CostSetID"))
            {
                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                {
                    isError = ListCostSet.Count(x => x.ID == (Guid?)cell.Value && x.IsActive) == 0;
                    if (isError)
                        NotificationCell(uGrid, cell, resSystem.MSG_System_30);
                    else
                        RemoveNotificationCell(uGrid, cell);
                }
                if (!isError)
                {
                    isError = ListCostSet.Where(x => x.ID == (Guid?)cell.Value && x.IsActive).Select(x => x.IsParentNode).FirstOrDefault();
                    ShowErrorOnCellHaveTree(isError, uGrid, cell);
                }
                cell.SetErrorForCell(isError);
            }
            else if (cell.Column.Key.Equals("StatisticsCodeID"))
            {
                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                {
                    isError = ListStatisticsCode.Count(x => x.ID == (Guid?)cell.Value && x.IsActive) == 0;
                    if (isError)
                        NotificationCell(uGrid, cell, resSystem.MSG_System_30);
                    else
                        RemoveNotificationCell(uGrid, cell);
                }
                if (!isError)
                {
                    isError = ListStatisticsCode.Where(x => x.ID == (Guid?)cell.Value && x.IsActive).Select(x => x.IsParentNode).FirstOrDefault();
                    ShowErrorOnCellHaveTree(isError, uGrid, cell);
                }
                cell.SetErrorForCell(isError);
            }
            else if (cell.Column.Key.Equals("CompanyTaxCode"))
            {
                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                {
                    isError = !Accounting.Core.Utils.CheckMST(cell.Value.ToString());
                    if (isError)
                        NotificationCell(uGrid, cell, resSystem.MSG_System_44);
                    else
                        RemoveNotificationCell(uGrid, cell);
                }
                cell.SetErrorForCell(isError);
            }
            else if (cell.Column.Key.Equals("InvoiceSeries"))
            {
                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                {
                    isError = !Accounting.Core.Utils.IsKyHieuHoaDon(cell.Value.ToString());
                    if (isError)
                        NotificationCell(uGrid, cell, resSystem.MSG_System_43);
                    else
                        RemoveNotificationCell(uGrid, cell);
                }
                cell.SetErrorForCell(isError);
            }
            else if (cell.Column.Key.Contains("Amount") && !cell.Column.Key.Contains("AutoOWAmountCal"))
            {
                isError = cell.CheckMaxValueByMoney();
            }
            #region Check Quantity
            else if (cell.Column.Key.Equals("MaterialGoodsID"))
            {
                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                {
                    isError = ListMaterialGoodsCustom.Count(x => x.ID == (Guid?)cell.Value && x.IsActive) == 0;
                    if (isError)
                        NotificationCell(uGrid, cell, resSystem.MSG_System_30);
                    else
                        RemoveNotificationCell(uGrid, cell);
                }
                cell.SetErrorForCell(isError);
                if (uGrid.DisplayLayout.Bands[0].Columns.Exists("Quantity"))
                {
                    var @base = @this as DetailBase<TK>;
                    if (@base != null) uGrid.CheckQuantity(cell, @base.TypeID);
                }
            }
            else if (cell.Column.Key.Equals("Quantity") && uGrid.DisplayLayout.Bands[0].Columns.Exists("MaterialGoodsID"))
            {
                if (cell.Value == null) cell.Value = (decimal)0;
                if (cell.Row.Cells["MaterialGoodsID"].Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                {
                    var @base = @this as DetailBase<TK>;
                    if (@base != null) isError = uGrid.CheckQuantity(cell, @base.TypeID);
                    cell.SetErrorForCell(isError);
                }
            }
            #endregion
            #region Check MaterialGoodsID
            bool isMaterialGoodsError = false;
            if (!uGrid.Name.Equals("uGridControl"))
            {
                UltraTabControl @TabControl =
            (UltraTabControl)@this.Controls.Find("ultraTabControl", true).FirstOrDefault();
                if (@TabControl != null)
                {
                    foreach (UltraTab tab in @TabControl.Tabs)
                    {
                        int index = tab.Index;
                        UltraGrid uGridCheck = (UltraGrid)@TabControl.Tabs[index].TabPage.Controls.Find(string.Format("uGrid{0}", index), true).FirstOrDefault();
                        if (uGridCheck != null && uGridCheck.DisplayLayout.Bands[0].Columns.Exists("MaterialGoodsID") && uGridCheck.Rows.Count > cell.Row.Index)
                            isMaterialGoodsError = uGridCheck.CheckMaterialGoods(uGridCheck.Rows[cell.Row.Index]);
                    }
                }
                else
                {
                    UltraGrid uGridCheck = (UltraGrid)@this.Controls.Find("uGrid0", true).FirstOrDefault();
                    if (uGridCheck != null && uGridCheck.DisplayLayout.Bands[0].Columns.Exists("MaterialGoodsID"))
                        isMaterialGoodsError = uGridCheck.CheckMaterialGoods(uGridCheck.Rows[cell.Row.Index]);
                }
            }
            #endregion


            #region Check FixedAssetID
            bool isFixedAssetError = false;
            if (!uGrid.Name.Equals("uGridControl"))
            {
                UltraTabControl tabControl =
            (UltraTabControl)@this.Controls.Find("ultraTabControl", true).FirstOrDefault();
                if (tabControl != null)
                {
                    foreach (UltraTab tab in tabControl.Tabs)
                    {
                        int index = tab.Index;
                        UltraGrid uGridCheck = (UltraGrid)tabControl.Tabs[index].TabPage.Controls.Find(string.Format("uGrid{0}", index), true).FirstOrDefault();
                        if (uGridCheck != null && uGridCheck.DisplayLayout.Bands[0].Columns.Exists("FixedAssetID") && uGridCheck.Rows.Count > cell.Row.Index)
                            isFixedAssetError = uGridCheck.CheckFixedAsset(uGridCheck.Rows[cell.Row.Index]);
                    }
                }
                else
                {
                    UltraGrid uGridCheck = (UltraGrid)@this.Controls.Find("uGrid0", true).FirstOrDefault();
                    if (uGridCheck != null && uGridCheck.DisplayLayout.Bands[0].Columns.Exists("FixedAssetID"))
                        isFixedAssetError = uGridCheck.CheckFixedAsset(uGridCheck.Rows[cell.Row.Index]);
                }
            }
            #endregion

            #region Tính và Kiểm tra Chiết khấu
            bool isDiscountError = false;
            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("DiscountRate"))
            {
                if (cell.Column.Key.Equals("DiscountRate") || cell.Column.Key.Equals("DiscountAmountOriginal") ||
                    cell.Column.Key.Equals("AmountOriginal"))
                {
                    isDiscountError = CheckDiscount(uGrid, cell.Row.Cells["DiscountRate"]);
                    cell.SetErrorForCell(isDiscountError);
                }
            }
            #endregion
            //else
            //    isError = IsFormError;
            if (!isError && !isAccountError && !isMaterialGoodsError && !isFixedAssetError && !isDiscountError)
            {
                RemoveNotificationCell(uGrid, cell);
                cell.SetErrorForCell(false);
            }
        }
        /// <summary>
        /// Check Error Cell dành cho form số dư đầu kỳ
        /// </summary>
        /// <typeparam name="TK"></typeparam>
        /// <param name="this"></param>
        /// <param name="uGrid"></param>
        public static void CheckActiveCell2<TK>(this Form @this, UltraGrid uGrid)//check riêng cho nhập số dư đầu kỳ
        {
            UltraGridCell cell = uGrid.ActiveCell;
            if (cell == null) return;
            if (cell.Column.ValueList != null)
            {
                //_isKeyPress = false;
                UltraCombo ultraCombo = (UltraCombo)cell.Column.ValueList;
                //ultraCombo.SetValueForTag("KeyPress", false);
                ultraCombo.ClearAutoComplete();

            }
            bool isAccountError = false;
            if (cell.Column.Key.Equals("DebitAccount") || cell.Column.Key.Equals("CreditAccount") ||
                cell.Column.Key.Equals("VATAccount") || cell.Column.Key.Equals("AmountOriginal") || cell.Column.Key.Equals("ExchangeRate"))
            {
                isAccountError = uGrid.CheckAccount();
                cell.SetErrorForCell(isAccountError);
            }
            bool isError = false;
            if (cell.Column.Key.Contains("BankAccountDetail"))
            {
                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                {
                    isError = ListBankAccountDetail.Count(x => x.ID == (Guid?)cell.Value && x.IsActive) == 0;
                    if (isError)
                        NotificationCell(uGrid, cell, resSystem.MSG_System_30);
                    else
                        RemoveNotificationCell(uGrid, cell);
                }
                cell.SetErrorForCell(isError);
            }
            else if (cell.Column.Key.Contains("PaymentClause"))
            {
                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                {
                    isError = ListPaymentClause.Count(x => x.ID == (Guid?)cell.Value && x.IsActive) == 0;
                    if (isError)
                        NotificationCell(uGrid, cell, resSystem.MSG_System_30);
                    else
                        RemoveNotificationCell(uGrid, cell);
                }
                cell.SetErrorForCell(isError);
            }
            else if (cell.Column.Key.Contains("Contract"))
            {
                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                {
                    isError = ListEmContract.Count(x => x.ID == (Guid?)cell.Value && x.IsActive) == 0;
                    if (isError)
                        NotificationCell(uGrid, cell, resSystem.MSG_System_30);
                    else
                        RemoveNotificationCell(uGrid, cell);
                }
                cell.SetErrorForCell(isError);
            }
            else if (cell.Column.Key.Equals("ExpenseItemID"))
            {
                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                {
                    isError = ListExpenseItem.Count(x => x.ID == (Guid?)cell.Value && x.IsActive) == 0;
                    if (isError)
                        NotificationCell(uGrid, cell, resSystem.MSG_System_30);
                    else
                        RemoveNotificationCell(uGrid, cell);
                }
                if (!isError)
                {
                    isError = ListExpenseItem.Where(x => x.ID == (Guid?)cell.Value && x.IsActive).Select(x => x.IsParentNode).FirstOrDefault();
                    ShowErrorOnCellHaveTree(isError, uGrid, cell);
                }
                cell.SetErrorForCell(isError);
            }
            else if (cell.Column.Key.Equals("CostSetID"))
            {
                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                {
                    isError = ListCostSet.Count(x => x.ID == (Guid?)cell.Value && x.IsActive) == 0;
                    if (isError)
                        NotificationCell(uGrid, cell, resSystem.MSG_System_30);
                    else
                        RemoveNotificationCell(uGrid, cell);
                }
                if (!isError)
                {
                    isError = ListCostSet.Where(x => x.ID == (Guid?)cell.Value && x.IsActive).Select(x => x.IsParentNode).FirstOrDefault();
                    ShowErrorOnCellHaveTree(isError, uGrid, cell);
                }
                cell.SetErrorForCell(isError);
            }
            else if (cell.Column.Key.Equals("StatisticsCodeID"))
            {
                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                {
                    isError = ListStatisticsCode.Count(x => x.ID == (Guid?)cell.Value && x.IsActive) == 0;
                    if (isError)
                        NotificationCell(uGrid, cell, resSystem.MSG_System_30);
                    else
                        RemoveNotificationCell(uGrid, cell);
                }
                if (!isError)
                {
                    isError = ListStatisticsCode.Where(x => x.ID == (Guid?)cell.Value && x.IsActive).Select(x => x.IsParentNode).FirstOrDefault();
                    ShowErrorOnCellHaveTree(isError, uGrid, cell);
                }
                cell.SetErrorForCell(isError);
            }
            #region Check MaterialGoodsID
            bool isMaterialGoodsError = false;
            if (!uGrid.Name.Equals("uGridControl"))
            {
                UltraTabControl @TabControl =
            (UltraTabControl)@this.Controls.Find("ultraTabControl", true).FirstOrDefault();
                if (@TabControl != null)
                {
                    foreach (UltraTab tab in @TabControl.Tabs)
                    {
                        int index = tab.Index;
                        UltraGrid uGridCheck = (UltraGrid)@TabControl.Tabs[index].TabPage.Controls.Find(string.Format("uGrid{0}", index), true).FirstOrDefault();
                        if (uGridCheck != null && uGridCheck.DisplayLayout.Bands[0].Columns.Exists("MaterialGoodsID"))
                            isMaterialGoodsError = uGridCheck.CheckMaterialGoods(uGridCheck.Rows[cell.Row.Index]);
                    }
                }
                else
                {
                    UltraGrid uGridCheck = (UltraGrid)@this.Controls.Find("uGrid0", true).FirstOrDefault();
                    if (uGridCheck != null && uGridCheck.DisplayLayout.Bands[0].Columns.Exists("MaterialGoodsID"))
                        isMaterialGoodsError = uGridCheck.CheckMaterialGoods(uGridCheck.Rows[cell.Row.Index]);
                }
            }
            #endregion

            #region Check Chiết khấu

            bool isDiscountError = false;
            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("DiscountRate"))
            {
                if (cell.Column.Key.Equals("DiscountRate") || cell.Column.Key.Equals("DiscountAmountOriginal") ||
                    cell.Column.Key.Equals("AmountOriginal"))
                {
                    isDiscountError = CheckDiscount(uGrid, cell.Row.Cells["DiscountRate"]);
                    cell.SetErrorForCell(isDiscountError);
                }
            }

            #endregion
            //else
            //    isError = IsFormError;
            if (!isError && !isAccountError && !isMaterialGoodsError && !isDiscountError)
            {
                RemoveNotificationCell(uGrid, cell);
            }
        }
        static void SetErrorForCell(this UltraGridCell cell, bool isError)
        {
            Dictionary<string, object> dicTag = new Dictionary<string, object>();
            if (cell.Tag != null)
            {
                dicTag = (Dictionary<string, object>)cell.Tag;
                if (dicTag.Count(p => p.Key.Equals("Error")) > 0)
                {
                    dicTag.Remove("Error");
                }
            }
            dicTag.Add("Error", isError);
            cell.Tag = dicTag;
        }

        static bool CheckCellError(this UltraGridCell cell)
        {
            bool isError = false;
            if (cell.Tag != null)
            {
                Dictionary<string, object> dicTag = (Dictionary<string, object>)cell.Tag;
                KeyValuePair<string, object>? item = dicTag.Where(p => p.Key.Equals("Error")).FirstOrDefault();
                if (item != null)
                {
                    isError = (bool)item.Value.Value;
                }
            }
            return isError;
        }

        /// <summary>
        /// Tính thành tiền từ đơn giá và số lượng
        /// </summary>
        /// <param name="this"> </param>
        /// <param name="uGrid"></param>
        /// <param name="cell"></param>
        private static void CalculateAmount<TK>(this Form @this, UltraGrid uGrid, UltraGridCell cell)
        {
            UltraGridBand band = uGrid.DisplayLayout.Bands[0];
            if (band.Columns.Exists("Quantity") && (band.Columns.Exists("UnitPriceOriginal") && band.Columns.Exists("UnitPrice")) && band.Columns.Exists("AmountOriginal"))
            {
                if (uGrid.ActiveCell != null)
                {
                    UltraGridRow row = uGrid.ActiveCell.Row;
                    var @base = @this as DetailBase<TK>;
                    if (cell.Column.Key.Equals("UnitPriceOriginal") || cell.Column.Key.Equals("Quantity"))
                    {
                        if (@base != null)
                        {
                            TK select = @base._select;
                            PropertyInfo propExRate = @select.GetType().GetProperty("ExchangeRate");
                            if (propExRate != null && propExRate.CanRead && propExRate.CanWrite)
                                @base.ExchangeRate = propExRate.GetValue(@select, null) != null ? (decimal)propExRate.GetValue(@select, null) : @base.ExchangeRate;
                            row.Cells["AmountOriginal"].Value = (row.Cells["Quantity"].Value == null ? 0 : (decimal)row.Cells["Quantity"].Value) *
                                                                (row.Cells["UnitPriceOriginal"].Value == null ? 0 : (decimal)row.Cells["UnitPriceOriginal"].Value);
                            row.Cells["UnitPrice"].Value = (row.Cells["UnitPriceOriginal"].Value == null
                                                                ? 0
                                                                : (decimal)row.Cells["UnitPriceOriginal"].Value) *
                                                           (band.Columns.Exists("ExchangeRate")
                                                                 ? (decimal)row.Cells["ExchangeRate"].Value
                                                                 : @base.ExchangeRate);
                        }
                    }
                    row.Cells["Amount"].Value = (row.Cells["Quantity"].Value == null ? 0 : (decimal)row.Cells["Quantity"].Value) *
                                                         (row.Cells["UnitPrice"].Value == null ? 0 : (decimal)row.Cells["UnitPrice"].Value);
                    if (!cell.Column.Key.Equals("UnitPriceOriginal") && !cell.Column.Key.Equals("Quantity"))
                        if (@base != null)
                            row.Cells["AmountOriginal"].Value = (decimal)row.Cells["Amount"].Value / (@base.ExchangeRate ?? 1);
                }
            }
        }
        private static bool CheckQuantity(this UltraGrid uGrid, UltraGridCell cell, int typeID)
        {
            bool isError = true;
            if (ListTypeInward.Any(p => p == typeID)) return false;
            uGrid.UpdateDataGrid();
            if (cell.Row.Cells["MaterialGoodsID"].Value != null)
            {
                MaterialGoodsCustom temp =
                    ListMaterialGoodsCustom.SingleOrDefault(
                        x => x.ID == (Guid?)cell.Row.Cells["MaterialGoodsID"].Value);
                if (temp != null)
                {
                    decimal? quantity = temp.SumIWQuantity;
                    if (!string.IsNullOrEmpty(quantity.ToString()) && quantity < (decimal)cell.Row.Cells["Quantity"].Value)
                    {
                        NotificationCell(uGrid, cell.Row.Cells["Quantity"], resSystem.MSG_System_37);
                    }
                    else
                    {
                        RemoveNotificationCell(uGrid, cell.Row.Cells["Quantity"]);
                        isError = false;
                    }
                }
            }
            cell.Row.Cells["Quantity"].SetErrorForCell(isError);
            return isError;
        }

        /// <summary>
        /// Hàm check lỗi cột Mã hàng. Lỗi trả về true
        /// </summary>
        /// <param name="uGrid"> </param>
        /// <param name="row"> </param>
        private static bool CheckMaterialGoods(this UltraGrid uGrid, UltraGridRow row)
        {
            bool isError;
            if (String.IsNullOrEmpty(row.Cells["MaterialGoodsID"].Text))
            {
                NotificationCell(uGrid, row.Cells["MaterialGoodsID"], resSystem.MSG_System_36);
                isError = true;
            }
            else
            {
                RemoveNotificationCell(uGrid, row.Cells["MaterialGoodsID"]);
                isError = false;
            }
            row.Cells["MaterialGoodsID"].SetErrorForCell(isError);
            return isError;
        }
        /// <summary>
        /// Hàm check lỗi cột Mã TS. Lỗi trả về true
        /// </summary>
        /// <param name="uGrid"> </param>
        /// <param name="row"> </param>
        private static bool CheckFixedAsset(this UltraGrid uGrid, UltraGridRow row)
        {
            bool isError;
            if (String.IsNullOrEmpty(row.Cells["FixedAssetID"].Text))
            {
                NotificationCell(uGrid, row.Cells["FixedAssetID"], resSystem.MSG_System_54);
                isError = true;
            }
            else
            {
                RemoveNotificationCell(uGrid, row.Cells["FixedAssetID"]);
                isError = false;
            }
            row.Cells["FixedAssetID"].SetErrorForCell(isError);
            return isError;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T">Đối tượng của Form</typeparam>
        /// <param name="this"></param>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void uGrid_KeyPress<T>(this Form @this, object sender, KeyPressEventArgs e)
        {
            if (((DetailBase<T>)@this)._statusForm.Equals(ConstFrm.optStatusForm.View)) return;
            UltraGrid uGrid = (UltraGrid)sender;
            if (uGrid.ActiveCell != null)
            {
                if (uGrid.ActiveCell.Column.ValueList != null)
                {
                    //_isKeyPress = true; 
                    //(uGrid.ActiveCell.ValueListResolved as UltraCombo).SetValueForTag("KeyPress", true);
                    if (!uGrid.ActiveCell.DroppedDown) uGrid.PerformAction(UltraGridAction.ToggleDropdown);
                }
                if (uGrid.Name.Equals("uGridControl"))
                {
                    var dicTag = new Dictionary<string, object>();
                    if (uGrid.ActiveCell.Tag != null)
                    {
                        dicTag = (Dictionary<string, object>)uGrid.ActiveCell.Tag;
                        if (dicTag.Any(p => p.Key.Equals("KeyPress")))
                            dicTag.Remove("KeyPress");
                    }
                    dicTag.Add("KeyPress", true);
                    uGrid.ActiveCell.Tag = dicTag;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TK">Đối tượng của Form</typeparam>
        /// <param name="this"> </param>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void uGrid_KeyDown<TK>(this Form @this, object sender, KeyEventArgs e)
        {
            var @base = @this as DetailBase<TK>;
            if (@base != null && @base._statusForm.Equals(ConstFrm.optStatusForm.View)) return;
            UltraGrid uGrid = (UltraGrid)sender;
            if (uGrid.ActiveCell != null)
            {
                if (uGrid.ActiveCell.ValueListResolved != null)
                    @this.ShowQuickUtils_ByKeys<TK>(e.KeyCode, uGrid.ActiveCell.Column.Key, uGrid);
                UltraGridCell oldIndex = null;
                if (uGrid.ActiveCell.IsInEditMode)
                {
                    switch (e.KeyCode)
                    {
                        //case Keys.Up:
                        //    oldIndex = uGrid.ActiveCell;
                        //    uGrid.PerformAction(UltraGridAction.ExitEditMode, false, false);
                        //    uGrid.PerformAction(UltraGridAction.AboveRow, false, false);
                        //    uGrid.ActiveCell = uGrid.ActiveRow.Cells[oldIndex.Column];
                        //    e.Handled = true;
                        //    uGrid.PerformAction(UltraGridAction.EnterEditMode, false, false);
                        //    break;
                        //case Keys.Down:
                        //    oldIndex = uGrid.ActiveCell;
                        //    uGrid.PerformAction(UltraGridAction.ExitEditMode, false, false);
                        //    uGrid.PerformAction(UltraGridAction.BelowRow, false, false);
                        //    uGrid.ActiveCell = uGrid.ActiveRow.Cells[oldIndex.Column];
                        //    e.Handled = true;
                        //    uGrid.PerformAction(UltraGridAction.EnterEditMode, false, false);
                        //    break;
                        //case Keys.Right:
                        //    uGrid.PerformAction(UltraGridAction.ExitEditMode, false, false);
                        //    uGrid.PerformAction(UltraGridAction.NextCellByTab, false, false);
                        //    e.Handled = true;
                        //    uGrid.PerformAction(UltraGridAction.EnterEditMode, false, false);
                        //    break;
                        //case Keys.Left:
                        //    uGrid.PerformAction(UltraGridAction.ExitEditMode, false, false);
                        //    uGrid.PerformAction(UltraGridAction.PrevCellByTab, false, false);
                        //    e.Handled = true;
                        //    uGrid.PerformAction(UltraGridAction.EnterEditMode, false, false);
                        //    break;
                        case Keys.Enter:
                            uGrid.PerformAction(UltraGridAction.ExitEditMode, false, false);
                            SendKeys.Send("{TAB}");
                            break;
                        case Keys.Tab:
                            List<UltraGridColumn> Columns = new List<UltraGridColumn>();
                            foreach (UltraGridColumn column in uGrid.DisplayLayout.Bands[0].Columns)
                            {
                                Columns.Add(column);
                            }
                            UltraGridColumn lastCol = Columns.OrderByDescending(p => p.Header.VisiblePosition).FirstOrDefault(p => !p.Hidden);
                            if (lastCol != null && uGrid.ActiveCell.Column.Key.Equals(lastCol.Key))
                            {
                                uGrid.PerformAction(UltraGridAction.ExitEditMode, false, false);
                                SendKeys.Send("{ESC}");
                                uGrid.ActiveCell = null;
                                SendKeys.Send("{TAB}");
                            }
                            break;
                    }
                }
                else
                {
                    switch (e.KeyCode)
                    {
                        case Keys.Enter:
                            uGrid.ActiveCell.Selected = true;
                            uGrid.PerformAction(UltraGridAction.EnterEditMode, false, false);
                            break;
                    }
                }
            }
        }

        public static List<Control> GetControlActive(Control ctrlContainer, ref List<string> abc)
        {
            var list = new List<Control>();
            abc.Add(ctrlContainer.GetType().Name);
            if (ctrlContainer is UltraTextEditor || ctrlContainer is UltraCombo || ctrlContainer is UltraOptionSet_Ex || ctrlContainer is UltraDateTimeEditor || ctrlContainer is UltraGrid)
            {
                if (!string.IsNullOrEmpty(ctrlContainer.Name))
                    list.Add(ctrlContainer);
            }
            var controls = ctrlContainer.Controls.Cast<Control>().ToList();
            var sort = controls.OrderBy(p => p.Location.Y);
            foreach (Control control in sort)
            {
                abc.Add(control.GetType().Name);
                if (control is UltraTextEditor || control is UltraCombo || control is UltraOptionSet_Ex || control is UltraDateTimeEditor || control is UltraGrid)
                {
                    if (!string.IsNullOrEmpty(control.Name))
                        list.Add(control);
                }
                else if (control.HasChildren)
                    list.AddRange(GetControlActive(control, ref abc));
            }
            return list;
        }

        /// <summary>
        /// Hiện Form tìm kiếm nhanh theo Shortcut Key
        /// </summary>
        /// <param name="this"> </param>
        /// <param name="key"></param>
        /// <param name="columnName"></param>
        /// <param name="control"></param>
        static void ShowQuickUtils_ByKeys<TK>(this Form @this, Keys @key, string columnName, Control @control)
        {
            if (@key == Keys.F4)
            {
                if (columnName.Contains("AccountingObject"))
                {
                    if (@control.GetType() == typeof(UltraGrid))
                    {
                        if (columnName.Contains("ID"))
                            @this.ShowQuickFilter<AccountingObject>(@control);
                    }
                    else if (@control.GetType() == typeof(UltraCombo))
                    {
                        if (columnName.Contains("Bank"))
                            @this.ShowQuickFilter<AccountingObjectBankAccount>(@control);
                        else
                            @this.ShowQuickFilter<AccountingObject>(@control);
                    }
                }
                else if (columnName.Contains("Account") && !columnName.Contains("BankAccount"))
                {
                    @this.ShowQuickFilter<Account>(@control);
                }
                else if (columnName.Contains("BankAccount") && !columnName.Contains("BankAccountingObject"))
                {
                    @this.ShowQuickFilter<BankAccountDetail>(@control);
                }
                else if (columnName.Contains("MaterialGoods"))
                {
                    @this.ShowQuickFilter<MaterialGoodsCustom>(@control);
                }
                else if (columnName.Contains("CurrencyID"))
                {
                    @this.ShowQuickFilter<Currency>(@control);
                }
                else if (columnName.Contains("InvoiceTypeID"))
                {
                    @this.ShowQuickFilter<InvoiceType>(@control);
                }
                else if (columnName.Equals("GoodsServicePurchaseID"))
                {
                    @this.ShowQuickFilter<GoodsServicePurchase>(@control);
                }
                else if (columnName.Contains("Employee") || columnName.Equals("0") || columnName.Equals("1") || columnName.Equals("2") || columnName.Equals("3"))
                {
                    @this.ShowQuickFilter<AccountingObject>(@control);
                }
                else if (columnName.Equals("BudgetItemID"))
                {
                    @this.ShowQuickFilter<BudgetItem>(@control);
                }
                else if (columnName.Equals("ExpenseItemID"))
                {
                    @this.ShowQuickFilter<ExpenseItem>(@control);
                }
                else if (columnName.Contains("Department"))
                {
                    @this.ShowQuickFilter<Department>(@control);
                }
                else if (columnName.Equals("CostSetID"))
                {
                    @this.ShowQuickFilter<CostSet>(@control);
                }
                else if (columnName.Equals("ContractID"))
                {
                    @this.ShowQuickFilter<EMContract>(@control);
                }
                else if (columnName.Equals("StatisticsCodeID"))
                {
                    @this.ShowQuickFilter<StatisticsCode>(@control);
                }
                else if (columnName.Contains("Repository"))
                {
                    @this.ShowQuickFilter<Repository>(@control);
                }
                else if (columnName.Contains("FixedAssetID"))
                {
                    @this.ShowQuickFilter<FixedAsset>(@control);
                }
                else if (columnName.Contains("CreditCard"))
                {
                    @this.ShowQuickFilter<CreditCard>(@control);
                }
                else if (columnName.Contains("SAQuote"))
                {
                    @this.ShowQuickFilter<SAQuote>(@control);
                }
                else if (columnName.Contains("PaymentClause"))
                {
                    @this.ShowQuickFilter<PaymentClause>(@control);
                }
            }
            else if (@key == Keys.F2)
            {
                if ((columnName.Contains("AccountingObject") && !columnName.Contains("Bank")) || columnName.Equals("0") || columnName.Equals("1") || columnName.Equals("3"))
                {
                    if (@control.GetType() == typeof(UltraGrid))
                    {
                        if (columnName.Contains("ID"))
                            new FAccountingObjectCustomersDetail().ShowFormCreate<AccountingObject, TK>(@this);
                    }
                    else if (@control.GetType() == typeof(UltraCombo))
                        new FAccountingObjectCustomersDetail().ShowFormCreate<AccountingObject, TK>(@this);
                }
                else if (columnName.Contains("BankAccount") && !columnName.Contains("BankAccountingObject") && !columnName.Contains("AccountingObjectBank"))
                {
                    new FBankAccountDetailDetail().ShowFormCreate<BankAccountDetail, TK>(@this);
                }
                else if (columnName.Contains("MaterialGoods"))
                {
                    new FMaterialGoodsDetail().ShowFormCreate<MaterialGoodsCustom, TK>(@this);
                }
                else if (columnName.Equals("GoodsServicePurchaseID"))
                {
                    new FGoodsServicePurchaseDetail().ShowFormCreate<GoodsServicePurchase, TK>(@this);
                }
                else if (columnName.Contains("Employee") || columnName.Equals("2"))
                {
                    new FAccountingObjectEmployeeDetail().ShowFormCreate<AccountingObject, TK>(@this);
                }
                else if (columnName.Equals("BudgetItemID"))
                {
                    new FBudgetItemDetail().ShowFormCreate<BudgetItem, TK>(@this);
                }
                else if (columnName.Equals("ExpenseItemID"))
                {
                    new FExpenseItemDetail().ShowFormCreate<ExpenseItem, TK>(@this);
                }
                else if (columnName.Contains("Department"))
                {
                    new FDepartmentDetail().ShowFormCreate<Department, TK>(@this);
                }
                else if (columnName.Equals("CostSetID"))
                {
                    new FCostSetDetail().ShowFormCreate<CostSet, TK>(@this);
                }
                else if (columnName.Equals("ContractID"))
                {
                    new AddEMContract().ShowFormCreate<EMContract, TK>(@this);
                }
                else if (columnName.Equals("StatisticsCodeID"))
                {
                    new FStatisticsCodeDetail().ShowFormCreate<StatisticsCode, TK>(@this);
                }
                else if (columnName.Contains("Repository"))
                {
                    new FRepositoryDetail().ShowFormCreate<Repository, TK>(@this);
                }
                else if (columnName.Contains("CreditCard"))
                {
                    new FCreditCardDetail().ShowFormCreate<CreditCard, TK>(@this);
                }
            }
        }

        /// <summary>
        /// Hàm hiện Form Thêm mới
        /// </summary>
        /// <typeparam name="TK">Đối tượng của Form chính</typeparam>
        /// <typeparam name="T">Đối tượng Form danh mục </typeparam>
        /// <param name="fCreateNew">Form con sẽ bật lên</param>
        /// <param name="this">Form chính</param>
        public static void ShowFormCreate<T, TK>(this Form fCreateNew, Form @this)
        {
            fCreateNew.FormClosed += new FormClosedEventHandler(@this.FormCreate_FormClosed<T, TK>);
            fCreateNew.ShowDialog();
        }

        static void FormCreate_FormClosed<T, TK>(this Form @this, object sender, FormClosedEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                //if(((Form)sender).DialogResult == DialogResult.OK)
                ClearCacheByType<T>();
                var form = sender as Form;
                if (form != null && form.Name.Equals("FMaterialGoodsDetail"))
                    @this.ReloadComboInGridByName("MaterialGoodsID");
                UltraGrid uGrid = FindGridIsActived(@this);
                if (uGrid != null)
                {
                    if (uGrid.ActiveCell != null)
                    {
                        uGrid.ActiveCell.ValueListResolved.CloseUp();
                        //_isKeyPress = false;
                        (uGrid.ActiveCell.ValueListResolved as UltraCombo).SetValueForTag("KeyPress", false);
                        var @base = @this as DetailBase<TK>;
                        if (@base != null)
                        {
                            int typeId = @base.TypeID;
                            @this.ConfigEachColumn4Grid<TK>(typeId, uGrid.ActiveCell.Column, uGrid);
                            PropertyInfo propId = sender.GetType().GetProperty("Id");
                            if (propId != null && propId.CanWrite)
                            {
                                var id = propId.GetValue(sender, null);
                                if (id == null || (Guid)id == Guid.Empty) return;
                                uGrid.ActiveCell.Value = id;
                                uGrid.ActiveCell.Row.UpdateData();
                                while (((IList)((UltraCombo)uGrid.ActiveCell.ValueListResolved).DataSource).Count == 0)
                                {
                                    @this.ConfigEachColumn4Grid<TK>(typeId, uGrid.ActiveCell.Column, uGrid);
                                }
                                if (((UltraCombo)uGrid.ActiveCell.ValueListResolved).DataSource.GetType().FullName.GetObjectNameFromBindingName().Equals(typeof(AccountingObject).Name))
                                {
                                    PropertyInfo propSelect = sender.GetType().GetProperty("SelectAccounting");
                                    if (propSelect != null && propSelect.CanWrite)
                                    {
                                        var select = (AccountingObject)propSelect.GetValue(sender, null);
                                        if (@select != null && @select.IsActive)
                                        {
                                            uGrid.AutoFillAccoutingObject(@select);
                                            uGrid.UpdateDataGrid();
                                            uGrid.ActiveCell.Selected = true;
                                            uGrid.PerformAction(UltraGridAction.EnterEditMode, false, false);
                                        }
                                    }
                                    else
                                    {
                                        var select = ListAccountingObject.FirstOrDefault(p => p.ID == (Guid)id);
                                        if (@select != null && @select.IsActive)
                                        {
                                            uGrid.AutoFillAccoutingObject(@select);
                                            uGrid.UpdateDataGrid();
                                            uGrid.ActiveCell.Selected = true;
                                            uGrid.PerformAction(UltraGridAction.EnterEditMode, false, false);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    UltraCombo combo = FindComboIsActived(@this);
                    if (combo != null)
                    {
                        //_isKeyPress = false;
                        combo.SetValueForTag("KeyPress", false);
                        if (combo.IsDroppedDown) combo.ToggleDropdown();
                        combo.Refresh();
                        combo.RefreshSource();
                        PropertyInfo propId = sender.GetType().GetProperty("Id");
                        if (propId != null && propId.CanWrite)
                        {
                            var id = propId.GetValue(sender, null);
                            if (id == null || (Guid)id == Guid.Empty) goto End;
                            combo.Value = id;
                            combo.Update();
                            if (combo.DataSource.GetType().FullName.GetObjectNameFromBindingName().Equals(typeof(AccountingObject).Name))
                            {
                                if (((IList)combo.DataSource).Count > 0)
                                    combo.ConfigComboByTag();
                                while (((IList)combo.DataSource).Count == 0)
                                {
                                    combo.DataSource = ListAccountingObject;
                                    combo.ConfigComboByTag();
                                }
                                combo.ExcuteSelectedRowAutoComplate<AccountingObject>(@this, (Guid)id);
                                //AccountingObject accObj =
                                //    ((BindingList<AccountingObject>)@Combo.DataSource).FirstOrDefault(
                                //        p => p.ID == (Guid)id);
                                //accObj.ExcuteRowSelectedEventOfcbbAutoFilter(@Combo, true);
                            }
                            else if (combo.DataSource.GetType().FullName.GetObjectNameFromBindingName().Equals(typeof(BankAccountDetail).Name))
                            {
                                while (((IList)combo.DataSource).Count == 0)
                                {
                                    combo.DataSource = ListBankAccountDetail;
                                    combo.ClearAutoComplete();
                                    combo.Value = id;
                                    combo.Update();
                                }
                                combo.ExcuteSelectedRowAutoComplate<BankAccountDetail>(@this, (Guid)id);
                            }
                            combo.Update();
                            SendKeys.Send("{TAB}");
                            SendKeys.Send("+{TAB}");
                        }
                        else
                        {
                            if (combo.DataSource.GetType().FullName.GetObjectNameFromBindingName().Equals(typeof(CreditCard).Name))
                            {
                                PropertyInfo propCreditCardNumber = sender.GetType().GetProperty("CreditCardNumber");
                                if (propCreditCardNumber != null && propCreditCardNumber.CanWrite)
                                {
                                    var number = propCreditCardNumber.GetValue(sender, null);
                                    if (number == null || string.IsNullOrEmpty(number.ToString())) goto End;
                                    combo.Value = number;
                                    combo.Update();
                                    while (((IList)combo.DataSource).Count == 0)
                                    {
                                        combo.DataSource = ListCreditCard;
                                        combo.ClearAutoComplete();
                                        combo.Value = propCreditCardNumber;
                                        combo.Update();
                                    }
                                    if (combo.Tag != null)
                                    {
                                        var tag = (Dictionary<string, object>)combo.Tag;
                                        KeyValuePair<string, object>? temp =
                                                tag.FirstOrDefault(
                                                    p => p.Key.Equals("CreditCardControl"));
                                        if (temp != null)
                                        {
                                            var controls = (List<Control>)temp.Value.Value;
                                            combo.ExcuteSelectedRowAutoComplate<CreditCard>(@this, number.ToString(), controls);
                                        }
                                        else
                                            combo.ExcuteSelectedRowAutoComplate<CreditCard>(@this, number.ToString());
                                    }
                                    else
                                        combo.ExcuteSelectedRowAutoComplate<CreditCard>(@this, number.ToString());
                                }
                            }
                        }
                    End:
                        if (StopWatch.IsRunning)
                            StopWatch.Reset();
                        else
                            StopWatch.Start();
                    }
                }
            }
        }
        static void ConfigComboByTag(this UltraCombo combo)
        {
            if (combo.Tag != null)
            {
                var accObjType =
                    (int?)
                    ((Dictionary<string, object>)combo.Tag).FirstOrDefault(
                        p => p.Key.Equals("AccountingObjectType")).Value;
                if (accObjType != null)
                    ConfigComboAccouting(combo, accObjType);
                else combo.ClearAutoComplete();
            }
            else combo.ClearAutoComplete();
        }
        private static void ExcuteSelectedRowAutoComplate<T>(this UltraCombo combo, Form @this, Guid id)
        {
            T model = ((BindingList<T>)combo.DataSource).FirstOrDefault(
                                            p => (Guid)(p.GetType().GetProperty("ID")).GetValue(p, null) == id);
            model.ExcuteRowSelectedEventOfcbbAutoFilter(@this, combo, true);
        }
        private static void ExcuteSelectedRowAutoComplate<T>(this UltraCombo combo, Form @this, string number, List<Control> controls = null)
        {
            T model = ((BindingList<T>)combo.DataSource).FirstOrDefault(
                                            p => (string)(p.GetType().GetProperty("CreditCardNumber")).GetValue(p, null) == number);
            model.ExcuteRowSelectedEventOfcbbAutoFilter(@this, combo, true, controls);
        }
        /// <summary>
        /// Reset DataSource
        /// </summary>
        /// <param name="combo"></param>
        public static void RefreshSource(this UltraCombo combo)
        {
            if (combo.DataBindings.Count == 0) return;
            //var bindingMember = combo.DataBindings[0].DataSource;
            //PropertyInfo propInfo = bindingMember.GetType().GetProperty("AccountingObjectType");
            //if ((combo.DataSource is BindingList<AccountingObject>) && propInfo != null && propInfo.CanWrite && propInfo.CanRead)
            //{
            //    var accType = (int?)propInfo.GetValue(bindingMember, null);
            //    if (accType != null)
            //    {
            //        combo.DataSource = ListAccountingObject;
            //        combo.ConfigComboAccouting(accType);
            //        combo.Update();
            //        while (((IList)combo.DataSource).Count == 0)
            //        {
            //            combo.DataSource = ListAccountingObject;
            //            combo.ConfigComboAccouting(accType);
            //        }
            //    }
            //    else combo.ClearAutoComplete();
            //}
            if ((combo.DataSource is BindingList<AccountingObject>))
            {
                var bindingMember = combo.DataBindings[0].DataSource;
                if (bindingMember.HasProperty("AccountingObjectType"))
                {
                    var accType = (int?)bindingMember.GetProperty("AccountingObjectType");
                    if (accType != null)
                    {
                        ClearCacheByType<AccountingObject>();
                        combo.DataSource = ListAccountingObject;
                        combo.ConfigComboAccouting(accType);
                        combo.Update();
                        while (((IList)combo.DataSource).Count == 0)
                        {
                            combo.DataSource = ListAccountingObject;
                            combo.ConfigComboAccouting(accType);
                        }
                    }
                    else combo.ClearAutoComplete();
                }
                else combo.ClearAutoComplete();
            }
            else if (combo.DataSource is BindingList<BankAccountDetail>)
            {
                ClearCacheByType<BankAccountDetail>();
                combo.DataSource = ListBankAccountDetail;
                combo.ClearAutoComplete();
                combo.Update();
                while (((IList)combo.DataSource).Count == 0)
                {
                    combo.DataSource = ListBankAccountDetail;
                    combo.ClearAutoComplete();
                }
            }
            else if (combo.DataSource is BindingList<CreditCard>)
            {
                ClearCacheByType<CreditCard>();
                combo.DataSource = ListCreditCard;
                combo.ClearAutoComplete();
                combo.Update();
                while (((IList)combo.DataSource).Count == 0)
                {
                    combo.DataSource = ListCreditCard;
                    combo.ClearAutoComplete();
                }
            }
            else if (combo.DataSource is BindingList<FixedAsset>)
            {
                ClearCacheByType<FixedAsset>();
                combo.DataSource = ListFixedAsset;
                int? fixedAssetType = null;
                if (@combo.Tag != null)
                {
                    var dicTag = (Dictionary<string, object>)@combo.Tag;
                    if (dicTag.Any(p => p.Key.Equals("FixedAssetType")))
                        fixedAssetType = (int?)dicTag.FirstOrDefault(p => p.Key.Equals("FixedAssetType")).Value;
                }
                combo.ConfigComboFixedAsset(fixedAssetType);
                combo.Update();
                while (((IList)combo.DataSource).Count == 0)
                {
                    combo.DataSource = ListFixedAsset;
                    combo.ConfigComboFixedAsset(fixedAssetType);
                }
            }
        }

        /// <summary>
        /// Sự kiện CellChange xử lý sự kiện autocomplate
        /// [Huy Anh]
        /// </summary>
        /// <param name="this"> </param>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void uGrid_CellChange<TK>(this Form @this, object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            UltraGrid uGrid = (UltraGrid)sender;
            var @base = @this as DetailBase<TK>;
            if (e.Cell.Column.Key.Equals("UnitPrice") && e.Cell.Value != null && (decimal)e.Cell.Row.Cells["UnitPriceOriginal"].Value != 0)
            {
                e.Cell.Row.UpdateData();
                if (@base != null)
                    @base.ExchangeRate = (decimal)e.Cell.Value / (decimal)e.Cell.Row.Cells["UnitPriceOriginal"].Value;
            }
            if (e.Cell.Column.ValueList != null)
            {
                UltraCombo ultraCombo = (UltraCombo)e.Cell.Column.ValueList;
                AutoComplateForUltraCombo(ultraCombo, e.Cell.Text);
            }
            if (e.Cell.Column.Key.Contains("AccountingObject"))
            {
                //if (_isKeyPress)
                //if (e.Cell.Column.ValueList != null && (e.Cell.ValueListResolved as UltraCombo).GetValueFromTag<bool>("KeyPress") == true)
                //{
                //    RemoveNotificationCell(uGrid, e.Cell);
                //}
            }
            else
            {
                //if (_isKeyPress)
                //{
                //    if (string.IsNullOrEmpty(e.Cell.Text.Trim()))
                //        RemoveNotificationCell(uGrid, e.Cell);
                //}
            }
            if (e.Cell.Column.Key.Equals("VATRate") || e.Cell.Column.Key.Equals("VATAmount"))
            {
                e.Cell.Row.UpdateData();
                if (uGrid.DisplayLayout.Bands[0].Columns.Exists("MaterialGoodsID") && uGrid.DisplayLayout.Bands[0].Columns.Exists("Amount"))
                {
                    if (e.Cell.Column.Key.Equals("VATRate"))
                    {
                        decimal vatRate = e.Cell.Value != null
                                                  ? (decimal)e.Cell.Value / 100
                                                  : 0;
                        e.Cell.Row.Cells["VATAmount"].Value = (e.Cell.Row.Cells["Amount"].Value != null ? (decimal)e.Cell.Row.Cells["Amount"].Value : 0) * vatRate;
                        e.Cell.Row.Cells["VATAmountOriginal"].Value = (e.Cell.Row.Cells["AmountOriginal"].Value != null ? (decimal)e.Cell.Row.Cells["AmountOriginal"].Value : 0) * vatRate;
                        if (@base != null) @this.SetVatAmount<TK>(uGrid, e.Cell, @base.TypeID);
                        e.Cell.Row.UpdateData();
                    }
                }
                else
                    CalculationVat(uGrid, e.Cell.Row.Index);
            }
            if (@base != null)
            {
                @base.Checked = false;
                if (e.Cell.Column.Key == "IsImportPurchase")
                {
                    if (e.Cell.EditorComponentResolved != null)
                    {
                        if (e.Cell.EditorResolved.IsDroppedDown)
                        {
                            var typeId = @base.TypeID;
                            Template template = GetMauGiaoDien(@base.TypeID, null);
                            int countTabs = template.TemplateDetails.Count(p => p.TabIndex != 100);
                            for (int i = 0; i < countTabs; i++)
                            {
                                var ultraGrid =
                                    (UltraGrid)@this.Controls.Find(string.Format("uGrid{0}", i), true).FirstOrDefault();
                                if (ultraGrid != null)
                                {
                                    if (e.Cell.EditorResolved.Value != null)
                                    {
                                        var isImportPurchase = (bool)e.Cell.EditorResolved.Value;
                                        ultraGrid.ConfigColumnByImportPurchase(isImportPurchase);
                                    }
                                    foreach (var column in ultraGrid.DisplayLayout.Bands[0].Columns)
                                    {
                                        if (@column.Key.Contains("Account") && !@column.Key.Contains("BankAccount") &&
                                                 !@column.Key.Contains("AccountingObject")) //tài khoản nợ, tài khoản có
                                        {
                                            @this.ConfigAccountColumnInGrid<TK>(typeId, ultraGrid, column);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (e.Cell.Column.Key == "CurrencyID" && uGrid.Name.Equals("uGridControl") && e.Cell.ValueListResolved.IsDroppedDown)
                {
                    if (@base._select.HasProperty("CurrencyID"))
                        @base._select.SetProperty("CurrencyID", e.Cell.Text);
                }
            }
        }

        public static void UpdateData(this UltraGridRow row)
        {
            try
            {
                row.Update();
            }
            catch
            { }
        }
        public static void UpdateData(this UltraGridBase grid)
        {
            try
            {
                grid.UpdateData();
            }
            catch
            { }
        }

        public static void uGrid_AfterCellActivate<TK>(this Form _this, object sender, EventArgs e)
        {
            UltraGrid uGrid = (UltraGrid)sender;
            UltraGridCell cell = uGrid.ActiveCell;
            if (cell.Column.ValueList != null)
            {
                if (!(cell.Column.Key.Contains("Account") && !cell.Column.Key.Contains("AccountingObject") && !cell.Column.Key.Contains("BankAccount") || cell.Column.Key.Contains("Currency") || cell.Column.Key.Equals("InvoiceTypeID") || cell.Column.Key.Equals("FixedAssetID") || cell.Column.Key.Equals("SAQuoteID") || cell.Column.Key.Equals("PaymentClauseID")))
                    ((DetailBase<TK>)_this).SetTextStatusBar(resSystem.MSG_System_08);
                else
                    ((DetailBase<TK>)_this).SetTextStatusBar(resSystem.MSG_System_09);
            }
            else
            {
                ((DetailBase<TK>)_this).SetTextStatusBar(string.Empty);
            }
        }

        /// <summary>
        /// Tự động Fill tổng tiền vào Grid Tiền tệ
        /// </summary>
        /// <typeparam name="TK"></typeparam>
        /// <param name="this"></param>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static void uGrid_SummaryValueChanged<TK>(this Form @this, object sender, SummaryValueChangedEventArgs e)
        {
            UltraGrid uGrid = (UltraGrid)sender;
            List<int> blackList = new List<int>() { 10, 11, 12, 16 };//DS các Form nghiệp vụ không sử dụng chức năng này
            var @base = @this as DetailBase<TK>;
            if (@base == null) return;
            if (blackList.Any(p => p == @base.TypeGroup)) return;
            TK select = @base._select;
            if (e.SummaryValue.Key.Contains("Sum") && (e.SummaryValue.Key.Contains("Amount") || e.SummaryValue.Key.Contains("OrgPrice")) && e.SummaryValue.Key.Contains("Original"))
            {
                string name = e.SummaryValue.Key.Split(new string[] { "Sum" }, StringSplitOptions.RemoveEmptyEntries)[0];
                string keyName = string.Format("Total{0}", name);
                UltraGrid uGridControl =
                            (UltraGrid)@this.Controls.Find("uGridControl", true).FirstOrDefault();
                if (uGridControl != null)
                {
                    if (uGridControl.DisplayLayout.Bands[0].Columns.Exists(keyName))
                    {
                        uGridControl.Rows[0].Cells[keyName].Value = e.SummaryValue.Value;
                        uGridControl.Rows[0].Cells[keyName].CheckMaxValueByMoney();
                        goto Step1;
                    }
                }
                if (select.HasProperty(keyName))
                    select.SetProperty(keyName, Convert.ToDecimal(e.SummaryValue.Value));
            Step1:
                string key = name.Split(new string[] { "Original" }, StringSplitOptions.RemoveEmptyEntries)[0];
                string keyCol = string.Format("Total{0}", key);
                if (uGridControl != null && uGridControl.DisplayLayout.Bands[0].Columns.Exists(keyCol))
                {
                    if (!uGridControl.DisplayLayout.Bands[0].Columns.Exists(keyName)) goto Step2;
                    uGridControl.Rows[0].Cells[keyCol].Value = ((decimal?)uGridControl.Rows[0].Cells[keyName].Value ?? 0) * (select.GetProperty<TK, decimal?>("ExchangeRate") ?? 1);
                    uGridControl.Rows[0].Cells[keyCol].CheckMaxValueByMoney();
                }
                else if (select.HasProperty(keyCol))
                {
                    if (select.HasProperty(keyName))
                        select.SetProperty(keyCol, (select.GetProperty<TK, decimal?>(keyName) ?? 0) * (select.GetProperty<TK, decimal?>("ExchangeRate") ?? 1));
                    else goto Step2;
                }
                return;
            Step2:
                if (uGrid.DisplayLayout.Bands[0].Columns[name].Hidden)
                {
                    //((UltraGrid)sender).Rows.SummaryValues[e.SummaryValue.Key.Split(new string[] { "Original" }, StringSplitOptions.RemoveEmptyEntries)[0]].Value=
                    decimal sum = 0;
                    name = name.Split(new string[] { "Original" }, StringSplitOptions.RemoveEmptyEntries)[0];
                    keyName = string.Format("Total{0}", name);
                    foreach (UltraGridRow row in uGrid.Rows)
                    {
                        var value =
                            (decimal?)row.Cells[name].
                                Value;
                        sum += value ?? 0;
                    }
                    if (uGridControl != null)
                    {
                        if (uGridControl.DisplayLayout.Bands[0].Columns.Exists(keyName))
                        {
                            uGridControl.Rows[0].Cells[keyName].Value = e.SummaryValue.Value;
                            uGridControl.Rows[0].Cells[keyName].CheckMaxValueByMoney();
                            return;
                        }
                    }
                    PropertyInfo propTotalAmount = select.GetType().GetProperty(keyName);
                    if (propTotalAmount != null && propTotalAmount.CanWrite)
                        propTotalAmount.SetValue(select, sum, null);
                }
            }
            else if (e.SummaryValue.Key.Contains("Sum") && (e.SummaryValue.Key.Contains("Amount") || e.SummaryValue.Key.Contains("OrgPrice")) && !e.SummaryValue.Key.Contains("Original") && !e.SummaryValue.Key.Contains("AfterTax"))
            {
                string name = e.SummaryValue.Key.Split(new string[] { "Sum" }, StringSplitOptions.RemoveEmptyEntries)[0];
                string keyName = string.Format("Total{0}", name);
                if (e.SummaryValue.Value == null || (decimal)e.SummaryValue.Value == 0)
                {
                    PropertyInfo[] propInfo = select.GetType().GetProperties();
                    decimal exchangeRate = 1;
                    if (propInfo.Any(p => p.Name.Equals("ExchangeRate")))
                    {
                        var firstOrDefault = propInfo.FirstOrDefault(p => p.Name.Equals("ExchangeRate"));
                        if (firstOrDefault != null)
                        {
                            var changeType = firstOrDefault.GetValue(@select, null);
                            exchangeRate = changeType == null
                                                   ? 1
                                                   : (decimal)
                                                     changeType;
                        }
                    }

                    foreach (PropertyInfo propItem in propInfo)
                    {
                        if (propItem.Name.Contains("Total") && (e.SummaryValue.Key.Contains("Amount") || e.SummaryValue.Key.Contains("OrgPrice")) &&
                            !propItem.Name.Contains("Original") && ((UltraGrid)sender).DisplayLayout.Bands[0].Summaries.Exists(string.Format("Sum{0}Original", name)))
                            propItem.SetValue(select,
                                                Convert.ToDecimal(e.SummaryValue.Value ?? (exchangeRate *
                                                                                            (decimal?)
                                                                                            ((UltraGrid)sender).Rows.SummaryValues[
                                                                                                string.Format("Sum{0}Original", name)].Value ?? 0)), null);
                    }
                    UltraGrid uGridControl =
                                (UltraGrid)@this.Controls.Find("uGridControl", true).FirstOrDefault();
                    if (uGridControl != null)
                    {
                        uGridControl.Refresh();
                    }
                }
                else
                {
                    UltraGrid uGridControl =
                                (UltraGrid)@this.Controls.Find("uGridControl", true).FirstOrDefault();
                    if (uGridControl != null)
                    {
                        if (uGridControl.DisplayLayout.Bands[0].Columns.Exists(keyName))
                        {
                            uGridControl.Rows[0].Cells[keyName].Value = e.SummaryValue.Value;
                            uGridControl.Rows[0].Cells[keyName].CheckMaxValueByMoney();
                            return;
                        }
                    }
                    PropertyInfo propInfo = select.GetType().GetProperty(keyName);
                    if (propInfo != null && propInfo.CanWrite)
                        propInfo.SetValue(select, Convert.ChangeType(e.SummaryValue.Value, propInfo.PropertyType), null);
                }
            }
        }

        public static void uGrid_Validated(object sender, EventArgs e)
        {
            //_isKeyPress = false;
        }



        /// <summary>
        /// Trả về trạng thái ban đầu của Combo
        /// [huy anh]
        /// </summary>
        /// <param name="ultraCombo"></param>
        public static void ClearAutoComplete(this UltraCombo ultraCombo)
        {
            if (ultraCombo.Tag != null)
            {
                var dicTag = (Dictionary<string, object>)ultraCombo.Tag;
                if (dicTag.Any(p => p.Key.Equals("AccountingObjectType")))
                {
                    if ((ultraCombo.DataSource is BindingList<AccountingObject>) && ultraCombo.Rows.Count == 0)
                        ultraCombo.DataSource = ListAccountingObject;
                    foreach (UltraGridRow row in ultraCombo.Rows)
                    {
                        row.ConfigComboRowAccounting((int?)dicTag.FirstOrDefault(p => p.Key.Equals("AccountingObjectType")).Value);
                    }
                }
                else
                    ultraCombo.ConfigComboByStatusObject();
            }
            else
            {
                ultraCombo.ConfigComboByStatusObject();
            }
        }

        public static void ConfigComboByStatusObject(this UltraCombo combo)
        {
            if (combo.DisplayLayout.Bands[0].Columns.Exists("IsActive"))
                foreach (UltraGridRow row in combo.Rows)
                {
                    if ((bool)row.Cells["IsActive"].Value)
                    {
                        row.Hidden = false;
                        row.Tag = null;
                    }
                    else
                    {
                        row.Hidden = true;
                        row.Tag = true;
                    }
                }
        }
        public static void ConfigGridByStatusObject(this UltraGrid uGrid)
        {
            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("IsActive"))
                foreach (UltraGridRow row in uGrid.Rows)
                {
                    if ((bool)row.Cells["IsActive"].Value)
                    {
                        row.Hidden = false;
                        row.Tag = null;
                    }
                    else
                    {
                        row.Hidden = true;
                        row.Tag = true;
                    }
                }
        }
        /// <summary>
        /// Kiểm tra tỷ lệ chiết khấu
        /// </summary>
        /// <param name="uGrid"></param>
        /// <param name="cell"></param>
        private static bool CheckDiscount(this UltraGrid uGrid, UltraGridCell cell)
        {
            bool isError = false;
            decimal rate = GetValueFromText(cell);
            if (rate > 100)
            {
                uGrid.NotificationCell(cell, resSystem.MSG_System_39);
                isError = true;
            }
            else
            {
                //if (uGrid.DisplayLayout.Bands[0].Columns.Exists("DiscountAmountOriginal") &&
                //    uGrid.DisplayLayout.Bands[0].Columns.Exists("AmountOriginal"))
                //{
                //    var a = Convert.ToDecimal(cell.Row.Cells["AmountOriginal"].Value.ToString());
                //    var b = a * rate / 100;
                //    cell.Row.Cells["DiscountAmountOriginal"].Value = b;
                //    if (exchangeRate != null) cell.Row.Cells["DiscountAmount"].Value = b * exchangeRate;
                //    uGrid.UpdateDataGrid();
                //}
                uGrid.RemoveNotificationCell(cell);
            }
            return isError;
        }
        /// <summary>
        /// Cấu hình ẩn hiện column khi thay đổi loại tiền
        /// </summary>
        /// <param name="uGrid"></param>
        /// <param name="columnName"></param>
        /// <param name="currency"></param>
        public static void ConfigColumnByCurrency(this UltraGrid uGrid, string columnName, string currency)
        {
            bool status = true;
            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("CurrencyID"))
                if (uGrid.Rows.Any(row => !row.Cells["CurrencyID"].Value.Equals("VND")))
                {
                    status = false;
                }
            if (!columnName.Contains("PretaxAmount"))
            {
                if (columnName.Contains("VATAmount"))
                {
                    //Không config ẩn hiện cột nếu:
                    //1.Có "Tiền thuế nguyên tệ" + Cột "Tiền thuế nguyên tệ" ẩn
                    //2.Ko có "Tiền thuế nguyên tệ" + Cột "Tiền thuế quy đổi" ẩn
                    if ((uGrid.DisplayLayout.Bands[0].Columns.Exists(string.Format("{0}Original", columnName)) && uGrid.DisplayLayout.Bands[0].Columns[string.Format("{0}Original", columnName)].Hidden) ||
                        (!uGrid.DisplayLayout.Bands[0].Columns.Exists(string.Format("{0}Original", columnName)) && !uGrid.DisplayLayout.Bands[0].Columns[columnName].Hidden))
                        return;
                }
                if (currency == null) return;
                uGrid.DisplayLayout.Bands[0].Columns[columnName].Hidden = status && currency.Equals("VND");
            }
        }

        /// <summary>
        /// Hàm xử lý khi báo lỗi DataCell
        /// </summary>
        /// <param name="uGrid"></param>
        public static void CellDataError(this UltraGrid uGrid)
        {
            UltraGridCell cell = uGrid.ActiveCell;
            //if (uGrid.Name.Equals("uGridPosted") || uGrid.Name.Equals("uGridStatistics"))
            //{
            if (cell.Column.Key.Equals("DebitAccountingObjectID") || cell.Column.Key.Equals("CreditAccountingObjectID") || cell.Column.Key.Equals("EmployeeID") || cell.Column.Key.Equals("AccountingObjectID"))
            {
                //string filter = cell.Text;
                int count = cell.Column.Key.Equals("EmployeeID")
                                ? ListAccountingObject.Count(x => x.AccountingObjectCode == cell.Text && x.IsEmployee)
                                : ListAccountingObject.Count(x => x.AccountingObjectCode == cell.Text);
                if (count == 0)
                {
                    if (uGrid.Name.Equals("DebitAccountingObjectID"))
                    {
                        NotificationCell(uGrid, cell,
                                               resSystem.MSG_System_32, true);
                        ClearCellAccountingObject(uGrid);
                    }
                    else if (cell.Column.Key.Equals("CreditAccountingObjectID"))
                    {
                        NotificationCell(uGrid, cell,
                                               resSystem.MSG_System_33, true);
                        ClearCellAccountingObject(uGrid);
                    }
                    else if (cell.Column.Key.Equals("EmployeeID"))
                    {
                        NotificationCell(uGrid, cell,
                                               resSystem.MSG_System_34, true);
                    }
                    else
                    {
                        NotificationCell(uGrid, cell,
                                              resSystem.MSG_System_35, true);
                        ClearCellAccountingObject(uGrid);
                    }
                }
            }
            else if (new[] { "MaterialGoodsID", "DebitAccount", "CreditAccount", "BankAccountDetailID", "BudgetItemID", "ExpenseItemID", "CostSetID", "StatisticsCodeID", "ContractID",
            "GoodsServicePurchaseID","VATAccount","PaymentClauseID","InvoiceTypeID","FixedAssetID"}.Contains(cell.Column.Key) ||
                     cell.Column.Key.Contains("Repository") ||
                     cell.Column.Key.Contains("Department"))
            {
                NotificationCell(uGrid, cell,
                                       resSystem.MSG_System_30, true);
            }
            else if (cell.Column.Key.Equals("FinalDate")) cell.Value = 0;
        }

        static void ClearCellAccountingObject(this UltraGrid uGrid, UltraGridCell cell = null)
        {
            if (cell != null || (cell == null && uGrid.ActiveCell != null))
            {
                if (cell == null) cell = uGrid.ActiveCell;
                int index = cell.Row.Index;
                string[] nameCol = cell.Column.Key.Split(new string[] { "AccountingObject", "ID" },
                                                                        StringSplitOptions
                                                                            .RemoveEmptyEntries);
                if (cell.Column.Key.Contains("AccountingObject") && cell.Column.Key.Contains("ID"))
                {
                    string key;
                    if (
                        cell.Column.Key.Split(new string[] { "AccountingObject" },
                                                          StringSplitOptions.RemoveEmptyEntries).Length > 1)
                    {
                        key = string.Format("{0}AccountingObject{1}Name", nameCol[0],
                                            (nameCol.Length > 1 ? nameCol[1] : string.Empty));
                        if (uGrid.DisplayLayout.Bands[0].Columns.Exists(key))
                            uGrid.Rows[index].Cells[key].Value = "";
                        key = string.Format("{0}AccountingObject{1}Address", nameCol[0],
                                            (nameCol.Length > 1 ? nameCol[1] : string.Empty));
                        if (uGrid.DisplayLayout.Bands[0].Columns.Exists(key))
                            uGrid.Rows[index].Cells[key].Value = "";
                    }
                    else
                    {
                        key = string.Format("AccountingObject{0}Name", (nameCol.Length > 0 ? nameCol[0] : string.Empty));
                        if (uGrid.DisplayLayout.Bands[0].Columns.Exists(key))
                            uGrid.Rows[index].Cells[key].Value = "";
                        key = string.Format("AccountingObject{0}Address",
                                            (nameCol.Length > 0 ? nameCol[0] : string.Empty));
                        if (uGrid.DisplayLayout.Bands[0].Columns.Exists(key))
                            uGrid.Rows[index].Cells[key].Value = "";
                    }
                    if (uGrid.DisplayLayout.Bands[0].Columns.Exists("CompanyTaxCode"))
                    {
                        uGrid.Rows[index].Cells["CompanyTaxCode"].Value = "";
                        bool isError = false;
                        if (uGrid.Rows[index].Cells["CompanyTaxCode"].Value != null && !string.IsNullOrEmpty(uGrid.Rows[index].Cells["CompanyTaxCode"].Value.ToString()))
                        {
                            isError = !Accounting.Core.Utils.CheckMST(uGrid.Rows[index].Cells["CompanyTaxCode"].Value.ToString());
                            if (isError)
                                NotificationCell(uGrid, uGrid.Rows[index].Cells["CompanyTaxCode"], resSystem.MSG_System_44);
                            else
                                RemoveNotificationCell(uGrid, uGrid.Rows[index].Cells["CompanyTaxCode"]);
                        }
                        uGrid.Rows[index].Cells["CompanyTaxCode"].SetErrorForCell(isError);
                    }
                }
            }
        }

        /// <summary>
        /// Kiểm tra tài khoản
        /// </summary>
        /// <param name="uGrid"></param>
        /// <param name="cell"> </param>
        /// <returns></returns>
        public static bool CheckAccount(this UltraGrid uGrid, UltraGridCell cell = null)
        {
            bool isError = false;
            if (cell == null) cell = uGrid.ActiveCell;
            if (cell.Column.Key.Equals("DebitAccount") || cell.Column.Key.Equals("CreditAccount") ||
                cell.Column.Key.Equals("VATAccount"))
            {
                List<Account> listAccounts =
                    ((List<Account>)((UltraCombo)cell.Column.ValueList).DataSource).Where(
                        p => p.AccountNumber == cell.Text.Trim() && p.GetProperty<Account, bool>("IsActive")).ToList();
                if (string.IsNullOrEmpty(cell.Text.Trim()))
                {
                    if (cell.Column.Key.Equals("DebitAccount"))
                    {
                        if (uGrid.DisplayLayout.Bands[0].Columns.Exists("CreditAccount") && cell.Row.Cells["CreditAccount"].Value != null &&
                            !cell.Row.Cells["CreditAccount"].Value.ToString().StartsWith("00"))
                            isError = true;
                    }
                    else if (cell.Column.Key.Equals("CreditAccount"))
                    {
                        if (uGrid.DisplayLayout.Bands[0].Columns.Exists("DebitAccount") && cell.Row.Cells["DebitAccount"].Value != null &&
                            !cell.Row.Cells["DebitAccount"].Value.ToString().StartsWith("00"))
                            isError = true;
                    }
                    if (isError)
                        NotificationCell(uGrid, cell,
                                       resSystem.MSG_System_31);
                    else
                        RemoveNotificationCell(uGrid, cell);
                }
                else if (listAccounts.Count == 0)
                {
                    NotificationCell(uGrid, cell,
                                       resSystem.MSG_System_30);
                    isError = true;
                }
                else
                {
                    var account = listAccounts.FirstOrDefault();
                    if (account != null && account.IsParentNode)
                    {
                        NotificationCell(uGrid, cell, resSystem.MSG_System_29);
                        isError = true;
                    }
                    else
                    {
                        if (cell.Column.Key.Equals("DebitAccount"))
                        {
                            if (cell.Value.ToString().StartsWith("00") && string.IsNullOrEmpty(cell.Row.Cells["CreditAccount"].Text.Trim()))
                            {
                                RemoveNotificationCell(uGrid, cell.Row.Cells["CreditAccount"]);
                                cell.Row.Cells["CreditAccount"].SetErrorForCell(false);
                            }
                            else if (string.IsNullOrEmpty(cell.Row.Cells["CreditAccount"].Text.Trim()))
                            {
                                NotificationCell(uGrid, cell.Row.Cells["CreditAccount"], resSystem.MSG_System_31);
                                cell.Row.Cells["CreditAccount"].SetErrorForCell(true);
                            }
                        }
                        else if (cell.Column.Key.Equals("CreditAccount"))
                        {
                            if (cell.Value.ToString().StartsWith("00") && string.IsNullOrEmpty(cell.Row.Cells["DebitAccount"].Text.Trim()))
                            {
                                RemoveNotificationCell(uGrid, cell.Row.Cells["DebitAccount"]);
                                cell.Row.Cells["DebitAccount"].SetErrorForCell(false);
                            }
                            else if (string.IsNullOrEmpty(cell.Row.Cells["DebitAccount"].Text.Trim()))
                            {
                                NotificationCell(uGrid, cell.Row.Cells["DebitAccount"], resSystem.MSG_System_31);
                                cell.Row.Cells["DebitAccount"].SetErrorForCell(true);
                            }
                        }
                        RemoveNotificationCell(uGrid, cell);
                    }
                }
                //if (cell.Column.Key.Equals("DebitAccount"))
                //{
                //    if (uGrid.DisplayLayout.Bands[0].Columns.Exists("CreditAccount") &&
                //            cell.Value.ToString().StartsWith("00"))
                //        RemoveNotificationCell(uGrid, cell.Row.Cells["CreditAccount"]);
                //}  
                //else if (cell.Column.Key.Equals("CreditAccount"))
                //{
                //    if (uGrid.DisplayLayout.Bands[0].Columns.Exists("DebitAccount") &&
                //        cell.Value.ToString().StartsWith("00"))
                //        RemoveNotificationCell(uGrid, cell.Row.Cells["DebitAccount"]);
                //}
            }
            return isError;
        }
        #endregion
        #endregion

        #region xử lý combo
        #region Config Combo

        /// <summary>
        /// Config combo bình thường
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="this"> </param>
        /// <param name="input"></param>
        /// <param name="combo"></param>
        /// <param name="displayMember"></param>
        /// <param name="valueMember"></param>
        /// <param name="accountingObjectType">Loại đối tượng AccountingObject: 0-Khách hàng. 1-Nhà cung cấp. 2-Nhân viên. 3-KH và NCC [Mặc định - Lấy tất cả danh sách được Active]</param>
        public static void ConfigCombo<T>(this Form @this, IList<T> input, UltraCombo combo, string displayMember, string valueMember, int? accountingObjectType = null)
        {
            if (typeof(T) != typeof(AccountingObject))
            {
                combo.AutoCompleteMode = AutoCompleteMode.Suggest;
                combo.AutoSuggestFilterMode = AutoSuggestFilterMode.Contains;
            }
            combo.DataSource = input;
            combo.DisplayMember = displayMember;
            combo.ValueMember = valueMember;
            @this.ConfigGrid<T>(combo, typeof(T).Name);
            combo.TextChanged += new EventHandler(Combo_TextChanged);
            var dicTag = new Dictionary<string, object>();
            if (combo.Tag != null)
            {
                dicTag = (Dictionary<string, object>)combo.Tag;
                if (dicTag.Any(p => p.Key.Equals("AccountingObjectType")))
                    dicTag.Remove("AccountingObjectType");
            }
            if (typeof(T) == typeof(AccountingObject))
                dicTag.Add("AccountingObjectType", accountingObjectType);
            combo.Tag = dicTag;
            combo.KeyPress += new KeyPressEventHandler(Combo_KeyPress);
            combo.Leave += new EventHandler(Combo_Leave);
            combo.MouseEnter += new EventHandler(Combo_MouseEnter);
            if (typeof(T) == typeof(AccountingObject))
            {
                ConfigComboAccouting(combo, accountingObjectType);
                combo.RowSelected += cbbAutoFilter_RowSelected;
            }
            else combo.ClearAutoComplete();
        }
        /// <summary>
        /// Cấu hình cặp đôi 2 combo AccountingObject và AccountingObjectBank
        /// </summary>
        /// <typeparam name="TK">Đối tượng T của Form</typeparam>
        /// <param name="this"></param>
        /// <param name="objectForm">Đối tượng chính cho cả form sẽ được gắn DataBinding với Combo</param>
        /// <param name="sourceAccountingObject">DataSource của Combo AccountingObject</param>
        /// <param name="comboAccountingObject">UltraCombo AccountingObject</param>
        /// <param name="nameBindingAccountingObject">Tên thuộc tính sẽ được Binding với Combo AccountingObject của đối tượng chính</param>
        /// <param name="comboAccountingObjectBank">UltraCombo AccountingObjectBank</param>
        /// <param name="nameBindingAccountingObjectBank">Tên thuộc tính sẽ được Binding với Combo AccountingObjectBank của đối tượng chính</param>
        /// <param name="dataSourceUpdateMode">Chế độ UpdateMode của DataBinding</param>               
        /// <param name="haveEditorButton">Có nút Add bên phải hay không</param>            
        /// <param name="accountingObjectType">Loại đối tượng AccountingObject: 0-Khách hàng. 1-Nhà cung cấp. 2-Nhân viên. 3-KH và NCC</param>
        public static void ConfigCombo<TK>(this Form @this, TK objectForm, IList<AccountingObject> sourceAccountingObject,
                                        UltraCombo comboAccountingObject,
                                        string nameBindingAccountingObject, UltraCombo comboAccountingObjectBank,
                                        string nameBindingAccountingObjectBank,
                                        DataSourceUpdateMode dataSourceUpdateMode =
                                            DataSourceUpdateMode.OnPropertyChanged, bool haveEditorButton = true, int? accountingObjectType = null)
        {
            @this.ConfigCombo(sourceAccountingObject, comboAccountingObject, "AccountingObjectCode", "ID",
                                                    objectForm, nameBindingAccountingObject, dataSourceUpdateMode, comboAccountingObjectBank, haveEditorButton, accountingObjectType);
            var listAccountingObjectBank = new BindingList<AccountingObjectBankAccount>();
            var @base = @this as DetailBase<TK>;
            if (@base != null)
            {
                int statusForm = @base._statusForm;
                if (statusForm == ConstFrm.optStatusForm.View)
                {
                    //var select = @base._select;
                    PropertyInfo propAccId = objectForm.GetType().GetProperty(nameBindingAccountingObject);
                    if (propAccId != null && propAccId.CanRead)
                    {
                        listAccountingObjectBank = new BindingList<AccountingObjectBankAccount>(IAccountingObjectBankAccountService.GetByAccountingObjectID(new Guid(propAccId.GetValue(objectForm, null).ToString())));
                    }
                }
                @this.ConfigCombo(listAccountingObjectBank, comboAccountingObjectBank, "BankAccount", "ID",
                                  objectForm, nameBindingAccountingObjectBank, dataSourceUpdateMode, haveEditorButton: false);
                if (statusForm == ConstFrm.optStatusForm.Add) return;
                comboAccountingObject.Refresh();
                if (statusForm == ConstFrm.optStatusForm.View)
                {
                    var dataBinding = (TK)comboAccountingObject.DataBindings[0].DataSource;
                    PropertyInfo propAccId = dataBinding.GetType().GetProperty("AccountingObjectID");
                    if (propAccId != null && propAccId.CanWrite)
                        comboAccountingObject.Value = (Guid?)propAccId.GetValue(dataBinding, null);
                }
            }
            @this.ConfigCombo<TK>(comboAccountingObject, comboAccountingObjectBank);
        }
        /// <summary>
        /// Cấu hình cho Combo CreditCard có kèm icon loại thẻ, tên loại thẻ và tên chủ thẻ
        /// </summary>
        /// <typeparam name="TK">Đối tượng T của Form</typeparam>
        /// <param name="this"></param>
        /// <param name="dataBinding">Đối tượng chính cho cả form sẽ được gắn DataBinding với Combo</param>
        /// <param name="bindingObjectName">Tên thuộc tính sẽ được Binding của đối tượng chính</param>
        /// <param name="source">List hoặc BindingList làm Datasource</param>
        /// <param name="combo">Combo CreditCard</param>
        /// <param name="picture">UltraPictureBox icon loại thẻ</param>
        /// <param name="creditCardType">Control tên loại thẻ</param>
        /// <param name="ownerCard">Control chủ thẻ</param>
        /// <param name="dataSourceUpdateMode"></param>     
        /// <param name="haveEditorButton">Có nút Add bên phải hay không</param>
        public static void ConfigCombo<TK>(this Form @this, TK dataBinding, string bindingObjectName, IList<CreditCard> source,
                                       UltraCombo combo, Control picture, Control creditCardType, Control ownerCard,
                                       DataSourceUpdateMode dataSourceUpdateMode =
                                           DataSourceUpdateMode.OnPropertyChanged, bool haveEditorButton = true)
        {
            picture.Visible = false;
            var controls = new List<Control> { picture, creditCardType, ownerCard };
            @this.ConfigCombo(source, combo, "CreditCardNumber", "CreditCardNumber",
                                                    dataBinding, bindingObjectName, controls, dataSourceUpdateMode, haveEditorButton);
        }
        /// <summary>
        /// Hàm cấu hình cho UltraCombo độc lập với đầu vào là 1 IList
        /// </summary>
        /// <typeparam name="T">Đối tượng T của Datasource của combo</typeparam>
        /// <typeparam name="TK">Đối tượng T của Form</typeparam>
        /// <param name="this"></param>
        /// <param name="input">List hoặc BindingList làm Datasource</param>
        /// <param name="combo">UltraCombo cần cấu hình</param>
        /// <param name="displayMember">DisplayMember</param>
        /// <param name="valueMember">ValueMember</param>
        /// <param name="dataBinding">Đối tượng chính cho cả form sẽ được gắn DataBinding với Combo</param>
        /// <param name="bindingObjectName">Tên thuộc tính sẽ được Binding của đối tượng chính</param>
        /// <param name="dataSourceUpdateMode">Chế độ UpdateMode của DataBinding</param>
        /// <param name="comboAccountingObjectBank">UltraCombo AccountingObjectBank theo AccountingObjectID nếu có</param>
        /// <param name="haveEditorButton">Có nút Add bên phải hay không</param>         
        /// <param name="accountingObjectType">Loại đối tượng AccountingObject: 0-Khách hàng. 1-Nhà cung cấp. 2-Nhân viên. 3-KH và NCC</param>
        public static void ConfigCombo<T, TK>(this Form @this, IList<T> input, UltraCombo combo, string displayMember, string valueMember, TK dataBinding, string bindingObjectName = null, DataSourceUpdateMode dataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged, UltraCombo comboAccountingObjectBank = null, bool haveEditorButton = true, int? accountingObjectType = null)
        {
            if (typeof(T) == typeof(AccountingObject))
            {
                var @base = @this as DetailBase<TK>;
                if (@base != null) @base._cbbAccountingObject = combo;
                combo.AutoCompleteMode = AutoCompleteMode.None;
            }
            else
            {
                combo.AutoCompleteMode = AutoCompleteMode.Suggest;
                combo.AutoSuggestFilterMode = AutoSuggestFilterMode.Contains;
            }
            combo.DataSource = input;
            combo.DisplayMember = displayMember;
            combo.ValueMember = valueMember;
            @this.ConfigGrid<T>(combo, typeof(T).Name);
            var baseType = @this.GetType().BaseType;
            if (baseType != null && (baseType.BaseType != null && baseType.BaseType.Name.Contains(typeof(DetailBase<TK>).Name)))
                combo.Enter += new EventHandler(@this.Combo_Enter<T, TK>);
            combo.KeyDown += new KeyEventHandler(@this.Combo_KeyDown<TK>);
            combo.TextChanged += new EventHandler(Combo_TextChanged);
            //if (@ComboAccountingObjectBank == null)
            combo.RowSelected += new RowSelectedEventHandler(@this.cbbAutoFilter_RowSelected<T, TK>);
            //else
            //    @Combo.RowSelected +=
            //        new RowSelectedEventHandler(@ComboAccountingObjectBank.cbbAutoFilter_RowSelected<TK>);
            var dicTag = new Dictionary<string, object>();
            if (combo.Tag != null)
            {
                dicTag = (Dictionary<string, object>)combo.Tag;
                if (dicTag.Any(p => p.Key.Equals("AccountingObjectType")))
                    dicTag.Remove("AccountingObjectType");
            }
            if (typeof(T) == typeof(AccountingObject))
                dicTag.Add("AccountingObjectType", accountingObjectType);
            combo.Tag = dicTag;
            combo.KeyPress += new KeyPressEventHandler(Combo_KeyPress);
            combo.Validated += new EventHandler(@this.Combo_Validated<TK>);
            combo.Leave += new EventHandler((sender, e) => @this.Combo_Leave<T, TK>(sender, e, comboAccountingObjectBank ?? null));
            combo.MouseEnter += new EventHandler(Combo_MouseEnter);
            if (dataBinding != null)
            {
                combo.DataBindings.Clear();
                combo.DataBindings.Add("Value", dataBinding, bindingObjectName, true, dataSourceUpdateMode);
            }
            if (haveEditorButton)
            {
                var @base = @this as DetailBase<TK>;
                var editorButton = new EditorButton
                                       {
                                           Appearance =
                                               {
                                                   BorderAlpha = Alpha.Transparent,
                                                   BackGradientStyle = GradientStyle.None,
                                                   Image = Properties.Resources.ubtnAdd4,
                                                   ImageHAlign = HAlign.Center,
                                                   ImageVAlign = VAlign.Middle
                                               },
                                           Enabled =
                                               @base != null && !@base._statusForm.Equals(ConstFrm.optStatusForm.View)
                                       };
                combo.ButtonsRight.Clear();
                combo.ButtonsRight.Add(editorButton);
                combo.EditorButtonClick += @this.Combo_EditorButtonClick<TK>;
            }
            if (typeof(T) == typeof(AccountingObject)) ConfigComboAccouting(combo, accountingObjectType);
            else combo.ClearAutoComplete();
        }

        /// <summary>
        /// Cấu hình combo AccountingObjectBankAccount theo Combo AccountingObject
        /// </summary>
        /// <typeparam name="TK">Đối tượng Form</typeparam>
        /// <param name="this">Form</param>
        /// <param name="combo">Combo AccountingObject</param>
        /// <param name="comboAccBank">combo AccountingObjectBankAccount</param>
        private static void ConfigCombo<TK>(this Form @this, UltraCombo combo, UltraCombo comboAccBank)
        {
            combo.Refresh();
            var listAccountingObjectBank = new BindingList<AccountingObjectBankAccount>();
            if (combo.Value != null)
            {
                AccountingObject model = ((BindingList<AccountingObject>)combo.DataSource).FirstOrDefault(p => p.ID == (Guid)combo.Value);
                if (model != null)
                {
                    listAccountingObjectBank = new BindingList<AccountingObjectBankAccount>(IAccountingObjectBankAccountService.GetByAccountingObjectID(model.ID));
                    IAccountingObjectBankAccountService.UnbindSession(listAccountingObjectBank);
                    listAccountingObjectBank = new BindingList<AccountingObjectBankAccount>(IAccountingObjectBankAccountService.GetByAccountingObjectID(model.ID));
                }
            }
            TK dataBinding = (TK)comboAccBank.DataBindings[0].DataSource;
            string nameBindingAccountingObjectBank = comboAccBank.DataBindings[0].BindingMemberInfo.BindingMember;
            DataSourceUpdateMode dataSourceUpdateMode = comboAccBank.DataBindings[0].DataSourceUpdateMode;
            //@combo.Text = string.Empty;
            @this.ConfigCombo(listAccountingObjectBank, comboAccBank, "BankAccount", "ID",
                                                dataBinding, nameBindingAccountingObjectBank, dataSourceUpdateMode, haveEditorButton: false);
            var @base = @this as DetailBase<TK>;
            if (@base != null && !@base._statusForm.Equals(ConstFrm.optStatusForm.View))
            {
                PropertyInfo propId = dataBinding.GetType().GetProperty(nameBindingAccountingObjectBank);
                if (propId != null && propId.CanWrite)
                    propId.SetValue(dataBinding, null, null);
                PropertyInfo propName = dataBinding.GetType().GetProperty("AccountingObjectBankName");
                if (propName != null && propName.CanWrite)
                    propName.SetValue(dataBinding, string.Empty, null);
            }
        }
        /// <summary>
        /// Hàm config riêng cho Combo CreditCard
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TK"></typeparam>
        /// <param name="this"></param>
        /// <param name="input"></param>
        /// <param name="combo"></param>
        /// <param name="displayMember"></param>
        /// <param name="valueMember"></param>
        /// <param name="DataBinding"></param>
        /// <param name="bindingObjectName"></param>
        /// <param name="Controls"></param>
        /// <param name="dataSourceUpdateMode"></param>    
        /// <param name="haveEditorButton">Có nút Add bên phải hay không</param>
        private static void ConfigCombo<T, TK>(this Form @this, IList<T> input, UltraCombo combo, string displayMember, string valueMember, TK @DataBinding, string bindingObjectName, List<Control> @Controls, DataSourceUpdateMode dataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged, bool haveEditorButton = true)
        {
            combo.DataSource = input;
            combo.DisplayMember = displayMember;
            combo.ValueMember = valueMember;
            @this.ConfigGrid<T>(combo, typeof(T).Name);
            if (combo.Tag != null)
            {
                Dictionary<string, object> tag = (Dictionary<string, object>)combo.Tag;
                KeyValuePair<string, object>? temp =
                        tag.FirstOrDefault(
                            p => p.Key.Equals("CreditCardControl"));
                if (temp != null)
                    tag.Remove("CreditCardControl");
                tag.Add("CreditCardControl", Controls);
                combo.Tag = tag;
            }
            else
            {
                combo.Tag = new Dictionary<string, object>() { { "CreditCardControl", Controls } };
            }
            combo.Enter += new EventHandler(@this.Combo_Enter<T, TK>);
            combo.KeyDown += new KeyEventHandler(@this.Combo_KeyDown<TK>);
            combo.TextChanged += new EventHandler((s, e) => @this.Combo_TextChanged<TK>(s, e, @Controls));
            combo.RowSelected +=
                new RowSelectedEventHandler((s, e) => @this.cbbAutoFilter_RowSelected(Controls, s, e));
            combo.KeyPress += new KeyPressEventHandler(Combo_KeyPress);
            combo.Validated += new EventHandler(@this.Combo_Validated<TK>);
            combo.MouseEnter += new EventHandler(Combo_MouseEnter);
            combo.Leave += new EventHandler((sender, e) => @this.Combo_Leave<T, TK>(sender, e));
            if (@DataBinding != null)
            {
                combo.DataBindings.Clear();
                combo.DataBindings.Add("Value", @DataBinding, bindingObjectName, true, dataSourceUpdateMode);
            }
            if (haveEditorButton)
            {
                EditorButton editorButton = new EditorButton();
                editorButton.Appearance.Image = Properties.Resources.ubtnAdd4;
                combo.ButtonsRight.Clear();
                combo.ButtonsRight.Add(editorButton);
                combo.EditorButtonClick += @this.Combo_EditorButtonClick<TK>;
            }
            combo.ClearAutoComplete();
        }
        #endregion

        #region Event Combo
        static void Combo_MouseEnter(object sender, EventArgs e)
        {
            if (StopWatch.IsRunning)
            {
                StopWatch.Stop();
                StopWatch.Reset();
            }
        }

        static void Combo_EditorButtonClick<TK>(this Form @this, object sender, EditorButtonEventArgs e)
        {
            UltraCombo ultraCombo = (UltraCombo)sender;
            if (StopWatch.IsRunning)// && _stopWatch.Elapsed.Seconds < 3
            {
                return;
            }
            var bindingMember = ultraCombo.DataBindings[0].DataSource;
            PropertyInfo propInfo = bindingMember.GetType().GetProperty("AccountingObjectType");
            var fullName = ultraCombo.DataSource.GetType().FullName;
            if (fullName != null && (fullName.Contains(typeof(BindingList<AccountingObject>).FullName) && propInfo != null && propInfo.CanRead))
            {
                int? accType = (int?)propInfo.GetValue(bindingMember, null);
                if (accType != null)
                {
                    switch (accType)
                    {
                        case 0:
                            new FAccountingObjectCustomersDetail().ShowFormCreate<AccountingObject, TK>(@this);
                            break;
                        case 1:
                            new FAccountingObjectCustomersDetail().ShowFormCreate<AccountingObject, TK>(@this);
                            break;
                        case 2:
                            new FAccountingObjectEmployeeDetail().ShowFormCreate<AccountingObject, TK>(@this);
                            break;
                    }
                }
                else
                    new FAccountingObjectCustomersDetail().ShowFormCreate<AccountingObject, TK>(@this);
            }
            else
            {
                var name = ultraCombo.DataSource.GetType().FullName;
                if (name != null && (propInfo != null && propInfo.CanRead && name.Contains(typeof(BindingList<AccountingObjectBankAccount>).FullName)))
                {
                    int? accType = (int?)propInfo.GetValue(bindingMember, null);
                    if (accType != null)
                    {
                        switch (accType)
                        {
                            case 0:
                                new FAccountingObjectCustomers().ShowFormCreate<AccountingObject, TK>(@this);
                                break;
                            case 1:
                                new FAccountingObjectCustomers().ShowFormCreate<AccountingObject, TK>(@this);
                                break;
                            case 2:
                                new FAccountingObjectEmployee().ShowFormCreate<AccountingObject, TK>(@this);
                                break;
                        }
                    }
                    else
                        new FAccountingObjectCustomers().ShowFormCreate<AccountingObject, TK>(@this);
                }
                else
                {
                    string nameCol = ultraCombo.Name;
                    if (ultraCombo.Tag != null && ultraCombo.DataSource.GetType().FullName.GetObjectNameFromBindingName().Equals(typeof(AccountingObject).Name))
                    {
                        KeyValuePair<string, object>? temp =
                            ((Dictionary<string, object>)ultraCombo.Tag).FirstOrDefault(
                                p => p.Key.Equals("AccountingObjectType"));
                        if (temp != null && temp.Value.Value != null)
                        {
                            nameCol = temp.Value.Value.ToString();
                        }
                    }
                    else
                    {
                        string nameSource = ultraCombo.DataSource.GetType().FullName.GetObjectNameFromBindingName();
                        if (nameSource.Equals(typeof(CreditCard).Name) || nameSource.Equals(typeof(BankAccountDetail).Name) || nameSource.Equals(typeof(AccountingObjectBankAccount).Name))
                            nameCol = ultraCombo.DataSource.GetType().FullName.GetObjectNameFromBindingName();
                    }
                    @this.ShowQuickUtils_ByKeys<TK>(Keys.F2, nameCol, ultraCombo);
                }
            }
        }

        static void Combo_Leave(object sender, EventArgs e)
        {
            UltraCombo combo = (UltraCombo)sender;
            bool? isKeypress = false;
            //combo.Update();
            if (combo.Value == null) return;
            bool haveItem = false;
            if (!combo.Value.GetType().Name.Contains(typeof(Guid).Name))
            {
                haveItem = false;
                goto End;
            }
            foreach (var row in combo.Rows)
            {
                var id = (Guid)row.Cells["ID"].Value;
                if (id.Equals((Guid)combo.Value) && !(row.Tag != null && (bool)row.Tag))
                {
                    haveItem = true;
                    break;
                }
            }
        End:
            if (!haveItem)
            {
                if (combo.Tag != null)
                {
                    isKeypress =
                        (bool?)((Dictionary<string, object>)combo.Tag).FirstOrDefault(p => p.Key.Equals("KeyPress")).Value ??
                        false;
                }
                if (!(bool)isKeypress) return;
                string fullName = combo.DataSource.GetType().FullName;
                var value = typeof(BindingList<AccountingObjectBankAccount>).FullName;
                var s = typeof(BindingList<CreditCard>).FullName;
                if (s != null && (value != null && (fullName != null && (fullName.Contains(value) || fullName.Contains(s)))))
                    goto Then;
                else
                {
                    var name = typeof(BindingList<AccountingObject>).FullName;
                    if (name != null && (fullName != null && fullName.Contains(name)))
                    {
                        MSG.Warning(resSystem.MSG_System_41);
                    }
                    else
                    {
                        MSG.Warning(resSystem.MSG_System_42);
                    }
                }
                combo.Focus();
            }
        Then:
            combo.SetValueForTag("KeyPress", false);
            combo.ClearAutoComplete();
        }
        static void Combo_Leave<T, TK>(this Form @this, object sender, EventArgs e, UltraCombo comboAccBank = null)
        {
            var @base = @this as DetailBase<TK>;
            if (@base != null && @base._statusForm.Equals(ConstFrm.optStatusForm.View)) return;
            UltraCombo combo = (UltraCombo)sender;
            bool? isKeypress = false;
            //combo.Update();
            if (combo.Value == null) return;
            bool haveItem = false;
            if (!combo.Value.GetType().Name.Contains(typeof(Guid).Name))
            {
                haveItem = false;
                goto End;
            }
            foreach (var row in combo.Rows)
            {
                var id = (Guid)row.Cells["ID"].Value;
                if (id.Equals((Guid)combo.Value) && !(row.Tag != null && (bool)row.Tag))
                {
                    haveItem = true;
                    break;
                }
            }
        End:
            if (!haveItem)
            {
                if (combo.Tag != null)
                {
                    isKeypress =
                        (bool?)((Dictionary<string, object>)combo.Tag).FirstOrDefault(p => p.Key.Equals("KeyPress")).Value ??
                        false;
                }
                if (!(bool)isKeypress) return;
                string fullName = combo.DataSource.GetType().FullName;
                var value = typeof(BindingList<AccountingObjectBankAccount>).FullName;
                var s = typeof(BindingList<CreditCard>).FullName;
                if (s != null && (value != null && (fullName != null && (fullName.Contains(value) || fullName.Contains(s)))))
                    goto Then;
                else
                {
                    var name = typeof(BindingList<AccountingObject>).FullName;
                    if (name != null && (fullName != null && fullName.Contains(name)))
                    {
                        MSG.Warning(resSystem.MSG_System_41);
                    }
                    else
                    {
                        MSG.Warning(resSystem.MSG_System_42);
                    }
                }
                combo.Focus();
            }
            else if (comboAccBank != null && typeof(T).Name.Equals(typeof(AccountingObject).Name))
            {
                @this.ConfigCombo<TK>(combo, comboAccBank);
            }
        Then:
            combo.SetValueForTag("KeyPress", false);
            combo.ClearAutoComplete();
            //if (combo.Tag != null)
            //{
            //    if (((Dictionary<string, object>)combo.Tag).Any(p => p.Key.Equals("KeyPress")))
            //        ((Dictionary<string, object>)combo.Tag).Remove("KeyPress");
            //    ((Dictionary<string, object>)combo.Tag).Add("KeyPress", false);
            //}
            //else
            //{
            //    combo.Tag = new Dictionary<string, object>() { { "KeyPress", false } };
            //}
        }

        static void Combo_Validated<TK>(this Form @this, object sender, EventArgs e)
        {
            //_isKeyPress = false;
            //(sender as UltraCombo).SetValueForTag("KeyPress", false);
            var @base = @this as DetailBase<TK>;
            if (@base != null) @base.SetTextStatusBar("");
        }

        static void Combo_KeyPress(object sender, KeyPressEventArgs e)
        {
            //_isKeyPress = true;
            //(sender as UltraCombo).SetValueForTag("KeyPress", true);
            var ultraCombo = (UltraCombo)sender;
            AutoComplateForUltraCombo(ultraCombo, ultraCombo.Text);
        }

        static void Combo_TextChanged(object sender, EventArgs e)
        {
            var ultraCombo = (UltraCombo)sender;
            //if (ultraCombo.Tag == null) return;
            //var isKeyPress =
            //    (bool?)
            //    ((Dictionary<string, object>)ultraCombo.Tag).FirstOrDefault(p => p.Key.Equals("KeyPress")).Value;
            //if (isKeyPress != null && (bool)isKeyPress)
            //AutoComplateForUltraCombo(ultraCombo, ultraCombo.Text);
        }

        private static void Combo_TextChanged<TK>(this Form @this, object sender, EventArgs e, List<Control> controls)
        {
            var ultraCombo = (UltraCombo)sender;
            AutoComplateForUltraCombo(ultraCombo, ultraCombo.Text);
            if (ultraCombo.DataSource.GetType().Name.Contains(typeof(BindingList<CreditCard>).Name))
                if (string.IsNullOrEmpty(ultraCombo.Text))
                {
                    if (controls[0].GetType().Name.Contains(typeof(UltraPictureBox).Name))
                    {
                        ((UltraPictureBox)controls[0]).Image = null;
                        controls[0].Visible = false;
                    }
                    controls[1].Text = string.Empty;
                    controls[2].Text = string.Empty;
                }
        }

        static void Combo_KeyDown<TK>(this Form @this, object sender, KeyEventArgs e)
        {
            var ultraCombo = (UltraCombo)sender;

            if (StopWatch.IsRunning)// && _stopWatch.Elapsed.Seconds < 3
            {
                return;
            }
            if (ultraCombo.Tag != null)
            {
                if (((Dictionary<string, object>)ultraCombo.Tag).Any(p => p.Key.Equals("KeyPress")))
                    ((Dictionary<string, object>)ultraCombo.Tag).Remove("KeyPress");
                ((Dictionary<string, object>)ultraCombo.Tag).Add("KeyPress", true);
            }
            else
            {
                ultraCombo.Tag = new Dictionary<string, object>() { { "KeyPress", true } };
            }

            var bindingMember = ultraCombo.DataBindings[0].DataSource;
            PropertyInfo propInfo = bindingMember.GetType().GetProperty("AccountingObjectType");
            var fullName = ultraCombo.DataSource.GetType().FullName;
            var s = typeof(BindingList<AccountingObject>).FullName;
            if (s != null && (fullName != null && (e.KeyCode.Equals(Keys.F2) && propInfo != null && propInfo.CanRead && fullName.Contains(s))))
            {
                var accType = (int?)propInfo.GetValue(bindingMember, null);
                if (accType != null)
                {
                    switch (accType)
                    {
                        case 0:
                            new FAccountingObjectCustomersDetail().ShowFormCreate<AccountingObject, TK>(@this);
                            break;
                        case 1:
                            new FAccountingObjectCustomersDetail().ShowFormCreate<AccountingObject, TK>(@this);
                            break;
                        case 2:
                            new FAccountingObjectEmployeeDetail().ShowFormCreate<AccountingObject, TK>(@this);
                            break;
                    }
                }
                else
                    new FAccountingObjectCustomersDetail().ShowFormCreate<AccountingObject, TK>(@this);
            }
            else
            {
                var name = ultraCombo.DataSource.GetType().FullName;
                var st = typeof(BindingList<AccountingObjectBankAccount>).FullName;
                if (st != null && (name != null && (e.KeyCode.Equals(Keys.F2) && propInfo != null && propInfo.CanRead && name.Contains(st))))
                {
                    var accType = (int?)propInfo.GetValue(bindingMember, null);
                    if (accType != null)
                    {
                        switch (accType)
                        {
                            case 0:
                                new FAccountingObjectCustomers().ShowFormCreate<AccountingObject, TK>(@this);
                                break;
                            case 1:
                                new FAccountingObjectCustomers().ShowFormCreate<AccountingObject, TK>(@this);
                                break;
                            case 2:
                                new FAccountingObjectEmployee().ShowFormCreate<AccountingObject, TK>(@this);
                                break;
                        }
                    }
                    else
                        new FAccountingObjectCustomers().ShowFormCreate<AccountingObject, TK>(@this);
                }
                else
                {
                    string nameCol = ultraCombo.Name;
                    if (ultraCombo.Tag != null)
                    {
                        var accType =
                            (int?)
                            ((Dictionary<string, object>)ultraCombo.Tag).FirstOrDefault(
                                p => p.Key.Equals("AccountingObjectType")).Value;
                        if (accType != null)
                        {
                            nameCol = accType.ToString();
                        }
                    }
                    else
                    {
                        string nameSource = ultraCombo.DataSource.GetType().FullName.GetObjectNameFromBindingName();
                        if (nameSource.Equals(typeof(CreditCard).Name) || nameSource.Equals(typeof(BankAccountDetail).Name) || nameSource.Equals(typeof(AccountingObjectBankAccount).Name))
                            nameCol = ultraCombo.DataSource.GetType().FullName.GetObjectNameFromBindingName();
                    }
                    @this.ShowQuickUtils_ByKeys<TK>(e.KeyCode, nameCol, ultraCombo);
                }
            }
        }
        static void Combo_Enter<T, TK>(this Form @this, object sender, EventArgs e)
        {
            string typeName = typeof(T).Name;
            var @base = @this as DetailBase<TK>;
            if (@base == null) return;
            if (!(typeName.Contains("Account") && !typeName.Contains("AccountingObject") && !typeName.Contains("BankAccount") || typeName.Contains("Currency")) && !typeName.Contains("AccountingObjectBankAccount"))
                @base.SetTextStatusBar(resSystem.MSG_System_08);
            else
                @base.SetTextStatusBar(resSystem.MSG_System_09);
        }

        #region Hàm sự kiện mặc định khi chạy Combo dạng cây cha con
        private static void Combo_MouseEnterElement<T>(object sender, UIElementEventArgs e)
        {
            if (e.Element is RowUIElement)
            {
                UltraCombo cbb = (UltraCombo)sender;
                CustomCreationFilter<T> filter = (CustomCreationFilter<T>)cbb.CreationFilter;
                filter.HotTrackedRow = ((RowUIElement)e.Element).Row;
            }
        }

        private static void Combo_MouseLeaveElement<T>(object sender, UIElementEventArgs e)
        {
            if (e.Element is RowUIElement)
            {
                UltraCombo cbb = (UltraCombo)sender;
                CustomCreationFilter<T> filter = (CustomCreationFilter<T>)cbb.CreationFilter;
                filter.HotTrackedRow = null;
            }
        }
        #endregion
        #endregion
        #endregion

        /// <summary>
        /// lấy UIElement từ UltraGridBase obj 
        /// (tạm thời chưa dùng đến)
        /// </summary>
        /// <param name="sender">đối tượng </param>
        /// <param name="point"></param>
        /// <returns></returns>
        public static UIElement GetUiElement(object sender, System.Drawing.Point point)
        {
            return ((UltraGridBase)sender).DisplayLayout.UIElement.ElementFromPoint(point);
        }

        /// <summary>
        /// Show menu item khi click chuột phải vào ultraGrid, ultraTree
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <param name="cms4Grid"></param>
        public static void uGrid_MouseDown(object sender, MouseEventArgs e, ContextMenuStrip cms4Grid)
        {
            uGrid_MouseDown(sender, e, null, cms4Grid);
        }
        public static void uGrid_MouseDown(object sender, MouseEventArgs e, UltraGrid uGridInput, ContextMenuStrip cms4Grid)
        {
            //nếu không phải là chuột phải thì bỏ qua
            if (e.Button != MouseButtons.Right) return;
            //lấy vị trí
            Point point = new Point(e.X, e.Y);
            //lấy kiểu của đối tượng sender (đối tượng gây ra sự kiện)
            System.Type type = sender.GetType();

            //kiểm tra đối tượng là gì và đưa ra cách xử lý tương ứng
            if (type == typeof(UltraTree))
            {//=> đối tượng UltraTree
                UltraTree uTree = (UltraTree)sender;
                //lấy UltraTreeNode từ vị trí 
                UltraTreeNode node = uTree.GetNodeFromPoint(point);
                //nếu đang bấm chuột phải và đối tượng đang chọn khác null thì thao tác
                if (e.Button == MouseButtons.Right && node != null)
                {
                    uTree.ActiveNode = node;
                    uTree.ActiveNode.Selected = true;
                    cms4Grid.Show(uTree, point);
                }
            }
            else if (type == typeof(UltraGrid))
            {//=> đối tượng UltraGrid
                UltraGrid uGrid = (UltraGrid)sender;
                //lấy UltraGridCell từ vị trí
                UltraGridCell cell = (UltraGridCell)((UltraGridBase)sender).DisplayLayout.UIElement.ElementFromPoint(point).GetContext(typeof(UltraGridCell));
                //nếu đang bấm chuột phải và đối tượng đang chọn khác null thì thao tác
                if (e.Button == MouseButtons.Right && cell != null && cell.Row.Index >= 0)
                {
                    uGrid.Rows[cell.Row.Index].Selected = true;
                    uGrid.ActiveRow = uGrid.Rows[cell.Row.Index];
                    cms4Grid.Show(uGrid, point);
                }
            }
        }

        /// <summary>
        /// Kiểm tra lỗi Cell
        /// [Huy anh]
        /// </summary>
        /// <param name="ultraGridCell"></param>
        /// <param name="funCheck"></param>
        /// <param name="msg"></param>
        /// <param name="isShowMsgBox"> </param>
        public static void CheckErrorCell(UltraGridCell ultraGridCell, bool funCheck, string @msg = "", bool isShowMsgBox = false)
        {
            @msg = string.IsNullOrEmpty(@msg) ? resSystem.MSG_System_12 : @msg;
            ultraGridCell.Appearance.ForeColor = funCheck ? Color.Red : Color.Black;
            ultraGridCell.ToolTipText = funCheck ? @msg : string.Empty;
            if (funCheck && isShowMsgBox) MSG.Error(@msg);
        }

        public static EmbeddableEditorBase GetEmbeddableEditorBase<T>(this Form parent, List<T> inputItem, string valueMember, string displayMember, string nameTable)
        {
            UltraDropDown dropDown = new UltraDropDown
            {
                Visible = false,
                DataSource = inputItem,
                ValueMember = valueMember,
                DisplayMember = displayMember
            };
            parent.Controls.Add(dropDown);
            ConfigGrid(dropDown, nameTable);
            EmbeddableEditorBase editor = new EditorWithCombo(new DefaultEditorOwner(new DefaultEditorOwnerSettings { ValueList = dropDown, DataType = typeof(int) }));
            return editor;
        }
        //
        public static EmbeddableEditorBase GetEmbeddableEditorBase<T>(Control.ControlCollection _this, List<T> inputItem, string valueMember, string displayMember, string nameTable)
        {
            UltraCombo cbb = new UltraCombo
                                         {
                                             Visible = false,
                                             DataSource = inputItem,
                                             ValueMember = valueMember,
                                             DisplayMember = displayMember,
                                         };
            _this.Add(cbb);
            ConfigGrid(cbb, nameTable);
            EmbeddableEditorBase editor = new EditorWithCombo(new DefaultEditorOwner(new DefaultEditorOwnerSettings { ValueList = cbb, DataType = typeof(int) }));
            return editor;
        }

        /// <summary>
        /// Gắn Dropdown vào Grid qua ValueList (Có hiện dạng Cây cha con)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="this"> </param>
        /// <param name="uGridInput"> </param>
        /// <param name="inputItem"></param>
        /// <param name="valueMember"></param>
        /// <param name="displayMember"></param>
        /// <param name="nameTable"></param>
        /// <param name="nameParentId">Tên cột chứa parentID</param>
        /// <param name="nameParentNode">Tên cột xác định có phải là cha hay ko</param>
        /// <param name="accountingObjectType">Loại đối tượng AccountingObject: 0-Khách hàng. 1-Nhà cung cấp. 2-Nhân viên. 3-KH và NCC. [Mặc định là lấy đầy đủ ds]</param>
        /// <returns></returns>
        public static UltraCombo GetValueList<T>(this Form @this, UltraGrid @uGridInput, IList<T> @inputItem, string @valueMember, string @displayMember, string @nameTable, string nameParentId = null, string @nameParentNode = null, int? accountingObjectType = null)
        {
            var @combo = new UltraCombo
            {
                Visible = false,
                DataSource = @inputItem,
                ValueMember = @valueMember,
                DisplayMember = @displayMember,
                AutoCompleteMode = Infragistics.Win.AutoCompleteMode.Suggest,
                AutoSuggestFilterMode = AutoSuggestFilterMode.Contains
            };
            @combo.RowSelected += new RowSelectedEventHandler(@this.cbbAutoFilter_RowSelected);
            @combo.CreationFilter = new CustomCreationFilter<T>(@this) { UltraGrid = @uGridInput };
            if (typeof(T) == typeof(Currency))
                @combo.RowSelected +=
                    new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(@this.cbbCurrency_RowSelected);
            else if (typeof(T) == typeof(MaterialGoodsCustom))
            {
                @combo.RowSelected += new RowSelectedEventHandler(cbbMaterialGoods_RowSelected);
                @combo.CreationFilter = new CustomCreationFilter<MaterialGoodsCustom>(@this) { Key = "MaterialGoodsCode", HaveAddButton = true, UltraGrid = @uGridInput, Image = Properties.Resources.ubtnAdd4, EventButtonAdd = editorButton_ElementClick };
            }
            @this.Controls.Add(@combo);
            @this.ConfigGrid<T>(@combo, @nameTable, nameParentId, @nameParentNode);

            if (typeof(T) == typeof(MaterialGoodsCustom) && @uGridInput != null && @uGridInput.Tag is System.Type && (System.Type)@uGridInput.Tag == typeof(PPServiceDetail))
            {
                @combo.ConfigComboServices();
            }
            else
            {
                if (accountingObjectType != null)
                {
                    @combo.ConfigComboAccouting(accountingObjectType);
                }
                else
                {
                    @combo.ClearAutoComplete();
                }
            }
            return @combo;
        }
        /// <summary>
        /// Gắn Dropdown vào Grid qua ValueList (Có hiện dạng Cây cha con)
        /// </summary>       
        /// <typeparam name="TK">Đối tượng của Form</typeparam>
        /// <typeparam name="T">Đối tượng của combo</typeparam>
        /// <param name="this">Form chính</param>
        /// <param name="valueMember"></param>
        /// <param name="inputItem"></param>
        /// <param name="uGridInput"></param>
        /// <param name="displayMember"></param>
        /// <param name="nameTable"></param>
        /// <param name="nameParentId">Tên cột chứa parentID</param>
        /// <param name="nameParentNode">Tên cột xác định có phải là cha hay ko</param>
        /// <param name="accountingObjectType">Loại đối tượng AccountingObject: 0-Khách hàng. 1-Nhà cung cấp. 2-Nhân viên. 3-KH và NCC. [Mặc định là lấy đầy đủ ds]</param>
        /// <returns></returns>
        public static UltraCombo GetValueList<TK, T>(this Form @this, UltraGrid @uGridInput, IList<T> @inputItem, string @valueMember, string @displayMember, string @nameTable, string @nameParentId = null, string @nameParentNode = null, int? accountingObjectType = null)
        {
            var @combo = new UltraCombo
            {
                Visible = false,
                DataSource = @inputItem,
                ValueMember = @valueMember,
                DisplayMember = @displayMember
            };
            @combo.RowSelected += new RowSelectedEventHandler(@this.cbbAutoFilter_RowSelected);
            if (@uGridInput != null)//Sửa theo y/c của Duy
            {
                @combo.CreationFilter = new CustomCreationFilter<T>(@this) { UltraGrid = @uGridInput };
                if (typeof(T) == typeof(Currency))
                    @combo.RowSelected +=
                        new RowSelectedEventHandler(@this.cbbCurrency_RowSelected);
                else if (typeof(T) == typeof(MaterialGoodsCustom))
                {
                    @combo.RowSelected += new RowSelectedEventHandler(@this.cbbMaterialGoods_RowSelected<TK>);
                    @combo.ValueChanged += new EventHandler(@this.cbbMaterialGoods_ValueChanged<TK>);
                    @combo.CreationFilter = new CustomCreationFilter<MaterialGoodsCustom>(@this) { Key = "MaterialGoodsCode", HaveAddButton = true, UltraGrid = @uGridInput, Image = Properties.Resources.ubtnAdd4, EventButtonAdd = editorButton_ElementClick };
                }
                else if (typeof(T) == typeof(SAQuote))
                {
                    @combo.RowSelected += new RowSelectedEventHandler(@this.cbbSAQuote_RowSelected<TK>);
                }
            }
            @this.Controls.Add(@combo);
            @this.ConfigGrid<T>(@combo, @nameTable, @nameParentId, @nameParentNode);
            if (typeof(T) == typeof(MaterialGoodsCustom) && @uGridInput != null && @uGridInput.Tag is System.Type && (System.Type)@uGridInput.Tag == typeof(PPServiceDetail))
            {
                @combo.ConfigComboServices();
            }
            else
            {
                if (accountingObjectType != null)
                {
                    @combo.ConfigComboAccouting(accountingObjectType);
                }
                else
                {
                    @combo.ClearAutoComplete();
                }
            }
            return @combo;
        }

        /// <summary>
        /// Gắn Dropdown Mã TS vào Grid qua ValueList
        /// </summary>       
        /// <typeparam name="T">Đối tượng của combo</typeparam>
        /// <param name="this">Form chính</param>
        /// <param name="valueMember"></param>
        /// <param name="inputItem"></param>
        /// <param name="uGridInput"></param>
        /// <param name="displayMember"></param>
        /// <param name="nameTable"></param>
        /// <param name="fixedAssetType">Loại đối tượng AccountingObject: 0-Khách hàng. 1-Nhà cung cấp. 2-Nhân viên. 3-KH và NCC. [Mặc định là lấy đầy đủ ds]</param>
        /// <returns></returns>
        public static UltraCombo GetValueList<T>(this Form @this, UltraGrid @uGridInput, IList<T> @inputItem, string @valueMember, string @displayMember, string @nameTable, int? fixedAssetType = null)
        {
            var @combo = new UltraCombo
            {
                Visible = false,
                DataSource = @inputItem,
                ValueMember = @valueMember,
                DisplayMember = @displayMember
            };
            @combo.RowSelected += new RowSelectedEventHandler(@this.cbbAutoFilter_RowSelected);
            if (@uGridInput != null)//Sửa theo y/c của Duy
            {
                @combo.CreationFilter = new CustomCreationFilter<T>(@this) { UltraGrid = @uGridInput };
            }
            @this.Controls.Add(@combo);
            @this.ConfigGrid<T>(@combo, @nameTable);
            if (fixedAssetType != null)
            {
                @combo.ConfigComboFixedAsset(fixedAssetType);
            }
            else
            {
                @combo.ClearAutoComplete();
            }
            return @combo;
        }

        static void cbbMaterialGoods_ValueChanged<TK>(this Form @this, object sender, EventArgs e)
        {
            //if (e.Row == null) return;
            //MaterialGoodsCustom model = (MaterialGoodsCustom)e.Row.ListObject;
            //UltraGrid uGrid = @this.FindGridIsActived();
            //if (uGrid != null)
            //{
            //    if (uGrid.ActiveCell != null && uGrid.DisplayLayout.Bands[0].Columns.Exists("Description") && uGrid.DisplayLayout.Bands[0].Columns.Exists("UnitPrice"))
            //    {
            //        UltraGridCell cell = uGrid.ActiveCell;
            //        cell.Row.Cells["Description"].Value = model.MaterialGoodsName;
            //        if (@this.GetType().BaseType != null &&
            //            @this.GetType().BaseType.BaseType.Name.Contains(typeof(DetailBase<TK>).Name))
            //        {
            //            int? typeGroup = ((DetailBase<TK>)@this).TypeGroup;
            //            if (typeGroup != null)
            //            {
            //                if (PurchaseTypeGroup.Any(p => p.Equals(typeGroup)))
            //                {
            //                    cell.Row.Cells["UnitPriceOriginal"].Value = model.PurchasePrice;
            //                }
            //                else if (SaleTypeGroup.Any(p => p.Equals(typeGroup)))
            //                {
            //                    cell.Row.Cells["UnitPriceOriginal"].Value = model.SalePrice;
            //                    sourceUnitPrice[0] = sourceUnitPrice[0] ?? model.SalePrice;
            //                    sourceUnitPrice[1] = sourceUnitPrice[1] ?? model.SalePrice2;
            //                    sourceUnitPrice[2] = sourceUnitPrice[2] ?? model.SalePrice3;
            //                }
            //                cell.Row.UpdateData();
            //                uGrid.PerformAction(UltraGridAction.ExitEditMode, false, false);
            //                @this.CalculateAmount<TK>(uGrid, cell.Row.Cells["UnitPriceOriginal"]);
            //            }
            //            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("RepositoryID"))
            //                cell.Row.Cells["RepositoryID"].Value = model.RepositoryID;
            //            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("Unit"))
            //                cell.Row.Cells["Unit"].Value = model.Unit;
            //        }
            //    }
            //}
        }
        public static void GetValueList<T>(this Form @this, UltraCombo ultraCombo, IList<T> @inputItem, string @valueMember, string @displayMember, string @nameTable, string @nameParentId = null, string @nameParentNode = null, int? accountingObjectType = null)
        {
            ultraCombo.DataSource = @inputItem;
            ultraCombo.ValueMember = @valueMember;
            ultraCombo.DisplayMember = @displayMember;
            ultraCombo.AutoCompleteMode = AutoCompleteMode.Suggest;
            ultraCombo.AutoSuggestFilterMode = AutoSuggestFilterMode.Contains;
            @this.ConfigGrid<T>(ultraCombo, @nameTable, @nameParentId, @nameParentNode);
            if (accountingObjectType != null)
            {
                ultraCombo.ConfigComboAccouting(accountingObjectType);
            }
            else
            {
                ultraCombo.ClearAutoComplete();
            }
        }
        #endregion

        /// <summary>
        /// Config một nút bên top menu phía trên.
        /// 0"mnbtnBack", 1"mnbtnForward", 2"mnbtnAdd4", 3"mnbtnEdit1", 4"mnbtnSave", 5"mnbtnDelete", 6"mnbtnComeBack", 7"mnbtnPost", 18"mnbtnUnPost", 8"mnbtnReset", 9"mnbtnViewList", 10"mnbtnUtilities", 11"mnbtnTemplate", 12"mnbtnPrint", 13"mnbtnHelp", 14"mnbtnClose"
        /// </summary>
        /// <param name="ultraToolbarsManager">Đối tượng thao tác</param>
        /// <param name="nameActive">vị trí tác động</param>
        /// <param name="status">0: ẩn, 1: Hiện không thao tác, 2: Hiện thao tác, Default: Mặc định ẩn</param>
        public static void ConfigTopMenuDetails(UltraToolbarsManager ultraToolbarsManager, string nameActive, int status)
        {
            switch (status)
            {
                case 0:
                    ultraToolbarsManager.Tools[nameActive].SharedProps.Visible = false;
                    break;
                case 1:
                    ultraToolbarsManager.Tools[nameActive].SharedProps.Enabled = false;
                    ultraToolbarsManager.Tools[nameActive].SharedProps.Visible = true;
                    break;
                case 2:
                    ultraToolbarsManager.Tools[nameActive].SharedProps.Enabled = true;
                    ultraToolbarsManager.Tools[nameActive].SharedProps.Visible = true;
                    break;
                default:
                    ultraToolbarsManager.Tools[nameActive].SharedProps.Visible = false;
                    break;
            }
        }

        /// <summary>
        /// Config một nút bên top menu phía trên.
        /// 0"mnbtnBack", 1"mnbtnForward", 2"mnbtnAdd4", 3"mnbtnEdit1", 4"mnbtnSave", 5"mnbtnDelete", 6"mnbtnComeBack", 7"mnbtnPost", 18"mnbtnUnPost", 8"mnbtnReset", 9"mnbtnViewList", 10"mnbtnUtilities", 11"mnbtnTemplate", 12"mnbtnPrint", 13"mnbtnHelp", 14"mnbtnClose"
        /// </summary>
        /// <param name="ultraToolbarsManager">Đối tượng thao tác</param>
        /// <param name="indexActive">vị trí tác động</param>
        /// <param name="strEnabled">Cho phép hoạt động hay không?</param>
        /// <param name="strVisible">Cho phép hiển thị hay không?</param>
        public static void ConfigTopMenuDetails(UltraToolbarsManager ultraToolbarsManager, int indexActive, bool strEnabled, bool strVisible)
        {
            ultraToolbarsManager.Tools[indexActive].SharedProps.Enabled = strEnabled;
            ultraToolbarsManager.Tools[indexActive].SharedProps.Visible = strVisible;
        }

        public static void ScrollRowToMiddle(this UltraGridRow row)
        {
            UltraGrid grid = (UltraGrid)row.Band.Layout.Grid;
            UIElement gridElement = grid.DisplayLayout.UIElement;
            if (gridElement == null)
                return;
            UIElement rowColRegionIntersectionUIElement = gridElement.GetDescendant(typeof(RowColRegionIntersectionUIElement));
            if (rowColRegionIntersectionUIElement == null)
                return;
            // Determine the total number of rows that can fit in the visible
            // area of the grid. We are assuming that the Rows are all the same height.
            int visibleRows = rowColRegionIntersectionUIElement.Rect.Height / row.Height;
            // Find the row that is half the visibleRows prior to the row we want.             
            //
            UltraGridRow newFirstRow = row;
            int rowOffset = visibleRows / 2;
            for (int i = 0; i < rowOffset; i++)
            {
                if (newFirstRow.HasPrevSibling(false, true))
                    newFirstRow = newFirstRow.GetSibling(SiblingRow.Previous, false, true);
            }
            // Set the new FirstRow. 
            grid.ActiveRowScrollRegion.FirstRow = newFirstRow;

        }

        #region Template
        /// <summary>
        /// Lấy giao diện mẫu trong cơ sở dữ liệu của một form cụ thể (sử dụng bộ nhớ cache)
        /// [kiendd]
        /// </summary>
        /// <param name="typeId"></param>
        /// <param name="templateId"></param>
        /// <param name="dsTemplate"></param>
        /// <returns></returns>
        public static Template GetMauGiaoDien(int typeId, Guid? templateId, Dictionary<string, Template> dsTemplate = null)    //out string keyofdsTemplate
        {
            if (dsTemplate == null) dsTemplate = ListTemplate;
            string keyofdsTemplate = string.Format("{0}@{1}", typeId, templateId);
            if (!dsTemplate.Keys.Contains(keyofdsTemplate))
            {
                //Note: các chứng từ được sinh tự động từ nghiệp vụ khác thì phải truyền TypeID của nghiệp vụ khác đó
                int typeIdTemp = GetTypeIdRef(typeId);
                Template template = GetTemplateInDatabase(templateId, typeIdTemp);
                dsTemplate.Add(keyofdsTemplate, template);
                return template;
            }
            return dsTemplate[keyofdsTemplate];
        }

        static int GetTypeIdRef(int typeId)
        {//phiếu chi
            //if (typeID == 111) return 111568658;  //phiếu chi trả lương ([Tiền lương] -> [Trả lương])
            //else if (typeID == 112) return 111568658;  //phiếu chi trả lương ([Tiền lương] -> [Trả lương])....
            return typeId;
        }

        /// <summary>
        /// [Huy Anh] Lấy TypeGroup từ TypeID
        /// </summary>
        /// <param name="typeId"></param>
        /// <returns></returns>
        public static int? GetTypeGroupFromTypeId(this int typeId)
        {
            int? typeGroup = null;
            Accounting.Core.Domain.Type type = ListType.FirstOrDefault(p => p.ID.Equals(typeId));
            if (type != null)
            {
                typeGroup = type.TypeGroupID;
            }
            return typeGroup;
        }

        /// <summary>
        /// Lấy giao diện mẫu trong cơ sở dữ liệu của một form cụ thể
        /// [kiendd]
        /// </summary>
        /// <param name="inputTemplateId">TemplateID (kiểu GUID) của mẫu (trong trạng thái sửa thì mới cần)</param>
        /// <param name="typeId">TypeID của form cần lấy mẫu giao diện</param>
        /// <returns></returns>
        public static Template GetTemplateInDatabase(Guid? inputTemplateId, int? typeId)
        {
            //nếu inputTemplateID và typeID đều null thì trả về null
            if (inputTemplateId == null && typeId == null) return null;
            //nếu [đang thêm] hoặc [đang sửa nhưng mẫu giao diện bị mất] thì load mẫu chuẩn (k.IsDefault == false && k.IsSecurity, NGƯỢC LẠI nếu đang sửa thì chỉ cần load mẫu giao diện tương ứng với inputTemplateID
            Template mauGiaoDien = inputTemplateId == null
                       ? ITemplateService.Query.FirstOrDefault(k => k.TypeID == typeId && k.IsDefault == false && k.IsSecurity)
                       : ITemplateService.Query.FirstOrDefault(k => k.ID == inputTemplateId);
            ITemplateService.UnbindSession(mauGiaoDien);
            mauGiaoDien = (inputTemplateId == null
                               ? ITemplateService.Query.FirstOrDefault(k => k.TypeID == typeId && k.IsDefault == false && k.IsSecurity)
                               : ITemplateService.Query.FirstOrDefault(k => k.ID == inputTemplateId)) ??
                          ITemplateService.Query.FirstOrDefault(k => k.TypeID == typeId && k.IsDefault == false && k.IsSecurity);
            return mauGiaoDien;
        }

        /// <summary>
        /// Lấy giao diện mẫu trong cơ sở dữ liệu của một form cụ thể
        /// ----TẠM THỜI KHÔNG DÙNG
        /// </summary>
        /// <param name="form">TypeID của form cần lấy mẫu giao diện</param>
        /// <param name="isAdd">TRUE: trạng thái thêm, FALSE: trạng thái sửa</param>
        /// <param name="inputTemplateId">TemplateID (kiểu GUID) của mẫu (trong trạng thái sửa thì mới cần)</param>
        /// <returns></returns>
        public static Template GetTemplateUIfromDatabase(int form, bool isAdd, Guid? inputTemplateId)
        {
            //khai báo các services
            ITemplateService templateSer = IoC.Resolve<ITemplateService>();
            ITemplateDetailService templateDetailSer = IoC.Resolve<ITemplateDetailService>();
            ITemplateColumnService templateColumnSer = IoC.Resolve<ITemplateColumnService>();

            //lấy các Template, TemplateDetail, TemplateColumn tương ứng form
            List<Template> templates = templateSer.getTemplateinTypeForm(form);

            //nếu nó đang ở trạng thái sửa và mẫu giao diện load tương ứng với chứng từ đó không có thì sẽ load giao diện mặc định (k.IsDefault == false && k.IsSecurity -> theo quy định của nghiệp vụ)
            //if (inputTemplateID == null && !isAdd) return templates.FirstOrDefault(k => k.IsDefault == false && k.IsSecurity);

            //Add thì lấy "Mẫu chuẩn" (k.IsDefault == false && k.IsSecurity), Ngược lại nếu sửa thì lấy mẫu được lưu trong chứng từ đó
            Template mauGiaoDien = (isAdd || inputTemplateId == null ? (templates.FirstOrDefault(k => k.IsDefault == false && k.IsSecurity)) : templates.FirstOrDefault(k => k.ID == inputTemplateId)) ??
                                   templates.FirstOrDefault(k => k.IsDefault == false && k.IsSecurity);
            if (mauGiaoDien != null)
            {
                List<TemplateDetail> templateDetails = templateDetailSer.getTableinTemplate(mauGiaoDien.ID);
                mauGiaoDien.TemplateDetails = templateDetails;
                foreach (TemplateDetail item2 in templateDetails) item2.TemplateColumns = templateColumnSer.getTemplateColumnsByTable(item2.ID);
            }
            return mauGiaoDien;
        }
        #endregion

        #region xử lý Tree
        public static void ConfigTree(UltraTree ultraTree, string nameTable)
        {
            ConfigTree(ultraTree, nameTable, new List<TemplateColumn>(), 0);
        }
        public static void ConfigTree(UltraTree ultraTree, string nameTable, List<TemplateColumn> configGUI, int loaiGrid)
        {//biến loaiGrid trước cần dùng, sau ko cần nữa nhưng hàm đã được mọi người dùng nên vẫn giữ hàm
            ConfigTree(ultraTree, nameTable, configGUI);
        }
        public static void ConfigTree(UltraTree ultraTree, string nameTable, List<TemplateColumn> configGUI)
        {
            //Chuyển sang style OutlookExpress
            ultraTree.ViewStyle = Infragistics.Win.UltraWinTree.ViewStyle.OutlookExpress;
            //Tự co giãn các cột (chỉ có tác dụng trong chế độ OutlookExpress)
            ultraTree.ColumnSettings.AutoFitColumns = AutoFitColumns.ResizeAllColumns;
            //
            ultraTree.ColumnSettings.ShowBandNodes = ShowBandNodes.OnlyForSiblingBands;
            //Chỉnh nút mỗi khi click vô nút mở rộng (+ hoặc -) một tập đối tượng
            ultraTree.Override.ShowExpansionIndicator = Infragistics.Win.UltraWinTree.ShowExpansionIndicator.CheckOnDisplay;
        }

        public static void uTree_InitializeDataNode(object sender, InitializeDataNodeEventArgs e)
        {
            uTree_InitializeDataNode(sender, e, string.Empty);
        }
        public static void uTree_InitializeDataNode(object sender, InitializeDataNodeEventArgs e, string nameTable)
        {
            const string strParentID = "ParentID";
            //chạy mỗi khi cần khởi tạo dữ liệu trong nhánh con của cây (thiết lập lại thể hiện nút con)
            if (e.Node.Parent == null && e.Node.Cells[strParentID].Value != DBNull.Value)
            {
                e.Node.Visible = false;
                return;
            }

            if (e.Node.Nodes.Count > 0)
            {
                if (e.Node.Parent != null) e.Node.Override.NodeSpacingBefore = 0;

                //e.Node.Override.NodeAppearance.BackColor = System.Drawing.Color.FromArgb(238, 238, 238);
                //e.Node.Override.NodeAppearance.BackColor2 = System.Drawing.Color.White;
                //e.Node.Override.NodeAppearance.BackGradientStyle = GradientStyle.Vertical;
                //e.Node.Override.NodeAppearance.BorderColor = System.Drawing.Color.FromArgb(204, 204, 204);
            }

            if (e.Node.Parent == null) e.Node.Expanded = true;
        }
        public static void uTree_ColumnSetGenerated(object sender, ColumnSetGeneratedEventArgs e, string nameTable)
        {
            const string strIsActive = "IsActive";
            //config ẩn một số cột không cần thiết
            foreach (var item in ConstDatabase.TablesInDatabase[nameTable])
            {//duyệt các cột trong bảng
                e.ColumnSet.Columns[item.Key].Text = item.Value.ColumnCaption;
                e.ColumnSet.Columns[item.Key].Visible = item.Value.IsVisible;
                if (item.Key.Equals(strIsActive))
                {
                    e.ColumnSet.Columns[item.Key].DataType = typeof(Boolean);
                    var checkBox = new UltraCheckEditor();
                    checkBox.CheckAlign = ContentAlignment.MiddleCenter;
                    e.ColumnSet.Columns[item.Key].EditorControl = checkBox;
                }
            }
        }

        public static void uTree_AfterDataNodesCollectionPopulated(object sender, AfterDataNodesCollectionPopulatedEventArgs e)
        {
            foreach (UltraTreeNodeColumn column in e.Nodes.ColumnSetResolved.Columns)
                column.PerformAutoResize();
        }

        /// <summary>
        /// Trả về OrderFixCode
        /// </summary>
        /// <param name="lstOrderFixCodeChild"></param>
        /// <param name="orderFixCodeParent"></param>
        /// <returns></returns>
        internal static string GetOrderFixCode(List<string> lstOrderFixCodeChild, string orderFixCodeParent)
        {
            string orderFixCode = null;
            List<int> temp = new List<int>();
            int rs;
            foreach (var t in lstOrderFixCodeChild)
            {
                temp.Add(String.IsNullOrEmpty(orderFixCodeParent)
                             ? Convert.ToInt32(t)
                             : Convert.ToInt32(t.Replace(orderFixCodeParent + "-", "")));
            }
            if (temp.Count == 0)
                rs = 1;
            else
            {
                rs = temp.Max() + 1;
            }
            if (String.IsNullOrEmpty(orderFixCodeParent))
                orderFixCode = rs.ToString(CultureInfo.InvariantCulture);
            else
            {
                orderFixCode = string.Format("{0}-{1}", orderFixCodeParent, rs.ToString(CultureInfo.InvariantCulture));
            }
            return orderFixCode;
        }
        #endregion xử lý Tree

        /// <summary>
        /// lấy đối tượng đang được chọn trong một Grid đối tượng ở Combobox
        /// </summary>
        /// <param name="cbb"></param>
        /// <returns></returns>
        public static object getSelectCbbItem(UltraCombo cbb)
        {
            return cbb.SelectedRow == null ? cbb.SelectedRow : cbb.SelectedRow.ListObject;
        }

        public static void SetConfigNormalUltraGrid(UltraGrid uGrid)
        {

            uGrid.DisplayLayout.Override.AllowAddNew = AllowAddNew.TemplateOnBottom;
            uGrid.DisplayLayout.Override.TemplateAddRowPrompt = resSystem.MSG_System_07;
            uGrid.DisplayLayout.Override.SpecialRowSeparator = SpecialRowSeparator.None;

            //hiển thị 1 band
            uGrid.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            //tắt lọc cột
            uGrid.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.FilterUIType = FilterUIType.Default;
            //tự thay đổi kích thước cột
            uGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            //tắt tiêu đề
            uGrid.DisplayLayout.CaptionVisible = DefaultableBoolean.False;
            //select cả hàng hay ko?
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            //Hiển thị SupportDataErrorInfo
            uGrid.DisplayLayout.Override.SupportDataErrorInfo = SupportDataErrorInfo.CellsOnly;

            uGrid.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            uGrid.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.ExtendFirstCell;

            uGrid.DisplayLayout.UseFixedHeaders = true;

        }

        public static void SetConfigDynamicUltraGrid(UltraGrid uGrid)
        {
            //hiển thị 1 band
            uGrid.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            //tắt lọc cột
            uGrid.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.FilterUIType = FilterUIType.Default;
            //tự thay đổi kích thước cột
            uGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            //tắt tiêu đề
            uGrid.DisplayLayout.CaptionVisible = DefaultableBoolean.False;
            //select cả hàng hay ko?
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            //Hiển thị SupportDataErrorInfo
            uGrid.DisplayLayout.Override.SupportDataErrorInfo = SupportDataErrorInfo.CellsOnly;
            uGrid.DisplayLayout.EmptyRowSettings.ShowEmptyRows = false;
            uGrid.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.ExtendFirstCell;

        }

        #region Huy Anh
        /// <summary>
        /// Hàm Autocomplate của UltraCombobox
        /// </summary>
        /// <typeparam name="T">Đối tượng</typeparam>
        /// <param name="combo">UltraCOmbobox được gán AutoComplate</param>
        /// <param name="list">DS lọc</param>
        public static void AutoComplateUltraCombo<T>(UltraCombo combo, List<T> list)
        {
            combo.DataSource = list;
            if (!combo.IsDroppedDown) combo.ToggleDropdown();
        }

        /// <summary>
        /// Hàm Autocomplate của UltraCombobox
        /// </summary>
        /// <param name="combo">UltraCombobox được gán AutoComplate</param>
        /// <param name="text">text</param>
        public static void AutoComplateForUltraCombo(this UltraCombo combo, string text)
        {
            if (!combo.IsDroppedDown) combo.ToggleDropdown();
            //if (!_isKeyPress)
            //{
            //    combo.ClearAutoComplete();
            //    return;
            //}
            if (combo.DataSource.GetType() == typeof(BindingList<AccountingObject>) || combo.DataSource.GetType() == typeof(BindingList<MaterialGoodsCustom>))
            {
                if (!String.IsNullOrEmpty(text.Trim()))
                {
                    foreach (var row in combo.Rows)
                    {
                        bool filterRow = true;
                        foreach (var column in combo.DisplayLayout.Bands[0].Columns)
                        {
                            if (!column.IsVisibleInLayout) continue;
                            if (row.Cells[column].Text.ToUpper().Contains(text.Trim().ToUpper()))
                            {
                                filterRow = false;
                                break;
                            }
                        }
                        if (filterRow)
                        {
                            if (row.Tag == null || (row.Tag != null && !(bool)row.Tag))
                                row.Hidden = true;
                        }
                        else
                        {
                            if (row.Tag == null || (row.Tag != null && !(bool)row.Tag))
                                row.Hidden = false;
                        }
                    }
                }
                else
                {
                    combo.ClearAutoComplete();
                }
            }
        }

        #region Lọc Text trả về Decimal
        /// <summary>
        /// Hàm lọc các ký tự của text để trả về kiểu Decimal, nếu null trả về 0.0
        /// </summary>
        /// <param name="cell">UltraGridCell</param>
        /// <returns>Double</returns>
        public static decimal GetValueFromTextAndNotNull(this UltraGridCell cell)
        {
            string str = cell.Text;
            str = str.Replace(GetFormatThousands(), "");
            str = str.Replace("_", "");
            if (GetFormatNegative() == "(n)")
            {
                str = str.Replace("(", "-");
                str = str.Replace(")", "");
            }
            if (str.Replace(GetFormatUnit(), "") == "")
            {
                str = "0.0";
                cell.Value = 0.0;
            }
            return Convert.ToDecimal(str);
        }
        /// <summary>
        /// Hàm lọc các ký tự của text để trả về kiểu decimal
        /// </summary>
        /// <param name="cell">UltraGridCell</param>
        /// <returns>Decimal</returns>
        public static decimal GetValueFromText(this UltraGridCell cell)
        {
            string str = cell.Text;
            str = str.Replace(GetFormatThousands(), "");
            str = str.Replace("_", "");
            if (GetFormatNegative() == "(n)")
            {
                str = str.Replace("(", "-");
                str = str.Replace(")", "");
            }
            if (str.Replace(GetFormatUnit(), "") == "")
            {
                str = "0.0";
                cell.Value = 0.0;
            }
            return Convert.ToDecimal(str);
        }
        /// <summary>
        /// Chuyển từ string sang Decimal
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static decimal StringToDecimal(this string input)
        {
            input = input.Replace(GetFormatThousands(), "");
            input = input.Replace("_", "");
            if (GetFormatNegative() == "(n)")
            {
                input = input.Replace("(", "-");
                input = input.Replace(")", "");
            }
            if (input.Replace(GetFormatUnit(), "") == "")
            {
                input = "0.0";
            }
            return Convert.ToDecimal(input);
        }
        #endregion

        #region Hiển thị lỗi
        /// <summary>
        /// Hiển thị báo lỗi có sử dụng UltraToolTipManager
        /// </summary>
        /// <param name="uGrid"></param>
        /// <param name="cell"></param>
        /// <param name="toolTipText"></param>
        /// <param name="ultraToolTipManager"></param>
        /// <param name="p">Tọa độ UGrid</param>
        public static void NotificationCell(UltraGrid uGrid, UltraGridCell cell, string toolTipText, UltraToolTipManager ultraToolTipManager, Point p)
        {
            cell.Appearance.Image = Properties.Resources.error;
            cell.Appearance.ForeColor = Color.Red;
            cell.ToolTipText = toolTipText;
            cell.Appearance.ImageVAlign = VAlign.Middle;
            SetToolTip(uGrid, cell, toolTipText, ultraToolTipManager, p);
        }

        /// <summary>
        /// Hiển thị báo lỗi Cell không có UltraToolTipManager
        /// </summary>
        /// <param name="uGrid"></param>
        /// <param name="cell"></param>
        /// <param name="toolTipText"></param>
        /// <param name="isMessageBox"> </param>
        public static void NotificationCell(this UltraGrid uGrid, UltraGridCell cell, string toolTipText, bool isMessageBox = false)
        {
            cell.Appearance.Image = Properties.Resources.error;
            cell.Appearance.ForeColor = Color.Red;
            cell.ToolTipText = toolTipText;
            cell.Appearance.ImageVAlign = VAlign.Middle;
            if (isMessageBox)
                MessageBox.Show(toolTipText, resSystem.MSG_System_38,
                                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
        /// <summary>
        /// Hiển thị báo lỗi Row không có UltraToolTipManager
        /// </summary>
        /// <param name="uGrid"></param>
        /// <param name="row"></param>
        /// <param name="toolTipText"></param>
        /// <param name="isMessageBox"></param>
        public static void NotificationRow(this UltraGrid uGrid, UltraGridRow row, string toolTipText, bool isMessageBox = false)
        {
            row.Appearance.Image = Properties.Resources.error;
            row.Appearance.ForeColor = Color.Red;
            row.ToolTipText = toolTipText;
            row.Appearance.ImageVAlign = VAlign.Middle;
            if (isMessageBox)
                MessageBox.Show(toolTipText, resSystem.MSG_System_38,
                                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
        /// <summary>
        /// Xóa báo lỗi có sử dụng UltraToolTipManager
        /// </summary>
        /// <param name="uGrid"></param>
        /// <param name="cell"></param>
        /// <param name="ultraToolTipManager"></param>
        /// <param name="p">Tọa độ UGrid</param>
        public static void RemoveNotificationCell(this UltraGrid uGrid, UltraGridCell cell, UltraToolTipManager ultraToolTipManager, Point p)
        {
            if (cell.Appearance == null) return;
            cell.Appearance.Image = null;
            cell.Appearance.ForeColor = Color.Black;
            cell.ToolTipText = null;
            SetToolTip(uGrid, cell, null, ultraToolTipManager, p);
        }
        /// <summary>
        /// Xóa báo lỗi Cell không có UltraToolTipManager
        /// </summary>
        /// <param name="uGrid"></param>
        /// <param name="cell"></param>
        public static void RemoveNotificationCell(this UltraGrid uGrid, UltraGridCell cell)
        {
            cell.Appearance.Image = null;
            cell.Appearance.ForeColor = Color.Black;
            cell.ToolTipText = null;
        }
        /// <summary>
        /// Xóa báo lỗi Row không có UltraToolTipManager
        /// </summary>
        /// <param name="uGrid"></param>
        /// <param name="row"></param>
        public static void RemoveNotificationRow(this UltraGrid uGrid, UltraGridRow row)
        {
            row.Appearance.Image = null;
            row.Appearance.ForeColor = Color.Black;
            row.ToolTipText = null;
        }

        public static void SetToolTip(UltraGrid uGrid, UltraGridCell cell, string toolTipText, UltraToolTipManager ultraToolTipManager, Point p)
        {
            UltraToolTipInfo infoRow;
            UIElement cellUIElement = cell.GetUIElement();
            //p.X += cell.GetUIElement().Rect.Left;
            //p.Y += cell.GetUIElement().Rect.Top;
            if (!string.IsNullOrEmpty(toolTipText))
            {
                infoRow = new UltraToolTipInfo(toolTipText, Infragistics.Win.ToolTipImage.Error, "Lỗi nhập dữ liệu", Infragistics.Win.DefaultableBoolean.True);
                ultraToolTipManager.SetUltraToolTip(uGrid, infoRow);
                ultraToolTipManager.ShowToolTip(uGrid, p);
            }
            else
                ultraToolTipManager.HideToolTip();
        }

        #endregion

        public static List<UltraGrid> FindAllGridInForm(this Control @this)
        {
            var allGrids = new List<UltraGrid>();
            foreach (Control control in @this.Controls)
            {
                if (control is UltraGrid)
                    allGrids.Add((UltraGrid)control);
                else if (control.HasChildren)
                    allGrids.AddRange(control.FindAllGridInForm());
            }
            return allGrids;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="this">Form chính</param>
        /// <param name="name"></param>
        public static void ReloadComboInGridByName(this Form @this, string name)
        {
            List<UltraGrid> allGrids = @this.FindAllGridInForm();
            foreach (UltraGrid uGrid in allGrids)
            {
                uGrid.ReloadComboInGrid(name);
            }
            //UltraTabControl ultraTabControl =
            //    (UltraTabControl)Form.Controls.Find("ultraTabControl", true).FirstOrDefault();
            //if (ultraTabControl != null)
            //{
            //    foreach (UltraTab tab in ultraTabControl.Tabs)
            //    {
            //        UltraGrid uGrid = (UltraGrid)Form.Controls.Find("uGrid" + tab.Index, true).FirstOrDefault();
            //        if (uGrid != null)
            //        {
            //            uGrid.ReloadComboInGrid(name);
            //        }
            //    }
            //}
            //else
            //{
            //    UltraGrid uGrid = (UltraGrid)Form.Controls.Find("uGrid0", true).FirstOrDefault();
            //    if (uGrid != null)
            //    {
            //        uGrid.ReloadComboInGrid(name);
            //    }
            //}
        }

        private static void ReloadComboInGrid(this UltraGrid uGrid, string name)
        {
            foreach (UltraGridRow row in uGrid.Rows)
            {
                foreach (UltraGridCell cell in row.Cells)
                {
                    if (cell.Column.Key.Contains(name))
                    {
                        if (cell.ValueListResolved != null)
                        {
                            ((UltraCombo)cell.ValueListResolved).ClearAutoComplete();
                        }
                    }
                }
            }
        }
        #region Gắn databinding có kèm tự động format

        public static void BindControl<T>(this Control control, string propertyName, T input, string bindingName, int typeFormat1, int? typeFormat2 = null, DetailBase<T> @base = null, DataSourceUpdateMode dataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged)
        {
            // Creates the binding first. The OrderAmount is a Decimal type.
            Binding b = new Binding
               (propertyName, input, bindingName, true, dataSourceUpdateMode);
            // Add the delegates to the event.
            b.Format += new ConvertEventHandler((s, e) => DecimalToCurrencyString(typeFormat1, typeFormat2, @base == null ? input : @base._select, s, e));
            b.Parse += new ConvertEventHandler(CurrencyStringToDecimal);
            control.DataBindings.Add(b);
        }

        public static void BindControl<T1, T2>(this Control control, string propertyName, T1 input, string bindingName, int typeFormat1, int? typeFormat2 = null, DetailBase<T2> @base = null, DataSourceUpdateMode dataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged)
        {
            // Creates the binding first. The OrderAmount is a Decimal type.
            Binding b = new Binding
               (propertyName, input, bindingName, true, dataSourceUpdateMode);
            // Add the delegates to the event.
            if (@base != null)
                b.Format += new ConvertEventHandler((s, e) => DecimalToCurrencyString(typeFormat1, typeFormat2, @base._select, s, e));
            b.Parse += new ConvertEventHandler(CurrencyStringToDecimal);
            control.DataBindings.Add(b);
        }

        private static void DecimalToCurrencyString<T>(int typeFormat1, int? typeFormat2, T select, object sender, ConvertEventArgs cevent)
        {
            // The method converts only to string type. Test this using the DesiredType. 
            if (cevent.DesiredType != typeof(string)) return;

            if (cevent.Value == null) return;
            if (typeFormat2 != null)
            {
                if (select.HasProperty("CurrencyID"))
                {
                    string cur = select.GetProperty<T, string>("CurrencyID");
                    cevent.Value = cur.Equals("VND") ? ((decimal)cevent.Value).FormatNumberic(typeFormat1) : ((decimal)cevent.Value).FormatNumberic((int)typeFormat2);
                }
            }
            else
                cevent.Value = ((decimal)cevent.Value).FormatNumberic(typeFormat1);
        }

        private static void CurrencyStringToDecimal(object sender, ConvertEventArgs cevent)
        {
            // The method converts back to decimal type only.  
            if (cevent.DesiredType != typeof(decimal)) return;

            if (cevent.Value == null) return;
            cevent.Value = cevent.Value.ToString().StringToDecimal();
        }
        #endregion

        /// <summary>
        /// Hàm Format định dạng số theo mẫu cho Grid
        /// </summary>
        /// <param name="column">UltraGridColumn</param>
        /// <param name="typeFormat">TypeID trong ConstDatabase</param>
        public static void FormatNumberic(this UltraGridColumn @column, int typeFormat)
        {
            @column.Format = string.Format("{0};{1}", GetTypeFormatNumberic(typeFormat), GetFormatNegative().Replace("n", GetTypeFormatNumberic(typeFormat)));
            @column.MaskInput = string.Format("{{LOC}}{0}", GetTypeFormatNumberic(typeFormat).Replace('#', 'n').Replace('0', 'n'));
            @column.MaskDataMode = MaskMode.Raw;
            @column.MaskDisplayMode = MaskMode.IncludeBoth;
            @column.MaskClipMode = MaskMode.IncludeLiterals;
            @column.PadChar = ' ';
            @column.CellAppearance.TextHAlign = HAlign.Right;
        }
        /// <summary>
        /// Hàm Format định dạng số theo mẫu cho UltraMaskedEdit
        /// </summary>
        /// <param name="control"></param>
        /// <param name="typeFormat"></param>
        public static void FormatNumberic(this UltraMaskedEdit control, int typeFormat)
        {
            control.FormatString = string.Format("{0};{1}", GetTypeFormatNumberic(typeFormat), GetFormatNegative().Replace("n", GetTypeFormatNumberic(typeFormat)));
            control.InputMask = string.Format("{{LOC}}{0}", GetTypeFormatNumberic(typeFormat).Replace('#', 'n').Replace('0', 'n'));
            control.DataMode = MaskMode.Raw;
            control.DisplayMode = MaskMode.IncludeBoth;
            control.ClipMode = MaskMode.IncludeLiterals;
            control.EditAs = EditAsType.Double;
        }
        /// <summary>
        /// Hàm Format định dạng số theo mẫu cho Control
        /// </summary>
        /// <param name="output">Control cần Format</param>
        /// <param name="input">Giá trị đầu vào</param>
        /// <param name="typeFormat">TypeID trong ConstDatabase</param>
        public static void FormatNumberic(this Control output, Object input, int typeFormat)
        {
            if (input == null || input == "") input = 0;
            try
            {
                output.Text = String.Format("{0:" + GetTypeFormatNumberic(typeFormat) + ";" + GetFormatNegative().Replace("n", GetTypeFormatNumberic(typeFormat)) + "}", Convert.ToDecimal(input));
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Cross-thread operation not valid"))
                    output.SetPropertyValue(a => a.Text,
                        String.Format(
                            "{0:" + GetTypeFormatNumberic(typeFormat) + ";" +
                            GetFormatNegative().Replace("n", GetTypeFormatNumberic(typeFormat)) + "}",
                            Convert.ToDecimal(input)));
            }
            //output.RightToLeft = RightToLeft.Yes;
        }

        #region Gán giá trị cho thuộc tính của Control khi khác Thread
        delegate void SetPropertyValueHandler<TResult>(Control souce, Expression<Func<Control, TResult>> selector, TResult value);

        public static void SetPropertyValue<TResult>(this Control source, Expression<Func<Control, TResult>> selector, TResult value)
        {
            if (source.InvokeRequired)
            {
                var del = new SetPropertyValueHandler<TResult>(SetPropertyValue);
                source.Invoke(del, new object[] { source, selector, value });
            }
            else
            {
                var propInfo = ((MemberExpression)selector.Body).Member as PropertyInfo;
                propInfo.SetValue(source, value, null);
            }
        }
        #endregion

        #region Tự động invoke sự kiện nếu cần
        public static void Invoke(this Control control, MethodInvoker action)
        {
            if (control.InvokeRequired)
            {
                control.Invoke(action);
            }
            else
            {
                action();
            }
        }
        #endregion
        public static string FormatNumberic(this Object input, int typeFormat)
        {
            return String.Format("{0:" + GetTypeFormatNumberic(typeFormat) + ";" + GetFormatNegative().Replace("n", GetTypeFormatNumberic(typeFormat)) + "}", Convert.ToDecimal(input));
            //output.RightToLeft = RightToLeft.Yes;
        }
        public static string ToStringNumbericFormat(this Object input, int typeFormat)
        {
            return String.Format("{0:" + GetTypeFormatNumberic(typeFormat) + ";" + GetFormatNegative().Replace("n", GetTypeFormatNumberic(typeFormat)) + "}", Convert.ToDecimal(input));
            //output.RightToLeft = RightToLeft.Yes;
        }
        /// <summary>
        /// Hàm Format định dạng số theo mẫu cho Control
        /// </summary>
        /// <param name="control">Control cần Format</param>
        /// <param name="typeFormat">TypeID trong ConstDatabase</param>
        public static void FormatNumberic(this Control control, int typeFormat)
        {
            if (control.GetType().Name.Equals(typeof(UltraMaskedEdit).Name))
            {
                ((UltraMaskedEdit)control).EditAs = EditAsType.Double;
                ((UltraMaskedEdit)control).NullText = "0";
                //((UltraMaskedEdit)control).FormatString = "{0:" + GetTypeFormatNumberic(typeFormat) + ";" + GetFormatNegative().Replace("n", GetTypeFormatNumberic(typeFormat)) + "}";
                ((UltraMaskedEdit)control).InputMask = "{LOC}" + GetTypeFormatNumberic(typeFormat).Replace('#', 'n').Replace('0', 'n');
            }
            //output.RightToLeft = RightToLeft.Yes;
        }
        public static string GetTypeFormatNumberic(int typeFormat, bool isHaveSymbol = false)
        {
            //string st;
            //switch (typeFormat)
            //{
            //    case 1://Tiền VND

            //        break;
            //    case 2://Tiền ngoại tệ
            //        st = "###,###,###,###,##0.00";
            //        break;
            //    case 3://Đơn giá quy đổi
            //        st = "###,###,###,###,##0.00";
            //        break;
            //    case 4://Đơn giá ngoại tệ
            //        st = "###,###,###,###,##0.00";
            //        break;
            //    case 5://Số lượng
            //        st = "###,###,###,###,##0.00";
            //        break;
            //    case 6://Số lượng chuyển đổi
            //        st = "###,###,###,###,##0.00";
            //        break;
            //    case 7://Tỷ giá
            //        st = "###,###,###,###,##0.00";
            //        break;
            //    case 8://Hệ số, tỷ lệ
            //        st = "###,###,###,###,##0.00";
            //        break;
            //    case 9://Tỷ lệ phân bố
            //        st = "###,###,###,###,##0.0000000000";
            //        break;
            //    case 10:
            //        st = "#,###,###,###.##0";
            //        break;
            //    default:
            //        st = "###,###,###,###,##0.00";
            //        break;
            //}
            return GenFormatString(GetFormatDecimalPlaces(typeFormat), isHaveSymbol ? GetFormatSymbol() : string.Empty);
        }

        private static int GetFormatDecimalPlaces(int typeFormat)
        {
            string decimalPlaces;
            switch (typeFormat)
            {
                case 1:
                    SystemOption ddSoTienVnd = ListSystemOption.FirstOrDefault(p => p.Code.Equals("DDSo_TienVND"));
                    if (ddSoTienVnd != null)
                        decimalPlaces = ddSoTienVnd.Data;
                    else goto default;
                    break;
                case 2:
                    SystemOption ddSoNgoaiTe = ListSystemOption.FirstOrDefault(p => p.Code.Equals("DDSo_NgoaiTe"));
                    if (ddSoNgoaiTe != null)
                        decimalPlaces = ddSoNgoaiTe.Data;
                    else goto default;
                    break;
                case 3:
                    SystemOption ddSoDonGia = ListSystemOption.FirstOrDefault(p => p.Code.Equals("DDSo_DonGia"));
                    if (ddSoDonGia != null)
                        decimalPlaces = ddSoDonGia.Data;
                    else goto default;
                    break;
                case 4:
                    SystemOption ddSoDonGiaNgoaiTe = ListSystemOption.FirstOrDefault(p => p.Code.Equals("DDSo_DonGia"));
                    if (ddSoDonGiaNgoaiTe != null)
                        decimalPlaces = ddSoDonGiaNgoaiTe.Data;
                    else goto default;
                    break;
                case 5:
                    SystemOption ddSoSoLuong = ListSystemOption.FirstOrDefault(p => p.Code.Equals("DDSo_SoLuong"));
                    if (ddSoSoLuong != null)
                        decimalPlaces = ddSoSoLuong.Data;
                    else goto default;
                    break;
                case 6:
                    SystemOption ddSoSoLuongChuyenDoi = ListSystemOption.FirstOrDefault(p => p.Code.Equals("DDSo_SoLuong"));
                    if (ddSoSoLuongChuyenDoi != null)
                        decimalPlaces = ddSoSoLuongChuyenDoi.Data;
                    else goto default;
                    break;
                case 7:
                    SystemOption ddSoTyGia = ListSystemOption.FirstOrDefault(p => p.Code.Equals("DDSo_TyGia"));
                    if (ddSoTyGia != null)
                        decimalPlaces = ddSoTyGia.Data;
                    else goto default;
                    break;
                case 8:
                    SystemOption ddSoTyLe = ListSystemOption.FirstOrDefault(p => p.Code.Equals("DDSo_TyLe"));
                    if (ddSoTyLe != null)
                        decimalPlaces = ddSoTyLe.Data;
                    else goto default;
                    break;
                case 9:
                    SystemOption ddSoTyLePBo = ListSystemOption.FirstOrDefault(p => p.Code.Equals("DDSo_TyLePBo"));
                    if (ddSoTyLePBo != null)
                        decimalPlaces = ddSoTyLePBo.Data;
                    else goto default;
                    break;
                default: decimalPlaces = "0"; break;
            }
            return Convert.ToInt32(decimalPlaces.Trim());
        }

        private static string GenFormatString(int decimalPlaces, string other)
        {
            string format = string.Empty;
            format += "###,###,###,###,##0";
            if (decimalPlaces > 0)
            {
                format += ".";
                for (int i = 0; i < decimalPlaces; i++)
                {
                    format += "0";
                }
            }
            if (!string.IsNullOrEmpty(other))
                format += other;
            return format;
        }

        /// <summary>
        /// Lấy format kiểu số âm từ CSDL
        /// </summary>
        /// <returns></returns>
        public static string GetFormatNegative()
        {
            SystemOption firstOrDefault = ListSystemOption.FirstOrDefault(p => p.Code.Equals("DDSo_SoAm"));
            if (firstOrDefault != null)
                return firstOrDefault.Data;
            else return "(n)";
        }

        /// <summary>
        /// Lấy ngăn cách phần nghìn từ CSDL
        /// </summary>
        /// <returns></returns>
        public static string GetFormatThousands()
        {
            SystemOption firstOrDefault = ListSystemOption.FirstOrDefault(p => p.Code.Equals("DDSo_NCachHangNghin"));
            if (firstOrDefault != null)
                return firstOrDefault.Data;
            else return ".";
        }

        /// <summary>
        /// Lấy ngăn cách phần đơn vị từ CSDL
        /// </summary>
        /// <returns></returns>
        public static string GetFormatUnit()
        {
            SystemOption firstOrDefault = ListSystemOption.FirstOrDefault(p => p.Code.Equals("DDSo_NCachHangDVi"));
            if (firstOrDefault != null)
                return firstOrDefault.Data;
            else return ",";
        }

        /// <summary>
        /// Lấy ký tự đơn vị tiền tệ từ CSDL
        /// </summary>
        /// <returns></returns>
        public static string GetFormatSymbol()
        {
            SystemOption firstOrDefault = ListSystemOption.FirstOrDefault(p => p.Code.Equals("DDSo_KyHieuTien"));
            if (firstOrDefault != null)
                return firstOrDefault.Data;
            else return string.Empty;
        }

        /// <summary>
        /// Lấy thời gian bắt đầu chương trình từ CSDL
        /// </summary>
        /// <returns></returns>
        public static string GetDbStartDate()
        {
            SystemOption firstOrDefault = ListSystemOption.FirstOrDefault(p => p.Code.Equals("NgayHachToan"));
            if (firstOrDefault != null)
                return firstOrDefault.Data;
            else return DateTime.Now.ToString("dd/MM/yyyy");
        }

        /// <summary>
        /// Lấy cấu hình có cho phép ghi sổ khi lưu không (từ CSDL)
        /// </summary>
        /// <returns></returns>
        public static bool GetIsSaveGeneralLedger()
        {
            SystemOption firstOrDefault = ListSystemOption.FirstOrDefault(p => p.Code.Equals("TCKHAC_ChiLuuKoGSo"));
            if (firstOrDefault != null)
                return !firstOrDefault.Data.Equals("1");
            else return true;
        }

        /// <summary>
        /// Lấy giá trị mặc định của Nhóm VTHH (từ CSDL)
        /// </summary>
        /// <returns></returns>
        public static string GetGoodsServicePurchaseDefault()
        {
            SystemOption firstOrDefault = ListSystemOption.FirstOrDefault(p => p.Code.Equals("VTHH_NhomHHDV"));
            if (firstOrDefault != null)
                return firstOrDefault.Data;
            else return null;
        }

        /// <summary>
        /// Lấy màu hiển thị cho chứng từ chưa ghi sổ từ CSDL
        /// </summary>
        /// <returns></returns>
        public static Color GetColorNotRecorded()
        {
            Color color = DefaultRowUnRecordedColor;
            SystemOption firstOrDefault = ListSystemOption.FirstOrDefault(p => p.Code.Equals("TCKHAC_MauCTuChuaGS"));
            if (firstOrDefault != null)
                color = ColorPickerEditor.ColorFromString(firstOrDefault.Data, color, true);
            return color;
        }

        /// <summary>
        /// Lấy cấu hình có Cho phép sao chép dữ liệu khi thêm mới dòng dữ liệu (từ CSDL)
        /// </summary>
        /// <returns></returns>
        public static bool GetIsCopyRow()
        {
            SystemOption firstOrDefault = ListSystemOption.FirstOrDefault(p => p.Code.Equals("TCKHAC_ChepDL"));
            if (firstOrDefault != null)
                return !firstOrDefault.Data.Equals("0");
            else return true;
        }

        /// <summary>
        /// Lấy cấu hình cho phép Theo dõi các khoản chi phí không hợp lý (từ CSDL)
        /// </summary>
        /// <returns></returns>
        public static bool GetIsTrackIrrationalCost()
        {
            SystemOption firstOrDefault = ListSystemOption.FirstOrDefault(p => p.Code.Equals("TCKHAC_CPKoHLy"));
            if (firstOrDefault != null)
                return !firstOrDefault.Data.Equals("0");
            else return true;
        }

        public static string GetObjectNameFromBindingName(this string input)
        {
            return input.Split(new string[] { "Accounting.Core.Domain.", ", Accounting.Core" },
                                     StringSplitOptions.RemoveEmptyEntries)[1];
        }

        /// <summary>
        /// Hàm báo lỗi không được chọn nút cha trên Tree khi check=true
        /// </summary>
        /// <param name="check">= true để báo lỗi</param>
        /// <param name="ug">UltraGrid</param>
        /// <param name="cell">UltraGridCell</param>
        public static void ShowErrorOnCellHaveTree(bool check, UltraGrid ug, UltraGridCell cell)
        {
            if (check)
                NotificationCell(ug, cell, resSystem.MSG_System_29);
            else
                RemoveNotificationCell(ug, cell);
        }

        /// <summary>
        /// Hàm tính VAT
        /// </summary>
        /// <param name="uGrid"> </param>
        /// <param name="index"></param>
        private static void CalculationVat(this UltraGrid uGrid, int index)
        {
            UltraGridBand band = uGrid.DisplayLayout.Bands[0];
            if (band.Columns.Exists("PretaxAmount") && band.Columns.Exists("VATRate") &&
                band.Columns.Exists("VATAmount"))
                uGrid.Rows[index].Cells["PretaxAmount"].Value =
                    (uGrid.Rows[index].Cells["VATRate"].Value != null && (decimal)uGrid.Rows[index].Cells["VATRate"].Value != (decimal)0 && (decimal)uGrid.Rows[index].Cells["VATRate"].Value != (decimal)-1)
                        ? (decimal)uGrid.Rows[index].Cells["VATAmount"].Value /
                          ((decimal)uGrid.Rows[index].Cells["VATRate"].Value / 100)
                        : 0;
            if (band.Columns.Exists("PretaxAmountOriginal") && band.Columns.Exists("VATRate") &&
                band.Columns.Exists("VATAmountOriginal"))
                uGrid.Rows[index].Cells["PretaxAmountOriginal"].Value =
                    (uGrid.Rows[index].Cells["VATRate"].Value != null && (decimal)uGrid.Rows[index].Cells["VATRate"].Value != (decimal)0 && (decimal)uGrid.Rows[index].Cells["VATRate"].Value != (decimal)-1)
                        ? (decimal)uGrid.Rows[index].Cells["VATAmountOriginal"].Value /
                          ((decimal)uGrid.Rows[index].Cells["VATRate"].Value / 100)
                        : 0;
        }

        /// <summary>
        /// Truyền giá trị thuế từ Tab Hạch toán sang Tab Thuế
        /// </summary>
        /// <param name="this"> </param>
        /// <param name="uGrid"></param>
        /// <param name="cell"> </param>
        /// <param name="typeId"> </param>
        private static void SetVatAmount<T>(this Form @this, UltraGrid uGrid, UltraGridCell cell, int typeId)
        {
            if ((uGrid.DisplayLayout.Bands[0].Columns.Exists("DebitAccount") ||
                 uGrid.DisplayLayout.Bands[0].Columns.Exists("CreditAccount")) &&
                (cell.Column.Key.Equals("DebitAccount") || cell.Column.Key.Equals("CreditAccount") ||
                 cell.Column.Key.Equals("AmountOriginal") || cell.Column.Key.Equals("ExchangeRate") ||
                 cell.Column.Key.Equals("Quantity") || cell.Column.Key.Contains("UnitPrice") ||
                 cell.Column.Key.Equals("DiscountAmount") || cell.Column.Key.Equals("DiscountAmountOriginal") ||
                 cell.Column.Key.Equals("DiscountRate") || cell.Column.Key.Equals("VATRate")))
            {
                decimal totalVat133 = 0;
                decimal totalVat133Original = 0;
                decimal totalVat3331 = 0;
                decimal totalVat3331Original = 0;
                decimal totalVat = 0;
                decimal totalVatOriginal = 0;
                bool isVat133 = false;
                bool isVat3331 = false;
                bool isVat = false;
                string vat133Account = "";
                string vat3331Account = "";
                string vatAccount = "";
                UltraGridBand band = uGrid.DisplayLayout.Bands[0];
                //AutoExchange(index, _bdlGOtherVoucherDetail[index].ExchangeRate);

                #region Tính tiền thuế không theo điều kiện (Đối vs TH Tab Hàng tiền và Tab Thuế cùng chung 1 đối tượng)

                if (band.Columns.Exists("MaterialGoodsID") &&
                    (!ListTypeByCreditAccount.Any(p => p.Equals(typeId)) &&
                     !ListTypeByDebitAccount.Any(p => p.Equals(typeId)) &&
                     !ListTypeByBothAccount.Any(p => p.Equals(typeId))))
                {
                    int index = cell.Row.Index;
                    if (index < 0 && index >= uGrid.Rows.Count) return;
                    if (band.Columns.Exists("DiscountRate") && band.Columns.Exists("DiscountAmount"))
                    {
                        totalVat = (cell.Row.Cells["Amount"].Value == null
                                        ? 0
                                        : (decimal)cell.Row.Cells["Amount"].Value) -
                                   (cell.Row.Cells["DiscountAmount"].Value == null
                                        ? 0
                                        : (decimal)cell.Row.Cells["DiscountAmount"].Value);
                        totalVatOriginal = (cell.Row.Cells["AmountOriginal"].Value == null
                                                ? 0
                                                : (decimal)cell.Row.Cells["AmountOriginal"].Value) -
                                           (cell.Row.Cells["DiscountAmountOriginal"].Value == null
                                                ? 0
                                                : (decimal)cell.Row.Cells["DiscountAmountOriginal"].Value);
                    }
                    else
                    {
                        totalVat = cell.Row.Cells["Amount"].Value == null
                                       ? 0
                                       : (decimal)cell.Row.Cells["Amount"].Value;
                        totalVatOriginal = cell.Row.Cells["AmountOriginal"].Value == null
                                               ? 0
                                               : (decimal)cell.Row.Cells["AmountOriginal"].Value;
                    }
                    Template template = GetMauGiaoDien(typeId, null, ListTemplate);
                    int countTabs = template.TemplateDetails.Count(p => p.TabIndex != 100);
                    for (int i = 0; i < countTabs; i++)
                    {
                        var ultraGrid =
                            (UltraGrid)@this.Controls.Find(string.Format("uGrid{0}", i), true).FirstOrDefault();
                        if (ultraGrid != null && ultraGrid.DisplayLayout.Bands[0].Columns.Exists("VATAmount") &&
                            ultraGrid.DisplayLayout.Bands[0].Columns.Exists("VATRate"))
                        {
                            decimal vatRate = uGrid.Rows[index].Cells["VATRate"].Value != null
                                                  ? (decimal)uGrid.Rows[index].Cells["VATRate"].Value / 100
                                                  : 0;
                            ultraGrid.Rows[index].Cells["VATAmount"].Value = totalVat * vatRate;
                            ultraGrid.Rows[index].Cells["VATAmountOriginal"].Value = totalVatOriginal * vatRate;
                            ultraGrid.Rows[index].UpdateData();
                            ultraGrid.CalculationVat(index);
                            @this.ReUpdateValueCell(new[] { "VATAmount" }, cell, ultraGrid);
                        }
                    }
                }
                #endregion

                #region Tính Tiền thuế theo điều kiện

                else
                {
                    #region TH tự sinh dòng chứng từ Thuế tương đương mỗi dòng chứng từ bên Tab Hàng tiền

                    if (band.Columns.Exists("MaterialGoodsID"))
                    {
                        int index = cell.Row.Index;
                        if (index < 0 && index >= uGrid.Rows.Count) return;
                        if (band.Columns.Exists("DiscountRate") && band.Columns.Exists("DiscountAmount"))
                        {
                            if (ListTypeByCreditAccount.Any(p => p.Equals(typeId)))
                            {
                                if (band.Columns.Exists("CreditAccount") &&
                                    cell.Row.Cells["CreditAccount"].Value != null)
                                {
                                    if (cell.Row.Cells["CreditAccount"].Value.ToString().StartsWith("133") ||
                                        cell.Row.Cells["CreditAccount"].Value.ToString().StartsWith("3331"))
                                    {
                                        if (band.Columns.Exists("VATAmount"))
                                            cell.Row.Cells["VATAmount"].Value =
                                                (cell.Row.Cells["Amount"].Value == null
                                                     ? 0
                                                     : (decimal)cell.Row.Cells["Amount"].Value) -
                                                (cell.Row.Cells["DiscountAmount"].Value == null
                                                     ? 0
                                                     : (decimal)cell.Row.Cells["DiscountAmount"].Value);
                                        if (band.Columns.Exists("VATAmountOriginal"))
                                            cell.Row.Cells["VATAmountOriginal"].Value =
                                                (cell.Row.Cells["AmountOriginal"].Value == null
                                                     ? 0
                                                     : (decimal)cell.Row.Cells["AmountOriginal"].Value) -
                                                (cell.Row.Cells["DiscountAmountOriginal"].Value == null
                                                     ? 0
                                                     : (decimal)cell.Row.Cells["DiscountAmountOriginal"].Value);
                                        uGrid.CalculationVat(cell.Row.Index);
                                    }
                                }
                            }
                            else if (ListTypeByDebitAccount.Any(p => p.Equals(typeId)))
                            {
                                if (band.Columns.Exists("DebitAccount") && cell.Row.Cells["DebitAccount"].Value != null)
                                {
                                    if (cell.Row.Cells["DebitAccount"].Value.ToString().StartsWith("133") ||
                                        cell.Row.Cells["DebitAccount"].Value.ToString().StartsWith("3331"))
                                    {
                                        if (band.Columns.Exists("VATAmount"))
                                            cell.Row.Cells["VATAmount"].Value =
                                                (cell.Row.Cells["Amount"].Value == null
                                                     ? 0
                                                     : (decimal)cell.Row.Cells["Amount"].Value) -
                                                (cell.Row.Cells["DiscountAmount"].Value == null
                                                     ? 0
                                                     : (decimal)cell.Row.Cells["DiscountAmount"].Value);
                                        if (band.Columns.Exists("VATAmountOriginal"))
                                            cell.Row.Cells["VATAmountOriginal"].Value =
                                                (cell.Row.Cells["AmountOriginal"].Value == null
                                                     ? 0
                                                     : (decimal)cell.Row.Cells["AmountOriginal"].Value) -
                                                (cell.Row.Cells["DiscountAmountOriginal"].Value == null
                                                     ? 0
                                                     : (decimal)cell.Row.Cells["DiscountAmountOriginal"].Value);
                                        uGrid.CalculationVat(cell.Row.Index);
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (ListTypeByCreditAccount.Any(p => p.Equals(typeId)))
                            {
                                if (band.Columns.Exists("CreditAccount") &&
                                    cell.Row.Cells["CreditAccount"].Value != null)
                                {
                                    if (cell.Row.Cells["CreditAccount"].Value.ToString().StartsWith("133") ||
                                        cell.Row.Cells["CreditAccount"].Value.ToString().StartsWith("3331"))
                                    {
                                        if (band.Columns.Exists("VATAmount"))
                                            cell.Row.Cells["VATAmount"].Value = cell.Row.Cells["Amount"].Value == null
                                                                                    ? 0
                                                                                    : (decimal)
                                                                                      cell.Row.Cells["Amount"].Value;
                                        if (band.Columns.Exists("VATAmountOriginal"))
                                            cell.Row.Cells["VATAmountOriginal"].Value =
                                                cell.Row.Cells["AmountOriginal"].Value == null
                                                    ? 0
                                                    : (decimal)cell.Row.Cells["AmountOriginal"].Value;
                                        uGrid.CalculationVat(cell.Row.Index);
                                    }
                                }
                            }
                            else if (ListTypeByDebitAccount.Any(p => p.Equals(typeId)))
                            {
                                if (band.Columns.Exists("DebitAccount") && cell.Row.Cells["DebitAccount"].Value != null)
                                {
                                    if (cell.Row.Cells["DebitAccount"].Value.ToString().StartsWith("133") ||
                                        cell.Row.Cells["DebitAccount"].Value.ToString().StartsWith("3331"))
                                    {
                                        if (band.Columns.Exists("VATAmount"))
                                            cell.Row.Cells["VATAmount"].Value = cell.Row.Cells["Amount"].Value == null
                                                                                    ? 0
                                                                                    : (decimal)
                                                                                      cell.Row.Cells["Amount"].Value;
                                        if (band.Columns.Exists("VATAmountOriginal"))
                                            cell.Row.Cells["VATAmountOriginal"].Value =
                                                cell.Row.Cells["AmountOriginal"].Value == null
                                                    ? 0
                                                    : (decimal)cell.Row.Cells["AmountOriginal"].Value;
                                        uGrid.CalculationVat(cell.Row.Index);
                                    }
                                }
                            }
                        }
                    }
                    #endregion

                    #region TH sinh dòng chứng từ thuế tổng hợp của 2 loại TK 133x và 3331x

                    else
                    {
                        foreach (var row in uGrid.Rows)
                        {
                            if (band.Columns.Exists("DiscountRate") && band.Columns.Exists("DiscountAmount"))
                            {
                                if (ListTypeByCreditAccount.Any(p => p.Equals(typeId)))
                                {
                                    if (band.Columns.Exists("CreditAccount") && row.Cells["CreditAccount"].Value != null)
                                    {
                                        if (row.Cells["CreditAccount"].Value.ToString().StartsWith("133"))
                                        {
                                            totalVat133 += (row.Cells["Amount"].Value == null
                                                                ? 0
                                                                : (decimal)row.Cells["Amount"].Value) -
                                                           (row.Cells["DiscountAmount"].Value == null
                                                                ? 0
                                                                : (decimal)row.Cells["DiscountAmount"].Value);
                                            totalVat133Original += (row.Cells["AmountOriginal"].Value == null
                                                                        ? 0
                                                                        : (decimal)row.Cells["AmountOriginal"].Value) -
                                                                   (row.Cells["DiscountAmountOriginal"].Value == null
                                                                        ? 0
                                                                        : (decimal)
                                                                          row.Cells["DiscountAmountOriginal"].Value);
                                            isVat133 = true;
                                            if (string.IsNullOrEmpty(vat133Account))
                                                vat133Account = row.Cells["CreditAccount"].Value.ToString();
                                        }
                                        else if (row.Cells["CreditAccount"].Value.ToString().StartsWith("3331"))
                                        {
                                            totalVat3331 += (row.Cells["Amount"].Value == null
                                                                 ? 0
                                                                 : (decimal)row.Cells["Amount"].Value) -
                                                            (row.Cells["DiscountAmount"].Value == null
                                                                 ? 0
                                                                 : (decimal)row.Cells["DiscountAmount"].Value);
                                            totalVat3331Original += (row.Cells["AmountOriginal"].Value == null
                                                                         ? 0
                                                                         : (decimal)row.Cells["AmountOriginal"].Value) -
                                                                    (row.Cells["DiscountAmountOriginal"].Value == null
                                                                         ? 0
                                                                         : (decimal)
                                                                           row.Cells["DiscountAmountOriginal"].Value);
                                            isVat3331 = true;
                                            if (string.IsNullOrEmpty(vat3331Account))
                                                vat3331Account = row.Cells["CreditAccount"].Value.ToString();
                                        }
                                    }
                                }
                                else if (ListTypeByDebitAccount.Any(p => p.Equals(typeId)))
                                {
                                    if (band.Columns.Exists("DebitAccount") && row.Cells["DebitAccount"].Value != null)
                                    {
                                        if (row.Cells["DebitAccount"].Value.ToString().StartsWith("133"))
                                        {
                                            totalVat133 += (row.Cells["Amount"].Value == null
                                                                ? 0
                                                                : (decimal)row.Cells["Amount"].Value) -
                                                           (row.Cells["DiscountAmount"].Value == null
                                                                ? 0
                                                                : (decimal)row.Cells["DiscountAmount"].Value);
                                            totalVat133Original += (row.Cells["AmountOriginal"].Value == null
                                                                        ? 0
                                                                        : (decimal)row.Cells["AmountOriginal"].Value) -
                                                                   (row.Cells["DiscountAmountOriginal"].Value == null
                                                                        ? 0
                                                                        : (decimal)
                                                                          row.Cells["DiscountAmountOriginal"].Value);
                                            isVat133 = true;
                                            if (string.IsNullOrEmpty(vat133Account))
                                                vat133Account = row.Cells["DebitAccount"].Value.ToString();
                                        }
                                        else if (row.Cells["DebitAccount"].Value.ToString().StartsWith("3331"))
                                        {
                                            totalVat3331 += (row.Cells["Amount"].Value == null
                                                                 ? 0
                                                                 : (decimal)row.Cells["Amount"].Value) -
                                                            (row.Cells["DiscountAmount"].Value == null
                                                                 ? 0
                                                                 : (decimal)row.Cells["DiscountAmount"].Value);
                                            totalVat3331Original += (row.Cells["AmountOriginal"].Value == null
                                                                         ? 0
                                                                         : (decimal)row.Cells["AmountOriginal"].Value) -
                                                                    (row.Cells["DiscountAmountOriginal"].Value == null
                                                                         ? 0
                                                                         : (decimal)
                                                                           row.Cells["DiscountAmountOriginal"].Value);
                                            isVat3331 = true;
                                            if (string.IsNullOrEmpty(vat3331Account))
                                                vat3331Account = row.Cells["DebitAccount"].Value.ToString();
                                        }
                                    }
                                }
                                else if (ListTypeByBothAccount.Any(p => p.Equals(typeId)))
                                {
                                    if ((band.Columns.Exists("DebitAccount") && band.Columns.Exists("CreditAccount") &&
                                         (row.Cells["DebitAccount"].Value != null ||
                                          row.Cells["CreditAccount"].Value != null)))
                                    {
                                        if ((row.Cells["DebitAccount"].Value != null &&
                                             (row.Cells["DebitAccount"].Value.ToString().StartsWith("133") ||
                                              row.Cells["DebitAccount"].Value.ToString().StartsWith("3331"))) ||
                                            (row.Cells["CreditAccount"].Value != null &&
                                             (row.Cells["CreditAccount"].Value.ToString().StartsWith("133") ||
                                              row.Cells["CreditAccount"].Value.ToString().StartsWith("3331"))))
                                        {
                                            totalVat += (row.Cells["Amount"].Value == null
                                                             ? 0
                                                             : (decimal)row.Cells["Amount"].Value) -
                                                        (row.Cells["DiscountAmount"].Value == null
                                                             ? 0
                                                             : (decimal)row.Cells["DiscountAmount"].Value);
                                            totalVatOriginal += (row.Cells["AmountOriginal"].Value == null
                                                                     ? 0
                                                                     : (decimal)row.Cells["AmountOriginal"].Value) -
                                                                (row.Cells["DiscountAmountOriginal"].Value == null
                                                                     ? 0
                                                                     : (decimal)
                                                                       row.Cells["DiscountAmountOriginal"].Value);
                                            isVat = true;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (ListTypeByCreditAccount.Any(p => p.Equals(typeId)))
                                {
                                    if (band.Columns.Exists("CreditAccount") && row.Cells["CreditAccount"].Value != null)
                                    {
                                        if (row.Cells["CreditAccount"].Value.ToString().StartsWith("133"))
                                        {
                                            totalVat133 += row.Cells["Amount"].Value == null
                                                               ? 0
                                                               : (decimal)row.Cells["Amount"].Value;
                                            totalVat133Original += row.Cells["AmountOriginal"].Value == null
                                                                       ? 0
                                                                       : (decimal)row.Cells["AmountOriginal"].Value;
                                            isVat133 = true;
                                            if (string.IsNullOrEmpty(vat133Account))
                                                vat133Account = row.Cells["CreditAccount"].Value.ToString();
                                        }
                                        else if (row.Cells["CreditAccount"].Value.ToString().StartsWith("3331"))
                                        {
                                            totalVat3331 += row.Cells["Amount"].Value == null
                                                                ? 0
                                                                : (decimal)row.Cells["Amount"].Value;
                                            totalVat3331Original += row.Cells["AmountOriginal"].Value == null
                                                                        ? 0
                                                                        : (decimal)row.Cells["AmountOriginal"].Value;
                                            isVat3331 = true;
                                            if (string.IsNullOrEmpty(vat3331Account))
                                                vat3331Account = row.Cells["CreditAccount"].Value.ToString();
                                        }
                                    }
                                }
                                else if (ListTypeByDebitAccount.Any(p => p.Equals(typeId)))
                                {
                                    if (band.Columns.Exists("DebitAccount") && row.Cells["DebitAccount"].Value != null)
                                    {
                                        if (row.Cells["DebitAccount"].Value.ToString().StartsWith("133"))
                                        {
                                            totalVat133 += row.Cells["Amount"].Value == null
                                                               ? 0
                                                               : (decimal)row.Cells["Amount"].Value;
                                            totalVat133Original += row.Cells["AmountOriginal"].Value == null
                                                                       ? 0
                                                                       : (decimal)row.Cells["AmountOriginal"].Value;
                                            isVat133 = true;
                                            if (string.IsNullOrEmpty(vat133Account))
                                                vat133Account = row.Cells["DebitAccount"].Value.ToString();
                                        }
                                        else if (row.Cells["DebitAccount"].Value.ToString().StartsWith("3331"))
                                        {
                                            totalVat3331 += row.Cells["Amount"].Value == null
                                                                ? 0
                                                                : (decimal)row.Cells["Amount"].Value;
                                            totalVat3331Original += row.Cells["AmountOriginal"].Value == null
                                                                        ? 0
                                                                        : (decimal)row.Cells["AmountOriginal"].Value;
                                            isVat3331 = true;
                                            if (string.IsNullOrEmpty(vat3331Account))
                                                vat3331Account = row.Cells["DebitAccount"].Value.ToString();
                                        }
                                    }
                                }
                                else if (ListTypeByBothAccount.Any(p => p.Equals(typeId)))
                                {
                                    if ((band.Columns.Exists("DebitAccount") && band.Columns.Exists("CreditAccount") &&
                                         (row.Cells["DebitAccount"].Value != null ||
                                          row.Cells["CreditAccount"].Value != null)))
                                    {
                                        if (row.Cells["DebitAccount"].Value != null &&
                                            (row.Cells["DebitAccount"].Value.ToString().StartsWith("133") ||
                                             row.Cells["DebitAccount"].Value.ToString().StartsWith("3331")))
                                        {
                                            totalVat += row.Cells["Amount"].Value == null
                                                            ? 0
                                                            : (decimal)row.Cells["Amount"].Value;
                                            totalVatOriginal += row.Cells["AmountOriginal"].Value == null
                                                                    ? 0
                                                                    : (decimal)row.Cells["AmountOriginal"].Value;
                                            isVat = true;
                                        }
                                        else if (row.Cells["CreditAccount"].Value != null &&
                                                 (row.Cells["CreditAccount"].Value.ToString().StartsWith("133") ||
                                                  row.Cells["CreditAccount"].Value.ToString().StartsWith("3331")))
                                        {
                                            totalVat += row.Cells["Amount"].Value == null
                                                            ? 0
                                                            : (decimal)row.Cells["Amount"].Value;
                                            totalVatOriginal += row.Cells["AmountOriginal"].Value == null
                                                                    ? 0
                                                                    : (decimal)row.Cells["AmountOriginal"].Value;
                                            isVat = true;
                                        }
                                    }
                                }
                            }
                        }
                        Template mauGiaoDien = GetMauGiaoDien(typeId, null, ListTemplate);
                        int countTabs = mauGiaoDien.TemplateDetails.Count(p => p.TabIndex != 100);
                        for (int i = 0; i < countTabs; i++)
                        {
                            var ultraGrid =
                                (UltraGrid)@this.Controls.Find(string.Format("uGrid{0}", i), true).FirstOrDefault();
                            if (ultraGrid.DisplayLayout.Bands[0].Columns.Exists("VATAmount"))
                            {
                                int current = ultraGrid.Rows.Count - 1;
                                bool haveRowVat133 = false;
                                bool haveRowVat3331 = false;
                                bool haveRowVat = false;
                                int k = 0;
                                while (k < ultraGrid.Rows.Count)
                                {
                                    UltraGridRow row = ultraGrid.Rows[k];
                                    if (row.Tag == null)
                                    {
                                        k++;
                                        continue;
                                    }
                                    if (row.Tag != null && row.Tag.ToString().Equals("Vat133"))
                                    {
                                        if (isVat133)
                                        {
                                            haveRowVat133 = true;
                                            row.Cells["VATAmount"].Value = totalVat133;
                                            row.Cells["VATAmountOriginal"].Value = totalVat133Original;
                                            if (ultraGrid.DisplayLayout.Bands[0].Columns.Exists("VATAccount"))
                                                row.Cells["VATAccount"].Value = vat133Account;
                                            current = row.Index;
                                            CalculationVat(ultraGrid, current);
                                            ultraGrid.Update();
                                            k++;
                                        }
                                        else
                                        {
                                            if (current >= row.Index) current--;
                                            row.Delete(false);
                                        }
                                    }
                                    else if (row.Tag != null && row.Tag.ToString().Equals("Vat3331"))
                                    {
                                        if (isVat3331)
                                        {
                                            haveRowVat3331 = true;
                                            row.Cells["VATAmount"].Value = totalVat3331;
                                            row.Cells["VATAmountOriginal"].Value = totalVat3331Original;
                                            if (ultraGrid.DisplayLayout.Bands[0].Columns.Exists("VATAccount"))
                                                row.Cells["VATAccount"].Value = vat3331Account;
                                            current = row.Index;
                                            CalculationVat(ultraGrid, current);
                                            ultraGrid.Update();
                                            k++;
                                        }
                                        else
                                        {
                                            if (current >= row.Index) current--;
                                            row.Delete(false);
                                        }
                                    }
                                    if (row.Tag != null && row.Tag.ToString().Equals("Vat") &&
                                        !ultraGrid.DisplayLayout.Bands[0].Columns.Exists("MaterialGoodsID"))
                                    {
                                        if (isVat)
                                        {
                                            haveRowVat = true;
                                            row.Cells["VATAmount"].Value = totalVat;
                                            row.Cells["VATAmountOriginal"].Value = totalVatOriginal;
                                            if (ultraGrid.DisplayLayout.Bands[0].Columns.Exists("VATAccount"))
                                                row.Cells["VATAccount"].Value = vatAccount;
                                            current = row.Index;
                                            CalculationVat(ultraGrid, current);
                                            ultraGrid.Update();
                                            k++;
                                        }
                                        else
                                        {
                                            if (current >= row.Index) current--;
                                            row.Delete(false);
                                        }
                                    }
                                }
                                if (!haveRowVat133 && isVat133)
                                {
                                    //IList input = ultraGrid.DataSource as IList;
                                    if (!current.Equals(0) || (current.Equals(0) && haveRowVat3331) ||
                                        ultraGrid.Rows.Count.Equals(0))
                                    {
                                        ultraGrid.DisplayLayout.Bands[0].AddNew();
                                        //BindingListAddNew<T>(ultraGrid, current);
                                        if (ultraGrid.Rows.Count.Equals(1))
                                            current = 0;
                                        else
                                            current++;
                                    }
                                    ultraGrid.Rows[current].Cells["VATAmount"].Value = totalVat133;
                                    ultraGrid.Rows[current].Cells["VATAmountOriginal"].Value = totalVat133Original;
                                    ultraGrid.Rows[current].Tag = "Vat133";
                                    if (ultraGrid.DisplayLayout.Bands[0].Columns.Exists("VATAccount"))
                                        ultraGrid.Rows[current].Cells["VATAccount"].Value = vat133Account;
                                    CalculationVat(ultraGrid, current);
                                    ultraGrid.Rows[current].Update();
                                }
                                if (!haveRowVat3331 && isVat3331)
                                {
                                    if (!current.Equals(0) || (current.Equals(0) && haveRowVat133) ||
                                        ultraGrid.Rows.Count.Equals(0))
                                    {
                                        ultraGrid.DisplayLayout.Bands[0].AddNew();
                                        //BindingListAddNew<T>(ultraGrid, current);
                                        if (ultraGrid.Rows.Count.Equals(1))
                                            current = 0;
                                        else
                                            current++;
                                    }
                                    ultraGrid.Rows[current].Cells["VATAmount"].Value = totalVat3331;
                                    ultraGrid.Rows[current].Cells["VATAmountOriginal"].Value = totalVat3331Original;
                                    ultraGrid.Rows[current].Tag = "Vat3331";
                                    if (ultraGrid.DisplayLayout.Bands[0].Columns.Exists("VATAccount"))
                                        ultraGrid.Rows[current].Cells["VATAccount"].Value = vat3331Account;
                                    CalculationVat(ultraGrid, current);
                                    ultraGrid.Rows[current].Update();
                                }
                                if (!haveRowVat && isVat)
                                {
                                    ultraGrid.DisplayLayout.Bands[0].AddNew();
                                    //BindingListAddNew<T>(ultraGrid, current);
                                    if (ultraGrid.Rows.Count.Equals(1))
                                        current = 0;
                                    else
                                        current++;
                                    ultraGrid.Rows[current].Cells["VATAmount"].Value = totalVat;
                                    ultraGrid.Rows[current].Cells["VATAmountOriginal"].Value = totalVatOriginal;
                                    ultraGrid.Rows[current].Tag = "Vat";
                                    //if (ultraGrid.DisplayLayout.Bands[0].Columns.Exists("VATAccount"))
                                    //    ultraGrid.Rows[current].Cells["VATAccount"].Value = vatAccount;
                                    CalculationVat(ultraGrid, current);
                                    ultraGrid.Rows[current].Update();
                                }
                            }
                        }
                    }

                    #endregion
                }

                #endregion
            }
        }

        public static void ClearBindingList<T>(this BindingList<T> bindingList)
        {
            while (bindingList.Count > 0)
            {
                bindingList.Remove(bindingList[0]);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="combo"></param>
        /// <param name="select">Loại đối tượng AccountingObject: 0-Khách hàng. 1-Nhà cung cấp. 2-Nhân viên. 3-KH và NCC</param>
        public static void ConfigComboAccouting(this UltraCombo combo, int? select = null)
        {
            foreach (UltraGridRow row in combo.Rows)
            {
                row.ConfigComboRowAccounting(select);
            }
            var dicTag = new Dictionary<string, object>();
            if (@combo.Tag != null)
            {
                dicTag = (Dictionary<string, object>)@combo.Tag;
                if (dicTag.Any(p => p.Key.Equals("AccountingObjectType")))
                    dicTag.Remove("AccountingObjectType");
            }
            dicTag.Add("AccountingObjectType", select);
            @combo.Tag = dicTag;
        }

        public static bool IsTypeFaIncrement(this int typeId)
        {
            return new[] { 500, 510, 119, 129, 132, 142, 172 }.Any(x => x == typeId);
        }
        public static bool IsTypeFaDecrement(this int typeId)
        {
            return new[] { 520 }.Any(x => x == typeId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="combo"></param>
        /// <param name="select">Loại ds Mã TS: 0-Nằm trong FAIncrement. 1-Không nằm trong FAIncrement. null-Tất cả.</param>
        public static void ConfigComboFixedAsset(this UltraCombo combo, int? select = null)
        {
            var dicTag = new Dictionary<string, object>();
            List<Guid?> fixedAssetIdsInFaIncrement = null;
            if (@combo.Tag != null)
            {
                dicTag = (Dictionary<string, object>)@combo.Tag;
                if (dicTag.Any(p => p.Key.Equals("FixedAssetType")))
                    dicTag.Remove("FixedAssetType");
                if (dicTag.Any(p => p.Key.Equals("FixedAssetIdJoinFAIncrement")))
                    goto Jump;
            }
            if (select == null) goto Jump;
            var lstTypeFaIncrement = new List<int> { 500, 510, 119, 129, 132, 142, 172 };
            if (select == 0)//Ghi giảm - DS các TS có trong sổ TSCĐ với type Ghi tăng và chưa phát sinh ghi giảm
            {
                var a = (from inc in IFixedAssetLedgerService.Query.ToList()
                         join i in lstTypeFaIncrement on inc.TypeID equals i
                         select inc).ToList();
                var b = (from f in a
                         join i in
                             (from de in IFixedAssetLedgerService.Query where de.TypeID == 520 select de).ToList()
                             on f.FixedAssetID equals i.FixedAssetID
                         select f).ToList();
                fixedAssetIdsInFaIncrement = (from u in a
                                              where b.All(x => x.FixedAssetID != u.FixedAssetID)
                                              group u by new { ID = u.FixedAssetID } into g
                                              select g.Key.ID).ToList();
            }
            else if (select == 1)//Ghi tăng - DS các TS có trong sổ TSCĐ với type Ghi tăng
                fixedAssetIdsInFaIncrement = (from f in IFixedAssetLedgerService.Query.ToList()
                                              join i in lstTypeFaIncrement on f.TypeID equals i
                                              group f by new { ID = f.FixedAssetID } into g
                                              select g.Key.ID).ToList();
            dicTag.Add("FixedAssetIdJoinFAIncrement", fixedAssetIdsInFaIncrement);
        Jump:
            dicTag.Add("FixedAssetType", select);
            @combo.Tag = dicTag;

            foreach (UltraGridRow row in combo.Rows)
            {
                row.ConfigComboRowFixedAsset(select, fixedAssetIdsInFaIncrement);
            }
        }
        public static void ConfigComboServices(this UltraCombo combo)
        {
            foreach (UltraGridRow row in combo.Rows)
            {
                if ((int?)row.Cells["MaterialGoodsType"].Value != 2)
                {
                    row.Hidden = true;
                    row.Tag = true;
                }
                else
                {
                    if ((bool)row.Cells["IsActive"].Value)
                    {
                        row.Hidden = false;
                        row.Tag = null;
                    }
                    else
                    {
                        row.Hidden = true;
                        row.Tag = true;
                    }
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="row"></param>
        /// <param name="select">Loại đối tượng AccountingObject: 0-Khách hàng. 1-Nhà cung cấp. 2-Nhân viên. 3-KH và NCC</param>
        public static void ConfigComboRowAccounting(this UltraGridRow row, int? select = null)
        {
            switch (select)
            {
                case 0://Khách hàng
                    if (!((int?)row.Cells["ObjectType"].Value == 0 || (int?)row.Cells["ObjectType"].Value == 2))
                    {
                        row.Hidden = true;
                        row.Tag = true;
                    }
                    else
                    {
                        if (row.Band.Columns.Exists("IsActive"))
                            if ((bool)row.Cells["IsActive"].Value)
                            {
                                row.Hidden = false;
                                row.Tag = null;
                            }
                            else
                            {
                                row.Hidden = true;
                                row.Tag = true;
                            }
                    }
                    break;
                case 1://Nhà cung cấp
                    if (!((int?)row.Cells["ObjectType"].Value == 1 || (int?)row.Cells["ObjectType"].Value == 2))
                    {
                        row.Hidden = true;
                        row.Tag = true;
                    }
                    else
                    {
                        if (row.Band.Columns.Exists("IsActive"))
                            if ((bool)row.Cells["IsActive"].Value)
                            {
                                row.Hidden = false;
                                row.Tag = null;
                            }
                            else
                            {
                                row.Hidden = true;
                                row.Tag = true;
                            }
                    }
                    break;
                case 2://Nhân viên
                    if (!(bool)row.Cells["IsEmployee"].Value)
                    {
                        row.Hidden = true;
                        row.Tag = true;
                    }
                    else
                    {
                        if (row.Band.Columns.Exists("IsActive"))
                            if ((bool)row.Cells["IsActive"].Value)
                            {
                                row.Hidden = false;
                                row.Tag = null;
                            }
                            else
                            {
                                row.Hidden = true;
                                row.Tag = true;
                            }
                    }
                    break;
                case 3://Khách hàng + Nhà cung cấp
                    if ((bool)row.Cells["IsEmployee"].Value)
                    {
                        row.Hidden = true;
                        row.Tag = true;
                    }
                    else
                    {
                        if (row.Band.Columns.Exists("IsActive"))
                            if ((bool)row.Cells["IsActive"].Value)
                            {
                                row.Hidden = false;
                                row.Tag = null;
                            }
                            else
                            {
                                row.Hidden = true;
                                row.Tag = true;
                            }
                    }
                    break;
                default://All
                    if (row.Band.Columns.Exists("IsActive"))
                        if ((bool)row.Cells["IsActive"].Value)
                        {
                            row.Hidden = false;
                            row.Tag = null;
                        }
                        else
                        {
                            row.Hidden = true;
                            row.Tag = true;
                        }
                    break;
            }
        }

        /// <summary>
        /// Kiểm tra TS đã phát sinh Mua hoặc ghi tăng chưa
        /// </summary>
        /// <param name="id">ID TS</param>
        /// <param name="fixedAssetIdsInFaIncrement">DS các ID TS có phát sinh Mua hoặc ghi tăng </param>
        /// <returns></returns>
        private static bool CheckFixedAssetInFaIncrement(Guid id, List<Guid?> fixedAssetIdsInFaIncrement = null)
        {
            return fixedAssetIdsInFaIncrement != null && fixedAssetIdsInFaIncrement.Any(x => x == id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="row"></param>
        /// <param name="select">Loại ds Mã TS: 0-Nằm trong FAIncrement. 1-Không nằm trong FAIncrement. null-Tất cả.</param>
        /// <param name="fixedAssetIdsInFaIncrement">DS FixedAssetId của các FixedAsset có trong FAIncrement.</param>
        public static void ConfigComboRowFixedAsset(this UltraGridRow row, int? select = null, List<Guid?> fixedAssetIdsInFaIncrement = null)
        {
            switch (select)
            {
                case 0://Nằm trong FAIncrement - Ghi giảm
                    if (!CheckFixedAssetInFaIncrement((Guid)row.Cells["ID"].Value, fixedAssetIdsInFaIncrement))
                    {
                        row.Hidden = true;
                        row.Tag = true;
                    }
                    else
                    {
                        if (row.Band.Columns.Exists("IsActive"))
                            if ((bool)row.Cells["IsActive"].Value)
                            {
                                row.Hidden = false;
                                row.Tag = null;
                            }
                            else
                            {
                                row.Hidden = true;
                                row.Tag = true;
                            }
                    }
                    break;
                case 1://Không nằm trong FAIncrement - Ghi tăng
                    if (CheckFixedAssetInFaIncrement((Guid)row.Cells["ID"].Value, fixedAssetIdsInFaIncrement))
                    {
                        row.Hidden = true;
                        row.Tag = true;
                    }
                    else
                    {
                        if (row.Band.Columns.Exists("IsActive"))
                            if ((bool)row.Cells["IsActive"].Value)
                            {
                                row.Hidden = false;
                                row.Tag = null;
                            }
                            else
                            {
                                row.Hidden = true;
                                row.Tag = true;
                            }
                    }
                    break;
                default://All
                    if (row.Band.Columns.Exists("IsActive"))
                        if ((bool)row.Cells["IsActive"].Value)
                        {
                            row.Hidden = false;
                            row.Tag = null;
                        }
                        else
                        {
                            row.Hidden = true;
                            row.Tag = true;
                        }
                    break;
            }
        }

        public static void SetMaxLengthForTextBox(this Control ctrlContainer)
        {
            if (!ctrlContainer.GetType().Name.Equals(typeof(UltraGrid).Name) && !ctrlContainer.GetType().Name.Contains("UTopVouchers"))
            {
                foreach (Control control in ctrlContainer.Controls)
                {
                    if (control.GetType().Name.Equals(typeof(UltraTextEditor).Name))
                    {
                        PropertyInfo propMaxLength = control.GetType().GetProperty("MaxLength");
                        if (propMaxLength != null && propMaxLength.CanWrite)
                        {
                            if ((int)propMaxLength.GetValue(control, null) != 25)
                                propMaxLength.SetValue(control, 512, null);
                        }
                        //UltraTextEditor txtEditor = (UltraTextEditor)control;
                        //txtEditor.MaxLength
                    }
                    else if (control.HasChildren)
                        SetMaxLengthForTextBox(control);
                }
            }
        }

        /// <summary>
        /// Gan ds quan ly danh sach List chung dang su dung
        /// Lưu ý: Chỉ có thể add duy nhất 1 List cho 1 kiểu đối tượng
        /// </summary>
        /// <param name="input"></param>
        public static void ManagerCache(this IList input)
        {
            if (input.GetType().HaveCache())
                input.GetType().RemoveCache();
            Cache.Add(input);
        }
        public static bool HaveCache(this System.Type type)
        {
            return Cache.Any(p =>
            {
                var fullName = p.GetType().FullName;
                return fullName != null && fullName.Equals(type.FullName);
            });
        }
        public static void RemoveCache(this System.Type type)
        {
            if (type.HaveCache())
                Cache.Remove(Cache.FirstOrDefault(p =>
                                                      {
                                                          var fullName = p.GetType().FullName;
                                                          return fullName != null &&
                                                                 fullName.Equals(type.FullName);
                                                      }));
        }
        public static BindingList<T> GetBindingListFromCache<T>(this System.Type type)
        {
            BindingList<T> result = null;
            if (type.HaveCache())
            {
                var firstOrDefault = Cache.FirstOrDefault(p =>
                                                              {
                                                                  var fullName = p.GetType().FullName;
                                                                  return fullName != null && fullName.Equals(type.FullName);
                                                              });
                if (firstOrDefault != null)
                    result = (BindingList<T>)firstOrDefault;
            }
            return result;
        }
        public static List<T> GetListFromCache<T>(this System.Type type)
        {
            List<T> result = null;
            if (type.HaveCache())
            {
                var firstOrDefault = Cache.FirstOrDefault(p =>
                {
                    var fullName = p.GetType().FullName;
                    return fullName != null && fullName.Equals(type.FullName);
                });
                if (firstOrDefault != null)
                    result = (List<T>)firstOrDefault;
            }
            return result;
        }
        /// <summary>
        /// Làm sạch ds chung
        /// </summary>
        public static void ClearCache()
        {
            for (int i = 0; i < Cache.Count; i++)
            {
                var data = Cache[i];
                try
                {
                    if (data is BindingList<AccountingObject>)
                        IAccountingObjectService.UnbindSession(_cacheAccountingObjects);
                    else if (data is BindingList<AccountDefault>)
                        IAccountDefaultService.UnbindSession(_cacheAccountDefaults);
                    else if (data is BindingList<Currency>)
                        ICurrencyService.UnbindSession(_cacheCurrencys);
                    else if (data is BindingList<AccountingObjectBankAccount>)
                        IAccountingObjectBankAccountService.UnbindSession(_cacheAccountingObjectBanks);
                    else if (data is BindingList<BankAccountDetail>)
                        IBankAccountDetailService.UnbindSession(_cacheBankAccountDetails);
                    else if (data is BindingList<BudgetItem>)
                        IBudgetItemService.UnbindSession(_cacheBudgetItems);
                    else if (data is BindingList<CostSet>)
                        ICostSetService.UnbindSession(_cacheCostSets);
                    else if (data is BindingList<CreditCard>)
                        ICreditCardService.UnbindSession(_cacheCreditCards);
                    else if (data is BindingList<Department>)
                        IDepartmentService.UnbindSession(_cacheDepartments);
                    else if (data is BindingList<EMContract>)
                        IEMContractService.UnbindSession(_cacheEmContracts);
                    else if (data is BindingList<ExpenseItem>)
                        IExpenseItemService.UnbindSession(_cacheExpenseItems);
                    else if (data is BindingList<FixedAsset>)
                        IFixedAssetService.UnbindSession(_cacheFixedAssets);
                    else if (data is BindingList<GoodsServicePurchase>)
                        IGoodsServicePurchaseService.UnbindSession(_cacheGoodsServicePurchases);
                    else if (data is BindingList<InvoiceType>)
                        IInvoiceTypeService.UnbindSession(_cacheInvoiceTypes);
                    else if (data is BindingList<MaterialGoodsCustom>)
                        IMaterialGoodsService.UnbindSession(_cacheMaterialGoodsCustoms);
                    else if (data is BindingList<PaymentClause>)
                        IPaymentClauseService.UnbindSession(_cachePaymentClauses);
                    else if (data is BindingList<Repository>)
                        IRepositoryService.UnbindSession(_cacheRepositorys);
                    else if (data is BindingList<SAQuote>)
                        ISAQuoteService.UnbindSession(_cacheSaQuotes);
                    else if (data is BindingList<StatisticsCode>)
                        IStatisticsCodeService.UnbindSession(_cacheStatisticsCodes);
                    else if (data is BindingList<SystemOption>)
                        ISystemOptionService.UnbindSession(_cacheSystemOptions);
                }
                catch (Exception)
                {
                }
                data.Clear();
            }
            ListTemplate.Clear();
            System.Threading.Thread.CurrentThread.CurrentCulture = Config();
        }
        public static void ClearCacheByType<T>()
        {
            if (!typeof(BindingList<T>).HaveCache()) return;
            var cache = typeof(BindingList<T>).GetBindingListFromCache<T>();
            cache.Clear();
            try
            {
                if (typeof(T) == typeof(AccountingObject))
                    IAccountingObjectService.UnbindSession(_cacheAccountingObjects);
                else if (typeof(T) == typeof(AccountDefault))
                    IAccountDefaultService.UnbindSession(_cacheAccountDefaults);
                else if (typeof(T) == typeof(Currency))
                    ICurrencyService.UnbindSession(_cacheCurrencys);
                else if (typeof(T) == typeof(AccountingObjectBankAccount))
                    IAccountingObjectBankAccountService.UnbindSession(_cacheAccountingObjectBanks);
                else if (typeof(T) == typeof(BankAccountDetail))
                    IBankAccountDetailService.UnbindSession(_cacheBankAccountDetails);
                else if (typeof(T) == typeof(BudgetItem))
                    IBudgetItemService.UnbindSession(_cacheBudgetItems);
                else if (typeof(T) == typeof(CostSet))
                    ICostSetService.UnbindSession(_cacheCostSets);
                else if (typeof(T) == typeof(CreditCard))
                    ICreditCardService.UnbindSession(_cacheCreditCards);
                else if (typeof(T) == typeof(Department))
                    IDepartmentService.UnbindSession(_cacheDepartments);
                else if (typeof(T) == typeof(EMContract))
                    IEMContractService.UnbindSession(_cacheEmContracts);
                else if (typeof(T) == typeof(ExpenseItem))
                    IExpenseItemService.UnbindSession(_cacheExpenseItems);
                else if (typeof(T) == typeof(FixedAsset))
                    IFixedAssetService.UnbindSession(_cacheFixedAssets);
                else if (typeof(T) == typeof(GoodsServicePurchase))
                    IGoodsServicePurchaseService.UnbindSession(_cacheGoodsServicePurchases);
                else if (typeof(T) == typeof(InvoiceType))
                    IInvoiceTypeService.UnbindSession(_cacheInvoiceTypes);
                else if (typeof(T) == typeof(MaterialGoodsCustom))
                    IMaterialGoodsService.UnbindSession(_cacheMaterialGoodsCustoms);
                else if (typeof(T) == typeof(PaymentClause))
                    IPaymentClauseService.UnbindSession(_cachePaymentClauses);
                else if (typeof(T) == typeof(Repository))
                    IRepositoryService.UnbindSession(_cacheRepositorys);
                else if (typeof(T) == typeof(SAQuote))
                    ISAQuoteService.UnbindSession(_cacheSaQuotes);
                else if (typeof(T) == typeof(StatisticsCode))
                    IStatisticsCodeService.UnbindSession(_cacheStatisticsCodes);
                else if (typeof(T) == typeof(SystemOption))
                    ISystemOptionService.UnbindSession(_cacheSystemOptions);
            }
            catch (Exception)
            {
            }
        }
        /// <summary>
        /// Reset cacs biến chung khi đóng Form
        /// </summary>
        public static void ResetVariable()
        {
            //@Form = null;
            //_isKeyPress = false;
            //Checked = true;
            StopWatch.Stop();
            StopWatch.Reset();
        }
        /// <summary>
        /// Hàm check tồn tại của chứng từ
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="this"></param>
        /// <param name="select"></param>
        /// <returns></returns>
        public static bool CheckExistVoucher<T>(this Form @this, T @select)
        {
            bool check = false;
            PropertyInfo propId = @select.GetType().GetProperty("ID");
            if (propId != null && propId.CanWrite)
            {
                var id = (Guid?)propId.GetValue(@select, null);
                if (id != null)
                {
                    BaseService<T, Guid> services = @this.GetIService(@select);
                    T temp = services.Getbykey((Guid)id);
                    if (temp != null)
                    {
                        services.UnbindSession(temp);
                        temp = services.Getbykey((Guid)id);
                        if (temp != null)
                            check = true;
                        services.UnbindSession(temp);
                    }
                }
            }
            if (!check)
            {
                MSG.Warning(resSystem.MSG_Error_13);
            }
            return check;
        }
        /// <summary>
        /// Check sự tồn tại của một đối tượng trong CSDL theo ID 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="this"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool CheckExistObject<T>(this Form @this, Guid id)
        {
            bool check = false;
            try
            {
                BaseService<T, Guid> services = @this.GetIService<T>();
                T temp = services.Getbykey(id);
                services.UnbindSession(temp);
                temp = services.Getbykey(id);
                if (temp != null)
                    check = true;
            }
            catch (Exception)
            {
                check = false;
            }
            return check;
        }
        /// <summary>
        /// Hàm Sắp xếp và gắn TabIndex cho control form theo thứ tự tọa độ
        /// </summary>
        /// <param name="container">Form</param>
        /// <param name="beginTabIndex">TabIndex bắt đầu</param>
        public static void SortFormControls(this Control container, int beginTabIndex)
        {
            if (container is Form)
            {
                container.TabIndex = 0;
            }
            else
            {
                container.TabIndex = beginTabIndex;
                beginTabIndex++;
            }
            var controls = new BindingList<Control>();
            foreach (Control control in container.Controls)
            {
                controls.Add(control);
            }
            var sort = controls.OrderBy(p => p.Location.Y).ThenBy(p => p.Location.X);
            foreach (Control control in sort)
            {
                if (control is UltraTextEditor || control is UltraCombo || control is UltraOptionSet_Ex || control is UltraDateTimeEditor || control is UltraGrid && !string.IsNullOrEmpty(control.Name))
                {
                    control.TabIndex = beginTabIndex;
                    beginTabIndex++;
                }
                else if (control.HasChildren)
                    control.SortFormControls(beginTabIndex);
            }
        }
        /// <summary>
        /// Hàm Sắp xếp và gắn TabIndex cho control form theo thứ tự 
        /// </summary>
        /// <param name="container">Form</param>
        public static void SortFormControls(this Control container)
        {
            if (container is Form)
            {
                if (!container.Exists(typeof(SMcMaster.TabSchemeProvider)))
                {
                    var tabSchemeProvider = new SMcMaster.TabSchemeProvider();
                    tabSchemeProvider.SetTabScheme(container, SMcMaster.TabOrderManager.TabScheme.AcrossFirst);
                }
            }
            foreach (Control control in container.Controls)
            {
                SMcMaster.TabOrderManager.TabScheme scheme = SMcMaster.TabOrderManager.TabScheme.AcrossFirst;
                var tom = new SMcMaster.TabOrderManager(control);
                tom.SetTabOrder(scheme);
                if (control.HasChildren)
                    control.SortFormControls();
            }
        }
        /// <summary>
        /// Hàm kiểm tra có tồn tại kiểu Control này trong Form không
        /// </summary>
        /// <param name="this">Form</param>
        /// <param name="type">Kiểu Control cần kiểm tra</param>
        /// <returns></returns>
        public static bool Exists(this Control @this, System.Type type)
        {
            bool status = false;
            foreach (var control in @this.Controls)
            {
                if (control.GetType() == type)
                    status = true;
                else if (((Control)control).HasChildren)
                    if (((Control)control).Exists(type))
                        status = true;
            }
            return status;
        }
        /// <summary>
        /// [Huy Anh] Gán giá trị cho Tag
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="control"></param>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        public static void SetValueForTag<T>(this Control control, string key, T defaultValue)
        {
            if (control.Tag != null)
            {
                if (((Dictionary<string, object>)control.Tag).Any(p => p.Key.Equals(key)))
                    ((Dictionary<string, object>)control.Tag).Remove(key);
                ((Dictionary<string, object>)control.Tag).Add(key, defaultValue);
            }
            else
            {
                control.Tag = new Dictionary<string, object> { { key, defaultValue } };
            }
        }
        /// <summary>
        /// [Huy Anh] Gán giá trị cho Tag
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cell"></param>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        public static void SetValueForTag<T>(this UltraGridCell cell, string key, T defaultValue)
        {
            if (cell.Tag != null)
            {
                if (((Dictionary<string, object>)cell.Tag).Any(p => p.Key.Equals(key)))
                    ((Dictionary<string, object>)cell.Tag).Remove(key);
                ((Dictionary<string, object>)cell.Tag).Add(key, defaultValue);
            }
            else
            {
                cell.Tag = new Dictionary<string, object>() { { key, defaultValue } };
            }
        }
        /// <summary>
        /// [Huy Anh] Lấy giá trị từ Tag
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="control"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static T? GetValueFromTag<T>(this Control control, string key) where T : struct
        {
            T? result = null;
            if (control.ExistsTag(key))
            {
                var val = ((Dictionary<string, object>)control.Tag).FirstOrDefault(p => p.Key.Equals(key)).Value;
                result = (T?)val;
            }
            return result;
        }
        /// <summary>
        /// [Huy Anh] Lấy giá trị từ Tag
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cell"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static T? GetValueFromTag<T>(this UltraGridCell cell, string key) where T : struct
        {
            T? result = null;
            if (cell.ExistsTag(key))
            {
                var val = ((Dictionary<string, object>)cell.Tag).FirstOrDefault(p => p.Key.Equals(key)).Value;
                result = (T?)val;
            }
            return result;
        }
        /// <summary>
        /// [Huy Anh] Kiểm tra Tag có tồn tại không
        /// </summary>
        /// <param name="control"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool ExistsTag(this Control control, string key)
        {
            bool result = false;
            if (control.Tag != null)
            {
                var dictionary = control.Tag as Dictionary<string, object>;
                if (dictionary != null)
                    if ((dictionary).Any(p => p.Key.Equals(key)))
                        result = true;
            }
            return result;
        }
        /// <summary>
        /// [Huy Anh] Kiểm tra Tag có tồn tại không
        /// </summary>
        /// <param name="cell"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool ExistsTag(this UltraGridCell cell, string key)
        {
            bool result = false;
            if (cell.Tag != null)
            {
                var dictionary = cell.Tag as Dictionary<string, object>;
                if (dictionary != null)
                    if ((dictionary).Any(p => p.Key.Equals(key)))
                        result = true;
            }
            return result;
        }
        //public static void AddCachesForm(this Form @this)
        //{
        //    bool any = Forms.Any(x => x.Key == @this.Name);
        //    if (!any)
        //        Forms.Add(@this.Name, @this);
        //}

        //public static Form GetFormFromCache(string name)
        //{
        //    return Forms.Any(x => x.Key == name) ? Forms.FirstOrDefault(x => x.Key == name).Value : null;
        //}

        public static decimal GetMonthlyDepreciationAmount(DateTime from, DateTime to, decimal depreciationAmount, decimal monthDepreciationRate)
        {
            decimal result = 0;
            if (from.Month <= to.Month && from.Year == to.Year || from.Year < to.Year)
            {
                if ((from.Day > 1 && to.Day == new DateTime(from.Year, from.Month,
                                                          DateTime.DaysInMonth(from.Year, from.Month)).Day && from.Year == to.Year && from.Month == to.Month) ||
                    (from.Day > 1 && from.Month < to.Month && from.Year == to.Year) ||
                    (from.Day > 1 && from.Year < to.Year))
                {
                    //Nếu ngày tính khấu hao ko tròn tháng:
                    //Giá trị tính khâu hao = Giá trị tính KH tháng * Tỷ lệ KH tháng * (Tổng số ngày tính KH / Tổng số ngày trong kì tính KH)
                    //TH 1: Từ ko phải ngày đầu tiên của tháng => đến cuối tháng trong cùng 1 tháng và cùng 1 năm
                    //TH 2: Từ ko phải ngày đầu tiên của tháng => tháng đến lớn hơn tháng hiện tại và cùng 1 năm
                    //TH 3: Từ ko phải ngày đầu tiên của tháng => năm đến lớn hơn năm hiện tại
                    result += depreciationAmount * monthDepreciationRate *
                              ((new DateDiff(from,
                                             new DateTime(from.Year, from.Month,
                                                          DateTime.DaysInMonth(from.Year, from.Month)))).Days /
                               DateTime.DaysInMonth(from.Year, from.Month));
                }
                else if ((from.Day == 1 && to.Day < new DateTime(from.Year, from.Month,
                                                          DateTime.DaysInMonth(from.Year, from.Month)).Day && from.Year == to.Year && from.Month == to.Month) ||
                    (from.Day > 1 && to.Day < new DateTime(from.Year, from.Month,
                                                          DateTime.DaysInMonth(from.Year, from.Month)).Day && from.Year == to.Year && from.Month == to.Month))
                {
                    //TH 4: Từ ngày đầu tiên của tháng => đến ko phải ngày cuối tháng trong cùng 1 tháng và cùng 1 năm
                    //TH 5: Từ ko phải ngày đầu tiên của tháng đến ko phải ngày cuối tháng trong cùng 1 tháng và cùng 1 năm
                    result += depreciationAmount * monthDepreciationRate *
                              ((new DateDiff(from, to)).Days /
                               DateTime.DaysInMonth(from.Year, from.Month));
                }
                else
                {
                    //Nếu ngày tính khấu hao tròn tháng:
                    //Giá trị tính khâu hao = Giá trị tính KH tháng * Tỷ lệ KH tháng
                    result += depreciationAmount * monthDepreciationRate;
                }
                if ((from.AddMonths(1).Month <= to.Month && from.AddMonths(1).Year == to.Year) ||
                    (from.AddMonths(1).Year < to.Year))
                    result +=
                        GetMonthlyDepreciationAmount(
                            new DateTime(from.AddMonths(1).Year, from.AddMonths(1).Month, 1), to, depreciationAmount,
                            monthDepreciationRate);
            }
            return result;
        }

        public static void ReloadCombosInGridByObject<T>(this UltraGrid uGrid)
        {
            foreach (UltraGridRow row in uGrid.Rows)
            {
                foreach (UltraGridCell cell in row.Cells)
                {
                    if (cell.ValueListResolved != null)
                    {
                        var combo = (UltraCombo)cell.ValueListResolved;
                        if (combo.DataSource is BindingList<T>)
                            combo.ClearAutoComplete();
                    }
                }
            }
        }

        public static void ReloadCombosInFormByObject<T>(this Control @this)
        {
            foreach (Control control in @this.Controls)
            {
                if (control is UltraCombo)
                {
                    var combo = (UltraCombo)control;
                    if (combo.DataSource is BindingList<T>)
                        combo.ClearAutoComplete();
                }
                else if (control is UltraGrid)
                {
                    ((UltraGrid)control).ReloadCombosInGridByObject<T>();
                }
                if (control.HasChildren)
                    control.ReloadCombosInFormByObject<T>();
            }
        }

        public static bool SaveLedger<T>(T input)
        {
            var status = IGeneralLedgerService.SaveLedger(input);
            if (status)
            {
                if (input.HasProperty("TypeID"))
                {
                    var typeId = (int)input.GetProperty("TypeID");
                    if (typesUpdateRepositoryLedger.Any(x => x == typeId))
                    {
                        ClearCacheByType<MaterialGoodsCustom>();
                        var a = ListMaterialGoodsCustom;
                        if (Form.ActiveForm != null && !(Form.ActiveForm is FMain))
                            Form.ActiveForm.ReloadCombosInFormByObject<MaterialGoodsCustom>();
                    }
                }
            }
            return status;
        }



        public static bool SaveLedgerNoTran<T>(T input)
        {
            var status = IGeneralLedgerService.SaveLedgerNoTran(input);
            if (status)
            {
                if (input.HasProperty("TypeID"))
                {
                    var typeId = (int)input.GetProperty("TypeID");
                    if (typesUpdateRepositoryLedger.Any(x => x == typeId))
                    {
                        ClearCacheByType<MaterialGoodsCustom>();
                        var a = ListMaterialGoodsCustom;
                        if (Form.ActiveForm != null && !(Form.ActiveForm is FMain))
                            Form.ActiveForm.ReloadCombosInFormByObject<MaterialGoodsCustom>();
                    }
                }
            }
            return status;
        }

        public static bool SaveLedger<T1, T2>(T1 input1, T2 input2)
        {
            var status = IGeneralLedgerService.SaveLedger(input1, input2);
            if (status)
            {
                if (input1.HasProperty("TypeID"))
                {
                    var typeId = (int)input1.GetProperty("TypeID");
                    if (typesUpdateRepositoryLedger.Any(x => x == typeId))
                    {
                        ClearCacheByType<MaterialGoodsCustom>();
                        var a = ListMaterialGoodsCustom;
                    }
                }
                else if (input2.HasProperty("TypeID"))
                {
                    var typeId = (int)input2.GetProperty("TypeID");
                    if (typesUpdateRepositoryLedger.Any(x => x == typeId))
                    {
                        ClearCacheByType<MaterialGoodsCustom>();
                        var a = ListMaterialGoodsCustom;
                    }
                }
            }
            return status;
        }

        public static bool SaveLedgerNoTran<T1, T2>(T1 input1, T2 input2)
        {
            var status = IGeneralLedgerService.SaveLedgerNoTran(input1, input2);
            if (status)
            {
                if (input1.HasProperty("TypeID"))
                {
                    var typeId = (int)input1.GetProperty("TypeID");
                    if (typesUpdateRepositoryLedger.Any(x => x == typeId))
                    {
                        ClearCacheByType<MaterialGoodsCustom>();
                        var a = ListMaterialGoodsCustom;
                    }
                }
                else if (input2.HasProperty("TypeID"))
                {
                    var typeId = (int)input2.GetProperty("TypeID");
                    if (typesUpdateRepositoryLedger.Any(x => x == typeId))
                    {
                        ClearCacheByType<MaterialGoodsCustom>();
                        var a = ListMaterialGoodsCustom;
                    }
                }
            }
            return status;
        }

        public static bool RemoveLedger<T>(T input)
        {
            var status = IGeneralLedgerService.RemoveLedger(input);
            if (status)
            {
                if (input.HasProperty("TypeID"))
                {
                    var typeId = (int)input.GetProperty("TypeID");
                    if (typesUpdateRepositoryLedger.Any(x => x == typeId))
                    {
                        ClearCacheByType<MaterialGoodsCustom>();
                        var a = ListMaterialGoodsCustom;
                    }
                }
            }
            return status;
        }

        public static bool RemoveLedgerNoTran<T>(T input)
        {
            var status = IGeneralLedgerService.RemoveLedgerNoTran(input);
            if (status)
            {
                if (input.HasProperty("TypeID"))
                {
                    var typeId = (int)input.GetProperty("TypeID");
                    if (typesUpdateRepositoryLedger.Any(x => x == typeId))
                    {
                        ClearCacheByType<MaterialGoodsCustom>();
                        var a = ListMaterialGoodsCustom;
                    }
                }
            }
            return status;
        }

        public static bool RemoveLedger<T1, T2>(T1 input1, T2 input2)
        {
            var status = IGeneralLedgerService.RemoveLedger(input1, input2);
            if (status)
            {
                if (input1.HasProperty("TypeID"))
                {
                    var typeId = (int)input1.GetProperty("TypeID");
                    if (typesUpdateRepositoryLedger.Any(x => x == typeId))
                    {
                        ClearCacheByType<MaterialGoodsCustom>();
                        var a = ListMaterialGoodsCustom;
                    }
                }
                else if (input2.HasProperty("TypeID"))
                {
                    var typeId = (int)input2.GetProperty("TypeID");
                    if (typesUpdateRepositoryLedger.Any(x => x == typeId))
                    {
                        ClearCacheByType<MaterialGoodsCustom>();
                        var a = ListMaterialGoodsCustom;
                    }
                }
            }
            return status;
        }

        public static bool RemoveLedgerNoTran<T1, T2>(T1 input1, T2 input2)
        {
            var status = IGeneralLedgerService.RemoveLedgerNoTran(input1, input2);
            if (status)
            {
                if (input1.HasProperty("TypeID"))
                {
                    var typeId = (int)input1.GetProperty("TypeID");
                    if (typesUpdateRepositoryLedger.Any(x => x == typeId))
                    {
                        ClearCacheByType<MaterialGoodsCustom>();
                        var a = ListMaterialGoodsCustom;
                    }
                }
                else if (input2.HasProperty("TypeID"))
                {
                    var typeId = (int)input2.GetProperty("TypeID");
                    if (typesUpdateRepositoryLedger.Any(x => x == typeId))
                    {
                        ClearCacheByType<MaterialGoodsCustom>();
                        var a = ListMaterialGoodsCustom;
                    }
                }
            }
            return status;
        }

        public static UTopVouchers<T> FirstOrDefaultTopVouchers<T>(this Control @this)
        {
            UTopVouchers<T> topVoucher = null;
            foreach (Control control in @this.Controls)
            {
                if (control is UTopVouchers<T>)
                    topVoucher = (UTopVouchers<T>)control;
                else if (control.HasChildren)
                    topVoucher = control.FirstOrDefaultTopVouchers<T>();
            }
            return topVoucher;
        }
        public static UltraTextEditor FirstOrDefaultTextBoxNo<T>(this Control @this)
        {
            UltraTextEditor txt = null;
            foreach (Control control in @this.Controls)
            {
                if (control is UltraTextEditor && control.DataBindings.Count > 0 && control.DataBindings[0].DataSource.GetType().Name == typeof(T).Name && control.DataBindings[0].BindingMemberInfo.BindingMember == "No")
                    txt = (UltraTextEditor)control;
                else if (control.HasChildren)
                {
                    txt = control.FirstOrDefaultTextBoxNo<T>();
                    if (txt != null)
                        return txt;
                }
            }
            return txt;
        }

        /// <summary>
        /// Cap nhat lai Template cho Form
        /// </summary>
        /// <param name="this"> </param>
        /// <param name="templateId"></param>
        /// <param name="isNew">Lấy mới Template từ CSDL ko qua cache</param>
        /// <param name="isBusiness"> </param>
        /// <param name="isStandard"> </param>
        /// <param name="blackStandList">DS type cua DataSource của grid không phải là grid cho thêm dòng </param>
        public static void ReloadTemplate<TK>(this Form @this, Guid templateId, bool isNew = true, bool isBusiness = true, bool isStandard = true, System.Type[] blackStandList = null)
        {
            if (!(@this is DetailBase<TK>)) return;
            var fbase = (DetailBase<TK>)@this;
            fbase._templateID = templateId;
            var select = fbase._select;
            PropertyInfo propTemplate = select.GetType().GetProperty("TemplateID");
            if (propTemplate != null && propTemplate.CanWrite)
                propTemplate.SetValue(select, fbase._templateID, null);
            Template template = isNew ? GetTemplateInDatabase(fbase._templateID, fbase.TypeID) : GetMauGiaoDien(fbase.TypeID, fbase._templateID);
            List<TemplateDetail> tabs =
            template.TemplateDetails.Where(p => p.TabIndex != 100).OrderBy(p => p.TabIndex).ToList();
            //UltraPanel panel = null;
            //if (tabs.Count > 1)
            //{
            //    var ultraTabControl =
            //        (UltraTabControl)@this.Controls.Find("ultraTabControl", true).FirstOrDefault();
            //    if (ultraTabControl != null)
            //        panel = (UltraPanel)ultraTabControl.Parent.Parent;
            //}
            //else
            //{
            //    var uGrid = (UltraGrid)@this.Controls.Find("uGrid0", true).FirstOrDefault();
            //    if (uGrid != null) panel = (UltraPanel)uGrid.Parent.Parent;
            //}
            //if (panel == null) return;
            //@this.ConfigGridByTemplete_General<TK>(panel, mauGiaoDien);
            for (int i = 0; i < tabs.Count; i++)
            {
                var uGrid = (UltraGrid)@this.Controls.Find(string.Format("uGrid{0}", i), true).FirstOrDefault();
                if (uGrid == null) continue;
                var firstOrDefault = template.TemplateDetails.FirstOrDefault(k => k.TabIndex == i);
                if (firstOrDefault != null)
                {
                    var templateColumns =
                        firstOrDefault.TemplateColumns;

                    #region Cấu hình giao diện cho Grid
                    if (blackStandList != null && blackStandList.Any(x => x == uGrid.DataSource.GetType()))
                        ConfigGrid(uGrid, "", templateColumns, isBusiness, false);
                    else
                        ConfigGrid(uGrid, "", templateColumns, isBusiness, isStandard);
                    if (select.HasProperty("CurrencyID"))
                    {
                        var currencyId = select.GetProperty<TK, string>("CurrencyID");
                        foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
                        {
                            if (column.Key.Contains("Amount") && !column.Key.Contains("Original"))
                                uGrid.ConfigColumnByCurrency(column.Key, currencyId);
                        }
                    }
                    uGrid.ConfigSummaryOfGrid();

                    #endregion
                }

                #region TH là Nhập khẩu
                if (fbase._select.HasProperty("IsImportPurchase"))
                {
                    if (fbase._select.GetProperty<TK, bool?>("IsImportPurchase") == true)
                    {
                        uGrid.ConfigColumnByImportPurchase(true);
                    }
                }
                #endregion
            }
            //@this.ConfigGridByTemplete<TK>(fbase.TypeID, mauGiaoDien, true, fbase._listObjectInput);
            if (fbase.uGridControl != null)
            {
                //T backup = (T) (uGridControl.DataSource as List<T>)[0];
                bool have = false;
                if (fbase.uGridControl.DisplayLayout.Bands[0].Columns.Exists("CurrencyID") && fbase.uGridControl.DisplayLayout.Bands[0].Columns.Exists("TotalAmount"))
                {
                    if (!fbase.uGridControl.Rows[0].Cells["CurrencyID"].Value.Equals("VND") && !fbase.uGridControl.DisplayLayout.Bands[0].Columns["TotalAmount"].Hidden)
                        have = true;
                }
                //var input = fbase.uGridControl.DataSource as List<TK>;
                //@this.ConfigGridCurrencyByTemplate(fbase.TypeID, new List<System.Collections.IList> { input }, fbase.uGridControl, template);
                var firstOrDefault = template.TemplateDetails.FirstOrDefault(k => k.TabIndex == 100);
                if (firstOrDefault != null)
                    ConfigGrid(fbase.uGridControl, "", firstOrDefault.TemplateColumns, true, false, isHaveSum: false);
                Currency currency = null;
                if (fbase.uGridControl.DisplayLayout.Bands[0].Columns.Exists("CurrencyID")) currency = ListCurrency.FirstOrDefault(
                      p => p.ID.Equals(fbase.uGridControl.Rows[0].Cells["CurrencyID"].Value));
                else if (select.HasProperty("CurrencyID"))
                {
                    currency = ListCurrency.FirstOrDefault(
                         p => p.ID.Equals(select.GetProperty<TK, string>("CurrencyID")));
                }
                //@this.AutoExchangeByCurrencySelected(currency);
                foreach (UltraGridColumn column in fbase.uGridControl.DisplayLayout.Bands[0].Columns)
                {
                    if ((column.Key.Contains("Amount") && !column.Key.Contains("Original") && have) || column.Key.Equals("ExchangeRate"))
                        if (currency != null) fbase.uGridControl.ConfigColumnByCurrency(column.Key, currency.ID);
                }
                fbase.uGridControl.ConfigSizeGridCurrency(@this);
            }
            else
            {
                foreach (TemplateDetail detail in tabs)
                {
                    UltraGrid uGrid = (UltraGrid)@this.Controls.Find(string.Format("uGrid{0}", detail.TabIndex), true).FirstOrDefault();
                    if (uGrid != null && uGrid.DisplayLayout.Bands[0].Columns.Exists("CurrencyID"))
                    {
                        foreach (UltraGridColumn column in uGrid.DisplayLayout.Bands[0].Columns)
                        {
                            if ((column.Key.Contains("Amount") && !column.Key.Contains("Original")) || column.Key.Equals("ExchangeRate"))
                                uGrid.ConfigColumnByCurrency(column.Key, null);
                        }
                    }
                }
            }
        }

        public static void ConfigTemplateEdit<TK>(this Form @this, UltraToolbarsManager toolMgr, int typeId, int statusForm, Guid? templateId)
        {
            if (statusForm != ConstFrm.optStatusForm.View)
            {
                var popupMenuTool =
                    (PopupMenuTool)toolMgr.Tools["mnpopTemplate"];
                if (popupMenuTool != null)
                {
                    while (popupMenuTool.Tools.Count != 0)
                    {
                        toolMgr.Tools.Remove(popupMenuTool.Tools[0]);
                    }
                    var templates = ITemplateService.getTemplateinTypeForm(typeId);
                    bool exist = false;
                    if (templateId != null)
                        exist = @this.CheckExistObject<Template>((Guid)templateId);
                    foreach (var template in templates)
                    {
                        if (template.IsDefault != null && (!(bool)template.IsDefault || !template.IsSecurity))
                        {
                            var stateButtonTool = new StateButtonTool(string.Format("mnbtnTemplate_{0}", template.ID), "");
                            stateButtonTool.SharedPropsInternal.Caption = template.TemplateName;
                            stateButtonTool.SharedPropsInternal.DisplayStyle = ToolDisplayStyle.TextOnlyInMenus;
                            stateButtonTool.Checked = templateId == null || (!exist) ? (template.IsDefault == false && template.IsSecurity) : template.ID.Equals(templateId);
                            ((DetailBase<TK>)@this).AddTools(toolMgr, popupMenuTool.Tools, stateButtonTool);
                        }
                    }
                    if (!popupMenuTool.Tools.Exists("mnbtnTemplateEdit"))
                    {
                        ButtonTool btnTemplateEdit = new ButtonTool("mnbtnTemplateEdit");
                        btnTemplateEdit.SharedPropsInternal.Caption = @"Thay đổi mẫu";
                        btnTemplateEdit.SharedPropsInternal.DisplayStyle = ToolDisplayStyle.TextOnlyInMenus;
                        ((DetailBase<TK>)@this).AddTools(toolMgr, popupMenuTool.Tools, btnTemplateEdit);
                    }
                    // button cuối cùng là bắt đầu 1 group
                    popupMenuTool.Tools[popupMenuTool.Tools.Count - 1].InstanceProps.IsFirstInGroup = true;
                    popupMenuTool.Tools[popupMenuTool.Tools.Count - 1].CustomizedIsFirstInGroup = DefaultableBoolean.True;
                }
            }
        }

        public static void ReconfigAccountColumn<TK>(this Form @this, int typeId)
        {
            var tabControl = @this.Controls.Find("ultraTabControl", true).FirstOrDefault() as UltraTabControl;
            if (tabControl != null)
            {
                for (int i = 0; i < tabControl.Tabs.Count; i++)
                {
                    var uGrid = @this.Controls.Find(string.Format("uGrid{0}", i), true).FirstOrDefault() as UltraGrid;
                    if (uGrid != null)
                    {
                        foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
                        {
                            if (@column.Key.Contains("Account") && !@column.Key.Contains("BankAccount") &&
                                     !@column.Key.Contains("AccountingObject")) //tài khoản nợ, tài khoản có
                            {
                                @this.ConfigAccountColumnInGrid<TK>(typeId, uGrid, column);
                            }
                        }
                    }
                }
            }
            else
            {
                var uGrid = @this.Controls.Find("uGrid0", true).FirstOrDefault() as UltraGrid;
                if (uGrid != null)
                {
                    foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
                    {
                        if (@column.Key.Contains("Account") && !@column.Key.Contains("BankAccount") &&
                                 !@column.Key.Contains("AccountingObject")) //tài khoản nợ, tài khoản có
                        {
                            @this.ConfigAccountColumnInGrid<TK>(typeId, uGrid, column);
                        }
                    }
                }
            }
        }

        public static void ConfigColumnByImportPurchase(this UltraGrid ultraGrid, bool isImportPurchase)
        {
            var band = ultraGrid.DisplayLayout.Bands[0];
            if (band.Columns.Exists("VATRate") && !band.Columns["VATRate"].Hidden &&
                band.Columns.Exists("ImportTaxAccount") &&
                band.Columns.Exists("ImportTaxRate") &&
                band.Columns.Exists("ImportTaxAmountOriginal") &&
                band.Columns.Exists("DeductionDebitAccount"))
            {
                band.Columns["ImportTaxAccount"].Hidden = !isImportPurchase;
                band.Columns["ImportTaxRate"].Hidden = !isImportPurchase;
                band.Columns["ImportTaxAmountOriginal"].Hidden = !isImportPurchase;
                band.Columns["DeductionDebitAccount"].Hidden = !isImportPurchase;
            }
        }

        static void ConfigAccountColumnInGrid<TK>(this Form @this, int typeId, UltraGrid @uGridInput, UltraGridColumn @column)
        {
                var @base = @this as DetailBase<TK>;
                TK @select = (TK)Activator.CreateInstance(typeof(TK));
                if (@base != null)
                {
                    @select = @base._select;
                }
                else
                {
                    var @baseBusiness = @this as BaseBusiness<TK>;

                    if (@baseBusiness != null)
                    {
                        @select = @baseBusiness._select;
                    }
                }
                bool isImportPurchase = false;
                if (@select.HasProperty("IsImportPurchase"))
                    isImportPurchase = @select.GetProperty<TK, bool?>("IsImportPurchase") ?? false;
                IList<Account> lstTemp =
                    IAccountDefaultService.GetAccountDefaultByTypeId(typeId, @column.Key, ListAccountDefault,
                                                                     ListAccount, isImportPurchase);
                var u = new UtilsNoStatic();
                Thread th = null;
                u.ConfigCbbToGrid<TK, Account>(@this, @uGridInput, @column.Key, lstTemp, "AccountNumber", "AccountNumber",
                    ConstDatabase.Account_TableName, isThread: true, thread: th);
                @column.CellAppearance.TextHAlign = HAlign.Center;
                //@column.MaxLength = 25;
        }

        public static void DeleteSelectedBorderOfUltraTabControl(this Control @this)
        {
            foreach (Control control in @this.Controls)
            {
                if (control is UltraTabControl)
                    ((UltraTabControl)control).DrawFilter = new DrawFilter();
                if (control.HasChildren)
                    control.DeleteSelectedBorderOfUltraTabControl();
            }
        }

        public static void CreateThread(ref Thread thread, Action func)
        {
            if (thread != null && thread.ThreadState == ThreadState.Running)
                thread.StopThread();
            thread = new Thread(new ThreadStart(func));
        }

        public static void StartThread(this Thread thread)
        {
            thread.IsBackground = true;
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
        }

        public static void StopThread(this Thread thread)
        {
            if (thread == null) return;
            thread.Abort();
            thread = null;
        }

        public static bool IsNumericType(this System.Type type)
        {
            switch (System.Type.GetTypeCode(type))
            {
                case TypeCode.Byte:
                case TypeCode.SByte:
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                case TypeCode.UInt64:
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.Decimal:
                case TypeCode.Double:
                case TypeCode.Single:
                    return true;
                default:
                    return false;
            }
        }
        #endregion
        #endregion
    }
}
