﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using Accounting.Core.Domain;
using FX.Core;
using FX.Data;
using Infragistics.Win.UltraWinGrid;
using Type = System.Type;

namespace Accounting
{
    /// <summary>
    /// [Kiendd]
    /// Reflection utility functions
    /// </summary>
    public static class ReflectionUtils
    {
        /// <summary>
        /// Các giá trị Binding Flags
        /// </summary>
        public const BindingFlags MemberAccess =
            BindingFlags.Public | BindingFlags.NonPublic |
            BindingFlags.Static | BindingFlags.Instance | BindingFlags.IgnoreCase;

        /// <summary>
        /// [kiendd]
        /// Lấy Value của một property trong một Object theo propertyName
        /// </summary>
        /// <param name="instance">Object</param>
        /// <param name="property">Tên property</param>
        /// <returns>Object - kiểu của property (lỗi thì trả về null)</returns>
        public static TK GetProperty<T, TK>(this T instance, string property)
        {
            try
            {
                var gt = instance.GetType().GetProperty(property, MemberAccess);
                if (gt == null)
                    return default(TK);
                var temp = (TK)instance.GetType().GetProperty(property, MemberAccess).GetValue(instance, null);
                return temp;
            }
            catch (Exception ex)
            {
                return default(TK);
            }
        }
        public static object GetProperty<T>(this T instance, string property)
        {
            try
            {
                return instance.GetType().GetProperty(property, MemberAccess).GetValue(instance, null);
            }
            catch (Exception)
            {
                return default(object);
            }
        }

        /// <summary>
        /// [kiendd]
        /// Gán Value cho một property trong một Object theo propertyName
        /// </summary>
        /// <param name="instance">Object</param>
        /// <param name="property">Tên property</param>
        /// <param name="value">value</param>
        /// <returns>Object - kiểu của property (lỗi thì trả về null)</returns>
        public static void SetProperty<T>(this T instance, string property, object value)
        {
            try
            {
                var prop = instance.GetType().GetProperty(property, BindingFlags.Public | BindingFlags.Instance);
                if (null != prop && prop.CanWrite) prop.SetValue(instance, value, null);
            }
            catch { }
        }

        /// <summary>
        /// [kiendd]
        /// Check propertyName có trong object không?
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static bool HasProperty(this object obj, string propertyName)
        {
            return obj.GetType().GetProperty(propertyName) != null;
        }

        /// <summary>
        /// Sao chép các value của A1 sang A2 (cùng một kiểu đối tượng A)
        /// </summary>
        /// <param name="instanceFrom"></param>
        /// <param name="instanceTo"></param>
        /// <returns></returns>
        public static T Copy<T>(T instanceFrom, T instanceTo)
        {
            if (instanceFrom == null) throw new ArgumentNullException("instanceFrom", "đối tượng copy null");
            if (instanceTo == null) throw new ArgumentNullException("instanceTo", "đối tượng trả về null");

            ////lấy danh sách các property đối tượng đích
            //PropertyInfo[] properties = instanceTo.GetType().GetProperties();
            ////duyệt danh sách, thực hiện cập nhật value nếu đối tượng kia cũng có property có tên tương ứng
            //foreach (PropertyInfo propertyInfo in properties)
            //{
            //    //
            //}


            //if (ReferenceEquals(instanceFrom, instanceTo))  //cùng kiểu thì thực hiện
            {
                const BindingFlags flags = BindingFlags.Public | BindingFlags.Instance | BindingFlags.GetProperty |
                                           BindingFlags.SetProperty;
                PropertyInfo[] properties = instanceTo.GetType().GetProperties();
                Dictionary<string, PropertyInfo> toProperties = properties.ToDictionary(property => property.Name);
                properties = instanceFrom.GetType().GetProperties(flags);
                foreach (PropertyInfo fromProperty in properties)
                {
                    if (!toProperties.ContainsKey(fromProperty.Name)) continue;
                    PropertyInfo toProperty = toProperties[fromProperty.Name];
                    if (toProperty.PropertyType != fromProperty.PropertyType) continue;
                    object value = fromProperty.GetValue(instanceFrom, null);
                    toProperty.SetValue(instanceTo, value, null);
                }
            }

            return instanceTo;
        }

        /// <summary>
        /// [kiendd]
        /// Copy một object A sang một Object B (trùng property thì copy value)
        /// </summary>
        /// <param name="fromType"></param>
        /// <param name="from"></param>
        /// <param name="toType"></param>
        /// <param name="to"></param>
        public static void Copy(Type fromType, object from, Type toType, object to, List<string> blackList = null)
        {
            if (blackList == null) blackList = new List<string>();
            if (fromType == null)
                throw new ArgumentNullException("fromType", "The type that you are copying from cannot be null");

            if (from == null)
                throw new ArgumentNullException("from", "The object you are copying from cannot be null");

            if (toType == null)
                throw new ArgumentNullException("toType", "The type that you are copying to cannot be null");

            if (to == null)
                throw new ArgumentNullException("to", "The object you are copying to cannot be null");

            if (!ReferenceEquals(from, to))
            {
                const BindingFlags flags = BindingFlags.Public | BindingFlags.Instance | BindingFlags.GetProperty |
                                           BindingFlags.SetProperty;
                Dictionary<string, PropertyInfo> toProperties = new Dictionary<string, PropertyInfo>();
                PropertyInfo[] properties = toType.GetProperties(flags);
                foreach (PropertyInfo property in properties)
                {
                    toProperties.Add(property.Name, property);
                }

                properties = fromType.GetProperties(flags);
                try
                {
                    foreach (PropertyInfo fromProperty in properties)
                    {
                        if (toProperties.ContainsKey(fromProperty.Name) && !blackList.Contains(fromProperty.Name))
                        {
                            PropertyInfo toProperty = toProperties[fromProperty.Name];
                            if (toProperty.PropertyType == fromProperty.PropertyType && toProperty.CanRead && toProperty.CanWrite)// H.A sửa thêm .CanRead và .CanWrite
                            {
                                object value = fromProperty.GetValue(from, null);
                                toProperty.SetValue(to, value, null);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                    throw;
                }
            }
        }

        /// <summary>
        /// Sắp xếp danh sách theo property
        /// [kiendd]
        /// [H.A] sửa thành TH sắp xếp lớn => bé theo 2 thuộc tính
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="input"></param>
        /// <param name="property1"></param>
        /// <param name="property2"> </param>
        /// <returns></returns>
        public static List<T> OrderByStand<T>(this List<T> input, string property1 = "", string property2 = "")
        {
            if (string.IsNullOrEmpty(property1)) return input;
            T temp = (T)Activator.CreateInstance(typeof(T));
            if (temp.HasProperty(property1)) input = (!string.IsNullOrEmpty(property2) && temp.HasProperty(property2)) ? input.OrderByDescending(k => k.GetType().GetProperty(property1).GetValue(k, null)).ThenByDescending(k => k.GetType().GetProperty(property2).GetValue(k, null)).ToList() : input.OrderByDescending(k => k.GetType().GetProperty(property1).GetValue(k, null)).ToList();
            return input;
        }
        public static IList<T> OrderByStand<T>(this IList<T> input, string property1 = "", string property2 = "")
        {
            if (string.IsNullOrEmpty(property1)) return input;
            T temp = (T)Activator.CreateInstance(typeof(T));
            if (temp.HasProperty(property1)) input = (!string.IsNullOrEmpty(property2) && temp.HasProperty(property2)) ? input.OrderByDescending(k => k.GetType().GetProperty(property1).GetValue(k, null)).ThenByDescending(k => k.GetType().GetProperty(property2).GetValue(k, null)).ToList() : input.OrderByDescending(k => k.GetType().GetProperty(property1).GetValue(k, null)).ToList();
            return input;
        }
        public static List<T> OrderByStand<T>(this List<T> input, string property1 = "")
        {
            if (string.IsNullOrEmpty(property1)) return input;
            T temp = (T)Activator.CreateInstance(typeof(T));
            if (temp.HasProperty(property1)) input = input.OrderBy(k => k.GetType().GetProperty(property1).GetValue(k, null)).ToList();
            return input;
        }

        /// <summary>
        /// Kiểm tra chứng từ có tồn tại không?
        /// [kiendd]
        /// TRUE: không tồn tại (Default), FALSE: có tồn tại
        /// </summary>
        /// <returns>TRUE: không tồn tại (Default), FALSE: có tồn tại</returns>
        public static bool IsVocherExit<T>(this Form frm, T input)
        {
            try
            {
                return frm.GetIServiceStand<T, Guid>().Getbykey(input.GetProperty<T, Guid>("ID")) == null;
            }
            catch { }
            return true;
        }

        /// <summary>
        /// Check chi tiết theo 
        /// </summary>
        /// <returns></returns>
        public static bool CheckDetailTypeAccount<T, TK>(T _select, TK _selectItem, Account account, out string msg)
        {
            msg = string.Empty;
            if (account == null) return true;   //không có account để check chi tiết theo thì không check
            if (_select.GetProperty<T, int>("TypeID") == 660 || _select.GetProperty<T, int>("TypeID") == 601) return true;
            string detailType = account.DetailType;
            if (string.IsNullOrEmpty(detailType) || detailType.Equals("-1")) return true;   //không chi tiết theo thì không check
            string[] lstDetailType = detailType.Split(';');
            foreach (string item in lstDetailType)
            {
                switch (item.Trim())
                {
                    case "0":   //Đối tượng - Nhà cung cấp
                    case "1":   //Đối tượng - Khách hàng
                    case "2":   //Đối tượng - Nhân viên
                        {
                            if (typeof(T) != typeof(GOtherVoucher))
                            {
                                if (!_select.HasProperty(item.Trim().Equals("2") ? "EmployeeID" : "AccountingObjectID"))
                                    break;
                                Guid tGuid = _select.GetProperty<T, Guid>(/*item.Trim().Equals("2") ? "EmployeeID" :*/ "AccountingObjectID");
                                if (tGuid == null || tGuid == Guid.Empty)
                                {
                                    msg = string.Format("Tài khoản {0} chi tiết theo {1}", account.AccountNumber, item.Trim().Equals("2") ? "nhân viên" : "đối tượng");
                                    return false;
                                }
                                else
                                {
                                    //AccountingObject accountingObject = Utils.IAccountingObjectService.Getbykey(tGuid).CloneObject
                                    AccountingObject accountingObject = Utils.ListAccountingObject.FirstOrDefault(o => o.ID == tGuid);
                                    if (accountingObject == null) continue;
                                    if (item.Trim().Equals("1") && accountingObject.ObjectType != 0 && accountingObject.ObjectType != 2)
                                    {
                                        msg = string.Format("Tài khoản {0} chi tiết theo Đối tượng - Khách hàng", account.AccountNumber);
                                        return false;
                                    }
                                    else if (item.Trim().Equals("0") && accountingObject.ObjectType != 1 && accountingObject.ObjectType != 2)
                                    {
                                        msg = string.Format("Tài khoản {0} chi tiết theo Đối tượng - Nhà cung cấp", account.AccountNumber);
                                        return false;
                                    }
                                    else if (item.Trim().Equals("2") && !accountingObject.IsEmployee)
                                    {
                                        msg = string.Format("Tài khoản {0} chi tiết theo Đối tượng - Nhân viên", account.AccountNumber);
                                        return false;
                                    }
                                }
                            }
                            else
                            {
                                GOtherVoucherDetail data = _selectItem as GOtherVoucherDetail;
                                Guid tGuidDebit = _selectItem.GetProperty<TK, Guid>("DebitAccountingObjectID");
                                Guid tGuidCredit = _selectItem.GetProperty<TK, Guid>("CreditAccountingObjectID");
                                if ((tGuidDebit == null || tGuidDebit == Guid.Empty) && data.DebitAccount == account.AccountNumber)
                                {
                                    msg = string.Format("Tài khoản nợ {0} chi tiết theo đối tượng", account.AccountNumber);
                                    return false;
                                }
                                else if ((tGuidCredit == null || tGuidCredit == Guid.Empty) && data.CreditAccount == account.AccountNumber)
                                {
                                    msg = string.Format("Tài khoản có {0} chi tiết theo đối tượng", account.AccountNumber);
                                    return false;
                                }
                                else
                                {
                                    //AccountingObject accountingObjectDebit = Utils.IAccountingObjectService.Getbykey(tGuidDebit).CloneObject();
                                    //AccountingObject accountingObjectCredit = Utils.IAccountingObjectService.Getbykey(tGuidCredit).CloneObject();
                                    AccountingObject accountingObjectDebit = Utils.ListAccountingObject.FirstOrDefault(o => o.ID == tGuidDebit);
                                    AccountingObject accountingObjectCredit = Utils.ListAccountingObject.FirstOrDefault(o => o.ID == tGuidCredit);
                                    if ((tGuidDebit != null && tGuidDebit != Guid.Empty) && accountingObjectDebit == null)
                                    {
                                        msg = string.Format("Đối tượng kế toán ghi nợ không có trong danh mục!");
                                        return false;
                                    }

                                    if ((tGuidCredit != null && tGuidCredit != Guid.Empty) && accountingObjectCredit == null)
                                    {
                                        msg = string.Format("Đối tượng kế toán ghi có không có trong danh mục!");
                                        return false;
                                    }
                                    //if (item.Trim().Equals("1") && accountingObject.ObjectType != 0 && accountingObject.ObjectType != 2)
                                    //{
                                    //    msg = string.Format("Tài khoản {0} chi tiết theo Đối tượng - Khách hàng", account.AccountNumber);
                                    //    return false;
                                    //}
                                    //else if (item.Trim().Equals("0") && accountingObject.ObjectType != 1 && accountingObject.ObjectType != 2)
                                    //{
                                    //    msg = string.Format("Tài khoản {0} chi tiết theo Đối tượng - Nhà cung cấp", account.AccountNumber);
                                    //    return false;
                                    //}
                                    //else if (item.Trim().Equals("2") && !accountingObject.IsEmployee)
                                    //{
                                    //    msg = string.Format("Tài khoản {0} chi tiết theo Đối tượng - Nhân viên", account.AccountNumber);
                                    //    return false;
                                    //}
                                }
                            }

                        }
                        break;
                    case "3":   //Đối tượng tập hợp chi phí
                        {
                            if (!_selectItem.HasProperty("CostSetID"))
                                break;
                            Guid tGuid = _selectItem.GetProperty<TK, Guid>("CostSetID");
                            if (tGuid == null || tGuid == Guid.Empty)
                            {
                                msg = string.Format("Tài khoản {0} chi tiết theo đối tượng tập hợp chi phí", account.AccountNumber);
                                return false;
                            }
                        }
                        break;
                    case "4":   //Hợp đồng
                        {
                            if (!_selectItem.HasProperty("ContractID"))
                                break;
                            Guid tGuid = _selectItem.GetProperty<TK, Guid>("ContractID");
                            if (tGuid == null || tGuid == Guid.Empty)
                            {
                                msg = string.Format("Tài khoản {0} chi tiết theo hợp đồng", account.AccountNumber);
                                return false;
                            }
                        }
                        break;
                    case "5":   //Vật tư hàng hóa, Công cụ dụng cụ
                        {
                            if (!_selectItem.HasProperty("MaterialGoodsID"))
                                break;
                            Guid tGuid = _selectItem.GetProperty<TK, Guid>("MaterialGoodsID");
                            if (tGuid == null || tGuid == Guid.Empty)
                            {
                                msg = string.Format("Tài khoản {0} chi tiết theo vật tư hàng hóa, công cụ dụng cụ", account.AccountNumber);
                                return false;
                            }
                        }
                        break;
                    case "6":   //Tk Ngân hàng
                        {
                            //if (!_selectItem.HasProperty("BankAccountDetailID"))
                            //    break;
                            //Guid tGuid = _selectItem.GetProperty<TK, Guid>("BankAccountDetailID");
                            //int? type = _select.GetProperty<T, int?>("TypeID");
                            //if (type != null && type == 150) break;
                            //if ((tGuid == null || tGuid == Guid.Empty))
                            //{
                            //    msg = string.Format("Tài khoản {0} chi tiết theo tài khoản ngân hàng", account.AccountNumber);
                            //    return false;
                            //}
                        }
                        break;
                    case "7":   //Phát sinh theo khoản mục chi phí
                        {
                            if (!_selectItem.HasProperty("ExpenseItemID"))
                                break;
                            Guid tGuid = _selectItem.GetProperty<TK, Guid>("ExpenseItemID");
                            if (tGuid == null || tGuid == Guid.Empty)
                            {
                                msg = string.Format("Tài khoản {0} chi tiết theo khoản mục chi phí", account.AccountNumber);
                                return false;
                            }
                        }
                        break;
                    case "8":   //Ngoại tệ
                        {
                            if (!_selectItem.HasProperty("CurrencyID"))
                                break;
                            string tGuid = _select.GetProperty<T, string>("CurrencyID");
                            //Currency currency = Utils.ICurrencyService.Getbykey(tGuid);
                            if (string.IsNullOrEmpty(tGuid)) continue;
                            if (tGuid.Equals("VND"))
                            {
                                msg = string.Format("Tài khoản {0} chi tiết theo ngoại tệ", account.AccountNumber);
                                return false;
                            }
                        }
                        break;
                    case "9":   //Phát sinh theo phòng ban
                        {
                            if (!_selectItem.HasProperty("DepartmentID"))
                                break;
                            Guid tGuid = _selectItem.GetProperty<TK, Guid>("DepartmentID");
                            if (tGuid == null || tGuid == Guid.Empty)
                            {
                                msg = string.Format("Tài khoản {0} chi tiết theo phòng ban", account.AccountNumber);
                                return false;
                            }
                        }
                        break;
                    case "10":  //Phát sinh theo mục thu/chi
                        {
                            if (!_selectItem.HasProperty("BudgetItemID"))
                                break;
                            Guid tGuid = _selectItem.GetProperty<TK, Guid>("BudgetItemID");
                            if (tGuid == null || tGuid == Guid.Empty)
                            {
                                msg = string.Format("Tài khoản {0} chi tiết theo mục thu/chi", account.AccountNumber);
                                return false;
                            }
                        }
                        break;
                }
            }
            return true;
        }

        /// <summary>
        /// Gọi Iservices của đối tượng kiểu T với khóa chính kiểu TK
        /// [kiendd]
        /// </summary>
        /// <typeparam name="T">kiểu của đối tượng</typeparam>
        /// <typeparam name="TK">kiểu dữ liệu của khóa chính</typeparam>
        /// <param name="_this">form</param>
        /// <returns></returns>
        public static BaseService<T, TK> GetIServiceStand<T, TK>(this object _this)
        {
            var method = typeof(IoC).GetMethod("Resolve", new Type[0]).MakeGenericMethod(Core.Utils.getType(string.Format("Accounting.Core.IService.I{0}Service", typeof(T).Name)));
            return (BaseService<T, TK>)method.Invoke(_this, null);
        }

        /// <summary>
        /// Refresh dữ liệu trong trường hợp dữ liệu obj thay đổi mà dữ liệu control chưa thay đổi theo!
        /// [kiendd]
        /// </summary>
        /// <param name="ultraGrid"></param>
        public static void RefreshRowGrid(this UltraGrid ultraGrid)
        {
            if (ultraGrid == null) return;
            foreach (UltraGridRow ultraGridRow in ultraGrid.Rows)
            {
                ultraGridRow.Refresh(RefreshRow.ReloadData);
                ultraGridRow.Refresh(RefreshRow.RefreshDisplay);
                ultraGridRow.Refresh(RefreshRow.FireInitializeRow);
            }
            ultraGrid.DisplayLayout.RefreshSummaries();
        }
    }

    public class ErrorObj
    {//Lưu tạm, sau chuyển sang Domain
        public string Id { get; set; }  //key định danh của lỗi (có thể dùng luôn Property của _Select)
        public string Location { get; set; }    //vị trí xảy ra lỗi
        public object Object { get; set; } //đối tượng bị lỗi
        public string Msg { get; set; } //thông báo lỗi hiển thị
        public Exception Exception { get; set; } //thông báo lỗi hiển thị
    }
}