﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting
{
    public class TabLine : Infragistics.Win.IUIElementDrawFilter
    {
        private System.Drawing.Color private_LineColor = System.Drawing.Color.Red;
        public TabLine()
        {
            //private_LineColor = _LineColor;
        }

        public bool DrawElement(Infragistics.Win.DrawPhase drawPhase, ref Infragistics.Win.UIElementDrawParams drawParams)
        {
            if (drawPhase == Infragistics.Win.DrawPhase.BeforeDrawFocus)
                return true;
            else
            {
                drawParams.Element.Rect = new System.Drawing.Rectangle(drawParams.Element.Rect.Location, new System.Drawing.Size(drawParams.Element.Rect.Width, 5));
                drawParams.Graphics.FillRectangle(new System.Drawing.SolidBrush(private_LineColor), drawParams.Element.Rect);
                return false;
            }
        }

        public Infragistics.Win.DrawPhase GetPhasesToFilter(ref Infragistics.Win.UIElementDrawParams drawParams)
        {
            if (drawParams.Element is Infragistics.Win.UltraWinTabs.TabLineUIElement)
            {
                return Infragistics.Win.DrawPhase.AfterDrawElement;
            }
            else if (drawParams.Element is Infragistics.Win.UltraWinTabs.TabItemUIElement) return Infragistics.Win.DrawPhase.BeforeDrawFocus;
            return Infragistics.Win.DrawPhase.None;
        }
    }  
}
