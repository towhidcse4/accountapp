﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using Accounting.TextMessage;
using Infragistics.Shared;
using Infragistics.Win;

namespace Accounting
{   
    public class CreationFilterForRefDateTime : IUIElementCreationFilter
    {       
        void IUIElementCreationFilter.AfterCreateChildElements(UIElement parent)
        {
            ButtonUIElementBase buttonElement = parent as ButtonUIElementBase;
            if (buttonElement != null)
            {
                ToolTipManager tooltipElement = new ToolTipManager(buttonElement);
                tooltipElement.Rect = buttonElement.Rect;
                buttonElement.ChildElements.Add(tooltipElement);
            }
        }

        bool IUIElementCreationFilter.BeforeCreateChildElements(UIElement parent)
        {
            return false;
        }
    }
    public class ToolTipManager : UIElement, IToolTipItem
    {
        
        public ToolTipManager(UIElement parent)
            : base(parent)
        {
            ToolTipItem = this;
        }

        protected override void DrawBackColor(ref UIElementDrawParams drawParams) { }

        protected override bool WantsInputNotification(UIElementInputType inputType, Point point)
        {
            return inputType == UIElementInputType.MouseHover;
        }

        ToolTipInfo IToolTipItem.GetToolTipInfo(Point mousePosition, UIElement element, UIElement previousToolTipElement, ToolTipInfo toolTipInfoDefault)
        {
            toolTipInfoDefault.AutoPopDelay = 4000;
            // toolTipInfoDefault.ToolTipText = "Nhấn vào đây để điều chỉnh giờ kiểm kê quỹ";
                toolTipInfoDefault.ToolTipText = resSystem.String_05;
            return toolTipInfoDefault;
        }
    }
}
