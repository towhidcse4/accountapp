﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using Accounting.TextMessage;
using System.Configuration;
using System.Diagnostics;
using System.Threading.Tasks;
using Accounting.Core.Domain;
using Accounting.Core;
using System.Data.SqlClient;
using System.Data.Sql;

namespace Accounting
{
    //Author : Hautv
    public partial class FStartApp : StartApplication
    {
        private string server;
        private string database;
        private string user;
        private string pass;
        private Company Session;
        private string[] args;
        public FStartApp(string[] arguments)
        {
            InitializeComponent();
            this.Load += FStartApp_Load;
            this.Activated += AutoRun;
            SplashScreen.CloseForm();
            FStartApp_Resize(this, null);
            string path = System.IO.Path.GetDirectoryName(
            System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            //RegistryUtils.HKEY_LOCAL_MACHINE.SetValue("path", path);
            args = arguments;
        }

        void AutoRun (object sender, EventArgs e)
        {
            if (args.Contains("--auto"))
            {
                string AppUser = "ADMIN";
                string AppPass = "tvan@123";

                if (args.Contains("--app-user"))
                {
                    AppUser = args[Array.IndexOf(args, "--app-user") + 1];
                }

                if (args.Contains("--app-password"))
                {
                    var pass = ITripleDes.TripleDesDefaultDecryption(args[Array.IndexOf(args, "--app-password") + 1]);
                    AppPass = pass.IsNullOrEmpty() ? AppPass : pass;
                }

                if (args.Contains("-s"))
                {
                    server = args[Array.IndexOf(args, "-s") + 1];
                }
                else
                {
                    MSG.Error("Server Name not found!");
                    return;
                }

                if (args.Contains("-d"))
                {
                    database = args[Array.IndexOf(args, "-d") + 1];
                }
                else
                {
                    MSG.Error("Database not found!");
                    return;
                }

                if (args.Contains("-u"))
                {
                    user = args[Array.IndexOf(args, "-u") + 1];
                }
                else
                {
                    MSG.Error("Username not found!");
                    return;
                }

                if (args.Contains("-p"))
                {
                    pass = ITripleDes.TripleDesDefaultDecryption(args[Array.IndexOf(args, "-p") + 1]);
                }
                else
                {
                    MSG.Error("Password not found!");
                    return;
                }

                OpenProcessAuto(AppUser, AppPass);
            }
        }

        void FStartApp_Load(object sender, EventArgs e)
        {
            //DrawControlBackground(this.btnClose, false);
            //UpdateLayeredBackground();

            //For convenience when switching between the on / off images
            //m_closeBtnOff = this.btnClose.Image;
            //m_closeBtnOn = this.btnClose.BackgroundImage;
        }

        private void FStartApp_Resize(object sender, EventArgs e)
        {
            //pnlCenter.Location = new Point((this.Size.Width / 2) - 168, (this.Size.Height / 2) - 134);
        }

        private void btnCreateDb_Click(object sender, EventArgs e)
        {
            var f = new FCreateDatabase();
            try
            {
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                WaitingFrm.StopWaiting();
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
            if (f.DialogResult == DialogResult.OK)
            {
                MSG.MessageBoxStand("Tạo mới dữ liệu kế toán thành công !", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            OpenProcess();
        }

        public void OpenProcess()
        {
            if (!Authenticate.LogOn.HasValue) return;
            if (Authenticate.LogOn == false)
            {
                ////Đóng toàn bộ chương trình
                //System.Environment.Exit(-1); 
                Authenticate.LogOn = null;
                return;
            }
            Authenticate.LogOn = null;
            try
            {
                StartMain();
            }
            catch (Exception ex)
            {
                WaitingFrm.StopWaiting();
                if (ex.ToString().Contains("System.UnauthorizedAccessException"))
                {
                    string path = System.IO.Path.GetDirectoryName(
                        System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                    MSG.Warning(string.Format(resSystem.MSG_System_75, path.Split(new[] { "file:\\" }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault()));
                }
                else MSG.Warning(resSystem.MSG_System_55);// + " \r\n" + ex.ToString()
                if (this.Visible == false)
                    this.Show();
            }
        }

        private void btnOpenVnpt_Click(object sender, EventArgs e)
        {
            OpenProcessAuto("ADMIN", "tvan@123", true);
        }
        //Edit by Hautv Tao Dữ liệu mẫu
        private void OpenProcessAuto(string appUser, string appPassword, bool isDBMau = false)
        {
            try
            {
                if (isDBMau)
                {
                    WaitingFrm.StartWaiting();
                    #region TH tạo mới CSDL
                    //string myServer = Environment.MachineName;//comment by cuongpv
                    string myServer = @".\HDSAccouting";//add by cuongpv de build ban khach hang
                    string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                    var filePatch = string.Format("{0}\\Data", path).Replace("file:\\", "");
                    string tenCSDLMau = "EB_DULIEUMAU";
                    bool @finally = false;
                    //string sqlConnectionString =
                    //    string.Format(
                    //        "Data Source={0};initial catalog=master;Integrated Security=false;User ID={1};Password={2};",
                    //        server, user, pass);
                    //if (txt.Trim().ToUpper().Equals("ADMIN") && string.IsNullOrEmpty(txtPwd.Text))
                    string sqlConnectionString =
                            string.Format("Data Source={0};initial catalog=master;Integrated Security=SSPI;", myServer);
                    var con = new SqlConnection();
                    con = new SqlConnection(sqlConnectionString);
                    SqlTransaction transaction;
                    try
                    {
                        con.Open();
                    }
                    catch (Exception)
                    {
                        WaitingFrm.StopWaiting();
                        MSG.Warning("Máy chủ SQL không tồn tại. Vui lòng chọn máy chủ SQL khác.");
                        return;
                    }
                    try
                    {
                        //Tạo mới Patch lưu file database nếu chưa có
                        bool exist = false;
                        if (!System.IO.Directory.Exists(filePatch))
                            System.IO.Directory.CreateDirectory(filePatch);
                        //Cấp quyền cho thư mục mới tạo
                        DirectorySecurity sec = Directory.GetAccessControl(filePatch);
                        SecurityIdentifier everyone = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
                        sec.AddAccessRule(new FileSystemAccessRule(everyone, FileSystemRights.Modify | FileSystemRights.Synchronize, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                        Directory.SetAccessControl(filePatch, sec);
                        //Tạo Database mới
                        //1.Kiểm tra CSDL đã tồn tại chưa. Nếu đã tồn tại thì báo lỗi
                        var qr =
                            string.Format(
                                @"SELECT name FROM master.sys.databases WHERE name = N'{0}'",
                                tenCSDLMau);
                        try
                        {
                            using (SqlCommand sqlCmd = new SqlCommand(qr, con))
                            {
                                SqlDataReader reader = sqlCmd.ExecuteReader();
                                exist = reader.HasRows;
                                reader.Close();
                            }
                        }
                        catch (SqlException)
                        {
                            WaitingFrm.StopWaiting();
                            MSG.Warning("Máy chủ SQL không tồn tại. Vui lòng chọn máy chủ SQL khác.");
                            return;
                        }
                        if (exist)
                        {
                            //Cơ sở dữ liệu đã tồn tại
                            try
                            {
                                string path_ = System.IO.Path.GetDirectoryName(
                                  System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
                                string filePath_ = string.Format("{0}\\Notify_Update\\{1}.xml", path_, Frm.FrmCauHinh.GetLocalVersionNumber());
                                XmlDocument doc = new XmlDocument();
                                // Loading from a XML string (use Load() for file)
                                doc.Load(filePath_);
                                XmlNode xmlNode = doc.SelectSingleNode("Notifys/isDeleteEBMau");
                                int n = int.Parse(xmlNode.InnerText.Replace("\n", String.Empty).Replace("\r", String.Empty).Replace("\t", String.Empty).Trim(' '));
                                if (n == 0)
                                {
                                    //Xóa database cũ nếu update
                                    string qr_delete = @"EXEC msdb.dbo.sp_delete_database_backuphistory @database_name = N'EB_DULIEUMAU'
                                                    USE [master]
                                                    ALTER DATABASE [EB_DULIEUMAU] SET  SINGLE_USER WITH ROLLBACK IMMEDIATE
                                                    USE [master]
                                                    DROP DATABASE [EB_DULIEUMAU]";
                                    new SqlCommand(qr_delete, con).ExecuteNonQuery();
                                    //-----------
                                    xmlNode.InnerText = (n + 1).ToString();
                                    doc.Save(filePath_);
                                }
                                else
                                {
                                    WaitingFrm.StopWaiting();
                                    @finally = true;
                                    goto FINALLY;
                                }
                            }
                            catch (Exception ex)
                            {
                                WaitingFrm.StopWaiting();
                                MSG.Error(ex.Message);
                                return;
                            }
                        }
                        //2.Nếu CSDL chưa tồn tại thì tạo mới
                        var newData = string.Format(@"{1}\{0}.mdf", tenCSDLMau, filePatch).Replace("\\\\", @"\");
                        var newLog = string.Format(@"{1}\{0}Log.ldf", tenCSDLMau, filePatch).Replace("\\\\", @"\");

                        //Xóa các file mô tả đã tồn tại
                        if (File.Exists(newData))
                        {
                            File.Delete(newData);
                        }

                        if (File.Exists(newLog))
                        {
                            File.Delete(newLog);
                        }

                        qr = string.Format("CREATE DATABASE {0} ON PRIMARY " +
                                                "(NAME = {0}_Data, " +
                                                "FILENAME = '{2}', " +
                                                "SIZE = 10096KB, MAXSIZE = 29687808KB, FILEGROWTH = 262144KB) " +
                                                "LOG ON (NAME = {0}_Log, " +
                                                "FILENAME = '{3}', " +
                                                "SIZE = 10096KB, " +
                                                "MAXSIZE = 29687808KB, " +
                                                "FILEGROWTH = 10%)", tenCSDLMau, filePatch, newData, newLog);
                        new SqlCommand(qr, con).ExecuteNonQuery();

                        //3. Sử dụng CSDL mới tạo
                        var qr_use = string.Format(@"USE {0}", tenCSDLMau);
                        new SqlCommand(qr_use, con).ExecuteNonQuery();
                    }
                    catch (SqlException sqlError)
                    {
                        WaitingFrm.StopWaiting();
                        MSG.Warning("Có lỗi xảy ra khi khởi tạo cơ sở dữ liệu mới. \r\n " + sqlError);
                        goto FINALLY;
                    }
                    //Thực hiện chạy file srcipt SQL có sẵn
                    transaction = con.BeginTransaction();
                    try
                    {
                        using (System.IO.Stream stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("Accounting.Data.EB_DULIEUMAU.sql"))
                        {
                            if (stream != null)
                            {
                                using (StreamReader sr = new StreamReader(stream))
                                {
                                    string lq = "";
                                    var fileContent = sr.ReadToEnd();
                                    fileContent = fileContent.Replace("ACCOUNTING", tenCSDLMau).Replace("\r\n", " ");
                                    var sqlqueries = fileContent.Split(new[] { " GO " }, StringSplitOptions.RemoveEmptyEntries);
                                    foreach (var query in sqlqueries)
                                    {
                                        try
                                        {
                                            lq = query;
                                            new SqlCommand(query, con, transaction).ExecuteNonQuery();
                                        }
                                        catch (Exception ex)
                                        {
                                            WaitingFrm.StopWaiting();
                                            MSG.Warning(query + "\r\n" + ex.ToString());
                                            goto ERROR;
                                        }

                                    }
                                    transaction.Commit();
                                    WaitingFrm.StopWaiting();
                                    @finally = true;
                                }
                            }
                            else goto ERROR;
                        }
                    }
                    catch (SqlException sqlError)
                    {
                        WaitingFrm.StopWaiting();
                        MSG.Warning(sqlError.ToString());
                        goto ERROR;
                    }
                    goto FINALLY;
                ERROR:
                    transaction.Rollback();
                    try
                    {
                        new SqlCommand("USE [master]", con).ExecuteNonQuery();
                        new SqlCommand(string.Format("DROP DATABASE {0};", database), con);
                    }
                    catch (Exception ex)
                    {
                    }
                    WaitingFrm.StopWaiting();
                    MSG.Warning("Có lỗi xảy ra khi khởi tạo dữ liệu mẫu.");
                FINALLY:
                    if (con.State == ConnectionState.Open)
                    {
                        //con.Close();
                    }
                    #endregion
                    if (@finally)
                    {
                        FLogon logon = new FLogon();
                        if (logon.LogOnProcess(myServer, "ADMIN", "", tenCSDLMau, "ADMIN", ""))
                        {
                            Utils.isDuLieuMau = true;
                            logon.Close();
                            StartMain(true);
                            
                        }
                    }
                }
                else
                {
                    FLogon logon = new FLogon();
                    if (logon.LogOnProcess(server, user, pass, database, appUser, appPassword))
                    {
                        logon.Close();
                        StartMain();
                    }
                }
            }
            catch (Exception ex)
            {
                WaitingFrm.StopWaiting();
                if (ex.ToString().Contains("System.UnauthorizedAccessException"))
                {
                    string path = System.IO.Path.GetDirectoryName(
                        System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                    MSG.Warning(string.Format(resSystem.MSG_System_75, path.Split(new[] { "file:\\" }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault()));
                }
                else MSG.Warning(resSystem.MSG_System_55);// + " \r\n" + ex.ToString()
                if (this.Visible == false)
                    this.Show();
            }
        }
        
        void StartMain(bool isDuLieuMau = false)
        {
            if (isDuLieuMau)
            {
                var f = new FMain(isDuLieuMau : true);
                this.Hide();
                f.ShowDialog(this);
                this.Close();
                if (f.NextSession != null)
                {
                    Session = f.NextSession;
                    Utils.setConnectConfig(Session);
                    Application.Exit();
                    string agrs = string.Format("--auto -s {0} -u {1} -p {2} -d {3} --app-user {4} --app-password {5}", Session.ServerName, Session.Username, Session.Password, Session.Database, Session.AppUser, Session.AppPass);
                    System.Diagnostics.Process.Start(Application.ExecutablePath, agrs);
                }
            }
            else
            {
                var f = new FMain();
                this.Hide();
                f.ShowDialog(this);
                this.Close();
                if (f.NextSession != null)
                {
                    Session = f.NextSession;
                    Utils.setConnectConfig(Session);
                    Application.Exit();
                    string agrs = string.Format("--auto -s {0} -u {1} -p {2} -d {3} --app-user {4} --app-password {5}", Session.ServerName, Session.Username, Session.Password, Session.Database, Session.AppUser, Session.AppPass);
                    System.Diagnostics.Process.Start(Application.ExecutablePath, agrs);
                }
            }
            
        }

        void f_FormClosed(object sender, FormClosedEventArgs e)
        {
            Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FStartApp_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Authenticate.User != null)
            {
                try
                {
                    var user = Utils.ISysUserService.Getbykey(Authenticate.User.userid);
                    Utils.ISysUserService.UnbindSession(user);
                    user = Utils.ISysUserService.Getbykey(Authenticate.User.userid);
                    user.IsOnline = false;
                    Utils.ISysUserService.Update(user);
                }
                catch (Exception)
                {
                }

                Logs.Save(0, "Bảo mật", Logs.Action.Logout, string.Empty, null, string.Empty);
            }

        }
    }
}
