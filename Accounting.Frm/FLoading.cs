﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Accounting.Core;
using FX.Data;

namespace Accounting
{
    public partial class FLoading<T> : Form
    {
        BindingList<T> _bdlInput = new BindingList<T>();
        public FLoading(T input)
        {
            InitializeComponent();
            this.uProgressBar.Minimum = 0;
            this.uProgressBar.Maximum = 100;
            _bdlInput.AddingNew += _bdlInput_AddingNew;
        }

        void _bdlInput_AddingNew(object sender, AddingNewEventArgs e)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This event fires when the BG Worker is invoked. This 
        /// event fires async
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bgwLoadData_DoWork(object sender, DoWorkEventArgs e)
        {
            BaseService<T, Guid> testInvoke = this.GetIService((T)Activator.CreateInstance(typeof(T)));
            _bdlInput = new BindingList<T>(testInvoke.GetAll());
            ////This is a new instance of a Customers collection that
            ////may have been fetched from a DB or wherever. 
            //CustomerCollection newCustomers = new CustomerCollection();

            ////This simulates the known number of objects that 
            ////were (or are about to be) returned
            //int returnedRecordCount = 100;

            ////Here we loop through and add a new instance of 
            ////a customer object to the collection and each iteration
            ////we check to see if someone cancelled the operation
            //for (int i = 1; i <= returnedRecordCount; i++)
            //{
            //    if (this.bgwLoadData.CancellationPending)
            //    {
            //        e.Cancel = true;
            //        return;
            //    }

            //    Customer theCustomer = new Customer(DateTime.Now.Ticks, Guid.NewGuid().ToString());

            //    newCustomers.Add(theCustomer);

            //    //Here is where we use this method to pass back
            //    //information to the UI Thread. Notice how we
            //    //pass the current progress (limited from 0 to 100) 
            //    //as well as the actual customer object. 
            //    this.bgwLoadData.ReportProgress(i, theCustomer);

            //    System.Threading.Thread.Sleep(20);
            //}
        }

        /// <summary>
        /// This event is fired on the UI thread. Here we can get the event args
        /// and update the Progress bar as well as add the Customer object
        /// to our CustomerCollection. Doing this causes the new Customer
        /// to show in the WinGrid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bgwLoadData_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
           // _customers.Add(e.UserState as Customer);

            if (e.ProgressPercentage <= this.uProgressBar.Maximum)
            {
                this.uProgressBar.Value = e.ProgressPercentage;
            }
        }

        /// <summary>
        /// When complete, we need to enable the button and update the label
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bgwLoadData_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //this.btnGetData.Enabled = true;
            //this.lblMessage.Text = "Retrieval Complete. Total Records: " + _customers.Count.ToString();
        }
        
    }
}
