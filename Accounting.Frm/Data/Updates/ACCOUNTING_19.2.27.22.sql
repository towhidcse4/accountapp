SET CONCAT_NULL_YIELDS_NULL, ANSI_NULLS, ANSI_PADDING, QUOTED_IDENTIFIER, ANSI_WARNINGS, ARITHABORT, XACT_ABORT ON
SET NUMERIC_ROUNDABORT, IMPLICIT_TRANSACTIONS OFF
GO

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

BEGIN TRANSACTION
GO

DECLARE @res int
EXEC @res = sp_rename N'[dbo].[TITransferDetail]', N'tmp_devart_TITransferDetail', 'OBJECT'
IF @res <> 0
  RAISERROR ('Error while Rename table [dbo].[TITransferDetail] to [dbo].[tmp_devart_TITransferDetail]', 11, 1 );
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[TITransferDetail] (
  [ID] [uniqueidentifier] NOT NULL,
  [TITransferID] [uniqueidentifier] NOT NULL,
  [ToolsID] [uniqueidentifier] NOT NULL,
  [Description] [nvarchar](512) NULL DEFAULT (NULL),
  [FromDepartmentID] [uniqueidentifier] NULL DEFAULT (NULL),
  [ToDepartmentID] [uniqueidentifier] NULL DEFAULT (NULL),
  [Quantity] [decimal](25, 10) NULL DEFAULT (NULL),
  [OWAmount] [decimal](19, 4) NOT NULL,
  [AllocateAmount] [decimal](19, 4) NOT NULL,
  [RemainAmount] [decimal](19, 4) NOT NULL,
  [DebitAccount] [nvarchar](25) NULL DEFAULT (NULL),
  [CreditAccount] [nvarchar](25) NULL DEFAULT (NULL),
  [CostSetID] [uniqueidentifier] NULL DEFAULT (NULL),
  [StatisticsCodeID] [uniqueidentifier] NULL DEFAULT (NULL),
  [TIIncrementID] [uniqueidentifier] NULL DEFAULT (NULL),
  [UnitPrice] [decimal](19, 4) NOT NULL,
  [AllocationTimes] [int] NULL DEFAULT (NULL),
  [AllocationType] [int] NULL DEFAULT (NULL),
  [ConfrontTypeID] [int] NULL DEFAULT (NULL),
  [IsIrrationalCost] [bit] NULL DEFAULT (NULL),
  [TransferQuantity] [decimal](25, 10) NULL,
  [BudgetItemID] [uniqueidentifier] NULL,
  [ExpenseItemID] [uniqueidentifier] NULL,
  [OrderPriority] [int] IDENTITY
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

SET IDENTITY_INSERT [dbo].[TITransferDetail] OFF

INSERT [dbo].[TITransferDetail](ID, TITransferID, ToolsID, Description, FromDepartmentID, ToDepartmentID, Quantity, OWAmount, AllocateAmount, RemainAmount, DebitAccount, CreditAccount, CostSetID, StatisticsCodeID, TIIncrementID, UnitPrice, AllocationTimes, AllocationType, ConfrontTypeID, IsIrrationalCost, TransferQuantity, BudgetItemID, ExpenseItemID)
  SELECT ID, TITransferID, ToolsID, Description, FromDepartmentID, ToDepartmentID, Quantity, OWAmount, AllocateAmount, RemainAmount, DebitAccount, CreditAccount, CostSetID, StatisticsCodeID, TIIncrementID, UnitPrice, AllocationTimes, AllocationType, ConfrontTypeID, IsIrrationalCost, TransferQuantity, BudgetItemID, ExpenseItemID FROM [dbo].[tmp_devart_TITransferDetail] WITH (NOLOCK)

if @@ERROR = 0
  DROP TABLE [dbo].[tmp_devart_TITransferDetail]

SET IDENTITY_INSERT [dbo].[TITransferDetail] OFF
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TITransferDetail]
  ADD PRIMARY KEY CLUSTERED ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @res int
EXEC @res = sp_rename N'[dbo].[TIInit]', N'tmp_devart_TIInit', 'OBJECT'
IF @res <> 0
  RAISERROR ('Error while Rename table [dbo].[TIInit] to [dbo].[tmp_devart_TIInit]', 11, 1 );
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @res int
EXEC @res = sp_rename N'[dbo].[PK__TIInit__3214EC27C04826C2]', N'tmp_devart_PK__TIInit__3214EC27C04826C2', 'OBJECT'
IF @res <> 0
  RAISERROR ('Error while Rename index [PK__TIInit__3214EC27C04826C2] to [tmp_devart_PK__TIInit__3214EC27C04826C2] on table [dbo].[TIInit]', 11, 1 );
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[TIInit] (
  [ID] [uniqueidentifier] NOT NULL,
  [BranchID] [uniqueidentifier] NULL,
  [TypeID] [int] NULL,
  [MaterialGoodsID] [uniqueidentifier] NULL,
  [Quantity] [decimal](25, 10) NULL,
  [UnitPrice] [decimal](19, 4) NULL,
  [Amount] [decimal](19, 4) NULL,
  [AllocationTimes] [int] NULL,
  [AllocatedAmount] [decimal](19, 4) NULL,
  [RemainAmount] [decimal](19, 4) NULL,
  [CostSetID] [uniqueidentifier] NULL,
  [AllocationType] [int] NULL,
  [RemainAllocationTimes] [int] NULL,
  [InitStatus] [int] NULL,
  [IsIrrationalCost] [bit] NULL,
  [TemplateID] [uniqueidentifier] NULL,
  [AllocationAwaitAccount] [nvarchar](15) NULL,
  [AllocationAmount] [decimal](19, 4) NULL,
  [IsActive] [int] NULL,
  [FixedAssetAccessoriesUnit] [nvarchar](25) NULL,
  [Description] [nvarchar](225) NULL,
  [DeclareType] [int] NULL,
  [MaterialGoodsCategoryID] [uniqueidentifier] NULL,
  [ToolsName] [nvarchar](255) NULL,
  [ToolsCode] [nvarchar](25) NULL,
  [Unit] [nvarchar](255) NULL,
  [PostedDate] [datetime] NULL
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

INSERT [dbo].[TIInit](ID, BranchID, TypeID, PostedDate, MaterialGoodsID, Quantity, UnitPrice, Amount, AllocationTimes, AllocatedAmount, RemainAmount, CostSetID, AllocationType, RemainAllocationTimes, InitStatus, IsIrrationalCost, TemplateID, AllocationAwaitAccount, AllocationAmount, IsActive, FixedAssetAccessoriesUnit, Description, DeclareType, MaterialGoodsCategoryID, ToolsName, ToolsCode, Unit)
  SELECT ID, BranchID, TypeID, PostedDate, MaterialGoodsID, Quantity, UnitPrice, Amount, AllocationTimes, AllocatedAmount, RemainAmount, CostSetID, AllocationType, RemainAllocationTimes, InitStatus, IsIrrationalCost, TemplateID, AllocationAwaitAccount, AllocationAmount, IsActive, FixedAssetAccessoriesUnit, Description, DeclareType, MaterialGoodsCategoryID, ToolsName, ToolsCode, Unit FROM [dbo].[tmp_devart_TIInit] WITH (NOLOCK)

if @@ERROR = 0
  DROP TABLE [dbo].[tmp_devart_TIInit]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIInit]
  ADD CONSTRAINT [PK__TIInit__3214EC27C04826C2] PRIMARY KEY CLUSTERED ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @value SQL_VARIANT = CAST('' AS varchar(1))
EXEC sys.sp_addextendedproperty N'MS_Description', @value, 'SCHEMA', N'dbo', 'TABLE', N'TIInit', 'COLUMN', N'PostedDate'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @res int
EXEC @res = sp_rename N'[dbo].[TIIncrementDetail]', N'tmp_devart_TIIncrementDetail', 'OBJECT'
IF @res <> 0
  RAISERROR ('Error while Rename table [dbo].[TIIncrementDetail] to [dbo].[tmp_devart_TIIncrementDetail]', 11, 1 );
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @res int
EXEC @res = sp_rename N'[dbo].[PK__TIIncrem__3214EC27D14CBCCE]', N'tmp_devart_PK__TIIncrem__3214EC27D14CBCCE', 'OBJECT'
IF @res <> 0
  RAISERROR ('Error while Rename index [PK__TIIncrem__3214EC27D14CBCCE] to [tmp_devart_PK__TIIncrem__3214EC27D14CBCCE] on table [dbo].[TIIncrementDetail]', 11, 1 );
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @res int
EXEC @res = sp_rename N'[dbo].[DF_TIIncrementDetail_TaxExchangeRate]', N'tmp_devart_DF_TIIncrementDetail_TaxExchangeRate', 'OBJECT'
IF @res <> 0
  RAISERROR ('Error while Rename default [DF_TIIncrementDetail_TaxExchangeRate] to [tmp_devart_DF_TIIncrementDetail_TaxExchangeRate] on table [dbo].[TIIncrementDetail]', 11, 1 );
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[TIIncrementDetail] (
  [ID] [uniqueidentifier] NOT NULL,
  [TIIncrementID] [uniqueidentifier] NOT NULL,
  [ToolsID] [uniqueidentifier] NOT NULL,
  [Description] [nvarchar](512) NULL DEFAULT (NULL),
  [DepartmentID] [uniqueidentifier] NULL,
  [DebitAccount] [nvarchar](15) NULL DEFAULT (NULL),
  [CreditAccount] [nvarchar](15) NULL DEFAULT (NULL),
  [Quantity] [decimal](25, 10) NULL DEFAULT (NULL),
  [UnitPrice] [decimal](19, 4) NOT NULL,
  [Amount] [decimal](19, 4) NOT NULL,
  [AllocationTimes] [int] NULL DEFAULT (NULL),
  [AllocationType] [int] NULL DEFAULT (NULL),
  [StatisticsCodeID] [uniqueidentifier] NULL DEFAULT (NULL),
  [CostSetID] [uniqueidentifier] NULL DEFAULT (NULL),
  [RSInwardOutwardID] [uniqueidentifier] NULL DEFAULT (NULL),
  [ExpenseItemID] [uniqueidentifier] NULL DEFAULT (NULL),
  [BudgetItemID] [uniqueidentifier] NULL DEFAULT (NULL),
  [RSInwardOutwardDetailID] [uniqueidentifier] NULL DEFAULT (NULL),
  [IsIrrationalCost] [bit] NULL DEFAULT (NULL),
  [AmountOriginal] [money] NOT NULL,
  [DiscountRate] [decimal](25, 10) NULL,
  [DiscountAmount] [money] NOT NULL,
  [DiscountAmountOriginal] [money] NOT NULL,
  [CustomUnitPrice] [money] NOT NULL,
  [CustomUnitPriceOriginal] [money] NOT NULL,
  [ImportTaxRate] [decimal](25, 10) NULL,
  [ImportTaxAmount] [money] NOT NULL,
  [ImportTaxAmountOriginal] [money] NOT NULL,
  [ImportTaxAccount] [nvarchar](25) NULL,
  [SpecialConsumeTaxRate] [decimal](25, 10) NULL,
  [SpecialConsumeTaxAmount] [money] NOT NULL,
  [SpecialConsumeTaxAmountOriginal] [money] NOT NULL,
  [SpecialConsumeTaxAccount] [nvarchar](25) NULL,
  [VATRate] [decimal](25) NULL,
  [VATAmount] [money] NOT NULL,
  [VATAmountOriginal] [money] NOT NULL,
  [VATAccount] [nvarchar](25) NULL,
  [DeductionDebitAccount] [nvarchar](25) NULL,
  [ContractID] [uniqueidentifier] NULL,
  [AccountingObjectID] [uniqueidentifier] NULL,
  [AccountingObjectTaxID] [uniqueidentifier] NULL,
  [AccountingObjectTaxName] [nvarchar](512) NULL,
  [PaymentID] [uniqueidentifier] NULL,
  [CompanyTaxCode] [nvarchar](50) NULL,
  [InvoiceTypeID] [uniqueidentifier] NULL,
  [ImportTaxExpenseAmount] [money] NOT NULL,
  [ImportTaxExpenseAmountOriginal] [money] NOT NULL,
  [IsVATPaid] [bit] NULL,
  [InvoiceType] [int] NULL,
  [InvoiceDate] [datetime] NULL,
  [InvoiceNo] [nvarchar](25) NULL,
  [InvoiceSeries] [nvarchar](25) NULL,
  [GoodsServicePurchaseID] [uniqueidentifier] NULL,
  [FreightAmount] [money] NOT NULL,
  [FreightAmountOriginal] [money] NOT NULL,
  [TIAuditID] [uniqueidentifier] NULL,
  [UnitPriceOriginal] [decimal](19, 4) NULL,
  [InwardAmount] [money] NULL,
  [InwardAmountOriginal] [money] NULL,
  [ToolsName] [nvarchar](255) NULL,
  [InvoiceTemplate] [nvarchar](25) NULL,
  [TaxExchangeRate] [decimal](25, 10) NULL CONSTRAINT [DF_TIIncrementDetail_TaxExchangeRate] DEFAULT (NULL),
  [VATDescription] [nvarchar](512) NULL,
  [OrderPriority] [int] IDENTITY
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

SET IDENTITY_INSERT [dbo].[TIIncrementDetail] OFF

INSERT [dbo].[TIIncrementDetail](ID, TIIncrementID, ToolsID, Description, DepartmentID, DebitAccount, CreditAccount, Quantity, UnitPrice, Amount, AllocationTimes, AllocationType, StatisticsCodeID, CostSetID, RSInwardOutwardID, ExpenseItemID, BudgetItemID, RSInwardOutwardDetailID, IsIrrationalCost, AmountOriginal, DiscountRate, DiscountAmount, DiscountAmountOriginal, CustomUnitPrice, CustomUnitPriceOriginal, ImportTaxRate, ImportTaxAmount, ImportTaxAmountOriginal, ImportTaxAccount, SpecialConsumeTaxRate, SpecialConsumeTaxAmount, SpecialConsumeTaxAmountOriginal, SpecialConsumeTaxAccount, VATRate, VATAmount, VATAmountOriginal, VATAccount, DeductionDebitAccount, ContractID, AccountingObjectID, AccountingObjectTaxID, AccountingObjectTaxName, PaymentID, CompanyTaxCode, InvoiceTypeID, ImportTaxExpenseAmount, ImportTaxExpenseAmountOriginal, IsVATPaid, InvoiceType, InvoiceDate, InvoiceNo, InvoiceSeries, GoodsServicePurchaseID, FreightAmount, FreightAmountOriginal, TIAuditID, UnitPriceOriginal, InwardAmount, InwardAmountOriginal, ToolsName, InvoiceTemplate, TaxExchangeRate, VATDescription)
  SELECT ID, TIIncrementID, ToolsID, Description, DepartmentID, DebitAccount, CreditAccount, Quantity, UnitPrice, Amount, AllocationTimes, AllocationType, StatisticsCodeID, CostSetID, RSInwardOutwardID, ExpenseItemID, BudgetItemID, RSInwardOutwardDetailID, IsIrrationalCost, AmountOriginal, DiscountRate, DiscountAmount, DiscountAmountOriginal, CustomUnitPrice, CustomUnitPriceOriginal, ImportTaxRate, ImportTaxAmount, ImportTaxAmountOriginal, ImportTaxAccount, SpecialConsumeTaxRate, SpecialConsumeTaxAmount, SpecialConsumeTaxAmountOriginal, SpecialConsumeTaxAccount, VATRate, VATAmount, VATAmountOriginal, VATAccount, DeductionDebitAccount, ContractID, AccountingObjectID, AccountingObjectTaxID, AccountingObjectTaxName, PaymentID, CompanyTaxCode, InvoiceTypeID, ImportTaxExpenseAmount, ImportTaxExpenseAmountOriginal, IsVATPaid, InvoiceType, InvoiceDate, InvoiceNo, InvoiceSeries, GoodsServicePurchaseID, FreightAmount, FreightAmountOriginal, TIAuditID, UnitPriceOriginal, InwardAmount, InwardAmountOriginal, ToolsName, InvoiceTemplate, TaxExchangeRate, VATDescription FROM [dbo].[tmp_devart_TIIncrementDetail] WITH (NOLOCK)

if @@ERROR = 0
  DROP TABLE [dbo].[tmp_devart_TIIncrementDetail]

SET IDENTITY_INSERT [dbo].[TIIncrementDetail] OFF
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD CONSTRAINT [PK__TIIncrem__3214EC27D14CBCCE] PRIMARY KEY CLUSTERED ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @value SQL_VARIANT = CAST('' AS varchar(1))
EXEC sys.sp_addextendedproperty N'MS_Description', @value, 'SCHEMA', N'dbo', 'TABLE', N'TIIncrementDetail', 'COLUMN', N'TaxExchangeRate'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @res int
EXEC @res = sp_rename N'[dbo].[TIAdjustmentDetail]', N'tmp_devart_TIAdjustmentDetail', 'OBJECT'
IF @res <> 0
  RAISERROR ('Error while Rename table [dbo].[TIAdjustmentDetail] to [dbo].[tmp_devart_TIAdjustmentDetail]', 11, 1 );
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @res int
EXEC @res = sp_rename N'[dbo].[PK__TIAdjust__3214EC2755A1136A]', N'tmp_devart_PK__TIAdjust__3214EC2755A1136A', 'OBJECT'
IF @res <> 0
  RAISERROR ('Error while Rename index [PK__TIAdjust__3214EC2755A1136A] to [tmp_devart_PK__TIAdjust__3214EC2755A1136A] on table [dbo].[TIAdjustmentDetail]', 11, 1 );
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[TIAdjustmentDetail] (
  [ID] [uniqueidentifier] NOT NULL,
  [TIAdjustmentID] [uniqueidentifier] NOT NULL,
  [ToolsID] [uniqueidentifier] NOT NULL,
  [Description] [nvarchar](512) NULL DEFAULT (NULL),
  [DebitAccount] [nvarchar](25) NULL DEFAULT (NULL),
  [CreditAccount] [nvarchar](25) NULL DEFAULT (NULL),
  [Amount] [decimal](19, 4) NOT NULL,
  [RemainAllocationTimes] [int] NOT NULL,
  [AllocatedAmount] [decimal](19, 4) NOT NULL,
  [StatisticsCodeID] [uniqueidentifier] NULL DEFAULT (NULL),
  [TIIncrementID] [uniqueidentifier] NULL DEFAULT (NULL),
  [Quantity] [decimal](25, 10) NULL DEFAULT (NULL),
  [DepartmentID] [uniqueidentifier] NULL DEFAULT (NULL),
  [ExpenseItemID] [uniqueidentifier] NULL DEFAULT (NULL),
  [BudgetItemID] [uniqueidentifier] NULL DEFAULT (NULL),
  [ConfrontTypeID] [int] NULL,
  [RemainAmount] [decimal](19, 4) NULL,
  [AllocationTimes] [decimal](19, 4) NULL,
  [RemainingAmount] [money] NULL,
  [NewRemainingAmount] [money] NULL,
  [DifferRemainingAmount] [money] NULL,
  [NewRemainAllocationTimes] [decimal](19, 4) NULL,
  [DifferRemainAllocationTimes] [decimal](19, 4) NULL,
  [OrderPriority] [int] IDENTITY
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

SET IDENTITY_INSERT [dbo].[TIAdjustmentDetail] OFF

INSERT [dbo].[TIAdjustmentDetail](ID, TIAdjustmentID, ToolsID, Description, DebitAccount, CreditAccount, Amount, RemainAllocationTimes, AllocatedAmount, StatisticsCodeID, TIIncrementID, Quantity, DepartmentID, ExpenseItemID, BudgetItemID, ConfrontTypeID, RemainAmount, AllocationTimes, RemainingAmount, NewRemainingAmount, DifferRemainingAmount, NewRemainAllocationTimes, DifferRemainAllocationTimes)
  SELECT ID, TIAdjustmentID, ToolsID, Description, DebitAccount, CreditAccount, Amount, RemainAllocationTimes, AllocatedAmount, StatisticsCodeID, TIIncrementID, Quantity, DepartmentID, ExpenseItemID, BudgetItemID, ConfrontTypeID, RemainAmount, AllocationTimes, RemainingAmount, NewRemainingAmount, DifferRemainingAmount, NewRemainAllocationTimes, DifferRemainAllocationTimes FROM [dbo].[tmp_devart_TIAdjustmentDetail] WITH (NOLOCK)

if @@ERROR = 0
  DROP TABLE [dbo].[tmp_devart_TIAdjustmentDetail]

SET IDENTITY_INSERT [dbo].[TIAdjustmentDetail] OFF
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIAdjustmentDetail]
  ADD CONSTRAINT [PK__TIAdjust__3214EC2755A1136A] PRIMARY KEY CLUSTERED ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MCReceiptDetail]
  ADD [VATAccount] [nvarchar](512) NULL DEFAULT (NULL)
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @value SQL_VARIANT = CAST('' AS varchar(1))
EXEC sys.sp_addextendedproperty N'MS_Description', @value, 'SCHEMA', N'dbo', 'TABLE', N'MCReceiptDetail', 'COLUMN', N'VATAccount'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MCReceipt]
  ADD [InvoiceTemplate] [nvarchar](25) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MCAuditDetailMember]
  ADD [OrderPriority] [int] IDENTITY
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MBDeposit]
  ADD [InvoiceTemplate] [nvarchar](25) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sp_refreshview '[dbo].[ViewVoucherInvisible]'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[GOtherVoucherDetailExpenseAllocation]
  ADD [OrderPriority] [int] IDENTITY
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[GOtherVoucherDetailExpense]
  ADD [OrderPriority] [int] IDENTITY
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @res int
EXEC @res = sp_rename N'[dbo].[GOtherVoucherDetail]', N'tmp_devart_GOtherVoucherDetail', 'OBJECT'
IF @res <> 0
  RAISERROR ('Error while Rename table [dbo].[GOtherVoucherDetail] to [dbo].[tmp_devart_GOtherVoucherDetail]', 11, 1 );
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @res int
EXEC @res = sp_rename N'[dbo].[PK_GOtherVoucherDetail]', N'tmp_devart_PK_GOtherVoucherDetail', 'OBJECT'
IF @res <> 0
  RAISERROR ('Error while Rename index [PK_GOtherVoucherDetail] to [tmp_devart_PK_GOtherVoucherDetail] on table [dbo].[GOtherVoucherDetail]', 11, 1 );
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @res int
EXEC @res = sp_rename N'[dbo].[DF_IsMatch_GOtherVoucherDetail]', N'tmp_devart_DF_IsMatch_GOtherVoucherDetail', 'OBJECT'
IF @res <> 0
  RAISERROR ('Error while Rename default [DF_IsMatch_GOtherVoucherDetail] to [tmp_devart_DF_IsMatch_GOtherVoucherDetail] on table [dbo].[GOtherVoucherDetail]', 11, 1 );
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[GOtherVoucherDetail] (
  [ID] [uniqueidentifier] NOT NULL,
  [GOtherVoucherID] [uniqueidentifier] NOT NULL,
  [Description] [nvarchar](512) NULL,
  [DebitAccount] [nvarchar](25) NULL,
  [CreditAccount] [nvarchar](25) NULL,
  [CurrencyID] [nvarchar](3) NULL,
  [ExchangeRate] [decimal](25, 10) NULL,
  [Amount] [money] NOT NULL,
  [AmountOriginal] [money] NOT NULL,
  [BudgetItemID] [uniqueidentifier] NULL,
  [CostSetID] [uniqueidentifier] NULL,
  [ContractID] [uniqueidentifier] NULL,
  [DebitAccountingObjectID] [uniqueidentifier] NULL,
  [CreditAccountingObjectID] [uniqueidentifier] NULL,
  [EmployeeID] [uniqueidentifier] NULL,
  [StatisticsCodeID] [uniqueidentifier] NULL,
  [DepartmentID] [uniqueidentifier] NULL,
  [BankAccountDetailID] [uniqueidentifier] NULL,
  [ExpenseItemID] [uniqueidentifier] NULL,
  [IsMatch] [bit] NOT NULL CONSTRAINT [DF_IsMatch_GOtherVoucherDetail] DEFAULT (0),
  [MatchDate] [datetime] NULL,
  [CustomProperty1] [nvarchar](512) NULL,
  [CustomProperty2] [nvarchar](512) NULL,
  [CustomProperty3] [nvarchar](512) NULL,
  [IsIrrationalCost] [bit] NULL,
  [OrderPriority] [int] IDENTITY
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

SET IDENTITY_INSERT [dbo].[GOtherVoucherDetail] OFF

INSERT [dbo].[GOtherVoucherDetail](ID, GOtherVoucherID, Description, DebitAccount, CreditAccount, CurrencyID, ExchangeRate, Amount, AmountOriginal, BudgetItemID, CostSetID, ContractID, DebitAccountingObjectID, CreditAccountingObjectID, EmployeeID, StatisticsCodeID, DepartmentID, BankAccountDetailID, ExpenseItemID, IsMatch, MatchDate, CustomProperty1, CustomProperty2, CustomProperty3, IsIrrationalCost)
  SELECT ID, GOtherVoucherID, Description, DebitAccount, CreditAccount, CurrencyID, ExchangeRate, Amount, AmountOriginal, BudgetItemID, CostSetID, ContractID, DebitAccountingObjectID, CreditAccountingObjectID, EmployeeID, StatisticsCodeID, DepartmentID, BankAccountDetailID, ExpenseItemID, IsMatch, MatchDate, CustomProperty1, CustomProperty2, CustomProperty3, IsIrrationalCost FROM [dbo].[tmp_devart_GOtherVoucherDetail] WITH (NOLOCK)

if @@ERROR = 0
  DROP TABLE [dbo].[tmp_devart_GOtherVoucherDetail]

SET IDENTITY_INSERT [dbo].[GOtherVoucherDetail] OFF
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[GOtherVoucherDetail]
  ADD CONSTRAINT [PK_GOtherVoucherDetail] PRIMARY KEY CLUSTERED ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @res int
EXEC @res = sp_rename N'[dbo].[FADepreciationPost].[DepartmentID]', N'ObjectID', 'COLUMN'
IF @res <> 0
  RAISERROR ('Error while Rename column [DepartmentID] to [ObjectID] on table [dbo].[FADepreciationPost]', 11, 1 );
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @res int
EXEC @res = sp_rename N'[dbo].[FADepreciationDetail].[DepartmentID]', N'ObjectID', 'COLUMN'
IF @res <> 0
  RAISERROR ('Error while Rename column [DepartmentID] to [ObjectID] on table [dbo].[FADepreciationDetail]', 11, 1 );
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @res int
EXEC @res = sp_rename N'[dbo].[FADepreciatioAllocation].[DepartmentID]', N'ObjectID', 'COLUMN'
IF @res <> 0
  RAISERROR ('Error while Rename column [DepartmentID] to [ObjectID] on table [dbo].[FADepreciatioAllocation]', 11, 1 );
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER VIEW [dbo].[ViewVouchersCloseBook]
AS
/*Create by tnson 24/08/2011 - View này chỉ giành riêng cho việc kiểm tra các chứng từ chưa ghi sổ trước khi khóa sổ không được dùng cho việc khác */ SELECT dbo.MCPayment.ID, dbo.MCPaymentDetail.ID AS DetailID, dbo.MCPayment.TypeID, 
                         dbo.Type.TypeName, dbo.MCPayment.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MCPayment.Date, dbo.MCPayment.PostedDate, dbo.MCPaymentDetail.DebitAccount, dbo.MCPaymentDetail.CreditAccount, 
                         dbo.MCPayment.CurrencyID, dbo.MCPaymentDetail.AmountOriginal, dbo.MCPaymentDetail.Amount, NULL AS OrgPrice, dbo.MCPayment.Reason, dbo.MCPaymentDetail.Description, dbo.MCPaymentDetail.CostSetID, 
                         CostSet.CostSetCode, dbo.MCPaymentDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, 
                         dbo.AccountingObject.AccountingObjectCode, dbo.MCPayment.EmployeeID, dbo.MCPayment.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL 
                         AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, 
                         dbo.MCPayment.Recorded, dbo.MCPayment.TotalAmount, dbo.MCPayment.TotalAmountOriginal, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL 
                         AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 
                         'MCPayment' AS RefTable
/*case AccountingObject.IsEmployee when 1 then  AccountingObject.AccountingObjectName end AS EmployeeName*/ FROM dbo.MCPayment INNER JOIN
                         dbo.MCPaymentDetail ON dbo.MCPayment.ID = dbo.MCPaymentDetail.MCPaymentID INNER JOIN
                         dbo.Type ON MCPayment.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON MCPaymentDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MCPaymentDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MCPaymentDetail.ContractID
UNION ALL
SELECT        dbo.MCPayment.ID, dbo.MCPaymentDetailTax.ID AS DetailID, dbo.MCPayment.TypeID, dbo.Type.TypeName, dbo.MCPayment.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MCPayment.Date, 
                         dbo.MCPayment.PostedDate, NULL AS DebitAccount, NULL AS CreditAccount, dbo.MCPayment.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.MCPayment.Reason, 
                         dbo.MCPaymentDetailTax.Description, NULL AS CostSetID, NULL AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, NULL AS AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL 
                         AS AccountingObjectCode, NULL AS EmployeeID, dbo.MCPayment.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL 
                         AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, dbo.MCPaymentDetailTax.VATAmountOriginal, dbo.MCPaymentDetailTax.VATAmount, NULL AS Quantity, NULL AS UnitPrice, 
                         dbo.MCPayment.Recorded, dbo.MCPayment.TotalAmount, dbo.MCPayment.TotalAmountOriginal, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL 
                         AS FreightAmount, 1 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MCPayment' AS RefTable
FROM            dbo.MCPayment INNER JOIN
                         dbo.MCPaymentDetailTax ON dbo.MCPayment.ID = dbo.MCPaymentDetailTax.MCPaymentID INNER JOIN
                         dbo.Type ON MCPayment.TypeID = dbo.Type.ID
UNION ALL
SELECT        dbo.MBDeposit.ID, dbo.MBDepositDetail.ID AS DetailID, dbo.MBDeposit.TypeID, dbo.Type.TypeName, dbo.MBDeposit.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBDeposit.Date, dbo.MBDeposit.PostedDate, 
                         dbo.MBDepositDetail.DebitAccount, dbo.MBDepositDetail.CreditAccount, dbo.MBDeposit.CurrencyID, dbo.MBDepositDetail.AmountOriginal, dbo.MBDepositDetail.Amount, NULL AS OrgPrice, dbo.MBDeposit.Reason, 
                         dbo.MBDepositDetail.Description, dbo.MBDepositDetail.CostSetID, CostSet.CostSetCode, dbo.MBDepositDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, dbo.MBDeposit.EmployeeID, dbo.MBDeposit.BranchID, NULL AS FixedAssetID, NULL 
                         AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL 
                         AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.MBDeposit.Recorded, dbo.MBDeposit.TotalAmountOriginal, dbo.MBDeposit.TotalAmount, NULL AS DiscountAmountOriginal, NULL 
                         AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MBDeposit' AS RefTable
FROM            dbo.MBDeposit INNER JOIN
                         dbo.MBDepositDetail ON dbo.MBDeposit.ID = dbo.MBDepositDetail.MBDepositID INNER JOIN
                         dbo.Type ON MBDeposit.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON MBDepositDetail.AccountingObjectID = dbo.AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MBDepositDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MBDepositDetail.ContractID
UNION ALL
SELECT        dbo.MBDeposit.ID, dbo.MBDepositDetailTax.ID AS DetailID, dbo.MBDeposit.TypeID, dbo.Type.TypeName, dbo.MBDeposit.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBDeposit.Date, dbo.MBDeposit.PostedDate, NULL 
                         AS DebitAccount, NULL AS CreditAccount, dbo.MBDeposit.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.MBDeposit.Reason, dbo.MBDepositDetailTax.Description, NULL AS CostSetID, NULL 
                         AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, NULL AS AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL AS AccountingObjectCode, NULL AS EmployeeID, dbo.MBDeposit.BranchID, NULL 
                         AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL 
                         AS VATAccount, dbo.MBDepositDetailTax.VATAmountOriginal, dbo.MBDepositDetailTax.VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.MBDeposit.Recorded, dbo.MBDeposit.TotalAmountOriginal, 
                         dbo.MBDeposit.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 1 AS DetailTax, NULL 
                         AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MBDeposit' AS RefTable
FROM            dbo.MBDeposit INNER JOIN
                         dbo.MBDepositDetailTax ON dbo.MBDeposit.ID = dbo.MBDepositDetailTax.MBDepositID INNER JOIN
                         dbo.Type ON MBDeposit.TypeID = dbo.Type.ID
UNION ALL
SELECT        dbo.MBInternalTransfer.ID, dbo.MBInternalTransferDetail.ID AS DetailID, dbo.MBInternalTransfer.TypeID, dbo.Type.TypeName, dbo.MBInternalTransfer.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBInternalTransfer.Date, 
                         dbo.MBInternalTransfer.PostedDate, dbo.MBInternalTransferDetail.DebitAccount, dbo.MBInternalTransferDetail.CreditAccount, dbo.MBInternalTransferDetail.CurrencyID, dbo.MBInternalTransferDetail.AmountOriginal, 
                         dbo.MBInternalTransferDetail.Amount, NULL AS OrgPrice, dbo.MBInternalTransfer.Reason, dbo.MBInternalTransfer.Reason AS Description, dbo.MBInternalTransferDetail.CostSetID, CostSet.CostSetCode, 
                         dbo.MBInternalTransferDetail.ContractID, EMContract.Code AS ContractCode, NULL AS AccountingObjectID, '' AS AccountingObjectCategoryName, '' AS AccountingObjectCode, NULL AS EmployeeID, 
                         dbo.MBInternalTransfer.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL 
                         AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.MBInternalTransfer.Recorded, dbo.MBInternalTransfer.TotalAmountOriginal, 
                         dbo.MBInternalTransfer.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL 
                         AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MBInternalTransfer' AS RefTable
FROM            dbo.MBInternalTransfer INNER JOIN
                         dbo.MBInternalTransferDetail ON dbo.MBInternalTransfer.ID = dbo.MBInternalTransferDetail.MBInternalTransferID INNER JOIN
                         dbo.Type ON dbo.MBInternalTransfer.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MBInternalTransferDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MBInternalTransferDetail.ContractID
UNION ALL
SELECT        dbo.MBTellerPaper.ID, dbo.MBTellerPaperDetail.ID AS DetailID, dbo.MBTellerPaper.TypeID, dbo.Type.TypeName, dbo.MBTellerPaper.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBTellerPaper.Date, 
                         dbo.MBTellerPaper.PostedDate, dbo.MBTellerPaperDetail.DebitAccount, dbo.MBTellerPaperDetail.CreditAccount, dbo.MBTellerPaper.CurrencyID, dbo.MBTellerPaperDetail.AmountOriginal, 
                         dbo.MBTellerPaperDetail.Amount, NULL AS OrgPrice, dbo.MBTellerPaper.Reason, dbo.MBTellerPaperDetail.Description, dbo.MBTellerPaperDetail.CostSetID, CostSet.CostSetCode, dbo.MBTellerPaperDetail.ContractID, 
                         EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, 
                         dbo.MBTellerPaper.EmployeeID, dbo.MBTellerPaper.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL 
                         AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.MBTellerPaper.Recorded, 
                         dbo.MBTellerPaper.TotalAmountOriginal, dbo.MBTellerPaper.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 
                         0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MBTellerPaper' AS RefTable
FROM            dbo.MBTellerPaper INNER JOIN
                         dbo.MBTellerPaperDetail ON dbo.MBTellerPaper.ID = dbo.MBTellerPaperDetail.MBTellerPaperID INNER JOIN
                         dbo.Type ON dbo.MBTellerPaper.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON MBTellerPaperDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MBTellerPaperDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MBTellerPaperDetail.ContractID
UNION ALL
SELECT        dbo.MBTellerPaper.ID, dbo.MBTellerPaperDetailTax.ID AS DetailID, dbo.MBTellerPaper.TypeID, dbo.Type.TypeName, dbo.MBTellerPaper.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBTellerPaper.Date, 
                         dbo.MBTellerPaper.PostedDate, NULL AS DebitAccount, NULL AS CreditAccount, dbo.MBTellerPaper.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.MBTellerPaper.Reason, 
                         dbo.MBTellerPaperDetailTax.Description, NULL AS CostSetID, NULL AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, NULL AS AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL 
                         AS AccountingObjectCode, NULL AS EmployeeID, dbo.MBTellerPaper.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL 
                         AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, dbo.MBTellerPaperDetailTax.VATAmountOriginal, dbo.MBTellerPaperDetailTax.VATAmount, NULL AS Quantity, NULL AS UnitPrice, 
                         dbo.MBTellerPaper.Recorded, dbo.MBTellerPaper.TotalAmountOriginal, dbo.MBTellerPaper.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL 
                         AS FreightAmountOriginal, NULL AS FreightAmount, 1 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 
                         'MBTellerPaper' AS RefTable
FROM            dbo.MBTellerPaper INNER JOIN
                         dbo.MBTellerPaperDetailTax ON dbo.MBTellerPaper.ID = dbo.MBTellerPaperDetailTax.MBTellerPaperID INNER JOIN
                         dbo.Type ON dbo.MBTellerPaper.TypeID = dbo.Type.ID
UNION ALL
SELECT        dbo.MBCreditCard.ID, dbo.MBCreditCardDetail.ID AS DetailID, dbo.MBCreditCard.TypeID, dbo.Type.TypeName, dbo.MBCreditCard.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBCreditCard.Date, 
                         dbo.MBCreditCard.PostedDate, dbo.MBCreditCardDetail.DebitAccount, dbo.MBCreditCardDetail.CreditAccount, dbo.MBCreditCard.CurrencyID, dbo.MBCreditCardDetail.AmountOriginal, dbo.MBCreditCardDetail.Amount, NULL 
                         AS OrgPrice, dbo.MBCreditCard.Reason, dbo.MBCreditCardDetail.Description, dbo.MBCreditCardDetail.CostSetID, CostSet.CostSetCode, dbo.MBCreditCardDetail.ContractID, EMContract.Code AS ContractCode, 
                         dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, dbo.MBCreditCard.EmployeeID, 
                         dbo.MBCreditCard.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL 
                         AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.MBCreditCard.Recorded, dbo.MBCreditCard.TotalAmountOriginal, 
                         dbo.MBCreditCard.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL 
                         AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MBCreditCard' AS RefTable
FROM            dbo.MBCreditCard INNER JOIN
                         dbo.MBCreditCardDetail ON dbo.MBCreditCard.ID = dbo.MBCreditCardDetail.MBCreditCardID INNER JOIN
                         dbo.Type ON dbo.MBCreditCard.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON MBCreditCardDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MBCreditCardDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MBCreditCardDetail.ContractID
UNION ALL
SELECT        dbo.MBCreditCard.ID, dbo.MBCreditCardDetailTax.ID AS DetailID, dbo.MBCreditCard.TypeID, dbo.Type.TypeName, dbo.MBCreditCard.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBCreditCard.Date, 
                         dbo.MBCreditCard.PostedDate, NULL AS DebitAccount, NULL AS CreditAccount, dbo.MBCreditCard.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.MBCreditCard.Reason, 
                         dbo.MBCreditCardDetailTax.Description, NULL AS CostSetID, NULL AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, NULL AS AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL 
                         AS AccountingObjectCode, NULL AS EmployeeID, dbo.MBCreditCard.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL 
                         AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, dbo.MBCreditCardDetailTax.VATAmountOriginal, dbo.MBCreditCardDetailTax.VATAmount, NULL AS Quantity, NULL AS UnitPrice, 
                         dbo.MBCreditCard.Recorded, dbo.MBCreditCard.TotalAmountOriginal, dbo.MBCreditCard.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL 
                         AS FreightAmountOriginal, NULL AS FreightAmount, 1 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 
                         'MBCreditCard' AS RefTable
FROM            dbo.MBCreditCard INNER JOIN
                         dbo.MBCreditCardDetailTax ON dbo.MBCreditCard.ID = dbo.MBCreditCardDetailTax.MBCreditCardID INNER JOIN
                         dbo.Type ON dbo.MBCreditCard.TypeID = dbo.Type.ID
/* =======================================================================*/ UNION ALL
SELECT        dbo.MCReceipt.ID, dbo.MCReceiptDetail.ID AS DetailID, dbo.MCReceipt.TypeID, dbo.Type.TypeName, dbo.MCReceipt.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MCReceipt.Date, dbo.MCReceipt.PostedDate, 
                         dbo.MCReceiptDetail.DebitAccount, dbo.MCReceiptDetail.CreditAccount, dbo.MCReceipt.CurrencyID, dbo.MCReceiptDetail.AmountOriginal, dbo.MCReceiptDetail.Amount, NULL AS OrgPrice, dbo.MCReceipt.Reason, 
                         dbo.MCReceiptDetail.Description, dbo.MCReceiptDetail.CostSetID, CostSet.CostSetCode, dbo.MCReceiptDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, dbo.MCReceipt.EmployeeID, dbo.MCReceipt.BranchID, NULL AS FixedAssetID, NULL 
                         AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL 
                         AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.MCReceipt.Recorded, dbo.MCReceipt.TotalAmountOriginal, dbo.MCReceipt.TotalAmount, NULL AS DiscountAmountOriginal, NULL 
                         AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MCReceipt' AS RefTable
FROM            dbo.MCReceipt INNER JOIN
                         dbo.MCReceiptDetail ON dbo.MCReceipt.ID = dbo.MCReceiptDetail.MCReceiptID INNER JOIN
                         dbo.Type ON MCReceipt.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON MCReceiptDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MCReceiptDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MCReceiptDetail.ContractID
UNION ALL
SELECT        dbo.MCReceipt.ID, dbo.MCReceiptDetailTax.ID AS DetailID, dbo.MCReceipt.TypeID, dbo.Type.TypeName, dbo.MCReceipt.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MCReceipt.Date, dbo.MCReceipt.PostedDate, NULL 
                         AS DebitAccount, NULL AS CreditAccount, dbo.MCReceipt.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.MCReceipt.Reason, dbo.MCReceiptDetailTax.Description, NULL AS CostSetID, NULL 
                         AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, NULL AS AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL AS AccountingObjectCode, NULL AS EmployeeID, dbo.MCReceipt.BranchID, NULL 
                         AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL 
                         AS VATAccount, dbo.MCReceiptDetailTax.VATAmountOriginal, dbo.MCReceiptDetailTax.VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.MCReceipt.Recorded, dbo.MCReceipt.TotalAmountOriginal, 
                         dbo.MCReceipt.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 1 AS DetailTax, NULL 
                         AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MCReceipt' AS RefTable
FROM            dbo.MCReceipt INNER JOIN
                         dbo.MCReceiptDetailTax ON dbo.MCReceipt.ID = dbo.MCReceiptDetailTax.MCReceiptID INNER JOIN
                         dbo.Type ON MCReceipt.TypeID = dbo.Type.ID
UNION ALL
SELECT        dbo.FAAdjustment.ID, dbo.FAAdjustmentDetail.ID AS DetailID, dbo.FAAdjustment.TypeID, dbo.Type.TypeName, dbo.FAAdjustment.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.FAAdjustment.Date, 
                         dbo.FAAdjustment.PostedDate, dbo.FAAdjustmentDetail.DebitAccount, dbo.FAAdjustmentDetail.CreditAccount, NULL AS CurrencyID, $ 0 AS AmountOriginal, dbo.FAAdjustmentDetail.Amount, $ 0 AS OrgPrice, 
                         dbo.FAAdjustment.Reason, dbo.FAAdjustmentDetail.Description, dbo.FAAdjustmentDetail.CostSetID, CostSet.CostSetCode, dbo.FAAdjustmentDetail.ContractID, EMContract.Code AS ContractCode, 
                         dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, NULL AS EmployeeID, 
                         dbo.FAAdjustment.BranchID, dbo.FixedAsset.ID AS FixedAssetID, dbo.FixedAsset.FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL 
                         AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, $ 0 AS VATAmountOriginal, $ 0 AS VATAmount, $ 0 AS Quantity, $ 0 AS UnitPrice, dbo.FAAdjustment.Recorded, $ 0 AS TotalAmountOriginal, 
                         dbo.FAAdjustment.TotalAmount, $ 0 AS DiscountAmountOriginal, $ 0 AS DiscountAmount, NULL AS DiscountAccount, $ 0 AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL 
                         AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'FAAdjustment' AS RefTable
FROM            dbo.FAAdjustment INNER JOIN
                         dbo.FAAdjustmentDetail ON dbo.FAAdjustment.ID = dbo.FAAdjustmentDetail.FAAdjustmentID INNER JOIN
                         dbo.Type ON FAAdjustment.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON FAAdjustmentDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.FixedAsset ON FAAdjustment.FixedAssetID = dbo.FixedAsset.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = FAAdjustmentDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = FAAdjustmentDetail.ContractID
UNION ALL
SELECT        dbo.FADepreciation.ID, dbo.FADepreciationDetail.ID AS DetailID, dbo.FADepreciation.TypeID, dbo.Type.TypeName, dbo.FADepreciation.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.FADepreciation.Date, 
                         dbo.FADepreciation.PostedDate, dbo.FADepreciationDetail.DebitAccount, dbo.FADepreciationDetail.CreditAccount, FADepreciationDetail.CurrencyID, dbo.FADepreciationDetail.AmountOriginal, dbo.FADepreciationDetail.Amount, 
                         dbo.FADepreciationDetail.OrgPrice, dbo.FADepreciation.Reason, dbo.FADepreciationDetail.Description, dbo.FADepreciationDetail.CostSetID, CostSet.CostSetCode, dbo.FADepreciationDetail.ContractID, 
                         EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, 
                         dbo.FADepreciationDetail.EmployeeID, dbo.FADepreciation.BranchID, dbo.FixedAsset.ID AS FixedAssetID, dbo.FixedAsset.FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL 
                         AS RepositoryID, NULL AS RepositoryName, FADepreciationDetail.ObjectID, Department.DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, 
                         dbo.FADepreciation.Recorded, dbo.FADepreciation.TotalAmountOriginal, dbo.FADepreciation.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL 
                         AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 
                         'FADepreciation' AS RefTable
FROM            dbo.FADepreciation INNER JOIN
                         dbo.FADepreciationDetail ON dbo.FADepreciation.ID = dbo.FADepreciationDetail.FADepreciationID INNER JOIN
                         dbo.Type ON FADepreciation.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON FADepreciationDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.FixedAsset ON FADepreciationDetail.FixedAssetID = dbo.FixedAsset.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = FADepreciationDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = FADepreciationDetail.ContractID LEFT JOIN
                         dbo.Department ON Department.ID = FADepreciationDetail.ObjectID
UNION ALL
SELECT        dbo.FAIncrement.ID, dbo.FAIncrementDetail.ID AS DetailID, dbo.FAIncrement.TypeID, dbo.Type.TypeName, dbo.FAIncrement.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.FAIncrement.Date, dbo.FAIncrement.PostedDate, 
                         dbo.FAIncrementDetail.DebitAccount, dbo.FAIncrementDetail.CreditAccount, FAIncrement.CurrencyID, dbo.FAIncrementDetail.AmountOriginal, dbo.FAIncrementDetail.Amount, dbo.FAIncrementDetail.OrgPriceOriginal AS OrgPrice, 
                         dbo.FAIncrement.Reason, dbo.FAIncrementDetail.Description, dbo.FAIncrementDetail.CostSetID, CostSet.CostSetCode, dbo.FAIncrementDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, dbo.FAIncrement.EmployeeID, dbo.FAIncrement.BranchID, dbo.FixedAsset.ID AS FixedAssetID, 
                         dbo.FixedAsset.FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, FAIncrementDetail.DepartmentID, dbo.Department.DepartmentName, 
                         dbo.FAIncrementDetail.VATAccount AS VATAccount, dbo.FAIncrementDetail.VATAmountOriginal AS VATAmountOriginal, dbo.FAIncrementDetail.VATAmount AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, 
                         dbo.FAIncrement.Recorded, dbo.FAIncrement.TotalAmountOriginal, dbo.FAIncrement.TotalAmount, dbo.FAIncrementDetail.DiscountAmountOriginal, dbo.FAIncrementDetail.DiscountAmount, NULL AS DiscountAccount, 
                         dbo.FAIncrementDetail.FreightAmountOriginal, dbo.FAIncrementDetail.FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'FAIncrement' AS RefTable
FROM            dbo.FAIncrement INNER JOIN
                         dbo.FAIncrementDetail ON dbo.FAIncrement.ID = dbo.FAIncrementDetail.ID INNER JOIN
                         dbo.Type ON FAIncrement.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON FAIncrementDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.FixedAsset ON FAIncrementDetail.FixedAssetID = dbo.FixedAsset.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = FAIncrementDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = FAIncrementDetail.ContractID LEFT JOIN
                         dbo.Department ON Department.ID = FAIncrementDetail.DepartmentID
UNION ALL
SELECT        dbo.FADecrement.ID, dbo.FADecrementDetail.ID AS DetailID, dbo.FADecrement.TypeID, dbo.Type.TypeName, dbo.FADecrement.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.FADecrement.Date, 
                         dbo.FADecrement.PostedDate, dbo.FADecrementDetail.DebitAccount, dbo.FADecrementDetail.CreditAccount, FADecrement.CurrencyID, dbo.FADecrementDetail.AmountOriginal, dbo.FADecrementDetail.Amount, NULL AS OrgPrice, 
                         dbo.FADecrement.Reason, dbo.FADecrementDetail.Description, dbo.FADecrementDetail.CostSetID, CostSet.CostSetCode, dbo.FADecrementDetail.ContractID, EMContract.Code AS ContractCode, 
                         dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, dbo.FADecrementDetail.EmployeeID, 
                         dbo.FADecrement.BranchID, dbo.FixedAsset.ID AS FixedAssetID, dbo.FixedAsset.FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, 
                         FADecrementDetail.DepartmentID, Department.DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.FADecrement.Recorded, 
                         dbo.FADecrement.TotalAmountOriginal, dbo.FADecrement.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 
                         0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'FADecrement' AS RefTable
FROM            dbo.FADecrement INNER JOIN
                         dbo.FADecrementDetail ON dbo.FADecrement.ID = dbo.FADecrementDetail.FADecrementID INNER JOIN
                         dbo.Type ON FADecrement.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON FADecrementDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.FixedAsset ON FADecrementDetail.FixedAssetID = dbo.FixedAsset.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = FADecrementDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = FADecrementDetail.ContractID LEFT JOIN
                         dbo.Department ON Department.ID = FADecrementDetail.DepartmentID
UNION ALL
/*Chứng từ nghiệp vụ khác*/ SELECT dbo.GOtherVoucher.ID, dbo.GOtherVoucherDetail.ID AS DetailID, dbo.GOtherVoucher.TypeID, dbo.Type.TypeName, dbo.GOtherVoucher.No, NULL AS InwardNo, NULL AS OutwardNo, 
                         dbo.GOtherVoucher.Date, dbo.GOtherVoucher.PostedDate, dbo.GOtherVoucherDetail.DebitAccount, dbo.GOtherVoucherDetail.CreditAccount, GOtherVoucherDetail.CurrencyID, dbo.GOtherVoucherDetail.AmountOriginal, 
                         dbo.GOtherVoucherDetail.Amount, NULL AS OrgPrice, dbo.GOtherVoucher.Reason, dbo.GOtherVoucherDetail.Description, dbo.GOtherVoucherDetail.CostSetID, CostSet.CostSetCode, dbo.GOtherVoucherDetail.ContractID, 
                         EMContract.Code AS ContractCode, dbo.GOtherVoucherDetail.DebitAccountingObjectID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, 
                         dbo.AccountingObject.AccountingObjectCode AS AccountingObjectCode, dbo.GOtherVoucherDetail.EmployeeID, dbo.GOtherVoucher.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL 
                         AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL 
                         AS UnitPrice, dbo.GOtherVoucher.Recorded, dbo.GOtherVoucher.TotalAmountOriginal, dbo.GOtherVoucher.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL 
                         AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 
                         'GOtherVoucher' AS RefTable
FROM            dbo.GOtherVoucher INNER JOIN
                         dbo.GOtherVoucherDetail ON dbo.GOtherVoucher.ID = dbo.GOtherVoucherDetail.GOtherVoucherID INNER JOIN
                         dbo.Type ON GOtherVoucher.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON GOtherVoucherDetail.DebitAccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = GOtherVoucherDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = GOtherVoucherDetail.ContractID
UNION ALL
SELECT        dbo.GOtherVoucher.ID, dbo.GOtherVoucherDetailTax.ID AS DetailID, dbo.GOtherVoucher.TypeID, dbo.Type.TypeName, dbo.GOtherVoucher.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.GOtherVoucher.Date, 
                         dbo.GOtherVoucher.PostedDate, NULL AS DebitAccount, NULL AS CreditAccount, GOtherVoucherDetailTax.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.GOtherVoucher.Reason, 
                         dbo.GOtherVoucherDetailTax.Description, NULL AS CostSetID, NULL AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, NULL AS AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL 
                         AccountingObjectCode, NULL AS EmployeeID, dbo.GOtherVoucher.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL 
                         AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, dbo.GOtherVoucherDetailTax.VATAmountOriginal, dbo.GOtherVoucherDetailTax.VATAmount, NULL AS Quantity, NULL AS UnitPrice, 
                         dbo.GOtherVoucher.Recorded, dbo.GOtherVoucher.TotalAmountOriginal, dbo.GOtherVoucher.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL 
                         AS FreightAmountOriginal, NULL AS FreightAmount, 1 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 
                         'GOtherVoucher' AS RefTable
FROM            dbo.GOtherVoucher INNER JOIN
                         dbo.GOtherVoucherDetailTax ON dbo.GOtherVoucher.ID = dbo.GOtherVoucherDetailTax.GOtherVoucherID INNER JOIN
                         dbo.Type ON GOtherVoucher.TypeID = dbo.Type.ID
UNION ALL
SELECT        dbo.RSInwardOutward.ID, dbo.RSInwardOutwardDetail.ID AS DetailID, dbo.RSInwardOutward.TypeID, dbo.Type.TypeName, dbo.RSInwardOutward.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.RSInwardOutward.Date, 
                         dbo.RSInwardOutward.PostedDate, dbo.RSInwardOutwardDetail.DebitAccount, dbo.RSInwardOutwardDetail.CreditAccount, RSInwardOutward.CurrencyID, dbo.RSInwardOutwardDetail.AmountOriginal, 
                         dbo.RSInwardOutwardDetail.Amount, NULL AS OrgPrice, dbo.RSInwardOutward.Reason, dbo.RSInwardOutwardDetail.Description, dbo.RSInwardOutwardDetail.CostSetID, CostSet.CostSetCode, 
                         dbo.RSInwardOutwardDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, 
                         dbo.AccountingObject.AccountingObjectCode, dbo.RSInwardOutwardDetail.EmployeeID, dbo.RSInwardOutward.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, 
                         dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID, dbo.Repository.RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, 
                         RSInwardOutwardDetail.Quantity, RSInwardOutwardDetail.UnitPrice, dbo.RSInwardOutward.Recorded, dbo.RSInwardOutward.TotalAmountOriginal, dbo.RSInwardOutward.TotalAmount, NULL AS DiscountAmountOriginal, NULL 
                         AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'RSInwardOutward' AS RefTable
FROM            dbo.RSInwardOutward INNER JOIN
                         dbo.RSInwardOutwardDetail ON dbo.RSInwardOutward.ID = dbo.RSInwardOutwardDetail.RSInwardOutwardID INNER JOIN
                         dbo.Type ON RSInwardOutward.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON RSInwardOutwardDetail.EmployeeID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON RSInwardOutwardDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON RSInwardOutwardDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = RSInwardOutwardDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = RSInwardOutwardDetail.ContractID
UNION ALL
SELECT        dbo.RSTransfer.ID, dbo.RSTransferDetail.ID AS DetailID, dbo.RSTransfer.TypeID, dbo.Type.TypeName, dbo.RSTransfer.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.RSTransfer.Date, dbo.RSTransfer.PostedDate, 
                         dbo.RSTransferDetail.DebitAccount, dbo.RSTransferDetail.CreditAccount, RSTransfer.CurrencyID, dbo.RSTransferDetail.AmountOriginal, dbo.RSTransferDetail.Amount, NULL AS OrgPrice, dbo.RSTransfer.Reason, 
                         dbo.RSTransferDetail.Description, dbo.RSTransferDetail.CostSetID, CostSet.CostSetCode, dbo.RSTransferDetail.ContractID, EMContract.Code AS ContractCode, NULL AccountingObjectID, NULL 
                         AS AccountingObjectCategoryName, NULL AS AccountingObjectCode, dbo.RSTransferDetail.EmployeeID, dbo.RSTransfer.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, 
                         dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID AS RepositoryID, dbo.Repository.RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL 
                         AS VATAmount, RSTransferDetail.Quantity, RSTransferDetail.UnitPrice, dbo.RSTransfer.Recorded, dbo.RSTransfer.TotalAmountOriginal, dbo.RSTransfer.TotalAmount, NULL AS DiscountAmountOriginal, NULL 
                         AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'RSTransfer' AS RefTable
FROM            dbo.RSTransfer INNER JOIN
                         dbo.RSTransferDetail ON dbo.RSTransfer.ID = dbo.RSTransferDetail.RSTransferID INNER JOIN
                         dbo.Type ON RSTransfer.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.MaterialGoods ON RSTransferDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON RSTransferDetail.ToRepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = RSTransferDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = RSTransferDetail.ContractID
UNION ALL
SELECT        dbo.PPInvoice.ID, dbo.PPInvoiceDetail.ID AS DetailID, dbo.PPInvoice.TypeID, dbo.Type.TypeName, dbo.PPInvoice.No, dbo.PPInvoice.InwardNo AS InwardNo, NULL AS OutwardNo, dbo.PPInvoice.Date, 
                         dbo.PPInvoice.PostedDate, dbo.PPInvoiceDetail.DebitAccount, dbo.PPInvoiceDetail.CreditAccount, PPInvoice.CurrencyID, dbo.PPInvoiceDetail.AmountOriginal, dbo.PPInvoiceDetail.Amount, NULL AS OrgPrice, 
                         dbo.PPInvoice.Reason, dbo.PPInvoiceDetail.Description, dbo.PPInvoiceDetail.CostSetID, CostSet.CostSetCode, dbo.PPInvoiceDetail.ContractID, EMContract.Code AS ContractCode, 
                         dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, PPInvoice.EmployeeID, 
                         dbo.PPInvoice.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID AS RepositoryID, 
                         dbo.Repository.RepositoryName, dbo.PPInvoiceDetail.DepartmentID, NULL AS DepartmentName, PPInvoiceDetail.VATAccount, PPInvoiceDetail.VATAmountOriginal, PPInvoiceDetail.VATAmount, PPInvoiceDetail.Quantity, 
                         PPInvoiceDetail.UnitPrice, dbo.PPInvoice.Recorded, dbo.PPInvoice.TotalAmountOriginal, dbo.PPInvoice.TotalAmount, dbo.PPInvoiceDetail.DiscountAmountOriginal, dbo.PPInvoiceDetail.DiscountAmount, NULL 
                         AS DiscountAccount, dbo.PPInvoiceDetail.FreightAmountOriginal, dbo.PPInvoiceDetail.FreightAmount, 0 AS DetailTax, PPInvoiceDetail.InwardAmountOriginal, PPInvoiceDetail.InwardAmount, 
                         dbo.PPInvoice.StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'PPInvoice' AS RefTable
FROM            dbo.PPInvoice INNER JOIN
                         dbo.PPInvoiceDetail ON dbo.PPInvoice.ID = dbo.PPInvoiceDetail.PPInvoiceID INNER JOIN
                         dbo.Type ON PPInvoice.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON PPInvoiceDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON PPInvoiceDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON PPInvoiceDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = PPInvoiceDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = PPInvoiceDetail.ContractID
UNION ALL
SELECT        dbo.PPDiscountReturn.ID, dbo.PPDiscountReturnDetail.ID AS DetailID, dbo.PPDiscountReturn.TypeID, dbo.Type.TypeName, dbo.PPDiscountReturn.No, NULL AS InwardNo, dbo.PPDiscountReturn.OutwardNo AS OutwardNo, 
                         dbo.PPDiscountReturn.Date, dbo.PPDiscountReturn.PostedDate, dbo.PPDiscountReturnDetail.DebitAccount, dbo.PPDiscountReturnDetail.CreditAccount, PPDiscountReturn.CurrencyID, 
                         dbo.PPDiscountReturnDetail.AmountOriginal, dbo.PPDiscountReturnDetail.Amount, NULL AS OrgPrice, dbo.PPDiscountReturn.Reason, dbo.PPDiscountReturnDetail.Description, dbo.PPDiscountReturnDetail.CostSetID, 
                         CostSet.CostSetCode, dbo.PPDiscountReturnDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, PPDiscountReturn.EmployeeID, dbo.PPDiscountReturn.BrachID AS BranchID, NULL 
                         AS FixedAssetID, NULL AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID AS RepositoryID, dbo.Repository.RepositoryName, NULL 
                         AS DepartmentID, NULL AS DepartmentName, PPDiscountReturnDetail.VATAccount, PPDiscountReturnDetail.VATAmountOriginal, PPDiscountReturnDetail.VATAmount, PPDiscountReturnDetail.Quantity, 
                         PPDiscountReturnDetail.UnitPrice, dbo.PPDiscountReturn.Recorded, dbo.PPDiscountReturn.TotalAmountOriginal, dbo.PPDiscountReturn.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL 
                         AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL 
                         AS OutwardAmount, 'PPDiscountReturn' AS RefTable
FROM            dbo.PPDiscountReturn INNER JOIN
                         dbo.PPDiscountReturnDetail ON dbo.PPDiscountReturn.ID = dbo.PPDiscountReturnDetail.PPDiscountReturnID INNER JOIN
                         dbo.Type ON PPDiscountReturn.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON PPDiscountReturnDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON PPDiscountReturnDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON PPDiscountReturnDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = PPDiscountReturnDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = PPDiscountReturnDetail.ContractID
UNION ALL
SELECT        dbo.PPService.ID, dbo.PPServiceDetail.ID AS DetailID, dbo.PPService.TypeID, dbo.Type.TypeName, dbo.PPService.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.PPService.Date, dbo.PPService.PostedDate, 
                         dbo.PPServiceDetail.DebitAccount, dbo.PPServiceDetail.CreditAccount, PPService.CurrencyID, dbo.PPServiceDetail.AmountOriginal, dbo.PPServiceDetail.Amount, NULL AS OrgPrice, dbo.PPService.Reason, 
                         dbo.PPServiceDetail.Description, dbo.PPServiceDetail.CostSetID, CostSet.CostSetCode, dbo.PPServiceDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, PPService.EmployeeID, dbo.PPService.BranchID, NULL AS FixedAssetID, NULL 
                         AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, 
                         PPServiceDetail.VATAccount, PPServiceDetail.VATAmountOriginal, PPServiceDetail.VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.PPService.Recorded, dbo.PPService.TotalAmountOriginal, dbo.PPService.TotalAmount, 
                         PPServiceDetail.DiscountAmountOriginal, PPServiceDetail.DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL 
                         AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'PPService' AS RefTable
FROM            dbo.PPService INNER JOIN
                         dbo.PPServiceDetail ON dbo.PPService.ID = dbo.PPServiceDetail.PPServiceID INNER JOIN
                         dbo.Type ON PPService.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON PPServiceDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON PPServiceDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = PPServiceDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = PPServiceDetail.ContractID
UNION ALL
SELECT        dbo.PPOrder.ID, dbo.PPOrderDetail.ID AS DetailID, dbo.PPOrder.TypeID, dbo.Type.TypeName, dbo.PPOrder.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.PPOrder.Date, dbo.PPOrder.DeliverDate AS PostedDate, 
                         dbo.PPOrderDetail.DebitAccount, dbo.PPOrderDetail.CreditAccount, PPOrder.CurrencyID, dbo.PPOrderDetail.AmountOriginal, dbo.PPOrderDetail.Amount, NULL AS OrgPrice, dbo.PPOrder.Reason, dbo.PPOrderDetail.Description, 
                         dbo.PPOrderDetail.CostSetID, CostSet.CostSetCode, dbo.PPOrderDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, PPOrder.EmployeeID, dbo.PPOrder.BranchID, NULL AS FixedAssetID, NULL 
                         AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, 
                         PPOrderDetail.VATAccount, PPOrderDetail.VATAmountOriginal, PPOrderDetail.VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.PPOrder.Exported AS Recorded, dbo.PPOrder.TotalAmountOriginal, dbo.PPOrder.TotalAmount, 
                         PPOrderDetail.DiscountAmountOriginal, PPOrderDetail.DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL 
                         AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'PPOrder' AS RefTable
FROM            dbo.PPOrder INNER JOIN
                         dbo.PPOrderDetail ON dbo.PPOrder.ID = dbo.PPOrderDetail.PPOrderID INNER JOIN
                         dbo.Type ON PPOrder.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON PPOrderDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON PPOrderDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = PPOrderDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = PPOrderDetail.ContractID
UNION ALL
SELECT        dbo.SAInvoice.ID, dbo.SAInvoiceDetail.ID AS DetailID, dbo.SAInvoice.TypeID, dbo.Type.TypeName, dbo.SAInvoice.No, NULL AS InwardNo, dbo.SAInvoice.OutwardNo AS OutwardNo, dbo.SAInvoice.Date, 
                         dbo.SAInvoice.PostedDate, dbo.SAInvoiceDetail.DebitAccount, dbo.SAInvoiceDetail.CreditAccount, SAInvoice.CurrencyID, dbo.SAInvoiceDetail.AmountOriginal, dbo.SAInvoiceDetail.Amount, NULL AS OrgPrice, 
                         dbo.SAInvoice.Reason, dbo.SAInvoiceDetail.Description, dbo.SAInvoiceDetail.CostSetID, CostSet.CostSetCode, dbo.SAInvoiceDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, SAInvoice.EmployeeID, dbo.SAInvoice.BranchID, NULL AS FixedAssetID, NULL 
                         AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID AS RepositoryID, dbo.Repository.RepositoryName, NULL AS DepartmentID, NULL 
                         AS DepartmentName, SAInvoiceDetail.VATAccount, SAInvoiceDetail.VATAmountOriginal, SAInvoiceDetail.VATAmount, SAInvoiceDetail.Quantity, SAInvoiceDetail.UnitPrice, dbo.SAInvoice.Recorded, 
                         dbo.SAInvoice.TotalAmountOriginal, dbo.SAInvoice.TotalAmount, dbo.SAInvoiceDetail.DiscountAmountOriginal, dbo.SAInvoiceDetail.DiscountAmount, dbo.SAInvoiceDetail.DiscountAccount, NULL AS FreightAmountOriginal, NULL 
                         AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, dbo.SAInvoiceDetail.OWAmountOriginal AS OutwardAmountOriginal, 
                         dbo.SAInvoiceDetail.OWAmount AS OutwardAmount, 'SAInvoice' AS RefTable
FROM            dbo.SAInvoice INNER JOIN
                         dbo.SAInvoiceDetail ON dbo.SAInvoice.ID = dbo.SAInvoiceDetail.SAInvoiceID INNER JOIN
                         dbo.Type ON dbo.SAInvoice.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON SAInvoiceDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON SAInvoiceDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON SAInvoiceDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = SAInvoiceDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = SAInvoiceDetail.ContractID
UNION ALL
SELECT        dbo.SAReturn.ID, dbo.SAReturnDetail.ID AS DetailID, dbo.SAReturn.TypeID, dbo.Type.TypeName, dbo.SAReturn.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.SAReturn.Date, dbo.SAReturn.PostedDate, NULL 
                         AS DebitAccount, NULL AS CreditAccount, SAReturn.CurrencyID, dbo.SAReturnDetail.AmountOriginal, dbo.SAReturnDetail.Amount, NULL AS OrgPrice, dbo.SAReturn.Reason, dbo.SAReturnDetail.Description, 
                         dbo.SAReturnDetail.CostSetID, CostSet.CostSetCode, dbo.SAReturnDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, SAReturn.EmployeeID, dbo.SAReturn.BranchID, NULL AS FixedAssetID, NULL 
                         AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID AS RepositoryID, dbo.Repository.RepositoryName, NULL AS DepartmentID, NULL 
                         AS DepartmentName, SAReturnDetail.VATAccount, SAReturnDetail.VATAmountOriginal, SAReturnDetail.VATAmount, SAReturnDetail.Quantity, SAReturnDetail.UnitPrice, SAReturn.Recorded AS Posted, 
                         dbo.SAReturn.TotalAmountOriginal, dbo.SAReturn.TotalAmount, dbo.SAReturnDetail.DiscountAmountOriginal, dbo.SAReturnDetail.DiscountAmount, dbo.SAReturnDetail.DiscountAccount, NULL AS FreightAmountOriginal, NULL 
                         AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'SAReturn' AS RefTable
FROM            dbo.SAReturn INNER JOIN
                         dbo.SAReturnDetail ON dbo.SAReturn.ID = dbo.SAReturnDetail.SAReturnID INNER JOIN
                         dbo.Type ON dbo.SAReturn.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON SAReturnDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON SAReturnDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON SAReturnDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = SAReturnDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = SAReturnDetail.ContractID
UNION ALL
SELECT        dbo.SAOrder.ID, dbo.SAOrderDetail.ID AS DetailID, dbo.SAOrder.TypeID, dbo.Type.TypeName, dbo.SAOrder.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.SAOrder.Date, dbo.SAOrder.DeliveDate AS PostedDate, NULL 
                         AS DebitAccount, NULL AS CreditAccount, SAOrder.CurrencyID, dbo.SAOrderDetail.AmountOriginal, dbo.SAOrderDetail.Amount, NULL AS OrgPrice, dbo.SAOrder.Reason, dbo.SAOrderDetail.Description, 
                         dbo.SAOrderDetail.CostSetID, CostSet.CostSetCode, dbo.SAOrderDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, SAOrder.EmployeeID, dbo.SAOrder.BranchID, NULL AS FixedAssetID, NULL 
                         AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID AS RepositoryID, dbo.Repository.RepositoryName, NULL AS DepartmentID, NULL 
                         AS DepartmentName, SAOrderDetail.VATAccount, SAOrderDetail.VATAmountOriginal, SAOrderDetail.VATAmount, SAOrderDetail.Quantity, SAOrderDetail.UnitPrice, SAOrder.Exported AS Recorded, dbo.SAOrder.TotalAmountOriginal, 
                         dbo.SAOrder.TotalAmount, dbo.SAOrderDetail.DiscountAmountOriginal, dbo.SAOrderDetail.DiscountAmount, dbo.SAOrderDetail.DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL 
                         AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'SAOrder' AS RefTable
FROM            dbo.SAOrder INNER JOIN
                         dbo.SAOrderDetail ON dbo.SAOrder.ID = dbo.SAOrderDetail.SAOrderID INNER JOIN
                         dbo.Type ON dbo.SAOrder.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON SAOrderDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON SAOrderDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON SAOrderDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = SAOrderDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = SAOrderDetail.ContractID
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

GO
CREATE VIEW [dbo].[ViewGLPayExceedCash]
AS
SELECT        NEWID() AS ID, Account, SUM(DebitAmount) AS DebitAmount, SUM(DebitAmountOriginal) AS DebitAmountOriginal, SUM(CreditAmount) AS CreditAmount, SUM(CreditAmountOriginal) AS CreditAmountOriginal
FROM            dbo.GeneralLedger
WHERE        (Account LIKE N'111%') OR
                         (Account LIKE N'112%')
GROUP BY Account
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE dbo.TemplateColumn
  DROP CONSTRAINT FK_TemplateColumn_TemplateDetail
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.MaterialGoodsSpecialTaxGroup SET Unit = N'cái' WHERE ID = '6f8135eb-bffa-48cb-acf1-2612c902927b'
UPDATE dbo.MaterialGoodsSpecialTaxGroup SET Unit = N'cái' WHERE ID = '92e2f8e4-a52c-40cc-b91f-32b7eb4c8448'
UPDATE dbo.MaterialGoodsSpecialTaxGroup SET TaxRate = 75.00, Unit = N'bao' WHERE ID = '29bba896-0640-4766-b7a7-3363258e9304'
UPDATE dbo.MaterialGoodsSpecialTaxGroup SET TaxRate = 15.00, Unit = N'cái' WHERE ID = '64f9235d-ceed-466b-a591-3735c0723e9d'
UPDATE dbo.MaterialGoodsSpecialTaxGroup SET Unit = N'cái' WHERE ID = '825308c2-f727-4651-97c8-47aca01c152e'
UPDATE dbo.MaterialGoodsSpecialTaxGroup SET TaxRate = 10.50, Unit = N'cái' WHERE ID = '3ba757dd-6b89-4e0e-994e-58f7a7298540'
UPDATE dbo.MaterialGoodsSpecialTaxGroup SET Unit = N'cái' WHERE ID = '67be9a0a-2340-4154-95e3-62e10ee74de1'
UPDATE dbo.MaterialGoodsSpecialTaxGroup SET Unit = N'cái' WHERE ID = '731cc25b-d1e8-48d7-8748-7f512d8f3ac6'
UPDATE dbo.MaterialGoodsSpecialTaxGroup SET TaxRate = 55.00, Unit = N'cái' WHERE ID = '851e677b-7efc-4d96-af38-9db163cf4c8a'
UPDATE dbo.MaterialGoodsSpecialTaxGroup SET Unit = N'cái' WHERE ID = 'eae38dc6-1ef1-4982-8459-ad5554484872'
UPDATE dbo.MaterialGoodsSpecialTaxGroup SET Unit = N'lít' WHERE ID = 'b22109e9-376e-481c-b1c4-ae8a5418c027'
UPDATE dbo.MaterialGoodsSpecialTaxGroup SET TaxRate = 60.00, Unit = N'cái' WHERE ID = '2b247f67-76dc-4ac6-bb3e-b835b6beaa3d'
UPDATE dbo.MaterialGoodsSpecialTaxGroup SET Unit = N'lít' WHERE ID = 'f0dd22e0-3e97-4720-bf25-bfa5b91bea0c'
UPDATE dbo.MaterialGoodsSpecialTaxGroup SET Unit = N'lít' WHERE ID = '3eb99e78-62d5-4662-a13f-c2f844df3b42'
UPDATE dbo.MaterialGoodsSpecialTaxGroup SET TaxRate = 7.00, Unit = N'cái' WHERE ID = 'e8d0caa2-0ac0-47dc-96fd-e7aecce54264'
UPDATE dbo.MaterialGoodsSpecialTaxGroup SET Unit = N'cái' WHERE ID = '8fcc4cb3-b84f-4ee6-8644-fed585ad1f36'
UPDATE dbo.MaterialGoodsSpecialTaxGroup SET Unit = N'cái' WHERE ID = '8db1776f-e188-4e47-acea-ffb6725259a9'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

INSERT dbo.SystemOption(ID, Code, Name, Type, Data, DefaultData, Note, IsSecurity) VALUES (129, N'SignType', N'Ký server hoặc ký token', 0, N'1', N'1', N'', 0)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.TemplateColumn SET ColumnName = N'InvoiceTemplate', VisiblePosition = 2 WHERE ID = '6ff39cdb-63f0-49b8-8e88-22adb8add741'
UPDATE dbo.TemplateColumn SET VisiblePosition = 3 WHERE ID = 'a00a2f67-239d-4570-96d5-70825e7e4464'
UPDATE dbo.TemplateColumn SET VisiblePosition = 5 WHERE ID = '56445fdf-7ece-4ad1-bb96-7897f0ee224d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 4 WHERE ID = '676249a9-51ee-4675-ba08-78bfb24fef4e'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Số hóa đơn', VisiblePosition = 3 WHERE ID = '522f5407-2357-4bf4-8b63-7d597f93858e'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'b02404d0-e612-4cb7-bf8b-94795230a119'
UPDATE dbo.TemplateColumn SET ColumnName = N'InvoiceTypeID', ColumnCaption = N'Loại hóa đơn' WHERE ID = '4b16a9c1-c284-4f13-879b-a74c900923f5'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Ký hiệu hóa đơn', VisiblePosition = 2 WHERE ID = '8858c8fd-d5a9-436f-8825-b6ea21c40e4e'
UPDATE dbo.TemplateColumn SET ColumnName = N'InvoiceTypeID' WHERE ID = '8af76b08-d4cb-4fe6-8633-b9af9da83fb6'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Ngày hóa đơn', VisiblePosition = 4 WHERE ID = 'a13c9b5d-3561-467e-816b-caddfdaab254'
UPDATE dbo.TemplateColumn SET ColumnName = N'ObjectID' WHERE ID = 'e694f3e3-b588-43ad-a134-e54472bddfb4'
UPDATE dbo.TemplateColumn SET ColumnName = N'ObjectID', ColumnCaption = N'Đối tượng PB' WHERE ID = '1c14b9dc-86b7-416c-bff7-ed8a914dc9fd'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '0485fccf-741b-453e-90ce-f107cbf79114'
UPDATE dbo.TemplateColumn SET ColumnName = N'InvoiceTemplate', ColumnCaption = N'Mẫu số hóa đơn' WHERE ID = '900c91ba-3339-47e5-a01f-f2ded201fbfb'
UPDATE dbo.TemplateColumn SET ColumnName = N'ObjectID', ColumnCaption = N'Đối tượng PB' WHERE ID = 'a73976a9-d078-4eba-81ff-fb133dbcdfc7'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

SET IDENTITY_INSERT dbo.VoucherPatternsReport ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (223, 0, 0, N'SAReturn-BH-HDBHTT', N'Hóa đơn bán hàng', N'SAReturn-BH-HDBHTT.rst', 1, N'340')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (224, 0, 0, N'SAReturn-BH-HDBH', N'Hóa đơn GTGT', N'SAReturn-BH-HDBH.rst', 1, N'340')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (225, 0, 0, N'SABill-BH-HDBH', N'Hóa đơn GTGT', N'SABill-BH-HDBH.rst', 1, N'326')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (226, 0, 0, N'SABill-BH-HDBHTT', N'Hóa đơn bán hàng', N'SABill-BH-HDBHTT.rst', 1, N'326')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (227, 0, 0, N'PPDiscount-MH-HDBH', N'Hóa đơn GTGT', N'PPDiscount-MH-HDBH.rst', 1, N'220')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (228, 0, 0, N'PPDiscount-MH-HDBHTT', N'Hóa đơn bán hàng', N'PPDiscount-MH-HDBHTT.rst', 1, N'220')
GO
SET IDENTITY_INSERT dbo.VoucherPatternsReport OFF
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.SystemOption SET Data = '19.2.27.22' WHERE Code = 'DB_Version'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DBCC CHECKIDENT('dbo.VoucherPatternsReport', RESEED, 228)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateColumn WITH NOCHECK
  ADD CONSTRAINT FK_TemplateColumn_TemplateDetail FOREIGN KEY (TemplateDetailID) REFERENCES dbo.TemplateDetail(ID) ON DELETE CASCADE ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

IF @@TRANCOUNT>0 COMMIT TRANSACTION
GO

SET NOEXEC OFF
GO
