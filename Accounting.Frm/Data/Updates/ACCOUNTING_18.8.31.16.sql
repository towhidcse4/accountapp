
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

BEGIN TRANSACTION
GO

DROP TABLE [dbo].[  ]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[Backup] (
  [ID] [uniqueidentifier] NOT NULL,
  [FileName] [varchar](250) NOT NULL,
  [FilePath] [varchar](2000) NOT NULL,
  [CreateDate] [datetime] NOT NULL,
  [SyncState] [bit] NULL,
  [SyncTime] [datetime] NULL,
  [SyncVersion] [nvarchar](500) NULL DEFAULT (NULL)
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
DROP VIEW [dbo].[ViewVouchersCloseBook]
GO
CREATE VIEW [dbo].[ViewVouchersCloseBook]
AS
/*Create by tnson 24/08/2011 - View này chỉ giành riêng cho việc kiểm tra các chứng từ chưa ghi sổ trước khi khóa sổ không được dùng cho việc khác */ 
SELECT dbo.MCPayment.ID, dbo.MCPaymentDetail.ID AS DetailID, 
                         dbo.MCPayment.TypeID, dbo.Type.TypeName, dbo.MCPayment.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MCPayment.Date, dbo.MCPayment.PostedDate, dbo.MCPaymentDetail.DebitAccount, 
                         dbo.MCPaymentDetail.CreditAccount, dbo.MCPayment.CurrencyID, dbo.MCPaymentDetail.AmountOriginal, dbo.MCPaymentDetail.Amount, NULL AS OrgPrice, dbo.MCPayment.Reason, 
                         dbo.MCPaymentDetail.Description, dbo.MCPaymentDetail.CostSetID, CostSet.CostSetCode, dbo.MCPaymentDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, dbo.MCPayment.EmployeeID, dbo.MCPayment.BranchID, NULL 
                         AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL 
                         AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.MCPayment.Recorded, dbo.MCPayment.TotalAmount, 
                         dbo.MCPayment.TotalAmountOriginal, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL 
                         AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 
                         'MCPayment' AS RefTable
/*case AccountingObject.IsEmployee when 1 then  AccountingObject.AccountingObjectName end AS EmployeeName*/ 
FROM dbo.MCPayment INNER JOIN
                         dbo.MCPaymentDetail ON dbo.MCPayment.ID = dbo.MCPaymentDetail.MCPaymentID INNER JOIN
                         dbo.Type ON MCPayment.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON MCPaymentDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MCPaymentDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MCPaymentDetail.ContractID

UNION ALL
SELECT        dbo.MCPayment.ID, dbo.MCPaymentDetailTax.ID AS DetailID, dbo.MCPayment.TypeID, dbo.Type.TypeName, dbo.MCPayment.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MCPayment.Date, 
                         dbo.MCPayment.PostedDate, NULL AS DebitAccount, NULL AS CreditAccount, dbo.MCPayment.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.MCPayment.Reason, 
                         dbo.MCPaymentDetailTax.Description, NULL AS CostSetID, NULL AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, NULL AS AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL 
                         AS AccountingObjectCode, NULL AS EmployeeID, dbo.MCPayment.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL 
                         AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, dbo.MCPaymentDetailTax.VATAmountOriginal, dbo.MCPaymentDetailTax.VATAmount, NULL 
                         AS Quantity, NULL AS UnitPrice, dbo.MCPayment.Recorded, dbo.MCPayment.TotalAmount, dbo.MCPayment.TotalAmountOriginal, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL 
                         AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 1 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MCPayment' AS RefTable
FROM            dbo.MCPayment INNER JOIN
                         dbo.MCPaymentDetailTax ON dbo.MCPayment.ID = dbo.MCPaymentDetailTax.MCPaymentID INNER JOIN
                         dbo.Type ON MCPayment.TypeID = dbo.Type.ID

UNION ALL
SELECT        dbo.MBDeposit.ID, dbo.MBDepositDetail.ID AS DetailID, dbo.MBDeposit.TypeID, dbo.Type.TypeName, dbo.MBDeposit.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBDeposit.Date, 
                         dbo.MBDeposit.PostedDate, dbo.MBDepositDetail.DebitAccount, dbo.MBDepositDetail.CreditAccount, dbo.MBDeposit.CurrencyID, dbo.MBDepositDetail.AmountOriginal, dbo.MBDepositDetail.Amount, NULL 
                         AS OrgPrice, dbo.MBDeposit.Reason, dbo.MBDepositDetail.Description, dbo.MBDepositDetail.CostSetID, CostSet.CostSetCode, dbo.MBDepositDetail.ContractID, EMContract.Code AS ContractCode, 
                         dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, dbo.MBDeposit.EmployeeID, 
                         dbo.MBDeposit.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL 
                         AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.MBDeposit.Recorded, 
                         dbo.MBDeposit.TotalAmountOriginal, dbo.MBDeposit.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL 
                         AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 
                         'MBDeposit' AS RefTable
FROM            dbo.MBDeposit INNER JOIN
                         dbo.MBDepositDetail ON dbo.MBDeposit.ID = dbo.MBDepositDetail.MBDepositID INNER JOIN
                         dbo.Type ON MBDeposit.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON MBDepositDetail.AccountingObjectID = dbo.AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MBDepositDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MBDepositDetail.ContractID

UNION ALL
SELECT        dbo.MBDeposit.ID, dbo.MBDepositDetailTax.ID AS DetailID, dbo.MBDeposit.TypeID, dbo.Type.TypeName, dbo.MBDeposit.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBDeposit.Date, 
                         dbo.MBDeposit.PostedDate, NULL AS DebitAccount, NULL AS CreditAccount, dbo.MBDeposit.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.MBDeposit.Reason, 
                         dbo.MBDepositDetailTax.Description, NULL AS CostSetID, NULL AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, NULL AS AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL 
                         AS AccountingObjectCode, NULL AS EmployeeID, dbo.MBDeposit.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL 
                         AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, dbo.MBDepositDetailTax.VATAmountOriginal, dbo.MBDepositDetailTax.VATAmount, NULL 
                         AS Quantity, NULL AS UnitPrice, dbo.MBDeposit.Recorded, dbo.MBDeposit.TotalAmountOriginal, dbo.MBDeposit.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL 
                         AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 1 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MBDeposit' AS RefTable
FROM            dbo.MBDeposit INNER JOIN
                         dbo.MBDepositDetailTax ON dbo.MBDeposit.ID = dbo.MBDepositDetailTax.MBDepositID INNER JOIN
                         dbo.Type ON MBDeposit.TypeID = dbo.Type.ID

UNION ALL
SELECT        dbo.MBInternalTransfer.ID, dbo.MBInternalTransferDetail.ID AS DetailID, dbo.MBInternalTransfer.TypeID, dbo.Type.TypeName, dbo.MBInternalTransfer.No, NULL AS InwardNo, NULL AS OutwardNo, 
                         dbo.MBInternalTransfer.Date, dbo.MBInternalTransfer.PostedDate, dbo.MBInternalTransferDetail.DebitAccount, dbo.MBInternalTransferDetail.CreditAccount, dbo.MBInternalTransferDetail.CurrencyID, 
                         dbo.MBInternalTransferDetail.AmountOriginal, dbo.MBInternalTransferDetail.Amount, NULL AS OrgPrice, dbo.MBInternalTransfer.Reason, dbo.MBInternalTransfer.Reason AS Description, 
                         dbo.MBInternalTransferDetail.CostSetID, CostSet.CostSetCode, dbo.MBInternalTransferDetail.ContractID, EMContract.Code AS ContractCode, NULL AS AccountingObjectID, '' AS AccountingObjectCategoryName, 
                         '' AS AccountingObjectCode, NULL AS EmployeeID, dbo.MBInternalTransfer.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL 
                         AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, 
                         dbo.MBInternalTransfer.Recorded, dbo.MBInternalTransfer.TotalAmountOriginal, dbo.MBInternalTransfer.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL 
                         AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MBInternalTransfer' AS RefTable
FROM            dbo.MBInternalTransfer INNER JOIN
                         dbo.MBInternalTransferDetail ON dbo.MBInternalTransfer.ID = dbo.MBInternalTransferDetail.MBInternalTransferID INNER JOIN
                         dbo.Type ON dbo.MBInternalTransfer.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MBInternalTransferDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MBInternalTransferDetail.ContractID

UNION ALL
SELECT        dbo.MBTellerPaper.ID, dbo.MBTellerPaperDetail.ID AS DetailID, dbo.MBTellerPaper.TypeID, dbo.Type.TypeName, dbo.MBTellerPaper.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBTellerPaper.Date, 
                         dbo.MBTellerPaper.PostedDate, dbo.MBTellerPaperDetail.DebitAccount, dbo.MBTellerPaperDetail.CreditAccount, dbo.MBTellerPaper.CurrencyID, dbo.MBTellerPaperDetail.AmountOriginal, 
                         dbo.MBTellerPaperDetail.Amount, NULL AS OrgPrice, dbo.MBTellerPaper.Reason, dbo.MBTellerPaperDetail.Description, dbo.MBTellerPaperDetail.CostSetID, CostSet.CostSetCode, 
                         dbo.MBTellerPaperDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, 
                         dbo.AccountingObject.AccountingObjectCode, dbo.MBTellerPaper.EmployeeID, dbo.MBTellerPaper.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL 
                         AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL 
                         AS Quantity, NULL AS UnitPrice, dbo.MBTellerPaper.Recorded, dbo.MBTellerPaper.TotalAmountOriginal, dbo.MBTellerPaper.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL 
                         AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MBTellerPaper' AS RefTable
FROM            dbo.MBTellerPaper INNER JOIN
                         dbo.MBTellerPaperDetail ON dbo.MBTellerPaper.ID = dbo.MBTellerPaperDetail.MBTellerPaperID INNER JOIN
                         dbo.Type ON dbo.MBTellerPaper.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON MBTellerPaperDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MBTellerPaperDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MBTellerPaperDetail.ContractID

UNION ALL
SELECT        dbo.MBTellerPaper.ID, dbo.MBTellerPaperDetailTax.ID AS DetailID, dbo.MBTellerPaper.TypeID, dbo.Type.TypeName, dbo.MBTellerPaper.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBTellerPaper.Date, 
                         dbo.MBTellerPaper.PostedDate, NULL AS DebitAccount, NULL AS CreditAccount, dbo.MBTellerPaper.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.MBTellerPaper.Reason, 
                         dbo.MBTellerPaperDetailTax.Description, NULL AS CostSetID, NULL AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, NULL AS AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL 
                         AS AccountingObjectCode, NULL AS EmployeeID, dbo.MBTellerPaper.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL 
                         AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, dbo.MBTellerPaperDetailTax.VATAmountOriginal, 
                         dbo.MBTellerPaperDetailTax.VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.MBTellerPaper.Recorded, dbo.MBTellerPaper.TotalAmountOriginal, dbo.MBTellerPaper.TotalAmount, NULL 
                         AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 1 AS DetailTax, NULL AS InwardAmountOriginal, NULL 
                         AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MBTellerPaper' AS RefTable
FROM            dbo.MBTellerPaper INNER JOIN
                         dbo.MBTellerPaperDetailTax ON dbo.MBTellerPaper.ID = dbo.MBTellerPaperDetailTax.MBTellerPaperID INNER JOIN
                         dbo.Type ON dbo.MBTellerPaper.TypeID = dbo.Type.ID

UNION ALL
SELECT        dbo.MBCreditCard.ID, dbo.MBCreditCardDetail.ID AS DetailID, dbo.MBCreditCard.TypeID, dbo.Type.TypeName, dbo.MBCreditCard.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBCreditCard.Date, 
                         dbo.MBCreditCard.PostedDate, dbo.MBCreditCardDetail.DebitAccount, dbo.MBCreditCardDetail.CreditAccount, dbo.MBCreditCard.CurrencyID, dbo.MBCreditCardDetail.AmountOriginal, 
                         dbo.MBCreditCardDetail.Amount, NULL AS OrgPrice, dbo.MBCreditCard.Reason, dbo.MBCreditCardDetail.Description, dbo.MBCreditCardDetail.CostSetID, CostSet.CostSetCode, dbo.MBCreditCardDetail.ContractID, 
                         EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, 
                         dbo.AccountingObject.AccountingObjectCode, dbo.MBCreditCard.EmployeeID, dbo.MBCreditCard.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL 
                         AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL 
                         AS Quantity, NULL AS UnitPrice, dbo.MBCreditCard.Recorded, dbo.MBCreditCard.TotalAmountOriginal, dbo.MBCreditCard.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL 
                         AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MBCreditCard' AS RefTable
FROM            dbo.MBCreditCard INNER JOIN
                         dbo.MBCreditCardDetail ON dbo.MBCreditCard.ID = dbo.MBCreditCardDetail.MBCreditCardID INNER JOIN
                         dbo.Type ON dbo.MBCreditCard.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON MBCreditCardDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MBCreditCardDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MBCreditCardDetail.ContractID

UNION ALL
SELECT        dbo.MBCreditCard.ID, dbo.MBCreditCardDetailTax.ID AS DetailID, dbo.MBCreditCard.TypeID, dbo.Type.TypeName, dbo.MBCreditCard.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBCreditCard.Date, 
                         dbo.MBCreditCard.PostedDate, NULL AS DebitAccount, NULL AS CreditAccount, dbo.MBCreditCard.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.MBCreditCard.Reason, 
                         dbo.MBCreditCardDetailTax.Description, NULL AS CostSetID, NULL AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, NULL AS AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL 
                         AS AccountingObjectCode, NULL AS EmployeeID, dbo.MBCreditCard.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL 
                         AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, dbo.MBCreditCardDetailTax.VATAmountOriginal, 
                         dbo.MBCreditCardDetailTax.VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.MBCreditCard.Recorded, dbo.MBCreditCard.TotalAmountOriginal, dbo.MBCreditCard.TotalAmount, NULL 
                         AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 1 AS DetailTax, NULL AS InwardAmountOriginal, NULL 
                         AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MBCreditCard' AS RefTable
FROM            dbo.MBCreditCard INNER JOIN
                         dbo.MBCreditCardDetailTax ON dbo.MBCreditCard.ID = dbo.MBCreditCardDetailTax.MBCreditCardID INNER JOIN
                         dbo.Type ON dbo.MBCreditCard.TypeID = dbo.Type.ID
/* =======================================================================*/ 
UNION ALL
SELECT        dbo.MCReceipt.ID, dbo.MCReceiptDetail.ID AS DetailID, dbo.MCReceipt.TypeID, dbo.Type.TypeName, dbo.MCReceipt.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MCReceipt.Date, 
                         dbo.MCReceipt.PostedDate, dbo.MCReceiptDetail.DebitAccount, dbo.MCReceiptDetail.CreditAccount, dbo.MCReceipt.CurrencyID, dbo.MCReceiptDetail.AmountOriginal, dbo.MCReceiptDetail.Amount, NULL 
                         AS OrgPrice, dbo.MCReceipt.Reason, dbo.MCReceiptDetail.Description, dbo.MCReceiptDetail.CostSetID, CostSet.CostSetCode, dbo.MCReceiptDetail.ContractID, EMContract.Code AS ContractCode, 
                         dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, dbo.MCReceipt.EmployeeID, 
                         dbo.MCReceipt.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL 
                         AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.MCReceipt.Recorded, 
                         dbo.MCReceipt.TotalAmountOriginal, dbo.MCReceipt.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL 
                         AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 
                         'MCReceipt' AS RefTable
FROM            dbo.MCReceipt INNER JOIN
                         dbo.MCReceiptDetail ON dbo.MCReceipt.ID = dbo.MCReceiptDetail.MCReceiptID INNER JOIN
                         dbo.Type ON MCReceipt.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON MCReceiptDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MCReceiptDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MCReceiptDetail.ContractID

UNION ALL
SELECT        dbo.MCReceipt.ID, dbo.MCReceiptDetailTax.ID AS DetailID, dbo.MCReceipt.TypeID, dbo.Type.TypeName, dbo.MCReceipt.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MCReceipt.Date, 
                         dbo.MCReceipt.PostedDate, NULL AS DebitAccount, NULL AS CreditAccount, dbo.MCReceipt.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.MCReceipt.Reason, 
                         dbo.MCReceiptDetailTax.Description, NULL AS CostSetID, NULL AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, NULL AS AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL 
                         AS AccountingObjectCode, NULL AS EmployeeID, dbo.MCReceipt.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL 
                         AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, dbo.MCReceiptDetailTax.VATAmountOriginal, dbo.MCReceiptDetailTax.VATAmount, NULL 
                         AS Quantity, NULL AS UnitPrice, dbo.MCReceipt.Recorded, dbo.MCReceipt.TotalAmountOriginal, dbo.MCReceipt.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL 
                         AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 1 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MCReceipt' AS RefTable
FROM            dbo.MCReceipt INNER JOIN
                         dbo.MCReceiptDetailTax ON dbo.MCReceipt.ID = dbo.MCReceiptDetailTax.MCReceiptID INNER JOIN
                         dbo.Type ON MCReceipt.TypeID = dbo.Type.ID

UNION ALL
SELECT        dbo.FAAdjustment.ID, dbo.FAAdjustmentDetail.ID AS DetailID, dbo.FAAdjustment.TypeID, dbo.Type.TypeName, dbo.FAAdjustment.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.FAAdjustment.Date, 
                         dbo.FAAdjustment.PostedDate, dbo.FAAdjustmentDetail.DebitAccount, dbo.FAAdjustmentDetail.CreditAccount, NULL AS CurrencyID, $ 0 AS AmountOriginal, dbo.FAAdjustmentDetail.Amount, $ 0 AS OrgPrice, 
                         dbo.FAAdjustment.Reason, dbo.FAAdjustmentDetail.Description, dbo.FAAdjustmentDetail.CostSetID, CostSet.CostSetCode, dbo.FAAdjustmentDetail.ContractID, EMContract.Code AS ContractCode, 
                         dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, NULL AS EmployeeID, 
                         dbo.FAAdjustment.BranchID, dbo.FixedAsset.ID AS FixedAssetID, dbo.FixedAsset.FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL 
                         AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, $ 0 AS VATAmountOriginal, $ 0 AS VATAmount, $ 0 AS Quantity, $ 0 AS UnitPrice, dbo.FAAdjustment.Recorded, 
                         $ 0 AS TotalAmountOriginal, dbo.FAAdjustment.TotalAmount, $ 0 AS DiscountAmountOriginal, $ 0 AS DiscountAmount, NULL AS DiscountAccount, $ 0 AS FreightAmountOriginal, NULL AS FreightAmount, 
                         0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'FAAdjustment' AS RefTable
FROM            dbo.FAAdjustment INNER JOIN
                         dbo.FAAdjustmentDetail ON dbo.FAAdjustment.ID = dbo.FAAdjustmentDetail.FAAdjustmentID INNER JOIN
                         dbo.Type ON FAAdjustment.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON FAAdjustmentDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.FixedAsset ON FAAdjustment.FixedAssetID = dbo.FixedAsset.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = FAAdjustmentDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = FAAdjustmentDetail.ContractID

UNION ALL
SELECT        dbo.FADepreciation.ID, dbo.FADepreciationDetail.ID AS DetailID, dbo.FADepreciation.TypeID, dbo.Type.TypeName, dbo.FADepreciation.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.FADepreciation.Date, 
                         dbo.FADepreciation.PostedDate, dbo.FADepreciationDetail.DebitAccount, dbo.FADepreciationDetail.CreditAccount, FADepreciationDetail.CurrencyID, dbo.FADepreciationDetail.AmountOriginal, 
                         dbo.FADepreciationDetail.Amount, dbo.FADepreciationDetail.OrgPrice, dbo.FADepreciation.Reason, dbo.FADepreciationDetail.Description, dbo.FADepreciationDetail.CostSetID, CostSet.CostSetCode, 
                         dbo.FADepreciationDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, 
                         dbo.AccountingObject.AccountingObjectCode, dbo.FADepreciationDetail.EmployeeID, dbo.FADepreciation.BranchID, dbo.FixedAsset.ID AS FixedAssetID, dbo.FixedAsset.FixedAssetName, NULL 
                         AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, FADepreciationDetail.DepartmentID, Department.DepartmentName, NULL AS VATAccount, NULL 
                         AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.FADepreciation.Recorded, dbo.FADepreciation.TotalAmountOriginal, dbo.FADepreciation.TotalAmount, NULL 
                         AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL 
                         AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'FADepreciation' AS RefTable
FROM            dbo.FADepreciation INNER JOIN
                         dbo.FADepreciationDetail ON dbo.FADepreciation.ID = dbo.FADepreciationDetail.FADepreciationID INNER JOIN
                         dbo.Type ON FADepreciation.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON FADepreciationDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.FixedAsset ON FADepreciationDetail.FixedAssetID = dbo.FixedAsset.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = FADepreciationDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = FADepreciationDetail.ContractID LEFT JOIN
                         dbo.Department ON Department.ID = FADepreciationDetail.DepartmentID

UNION ALL
SELECT        dbo.FAIncrement.ID, dbo.FAIncrementDetail.ID AS DetailID, dbo.FAIncrement.TypeID, dbo.Type.TypeName, dbo.FAIncrement.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.FAIncrement.Date, 
                         dbo.FAIncrement.PostedDate, dbo.FAIncrementDetail.DebitAccount, dbo.FAIncrementDetail.CreditAccount, FAIncrement.CurrencyID, dbo.FAIncrementDetail.AmountOriginal, dbo.FAIncrementDetail.Amount, 
                         dbo.FAIncrementDetail.OrgPriceOriginal AS OrgPrice, dbo.FAIncrement.Reason, dbo.FAIncrementDetail.Description, dbo.FAIncrementDetail.CostSetID, CostSet.CostSetCode, dbo.FAIncrementDetail.ContractID, 
                         EMContract.Code AS ContractCode, dbo.AccountingObject.ID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, 
                         dbo.FAIncrement.EmployeeID, dbo.FAIncrement.BranchID, dbo.FixedAsset.ID AS FixedAssetID, dbo.FixedAsset.FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL 
                         AS RepositoryID, NULL AS RepositoryName, FAIncrementDetail.DepartmentID, dbo.Department.DepartmentName, dbo.FAIncrementDetail.VATAccount AS VATAccount, 
                         dbo.FAIncrementDetail.VATAmountOriginal AS VATAmountOriginal, dbo.FAIncrementDetail.VATAmount AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.FAIncrement.Recorded, 
                         dbo.FAIncrement.TotalAmountOriginal, dbo.FAIncrement.TotalAmount, dbo.FAIncrementDetail.DiscountAmountOriginal, dbo.FAIncrementDetail.DiscountAmount, NULL AS DiscountAccount, 
                         dbo.FAIncrementDetail.FreightAmountOriginal, dbo.FAIncrementDetail.FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'FAIncrement' AS RefTable
FROM            dbo.FAIncrement INNER JOIN
                         dbo.FAIncrementDetail ON dbo.FAIncrement.ID = dbo.FAIncrementDetail.ID INNER JOIN
                         dbo.Type ON FAIncrement.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON FAIncrementDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.FixedAsset ON FAIncrementDetail.FixedAssetID = dbo.FixedAsset.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = FAIncrementDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = FAIncrementDetail.ContractID LEFT JOIN
                         dbo.Department ON Department.ID = FAIncrementDetail.DepartmentID

UNION ALL
SELECT        dbo.FADecrement.ID, dbo.FADecrementDetail.ID AS DetailID, dbo.FADecrement.TypeID, dbo.Type.TypeName, dbo.FADecrement.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.FADecrement.Date, 
                         dbo.FADecrement.PostedDate, dbo.FADecrementDetail.DebitAccount, dbo.FADecrementDetail.CreditAccount, FADecrement.CurrencyID, dbo.FADecrementDetail.AmountOriginal, 
                         dbo.FADecrementDetail.Amount, NULL AS OrgPrice, dbo.FADecrement.Reason, dbo.FADecrementDetail.Description, dbo.FADecrementDetail.CostSetID, CostSet.CostSetCode, dbo.FADecrementDetail.ContractID, 
                         EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, 
                         dbo.AccountingObject.AccountingObjectCode, dbo.FADecrementDetail.EmployeeID, dbo.FADecrement.BranchID, dbo.FixedAsset.ID AS FixedAssetID, dbo.FixedAsset.FixedAssetName, NULL 
                         AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, FADecrementDetail.DepartmentID, Department.DepartmentName, NULL AS VATAccount, NULL 
                         AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.FADecrement.Recorded, dbo.FADecrement.TotalAmountOriginal, dbo.FADecrement.TotalAmount, NULL 
                         AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL 
                         AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'FADecrement' AS RefTable
FROM            dbo.FADecrement INNER JOIN
                         dbo.FADecrementDetail ON dbo.FADecrement.ID = dbo.FADecrementDetail.FADecrementID INNER JOIN
                         dbo.Type ON FADecrement.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON FADecrementDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.FixedAsset ON FADecrementDetail.FixedAssetID = dbo.FixedAsset.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = FADecrementDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = FADecrementDetail.ContractID LEFT JOIN
                         dbo.Department ON Department.ID = FADecrementDetail.DepartmentID

UNION ALL
/*Chứng từ nghiệp vụ khác*/ 
SELECT dbo.GOtherVoucher.ID, dbo.GOtherVoucherDetail.ID AS DetailID, dbo.GOtherVoucher.TypeID, dbo.Type.TypeName, dbo.GOtherVoucher.No, NULL AS InwardNo, NULL AS OutwardNo, 
                         dbo.GOtherVoucher.Date, dbo.GOtherVoucher.PostedDate, dbo.GOtherVoucherDetail.DebitAccount, dbo.GOtherVoucherDetail.CreditAccount, GOtherVoucherDetail.CurrencyID, 
                         dbo.GOtherVoucherDetail.AmountOriginal, dbo.GOtherVoucherDetail.Amount, NULL AS OrgPrice, dbo.GOtherVoucher.Reason, dbo.GOtherVoucherDetail.Description, dbo.GOtherVoucherDetail.CostSetID, 
                         CostSet.CostSetCode, dbo.GOtherVoucherDetail.ContractID, EMContract.Code AS ContractCode, dbo.GOtherVoucherDetail.DebitAccountingObjectID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode AS AccountingObjectCode, dbo.GOtherVoucherDetail.EmployeeID, 
                         dbo.GOtherVoucher.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL 
                         AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.GOtherVoucher.Recorded, 
                         dbo.GOtherVoucher.TotalAmountOriginal, dbo.GOtherVoucher.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL 
                         AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 
                         'GOtherVoucher' AS RefTable
FROM            dbo.GOtherVoucher INNER JOIN
                         dbo.GOtherVoucherDetail ON dbo.GOtherVoucher.ID = dbo.GOtherVoucherDetail.GOtherVoucherID INNER JOIN
                         dbo.Type ON GOtherVoucher.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON GOtherVoucherDetail.DebitAccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = GOtherVoucherDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = GOtherVoucherDetail.ContractID

UNION ALL
SELECT        dbo.GOtherVoucher.ID, dbo.GOtherVoucherDetailTax.ID AS DetailID, dbo.GOtherVoucher.TypeID, dbo.Type.TypeName, dbo.GOtherVoucher.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.GOtherVoucher.Date, 
                         dbo.GOtherVoucher.PostedDate, NULL AS DebitAccount, NULL AS CreditAccount, GOtherVoucherDetailTax.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.GOtherVoucher.Reason, 
                         dbo.GOtherVoucherDetailTax.Description, NULL AS CostSetID, NULL AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, NULL AS AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL 
                         AccountingObjectCode, NULL AS EmployeeID, dbo.GOtherVoucher.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL 
                         AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, dbo.GOtherVoucherDetailTax.VATAmountOriginal, 
                         dbo.GOtherVoucherDetailTax.VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.GOtherVoucher.Recorded, dbo.GOtherVoucher.TotalAmountOriginal, dbo.GOtherVoucher.TotalAmount, NULL 
                         AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 1 AS DetailTax, NULL AS InwardAmountOriginal, NULL 
                         AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'GOtherVoucher' AS RefTable
FROM            dbo.GOtherVoucher INNER JOIN
                         dbo.GOtherVoucherDetailTax ON dbo.GOtherVoucher.ID = dbo.GOtherVoucherDetailTax.GOtherVoucherID INNER JOIN
                         dbo.Type ON GOtherVoucher.TypeID = dbo.Type.ID

UNION ALL
SELECT        dbo.RSInwardOutward.ID, dbo.RSInwardOutwardDetail.ID AS DetailID, dbo.RSInwardOutward.TypeID, dbo.Type.TypeName, dbo.RSInwardOutward.No, NULL AS InwardNo, NULL AS OutwardNo, 
                         dbo.RSInwardOutward.Date, dbo.RSInwardOutward.PostedDate, dbo.RSInwardOutwardDetail.DebitAccount, dbo.RSInwardOutwardDetail.CreditAccount, RSInwardOutward.CurrencyID, 
                         dbo.RSInwardOutwardDetail.AmountOriginal, dbo.RSInwardOutwardDetail.Amount, NULL AS OrgPrice, dbo.RSInwardOutward.Reason, dbo.RSInwardOutwardDetail.Description, 
                         dbo.RSInwardOutwardDetail.CostSetID, CostSet.CostSetCode, dbo.RSInwardOutwardDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, dbo.RSInwardOutwardDetail.EmployeeID, dbo.RSInwardOutward.BranchID, NULL 
                         AS FixedAssetID, NULL AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID, dbo.Repository.RepositoryName, NULL 
                         AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, RSInwardOutwardDetail.Quantity, RSInwardOutwardDetail.UnitPrice, 
                         dbo.RSInwardOutward.Recorded, dbo.RSInwardOutward.TotalAmountOriginal, dbo.RSInwardOutward.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL 
                         AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL 
                         AS OutwardAmount, 'RSInwardOutward' AS RefTable
FROM            dbo.RSInwardOutward INNER JOIN
                         dbo.RSInwardOutwardDetail ON dbo.RSInwardOutward.ID = dbo.RSInwardOutwardDetail.RSInwardOutwardID INNER JOIN
                         dbo.Type ON RSInwardOutward.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON RSInwardOutwardDetail.EmployeeID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON RSInwardOutwardDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON RSInwardOutwardDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = RSInwardOutwardDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = RSInwardOutwardDetail.ContractID

UNION ALL
SELECT        dbo.RSTransfer.ID, dbo.RSTransferDetail.ID AS DetailID, dbo.RSTransfer.TypeID, dbo.Type.TypeName, dbo.RSTransfer.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.RSTransfer.Date, 
                         dbo.RSTransfer.PostedDate, dbo.RSTransferDetail.DebitAccount, dbo.RSTransferDetail.CreditAccount, RSTransfer.CurrencyID, dbo.RSTransferDetail.AmountOriginal, dbo.RSTransferDetail.Amount, NULL 
                         AS OrgPrice, dbo.RSTransfer.Reason, dbo.RSTransferDetail.Description, dbo.RSTransferDetail.CostSetID, CostSet.CostSetCode, dbo.RSTransferDetail.ContractID, EMContract.Code AS ContractCode, NULL 
                         AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL AS AccountingObjectCode, dbo.RSTransferDetail.EmployeeID, dbo.RSTransfer.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, 
                         dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID AS RepositoryID, dbo.Repository.RepositoryName, NULL AS DepartmentID, NULL 
                         AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, RSTransferDetail.Quantity, RSTransferDetail.UnitPrice, dbo.RSTransfer.Recorded, 
                         dbo.RSTransfer.TotalAmountOriginal, dbo.RSTransfer.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL 
                         AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 
                         'RSTransfer' AS RefTable
FROM            dbo.RSTransfer INNER JOIN
                         dbo.RSTransferDetail ON dbo.RSTransfer.ID = dbo.RSTransferDetail.RSTransferID INNER JOIN
                         dbo.Type ON RSTransfer.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.MaterialGoods ON RSTransferDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON RSTransferDetail.ToRepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = RSTransferDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = RSTransferDetail.ContractID

UNION ALL
SELECT        dbo.PPInvoice.ID, dbo.PPInvoiceDetail.ID AS DetailID, dbo.PPInvoice.TypeID, dbo.Type.TypeName, dbo.PPInvoice.No, dbo.PPInvoice.InwardNo AS InwardNo, NULL AS OutwardNo, dbo.PPInvoice.Date, 
                         dbo.PPInvoice.PostedDate, dbo.PPInvoiceDetail.DebitAccount, dbo.PPInvoiceDetail.CreditAccount, PPInvoice.CurrencyID, dbo.PPInvoiceDetail.AmountOriginal, dbo.PPInvoiceDetail.Amount, NULL AS OrgPrice, 
                         dbo.PPInvoice.Reason, dbo.PPInvoiceDetail.Description, dbo.PPInvoiceDetail.CostSetID, CostSet.CostSetCode, dbo.PPInvoiceDetail.ContractID, EMContract.Code AS ContractCode, 
                         dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, PPInvoice.EmployeeID, 
                         dbo.PPInvoice.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID AS RepositoryID, 
                         dbo.Repository.RepositoryName, dbo.PPInvoiceDetail.DepartmentID, NULL AS DepartmentName, PPInvoiceDetail.VATAccount, PPInvoiceDetail.VATAmountOriginal, PPInvoiceDetail.VATAmount, 
                         PPInvoiceDetail.Quantity, PPInvoiceDetail.UnitPrice, dbo.PPInvoice.Recorded, dbo.PPInvoice.TotalAmountOriginal, dbo.PPInvoice.TotalAmount, dbo.PPInvoiceDetail.DiscountAmountOriginal, 
                         dbo.PPInvoiceDetail.DiscountAmount, NULL AS DiscountAccount, dbo.PPInvoiceDetail.FreightAmountOriginal, dbo.PPInvoiceDetail.FreightAmount, 0 AS DetailTax, PPInvoiceDetail.InwardAmountOriginal, 
                         PPInvoiceDetail.InwardAmount, dbo.PPInvoice.StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'PPInvoice' AS RefTable
FROM            dbo.PPInvoice INNER JOIN
                         dbo.PPInvoiceDetail ON dbo.PPInvoice.ID = dbo.PPInvoiceDetail.PPInvoiceID INNER JOIN
                         dbo.Type ON PPInvoice.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON PPInvoiceDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON PPInvoiceDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON PPInvoiceDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = PPInvoiceDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = PPInvoiceDetail.ContractID

UNION ALL
SELECT        dbo.PPDiscountReturn.ID, dbo.PPDiscountReturnDetail.ID AS DetailID, dbo.PPDiscountReturn.TypeID, dbo.Type.TypeName, dbo.PPDiscountReturn.No, NULL AS InwardNo, 
                         dbo.PPDiscountReturn.OutwardNo AS OutwardNo, dbo.PPDiscountReturn.Date, dbo.PPDiscountReturn.PostedDate, dbo.PPDiscountReturnDetail.DebitAccount, dbo.PPDiscountReturnDetail.CreditAccount, 
                         PPDiscountReturn.CurrencyID, dbo.PPDiscountReturnDetail.AmountOriginal, dbo.PPDiscountReturnDetail.Amount, NULL AS OrgPrice, dbo.PPDiscountReturn.Reason, dbo.PPDiscountReturnDetail.Description, 
                         dbo.PPDiscountReturnDetail.CostSetID, CostSet.CostSetCode, dbo.PPDiscountReturnDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, PPDiscountReturn.EmployeeID, 
                         dbo.PPDiscountReturn.BrachID AS BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, 
                         dbo.Repository.ID AS RepositoryID, dbo.Repository.RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, PPDiscountReturnDetail.VATAccount, PPDiscountReturnDetail.VATAmountOriginal, 
                         PPDiscountReturnDetail.VATAmount, PPDiscountReturnDetail.Quantity, PPDiscountReturnDetail.UnitPrice, dbo.PPDiscountReturn.Recorded, dbo.PPDiscountReturn.TotalAmountOriginal, 
                         dbo.PPDiscountReturn.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL 
                         AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'PPDiscountReturn' AS RefTable
FROM            dbo.PPDiscountReturn INNER JOIN
                         dbo.PPDiscountReturnDetail ON dbo.PPDiscountReturn.ID = dbo.PPDiscountReturnDetail.PPDiscountReturnID INNER JOIN
                         dbo.Type ON PPDiscountReturn.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON PPDiscountReturnDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON PPDiscountReturnDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON PPDiscountReturnDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = PPDiscountReturnDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = PPDiscountReturnDetail.ContractID

UNION ALL
SELECT        dbo.PPService.ID, dbo.PPServiceDetail.ID AS DetailID, dbo.PPService.TypeID, dbo.Type.TypeName, dbo.PPService.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.PPService.Date, dbo.PPService.PostedDate, 
                         dbo.PPServiceDetail.DebitAccount, dbo.PPServiceDetail.CreditAccount, PPService.CurrencyID, dbo.PPServiceDetail.AmountOriginal, dbo.PPServiceDetail.Amount, NULL AS OrgPrice, dbo.PPService.Reason, 
                         dbo.PPServiceDetail.Description, dbo.PPServiceDetail.CostSetID, CostSet.CostSetCode, dbo.PPServiceDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, PPService.EmployeeID, dbo.PPService.BranchID, NULL AS FixedAssetID, NULL 
                         AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, 
                         PPServiceDetail.VATAccount, PPServiceDetail.VATAmountOriginal, PPServiceDetail.VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.PPService.Recorded, dbo.PPService.TotalAmountOriginal, 
                         dbo.PPService.TotalAmount, PPServiceDetail.DiscountAmountOriginal, PPServiceDetail.DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL
                          AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'PPService' AS RefTable
FROM            dbo.PPService INNER JOIN
                         dbo.PPServiceDetail ON dbo.PPService.ID = dbo.PPServiceDetail.PPServiceID INNER JOIN
                         dbo.Type ON PPService.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON PPServiceDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON PPServiceDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = PPServiceDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = PPServiceDetail.ContractID

UNION ALL
SELECT        dbo.PPOrder.ID, dbo.PPOrderDetail.ID AS DetailID, dbo.PPOrder.TypeID, dbo.Type.TypeName, dbo.PPOrder.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.PPOrder.Date, 
                         dbo.PPOrder.DeliverDate AS PostedDate, dbo.PPOrderDetail.DebitAccount, dbo.PPOrderDetail.CreditAccount, PPOrder.CurrencyID, dbo.PPOrderDetail.AmountOriginal, dbo.PPOrderDetail.Amount, NULL 
                         AS OrgPrice, dbo.PPOrder.Reason, dbo.PPOrderDetail.Description, dbo.PPOrderDetail.CostSetID, CostSet.CostSetCode, dbo.PPOrderDetail.ContractID, EMContract.Code AS ContractCode, 
                         dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, PPOrder.EmployeeID, 
                         dbo.PPOrder.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, NULL AS RepositoryID, NULL 
                         AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, PPOrderDetail.VATAccount, PPOrderDetail.VATAmountOriginal, PPOrderDetail.VATAmount, NULL AS Quantity, NULL AS UnitPrice, 
                         dbo.PPOrder.Exported AS Recorded, dbo.PPOrder.TotalAmountOriginal, dbo.PPOrder.TotalAmount, PPOrderDetail.DiscountAmountOriginal, PPOrderDetail.DiscountAmount, NULL AS DiscountAccount, NULL 
                         AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL 
                         AS OutwardAmount, 'PPOrder' AS RefTable
FROM            dbo.PPOrder INNER JOIN
                         dbo.PPOrderDetail ON dbo.PPOrder.ID = dbo.PPOrderDetail.PPOrderID INNER JOIN
                         dbo.Type ON PPOrder.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON PPOrderDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON PPOrderDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = PPOrderDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = PPOrderDetail.ContractID

UNION ALL
SELECT        dbo.SAInvoice.ID, dbo.SAInvoiceDetail.ID AS DetailID, dbo.SAInvoice.TypeID, dbo.Type.TypeName, dbo.SAInvoice.No, NULL AS InwardNo, dbo.SAInvoice.OutwardNo AS OutwardNo, dbo.SAInvoice.Date, 
                         dbo.SAInvoice.PostedDate, dbo.SAInvoiceDetail.DebitAccount, dbo.SAInvoiceDetail.CreditAccount, SAInvoice.CurrencyID, dbo.SAInvoiceDetail.AmountOriginal, dbo.SAInvoiceDetail.Amount, NULL AS OrgPrice, 
                         dbo.SAInvoice.Reason, dbo.SAInvoiceDetail.Description, dbo.SAInvoiceDetail.CostSetID, CostSet.CostSetCode, dbo.SAInvoiceDetail.ContractID, EMContract.Code AS ContractCode, 
                         dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, SAInvoice.EmployeeID, 
                         dbo.SAInvoice.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID AS RepositoryID, 
                         dbo.Repository.RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, SAInvoiceDetail.VATAccount, SAInvoiceDetail.VATAmountOriginal, SAInvoiceDetail.VATAmount, SAInvoiceDetail.Quantity, 
                         SAInvoiceDetail.UnitPrice, dbo.SAInvoice.Recorded, dbo.SAInvoice.TotalAmountOriginal, dbo.SAInvoice.TotalAmount, dbo.SAInvoiceDetail.DiscountAmountOriginal, dbo.SAInvoiceDetail.DiscountAmount, 
                         dbo.SAInvoiceDetail.DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, 
                         dbo.SAInvoiceDetail.OWAmountOriginal AS OutwardAmountOriginal, dbo.SAInvoiceDetail.OWAmount AS OutwardAmount, 'SAInvoice' AS RefTable
FROM            dbo.SAInvoice INNER JOIN
                         dbo.SAInvoiceDetail ON dbo.SAInvoice.ID = dbo.SAInvoiceDetail.SAInvoiceID INNER JOIN
                         dbo.Type ON dbo.SAInvoice.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON SAInvoiceDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON SAInvoiceDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON SAInvoiceDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = SAInvoiceDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = SAInvoiceDetail.ContractID

UNION ALL
SELECT        dbo.SAReturn.ID, dbo.SAReturnDetail.ID AS DetailID, dbo.SAReturn.TypeID, dbo.Type.TypeName, dbo.SAReturn.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.SAReturn.Date, dbo.SAReturn.PostedDate, NULL 
                         AS DebitAccount, NULL AS CreditAccount, SAReturn.CurrencyID, dbo.SAReturnDetail.AmountOriginal, dbo.SAReturnDetail.Amount, NULL AS OrgPrice, dbo.SAReturn.Reason, dbo.SAReturnDetail.Description, 
                         dbo.SAReturnDetail.CostSetID, CostSet.CostSetCode, dbo.SAReturnDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, SAReturn.EmployeeID, dbo.SAReturn.BranchID, NULL AS FixedAssetID, NULL 
                         AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID AS RepositoryID, dbo.Repository.RepositoryName, NULL AS DepartmentID, NULL 
                         AS DepartmentName, SAReturnDetail.VATAccount, SAReturnDetail.VATAmountOriginal, SAReturnDetail.VATAmount, SAReturnDetail.Quantity, SAReturnDetail.UnitPrice, SAReturn.Recorded AS Posted, 
                         dbo.SAReturn.TotalAmountOriginal, dbo.SAReturn.TotalAmount, dbo.SAReturnDetail.DiscountAmountOriginal, dbo.SAReturnDetail.DiscountAmount, dbo.SAReturnDetail.DiscountAccount, NULL 
                         AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL 
                         AS OutwardAmount, 'SAReturn' AS RefTable
FROM            dbo.SAReturn INNER JOIN
                         dbo.SAReturnDetail ON dbo.SAReturn.ID = dbo.SAReturnDetail.SAReturnID INNER JOIN
                         dbo.Type ON dbo.SAReturn.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON SAReturnDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON SAReturnDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON SAReturnDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = SAReturnDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = SAReturnDetail.ContractID

UNION ALL
SELECT        dbo.SAOrder.ID, dbo.SAOrderDetail.ID AS DetailID, dbo.SAOrder.TypeID, dbo.Type.TypeName, dbo.SAOrder.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.SAOrder.Date, 
                         dbo.SAOrder.DeliveDate AS PostedDate, NULL AS DebitAccount, NULL AS CreditAccount, SAOrder.CurrencyID, dbo.SAOrderDetail.AmountOriginal, dbo.SAOrderDetail.Amount, NULL AS OrgPrice, 
                         dbo.SAOrder.Reason, dbo.SAOrderDetail.Description, dbo.SAOrderDetail.CostSetID, CostSet.CostSetCode, dbo.SAOrderDetail.ContractID, EMContract.Code AS ContractCode, 
                         dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, SAOrder.EmployeeID, 
                         dbo.SAOrder.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID AS RepositoryID, 
                         dbo.Repository.RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, SAOrderDetail.VATAccount, SAOrderDetail.VATAmountOriginal, SAOrderDetail.VATAmount, SAOrderDetail.Quantity, 
                         SAOrderDetail.UnitPrice, SAOrder.Exported AS Recorded, dbo.SAOrder.TotalAmountOriginal, dbo.SAOrder.TotalAmount, dbo.SAOrderDetail.DiscountAmountOriginal, dbo.SAOrderDetail.DiscountAmount, 
                         dbo.SAOrderDetail.DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'SAOrder' AS RefTable
FROM            dbo.SAOrder INNER JOIN
                         dbo.SAOrderDetail ON dbo.SAOrder.ID = dbo.SAOrderDetail.SAOrderID INNER JOIN
                         dbo.Type ON dbo.SAOrder.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON SAOrderDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON SAOrderDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON SAOrderDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = SAOrderDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = SAOrderDetail.ContractID
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO


DECLARE @value SQL_VARIANT = CAST('' AS varchar(1))
EXEC sys.sp_addextendedproperty N'MS_Description', @value, 'SCHEMA', N'dbo', 'TABLE', N'Backup', 'COLUMN', N'SyncVersion'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @value SQL_VARIANT = CAST('' AS varchar(1))
EXEC sys.sp_updateextendedproperty N'MS_Description', @value, 'SCHEMA', N'dbo', 'TABLE', N'TIDecrement', 'COLUMN', N'BranchID'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @value SQL_VARIANT = CAST('' AS varchar(1))
EXEC sys.sp_updateextendedproperty N'MS_Description', @value, 'SCHEMA', N'dbo', 'TABLE', N'Sys_User', 'COLUMN', N'WorkPhone'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @value SQL_VARIANT = CAST('' AS varchar(1))
EXEC sys.sp_updateextendedproperty N'MS_Description', @value, 'SCHEMA', N'dbo', 'TABLE', N'Sys_User', 'COLUMN', N'HomePhone'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @value SQL_VARIANT = CAST('' AS varchar(1))
EXEC sys.sp_updateextendedproperty N'MS_Description', @value, 'SCHEMA', N'dbo', 'TABLE', N'Sys_User', 'COLUMN', N'MobilePhone'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @value SQL_VARIANT = CAST('' AS varchar(1))
EXEC sys.sp_updateextendedproperty N'MS_Description', @value, 'SCHEMA', N'dbo', 'TABLE', N'Sys_User', 'COLUMN', N'Fax'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAReturn]
  ADD [RefDateTime] [datetime] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @value SQL_VARIANT = CAST('' AS varchar(1))
EXEC sys.sp_updateextendedproperty N'MS_Description', @value, 'SCHEMA', N'dbo', 'TABLE', N'SAInvoice', 'COLUMN', N'ModifiedDate'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @value SQL_VARIANT = CAST('' AS varchar(1))
EXEC sys.sp_updateextendedproperty N'MS_Description', @value, 'SCHEMA', N'dbo', 'TABLE', N'RSInwardOutwardDetail', 'COLUMN', N'OWPurpose'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @value SQL_VARIANT = CAST('' AS varchar(1))
EXEC sys.sp_updateextendedproperty N'MS_Description', @value, 'SCHEMA', N'dbo', 'TABLE', N'RSAssemblyDismantlement_new', 'COLUMN', N'RepositoryID'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @value SQL_VARIANT = CAST('' AS varchar(1))
EXEC sys.sp_updateextendedproperty N'MS_Description', @value, 'SCHEMA', N'dbo', 'TABLE', N'RSAssemblyDismantlement', 'COLUMN', N'OWPostedDate'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @value SQL_VARIANT = CAST('' AS varchar(1))
EXEC sys.sp_updateextendedproperty N'MS_Description', @value, 'SCHEMA', N'dbo', 'TABLE', N'RSAssemblyDismantlement', 'COLUMN', N'OWDate'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @value SQL_VARIANT = CAST('' AS varchar(1))
EXEC sys.sp_updateextendedproperty N'MS_Description', @value, 'SCHEMA', N'dbo', 'TABLE', N'RSAssemblyDismantlement', 'COLUMN', N'OWNo'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @value SQL_VARIANT = CAST('' AS varchar(1))
EXEC sys.sp_updateextendedproperty N'MS_Description', @value, 'SCHEMA', N'dbo', 'TABLE', N'PPInvoiceDetail', 'COLUMN', N'ExchangeRate'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @value SQL_VARIANT = CAST('' AS varchar(1))
EXEC sys.sp_updateextendedproperty N'MS_Description', @value, 'SCHEMA', N'dbo', 'TABLE', N'PPInvoiceDetail', 'COLUMN', N'TaxExchangeRate'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @value SQL_VARIANT = CAST('' AS varchar(1))
EXEC sys.sp_updateextendedproperty N'MS_Description', @value, 'SCHEMA', N'dbo', 'TABLE', N'PPInvoice', 'COLUMN', N'RefDateTime'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @value SQL_VARIANT = CAST('' AS varchar(1))
EXEC sys.sp_updateextendedproperty N'MS_Description', @value, 'SCHEMA', N'dbo', 'TABLE', N'Parameters', 'COLUMN', N'isActive'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MCPaymentDetail]
  ADD [VATAccount] [nvarchar](25) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sp_refreshview '[dbo].[ViewVouchersCloseBook]'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @value SQL_VARIANT = CAST('' AS varchar(1))
EXEC sys.sp_updateextendedproperty N'MS_Description', @value, 'SCHEMA', N'dbo', 'TABLE', N'MaterialGoods', 'COLUMN', N'WarrantyTime'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @value SQL_VARIANT = CAST('' AS varchar(1))
EXEC sys.sp_updateextendedproperty N'MS_Description', @value, 'SCHEMA', N'dbo', 'TABLE', N'AccountingObject', 'COLUMN', N'NumberOfDependent'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[CPAllocationGeneralExpense]
  ADD [ReferenceID] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO


GO
DISABLE TRIGGER ALL ON dbo.TemplateColumn
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateColumn
  DROP CONSTRAINT FK_TemplateColumn_TemplateDetail
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '128fd6e6-b779-41b6-96ac-38bd1999bddc'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = 'b46d1b4f-0d33-4b27-bc27-3fff3bfbe2d2'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '974d5fd5-4cff-492c-906c-4f0c22618c59'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = 'dfc93e2a-766d-4f55-9293-84444defbc29'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '3b5332e3-dd54-497b-965a-9d3edc06b6bc'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '9311648e-0267-4eea-8363-b3e0a493ed6f'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = 'eca5f9c4-2c23-4287-8c81-cc6529878864'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '8221eb36-257a-44a7-a50b-d60be971e968'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '4e2e0b33-b6b2-41e1-a20c-d8afd1666438'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = 'fee7b3fb-f80f-4f9d-9f39-f5de856fe4e2'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

INSERT dbo.GoodsServicePurchase(ID, GoodsServicePurchaseCode, GoodsServicePurchaseName, Description, IsActive, IsSecurity) VALUES ('f315b983-d06e-4137-aad1-4c5a1cb59d7d', N'2', N'Hàng hóa, dịch vụ không đủ điều kiện khấu trừ', N'Hàng hóa, dịch vụ không đủ điều kiện khấu trừ', 0, 0)
INSERT dbo.GoodsServicePurchase(ID, GoodsServicePurchaseCode, GoodsServicePurchaseName, Description, IsActive, IsSecurity) VALUES ('9fa9d9e2-97e9-4d5b-a2c0-6978ed67da92', N'1', N'Hàng hóa, dịch vụ dùng riêng cho SXKD chịu thuế GTGT đủ điều kiện khấu trừ thuế', N'Hàng hóa, dịch vụ dùng riêng cho SXKD chịu thuế GTGT đủ điều kiện khấu trừ thuế', 1, 0)
INSERT dbo.GoodsServicePurchase(ID, GoodsServicePurchaseCode, GoodsServicePurchaseName, Description, IsActive, IsSecurity) VALUES ('35d6384d-097c-45ca-ba5e-9ac5bee09372', N'4', N'Hàng hóa, dịch vụ dùng cho dự án đầu tư đủ điều kiện được khấu trừ thuế', N'Hàng hóa, dịch vụ dùng cho dự án đầu tư đủ điều kiện được khấu trừ thuế', 1, 0)
INSERT dbo.GoodsServicePurchase(ID, GoodsServicePurchaseCode, GoodsServicePurchaseName, Description, IsActive, IsSecurity) VALUES ('f013d981-8c0f-48a3-b51a-c484356a828b', N'5', N'Hàng hóa, dịch vụ không phải tổng hợp trên tờ khai 01/GTGT', N'Hàng hóa, dịch vụ không phải tổng hợp trên tờ khai 01/GTGT', 1, 0)
INSERT dbo.GoodsServicePurchase(ID, GoodsServicePurchaseCode, GoodsServicePurchaseName, Description, IsActive, IsSecurity) VALUES ('9852da2a-671b-41c3-bb0a-cead0ed66878', N'3', N'Hàng hóa, dịch vụ dùng chung cho SXKD chịu thuế và không chịu thuế đủ điều kiện khấu trừ thuế', N'Hàng hóa, dịch vụ dùng chung cho SXKD chịu thuế và không chịu thuế đủ điều kiện khấu trừ thuế', 0, 0)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.SystemOption SET Data = N'', DefaultData = N'' WHERE ID = 19
UPDATE dbo.SystemOption SET Data = N'07/09/2018', DefaultData = N'07/09/2018' WHERE ID = 103
UPDATE dbo.SystemOption SET Data = N'', DefaultData = N'' WHERE ID = 112
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ae78be82-018d-45b9-9264-002fa535bb2b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '86115ade-d36b-4acb-b0d3-00673b4299f3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '82ff2370-177f-4a9b-be6b-00785c42db72'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '635dbfde-b82a-44b3-b006-007aa4eb1b1a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c79af571-a158-4e08-a309-00af0ddbb85d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e94c6dc8-ea64-41f6-8568-00d8d0be964a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8fbe4b7d-547d-4fb3-ad4b-00fcdf1f3dd1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7d1b3a48-960c-447d-ae31-01075db0f36d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '97bc116e-416d-4ff6-aa22-01740b6168a1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'fb27b153-92d8-4a15-9473-01f9169ebfad'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8eddcfda-9926-4b34-89ab-020d774dfec1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'eb18f889-7544-42ea-a45a-025ea5232167'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ddb3a10d-9fce-471d-865b-026ca405c158'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'bb639c8f-2400-4250-bc7f-02825d67fb0b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '773c361d-409d-48e3-8b7c-02a5f29a6486'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2606e4a1-ee9c-4ee5-9ba6-02b227e61423'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c358e60a-24b9-4b00-87ec-0306c3daaf6c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '260aa673-d9b3-4ebb-a385-035c02a82dcc'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '57e3c931-998c-4ec1-86ea-0369d69bef75'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ff2c3215-1228-483d-a6fa-03b49d9fc109'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ede0f765-63e0-4aa6-a6b5-03f9cf1519da'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8e254896-4131-4c04-821b-046eb79482a7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4d88ac2b-b327-4be1-9278-04995baeb722'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '313f37d3-dee5-42fb-9ca1-049a64db3a6b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4608cc1f-0f91-4c17-9af4-049cf5fb3c02'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'bd2c87de-01c4-4f65-b601-04c0ac1dff9a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd64e69f1-74ff-457a-85cf-04d987b2c652'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8ab0a28d-dab6-407c-ae58-04f7928e5f25'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9a6e4c3f-9529-43c1-8aa7-051e29962d52'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'de04adcb-ab08-48d3-8125-053dd721a3ed'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7bf940e0-1ce6-4c9b-997f-054006edb34e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0f5a4972-3aa5-407a-8cfb-05613c86189e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c4e1c156-5d18-47ec-a054-0573c6b4f776'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'fa434d27-80f6-4ff5-a88f-059d1b447fc1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7b07e8b7-d22c-49d9-91f2-05faf626346f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '626fa5be-3b68-4925-97a8-060bba6c356e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3c189567-5306-453a-9ae5-0628d2526a0a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '75d64748-ba9f-4d60-a879-063b346da59b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4775a5e4-7cac-415b-8c85-06a5e5c73dcb'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '462eb72f-f3cd-4121-8e6e-06aa2527e93d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2c5af507-d548-4f55-ba43-06fd8e740be8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6d314884-3ef9-4f51-8234-0703ec25d28c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '22feb002-6939-469e-a2e8-070f9a823b93'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '612e5b7c-0a32-428f-b3a8-0712b6cc5110'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '259b1153-585f-4d1b-8842-071dfb8bc50a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a3737fc8-b3e3-491e-8797-073481bfb5be'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c3993dc5-e55c-47de-856b-073c7b585444'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '99fce5b7-a985-4ebe-b43c-07b071fec64a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8d08e8fd-f9b8-48f9-9295-07b2032141ad'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e50c4ec9-35ce-4c61-8786-07e8ded0949a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e54e0975-97fa-4999-96f0-0814b02ac4e6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '5ce6d315-0245-4f76-ac72-0836b99e88df'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ad03a45f-4390-4ee4-b73a-08562409ac0e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6267e047-e83e-47b9-b9fb-08571e521ddf'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'fb12cf28-6a84-4763-937b-0898c6fece5d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1cbc40e7-acd5-494d-9579-08e12165bc81'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f71fa258-4627-4939-898a-091afd18ed4b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9688fd89-ea37-4793-afce-092d4a11019a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c3bbdc4a-d2f9-4988-b588-09a16f93ae88'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1893972d-b30b-440a-8512-09f3977d5d73'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd74545b6-35f7-44e5-ad48-0a038b7c335d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '73e491e7-7161-418b-8e91-0a086a2ea083'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '76ddf666-6bee-47b8-94e3-0a1b8cbddb22'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8255ce5c-b261-4671-b6aa-0a21a3926093'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '963ca6ec-47a8-41ed-b4f9-0a2605f83f07'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9309a54f-00f5-4721-a546-0a43bf06b734'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '165f2521-cb53-4d0a-bbf4-0a58285c9731'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '319ba8c5-f4ee-41c0-8721-0a89cd6c8bf8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4459854a-5656-49ab-a1a9-0a9331b93e84'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '00ce1327-8f05-4ea2-9af5-0af29c10dcba'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f0f36e63-8bde-4f2c-9438-0b16fb23e240'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c9603786-5da2-400d-9ca4-0b6046de781f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b5d35e98-a093-4ae5-885b-0c03f843c0f2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '13f5cdbc-ae55-4914-8395-0c2af774c1b6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd699801f-11d6-4165-82f3-0c2bec37c277'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '440707b0-1c73-4935-ab3d-0c8ee34914b2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'afe47b11-a85b-48db-a01e-0d02063b531f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd759bc12-7641-4ef2-90d7-0d5119f9b9d2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b7ddda1a-7320-4322-91c4-0d6715af400c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ce423932-65b2-4580-844f-0d71311c21fe'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ea5c38d4-6659-43f1-9c91-0d74506768aa'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '592ba499-68fe-4eb8-ab8d-0d87310fe7d0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6b679c48-3ab2-4b16-96d7-0da685c58b73'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '5445aafc-979d-4d6b-a157-0dc760a023b2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '927e364b-d8a3-47f8-99a2-0dfae615272c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '34c2bbc6-c714-442f-bddd-0e49a2b104bb'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '98d6b072-2e17-4c8d-9292-0e4e41a7d261'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4b34447a-388e-4e54-9fff-0e5e4af9d8dc'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '936e4583-76bb-4865-aab1-0f6c11cfa60a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '595cfc38-a05c-4f72-b6e2-101719a29b17'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2160045f-3468-4a82-afb8-1047fb77f38c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '58152c48-4eb2-49fc-902f-10a6fef480c0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2de3aef0-4d2d-41e0-a3ca-10b2fb53661b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '96212f0c-39a7-4bd5-97d6-10d0f3c93f82'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd9e64605-7382-4ccc-b43f-10e46c0d910b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3cf0fcfe-8279-4384-a1d0-110b9bbe0bae'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3203bc10-2402-4295-b1ad-117b7d381437'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '048bfb06-4e2c-41a1-b374-11a1e2e0acbd'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '98d3f1c8-d388-4f44-98a1-11ae5a6fe128'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '698b218e-f69b-4e86-80b0-11e150a310c3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ddf029b8-ba6c-417c-a05f-12219a5b64de'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '35ec9370-e469-4eb5-843a-123d0c8d078e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4816189d-8a46-400b-96e1-124f4b9edbf3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '08e7bcd8-0e77-4cad-af1e-12e04c74d046'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ed15d61c-508e-43f3-860f-12f431f31f38'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a292e7d4-6e75-4efa-86c2-13b26d77d015'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '5da9d50d-73fb-4167-ac1f-13cc6a686bc1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '06bf3cb1-be31-4153-b82e-13dd76f9c4d6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1665d2dd-6026-4e67-99ba-13eb307f9456'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0dc4ebcd-7f53-4b0f-9a45-141229a2612e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a13dbc0e-1df4-46e4-8e60-1471e594ff86'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd214ecc5-aa9e-4640-b93c-14d0cb985df4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b6c707eb-bc9b-44a1-a846-14e659765d01'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '5584334d-07bc-46cc-ac21-14fada965b05'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ff86ce12-0a75-4062-aedd-15558ad2d03b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'cc42e6e4-cad2-4ded-983e-155dcfd008b5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a356625b-245d-4b3f-a81a-15a5af726547'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1f5ebd3e-aed2-4968-b4c6-15b9c93b4589'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e28179cc-3e33-40e8-9a3f-1680c0088306'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'baa5175e-80bc-459b-b44c-168617c55d1c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7e7eee2d-d3a4-42d2-af73-1688d1c6c067'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9e1900cc-860a-4472-a483-16c592f17604'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b59dbd1d-f83e-4bd9-a4a3-16f2c0217a9e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a8410e41-eb4d-4e1c-a705-179b0d1062e4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a5a44754-3baf-49a4-8f06-179f70a92e74'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a90dcd8b-cc58-4823-8229-17d29bd2c7b9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3387ccf0-118c-4875-8d22-1801f175d5f0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '300ca74e-0e3c-4fbd-8e4a-18776b0c536b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e24517cf-b887-49b3-8229-18d5990c7d4f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1c9a5ff5-b373-444f-b8bb-1930e87579ce'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '009ebcdd-54ca-4aee-8f95-19bea4d06f20'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '25662a36-1ee2-460f-8dc7-19dcbb781772'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3580fa97-9791-4c98-a2c7-1a844733bdd6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c8af075d-c5f6-4deb-8fd0-1a8ff09c5258'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8f64da40-eb98-4592-88be-1a9f60372551'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1fd65820-dde3-4637-bb13-1af3afd5f917'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '996ef596-91a2-4812-acbf-1af510c2008f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9c2967cc-5f2c-4b52-8bed-1b2d55db6bbb'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b485e09c-ac31-4b8d-8953-1b2dc80b76c6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '02ec537d-e7b7-4400-aa61-1b8c2f4dadf2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6345d8b6-8a35-48b4-a830-1ba09584d2b9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7dab254a-625c-4df1-a755-1be3d4c9db55'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '81de24e0-31bb-4927-8b33-1c78fdb3a3f4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '19a366af-cc0c-490e-9a1f-1c9c8e2ef8c4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7c32bd56-6912-4fd3-8242-1cd1c3fd0ee9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ad12604e-e35e-4acd-bd63-1cee4256221e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '43845dc5-1009-40c3-a62d-1d25f8355919'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '418319ef-0c07-482f-b85f-1d671a9c695f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c9260051-bcbf-433e-b20b-1d7c1ceca934'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '48fdbe62-73ad-413d-978d-1d91d8f3191d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0ace9599-a88d-440e-b811-1dcb78521df7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'bdcf939b-dad1-43cf-84b0-1ddb183a2325'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f4ce567b-9aee-498d-a157-1de9514112fe'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c62ef9fb-eda2-4366-933f-1e3c33e7b5c1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e9096718-2cf5-4aae-bad6-1e46ad1d0fae'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '404841ee-4287-412d-9258-1e68f23a780b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8094879a-a7b9-4f7b-96e8-1e7a2d7482e9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'adeda0d1-2ffe-49ad-b1a7-1e7f7fe9f88c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1f422e2c-10a5-419c-8a5e-1eaf5632de82'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '92beb9d0-6c39-47ae-840c-1ef236543cf5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '87f4174a-ca61-4bca-a987-1ef30223e56c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b9d05ed5-c440-46c3-a0bc-1ef5f1294cfb'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'fd920046-b2b0-4aea-ad78-1f0b0d703d87'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '779f36d2-eb32-468c-81a3-1f0eb2d9ce34'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6e10306a-baf4-4315-b465-1f71faa770bb'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a1c28bd6-590a-44aa-887f-1f9c03d1a8ed'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '881da5c0-8bab-4c57-bb08-1fba1b8403f8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b09811d9-2b0f-4b6e-997a-208d04ede5af'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'af7be8da-1f4a-48c2-afaf-2093cc9d3fe0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7affdac0-7901-41cf-ae7e-20bfdb617834'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '31fed95e-c3fc-4ec6-9996-20fbe77f689a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2a3384c0-eb79-43f4-8d63-21035e09628c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f8e675bb-d406-489b-b044-2166dd417580'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e204d067-6b20-428f-8cfe-216ac9d4b152'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c020ca50-0841-46ba-82ef-21942fe0093a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '44dcc5c6-a8da-4366-a60b-21dcfb906f80'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b06964e8-2e0a-4c10-8b41-21fe81d9e810'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '23261a0a-fdff-4c7c-8a52-22180a871217'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'dc057be5-3764-40ab-9383-22211647dab9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '5553cc39-98ff-41ae-ad6d-2293a22e1cc7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '875afadb-2ea7-4bce-9f50-22e55d18f68e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2a10f0fb-6952-44d4-85bc-22e84590f2aa'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4e3e6d00-7729-4ef5-962a-22e90ab74a87'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '47a6bc58-704a-423c-9790-236cb357c0cf'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1390a47d-5ed4-4b7a-b14e-238ff5d289c7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '81acea1d-c9e6-46ae-8509-23e941082a9e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6c107150-91b5-4dbf-98d8-23ed50e6a91d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'dea62161-2439-4de8-9645-2445de1f8b54'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0afad2dc-c771-40ed-8619-2453c4a56934'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'da9d3034-a294-431e-9ce9-24751e16bad2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '041511a9-1bbd-4366-beab-25162083a530'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8b9f0017-ba73-4c55-9356-2535cff7abe3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'cc1742af-9d98-4915-a046-256cc01d5f3c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'cb019785-f737-451c-8f7d-25b0833e44f4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '515faea5-b98d-40e6-b2f5-25d006a3d479'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0a215c97-da91-4c38-b6cd-25dd6e7c74f2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'dba56b0f-65a5-4a50-a41f-266329adbb80'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0f0db542-7b5f-46f9-9c02-2694f3c92439'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7c1c49d8-7d13-4d64-ad33-26a033c63d95'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'cb9ca4f4-6449-4335-bf90-270eb586d666'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f8ef73f4-ae3d-4334-9447-278371dbd1e3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ca3b70e1-ba52-4527-ad74-27d0c551a472'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9e9e02f1-d3cd-4bf3-92fd-283c7242c359'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '638f18d4-5d20-4614-9d7e-284f0db996f1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2803367a-5c27-481e-93e7-285610c6a65f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a1feb412-a9f4-46b2-97c8-285f98818485'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '49760a85-ab48-41a4-b45f-28a10c4c2f96'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4a332e55-7430-418f-9bbf-2909e5124c97'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1802ff3c-7df5-4ccb-9a50-2936fe518ad4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b80ea7e9-3926-4d7f-9486-29427a63ca38'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a6e265da-4905-4d9b-88a9-29554f9cf1e0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9c7f1c48-b4b9-4c0f-82d2-2963d220b4d4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c006342f-136d-4d66-b26e-297f0566e8d4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '07f75104-a623-4d26-8236-298e42c09312'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '176912dd-f0bb-44cb-8545-29948cda765d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b31758b1-2aaf-4e00-8567-299a9a5ce8c2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1233a43b-d483-4271-8d5a-299f6d1adcce'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd09a4a98-7602-4aec-9bf9-299fed4518fe'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '856051f0-2044-428b-8404-29a3cd2d9bec'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9d2c63a3-074a-41d4-aa13-29a575241dea'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6a8a20dd-5f4d-4ec7-9aae-29a5a2a96063'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '938b0c0b-895f-477d-b885-29ba7faab38f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e77581d2-0574-4c60-a797-2a06af4f0d15'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3966c64e-71e7-4d0e-86f1-2adf4c617d3a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0dfd79cb-e3cb-4c64-b596-2b0c63d63a0d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'bde08212-12e8-4a61-b440-2b2d933400c3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c1f3d44a-b6bd-4d45-947e-2b4bf50a78ae'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7cb91b13-0672-471c-a220-2b59c62e8258'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9fcd7b67-7e0c-4dbd-a83d-2b694f7da829'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f49179a1-716e-4858-9f9e-2b6fc4cf285f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'cc30e1fd-db49-4ec4-811d-2b7377c69167'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '5b05fc44-6b2e-4445-97c5-2bb1c20384a6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '52ab7bf3-1d9d-45ea-8692-2bf1eb0f223c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e7fbb397-940e-4d4c-9a5b-2c114afc5ad2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e2efa898-9b89-45c2-b66e-2c97a82e8b76'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a33fedc3-cee6-41a3-b66f-2d5bf4bf4824'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7c7bbd4e-7009-4454-8bf6-2daf77cd55ed'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd78b2c50-af24-477d-93bd-2dc001fedb86'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '37cdd762-9724-4e79-8642-2dc7aee9e181'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '51ce1fac-f8b3-403c-9c20-2e25133c9841'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd22a0205-1a3b-45b8-ba6e-2e8d1fa0da6d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b1911c6f-b177-4b4a-a2a7-2ebdf29a656e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd8ce59cd-516c-4079-a86a-2ed73bed481f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4fef71c0-26d8-4849-a553-2f3ebce6e75f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ce70c486-36fa-46d3-aec3-2f85bdeeb425'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'efd973ff-af6e-4091-a0e9-2f90f6d2ba6a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd1ba651c-607c-4aa9-a9af-2ff5dc4a0a24'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6678b778-9bb2-4452-b946-3009f293fde8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c2030dc7-5d08-4eca-a7a5-30244bfba37d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f8ee8184-e712-4a12-874f-308cf939cdbd'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '470b6b3a-1c24-4e52-bb8e-3127538c64d8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a2c21986-96db-4e60-9d9c-31339d1a9eb5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '070b88cb-105f-495a-bc14-31974d6f8faa'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'aac770fb-c9ef-4f44-8262-321523c8c220'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '055a1b6e-c018-4d9e-b1ab-32a0160950f4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a57d8ed4-4fd4-4d0c-9858-32c67ce5f999'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'afb57acc-55e9-4d8d-a68a-32d5e7b2270b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1af02228-24fc-4adc-b495-32f6954228d1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9697c077-8556-43a5-b7ce-32fa01b22f00'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'fff368b5-b6b8-4c8d-bc0d-33289f6ad239'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '736f85f5-f309-4045-83f7-3342fe0e2790'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'db3fc719-ba0c-4b8e-912b-335537928577'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '29755c52-11e3-483c-a64d-336de3eaa5f6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ff228a94-bec8-491d-aa67-337ac7509e85'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '87efdb12-6ace-46c9-8506-344cfc1129e6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'fe3c060b-42a5-4f23-9b50-34501a728c67'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '67412ccd-dc4d-4af7-b4fd-34933f1441ac'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a68fd22d-ed35-45ac-aef8-34aaf544f77a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '89612668-b910-4dc5-a1b3-34af10d9087e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '328d7ac3-4ccb-41bf-bc6a-34b08dabc1ee'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '03926d68-cd87-4430-9db8-34b370a9e590'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '43419959-b9e9-4cc9-89df-34f92124f2c8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e486cbbe-2a9d-4fc1-919c-3500068a85c0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e51b9200-abea-40e2-87cc-356373fe8be9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f62230a9-7944-4c58-9da2-357896ce06cf'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ee394e73-045d-4309-9d0a-3623635bdd39'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd9097f9f-bd58-4f06-b578-36c01b9ea203'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd4f29763-00b6-44a2-a78e-36fb6afd90c9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'dca545eb-9be3-4c34-9270-374c1cc19d18'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a62813f5-ae01-4600-b750-3775b09139c4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b1c573d0-ba9b-4ed3-8991-37d9d355ca12'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '43291ce5-7371-4e0d-9a2b-386e500995f2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c7ddbfe1-112f-4d73-837e-387b17e426e8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd7a410f5-ba5c-4ee9-9680-38b58d344554'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'eb4cc12f-41cf-49a5-b4e6-38bcdb50e748'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'dc83d6e6-4760-41d5-bdd9-38f46b6e5eed'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '00c9073e-6abc-4afa-a93b-390ed7aac3c4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '38c53495-6de9-40d9-8e4a-39148cfd0b3a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7dff304b-6b20-42d9-a403-392b02ba2e1f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e02f84f3-0566-46c4-ac35-39407ed79c7e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd88ce338-06cd-4837-9264-39ac6e111517'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4a8d103a-7006-4f53-a9bc-39ba29a79a95'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd5e550d4-47cc-4241-bae7-39c5ef0491e4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'adedf362-201e-420b-aadc-39cdb5947586'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd35e1305-7307-4848-91b0-3a0b29add8bd'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '855cb914-4657-4264-b4fc-3a195eb2d4fd'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'bf6e3e4e-e5f5-4d1e-a0bc-3a27d1c4ed63'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0ae5e9ff-357e-4f5a-a9f3-3a6d056a4476'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '40211460-d12a-461b-8d2e-3ae6021cc46b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2b76394b-eb67-45e4-a705-3aebf703ad80'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b9529e1f-98a4-426c-86c6-3b0100c5854d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6f2031a5-8a6b-40a3-844f-3b2658870975'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e18b45c3-50a9-4dd8-9246-3b430d712cea'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1f2cceca-0799-4899-8fde-3b62b67aca8d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8cb9b5f4-e3cc-4972-9ad7-3b879c2ba35a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f196359a-b4c0-495b-a608-3b92b532e0c1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '55d4e474-a54a-4f27-b176-3b9d132caed5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '708794ac-c6f7-43d6-bab5-3befbc821813'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6606d431-a01b-4afe-a253-3c05363ae3d0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '5f51b3eb-b69f-4c74-af00-3c3e396e249f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '23cd075b-4fcf-4cf7-af4d-3c4e79d4d142'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd6c79bbf-1964-4710-ac4f-3c4fb2d93856'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '75c0f46f-2e5c-4216-a9b9-3c8434864069'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '50604da5-63cf-40a2-861e-3cd5771b4364'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd2555d88-2e1e-4207-a4aa-3d360d8c64ca'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '838c3588-2bed-4731-9acc-3d741ab8d0b0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '95ad7862-ee26-4dd5-b1dc-3d8248733b09'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1cb00173-5c25-43a8-94c3-3dd62acd2520'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'be6be530-785f-4e96-a830-3de7035b20f3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c7756bea-d797-4802-a597-3df2fe4cdfef'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '780837f7-31bb-4c37-bc84-3e2a9cf20f2c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '97e8c9f8-4230-4050-8f85-3e2bfbed6b61'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4bbafd50-157c-4e9f-8b35-3e5e315a13d1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '19be11b7-8f87-4b50-86a4-3e7c70412efd'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e66de085-9983-450e-99ae-3ed11f2f6cf6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'fb8176a9-e7a4-4c4c-9e81-3f01a55b923e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '32947309-64fa-4353-8a25-3f0497ca20d2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4c6c8258-347d-4a5b-b12d-3fb4e6055faf'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '73f93418-32c9-4052-9254-3fcb1828f521'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'de51917e-9008-49f1-8f98-3fd4caef51dd'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '19ca6f48-53af-4a63-8890-40d7b4545e63'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'adbe3ad8-00ff-42e2-a104-40f0bf47f457'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f99748ed-fffa-4f22-a604-4176e8036bc0'
UPDATE dbo.TemplateColumn SET VisiblePosition = 47 WHERE ID = '6b320eb3-3684-4b4c-b75d-4189c06ba157'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9da4b877-7ac2-40f0-9f41-418c3696fdab'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '00808091-a4eb-40bb-984d-41cd8fbd8e61'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a240cc78-e1cb-435a-b4d5-41e878f3f19b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'dd5cfcd2-f8a7-4490-9d0e-420a3b670f24'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'eca084a4-1f1f-42c0-b9e1-422285180455'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '404d380c-742e-441b-ba89-4283d2d85612'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8824674f-acd2-4699-b2f2-42ae8997a76d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'cc36f7ea-7a5b-4e79-9323-42b9284be72a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '30861833-dd4d-488f-a36d-430d490a7949'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6d1e172e-5d58-4d52-a20f-432d9275f94a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1804f193-f136-4f7a-86c3-4336cff515bb'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '48fa9221-9087-494d-be2a-433d2f419498'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '95b88fe3-6a34-4665-85cf-435b5f5a2a8b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e5dba917-7e1f-4ebe-bf27-435ee9ec3703'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7064175a-865a-4b51-a408-43a2599b6ac1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '332c63b7-a427-4259-8ef1-43b3a5e36b52'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '44568612-69bf-4272-bba4-43e318b90681'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b9946e27-64b0-4c61-a463-43efbc86b992'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '908d755f-4dfe-4ac6-8d99-441f229fd870'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '96f731eb-cb71-4f2f-80b1-442ebff3cf88'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ca054706-2e52-474d-87d1-44adaf7fd4dc'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b9447528-9d56-4eb0-b5e8-44de524459a6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '37b8ac9c-c7b4-4333-b23f-44e90212c62e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'cd8aa429-8040-46c9-b88f-457c3c4b384f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '52d9da90-1234-4980-899c-45d5291b57d5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e04eb4b5-00dd-4ecf-a7b9-465f1c0eea1a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '40dbaab1-c168-4a5d-90a8-467cc1deb4e0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4b3b78a9-08e2-461b-a433-4695dc35937d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3b04f9f9-4182-4525-ac49-46dc3ff0f1f2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3cfc13ed-210a-4e0a-8f0f-4741d48d749a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c7a14490-481a-4d3f-a823-474414cb74bc'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '675833bd-d17a-4877-8caa-474b3108e5c3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c6cb304d-ebd0-461b-9d3e-474c7225d028'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4ed9d3a3-090d-4382-b954-47ded2bf1a5b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '57cd893d-271d-48ca-93b5-481ebf30ff12'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c0fa3d05-5f1a-4699-ac3c-486bd8c5f7b9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '5d03e979-93af-4083-bdea-489533d1fff8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '90c8bec3-48e4-4c90-9bce-48baacb359e0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '20e37439-597b-470e-8c53-48bca81034b0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '16a86a9f-4058-41e8-8550-48cbad98d6e3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ad29ece0-da64-44ff-89ca-48d6a07a8ce4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ce50217d-d5e9-474a-bc92-494543dcdb0c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '702fee37-56fa-4619-a530-495dc36884f6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '13254c9d-ebee-47b2-96a5-496bc0639924'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'def14eaf-4775-4e50-8cbc-49a22ca943f2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '11546051-56c8-437d-8dd8-49d90da95e87'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f4fb66d8-f419-4e90-a727-4a61eba9ba77'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '819ad82f-d930-4294-a83e-4aaa9c708673'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1d41b6be-d9ab-4d4d-9096-4acae4a61cf8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2c189cd1-1b04-4bef-af66-4b62c71efcce'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'cde96a4a-ce54-4bbc-be97-4b63a24bc336'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '40a5ca0c-befb-4229-ae59-4b641ba93d35'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'dc5a99f3-ece3-404f-916f-4b7a3329f355'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ed6bc978-3294-4b6c-8013-4ba2ef5fed4c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a7677ba7-08e2-4c6f-b62e-4bc7fe22da41'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '69cef27d-c231-40ee-964f-4bf53e4d41c8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f2c98d0c-7462-42c4-a63d-4c14940781e5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '233318cf-0886-4c0d-8e0c-4c6299c94817'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '209e81ec-78f3-4dca-97f5-4c861aa6d89c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c76781bb-7fa5-48fa-b56b-4c9a319a473d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8e3f608f-9afa-4229-bd3c-4cae0f5763bd'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'bf0a0278-7c09-4d43-987b-4cd0c4aa2b9d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '66e162a3-6d54-4820-b1cb-4d17220474b8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd942f377-e59e-46fc-a4bd-4d57ab001b31'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '366746f8-ad26-4f20-995f-4d5fcc32fcb2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '30e1b047-aa73-47e7-a074-4d758eb6fa19'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'de8edb2e-2950-49e4-bef9-4d8315bcf545'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '001fa941-29ee-45a9-9421-4d8aab191490'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f605f9f1-7642-423f-8461-4e386fc9f146'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'fb9f832b-4350-4984-bd8e-4e3d9178e3ed'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '24942e07-ba02-4bd0-96e4-4e4d622669d0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e101da12-7a82-4950-9ca6-4e5de4ec0967'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '5632468c-e030-452a-b360-4e77777d4db8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '640b4eaf-e6ac-4b3c-b65a-4ec06f84eb95'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1689f908-425f-4e12-86ea-4f21309a7438'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6e75f305-3c46-4621-ab59-4fc5a8693889'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c2d8bf73-be70-4ee8-9272-503e1a2c85eb'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '31d44fa3-7118-4fcc-b6da-50d092d5b3e5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '80d1c710-9fd3-47a3-9888-50fc066760ac'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6cc75c72-d85d-4e95-ac6e-51161983011a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '94c9b7bb-27d1-4b2d-b73d-511958a0f55b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f8e57c0d-027e-45c8-ac40-51195c15e0f6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '52c5f8df-6177-4d88-957f-5167e466450c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a90235a0-c349-4700-9026-51af978228b9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd9225de9-78cb-4ed9-95f1-51c463de4e56'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '18165c23-e44a-4f41-96b5-51c53ddd99f8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '55fd077c-a6a6-4469-9da3-51d488e1ad92'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '07d354f2-c354-4701-99ad-51d96453e7d2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8927fff4-11f7-441c-9d53-522e2773cb5a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4f96e391-999d-4522-912e-525ea9410d70'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '5dac1fc0-8d0b-4f7b-a9ea-5275a89ead2f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '92004d40-1c81-490a-be00-5275d0bd741a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '851d77c8-760a-489d-bf2f-52874035a7c2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8a4dd3ad-fc54-4eed-b215-52c1e703a598'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1c35f22b-6b8d-4da7-9c7d-531596ddf061'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd297e915-c7f1-4a03-98db-537e1015023b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '312ec950-9e77-4580-a5d9-53efd694ee33'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '417cd1a1-337a-4986-91fd-542284299460'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2056b9d6-5113-4d63-9bd4-54268af31885'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3035dbd7-561a-4461-b369-542da4567ed8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b69d187f-dc33-4816-bc2b-544aead9523a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c9b68cb2-f9ff-4ed9-83a8-5489e1477a8d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '00593073-23a2-416e-b768-548dde41a612'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b0b55e01-66a3-4427-a8b4-549569f974af'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b2d35c66-40ff-42a5-bc93-5497a88d2fe4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e51dbb08-a378-448a-a92f-54e3a21a2f48'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '93df4390-a1ad-47c4-8f4a-5534682825a6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0acde92a-980d-4a2b-bde4-554477e80903'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f1b77d00-08e4-415d-a76e-555b2840acca'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0aae431d-f22c-46df-8da5-55626fa5b12d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6f8c3382-48c8-4492-a65d-556ac91ab368'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '811c809e-2660-43be-b32d-559d983beb49'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4e3ff869-b0f7-41d8-9122-55be03a627be'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0bbecd7a-7efd-4d7b-b3ea-560623ed7d0e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '04c1d649-1087-4c51-b9fd-561ae9dadaa3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3a127b29-6de3-4008-8fed-564953150741'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6fa3a7cd-ac4f-4823-b727-5659ffe0d9d4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '11fd1f7f-6f4f-4500-975f-565e1cd17404'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '025d253f-5ecd-43ee-a121-5661da7fdf48'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b3bcf0a6-ff09-462b-a754-56def3eb9042'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0d6cb72b-aa6e-4c54-bb22-56f8f9986b01'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'be57618d-ad64-4634-b2e6-58109142064b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7084e88a-4a88-4877-a337-58168d864a42'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '54bdd93d-b58d-41d9-a373-581d5f83f6a5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8489248c-ae8a-4900-a7e8-5836ce48fa67'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c14dcfea-4657-4837-b7c6-583976464298'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '60819c5d-5953-4122-91ff-586c3b106e4d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'bb209d33-e7f1-463c-9658-588b1f275682'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '19be1c71-2ced-4922-9850-58ce70baa5fd'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '388dfe6a-c613-40bd-aa46-58e2069aa902'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7cacf7a3-5a44-4042-9c78-58e343d02c0b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'fbf87239-018a-45e6-86bb-58ea7b2ec0c0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '41355917-5306-45ba-85e4-592a3904a110'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'daa39d39-9c10-4f2d-9430-5959364eff3e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a1aca78a-3fe2-4d9a-b2b5-598826abf289'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e1fc5a30-d193-4e61-b777-59891f956731'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '30c10968-4898-47b0-8e33-59a0719eb8e3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '211f5970-66fe-4a64-90cc-59f551b2c52f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e3988ebb-15e5-4726-9138-5a36890b7500'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0bb36641-6197-4359-9737-5a38cf727cf6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7763f409-e545-440d-8dd5-5a9bb87c9f3d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '623c5961-2f4b-437a-80ad-5ad841cb74f3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a5baaa8c-bcfe-412c-a688-5b59d6272d2e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'bed17c5f-e802-4f53-87fa-5b999ad5b3e9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a29b2713-44d4-496e-86b0-5c14143105a4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a59eef43-0ae3-4804-affc-5c425be6578c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '78404ac6-ce77-4aea-9a10-5c801fca61f3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '32f12bf7-9882-4637-8328-5cc49321717f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'fb1dafd4-e228-4a41-9bc8-5cc7e45a25b8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '85eca231-0dd9-4cfa-bf9f-5ce20fa6df70'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8d146ad0-3b68-467e-aa57-5d0920f27d0d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'bd40f8ae-bc63-42e6-a4f5-5d81360e1b83'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '5ac3c369-05f7-4d8a-86a6-5d9b490124e7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '36289f9e-2193-4d36-971b-5dd2d0554efe'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '19fc2425-45f6-4c4b-8330-5ddffebe9d21'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9decbd49-c314-4a0a-9579-5df64b10b7ae'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '711a3aa6-4246-4ce2-890e-5e3f927162d3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6b25ca02-8e38-4da2-bdab-5e5fe3aced7d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1fcb1d86-2a0e-4a07-b1f8-5e9a183df6f8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '035c8c67-0c1a-4ce4-9d64-5ebfb830d85e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9c067595-4179-4c0d-a960-5ec05288d90b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7a88cb5b-3d8d-4591-8765-5ec66612c80d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'bd0d1e1f-4b50-4af4-9853-5ed00bc9faea'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9d83f687-081c-4cd5-b69c-5ef6d182caa5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '5aa80809-aff3-4162-8d71-5f005936185f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b833d2d2-a6e2-47ad-9d45-5f12aaa42ae7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0e1a16df-1275-4c68-838f-5f5dea360ff0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b126320b-b684-41a8-98be-5f72e6c6aacc'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c2ec3b20-47d3-4e9e-ae6a-5f7c0692bd7e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd9327b8f-b8c2-4c43-bd07-5f8757000516'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6483880c-b09a-4afb-a732-5f9568437c9a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'dff718d2-ed96-4dbc-b5e9-5f9b4ff63c38'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9a92bace-fe52-4e28-a144-5fcca16e66be'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4b0c21eb-9ce0-4b16-a55e-60d492632b68'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '506aef32-9119-4fb5-9045-610fe81ac111'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '25a0a819-3771-471a-b21c-611676508b60'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1a24f8bb-daf8-4905-ae8a-6125f9e341f6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7004818e-ab3f-4317-ae6b-6129aa117261'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8193c2cb-41b7-4313-961c-614e3abba117'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3ba37115-9d5e-41dc-93aa-617660700e6f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1cff1890-dab3-4e2c-bfd0-617c45250bae'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '31265c70-1aee-4ed6-abd5-61b33a94486c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ac0d90d5-8aea-4fe3-9a97-61bdc69fd405'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c008a165-c98d-4bdf-81f5-61c5162de270'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3a25bc2f-2bcc-4b17-bf92-61ef915edb78'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f72d706b-bcaf-4fb3-8853-62755a6cbe39'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7982e464-2c94-4522-b4a7-62884f6b41d4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4c347b60-699a-419f-994a-628a3ede86ee'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '03add716-c19c-496a-ae31-62cab74cd942'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0a74fc9c-3109-419b-9ec7-62fca34b6c6c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a27bdb9d-4ed9-453b-966f-634292d35af0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '27b35977-c16d-4d48-b486-6347fa6b820c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9ae5deeb-d734-45c2-800a-634a9c21d0d0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd8303afc-2161-4d94-95a4-6371e3028a35'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9c4dda69-a1d6-45b0-a9d4-6390aa942ba7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '50686c09-06f8-4d26-81b5-6432e76439bf'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd5c1aae3-9ce4-4b88-bcc0-6439325b345c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '061f5d2c-4366-4857-983a-6456170a697e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '67b26f78-579a-4f25-b61e-64d22ad37072'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ef9dd06a-276a-4107-a4f5-652b8e2bf607'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2908a581-ba74-4d6a-8baf-652c4eeaae20'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'be2681f7-5528-4e45-b179-6580dfe91234'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b669d52a-bb7b-4389-a6d8-65844edb6ffb'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '491ab521-17d2-486f-88f8-658daf174483'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1ff11948-6e7a-49fa-a12f-6594deb1c476'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'aa767843-0f82-4d08-9e0c-65dc489ddb07'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9d75dbeb-e26b-44b7-bc44-65ea56da9563'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9f7b14c8-00a1-460c-884f-660082240c9f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ab2e982f-f003-49af-ae27-661a3a706b61'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6ebd6f88-0abe-4e6a-a619-663355a85d93'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ccae93c6-fa5a-48d3-902d-6662b86df2e8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a5ec0f8d-9949-484e-a601-66d5b00b7b5e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'adc5047b-c552-4c26-9ed3-66fdec970628'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0a5f6119-54ca-42e6-8bb7-671b58cbe9f7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '908c1dfb-cd44-42f9-b1b1-67840b63f606'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd1479c43-fea5-4b8f-8746-6798507ad2a9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd7018446-7817-45d9-8516-67b4d09ff81e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'cbd6c974-e096-4630-aeec-67e4068d98e3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f3184cf4-f397-4d58-bab3-67fad72ec779'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'dc8e5f86-2989-43d9-9b13-68193d78ca7b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '5243079a-bf6e-4cb9-8a92-68c1750679e8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4bc02d6a-15bb-4d58-9b6d-6927193fd66f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '27a02555-3d9c-4614-af04-6930d122d968'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '87aed74e-d603-4658-8528-69a52dd905eb'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '466420ad-2fbe-4f13-a87b-69a718e5a2f6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ad744d91-08b9-49db-bd25-6b2ef60ab6d0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0c8d5810-2949-4228-8bf0-6b890c146a82'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '5910e7ef-8088-4461-80e5-6bff4b695e24'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '82b4ec54-2590-47aa-a0d5-6c1241385dda'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b0f4e9f5-45d7-410d-84cc-6c285cfc03ab'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f408a137-46d6-428e-bffe-6c5b7e57d31f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7ce7f27f-967d-464f-9cb6-6c62b34306cb'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '656878cf-c6bb-4207-a085-6c8e6d9a40f9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'bab727d3-1abb-4595-9fd0-6cc141bfa337'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd9d06bf5-7dfd-4410-8cf5-6d0a4d8fb3b4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2c300c09-9991-4cfe-aa11-6d3d5a080ee9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ca8c2cc1-4f9b-4e61-afb9-6d53fb1578af'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '27869a59-1c49-4828-bdaa-6dc3938a16e7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '33269ed2-338b-4a95-aa3f-6dc78f1237e4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f29582cf-d1af-48b7-91f6-6e061dd72bb2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd2931edd-ab08-4334-9d80-6e3fc96c34c6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '05d93c57-a5f3-4557-beb6-6e63928d6eaa'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c2523e1c-e97f-4419-942e-6e7276d0b361'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0c793dfd-41a6-4719-a2de-6e7cd81e40c8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7b35a00d-cbcc-4dd4-ae02-6ea00e5e9633'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e98e0cc0-0c93-4c7a-903e-6ef6762a3dad'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8e52e9fd-d48a-4c67-8bc9-6f09ac2573fd'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '414ffefb-e1f8-42e5-8f6f-6f5045afd937'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '29c3b8b0-e6c1-4d44-abe1-6f755cd85bb0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '150abd90-47e0-40e7-a1f0-6fae6d295627'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3d1d9109-26d0-4eec-9feb-6fc6010d8211'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8341ca16-ffe6-4775-a0e2-702ec03867d9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '67cd0db0-737f-4f5b-bb82-703550e2163d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c3417c66-965f-4576-a369-706e96e951d2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '136b2316-b222-43c3-8923-70af2330e6e7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9f0e115b-7369-48e4-9efd-70e71756483f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b2bb91d5-b054-434b-a65d-70f3d53289f9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ba8f4caf-1f89-48df-a2b6-70fbd62f8d71'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ebde0a61-5736-493b-9ad8-7106bbc3ac6b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '56187519-6526-473f-b528-713f3fde0b44'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7c2fec9d-2fe7-41d2-b824-7159f756ffaa'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9f92dbed-242d-4890-a8b2-7181abc7cb8d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1ce823d1-0977-4b45-93d0-7195f7a7b7c0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7b41810f-2a58-408d-a0b8-719a262480f2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a969b13c-2ae6-46d4-86f1-71a13c0a0a76'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'de5588f0-ba73-4a09-bc01-71b0edd646f5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '5b6f6f97-2897-4be1-b4cc-71c01efcf91d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '865eb40d-842c-42bd-96b0-71c5579ae47c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '48edd745-b3fc-4f2a-84c4-71cdfbb3ba6c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ade16f57-8540-4671-b495-7206459e31aa'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ffef8851-a569-45ad-961e-72649c5a83e1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3c095c80-08b9-4c15-aba9-726707f2afa2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0049e6f3-1033-449d-962c-730954b413c5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3a2f71f1-1092-40b8-980e-747b8b4a6b14'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'deaaa619-d9f4-41af-9f1b-7489093b0327'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0274a352-d46f-4dc3-9c40-749a984b13d7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '73bb485e-7f74-4569-9306-74a8b8e3cc91'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8dc03db4-733c-4ad1-bbcf-74c0210b0c53'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'fc565a55-2d1e-4d09-be1e-74c7f21b35b7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '296e454a-db1f-41e8-a2a1-751b71ce11d1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '01a467d5-8540-451e-8cdd-75451f358a8d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '410ddbf2-f032-400a-a7d6-75b7eb5a89d3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'dd0f5497-ac0f-4cf8-a669-75bd5e700f52'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c987bfd6-c268-4e5e-8e63-7717e23b9c13'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '990529c5-4696-4c85-a8f5-772446dd85fd'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd144b9fa-e0f1-4f78-bbfb-77bc3de92eaa'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '13861be0-af5d-4a2c-93f8-77cd4551b3e9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ffb55d31-800a-46ba-bbfc-77e2e4340ca2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'cfe48fc7-b2b7-4610-9baa-782ab79f3a2d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c0c099a1-0916-4ace-9e1e-782bc8bb95a6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a2a20f72-b396-4cae-9e8c-7856303fc32b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '08b7b549-171e-4a83-8226-786b777618a5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '00954fbb-b1c9-4711-b09c-786ff9ba3b78'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a7de897b-60db-4710-b268-78715eeea859'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '79fdf0fe-f264-4515-b336-7877fb87e73f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '784c7919-5cb9-474d-bb1e-787bf0a2ee51'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3e71ed6d-920a-4e8e-a566-78a8146e9cb2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1cb60300-7cfe-430e-8a7a-78e3578b2d6e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '14dda33e-406e-4383-8009-78eaf919050e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c3a4f157-251c-4f6a-8230-78f623c7a637'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0d2e914d-78dd-40b0-bad9-78f78f8fe6a8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ac3b2668-1edc-490f-bc2c-78ff173d613b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'fa7afa04-82ca-423b-a8ae-791050d4b47f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '05eeead1-e445-4aaa-b55d-791d80ec8b13'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '5762fa58-a9e2-4c41-b728-7981f21f0907'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c4ddc79b-5992-4113-be50-79831f570afd'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'fe88c59c-3926-49e7-8d61-79dcda446cd8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '04d88580-0bea-4ff1-bdd9-7a0f9d11df4f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4a30adfa-219b-473a-b312-7a766a2c061d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'bd3ffc13-66c4-4028-b6bc-7a7894043573'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4ec7dc66-c9b8-410f-8785-7ab3cbd21e44'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8e2580c0-6b0a-4b95-ab9a-7af3f0601829'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ba3f3dac-0cc9-4964-b031-7b36fe0cc14f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8c0a115b-8c39-42a2-aac3-7b92ba8c2202'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '56c74a39-7b8f-4ae8-a96d-7bc5b318a1be'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '94358f2a-6623-4ff1-be5d-7be989cda2f7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'af7e3056-bde1-4732-8949-7c04fcb9cd13'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ea464aeb-cf62-4514-93c3-7c2af736e5ad'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '88f3c79c-8081-4f6f-8ee9-7c9ae4cff046'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c135ede5-ebe1-491c-9227-7ca087383bb8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9358ca03-2901-4531-a5e6-7cb8f8103c5c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '45b351cb-2a8c-41b5-b159-7cf55b62c02e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2c352eee-b311-48a5-a4bc-7d0b4e0a2d43'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '822af77b-1ea4-4502-97ff-7dd5e0813ea0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd32dd71e-fee3-4fb9-a6b1-7df31e23e300'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b6eab0bb-73a1-4cbf-9fb8-7e35b2b86df9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1c11fbde-cae3-4bbd-8783-7fcd782ad7c3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '098e8b5f-ded0-4cc2-b828-7ff1f82e40b4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3d6b35d2-7b5c-4023-853e-80169c3e9a3c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0d17932c-8168-4dd3-bedb-8040a8aecff7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8eb36143-0663-45b2-90e3-8069975f75e4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f271ae9a-28ac-40d5-a00f-807bb7bff959'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c70f9c99-1505-4612-a9df-80afb32e4417'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0aeac8ea-f6d3-43f3-978b-80bbdcaff8ce'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8907f97a-c208-496d-9d29-80f7e62f020d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3b34207e-f2f5-464f-b5e0-813787fc5709'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'fdef18ea-1e0a-47f5-81ad-813e4440b0b2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '07c19fc2-a8c5-4e2e-b338-815a1c5aed6c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '039d75f7-9b88-426c-b29a-8164c31182cf'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '66258efe-2eeb-4a06-9c25-81a7d717ea3a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'fbb65322-b2a6-4cd7-bb51-81d1100fea02'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6c58f657-d0b4-428d-b713-81f2b48df543'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'cd4a6828-74d6-4e64-8fe8-82090e01a546'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0b8789cf-704b-4b2a-99b9-82412a83003e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a8903833-8609-4e0e-8629-82444e2a8518'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f01c466a-3495-46f8-942e-825cc855e146'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '94e638ba-243d-4cd6-b140-826bde51f513'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7f0e79fe-72b4-498c-be62-827a99b3fd3f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7dba5100-6719-4ae9-bfaa-8282e2b93f95'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '643676cf-7905-467d-95a1-82f3709e1abd'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'da168e86-a4af-4f47-924b-831e59d8a724'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b64b5c6b-d8ad-49d6-a9e3-8363b3bb5e82'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ed1dfc09-e770-4c3b-9994-83a3cbd22731'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '38e953c6-2d3c-4fbd-a129-8405b7a30e00'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3472d1c2-e429-402e-8ff9-84272d841ac4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '635a9992-9bf3-40af-bc0f-845bb764ba4b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '5d4c2858-d379-4190-bf1a-848138015c98'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2cfe9dfa-ff4e-4999-b245-849cbe0a456d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '39969d96-d9ba-4907-9caf-84d153c819ab'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '5d920195-afaf-425e-a060-84ed8f110fa9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3db115b5-8b39-45c5-834b-851a0d98ed22'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'db2f886e-32af-40c3-9fe9-8529afe9e720'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'de9f469b-48af-4167-bd47-857e0e4a7bad'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b77b5548-a4a5-4756-a82a-85862ee6cd92'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9ce57914-069c-45f9-a0c4-85993aae4b97'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'bdc958d6-09e5-42a8-9470-85b5c3a8a0a3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0e7198a5-d707-4330-a6ed-860b472e09c4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b87d0149-cb61-415d-b6b7-869a9e6b3032'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '67776264-e28a-4d9f-be15-86b7b4afccd9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '32e86184-4314-47bf-b22c-86e1e6640ec7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '118b7bea-2ef5-4607-87a9-87077f9886f8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'bd59d8c8-b88b-4b9d-a671-8758476efe5f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b86ae6de-3c18-4d2b-b64c-876abe1c79d8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a1ff9611-5012-4cc8-9aad-87b221bbb8cc'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e399fcd1-9371-48e3-825d-87e75423d763'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0ea46d47-e37d-4372-ac55-8826011d455c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd9823b63-e2c2-43b7-b313-8827b35e16df'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'be061a49-3fa3-4509-b8e0-884e54c5eb87'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '32410bb3-c219-4dac-b403-886741374f01'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '362abe2f-9139-481a-8900-8896c8da3d5c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd1fd3007-a301-49d2-9e5b-88adcd4f32f3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'eab1217a-9c94-4ca2-88b4-88c2fc39c53f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '93cb16b5-4c8e-4e65-abdc-8905747124e5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a8d4eae6-b62f-4608-9c1e-892d0f462a92'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'aae63e91-9e81-4d0c-bf50-8944c6e8fa35'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '793390c6-d57e-430f-a55b-8945fd51b641'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '27003906-edc3-4d48-83d1-89c66f9787f8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4119a64b-0755-4bf4-937d-89df63683b21'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '08b89543-56a8-4340-a027-89e1ffc32415'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e24ddcd4-bb1f-47d9-91ac-8a2c9da1e0c4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'fde83317-8e58-402f-aefc-8a592dcb1013'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '96ef8496-c746-485b-be47-8a6ee622c7f2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '40481452-bc29-4b3d-81f9-8a8b005836b7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1f9feed2-66f9-4b6e-afa5-8aa2b54bed6c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'fd06d5a1-7c3b-4a85-96e0-8aafefdfa3f5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ad1c2bdf-314a-4bb1-acd9-8b0120ff62c7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7f8565fe-964e-40cd-85bd-8b2aa9cc4a8a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '77bf9578-ef41-4e4c-b415-8b3ab321e0ac'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '24e444fd-780d-45cb-a0d5-8b678c95a2ae'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '88899a47-861d-4486-bede-8ba88ec02636'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b55bf7fe-1520-4dcb-9b2b-8c0b3e150e60'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'dc1156c9-74c2-4251-8237-8c124146844b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'de6520d7-91d3-45fa-a761-8c1fac3ca972'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '91b1ea78-1dd0-4dac-82b1-8c39b1ec783e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c318a8d0-7e05-4dea-b59b-8c3e81c536ce'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c8d7c124-d12f-4bda-a1d8-8c49b4ab9b54'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7a113dcb-3b5d-4812-8a3c-8d3d5a1a40f1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3834e3ed-e595-49a9-98d4-8daa62508300'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a9039df6-9d32-477d-8715-8db76ef2a0aa'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '09b60c1b-e4bb-4565-8e2f-8de2893ac9ed'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9627ebe3-e07b-4adb-aa06-8e1610e1a2c5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9f2ce413-caa7-4d3b-9d8b-8e294b4bf371'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0d8768eb-de07-4e75-b6a9-8e3dedcaaedf'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '95152a5d-e61b-4c6b-8ae3-8e6d52504069'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '28e904aa-1546-4adb-8415-8ea812a7e65a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '93f6877e-4345-4e3a-8578-8ed766da5a73'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f6429fca-ce3a-429f-9ca7-8ee36a538465'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd832765f-741f-41cb-a530-8ee473baed5f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '666cc2a6-9193-41aa-a35c-8f0335f335bf'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f98b9e5f-4119-4c92-be95-8f3faf1e2454'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3e559227-d1c3-4787-88e1-8f881d479ec7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '60884bfc-0b93-4c93-9156-8fa62c6acce4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd2c80438-9ab4-4a5e-be59-8fd174b428bd'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6f67d85d-c6f2-4a94-87d8-9035c6695b90'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'de79daf2-76a1-44e2-9b36-90ccf7da6083'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f62df1d2-f7b8-4052-a1e4-9100b77920a1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e58e7c57-80ee-467d-bd28-9108d0def742'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9b5ec832-5718-45cf-9d74-910e46bd5443'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '45d4f640-a810-4a5b-928d-911e6cb42849'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'bc1769a2-9ef7-449a-80c0-914382efa96d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '57fe791a-acb1-4ad5-b10f-91467da1727a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '5847f646-dde0-4e33-867d-917428012ad9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '97e99d16-3803-487c-9c4c-918c5dfe0d7a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0a680027-f16a-490e-9114-91a7944b0243'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '75bb6389-5c16-4f69-91fb-9201177b5e17'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '70c98e89-32ba-4129-ae44-9206bf35860c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b8b59027-fd5a-42f1-8f76-921e702793e0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '913336d7-3d10-442e-ac1e-924f5da193f4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1edc6ba1-f55e-4eb3-a408-925f891ec3da'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '5ef36b93-2a46-4a6e-861e-928367874cf0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '890c5318-79ce-4a80-afb7-9285fa620219'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4c6fe857-7d18-4ad5-a761-937a2ec2d3c3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '42c6b220-bdc9-4311-961b-938df7d9d71b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '75d3bc80-6686-469e-b8a5-9395b6f35448'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ffbc0bd5-bd34-493a-ac00-93a2fa3b581c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'aecabef8-af1f-4fe6-b0e1-93ae2ec6e224'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd617f58c-afe4-4014-a930-93cf8e75f13a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '097b9043-6e33-4f39-8b6a-93f9b4c9a515'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'aa3c9f5f-ac4e-477a-8ffb-944e1b188a07'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3d7c622c-7af1-437e-a12a-94b9f099821c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9aaf6b2b-e794-4f87-8992-94cceac42ee0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6cbef718-fa9f-43d9-bf1d-94dc1cdf032e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'da604501-c552-4d15-b437-94e693ddae14'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '37133136-2221-4f9c-91aa-955e76ed6331'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'cf3503c5-07cd-4e49-b194-95653bc6878d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c72dcf54-f9ee-4a9f-90c7-95a35910e2ec'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c90814e2-9638-4070-9efb-95a758c2e348'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1560899a-5120-484f-bdb9-95cc509a6a01'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '64a77163-74b3-4203-acd2-96083f3cafde'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'bf2482ed-5ce4-45b4-a6b0-9651c7b7df13'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f7bc9492-b5c6-44ab-a212-96cdc67dad20'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'db2bbc65-c811-4183-b5b3-973e24e3aa73'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '05e3ec78-73a0-49c2-9b93-9741e051d46a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e0d40d86-9222-438c-ba57-975313fe1e7f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '432e3015-0a64-4bd0-83a8-977cac7a8dc3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4234b261-3955-4c99-96c7-97a158158bc3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a7f15e10-4b14-4ff9-9be3-97adb3372864'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c6d9de7f-a26d-4ff7-9a3c-97b98324e176'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '90af8668-9599-4bd3-82ad-97c3910ffe7a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'fec0f6cf-66c7-4c4d-b957-97c4da71c024'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'aeafe9f9-0ba9-4a2b-a757-97ea24e022c6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'df6956bd-38a2-4d3d-a775-97fc982681c9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '74dbd992-30f7-4ad9-96ee-980db3dd32ee'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c97dddcd-9420-46a1-8404-980e7cd11ff4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2998f071-a195-4300-8d43-980fb28fd3b9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd983b473-8f4e-4e0e-a9e9-984b94c3f3e5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b403e9b6-fc80-4440-949f-98a023559bde'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8ca4e6f4-c32c-40f3-86e0-991edaad2fea'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd993dd4a-a5fb-4179-a5d7-992de4ea19c9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8bf02083-7cc2-4d2b-adf5-99443403f5dc'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'fc95db13-00cb-49f2-87e7-99463d35e0da'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8f725818-b0fe-4a1d-a5a5-997ef2d604f8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'be28d3e7-3a51-4433-81a8-999a86b89b74'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f14e8896-327b-4d65-a7aa-9a7d152f8bb8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c2591485-8188-4aac-8644-9a80c40f4468'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '60fadfd7-6e3a-49b4-98e1-9aa537d25d19'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9c4f454b-38a6-4245-ac93-9b17b55a3dd9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8a02e0cc-58f5-46f3-9435-9b1af0dc9846'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2990721e-fdc1-474c-8554-9b1b454f1388'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '813f9dcf-daf5-41d8-8faa-9b2c56a7d667'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '20bdd01a-5e3a-4c9b-9f8b-9b670dce6dbe'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '45f0f9f4-2a81-4b90-b43d-9ba3f27fab49'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2bf67c71-a43c-4128-8d5f-9ba73c2a6e23'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ae7d8d9c-5a25-44f4-b73c-9bcb86510e07'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9405000b-27e7-4fd7-9999-9be1d8cbd79e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a2d08ce5-2a81-4819-acc7-9be2c815ab50'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a30358fb-8553-4a64-86d8-9c03d36c8aa9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8f97f64f-cb09-406f-918f-9c2011999cc1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '5b62564c-a0d2-4699-a272-9c4efc312cbd'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f5b3339f-6a86-4428-ac73-9c6204fc66d3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7aa48d8b-458a-4e63-96b8-9cad47c80c52'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0e64d3df-cb2b-421e-8cd6-9cfc6d45940c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '5771a93a-fce4-4a20-978b-9d0639947d99'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '52282ce0-152b-4f85-ab50-9d638822edbc'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ac51e6ee-5570-4006-a9b2-9d7d2d9f3afa'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '65187cfe-750d-4e16-bee9-9dade45199f3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'adcd889c-7d9c-47a7-84a8-9dfcc3ffa3e9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'fa84b55e-1fa7-480b-899c-9e44aa3c3eb6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6c5a5774-dec1-4ec4-ae9c-9e58888fb978'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '03de9d4c-9c41-4ce2-a61c-9e9b709f6fbd'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0ed6112b-f615-4968-8095-9eb0e99589ca'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '179d01c9-3efe-46f0-bb7f-9ef22449e275'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '11abf832-48db-46d1-9bdf-9f041aa9376d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'caaab5f3-cbc8-4206-b695-9f4a1c8d4915'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '747f51c2-b867-44e7-9765-9f9d74c94870'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e4af21dc-0582-41de-8ee4-9fe1e319d67a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9743b1e5-618b-4bed-9d8c-a02e59154572'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '22641053-354d-4b46-bf76-a02f9432ed13'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '31ea06ea-7216-4e53-ad14-a053d02351a9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'be7f3f75-cc76-4fed-ae83-a084bbda3b43'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '54f79973-2e8f-4fc9-911b-a084d9f1d03a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1d8408e3-347b-4035-b9b8-a08f20208384'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f3e8ccee-07d1-46ba-b715-a137fc80d3b2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a0499cf0-96ad-47cb-9230-a154daf62fbc'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '230b8650-fe23-4b74-b0c6-a16f94f85956'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'fc10023b-3e3c-41dd-89e6-a174b47f67cb'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e5750894-f933-40f8-9fd3-a182d9ee9783'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '910b60fb-55bd-4645-b02f-a18ce0792ba6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '16d614e1-6058-4166-9a7e-a19d2852529f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1498c44a-10a2-47f2-bc32-a1dda63ae309'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '87a2ae6b-ddeb-4040-bd3d-a1fecf1046eb'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '09c0ad96-04e0-4217-84e7-a2020589f5db'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '675274b3-8723-4ebc-b9e6-a2098844d9e7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f8130381-9cfe-4d74-9350-a22969872cf7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6b9f29ae-e231-4dbc-852c-a238b6cff371'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '30721697-9419-450c-a4c7-a23aca937c3b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3c60a23a-87e4-46d5-9358-a246867bd427'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6ef03e5e-c271-454e-ab03-a27fce7ffa06'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd3905906-e50b-49d9-92e1-a2d9ba6b70be'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '427e68b0-a75b-4f0e-9d48-a3066ed411b4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0e8c3064-5379-4ce0-8f2a-a37cf6801d85'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9f8699fa-aaea-4c7a-9937-a3dbc469db82'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0a46557d-2120-4165-8977-a41e1bfa27fd'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9cf51bd2-24bd-401b-89ae-a4392b22797b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2ba958f8-d663-4df4-9ee4-a43c4009782e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0fc39f8e-dfa3-45b6-b0be-a49c777a45f2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b6e7212c-6349-4772-ab1d-a4d0ce41c7c7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b2d01117-b605-48be-a69c-a54474378fd6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'cbcb7f07-6160-4b88-87aa-a5830aaea10a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ad725a2d-5fd2-4e32-81a7-a5a23300d2d1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3ae6b40a-8905-4a8b-a7a8-a5ae37e37549'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '5767b362-d630-4c5e-be68-a5eb43931d22'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '564d51a2-fcd9-43e5-85ef-a5fc57fad594'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e464a82b-0cf7-405a-b109-a61862730618'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0a817414-50d4-4e44-8c73-a62281e2f67f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '098e6df6-2dea-44eb-8ce8-a62e00f5db3b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '09f6778c-2f40-455b-9a4e-a63ca19f7093'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '671378c4-be2d-4292-9100-a64b3efb0ee3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd238662d-409b-4f1d-83de-a6669ca8dbe0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a3a1505f-aa1d-44fe-9bdd-a668670df45d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ec60922a-2477-41e9-b6ae-a683a9012e4f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '85911694-be15-478f-b6de-a69ae3de1f8f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '87fd53b7-468b-4c04-9ca9-a6c617b3dff9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '03fc795e-bd12-4fe9-abda-a6ce08c52f3e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'cff61d73-18cd-4e3d-9171-a6f496e756ae'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '61ab5da4-e942-425b-8b7f-a7234adc8041'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b2e15295-d70e-4b26-8787-a7531662de1a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '55929ca1-7caf-41b0-a914-a76f1218942f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'cefac83e-6fde-4928-b503-a7ca8c01fe59'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6d03d7f2-1644-4b5e-b4df-a7ecd007e144'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '092a8056-cf15-4f5a-ae6a-a85814f7122a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '32edf215-3f23-4a80-a205-a8b311304114'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '63baf285-8420-4ad1-8b55-a8ce5ff47b2a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9910c4e2-7dca-4b52-8659-a90bb51d8138'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b6cb555c-82cc-4b67-be62-a9625406dbf8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3560b309-cf09-451c-b19a-a96e70618876'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9c16fb2b-e8be-4bb5-b761-a9810e1049d5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '043d7f73-b890-4f3d-8d38-a987fef0ba5f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '888741e9-1896-4355-a93e-a9a9ae9bb0a9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '5db83f27-6eab-46a0-b6c0-a9a9be79a6cb'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6ca07f7e-3ab4-40b6-adc4-a9b66e68f9a8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0b608b36-279b-46dd-b432-a9e88611e1ab'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '15ea41b3-e73e-4d54-8851-aa4783b0447e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0b2bb902-af47-441e-ade4-aa6877915fee'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1c56fba5-bee1-41ee-8042-aa6b9638fb42'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6350b467-0756-4927-8bde-aa9afd93fb29'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '08f46895-eca4-496a-8874-aae33113fc67'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '5384f2e7-829d-41b7-b9e5-ab281ace1ce9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '825d2a59-1c1f-4659-a63b-ab318364c22b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0eff0846-24e4-4103-b227-ab421d8d25da'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4fe7b916-0a5a-43bc-8849-ab7061696be5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c017485f-aeab-4123-9f3b-ab743959bd06'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '07b1d59f-d92c-4bb7-87e6-ab962784ac42'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '44fc04b2-c786-4af8-b179-abb7b79f6915'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e5e0b3f4-c546-4018-9470-abcaf996670b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4a91e769-91aa-4965-9562-abceda445aea'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7bd69bdd-2d1a-484e-8326-abda3a7bb4a3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3df411f5-704f-4969-9057-ac41c4cf2858'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'fba4ce7d-fa36-42e8-a7c0-ac5a1c5e94fd'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9ae9a8c7-fc06-4b23-8d5b-ace9546bfd01'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6c404603-948d-45d8-ae86-aceeae5d9cbd'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b6a334fa-fa09-48be-85a8-ad6270e5b82f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b9b17632-4935-4f19-8674-adaac787312b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '56c445d6-c096-4529-a00a-addfe72b261c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd4f61dca-1d33-4828-a231-adf207bed867'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd6211053-7cef-4013-83b4-ae19f3ea6b5d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1bb35d67-9332-4a0c-b2d9-aed79d834bc0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4a9117eb-d3af-4170-9911-aefe63b80eb3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3e97ef4f-71f0-449f-b422-af1390ce08a4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'aec20ef7-4cb4-4d20-9474-af19bb1942b1'
UPDATE dbo.TemplateColumn SET VisiblePosition = 50 WHERE ID = 'e71dfe34-5c48-4698-bb17-af3297694097'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'bcf3abdb-9b85-4079-8465-af8014c10f3d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'cd72d7a2-46c0-4ad1-a099-afbf65b67a03'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '66b7ab94-8cdd-4877-ace8-afce3af66125'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '010c0903-461b-4b6d-b8d7-b002511fee2c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3541de5e-ee15-4f9a-9c21-b02e4e594be9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'bbdd7a4f-83f7-4c10-9594-b067fe745cd1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b275d5ab-a43d-497a-ab94-b0b6e83a1ce4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '05c0dccc-e3d6-4af8-9173-b0ba4fe1ae0a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ae31590d-7fc3-459c-b955-b17018f9e706'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4307eda4-39c4-4860-91d8-b1b18685ebab'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f847df81-e2db-45bb-be71-b1c68b3df807'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c07cc787-46b6-4eef-8c63-b1facf811c4f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'fc6e10cf-7d9b-47f2-afa2-b2139d7a3dab'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'cfede1f4-6903-42ab-baa6-b26d0a391088'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0ea8f332-cf7a-4d89-86c8-b2934d4f4210'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a48317c6-f44e-4d00-af21-b2c94240339d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'fb4fc96d-540c-45bb-a1d8-b2eacf235a97'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1586a17e-a0c2-4633-ad27-b319739776d6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '70cfdbef-aefc-427c-a653-b344876e8863'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8658aa26-dd79-430e-955c-b356568cac33'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2786e493-df73-4ee1-a176-b35c99d70d0c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd5c7b714-7a0f-4650-93da-b3984ff1b0a6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'bd48f19d-0490-440b-8b62-b3aa2649415a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6ac6bcf5-039e-43e6-8211-b3e10801294d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '27975dc3-1123-4aac-adc4-b48e816fa130'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0c750efe-840e-48be-aae9-b49480da4d9b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8af69184-3e90-4148-9f12-b4abda0020ba'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c4abf46c-8d67-4809-9b32-b4f37db6ec3f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '64204c4c-1bd3-4d96-8e0a-b4f826ac9d5b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6fbdb7d4-046a-4662-b1ab-b50bf666f498'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '5108b35a-9399-4011-9da0-b527fe2ab62a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'cf42b8df-3dfe-4adc-af03-b56bab8899ac'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e05118e2-a46d-4c04-9880-b59e1093ba43'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '60070617-fc62-4f0c-9051-b5c207fd3930'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8ded385f-3b96-4206-9927-b5cc3c7650bc'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6595bebc-980b-425f-8a99-b5cfa653a978'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '314d1d9b-9485-4852-9cf9-b5d5fa82f6bb'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e4e41510-ebf4-4d8f-a8db-b5e2a8a7e936'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3aa63e7a-bfd5-46de-a058-b5f7dfe694e6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'aa169626-13d8-4773-9338-b639ac7a9fb7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '07576131-109e-46fb-b58b-b6c83a4444cc'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '61dbc2a0-8d09-4d68-953b-b742f694a90b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6feac7f9-2bfd-4fc3-a8a6-b74435ae4228'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '577e21c6-1e9a-4af0-829e-b75d0316b669'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '5400a51c-50ac-40a3-a824-b82d59365fa1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0ffb4596-fe84-43ed-99a9-b871fd8244d3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e9b01f75-c624-40ee-9ae4-b87e646f550f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '14b2eb1c-cc34-44a4-b5aa-b8b7674542d7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '25a1b6a5-901b-43dd-8d1e-b9082ffe4bba'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '050b565f-d5bb-4164-a4e6-b90c4c36af03'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9e9901c9-f46d-445b-8d4c-b912cd886a6e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '819d3214-08d8-40f4-bcce-b9437ad41312'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '78d565ca-c7aa-4eb7-ad58-b982e55ccbb4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9fac8af8-26f8-434e-81ae-b9a8e61d6e71'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '954a2582-04c6-451f-bbc1-ba97a1ff9fa8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e76bd1dd-a622-450e-9a78-baa96c6d822a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd153a767-837a-4df4-b399-bac8ff04c75d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '57513412-13c6-4efa-9efd-bae25a92fbaa'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c8af2b03-70c7-4764-85e1-bb23e9935b8a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '045f70f8-f47a-43c5-be25-bb25928026b1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a1c66c27-c503-4e1e-9507-bb2a55f0bc12'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9144ea86-72f6-4e36-9ce4-bb686be39505'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '65f83d42-dc26-4cb0-9b22-bb7c2466f394'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3ab03dcb-d857-4ba7-b03c-bb84978a882a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '343f03d9-52a5-40c6-8ecc-bbe817d7d33f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c534667e-f736-4ccf-bdce-bbe964d328fe'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '621e9cdd-14e4-4f30-858a-bc631b54162d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '22bdd366-260a-4221-9985-bc6c281deafe'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a712e950-be6d-45b1-b35c-bc8ef8073aa2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9a89049c-f58c-4d26-b374-bcbb79217d01'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e62d38d1-a7e6-46b9-9a3f-bcbf2379806f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '46aa0499-f11e-4556-857c-bcdacdd07bd8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'de7a965c-708e-49e7-aead-bd13fb06426d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8e83eb67-d85d-4617-af00-bd31482a59b4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4f07fd6b-a49b-4347-b1a5-bd3ed338b5a0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd4ee1134-6936-4cd8-88e1-bd6a630e1bfa'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8afa6828-c43b-46f9-a49d-bd82f5028e3b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a5e87786-d88b-4397-817c-bd95a126c36e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '398e8a03-2dfc-474a-aeaf-bd9b7bd54d8d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd49a8672-9211-42c5-b73e-bdb836db3428'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8db855fa-4aea-4723-90c0-bdb9633b7ef2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1051a497-de2a-4331-ad3e-bdcb0ca592d3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7543e454-68ac-46fc-adb5-bdec0858d4fb'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3ecbdab7-2bf3-462a-930c-be1a6d411b11'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9bacee75-2821-4a7f-9310-be552d591e86'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '99f89c16-f9f6-4095-9954-be5e0603500b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '22bf5af9-5439-4d27-abea-be7d84762d18'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'dad9eb1e-3606-4e0c-8888-bea62e7dbe02'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'dfe3bc2d-cd8d-453b-a8d3-bea9bda8b690'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '46d77bc8-2e6b-45ec-a9eb-bf04175bd43d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '12022203-e73f-4974-9a61-bf1da8b2362f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e7a3994e-f61a-4686-b4fc-bf2c326e59c7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3176f39e-f5d2-4e93-8f9e-bf43e34e22da'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '91f66c75-a39a-47f5-bf10-bf78de1993c4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f3340976-ba2d-46d5-86e5-bf89bcac28ee'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '648db23a-7a3a-477c-8c23-bf92dcba78e6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c79c7457-4038-476e-88b3-bfa6d60df3b1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '355356b8-a2d6-48d6-a30e-c00aba288110'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ddb27295-4c9c-4e12-8ffe-c03cb96c81bf'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f734856c-1460-4531-993c-c03f5d017e0e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9a603516-1f2b-482d-941b-c04097473055'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e7c4d2b3-992d-4065-9971-c048d22cea53'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'af695fbf-7dcf-4f07-b9ee-c06679b98dbe'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '87b5759f-dc82-4f7d-b1b6-c09709bb05ec'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '63e4ab15-9bec-4e8e-8a8f-c0b903ac2db9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c912ee88-b343-403c-93de-c0bdb2933866'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c93b2331-f7d9-46d1-ac7c-c0df579df53c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ff29c87e-0a29-4aaa-8318-c0fd9345239a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd7d0a740-2e77-4bfc-abc2-c0ff33c141d9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'bf6ea5d8-337b-4711-898e-c12e48ffab7c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '91aba27c-e663-4273-b40a-c17374a20196'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0bcbf68e-8dbe-43d0-ad5b-c1c5667ad1f9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '06174800-e44e-4215-b60a-c1c82bb3ad4e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '09032669-f3c7-41d2-bade-c215e85249ad'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '818d9940-668b-4df5-84de-c22d689ca9d7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b9eb2220-fd20-4b59-9d87-c240a32df804'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2612c319-cbd8-44f3-8f4f-c2451286b584'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1fb3d0a9-72bc-4a6f-bc70-c26b11272099'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ffbffb76-a4ac-4232-89bf-c26e0e804511'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8c251b97-c5fd-46d1-849f-c2a8d499d5c6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '91425207-1d73-4cf6-8c15-c303ce218aa0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '818747ea-4f8b-424c-8d6b-c32b021088b7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b67dfe2f-fb5b-4c81-9821-c3739f509f6f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '45103bc8-02ca-4bbc-a26a-c375aafba833'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1849ff12-4bd6-4427-aa76-c398ed774c5e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c11debc3-0033-459d-8f86-c3b5b3940bdb'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f9c68312-e957-43dd-909b-c3e4d4429b5d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '82d88d8c-10a3-4599-9872-c3f318c76ab6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e58cf14b-e948-4662-98dd-c3f3fd78119f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2948d8f2-acdf-41c1-a6f2-c40fdff5b325'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '00decbf0-b5ae-47f9-9b81-c4188afe570e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7baa7d96-171d-457d-9084-c43474b47fee'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6eca65bd-cad1-42f6-a8ab-c4360f156666'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8e97a0bf-b434-44fd-a09d-c46619d71e68'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '38f7ae66-b057-4f66-b5c3-c49dd6df50b0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c5a3c8a3-9ee5-479b-8470-c4beefd8af20'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '63a6ab33-4fd8-47e3-8c00-c4e6d930cb92'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ff1af6d1-ea21-41ec-8885-c4ed531df460'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4d20dfbf-6c2a-4eb0-bdfd-c4ee21a90980'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a308bc8f-1b2e-498c-ad1e-c525a83c772c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6396f2da-f132-4d40-ac7e-c526f7ce697a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a32e2a94-0a9b-4326-8549-c551505ca86c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ce5f807d-6525-4834-86b5-c56243028d39'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9bc28e28-62d9-4fb8-a507-c66f61545f2f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '87f9f114-66a1-471e-8915-c6a985263e7d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '64a767f4-9940-4591-affd-c78dadfe09e5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c17b2f65-8e51-43d0-90b8-c7b0ad7f150e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '44c5a927-7d59-4c1e-96fb-c7c77cb76178'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3d14de2f-30eb-4e94-a7ba-c7e504a56798'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '5dfa1802-bb8e-4c0f-bb96-c7f3f5e4d476'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '75217b90-f876-46b5-9d3f-c81ff8312729'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e43d8471-25c1-4ba2-9f69-c852c682f763'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f45102cb-11c3-4cb2-9873-c8533e3c7917'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f6d897af-fe47-407a-8946-c8c6f6b4aca9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '39ebb36c-e731-4aa6-9b54-c91d997599e4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '007e3672-8be4-4008-82e4-c9399db25a64'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4f03d2e1-1cf0-49ff-ab1f-c9e26d8f388a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '42af796d-e9f6-4b0f-bfe5-ca0076f4a78b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '5765228e-e360-4a40-983a-ca0eb60c3f12'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f4408396-a83f-4786-96b2-ca3f3e21e337'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '468f3713-73f7-490b-8ef1-ca5a14ef496b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0eb98cb9-800e-4b9b-84af-cb0a816fd48e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '45490de6-7d7a-405c-8466-cb387c786e5c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4d906310-7f89-4ed6-a796-cb483b00f62d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b583d659-1692-4609-82cb-cbd2b657c3d1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'acc269fe-eef6-4b0f-b62a-cbf43c6496bc'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '005232a0-482f-40b9-ac71-cc1a903e3950'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '72239895-00a0-478a-94da-cc3cee85e571'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a3222c91-2f73-44a4-baf3-cc6f4d5eaf9c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd89f30eb-c25c-4858-830c-cc72871fa9f4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7fecad6f-7d9f-491b-8e7e-cc8a20df1919'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '361c907a-3181-4e69-b159-cc96037a3900'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6ac6ddef-cd0f-4c38-973c-ccda3b454ca4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0a59bc82-8167-4db2-810b-ccf9a3bf0d19'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a6718c3c-d672-463a-9aa5-cd45781c6156'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'eea15c3c-77f7-41cf-a305-cd48265f339c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b7bee5a3-8139-48d2-be17-cd54b7704b45'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '55976662-6c1a-49d0-91ad-cd60cb1923a1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '60160434-8168-42d2-9328-cda610ef9a4b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd4baa077-5d9d-474c-9893-cdffd04c1e90'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e406548b-ca8c-435d-9787-ce04bd4eed95'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '92b6efd2-5e6a-4e3f-8c8b-ce05445a6a5e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c0075ca9-84b7-4643-ab73-ce240185b7b4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1d57cc5d-1664-4176-bfbe-ce69186d3b6f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8e9b151c-cc89-4e62-80b2-ce8d4ab27b4a'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Mẫu số hóa đơn' WHERE ID = 'f826dc5b-247a-4f92-86f1-ce9a74168b11'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f0a6ac7c-034e-45ae-b184-ceaa67ab141b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a12fee06-3671-4c00-a5e8-ceb22fe13f60'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6a929f37-c9e7-4dfa-bb9a-cec55a06737e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '06ac3bdd-c0c9-4dcc-bee5-ced3b4bcd47f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '84f74c11-5563-44c6-bd95-cf19c203799a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b9245ffd-df7f-47bd-863a-cf45729b1af4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '724967bd-d6a9-4722-8679-cf5dd9d2e4a8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '56ce684e-4d86-4a3b-89a9-cf7464483d62'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '89a40599-47a4-47c3-b13e-cf821d4430cb'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '532b776b-4599-4242-a70d-cfb6184ce326'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '52433bf9-14f8-4533-b30d-cfe4e2052f21'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f7d5267d-5264-4ca6-a006-cfe6001eb1b3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'aaad2231-8329-435a-835e-cff6c5503ce9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '89055d9d-6f8c-4390-a9bc-d09716b09007'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f1e5e0d9-d4df-4468-b178-d0aed3dbacb4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '5e8448a0-a20e-4e26-a10b-d0caf072dd1e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b821d042-5ffb-4777-b1bd-d0e6a1062909'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1035cb53-b762-4610-94de-d129a377e68d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6ff9cc1a-ed9e-4843-ba97-d143c31d8d6b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8655189d-8a3a-47d5-93ac-d1569f7490f7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6fd6db25-7d8a-4b61-8346-d168175c63d6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '979e5313-0bf8-425b-b352-d18236ea2b98'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9aad8545-25b9-4cb8-b25b-d1c7bec20e35'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '376a9cba-2ff9-4017-a2ab-d22310d3c3ab'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e84746dd-9a3c-415d-aff1-d2472b7fdc65'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '84663163-ddec-43d8-8123-d26c96ec12f2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'dc30faba-92aa-4543-9362-d2fc0bdd3620'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2791715d-487c-400d-966f-d3d9841ed4de'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '64f96104-38ce-4270-90eb-d3db586427d7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8fe7503e-5e10-4a3c-bf64-d40ff5e34fda'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '50ae044f-f65a-4127-8e28-d41b2b192723'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a4b2bf0f-399d-456e-a121-d43806c47f8c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '96529c79-837a-4844-97d6-d449312c8777'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '82b5a2a9-0b8a-4299-b23f-d48baeef68ca'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4cd2f404-f347-4e23-bd68-d51d1b1d9173'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6afc02b5-a265-4927-8321-d53297d1d1d8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '59b297d1-e8f6-44df-8ba6-d537938c16e5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '68659b47-8e4d-4ef2-806d-d55050337815'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'aefd7484-cab3-4277-866b-d56215852549'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8a23caae-3556-4890-beae-d56fbb052b5b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6a509a66-6f5c-4637-8feb-d5972f91a22c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f7ce580c-b658-43a3-819b-d5b23fdb3ab4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f46ba24f-e8d0-46b3-a704-d5d7c8d6f131'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '397f171e-5e99-4f75-a1ac-d5f2d932ba88'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '426bdcdf-3beb-420b-b1be-d5f8f7fccbbe'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ec29b307-c5a9-4960-97ab-d64817e2e5fc'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2f34d7ce-f8a8-4b60-a9d6-d64951a57d7d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7234185b-2dd4-4b66-9544-d66c2874f015'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '822863b5-6bfd-41a3-89fb-d68010641f5a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3d8837cd-8203-434e-b463-d69662297834'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'be5dc9e5-195d-413b-9411-d699d502439e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9f14b911-e670-45b5-888b-d73c4c3a4151'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3284b7fb-c9cc-41a3-9686-d74fe28c88fd'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '94130cc6-f68f-4aa0-a6e5-d7870c574bf2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3925e254-bce0-4d58-9c84-d7fef5940dd5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ebce5016-d1b7-44b5-bd6f-d8460baee996'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '07c2204d-bfd2-49f3-9662-d847c6c0f01d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3b4b7f30-d53f-4099-84aa-d883a64636c9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'dea488a2-e0c1-4e74-9215-d8b64ff5bbe1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1163a5e4-c552-4bd3-9fdc-d8cd82b6437d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7387f8c2-f44e-4625-a55c-d8cf5586d44e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '03d495cc-5079-4e01-b989-d8ea433a010a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '61a33a19-28de-4a88-bbd0-d90805c19b9c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9f84720a-fb83-40e8-844b-d90efdbc06bc'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'dcbf55e5-f8fa-404d-9760-d9152e706152'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7b5e5894-a8f4-454c-8100-d9457db9cb2f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8e89eb4c-6594-491e-ae75-d95425e2321d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '630798cf-5f71-495b-ace0-d9990e18bdf1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8da0617b-fa12-44c6-bb40-d9edbaec2abd'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '80f050ac-f62d-4e99-a2d6-da45e8ceaaf1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '51a2e143-b789-4b92-999d-da4b1109df96'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '52520985-9116-44d1-9c52-da793caca79c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1ac153d4-1e8b-4d20-9ed6-da87e28d2a68'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e3ab2514-0d77-4ed8-a5cc-daa4b9b6e8e6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a030cf2f-d598-4024-a901-daf3e0eba754'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '47cef2ed-9708-4f4d-9a43-db05ab3333c1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c6a9b51c-bcda-4d24-9397-db1f7eb45f2b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6956e164-8b7b-4a99-975d-db518ab5a091'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2e750573-5e65-47e3-bd89-db53ea0d00f0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7d426548-4cc6-44db-bf00-db754b63fa19'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8345f61d-1d0a-4919-b551-dbadf036ca87'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7a331e76-d1ad-49b7-9cb7-dc18b0fd0457'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4cf19023-f1a4-46fa-a9d7-dc4636786631'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7ed68b54-224e-42ae-9010-dc84b1fc9f5a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '199239b1-5eab-4cdf-a7b9-dcbb304fbd31'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '179e0e11-d03b-4171-a5d3-dcbbb5445e87'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '730991f9-ba43-45d9-af8d-dcccf20bc5e9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b2d49313-7e8d-4b4b-abc5-dcd4035a4f7f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '70bdfd85-b03a-4916-9068-dd2efb7b15cb'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '43a19c28-d9bd-41c4-9976-dd665d4eb132'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c983c0d5-b679-4479-aefb-dd7085808516'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '098d8566-b700-45dd-9a94-dd83165c0a9a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4abdad52-5a54-46b1-b7ba-ddda71a8a8c1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '899665ef-5747-46db-9f9b-de244cb54456'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2c1f0aec-3e86-49f3-9aad-de38393554c3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '673e614b-acf1-4aa6-9e6a-de497f0d5dce'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f6a30b55-ad2a-4785-b577-dea201b05ed2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '16829d7c-e2d8-4e75-82bf-deb7ec1f59c8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd6cbbab5-d176-40c6-a19d-debd8c245c4f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b442b4e1-4d45-4534-8284-dee834e08691'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '371cc3c1-05d8-4a90-aefb-df3c500b4ca8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '09f4aadd-a99d-472a-9c51-df4b8884be08'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '606956d6-a81d-494c-92f4-df4f3336c33a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7cfefdcd-1844-446f-ba42-df696c132277'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '11cebd0d-7daf-4614-a2e4-dfc0a35f80e3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd9329cbb-3084-41fc-9637-e01b476897d0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '429e0c80-f2c0-42f0-ade0-e0346c689451'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7ad9db4a-5ab2-42dc-bb37-e0395c4542f1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e0732844-6a84-4eb1-b67c-e046b7b0baf7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '675cc848-57d9-4d4e-9efa-e05602374de6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a901e79f-247a-4524-a095-e0ee64ef03c1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2fee27be-1f8c-497d-b49c-e0f9c171b720'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '086d4783-7bca-4c66-a3ff-e114d34cbd13'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a14fa51e-b215-4521-b6b7-e12cf13ce402'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2faf9d48-7c85-4e41-a97a-e12db6d4f4d5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd3c8158e-f64f-47b0-a84b-e17c823c657b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '94df597f-00dd-4fd6-ac0a-e19f75faa180'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '039059e6-144f-4222-b44f-e1e8092fa0ce'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8d1fdaa8-48a4-4c38-9c73-e2056b2cce3a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e23c8312-f5f2-484a-bf2e-e216819a0204'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'aa1a25df-aa33-4075-94e1-e2a278b3a6d5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '5bbcfbc0-5705-42d2-ab88-e2b4481b99b4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6d27ee13-dd0d-4cee-b657-e3212c5eb22d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'bb51ad3b-1bca-4347-b9db-e32209778171'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7d63eb1f-355d-46b6-86ba-e32c2cb6798b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '57c61eb1-6cda-4ecb-a226-e3b4bb2ebd2a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c44ec458-8b38-4685-8f3c-e3c6f1d41a27'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '723c85ea-2968-451e-82bc-e423f3e99e7b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '78b98d84-0637-495c-87c8-e46ab9b7c82f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'eee000c4-2d6e-43da-a430-e47fad3e5c83'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8eedf40f-aace-477e-91bf-e486f0621dc0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '25b3ac44-a4ff-483f-81a6-e4b4b82aa9f8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9cf2ba01-dc4d-4cb4-bac3-e4e6c560ac57'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7ba3fdfa-8c65-4284-a281-e51d8d8d9633'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0cac8d9a-04d2-4bb7-9e64-e51dbcb16725'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b5e27ec2-5087-47f3-90c6-e55a2116bfb8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0e409557-4464-45bd-94d4-e5c9c7129add'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'bad25e33-90d2-48d3-bc7e-e5da979ff626'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '08ddd71a-2537-47d4-b5a7-e63ef153a0ca'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '35b2e36f-9ce5-4f8e-9410-e6925b2e6533'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b52523b9-7a29-45be-a9de-e6ae9ade529c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '867a1de7-7ebb-4d68-8d3e-e6b23c3deca9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3a3afd59-eedf-4a56-a062-e6da765bbec2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '646c78b0-3770-44a3-a1d1-e707a069de30'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c817542e-8d08-45d1-a218-e73ba96558a8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '5f2aad73-9dbd-4e70-a42b-e74c5c63acae'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a34409fb-e01c-4bae-9553-e74ca11ee0be'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e44c5627-4342-4446-af86-e754746f59e5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '35214391-f4a1-4b0a-afb9-e77a872b8dd9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c2f7fd07-7ca2-4545-93b4-e78b6203fee3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c68f2d07-e079-4814-ab5c-e7febd4af15f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ab3fdbdc-9b07-4821-88ab-e814dcdf3128'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9b002418-8014-4949-b4c8-e815b7f225b6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '31a98213-7656-4958-bb54-e838de22bee2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3835f7a8-0d45-4f37-a33b-e84d1e088590'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '067f5fbe-cf75-4919-972b-e8af1788597a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b6694a47-a55d-49df-a65d-e9698e1fac1d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0cccf5cb-3092-4eb0-bf48-e974d1ae21a8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9f1c804a-cbe1-434a-b7d4-e97918f449b7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '614e283c-2fd4-42f7-867d-e98dd311d74f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a1bc94c5-9de8-4f8f-9a35-e9a5eb0f3e79'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd7886c7f-c210-4261-8f38-e9b4fc5556ef'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ba3f996f-6e97-46b7-b42d-e9c1a76952d9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e90d414f-62ce-405e-a11c-e9ce86a8af16'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'aff05876-196e-4714-8b4e-e9f9b3523f65'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8b260b4f-c6a2-4e6d-b4ea-ea028e1f1364'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0705d1f0-3423-4f59-97ae-ea02ac4e905c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '48febfee-80bb-4077-943f-ea451f94abf3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6062458b-fd80-4b26-a72a-ea6050a62bb7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c7ab623c-34a5-49b5-8465-ea6fc344cf75'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'cc6efe2c-70a1-4b37-a803-ea785b00c972'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f6b8ff00-7255-4e5a-be28-eabb4939e3b7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '377f270a-e6fb-439d-a866-ead4a7834021'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1b00439c-a9f2-435d-8dd6-ead4ffc54ce6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7d3c2d7a-39f0-45fd-9e87-eb3ee50240a3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c533922d-b164-4e0b-9d7c-eb5f289dedaa'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'be08d80b-2be8-4edc-b0a9-ebc775f7754a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4b9b7ff2-c254-436c-9b94-ebc9583aba30'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6acb32a6-ddfb-4b54-a227-ebe83a55c651'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e65b4461-293b-47ac-9c1f-ebf167234c0b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '945d9144-4c04-484c-aa2a-ec732bf2de6b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e875e398-773e-4007-9b55-ece636498fa3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b8794276-5eb6-447f-b444-ecf721262c22'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '50a13cb0-41c6-4290-ae38-ed027d3e54a1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '099145dd-b5a8-43c3-88ea-ed1a9ecbbdd6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8f1ab7de-193b-4e12-b3e6-ed3794d9579a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '249770c7-2df0-455d-9657-ed607fe4f3f7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '087fc060-5341-45b6-9f85-ed76cef9d04a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '51775193-6b67-4c60-befc-edb393746ead'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3bff8d27-f1d6-4b6b-a21c-ee1ea17e6c82'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'aaf6dcb9-a591-455c-866a-ee7136102f84'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0b0114d1-67e6-4dd9-b4ec-ee76232152c1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'cb7a5ae3-8d6c-40a8-99f1-ee99174307a9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f5d61fbc-9b3b-44d6-a7b5-eea5e5204713'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a1254f93-30aa-409e-9a24-eec242ffa3ae'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '63e782a9-4424-475f-a595-eed32c5a430d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a5a4d392-517b-4f98-b168-eedefd0eb551'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f34b1627-f2d4-4d2f-8e35-eee544db9e9b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd260d87a-8da5-479f-b143-ef314f9886a9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'fe77e921-d2d9-4e28-a2b7-ef54ab149daf'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c3423999-01d6-41e6-babf-ef5aec8b25c8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2175f6a5-e4fe-426c-9420-ef75b91b8fb7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c54cc438-588a-4c1a-8e52-efde644c8bf3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7bd86892-7bf6-4868-b09b-f0071ac57e05'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '35c1337c-0a10-485d-b5e2-f0113378d76b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6af30839-8f90-4816-9aff-f04ce5db5d61'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e8f176f2-8d55-4ef3-bed8-f082f5c80d2f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ec45fcd4-7714-4e43-a638-f08ab360879c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '60eda7a3-07ed-4293-ad76-f0db0406cf98'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '30f86513-14d7-49e5-a61f-f119ba8bc889'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd97c8342-4068-4706-9514-f17f87e56e9a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '992d3114-d0f7-4784-87cc-f1814fa12a7f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd5bdc2cc-54a7-4fde-b455-f1f6c1b54ba5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd6603a2a-eba1-49f3-9faf-f224c916bfc0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7ffebc8b-4cba-4e0c-bc57-f25ae3d747c6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1223983a-1fcd-4ffb-877d-f2a4cdc16d41'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9806785c-a359-46a3-824d-f2c984dcad81'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7eef7621-a327-4eae-9247-f2fa312f5d99'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '86c94f9d-e3e9-4733-b868-f365a43b4d41'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c206b629-3afb-4bc9-a706-f38345959c18'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f7226e51-d492-4361-9ff8-f392e7f547fc'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4a588853-82ab-45fe-b66d-f3943ef4a711'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '573161bd-aa8a-4ae7-bfb8-f3ae6bd32fc8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2ebddffb-f74b-41c0-8fc0-f4077e0d27f9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f1b66789-58ca-408e-9ea1-f414ad8d87fd'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8b09fa3f-1211-46bf-a157-f42eb6e8f344'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'fa94ab50-b186-4016-a67e-f489421a1e1f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7dab1c8a-ce41-4cc3-a471-f499da3d959b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '53378295-3e56-4575-ab80-f4a59a1919a2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '43168f65-6428-49f5-b13e-f4e92358bab5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2879c47e-3a52-47a3-b70d-f4ed9b184d86'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9e56becc-9456-4ac6-95c7-f50e0156706d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'dcffe8ec-3cde-4413-a2f8-f50eb69c2afe'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '73f43502-678f-45d7-8cae-f5424226c515'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '935ddc47-43e4-43e8-981d-f57fd0b9b988'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ba3781d3-0c99-4db8-928a-f5973a435728'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8fccca15-e0b8-4dec-9457-f5be24dfdca7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4bb5aadb-4c9e-41a4-aa05-f5f7a5bb9cd9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3092d96f-8b03-42f0-84fb-f61554483b55'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0f5c0b1b-3782-4fc0-adf4-f621c6fa0edb'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4d0c7420-25d1-4500-8f6e-f6323aa22a85'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'cce377e2-6fb9-49a7-959e-f63db8bcd932'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd7fb8e97-c632-482d-9405-f6472b180cc4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '367fbec9-f468-4eab-a191-f6529d5cf394'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'fa7f37d5-9eaf-455d-89a7-f6b0c34fc55a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'faec3693-a3a8-42ad-95c9-f6fe37aee8e4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '75b728f6-b17e-4ccb-8c14-f744205a09fc'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '24fe4fa7-fb8f-46fa-8d9e-f750d7382602'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'afe283cc-59ad-40fc-b81e-f75ee09a3f7f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ecd736c6-db4c-4a73-93c1-f7a14641e0f2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '128e54d0-d5c2-410d-a3b7-f7a3a9ddad4b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9471adab-44f0-4e52-9b9c-f889f2b7c736'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1139b3be-cf7c-4fe1-8d06-f8d0206d82cc'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '25353c4d-eab7-4fc6-a99a-f8d7720be014'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '92688425-adc8-4467-b720-f8d7912aea30'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '97cf4ab5-89ea-4a0a-b4d9-f91418eaec00'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8a544708-add4-40c3-92c5-f9149e2394e2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '21f5b42f-6d71-4acc-991d-f91f6802ac65'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '28a48af5-a0e8-428b-a7aa-f973a2296e56'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0db3ff52-adc0-4152-bb99-f97d8b002a96'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '14ebadfa-174e-49d6-8897-f985732d50bc'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f7002a3b-21ce-4086-878a-f9a2152bfdc6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd6680d15-eaaf-44dc-bfb4-f9e4234efa46'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '36744603-b551-46a1-acb0-fa1cf509ddb2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd25cebf6-8a33-411c-8030-fa21ab4db552'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '45bdc4ab-7e62-468d-a473-fa30b81d84fc'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7131a10c-a307-47c6-9dcd-fb2fc9aee5f3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ae471df8-5492-4e1a-876c-fb66895e423d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a358ac7f-2dc7-4ad0-a994-fbb6035c3621'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a2a6492b-3840-43e5-9908-fbc657fc4b08'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '5a47adb9-d23b-468f-a5c5-fc2146291c4b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'db466504-1d34-4a54-81f7-fc2353909326'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0416e0e0-4e84-4259-a60f-fc23e8e1bc62'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '04708951-dd14-4dda-a65c-fc7e80b9f92a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9a2ceb0a-c6c8-45fc-a368-fc9c13545387'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '96abcc83-e3e0-4521-942b-fcc9b2e72407'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '674c5484-2d1e-4b5e-b1d6-fd15745b223e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ed5d200e-5376-4d6c-9268-fd390a9764ba'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9883ff7f-262d-4819-a8c7-fd62b613113f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1c8ebea0-7146-4632-8dce-fd66436752aa'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '369970d2-0bbd-4e74-9185-fd86d47bbb41'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a4a019c9-0c34-46ff-8cb2-fdb976982112'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0e9a471a-4440-4980-946d-fe2297c1b2e6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '43d2f297-c919-40cb-b7d0-fe3a9a6a6b6d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'cfe7cb90-e45f-4a99-bd57-fe54a0f7570e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '46c3707c-c031-40e4-896e-fe68107da1f8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2e4d7d7b-d05d-4576-99b2-fe7a1d766d74'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '488d6f92-c8bc-432e-84c9-ff016e6a59c0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b2f7c854-7498-4d1d-89da-ff3de80885aa'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '87f4844a-a581-476c-87ca-ff5c811607bf'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd5f3b903-0a00-4ea0-a791-ff653cb7aca3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e81341b3-8f7a-4e41-b04d-ff70f7f739fe'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '058e9c51-f373-4f10-aa30-ff7837f801db'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '5206bae3-5ad1-42d4-92fe-ff8310afde20'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ed560832-946c-4362-a107-ff83d137eee8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9fa80f57-1d61-48d0-b256-ff8fc1d32714'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b2bf9c8e-8e2f-48fd-885c-ffbdf0088639'

INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('be4e6529-1044-4bc8-b8b9-1acea3cf0c52', 'dbd2299f-a098-4c7a-8283-4ed38c7ed486', N'TaxExchangeRate', N'Tỷ giá tính thuế', NULL, 0, 0, 100, 0, 0, 0, 6)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('a63ee478-5c00-4f2e-8de0-1df19b91f406', 'a0e9ea20-66d2-440b-96b1-6312999f6f58', N'TaxExchangeRate', N'Tỷ giá tính thuế', NULL, 0, 0, 100, 0, 0, 0, 6)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.VoucherPatternsReport SET VoucherPatternsCode = N'MCPayment-Voucher' WHERE ID = 104
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateColumn WITH NOCHECK
  ADD CONSTRAINT FK_TemplateColumn_TemplateDetail FOREIGN KEY (TemplateDetailID) REFERENCES dbo.TemplateDetail(ID) ON DELETE CASCADE ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

GO
ENABLE TRIGGER ALL ON dbo.TemplateColumn
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

IF @@TRANCOUNT>0 COMMIT TRANSACTION
GO

SET NOEXEC OFF

