SET CONCAT_NULL_YIELDS_NULL, ANSI_NULLS, ANSI_PADDING, QUOTED_IDENTIFIER, ANSI_WARNINGS, ARITHABORT, XACT_ABORT ON
SET NUMERIC_ROUNDABORT, IMPLICIT_TRANSACTIONS OFF
GO

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

BEGIN TRANSACTION
GO

ALTER TABLE [dbo].[RSTransfer]
  ADD [InvNo] [nvarchar](25) NULL DEFAULT (NULL)
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[RSTransfer]
  ADD [InvTemplate] [nvarchar](25) NULL DEFAULT (NULL)
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'RSTransfer', 'COLUMN', N'InvNo'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'RSTransfer', 'COLUMN', N'InvTemplate'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[RefVoucherRSInwardOutward]
  ADD [TypeID] [int] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'RefVoucherRSInwardOutward', 'COLUMN', N'TypeID'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[RefVoucher]
  ADD [TypeID] [int] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'RefVoucher', 'COLUMN', N'TypeID'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_GetBkeInv_Sale]
      (
      @BranchID UNIQUEIDENTIFIER ,
      @IncludeDependentBranch BIT ,
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @IsSimilarBranch BIT
    )
AS
BEGIN 
/*
VATRate type :
-1: Không chịu thuế
0: thuê 0%
5: 5%
10: 10%
*/
/*add by cuongpv de thong nhat cach lam tron so lieu*/
	DECLARE @tbDataLocal TABLE(
		VATRate DECIMAL(25,10),
		so_hd NVARCHAR(25),
		ngay_hd DATETIME,
		Nguoi_mua NVARCHAR(512),
		MST NVARCHAR(50),
		Mat_hang NVARCHAR(512),
		Amount DECIMAL(25,0),
		DiscountAmount DECIMAL(25,0),
		Thue_GTGT DECIMAL(25,0),
		TKThue NVARCHAR(25),
		flag int,
		RefID UNIQUEIDENTIFIER,
		RefType int,
		StatusInvoice int
	)
/*end add by cuongpv*/
	if @IsSimilarBranch = 0
	Begin
		/*add by cuongpv*/
		INSERT INTO @tbDataLocal
		/*end add by cuongpv*/
		 select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST,
		  Description as Mat_hang, Amount, DiscountAmount, VATAmount as Thue_GTGT, VATAccount as TKThue,1 flag
		  ,a.ID as RefID, a.TypeID as RefType, a.StatusInvoice
		    /*Edit by cuongpv (Amount-DiscountAmount) as Doanh_so*/
		   from SAinvoice a join SAInvoiceDetail b on a.id = b.SAInvoiceID where cast(InvoiceDate As Date) between @FromDate and @ToDate and Recorded = 1 AND a.BillRefID is null
		union all
		select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST, 
		Description as Mat_hang, Amount, DiscountAmount, VATAmount as Thue_GTGT, VATAccount as TKThue,1 flag  /*Edit by cuongpv (Amount-DiscountAmount) as Doanh_so*/
		,a.ID as RefID, a.TypeID as RefType, a.StatusInvoice
		 from  SABill a join SABillDetail b on a.ID = b.SABillID  where cast(InvoiceDate As Date) between @FromDate and @ToDate
		union all
		select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST,
		  Description as Mat_hang,  Amount, DiscountAmount, VATAmount as Thue_GTGT, VATAccount as TKThue,-1 flag  /*Edit by cuongpv (Amount-DiscountAmount) as Doanh_so*/
		,a.ID as RefID, a.TypeID as RefType, a.StatusInvoice
		 from SAReturn a join SAReturnDetail b on a.id = b.SAReturnID
		where cast(InvoiceDate As Date) between @FromDate and @ToDate and recorded = 1 

		update @tbDataLocal set Amount = 0, DiscountAmount = 0, Thue_GTGT = 0 where StatusInvoice = 5 or StatusInvoice = 3 or RefID In (Select SAInvoiceID from TT153DeletedInvoice)
		

		select VATRate, so_hd, ngay_hd, Nguoi_mua, MST, Mat_hang, (Amount - DiscountAmount) as Doanh_so, Thue_GTGT, TKThue, flag,RefID,RefType
		from @tbDataLocal order by VATRate, ngay_hd, so_hd, Nguoi_mua
	End
	else
	Begin
		/*add by cuongpv*/
		INSERT INTO @tbDataLocal
		/*end add by cuongpv*/
			select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST,
			  a.Reason as Mat_hang, Amount, DiscountAmount, VATAmount as Thue_GTGT, VATAccount as TKThue,1 flag  /*Edit by cuongpv (Amount-DiscountAmount) as Doanh_so*/
			   ,a.ID as RefID, a.TypeID as RefType, a.StatusInvoice
			   from SAinvoice a join SAInvoiceDetail b on a.id = b.SAInvoiceID where cast(InvoiceDate As Date) between @FromDate and @ToDate and Recorded = 1 AND a.BillRefID is null
			union all
			select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST, 
			a.Reason as Mat_hang, Amount, DiscountAmount, VATAmount as Thue_GTGT, VATAccount as TKThue,1 flag  /*Edit by cuongpv (Amount-DiscountAmount) as Doanh_so*/
			 ,a.ID as RefID, a.TypeID as RefType, a.StatusInvoice
			 from  SABill a join SABillDetail b on a.ID = b.SABillID  where cast(InvoiceDate As Date) between @FromDate and @ToDate
			union all
			select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST,
			  a.Reason as Mat_hang,  Amount, DiscountAmount, VATAmount as Thue_GTGT, VATAccount as TKThue,-1 flag  /*Edit by cuongpv (Amount-DiscountAmount) as Doanh_so*/
			 ,a.ID as RefID, a.TypeID as RefType, a.StatusInvoice
			 from SAReturn a join SAReturnDetail b on a.id = b.SAReturnID
			where cast(InvoiceDate As Date) between @FromDate and @ToDate and recorded = 1

		update @tbDataLocal set Amount = 0, DiscountAmount = 0, Thue_GTGT = 0 where StatusInvoice = 5 or StatusInvoice = 3 or RefID In (Select SAInvoiceID from TT153DeletedInvoice)

		select VATRate, 
			   so_hd, 
			   ngay_hd, 
			   Nguoi_mua, 
			   MST,
			   Mat_hang, 
			   sum(Doanh_so) Doanh_so, 
			   sum(Thue_GTGT) Thue_GTGT, 
			   TKThue,
			   flag
		from
			( select VATRate, so_hd, ngay_hd, Nguoi_mua, MST, Mat_hang, (Amount - DiscountAmount) as Doanh_so, Thue_GTGT, TKThue, flag 
			  from @tbDataLocal
			) aa
		group by VATRate, 
			   so_hd, 
			   ngay_hd, 
			   Nguoi_mua, 
			   MST,
			   Mat_hang, 
			   TKThue ,
			   flag
		order by VATRate, ngay_hd, so_hd,Nguoi_mua
	End
end;
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[EMContract]
  ADD [InvoiceDate] [datetime] NULL DEFAULT (NULL)
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'EMContract', 'COLUMN', N'InvoiceDate'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sp_refreshview '[dbo].[ViewVoucherInvisible]'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sp_refreshview '[dbo].[ViewVouchersCloseBook]'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sp_refreshview '[dbo].[ViewVouchersCloseBook1]'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

ALTER PROCEDURE [dbo].[Proc_GL_GeneralDiaryBook_S03a]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @GroupTheSameItem BIT ,/*cộng gộp các bút toán giống nhau*/
    @IsShowAccumAmount BIT
AS 
    BEGIN
    
       DECLARE @PrevFromDate AS DATETIME
       SET @PrevFromDate = DATEADD(MILLISECOND, -10, @FromDate)
       DECLARE @StartDate DATETIME
        SELECT TOP 1
                @StartDate = CONVERT(DATETIME, Data, 103)
        FROM    dbo.SystemOption
        WHERE   Code LIKE N'%ApplicationStartDate%'

		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/

        IF @GroupTheSameItem = 1 
            BEGIN
			select * into #t1 from
            (

        SELECT  ROW_NUMBER() OVER ( ORDER BY gl.PostedDate, gl.RefDate
						 		,CASE WHEN GL.TypeID = 4012 THEN 1 ELSE 0 END
								, GL.RefNo , GL.TypeID ) AS RowNum ,
                        CAST (1 AS BIT) AS IsSummaryRow ,
						CAST(0 AS BIT) AS IsBold ,
                        GL.[ReferenceID] ,
                        GL.[TypeID] ,
                        GL.[PostedDate] ,
                        GL.[RefDate] ,
                        GL.No AS [RefNo],
                        GL.No AS RefNo1,
                        NULL AS RefOrder ,
                        GL.[InvoiceDate] ,
                        GL.[InvoiceNo] ,
                        CASE WHEN (Account like '133%' OR AccountCorresponding like '3331%') OR ( AccountCorresponding like '133%' OR Account like '3331%')
						THEN (CASE WHEN (GL.[VATDescription] IS NULL OR GL.[VATDescription] ='' ) THEN GL.[Reason] ELSE GL.[VATDescription] END)
						ELSE GL.[Reason]
						END AS JournalMemo,/*edit by cuongpv Description -> Reason*/

                        /*bổ sung trường diễn giải thông tin chung cr 137228*/              
						GL.Reason AS JournalMemoMaster ,/*edit by cuongpv Description -> Reason*/
                        GL.[Account] as AccountNumber ,
                        GL.[AccountCorresponding] as CorrespondingAccountNumber ,
						'' AS [RefTypeName],
                        '' AS AccountObjectCode ,
                        '' AS AccountObjectName ,
                        '' AS EmployeeCode ,
                        '' AS EmployeeName ,
                        '' AS ExpenseItemCode ,
                        '' AS ExpenseItemName ,
                        '' AS JobCode ,
                        '' AS JobName ,
                        '' AS ProjectWorkCode ,
                        '' AS ProjectWorkName ,
                        '' AS OrderNo ,
                        '' AS [PUContractCode] ,
                        '' AS [ContractCode] ,
                        '' AS ListItemCode ,
                        '' AS ListItemName ,
                        SUM(GL.[DebitAmount]) AS DebitAmount ,
                        null AS CreditAmount ,
                        '' as BranchName,
                        '' AS OrganizationUnitCode ,
                        '' AS OrganizationUnitName ,
                        '' AS MasterCustomField1 ,
                        '' AS MasterCustomField2 ,
                        '' AS MasterCustomField3 ,
                        '' AS MasterCustomField4 ,
                        '' AS MasterCustomField5 ,
                        '' AS MasterCustomField6 ,
                        '' AS MasterCustomField7 ,
                        '' AS MasterCustomField8 ,
                        '' AS MasterCustomField9 ,
                        '' AS MasterCustomField10 ,
                        '' AS CustomField1 ,
                        '' AS CustomField2 ,
                        '' AS CustomField3 ,
                        '' AS CustomField4 ,
                        '' AS CustomField5 ,
                        '' AS CustomField6 ,
                        '' AS CustomField7 ,
                        '' AS CustomField8 ,
                        '' AS CustomField9 ,
                        '' AS CustomField10 ,
                        '' AS UnResonableCost ,
                        CASE WHEN SUM(DebitAmount) <> 0
                                THEN Gl.Account
                                    + AccountCorresponding
                                WHEN SUM(CreditAmount) <> 0
                                THEN gl.AccountCorresponding
                                    + GL.Account
                        END AS Sort ,
                        GL.ReferenceID
                        AS RefIDSort,
                        0 AS SortOrder,
                        0 AS DetailPostOrder,
						MAX(GL.OrderPriority) as OrderPriority,
                        SUM(DebitAmount) as OrderTotal,
						1 AS OderType
                FROM      @tbDataGL GL /*edit by cuongpv [dbo].[GeneralLedger] -> @tbDataGL*/
                        LEFT JOIN dbo.AccountingObject ao ON ao.ID = GL.AccountingObjectID
                WHERE     gl.PostedDate BETWEEN @FromDate AND @ToDate
                        AND ISNULL(gl.AccountCorresponding,'') <> ''
                GROUP BY  GL.[ReferenceID] ,
                        GL.[TypeID] ,
                        GL.[PostedDate] ,
                        GL.[RefDate] ,
                        GL.[RefNo] ,
                        GL.No ,
                        GL.[InvoiceDate] ,
                        GL.[InvoiceNo] ,
                        GL.[VATDescription],
						GL.[Reason] ,
                        GL.[Account] ,
                        GL.[AccountCorresponding]
                                            
                HAVING    SUM(GL.[DebitAmount]) <> 0 /*edit by cuongpv bo (OR SUM(GL.[CreditAmount]) <> 0)*/
			UNION ALL 
			/*add by cuongpv*/
			SELECT  ROW_NUMBER() OVER ( ORDER BY gl.PostedDate, gl.RefDate
						 		,CASE WHEN GL.TypeID = 4012 THEN 1 ELSE 0 END
								, GL.RefNo , GL.TypeID ) AS RowNum ,
                        CAST (1 AS BIT) AS IsSummaryRow ,
						CAST(0 AS BIT) AS IsBold ,
                        GL.[ReferenceID] ,
                        GL.[TypeID] ,
                        GL.[PostedDate] ,
                        GL.[RefDate] ,
                        GL.No,
                        GL.No ,
                        NULL AS RefOrder ,
                        GL.[InvoiceDate] ,
                        GL.[InvoiceNo] ,
                        CASE WHEN (Account like '133%' OR AccountCorresponding like '3331%') OR ( AccountCorresponding like '133%' OR Account like '3331%')
						THEN (CASE WHEN (GL.[VATDescription] IS NULL OR GL.[VATDescription] ='') THEN GL.[Reason] ELSE GL.[VATDescription] END)
						ELSE GL.[Reason]
						END AS JournalMemo,/*edit by cuongpv Description -> Reason*/
                        /*bổ sung trường diễn giải thông tin chung cr 137228*/              
						GL.Reason AS JournalMemoMaster ,/*edit by cuongpv Description -> Reason*/
                        GL.[Account] as AccountNumber ,
                        GL.[AccountCorresponding] as CorrespondingAccountNumber ,
						'' AS [RefTypeName],
                        '' AS AccountObjectCode ,
                        '' AS AccountObjectName ,
                        '' AS EmployeeCode ,
                        '' AS EmployeeName ,
                        '' AS ExpenseItemCode ,
                        '' AS ExpenseItemName ,
                        '' AS JobCode ,
                        '' AS JobName ,
                        '' AS ProjectWorkCode ,
                        '' AS ProjectWorkName ,
                        '' AS OrderNo ,
                        '' AS [PUContractCode] ,
                        '' AS [ContractCode] ,
                        '' AS ListItemCode ,
                        '' AS ListItemName ,
                        null AS DebitAmount ,
                        SUM(GL.[CreditAmount]) AS CreditAmount ,
                        '' as BranchName,
                        '' AS OrganizationUnitCode ,
                        '' AS OrganizationUnitName ,
                        '' AS MasterCustomField1 ,
                        '' AS MasterCustomField2 ,
                        '' AS MasterCustomField3 ,
                        '' AS MasterCustomField4 ,
                        '' AS MasterCustomField5 ,
                        '' AS MasterCustomField6 ,
                        '' AS MasterCustomField7 ,
                        '' AS MasterCustomField8 ,
                        '' AS MasterCustomField9 ,
                        '' AS MasterCustomField10 ,
                        '' AS CustomField1 ,
                        '' AS CustomField2 ,
                        '' AS CustomField3 ,
                        '' AS CustomField4 ,
                        '' AS CustomField5 ,
                        '' AS CustomField6 ,
                        '' AS CustomField7 ,
                        '' AS CustomField8 ,
                        '' AS CustomField9 ,
                        '' AS CustomField10 ,
                        '' AS UnResonableCost ,
                        CASE WHEN SUM(DebitAmount) <> 0
                                THEN Gl.Account
                                    + AccountCorresponding
                                WHEN SUM(CreditAmount) <> 0
                                THEN gl.AccountCorresponding
                                    + GL.Account
                        END AS Sort ,
                        GL.ReferenceID
                        AS RefIDSort,
                        1 AS SortOrder,
                        0 AS DetailPostOrder,
						MAX(GL.OrderPriority) as OrderPriority,
                        SUM(CreditAmount) as OrderTotal,
						1 AS OderType                    
                FROM      @tbDataGL GL /*edit by cuongpv [dbo].[GeneralLedger] -> @tbDataGL*/
                        LEFT JOIN dbo.AccountingObject ao ON ao.ID = GL.AccountingObjectID
                WHERE     gl.PostedDate BETWEEN @FromDate AND @ToDate
                        AND ISNULL(gl.AccountCorresponding,'') <> ''
                GROUP BY  GL.[ReferenceID] ,
                        GL.[TypeID] ,
                        GL.[PostedDate] ,
                        GL.[RefDate] ,
                        GL.[RefNo] ,
                        GL.No ,
                        GL.[InvoiceDate] ,
                        GL.[InvoiceNo] ,
                        GL.[VATDescription],
						GL.[Reason] ,
                        GL.[Account] ,
                        GL.[AccountCorresponding]
                                            
                HAVING    SUM(GL.[CreditAmount]) <> 0
			/*end add by cuongpv*/
            ) t
			order by PostedDate,RefNo,OrderPriority,SortOrder
			END
        ELSE 
            BEGIN
			select * into #t2 from
				(

			SELECT  
								ROW_NUMBER() OVER ( ORDER BY gl.PostedDate, gl.RefDate
						 			,CASE WHEN GL.TypeID = 4012 THEN 1 ELSE 0 END
									, GL.RefNo , GL.TypeID ) AS RowNum ,							
								CAST (1 AS BIT) AS IsSummaryRow ,
								CAST(0 AS BIT) AS IsBold ,
								GL.[ReferenceID] ,
								GL.[TypeID] ,
								GL.[PostedDate] as PostedDate,
								GL.[RefDate] ,
								GL.No AS [RefNo],
								GL.No AS RefNo1,
								'' AS RefOrder ,
								GL.[InvoiceDate] ,
								GL.[InvoiceNo] ,
								CASE WHEN (Account like '133%' OR AccountCorresponding like '3331%') OR ( AccountCorresponding like '133%' OR Account like '3331%')
								THEN (CASE WHEN (GL.[VATDescription] IS NULL OR GL.[VATDescription] ='') THEN GL.[Reason] ELSE GL.[VATDescription] END)
								ELSE GL.[Reason]
								END AS JournalMemo,/*edit by cuongpv Description -> Reason*/
								/* bổ sung trường diễn giải thông tin chung cr 137228*/              
								GL.Reason AS JournalMemoMaster ,
								GL.[Account] as AccountNumber ,
								GL.[AccountCorresponding] as CorrespondingAccountNumber ,
								'' as RefTypeName,
								ao.AccountingObjectCode ,
								ao.AccountingObjectName ,
								'' as EmployeeCode,
								'' as EmployeeName,
								'' as ExpenseItemCode ,
								'' as ExpenseItemName ,
								'' as JobCode,
								'' as JobName,
								'' as ProjectWorkCode ,
								'' as ProjectWorkName ,
								'' as OrderNo,
								'' as PUContractCode,
								'' as ContractCode ,
								'' as ListItemCode ,
								'' as ListItemName ,
								GL.[DebitAmount] ,
								GL.[CreditAmount] ,
								'' as BranchName,
								'' as OrganizationUnitCode,
								'' as OrganizationUnitName,
								'' as MasterCustomField1,
								'' as MasterCustomField2,
								'' as MasterCustomField3,
								'' as MasterCustomField4,
								'' as MasterCustomField5,
								'' as MasterCustomField6,
								'' as MasterCustomField7,
								'' as MasterCustomField8,
								'' as MasterCustomField9,
								'' as MasterCustomField10,
								'' as CustomField1,
								'' as CustomField2,
								'' as CustomField3,
								'' as CustomField4,
								'' as CustomField5,
								'' as CustomField6,
								'' as CustomField7,
								'' as CustomField8,
								'' as CustomField9,
								'' as CustomField10,
								N'Chi phí hợp lý' AS UnResonableCost ,
								CASE WHEN DebitAmount <> 0
										THEN Gl.Account
											+ AccountCorresponding
											+ CAST(DebitAmount AS NVARCHAR(22))
										WHEN CreditAmount <> 0
										THEN gl.AccountCorresponding
											+ GL.Account
											+ CAST(CreditAmount AS NVARCHAR(22))
								END AS Sort ,
								GL.ReferenceID
								AS RefIDSort,
								0 as SortOrder,
								0 as DetailPostOrder,
								GL.OrderPriority as OrderPriority,
								1 AS OderType

						FROM      @tbDataGL GL /*edit by cuongpv [dbo].[GeneralLedger] -> @tbDataGL*/
								LEFT JOIN dbo.AccountingObject ao ON ao.ID = GL.AccountingObjectID
						WHERE     gl.PostedDate BETWEEN @FromDate AND @ToDate
								AND ISNULL(gl.AccountCorresponding,
											'') <> ''
								AND ( GL.[DebitAmount] <> 0
										OR GL.[CreditAmount] <> 0
									)
				 ) t 
				order by t.PostedDate, t.OrderPriority
            END 
		IF @IsShowAccumAmount = 1 
		BEGIN
		if @GroupTheSameItem=1
		begin
		insert into #t1 (JournalMemo,JournalMemoMaster,RefTypeName,AccountObjectCode ,AccountObjectName , EmployeeCode ,EmployeeName ,ExpenseItemCode ,ExpenseItemName ,JobCode , JobName , ProjectWorkCode ,ProjectWorkName ,OrderNo , PUContractCode ,ContractCode,ListItemCode , ListItemName ,DebitAmount,CreditAmount, BranchName, OrganizationUnitCode ,OrganizationUnitName , MasterCustomField1 ,MasterCustomField2 , MasterCustomField3 ,MasterCustomField4 ,MasterCustomField5 , MasterCustomField6 ,
                         MasterCustomField7 ,
                         MasterCustomField8 ,
                         MasterCustomField9 ,
                         MasterCustomField10 ,
               CustomField1 ,
                         CustomField2 ,
                        CustomField3 ,
                         CustomField4 ,
                        CustomField5 ,
                         CustomField6 ,
                        CustomField7 ,
                       CustomField8 ,
                         CustomField9 ,
                         CustomField10 ,
                         UnResonableCost ,SortOrder,DetailPostOrder,OderType)
		select 
		N'Số lũy kế kỳ trước chuyển sang'AS JournalMemo,
		N'Số lũy kế kỳ trước chuyển sang'AS JournalMemoMaster,
		'' AS [RefTypeName],
		'' AS AccountObjectCode ,
                        '' AS AccountObjectName ,
                        '' AS EmployeeCode ,
                        '' AS EmployeeName ,
                        '' AS ExpenseItemCode ,
                        '' AS ExpenseItemName ,
                        '' AS JobCode ,
                        '' AS JobName ,
                        '' AS ProjectWorkCode ,
                        '' AS ProjectWorkName ,
                        '' AS OrderNo ,
                        '' AS [PUContractCode] ,
                        '' AS [ContractCode] ,
                        '' AS ListItemCode ,
                        '' AS ListItemName ,
						 SUM(DebitAmount) AS DebitAmount,
						 SUM(CreditAmount) AS CreditAmount,
						'' as BranchName, 
                        '' AS OrganizationUnitCode ,
                        '' AS OrganizationUnitName ,
                        '' AS MasterCustomField1 ,
                        '' AS MasterCustomField2 ,
                        '' AS MasterCustomField3 ,
                        '' AS MasterCustomField4 ,
                        '' AS MasterCustomField5 ,
                        '' AS MasterCustomField6 ,
                        '' AS MasterCustomField7 ,
                        '' AS MasterCustomField8 ,
                        '' AS MasterCustomField9 ,
                        '' AS MasterCustomField10 ,
                        '' AS CustomField1 ,
                        '' AS CustomField2 ,
                        '' AS CustomField3 ,
                        '' AS CustomField4 ,
                        '' AS CustomField5 ,
                        '' AS CustomField6 ,
                        '' AS CustomField7 ,
                        '' AS CustomField8 ,
                        '' AS CustomField9 ,
                        '' AS CustomField10 ,
                        '' AS UnResonableCost ,
						0 as SortOrder,
						0 as DetailPostOrder,
						0 AS OderType
						 from @tbDataGL GL where GL.PostedDate<@FromDate
		end
		else
		begin
		insert into #t2 (RefOrder,JournalMemo,JournalMemoMaster,RefTypeName,AccountingObjectCode ,AccountingObjectName , EmployeeCode ,EmployeeName ,ExpenseItemCode ,ExpenseItemName ,JobCode , JobName , ProjectWorkCode ,ProjectWorkName ,OrderNo , PUContractCode ,ContractCode,ListItemCode , ListItemName ,DebitAmount,CreditAmount,BranchName, OrganizationUnitCode ,OrganizationUnitName , MasterCustomField1 ,MasterCustomField2 , MasterCustomField3 ,MasterCustomField4 ,MasterCustomField5 , MasterCustomField6 ,
                         MasterCustomField7 ,
                         MasterCustomField8 ,
                         MasterCustomField9 ,
                         MasterCustomField10 ,
               CustomField1 ,
                         CustomField2 ,
                        CustomField3 ,
                         CustomField4 ,
                        CustomField5 ,
                         CustomField6 ,
                        CustomField7 ,
                       CustomField8 ,
                         CustomField9 ,
                         CustomField10 ,
                         UnResonableCost ,SortOrder,DetailPostOrder,OderType)
		select '' as RefOrder,N'Số lũy kế kỳ trước chuyển sang'AS JournalMemo,N'Số lũy kế kỳ trước chuyển sang'AS JournalMemoMaster,'' AS [RefTypeName],
		'' AS AccountObjectCode ,
                        '' AS AccountObjectName ,
                        '' AS EmployeeCode ,
                        '' AS EmployeeName ,
                        '' AS ExpenseItemCode ,
                        '' AS ExpenseItemName ,
                        '' AS JobCode ,
                        '' AS JobName ,
                        '' AS ProjectWorkCode ,
                        '' AS ProjectWorkName ,
                        '' AS OrderNo ,
                        '' AS [PUContractCode] ,
                        '' AS [ContractCode] ,
                        '' AS ListItemCode ,
                        '' AS ListItemName ,SUM(DebitAmount) AS DebitAmount,
						 SUM(CreditAmount) AS CreditAmount,'' as BranchName, 
                        '' AS OrganizationUnitCode ,
                        '' AS OrganizationUnitName ,
                        '' AS MasterCustomField1 ,
                        '' AS MasterCustomField2 ,
                        '' AS MasterCustomField3 ,
                        '' AS MasterCustomField4 ,
                        '' AS MasterCustomField5 ,
                        '' AS MasterCustomField6 ,
                        '' AS MasterCustomField7 ,
                        '' AS MasterCustomField8 ,
                        '' AS MasterCustomField9 ,
                        '' AS MasterCustomField10 ,
                        '' AS CustomField1 ,
                        '' AS CustomField2 ,
                        '' AS CustomField3 ,
                        '' AS CustomField4 ,
                        '' AS CustomField5 ,
                        '' AS CustomField6 ,
                        '' AS CustomField7 ,
                        '' AS CustomField8 ,
                        '' AS CustomField9 ,
                        '' AS CustomField10 ,
                        '' AS UnResonableCost ,0 as SortOrder,0 as DetailPostOrder,0 AS OderType from @tbDataGL GL where GL.PostedDate<@FromDate
		end
		END
		IF @IsShowAccumAmount = 1
        BEGIN
            if @GroupTheSameItem = 1
                begin
                    select * from #t1 order by OderType, PostedDate, OrderPriority
                end
            else
                begin
                    select * from #t2 order by OderType, PostedDate, RefNo, OrderPriority, SortOrder
                end
        END
    else
        begin
            if
                @GroupTheSameItem = 1
                begin
                    select * from #t1 order by PostedDate, OrderPriority
                end
            else
                begin
                    select * from #t2 order by PostedDate, RefNo, OrderPriority, SortOrder
                end
        end
		
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE dbo.TemplateColumn
  DROP CONSTRAINT FK_TemplateColumn_TemplateDetail
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.Type
  DROP CONSTRAINT FK_Type_TypeGroup
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.Template
  DROP CONSTRAINT FK_Template_Type
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
IF (OBJECT_ID('FK__TT153Adju__TypeI__25869641', 'F') IS NOT NULL)
BEGIN
ALTER TABLE dbo.TT153AdjustAnnouncement
  DROP CONSTRAINT FK__TT153Adju__TypeI__25869641
END
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
IF (OBJECT_ID('FK__TT153Dele__TypeI__2A6B46EF', 'F') IS NOT NULL)
BEGIN
ALTER TABLE dbo.TT153DeletedInvoice
  DROP CONSTRAINT FK__TT153Dele__TypeI__2A6B46EF
END
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
IF (OBJECT_ID('FK__TT153Dest__TypeI__22AA2996', 'F') IS NOT NULL)
BEGIN
ALTER TABLE dbo.TT153DestructionInvoice
  DROP CONSTRAINT FK__TT153Dest__TypeI__22AA2996
END
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
IF (OBJECT_ID('FK__TT153Lost__TypeI__1FCDBCEB', 'F') IS NOT NULL)
BEGIN
ALTER TABLE dbo.TT153LostInvoice
  DROP CONSTRAINT FK__TT153Lost__TypeI__1FCDBCEB
END
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
IF (OBJECT_ID('FK__TT153Publ__TypeI__1CF15040', 'F') IS NOT NULL)
BEGIN
ALTER TABLE dbo.TT153PublishInvoice
  DROP CONSTRAINT FK__TT153Publ__TypeI__1CF15040
END
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
IF (OBJECT_ID('FK__TT153Regi__TypeI__1A14E395', 'F') IS NOT NULL)
BEGIN
ALTER TABLE dbo.TT153RegisterInvoice
  DROP CONSTRAINT FK__TT153Regi__TypeI__1A14E395
END
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

INSERT dbo.SystemOption(ID, Code, Name, Type, Data, DefaultData, Note, IsSecurity) VALUES (137, N'AllowInputCostUnit', N'cho phép nhập đơn giá vống bằng tay', 6, N'0', N'0', N'0: not check 1:check', 0)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'fc27a63f-a860-4923-93d8-00c50b90981d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b68e16f1-b7c6-46ce-8189-02c4d6065bca'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3e824812-60f9-4098-8f12-044539eebf9e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6e08ad6d-d749-45a0-b781-05b3c92619e6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a355f881-7056-4a6f-92e6-07314c79477c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '962231e7-047e-4dd9-ae39-083a02296e40'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c316d687-25d8-4f92-97c7-0958ed16a0a4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '899ac36a-50e6-4af6-9bdd-097cc6155605'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f51eb1e7-0e5e-4464-a370-09c45483108d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7759f004-1039-4ab6-b305-09f0db235033'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6851efb6-ced4-43da-b6af-09fdeb45d92e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'bdc916a4-0597-4ece-b33a-0a26bbcdf7fc'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd0477fb1-f7b4-41a2-83f4-0b3761958acf'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ff3b4087-8396-479c-9cc0-0c25f220b9d7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ad7cf3cf-4051-4380-a975-0c8950097344'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '49f59a01-66c8-4a26-a7ad-0db298ca8933'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'dbdc415e-e756-4063-8fa5-0e291b4a1c42'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ac3a58cb-3a24-49f1-a548-0e2de80387ed'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '80e3373e-e963-49d1-ab37-0f526a76605f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0802e018-2265-4c23-9301-12cfdea022ea'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4add69ae-3aa4-4b63-acac-14459d74bf19'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4890c84f-b25b-4b2e-9290-14495eb42df8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4da8fb5b-36a6-442b-8130-14df271d5e61'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'fc001771-2961-45a8-b5ee-15766556ed9b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4f3dd4e3-4382-4a4d-b8c2-15fd85ee8193'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4684cdbb-daed-4d09-a4d1-1613b1402d74'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '41be0902-33a0-4d74-a4d0-1aad0bdf8724'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd80a650b-a567-4ab6-b216-1b7cd1d4268c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ffe39249-3f3f-4f5a-b821-1c2a64d70fa2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'fa4311b7-71c1-4c5c-94d8-1db350a8aa25'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '832709e4-fa3b-4189-90fd-1f1eb25b41f5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a6370f07-75be-4d3e-b88a-1f8cd246d082'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8e23107c-d371-44ee-903e-1ffd8b36e6ed'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9b2dc173-c370-4794-81a9-2001446421ba'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b76c5295-91bd-4782-b481-2019eb44c10a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0a0ed8c2-f6ef-4920-a120-2064f7e93d17'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '664fe7f8-2878-4e43-b6b5-2107ea22a732'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '903da45e-5f58-428a-ab21-2449d7dabb2e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e73aa1b1-d1fc-4cea-95ce-24fa670050a4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd013fd2e-cda3-4bc0-90f4-2538dea955d5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6fc3ac71-9683-4362-ac51-253d8e8f94ce'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd50ad3de-083b-44f3-82c5-25d4310f775b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2dd486af-e2cc-4a9a-aafa-260780468d36'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c1644b93-fd2b-4834-b678-26efc836b478'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '652e3ecf-0287-4d98-a473-276b7a32e988'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '36367dc6-097e-47f7-8f5d-277bb77023eb'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '143cdc34-d48e-40a8-a794-2c9292b6ba3a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'cc1eb229-f402-4cc1-b168-2ced4bfb3b2e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '668c3d53-2a8f-4d3f-aec6-32d47cb5e43f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '5b2fa02d-8b77-42ca-ae88-345fcc96bf90'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2095412f-193d-412b-8acb-34b61abb747f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6d15f371-4b2e-4b20-814f-368cea00d559'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '74e8d606-d278-4ace-bdf8-36b6c42060ff'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '81a1caca-e350-47c4-89fc-37478d564c64'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '5d0f56ec-71f7-409d-86db-37f3fa8dc290'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3cfd3ec2-1de7-47f7-8f5d-3816165758d3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b0f9e916-ecd6-424e-a713-39ed4e050738'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '893f7d1b-1dfe-464d-982c-3b832562f013'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2ec50789-67fb-4b0c-9c01-3c515234c8f0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '77afbe83-0adb-4c61-b213-3ccf8b9433b9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '69756ea2-4ab2-4ac6-9344-3d6079d48e97'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '97dfed3d-11ba-448e-89ae-3f36991c8a3d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c41bfbd1-9a96-4f91-b9df-3f629ada1827'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9a1dcf1c-c657-4c4e-87fc-3f7167a301e9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd258f003-258f-4ba8-a4d0-3fb532626c08'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '5fb019ce-44f9-47ff-a31c-41918cb49d47'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1ac31c57-f1b4-44c5-8593-42260ba9089a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '07b995a2-fee9-4d13-8ad9-42f419f0732a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b687faa5-9217-4f9d-b4a0-435f5541056a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '73942eaf-84d9-4fbe-8260-43d721bb63bb'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '152d9f6b-bf7a-41d7-be50-444b56475f8d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '26e3f856-76ac-46fd-bad0-45c0137a7a18'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '343c1ba9-a041-4e00-8e54-4a3f23376153'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '48aeabde-4ed1-4fbe-81bb-4a5c38619c6b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a9f02183-c49b-42e7-9e59-4bf45b77a4ae'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '1207e407-4b08-4f18-b31b-4d29fbbb4427'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4182d17d-bc49-4e15-af21-4d88e70dd0c6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0e59eca7-d42d-409b-9644-4dca7d68e4dc'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c13bf850-e8e4-4eda-a7f8-4e3078484e2e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c960aec1-7f62-4e9c-8046-4e33812a0181'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9e0cc203-76fd-47d7-a826-4e7256124995'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '91ae2d6f-85ce-43d5-ac03-4e9be1e299c5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '928e8674-235a-4334-a968-4fd870ec0237'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '468322bd-b0c4-4b01-8e37-503e0ca29400'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6cbf97ae-bcd1-480f-a150-516bbce755b6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1744b927-39ac-440a-8990-559817b83b40'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3c8265ea-44d5-4599-b560-55d5db0a1379'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f5340233-3235-4321-b511-576892bc5498'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '672bc0a8-8152-45a2-8f46-5ce0db51ab2d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6ce40e57-18d9-4e8e-8109-60569ff6c01c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'bb718386-45ed-42f2-9ddb-60a378fccb61'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b2c509fd-20f6-4bc6-8e4b-60fcfd26bf20'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c0295430-93ec-4bdd-b7f0-613d08a90a33'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '1cff1890-dab3-4e2c-bfd0-617c45250bae'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1fe7e106-1a97-4201-b1e1-620ed54c4db9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '84f40b94-9606-4c41-8305-628b12f0c580'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8b2330a2-9032-48fd-9188-62aafe4514b8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '18d1b442-7b92-4dce-9fcf-62ba9da5f360'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e2bf7e4b-8852-4d28-8893-62de476cac4b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd428824d-5101-4564-8bb1-6495d50623a5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7ad3da8f-ee13-40e6-a6f3-668a28365c6c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8f2fa2b1-8b60-4256-aca1-66a06b6f085f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'efd43b66-c2fa-4ca4-8c0a-6798147f6eca'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b4e9e4b2-a333-4538-946c-6a49de4fbcd2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '40d4e882-e37c-47a6-8431-6c884ed02642'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '5290e9bc-3fa1-4e18-bafc-6cb9d2faf2a2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6f1278a1-c44c-44d7-8565-6e46b874dc80'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '05969c5d-4510-4941-b3ba-6e530286e0e4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b5e4faf6-c78b-4558-800e-6e92454ae7b3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b72884b9-5075-4a7e-a6d4-6ea6fe78397f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b96f1767-d3c0-4f89-ac0e-6fcb82ca63c7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7bdb5f14-0133-4069-84f6-711051773492'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8f8e2503-91cd-4178-9e06-7373f0a001a6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c3ecf3b3-4304-48ac-8891-75028acc23d1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '6f14b7ee-5886-400b-ab93-756efc4faa9e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e3205507-6987-4643-bf1f-75ffd240db3d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '47e9e981-d143-4d66-b721-763a209e4582'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'be2a22dd-5d0e-43b5-9117-7790b541ff1c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2676aa30-c028-4b8e-95f6-784b1fa78f14'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6af58a63-6bea-4fde-b098-785182ca946a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '5493dda3-f900-42ff-8067-78a3ce783b3e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '340b2b51-9c3b-4349-9a41-7941a9b98dfe'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '12ef6ed0-d3e0-4fbd-b68b-796bdd220511'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2bd53789-8cb6-47a4-a8de-7dcc1f2a0482'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '92299a79-f271-417e-a258-7dff099b09ed'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'cf740cf2-4397-4af7-9be2-7e32491b713d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '861086d5-b778-4c5c-b096-7ee67858e578'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '0aeac8ea-f6d3-43f3-978b-80bbdcaff8ce'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '00b52291-e4a9-4dc4-ba15-8247225d4bbd'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'df37fe1b-d654-4e76-a2c5-82d54f8d4958'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '094a75cb-b454-43f7-b2b9-83508ff2984c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f136ffab-545a-4916-8212-84172bca3939'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a74d55ce-274e-49c3-81fb-86cb373341c8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b2454890-99d7-4afa-afea-89119c2ce6ad'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '56788be2-f078-4bd3-b9f0-8abbffe616f9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '201f58c2-957a-439b-be33-8c6d1166361f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '653a25eb-8b44-4387-b502-8db1a19746d6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'dfa3adda-eb61-4aed-b746-8f6acc5c3eba'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7ef70a73-e39d-4ce3-8540-8fb758c51cca'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'fd8ab9b4-87e0-48bb-8c93-8fd1cf722132'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '947c3d36-5ac5-4d04-868c-9100b324938e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'f62df1d2-f7b8-4052-a1e4-9100b77920a1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f618c241-ddb4-4a8c-a335-926229cbacc5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '437c4035-a349-48f5-b308-932d91b1d1b7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2cb8f5be-776f-4b87-8ec5-9607ce738d0d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a58903ea-3f78-40da-a252-97bb3394087c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2354a68c-fcda-4569-833c-9a2d23c2f29e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9515e3af-c6e3-4733-9362-9ad41a9b9917'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a44b60ee-5907-4b61-bcbe-9b2bf27a2e11'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '90833f78-13f5-476d-9fca-9ddf661746a5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c5e3172a-ef37-4e10-85fa-9fb30424dacf'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'bff8980c-1840-40fe-adf8-a11e5ffc9f31'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'bbe64d0a-ed82-439e-bb32-a1db5e260f34'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a352073a-f2d2-4aa3-b9b9-a239c8d13c7e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e3ff4c27-c017-446a-9edb-a2bb9fdef544'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ebde1185-544d-478e-a3ff-a3336da7a2fa'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '59e20ce3-6b84-449d-88eb-a38b90cb74d7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8dc19930-aec8-4d82-bd9e-a40e9941762f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8bc1554c-a5b1-46b0-95aa-a57ee42bcdf5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f4bdafa8-0f10-45ed-b01d-a632791a9d9a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8c599b78-abae-4c7e-ac4f-a6ec5c7a5803'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7156a7f6-813e-4e05-99c6-a72691b4c4c0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'da6412b0-3ae1-4d15-b09f-a88e99391291'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '587952c3-bef4-456b-9a8b-a8a6308099b7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c549a582-300d-463d-aba2-a97089e4f478'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6458246d-e915-4d64-8b13-a9f274f70b74'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '29341e09-5818-4642-9f21-aa36358da17b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '034ce2c7-6cf4-4c1f-bdba-aa4eb6932983'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '75eccc95-bf09-4a20-9619-aa609ff3299c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '9ae9a8c7-fc06-4b23-8d5b-ace9546bfd01'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'fe8138b5-4c19-4f58-8f41-ad3c48fae17b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'b9b17632-4935-4f19-8674-adaac787312b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'cd544513-e708-4d86-b0f9-adbfbb0a5171'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '32810956-3a66-4d42-839c-ae7948a689a6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ac56c4cb-2c26-453d-8f9d-aea1a9c5f80c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '569c9f4a-3458-4f7c-8ee8-af9cceab7230'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9967c4e3-9e40-4542-9f60-b039393e8ace'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8cfca8f8-c5cd-4e8f-831f-b278883b5900'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '82f2b208-662f-48d5-8831-b597d484ce0d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c3dd7314-940e-4e4a-8314-b8cfa6035f9d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd5c684d4-2407-4f1c-a4f9-b94aa42acebc'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7f7186a9-1e52-41ec-a587-b97380c04dad'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1d625c16-2d3e-4b67-b991-ba1cb1532573'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '13fd80ef-3a05-4a6f-99fb-ba4fe7ce98aa'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '38419242-ad66-41f1-8700-bc9cbdf89da7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '817971ea-eb69-43e7-91f1-bcec53c6c0b7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'fbfddac9-ae58-4e88-9dbe-bd5753ff866e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'ff29c87e-0a29-4aaa-8318-c0fd9345239a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2032434a-6da2-4409-a402-c16260ae546f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9d5894e7-2c93-4e14-bf75-c28472a71fb6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '74c5b3c9-3bb9-443c-88fa-c3c88ea70fbb'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a9cbfef1-34e2-4876-b7cf-c5d3df42c323'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '48352cf1-3851-47f8-a20c-c61eac15c6c9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c2c5c067-272c-450e-948a-ca3892d873ec'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '35e06c02-81bd-4800-bb8b-caecc3d70148'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b0da1d58-331b-4404-b255-cb901adb6ef2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd36e6ae4-74ea-45f0-bb6f-cbb1733d46e3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd7deb7eb-cbb4-415f-9a7f-cc7982409e01'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ead081f7-5a6d-40a9-a086-cda9bb9a31ed'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '59ac9de5-2fa7-480e-a9b7-cedd3ee53495'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9b58f952-ee32-4c6a-b84d-cf4db79e1486'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '23108d18-fad9-4391-91d5-cf5b4b32a339'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '34ce3e0e-60d8-49fd-9d52-cfae93174dad'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '191a8c3c-0f35-490b-86d1-d002bd685ded'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'dc5f2a05-bf88-421a-b9c7-d02b38233dd0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4106dd3d-e3fd-447d-b7d1-d32654166902'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '54b4429e-dcb5-4c86-a0ab-d36f2961b44e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '43abd25d-0db6-4f62-a4a5-d7e43b7ab106'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9a560359-131a-4bb8-84c2-d9a89f2a31d1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2d532023-956b-4364-8086-da2c3c6e1e9f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7a23cb31-0305-4434-b4f4-da80c6d8fc0a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'df3e84c6-a886-481c-83b8-dbb6e9e11a90'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7c25fc32-8fbb-4e14-b1b8-dbccd3ac65f2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3a59cbfe-bdc3-4afe-89ab-dbd4d6664c87'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6c7b9536-18ce-420c-a56c-dcff0fea7989'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e5f62741-89f8-4275-8a62-dd5d4bda238f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ad991f32-b460-49d4-a9de-de19c913bc90'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b1338dcb-8615-49eb-a064-e0190098bfc4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '791ca4be-5deb-4d90-94ce-e1089d209af5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ff78fcda-f958-4872-a32a-e3aa7763d737'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1d5cca5c-e212-443a-8f2c-e434ffacc12e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '99d8a0d0-3bf9-4a55-ab2b-e43b2babace8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6ec63054-3c20-420a-968e-e4722e2bddca'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '77c67bf9-5844-489a-ad49-e47a6d7d4199'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'da9f8717-5dd7-44ea-b1e6-e723ac10bd65'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '35214391-f4a1-4b0a-afb9-e77a872b8dd9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7ac0cd3d-0591-4cab-b706-e7a6e75c1631'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a813f580-1ddc-455c-9ed3-e7afbb391370'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0b264373-1137-4fad-84ff-e880d3bfd452'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '5af260be-8e92-42eb-b787-e930fc0c1708'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f6707baf-b557-4cb1-b2d2-e9e1e37a0e52'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b768bfc3-9b9b-4ede-b28d-eb369721df65'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0cb6e53a-cc2b-4df2-b471-ec511b0246e6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1c1d1a15-8463-48c1-a1e4-ec73aa962dc9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd12a546c-741b-40ed-b1d9-ed807dcecd84'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'fa886121-f586-4ea7-bf52-ee004c4a3bca'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '690e07c3-e742-4fc6-92c3-ee0a97dac65e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9054b638-9988-4c6a-ba14-ee13f128b66c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b21cf324-4925-49d7-9368-ef5303f7cf4b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '71fce5b1-d268-44b4-846d-f01c51486c48'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '14906112-f347-4067-b0ad-f1047e855818'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '06513697-7a8f-409c-ac76-f186e0f2083d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '22958800-dc8a-4e49-9932-f1fecbff4112'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b3658b10-b3e7-40ce-ab90-f2a0576c6ce4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '323a695b-3905-4770-849f-f2b626a4f33b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f6020767-d0c4-40f3-8961-f3d0a5caa1f0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'de7112c3-432e-41b5-9ae5-f4221f04b2a5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '64b79cdd-e0b6-41c7-ab85-f580cf557865'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8efc1948-7ee9-42c1-a1d9-f6feb9cd5021'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4532bee6-b33c-46f3-8586-f789593238ec'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a5501f9e-0220-42f1-9b28-f7f2b0905e74'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e42504aa-1e8a-4c85-b02d-f81c217446fd'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '13e96dad-f5a0-41c9-8ac6-fa4bd88e8740'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '889eaea2-bd5a-4a85-8f29-fb24182196da'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ed2b02ac-0b10-46a7-b851-fc83b34db51f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6f408c3c-4ccc-4e33-b928-fc8587adb46f'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.Type SET TypeName = N'Chuyển kho - Xuất kho kiêm vận chuyển nội bộ' WHERE ID = 420

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
SET IDENTITY_INSERT dbo.Type ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
INSERT dbo.Type(ID, TypeName, TypeGroupID, Recordable, Searchable, PostType, OrderPriority) VALUES (421, N'Chuyển kho - Xuất gửi đại lý', 42, 1, 1, 1, 71)
INSERT dbo.Type(ID, TypeName, TypeGroupID, Recordable, Searchable, PostType, OrderPriority) VALUES (422, N'Chuyển kho - Xuất chuyển nội bộ', 42, 1, 1, 1, 71)
GO
SET IDENTITY_INSERT dbo.Type OFF
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.VoucherPatternsReport SET VoucherPatternsName = N'Đơn mua hàng' WHERE ID = 122

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
SET IDENTITY_INSERT dbo.VoucherPatternsReport ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (316, 0, 0, N'GOtherVoucher-CT-2', N'Chứng từ kế toán (Mẫu quy đổi)', N'GOtherVoucher-CT-2.rst', 1, N'600,690,602,620,601,660,670')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (317, 0, 0, N'MBCredit-Voucher-TTBT-2', N'Chứng từ kế toán (Mẫu quy đổi)', N'MBCredit-Voucher-TTBT-2.rst', 1, N'170,174')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (318, 0, 0, N'MBDeposit-Voucher-2', N'Chứng từ kế toán (Mẫu quy đổi)', N'MBDeposit-Voucher-2.rst', 1, N'160,161,163,415')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (319, 0, 0, N'MBITransfer-Voucher-2', N'Chứng từ kế toán (Mẫu quy đổi)', N'MBInternalTransfer-Voucher-2.rst', 1, N'150')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (320, 0, 0, N'MBTellerPaper-Voucher-2', N'Chứng từ kế toán (Mẫu quy đổi)', N'MBTellerPaper-Voucher-2.rst', 1, N'120,121,122,123,124,125,128')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (321, 0, 0, N'MCPayment-Voucher-2', N'Chứng từ kế toán (Mẫu quy đổi)', N'MCPayment-Voucher-2.rst', 1, N'110,111,112,113,114,115,116,118')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (322, 100, 0, N'MCReceipt-Voucher-2', N'Chứng từ kế toán (Mẫu quy đổi)', N'MCReceipt-Voucher-2.rst', 1, N'100,101')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (323, 400, 0, N'NhapKho.CTKT-2', N'Chứng từ kế toán (Mẫu quy đổi)', N'NhapKho.CTKT-2.rst', 1, N'400,401,404,410,414,411')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (324, 0, 0, N'PPDiscountReturn-Voucher-2', N'Chứng từ kế toán (Mẫu quy đổi)', N'PPDiscountReturn-Voucher-2.rst', 1, N'220,230,413')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (325, 102, 0, N'PPInvoice-Voucher-SECCT-2', N'Chứng từ kế toán (Mẫu quy đổi)', N'PPInvoice-Voucher-SECCT-2.rst', 1, N'131,141')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (326, 0, 0, N'PPInvoice-Voucher-2', N'Chứng từ kế toán (Mẫu quy đổi)', N'PPInvoice-Voucher-2.rst', 1, N'210,260,261,263,117,127,402,264,262,171')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (327, 0, 0, N'PPService-DV-CT-2', N'Chứng từ kế toán (Mẫu quy đổi)', N'PPService-DV-CT-2.rst', 1, N'133,143,173,126')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (328, 0, 0, N'PPService-Voucher-2', N'Chứng từ kế toán (Mẫu quy đổi)', N'PPService-Voucher-2.rst', 1, N'240')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (329, 0, 0, N'SAQuote-BH-CT-2', N'Chứng từ kế toán (Mẫu quy đổi)', N'SAQuote-BH-CT-2.rst', 1, N'321,322,162,320,102,412,323,324,325,103')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (330, 0, 0, N'SAReturn-CT-2', N'Chứng từ kế toán (Mẫu quy đổi)', N'SAReturn-CT-2.rst', 1, N'330,340,403')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (331, 0, 0, N'TIIncrement-ChungTuKT-2', N'Chứng từ kế toán (Mẫu quy đổi)', N'TIIncrement-ChungTuKT-2.rst', 1, N'430,902,903,904,905,906')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (332, 0, 0, N'SAQuote-BH-PXK-SL-HD-A4', N'Phiếu xuất kho (Số lô - Hạn dùng)', N'SAQuote-BH-PXK-SL-HD-A4.rst', 1, N'321,322,320,412,323,324,325')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (333, 0, 0, N'SAQuote-BH-PXKBH-SL-HD', N'Phiếu xuất kho bán hàng (Số lô - Hạn dùng)', N'SAQuote-BH-PXKBH-SL-HD.rst', 1, N'321,322,320,412,323,324,325')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (334, 0, 41, N'RSOutward-SL-HD', N'Phiếu xuất kho (Số lô - Hạn dùng)', N'RSOutward-SL-HD.rst', 1, NULL)
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (335, 0, 43, N'RSOutward-SAInvoice-SL-HD', N'Phiếu xuất kho bán hàng (Số lô - Hạn dùng)', N'RSOutward-SAInvoice-SL-HD.rst', 1, NULL)
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (336, 0, 0, N'FAIncrement-ChungTuTSCD-2', N'Chứng từ kế toán (Mẫu quy đổi)', N'FAIncrement-ChungTuTSCD-2.rst', 1, N'132,142,119,129,172,500')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (337, 0, 0, N'SAReturn-PNK-SL-HD', N'Phiếu nhập kho (Số lô - Hạn dùng)', N'SAReturn-PNK-SL-HD.rst', 1, N'330,403')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (338, 0, 0, N'PPInvoice-InwardStockaa-SL-HD', N'Phiếu nhập kho (Số lô - Hạn dùng)', N'PPInvoice-InwardStockaa-SL-HD.rst', 1, N'210,260, 261, 263, 264, 262, 402')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (339, 400, 0, N'NhapKho.PNK-SL-HD', N'Phiếu nhập kho (Số lô - Hạn dùng)', N'NhapKho.PNK-SL-HD.rst', 1, N'400,401,404')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (340, 0, 0, N'PPDiscountReturn-PXK-SL-HD', N'Phiếu xuất kho (Số lô - Hạn dùng)', N'PPDiscountReturn-PXK-SL-HD.rst', 1, N'220,413')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (341, 0, 0, N'XuatKho.PXK-A4-SL-HD', N'Phiếu xuất kho (Số lô - Hạn dùng)', N'XuatKho.PXK-A4-SL-HD.rst', 1, N'410,414,411')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (402, 0, 40, N'RSInward-SL-HD', N'Phiếu nhập kho (Số lô - Hạn dùng)', N'RSInward-SL-HD.rst', 1, NULL)
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (403, 0, 0, N'SAQuote-BH-BK-HH-DV', N'Bảng kê hàng hóa, dịch vụ', N'SAQuote-BH-BK-HH-DV.rst', 1, N'321,322,162,320,102,412,323,324,325,103')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (404, 0, 0, N'PPDiscount-MH-BK-HH-DV', N'Bảng kê hàng hóa, dịch vụ', N'PPDiscount-MH-BK-HH-DV.rst', 1, N'220')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (405, 0, 0, N'SABill-BH-BK-HH-DV', N'Bảng kê hàng hóa, dịch vụ', N'SABill-BH-BK-HH-DV.rst', 1, N'326')
GO
SET IDENTITY_INSERT dbo.VoucherPatternsReport OFF
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DBCC CHECKIDENT('dbo.VoucherPatternsReport', RESEED, 405)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateColumn WITH NOCHECK
  ADD CONSTRAINT FK_TemplateColumn_TemplateDetail FOREIGN KEY (TemplateDetailID) REFERENCES dbo.TemplateDetail(ID) ON DELETE CASCADE ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.Type WITH NOCHECK
  ADD CONSTRAINT FK_Type_TypeGroup FOREIGN KEY (TypeGroupID) REFERENCES dbo.TypeGroup(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.Template WITH NOCHECK
  ADD CONSTRAINT FK_Template_Type FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE CASCADE ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153AdjustAnnouncement WITH NOCHECK
  ADD CONSTRAINT FK__TT153Adju__TypeI__25869641 FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153DeletedInvoice WITH NOCHECK
  ADD CONSTRAINT FK__TT153Dele__TypeI__2A6B46EF FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153DestructionInvoice WITH NOCHECK
  ADD CONSTRAINT FK__TT153Dest__TypeI__22AA2996 FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153LostInvoice WITH NOCHECK
  ADD CONSTRAINT FK__TT153Lost__TypeI__1FCDBCEB FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153PublishInvoice WITH NOCHECK
  ADD CONSTRAINT FK__TT153Publ__TypeI__1CF15040 FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153RegisterInvoice WITH NOCHECK
  ADD CONSTRAINT FK__TT153Regi__TypeI__1A14E395 FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

IF @@TRANCOUNT>0 COMMIT TRANSACTION
GO

SET NOEXEC OFF