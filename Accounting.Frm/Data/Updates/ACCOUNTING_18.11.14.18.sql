SET CONCAT_NULL_YIELDS_NULL, ANSI_NULLS, ANSI_PADDING, QUOTED_IDENTIFIER, ANSI_WARNINGS, ARITHABORT, XACT_ABORT ON
SET NUMERIC_ROUNDABORT, IMPLICIT_TRANSACTIONS OFF
GO

USE [ACCCompare]
GO

IF DB_NAME() <> N'ACCCompare' SET NOEXEC ON
GO

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

BEGIN TRANSACTION
GO

CREATE TABLE [dbo].[SABillDetail] (
  [ID] [uniqueidentifier] NOT NULL,
  [SABillID] [uniqueidentifier] NOT NULL,
  [MaterialGoodsID] [uniqueidentifier] NOT NULL,
  [RepositoryID] [uniqueidentifier] NULL,
  [Description] [nvarchar](512) NULL,
  [DebitAccount] [nvarchar](25) NULL,
  [CreditAccount] [nvarchar](25) NULL,
  [Unit] [nvarchar](25) NULL,
  [Quantity] [decimal](25, 10) NULL,
  [UnitPrice] [money] NOT NULL,
  [UnitPriceOriginal] [money] NOT NULL,
  [Amount] [money] NOT NULL,
  [AmountOriginal] [money] NOT NULL,
  [DiscountRate] [decimal](25, 10) NULL,
  [DiscountAmount] [money] NOT NULL,
  [DiscountAmountOriginal] [money] NOT NULL,
  [DiscountAccount] [nvarchar](25) NULL,
  [VATRate] [decimal](25, 10) NULL,
  [VATAmount] [money] NOT NULL,
  [VATAmountOriginal] [money] NOT NULL,
  [VATAccount] [nvarchar](25) NULL,
  [RepositoryAccount] [nvarchar](25) NULL,
  [CostAccount] [nvarchar](25) NULL,
  [AccountingObjectID] [uniqueidentifier] NULL,
  [CostSetID] [uniqueidentifier] NULL,
  [ContractID] [uniqueidentifier] NULL,
  [StatisticsCodeID] [uniqueidentifier] NULL,
  [DepartmentID] [uniqueidentifier] NULL,
  [ExpenseItemID] [uniqueidentifier] NULL,
  [BudgetItemID] [uniqueidentifier] NULL,
  [CustomProperty1] [nvarchar](512) NULL,
  [CustomProperty2] [nvarchar](512) NULL,
  [CustomProperty3] [nvarchar](512) NULL,
  [OrderPriority] [int] IDENTITY,
  [DetailID] [uniqueidentifier] NULL,
  CONSTRAINT [PK__SABillDetail] PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[SABill] (
  [ID] [uniqueidentifier] NOT NULL,
  [BranchID] [uniqueidentifier] NULL,
  [TypeID] [int] NOT NULL,
  [AccountingObjectID] [uniqueidentifier] NULL,
  [AccountingObjectName] [nvarchar](512) NULL,
  [AccountingObjectAddress] [nvarchar](512) NULL,
  [InvoiceType] [int] NOT NULL,
  [InvoiceDate] [datetime] NULL,
  [InvoiceNo] [nvarchar](25) NULL,
  [InvoiceSeries] [nvarchar](25) NULL,
  [InvoiceForm] [int] NULL,
  [InvoiceTypeID] [uniqueidentifier] NULL,
  [InvoiceTemplate] [nvarchar](25) NULL,
  [CurrencyID] [nvarchar](3) NULL,
  [ExchangeRate] [decimal](25, 10) NULL,
  [TotalAmount] [money] NOT NULL,
  [TotalAmountOriginal] [money] NOT NULL,
  [TotalDiscountAmount] [money] NOT NULL,
  [TotalDiscountAmountOriginal] [money] NOT NULL,
  [TotalVATAmount] [money] NOT NULL,
  [TotalVATAmountOriginal] [money] NOT NULL,
  [CustomProperty1] [nvarchar](512) NULL,
  [CustomProperty2] [nvarchar](512) NULL,
  [CustomProperty3] [nvarchar](512) NULL,
  [RefDateTime] [datetime] NULL,
  [TemplateID] [uniqueidentifier] NULL,
  [Reason] [nvarchar](512) NULL,
  [ListNo] [nvarchar](25) NULL,
  [ListDate] [datetime] NULL,
  [CompanyTaxCode] [nvarchar](50) NULL,
  [ContactName] [nvarchar](512) NULL,
  [PaymentMethod] [nvarchar](50) NULL,
  [IsAttachList] [bit] NULL,
  [ListCommonNameInventory] [nvarchar](512) NULL,
  [BankAccountDetailID] [uniqueidentifier] NULL,
  [StatusInvoice] [int] NULL,
  [StatusSendMail] [bit] NULL,
  [DateSendMail] [datetime] NULL,
  [Email] [nvarchar](512) NULL,
  [StatusConverted] [bit] NULL,
  [IDReplaceInv] [uniqueidentifier] NULL,
  [IDAdjustInv] [uniqueidentifier] NULL,
  [Type] [int] NULL,
  CONSTRAINT [PK__SABill] PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[Package] (
  [Id] [uniqueidentifier] NOT NULL,
  [Code] [nvarchar](250) NULL,
  [Name] [nvarchar](250) NULL,
  [CreatedDate] [datetime] NULL,
  [CreatedBy] [nvarchar](50) NULL,
  [ModifiedBy] [nvarchar](50) NULL,
  [ModifiedDate] [datetime] NULL,
  CONSTRAINT [PK_Package] PRIMARY KEY CLUSTERED ([Id])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [VATDescription] [nvarchar](512) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAReturn]
  ADD [PaymentMethod] [nvarchar](25) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAReturn]
  ADD [BillRefID] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAReturn]
  ADD [StatusInvoice] [int] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAReturn]
  ADD [StatusSendMail] [bit] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAReturn]
  ADD [DateSendMail] [datetime] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAReturn]
  ADD [Email] [nvarchar](512) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAReturn]
  ADD [StatusConverted] [bit] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAReturn]
  ADD [IDReplaceInv] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAReturn]
  ADD [IDAdjustInv] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAReturn]
  ADD [IsBill] [bit] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAReturn]
  ADD [IsAttachListBill] [bit] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAQuoteDetail]
  ADD [VATDescription] [nvarchar](512) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAOrderDetail]
  ADD [VATDescription] [nvarchar](512) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAInvoice]
  ADD [StatusConverted] [bit] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAInvoice]
  ADD [DateSendMail] [datetime] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAInvoice]
  ADD [Email] [nvarchar](512) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAInvoice]
  ADD [IDAdjustInv] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAInvoice]
  ADD [IDReplaceInv] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAInvoice]
  ADD [IsBill] [bit] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAInvoice]
  ADD [IsAttachListBill] [bit] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAInvoice]
  ADD [PaymentMethod] [nvarchar](25) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAInvoice]
  ADD [BillRefID] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAInvoice]
  ADD [SABillID] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PPOrderDetail]
  ADD [VATDescription] [nvarchar](512) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PPInvoice]
  ADD [TotalFreightAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PPInvoice]
  ADD [TotalFreightAmountOriginal] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PPInvoice]
  ADD [TotalImportTaxExpenseAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PPInvoice]
  ADD [TotalImportTaxExpenseAmountOriginal] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PPDiscountReturn]
  ADD [StatusInvoice] [int] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PPDiscountReturn]
  ADD [StatusSendMail] [bit] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PPDiscountReturn]
  ADD [IDAdjustInv] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PPDiscountReturn]
  ADD [IDReplaceInv] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PPDiscountReturn]
  ADD [PaymentMethod] [nvarchar](512) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PPDiscountReturn]
  ADD [DateSendMail] [datetime] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PPDiscountReturn]
  ADD [Email] [nvarchar](512) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PPDiscountReturn]
  ADD [StatusConverted] [bit] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PPDiscountReturn]
  ADD [IsBill] [bit] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PPDiscountReturn]
  ADD [IsAttachListBill] [bit] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PPDiscountReturn]
  ADD [BillRefID] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MBCreditCardDetail]
  ADD [EmployeeID] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[FAIncrementDetail]
  ADD [VATDescription] [nvarchar](512) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
Create View [dbo].[ViewVoucherInvisible] AS
SELECT 
	dbo.MCPayment.ID, 
	dbo.MCPayment.TypeID, 
	dbo.Type.TypeName, 
	dbo.MCPayment.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.MCPayment.Date, 
	dbo.MCPayment.PostedDate, 
	dbo.MCPayment.CurrencyID, 
	dbo.MCPayment.Reason, 
	dbo.MCPayment.EmployeeID, 
	dbo.MCPayment.BranchID, 
	dbo.MCPayment.Recorded, 
	dbo.MCPayment.TotalAmount, 
	dbo.MCPayment.TotalAmountOriginal, 
	'MCPayment' AS RefTable
FROM dbo.MCPayment INNER JOIN
	dbo.Type ON MCPayment.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	dbo.MBDeposit.ID, 
	dbo.MBDeposit.TypeID, 
	dbo.Type.TypeName, 
	dbo.MBDeposit.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.MBDeposit.Date, 
	dbo.MBDeposit.PostedDate, 
	dbo.MBDeposit.CurrencyID, 
	dbo.MBDeposit.Reason, 
	dbo.MBDeposit.EmployeeID, 
	dbo.MBDeposit.BranchID, 
	dbo.MBDeposit.Recorded, 
	dbo.MBDeposit.TotalAmount, 
	dbo.MBDeposit.TotalAmountOriginal, 
	'MBDeposit' AS RefTable
FROM dbo.MBDeposit INNER JOIN
	dbo.Type ON MBDeposit.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	dbo.MBInternalTransfer.ID, 
	dbo.MBInternalTransfer.TypeID, 
	dbo.Type.TypeName, 
	dbo.MBInternalTransfer.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.MBInternalTransfer.Date, 
	dbo.MBInternalTransfer.PostedDate, 
	'' AS CurrencyID,
	dbo.MBInternalTransfer.Reason,
	NULL AS EmployeeID, 
	dbo.MBInternalTransfer.BranchID, 
	dbo.MBInternalTransfer.Recorded, 
	dbo.MBInternalTransfer.TotalAmountOriginal, 
	dbo.MBInternalTransfer.TotalAmount, 
	'MBInternalTransfer' AS RefTable
FROM dbo.MBInternalTransfer INNER JOIN
	dbo.Type ON dbo.MBInternalTransfer.TypeID = dbo.Type.ID
/* =======================================================================*/ 
UNION ALL
SELECT        
	dbo.MBTellerPaper.ID, 
	dbo.MBTellerPaper.TypeID, 
	dbo.Type.TypeName, 
	dbo.MBTellerPaper.No, 
	NULL AS InwardNo,
	NULL AS OutwardNo, 
	dbo.MBTellerPaper.Date, 
	dbo.MBTellerPaper.PostedDate, 
	dbo.MBTellerPaper.CurrencyID, 
	dbo.MBTellerPaper.Reason, 
	dbo.MBTellerPaper.EmployeeID, 
	dbo.MBTellerPaper.BranchID, 
	dbo.MBTellerPaper.Recorded, 
	dbo.MBTellerPaper.TotalAmountOriginal, 
	dbo.MBTellerPaper.TotalAmount, 
	'MBTellerPaper' AS RefTable
FROM dbo.MBTellerPaper INNER JOIN
	dbo.Type ON dbo.MBTellerPaper.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	dbo.MBCreditCard.ID, 
	dbo.MBCreditCard.TypeID, 
	dbo.Type.TypeName, 
	dbo.MBCreditCard.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.MBCreditCard.Date, 
	dbo.MBCreditCard.PostedDate, 
	dbo.MBCreditCard.CurrencyID, 
	dbo.MBCreditCard.Reason, 
	dbo.MBCreditCard.EmployeeID, 
	dbo.MBCreditCard.BranchID, 
	dbo.MBCreditCard.Recorded, 
	dbo.MBCreditCard.TotalAmountOriginal, 
	dbo.MBCreditCard.TotalAmount, 
	'MBCreditCard' AS RefTable
FROM dbo.MBCreditCard INNER JOIN
	dbo.Type ON dbo.MBCreditCard.TypeID = dbo.Type.ID
/* =======================================================================*/ 
UNION ALL
SELECT        
	dbo.MCReceipt.ID, 
	dbo.MCReceipt.TypeID, 
	dbo.Type.TypeName, 
	dbo.MCReceipt.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.MCReceipt.Date, 
	dbo.MCReceipt.PostedDate, 
	dbo.MCReceipt.CurrencyID, 
	dbo.MCReceipt.Reason, 
	dbo.MCReceipt.EmployeeID, 
	dbo.MCReceipt.BranchID, 
	dbo.MCReceipt.Recorded, 
	dbo.MCReceipt.TotalAmountOriginal, 
	dbo.MCReceipt.TotalAmount,
	'MCReceipt' AS RefTable
FROM dbo.MCReceipt INNER JOIN
	dbo.Type ON MCReceipt.TypeID = dbo.Type.ID
/* =======================================================================*/ 
UNION ALL
SELECT        
	dbo.FAAdjustment.ID,
	dbo.FAAdjustment.TypeID, 
	dbo.Type.TypeName, 
	dbo.FAAdjustment.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.FAAdjustment.Date, 
	dbo.FAAdjustment.PostedDate,
	NULL AS CurrencyID, 
	dbo.FAAdjustment.Reason, 
	NULL AS EmployeeID, 
	dbo.FAAdjustment.BranchID, 
	dbo.FAAdjustment.Recorded, 
	$ 0 AS TotalAmountOriginal, 
	dbo.FAAdjustment.TotalAmount, 
	'FAAdjustment' AS RefTable
FROM dbo.FAAdjustment INNER JOIN
	dbo.Type ON FAAdjustment.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	dbo.FADepreciation.ID, 
	dbo.FADepreciation.TypeID, 
	dbo.Type.TypeName, 
	dbo.FADepreciation.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.FADepreciation.Date, 
	dbo.FADepreciation.PostedDate, 
	NULL AS CurrencyID,
	dbo.FADepreciation.Reason,
	NULL AS EmployeeID, 
	dbo.FADepreciation.BranchID, 
	dbo.FADepreciation.Recorded, 
	dbo.FADepreciation.TotalAmountOriginal, 
	dbo.FADepreciation.TotalAmount,
	'FADepreciation' AS RefTable
FROM dbo.FADepreciation INNER JOIN
	dbo.Type ON FADepreciation.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	dbo.FAIncrement.ID, 
	dbo.FAIncrement.TypeID, 
	dbo.Type.TypeName, 
	dbo.FAIncrement.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.FAIncrement.Date, 
	dbo.FAIncrement.PostedDate, 
	FAIncrement.CurrencyID, 
	dbo.FAIncrement.Reason, 
	dbo.FAIncrement.EmployeeID, 
	dbo.FAIncrement.BranchID, 
	dbo.FAIncrement.Recorded, 
	dbo.FAIncrement.TotalAmountOriginal, 
	dbo.FAIncrement.TotalAmount, 
	'FAIncrement' AS RefTable
FROM dbo.FAIncrement INNER JOIN
	dbo.Type ON FAIncrement.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	dbo.FADecrement.ID, 
	dbo.FADecrement.TypeID, 
	dbo.Type.TypeName, 
	dbo.FADecrement.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.FADecrement.Date, 
	dbo.FADecrement.PostedDate, 
	FADecrement.CurrencyID, 
	dbo.FADecrement.Reason, 
	NULL AS EmployeeID, 
	dbo.FADecrement.BranchID, 
	dbo.FADecrement.Recorded, 
	dbo.FADecrement.TotalAmountOriginal, 
	dbo.FADecrement.TotalAmount, 
	'FADecrement' AS RefTable
FROM dbo.FADecrement INNER JOIN
	dbo.Type ON FADecrement.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT 
	dbo.GOtherVoucher.ID, 
	dbo.GOtherVoucher.TypeID, 
	dbo.Type.TypeName, 
	dbo.GOtherVoucher.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.GOtherVoucher.Date, 
	dbo.GOtherVoucher.PostedDate, 
	NULL AS CurrencyID, 
	dbo.GOtherVoucher.Reason, 
	NULL AS EmployeeID, 
	dbo.GOtherVoucher.BranchID, 
	dbo.GOtherVoucher.Recorded, 
	dbo.GOtherVoucher.TotalAmountOriginal, 
	dbo.GOtherVoucher.TotalAmount, 
	'GOtherVoucher' AS RefTable
FROM dbo.GOtherVoucher INNER JOIN
	dbo.Type ON GOtherVoucher.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	dbo.RSInwardOutward.ID, 
	dbo.RSInwardOutward.TypeID, 
	dbo.Type.TypeName, 
	dbo.RSInwardOutward.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.RSInwardOutward.Date, 
	dbo.RSInwardOutward.PostedDate, 
	RSInwardOutward.CurrencyID, 
	dbo.RSInwardOutward.Reason, 
	NULL AS EmployeeID, 
	dbo.RSInwardOutward.BranchID, 
	dbo.RSInwardOutward.Recorded, 
	dbo.RSInwardOutward.TotalAmountOriginal, 
	dbo.RSInwardOutward.TotalAmount, 
	'RSInwardOutward' AS RefTable
FROM dbo.RSInwardOutward INNER JOIN
	dbo.Type ON RSInwardOutward.TypeID = dbo.Type.ID
/* =======================================================================*/ 
UNION ALL
SELECT        
	dbo.RSTransfer.ID, 
	dbo.RSTransfer.TypeID, 
	dbo.Type.TypeName, 
	dbo.RSTransfer.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.RSTransfer.Date, 
	dbo.RSTransfer.PostedDate, 
	RSTransfer.CurrencyID, 
	dbo.RSTransfer.Reason, 
	NULL AS EmployeeID, 
	dbo.RSTransfer.BranchID,
	dbo.RSTransfer.Recorded, 
	dbo.RSTransfer.TotalAmountOriginal, 
	dbo.RSTransfer.TotalAmount, 
	'RSTransfer' AS RefTable
FROM dbo.RSTransfer INNER JOIN
	dbo.Type ON RSTransfer.TypeID = dbo.Type.ID
/* =======================================================================*/ 
UNION ALL
SELECT        
	dbo.PPInvoice.ID, 
	dbo.PPInvoice.TypeID, 
	dbo.Type.TypeName, 
	dbo.PPInvoice.No, 
	dbo.PPInvoice.InwardNo AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.PPInvoice.Date, 
	dbo.PPInvoice.PostedDate, 
	PPInvoice.CurrencyID,
	dbo.PPInvoice.Reason, 
	PPInvoice.EmployeeID, 
	dbo.PPInvoice.BranchID, 
	dbo.PPInvoice.Recorded, 
	dbo.PPInvoice.TotalAmountOriginal, 
	dbo.PPInvoice.TotalAmount,
	'PPInvoice' AS RefTable
FROM dbo.PPInvoice INNER JOIN
	dbo.Type ON PPInvoice.TypeID = dbo.Type.ID
/* =======================================================================*/ 
UNION ALL
SELECT        
	dbo.PPDiscountReturn.ID, 
	dbo.PPDiscountReturn.TypeID, 
	dbo.Type.TypeName, 
	dbo.PPDiscountReturn.No, 
	NULL AS InwardNo, 
	dbo.PPDiscountReturn.OutwardNo AS OutwardNo, 
	dbo.PPDiscountReturn.Date, 
	dbo.PPDiscountReturn.PostedDate, 
	PPDiscountReturn.CurrencyID, 
	dbo.PPDiscountReturn.Reason, 
	PPDiscountReturn.EmployeeID, 
	dbo.PPDiscountReturn.BrachID AS BranchID, 
	dbo.PPDiscountReturn.Recorded, 
	dbo.PPDiscountReturn.TotalAmountOriginal, 
	dbo.PPDiscountReturn.TotalAmount, 
	'PPDiscountReturn' AS RefTable
FROM dbo.PPDiscountReturn INNER JOIN
	dbo.Type ON PPDiscountReturn.TypeID = dbo.Type.ID
/* =======================================================================*/ 
UNION ALL
SELECT        
	dbo.PPService.ID, 
	dbo.PPService.TypeID, 
	dbo.Type.TypeName, 
	dbo.PPService.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.PPService.Date, 
	dbo.PPService.PostedDate, 
	PPService.CurrencyID, 
	dbo.PPService.Reason, 
	PPService.EmployeeID, 
	dbo.PPService.BranchID, 
	dbo.PPService.Recorded, 
	dbo.PPService.TotalAmountOriginal, 
	dbo.PPService.TotalAmount, 
	'PPService' AS RefTable
FROM dbo.PPService INNER JOIN
	dbo.Type ON PPService.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	dbo.PPOrder.ID, 
	dbo.PPOrder.TypeID, 
	dbo.Type.TypeName, 
	dbo.PPOrder.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.PPOrder.Date, 
	dbo.PPOrder.DeliverDate AS PostedDate, 
	dbo.PPOrder.CurrencyID, 
	dbo.PPOrder.Reason, 
	dbo.PPOrder.EmployeeID, 
	dbo.PPOrder.BranchID, 
	dbo.PPOrder.Exported AS Recorded, 
	dbo.PPOrder.TotalAmountOriginal, 
	dbo.PPOrder.TotalAmount, 
	'PPOrder' AS RefTable
FROM dbo.PPOrder INNER JOIN 
	dbo.Type ON PPOrder.TypeID = dbo.Type.ID
/* =======================================================================*/ 
UNION ALL
SELECT        
	dbo.SAInvoice.ID, 
	dbo.SAInvoice.TypeID, 
	dbo.Type.TypeName, 
	dbo.SAInvoice.No, 
	NULL AS InwardNo, 
	dbo.SAInvoice.OutwardNo AS OutwardNo, 
	dbo.SAInvoice.Date, 
	dbo.SAInvoice.PostedDate, 
	SAInvoice.CurrencyID,
	dbo.SAInvoice.Reason, 
	SAInvoice.EmployeeID, 
	dbo.SAInvoice.BranchID, 
	dbo.SAInvoice.Recorded, 
	dbo.SAInvoice.TotalAmountOriginal, 
	dbo.SAInvoice.TotalAmount, 
	'SAInvoice' AS RefTable
FROM dbo.SAInvoice INNER JOIN
	dbo.Type ON dbo.SAInvoice.TypeID = dbo.Type.ID
/* =======================================================================*/ 
UNION ALL
SELECT        
	dbo.SAReturn.ID, 
	dbo.SAReturn.TypeID, 
	dbo.Type.TypeName, 
	dbo.SAReturn.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.SAReturn.Date, 
	dbo.SAReturn.PostedDate, 
	SAReturn.CurrencyID, 
	dbo.SAReturn.Reason, 
	SAReturn.EmployeeID, 
	dbo.SAReturn.BranchID, 
	SAReturn.Recorded AS Posted, 
	dbo.SAReturn.TotalAmountOriginal, 
	dbo.SAReturn.TotalAmount, 
	'SAReturn' AS RefTable
FROM dbo.SAReturn INNER JOIN
	dbo.Type ON dbo.SAReturn.TypeID = dbo.Type.ID
/* =======================================================================*/ 
UNION ALL
SELECT        
	dbo.SAOrder.ID, 
	dbo.SAOrder.TypeID, 
	dbo.Type.TypeName, 
	dbo.SAOrder.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.SAOrder.Date, 
	dbo.SAOrder.DeliveDate AS PostedDate,
	SAOrder.CurrencyID, 
	dbo.SAOrder.Reason, 
	SAOrder.EmployeeID, 
	dbo.SAOrder.BranchID,
	SAOrder.Exported AS Recorded, 
	dbo.SAOrder.TotalAmountOriginal, 
	dbo.SAOrder.TotalAmount, 
	'SAOrder' AS RefTable
FROM dbo.SAOrder INNER JOIN
	dbo.Type ON dbo.SAOrder.TypeID = dbo.Type.ID
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*

*/
ALTER PROCEDURE [dbo].[Proc_SA_GetSalesDiaryBook]
    @FromDate DATETIME ,
    @ToDate DATETIME,
    @IsDisplayNotReceiptOnly BIT
AS 
    BEGIN 
		
        DECLARE @listRefType NVARCHAR(MAX)
        IF @IsDisplayNotReceiptOnly = 1 
            SET @listRefType = '3530,3532,3534,3536,'  
        ELSE 
            SET @listRefType = '3530,3531,3532,3534,3535,3536,3537,3538,3540,3541,3542,3543,3544,3545,3550,3551,3552,3553,3554,3555,'

              select ROW_NUMBER() OVER ( ORDER BY a.PostedDate, a.Date, a.No ) AS RowNum ,
			         a.Date as ngay_CTU, 
			         a.PostedDate as ngay_HT, 
					 a.No as So_CTU,
					 a.InvoiceDate as Ngay_HD, 
					 a.InvoiceNo as SO_HD, 
					 Sreason as Dien_giai,
					 SUM(CASE WHEN a.Account IN ( '5111' ) THEN a.CreditAmount
                         ELSE $0
                    END) AS TurnOverAmountInv ,
                    SUM(CASE WHEN a.Account IN ( '5112' ) THEN a.CreditAmount
                         ELSE $0
                    END) AS TurnOverAmountFinishedInv ,
                SUM(CASE WHEN a.Account IN ( '5113' ) THEN a.CreditAmount
                         ELSE $0
                    END) AS TurnOverAmountServiceInv ,
                SUM(CASE WHEN a.Account IN ( '5118' ) THEN a.CreditAmount
                         ELSE $0
                    END)AS TurnOverAmountOther,
                SUM(CASE WHEN a.Account IN ( '5111' ) THEN a.DebitAmount
                         ELSE $0
                    END) AS DiscountAmount ,
				SUM(CASE WHEN (a.typeID = '330' and a.Account IN ( '5111' )) THEN a.DebitAmount
                         ELSE $0
                    END) AS ReturnAmount,
                SUM(CASE WHEN (a.typeID = '340' and a.Account IN ( '5111' )) THEN a.DebitAmount
                         ELSE $0
                    END) AS ReduceAmount,
			    SUM(b.VATAmount) AS VATAmount,
				ao.AccountingObjectCode as CustomerCode, 
				ao.AccountingObjectName as CustomerName 
				INTO    #Result
				from GeneralLedger a 
				join SAInvoiceDetail b on a.DetailID = b.ID 
				join MaterialGoods c on b.MaterialGoodsID = c.ID
				join SAInvoice d on a.ReferenceID = d.ID 
				join Type e on a.TypeID = e.ID
				join AccountingObject ao on a.AccountingObjectID = ao.ID 
				where a.PostedDate between  @FromDate and @ToDate
				group by a.Date,  a.PostedDate, a.No ,a.InvoiceDate , a.InvoiceNo , Sreason,
				ao.AccountingObjectCode,ao.AccountingObjectName;

       select	RowNum ,
				ngay_CTU, 
				ngay_HT, 
				So_CTU,
				Ngay_HD, 
				SO_HD, 
				Dien_giai,
				TurnOverAmountInv + TurnOverAmountFinishedInv + TurnOverAmountServiceInv +
				TurnOverAmountOther as SumTurnOver,
				TurnOverAmountInv ,
				TurnOverAmountFinishedInv ,
                TurnOverAmountServiceInv ,
                TurnOverAmountOther,
                DiscountAmount ,
				ReturnAmount,
                ReduceAmount,
				(TurnOverAmountInv + TurnOverAmountFinishedInv + TurnOverAmountServiceInv +
				TurnOverAmountOther) - (DiscountAmount + ReturnAmount + ReduceAmount) as TurnOverPure,
				CustomerCode, 
				CustomerName 
   from #Result 
   where TurnOverAmountInv + TurnOverAmountFinishedInv + TurnOverAmountServiceInv +
				TurnOverAmountOther <> 0
   ORDER BY RowNum   
    
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

/*=================================================================================================
 * Created By:		thaivh	
 * Created Date:	11/7/2018
 * Description:		Lay du lieu cho uy nhiem chi
===================================================================================================== */
ALTER PROCEDURE [dbo].[Proc_Accreditative]
    @BankCode nvarchar(25)
AS

    BEGIN
        SET NOCOUNT ON;
   		                         
		select distinct a.ID, a.No, a.Date, a.BankName, a.AccountingObjectAddress, h.BankAccount as AccountingObjectBankAccount, a.AccountingObjectBankName,
		a.AccountingObjectName, c.BankAccount, c.BankBranchName, d.Tel, e.ID as CurrencyName, Sum(b.AmountOriginal) as TotalAmount, a.Reason, h.BankBranchName as Branch, f.BankCode
		from MBTellerPaper a inner join MBTellerPaperDetail b on a.ID = b.MBTellerPaperID
		left join BankAccountDetail c on a.BankAccountDetailID = c.ID 
		left join AccountingObject d on a.AccountingObjectID = d.ID
		left join Currency e on a.CurrencyID = e.ID 		
		left join Bank f on c.BankID = f.ID
		left join AccountingObjectBankAccount h on a.AccountingObjectBankAccount = h.ID
		where f.BankCode = @BankCode and (b.CreditAccount = '1121' or b.CreditAccount = '1122')
		group by a.ID, a.No, a.Date, a.BankName, a.AccountingObjectAddress, h.BankAccount, h.BankBranchName, a.AccountingObjectBankName,
		a.AccountingObjectName, c.BankAccount, c.BankBranchName, d.Tel, e.ID, a.Reason, f.BankCode
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER VIEW [dbo].[ViewVouchersCloseBook]
AS
/*Create by tnson 24/08/2011 - View này chỉ giành riêng cho việc kiểm tra các chứng từ chưa ghi sổ trước khi khóa sổ không được dùng cho việc khác */ SELECT dbo.MCPayment.ID, dbo.MCPaymentDetail.ID AS DetailID, 
                         dbo.MCPayment.TypeID, dbo.Type.TypeName, dbo.MCPayment.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MCPayment.Date, dbo.MCPayment.PostedDate, dbo.MCPaymentDetail.DebitAccount, 
                         dbo.MCPaymentDetail.CreditAccount, dbo.MCPayment.CurrencyID, dbo.MCPaymentDetail.AmountOriginal, dbo.MCPaymentDetail.Amount, NULL AS OrgPrice, dbo.MCPayment.Reason, 
                         dbo.MCPaymentDetail.Description, dbo.MCPaymentDetail.CostSetID, CostSet.CostSetCode, dbo.MCPaymentDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, dbo.MCPayment.EmployeeID, dbo.MCPayment.BranchID, NULL 
                         AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL 
                         AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.MCPayment.Recorded, dbo.MCPayment.TotalAmount, 
                         dbo.MCPayment.TotalAmountOriginal, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL 
                         AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 
                         'MCPayment' AS RefTable
/*case AccountingObject.IsEmployee when 1 then  AccountingObject.AccountingObjectName end AS EmployeeName*/ FROM dbo.MCPayment INNER JOIN
                         dbo.MCPaymentDetail ON dbo.MCPayment.ID = dbo.MCPaymentDetail.MCPaymentID INNER JOIN
                         dbo.Type ON MCPayment.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON MCPaymentDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MCPaymentDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MCPaymentDetail.ContractID
/**/
UNION ALL
SELECT        dbo.MCPayment.ID, dbo.MCPaymentDetailTax.ID AS DetailID, dbo.MCPayment.TypeID, dbo.Type.TypeName, dbo.MCPayment.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MCPayment.Date, 
                         dbo.MCPayment.PostedDate, NULL AS DebitAccount, NULL AS CreditAccount, dbo.MCPayment.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.MCPayment.Reason, 
                         dbo.MCPaymentDetailTax.Description, NULL AS CostSetID, NULL AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, NULL AS AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL 
                         AS AccountingObjectCode, NULL AS EmployeeID, dbo.MCPayment.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL 
                         AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, dbo.MCPaymentDetailTax.VATAmountOriginal, dbo.MCPaymentDetailTax.VATAmount, NULL 
                         AS Quantity, NULL AS UnitPrice, dbo.MCPayment.Recorded, dbo.MCPayment.TotalAmount, dbo.MCPayment.TotalAmountOriginal, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL 
                         AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 1 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MCPayment' AS RefTable
FROM            dbo.MCPayment INNER JOIN
                         dbo.MCPaymentDetailTax ON dbo.MCPayment.ID = dbo.MCPaymentDetailTax.MCPaymentID INNER JOIN
                         dbo.Type ON MCPayment.TypeID = dbo.Type.ID
/**/
UNION ALL
SELECT        dbo.MBDeposit.ID, dbo.MBDepositDetail.ID AS DetailID, dbo.MBDeposit.TypeID, dbo.Type.TypeName, dbo.MBDeposit.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBDeposit.Date, 
                         dbo.MBDeposit.PostedDate, dbo.MBDepositDetail.DebitAccount, dbo.MBDepositDetail.CreditAccount, dbo.MBDeposit.CurrencyID, dbo.MBDepositDetail.AmountOriginal, dbo.MBDepositDetail.Amount, NULL 
                         AS OrgPrice, dbo.MBDeposit.Reason, dbo.MBDepositDetail.Description, dbo.MBDepositDetail.CostSetID, CostSet.CostSetCode, dbo.MBDepositDetail.ContractID, EMContract.Code AS ContractCode, 
                         dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, dbo.MBDeposit.EmployeeID, 
                         dbo.MBDeposit.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL 
                         AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.MBDeposit.Recorded, 
                         dbo.MBDeposit.TotalAmountOriginal, dbo.MBDeposit.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL 
                         AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 
                         'MBDeposit' AS RefTable
FROM            dbo.MBDeposit INNER JOIN
                         dbo.MBDepositDetail ON dbo.MBDeposit.ID = dbo.MBDepositDetail.MBDepositID INNER JOIN
                         dbo.Type ON MBDeposit.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON MBDepositDetail.AccountingObjectID = dbo.AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MBDepositDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MBDepositDetail.ContractID
/**/
UNION ALL
SELECT        dbo.MBDeposit.ID, dbo.MBDepositDetailTax.ID AS DetailID, dbo.MBDeposit.TypeID, dbo.Type.TypeName, dbo.MBDeposit.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBDeposit.Date, 
                         dbo.MBDeposit.PostedDate, NULL AS DebitAccount, NULL AS CreditAccount, dbo.MBDeposit.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.MBDeposit.Reason, 
                         dbo.MBDepositDetailTax.Description, NULL AS CostSetID, NULL AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, NULL AS AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL 
                         AS AccountingObjectCode, NULL AS EmployeeID, dbo.MBDeposit.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL 
                         AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, dbo.MBDepositDetailTax.VATAmountOriginal, dbo.MBDepositDetailTax.VATAmount, NULL 
                         AS Quantity, NULL AS UnitPrice, dbo.MBDeposit.Recorded, dbo.MBDeposit.TotalAmountOriginal, dbo.MBDeposit.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL 
                         AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 1 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MBDeposit' AS RefTable
FROM            dbo.MBDeposit INNER JOIN
                         dbo.MBDepositDetailTax ON dbo.MBDeposit.ID = dbo.MBDepositDetailTax.MBDepositID INNER JOIN
                         dbo.Type ON MBDeposit.TypeID = dbo.Type.ID
/**/
UNION ALL
SELECT        dbo.MBInternalTransfer.ID, dbo.MBInternalTransferDetail.ID AS DetailID, dbo.MBInternalTransfer.TypeID, dbo.Type.TypeName, dbo.MBInternalTransfer.No, NULL AS InwardNo, NULL AS OutwardNo, 
                         dbo.MBInternalTransfer.Date, dbo.MBInternalTransfer.PostedDate, dbo.MBInternalTransferDetail.DebitAccount, dbo.MBInternalTransferDetail.CreditAccount, dbo.MBInternalTransferDetail.CurrencyID, 
                         dbo.MBInternalTransferDetail.AmountOriginal, dbo.MBInternalTransferDetail.Amount, NULL AS OrgPrice, dbo.MBInternalTransfer.Reason, dbo.MBInternalTransfer.Reason AS Description, 
                         dbo.MBInternalTransferDetail.CostSetID, CostSet.CostSetCode, dbo.MBInternalTransferDetail.ContractID, EMContract.Code AS ContractCode, NULL AS AccountingObjectID, '' AS AccountingObjectCategoryName, 
                         '' AS AccountingObjectCode, NULL AS EmployeeID, dbo.MBInternalTransfer.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL 
                         AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, 
                         dbo.MBInternalTransfer.Recorded, dbo.MBInternalTransfer.TotalAmountOriginal, dbo.MBInternalTransfer.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL 
                         AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MBInternalTransfer' AS RefTable
FROM            dbo.MBInternalTransfer INNER JOIN
                         dbo.MBInternalTransferDetail ON dbo.MBInternalTransfer.ID = dbo.MBInternalTransferDetail.MBInternalTransferID INNER JOIN
                         dbo.Type ON dbo.MBInternalTransfer.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MBInternalTransferDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MBInternalTransferDetail.ContractID
/**/
UNION ALL
SELECT        dbo.MBTellerPaper.ID, dbo.MBTellerPaperDetail.ID AS DetailID, dbo.MBTellerPaper.TypeID, dbo.Type.TypeName, dbo.MBTellerPaper.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBTellerPaper.Date, 
                         dbo.MBTellerPaper.PostedDate, dbo.MBTellerPaperDetail.DebitAccount, dbo.MBTellerPaperDetail.CreditAccount, dbo.MBTellerPaper.CurrencyID, dbo.MBTellerPaperDetail.AmountOriginal, 
                         dbo.MBTellerPaperDetail.Amount, NULL AS OrgPrice, dbo.MBTellerPaper.Reason, dbo.MBTellerPaperDetail.Description, dbo.MBTellerPaperDetail.CostSetID, CostSet.CostSetCode, 
                         dbo.MBTellerPaperDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, 
                         dbo.AccountingObject.AccountingObjectCode, dbo.MBTellerPaper.EmployeeID, dbo.MBTellerPaper.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL 
                         AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL 
                         AS Quantity, NULL AS UnitPrice, dbo.MBTellerPaper.Recorded, dbo.MBTellerPaper.TotalAmountOriginal, dbo.MBTellerPaper.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL 
                         AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MBTellerPaper' AS RefTable
FROM            dbo.MBTellerPaper INNER JOIN
                         dbo.MBTellerPaperDetail ON dbo.MBTellerPaper.ID = dbo.MBTellerPaperDetail.MBTellerPaperID INNER JOIN
                         dbo.Type ON dbo.MBTellerPaper.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON MBTellerPaperDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MBTellerPaperDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MBTellerPaperDetail.ContractID
/**/
UNION ALL
SELECT        dbo.MBTellerPaper.ID, dbo.MBTellerPaperDetailTax.ID AS DetailID, dbo.MBTellerPaper.TypeID, dbo.Type.TypeName, dbo.MBTellerPaper.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBTellerPaper.Date, 
                         dbo.MBTellerPaper.PostedDate, NULL AS DebitAccount, NULL AS CreditAccount, dbo.MBTellerPaper.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.MBTellerPaper.Reason, 
                         dbo.MBTellerPaperDetailTax.Description, NULL AS CostSetID, NULL AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, NULL AS AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL 
                         AS AccountingObjectCode, NULL AS EmployeeID, dbo.MBTellerPaper.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL 
                         AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, dbo.MBTellerPaperDetailTax.VATAmountOriginal, 
                         dbo.MBTellerPaperDetailTax.VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.MBTellerPaper.Recorded, dbo.MBTellerPaper.TotalAmountOriginal, dbo.MBTellerPaper.TotalAmount, NULL 
                         AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 1 AS DetailTax, NULL AS InwardAmountOriginal, NULL 
                         AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MBTellerPaper' AS RefTable
FROM            dbo.MBTellerPaper INNER JOIN
                         dbo.MBTellerPaperDetailTax ON dbo.MBTellerPaper.ID = dbo.MBTellerPaperDetailTax.MBTellerPaperID INNER JOIN
                         dbo.Type ON dbo.MBTellerPaper.TypeID = dbo.Type.ID
/**/
UNION ALL
SELECT        dbo.MBCreditCard.ID, dbo.MBCreditCardDetail.ID AS DetailID, dbo.MBCreditCard.TypeID, dbo.Type.TypeName, dbo.MBCreditCard.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBCreditCard.Date, 
                         dbo.MBCreditCard.PostedDate, dbo.MBCreditCardDetail.DebitAccount, dbo.MBCreditCardDetail.CreditAccount, dbo.MBCreditCard.CurrencyID, dbo.MBCreditCardDetail.AmountOriginal, 
                         dbo.MBCreditCardDetail.Amount, NULL AS OrgPrice, dbo.MBCreditCard.Reason, dbo.MBCreditCardDetail.Description, dbo.MBCreditCardDetail.CostSetID, CostSet.CostSetCode, dbo.MBCreditCardDetail.ContractID, 
                         EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, 
                         dbo.AccountingObject.AccountingObjectCode, dbo.MBCreditCard.EmployeeID, dbo.MBCreditCard.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL 
                         AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL 
                         AS Quantity, NULL AS UnitPrice, dbo.MBCreditCard.Recorded, dbo.MBCreditCard.TotalAmountOriginal, dbo.MBCreditCard.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL 
                         AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MBCreditCard' AS RefTable
FROM            dbo.MBCreditCard INNER JOIN
                         dbo.MBCreditCardDetail ON dbo.MBCreditCard.ID = dbo.MBCreditCardDetail.MBCreditCardID INNER JOIN
                         dbo.Type ON dbo.MBCreditCard.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON MBCreditCardDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MBCreditCardDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MBCreditCardDetail.ContractID
/**/
UNION ALL
SELECT        dbo.MBCreditCard.ID, dbo.MBCreditCardDetailTax.ID AS DetailID, dbo.MBCreditCard.TypeID, dbo.Type.TypeName, dbo.MBCreditCard.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBCreditCard.Date, 
                         dbo.MBCreditCard.PostedDate, NULL AS DebitAccount, NULL AS CreditAccount, dbo.MBCreditCard.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.MBCreditCard.Reason, 
                         dbo.MBCreditCardDetailTax.Description, NULL AS CostSetID, NULL AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, NULL AS AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL 
                         AS AccountingObjectCode, NULL AS EmployeeID, dbo.MBCreditCard.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL 
                         AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, dbo.MBCreditCardDetailTax.VATAmountOriginal, 
                         dbo.MBCreditCardDetailTax.VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.MBCreditCard.Recorded, dbo.MBCreditCard.TotalAmountOriginal, dbo.MBCreditCard.TotalAmount, NULL 
                         AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 1 AS DetailTax, NULL AS InwardAmountOriginal, NULL 
                         AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MBCreditCard' AS RefTable
FROM            dbo.MBCreditCard INNER JOIN
                         dbo.MBCreditCardDetailTax ON dbo.MBCreditCard.ID = dbo.MBCreditCardDetailTax.MBCreditCardID INNER JOIN
                         dbo.Type ON dbo.MBCreditCard.TypeID = dbo.Type.ID
/* =======================================================================*/ UNION ALL
SELECT        dbo.MCReceipt.ID, dbo.MCReceiptDetail.ID AS DetailID, dbo.MCReceipt.TypeID, dbo.Type.TypeName, dbo.MCReceipt.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MCReceipt.Date, 
                         dbo.MCReceipt.PostedDate, dbo.MCReceiptDetail.DebitAccount, dbo.MCReceiptDetail.CreditAccount, dbo.MCReceipt.CurrencyID, dbo.MCReceiptDetail.AmountOriginal, dbo.MCReceiptDetail.Amount, NULL 
                         AS OrgPrice, dbo.MCReceipt.Reason, dbo.MCReceiptDetail.Description, dbo.MCReceiptDetail.CostSetID, CostSet.CostSetCode, dbo.MCReceiptDetail.ContractID, EMContract.Code AS ContractCode, 
                         dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, dbo.MCReceipt.EmployeeID, 
                         dbo.MCReceipt.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL 
                         AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.MCReceipt.Recorded, 
                         dbo.MCReceipt.TotalAmountOriginal, dbo.MCReceipt.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL 
                         AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 
                         'MCReceipt' AS RefTable
FROM            dbo.MCReceipt INNER JOIN
                         dbo.MCReceiptDetail ON dbo.MCReceipt.ID = dbo.MCReceiptDetail.MCReceiptID INNER JOIN
                         dbo.Type ON MCReceipt.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON MCReceiptDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MCReceiptDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MCReceiptDetail.ContractID
/**/
UNION ALL
SELECT        dbo.MCReceipt.ID, dbo.MCReceiptDetailTax.ID AS DetailID, dbo.MCReceipt.TypeID, dbo.Type.TypeName, dbo.MCReceipt.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MCReceipt.Date, 
                         dbo.MCReceipt.PostedDate, NULL AS DebitAccount, NULL AS CreditAccount, dbo.MCReceipt.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.MCReceipt.Reason, 
                         dbo.MCReceiptDetailTax.Description, NULL AS CostSetID, NULL AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, NULL AS AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL 
                         AS AccountingObjectCode, NULL AS EmployeeID, dbo.MCReceipt.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL 
                         AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, dbo.MCReceiptDetailTax.VATAmountOriginal, dbo.MCReceiptDetailTax.VATAmount, NULL 
                         AS Quantity, NULL AS UnitPrice, dbo.MCReceipt.Recorded, dbo.MCReceipt.TotalAmountOriginal, dbo.MCReceipt.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL 
                         AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 1 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MCReceipt' AS RefTable
FROM            dbo.MCReceipt INNER JOIN
                         dbo.MCReceiptDetailTax ON dbo.MCReceipt.ID = dbo.MCReceiptDetailTax.MCReceiptID INNER JOIN
                         dbo.Type ON MCReceipt.TypeID = dbo.Type.ID
/**/
UNION ALL
SELECT        dbo.FAAdjustment.ID, dbo.FAAdjustmentDetail.ID AS DetailID, dbo.FAAdjustment.TypeID, dbo.Type.TypeName, dbo.FAAdjustment.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.FAAdjustment.Date, 
                         dbo.FAAdjustment.PostedDate, dbo.FAAdjustmentDetail.DebitAccount, dbo.FAAdjustmentDetail.CreditAccount, NULL AS CurrencyID, $ 0 AS AmountOriginal, dbo.FAAdjustmentDetail.Amount, $ 0 AS OrgPrice, 
                         dbo.FAAdjustment.Reason, dbo.FAAdjustmentDetail.Description, dbo.FAAdjustmentDetail.CostSetID, CostSet.CostSetCode, dbo.FAAdjustmentDetail.ContractID, EMContract.Code AS ContractCode, 
                         dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, NULL AS EmployeeID, 
                         dbo.FAAdjustment.BranchID, dbo.FixedAsset.ID AS FixedAssetID, dbo.FixedAsset.FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL 
                         AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, $ 0 AS VATAmountOriginal, $ 0 AS VATAmount, $ 0 AS Quantity, $ 0 AS UnitPrice, dbo.FAAdjustment.Recorded, 
                         $ 0 AS TotalAmountOriginal, dbo.FAAdjustment.TotalAmount, $ 0 AS DiscountAmountOriginal, $ 0 AS DiscountAmount, NULL AS DiscountAccount, $ 0 AS FreightAmountOriginal, NULL AS FreightAmount, 
                         0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'FAAdjustment' AS RefTable
FROM            dbo.FAAdjustment INNER JOIN
                         dbo.FAAdjustmentDetail ON dbo.FAAdjustment.ID = dbo.FAAdjustmentDetail.FAAdjustmentID INNER JOIN
                         dbo.Type ON FAAdjustment.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON FAAdjustmentDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.FixedAsset ON FAAdjustment.FixedAssetID = dbo.FixedAsset.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = FAAdjustmentDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = FAAdjustmentDetail.ContractID
/**/
UNION ALL
SELECT        dbo.FADepreciation.ID, dbo.FADepreciationDetail.ID AS DetailID, dbo.FADepreciation.TypeID, dbo.Type.TypeName, dbo.FADepreciation.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.FADepreciation.Date, 
                         dbo.FADepreciation.PostedDate, dbo.FADepreciationDetail.DebitAccount, dbo.FADepreciationDetail.CreditAccount, FADepreciationDetail.CurrencyID, dbo.FADepreciationDetail.AmountOriginal, 
                         dbo.FADepreciationDetail.Amount, dbo.FADepreciationDetail.OrgPrice, dbo.FADepreciation.Reason, dbo.FADepreciationDetail.Description, dbo.FADepreciationDetail.CostSetID, CostSet.CostSetCode, 
                         dbo.FADepreciationDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, 
                         dbo.AccountingObject.AccountingObjectCode, dbo.FADepreciationDetail.EmployeeID, dbo.FADepreciation.BranchID, dbo.FixedAsset.ID AS FixedAssetID, dbo.FixedAsset.FixedAssetName, NULL 
                         AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, FADepreciationDetail.DepartmentID, Department.DepartmentName, NULL AS VATAccount, NULL 
                         AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.FADepreciation.Recorded, dbo.FADepreciation.TotalAmountOriginal, dbo.FADepreciation.TotalAmount, NULL 
                         AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL 
                         AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'FADepreciation' AS RefTable
FROM            dbo.FADepreciation INNER JOIN
                         dbo.FADepreciationDetail ON dbo.FADepreciation.ID = dbo.FADepreciationDetail.FADepreciationID INNER JOIN
                         dbo.Type ON FADepreciation.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON FADepreciationDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.FixedAsset ON FADepreciationDetail.FixedAssetID = dbo.FixedAsset.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = FADepreciationDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = FADepreciationDetail.ContractID LEFT JOIN
                         dbo.Department ON Department.ID = FADepreciationDetail.DepartmentID
/**/
UNION ALL
SELECT        dbo.FAIncrement.ID, dbo.FAIncrementDetail.ID AS DetailID, dbo.FAIncrement.TypeID, dbo.Type.TypeName, dbo.FAIncrement.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.FAIncrement.Date, 
                         dbo.FAIncrement.PostedDate, dbo.FAIncrementDetail.DebitAccount, dbo.FAIncrementDetail.CreditAccount, FAIncrement.CurrencyID, dbo.FAIncrementDetail.AmountOriginal, dbo.FAIncrementDetail.Amount, 
                         dbo.FAIncrementDetail.OrgPriceOriginal AS OrgPrice, dbo.FAIncrement.Reason, dbo.FAIncrementDetail.Description, dbo.FAIncrementDetail.CostSetID, CostSet.CostSetCode, dbo.FAIncrementDetail.ContractID, 
                         EMContract.Code AS ContractCode, dbo.AccountingObject.ID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, 
                         dbo.FAIncrement.EmployeeID, dbo.FAIncrement.BranchID, dbo.FixedAsset.ID AS FixedAssetID, dbo.FixedAsset.FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL 
                         AS RepositoryID, NULL AS RepositoryName, FAIncrementDetail.DepartmentID, dbo.Department.DepartmentName, dbo.FAIncrementDetail.VATAccount AS VATAccount, 
                         dbo.FAIncrementDetail.VATAmountOriginal AS VATAmountOriginal, dbo.FAIncrementDetail.VATAmount AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.FAIncrement.Recorded, 
                         dbo.FAIncrement.TotalAmountOriginal, dbo.FAIncrement.TotalAmount, dbo.FAIncrementDetail.DiscountAmountOriginal, dbo.FAIncrementDetail.DiscountAmount, NULL AS DiscountAccount, 
                         dbo.FAIncrementDetail.FreightAmountOriginal, dbo.FAIncrementDetail.FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'FAIncrement' AS RefTable
FROM            dbo.FAIncrement INNER JOIN
                         dbo.FAIncrementDetail ON dbo.FAIncrement.ID = dbo.FAIncrementDetail.ID INNER JOIN
                         dbo.Type ON FAIncrement.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON FAIncrementDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.FixedAsset ON FAIncrementDetail.FixedAssetID = dbo.FixedAsset.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = FAIncrementDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = FAIncrementDetail.ContractID LEFT JOIN
                         dbo.Department ON Department.ID = FAIncrementDetail.DepartmentID
/**/
UNION ALL
SELECT        dbo.FADecrement.ID, dbo.FADecrementDetail.ID AS DetailID, dbo.FADecrement.TypeID, dbo.Type.TypeName, dbo.FADecrement.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.FADecrement.Date, 
                         dbo.FADecrement.PostedDate, dbo.FADecrementDetail.DebitAccount, dbo.FADecrementDetail.CreditAccount, FADecrement.CurrencyID, dbo.FADecrementDetail.AmountOriginal, 
                         dbo.FADecrementDetail.Amount, NULL AS OrgPrice, dbo.FADecrement.Reason, dbo.FADecrementDetail.Description, dbo.FADecrementDetail.CostSetID, CostSet.CostSetCode, dbo.FADecrementDetail.ContractID, 
                         EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, 
                         dbo.AccountingObject.AccountingObjectCode, dbo.FADecrementDetail.EmployeeID, dbo.FADecrement.BranchID, dbo.FixedAsset.ID AS FixedAssetID, dbo.FixedAsset.FixedAssetName, NULL 
                         AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, FADecrementDetail.DepartmentID, Department.DepartmentName, NULL AS VATAccount, NULL 
                         AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.FADecrement.Recorded, dbo.FADecrement.TotalAmountOriginal, dbo.FADecrement.TotalAmount, NULL 
                         AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL 
                         AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'FADecrement' AS RefTable
FROM            dbo.FADecrement INNER JOIN
                         dbo.FADecrementDetail ON dbo.FADecrement.ID = dbo.FADecrementDetail.FADecrementID INNER JOIN
                         dbo.Type ON FADecrement.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON FADecrementDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.FixedAsset ON FADecrementDetail.FixedAssetID = dbo.FixedAsset.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = FADecrementDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = FADecrementDetail.ContractID LEFT JOIN
                         dbo.Department ON Department.ID = FADecrementDetail.DepartmentID
/**/
UNION ALL
/*Chứng từ nghiệp vụ khác*/ SELECT dbo.GOtherVoucher.ID, dbo.GOtherVoucherDetail.ID AS DetailID, dbo.GOtherVoucher.TypeID, dbo.Type.TypeName, dbo.GOtherVoucher.No, NULL AS InwardNo, NULL AS OutwardNo, 
                         dbo.GOtherVoucher.Date, dbo.GOtherVoucher.PostedDate, dbo.GOtherVoucherDetail.DebitAccount, dbo.GOtherVoucherDetail.CreditAccount, GOtherVoucherDetail.CurrencyID, 
                         dbo.GOtherVoucherDetail.AmountOriginal, dbo.GOtherVoucherDetail.Amount, NULL AS OrgPrice, dbo.GOtherVoucher.Reason, dbo.GOtherVoucherDetail.Description, dbo.GOtherVoucherDetail.CostSetID, 
                         CostSet.CostSetCode, dbo.GOtherVoucherDetail.ContractID, EMContract.Code AS ContractCode, dbo.GOtherVoucherDetail.DebitAccountingObjectID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode AS AccountingObjectCode, dbo.GOtherVoucherDetail.EmployeeID, 
                         dbo.GOtherVoucher.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL 
                         AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.GOtherVoucher.Recorded, 
                         dbo.GOtherVoucher.TotalAmountOriginal, dbo.GOtherVoucher.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL 
                         AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 
                         'GOtherVoucher' AS RefTable
FROM            dbo.GOtherVoucher INNER JOIN
                         dbo.GOtherVoucherDetail ON dbo.GOtherVoucher.ID = dbo.GOtherVoucherDetail.GOtherVoucherID INNER JOIN
                         dbo.Type ON GOtherVoucher.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON GOtherVoucherDetail.DebitAccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = GOtherVoucherDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = GOtherVoucherDetail.ContractID
/**/
UNION ALL
SELECT        dbo.GOtherVoucher.ID, dbo.GOtherVoucherDetailTax.ID AS DetailID, dbo.GOtherVoucher.TypeID, dbo.Type.TypeName, dbo.GOtherVoucher.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.GOtherVoucher.Date, 
                         dbo.GOtherVoucher.PostedDate, NULL AS DebitAccount, NULL AS CreditAccount, GOtherVoucherDetailTax.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.GOtherVoucher.Reason, 
                         dbo.GOtherVoucherDetailTax.Description, NULL AS CostSetID, NULL AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, NULL AS AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL 
                         AccountingObjectCode, NULL AS EmployeeID, dbo.GOtherVoucher.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL 
                         AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, dbo.GOtherVoucherDetailTax.VATAmountOriginal, 
                         dbo.GOtherVoucherDetailTax.VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.GOtherVoucher.Recorded, dbo.GOtherVoucher.TotalAmountOriginal, dbo.GOtherVoucher.TotalAmount, NULL 
                         AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 1 AS DetailTax, NULL AS InwardAmountOriginal, NULL 
                         AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'GOtherVoucher' AS RefTable
FROM            dbo.GOtherVoucher INNER JOIN
                         dbo.GOtherVoucherDetailTax ON dbo.GOtherVoucher.ID = dbo.GOtherVoucherDetailTax.GOtherVoucherID INNER JOIN
                         dbo.Type ON GOtherVoucher.TypeID = dbo.Type.ID
/**/
UNION ALL
SELECT        dbo.RSInwardOutward.ID, dbo.RSInwardOutwardDetail.ID AS DetailID, dbo.RSInwardOutward.TypeID, dbo.Type.TypeName, dbo.RSInwardOutward.No, NULL AS InwardNo, NULL AS OutwardNo, 
                         dbo.RSInwardOutward.Date, dbo.RSInwardOutward.PostedDate, dbo.RSInwardOutwardDetail.DebitAccount, dbo.RSInwardOutwardDetail.CreditAccount, RSInwardOutward.CurrencyID, 
                         dbo.RSInwardOutwardDetail.AmountOriginal, dbo.RSInwardOutwardDetail.Amount, NULL AS OrgPrice, dbo.RSInwardOutward.Reason, dbo.RSInwardOutwardDetail.Description, 
                         dbo.RSInwardOutwardDetail.CostSetID, CostSet.CostSetCode, dbo.RSInwardOutwardDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, dbo.RSInwardOutwardDetail.EmployeeID, dbo.RSInwardOutward.BranchID, NULL 
                         AS FixedAssetID, NULL AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID, dbo.Repository.RepositoryName, NULL 
                         AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, RSInwardOutwardDetail.Quantity, RSInwardOutwardDetail.UnitPrice, 
                         dbo.RSInwardOutward.Recorded, dbo.RSInwardOutward.TotalAmountOriginal, dbo.RSInwardOutward.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL 
                         AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL 
                         AS OutwardAmount, 'RSInwardOutward' AS RefTable
FROM            dbo.RSInwardOutward INNER JOIN
                         dbo.RSInwardOutwardDetail ON dbo.RSInwardOutward.ID = dbo.RSInwardOutwardDetail.RSInwardOutwardID INNER JOIN
                         dbo.Type ON RSInwardOutward.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON RSInwardOutwardDetail.EmployeeID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON RSInwardOutwardDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON RSInwardOutwardDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = RSInwardOutwardDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = RSInwardOutwardDetail.ContractID
/**/
UNION ALL
SELECT        dbo.RSTransfer.ID, dbo.RSTransferDetail.ID AS DetailID, dbo.RSTransfer.TypeID, dbo.Type.TypeName, dbo.RSTransfer.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.RSTransfer.Date, 
                         dbo.RSTransfer.PostedDate, dbo.RSTransferDetail.DebitAccount, dbo.RSTransferDetail.CreditAccount, RSTransfer.CurrencyID, dbo.RSTransferDetail.AmountOriginal, dbo.RSTransferDetail.Amount, NULL 
                         AS OrgPrice, dbo.RSTransfer.Reason, dbo.RSTransferDetail.Description, dbo.RSTransferDetail.CostSetID, CostSet.CostSetCode, dbo.RSTransferDetail.ContractID, EMContract.Code AS ContractCode, NULL 
                         AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL AS AccountingObjectCode, dbo.RSTransferDetail.EmployeeID, dbo.RSTransfer.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, 
                         dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID AS RepositoryID, dbo.Repository.RepositoryName, NULL AS DepartmentID, NULL 
                         AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, RSTransferDetail.Quantity, RSTransferDetail.UnitPrice, dbo.RSTransfer.Recorded, 
                         dbo.RSTransfer.TotalAmountOriginal, dbo.RSTransfer.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL 
                         AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 
                         'RSTransfer' AS RefTable
FROM            dbo.RSTransfer INNER JOIN
                         dbo.RSTransferDetail ON dbo.RSTransfer.ID = dbo.RSTransferDetail.RSTransferID INNER JOIN
                         dbo.Type ON RSTransfer.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.MaterialGoods ON RSTransferDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON RSTransferDetail.ToRepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = RSTransferDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = RSTransferDetail.ContractID
/**/
UNION ALL
SELECT        dbo.PPInvoice.ID, dbo.PPInvoiceDetail.ID AS DetailID, dbo.PPInvoice.TypeID, dbo.Type.TypeName, dbo.PPInvoice.No, dbo.PPInvoice.InwardNo AS InwardNo, NULL AS OutwardNo, dbo.PPInvoice.Date, 
                         dbo.PPInvoice.PostedDate, dbo.PPInvoiceDetail.DebitAccount, dbo.PPInvoiceDetail.CreditAccount, PPInvoice.CurrencyID, dbo.PPInvoiceDetail.AmountOriginal, dbo.PPInvoiceDetail.Amount, NULL AS OrgPrice, 
                         dbo.PPInvoice.Reason, dbo.PPInvoiceDetail.Description, dbo.PPInvoiceDetail.CostSetID, CostSet.CostSetCode, dbo.PPInvoiceDetail.ContractID, EMContract.Code AS ContractCode, 
                         dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, PPInvoice.EmployeeID, 
                         dbo.PPInvoice.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID AS RepositoryID, 
                         dbo.Repository.RepositoryName, dbo.PPInvoiceDetail.DepartmentID, NULL AS DepartmentName, PPInvoiceDetail.VATAccount, PPInvoiceDetail.VATAmountOriginal, PPInvoiceDetail.VATAmount, 
                         PPInvoiceDetail.Quantity, PPInvoiceDetail.UnitPrice, dbo.PPInvoice.Recorded, dbo.PPInvoice.TotalAmountOriginal, dbo.PPInvoice.TotalAmount, dbo.PPInvoiceDetail.DiscountAmountOriginal, 
                         dbo.PPInvoiceDetail.DiscountAmount, NULL AS DiscountAccount, dbo.PPInvoiceDetail.FreightAmountOriginal, dbo.PPInvoiceDetail.FreightAmount, 0 AS DetailTax, PPInvoiceDetail.InwardAmountOriginal, 
                         PPInvoiceDetail.InwardAmount, dbo.PPInvoice.StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'PPInvoice' AS RefTable
FROM            dbo.PPInvoice INNER JOIN
                         dbo.PPInvoiceDetail ON dbo.PPInvoice.ID = dbo.PPInvoiceDetail.PPInvoiceID INNER JOIN
                         dbo.Type ON PPInvoice.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON PPInvoiceDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON PPInvoiceDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON PPInvoiceDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = PPInvoiceDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = PPInvoiceDetail.ContractID
/**/
UNION ALL
SELECT        dbo.PPDiscountReturn.ID, dbo.PPDiscountReturnDetail.ID AS DetailID, dbo.PPDiscountReturn.TypeID, dbo.Type.TypeName, dbo.PPDiscountReturn.No, NULL AS InwardNo, 
                         dbo.PPDiscountReturn.OutwardNo AS OutwardNo, dbo.PPDiscountReturn.Date, dbo.PPDiscountReturn.PostedDate, dbo.PPDiscountReturnDetail.DebitAccount, dbo.PPDiscountReturnDetail.CreditAccount, 
                         PPDiscountReturn.CurrencyID, dbo.PPDiscountReturnDetail.AmountOriginal, dbo.PPDiscountReturnDetail.Amount, NULL AS OrgPrice, dbo.PPDiscountReturn.Reason, dbo.PPDiscountReturnDetail.Description, 
                         dbo.PPDiscountReturnDetail.CostSetID, CostSet.CostSetCode, dbo.PPDiscountReturnDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, PPDiscountReturn.EmployeeID, 
                         dbo.PPDiscountReturn.BrachID AS BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, 
                         dbo.Repository.ID AS RepositoryID, dbo.Repository.RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, PPDiscountReturnDetail.VATAccount, PPDiscountReturnDetail.VATAmountOriginal, 
                         PPDiscountReturnDetail.VATAmount, PPDiscountReturnDetail.Quantity, PPDiscountReturnDetail.UnitPrice, dbo.PPDiscountReturn.Recorded, dbo.PPDiscountReturn.TotalAmountOriginal, 
                         dbo.PPDiscountReturn.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL 
                         AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'PPDiscountReturn' AS RefTable
FROM            dbo.PPDiscountReturn INNER JOIN
                         dbo.PPDiscountReturnDetail ON dbo.PPDiscountReturn.ID = dbo.PPDiscountReturnDetail.PPDiscountReturnID INNER JOIN
                         dbo.Type ON PPDiscountReturn.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON PPDiscountReturnDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON PPDiscountReturnDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON PPDiscountReturnDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = PPDiscountReturnDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = PPDiscountReturnDetail.ContractID
/**/
UNION ALL
SELECT        dbo.PPService.ID, dbo.PPServiceDetail.ID AS DetailID, dbo.PPService.TypeID, dbo.Type.TypeName, dbo.PPService.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.PPService.Date, dbo.PPService.PostedDate, 
                         dbo.PPServiceDetail.DebitAccount, dbo.PPServiceDetail.CreditAccount, PPService.CurrencyID, dbo.PPServiceDetail.AmountOriginal, dbo.PPServiceDetail.Amount, NULL AS OrgPrice, dbo.PPService.Reason, 
                         dbo.PPServiceDetail.Description, dbo.PPServiceDetail.CostSetID, CostSet.CostSetCode, dbo.PPServiceDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, PPService.EmployeeID, dbo.PPService.BranchID, NULL AS FixedAssetID, NULL 
                         AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, 
                         PPServiceDetail.VATAccount, PPServiceDetail.VATAmountOriginal, PPServiceDetail.VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.PPService.Recorded, dbo.PPService.TotalAmountOriginal, 
                         dbo.PPService.TotalAmount, PPServiceDetail.DiscountAmountOriginal, PPServiceDetail.DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL
                          AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'PPService' AS RefTable
FROM            dbo.PPService INNER JOIN
                         dbo.PPServiceDetail ON dbo.PPService.ID = dbo.PPServiceDetail.PPServiceID INNER JOIN
                         dbo.Type ON PPService.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON PPServiceDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON PPServiceDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = PPServiceDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = PPServiceDetail.ContractID
/**/
UNION ALL
SELECT        dbo.PPOrder.ID, dbo.PPOrderDetail.ID AS DetailID, dbo.PPOrder.TypeID, dbo.Type.TypeName, dbo.PPOrder.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.PPOrder.Date, 
                         dbo.PPOrder.DeliverDate AS PostedDate, dbo.PPOrderDetail.DebitAccount, dbo.PPOrderDetail.CreditAccount, PPOrder.CurrencyID, dbo.PPOrderDetail.AmountOriginal, dbo.PPOrderDetail.Amount, NULL 
                         AS OrgPrice, dbo.PPOrder.Reason, dbo.PPOrderDetail.Description, dbo.PPOrderDetail.CostSetID, CostSet.CostSetCode, dbo.PPOrderDetail.ContractID, EMContract.Code AS ContractCode, 
                         dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, PPOrder.EmployeeID, 
                         dbo.PPOrder.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, NULL AS RepositoryID, NULL 
                         AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, PPOrderDetail.VATAccount, PPOrderDetail.VATAmountOriginal, PPOrderDetail.VATAmount, NULL AS Quantity, NULL AS UnitPrice, 
                         dbo.PPOrder.Exported AS Recorded, dbo.PPOrder.TotalAmountOriginal, dbo.PPOrder.TotalAmount, PPOrderDetail.DiscountAmountOriginal, PPOrderDetail.DiscountAmount, NULL AS DiscountAccount, NULL 
                         AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL 
                         AS OutwardAmount, 'PPOrder' AS RefTable
FROM            dbo.PPOrder INNER JOIN
                         dbo.PPOrderDetail ON dbo.PPOrder.ID = dbo.PPOrderDetail.PPOrderID INNER JOIN
                         dbo.Type ON PPOrder.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON PPOrderDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON PPOrderDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = PPOrderDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = PPOrderDetail.ContractID
/**/
UNION ALL
SELECT        dbo.SAInvoice.ID, dbo.SAInvoiceDetail.ID AS DetailID, dbo.SAInvoice.TypeID, dbo.Type.TypeName, dbo.SAInvoice.No, NULL AS InwardNo, dbo.SAInvoice.OutwardNo AS OutwardNo, dbo.SAInvoice.Date, 
                         dbo.SAInvoice.PostedDate, dbo.SAInvoiceDetail.DebitAccount, dbo.SAInvoiceDetail.CreditAccount, SAInvoice.CurrencyID, dbo.SAInvoiceDetail.AmountOriginal, dbo.SAInvoiceDetail.Amount, NULL AS OrgPrice, 
                         dbo.SAInvoice.Reason, dbo.SAInvoiceDetail.Description, dbo.SAInvoiceDetail.CostSetID, CostSet.CostSetCode, dbo.SAInvoiceDetail.ContractID, EMContract.Code AS ContractCode, 
                         dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, SAInvoice.EmployeeID, 
                         dbo.SAInvoice.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID AS RepositoryID, 
                         dbo.Repository.RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, SAInvoiceDetail.VATAccount, SAInvoiceDetail.VATAmountOriginal, SAInvoiceDetail.VATAmount, SAInvoiceDetail.Quantity, 
                         SAInvoiceDetail.UnitPrice, dbo.SAInvoice.Recorded, dbo.SAInvoice.TotalAmountOriginal, dbo.SAInvoice.TotalAmount, dbo.SAInvoiceDetail.DiscountAmountOriginal, dbo.SAInvoiceDetail.DiscountAmount, 
                         dbo.SAInvoiceDetail.DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, 
                         dbo.SAInvoiceDetail.OWAmountOriginal AS OutwardAmountOriginal, dbo.SAInvoiceDetail.OWAmount AS OutwardAmount, 'SAInvoice' AS RefTable
FROM            dbo.SAInvoice INNER JOIN
                         dbo.SAInvoiceDetail ON dbo.SAInvoice.ID = dbo.SAInvoiceDetail.SAInvoiceID INNER JOIN
                         dbo.Type ON dbo.SAInvoice.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON SAInvoiceDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON SAInvoiceDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON SAInvoiceDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = SAInvoiceDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = SAInvoiceDetail.ContractID
/**/
UNION ALL
SELECT        dbo.SAReturn.ID, dbo.SAReturnDetail.ID AS DetailID, dbo.SAReturn.TypeID, dbo.Type.TypeName, dbo.SAReturn.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.SAReturn.Date, dbo.SAReturn.PostedDate, NULL 
                         AS DebitAccount, NULL AS CreditAccount, SAReturn.CurrencyID, dbo.SAReturnDetail.AmountOriginal, dbo.SAReturnDetail.Amount, NULL AS OrgPrice, dbo.SAReturn.Reason, dbo.SAReturnDetail.Description, 
                         dbo.SAReturnDetail.CostSetID, CostSet.CostSetCode, dbo.SAReturnDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, SAReturn.EmployeeID, dbo.SAReturn.BranchID, NULL AS FixedAssetID, NULL 
                         AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID AS RepositoryID, dbo.Repository.RepositoryName, NULL AS DepartmentID, NULL 
                         AS DepartmentName, SAReturnDetail.VATAccount, SAReturnDetail.VATAmountOriginal, SAReturnDetail.VATAmount, SAReturnDetail.Quantity, SAReturnDetail.UnitPrice, SAReturn.Recorded AS Posted, 
                         dbo.SAReturn.TotalAmountOriginal, dbo.SAReturn.TotalAmount, dbo.SAReturnDetail.DiscountAmountOriginal, dbo.SAReturnDetail.DiscountAmount, dbo.SAReturnDetail.DiscountAccount, NULL 
                         AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL 
                         AS OutwardAmount, 'SAReturn' AS RefTable
FROM            dbo.SAReturn INNER JOIN
                         dbo.SAReturnDetail ON dbo.SAReturn.ID = dbo.SAReturnDetail.SAReturnID INNER JOIN
                         dbo.Type ON dbo.SAReturn.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON SAReturnDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON SAReturnDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON SAReturnDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = SAReturnDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = SAReturnDetail.ContractID
/**/
UNION ALL
SELECT        dbo.SAOrder.ID, dbo.SAOrderDetail.ID AS DetailID, dbo.SAOrder.TypeID, dbo.Type.TypeName, dbo.SAOrder.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.SAOrder.Date, 
                         dbo.SAOrder.DeliveDate AS PostedDate, NULL AS DebitAccount, NULL AS CreditAccount, SAOrder.CurrencyID, dbo.SAOrderDetail.AmountOriginal, dbo.SAOrderDetail.Amount, NULL AS OrgPrice, 
                         dbo.SAOrder.Reason, dbo.SAOrderDetail.Description, dbo.SAOrderDetail.CostSetID, CostSet.CostSetCode, dbo.SAOrderDetail.ContractID, EMContract.Code AS ContractCode, 
                         dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, SAOrder.EmployeeID, 
                         dbo.SAOrder.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID AS RepositoryID, 
                         dbo.Repository.RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, SAOrderDetail.VATAccount, SAOrderDetail.VATAmountOriginal, SAOrderDetail.VATAmount, SAOrderDetail.Quantity, 
                         SAOrderDetail.UnitPrice, SAOrder.Exported AS Recorded, dbo.SAOrder.TotalAmountOriginal, dbo.SAOrder.TotalAmount, dbo.SAOrderDetail.DiscountAmountOriginal, dbo.SAOrderDetail.DiscountAmount, 
                         dbo.SAOrderDetail.DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'SAOrder' AS RefTable
FROM            dbo.SAOrder INNER JOIN
                         dbo.SAOrderDetail ON dbo.SAOrder.ID = dbo.SAOrderDetail.SAOrderID INNER JOIN
                         dbo.Type ON dbo.SAOrder.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON SAOrderDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON SAOrderDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON SAOrderDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = SAOrderDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = SAOrderDetail.ContractID
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
CREATE FUNCTION [dbo].[GODTOS]
(
	@datepart char(1), 
	@add smallint,
	@date datetime
)
RETURNS varchar(8)
WITH ENCRYPTION
AS
BEGIN
	DECLARE @dtos varchar(8)
	SET @dtos = '' 
	IF @datepart = 'Y'
	BEGIN
		SET @date = DATEADD(YY,@add,@date)
		SET @dtos = LTRIM(YEAR(@date))
	END
	ELSE IF @datepart = 'Q'
	BEGIN
		SET @date = DATEADD(QQ,@add,@date)
		SET @dtos = LTRIM(YEAR(@date))+LTRIM(DATEPART(QQ,@date))
	END
	ELSE IF @datepart = 'M'
	BEGIN
		SET @date = DATEADD(MM,@add,@date)
		SET @dtos = LEFT(CONVERT(varchar,@date,112),6) 
	END
	ELSE IF @datepart = 'D'
	BEGIN
		SET @date = DATEADD(DD,@add,@date)
		SET @dtos = CONVERT(varchar,@date,112) 
	END
	RETURN @dtos
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
CREATE FUNCTION [dbo].[CTOD]
(
	@datepart varchar(1), 
	@date varchar(8)
)
RETURNS varchar(8)
WITH ENCRYPTION
AS
BEGIN
	DECLARE @ctod varchar(8), @st varchar(1)
	SELECT @st = '1', @ctod = '' 
	IF CHARINDEX(@datepart,'Y/Q/M/D')=0
		SET @ctod = CASE LEN(@date) 
			WHEN 4 THEN LEFT(@date,4)
			WHEN 5 THEN LEFT(@date,4)+'1'
			WHEN 6 THEN LEFT(@date,4)+'01'
			ELSE LEFT(@date,6)+'01' END 
	ELSE IF @datepart = 'Y'
		SET @ctod = LEFT(@date,4)
	ELSE IF @datepart = 'Q'
	BEGIN
		IF LEN(@date)=4
			SET @st = '1'
		ELSE IF LEN(@date)=5 AND RIGHT(@date,1) BETWEEN 1 AND 4
			SET @st = RIGHT(@date,1)
		ELSE IF LEN(@date)>5
			SET @st = (SUBSTRING(@date,5,2)-1)/3+1 
		SET @ctod = LEFT(@date,4)+LTRIM(@st) 
	END
	ELSE IF @datepart = 'M'
	BEGIN	
		IF LEN(@date)=4
			SET @ctod = LEFT(@date,4)+'01'
		ELSE IF LEN(@date)=5 AND RIGHT(@date,1) BETWEEN 1 AND 4
			SET @ctod = LEFT(@date,4)+RIGHT('0'+LTRIM(RIGHT(@date,1)*3-2),2)
		ELSE IF LEN(@date)>5
			SET @ctod = LEFT(@date,6)
	END
	ELSE IF @datepart = 'D'
	BEGIN
		IF LEN(@date)=4 
			SET @ctod = @date+'0101'
		ELSE IF LEN(@date)=5 AND RIGHT(@date,1) BETWEEN 1 AND 4 
			SET @ctod = CASE RIGHT(@date,1)
					WHEN '1' THEN LEFT(@date,4)+'0101'
					WHEN '2' THEN LEFT(@date,4)+'0401'
					WHEN '3' THEN LEFT(@date,4)+'0701'
					WHEN '4' THEN LEFT(@date,4)+'1001' END 
		ELSE IF LEN(@date)>=6 
			SET @ctod = LEFT(@date,6)+'01' 	
	END	
	RETURN @ctod
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER FUNCTION [dbo].[Func_GetB02_DN]
    (
      @BranchID UNIQUEIDENTIFIER ,
      @IncludeDependentBranch BIT ,
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @PrevFromDate DATETIME ,
      @PrevToDate DATETIME
    )
RETURNS @Result TABLE
    (
      ItemID UNIQUEIDENTIFIER ,
      ItemCode NVARCHAR(25) ,
      ItemName NVARCHAR(255) ,
      ItemNameEnglish NVARCHAR(255) ,
      ItemIndex INT ,
      Description NVARCHAR(255) ,
      FormulaType INT ,
      FormulaFrontEnd NVARCHAR(MAX) ,
      Formula XML ,
      Hidden BIT ,
      IsBold BIT ,
      IsItalic BIT
      ,
      Amount DECIMAL(25, 4) ,
      PrevAmount DECIMAL(25, 4)
    )
AS 
    BEGIN
	DECLARE @AccountingSystem INT	
            SET @AccountingSystem = 48
        DECLARE @ReportID NVARCHAR(100)
        SET @ReportID = '2'/*báo cáo kết quả HĐ kinh doanh*/
    set @PrevToDate = DATEADD(DD,-1,@FromDate)
	declare @CalPrevDate nvarchar(20)
	declare @CalPrevMonth nvarchar(20)
	declare @CalPrevDay nvarchar(20)
	set @CalPrevDate = CAST(day(@FromDate) AS varchar) + CAST(month(@FromDate) AS varchar) + CAST(day(@ToDate) AS varchar) + CAST(month(@ToDate) AS varchar)
	set @CalPrevMonth = CAST(month(@FromDate) AS varchar) + CAST(month(@ToDate) AS varchar)
	set @CalPrevDay = CAST(day(@FromDate) AS varchar) + CAST(day(@ToDate) AS varchar)
	if @CalPrevDate = '113112'
	 set @PrevFromDate = convert(DATETIME,dbo.ctod('D',dbo.godtos('M', -12, @FromDate)),103)
	else if (@CalPrevMonth in (55,77,1010,1212) and @CalPrevDay in (131) )
	 set @PrevFromDate = DATEADD(DD,-30,@FromDate)
	else if (@CalPrevMonth in (11,22,88,44,66,99,1111) and @CalPrevDay in (131,130) )
	 set @PrevFromDate = DATEADD(DD,-31,@FromDate)
	else if (@CalPrevMonth in (33) and @CalPrevDay in (131) and CAST(year(@FromDate) AS varchar)%4 = 0)
	 set @PrevFromDate = DATEADD(DD,-29,@FromDate)
	else if (@CalPrevMonth in (33) and @CalPrevDay in (131) and CAST(year(@FromDate) AS varchar)%4 <> 0)
	 set @PrevFromDate = DATEADD(DD,-28,@FromDate)
	else if ( (@CalPrevMonth in (13,1012) and @CalPrevDay = '131') or (@CalPrevMonth in (46,79) and @CalPrevDay = '130') )
	 set @PrevFromDate = DATEADD(QQ,-1,@FromDate)
	else set @PrevFromDate = DATEADD(DD,-1-DATEDIFF(day, @FromDate, @ToDate),@FromDate)  
        DECLARE @tblItem TABLE
            (
              ItemID UNIQUEIDENTIFIER ,
              ItemName NVARCHAR(255) ,
              OperationSign INT ,
              OperandString NVARCHAR(255) ,
              AccountNumber VARCHAR(25) ,
              CorrespondingAccountNumber VARCHAR(25) ,
              IsDetailGreaterThanZero BIT
	      )
	
        INSERT  @tblItem
                SELECT  ItemID ,
                        ItemName ,
                        x.r.value('@OperationSign', 'INT') ,
                        x.r.value('@OperandString', 'nvarchar(255)') ,
                        x.r.value('@AccountNumber', 'nvarchar(25)') + '%' ,
                        CASE WHEN x.r.value('@CorrespondingAccountNumber',
                                            'nvarchar(25)') <> ''
                             THEN x.r.value('@CorrespondingAccountNumber',
                                            'nvarchar(25)') + '%'
                             ELSE ''
                        END ,
                        CASE WHEN FormulaType = 0 THEN CAST(0 AS BIT)
                             ELSE CAST(1 AS BIT)
                        END
                FROM    FRTemplate
                        CROSS APPLY Formula.nodes('/root/DetailFormula') AS x ( r )
                WHERE   AccountingSystem = @AccountingSystem
                        AND ReportID = @ReportID
                        AND ( FormulaType = 0/*chỉ tiêu chi tiết*/
                              OR FormulaType = 2/*chỉ tiêu chi tiết chỉ được lấy số liệu khi kết quả>0*/
                            )
                        AND Formula IS NOT NULL
						and itemcode not in ('31.1', '31.2', '32.1' , '32.2')
	
        DECLARE @Balance TABLE
            (
              AccountNumber NVARCHAR(25) ,
              CorrespondingAccountNumber NVARCHAR(25) ,
              PostedDate DATETIME ,
              CreditAmount DECIMAL(25, 4) ,
              DebitAmount DECIMAL(25, 4) ,
              CreditAmountDetailBy DECIMAL(25, 4) ,
              DebitAmountDetailBy DECIMAL(25, 4) ,
              IsDetailBy BIT,
              /*add by hoant 16.09.2016 theo cr 116741*/
                CreditAmountByBussinessType0 DECIMAL(25, 4) ,/*Có Chi tiết theo loại  0-Chiết khấu thương mai*/
               CreditAmountByBussinessType1 DECIMAL(25, 4) ,/*Có Chi tiết theo loại  1 - Giảm giá hàng bán*/
               CreditAmountByBussinessType2 DECIMAL(25, 4) ,/*Có Chi tiết theo loại   2- trả lại hàng bán*/
              DebitAmountByBussinessType0 DECIMAL(25, 4) ,/*Nợ Chi tiết theo loại  0-Chiết khấu thương mai*/
              DebitAmountByBussinessType1 DECIMAL(25, 4) ,/*Nợ Chi tiết theo loại  1 - Giảm giá hàng bán*/
              DebitAmountByBussinessType2 DECIMAL(25, 4) /*Nợ Chi tiết theo loại   2- trả lại hàng bán*/
            )			
/*
1. Doanh thu bán hàng và cung cấp dịch vụ

PS Có TK 511 (không kể PSĐƯ N911/C511, không kể các nghiệp vụ: Chiết khấu thương mại (bán hàng),

 Giảm giá hàng bán, Trả lại hàng bán)
  – PS Nợ TK 511 (không kể PSĐƯ N511/911, không kể các nghiệp vụ: Chiết khấu thương mại (bán hàng), 
  Giảm giá hàng bán, Trả lại hàng bán)
*/
		
        INSERT  INTO @Balance
                SELECT  GL.Account ,
                        GL.AccountCorresponding ,
                        GL.PostedDate ,
                        SUM(ISNULL(CreditAmount, 0)) AS CreditAmount ,
                        SUM(ISNULL(DebitAmount, 0)) AS DebitAmount ,
                        0 AS CreditAmountDetailBy ,
                        0 AS DebitAmountDetailBy ,
                        0,
                        SUM(CASE WHEN (GL.TypeID in ('320','321','322','323','324','325','350','360') and gl.Account like '511%')
                                 THEN ISNULL(CreditAmount, 0)
                                 ELSE 0
                            END) AS CreditAmountByBussinessType0 ,
                        SUM(CASE WHEN GL.TypeID =340
                                 THEN ISNULL(CreditAmount, 0)
                                 ELSE 0
                            END) AS CreditAmountByBussinessType1 
                            ,
                        SUM(CASE WHEN GL.TypeID =330
                                 THEN ISNULL(CreditAmount, 0)
                                 ELSE 0
                            END) AS CreditAmountByBussinessType2 ,
                        SUM(CASE WHEN (GL.TypeID in ('320','321','322','323','324','325','350','360') and gl.Account like '511%')
                                 THEN ISNULL(DebitAmount, 0)
                                 ELSE 0
                            END) AS DebitAmountByBussinessType0
                            ,
                        SUM(CASE WHEN GL.TypeID =340
                                 THEN ISNULL(DebitAmount, 0)
                                 ELSE 0
                            END) AS DebitAmountByBussinessType1
                            ,
                        SUM(CASE WHEN GL.TypeID =330
                                 THEN ISNULL(DebitAmount, 0)
                                 ELSE 0
                            END) AS DebitAmountByBussinessType2
                FROM    dbo.GeneralLedger GL
                WHERE   PostedDate BETWEEN @PrevFromDate AND @ToDate
                GROUP BY GL.Account ,
                        GL.AccountCorresponding,
                        GL.PostedDate                     
        OPTION  ( RECOMPILE )


        DECLARE @tblMasterDetail TABLE
            (
              ItemID UNIQUEIDENTIFIER ,
              DetailItemID UNIQUEIDENTIFIER ,
              OperationSign INT ,
              Grade INT ,
              DebitAmount DECIMAL(25, 4) ,
              PrevDebitAmount DECIMAL(25, 4)
            )	
	
	
	/*
	PhatsinhCO(511) - PhatsinhDU(911/511) + PhatsinhCO_ChitietChietKhauThuongmai(511)
	 + PhatsinhCO_ChitietGiamgiaHangBan(511) + PhatsinhCO_ChitietTralaiHangBan(511) +
	  PhatsinhNO(511) - PhatsinhDU(511/911) - PhatsinhNO_ChitietChietKhauThuongmai(511) - PhatsinhNO_ChitietGiamgiaHangBan(511) - PhatsinhNO_ChitietTralaiHangBan(511)
	*/
        INSERT  INTO @tblMasterDetail
                SELECT  I.ItemID ,
                        NULL ,
                        1 ,
                        -1
                        ,
                        CASE WHEN I.IsDetailGreaterThanZero = 0
                                  OR ( SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate
                                                THEN ( CASE WHEN I.OperandString = 'PhatsinhCO'
                                                            THEN GL.CreditAmount
                                                            WHEN I.OperandString IN (
                                                              'PhatsinhNO',
                                                              'PhatsinhDU' )
                                                            THEN GL.DebitAmount
                                                        
                                                            WHEN I.OperandString = 'PhatsinhNO_ChitietChietKhauThuongmai' THEN 
                                                              GL.DebitAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietGiamgiaHangBan' THEN 
                                                              GL.DebitAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietTralaiHangBan' THEN 
                                                              GL.DebitAmountByBussinessType2
                                                            WHEN I.OperandString = 'PhatsinhCO_ChitietChietKhauThuongmai1111' THEN 
                                                              GL.CreditAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietGiamgiaHangBan' THEN 
                                                              GL.CreditAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietTralaiHangBan' THEN 
                                                              GL.CreditAmountByBussinessType2
                                                        
                                                              
                                                            WHEN I.OperandString IN (
                                                              'PhatsinhCO_ChiTietThanhlyTSCD_BDSDT' )
                                                              AND GL.IsDetailBy = 1
                                                            THEN GL.CreditAmountDetailBy
                                                            WHEN I.OperandString IN (
                                                              'PhatsinhNO_ChiTietThanhlyTSCD_BDSDT' )
                                                              AND GL.IsDetailBy = 1
                                                            THEN GL.DebitAmountDetailBy
                                                            ELSE 0
                                                       END ) * I.OperationSign
                                                ELSE 0
                                           END) ) > 0
                             THEN ( SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate
                                             THEN ( CASE WHEN I.OperandString = 'PhatsinhCO'
                                                         THEN GL.CreditAmount
                                                         WHEN I.OperandString IN (
                                                              'PhatsinhNO',
                                                              'PhatsinhDU' )
                                                         THEN GL.DebitAmount
                                                    
                                                            WHEN I.OperandString = 'PhatsinhNO_ChitietChietKhauThuongmai' THEN 
                                                              GL.DebitAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietGiamgiaHangBan' THEN 
                                                              GL.DebitAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietTralaiHangBan' THEN 
                                                              GL.DebitAmountByBussinessType2
                                                            WHEN I.OperandString = 'PhatsinhCO_ChitietChietKhauThuongmai1111' THEN 
                                                              GL.CreditAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietGiamgiaHangBan' THEN 
                                                              GL.CreditAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietTralaiHangBan' THEN 
                                                              GL.CreditAmountByBussinessType2
                                                     
                                                              
                                                         WHEN I.OperandString IN (
                                                              'PhatsinhCO_ChiTietThanhlyTSCD_BDSDT' )
                                                              AND GL.IsDetailBy = 1
                                                         THEN GL.CreditAmountDetailBy
                                                         WHEN I.OperandString IN (
                                                              'PhatsinhNO_ChiTietThanhlyTSCD_BDSDT' )
                                                              AND GL.IsDetailBy = 1
                                                         THEN GL.DebitAmountDetailBy
                                                         ELSE 0
                                                    END ) * I.OperationSign
                                             ELSE 0
                                        END) )
                             ELSE 0
                        END AS DebitAmount ,
                        CASE WHEN I.IsDetailGreaterThanZero = 0
                                  OR ( SUM(CASE WHEN GL.PostedDate BETWEEN @PrevFromDate
                                                              AND
                                                              @PrevToDate
                                                THEN CASE WHEN I.OperandString = 'PhatsinhCO'
                                                          THEN GL.CreditAmount
                                                          WHEN I.OperandString IN (
                                                              'PhatsinhNO',
                                                              'PhatsinhDU' )
                                                          THEN GL.DebitAmount
            
                                                            WHEN I.OperandString = 'PhatsinhNO_ChitietChietKhauThuongmai' THEN 
                                                              GL.DebitAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietGiamgiaHangBan' THEN 
                                                              GL.DebitAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietTralaiHangBan' THEN 
                                                              GL.DebitAmountByBussinessType2
                                                            WHEN I.OperandString = 'PhatsinhCO_ChitietChietKhauThuongmai1111' THEN 
                                                              GL.CreditAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietGiamgiaHangBan' THEN 
                                                              GL.CreditAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietTralaiHangBan' THEN 
                                                              GL.CreditAmountByBussinessType2
                                                         
                                                              
                                                              
                                                          WHEN I.OperandString IN (
                                                              'PhatsinhCO_ChiTietThanhlyTSCD_BDSDT' )
                                                              AND GL.IsDetailBy = 1
                                                          THEN GL.CreditAmountDetailBy
                                                          WHEN I.OperandString IN (
                                                              'PhatsinhNO_ChiTietThanhlyTSCD_BDSDT' )
                                                              AND GL.IsDetailBy = 1
                                                          THEN GL.DebitAmountDetailBy
                                                          ELSE 0
                                                     END * I.OperationSign
                                                ELSE 0
                                           END) ) > 0
                             THEN SUM(CASE WHEN GL.PostedDate BETWEEN @PrevFromDate
                                                              AND
                                                              @PrevToDate
                                           THEN CASE WHEN I.OperandString = 'PhatsinhCO'
                                                     THEN GL.CreditAmount
                                                     WHEN I.OperandString IN (
                                                          'PhatsinhNO',
                                                          'PhatsinhDU' )
                                                     THEN GL.DebitAmount
                                                     
                                                            WHEN I.OperandString = 'PhatsinhNO_ChitietChietKhauThuongmai' THEN 
                                                              GL.DebitAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietGiamgiaHangBan' THEN 
                                                              GL.DebitAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietTralaiHangBan' THEN 
                                                              GL.DebitAmountByBussinessType2
                                                            WHEN I.OperandString = 'PhatsinhCO_ChitietChietKhauThuongmai1111' THEN 
                                                              GL.CreditAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietGiamgiaHangBan' THEN 
                                                              GL.CreditAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietTralaiHangBan' THEN 
                                                              GL.CreditAmountByBussinessType2
                                              
                                                              
                                                              
                                                     WHEN I.OperandString IN (
                                                          'PhatsinhCO_ChiTietThanhlyTSCD_BDSDT' )
                                                          AND GL.IsDetailBy = 1
                                                     THEN GL.CreditAmountDetailBy
                                                     WHEN I.OperandString IN (
                                                          'PhatsinhNO_ChiTietThanhlyTSCD_BDSDT' )
                                                          AND GL.IsDetailBy = 1
                                                     THEN GL.DebitAmountDetailBy
                                                     ELSE 0
                                                END * I.OperationSign
                                           ELSE 0
                                      END)
                             ELSE 0
                        END AS DebitAmount
                FROM    @tblItem I
                        INNER JOIN @Balance AS GL ON ( GL.AccountNumber LIKE I.AccountNumber )
                                                     AND ( I.OperandString <> 'PhatsinhDU'
                                                           OR ( I.OperandString = 'PhatsinhDU'
                                                              AND GL.CorrespondingAccountNumber LIKE I.CorrespondingAccountNumber
                                                              )
                                                         )
                GROUP BY I.ItemID ,
                        I.IsDetailGreaterThanZero
        OPTION  ( RECOMPILE )
			
        INSERT  @tblMasterDetail
                SELECT  ItemID ,
                        x.r.value('@ItemID', 'UNIQUEIDENTIFIER') ,
                        x.r.value('@OperationSign', 'INT') ,
                        0 ,
                        0.0 ,
                        0.0
                FROM    FRTemplate
                        CROSS APPLY Formula.nodes('/root/MasterFormula') AS x ( r )
                WHERE   AccountingSystem = @AccountingSystem
                        AND ReportID = @ReportID
                        AND FormulaType = 1
                        AND Formula IS NOT NULL 
						and itemcode not in ('31.1', '31.2', '32.1' , '32.2')
	
	

	;
        WITH    V ( ItemID, DetailItemID, DebitAmount, PrevDebitAmount, OperationSign )
                  AS ( SELECT   ItemID ,
                                DetailItemID ,
                                DebitAmount ,
                                PrevDebitAmount ,
                                OperationSign
                       FROM     @tblMasterDetail
                       WHERE    Grade = -1
                       UNION ALL
                       SELECT   B.ItemID ,
                                B.DetailItemID ,
                                V.DebitAmount ,
                                V.PrevDebitAmount ,
                                B.OperationSign * V.OperationSign AS OperationSign
                       FROM     @tblMasterDetail B ,
                                V
                       WHERE    B.DetailItemID = V.ItemID
                     )
	INSERT    @Result
                SELECT  FR.ItemID ,
                        FR.ItemCode ,
                        FR.ItemName ,
                        FR.ItemNameEnglish ,
                        FR.ItemIndex ,
                        FR.Description ,
                        FR.FormulaType ,
                        CASE WHEN FR.FormulaType = 1 THEN FR.FormulaFrontEnd
                             ELSE ''
                        END AS FormulaFrontEnd ,
                        CASE WHEN FR.FormulaType = 1 THEN FR.Formula
                             ELSE NULL
                        END AS Formula ,
                        FR.Hidden ,
                        FR.IsBold ,
                        FR.IsItalic ,
                        ISNULL(X.Amount, 0) AS Amount ,
                        ISNULL(X.PrevAmount, 0) AS PrevAmount
                FROM    ( SELECT    V.ItemID ,
                                    SUM(V.OperationSign * V.DebitAmount) AS Amount ,
                                    SUM(V.OperationSign * V.PrevDebitAmount) AS PrevAmount
                          FROM      V
                          GROUP BY  ItemID
                        ) AS X
                        RIGHT JOIN dbo.FRTemplate FR ON FR.ItemID = X.ItemID
                WHERE   AccountingSystem = @AccountingSystem
                        AND ReportID = @ReportID
						and itemcode not in ('31.1', '31.2', '32.1' , '32.2')
                ORDER BY ItemIndex
	
        RETURN 
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

GO
/*
*/
ALTER FUNCTION [dbo].[Func_ConvertStringIntoTable]
    (
      @GUIString NVARCHAR(MAX),
      @SeparateCharacter NCHAR(1)
    )
RETURNS @ValueTable TABLE (Value UNIQUEIDENTIFIER)
AS BEGIN
 
    DECLARE @ValueListLength INT,
			@StartingPosition INT,
			@Value NVARCHAR(50),
			@SecondPosition INT
			
    SET @ValueListLength = LEN(@GUIString)
    SET @StartingPosition = 1
    SET @Value = SPACE(0)
 
	WHILE @StartingPosition < @ValueListLength
		BEGIN
			SET @SecondPosition = CHARINDEX(@SeparateCharacter,@GUIString,@StartingPosition+1)
			SET @Value = SUBSTRING(@GUIString,@StartingPosition+1,@SecondPosition-@StartingPosition-1)	
			SET @StartingPosition = @SecondPosition
			IF @Value <> SPACE(0)
				INSERT  INTO @ValueTable (Value) VALUES(CAST(@Value AS UNIQUEIDENTIFIER))
		END
	RETURN 
   END



GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

/*=================================================================================================
 * Created By:		haipl
 * Created Date:	25/12/2017
 * Description:		Lay du lieu cho bao cao << Sổ chi tiết bán hàng >>
===================================================================================================== */
ALTER PROCEDURE [dbo].[Proc_SA_GetSalesBookDetail]
  
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @BranchID UNIQUEIDENTIFIER ,
    @IncludeDependentBranch BIT ,
    @InventoryItemID NVARCHAR(MAX) , 
    @OrganizationUnitID UNIQUEIDENTIFIER ,
    @AccountObjectID NVARCHAR(MAX) ,
	@SumGiaVon decimal output
AS

    BEGIN
        SET NOCOUNT ON;
        declare @v1 DECIMAL(29,4)                            
        CREATE TABLE #tblListInventoryItemID
            (
              InventoryItemID UNIQUEIDENTIFIER PRIMARY KEY
            ) 
        INSERT  INTO #tblListInventoryItemID
                    SELECT  T.*
                    FROM    dbo.Func_ConvertStringIntoTable(@InventoryItemID,
                                                              ',')	
                                                              AS T  
        select a.Date as ngay_CT, 
		a.PostedDate as ngay_HT, 
		a.No as So_Hieu,
		a.InvoiceDate as Ngay_HD, 
		a.InvoiceNo as SO_HD, 
		Sreason as Dien_giai, 
		accountCorresponding as TK_DOIUNG,
		c.Unit as DVT, 
		b.Quantity as SL, 
		UnitPriceOriginal as DON_GIA, 
		sum(debitAmount) as KHAC, 
		sum(CreditAmount) as TT, 
		c.ID as  InventoryItemID,
		c.MaterialGoodsName
		from GeneralLedger a join SAInvoiceDetail b on a.DetailID = b.ID 
		join MaterialGoods c on b.MaterialGoodsID = c.ID
		join SAInvoice d on a.ReferenceID = d.ID
		where a.PostedDate between  @FromDate and @ToDate and c.ID in (select InventoryItemID from #tblListInventoryItemID)
		and Account like '511%'
		group by a.Date,  a.PostedDate, a.No ,a.InvoiceDate , a.InvoiceNo , Sreason , accountCorresponding , 
		c.Unit , b.Quantity , UnitPriceOriginal, c.ID,c.MaterialGoodsName ;
		select  isnull(sum(a.debitAmount),0) Sum_Gia_Goc, c.MaterialGoodsCode,c.ID  as InventoryItemID
 from GeneralLedger a 
join SAInvoiceDetail b on a.ReferenceID = b.SAInvoiceID 
join MaterialGoods c on c.ID = b.MaterialGoodsID
 where PostedDate between @FromDate and @ToDate and Account = '632' and c.ID in (select InventoryItemID from #tblListInventoryItemID)
 group by c.MaterialGoodscode,c.ID;
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
CREATE FUNCTION [dbo].[GOCTOD]
(
	@datepart char(1), 
	@add smallint,
	@date varchar(11)
)
RETURNS varchar(8)
WITH ENCRYPTION
AS
BEGIN
	DECLARE @ctos varchar(8), @datetime datetime
	SET @ctos = '' 
	SET @datetime = CASE LEN(@date) 
		WHEN 4 THEN @date+'0101'
		WHEN 5 THEN LEFT(@date,4)+RIGHT('0'+LTRIM(RIGHT(@date,1)*3-2),2)+'01'
		WHEN 6 THEN @date+'01' 
		ELSE @date END
	IF CHARINDEX(@datepart,'Y/Q/M/D')=0
		SET @datepart=CASE LEN(@date)
			WHEN 4 THEN 'Y'
			WHEN 5 THEN 'Q'
			WHEN 6 THEN 'M'
			ELSE 'D' END
	IF @datepart = 'Y'
	BEGIN
		SET @datetime = DATEADD(YY,@add,@datetime)
		SET @ctos = LTRIM(YEAR(@datetime))
	END
	ELSE IF @datepart = 'Q'
	BEGIN
		SET @datetime = DATEADD(QQ,@add,@datetime)
		SET @ctos = LTRIM(YEAR(@datetime))+LTRIM(DATEPART(QQ, @datetime))
	END
	ELSE IF @datepart = 'M'
	BEGIN
		SET @datetime = DATEADD(MM,@add,@datetime)
		SET @ctos = LEFT(CONVERT(varchar,@datetime,112),6) 
	END
	ELSE IF @datepart = 'D'
	BEGIN
		SET @datetime = DATEADD(DD,@add,@datetime)
		SET @ctos = CONVERT(varchar,@datetime,112) 
	END
	RETURN @ctos
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
CREATE FUNCTION [dbo].[CTOS]
(
	@datepart varchar(1), 
	@date varchar(8)
)
RETURNS varchar(8)
WITH ENCRYPTION
AS
BEGIN
	DECLARE @ctos varchar(8), @st varchar(1), @datetime datetime
	SELECT @st = '4', @ctos = '' 
	IF CHARINDEX(@datepart,'Y/Q/M/D')=0
		SELECT @ctos=CASE LEN(@date) 
			WHEN 4 THEN LEFT(@date,4)
			WHEN 5 THEN LEFT(@date,4)+'4' 
			WHEN 6 THEN LEFT(@date,4)+'12' END
	ELSE IF @datepart = 'Y'
		SET @ctos = LEFT(@date,4)
	ELSE IF @datepart = 'Q'
	BEGIN
		IF LEN(@date)=4
			SET @st = '4'
		ELSE IF LEN(@date)=5 AND RIGHT(@date,1) BETWEEN 1 AND 4
			SET @st = RIGHT(@date,1)
		ELSE IF LEN(@date)>5
			SET @st = (SUBSTRING(@date,5,2)-1)/3+1 
		SET @ctos = LEFT(@date,4)+LTRIM(@st)
	END
	ELSE IF @datepart = 'M'
	BEGIN	
		IF LEN(@date)=4
			SET @ctos = LEFT(@date,4)+'12'
		ELSE IF LEN(@date)=5 AND RIGHT(@date,1) BETWEEN 1 AND 4
			SET @ctos = LEFT(@date,4)+RIGHT('0'+LTRIM(RIGHT(@date,1)*3),2)
		ELSE IF LEN(@date)>5
			SET @ctos = LEFT(@date,6)
	END
	ELSE IF @datepart = 'D'
	BEGIN
		IF LEN(@date)=4 
			SET @ctos = @date+'1231'
		ELSE IF LEN(@date)=5 AND RIGHT(@date,1) BETWEEN 1 AND 4 
			SET @ctos = CASE RIGHT(@date,1)
					WHEN '1' THEN LEFT(@date,4)+'0331'
					WHEN '2' THEN LEFT(@date,4)+'0630'
					WHEN '3' THEN LEFT(@date,4)+'0930'
					WHEN '4' THEN LEFT(@date,4)+'1231' END 
		ELSE IF LEN(@date)>=6 
		BEGIN
			SET @datetime = LEFT(@date,6)+'01' 	
			SET @datetime = DATEADD(M,+1,@datetime)
			SET @datetime = DATEADD(D,-1,@datetime)
			SET @ctos = CONVERT(varchar,@datetime,112)
		END
	END	
	RETURN @ctos
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO


GO
DISABLE TRIGGER ALL ON dbo.TemplateColumn
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.Template
  DROP CONSTRAINT FK_Template_Type
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateColumn
  DROP CONSTRAINT FK_TemplateColumn_TemplateDetail
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateDetail
  DROP CONSTRAINT FK_TemplateDetail_Template
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.Type
  DROP CONSTRAINT FK_Type_TypeGroup
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153AdjustAnnouncement
  DROP CONSTRAINT FK__TT153Adju__TypeI__0D64F3ED
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153DeletedInvoice
  DROP CONSTRAINT FK__TT153Dele__TypeI__5C37ACAD
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153DestructionInvoice
  DROP CONSTRAINT FK__TT153Dest__TypeI__0A888742
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153LostInvoice
  DROP CONSTRAINT FK__TT153Lost__TypeI__07AC1A97
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153PublishInvoice
  DROP CONSTRAINT FK__TT153Publ__TypeI__04CFADEC
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153RegisterInvoice
  DROP CONSTRAINT FK__TT153Regi__TypeI__01F34141
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.GenCode
  DROP CONSTRAINT FK_GenCode_TypeGroup
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.AccountDefault SET FilterAccount = N'111;112;1122' WHERE ID = '259cb47b-f744-4531-9749-6d6d6e394501'
UPDATE dbo.AccountDefault SET FilterAccount = N'112;141' WHERE ID = 'ec67e45f-86fb-41d9-9eb5-bc4f8f4e7c8e'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.ApiService SET ApiPath = N'api/publish/externalGetDigestForAdjustment' WHERE ID = '602b3dab-724e-4ccc-8f53-189c7ded2ea9'

INSERT dbo.ApiService(ID, SupplierServiceID, SupplierServiceCode, ApiName, ApiPath) VALUES ('b2d2e6d6-9926-4ea4-b666-29cc36d5209e', '0c38d0fb-067f-4875-9b68-ac56f65160cf', N'SDS', N'BaoCaoDoanhThuTheoSP', N'api/business/GetRevenueReportByProduct')
INSERT dbo.ApiService(ID, SupplierServiceID, SupplierServiceCode, ApiName, ApiPath) VALUES ('d71efb71-3203-4fb7-89c0-2b234cf1c912', '0c38d0fb-067f-4875-9b68-ac56f65160cf', N'SDS', N'ChuyenDoiHD', N'api/publish/getInvoicePdf')
INSERT dbo.ApiService(ID, SupplierServiceID, SupplierServiceCode, ApiName, ApiPath) VALUES ('54a865c0-ca28-45ca-aee7-4b050219a738', '0c38d0fb-067f-4875-9b68-ac56f65160cf', N'SDS', N'BaoCaoDoanhThuTheoBenMua', N'api/business/GetRevenueReportByCustomer')
INSERT dbo.ApiService(ID, SupplierServiceID, SupplierServiceCode, ApiName, ApiPath) VALUES ('5a1ab402-16af-4854-b658-7bc6a1e8f153', '0c38d0fb-067f-4875-9b68-ac56f65160cf', N'SDS', N'LayThongTinHoaDon', N'api/publish/GetInvoicesByIkeys')
INSERT dbo.ApiService(ID, SupplierServiceID, SupplierServiceCode, ApiName, ApiPath) VALUES ('ee131b31-6dc8-4e48-9337-916069d11db7', '0c38d0fb-067f-4875-9b68-ac56f65160cf', N'SDS', N'DanhSachHDCho', N'api/business/getInvoiceStrip')
INSERT dbo.ApiService(ID, SupplierServiceID, SupplierServiceCode, ApiName, ApiPath) VALUES ('bfcc90be-8481-4ca0-ab6d-efc1c215d8ae', '0c38d0fb-067f-4875-9b68-ac56f65160cf', N'SDS', N'BangKeHDChungTuHHDV', N'api/business/GetInvoiceReport')
INSERT dbo.ApiService(ID, SupplierServiceID, SupplierServiceCode, ApiName, ApiPath) VALUES ('42c31846-ab60-4a14-8dfe-fba5b2fcc2e1', '0c38d0fb-067f-4875-9b68-ac56f65160cf', N'SDS', N'GuiMail', N'/api/business/SendIssuanceNotice')
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

INSERT dbo.SupplierService(ID, SupplierServiceCode, SupplierServiceName, PathAccess) VALUES ('0c38d0fb-067f-4875-9b68-ac56f65160cf', N'SDS', N'Công ty Cổ phần đầu tư công nghệ và thương mại Sofdreams', N'http://demo2.easyinvoice.vn/')
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

INSERT dbo.SystemOption(ID, Code, Name, Type, Data, DefaultData, Note, IsSecurity) VALUES (120, N'CheckHDCD', N'Hóa đơn cách giải', 1, N'1', N'', N'', 1)
INSERT dbo.SystemOption(ID, Code, Name, Type, Data, DefaultData, Note, IsSecurity) VALUES (121, N'HDDT', N'Tích hợp hóa đơn điện tử', 4, N'1', N'0', NULL, 0)
INSERT dbo.SystemOption(ID, Code, Name, Type, Data, DefaultData, Note, IsSecurity) VALUES (122, N'VTHH_KiemHD', N'Chứng từ bán hàng kiêm hóa đơn', 6, N'1', N'1', N'0: Not check
1: Check', 0)
INSERT dbo.SystemOption(ID, Code, Name, Type, Data, DefaultData, Note, IsSecurity) VALUES (123, N'VTHH_KoKiemHD', N'Chứng từ bán hàng không kiêm hóa đơn', 6, N'0', N'0', N'0: Not check
1: Check', 0)
INSERT dbo.SystemOption(ID, Code, Name, Type, Data, DefaultData, Note, IsSecurity) VALUES (124, N'TCKHAC_SUDUNGTHEMSOQUANTRI', N'Sử dụng thêm sổ quản trị', 4, N'0', N'0', N'0: Not check 1: Check', 0)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

SET IDENTITY_INSERT dbo.Template ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
INSERT dbo.Template(ID, TemplateName, TypeID, IsDefault, IsActive, IsSecurity, OrderPriority) VALUES ('ec9ca46d-b8dc-4607-9ba0-2bebb8db4e9d', N'Mẫu chuẩn', 326, 0, 1, 1, 1)
GO
SET IDENTITY_INSERT dbo.Template OFF
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DELETE dbo.TemplateColumn WHERE ID = '906cc461-5717-46d4-be0d-66b7248346c7'
DELETE dbo.TemplateColumn WHERE ID = '0d9869f7-e171-4a85-bf67-e1c03922e705'

UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '74d858cc-18e0-42ec-bf62-00b93a7a7619'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên TSCĐ' WHERE ID = '35ce94d0-e501-4397-a2f4-0303b31e48b1'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên hàng' WHERE ID = '88057856-cc6a-4242-9147-03588606422e'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '46afa327-2ccd-4297-9469-0524f4789324'
UPDATE dbo.TemplateColumn SET VisiblePosition = 12 WHERE ID = '36e7cd26-d94d-4e08-90e1-0f28a3b010d5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '23987584-1443-47ee-bc04-193630d1d4d5'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '478332e8-615b-479d-90da-201bbf873077'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên hàng' WHERE ID = 'f2a3e55a-c0b6-460b-8494-239d84e19fb0'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên hàng' WHERE ID = '2d14e77b-789d-46a3-b2b5-254a80555fcf'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'c827f824-1cb4-43fc-b30a-26a69aac6832'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên hàng' WHERE ID = '2f06dc6b-8d6c-4625-bb25-26f4574dbc63'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '1233a43b-d483-4271-8d5a-299f6d1adcce'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '9fcd7b67-7e0c-4dbd-a83d-2b694f7da829'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'df917a92-c507-4213-815c-2ced85ae2751'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên hàng' WHERE ID = '1aa35ecb-6891-4cf8-b816-2d41e0f28f43'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên hàng' WHERE ID = '17c3b3d3-dfbb-47f9-8fd3-2ed46c830f03'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên TSCĐ' WHERE ID = '6485216d-a52e-4508-a07c-2f0e97e8e918'
UPDATE dbo.TemplateColumn SET VisiblePosition = 10 WHERE ID = 'bc0ffc1d-4671-4d1b-8b4f-2f14eebf3617'
UPDATE dbo.TemplateColumn SET VisiblePosition = 17 WHERE ID = '7671a151-87eb-4b3f-be0c-3140f1fc075a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0d7ad216-c0e3-4dd7-9875-352e555abf9f'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'ebe7683f-52d4-4f40-bc9b-38cb9673165f'
UPDATE dbo.TemplateColumn SET VisiblePosition = 11 WHERE ID = 'f526d298-a24a-4034-bd68-3c2f96bd9de7'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '9909bac0-45c9-45c0-81b6-426e842cc0b8'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên hàng' WHERE ID = '8dce256d-7a15-4170-816c-460ba8ff9f4d'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'c1c84b0f-f487-4bdc-a05d-462ebce2c908'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên hàng' WHERE ID = '54f287ae-02ce-4d5e-b93a-48a5d15ae65b'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên hàng' WHERE ID = '2c6824ee-90b1-4284-8056-4b39114c31cc'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '85e9308c-d665-448e-bcff-570a4cbabeda'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '388dfe6a-c613-40bd-aa46-58e2069aa902'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên hàng' WHERE ID = '63915f58-d93c-440f-a9bb-59816ae5d127'
UPDATE dbo.TemplateColumn SET IsVisible = 1 WHERE ID = '494a02fc-e900-4b12-a26f-5ff2f93f5523'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên hàng' WHERE ID = 'c2b700e7-887f-469e-a525-62b2013c85cb'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Mẫu số hóa đơn' WHERE ID = 'c1dccad4-91d5-4e4c-bf90-62bb2b155a74'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1, IsVisible = 0 WHERE ID = 'c94094e0-6aad-439a-80be-6604bdaadbe7'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên hàng' WHERE ID = '8f36f001-940c-4f51-b2d5-6c0ebe0cd2b9'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'c9186d38-d3a4-462e-b037-6d79ffec8b78'
UPDATE dbo.TemplateColumn SET ColumnName = N'VATDescription', ColumnCaption = N'Diễn giải thuế' WHERE ID = '74b0a283-d5cb-4a6f-9d39-6f12fc85bd0a'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên hàng' WHERE ID = '6026aabc-326d-4b8c-abe7-6f3fb17dc132'
UPDATE dbo.TemplateColumn SET VisiblePosition = 51 WHERE ID = '91db323c-18bb-4575-9cdc-70e361fbd8aa'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên hàng' WHERE ID = '461d502d-8561-472a-a7dc-71aea2706868'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '91a87b06-ffa1-4b96-a7b1-8047dd4e58e5'
UPDATE dbo.TemplateColumn SET ColumnName = N'VATDescription', ColumnCaption = N'Diễn giải thuế' WHERE ID = 'df945ddf-b90e-448e-a45b-81c64b4ef39c'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '2ffb309c-60a0-4c92-a873-81daa2ea0aa5'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên hàng' WHERE ID = '3c4c3753-7b48-42d5-8e8d-84c17e5cd3c0'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '4ef9db9c-4f64-4a5c-9792-8577ad6713cc'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên CCDC' WHERE ID = 'bdc25a99-14e8-42ff-9ad7-87808b44edde'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3f884677-3a17-429a-aae1-891917a06d38'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên hàng' WHERE ID = '92b143e0-954c-4e61-a6cd-8a0ac5efceac'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '7fab9ace-2e90-4498-863f-8dfbeb383ed4'
UPDATE dbo.TemplateColumn SET VisiblePosition = 13 WHERE ID = 'a8c27728-9936-4761-8929-8e14952bb19a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'd832765f-741f-41cb-a530-8ee473baed5f'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên hàng' WHERE ID = '5d50c8ba-e94f-4902-acf4-9039f2d8758f'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên hàng' WHERE ID = '831917b2-b99e-4454-9a6e-90efac037fc4'
UPDATE dbo.TemplateColumn SET VisiblePosition = 16 WHERE ID = '59c06851-927d-41c5-ac9a-953ecf8e2df5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ca6d2d2a-1616-496e-b205-960726733e73'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'a36f979f-d3ed-493a-84c1-96e0960a1d7b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 12 WHERE ID = '12a564ad-b8dc-46cf-9b53-98ca6a364d63'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên hàng' WHERE ID = '77a4aaa4-0bbd-4501-9af4-9e5f5c3a3de5'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên hàng' WHERE ID = '1af65d9a-e09a-401d-bf19-a1b11dfa4d08'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Mẫu số hóa đơn' WHERE ID = '41d07d61-ef68-4cd7-a6c9-a4faf1fcc0d4'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'eafe029b-f812-4a25-ab0e-a5bad453cc1c'
UPDATE dbo.TemplateColumn SET VisiblePosition = 9 WHERE ID = '26e1f111-4b34-4686-b908-a7335f541d42'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '96af0aa3-b433-4ec4-8475-a7501f81a896'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên hàng' WHERE ID = 'fecc84a3-f999-4cc0-870a-a89f1150cb56'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '3dbc7464-65e7-4c86-9cb4-a9576a0f225e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ba7959b3-7353-4b58-9a3c-a95bb7f9efd0'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '4fd2aab2-2d9d-4176-aaef-ab7945a65bc9'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên TSCĐ' WHERE ID = 'feb26084-d95b-4796-a78f-b199171b2002'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên hàng' WHERE ID = '851c84de-075c-4edc-8edd-b4a0171dbe2d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 19 WHERE ID = '570b95a4-1c8b-4d26-b1ba-b64f05ea5b9a'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên hàng' WHERE ID = 'fc08d380-bf1d-46fa-847c-b934b5f2edde'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '1ab122f3-1efd-4a38-91bb-babc7d4f4793'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '8d7385e9-07c6-4a2a-8327-bce7e512bd37'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên hàng' WHERE ID = '12876b23-7b15-48bb-a2b1-c377db118aba'
UPDATE dbo.TemplateColumn SET VisiblePosition = 18 WHERE ID = 'f05f938e-4753-4053-b036-c4af410eab0d'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên TSCĐ' WHERE ID = 'c26d31b4-e744-4f9c-9a9b-c8a19ae7f4f1'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên hàng' WHERE ID = 'c72981c5-d25d-48b8-b9fc-cccbdcbbe8de'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'a14036c0-47bb-409b-895b-cea29d382400'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'c5a1d67b-872a-4969-a859-cff1dc371c78'
UPDATE dbo.TemplateColumn SET ColumnName = N'VATDescription', ColumnCaption = N'Diễn giải thuế' WHERE ID = '008ac3d4-1ecf-486d-b916-d043e8f18979'
UPDATE dbo.TemplateColumn SET VisiblePosition = 50 WHERE ID = 'ea7f7ef6-73ea-4659-9681-d52e05097f09'
UPDATE dbo.TemplateColumn SET VisiblePosition = 15 WHERE ID = 'ab309d7c-1d64-4419-8ec5-d70cf438f403'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '1c054e5a-7095-4810-8f97-d948f0bdfd20'
UPDATE dbo.TemplateColumn SET ColumnName = N'VATDescription', ColumnCaption = N'Diễn giải thuế' WHERE ID = '2add1d84-1257-4c08-8196-db56c562a9e0'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '0c9c0267-bbe0-4bf9-8d85-dcf9c7327899'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên hàng' WHERE ID = '16304d4a-9b29-427b-8f7f-e19d2ee96c71'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Mẫu số hóa đơn' WHERE ID = 'abd14a2b-2d4b-4a0a-8592-e67af30b7ab7'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'aecc00a8-f3f7-45b4-ae83-eb27f837e9b3'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên hàng' WHERE ID = '327d7b5a-6585-46a1-9d73-ef1bbcf3b7f3'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '3d5b9028-defc-43f6-a491-fae0884869de'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Diễn giải thuế', IsVisible = 0 WHERE ID = '2ed4e537-e810-4576-9790-fb364869de49'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên hàng' WHERE ID = 'c8c64ee0-147a-44fd-9cc4-fb7afb541341'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên hàng' WHERE ID = '8fdb1e19-a000-47b2-b5d5-fb9cc937a4ca'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '576ec1fa-8988-4d3a-8632-fe1b8a265743'
UPDATE dbo.TemplateColumn SET ColumnName = N'VATDescription', ColumnCaption = N'Diễn giải thuế' WHERE ID = '2da9b59e-921f-4d15-a3c5-fee456c6dcc0'
UPDATE dbo.TemplateColumn SET VisiblePosition = 14 WHERE ID = '52e7238c-9150-4eff-8a35-ff3e9c83d672'

INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('b69a6dd3-de27-4665-8282-1d1f57706c1f', '790d6840-094e-4783-aa8b-fe0fbd06b487', N'ConfrontInvDate', N'Ngày hóa đơn', N'', 0, 1, 110, 0, 0, 1, 59)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('8e3375cf-f4f1-465c-a528-281d13b2d9ff', '21bf5f6f-da51-47d1-9581-071ed46e2ff2', N'UnitPriceOriginal', N'Đơn giá', N'', 0, 0, 120, 0, 0, 1, 5)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('00f1ae3b-e486-406b-8ef0-2d6d3132804c', '07adda22-3f92-4f6c-8c75-1f0027f07da8', N'InvoiceForm', N'Hình thức hóa đơn', N'', 0, 0, 100, 0, 0, 0, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('92034183-1700-48b6-ac89-3236cb854db5', '21bf5f6f-da51-47d1-9581-071ed46e2ff2', N'DiscountAmountOriginal', N'Tiền chiết khấu', N'', 0, 0, 140, 0, 0, 1, 10)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('1aa71abf-4ef0-481f-aa66-381552a62ee8', '57bf6962-429d-4cd7-8509-3e04daf44e84', N'InvoiceTemplate', N'Mẫu số hóa đơn', NULL, 0, 0, 98, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('96b26a8d-289a-4b0a-b056-4140c2640993', '790d6840-094e-4783-aa8b-fe0fbd06b487', N'No', N'Số chứng từ', N'', 0, 1, 110, 0, 0, 1, 56)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('983998d8-9fbe-418f-9d9d-49184df3e06f', '57bf6962-429d-4cd7-8509-3e04daf44e84', N'CurrencyID', N'Loại tiền', N'', 0, 0, 130, 0, 0, 1, 7)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d0e9aaa1-977b-4196-bbb6-515d8f6ea44b', '5c8f394b-080e-4c92-ae4e-57827fe9565f', N'ConfrontInvNo', N'Số hóa đơn', N'', 0, 1, 110, 0, 0, 1, 39)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('be33eddb-1947-4b52-8bcf-51b63e252e66', '21bf5f6f-da51-47d1-9581-071ed46e2ff2', N'AmountOriginal', N'Thành tiền', NULL, 0, 0, 120, 0, 0, 1, 7)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9b30da2b-d65e-4614-af6d-530d204de0d9', '57bf6962-429d-4cd7-8509-3e04daf44e84', N'InvoiceForm', N'Hình thức hóa đơn', N'', 0, 0, 100, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('49866e5b-08be-4527-90a0-53e30c2d8943', '21bf5f6f-da51-47d1-9581-071ed46e2ff2', N'DiscountAmount', N'Tiền chiết khấu QĐ', NULL, 0, 0, 120, 0, 0, 0, 11)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('8a3812ce-c293-44bf-8f77-55d7532eb0ed', '790d6840-094e-4783-aa8b-fe0fbd06b487', N'Date', N'Ngày chứng từ', N'', 0, 1, 110, 0, 0, 1, 57)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9eec8d5a-730b-4417-a3c3-5727d5d50b98', '21bf5f6f-da51-47d1-9581-071ed46e2ff2', N'VATRate', N'% thuế GTGT', N'', 0, 0, 105, 0, 0, 1, 12)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('41bc9a80-6d77-4a56-a7c1-6274d938e188', '5c8f394b-080e-4c92-ae4e-57827fe9565f', N'ConfrontInvDate', N'Ngày hóa đơn', N'', 0, 1, 110, 0, 0, 1, 40)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9606604d-43e6-4a3b-8c47-73b4b7d62b15', '21bf5f6f-da51-47d1-9581-071ed46e2ff2', N'UnitPrice', N'Đơn giá QĐ', N'', 0, 0, 115, 0, 0, 0, 6)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('a2ca00cf-436f-4c21-8605-7799ae5f54ee', '57bf6962-429d-4cd7-8509-3e04daf44e84', N'InvoiceNo', N'Số hóa đơn', N'', 0, 0, 120, 0, 0, 1, 5)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('7be21c1e-10b5-4564-a6e7-7b24108f07f7', '57bf6962-429d-4cd7-8509-3e04daf44e84', N'InvoiceTypeID', N'Loại hóa đơn', N'', 0, 0, 100, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('546c7a78-5c55-499a-8b8f-7ed63681ef15', '57bf6962-429d-4cd7-8509-3e04daf44e84', N'InvoiceSeries', N'Ký hiệu hóa đơn', N'', 0, 0, 130, 0, 0, 1, 4)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('cd682200-e6f3-4c4f-bbc3-7ef430b9f07b', '57bf6962-429d-4cd7-8509-3e04daf44e84', N'InvoiceDate', N'Ngày hóa đơn', NULL, 0, 0, 98, 0, 0, 1, 6)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('09a527bb-f25e-4e0e-854c-7fd47067fd91', '07adda22-3f92-4f6c-8c75-1f0027f07da8', N'InvoiceTypeID', N'Loại hóa đơn', N'', 0, 0, 100, 0, 0, 0, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9f1e4a0d-768b-4742-870b-829fe1309965', '21bf5f6f-da51-47d1-9581-071ed46e2ff2', N'VATAmountOriginal', N'Tiền thuế GTGT', N'', 0, 0, 130, 0, 0, 1, 13)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('b5dace54-6ddb-42a7-8b8f-8505ef8a2ce6', '21bf5f6f-da51-47d1-9581-071ed46e2ff2', N'Amount', N'Thành tiền QĐ', NULL, 0, 1, 120, 0, 0, 0, 8)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('6aa735fb-9a37-462e-b2dc-870451a68dbc', '21bf5f6f-da51-47d1-9581-071ed46e2ff2', N'Description', N'Tên hàng', N'', 0, 0, 130, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('7745a715-8cc2-49aa-9623-876948356ea0', '57bf6962-429d-4cd7-8509-3e04daf44e84', N'ExchangeRate', N'Tỷ giá', N'', 0, 0, 120, 0, 0, 0, 8)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('65719083-864b-4e29-9ae9-93f70616be3c', '21bf5f6f-da51-47d1-9581-071ed46e2ff2', N'Quantity', N'Số lượng', N'', 0, 0, 120, 0, 0, 1, 4)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('7bf19bcd-829e-4274-a86e-b61392f44561', '21bf5f6f-da51-47d1-9581-071ed46e2ff2', N'VATAmount', N'Tiền thuế GTGT QĐ', N'', 0, 0, 130, 0, 0, 0, 14)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('685d197b-2470-48f2-bd85-b9c41bb578c9', '21bf5f6f-da51-47d1-9581-071ed46e2ff2', N'MaterialGoodsID', N'Mã hàng', N'', 1, 0, 120, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('4b00e380-9678-40f7-ab8b-c95a62b7224b', '15ec5926-4ffc-4c85-a05b-8d474f326e32', N'EmployeeID', N'Nhân viên', NULL, 0, 0, 150, 0, 0, 1, 8)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ce75579c-3790-459b-b0a7-ca10e82755ef', '790d6840-094e-4783-aa8b-fe0fbd06b487', N'ConfrontInvNo', N'Số hóa đơn', N'', 0, 1, 110, 0, 0, 1, 58)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d70976f2-3881-4489-a1de-ca2161b99150', '21bf5f6f-da51-47d1-9581-071ed46e2ff2', N'DiscountRate', N'Tỷ lệ CK', N'', 0, 0, 110, 0, 0, 1, 9)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('0cca35d3-ad8c-4e6c-8cab-fb251bd9226f', '21bf5f6f-da51-47d1-9581-071ed46e2ff2', N'Unit', N'ĐVT', N'Đơn vị tính', 0, 0, 105, 0, 0, 1, 3)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.TemplateDetail SET TabCaption = N'&4. Chứng từ chi phí' WHERE ID = '343fcc24-da42-408f-b529-15836deeea7f'
UPDATE dbo.TemplateDetail SET TabCaption = N'&3. Thống kê' WHERE ID = '72ef4043-1d76-4738-9ada-dea7ef7bb1f8'

INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('21bf5f6f-da51-47d1-9581-071ed46e2ff2', 'ec9ca46d-b8dc-4607-9ba0-2bebb8db4e9d', 1, 0, N'&1. Hàng tiền')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('57bf6962-429d-4cd7-8509-3e04daf44e84', 'ec9ca46d-b8dc-4607-9ba0-2bebb8db4e9d', 0, 100, NULL)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

SET IDENTITY_INSERT dbo.Type ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
INSERT dbo.Type(ID, TypeName, TypeGroupID, Recordable, Searchable, PostType, OrderPriority) VALUES (326, N'Xuất hóa đơn', 326, 0, 1, 1, 0)
GO
SET IDENTITY_INSERT dbo.Type OFF
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

INSERT dbo.TypeGroup(ID, TypeGroupName, DebitAccountID, CreditAccountID) VALUES (326, N'Xuất hóa đơn', NULL, NULL)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.Template WITH NOCHECK
  ADD CONSTRAINT FK_Template_Type FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE CASCADE ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateColumn WITH NOCHECK
  ADD CONSTRAINT FK_TemplateColumn_TemplateDetail FOREIGN KEY (TemplateDetailID) REFERENCES dbo.TemplateDetail(ID) ON DELETE CASCADE ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateDetail WITH NOCHECK
  ADD CONSTRAINT FK_TemplateDetail_Template FOREIGN KEY (TemplateID) REFERENCES dbo.Template(ID) ON DELETE CASCADE ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.Type WITH NOCHECK
  ADD CONSTRAINT FK_Type_TypeGroup FOREIGN KEY (TypeGroupID) REFERENCES dbo.TypeGroup(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153AdjustAnnouncement WITH NOCHECK
  ADD CONSTRAINT FK__TT153Adju__TypeI__0D64F3ED FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153DeletedInvoice WITH NOCHECK
  ADD CONSTRAINT FK__TT153Dele__TypeI__5C37ACAD FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153DestructionInvoice WITH NOCHECK
  ADD CONSTRAINT FK__TT153Dest__TypeI__0A888742 FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153LostInvoice WITH NOCHECK
  ADD CONSTRAINT FK__TT153Lost__TypeI__07AC1A97 FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153PublishInvoice WITH NOCHECK
  ADD CONSTRAINT FK__TT153Publ__TypeI__04CFADEC FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153RegisterInvoice WITH NOCHECK
  ADD CONSTRAINT FK__TT153Regi__TypeI__01F34141 FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.GenCode WITH NOCHECK
  ADD CONSTRAINT FK_GenCode_TypeGroup FOREIGN KEY (TypeGroupID) REFERENCES dbo.TypeGroup(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

GO
ENABLE TRIGGER ALL ON dbo.TemplateColumn
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

GO
IF @@TRANCOUNT>0 COMMIT TRANSACTION
GO

SET NOEXEC OFF
GO
