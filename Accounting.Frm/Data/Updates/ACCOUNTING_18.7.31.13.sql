

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

BEGIN TRANSACTION
GO

DROP TABLE [dbo].[CPPeriodAllocationConfigDetail]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DROP TABLE [dbo].[CPPeriodJobList]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DROP TABLE [dbo].[CPPeriodProductCost]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DROP TABLE [dbo].[CPPeriodAllocationConfig]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DROP TABLE [dbo].[CPPeriodCostSetQuantum]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DROP TABLE [dbo].[CPPeriodProductQuantum]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[Parameters] (
  [ID] [uniqueidentifier] NOT NULL,
  [Code] [nvarchar](50) NOT NULL,
  [Name] [nvarchar](50) NOT NULL,
  [Data] [nvarchar](max) NOT NULL,
  [isActive] [bit] NOT NULL,
  [CreatedDate] [datetime] NOT NULL
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Parameters', 'COLUMN', N'isActive'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[MCAuditDetailMember] (
  [ID] [uniqueidentifier] NOT NULL,
  [MCAuditID] [uniqueidentifier] NOT NULL,
  [AccountingObjectID] [uniqueidentifier] NULL,
  [AccountingObjectName] [nvarchar](128) NULL,
  [AccountingObjectTitle] [nvarchar](182) NULL,
  [DepartmentID] [uniqueidentifier] NULL,
  CONSTRAINT [PK_MCAuditDetailMember] PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[MCAuditDetail] (
  [ID] [uniqueidentifier] NOT NULL,
  [MCAuditID] [uniqueidentifier] NOT NULL,
  [ValueOfMoney] [int] NULL,
  [Quantity] [int] NULL,
  [Description] [nvarchar](225) NULL,
  [Amount] [money] NULL,
  [OrderPriority] [int] IDENTITY,
  CONSTRAINT [PK_MCAuditDetail] PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[MCAudit] (
  [ID] [uniqueidentifier] NOT NULL,
  [BranchID] [uniqueidentifier] NULL,
  [TypeID] [int] NOT NULL,
  [No] [nvarchar](20) NOT NULL,
  [Date] [datetime] NOT NULL,
  [AuditDate] [datetime] NOT NULL,
  [Description] [nvarchar](225) NULL,
  [Summary] [nvarchar](225) NULL,
  [CurrencyID] [nvarchar](3) NULL,
  [ExchangeRate] [decimal](25, 10) NULL,
  [TotalAuditAmount] [money] NULL,
  [TotalBalanceAmount] [money] NULL,
  [DifferAmount] [money] NULL,
  [TemplateID] [uniqueidentifier] NOT NULL,
  CONSTRAINT [PK_MCAudit] PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[CPOPN] (
  [ID] [uniqueidentifier] NOT NULL,
  [BranchID] [uniqueidentifier] NULL,
  [ObjectType] [int] NULL,
  [CostSetID] [uniqueidentifier] NULL,
  [ContractID] [uniqueidentifier] NULL,
  [DirectMaterialAmount] [money] NULL,
  [DirectLaborAmount] [money] NULL,
  [GeneralExpensesAmount] [money] NULL,
  [TotalCostAmount] [money] NULL,
  [AcceptedAmount] [money] NULL,
  [NotAcceptedAmount] [money] NULL,
  [UncompletedAccount] [nvarchar](225) NULL,
  CONSTRAINT [PK_CPOPM] PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[CPExpenseList] (
  [ID] [uniqueidentifier] NOT NULL,
  [CPPeriodID] [uniqueidentifier] NULL,
  [TypeVoucher] [int] NULL,
  [CostSetID] [uniqueidentifier] NULL,
  [TypeID] [int] NULL,
  [Date] [datetime] NULL,
  [PostedDate] [datetime] NULL,
  [No] [nvarchar](25) NULL,
  [Description] [nvarchar](512) NULL,
  [Amount] [money] NULL,
  [ExpenseItemID] [uniqueidentifier] NULL,
  [ContractID] [uniqueidentifier] NULL,
  CONSTRAINT [PK_CPExpenseList] PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[CPAcceptanceDetail] (
  [ID] [uniqueidentifier] NOT NULL,
  [CPAcceptanceID] [uniqueidentifier] NULL,
  [Description] [nvarchar](512) NULL,
  [DebitAccount] [nvarchar](25) NULL,
  [CreditAccount] [nvarchar](25) NULL,
  [TotalAmount] [money] NULL,
  [RevenueAmount] [money] NULL,
  [Amount] [money] NULL,
  [UnitPrice] [money] NULL,
  [AcceptedRate] [decimal](25, 10) NULL,
  [TotalAcceptedAmount] [money] NULL,
  [ExpenseItemID] [uniqueidentifier] NULL,
  [StatisticsCodeID] [uniqueidentifier] NULL,
  [CPPeriodID] [uniqueidentifier] NULL,
  CONSTRAINT [PK_CPAcceptanceDetail] PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[CPAcceptance] (
  [ID] [uniqueidentifier] NOT NULL,
  [BranchID] [uniqueidentifier] NULL,
  [TypeID] [int] NULL,
  [Date] [datetime] NULL,
  [PostedDate] [datetime] NULL,
  [No] [nvarchar](25) NULL,
  [Description] [nvarchar](512) NULL,
  [CPPeriodID] [uniqueidentifier] NULL,
  [TotalAmount] [money] NULL,
  [TotalAmountOriginal] [money] NULL,
  [UnitPrice] [money] NULL,
  CONSTRAINT [PK_CPAcceptance] PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153RegisterInvoice]
  ADD FOREIGN KEY ([TypeID]) REFERENCES [dbo].[Type] ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153PublishInvoice]
  ALTER
    COLUMN [BranchID] [uniqueidentifier]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153PublishInvoice]
  ADD FOREIGN KEY ([TypeID]) REFERENCES [dbo].[Type] ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153LostInvoice]
  ADD [AttachFileName] [nvarchar](512) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153LostInvoice]
  ADD [AttachFileContent] [varbinary](max) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153LostInvoice]
  ADD FOREIGN KEY ([TypeID]) REFERENCES [dbo].[Type] ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153DestructionInvoice]
  ADD FOREIGN KEY ([TypeID]) REFERENCES [dbo].[Type] ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153AdjustAnnouncement]
  ADD FOREIGN KEY ([TypeID]) REFERENCES [dbo].[Type] ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
CREATE TRIGGER [trgInsteadOfOperation] ON [dbo].[TemplateColumn] 
INSTEAD OF UPDATE
AS
	begin
		print N'Vui lòng xem lại trường IsReadOnly'  
		rollback;
	end
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153DestructionInvoiceDetail]
  ADD [TT153PublishInvoiceDetailID] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [InvoiceTemplate] [nvarchar](25) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIAllocationAllocated]
  ADD [CostSetID] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[Sys_User]
  ALTER
    COLUMN [WorkPhone] [nvarchar](50)
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[Sys_User]
  ALTER
    COLUMN [HomePhone] [nvarchar](50)
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[Sys_User]
  ALTER
    COLUMN [MobilePhone] [nvarchar](50)
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[Sys_User]
  ALTER
    COLUMN [Fax] [nvarchar](50)
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Sys_User', 'COLUMN', N'WorkPhone'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Sys_User', 'COLUMN', N'HomePhone'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Sys_User', 'COLUMN', N'MobilePhone'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Sys_User', 'COLUMN', N'Fax'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAReturn]
  ADD [InvoiceForm] [int] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAReturn]
  ADD [InvoiceTemplate] [nvarchar](25) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAInvoice]
  ADD [ModifiedDate] [datetime] NULL CONSTRAINT [DF__SAInvoice__Modif__5F54107A] DEFAULT (getdate())
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAInvoice]
  ADD [InvoiceTemplate] [nvarchar](25) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SAInvoice', 'COLUMN', N'ModifiedDate'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSTimeSheetSummaryDetail]
  ALTER
    COLUMN [WorkAllDay] [decimal](25, 4)
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSTimeSheetSummaryDetail]
  ALTER
    COLUMN [WorkHalfADay] [decimal](25, 4)
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSTimeSheetSummaryDetail]
  ALTER
    COLUMN [TotalOverTime] [decimal](25, 4)
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSTimeSheetSummaryDetail]
  ALTER
    COLUMN [Total] [decimal](25, 4)
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheetDetail]
  ALTER
    COLUMN [SalaryCoefficient] [money]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheetDetail]
  ALTER
    COLUMN [BasicWage] [money]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheetDetail]
  ALTER
    COLUMN [WorkingDayUnitPrice] [money]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheetDetail]
  ALTER
    COLUMN [WorkingHourUnitPrice] [money]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheetDetail]
  ALTER
    COLUMN [AgreementSalary] [money]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheetDetail]
  ALTER
    COLUMN [NumberOfPaidWorkingDayTimeSheet] [money]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheetDetail]
  ALTER
    COLUMN [PaidWorkingDayAmount] [money]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheetDetail]
  ALTER
    COLUMN [NumberOfNonWorkingDayTimeSheet] [money]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheetDetail]
  ALTER
    COLUMN [NonWorkingDayAmount] [money]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheetDetail]
  ALTER
    COLUMN [ProductUnitPrice] [money]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheetDetail]
  ALTER
    COLUMN [TotalProduct] [money]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheetDetail]
  ALTER
    COLUMN [ProductAmount] [money]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheetDetail]
  ALTER
    COLUMN [ProductAmountOriginal] [money]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheetDetail]
  ALTER
    COLUMN [TotalOverTime] [money]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheetDetail]
  ALTER
    COLUMN [OverTimeAmount] [money]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheetDetail]
  ALTER
    COLUMN [PayrollFundAllowance] [money]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheetDetail]
  ALTER
    COLUMN [OtherAllowance] [money]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheetDetail]
  ALTER
    COLUMN [TotalAmount] [money]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheetDetail]
  ALTER
    COLUMN [TemporaryAmount] [money]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheetDetail]
  ALTER
    COLUMN [EmployeeSocityInsuranceAmount] [money]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheetDetail]
  ALTER
    COLUMN [EmployeeAccidentInsuranceAmount] [money]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheetDetail]
  ALTER
    COLUMN [EmployeeMedicalInsuranceAmount] [money]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheetDetail]
  ALTER
    COLUMN [EmployeeUnEmployeeInsuranceAmount] [money]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheetDetail]
  ALTER
    COLUMN [EmployeeTradeUnionInsuranceAmount] [money]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheetDetail]
  ALTER
    COLUMN [IncomeTaxAmount] [money]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheetDetail]
  ALTER
    COLUMN [SumOfDeductionAmount] [money]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheetDetail]
  ALTER
    COLUMN [TotalPersonalTaxIncomeAmount] [money]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheetDetail]
  ALTER
    COLUMN [ReduceSelfTaxAmount] [money]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheetDetail]
  ALTER
    COLUMN [ReduceDependTaxAmount] [money]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheetDetail]
  ALTER
    COLUMN [IncomeForTaxCalcuation] [money]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheetDetail]
  ALTER
    COLUMN [InsuranceSalary] [money]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheetDetail]
  ALTER
    COLUMN [NetAmount] [money]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheetDetail]
  ALTER
    COLUMN [CompanySocityInsuranceAmount] [money]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheetDetail]
  ALTER
    COLUMN [CompanytAccidentInsuranceAmount] [money]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheetDetail]
  ALTER
    COLUMN [CompanyMedicalInsuranceAmount] [money]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheetDetail]
  ALTER
    COLUMN [CompanyUnEmployeeInsuranceAmount] [money]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheetDetail]
  ALTER
    COLUMN [CompanyTradeUnionInsuranceAmount] [money]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheetDetail]
  ADD [NetAmountOriginal] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheet]
  ADD [TotalNetAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheet]
  ADD [TotalNetAmountOriginal] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheet]
  ADD [TotalTemporaryAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PPServiceDetail]
  ADD [InvoiceTemplate] [nvarchar](25) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PPInvoiceDetail]
  ADD [ExchangeRate] [decimal](25, 10) NULL DEFAULT (NULL)
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PPInvoiceDetail]
  ADD [TaxExchangeRate] [decimal](25, 10) NULL DEFAULT (NULL)
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PPInvoiceDetail]
  ADD [InvoiceTemplate] [nvarchar](25) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'PPInvoiceDetail', 'COLUMN', N'ExchangeRate'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'PPInvoiceDetail', 'COLUMN', N'TaxExchangeRate'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PPDiscountReturn]
  ADD [InvoiceTemplate] [nvarchar](25) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MCReceiptDetailTax]
  ADD [InvoiceTemplate] [nvarchar](25) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MCReceipt]
  ADD [AuditID] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MCPaymentDetailTax]
  ADD [InvoiceTemplate] [nvarchar](25) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MCPayment]
  ADD [AuditID] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sp_refreshview '[dbo].[ViewVouchersCloseBook]'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MBTellerPaperDetailTax]
  ADD [InvoiceTemplate] [nvarchar](25) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MBDepositDetailTax]
  ADD [InvoiceTemplate] [nvarchar](25) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MaterialQuantum]
  ALTER
    COLUMN [MaterialQuantumCode] [nvarchar](25)
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MaterialQuantum]
  ALTER
    COLUMN [MaterialQuantumName] [nvarchar](512)
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MaterialQuantum]
  ADD [CostSetID] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MaterialQuantum]
  ADD [CostSetName] [nvarchar](512) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MaterialQuantumDetail]
  ADD [Amount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153Report]
  DROP CONSTRAINT [FK__TT153Repo__Invoi__3CBF0154]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153Report]
  ADD [InvoiceTemplate] [nvarchar](25) NOT NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153Report] WITH NOCHECK
  ADD CONSTRAINT [FK__TT153Repo__Invoi__473C8FC7] FOREIGN KEY ([InvoiceTypeID]) REFERENCES [dbo].[InvoiceType] ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153Report]
  ADD FOREIGN KEY ([InvoiceTypeID]) REFERENCES [dbo].[InvoiceType] ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153PublishInvoiceDetail]
  DROP CONSTRAINT [FK__TT153Publ__Invoi__436BFEE3]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153PublishInvoiceDetail]
  ADD [TT153RegisterInvoiceDetailID] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153PublishInvoiceDetail] WITH NOCHECK
  ADD CONSTRAINT [FK__TT153Publ__Invoi__4DE98D56] FOREIGN KEY ([InvoiceTypeID]) REFERENCES [dbo].[InvoiceType] ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153PublishInvoiceDetail]
  ADD FOREIGN KEY ([InvoiceTypeID]) REFERENCES [dbo].[InvoiceType] ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153LostInvoiceDetail]
  DROP CONSTRAINT [FK__TT153Lost__Invoi__4830B400]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153LostInvoiceDetail]
  ADD [TT153PublishInvoiceDetailID] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153LostInvoiceDetail] WITH NOCHECK
  ADD CONSTRAINT [FK__TT153Lost__Invoi__52AE4273] FOREIGN KEY ([InvoiceTypeID]) REFERENCES [dbo].[InvoiceType] ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153LostInvoiceDetail]
  ADD FOREIGN KEY ([InvoiceTypeID]) REFERENCES [dbo].[InvoiceType] ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153DeletedInvoice]
  DROP CONSTRAINT [FK__TT153Dele__Invoi__4CF5691D]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153DeletedInvoice] WITH NOCHECK
  ADD CONSTRAINT [FK__TT153Dele__Invoi__5772F790] FOREIGN KEY ([InvoiceTypeID]) REFERENCES [dbo].[InvoiceType] ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153DeletedInvoice]
  ADD FOREIGN KEY ([InvoiceTypeID]) REFERENCES [dbo].[InvoiceType] ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153AdjustAnnouncementDetailListInvoice]
  DROP CONSTRAINT [FK__TT153Adju__Invoi__51BA1E3A]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153AdjustAnnouncementDetailListInvoice]
  ADD [TT153PublishInvoiceDetailID] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153AdjustAnnouncementDetailListInvoice]
  ADD [TT153ReportID] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153AdjustAnnouncementDetailListInvoice] WITH NOCHECK
  ADD CONSTRAINT [FK__TT153Adju__Invoi__5C37ACAD] FOREIGN KEY ([InvoiceTypeID]) REFERENCES [dbo].[InvoiceType] ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153AdjustAnnouncementDetailListInvoice]
  ADD FOREIGN KEY ([InvoiceTypeID]) REFERENCES [dbo].[InvoiceType] ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[GOtherVoucherDetailTax]
  ADD [InvoiceTemplate] [nvarchar](25) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER FUNCTION [dbo].[Func_GetB02_DN]
    (
      @BranchID UNIQUEIDENTIFIER ,
      @IncludeDependentBranch BIT ,
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @PrevFromDate DATETIME ,
      @PrevToDate DATETIME
    )
RETURNS @Result TABLE
    (
      ItemID UNIQUEIDENTIFIER ,
      ItemCode NVARCHAR(25) ,
      ItemName NVARCHAR(255) ,
      ItemNameEnglish NVARCHAR(255) ,
      ItemIndex INT ,
      Description NVARCHAR(255) ,
      FormulaType INT ,
      FormulaFrontEnd NVARCHAR(MAX) ,
      Formula XML ,
      Hidden BIT ,
      IsBold BIT ,
      IsItalic BIT
      ,
      Amount DECIMAL(25, 4) ,
      PrevAmount DECIMAL(25, 4)
    )
AS 
    BEGIN

        DECLARE @AccountingSystem INT	
            SET @AccountingSystem = 48
        DECLARE @ReportID NVARCHAR(100)
        SET @ReportID = '2'/*báo cáo kết quả HĐ kinh doanh*/
	
        DECLARE @tblItem TABLE
            (
              ItemID UNIQUEIDENTIFIER ,
              ItemName NVARCHAR(255) ,
              OperationSign INT ,
              OperandString NVARCHAR(255) ,
              AccountNumber VARCHAR(25) ,
              CorrespondingAccountNumber VARCHAR(25) ,
              IsDetailGreaterThanZero BIT
	      )
	
        INSERT  @tblItem
                SELECT  ItemID ,
                        ItemName ,
                        x.r.value('@OperationSign', 'INT') ,
                        x.r.value('@OperandString', 'nvarchar(255)') ,
                        x.r.value('@AccountNumber', 'nvarchar(25)') + '%' ,
                        CASE WHEN x.r.value('@CorrespondingAccountNumber',
                                            'nvarchar(25)') <> ''
                             THEN x.r.value('@CorrespondingAccountNumber',
                                            'nvarchar(25)') + '%'
                             ELSE ''
                        END ,
                        CASE WHEN FormulaType = 0 THEN CAST(0 AS BIT)
                             ELSE CAST(1 AS BIT)
                        END
                FROM    FRTemplate
                        CROSS APPLY Formula.nodes('/root/DetailFormula') AS x ( r )
                WHERE   AccountingSystem = @AccountingSystem
                        AND ReportID = @ReportID
                        AND ( FormulaType = 0/*chỉ tiêu chi tiết*/
                              OR FormulaType = 2/*chỉ tiêu chi tiết chỉ được lấy số liệu khi kết quả>0*/
                            )
                        AND Formula IS NOT NULL
	
        DECLARE @Balance TABLE
            (
              AccountNumber NVARCHAR(25) ,
              CorrespondingAccountNumber NVARCHAR(25) ,
              PostedDate DATETIME ,
              CreditAmount DECIMAL(25, 4) ,
              DebitAmount DECIMAL(25, 4) ,
              CreditAmountDetailBy DECIMAL(25, 4) ,
              DebitAmountDetailBy DECIMAL(25, 4) ,
              IsDetailBy BIT,
              /*add by hoant 16.09.2016 theo cr 116741*/
                CreditAmountByBussinessType0 DECIMAL(25, 4) ,/*Có Chi tiết theo loại  0-Chiết khấu thương mai*/
               CreditAmountByBussinessType1 DECIMAL(25, 4) ,/*Có Chi tiết theo loại  1 - Giảm giá hàng bán*/
               CreditAmountByBussinessType2 DECIMAL(25, 4) ,/*Có Chi tiết theo loại   2- trả lại hàng bán*/
              DebitAmountByBussinessType0 DECIMAL(25, 4) ,/*Nợ Chi tiết theo loại  0-Chiết khấu thương mai*/
              DebitAmountByBussinessType1 DECIMAL(25, 4) ,/*Nợ Chi tiết theo loại  1 - Giảm giá hàng bán*/
              DebitAmountByBussinessType2 DECIMAL(25, 4) /*Nợ Chi tiết theo loại   2- trả lại hàng bán*/
            )			
/*
1. Doanh thu bán hàng và cung cấp dịch vụ

PS Có TK 511 (không kể PSĐƯ N911/C511, không kể các nghiệp vụ: Chiết khấu thương mại (bán hàng),

 Giảm giá hàng bán, Trả lại hàng bán)
  – PS Nợ TK 511 (không kể PSĐƯ N511/911, không kể các nghiệp vụ: Chiết khấu thương mại (bán hàng), 
  Giảm giá hàng bán, Trả lại hàng bán)
*/
		
        INSERT  INTO @Balance
                SELECT  GL.Account ,
                        GL.AccountCorresponding ,
                        GL.PostedDate ,
                        SUM(ISNULL(CreditAmount, 0)) AS CreditAmount ,
                        SUM(ISNULL(DebitAmount, 0)) AS DebitAmount ,
                        0 AS CreditAmountDetailBy ,
                        0 AS DebitAmountDetailBy ,
                        0,
                        SUM(CASE WHEN (GL.TypeID in ('320','321','322','323','324','325','350','360') and gl.Account like '511%')
                                 THEN ISNULL(CreditAmount, 0)
                                 ELSE 0
                            END) AS CreditAmountByBussinessType0 ,
                        SUM(CASE WHEN GL.TypeID =340
                                 THEN ISNULL(CreditAmount, 0)
                                 ELSE 0
                            END) AS CreditAmountByBussinessType1 
                            ,
                        SUM(CASE WHEN GL.TypeID =330
                                 THEN ISNULL(CreditAmount, 0)
                                 ELSE 0
                            END) AS CreditAmountByBussinessType2 ,
                        SUM(CASE WHEN (GL.TypeID in ('320','321','322','323','324','325','350','360') and gl.Account like '511%')
                                 THEN ISNULL(DebitAmount, 0)
                                 ELSE 0
                            END) AS DebitAmountByBussinessType0
                            ,
                        SUM(CASE WHEN GL.TypeID =340
                                 THEN ISNULL(DebitAmount, 0)
                                 ELSE 0
                            END) AS DebitAmountByBussinessType1
                            ,
                        SUM(CASE WHEN GL.TypeID =330
                                 THEN ISNULL(DebitAmount, 0)
                                 ELSE 0
                            END) AS DebitAmountByBussinessType2
                FROM    dbo.GeneralLedger GL
                WHERE   PostedDate BETWEEN @PrevFromDate AND @ToDate
                GROUP BY GL.Account ,
                        GL.AccountCorresponding,
                        GL.PostedDate                     
        OPTION  ( RECOMPILE )


        DECLARE @tblMasterDetail TABLE
            (
              ItemID UNIQUEIDENTIFIER ,
              DetailItemID UNIQUEIDENTIFIER ,
              OperationSign INT ,
              Grade INT ,
              DebitAmount DECIMAL(25, 4) ,
              PrevDebitAmount DECIMAL(25, 4)
            )	
	
	
	/*
	PhatsinhCO(511) - PhatsinhDU(911/511) + PhatsinhCO_ChitietChietKhauThuongmai(511)
	 + PhatsinhCO_ChitietGiamgiaHangBan(511) + PhatsinhCO_ChitietTralaiHangBan(511) +
	  PhatsinhNO(511) - PhatsinhDU(511/911) - PhatsinhNO_ChitietChietKhauThuongmai(511) - PhatsinhNO_ChitietGiamgiaHangBan(511) - PhatsinhNO_ChitietTralaiHangBan(511)
	*/
        INSERT  INTO @tblMasterDetail
                SELECT  I.ItemID ,
                        NULL ,
                        1 ,
                        -1
                        ,
                        CASE WHEN I.IsDetailGreaterThanZero = 0
                                  OR ( SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate
                                                THEN ( CASE WHEN I.OperandString = 'PhatsinhCO'
                                                            THEN GL.CreditAmount
                                                            WHEN I.OperandString IN (
                                                              'PhatsinhNO',
                                                              'PhatsinhDU' )
                                                            THEN GL.DebitAmount
                                                            /*add by hoant 16.09.2016 theo cr 116741*/
                                                            WHEN I.OperandString = 'PhatsinhNO_ChitietChietKhauThuongmai' THEN 
                                                              GL.DebitAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietGiamgiaHangBan' THEN 
                                                              GL.DebitAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietTralaiHangBan' THEN 
                                                              GL.DebitAmountByBussinessType2
                                                            WHEN I.OperandString = 'PhatsinhCO_ChitietChietKhauThuongmai' THEN 
                                                              GL.CreditAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietGiamgiaHangBan' THEN 
                                                              GL.CreditAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietTralaiHangBan' THEN 
                                                              GL.CreditAmountByBussinessType2
                                                             /*end by hoant 16.09.2016 theo cr 116741*/
                                                              
                                                            WHEN I.OperandString IN (
                                                              'PhatsinhCO_ChiTietThanhlyTSCD_BDSDT' )
                                                              AND GL.IsDetailBy = 1
                                                            THEN GL.CreditAmountDetailBy
                                                            WHEN I.OperandString IN (
                                                              'PhatsinhNO_ChiTietThanhlyTSCD_BDSDT' )
                                                              AND GL.IsDetailBy = 1
                                                            THEN GL.DebitAmountDetailBy
                                                            ELSE 0
                                                       END ) * I.OperationSign
                                                ELSE 0
                                           END) ) > 0
                             THEN ( SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate
                                             THEN ( CASE WHEN I.OperandString = 'PhatsinhCO'
                                                         THEN GL.CreditAmount
                                                         WHEN I.OperandString IN (
                                                              'PhatsinhNO',
                                                              'PhatsinhDU' )
                                                         THEN GL.DebitAmount
                                                          /*add by hoant 16.09.2016 theo cr 116741*/
                                                            WHEN I.OperandString = 'PhatsinhNO_ChitietChietKhauThuongmai' THEN 
                                                              GL.DebitAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietGiamgiaHangBan' THEN 
                                                              GL.DebitAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietTralaiHangBan' THEN 
                                                              GL.DebitAmountByBussinessType2
                                                            WHEN I.OperandString = 'PhatsinhCO_ChitietChietKhauThuongmai' THEN 
                                                              GL.CreditAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietGiamgiaHangBan' THEN 
                                                              GL.CreditAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietTralaiHangBan' THEN 
                                                              GL.CreditAmountByBussinessType2
                                                             /*end by hoant 16.09.2016 theo cr 116741*/
                                                              
                                                         WHEN I.OperandString IN (
                                                              'PhatsinhCO_ChiTietThanhlyTSCD_BDSDT' )
                                                              AND GL.IsDetailBy = 1
                                                         THEN GL.CreditAmountDetailBy
                                                         WHEN I.OperandString IN (
                                                              'PhatsinhNO_ChiTietThanhlyTSCD_BDSDT' )
                                                              AND GL.IsDetailBy = 1
                                                         THEN GL.DebitAmountDetailBy
                                                         ELSE 0
                                                    END ) * I.OperationSign
                                             ELSE 0
                                        END) )
                             ELSE 0
                        END AS DebitAmount ,
                        CASE WHEN I.IsDetailGreaterThanZero = 0
                                  OR ( SUM(CASE WHEN GL.PostedDate BETWEEN @PrevFromDate
                                                              AND
                                                              @PrevToDate
                                                THEN CASE WHEN I.OperandString = 'PhatsinhCO'
                                                          THEN GL.CreditAmount
                                                          WHEN I.OperandString IN (
                                                              'PhatsinhNO',
                                                              'PhatsinhDU' )
                                                          THEN GL.DebitAmount
                                                            /*add by hoant 16.09.2016 theo cr 116741*/
                                                            WHEN I.OperandString = 'PhatsinhNO_ChitietChietKhauThuongmai' THEN 
                                                              GL.DebitAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietGiamgiaHangBan' THEN 
                                                              GL.DebitAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietTralaiHangBan' THEN 
                                                              GL.DebitAmountByBussinessType2
                                                            WHEN I.OperandString = 'PhatsinhCO_ChitietChietKhauThuongmai' THEN 
                                                              GL.CreditAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietGiamgiaHangBan' THEN 
                                                              GL.CreditAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietTralaiHangBan' THEN 
                                                              GL.CreditAmountByBussinessType2
                                                             /*end by hoant 16.09.2016 theo cr 116741*/
                                                              
                                                              
                                                          WHEN I.OperandString IN (
                                                              'PhatsinhCO_ChiTietThanhlyTSCD_BDSDT' )
                                                              AND GL.IsDetailBy = 1
                                                          THEN GL.CreditAmountDetailBy
                                                          WHEN I.OperandString IN (
                                                              'PhatsinhNO_ChiTietThanhlyTSCD_BDSDT' )
                                                              AND GL.IsDetailBy = 1
                                                          THEN GL.DebitAmountDetailBy
                                                          ELSE 0
                                                     END * I.OperationSign
                                                ELSE 0
                                           END) ) > 0
                             THEN SUM(CASE WHEN GL.PostedDate BETWEEN @PrevFromDate
                                                              AND
                                                              @PrevToDate
                                           THEN CASE WHEN I.OperandString = 'PhatsinhCO'
                                                     THEN GL.CreditAmount
                                                     WHEN I.OperandString IN (
                                                          'PhatsinhNO',
                                                          'PhatsinhDU' )
                                                     THEN GL.DebitAmount
                                                     /*add by hoant 16.09.2016 theo cr 116741*/
                                                            WHEN I.OperandString = 'PhatsinhNO_ChitietChietKhauThuongmai' THEN 
                                                              GL.DebitAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietGiamgiaHangBan' THEN 
                                                              GL.DebitAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietTralaiHangBan' THEN 
                                                              GL.DebitAmountByBussinessType2
                                                            WHEN I.OperandString = 'PhatsinhCO_ChitietChietKhauThuongmai' THEN 
                                                              GL.CreditAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietGiamgiaHangBan' THEN 
                                                              GL.CreditAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietTralaiHangBan' THEN 
                                                              GL.CreditAmountByBussinessType2
                                                             /*end by hoant 16.09.2016 theo cr 116741*/
                                                              
                                                              
                                                     WHEN I.OperandString IN (
                                                          'PhatsinhCO_ChiTietThanhlyTSCD_BDSDT' )
                                                          AND GL.IsDetailBy = 1
                                                     THEN GL.CreditAmountDetailBy
                                                     WHEN I.OperandString IN (
                                                          'PhatsinhNO_ChiTietThanhlyTSCD_BDSDT' )
                                                          AND GL.IsDetailBy = 1
                                                     THEN GL.DebitAmountDetailBy
                                                     ELSE 0
                                                END * I.OperationSign
                                           ELSE 0
                                      END)
                             ELSE 0
                        END AS DebitAmount
                FROM    @tblItem I
                        INNER JOIN @Balance AS GL ON ( GL.AccountNumber LIKE I.AccountNumber )
                                                     AND ( I.OperandString <> 'PhatsinhDU'
                                                           OR ( I.OperandString = 'PhatsinhDU'
                                                              AND GL.CorrespondingAccountNumber LIKE I.CorrespondingAccountNumber
                                                              )
                                                         )
                GROUP BY I.ItemID ,
                        I.IsDetailGreaterThanZero
        OPTION  ( RECOMPILE )
			
        INSERT  @tblMasterDetail
                SELECT  ItemID ,
                        x.r.value('@ItemID', 'UNIQUEIDENTIFIER') ,
                        x.r.value('@OperationSign', 'INT') ,
                        0 ,
                        0.0 ,
                        0.0
                FROM    FRTemplate
                        CROSS APPLY Formula.nodes('/root/MasterFormula') AS x ( r )
                WHERE   AccountingSystem = @AccountingSystem
                        AND ReportID = @ReportID
                        AND FormulaType = 1
                        AND Formula IS NOT NULL 
	
	

	;
        WITH    V ( ItemID, DetailItemID, DebitAmount, PrevDebitAmount, OperationSign )
                  AS ( SELECT   ItemID ,
                                DetailItemID ,
                                DebitAmount ,
                                PrevDebitAmount ,
                                OperationSign
                       FROM     @tblMasterDetail
                       WHERE    Grade = -1
                       UNION ALL
                       SELECT   B.ItemID ,
                                B.DetailItemID ,
                                V.DebitAmount ,
                                V.PrevDebitAmount ,
                                B.OperationSign * V.OperationSign AS OperationSign
                       FROM     @tblMasterDetail B ,
                                V
                       WHERE    B.DetailItemID = V.ItemID
                     )
	INSERT    @Result
                SELECT  FR.ItemID ,
                        FR.ItemCode ,
                        FR.ItemName ,
                        FR.ItemNameEnglish ,
                        FR.ItemIndex ,
                        FR.Description ,
                        FR.FormulaType ,
                        CASE WHEN FR.FormulaType = 1 THEN FR.FormulaFrontEnd
                             ELSE ''
                        END AS FormulaFrontEnd ,
                        CASE WHEN FR.FormulaType = 1 THEN FR.Formula
                             ELSE NULL
                        END AS Formula ,
                        FR.Hidden ,
                        FR.IsBold ,
                        FR.IsItalic ,
                        ISNULL(X.Amount, 0) AS Amount ,
                        ISNULL(X.PrevAmount, 0) AS PrevAmount
                FROM    ( SELECT    V.ItemID ,
                                    SUM(V.OperationSign * V.DebitAmount) AS Amount ,
                                    SUM(V.OperationSign * V.PrevDebitAmount) AS PrevAmount
                          FROM      V
                          GROUP BY  ItemID
                        ) AS X
                        RIGHT JOIN dbo.FRTemplate FR ON FR.ItemID = X.ItemID
                WHERE   AccountingSystem = @AccountingSystem
                        AND ReportID = @ReportID
                ORDER BY ItemIndex
	
        RETURN 
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[FAIncrementDetail]
  ADD [InvoiceTemplate] [nvarchar](25) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[ExpenseItem]
  ADD [ExpenseType] [int] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[CPPeriod]
  DROP COLUMN [Description]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[CPPeriod]
  DROP COLUMN [JobCostMethod]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*

*/
ALTER PROCEDURE [dbo].[Proc_BA_GetOverBalanceBook]
    @BranchID UNIQUEIDENTIFIER = NULL ,
    @AccountNumber NVARCHAR(25) = NULL ,
    @BankAccountID UNIQUEIDENTIFIER = NULL ,
    @CurrencyID NVARCHAR(3) = NULL ,
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @IsWorkingWithManagementBook BIT ,
    @IncludeDependentBranch BIT
AS 
    SET NOCOUNT ON		

    DECLARE @AccountNumberPercent NVARCHAR(26)
    SET @AccountNumberPercent = @AccountNumber + '%'
    SELECT  ROW_NUMBER() OVER ( ORDER BY B.BankName, BA.BankAccount ) AS RowNum ,
            BA.ID,
            BA.BankAccount,
            B.BankName,
			BA.BankBranchName ,
            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmountOriginal- GL.CreditAmountOriginal ELSE 0.0 END) AS OpenAmountOC ,
            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount ELSE 0.0 END) AS OpenAmount ,
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.DebitAmountOriginal ELSE 0.0 END) AS DebitAmountOC ,
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN  GL.DebitAmount ELSE 0.0 END) AS DebitAmount ,
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.CreditAmountOriginal ELSE 0.0 END) AS CreditAmountOC ,
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.CreditAmount ELSE 0.0 END) AS CreditAmount ,
            SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal) AS CloseAmountOC ,
            SUM(GL.DebitAmount - GL.CreditAmount) AS CloseAmount
    FROM    dbo.GeneralLedger GL
            INNER JOIN dbo.BankAccountDetail BA ON GL.BankAccountDetailID = BA.ID          
            INNER JOIN dbo.Bank B ON BA.BankID =  B.ID
    WHERE   GL.PostedDate <= @ToDate
            AND ( GL.Account LIKE @AccountNumberPercent )
            AND ( GL.BankAccountDetailID = @BankAccountID
                  OR @BankAccountID IS NULL
                )
            AND ( @CurrencyID IS NULL
                  OR GL.CurrencyID = @CurrencyID
                )
            
    GROUP BY BA.ID, 
			BA.BankAccount ,
            B.BankName,BA.BankBranchName
    HAVING
			SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmountOriginal- GL.CreditAmountOriginal ELSE 0.0 END)<>0 OR 
            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount ELSE 0.0 END)<>0 OR 
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.DebitAmountOriginal ELSE 0.0 END)<>0 OR 
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN  GL.DebitAmount ELSE 0.0 END)<>0 OR 
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.CreditAmountOriginal ELSE 0.0 END)<>0 OR 
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.CreditAmount ELSE 0.0 END)<>0 OR 
            SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal)<>0 OR 
            SUM(GL.DebitAmount - GL.CreditAmount)<>0
            
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_BA_GetBookDepositListDetail_Bck]
    @BranchID UNIQUEIDENTIFIER = NULL ,
    @AccountNumber NVARCHAR(25) = NULL ,
    @BankAccountID UNIQUEIDENTIFIER = NULL ,
    @CurrencyID NVARCHAR(3) = NULL ,
    @FromDate DATETIME ,
    @ToDate DATETIME 
AS 
    SET NOCOUNT ON		

    DECLARE @AccountNumberPercent NVARCHAR(26)
    SET @AccountNumberPercent = @AccountNumber + '%'
    SELECT  ROW_NUMBER() OVER ( ORDER BY B.BankName, BA.BankAccount ) AS RowNum ,
            BA.ID,
            BA.BankAccount,
            B.BankName,
            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmountOriginal- GL.CreditAmountOriginal ELSE 0.0 END) AS OpenAmountOC ,
            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount ELSE 0.0 END) AS OpenAmount ,
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.DebitAmountOriginal ELSE 0.0 END) AS DebitAmountOC ,
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN  GL.DebitAmount ELSE 0.0 END) AS DebitAmount ,
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.CreditAmountOriginal ELSE 0.0 END) AS CreditAmountOC ,
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.CreditAmount ELSE 0.0 END) AS CreditAmount ,
            SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal) AS CloseAmountOC ,
            SUM(GL.DebitAmount - GL.CreditAmount) AS CloseAmount
    FROM    dbo.GeneralLedger GL
            INNER JOIN dbo.BankAccountDetail BA ON GL.BankAccountDetailID = BA.ID          
            INNER JOIN dbo.Bank B ON BA.BankID =  B.ID
    WHERE   GL.PostedDate <= @ToDate
            AND ( GL.Account LIKE @AccountNumberPercent )
            AND ( GL.BankAccountDetailID = @BankAccountID
                  OR @BankAccountID IS NULL
                )
            AND ( @CurrencyID IS NULL
                  OR GL.CurrencyID = @CurrencyID
                )
            
    GROUP BY BA.ID, 
			BA.BankAccount ,
            B.BankName
    HAVING
			SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmountOriginal- GL.CreditAmountOriginal ELSE 0.0 END)<>0 OR 
            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount ELSE 0.0 END)<>0 OR 
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.DebitAmountOriginal ELSE 0.0 END)<>0 OR 
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN  GL.DebitAmount ELSE 0.0 END)<>0 OR 
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.CreditAmountOriginal ELSE 0.0 END)<>0 OR 
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.CreditAmount ELSE 0.0 END)<>0 OR 
            SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal)<>0 OR 
            SUM(GL.DebitAmount - GL.CreditAmount)<>0
            
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[AccountingObject]
  ADD UNIQUE ([AccountingObjectCode])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

/*=================================================================================================
 * Created By:		thaivh	
 * Created Date:	11/7/2018
 * Description:		Lay du lieu cho uy nhiem chi
===================================================================================================== */
CREATE PROCEDURE [dbo].[Proc_Accreditative]
    @BankCode nvarchar(25)
AS

    BEGIN
        SET NOCOUNT ON;
   		                         
		select distinct a.ID, a.No, a.Date, a.BankName, a.AccountingObjectAddress, h.BankAccount as AccountingObjectBankAccount, a.AccountingObjectBankName,
		a.AccountingObjectName, c.BankAccount, c.BankBranchName, d.Tel, e.ID as CurrencyName, Sum(b.AmountOriginal) as TotalAmount, a.Reason, c.BankBranchName as Branch
		from MBTellerPaper a inner join MBTellerPaperDetail b on a.ID = b.MBTellerPaperID
		left join BankAccountDetail c on a.BankAccountDetailID = c.ID 
		left join AccountingObject d on a.AccountingObjectID = d.ID
		left join Currency e on a.CurrencyID = e.ID 		
		left join Bank f on c.BankID = f.ID
		left join AccountingObjectBankAccount h on a.AccountingObjectBankAccount = h.ID
		where f.BankCode = @BankCode and (b.CreditAccount = '1121' or b.CreditAccount = '1122')
		group by a.ID, a.No, a.Date, a.BankName, a.AccountingObjectAddress, h.BankAccount, a.AccountingObjectBankName,
		a.AccountingObjectName, c.BankAccount, c.BankBranchName, d.Tel, e.ID, a.Reason
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*

*/
ALTER PROCEDURE [dbo].[Proc_SA_GetSalesDiaryBook]
    @FromDate DATETIME ,
    @ToDate DATETIME,
    @IsDisplayNotReceiptOnly BIT
AS 
    BEGIN 
		
        DECLARE @listRefType NVARCHAR(MAX)
        IF @IsDisplayNotReceiptOnly = 1 
            SET @listRefType = '3530,3532,3534,3536,'  
        ELSE 
            SET @listRefType = '3530,3531,3532,3534,3535,3536,3537,3538,3540,3541,3542,3543,3544,3545,3550,3551,3552,3553,3554,3555,'

              select ROW_NUMBER() OVER ( ORDER BY a.PostedDate, a.Date, a.No ) AS RowNum ,
			         a.Date as ngay_CTU, 
			         a.PostedDate as ngay_HT, 
					 a.No as So_CTU,
					 a.InvoiceDate as Ngay_HD, 
					 a.InvoiceNo as SO_HD, 
					 Sreason as Dien_giai,
					 SUM(CASE WHEN a.Account IN ( '5111' ) THEN a.CreditAmount
                         ELSE $0
                    END) AS TurnOverAmountInv ,
                    SUM(CASE WHEN a.Account IN ( '5112' ) THEN a.CreditAmount
                         ELSE $0
                    END) AS TurnOverAmountFinishedInv ,
                SUM(CASE WHEN a.Account IN ( '5113' ) THEN a.CreditAmount
                         ELSE $0
                    END) AS TurnOverAmountServiceInv ,
                SUM(CASE WHEN a.Account IN ( '5118' ) THEN a.CreditAmount
                         ELSE $0
                    END)AS TurnOverAmountOther,
                SUM(CASE WHEN a.Account IN ( '5111' ) THEN a.DebitAmount
                         ELSE $0
                    END) AS DiscountAmount ,
				SUM(CASE WHEN (a.typeID = '330' and a.Account IN ( '5111' )) THEN a.DebitAmount
                         ELSE $0
                    END) AS ReturnAmount,
                SUM(CASE WHEN (a.typeID = '340' and a.Account IN ( '5111' )) THEN a.DebitAmount
                         ELSE $0
                    END) AS ReduceAmount,
			    SUM(b.VATAmount) AS VATAmount,
				ao.AccountingObjectCode as CustomerCode, 
				ao.AccountingObjectName as CustomerName 
				INTO    #Result
				from GeneralLedger a 
				join SAInvoiceDetail b on a.DetailID = b.ID 
				join MaterialGoods c on b.MaterialGoodsID = c.ID
				join SAInvoice d on a.ReferenceID = d.ID 
				join Type e on a.TypeID = e.ID
				join AccountingObject ao on a.AccountingObjectID = ao.ID 
				where a.PostedDate between  @FromDate and @ToDate
				group by a.Date,  a.PostedDate, a.No ,a.InvoiceDate , a.InvoiceNo , Sreason,
				ao.AccountingObjectCode,ao.AccountingObjectName;

       select	RowNum ,
				ngay_CTU, 
				ngay_HT, 
				So_CTU,
				Ngay_HD, 
				SO_HD, 
				Dien_giai,
				TurnOverAmountInv + TurnOverAmountFinishedInv + TurnOverAmountServiceInv +
				TurnOverAmountOther as SumTurnOver,
				TurnOverAmountInv ,
				TurnOverAmountFinishedInv ,
                TurnOverAmountServiceInv ,
                TurnOverAmountOther,
                DiscountAmount ,
				ReturnAmount,
                ReduceAmount,
				(TurnOverAmountInv + TurnOverAmountFinishedInv + TurnOverAmountServiceInv +
				TurnOverAmountOther) - (DiscountAmount + ReturnAmount + ReduceAmount) as TurnOverPure,
				CustomerCode, 
				CustomerName 
   from #Result 
   where TurnOverAmountInv + TurnOverAmountFinishedInv + TurnOverAmountServiceInv +
				TurnOverAmountOther <> 0
   ORDER BY RowNum   
    
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER FUNCTION [dbo].[XXXXXXXXX]
      (
        @FromDate DATETIME ,
        @ToDate DATETIME ,
        @MaxAccountGrade INT ,
        @IsBalanceBothSide BIT
      )
RETURNS @Result TABLE
      (
        AccountID UNIQUEIDENTIFIER ,
        DetailByAccountObject BIT ,
        AccountObjectType INT ,
        AccountNumber NVARCHAR(50) ,
        AccountName NVARCHAR(255) ,
        AccountNameEnglish NVARCHAR(255) ,
        AccountCategoryKind INT ,
        IsParent BIT ,
        ParentID UNIQUEIDENTIFIER ,
        Grade INT ,
        AccountKind INT ,
        SortOrder INT ,
        OpeningDebitAmount DECIMAL(25, 4) ,
        OpeningCreditAmount DECIMAL(25, 4) ,
        DebitAmount DECIMAL(25, 4) ,
        CreditAmount DECIMAL(25, 4) ,
        DebitAmountAccum DECIMAL(25, 4) ,
        CreditAmountAccum DECIMAL(25, 4) ,
        ClosingDebitAmount DECIMAL(25, 4) ,
        ClosingCreditAmount DECIMAL(25, 4)
      )
AS
    BEGIN
	   DECLARE @MainCurrency NVARCHAR(3)
        SET @MainCurrency = 'VND'	
        /*ngày bắt đầu chương trình*/
        DECLARE @StartDate DATETIME
        SELECT TOP 1
                @StartDate = CONVERT(DATETIME, Data, 103)
        FROM    dbo.SystemOption
        WHERE   Code LIKE N'%ApplicationStartDate%'      
        IF @IsBalanceBothSide = 1
           BEGIN
					
                 DECLARE @GeneralLedger TABLE
                         (
                           AccountNumber NVARCHAR(20) ,
                           BranchID UNIQUEIDENTIFIER ,
                           CurrencyID NVARCHAR(3) ,
                           OpenningDebitAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,
                           CreditAmountAccum DECIMAL(25, 4) ,
                           OpenningDebitAmountOC DECIMAL(25, 4) ,
                           DebitAmountOC DECIMAL(25, 4) ,
                           CreditAmountOC DECIMAL(25, 4) ,
                           DebitAmountOCAccum DECIMAL(25, 4) ,
                           CreditAmountOCAccum DECIMAL(25, 4)
                         )
                 INSERT INTO @GeneralLedger
                        SELECT  G.*
                        FROM    (
                                  SELECT    Account ,
                                            GL.BranchID ,
                                            GL.CurrencyID ,
                                            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount
                                                     ELSE 0
                                                END) AS OpenningDebitAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmount )
                                                     ELSE 0
                                                END) AS DebitAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmount )
                                                     ELSE 0
                                                END) AS CreditAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmount )
                                                     ELSE 0
                                                END) AS DebitAmountAccum ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmount )
                                                     ELSE 0
                                                END) AS CreditAmountAccum ,
                                            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmountOriginal - GL.CreditAmountOriginal
                                                     ELSE 0
                                                END) AS OpenningDebitAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmountOriginal )
                                                     ELSE 0
                                                END) AS DebitAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmountOriginal )
                                                     ELSE 0
                                                END) AS CreditAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmountOriginal )
                                                     ELSE 0
                                                END) AS DebitAmountOCAccum ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmountOriginal )
                                                     ELSE 0
                                                END) AS CreditAmountOCAccum
                                  FROM      dbo.GeneralLedger AS GL
                                  WHERE    GL.PostedDate <= @ToDate
                                  GROUP BY  Account ,
                                            GL.BranchID ,
                                            GL.CurrencyID
                                ) G
			
                 DECLARE @DetailData TABLE
                         (
                           AccountNumber NVARCHAR(50) ,
                           CurrencyID NVARCHAR(20) ,
                           SortOrder INT ,
                           OpeningDebitAmount DECIMAL(25, 4) ,
                           OpeningCreditAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,/*Nợ lũy kế*/
                           CreditAmountAccum DECIMAL(25, 4) ,/*có lũy kế*/
                           ClosingDebitAmount DECIMAL(25, 4) ,
                           ClosingCreditAmount DECIMAL(25, 4)
                         )
	
                  INSERT @DetailData
                        SELECT  F.AccountNumber ,
                                '' ,
                                0 ,
                                SUM(CASE WHEN F.AccountCategoryKind = 0
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount > 0
                                                 ) THEN F.OpenningDebitAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN F.AccountCategoryKind = 1
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount < 0
                                                 ) THEN -1 * F.OpenningDebitAmount
                                         ELSE 0
                                    END) AS OpenningCreditAmount ,
                                SUM(F.DebitAmount) AS DebitAmount ,
                                SUM(F.CreditAmount) AS CreditAmount ,
                                SUM(F.DebitAmountAccum) AS DebitAmountAccum ,
                                SUM(F.CreditAmountAccum) AS CreditAmountAccum ,
                                SUM(CASE WHEN F.AccountCategoryKind = 0
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount > 0
                                                 ) THEN F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN F.AccountCategoryKind = 1
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount < 0
                                                 ) THEN F.CreditAmount - F.OpenningDebitAmount - F.DebitAmount
                                         ELSE 0
                                    END) AS OpenningCreditAmount
                        FROM    (
                                  SELECT    A.AccountNumber ,
                                            SUM(OpenningDebitAmount) AS OpenningDebitAmount ,
                                            SUM(DebitAmount) AS DebitAmount ,
                                            SUM(CreditAmount) AS CreditAmount ,
                                            SUM(DebitAmountAccum) AS DebitAmountAccum ,
                                            SUM(CreditAmountAccum) AS CreditAmountAccum ,
                                            A.AccountGroupKind AccountCategoryKind ,
                                            GL.BranchID BranchID
                                  FROM      @GeneralLedger AS GL
                                  INNER JOIN dbo.Account A ON GL.AccountNumber = A.AccountNumber
                                  WHERE     GL.AccountNumber NOT LIKE '0%'
                                            AND A.IsParentNode = 0
                                  GROUP BY  A.AccountNumber ,
                                            A.AccountGroupKind ,
                                            GL.BranchID
                                  HAVING    SUM(OpenningDebitAmount) <> 0
                                            OR SUM(DebitAmount) <> 0
                                            OR SUM(CreditAmount) <> 0
                                ) AS F
                        GROUP BY F.AccountNumber ,
                                F.AccountCategoryKind
                 OPTION ( RECOMPILE )	         					
				
                 INSERT @Result
                        SELECT  A.ID ,
						        null,
								null,
                                A.AccountNumber ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade ,
                                0 AccountKind,
                                D.SortOrder ,
                                SUM(D.OpeningDebitAmount) ,
                                SUM(D.OpeningCreditAmount) ,
                                SUM(D.DebitAmount) ,
                                SUM(D.CreditAmount) ,
                                SUM(D.DebitAmountAccum) ,
                                SUM(D.CreditAmountAccum) ,
                                SUM(D.ClosingDebitAmount) ,
                                SUM(D.ClosingCreditAmount)
                        FROM    @DetailData D
                        INNER JOIN dbo.Account A ON D.AccountNumber LIKE A.AccountNumber + '%'
                        WHERE   A.Grade <= @MaxAccountGrade
                        GROUP BY A.ID ,
                                A.AccountNumber ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade ,
                                D.SortOrder
                        HAVING  SUM(D.OpeningDebitAmount) <> 0
                                OR SUM(D.OpeningCreditAmount) <> 0
                                OR SUM(D.DebitAmount) <> 0
                                OR SUM(D.CreditAmount) <> 0
                                OR SUM(D.ClosingDebitAmount) <> 0
                                OR SUM(D.ClosingCreditAmount) <> 0
                 OPTION ( RECOMPILE )
           END
        ELSE
           BEGIN
                 DECLARE @GeneralLedgerP1 TABLE
                         (
                           AccountNumber NVARCHAR(20) ,
                           OpenningDebitAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,
                           CreditAmountAccum DECIMAL(25, 4)
                         )
                 INSERT INTO @GeneralLedgerP1
                        SELECT  Account ,
                                SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmount )
                                         ELSE 0
                                    END) AS DebitAmount ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmount )
                                         ELSE 0
                                    END) AS CreditAmount ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmount )
                                         ELSE 0
                                    END) AS DebitAmountAccum ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmount )
                                         ELSE 0
                                    END) AS CreditAmountAccum
                        FROM    dbo.GeneralLedger AS GL
                        WHERE   GL.PostedDate <= @ToDate
                        GROUP BY Account

                 INSERT @Result
                        SELECT  A.ID ,
                                null DetailByAccountObject ,
                                null AccountObjectType,
                                A.AccountNumber ,
                                CASE WHEN F.CurrencyID <> '' THEN F.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN F.CurrencyID <> '' THEN F.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade AS Grade ,
                                F.AccountKind, 
                                F.SortOrder ,
                                ( CASE WHEN A.AccountGroupKind = 0
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount > 0
                                               ) THEN F.OpenningDebitAmount
                                       ELSE 0
                                  END ) AS OpenningDebitAmount ,
                                ( CASE WHEN A.AccountGroupKind = 1
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount < 0
                                               ) THEN -1 * F.OpenningDebitAmount
                                       ELSE 0
                                  END ) AS OpenningCreditAmount ,
                                ( F.DebitAmount ) AS DebitAmount ,
                                ( F.CreditAmount ) AS CreditAmount ,
                                ( F.DebitAmountAccum ) AS DebitAmountAccum ,
                                ( F.CreditAmountAccum ) AS CreditAmountAccum ,
                                ( CASE WHEN A.AccountGroupKind = 0
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount > 0
                                               ) THEN F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount
                                       ELSE 0
                                  END ) AS OpenningDebitAmount ,
                                ( CASE WHEN A.AccountGroupKind = 1
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount < 0
                                               ) THEN F.CreditAmount - F.OpenningDebitAmount - F.DebitAmount
                                       ELSE 0
                                  END ) AS OpenningCreditAmount
                        FROM    (
                                  SELECT    A.ID ,
                                            null DetailByAccountObject,
                                            null AccountObjectType, 
                                            A.AccountNumber ,
                                            '' AS CurrencyID ,
                                            0 AS SortOrder ,
                                            SUM(OpenningDebitAmount) AS OpenningDebitAmount ,
                                            SUM(DebitAmount) AS DebitAmount ,
                                            SUM(CreditAmount) AS CreditAmount ,
                                            SUM(DebitAmountAccum) AS DebitAmountAccum ,
                                            SUM(CreditAmountAccum) AS CreditAmountAccum ,
                                            0 AS AccountKind
                                  FROM      @GeneralLedgerP1 AS GL
                                  INNER JOIN dbo.Account A ON GL.AccountNumber LIKE A.AccountNumber + '%' 
                                  WHERE     A.Grade <= @MaxAccountGrade
                                            AND (
                                                  OpenningDebitAmount <> 0
                                                  OR DebitAmount <> 0
                                                  OR CreditAmount <> 0
                                                )
                                  GROUP BY  A.ID ,
                                            A.AccountNumber      
                                ) AS F
                        INNER JOIN Account A ON F.AccountNumber = A.AccountNumber
                 OPTION ( RECOMPILE )      
           END
        RETURN
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

/*

*/
ALTER FUNCTION [dbo].[Func_SoDu]
      ( @FromDate DATETIME ,
        @ToDate DATETIME ,
        @MaxAccountGrade INT 
      )
RETURNS @Result TABLE
      (
        AccountID UNIQUEIDENTIFIER ,
        DetailByAccountObject BIT ,
        AccountObjectType INT ,
		AccountObjectID NVARCHAR(50) ,
        AccountNumber NVARCHAR(10) ,
        AccountName NVARCHAR(255) ,
        AccountNameEnglish NVARCHAR(255) ,
        AccountCategoryKind INT ,
        IsParent BIT ,
        ParentID UNIQUEIDENTIFIER ,
        Grade INT ,
        AccountKind INT ,
        SortOrder INT ,
        OpeningDebitAmount DECIMAL(25, 4) ,
        OpeningCreditAmount DECIMAL(25, 4) ,
        DebitAmount DECIMAL(25, 4) ,
        CreditAmount DECIMAL(25, 4) ,
        DebitAmountAccum DECIMAL(25, 4) ,
        CreditAmountAccum DECIMAL(25, 4) ,
        ClosingDebitAmount DECIMAL(25, 4) ,
        ClosingCreditAmount DECIMAL(25, 4)
      )
AS
    BEGIN
        DECLARE @MainCurrency NVARCHAR(3)
        SET @MainCurrency = 'VND'	
        /*ngày bắt đầu chương trình*/
        DECLARE @StartDate DATETIME

        SELECT TOP 1
                @StartDate = CONVERT(DATETIME, Data, 103)
        FROM    dbo.SystemOption
        WHERE   Code LIKE N'%ApplicationStartDate%'                
           BEGIN
					
                 DECLARE @GeneralLedger TABLE
                         (
                           AccountNumber NVARCHAR(30) ,
                           AccountObjectID UNIQUEIDENTIFIER ,
                           BranchID UNIQUEIDENTIFIER ,
                           CurrencyID NVARCHAR(3) ,
                           OpenningDebitAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,
                           CreditAmountAccum DECIMAL(25, 4) ,
                           OpenningDebitAmountOC DECIMAL(25, 4) ,
                           DebitAmountOC DECIMAL(25, 4) ,
                           CreditAmountOC DECIMAL(25, 4) ,
                           DebitAmountOCAccum DECIMAL(25, 4) ,
                           CreditAmountOCAccum DECIMAL(25, 4)
                         )
                 INSERT INTO @GeneralLedger
                        SELECT  G.*
                        FROM    (
                                  SELECT    Account ,
                                            AccountingObjectID ,
                                            GL.BranchID ,
                                            GL.CurrencyID ,
                                            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount
                                                     ELSE 0
                                                END) AS OpenningDebitAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmount )
                                                     ELSE 0
                                                END) AS DebitAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmount )
                                                     ELSE 0
                                                END) AS CreditAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmount )
                                                     ELSE 0
                                                END) AS DebitAmountAccum ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmount )
                                                     ELSE 0
                                                END) AS CreditAmountAccum ,
                                            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmountOriginal - GL.CreditAmountOriginal
                                                     ELSE 0
                                                END) AS OpenningDebitAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmountOriginal )
                                                     ELSE 0
                                                END) AS DebitAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmountOriginal )
                                                     ELSE 0
                                                END) AS CreditAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmountOriginal )
                                                     ELSE 0
                                                END) AS DebitAmountOCAccum ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmountOriginal )
                                                     ELSE 0
                                                END) AS CreditAmountOCAccum
                                  FROM      dbo.GeneralLedger AS GL
                                  WHERE    GL.PostedDate <= @ToDate
                                  GROUP BY  Account ,
                                            AccountingObjectID ,
                                            GL.BranchID ,
                                            GL.CurrencyID
                                ) G
			
                 DECLARE @Data TABLE
                         (
                           AccountNumber NVARCHAR(50) ,
						   AccountObjectID UNIQUEIDENTIFIER ,
                           CurrencyID NVARCHAR(20) ,
                           SortOrder INT ,
                           OpeningDebitAmount DECIMAL(25, 4) ,
                           OpeningCreditAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,/*Nợ lũy kế*/
                           CreditAmountAccum DECIMAL(25, 4) ,/*có lũy kế*/
                           ClosingDebitAmount DECIMAL(25, 4) ,
                           ClosingCreditAmount DECIMAL(25, 4)
                         )
	
                 INSERT @Data
                        SELECT  F.AccountNumber ,
						        F.AccountObjectID,
                                '' ,
                                0 ,
                                SUM(CASE WHEN F.AccountCategoryKind = 0
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount > 0
                                                 ) THEN F.OpenningDebitAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN F.AccountCategoryKind = 1
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount < 0
                                                 ) THEN -1 * F.OpenningDebitAmount
                                         ELSE 0
                                    END) AS OpenningCreditAmount ,
                                SUM(F.DebitAmount) AS DebitAmount ,
                                SUM(F.CreditAmount) AS CreditAmount ,
                                SUM(F.DebitAmountAccum) AS DebitAmountAccum ,
                                SUM(F.CreditAmountAccum) AS CreditAmountAccum ,
                                SUM(CASE WHEN F.AccountCategoryKind = 0
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount > 0
                                                 ) THEN F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN F.AccountCategoryKind = 1
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount < 0
                                                 ) THEN F.CreditAmount - F.OpenningDebitAmount - F.DebitAmount
                                         ELSE 0
                                    END) AS OpenningCreditAmount
                        FROM    (
                                  SELECT    A.AccountNumber ,
                                            SUM(OpenningDebitAmount) AS OpenningDebitAmount ,
                                            SUM(DebitAmount) AS DebitAmount ,
                                            SUM(CreditAmount) AS CreditAmount ,
                                            SUM(DebitAmountAccum) AS DebitAmountAccum ,
                                            SUM(CreditAmountAccum) AS CreditAmountAccum ,
                                            GL.AccountObjectID AccountObjectID ,
                                            A.AccountGroupKind AccountCategoryKind ,
                                            GL.BranchID BranchID
                                  FROM      @GeneralLedger AS GL
                                  INNER JOIN dbo.Account A ON GL.AccountNumber = A.AccountNumber
                                  WHERE     GL.AccountNumber NOT LIKE '0%'
                                            AND A.IsParentNode = 0
                                  GROUP BY  A.AccountNumber ,
                                            GL.AccountObjectID,
                                            A.AccountGroupKind ,
                                            GL.BranchID
                                  HAVING    SUM(OpenningDebitAmount) <> 0
                                            OR SUM(DebitAmount) <> 0
                                            OR SUM(CreditAmount) <> 0
                                ) AS F
                        GROUP BY F.AccountNumber ,
                                F.AccountCategoryKind,
								F.AccountObjectID
                 OPTION ( RECOMPILE )		         					    
                 INSERT @Result
                        SELECT  A.ID ,
						        null,
								null,
								D.AccountObjectID,
                                A.AccountNumber ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade ,
                                0 AccountKind,
                                D.SortOrder ,
                                SUM(D.OpeningDebitAmount) ,
                                SUM(D.OpeningCreditAmount) ,
                                SUM(D.DebitAmount) ,
                                SUM(D.CreditAmount) ,
                                SUM(D.DebitAmountAccum) ,
                                SUM(D.CreditAmountAccum) ,
                                SUM(D.ClosingDebitAmount) ,
                                SUM(D.ClosingCreditAmount)
                        FROM    @Data D
                        INNER JOIN dbo.Account A ON D.AccountNumber LIKE A.AccountNumber + '%'
                        WHERE   A.Grade <= @MaxAccountGrade
                        GROUP BY A.ID ,
                                A.AccountNumber ,
								D.AccountObjectID,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade ,
                                D.SortOrder
                        HAVING  SUM(D.OpeningDebitAmount) <> 0
                                OR SUM(D.OpeningCreditAmount) <> 0
                                OR SUM(D.DebitAmount) <> 0
                                OR SUM(D.CreditAmount) <> 0
                                OR SUM(D.ClosingDebitAmount) <> 0
                                OR SUM(D.ClosingCreditAmount) <> 0
                 OPTION ( RECOMPILE )
           END
        RETURN
    END



GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER FUNCTION [dbo].[Func_GetBalanceAccountF01]
      (
        @FromDate DATETIME ,
        @ToDate DATETIME ,
        @MaxAccountGrade INT ,
        @IsBalanceBothSide BIT
      )
RETURNS @Result TABLE
      (
        AccountID UNIQUEIDENTIFIER ,
        DetailByAccountObject BIT ,
        AccountObjectType INT ,
        AccountNumber NVARCHAR(50) ,
        AccountName NVARCHAR(255) ,
        AccountNameEnglish NVARCHAR(255) ,
        AccountCategoryKind INT ,
        IsParent BIT ,
        ParentID UNIQUEIDENTIFIER ,
        Grade INT ,
        AccountKind INT ,
        SortOrder INT ,
        OpeningDebitAmount DECIMAL(25, 4) ,
        OpeningCreditAmount DECIMAL(25, 4) ,
        DebitAmount DECIMAL(25, 4) ,
        CreditAmount DECIMAL(25, 4) ,
        DebitAmountAccum DECIMAL(25, 4) ,
        CreditAmountAccum DECIMAL(25, 4) ,
        ClosingDebitAmount DECIMAL(25, 4) ,
        ClosingCreditAmount DECIMAL(25, 4)
      )
AS
    BEGIN
	   DECLARE @MainCurrency NVARCHAR(3)
        SET @MainCurrency = 'VND'	
        /*ngày bắt đầu chương trình*/
        DECLARE @StartDate DATETIME
        SELECT TOP 1
                @StartDate = CONVERT(DATETIME, Data, 103)
        FROM    dbo.SystemOption
        WHERE   Code LIKE N'%ApplicationStartDate%'
        IF @IsBalanceBothSide = 1
           BEGIN
                 DECLARE @GeneralLedger TABLE
                         (
                           AccountNumber NVARCHAR(20) ,
                           AccountObjectID UNIQUEIDENTIFIER ,
                           BranchID UNIQUEIDENTIFIER ,
                           CurrencyID NVARCHAR(3) ,
                           OpenningDebitAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,
                           CreditAmountAccum DECIMAL(25, 4) ,
                           OpenningDebitAmountOC DECIMAL(25, 4) ,
                           DebitAmountOC DECIMAL(25, 4) ,
                           CreditAmountOC DECIMAL(25, 4) ,
                           DebitAmountOCAccum DECIMAL(25, 4) ,
                           CreditAmountOCAccum DECIMAL(25, 4)
                         )
                 INSERT INTO @GeneralLedger
                        SELECT  G.*
                        FROM    (
                                  SELECT    Account ,
                                            AccountingObjectID ,
                                            GL.BranchID ,
                                            GL.CurrencyID ,
                                            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount
                                                     ELSE 0
                                                END) AS OpenningDebitAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmount )
                                                     ELSE 0
                                                END) AS DebitAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmount )
                                                     ELSE 0
                                                END) AS CreditAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmount )
                                                     ELSE 0
                                                END) AS DebitAmountAccum ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmount )
                                                     ELSE 0
                                                END) AS CreditAmountAccum ,
                                            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmountOriginal - GL.CreditAmountOriginal
                                                     ELSE 0
                                                END) AS OpenningDebitAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmountOriginal )
                                                     ELSE 0
                                                END) AS DebitAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmountOriginal )
                                                     ELSE 0
                                                END) AS CreditAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmountOriginal )
                                                     ELSE 0
                                                END) AS DebitAmountOCAccum ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmountOriginal )
                                                     ELSE 0
                                                END) AS CreditAmountOCAccum
                                  FROM      dbo.GeneralLedger AS GL
                                  WHERE    GL.PostedDate <= @ToDate
                                  GROUP BY  Account ,
                                            AccountingObjectID ,
                                            GL.BranchID ,
                                            GL.CurrencyID
                                ) G
			
                 DECLARE @Data TABLE
                         (
                           AccountNumber NVARCHAR(50) ,
						   AccountObjectID UNIQUEIDENTIFIER ,
                           CurrencyID NVARCHAR(20) ,
                           SortOrder INT ,
                           OpeningDebitAmount DECIMAL(25, 4) ,
                           OpeningCreditAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,/*Nợ lũy kế*/
                           CreditAmountAccum DECIMAL(25, 4) ,/*có lũy kế*/
                           ClosingDebitAmount DECIMAL(25, 4) ,
                           ClosingCreditAmount DECIMAL(25, 4)
                         )
	
                 INSERT @Data
                        SELECT  F.AccountNumber ,
						        F.AccountObjectID,
                                '' ,
                                0 ,
                                SUM(CASE WHEN F.AccountCategoryKind = 0
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount > 0
                                                 ) THEN F.OpenningDebitAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN F.AccountCategoryKind = 1
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount < 0
                                                 ) THEN -1 * F.OpenningDebitAmount
                                         ELSE 0
                                    END) AS OpenningCreditAmount ,
                                SUM(F.DebitAmount) AS DebitAmount ,
                                SUM(F.CreditAmount) AS CreditAmount ,
                                SUM(F.DebitAmountAccum) AS DebitAmountAccum ,
                                SUM(F.CreditAmountAccum) AS CreditAmountAccum ,
                                SUM(CASE WHEN F.AccountCategoryKind = 0
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount > 0
                                                 ) THEN F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN F.AccountCategoryKind = 1
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount < 0
                                                 ) THEN F.CreditAmount - F.OpenningDebitAmount - F.DebitAmount
                                         ELSE 0
                                    END) AS OpenningCreditAmount
                        FROM    (
                                  SELECT    A.AccountNumber ,
                                            SUM(OpenningDebitAmount) AS OpenningDebitAmount ,
                                            SUM(DebitAmount) AS DebitAmount ,
                                            SUM(CreditAmount) AS CreditAmount ,
                                            SUM(DebitAmountAccum) AS DebitAmountAccum ,
                                            SUM(CreditAmountAccum) AS CreditAmountAccum ,
                                            GL.AccountObjectID AccountObjectID ,
                                            A.AccountGroupKind AccountCategoryKind ,
                                            GL.BranchID BranchID
                                  FROM      @GeneralLedger AS GL
                                  INNER JOIN dbo.Account A ON GL.AccountNumber = A.AccountNumber
                                  WHERE     GL.AccountNumber NOT LIKE '0%'
                                            AND A.IsParentNode = 0
                                  GROUP BY  A.AccountNumber ,
                                            GL.AccountObjectID,
                                            A.AccountGroupKind ,
                                            GL.BranchID
                                  HAVING    SUM(OpenningDebitAmount) <> 0
                                            OR SUM(DebitAmount) <> 0
                                            OR SUM(CreditAmount) <> 0
                                ) AS F
                        GROUP BY F.AccountNumber ,
                                F.AccountCategoryKind,
								F.AccountObjectID
                 OPTION ( RECOMPILE )		         					    
                 INSERT @Result
                        SELECT  A.ID ,
						        null,
								null,
                                A.AccountNumber ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade ,
                                0 AccountKind,
                                D.SortOrder ,
                                SUM(D.OpeningDebitAmount) ,
                                SUM(D.OpeningCreditAmount) ,
                                SUM(D.DebitAmount) ,
                                SUM(D.CreditAmount) ,
                                SUM(D.DebitAmountAccum) ,
                                SUM(D.CreditAmountAccum) ,
                                SUM(D.ClosingDebitAmount) ,
                                SUM(D.ClosingCreditAmount)
                        FROM    @Data D
                        INNER JOIN dbo.Account A ON D.AccountNumber LIKE A.AccountNumber + '%'
                        WHERE   A.Grade <= @MaxAccountGrade
                        GROUP BY A.ID ,
                                A.AccountNumber ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade ,
                                D.SortOrder
                        HAVING  SUM(D.OpeningDebitAmount) <> 0
                                OR SUM(D.OpeningCreditAmount) <> 0
                                OR SUM(D.DebitAmount) <> 0
                                OR SUM(D.CreditAmount) <> 0
                                OR SUM(D.ClosingDebitAmount) <> 0
                                OR SUM(D.ClosingCreditAmount) <> 0
                 OPTION ( RECOMPILE )
           END
        ELSE
           BEGIN
                 DECLARE @GeneralLedgerP1 TABLE
                         (
                           AccountNumber NVARCHAR(20) ,
                           OpenningDebitAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,
                           CreditAmountAccum DECIMAL(25, 4)
                         )
                 INSERT INTO @GeneralLedgerP1
                        SELECT  Account ,
                                SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmount )
                                         ELSE 0
                                    END) AS DebitAmount ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmount )
                                         ELSE 0
                                    END) AS CreditAmount ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmount )
                                         ELSE 0
                                    END) AS DebitAmountAccum ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmount )
                                         ELSE 0
                                    END) AS CreditAmountAccum
                        FROM    dbo.GeneralLedger AS GL
                        WHERE   GL.PostedDate <= @ToDate
                        GROUP BY Account
                 INSERT @Result
                        SELECT  A.ID ,
                                null DetailByAccountObject ,
                                null AccountObjectType,
                                A.AccountNumber ,
                                CASE WHEN F.CurrencyID <> '' THEN F.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN F.CurrencyID <> '' THEN F.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade AS Grade ,
                                F.AccountKind,
                                F.SortOrder ,
                                ( CASE WHEN A.AccountGroupKind = 0
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount > 0
                                               ) THEN F.OpenningDebitAmount
                                       ELSE 0
                                  END ) AS OpenningDebitAmount ,
                                ( CASE WHEN A.AccountGroupKind = 1
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount < 0
                                               ) THEN -1 * F.OpenningDebitAmount
                                       ELSE 0
                                  END ) AS OpenningCreditAmount ,
                                ( F.DebitAmount ) AS DebitAmount ,
                                ( F.CreditAmount ) AS CreditAmount ,
                                ( F.DebitAmountAccum ) AS DebitAmountAccum ,
                                ( F.CreditAmountAccum ) AS CreditAmountAccum ,
                                ( CASE WHEN A.AccountGroupKind = 0
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount > 0
                                               ) THEN F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount
                                       ELSE 0
                                  END ) AS OpenningDebitAmount ,
                                ( CASE WHEN A.AccountGroupKind = 1
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount < 0
                                               ) THEN F.CreditAmount - F.OpenningDebitAmount - F.DebitAmount
                                       ELSE 0
                                  END ) AS OpenningCreditAmount
                        FROM    (
                                  SELECT    A.ID ,
                                            null DetailByAccountObject,
                                            null AccountObjectType,
                                            A.AccountNumber ,
                                            '' AS CurrencyID ,
                                            0 AS SortOrder ,
                                            SUM(OpenningDebitAmount) AS OpenningDebitAmount ,
                                            SUM(DebitAmount) AS DebitAmount ,
                                            SUM(CreditAmount) AS CreditAmount ,
                                            SUM(DebitAmountAccum) AS DebitAmountAccum ,
                                            SUM(CreditAmountAccum) AS CreditAmountAccum ,
                                            0 AS AccountKind
                                  FROM      @GeneralLedgerP1 AS GL
                                  INNER JOIN dbo.Account A ON GL.AccountNumber LIKE A.AccountNumber + '%'
                                  WHERE     A.Grade <= @MaxAccountGrade
                                            AND (
                                                  OpenningDebitAmount <> 0
                                                  OR DebitAmount <> 0
                                                  OR CreditAmount <> 0
                                                )
                                  GROUP BY  A.ID ,
                                            A.AccountNumber      
                                ) AS F
                        INNER JOIN Account A ON F.AccountNumber = A.AccountNumber
                 OPTION ( RECOMPILE )      
           END
        RETURN
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*

*/
ALTER PROCEDURE [dbo].[Proc_GetBalanceAccountF01]
      ( @FromDate DATETIME ,
        @ToDate DATETIME ,
        @MaxAccountGrade INT,
		@IsBalanceBothSide BIT
		)
AS
BEGIN
 
SELECT t.AccountID ,
		       t.AccountCategoryKind,
			   t.AccountNumber,
			   t.AccountName,
			   round(sum(t.OpeningDebitAmount),0) OpeningDebitAmount,
			   round(sum(t.OpeningCreditAmount),0) OpeningCreditAmount,
			   round(sum(t.DebitAmount),0) DebitAmount,
			   round(sum(t.CreditAmount),0) CreditAmount,
			   round(sum(t.DebitAmountAccum),0) DebitAmountAccum,
			   round(sum(t.CreditAmountAccum),0) CreditAmountAccum,
			   round(sum(t.ClosingDebitAmount),0) ClosingDebitAmount,
			   round(sum(t.ClosingCreditAmount),0) ClosingCreditAmount FROM [dbo].[Func_GetBalanceAccountF01] (
   @FromDate
  ,@ToDate
  ,@MaxAccountGrade
  ,@IsBalanceBothSide) t
  group by t.AccountID,
		       t.AccountCategoryKind,
			   t.AccountNumber,
			   t.AccountName
  order by t.AccountNumber
end

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*

*/
ALTER FUNCTION [dbo].[Func_GetB01_DN]
    (
      @BranchID UNIQUEIDENTIFIER ,
      @IncludeDependentBranch BIT ,
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @IsSimilarBranch BIT,
      @IsB01bDNN BIT ,
      @isPrintByYear BIT,
      @PrevFromDate DATETIME,
      @PrevToDate DATETIME
    )
RETURNS @Result TABLE
    (
      ItemID UNIQUEIDENTIFIER ,
      ItemCode NVARCHAR(25) ,
      ItemName NVARCHAR(255) ,
      ItemNameEnglish NVARCHAR(255) ,
      ItemIndex INT ,
      Description NVARCHAR(255) ,
      FormulaType INT ,
      FormulaFrontEnd NVARCHAR(MAX) ,
      Formula XML ,
      Hidden BIT ,
      IsBold BIT ,
      IsItalic BIT ,
      Category INT ,
      SortOrder INT
      ,
      Amount DECIMAL(25, 4) ,
      PrevAmount DECIMAL(25, 4)
    )

    BEGIN 
            DECLARE @AccountingSystem INT	
            SET @AccountingSystem = 48 
    
            DECLARE @SubAccountSystem INT
            SELECT  @SubAccountSystem = 133
	
            DECLARE @ReportID NVARCHAR(100)
            SET @ReportID = '1'

            IF @AccountingSystem = 48
                AND @SubAccountSystem = 133
                AND @IsB01bDNN = 0
                BEGIN

                    SET @ReportID = '7'	
                END

            DECLARE @ItemForeignCurrency UNIQUEIDENTIFIER
            DECLARE @ItemIndex INT
            DECLARE @MainCurrency NVARCHAR(3)
            IF @AccountingSystem = 48
                AND @SubAccountSystem <> 133
                SET @ItemForeignCurrency = '6D6BA7EB-E60A-46ED-A556-E674709F5466'
            ELSE
                SET @ItemForeignCurrency = '00000000-0000-0000-0000-000000000000'	
	
            SET @ItemIndex = ( SELECT TOP 1
                                        ItemIndex
                               FROM     FRTemplate
                               WHERE    ItemID = @ItemForeignCurrency
                             )
            SET @MainCurrency = 'VND'
            DECLARE @tblItem TABLE
                (
                  ItemID UNIQUEIDENTIFIER ,
                  ItemIndex INT ,
                  ItemName NVARCHAR(255) ,
                  OperationSign INT ,
                  OperandString NVARCHAR(255) ,
                  AccountNumberPercent NVARCHAR(25) ,
                  AccountNumber NVARCHAR(25) ,
                  CorrespondingAccountNumber VARCHAR(25) ,
                  IsDetailByAO INT ,
                  AccountKind INT
                )
	
            INSERT  @tblItem
                    SELECT  ItemID ,
                            ItemIndex ,
                            ItemName ,
                            x.r.value('@OperationSign', 'INT') ,
                            RTRIM(LTRIM(x.r.value('@OperandString',
                                                  'nvarchar(255)'))) ,
                            x.r.value('@AccountNumber', 'nvarchar(25)') + '%' ,
                            x.r.value('@AccountNumber', 'nvarchar(25)') ,
                            CASE WHEN x.r.value('@CorrespondingAccountNumber',
                                                'nvarchar(25)') <> ''
                                 THEN x.r.value('@CorrespondingAccountNumber',
                                                'nvarchar(25)') + '%'
                                 ELSE ''
                            END ,
                            CASE WHEN x.r.value('@OperandString',
                                                'nvarchar(255)') LIKE '%ChitietTheoTKvaDoituong'
                                 THEN 1
                                 ELSE CASE WHEN x.r.value('@OperandString',
                                                          'nvarchar(255)') LIKE '%ChitietTheoTK'
                                           THEN 2
                                           ELSE 0
                                      END
                            END ,
                            NULL
                    FROM    dbo.FRTemplate
                            CROSS APPLY Formula.nodes('/root/DetailFormula')
                            AS x ( r )
                    WHERE   AccountingSystem = @AccountingSystem
                            AND ReportID = @ReportID
                            AND FormulaType = 0
                            AND Formula IS NOT NULL
                            AND ItemID <> @ItemForeignCurrency
	
            UPDATE  @tblItem
            SET     AccountKind = A.AccountGroupKind
            FROM    dbo.Account A
            WHERE   A.AccountNumber = [@tblItem].AccountNumber
		
            DECLARE @AccountBalance TABLE
                (
                  AccountNumber NVARCHAR(20) ,
                  AccountCategoryKind INT ,
                  IsDetailByAO INT 
	            )
	 
            INSERT  @AccountBalance
                    SELECT 
		DISTINCT            A.AccountNumber ,
                            A.AccountGroupKind ,
                            B.IsDetailByAO
                    FROM    dbo.Account A
                            INNER JOIN @tblItem B ON A.AccountNumber LIKE B.AccountNumberPercent
	
            DECLARE @Balance TABLE
                (
                  AccountNumber NVARCHAR(20) ,
                  AccountCategoryKind INT ,
                  AccountObjectID UNIQUEIDENTIFIER ,
                  IsDetailByAO INT ,
                  OpeningDebit DECIMAL(25, 4) ,
                  OpeningCredit DECIMAL(25, 4) ,
                  ClosingDebit DECIMAL(25, 4) ,
                  ClosingCredit DECIMAL(25, 4) ,
                  BranchID UNIQUEIDENTIFIER
                )
            DECLARE @GeneralLedger TABLE
                (
                  AccountNumber NVARCHAR(20) ,
                  AccountObjectID UNIQUEIDENTIFIER ,
                  BranchID UNIQUEIDENTIFIER ,
                  IsOPN BIT ,
                  DebitAmount DECIMAL(25, 4) ,
                  CreditAmount DECIMAL(25, 4)
                )
	
            INSERT  INTO @GeneralLedger
                    ( AccountNumber ,
                      AccountObjectID ,
                      BranchID ,
                      IsOPN ,
                      DebitAmount ,
                      CreditAmount
	                )
                    SELECT  GL.Account ,
                            GL.AccountingObjectID ,
                            GL.BranchID ,
                            CASE WHEN GL.PostedDate < @FromDate THEN 1
                                 ELSE 0
                            END ,
                            SUM(DebitAmount) AS DebitAmount ,
                            SUM(CreditAmount) AS CreditAmount
                    FROM    dbo.GeneralLedger GL
                    WHERE   PostedDate <= @ToDate
                    GROUP BY GL.Account ,
                            GL.AccountingObjectID ,
                            GL.BranchID ,
                            CASE WHEN GL.PostedDate < @FromDate THEN 1
                                 ELSE 0
                            END
	
	
            DECLARE @StartDate DATETIME
			SELECT TOP 1
                @StartDate = CONVERT(DATETIME, Data, 103)
        FROM    dbo.SystemOption
        WHERE   Code LIKE N'%ApplicationStartDate%'
	 
            DECLARE @GeneralLedger421 TABLE
                (
                  OperandString NVARCHAR(255) ,
                  ClosingAmount DECIMAL(25, 4) ,
                  OpeningAmount DECIMAL(25, 4)
                )
	
	
	
	
	/* 
	 

Đối với báo cáo kỳ khác năm:
*Cuối kỳ: (Dư Có – Dư Nợ) TK 4211 trên sổ cái tính đến Từ ngày -1 + (Dư Có – Dư Nợ) TK 4212 trên sổ cái tính đến Từ ngày -1 
* Đầu kỳ: 
(Dư Có – Dư Nợ) TK 4211 trên sổ cái tính đến từ ngày -1 của kỳ trước liền kề+ (Dư Có – Dư Nợ) TK 4212 trên sổ cái tính đến Từ ngày -1 của kỳ trước liền kề. 
→ Nếu Từ ngày của kỳ trùng với ngày bắt đầu kỳ kế toán năm (theo năm tài chính trên hệ thống, 
VD: kỳ năm tài chính bắt đầu từ 01/10 thì ngày bắt đầu kỳ kế toán năm sẽ là 01/10) thì đầu kỳ sẽ lấy theo công thức: Dư Có TK 4211 – Dư Nợ TK 4211 trên Sổ cái tính đến Từ ngày -1 

	 */
	 /*- Thêm điều kiện chọn theo năm hoặc khác năm.*/
            INSERT  INTO @GeneralLedger421
                    ( OperandString ,
                      ClosingAmount ,
                      OpeningAmount
	                )
                    SELECT  N'Solieuchitieu421a_Ky_khac_nam' AS AccountNumber ,
                            ISNULL( SUM(CASE WHEN GL.Account LIKE '4211%'
                                          AND GL.PostedDate < @FromDate
                                     THEN CreditAmount - DebitAmount
                                     ELSE 0
                                END),0)
                         
                            + ISNULL( SUM(CASE WHEN GL.Account LIKE '4212%'
                                            AND GL.PostedDate < @FromDate
                                       THEN CreditAmount -DebitAmount
                                       ELSE 0
                                  END),0)
                           AS ClosingAmount ,
                                  
                            ISNULL( SUM(
									CASE WHEN DAY(@StartDate) = DAY(@FromDate) AND MONTH(@StartDate) =MONTH(@FromDate)  THEN 0 ELSE 
										CASE WHEN GL.Account LIKE '4211%'
													  AND GL.PostedDate < @PrevFromDate
												 THEN CreditAmount -DebitAmount
												 ELSE 0
											END
									end		
                                ),0)
                         
                            + SUM(CASE WHEN  DAY(@StartDate) = DAY(@FromDate) AND MONTH(@StartDate) =MONTH(@FromDate)  THEN 0
                                       ELSE CASE WHEN GL.Account LIKE '4212%'
                                                      AND GL.PostedDate < @PrevFromDate
                                                 THEN CreditAmount -DebitAmount
                                                 ELSE 0
                                            END
                                  END)
                     
                        +   ISNULL(SUM(CASE WHEN  DAY(@StartDate) = DAY(@FromDate) AND MONTH(@StartDate) =MONTH(@FromDate) 
									  AND GL.Account LIKE '4211%'
												 THEN CASE WHEN GL.PostedDate < @FromDate then CreditAmount - DebitAmount ELSE 0 end
									 ELSE 0
								END)
								,0) AS OpeningAmount                                  
                                  
                    FROM    dbo.GeneralLedger GL
                    WHERE   PostedDate <= @ToDate
                            AND gl.Account LIKE '421%'
	
	/*
	

Đối với báo cáo kỳ khác năm:
* Cuối kỳ: PS Có TK 4212 trên sổ cái trong kỳ báo cáo (không bao gồm ĐU N4211/Có TK 4212) - PSN TK 4212 trên sổ cái trong kỳ báo cáo (không bao gồm ĐU N4212/Có TK 4211)
* Đầu kỳ: 
PS Có TK 4212 trên sổ cái trong kỳ trước liền kề (không bao gồm ĐU N4211/Có TK 4212) – PS Nợ TK 4212 trên sổ cái trong kỳ trước liền kề (không bao gồm ĐU N4212/Có TK 4211)
→ Nếu Từ ngày của kỳ trùng với ngày bắt đầu kỳ kế toán năm (theo năm tài chính trên hệ thống, VD: kỳ năm tài chính bắt đầu từ 01/10 thì ngày bắt đầu kỳ kế toán năm sẽ là 01/10) thì đầu kỳ sẽ lấy theo công thức: Dư Có TK 4212 – Dư Nợ TK 4212 trên Sổ cái tính đến Từ ngày -1 

	*/
	/*- Thêm điều kiện chọn theo năm hoặc khác năm.*/
	
            INSERT  INTO @GeneralLedger421
                    ( OperandString ,
                      ClosingAmount ,
                      OpeningAmount
	                )
                    SELECT  N'Solieuchitieu421b_Ky_khac_nam' AS AccountNumber ,
                           ISNULL( SUM(CASE WHEN GL.Account LIKE '4212%'
                                          AND GL.PostedDate BETWEEN @fromdate AND @ToDate
                                     THEN ( CASE WHEN GL.AccountCorresponding LIKE N'4211%'
                                                 THEN 0
                                                 ELSE CreditAmount
                                            END )
                                     ELSE 0
                                END),0)
                            - ISNULL( SUM(CASE WHEN GL.Account LIKE '4212%'
                                            AND GL.PostedDate BETWEEN @Fromdate AND @ToDate
                                       THEN ( CASE WHEN GL.AccountCorresponding LIKE N'4211%'
                                                   THEN 0
                                                   ELSE DebitAmount
                                              END )
                                       ELSE 0
                                  END),0) AS ClosingAmount ,
		 /** Đầu kỳ: 
PS Có TK 4212 trên sổ cái trong kỳ trước liền kề (không bao gồm ĐU N4211/Có TK 4212) – PS Nợ TK 4212 trên sổ cái trong kỳ trước liền kề (không bao gồm ĐU N4212/Có TK 4211)
→ Nếu Từ ngày của kỳ trùng với ngày bắt đầu kỳ kế toán năm (theo năm tài chính trên hệ thống, VD: kỳ năm tài chính bắt đầu từ 01/10 thì ngày bắt đầu kỳ kế toán năm sẽ là 01/10)
 thì đầu kỳ sẽ lấy theo công thức: Dư Có TK 4212 – Dư Nợ TK 4212 trên Sổ cái tính đến Từ ngày -1 
*/
                            ISNULL( SUM(CASE WHEN  DAY(@StartDate) = DAY(@FromDate) AND MONTH(@StartDate) =MONTH(@FromDate)  THEN 0
                                     ELSE ( CASE WHEN GL.Account LIKE '4212%'
                                                      AND GL.PostedDate BETWEEN @PrevFromDate
                                                              AND
                                                              @PrevToDate
                                                 THEN ( CASE WHEN GL.AccountCorresponding LIKE N'4211%'
                                                             THEN 0
                                                             ELSE CreditAmount
                                                        END )
                                                 ELSE 0
                                            END )
                                END),0)
                            - ISNULL(  SUM(CASE WHEN  DAY(@StartDate) = DAY(@FromDate) AND MONTH(@StartDate) =MONTH(@FromDate)  THEN 0
                                       ELSE CASE WHEN GL.Account LIKE '4212%'
                                                      AND GL.PostedDate BETWEEN @PrevFromDate
                                                              AND
                                                              @PrevToDate
                                                 THEN ( CASE WHEN GL.AccountCorresponding LIKE N'4211%'
                                                             THEN 0
                                                             ELSE DebitAmount
                                                        END )
                                                 ELSE 0
                                            END
                                  END),0)
                            + /*nếu không  là kỳ đầu tiên
		→ Nếu Từ ngày của kỳ trùng với ngày bắt đầu kỳ kế toán năm (theo năm tài chính trên hệ thống, 
		VD: kỳ năm tài chính bắt đầu từ 01/10 thì ngày bắt đầu kỳ kế toán năm sẽ là 01/10) thì đầu kỳ sẽ lấy theo công thức: Dư Có TK 4212 – Dư Nợ TK 4212 trên Sổ cái tính đến Từ ngày -1 
		*/
			ISNULL(SUM(CASE WHEN  DAY(@StartDate) = DAY(@FromDate) AND MONTH(@StartDate) =MONTH(@FromDate) 
                          AND GL.Account LIKE '4212%'
                     THEN CASE WHEN GL.PostedDate < @FromDate then CreditAmount - DebitAmount ELSE 0 end
                     ELSE 0
                END),0) AS OpeningAmount
                    FROM    dbo.GeneralLedger GL
                    WHERE   PostedDate <= @ToDate
                            AND gl.Account LIKE '421%'
	
            INSERT  INTO @Balance
                    SELECT  GL.AccountNumber ,
                            A.AccountCategoryKind ,
                            CASE WHEN A.IsDetailByAO = 1
                                 THEN GL.AccountObjectID
                                 ELSE NULL
                            END AS AccountObjectID ,
                            A.IsDetailByAO ,
                            SUM(CASE WHEN GL.IsOPN = 1
                                     THEN DebitAmount - CreditAmount
                                     ELSE 0
                                END) OpeningDebit ,
                            0 ,
                            SUM(DebitAmount - CreditAmount) ClosingDebit ,
                            0
                            ,
                            CASE WHEN A.IsDetailByAO = 1
                                      AND @IsSimilarBranch = 0
                                 THEN GL.BranchID
                                 ELSE NULL
                            END AS BranchID
                    FROM    @GeneralLedger GL
                            INNER JOIN @AccountBalance A ON GL.AccountNumber = A.AccountNumber
                    GROUP BY GL.AccountNumber ,
                            A.AccountCategoryKind ,
                            CASE WHEN A.IsDetailByAO = 1
                                 THEN GL.AccountObjectID
                                 ELSE NULL
                            END ,
                            A.IsDetailByAO
                            ,
                            CASE WHEN A.IsDetailByAO = 1
                                      AND @IsSimilarBranch = 0
                                 THEN GL.BranchID
                                 ELSE NULL
                            END
            OPTION  ( RECOMPILE )
            UPDATE  @Balance
            SET     OpeningCredit = -OpeningDebit ,
                    OpeningDebit = 0
            WHERE   AccountCategoryKind = 1
                    OR ( AccountCategoryKind NOT IN ( 0, 1 )
                         AND OpeningDebit < 0
                       )
			
            UPDATE  @Balance
            SET     ClosingCredit = -ClosingDebit ,
                    ClosingDebit = 0
            WHERE   AccountCategoryKind = 1
                    OR ( AccountCategoryKind NOT IN ( 0, 1 )
                         AND ClosingDebit < 0
                       )	
	
            DECLARE @tblMasterDetail TABLE
                (
                  ItemID UNIQUEIDENTIFIER ,
                  DetailItemID UNIQUEIDENTIFIER ,
                  OperationSign INT ,
                  Grade INT ,
                  OpeningAmount DECIMAL(25, 4) ,
                  ClosingAmount DECIMAL(25, 4)
                )	
	
	
            INSERT  INTO @tblMasterDetail
                    SELECT  I.ItemID ,
                            NULL ,
                            1 ,
                            -1
                            ,
                            SUM(( CASE I.OperandString
                                    WHEN 'DUNO'
                                    THEN
                                         CASE I.AccountKind
                                           WHEN 1 THEN 0
                                           WHEN 0
                                           THEN I.OpeningDebit
                                                - I.OpeningCredit
                                           ELSE CASE WHEN I.OpeningDebit
                                                          - I.OpeningCredit > 0
                                                     THEN I.OpeningDebit
                                                          - I.OpeningCredit
                                                     ELSE 0
                                                END
                                         END
                                         /*- Thêm điều kiện chọn theo năm hoặc khác năm.*/
                                    WHEN 'DUNO_ky_nam'
                                    THEN 
                                         CASE WHEN @isPrintByYear = 1
                                              THEN CASE I.AccountKind
                                                     WHEN 1 THEN 0
                                                     WHEN 0
                                                     THEN I.OpeningDebit
                                                          - I.OpeningCredit
                                                     ELSE CASE
                                                              WHEN I.OpeningDebit
                                                              - I.OpeningCredit > 0
                                                              THEN I.OpeningDebit
                                                              - I.OpeningCredit
                                                              ELSE 0
                                                          END
                                                   END
                                              ELSE 0
                                         END
                                    WHEN 'DUCO'
                                    THEN CASE I.AccountKind
                                           WHEN 0 THEN 0
                                           WHEN 1
                                           THEN I.OpeningCredit
                                                - I.OpeningDebit
                                           ELSE CASE WHEN I.OpeningCredit
                                                          - I.OpeningDebit > 0
                                                     THEN I.OpeningCredit
                                                          - I.OpeningDebit
                                                     ELSE 0
                                                END
                                         END
                                         /* Thêm điều kiện chọn theo năm hoặc khác năm.*/
                                    WHEN 'DUCO_ky_nam'
                                    THEN CASE WHEN @isPrintByYear = 1
                                              THEN CASE I.AccountKind
                                                     WHEN 0 THEN 0
                                                     WHEN 1
                                                     THEN I.OpeningCredit
                                                          - I.OpeningDebit
                                                     ELSE CASE
                                                              WHEN I.OpeningCredit
                                                              - I.OpeningDebit > 0
                                                              THEN I.OpeningCredit
                                                              - I.OpeningDebit
                                                              ELSE 0
                                                          END
                                                   END
                                              ELSE 0
                                         END
                                    WHEN 'Solieuchitieu421a_Ky_khac_nam'
                                    THEN CASE WHEN @isPrintByYear = 0
                                              THEN C.OpeningAmount
                                              ELSE 0
                                         END
                                    WHEN 'Solieuchitieu421b_Ky_khac_nam'
                                    THEN CASE WHEN @isPrintByYear = 0
                                              THEN C.OpeningAmount
                                              ELSE 0
                                         END
                                         /* - Thêm điều kiện chọn theo năm hoặc khác năm.*/
                                    ELSE CASE WHEN LEFT(I.OperandString, 4) = 'DUNO'
                                              THEN I.OpeningDebit
                                              ELSE I.OpeningCredit
                                         END
                                  END ) * I.OperationSign) AS OpeningAmount ,
                            SUM(( CASE I.OperandString
                                    WHEN 'DUNO'
                                    THEN CASE I.Accountkind
                                           WHEN 1 THEN 0
                                           WHEN 0
                                           THEN I.ClosingDebit
                                                - I.ClosingCredit
                                           ELSE CASE WHEN I.ClosingDebit
                                                          - I.ClosingCredit > 0
                                                     THEN I.ClosingDebit
                                                          - I.ClosingCredit
                                                     ELSE 0
                                                END
                                         END
                                         /*- Thêm điều kiện chọn theo năm hoặc khác năm.*/
                                    WHEN 'DUNO_ky_nam'
                                    THEN CASE WHEN @isPrintByYear = 1
                                              THEN CASE I.Accountkind
                                                     WHEN 1 THEN 0
                                                     WHEN 0
                                                     THEN I.ClosingDebit
                                                          - I.ClosingCredit
                                                     ELSE CASE
                                                              WHEN I.ClosingDebit
                                                              - I.ClosingCredit > 0
                                                              THEN I.ClosingDebit
                                                              - I.ClosingCredit
                                                              ELSE 0
                                                          END
                                                   END
                                              ELSE 0
                                         END
                                    WHEN 'DUCO'
                                    THEN CASE I.AccountKind
                                           WHEN 0 THEN 0
                                           WHEN 1
                                           THEN I.ClosingCredit
                                                - I.ClosingDebit
                                           ELSE CASE WHEN I.ClosingCredit
                                                          - I.ClosingDebit > 0
                                                     THEN I.ClosingCredit
                                                          - I.ClosingDebit
                                                     ELSE 0
                                                END
                                         END
                                         /*- Thêm điều kiện chọn theo năm hoặc khác năm.*/
                                    WHEN 'DUCO_ky_nam'
                                    THEN CASE WHEN @isPrintByYear = 1
                                              THEN CASE I.AccountKind
                                                     WHEN 0 THEN 0
                                                     WHEN 1
                                                     THEN I.ClosingCredit
                                                          - I.ClosingDebit
                                                     ELSE CASE
                                                              WHEN I.ClosingCredit
                                                              - I.ClosingDebit > 0
                                                              THEN I.ClosingCredit
                                                              - I.ClosingDebit
                                                              ELSE 0
                                                          END
                                                   END
                                              ELSE 0
                                         END
                                    WHEN 'Solieuchitieu421a_Ky_khac_nam'
                                    THEN CASE WHEN @isPrintByYear = 0
                                              THEN C.ClosingAmount
                                              ELSE 0
                                         END
                                    WHEN 'Solieuchitieu421b_Ky_khac_nam'
                                    THEN CASE WHEN @isPrintByYear = 0
                                              THEN C.ClosingAmount
                                              ELSE 0
                                         END
                                         /* - Thêm điều kiện chọn theo năm hoặc khác năm.*/
                                    ELSE CASE WHEN LEFT(I.OperandString, 4) = 'DUNO'
                                              THEN I.ClosingDebit
                                              ELSE I.ClosingCredit
                                         END
                                  END ) * I.OperationSign) AS ClosingAmount
                    FROM    ( SELECT    I.ItemID ,
                                        I.OperandString ,
                                        I.OperationSign ,
                                        I.AccountNumber ,
                                        I.AccountKind ,
                                        SUM(B.OpeningDebit) AS OpeningDebit ,
                                        SUM(B.OpeningCredit) AS OpeningCredit ,
                                        SUM(B.ClosingDebit) AS ClosingDebit ,
                                        SUM(B.ClosingCredit) AS ClosingCredit
                              FROM      @tblItem I
                                        INNER JOIN @Balance B ON B.AccountNumber LIKE I.AccountNumberPercent
                                                              AND B.IsDetailByAO = I.IsDetailByAO
                              GROUP BY  I.ItemID ,
                                        I.OperandString ,
                                        I.OperationSign ,
                                        I.AccountNumber ,
                                        I.AccountKind
                            ) I
                            /*- Thêm điều kiện chọn theo năm hoặc khác năm.*/
                            LEFT JOIN @GeneralLedger421 C ON I.OperandString = C.OperandString
                    GROUP BY I.ItemID			
	
            INSERT  @tblMasterDetail
                    SELECT  ItemID ,
                            x.r.value('@ItemID', 'NVARCHAR(100)') ,
                            x.r.value('@OperationSign', 'INT') ,
                            0 ,
                            0.0 ,
                            0.0
                    FROM    dbo.FRTemplate
                            CROSS APPLY Formula.nodes('/root/MasterFormula')
                            AS x ( r )
                    WHERE   AccountingSystem = @AccountingSystem
                            AND ReportID = @ReportID
                            AND FormulaType = 1
                            AND Formula IS NOT NULL 
	
	;
            WITH    V ( ItemID, DetailItemID, OpeningAmount, ClosingAmount, OperationSign )
                      AS ( SELECT   ItemID ,
                                    DetailItemID ,
                                    OpeningAmount ,
                                    ClosingAmount ,
                                    OperationSign
                           FROM     @tblMasterDetail
                           WHERE    Grade = -1
                           UNION ALL
                           SELECT   B.ItemID ,
                                    B.DetailItemID ,
                                    V.OpeningAmount ,
                                    V.ClosingAmount ,
                                    B.OperationSign * V.OperationSign AS OperationSign
                           FROM     @tblMasterDetail B ,
                                    V
                           WHERE    B.DetailItemID = V.ItemID
                         )
	INSERT  @Result
            SELECT  FR.ItemID ,
                    FR.ItemCode ,
                    FR.ItemName ,
                    FR.ItemNameEnglish ,
                    FR.ItemIndex ,
                    FR.Description ,
                    FR.FormulaType ,
                    CASE WHEN FR.FormulaType = 1 THEN FR.FormulaFrontEnd
                         ELSE ''
                    END AS FormulaFrontEnd ,
                    CASE WHEN FR.FormulaType = 1 THEN FR.Formula
                         ELSE NULL
                    END AS Formula ,
                    FR.Hidden ,
                    FR.IsBold ,
                    FR.IsItalic ,
                    FR.Category ,
                    -1 AS SortOrder ,
                    ISNULL(X.Amount, 0) AS Amount ,
                    ISNULL(X.PrevAmount, 0) AS PrevAmount
            FROM    ( SELECT    ItemID ,
                                SUM(V.OperationSign * V.OpeningAmount) AS PrevAmount ,
                                SUM(V.OperationSign * V.ClosingAmount) AS Amount
                      FROM      V
                      GROUP BY  ItemID
                    ) AS X
                    RIGHT JOIN FRTemplate FR ON FR.ItemID = X.ItemID
            WHERE   AccountingSystem = @AccountingSystem
                    AND ReportID = @ReportID
	
            RETURN 
        END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

/*

*/
ALTER PROCEDURE [dbo].[Proc_GetB01_DN]
      (
      @BranchID UNIQUEIDENTIFIER ,
      @IncludeDependentBranch BIT ,
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @IsSimilarBranch BIT
      ,
      @IsB01bDNN BIT ,
      @isPrintByYear BIT
      ,
      @PrevFromDate DATETIME
      ,
      @PrevToDate DATETIME
    )
AS
BEGIN
  
SELECT * FROM [dbo].[Func_GetB01_DN] (
    @BranchID
  ,@IncludeDependentBranch
  ,@FromDate
  ,@ToDate
  ,@IsSimilarBranch
  ,@IsB01bDNN
  ,@isPrintByYear
  ,@PrevFromDate
  ,@PrevToDate)
order by ItemIndex
end


GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_PO_DiaryBook]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @BranchID UNIQUEIDENTIFIER = NULL ,
    @IncludeDependentBranch BIT = 0 ,
    @IsNotPaid BIT = 0
AS 
    BEGIN
        
        DECLARE @listRefType NVARCHAR(MAX)
        IF @IsNotPaid = 1
            SET @listRefType = '302,312,318,324,330,352,362,368,374' 
        ELSE 
            SET @listRefType = '302,307,308,309,310,312,313,314,315,316,318,319,320,321,322,324,325,326,327,328,330,331,332,333,334,352,357,358,359,360,362,363,364,365,366,368,369,370,371,372,374,375,376,377,378' 
           
        SELECT  ROW_NUMBER() OVER ( ORDER BY PL.PostedDate , PL.RefDate , PL.RefNo ) AS RowNum ,
                PL.ReferenceID ,
                TypeID ,
                PL.PostedDate ,
                PL.RefDate ,
                PL.RefNo ,
                PL.InvoiceDate ,
                PL.InvoiceNo ,
                PL.Description ,
                SUM(CASE WHEN LEFT(Account, 3) = N'156'
                              
                         THEN DebitAmount
                         ELSE $0
                    END) AS GoodsAmount ,
                SUM(CASE WHEN LEFT(Account, 3) = N'152'
                         THEN ( DebitAmount )
                         ELSE $0
                    END) AS EquipmentAmount ,
                ( CASE WHEN LEFT(Account, 3) <> N'152'
                            AND LEFT(Account, 3) <> N'156'
                            AND LEFT(Account, 3) <> N'1331'
                       THEN Account
                       ELSE NULL
                  END ) AS AnotherAccount , 
                SUM(CASE WHEN LEFT(Account, 3) <> N'152'
                              AND LEFT(Account, 3) <> N'156'
                              AND LEFT(Account, 3) <> N'1331'
                         THEN (DebitAmount)
                         ELSE $0
                    END) AS AnotherAmount , 
                SUM(CASE WHEN LEFT(Account, 3) = N'156'
                         THEN DebitAmount
                         ELSE $0
                    END
                    + CASE WHEN LEFT(Account, 3) = N'152'
                         THEN ( DebitAmount )
                         ELSE $0
                    END
                    + CASE WHEN LEFT(Account, 3) <> N'152'
                              AND LEFT(Account, 3) <> N'156'
                              AND LEFT(Account, 3) <> N'1331'
                         THEN (DebitAmount)
                         ELSE $0
                    END) AS PaymentableAmount ,
                AO.AccountingObjectCode , 
                CASE WHEN PL.TypeID IN ( 330, 331, 332, 333, 334, 352, 357,
                                          358, 359, 360, 362, 363, 364, 365,
                                          366, 368, 369, 370, 371, 372, 374,
                                          375, 376, 377, 378 )
                     THEN null 
                     ELSE AO.AccountingObjectName
                END AS AccountObjectNameDI 
        FROM    dbo.GeneralLedger AS PL,
		        dbo.AccountingObject as AO
        WHERE   PL.PostedDate BETWEEN @FromDate AND @ToDate
		and PL.AccountingObjectID = Ao.ID
                AND @listRefType LIKE N'%' + CAST(PL.TypeID AS NVARCHAR(4))
                + N'%'

        GROUP BY PL.ReferenceID ,
                PL.TypeID ,
                PL.PostedDate ,
                PL.RefDate ,
                PL.RefNo ,
                PL.InvoiceDate ,
                PL.InvoiceNo ,
                PL.Description ,
                AO.AccountingObjectCode ,
				 ( CASE WHEN LEFT(Account, 3) <> N'152'
                            AND LEFT(Account, 3) <> N'156'
                            AND LEFT(Account, 3) <> N'1331'
                       THEN Account
                       ELSE NULL
                  END ),
				CASE WHEN PL.TypeID IN ( 330, 331, 332, 333, 334, 352, 357,
                                          358, 359, 360, 362, 363, 364, 365,
                                          366, 368, 369, 370, 371, 372, 374,
                                          375, 376, 377, 378 )
                     THEN null
                     ELSE AO.AccountingObjectName
                END 
        HAVING  sum(CASE WHEN LEFT(Account, 3) = N'156'
                              
                         THEN DebitAmount
                         ELSE $0
                    END) <> 0
                OR SUM(CASE WHEN LEFT(Account, 3) = N'152'
                              
                         THEN ( DebitAmount )
                         ELSE $0
                    END) <> 0
                OR SUM(CASE WHEN LEFT(Account, 3) <> N'152'
                              AND LEFT(Account, 3) <> N'156'
                              AND LEFT(Account, 3) <> N'1331'
                         THEN (DebitAmount)
                         ELSE $0
                    END) <> 0
        ORDER BY PL.PostedDate ,
                PL.RefDate ,
                PL.RefNo           
    END
    


GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_GL_GetGLAccountLedgerDiaryBook]
    @BranchID UNIQUEIDENTIFIER,
    @IncludeDependentBranch BIT,
    @StartDate DATETIME,
    @FromDate DATETIME,
    @ToDate DATETIME,    
    @AccountNumber NVARCHAR(MAX),
    @IsSimilarSum BIT,
	@IsSummaryChildAccount BIT
AS
    BEGIN    
        SET NOCOUNT ON       
        CREATE TABLE #Result
            (
             KeyID NVARCHAR(100)COLLATE SQL_Latin1_General_CP1_CI_AS,
             RefID UNIQUEIDENTIFIER,
             RefType INT,
             PostedDate DATETIME,
             RefNo NVARCHAR(25)COLLATE SQL_Latin1_General_CP1_CI_AS,
             RefDate DATETIME,
             InvDate DATETIME,
             InvNo NVARCHAR(MAX)COLLATE SQL_Latin1_General_CP1_CI_AS, 
             JournalMemo NVARCHAR(255)COLLATE SQL_Latin1_General_CP1_CI_AS,
             AccountNumber NVARCHAR(25)COLLATE SQL_Latin1_General_CP1_CI_AS,
			 DetailAccountNumber NVARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS,
             AccountCategoryKind INT,
             AccountName NVARCHAR(255)COLLATE SQL_Latin1_General_CP1_CI_AS,
             CorrespondingAccountNumber NVARCHAR(25)COLLATE SQL_Latin1_General_CP1_CI_AS,
             DebitAmount MONEY,
             CreditAmount MONEY,
             OrderType INT,
             IsBold BIT,
             OrderNumber int
            )
        CREATE TABLE #tblAccountNumber
            (
             AccountNumber NVARCHAR(25)  COLLATE SQL_Latin1_General_CP1_CI_AS  PRIMARY KEY,
             AccountName NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
             AccountNumberPercent NVARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS ,
             AccountCategoryKind INT,
			 IsParent BIT
            )
			
       
	
        INSERT  #tblAccountNumber
                SELECT  A1.AccountNumber,
                        A1.AccountName,
                        A1.AccountNumber + '%',
                        A1.AccountGroupKind,
						A1.IsParentNode
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountNumber,',') F
                        INNER JOIN dbo.Account A1 ON A1.AccountNumber = F.Value
	            					
               
        IF @IsSimilarSum = 0
            INSERT  #Result
                    (KeyID,
                     RefID,
                     RefType,
                     PostedDate,
                     RefNo,
                     RefDate,
                     InvDate,
                     InvNo,
                     JournalMemo,
                     AccountNumber,
					 DetailAccountNumber,
                     AccountName,
                     CorrespondingAccountNumber,
                     AccountCategoryKind,
                     DebitAmount,
                     CreditAmount,
                     ORDERType,
                     IsBold,
                     OrderNumber
					
                    )
                    SELECT 
							convert(nvarchar(100),GL.ID)  + '-' + AC.AccountNumber,
                            GL.ReferenceID,
                            GL.TypeID,
                            GL.PostedDate,
                            RefNo,
                            GL.RefDate,
                            Gl.InvoiceDate,
                            GL.InvoiceNo,
                            GL.Description AS JournalMemo,
                            AC.AccountNumber,
							GL.Account AS DetailAccountNumber,
                            AC.AccountName,
                            GL.AccountCorresponding,
                            AC.AccountCategoryKind,
                            DebitAmount,
                            CreditAmount,
                            1 AS ORDERType,
                            0,
                            CASE WHEN (AC.AccountNumber LIKE N'11%' OR AC.AccountNumber LIKE N'15%' AND AC.AccountNumber NOT LIKE N'154%') 
											AND GL.DebitAmount <> 0
                                             THEN 0
                                             ELSE 1
                                        END AS OrderNumber
                    FROM    dbo.GeneralLedger AS GL
                            INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                    WHERE   GL.PostedDate BETWEEN @FromDate AND @ToDate
                            AND (GL.DebitAmount <> 0
                                 OR GL.CreditAmount <> 0
                                 or GL.DebitAmountOriginal <> 0
                                 OR GL.CreditAmountOriginal <> 0
                                )                           
                     
        ELSE
			/*Nếu tick Hiển thị phát sinh theo tiết khoản của TK tổng hợp thì lấy lên chi tiết tài khoản và group by (CR 13084)*/
			INSERT  INTO #Result
						(KeyID,
						 RefID,
						 RefType,
						 PostedDate,
						 RefNo,
						 RefDate,
						 InvDate,
						 InvNo,
						 JournalMemo,
						 AccountNumber,
						 DetailAccountNumber,
						 AccountName,
						 CorrespondingAccountNumber,
						 AccountCategoryKind,
						 DebitAmount,
						 CreditAmount,
						 ORDERType,
						 IsBold,
						 OrderNumber
					
						)
						SELECT  CAST(MAX(GL.ID) AS NVARCHAR(20))
								+ '-' + AC.AccountNumber,
								GL.ReferenceID,
								GL.TypeID,
								GL.PostedDate,
								RefNo,
								GL.RefDate,
								Gl.InvoiceDate,
								GL.InvoiceNo,
								GL.Description AS JournalMemo,							
								AC.AccountNumber,
								CASE WHEN @IsSummaryChildAccount = 1 THEN GL.Account
									 ELSE NULL 
								END AS DetailAccountNumber,
								AC.AccountName,
								GL.AccountCorresponding,
								AC.AccountCategoryKind,
								SUM(GL.DebitAmount) AS DebitAmount,
								SUM(GL.CreditAmount) AS CreditAmount,
								1 AS ORDERType,
								0 AS IsBold,
								 CASE WHEN (AC.AccountNumber LIKE N'11%' OR AC.AccountNumber LIKE N'15%' AND AC.AccountNumber NOT LIKE N'154%') 
										   AND SUM(GL.DebitAmount) <> 0
										   THEN 0
										   ELSE 1
								  END AS OrderNumber
						FROM    dbo.GeneralLedger AS GL
								INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
						WHERE   (GL.PostedDate BETWEEN @FromDate AND @ToDate)                        
						GROUP BY GL.ReferenceID,
								GL.TypeID,
								GL.PostedDate,
								GL.RefNo,
								GL.RefDate,
								Gl.InvoiceDate,
								GL.InvoiceNo,
								GL.Description,
								AC.AccountNumber,
								CASE WHEN @IsSummaryChildAccount = 1 THEN GL.Account 
									 ELSE NULL 
								END,
								AC.AccountName,
								GL.AccountCorresponding,
								AC.AccountCategoryKind
						HAVING  SUM(GL.DebitAmount) <> 0
								OR SUM(GL.CreditAmount) <> 0 
								OR SUM(GL.DebitAmountOriginal) <> 0
								OR SUM(GL.CreditAmountOriginal) <> 0		
       
        SELECT  AccountNumber,
                AccountName,
				IsParent,
                SUM(OpenningDebitAmount) AS OpenningDebitAmount,
                SUM(OpenningCreditAmount) AS OpenningCreditAmount,
                SUM(ClosingDebitAmount) AS ClosingDebitAmount,
                SUM(ClosingCreditAmount) AS ClosingCreditAmount,
                SUM(AccumDebitAmount) AS AccumDebitAmount,
                SUM(AccumCreditAmount) AS AccumCreditAmount,
				AccountCategoryKind
        FROM    (
                 SELECT AC.AccountNumber,
                        AC.AccountName,
                        CASE WHEN AC.AccountCategoryKind = 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             WHEN ac.AccountCategoryKind = 2
                                  AND SUM(GL.DebitAmount - GL.CreditAmount) > 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             ELSE 0
                        END AS OpenningDebitAmount,
                        CASE WHEN AC.AccountCategoryKind = 1
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             WHEN AC.AccountCategoryKind = 2
                                  AND SUM(GL.CreditAmount - GL.DebitAmount) > 0
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             ELSE 0
                        END AS OpenningCreditAmount,
                        0 AS ClosingDebitAmount,
                        0 AS ClosingCreditAmount,
                        0 AS AccumDebitAmount,
                        0 AS AccumCreditAmount,
						AC.IsParent,
						AC.AccountCategoryKind
                 FROM   dbo.GeneralLedger GL
                        INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                 WHERE  GL.PostedDate < @FromDate                       
                 GROUP BY GL.BranchID,
                        AC.AccountNumber,
                        AC.AccountName,
                        AC.AccountCategoryKind,
						AC.IsParent
                 UNION ALL
                 SELECT AC.AccountNumber,
                        AC.AccountName,
                        0 AS OpenningDebitAmount,
                        0 AS OpenningCreditAmount,
                        CASE WHEN AC.AccountCategoryKind = 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             WHEN ac.AccountCategoryKind = 2
                                  AND SUM(GL.DebitAmount - GL.CreditAmount) > 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             ELSE 0
                        END AS ClosingDebitAmount,
                        CASE WHEN AC.AccountCategoryKind = 1
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             WHEN AC.AccountCategoryKind = 2
                                  AND SUM(GL.CreditAmount - GL.DebitAmount) > 0
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             ELSE 0
                        END AS ClosingCreditAmount,
                        0 AS AccumDebitAmount,
                        0 AS AccumCreditAmount,
						AC.IsParent,
						AC.AccountCategoryKind
                 FROM   dbo.GeneralLedger GL
                        INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                 WHERE  GL.PostedDate <= @ToDate                        
                 GROUP BY AC.AccountNumber,
                        AC.AccountName,
                        AC.AccountCategoryKind,
						AC.IsParent
                 UNION ALL
                 SELECT AC.AccountNumber,
                        AC.AccountName,
                        0 AS OpenningDebitAmount,
                        0 AS OpenningCreditAmount,
                        0 AS ClosingDebitAmount,
                        0 AS ClosingCreditAmount,
                        SUM(GL.DebitAmount) AS AccumDebitAmount,
                        SUM(GL.CreditAmount) AS AccumCreditAmount,
						AC.IsParent,
						AC.AccountCategoryKind
                 FROM   dbo.GeneralLedger GL
                        INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                 WHERE  GL.PostedDate BETWEEN @StartDate AND @ToDate                        
                 GROUP BY AC.AccountNumber,
                        AC.AccountName,
                        AC.AccountCategoryKind,
						AC.IsParent
                ) AS A
        GROUP BY AccountNumber,
                AccountName,
				IsParent ,
				AccountCategoryKind         
        HAVING
				SUM(OpenningDebitAmount) <>0 OR
                SUM(OpenningCreditAmount)  <>0 OR
                SUM(ClosingDebitAmount)  <>0 OR
                SUM(ClosingCreditAmount) <>0 OR
                SUM(AccumDebitAmount) <>0 OR
                SUM(AccumCreditAmount) <>0 
        ORDER BY AccountNumber
        		
		SELECT  ROW_NUMBER() OVER (ORDER BY AccountNumber,DetailAccountNumber, OrderType, PostedDate, RefDate,OrderNumber, RefNo) AS RowNum,
				KeyID,
				RefID,
				RefType,
				PostedDate,
				RefDate,
				RefNo,
				InvDate,
				InvNo,
				JournalMemo,
				AccountNumber,
				DetailAccountNumber,
				CorrespondingAccountNumber,
				DebitAmount,
				CreditAmount,
				IsBold,
				AccountCategoryKind
		FROM    #Result
			
              
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*





*/
ALTER PROCEDURE [dbo].[Proc_GL_GetBookDetailPaymentByAccountNumber]
    @BranchID UNIQUEIDENTIFIER ,
    @IncludeDependentBranch BIT ,
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountNumber NVARCHAR(20) ,
	@CurrencyId NVARCHAR(10) ,
    @GroupTheSameItem BIT
AS
    BEGIN

        CREATE  TABLE #Result
            (
              RowNum INT IDENTITY(1, 1) PRIMARY KEY,
              RefID UNIQUEIDENTIFIER ,
              PostedDate DATETIME ,
              RefDate DATETIME ,
              RefNo NVARCHAR(25) ,
              InvDate DATETIME ,
              InvNo NVARCHAR(25) ,
              RefType INT ,
              JournalMemo NVARCHAR(255) ,
              AccountNumber NVARCHAR(25),
              CorrespondingAccountNumber NVARCHAR(25) ,
              DebitAmount DECIMAL(22,4) ,
              CreditAmount DECIMAL(22,4) ,
              ClosingDebitAmount DECIMAL(22,4) ,
              ClosingCreditAmount DECIMAL(22,4) ,
              AccountObjectCode NVARCHAR(25) ,
              AccountObjectName NVARCHAR(255) ,
              OrderType INT , 
              IsBold BIT ,
              BranchName NVARCHAR(128)
            )      
       
        
        DECLARE @tblAccountNumber TABLE
            (              
			  AccountNumber  NVARCHAR(25),  
              AccountNumberPercent NVARCHAR(25)              
            )
        INSERT  @tblAccountNumber
                SELECT  A1.AccountNumber,
                        A1.AccountNumber + '%'
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountNumber,',') F
                        INNER JOIN dbo.Account A1 ON A1.AccountNumber = F.Value
        IF @GroupTheSameItem = 1
            BEGIN
                INSERT  #Result					
				SELECT
					RefID ,
                    PostedDate ,
                    RefDate ,
                    RefNo ,
                    InvDate ,
                    InvNo ,
                    RefType ,
                    JournalMemo ,
                    AccountNumber,
                    CorrespondingAccountNumber ,
                    ISNULL(DebitAmount,0) ,
                    ISNULL(CreditAmount,0) ,
                    ISNULL(ClosingDebitAmount,0),
                    ISNULL(ClosingCreditAmount,0),
                    AccountingObjectCode,
                    AccountObjectName,
                    OrderType ,
                    IsBold ,
                    BranchName	
                 FROM
					(SELECT  
								NULL as RefID ,
                                NULL as PostedDate ,
                                NULL as RefDate ,
                                NULL as RefNo ,
                                NULL as InvDate ,
                                NULL as InvNo ,
                                NULL as RefType ,
                                N'Số dư đầu kỳ' as JournalMemo ,
                                A.AccountNumber,
                                NULL as CorrespondingAccountNumber ,
								$0 AS DebitAmount ,
								$0 AS CreditAmount ,
								CASE WHEN SUM(GL.DebitAmount - GL.CreditAmount) > 0
									 THEN SUM(GL.DebitAmount - GL.CreditAmount)
									 ELSE 0
								END AS ClosingDebitAmount ,
								CASE WHEN SUM(GL.CreditAmount - GL.DebitAmount) > 0
									 THEN SUM(GL.CreditAmount - GL.DebitAmount)
									 ELSE 0
								END AS ClosingCreditAmount ,
                                null as AccountingObjectCode,
                                null as AccountObjectName,
                                0 AS OrderType ,
                                1 As isBold,
                                null as BranchName
                                ,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 As EntryType
                            FROM    dbo.GeneralLedger AS GL
							INNER JOIN @tblAccountNumber A ON GL.Account LIKE A.AccountNumberPercent
							WHERE   ( GL.PostedDate < @FromDate )  
							and GL.CurrencyID = @CurrencyId                    
							GROUP BY 
								A.AccountNumber
							HAVING  SUM(GL.DebitAmount - GL.CreditAmount) <> 0
						UNION ALL
                        SELECT  GL.ReferenceID ,
                                GL.PostedDate ,
                                GL.RefDate ,
                                GL.RefNo ,
                                gl.InvoiceDate ,
                                gl.InvoiceNo ,
                                GL.TypeID ,
                                GL.Description,
                                A.AccountNumber,
                                GL.AccountCorresponding ,
                                SUM(GL.DebitAmount) AS DebitAmount ,
                                SUM(GL.CreditAmount) AS CreditAmount ,
                                $0 AS ClosingDebitAmount ,
                                $0 AS ClosingCreditAmount ,
                                null as AccountingObjectCode,
                                null as AccountObjectName,
                                1 AS OrderType ,
                                0 As isBold,
                                '' as BranchName
								,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 As EntryType
                        FROM    dbo.GeneralLedger AS GL
                                INNER JOIN @tblAccountNumber A ON GL.Account LIKE A.AccountNumberPercent
                        WHERE   ( GL.PostedDate BETWEEN @FromDate AND @ToDate )    
						and GL.CurrencyID = @CurrencyId 
						and GL.AccountCorresponding <> ''      
                        GROUP BY GL.ReferenceID ,
                                GL.PostedDate ,
                                GL.RefDate ,
                                GL.RefNo ,
                                gl.InvoiceDate ,
                                gl.InvoiceNo ,
                                GL.TypeID ,
                                GL.Description ,
                                A.AccountNumber,
                                GL.AccountCorresponding 
                        HAVING  SUM(GL.DebitAmount) <> 0 OR SUM(GL.CreditAmount) <> 0                       
                      )T
                      ORDER BY 
						AccountNumber,
                        OrderType ,
                        postedDate ,
                        RefDate ,
                        RefNo      
                        ,SortOrder,DetailPostOrder,EntryType            
            END
            
        ELSE
            BEGIN             
                
                INSERT  #Result					
				SELECT
					RefID ,
                    PostedDate ,
                    RefDate ,
                    RefNo ,
                    InvDate ,
                    InvNo ,
                    RefType ,
                    JournalMemo ,
                    AccountNumber,
                    CorrespondingAccountNumber ,
                    DebitAmount ,
                    CreditAmount ,
                    ClosingDebitAmount ,
                    ClosingCreditAmount ,
                    AccountingObjectCode,
                    AccountObjectName,
                    OrderType ,
                    IsBold ,
                    BranchName	
                 FROM
					(SELECT  
								null as RefID ,
                                NULL as PostedDate ,
                                NULL as RefDate ,
                                NULL as RefNo ,
                                NULL as InvDate ,
                                NULL as InvNo ,
                                NULL as RefType ,
                                N'Số dư đầu kỳ' as JournalMemo ,
                                A.AccountNumber,
                                null as CorrespondingAccountNumber ,
                                $0 AS DebitAmount ,
								$0 AS CreditAmount ,
								CASE WHEN SUM(GL.DebitAmount - GL.CreditAmount) > 0
									 THEN SUM(GL.DebitAmount - GL.CreditAmount)
									 ELSE 0
								END AS ClosingDebitAmount ,
								CASE WHEN SUM(GL.CreditAmount - GL.DebitAmount) > 0
									 THEN SUM(GL.CreditAmount - GL.DebitAmount)
									 ELSE 0
								END AS ClosingCreditAmount ,
								null as AccountingObjectCode,
                                null as AccountObjectName,
                                0 AS OrderType ,
                                1 As isBold,
                                null as BranchName
                                ,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 as EntryType
                            FROM    dbo.GeneralLedger AS GL
							INNER JOIN @tblAccountNumber A ON GL.Account LIKE A.AccountNumberPercent
							WHERE   ( GL.PostedDate < @FromDate )   
							and GL.CurrencyID = @CurrencyId                    
							GROUP BY 
								A.AccountNumber
							HAVING  SUM(GL.DebitAmount - GL.CreditAmount) <> 0
						UNION ALL
                        SELECT  GL.ReferenceID ,
                                GL.PostedDate ,
                                GL.RefDate ,
                                GL.RefNo ,
                                gl.InvoiceDate ,
                                gl.InvoiceNo ,
                                GL.TypeID ,
                                CASE WHEN GL.DESCRIPTION IS NULL
                                          OR LTRIM(GL.Description) = ''
                                     THEN GL.Reason
                                     ELSE GL.Description
                                END AS JournalMemo ,
                                A.AccountNumber,
                                GL.AccountCorresponding ,
                                GL.DebitAmount ,
                                GL.CreditAmount ,
                                $0 AS ClosingDebitAmount ,
                                $0 AS ClosingCreditAmount ,
								null as AccountingObjectCode,
                                null as AccountObjectName,
                                1 AS OrderType ,
                                0 ,
                                null BranchName
								,null
								,null
								,null
                        FROM    dbo.GeneralLedger AS GL
                                INNER JOIN @tblAccountNumber A ON GL.Account LIKE A.AccountNumberPercent
                        WHERE   (GL.PostedDate BETWEEN @FromDate AND @ToDate )                              
                                AND ( GL.DebitAmount <> 0
                                      OR GL.CreditAmount <> 0
                                    )   	
									and GL.CurrencyID = @CurrencyId	
									and GL.AccountCorresponding <> ''                     
                      )T
                      ORDER BY 
						AccountNumber,
                        OrderType ,
                        postedDate ,
                        RefDate ,
                        RefNo,SortOrder,DetailPostOrder,EntryType                                                          
               
            END         
       
        DECLARE @AccountObjectCode NVARCHAR(25)
			 ,@AccountNumber1 NVARCHAR(25)
			 ,@ClosingDebitAmount DECIMAL(22,4)
			 
		SELECT @ClosingDebitAmount = 0
		
        UPDATE  #Result
        SET     @ClosingDebitAmount = @ClosingDebitAmount + ISNULL(ClosingDebitAmount,0) - 
		                              ISNULL(ClosingCreditAmount,0) + ISNULL(DebitAmount,0) - ISNULL(CreditAmount,0),
		
                ClosingDebitAmount = CASE WHEN @ClosingDebitAmount > 0
                                          THEN @ClosingDebitAmount
                                          ELSE 0
                                     END ,
                ClosingCreditAmount = CASE WHEN @ClosingDebitAmount < 0
                                           THEN - @ClosingDebitAmount
                                           ELSE 0
                                      END 
        DECLARE @DebitAmount MONEY  
		DECLARE @CreditAmount MONEY 
		DECLARE @ClosingDeditAmount_SDDK MONEY 
		DECLARE @ClosingCreditAmount_SDDK MONEY 
	    select @DebitAmount = sum(DebitAmount),
		       @CreditAmount = sum(CreditAmount)
		from #Result Res
		INNER JOIN @tblAccountNumber A ON Res.AccountNumber LIKE A.AccountNumberPercent
		where OrderType = 1
		select @ClosingDeditAmount_SDDK = sum(ClosingDebitAmount),
		       @ClosingCreditAmount_SDDK = sum(ClosingCreditAmount)
		from #Result where OrderType = 0
        SELECT  RowNum ,
                RefID ,
                AccountObjectCode ,
                AccountObjectName ,
                AccountNumber,
                PostedDate ,
                RefDate ,
                RefNo ,
                InvDate ,
                InvNo ,
                RefType ,
                JournalMemo ,
                CorrespondingAccountNumber ,
                DebitAmount ,
                CreditAmount ,
                ClosingDebitAmount ,
                ClosingCreditAmount ,
               
                OrderType AS InvoiceOrder ,
                IsBold ,
                BranchName
        FROM    #Result
        ORDER BY RowNum
		select 
		null as RefID ,
                                NULL as PostedDate ,
                                NULL as RefDate ,
                                NULL as RefNo ,
                                NULL as InvDate ,
                                NULL as InvNo ,
                                NULL as RefType ,
                                N'Số dư cuối kỳ' as JournalMemo ,
                                AccountNumber,
                                null as CorrespondingAccountNumber ,
                                $0 DebitAmount ,
								$0 CreditAmount ,
								sum(ClosingDebitAmount) + sum(sumDebitAmount) - sum(sumCreditAmount) as ClosingDebitAmount,
								$0 AS ClosingCreditAmount ,
								null as AccountingObjectCode,
                                null as AccountObjectName,
                                2 AS OrderType ,
                                2 As isBold,
                                null as BranchName
                                ,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 as EntryType
        from (SELECT  
								null as RefID ,
                                NULL as PostedDate ,
                                NULL as RefDate ,
                                NULL as RefNo ,
                                NULL as InvDate ,
                                NULL as InvNo ,
                                NULL as RefType ,
                                N'Số dư cuối kỳ' as JournalMemo ,
                                A.AccountNumber,
                                null as CorrespondingAccountNumber ,
                                $0 AS DebitAmount ,
								$0 AS CreditAmount ,
								CASE WHEN GL.OrderType = 0 and gl.IsBold = 1 THEN sum(ClosingDebitAmount)
									 ELSE 0 
								END
								AS ClosingDebitAmount,
								sum(DebitAmount) sumDebitAmount,
								sum(CreditAmount) sumCreditAmount,
								0 AS ClosingCreditAmount ,
								null as AccountingObjectCode,
                                null as AccountObjectName,
                                2 AS OrderType ,
                                2 As isBold,
                                null as BranchName
                                ,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 as EntryType
                            FROM    #Result AS GL
							INNER JOIN @tblAccountNumber A ON GL.AccountNumber LIKE A.AccountNumberPercent                 
							GROUP BY OrderType,IsBold,
								A.AccountNumber) as A1
        group by AccountNumber
        DROP TABLE #Result 
            
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_FU_GetCashDetailBook]
    (
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @AccountNumber NVARCHAR(20) ,
      @BranchID UNIQUEIDENTIFIER ,
      @CurrencyID NVARCHAR(3)
    )
AS 
    BEGIN

        CREATE  TABLE #Result
            (
              RowNum INT IDENTITY(1, 1)
                         PRIMARY KEY ,
              RefID UNIQUEIDENTIFIER ,
			  AccountNumber NVARCHAR(15),
              PostedDate DATETIME ,
              RefDate DATETIME ,
              ReceiptRefNo NVARCHAR(25) ,
              PaymentRefNo NVARCHAR(25) ,
              RefType INT ,
              JournalMemo NVARCHAR(255) ,
              CorrespondingAccountNumber NVARCHAR(20) ,
              ExchangeRate Numeric ,
              DebitAmount Numeric ,
              DebitAmountOC Numeric ,
              CreditAmount Numeric ,
              CreditAmountOC Numeric ,
              ClosingAmount Numeric ,
              ClosingAmountOC Numeric ,
              ContactName NVARCHAR(255) ,
			  AccountObjectId NVARCHAR(255),
              AccountObjectCode NVARCHAR(255) ,
              AccountObjectName NVARCHAR(255) , 
              EmployeeCode NVARCHAR(255) ,
              EmployeeName NVARCHAR(255) , 
              InvoiceOrder INT , 
              BranchID UNIQUEIDENTIFIER ,
              BranchName NVARCHAR(255) ,
              IsBold BIT
            )
         DECLARE @tblAccountNumber TABLE
            (              
			  AccountNumber  NVARCHAR(25),  
              AccountNumberPercent NVARCHAR(25)              
            )
        INSERT  @tblAccountNumber
                SELECT  A1.AccountNumber,
                        A1.AccountNumber + '%'
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountNumber,',') F
                        INNER JOIN dbo.Account A1 ON A1.AccountNumber = F.Value
        DECLARE @AccountNumberPercent NVARCHAR(25)
        SET @AccountNumberPercent = @AccountNumber + '%'
        
        DECLARE @DiscountNumber NVARCHAR(25)       
        SET @DiscountNumber = '5211%'
   
        DECLARE @ClosingAmountOC MONEY
        DECLARE @ClosingAmount MONEY
   
 INSERT  #Result
	          (RefID ,
			  AccountNumber,
              PostedDate ,
              RefDate ,
              ReceiptRefNo  ,
              PaymentRefNo,
              RefType ,
              JournalMemo,
              ExchangeRate ,
              DebitAmount  ,
              DebitAmountOC  ,
              CreditAmount  ,
              CreditAmountOC  ,
              ClosingAmount  ,
              ClosingAmountOC  ,
              ContactName,
              AccountObjectId,
              InvoiceOrder  , 
              BranchID  ,
              BranchName,
			  CorrespondingAccountNumber)
			  select ReferenceID ,
			  Account,
              PostedDate ,
              RefDate ,
              ReceiptRefNo  ,
              PaymentRefNo,
              RefType ,
              JournalMemo,
              ExchangeRate ,
              DebitAmount  ,
              DebitAmountOC  ,
              CreditAmount  ,
              CreditAmountOC  ,
              ClosingAmount  ,
              ClosingAmountOC  ,
              ContactName,
              AccountObjectId,
              InvoiceOrder  , 
              BranchID  ,
              BranchName,
			  AccountCorresponding from 
			            (SELECT null as ReferenceID,
						    Acc.AccountNumber as Account,
							null as PostedDate,
							null as RefDate,
							null as ReceiptRefNo,
							null as PaymentRefNo,
							null as RefType,
					        N'Số tồn đầu kỳ' AS JournalMemo ,
                            $0 AS ExchangeRate ,
                            $0 AS DebitAmount ,
                            $0 AS DebitAmountOC ,
                            $0 AS CreditAmount ,
                            $0 AS CreditAmountOC ,
							SUM(GL.DebitAmount - GL.CreditAmount) as ClosingAmount,
							SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal) as ClosingAmountOC,
							null as ContactName,
							null as AccountObjectId,
                            0 AS InvoiceOrder ,
                            null BranchID ,
                            0 as BranchName,
                            null AccountCorresponding
							 from GeneralLedger gl
							 inner join @tblAccountNumber Acc on GL.Account = Acc.AccountNumber
							 where ( GL.PostedDate < @FromDate )
								AND ( @CurrencyID IS NULL
								OR GL.CurrencyID = @CurrencyID)
						    group by Acc.AccountNumber
				        union all
                        SELECT  GL.ReferenceID,
						        GL.Account,
                                GL.PostedDate ,
                                GL.RefDate ,
                                CASE WHEN GL.DebitAmount <> 0
                                          OR GL.DebitAmountOriginal <> 0
                                     THEN GL.RefNo
                                     ELSE ''
                                END AS ReceiptRefNo ,
                                CASE WHEN GL.CreditAmount <> 0
                                          OR GL.CreditAmountOriginal <> 0
                                     THEN GL.RefNo
                                     ELSE ''
                                END AS PaymentRefNo ,
                                GL.TypeID ,
								case when GL.Reason is null then
                                GL.Description 
								else GL.Reason 
								end as Description, 
                                GL.ExchangeRate ,
                                GL.DebitAmount ,
                                GL.DebitAmountOriginal ,
                                GL.CreditAmount ,
                                GL.CreditAmountOriginal ,
                                $0 AS ClosingAmount ,
                                $0 AS ClosingAmountOC ,
                                GL.ContactName ,
                                GL.AccountingObjectID ,
                                CASE WHEN GL.DebitAmount <> 0
                                          OR GL.DebitAmountOriginal <> 0 THEN 1
                                     WHEN GL.CreditAmount <> 0
                                          OR GL.CreditAmountOriginal <> 0 THEN 2
                                     ELSE 3
                                END AS InvoiceOrder ,
                                GL.BranchID ,
                                0,
                               GL.AccountCorresponding
                        FROM    dbo.GeneralLedger AS GL ,
						        @tblAccountNumber Acc
                        WHERE   ( GL.PostedDate BETWEEN @FromDate AND @ToDate )
                                AND GL.Account = Acc.AccountNumber
                                AND ( @CurrencyID IS NULL
                                      OR GL.CurrencyID = @CurrencyID
                                    )
                                AND ( GL.CreditAmount <> 0
                                      OR GL.DebitAmount <> 0
                                      OR GL.CreditAmountOriginal <> 0
                                      OR GL.DebitAmountOriginal <> 0
                                    )) T
                        ORDER BY Account,
						        PostedDate ,
                                RefDate ,
                                InvoiceOrder 

		
        SET @ClosingAmount = 0
        SET @ClosingAmountOC = 0
        UPDATE  #Result
        SET     @ClosingAmount = @ClosingAmount  + ClosingAmount +ISNULL(DebitAmount, 0)
                - ISNULL(CreditAmount, 0) ,
                ClosingAmount = @ClosingAmount ,
                @ClosingAmountOC = @ClosingAmountOC + ClosingAmountOC + ISNULL(DebitAmountOC, 0)
                - ISNULL(CreditAmountOC, 0) ,
                ClosingAmountOC = @ClosingAmountOC 
        SELECT  *
        FROM    #Result R   
		order by PostedDate, ReceiptRefNo, PaymentRefNo  
        DROP TABLE #Result 
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER FUNCTION [dbo].[Func_ConvertStringIntoTable]
    (
      @GUIString NVARCHAR(MAX),
      @SeparateCharacter NCHAR(1)
    )
RETURNS @ValueTable TABLE (Value UNIQUEIDENTIFIER)
AS BEGIN
 
    DECLARE @ValueListLength INT,
			@StartingPosition INT,
			@Value NVARCHAR(50),
			@SecondPosition INT
			
    SET @ValueListLength = LEN(@GUIString)
    SET @StartingPosition = 1
    SET @Value = SPACE(0)
 
	WHILE @StartingPosition < @ValueListLength
		BEGIN
			SET @SecondPosition = CHARINDEX(@SeparateCharacter,@GUIString,@StartingPosition+1)
			SET @Value = SUBSTRING(@GUIString,@StartingPosition+1,@SecondPosition-@StartingPosition-1)	
			SET @StartingPosition = @SecondPosition
			IF @Value <> SPACE(0)
				INSERT  INTO @ValueTable (Value) VALUES(CAST(@Value AS UNIQUEIDENTIFIER))
		END
	RETURN 
   END



GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DROP FUNCTION [dbo].[Func_GetAccountObjectName]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
create FUNCTION [dbo].[Func_GetAccountObjectName]
    (
      @CategoryListCode NVARCHAR(MAX)
    )
RETURNS NVARCHAR(MAX)
AS 
    BEGIN
        DECLARE @CategoryListName AS NVARCHAR(MAX)
        DECLARE @CategoryListNameTable AS TABLE
            (
              CategoryName NVARCHAR(255)
            )      
        INSERT  INTO @CategoryListNameTable
                ( CategoryName                         
                )
                SELECT AOG.AccountingObjectGroupName
                FROM (SELECT  Value
                        FROM    dbo.Func_ConvertStringIntoTable(';'+ @CategoryListCode+ ';',
                                                              ';')
					) List INNER JOIN AccountingObjectGroup AOG ON List.Value = AOG.AccountingObjectGroupCode
                

        SET @CategoryListName = ''
        SELECT  @CategoryListName = @CategoryListName + '; '
                + ISNULL(CategoryName, '')
        FROM    @CategoryListNameTable
        
        IF LEN(@CategoryListName) > 1 
            BEGIN
                SET @CategoryListName = SUBSTRING(@CategoryListName, 3,
                                                  LEN(@CategoryListName))
            END
        RETURN  ISNULL(@CategoryListName,NULL)
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_SA_ReceivableDetail]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountNumber NVARCHAR(20) ,
    @AccountObjectID AS NVARCHAR(MAX) ,
    @CurrencyID NVARCHAR(3) ,
    @IsSimilarSum BIT 
AS
    BEGIN
        DECLARE @ID UNIQUEIDENTIFIER
        SET @ID = '00000000-0000-0000-0000-000000000000'
               
        CREATE TABLE #tblListAccountObjectID  
            (
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(25)
                COLLATE SQL_Latin1_General_CP1_CI_AS ,
              AccountObjectName NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS ,
              AccountObjectAddress NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS ,
              AccountObjectGroupListCode NVARCHAR(MAX)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              AccountObjectGroupListName NVARCHAR(MAX)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              CompanyTaxCode NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS
                                          NULL
            ) 
        INSERT  INTO #tblListAccountObjectID
                SELECT  AO.ID ,
                        AccountingObjectCode ,
                        AccountingObjectName ,
                        [Address] ,
                        AOG.AccountingObjectGroupCode , 
                        AOG.AccountingObjectGroupName , 
                        CASE WHEN AO.ObjectType = 0 THEN TaxCode
                             ELSE ''
                        END AS CompanyTaxCode 
                FROM    dbo.Func_ConvertStringIntoTable(@AccountObjectID,
                                                           ',') tblAccountObjectSelected
                        INNER JOIN dbo.AccountingObject AO ON AO.ID = tblAccountObjectSelected.Value 
                        INNER JOIN dbo.AccountingObjectGroup AOG ON AO.AccountObjectGroupID = AOG.ID 
          
        CREATE TABLE #tblResult
            (
              RowNum INT PRIMARY KEY
                         IDENTITY(1, 1) ,
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(100)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              AccountObjectName NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              AccountObjectGroupListCode NVARCHAR(MAX)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL , 
              AccountObjectGroupListName NVARCHAR(MAX)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,          
              AccountObjectAddress NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL , 
              CompanyTaxCode NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS
                                          NULL ,
              PostedDate DATETIME ,
              RefDate DATETIME , 
              RefNo NVARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS
                                 NULL ,
              InvDate DATETIME ,
              InvNo NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS
                                  NULL ,
              
              RefType INT ,
              RefID UNIQUEIDENTIFIER ,                        
              RefDetailID NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS
                                       NULL ,
              JournalMemo NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
                                        NULL ,
              AccountNumber NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS
                                         NULL ,
              AccountCategoryKind INT ,
              CorrespondingAccountNumber NVARCHAR(20)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              ExchangeRate DECIMAL(18, 4) , 
              DebitAmountOC MONEY ,
              DebitAmount MONEY , 
              CreditAmountOC MONEY ,
              CreditAmount MONEY , 
              ClosingDebitAmountOC MONEY , 
              ClosingDebitAmount MONEY , 
              ClosingCreditAmountOC MONEY ,
              ClosingCreditAmount MONEY ,
              InventoryItemCode NVARCHAR(50)
                COLLATE SQL_Latin1_General_CP1_CI_AS , 
              InventoryItemName NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS ,
              UnitName NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS
                                    NULL ,
              Quantity DECIMAL(22, 8) ,
              UnitPrice DECIMAL(20, 6) ,            
              IsBold BIT , 
              OrderType INT ,
              BranchName NVARCHAR(128) COLLATE SQL_Latin1_General_CP1_CI_AS
                                       NULL ,
              GLJournalMemo NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
                                          NULL 
              ,
              MasterCustomField1 NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              MasterCustomField2 NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              MasterCustomField3 NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              MasterCustomField4 NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              MasterCustomField5 NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              MasterCustomField6 NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              MasterCustomField7 NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              MasterCustomField8 NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              MasterCustomField9 NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              MasterCustomField10 NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              CustomField1 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
                                         NULL ,
              CustomField2 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
                                         NULL ,
              CustomField3 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
                                         NULL ,
              CustomField4 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
                                         NULL ,
              CustomField5 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
                                         NULL ,
              CustomField6 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
                                         NULL ,
              CustomField7 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
                                         NULL ,
              CustomField8 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
                                         NULL ,
              CustomField9 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
                                         NULL ,
              CustomField10 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
                                          NULL ,
              /*cột số lượng, đơn giá*/
              MainUnitName NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
                                         NULL ,
              MainUnitPrice DECIMAL(20, 6) ,
              MainQuantity DECIMAL(22, 8) ,
              /*mã thống kê, và tên loại chứng từ*/
              ListItemCode NVARCHAR(20) ,
              ListItemName NVARCHAR(128) ,
              RefTypeName NVARCHAR(128)
            )
        
        CREATE TABLE #tblAccountNumber
            (
              AccountNumber NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS
                                         PRIMARY KEY ,
              AccountCategoryKind INT
            )
        IF @AccountNumber IS NOT NULL
            BEGIN
			INSERT  INTO #tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = @AccountNumber
                        ORDER BY A.AccountNumber ,
                                A.AccountName 
            END
        ELSE
            BEGIN 
			   INSERT  INTO #tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   DetailType = '1'
                                AND Grade = 1
                                AND IsParentNode = 0
                        ORDER BY A.AccountNumber ,
                                A.AccountName
            END 

/*Lấy số dư đầu kỳ và dữ liệu phát sinh trong kỳ:*/ 
		
        IF @IsSimilarSum = 0 
            BEGIN
                INSERT  INTO #tblResult
                        ( AccountObjectID ,
                          AccountObjectCode ,  
                          AccountObjectName ,	
                          AccountObjectGroupListCode ,
                          AccountObjectGroupListName ,        
                          AccountObjectAddress , 
                          CompanyTaxCode , 
                          PostedDate ,
                          RefDate , 
                          RefNo ,
                          InvDate ,
                          InvNo ,
                          RefType ,	
                          RefID ,  
                          RefDetailID ,
                          JournalMemo , 				  
                          AccountNumber ,
                          AccountCategoryKind , 
                          CorrespondingAccountNumber ,			  
                          ExchangeRate , 		  
                          DebitAmountOC ,
                          DebitAmount , 
                          CreditAmountOC , 
                          CreditAmount , 
                          ClosingDebitAmountOC , 
                          ClosingDebitAmount , 
                          ClosingCreditAmountOC ,	
                          ClosingCreditAmount ,               
                          IsBold , 
                          OrderType
                        )
                        SELECT  AccountingObjectID ,
                                AccountObjectCode , 
                                AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,           
                                AccountObjectAddress ,
                                CompanyTaxCode , 
                                PostedDate ,
                                RefDate ,
                                RefNo , 
                                InvDate ,
                                InvNo ,
                                RefType ,
                                RefID ,
                                RefDetailID ,
                                JournalMemo , 				  
                                AccountNumber , 
                                AccountCategoryKind , 
                                CorrespondingAccountNumber , 		  
                                ExchangeRate ,  
                                SUM(DebitAmountOC) ,
                                SUM(DebitAmount) , 
                                SUM(CreditAmountOC) ,
                                SUM(CreditAmount) ,
                                ( CASE WHEN AccountCategoryKind = 0
                                       THEN SUM(ClosingDebitAmountOC)
                                            - SUM(ClosingCreditAmountOC)
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmountOC)
                                                        - SUM(ClosingCreditAmountOC) ) > 0
                                                 THEN ( SUM(ClosingDebitAmountOC)
                                                        - SUM(ClosingCreditAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmountOC ,         
                                ( CASE WHEN AccountCategoryKind = 0
                                       THEN ( SUM(ClosingDebitAmount)
                                              - SUM(ClosingCreditAmount) )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmount)
                                                        - SUM(ClosingCreditAmount) ) > 0
                                                 THEN SUM(ClosingDebitAmount)
                                                      - SUM(ClosingCreditAmount)
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmount ,
                                ( CASE WHEN AccountCategoryKind = 1
                                       THEN ( SUM(ClosingCreditAmountOC)
                                              - SUM(ClosingDebitAmountOC) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmountOC)
                                                        - SUM(ClosingDebitAmountOC) ) > 0
                                                 THEN ( SUM(ClosingCreditAmountOC)
                                                        - SUM(ClosingDebitAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmountOC ,
                                ( CASE WHEN AccountCategoryKind = 1
                                       THEN ( SUM(ClosingCreditAmount)
                                              - SUM(ClosingDebitAmount) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmount)
                                                        - SUM(ClosingDebitAmount) ) > 0
                                                 THEN ( SUM(ClosingCreditAmount)
                                                        - SUM(ClosingDebitAmount) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmount ,
                                
                                IsBold , 
                                OrderType 
                                
                        FROM    ( SELECT    AOL.AccountingObjectID ,
                                            LAOI.AccountObjectCode ,   
                                            LAOI.AccountObjectName ,	
                                            LAOI.AccountObjectGroupListCode ,
                                            LAOI.AccountObjectGroupListName ,          
                                            LAOI.AccountObjectAddress , 
                                            LAOI.CompanyTaxCode , 
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.PostedDate
                                            END AS PostedDate ,             
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.RefDate
                                            END AS RefDate , 
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.RefNo
                                            END AS RefNo , 
                                            
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.InvoiceDate
                                            END AS InvDate ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.InvoiceNo
                                            END AS InvNo ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.ReferenceID
                                            END AS RefID ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.TypeID
                                            END AS RefType ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE CAST(AOL.DetailID AS NVARCHAR(50))
                                            END AS RefDetailID ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN N'Số dư đầu kỳ'
                                                 ELSE ISNULL(AOL.DESCRIPTION,
                                                             AOL.Reason)
                                            END AS JournalMemo ,   
                                            TBAN.AccountNumber ,
                                            TBAN.AccountCategoryKind ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.AccountCorresponding
                                            END AS CorrespondingAccountNumber ,
                                            
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.ExchangeRate
                                            END AS ExchangeRate ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN $0
                                                 ELSE AOL.DebitAmountOriginal
                                            END AS DebitAmountOC ,                               
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN $0
                                                 ELSE AOL.DebitAmount
                                            END AS DebitAmount ,                                     
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN $0
                                                 ELSE AOL.CreditAmountOriginal
                                            END AS CreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN $0
                                                 ELSE AOL.CreditAmount
                                            END AS CreditAmount ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN AOL.DebitAmountOriginal
                                                 ELSE $0
                                            END AS ClosingDebitAmountOC ,           
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN AOL.DebitAmount
                                                 ELSE $0
                                            END AS ClosingDebitAmount ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN AOL.CreditAmountOriginal
                                                 ELSE $0
                                            END AS ClosingCreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN AOL.CreditAmount
                                                 ELSE $0
                                            END AS ClosingCreditAmount , 
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN 1
                                                 ELSE 0
                                            END AS IsBold ,	
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN 0
                                                 ELSE 1
                                            END AS OrderType 
                                  FROM      dbo.GeneralLedger AS AOL
                                            INNER JOIN #tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumber
                                                              + '%'
                                            INNER JOIN #tblListAccountObjectID
                                            AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID
                                  WHERE     AOL.PostedDate <= @ToDate
                                            AND ( @CurrencyID IS NULL
                                                  OR AOL.CurrencyID = @CurrencyID
                                                )

                                ) AS RSNS
                        GROUP BY RSNS.AccountingObjectID ,
                                RSNS.AccountObjectCode , 
                                RSNS.AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,        
                                RSNS.AccountObjectAddress ,
                                CompanyTaxCode , 
                                RSNS.PostedDate ,
                                RSNS.RefDate ,
                                RSNS.RefNo , 
                                RSNS.InvDate ,
                                RSNS.InvNo ,
                                RSNS.RefType ,
                                RSNS.RefID ,   
                                RSNS.RefDetailID ,
                                RSNS.JournalMemo , 				  
                                RSNS.AccountNumber ,
                                RSNS.AccountCategoryKind ,
                                RSNS.CorrespondingAccountNumber , 		  
                                RSNS.ExchangeRate ,           
                                RSNS.IsBold ,
                                RSNS.OrderType
                        HAVING  SUM(DebitAmountOC) <> 0
                                OR SUM(DebitAmount) <> 0
                                OR SUM(CreditAmountOC) <> 0
                                OR SUM(CreditAmount) <> 0
                                OR SUM(ClosingDebitAmountOC
                                       - ClosingCreditAmountOC) <> 0
                                OR SUM(ClosingDebitAmount
                                       - ClosingCreditAmount) <> 0
                        ORDER BY RSNS.AccountObjectCode ,
                                RSNS.AccountNumber ,
                                RSNS.OrderType ,
                                RSNS.PostedDate ,
                                RSNS.RefDate ,
                                RSNS.RefNo 
            END
        ELSE 
            BEGIN
                INSERT  INTO #tblResult
                        ( AccountObjectID ,
                          AccountObjectCode ,   
                          AccountObjectName ,
                          AccountObjectGroupListCode , 
                          AccountObjectGroupListName ,            
                          AccountObjectAddress ,
                          CompanyTaxCode ,
                          PostedDate ,
                          RefDate ,
                          RefNo ,
                          InvDate ,
                          InvNo ,
                          RefType ,
                          RefID ,   
                          RefDetailID ,
                          JournalMemo , 			  
                          AccountNumber ,
                          AccountCategoryKind , 
                          CorrespondingAccountNumber ,		  
                          ExchangeRate ,		  
                          DebitAmountOC ,
                          DebitAmount ,
                          CreditAmountOC , 
                          CreditAmount ,
                          ClosingDebitAmountOC ,
                          ClosingDebitAmount , 
                          ClosingCreditAmountOC ,
                          ClosingCreditAmount ,                                    
                          IsBold , 
                          OrderType
                        )
                        SELECT  AccountingObjectID ,
                                AccountObjectCode ,  
                                AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,             
                                AccountObjectAddress , 
                                CompanyTaxCode ,
                                PostedDate ,	
                                RefDate , 
                                RefNo , 
                                InvDate ,
                                InvNo ,
                                RefType ,
                                RefID , 
                                RefDetailID ,
                                JournalMemo ,				  
                                AccountNumber ,
                                AccountCategoryKind ,
                                CorrespondingAccountNumber ,			  
                                ExchangeRate , 	  
                                DebitAmountOC ,
                                DebitAmount ,
                                CreditAmountOC ,
                                CreditAmount , 
                                ( CASE WHEN AccountCategoryKind = 0
                                       THEN ( ClosingDebitAmountOC
                                              - ClosingCreditAmountOC )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( ClosingDebitAmountOC
                                                        - ClosingCreditAmountOC ) > 0
                                                 THEN ClosingDebitAmountOC
                                                      - ClosingCreditAmountOC
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmountOC ,  
                                ( CASE WHEN AccountCategoryKind = 0
                                       THEN ( ClosingDebitAmount
                                              - ClosingCreditAmount )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( ClosingDebitAmount
                                                        - ClosingCreditAmount ) > 0
                                                 THEN ClosingDebitAmount
                                                      - ClosingCreditAmount
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmount , 
                                ( CASE WHEN AccountCategoryKind = 1
                                       THEN ( ClosingCreditAmountOC
                                              - ClosingDebitAmountOC )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( ClosingCreditAmountOC
                                                        - ClosingDebitAmountOC ) > 0
                                                 THEN ( ClosingCreditAmountOC
                                                        - ClosingDebitAmountOC )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmountOC ,
                                ( CASE WHEN AccountCategoryKind = 1
                                       THEN ( ClosingCreditAmount
                                              - ClosingDebitAmount )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( ClosingCreditAmount
                                                        - ClosingDebitAmount ) > 0
                                                 THEN ( ClosingCreditAmount
                                                        - ClosingDebitAmount )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmount ,                                     
                                IsBold , 
                                OrderType
                        FROM    ( SELECT    AD.AccountingObjectID ,
                                            AD.AccountObjectCode , 
                                            AD.AccountObjectName ,
                                            AD.AccountObjectGroupListCode ,
                                            AD.AccountObjectGroupListName ,            
                                            AD.AccountObjectAddress ,
                                            CompanyTaxCode ,
                                            AD.PostedDate ,
                                            AD.RefDate ,
                                            AD.RefNo ,
                                            AD.InvDate ,
                                            AD.InvNo ,
                                            AD.RefID ,
                                            AD.RefType , 
                                            MAX(AD.RefDetailID) AS RefDetailID ,
                                            AD.JournalMemo ,   
                                            AD.AccountNumber ,
                                            AD.AccountCategoryKind ,
                                            AD.CorrespondingAccountNumber , 
                                            AD.ExchangeRate , 
                                            SUM(AD.DebitAmountOC) AS DebitAmountOC ,                              
                                            SUM(AD.DebitAmount) AS DebitAmount ,                                        
                                            SUM(AD.CreditAmountOC) AS CreditAmountOC , 
                                            SUM(AD.CreditAmount) AS CreditAmount ,
                                            SUM(AD.ClosingDebitAmountOC) AS ClosingDebitAmountOC ,       
                                            SUM(AD.ClosingDebitAmount) AS ClosingDebitAmount ,
                                            SUM(AD.ClosingCreditAmountOC) AS ClosingCreditAmountOC , 
                                            SUM(AD.ClosingCreditAmount) AS ClosingCreditAmount ,
                                            AD.IsBold ,	
                                            AD.OrderType 
                                  FROM      ( SELECT    AOL.AccountingObjectID ,
                                                        LAOI.AccountObjectCode ,   
                                                        LAOI.AccountObjectName ,	
                                                        LAOI.AccountObjectGroupListCode ,
                                                        LAOI.AccountObjectGroupListName ,              
                                                        LAOI.AccountObjectAddress , 
                                                        LAOI.CompanyTaxCode , 
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.PostedDate
                                                        END AS PostedDate ,             
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.RefDate
                                                        END AS RefDate , 
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.RefNo
                                                        END AS RefNo , 
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.InvoiceDate
                                                        END AS InvDate ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.InvoiceNo
                                                        END AS InvNo ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.ReferenceID
                                                        END AS RefID ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.TypeID
                                                        END AS RefType ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE CAST(DetailID AS NVARCHAR(50))
                                                        END AS RefDetailID ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN N'Số dư đầu kỳ'
                                                             ELSE AOL.Reason
                                                        END AS JournalMemo , 
                                                        TBAN.AccountNumber ,
                                                        TBAN.AccountCategoryKind ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.AccountCorresponding
                                                        END AS CorrespondingAccountNumber , 
                                                        
                                                        CASE WHEN AOL.PostedDate < @FromDate
															 THEN NULL
															 ELSE AOL.ExchangeRate
														END AS ExchangeRate , 
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN $0
                                                             ELSE AOL.DebitAmountOriginal
                                                        END AS DebitAmountOC ,                         
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN $0
                                                             ELSE AOL.DebitAmount
                                                        END AS DebitAmount ,                           
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN $0
                                                             ELSE AOL.CreditAmountOriginal
                                                        END AS CreditAmountOC ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN $0
                                                             ELSE AOL.CreditAmount
                                                        END AS CreditAmount ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN AOL.DebitAmountOriginal
                                                             ELSE $0
                                                        END AS ClosingDebitAmountOC ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN AOL.DebitAmount
                                                             ELSE $0
                                                        END AS ClosingDebitAmount ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN AOL.CreditAmountOriginal
                                                             ELSE $0
                                                        END AS ClosingCreditAmountOC ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN AOL.CreditAmount
                                                             ELSE $0
                                                        END AS ClosingCreditAmount ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN 1
                                                             ELSE 0
                                                        END AS IsBold ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN 0
                                                             ELSE 1
                                                        END AS OrderType 
                                                        
                                              FROM      dbo.GeneralLedger
                                                        AS AOL
                                                        INNER JOIN #tblListAccountObjectID
                                                        AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID
                                                        INNER JOIN #tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumber
                                                              + '%'
                                              WHERE     AOL.PostedDate <= @ToDate
                                                        AND ( @CurrencyID IS NULL
                                                              OR AOL.CurrencyID = @CurrencyID
                                                            )
                                            ) AS AD
                                  GROUP BY  AD.AccountingObjectID ,
                                            AD.AccountObjectCode , 
                                            AD.AccountObjectName ,
                                            AD.AccountObjectGroupListCode , 
                                            AD.AccountObjectGroupListName ,     
                                            AD.AccountObjectAddress ,
                                            AD.CompanyTaxCode ,
                                            AD.PostedDate ,
                                            AD.RefDate ,
                                            AD.RefNo ,
                                            AD.InvDate ,
                                            AD.InvNo ,
                                            AD.RefID ,
                                            AD.RefType ,
                                            AD.JournalMemo ,
                                            AD.AccountNumber ,
                                            AD.AccountCategoryKind ,
                                            AD.CorrespondingAccountNumber ,  
                                            AD.ExchangeRate ,
                                            AD.IsBold , 	
                                            AD.OrderType
                                ) AS RSS
                        WHERE   RSS.DebitAmountOC <> 0
                                OR RSS.CreditAmountOC <> 0
                                OR RSS.DebitAmount <> 0
                                OR RSS.CreditAmount <> 0
                                OR ClosingCreditAmountOC
                                - ClosingDebitAmountOC <> 0
                                OR ClosingCreditAmount - ClosingDebitAmount <> 0
                        ORDER BY RSS.AccountObjectCode ,
                                RSS.AccountNumber ,
                                RSS.OrderType ,
                                RSS.PostedDate ,
                                RSS.RefDate ,
                                RSS.RefNo ,
                                RSS.InvDate ,
                                RSS.InvNo
            END

/* Tính số tồn */
        DECLARE @CloseAmountOC AS DECIMAL(22, 8) ,
            @CloseAmount AS DECIMAL(22, 8) ,
            @AccountObjectCode_tmp NVARCHAR(100) ,
            @AccountNumber_tmp NVARCHAR(20)
        SELECT  @CloseAmountOC = 0 ,
                @CloseAmount = 0 ,
                @AccountObjectCode_tmp = N'' ,
                @AccountNumber_tmp = N''
        UPDATE  #tblResult
        SET     @CloseAmountOC = ( CASE WHEN OrderType = 0
                                        THEN ( CASE WHEN ClosingDebitAmountOC = 0
                                                    THEN ClosingCreditAmountOC
                                                    ELSE -1
                                                         * ClosingDebitAmountOC
                                               END )
                                        WHEN @AccountObjectCode_tmp <> AccountObjectCode
                                             OR @AccountNumber_tmp <> AccountNumber
                                        THEN CreditAmountOC - DebitAmountOC
                                        ELSE @CloseAmountOC + CreditAmountOC
                                             - DebitAmountOC
                                   END ) ,
                ClosingDebitAmountOC = ( CASE WHEN AccountCategoryKind = 0
                                              THEN -1 * @CloseAmountOC
                                              WHEN AccountCategoryKind = 1
                                              THEN $0
                                              ELSE CASE WHEN @CloseAmountOC < 0
                                                        THEN -1
                                                             * @CloseAmountOC
                                                        ELSE $0
                                                   END
                                         END ) ,
                ClosingCreditAmountOC = ( CASE WHEN AccountCategoryKind = 1
                                               THEN @CloseAmountOC
                                               WHEN AccountCategoryKind = 0
                                               THEN $0
                                               ELSE CASE WHEN @CloseAmountOC > 0
                                                         THEN @CloseAmountOC
                                                         ELSE $0
                                                    END
                                          END ) ,
                @CloseAmount = ( CASE WHEN OrderType = 0
                                      THEN ( CASE WHEN ClosingDebitAmount = 0
                                                  THEN ClosingCreditAmount
                                                  ELSE -1 * ClosingDebitAmount
                                             END )
                                      WHEN @AccountObjectCode_tmp <> AccountObjectCode
                                           OR @AccountNumber_tmp <> AccountNumber
                                      THEN CreditAmount - DebitAmount
                                      ELSE @CloseAmount + CreditAmount
                                           - DebitAmount
                                 END ) ,
                ClosingDebitAmount = ( CASE WHEN AccountCategoryKind = 0
                                            THEN -1 * @CloseAmount
                                            WHEN AccountCategoryKind = 1
                                            THEN $0
                                            ELSE CASE WHEN @CloseAmount < 0
                                                      THEN -1 * @CloseAmount
                                                      ELSE $0
                                                 END
                                       END ) ,
                ClosingCreditAmount = ( CASE WHEN AccountCategoryKind = 1
                                             THEN @CloseAmount
                                             WHEN AccountCategoryKind = 0
                                             THEN $0
                                             ELSE CASE WHEN @CloseAmount > 0
                                                       THEN @CloseAmount
                                                       ELSE $0
                                                  END
                                        END ) ,
                @AccountObjectCode_tmp = AccountObjectCode ,
                @AccountNumber_tmp = AccountNumber
        WHERE   OrderType <> 2
/*Lấy số liệu*/				
        SELECT  RS.*
        FROM    #tblResult RS
        ORDER BY RowNum
        
        DROP TABLE #tblResult
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

/*=================================================================================================
 * Created By:		haipl
 * Created Date:	25/12/2017
 * Description:		Lay du lieu cho bao cao << Sổ chi tiết bán hàng >>
===================================================================================================== */
ALTER PROCEDURE [dbo].[Proc_SA_GetSalesBookDetail]
  
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @BranchID UNIQUEIDENTIFIER ,
    @IncludeDependentBranch BIT ,
    @InventoryItemID NVARCHAR(MAX) , 
    @OrganizationUnitID UNIQUEIDENTIFIER ,
    @AccountObjectID NVARCHAR(MAX) ,
	@SumGiaVon decimal output
AS

    BEGIN
        SET NOCOUNT ON;
        declare @v1 DECIMAL(29,4)    
		                          
        CREATE TABLE #tblListInventoryItemID
            (
              InventoryItemID UNIQUEIDENTIFIER PRIMARY KEY
            ) 
        INSERT  INTO #tblListInventoryItemID
                    SELECT  T.*
                    FROM    dbo.Func_ConvertStringIntoTable(@InventoryItemID,
                                                              ',')	
                                                              AS T  
        select a.Date as ngay_CT, 
		a.PostedDate as ngay_HT, 
		a.No as So_Hieu,
		a.InvoiceDate as Ngay_HD, 
		a.InvoiceNo as SO_HD, 
		Sreason as Dien_giai, 
		accountCorresponding as TK_DOIUNG,
		c.Unit as DVT, 
		b.Quantity as SL, 
		UnitPriceOriginal as DON_GIA, 
		sum(debitAmount) as KHAC, 
		sum(CreditAmount) as TT, 
		c.ID as  InventoryItemID,
		c.MaterialGoodsName
		from GeneralLedger a join SAInvoiceDetail b on a.DetailID = b.ID 
		join MaterialGoods c on b.MaterialGoodsID = c.ID
		join SAInvoice d on a.ReferenceID = d.ID
		where a.PostedDate between  @FromDate and @ToDate and c.ID in (select InventoryItemID from #tblListInventoryItemID)
		and Account like '511%'
		group by a.Date,  a.PostedDate, a.No ,a.InvoiceDate , a.InvoiceNo , Sreason , accountCorresponding , 
		c.Unit , b.Quantity , UnitPriceOriginal, c.ID,c.MaterialGoodsName ;
		select  @SumGiaVon = isnull(sum(a.debitAmount),0) 
 from GeneralLedger a 
join SAInvoiceDetail b on a.ReferenceID = b.SAInvoiceID 
join MaterialGoods c on c.ID = b.MaterialGoodsID
 where PostedDate between @FromDate and @ToDate and Account = '632' and c.ID in (select InventoryItemID from #tblListInventoryItemID);
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_SA_GetReceivableSummaryByGroup]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @AccountNumber NVARCHAR(25),
    @AccountObjectID AS NVARCHAR(MAX),
    @CurrencyID NVARCHAR(3),
    @AccountObjectGroupID AS NVARCHAR(MAX) 
AS
    BEGIN          
        DECLARE @tblListAccountObjectID TABLE
            (
             AccountObjectID UNIQUEIDENTIFIER ,
             AccountObjectGroupList NVARCHAR(MAX)           
            ) 
             
        INSERT  INTO @tblListAccountObjectID
        SELECT  
			AO.ID
			,AO.AccountObjectGroupID
        FROM AccountingObject AS AO 
        LEFT JOIN dbo.Func_ConvertStringIntoTable(@AccountObjectID,',') AS TLAO 
			ON AO.ID = TLAO.Value
        WHERE 
		(@AccountObjectID IS NULL OR TLAO.Value IS NOT NULL)
                                    		
        DECLARE @tblAccountNumber TABLE
            (
             AccountNumber NVARCHAR(255) PRIMARY KEY,
             AccountName NVARCHAR(255),
             AccountNumberPercent NVARCHAR(255),
             AccountCategoryKind INT
            )
        IF @AccountNumber IS NOT NULL
            BEGIN
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber,
                                A.AccountName,
                                A.AccountNumber + '%',
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = @AccountNumber
                        ORDER BY A.AccountNumber,
                                A.AccountName                 
            END
        ELSE
            BEGIN 
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber,
                                A.AccountName,
                                A.AccountNumber + '%',
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   DetailType = '1'
                                AND Grade = 1
                                AND IsParentNode = 0
                        ORDER BY A.AccountNumber,
                                A.AccountName
            END
	    
	    DECLARE @tblDebt TABLE
			(
				AccountObjectID			UNIQUEIDENTIFIER
				,AccountNumber			NVARCHAR (25)				
				,AccountObjectGroupList NVARCHAR(Max)				
				,OpenDebitAmountOC		DECIMAL (28,4)
				,OpenDebitAmount		DECIMAL (28,4)
				,OpenCreditAmountOC		DECIMAL (28,4)
				,OpenCreditAmount		DECIMAL (28,4)
				,DebitAmountOC			DECIMAL (28,4)
				,DebitAmount			DECIMAL (28,4)
				,CreditAmountOC			DECIMAL (28,4)
				,CreditAmount			DECIMAL (28,4)
				,CloseDebitAmountOC		DECIMAL (28,4)
				,CloseDebitAmount		DECIMAL (28,4)
				,CloseCreditAmountOC	DECIMAL (28,4)
				,CloseCreditAmount		DECIMAL (28,4)					
			)				
		INSERT @tblDebt
        
        SELECT  
                AccountingObjectID,               
                AccountNumber,       
                AccountObjectGroupList,
                (CASE WHEN AccountCategoryKind = 0
                      THEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC)
                      WHEN AccountCategoryKind = 1 THEN $0
                      ELSE CASE WHEN (SUM(OpenningDebitAmountOC - OpenningCreditAmountOC)) > 0
                                THEN (SUM(OpenningDebitAmountOC - OpenningCreditAmountOC))
                                ELSE $0
                           END
                 END) AS OpenningDebitAmountOC,        
                (CASE WHEN AccountCategoryKind = 0
                      THEN SUM(OpenningDebitAmount - OpenningCreditAmount)
                      WHEN AccountCategoryKind = 1 THEN $0
                      ELSE CASE WHEN SUM(OpenningDebitAmount - OpenningCreditAmount) > 0
                                THEN SUM(OpenningDebitAmount  - OpenningCreditAmount)
                                ELSE $0
                           END
                 END) AS OpenningDebitAmount,
                (CASE WHEN AccountCategoryKind = 1
                      THEN (SUM(OpenningCreditAmountOC - OpenningDebitAmountOC))
                      WHEN AccountCategoryKind = 0 THEN $0
                      ELSE CASE WHEN (SUM(OpenningCreditAmountOC - OpenningDebitAmountOC)) > 0
                                THEN (SUM(OpenningCreditAmountOC - OpenningDebitAmountOC))
                                ELSE $0
                           END
                 END) AS OpenningCreditAmountOC,
                (CASE WHEN AccountCategoryKind = 1
                      THEN (SUM(OpenningCreditAmount - OpenningDebitAmount))
                      WHEN AccountCategoryKind = 0 THEN $0
                      ELSE CASE WHEN (SUM(OpenningCreditAmount - OpenningDebitAmount)) > 0
                                THEN (SUM(OpenningCreditAmount - OpenningDebitAmount))
                                ELSE $0
                           END
                 END) AS OpenningCreditAmount, 
                SUM(DebitAmountOC) AS DebitAmountOC,
                SUM(DebitAmount) AS DebitAmount, 
                SUM(CreditAmountOC) AS CreditAmountOC,
                SUM(CreditAmount) AS CreditAmount, 
                (CASE WHEN AccountCategoryKind = 0
                      THEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC + DebitAmountOC - CreditAmountOC)
                      WHEN AccountCategoryKind = 1 THEN $0
                      ELSE CASE WHEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC + DebitAmountOC - CreditAmountOC) > 0
                                THEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC + DebitAmountOC - CreditAmountOC)
                                ELSE $0
                           END
                 END) AS CloseDebitAmountOC,	
                 (CASE WHEN AccountCategoryKind = 0
                      THEN SUM(OpenningDebitAmount - OpenningCreditAmount
                               + DebitAmount - CreditAmount)
                      WHEN AccountCategoryKind = 1 THEN $0
                      ELSE CASE WHEN SUM(OpenningDebitAmount
                                         - OpenningCreditAmount - CreditAmount
                                         + DebitAmount) > 0
                                THEN SUM(OpenningDebitAmount
                                         - OpenningCreditAmount - CreditAmount
                                         + DebitAmount)
                                ELSE $0
                           END
                 END) AS CloseDebitAmount,
                (CASE WHEN AccountCategoryKind = 1
                      THEN SUM(OpenningCreditAmountOC - OpenningDebitAmountOC - DebitAmountOC + CreditAmountOC)
                      WHEN AccountCategoryKind = 0 THEN $0
                      ELSE CASE WHEN (SUM(OpenningCreditAmountOC - OpenningDebitAmountOC - DebitAmountOC + CreditAmountOC)) > 0
                                THEN SUM(OpenningCreditAmountOC  - OpenningDebitAmountOC - DebitAmountOC + CreditAmountOC)
                                ELSE $0
                           END
                 END) AS CloseCreditAmountOC,	     
                (CASE WHEN AccountCategoryKind = 1
                      THEN SUM(OpenningCreditAmount - OpenningDebitAmount
                               + CreditAmount - DebitAmount)
                      WHEN AccountCategoryKind = 0 THEN $0
                      ELSE CASE WHEN (SUM(OpenningCreditAmount
                                          - OpenningDebitAmount + CreditAmount
                                          - DebitAmount)) > 0
                                THEN SUM(OpenningCreditAmount
                                         - OpenningDebitAmount + CreditAmount
                                         - DebitAmount)
                                ELSE $0
                           END
                 END) AS CloseCreditAmount
        FROM    (SELECT AOL.AccountingObjectID,                        
                        TBAN.AccountNumber,
                        TBAN.AccountCategoryKind,
                        ISNULL(AccountObjectGroupList,'') AccountObjectGroupList,
                        SUM(CASE WHEN AOL.PostedDate < @FromDate
                             THEN AOL.DebitAmountOriginal
                             ELSE $0
                        END) AS OpenningDebitAmountOC,	
                        SUM(CASE WHEN AOL.PostedDate < @FromDate
                             THEN AOL.DebitAmount
                             ELSE $0
                        END) AS OpenningDebitAmount, 
                        SUM(CASE WHEN AOL.PostedDate < @FromDate
                             THEN AOL.CreditAmountOriginal
                             ELSE $0
                        END) AS OpenningCreditAmountOC, 
                        SUM(CASE WHEN AOL.PostedDate < @FromDate
                             THEN AOL.CreditAmount
                             ELSE $0
                        END) AS OpenningCreditAmount,                                         
                        SUM(CASE WHEN AOL.PostedDate BETWEEN @FromDate AND @ToDate
                             THEN AOL.DebitAmountOriginal
                             ELSE 0
                        END) AS DebitAmountOC,                      
                        SUM(CASE WHEN AOL.PostedDate BETWEEN @FromDate AND @ToDate
                             THEN AOL.DebitAmount
                             ELSE 0
                        END) AS DebitAmount,                    
                        SUM(CASE WHEN AOL.PostedDate BETWEEN @FromDate AND @ToDate
                             THEN AOL.CreditAmountOriginal
                             ELSE 0
                        END) AS CreditAmountOC, 
                        SUM(CASE WHEN AOL.PostedDate BETWEEN @FromDate AND @ToDate
                             THEN AOL.CreditAmount
                             ELSE 0
                        END) AS CreditAmount
                 FROM   dbo.GeneralLedger AS AOL                       
                        INNER JOIN @tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumberPercent
                        INNER JOIN dbo.Account AS AN ON AOL.Account = AN.AccountNumber
                        INNER JOIN @tblListAccountObjectID AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID                      
                 WHERE  AOL.PostedDate <= @ToDate
                        AND (@CurrencyID IS NULL OR AOL.CurrencyID = @CurrencyID)
                        AND AN.DetailType = '1'
                 GROUP BY 
						AOL.AccountingObjectID,                       
                        TBAN.AccountNumber, 
                        TBAN.AccountCategoryKind
                        ,AccountObjectGroupList                      
                ) AS RSNS
       
        GROUP BY RSNS.AccountingObjectID,               
                 RSNS.AccountNumber 
                 ,RSNS.AccountCategoryKind
                 ,RSNS.AccountObjectGroupList
        HAVING
				SUM(DebitAmountOC)<>0 OR
                SUM(DebitAmount)<>0 OR
                SUM(CreditAmountOC) <>0 OR
                SUM(CreditAmount) <>0 OR
                SUM(OpenningDebitAmountOC - OpenningCreditAmountOC)<>0 OR
                SUM(OpenningDebitAmount - OpenningCreditAmount)<>0
                

		SELECT   AOG.ID,
		        AOG.AccountingObjectGroupCode,
				AOG.AccountingObjectGroupName
		        ,AO.AccountingObjectCode
				,AO.AccountingObjectName
				,Ao.Address AS AccountObjectAddress
				,D.* FROM @tblDebt D
		INNER JOIN dbo.AccountingObject AO ON D.AccountObjectID = AO.ID
		INNER JOIN dbo.AccountingObjectGroup AOG ON AO.AccountObjectGroupID = AOG.ID
		ORDER BY D.AccountNumber
		
		
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*

*/
ALTER PROCEDURE [dbo].[Proc_SA_GetReceivableSummary]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @BranchID UNIQUEIDENTIFIER ,
    @AccountNumber NVARCHAR(25) ,
    @AccountObjectID AS NVARCHAR(MAX) ,
    @CurrencyID NVARCHAR(3) ,
    @IsShowInPeriodOnly BIT 
AS
    BEGIN
               
        DECLARE @tblListAccountObjectID TABLE 
            (
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectGroupListCode NVARCHAR(MAX) ,
              AccountObjectCategoryName NVARCHAR(MAX)
            ) 
             
        INSERT  INTO @tblListAccountObjectID
                SELECT  AO.ID ,
                        AO.AccountObjectGroupID ,
						null
                FROM    AccountingObject AS AO
                        LEFT JOIN dbo.Func_ConvertStringIntoTable(@AccountObjectID,
                                                              ',') AS TLAO ON AO.ID = TLAO.Value
                        
                WHERE  
				      ( @AccountObjectID IS NULL
                              OR TLAO.Value IS NOT NULL
                            )
                              			
        DECLARE @tblAccountNumber TABLE
            (
              AccountNumber NVARCHAR(255) PRIMARY KEY ,
              AccountName NVARCHAR(255) ,
              AccountNumberPercent NVARCHAR(255) ,
              AccountCategoryKind INT
            )
        IF @AccountNumber IS NOT NULL
            BEGIN
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = @AccountNumber
                        ORDER BY A.AccountNumber ,
                                A.AccountName 
                
            END
        ELSE
            BEGIN 
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   DetailType = '1'
                                AND Grade = 1
                                AND IsParentNode = 0
                        ORDER BY A.AccountNumber ,
                                A.AccountName
            END
            
	    DECLARE @FirstDateOfYear AS DATETIME
	    SET @FirstDateOfYear = '1/1/' + CAST(Year(@FromDate) AS NVARCHAR(4))	   
	    
        SELECT  ROW_NUMBER() OVER ( ORDER BY AccountingObjectCode ) AS RowNum ,
                AccountingObjectID ,
                AccountingObjectCode ,
                AccountingObjectName ,
                AccountObjectAddress ,
                AccountObjectTaxCode ,
                AccountNumber , 
                AccountCategoryKind ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmountOC)
                            - SUM(OpenningCreditAmountOC)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningDebitAmountOC
                                            - OpenningCreditAmountOC) ) > 0
                                 THEN ( SUM(OpenningDebitAmountOC
                                            - OpenningCreditAmountOC) )
                                 ELSE $0
                            END
                  END ) AS OpenningDebitAmountOC ,        
                ( CASE WHEN AccountCategoryKind = 0
                       THEN ( SUM(OpenningDebitAmount - OpenningCreditAmount) )
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningDebitAmount
                                            - OpenningCreditAmount) ) > 0
                                 THEN SUM(OpenningDebitAmount
                                          - OpenningCreditAmount)
                                 ELSE $0
                            END
                  END ) AS OpenningDebitAmount , 
                ( CASE WHEN AccountCategoryKind = 1
                       THEN ( SUM(OpenningCreditAmountOC
                                  - OpenningDebitAmountOC) )
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC) ) > 0
                                 THEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC) )
                                 ELSE $0
                            END
                  END ) AS OpenningCreditAmountOC ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN ( SUM(OpenningCreditAmount - OpenningDebitAmount) )
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount) ) > 0
                                 THEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount) )
                                 ELSE $0
                            END
                  END ) AS OpenningCreditAmount ,
                SUM(DebitAmountOC) AS DebitAmountOC ,
                SUM(DebitAmount) AS DebitAmount ,
                SUM(CreditAmountOC) AS CreditAmountOC ,
                SUM(CreditAmount) AS CreditAmount ,
                SUM(AccumDebitAmountOC) AS AccumDebitAmountOC ,
                SUM(AccumDebitAmount) AS AccumDebitAmount ,
                SUM(AccumCreditAmountOC) AS AccumCreditAmountOC ,
                SUM(AccumCreditAmount) AS AccumCreditAmount , 
                                        
                /* Số dư cuối kỳ = Dư Có đầu kỳ - Dư Nợ đầu kỳ + Phát sinh Có – Phát sinh Nợ
				Nếu Số dư cuối kỳ >0 thì hiển bên cột Dư Có cuối kỳ 
				Nếu số dư cuối kỳ <0 thì hiển thị bên cột Dư Nợ cuối kỳ */
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC
                                + DebitAmountOC - CreditAmountOC)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN SUM(OpenningDebitAmountOC
                                          - OpenningCreditAmountOC
                                          + DebitAmountOC - CreditAmountOC) > 0
                                 THEN SUM(OpenningDebitAmountOC
                                          - OpenningCreditAmountOC
                                          + DebitAmountOC - CreditAmountOC)
                                 ELSE $0
                            END
                  END ) AS CloseDebitAmountOC ,	
                ( CASE WHEN AccountCategoryKind = 1
                       THEN SUM(OpenningCreditAmountOC - OpenningDebitAmountOC
                                - DebitAmountOC + CreditAmountOC)
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC
                                            - DebitAmountOC + CreditAmountOC) ) > 0
                                 THEN SUM(OpenningCreditAmountOC
                                          - OpenningDebitAmountOC
                                          - DebitAmountOC + CreditAmountOC)
                                 ELSE $0
                            END
                  END ) AS CloseCreditAmountOC ,	
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmount - OpenningCreditAmount
                                + DebitAmount - CreditAmount)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN SUM(OpenningDebitAmount
                                          - OpenningCreditAmount
                                          - CreditAmount + DebitAmount) > 0
                                 THEN SUM(OpenningDebitAmount
                                          - OpenningCreditAmount
                                          - CreditAmount + DebitAmount)
                                 ELSE $0
                            END
                  END ) AS CloseDebitAmount ,	
                ( CASE WHEN AccountCategoryKind = 1
                       THEN SUM(OpenningCreditAmount - OpenningDebitAmount
                                + CreditAmount - DebitAmount)
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount
                                            + CreditAmount - DebitAmount) ) > 0
                                 THEN SUM(OpenningCreditAmount
                                          - OpenningDebitAmount + CreditAmount
                                          - DebitAmount)
                                 ELSE $0
                            END
                  END ) AS CloseCreditAmount ,	
                AccountObjectGroupListCode ,
                AccountObjectCategoryName
        FROM    ( SELECT    AO.ID as AccountingObjectID ,
                            AO.AccountingObjectCode ,  
                            AO.AccountingObjectName ,	
                            AO.Address AS AccountObjectAddress ,
                            AO.TaxCode AS AccountObjectTaxCode , 
                            TBAN.AccountNumber , 
                            TBAN.AccountCategoryKind , 
                            CASE WHEN GL.PostedDate < @FromDate
                                 THEN GL.DebitAmountOriginal
                                 ELSE $0
                            END AS OpenningDebitAmountOC ,
                            CASE WHEN GL.PostedDate < @FromDate
                                 THEN GL.DebitAmount
                                 ELSE $0
                            END AS OpenningDebitAmount ,
                            CASE WHEN GL.PostedDate < @FromDate
                                 THEN GL.CreditAmountOriginal
                                 ELSE $0
                            END AS OpenningCreditAmountOC , 
                            CASE WHEN GL.PostedDate < @FromDate
                                 THEN GL.CreditAmount
                                 ELSE $0
                            END AS OpenningCreditAmount ,                                                
                            CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate
                                 THEN GL.DebitAmountOriginal
                                 ELSE 0
                            END AS DebitAmountOC ,                       
                            CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate
                                 THEN GL.DebitAmount
                                 ELSE 0
                            END AS DebitAmount ,                                    
                            CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate
                                 THEN GL.CreditAmountOriginal
                                 ELSE 0
                            END AS CreditAmountOC ,
                            CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate
                                 THEN GL.CreditAmount
                                 ELSE 0
                            END AS CreditAmount ,
                            CASE WHEN GL.PostedDate BETWEEN @FirstDateOfYear AND @ToDate
                                 THEN GL.DebitAmountOriginal
                                 ELSE 0
                            END AS AccumDebitAmountOC ,                                  
                            CASE WHEN GL.PostedDate BETWEEN @FirstDateOfYear AND @ToDate
                                 THEN GL.DebitAmount
                                 ELSE 0
                            END AS AccumDebitAmount ,                                       
                            CASE WHEN GL.PostedDate BETWEEN @FirstDateOfYear AND @ToDate
                                 THEN GL.CreditAmountOriginal
                                 ELSE 0
                            END AS AccumCreditAmountOC ,
                            CASE WHEN GL.PostedDate BETWEEN @FirstDateOfYear AND @ToDate
                                 THEN GL.CreditAmount
                                 ELSE 0
                            END AS AccumCreditAmount ,
                            LAOI.AccountObjectGroupListCode ,
                            LAOI.AccountObjectCategoryName 
                  FROM      dbo.GeneralLedger AS GL
                            INNER JOIN dbo.AccountingObject AO ON AO.ID = GL.AccountingObjectID
                            INNER JOIN @tblAccountNumber TBAN ON GL.Account LIKE TBAN.AccountNumberPercent
                            INNER JOIN dbo.Account AS AN ON GL.Account = AN.AccountNumber
                            INNER JOIN @tblListAccountObjectID AS LAOI ON GL.AccountingObjectID = LAOI.AccountObjectID
                  WHERE     GL.PostedDate <= @ToDate
                            AND ( @CurrencyID IS NULL
                                  OR GL.CurrencyID = @CurrencyID
                                )
                            AND AN.DetailType = '1'
                ) AS RSNS
        GROUP BY RSNS.AccountingObjectID ,
                RSNS.AccountingObjectCode ,
                RSNS.AccountingObjectName ,    
                RSNS.AccountObjectAddress ,
                RSNS.AccountObjectTaxCode , 
                RSNS.AccountNumber ,
                RSNS.AccountCategoryKind ,
                RSNS.AccountObjectGroupListCode ,
                RSNS.AccountObjectCategoryName 
        HAVING  SUM(DebitAmountOC) <> 0
                OR SUM(DebitAmount) <> 0
                OR SUM(CreditAmountOC) <> 0
                OR SUM(CreditAmount) <> 0
                OR SUM(OpenningDebitAmountOC - OpenningCreditAmountOC) <> 0
                OR SUM(OpenningDebitAmount - OpenningCreditAmount) <> 0                
                OR(@IsShowInPeriodOnly = 0 AND (
                 SUM(AccumDebitAmountOC) <>0	
                OR SUM(AccumDebitAmount) <>0
                OR SUM(AccumCreditAmountOC) <>0  
                OR SUM(AccumCreditAmount)<>0))
        ORDER BY RSNS.AccountingObjectCode
        OPTION(RECOMPILE)
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

/*

*/
ALTER PROCEDURE [dbo].[Proc_PO_PayDetailNCC]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountNumber NVARCHAR(20) ,
    @AccountObjectID AS NVARCHAR(MAX) ,
    @CurrencyID NVARCHAR(3)
AS 
    BEGIN     
        DECLARE @tblListAccountObjectID TABLE 
            (
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(25) ,
              AccountObjectName NVARCHAR(255) ,
              AccountObjectAddress NVARCHAR(255) ,
              AccountObjectTaxCode NVARCHAR(200) ,
              AccountObjectGroupListCode NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS
                                                       NULL ,
              AccountObjectGroupListName NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS
                                                       NULL 
            ) 
        INSERT  INTO @tblListAccountObjectID
                SELECT  AO.ID as AccountObjectID ,
                        AO.AccountingObjectCode ,
                        AO.AccountingObjectName ,
                        AO.Address ,
                        AO.TaxCode ,
                        AOG.AccountingObjectGroupCode ,
                        AOG.AccountingObjectGroupName 
                FROM    dbo.Func_ConvertStringIntoTable(@AccountObjectID, ',') tblAccountObjectSelected
                        INNER JOIN dbo.AccountingObject AO ON AO.ID = tblAccountObjectSelected.Value		
                        INNER JOIN dbo.AccountingObjectGroup AOG ON AO.AccountObjectGroupID = AOG.ID
      
        CREATE TABLE #tblResult
            (
              RowNum INT IDENTITY(1, 1)
                         PRIMARY KEY ,
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              AccountObjectName NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              AccountObjectGroupListCode NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS
                                                       NULL ,
              AccountObjectGroupListName NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS
                                                       NULL ,             
              AccountObjectAddress NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              PostedDate DATETIME ,
              RefDate DATETIME , 
              RefNo NVARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              DueDate DATETIME , 
              InvDate DATETIME ,
              InvNo NVARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS ,

              RefType INT ,
              RefID UNIQUEIDENTIFIER ,
              JournalMemo NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS , 
              AccountNumber NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS , 
              AccountCategoryKind INT ,
              CorrespondingAccountNumber NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS , 
              ExchangeRate DECIMAL(18, 4) , 
              DebitAmountOC MONEY ,	
              DebitAmount MONEY , 
              CreditAmountOC MONEY , 
              CreditAmount MONEY , 
              ClosingDebitAmountOC MONEY ,
              ClosingDebitAmount MONEY , 
              ClosingCreditAmountOC MONEY ,	
              ClosingCreditAmount MONEY ,
              InventoryItemCode NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              InventoryItemName NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              UnitName NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              Quantity DECIMAL(22, 8) ,
              UnitPrice DECIMAL(18, 4) , 
              BranchName NVARCHAR(128) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              IsBold BIT ,	
              OrderType INT ,
              GLJournalMemo NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              DocumentIncluded NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField1 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField2 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField3 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField4 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField5 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField6 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField7 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField8 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField9 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField10 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS,
               MainUnitName NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              MainQuantity DECIMAL(22, 8) ,
              MainUnitPrice DECIMAL(18, 4)  
              
            )
        
        DECLARE @tblAccountNumber TABLE
            (
              AccountNumber NVARCHAR(20) PRIMARY KEY ,
              AccountName NVARCHAR(255) ,
              AccountNumberPercent NVARCHAR(25) ,
              AccountCategoryKind INT
            )
        IF @AccountNumber IS NOT NULL 
            BEGIN
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = @AccountNumber
                        ORDER BY A.AccountNumber ,
                                A.AccountName 
                
            END
        ELSE 
            BEGIN 
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = '331'
                        ORDER BY A.AccountNumber ,
                                A.AccountName
            END
            
                
/*Lấy số dư đầu kỳ và dữ liệu phát sinh trong kỳ:*/ 
INSERT  INTO #tblResult
                        ( AccountObjectID ,
                          AccountObjectCode , 
                          AccountObjectName ,
                          AccountObjectGroupListCode , 
                          AccountObjectGroupListName ,      
                          AccountObjectAddress , 
                          PostedDate ,
                          RefDate ,
                          RefNo ,
                          InvDate ,
                          InvNo ,
                          RefType ,
                          RefID , 
                          JournalMemo , 				  
                          AccountNumber , 
                          AccountCategoryKind ,
                          CorrespondingAccountNumber ,	  			  
                          DebitAmountOC ,	
                          DebitAmount , 
                          CreditAmountOC ,
                          CreditAmount ,
                          ClosingDebitAmountOC , 
                          ClosingDebitAmount ,
                          ClosingCreditAmountOC ,
                          ClosingCreditAmount,
						  OrderType   
                        )
                        SELECT  AccountingObjectID ,
                                AccountObjectCode ,
                                AccountObjectName ,	
                                AccountObjectGroupListCode , 
                                AccountObjectGroupListName ,            
                                AccountObjectAddress , 
                                RSNS.PostedDate ,
                                NgayCtu ,
                                SoCtu ,
                                InvDate ,
                                InvNo ,
                                RefType ,
                                RSNS.RefID , 
                                JournalMemo ,			  
                                AccountNumber , 
                                AccountCategoryKind , 
                                CorrespondingAccountNumber , 		  			  
                                SUM(DebitAmountOC) ,
                                SUM(DebitAmount) , 
                                SUM(CreditAmountOC) , 
                                SUM(CreditAmount) , 
                                ( CASE WHEN AccountCategoryKind = 0 THEN SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC)
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC) ) > 0 THEN ( SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmountOC ,      
                                ( CASE WHEN AccountCategoryKind = 0 THEN ( SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount) )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount) ) > 0 THEN SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount)
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmount , 
                                ( CASE WHEN AccountCategoryKind = 1 THEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) ) > 0 THEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmountOC ,
                                ( CASE WHEN AccountCategoryKind = 1 THEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) ) > 0 THEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmount,  
								  OrderType
                        FROM    ( SELECT    AOL.AccountingObjectID ,
                                            LAOI.AccountObjectCode ,   
                                            LAOI.AccountObjectName ,
                                            AccountObjectGroupListCode ,
                                            AccountObjectGroupListName ,         
                                            LAOI.AccountObjectAddress ,
                                            AOL.PostedDate as PostedDate ,  
											AOL.Date as NgayCtu ,     
											AOL.No as SoCtu,                    
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.InvoiceDate
                                            END AS InvDate ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.InvoiceNo
                                            END AS InvNo ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.ReferenceID
                                            END AS RefID ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.TypeID
                                            END AS RefType , 
                                            CASE WHEN AOL.PostedDate < @FromDate 
												      THEN NULL
                                                 ELSE CAST(AOL.DetailID AS NVARCHAR(50))
                                            END AS RefDetailID ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN N'Số dư đầu kỳ'
                                                 ELSE AOL.Description
                                            END AS JournalMemo , 
                                            TBAN.AccountNumber ,
                                            TBAN.AccountCategoryKind ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.AccountCorresponding
                                            END AS CorrespondingAccountNumber , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.DebitAmountOriginal
                                            END AS DebitAmountOC ,                             
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.DebitAmount
                                            END AS DebitAmount ,                                  
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.CreditAmountOriginal
                                            END AS CreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.CreditAmount
                                            END AS CreditAmount , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.DebitAmountOriginal
                                                 ELSE $0
                                            END AS ClosingDebitAmountOC , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.DebitAmount
                                                 ELSE $0
                                            END AS ClosingDebitAmount ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.CreditAmountOriginal
                                                 ELSE $0
                                            END AS ClosingCreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.CreditAmount
                                                 ELSE $0
                                            END AS ClosingCreditAmount,
											CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN 0
                                                 ELSE 1
                                            END AS OrderType 
                                  FROM      dbo.GeneralLedger AS AOL
                                            INNER JOIN @tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumberPercent
                                            INNER JOIN dbo.Account AS AN ON AOL.Account = AN.AccountNumber
                                            INNER JOIN @tblListAccountObjectID AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID                                         
                                  WHERE     AOL.PostedDate <= @ToDate
                                            AND ( @CurrencyID IS NULL
                                                  OR AOL.CurrencyID = @CurrencyID
                                                )
                                ) AS RSNS                               
                        GROUP BY RSNS.AccountingObjectID ,
                                RSNS.AccountObjectCode ,  
                                RSNS.AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,
                                RSNS.AccountObjectAddress ,
                                RSNS.PostedDate ,
                                RSNS.NgayCtu , 
                                RSNS.SoCtu , 
                                RSNS.InvDate ,
                                RSNS.InvNo ,
                                RSNS.RefType ,
                                RSNS.RefID ,
                                RSNS.JournalMemo , 		  
                                RSNS.AccountNumber ,
                                RSNS.AccountCategoryKind ,
                                RSNS.CorrespondingAccountNumber,
								OrderType 		   
                        HAVING  SUM(DebitAmountOC) <> 0
                                OR SUM(DebitAmount) <> 0
                                OR SUM(CreditAmountOC) <> 0
                                OR SUM(CreditAmount) <> 0
                                OR SUM(ClosingDebitAmountOC - ClosingCreditAmountOC) <> 0
                                OR SUM(ClosingDebitAmount - ClosingCreditAmount) <> 0
                        ORDER BY RSNS.AccountObjectCode ,
                                RSNS.AccountNumber ,
                                RSNS.PostedDate ,
                                RSNS.NgayCtu ,
                                RSNS.SoCtu
                OPTION  ( RECOMPILE )    
                        
/* Tính số tồn */
        DECLARE @CloseAmountOC AS DECIMAL(22, 8) ,
            @CloseAmount AS DECIMAL(22, 8) ,
            @AccountObjectCode_tmp NVARCHAR(100) ,
            @AccountNumber_tmp NVARCHAR(20)
        SELECT  @CloseAmountOC = 0 ,
                @CloseAmount = 0 ,
                @AccountObjectCode_tmp = N'' ,
                @AccountNumber_tmp = N''
        UPDATE  #tblResult
        SET     @CloseAmountOC = ( CASE WHEN OrderType = 0 THEN ( CASE WHEN ClosingDebitAmountOC = 0 THEN ClosingCreditAmountOC
                                                                       ELSE -1 * ClosingDebitAmountOC
                                                                  END )
                                        WHEN @AccountObjectCode_tmp <> AccountObjectCode
                                             OR @AccountNumber_tmp <> AccountNumber THEN CreditAmountOC - DebitAmountOC
                                        ELSE @CloseAmountOC + CreditAmountOC - DebitAmountOC
                                   END ) ,
                ClosingDebitAmountOC = ( CASE WHEN AccountCategoryKind = 0 THEN -1 * @CloseAmountOC
                                              WHEN AccountCategoryKind = 1 THEN $0
                                              ELSE CASE WHEN @CloseAmountOC < 0 THEN -1 * @CloseAmountOC
                                                        ELSE $0
                                                   END
                                         END ) ,
                ClosingCreditAmountOC = ( CASE WHEN AccountCategoryKind = 1 THEN @CloseAmountOC
                                               WHEN AccountCategoryKind = 0 THEN $0
                                               ELSE CASE WHEN @CloseAmountOC > 0 THEN @CloseAmountOC
                                                         ELSE $0
                                                    END
                                          END ) ,
                @CloseAmount = ( CASE WHEN OrderType = 0 THEN ( CASE WHEN ClosingDebitAmount = 0 THEN ClosingCreditAmount
                                                                     ELSE -1 * ClosingDebitAmount
                                                                END )
                                      WHEN @AccountObjectCode_tmp <> AccountObjectCode
                                           OR @AccountNumber_tmp <> AccountNumber THEN CreditAmount - DebitAmount
                                      ELSE @CloseAmount + CreditAmount - DebitAmount
                                 END ) ,
                ClosingDebitAmount = ( CASE WHEN AccountCategoryKind = 0 THEN -1 * @CloseAmount
                                            WHEN AccountCategoryKind = 1 THEN $0
                                            ELSE CASE WHEN @CloseAmount < 0 THEN -1 * @CloseAmount
                                                      ELSE $0
                                                 END
                                       END ) ,
                ClosingCreditAmount = ( CASE WHEN AccountCategoryKind = 1 THEN @CloseAmount
                                             WHEN AccountCategoryKind = 0 THEN $0
                                             ELSE CASE WHEN @CloseAmount > 0 THEN @CloseAmount
                                                       ELSE $0
                                                  END
                                        END ) ,
                @AccountObjectCode_tmp = AccountObjectCode ,
                @AccountNumber_tmp = AccountNumber
        WHERE   OrderType <> 2
/*Lấy số liệu*/				
        SELECT  RS.PostedDate as Ngay_HT, 
		        RS.RefDate as ngayCtu,
				RS.RefNo as SoCtu,
				Rs.JournalMemo as DienGiai,
				'331' as TK_CONGNO,
				Rs.CorrespondingAccountNumber as TkDoiUng,
                RS.AccountObjectCode,
				RS.AccountObjectName,
				RS.AccountNumber,
				fn.OpeningDebitAmount as OpeningDebitAmount,
				fn.OpeningCreditAmount as OpeningCreditAmount,
				Rs.DebitAmount as DebitAmount,
				Rs.CreditAmount as CreditAmount,
				Rs.ClosingDebitAmount as ClosingDebitAmount,
				Rs.ClosingCreditAmount as ClosingCreditAmount
        FROM    #tblResult RS ,
		        [dbo].[Func_SoDu] (
   @FromDate
  ,@ToDate
  ,3) fn
  where rs.AccountNumber = fn.AccountNumber
  and rs.AccountObjectID = fn.AccountObjectID
        DROP TABLE #tblResult
    END				
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

/*

*/
ALTER PROCEDURE [dbo].[Proc_PO_PayableDetail]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountNumber NVARCHAR(20) ,
    @AccountObjectID AS NVARCHAR(MAX) ,
    @CurrencyID NVARCHAR(3)
AS 
    BEGIN     
        DECLARE @tblListAccountObjectID TABLE 
            (
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(25) ,
              AccountObjectName NVARCHAR(255) ,
              AccountObjectAddress NVARCHAR(255) ,
              AccountObjectTaxCode NVARCHAR(200) ,
              AccountObjectGroupListCode NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS
                                                       NULL ,
              AccountObjectGroupListName NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS
                                                       NULL 
            ) 
        INSERT  INTO @tblListAccountObjectID
                SELECT  AO.ID as AccountObjectID ,
                        AO.AccountingObjectCode ,
                        AO.AccountingObjectName ,
                        AO.Address ,
                        AO.TaxCode ,
                        AOG.AccountingObjectGroupCode ,
                        AOG.AccountingObjectGroupName 
                FROM    dbo.Func_ConvertStringIntoTable(@AccountObjectID, ',') tblAccountObjectSelected
                        INNER JOIN dbo.AccountingObject AO ON AO.ID = tblAccountObjectSelected.Value		
                        INNER JOIN dbo.AccountingObjectGroup AOG ON AO.AccountObjectGroupID = AOG.ID
      
        CREATE TABLE #tblResult
            (
              RowNum INT IDENTITY(1, 1)
                         PRIMARY KEY ,
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              AccountObjectName NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              AccountObjectGroupListCode NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS
                                                       NULL ,
              AccountObjectGroupListName NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS
                                                       NULL ,             
              AccountObjectAddress NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              PostedDate DATETIME ,
              RefDate DATETIME , 
              RefNo NVARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              DueDate DATETIME , 
              InvDate DATETIME ,
              InvNo NVARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS ,

              RefType INT ,
              RefID UNIQUEIDENTIFIER ,
              JournalMemo NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS , 
              AccountNumber NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS , 
              AccountCategoryKind INT ,
              CorrespondingAccountNumber NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS , 
              ExchangeRate DECIMAL(18, 4) , 
              DebitAmountOC MONEY ,	
              DebitAmount MONEY , 
              CreditAmountOC MONEY , 
              CreditAmount MONEY , 
              ClosingDebitAmountOC MONEY ,
              ClosingDebitAmount MONEY , 
              ClosingCreditAmountOC MONEY ,	
              ClosingCreditAmount MONEY ,
              InventoryItemCode NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              InventoryItemName NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              UnitName NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              Quantity DECIMAL(22, 8) ,
              UnitPrice DECIMAL(18, 4) , 
              BranchName NVARCHAR(128) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              IsBold BIT ,	
              OrderType INT ,
              GLJournalMemo NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              DocumentIncluded NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField1 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField2 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField3 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField4 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField5 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField6 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField7 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField8 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField9 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField10 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS,
               MainUnitName NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              MainQuantity DECIMAL(22, 8) ,
              MainUnitPrice DECIMAL(18, 4)  
              
            )
        
        DECLARE @tblAccountNumber TABLE
            (
              AccountNumber NVARCHAR(20) PRIMARY KEY ,
              AccountName NVARCHAR(255) ,
              AccountNumberPercent NVARCHAR(25) ,
              AccountCategoryKind INT
            )
        IF @AccountNumber IS NOT NULL 
            BEGIN
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = @AccountNumber
                        ORDER BY A.AccountNumber ,
                                A.AccountName 
                
            END
        ELSE 
            BEGIN 
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = '331'
                        ORDER BY A.AccountNumber ,
                                A.AccountName
            END
            
                
/*Lấy số dư đầu kỳ và dữ liệu phát sinh trong kỳ:*/ 
INSERT  INTO #tblResult
                        ( AccountObjectID ,
                          AccountObjectCode , 
                          AccountObjectName ,
                          AccountObjectGroupListCode , 
                          AccountObjectGroupListName ,      
                          AccountObjectAddress , 
                          PostedDate ,
                          RefDate ,
                          RefNo ,
                          InvDate ,
                          InvNo ,
                          RefType ,
                          RefID , 
                          JournalMemo , 				  
                          AccountNumber , 
                          AccountCategoryKind ,
                          CorrespondingAccountNumber ,	  			  
                          DebitAmountOC ,	
                          DebitAmount , 
                          CreditAmountOC ,
                          CreditAmount ,
                          ClosingDebitAmountOC , 
                          ClosingDebitAmount ,
                          ClosingCreditAmountOC ,
                          ClosingCreditAmount,
						  OrderType   
                        )
                        SELECT  AccountingObjectID ,
                                AccountObjectCode ,
                                AccountObjectName ,	
                                AccountObjectGroupListCode , 
                                AccountObjectGroupListName ,            
                                AccountObjectAddress , 
                                RSNS.PostedDate ,
                                RefDate ,
                                RefNo ,
                                InvDate ,
                                InvNo ,
                                RefType ,
                                RSNS.RefID , 
                                JournalMemo ,			  
                                AccountNumber , 
                                AccountCategoryKind , 
                                CorrespondingAccountNumber , 		  			  
                                SUM(DebitAmountOC) ,
                                SUM(DebitAmount) , 
                                SUM(CreditAmountOC) , 
                                SUM(CreditAmount) , 
                                ( CASE WHEN AccountCategoryKind = 0 THEN SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC)
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC) ) > 0 THEN ( SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmountOC ,      
                                ( CASE WHEN AccountCategoryKind = 0 THEN ( SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount) )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount) ) > 0 THEN SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount)
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmount , 
                                ( CASE WHEN AccountCategoryKind = 1 THEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) ) > 0 THEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmountOC ,
                                ( CASE WHEN AccountCategoryKind = 1 THEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) ) > 0 THEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmount,  
								  OrderType
                        FROM    ( SELECT    AOL.AccountingObjectID ,
                                            LAOI.AccountObjectCode ,   
                                            LAOI.AccountObjectName ,
                                            AccountObjectGroupListCode ,
                                            AccountObjectGroupListName ,         
                                            LAOI.AccountObjectAddress ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.PostedDate
                                            END AS PostedDate ,                      
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE RefDate
                                            END AS RefDate , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.RefNo
                                            END AS RefNo ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.InvoiceDate
                                            END AS InvDate ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.InvoiceNo
                                            END AS InvNo ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.ReferenceID
                                            END AS RefID ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.TypeID
                                            END AS RefType , 
                                            CASE WHEN AOL.PostedDate < @FromDate 
												      THEN NULL
                                                 ELSE CAST(AOL.DetailID AS NVARCHAR(50))
                                            END AS RefDetailID ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN N'Số dư đầu kỳ'
                                                 ELSE AOL.Description
                                            END AS JournalMemo , 
                                            TBAN.AccountNumber ,
                                            TBAN.AccountCategoryKind ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.AccountCorresponding
                                            END AS CorrespondingAccountNumber , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.DebitAmountOriginal
                                            END AS DebitAmountOC ,                             
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.DebitAmount
                                            END AS DebitAmount ,                                  
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.CreditAmountOriginal
                                            END AS CreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.CreditAmount
                                            END AS CreditAmount , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.DebitAmountOriginal
                                                 ELSE $0
                                            END AS ClosingDebitAmountOC , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.DebitAmount
                                                 ELSE $0
                                            END AS ClosingDebitAmount ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.CreditAmountOriginal
                                                 ELSE $0
                                            END AS ClosingCreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.CreditAmount
                                                 ELSE $0
                                            END AS ClosingCreditAmount,
											CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN 0
                                                 ELSE 1
                                            END AS OrderType 
                                  FROM      dbo.GeneralLedger AS AOL
                                            INNER JOIN @tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumberPercent
                                            INNER JOIN dbo.Account AS AN ON AOL.Account = AN.AccountNumber
                                            INNER JOIN @tblListAccountObjectID AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID                                         
                                  WHERE     AOL.PostedDate <= @ToDate
                                            AND ( @CurrencyID IS NULL
                                                  OR AOL.CurrencyID = @CurrencyID
                                                )
                                ) AS RSNS                               
                        GROUP BY RSNS.AccountingObjectID ,
                                RSNS.AccountObjectCode ,  
                                RSNS.AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,
                                RSNS.AccountObjectAddress ,
                                RSNS.PostedDate ,
                                RSNS.RefDate , 
                                RSNS.RefNo , 
                                RSNS.InvDate ,
                                RSNS.InvNo ,
                                RSNS.RefType ,
                                RSNS.RefID ,
                                RSNS.JournalMemo , 		  
                                RSNS.AccountNumber ,
                                RSNS.AccountCategoryKind ,
                                RSNS.CorrespondingAccountNumber,
								OrderType 		   
                        HAVING  SUM(DebitAmountOC) <> 0
                                OR SUM(DebitAmount) <> 0
                                OR SUM(CreditAmountOC) <> 0
                                OR SUM(CreditAmount) <> 0
                                OR SUM(ClosingDebitAmountOC - ClosingCreditAmountOC) <> 0
                                OR SUM(ClosingDebitAmount - ClosingCreditAmount) <> 0
                        ORDER BY RSNS.AccountObjectCode ,
                                RSNS.AccountNumber ,
                                RSNS.PostedDate ,
                                RSNS.RefDate ,
                                RSNS.RefNo
                OPTION  ( RECOMPILE )    
                        
/* Tính số tồn */
        DECLARE @CloseAmountOC AS DECIMAL(22, 8) ,
            @CloseAmount AS DECIMAL(22, 8) ,
            @AccountObjectCode_tmp NVARCHAR(100) ,
            @AccountNumber_tmp NVARCHAR(20)
        SELECT  @CloseAmountOC = 0 ,
                @CloseAmount = 0 ,
                @AccountObjectCode_tmp = N'' ,
                @AccountNumber_tmp = N''
        UPDATE  #tblResult
        SET     @CloseAmountOC = ( CASE WHEN OrderType = 0 THEN ( CASE WHEN ClosingDebitAmountOC = 0 THEN ClosingCreditAmountOC
                                                                       ELSE -1 * ClosingDebitAmountOC
                                                                  END )
                                        WHEN @AccountObjectCode_tmp <> AccountObjectCode
                                             OR @AccountNumber_tmp <> AccountNumber THEN CreditAmountOC - DebitAmountOC
                                        ELSE @CloseAmountOC + CreditAmountOC - DebitAmountOC
                                   END ) ,
                ClosingDebitAmountOC = ( CASE WHEN AccountCategoryKind = 0 THEN -1 * @CloseAmountOC
                                              WHEN AccountCategoryKind = 1 THEN $0
                                              ELSE CASE WHEN @CloseAmountOC < 0 THEN -1 * @CloseAmountOC
                                                        ELSE $0
                                                   END
                                         END ) ,
                ClosingCreditAmountOC = ( CASE WHEN AccountCategoryKind = 1 THEN @CloseAmountOC
                                               WHEN AccountCategoryKind = 0 THEN $0
                                               ELSE CASE WHEN @CloseAmountOC > 0 THEN @CloseAmountOC
                                                         ELSE $0
                                                    END
                                          END ) ,
                @CloseAmount = ( CASE WHEN OrderType = 0 THEN ( CASE WHEN ClosingDebitAmount = 0 THEN ClosingCreditAmount
                                                                     ELSE -1 * ClosingDebitAmount
                                                                END )
                                      WHEN @AccountObjectCode_tmp <> AccountObjectCode
                                           OR @AccountNumber_tmp <> AccountNumber THEN CreditAmount - DebitAmount
                                      ELSE @CloseAmount + CreditAmount - DebitAmount
                                 END ) ,
                ClosingDebitAmount = ( CASE WHEN AccountCategoryKind = 0 THEN -1 * @CloseAmount
                                            WHEN AccountCategoryKind = 1 THEN $0
                                            ELSE CASE WHEN @CloseAmount < 0 THEN -1 * @CloseAmount
                                                      ELSE $0
                                                 END
                                       END ) ,
                ClosingCreditAmount = ( CASE WHEN AccountCategoryKind = 1 THEN @CloseAmount
                                             WHEN AccountCategoryKind = 0 THEN $0
                                             ELSE CASE WHEN @CloseAmount > 0 THEN @CloseAmount
                                                       ELSE $0
                                                  END
                                        END ) ,
                @AccountObjectCode_tmp = AccountObjectCode ,
                @AccountNumber_tmp = AccountNumber
        WHERE   OrderType <> 2
/*Lấy số liệu*/				
        SELECT  
                RS.AccountObjectCode,
				RS.AccountObjectName,
				RS.AccountNumber,
				sum(fn.OpeningDebitAmount) OpeningDebitAmount,
				sum(fn.OpeningCreditAmount) OpeningCreditAmount,
				sum(Rs.DebitAmount) DebitAmount,
				sum(Rs.CreditAmount) CreditAmount,
				sum(Rs.ClosingDebitAmount) ClosingDebitAmount,
				sum(Rs.ClosingCreditAmount) ClosingCreditAmount
        FROM    #tblResult RS ,
		        [dbo].[Func_SoDu] (
   @FromDate
  ,@ToDate
  ,3) fn
  where rs.AccountNumber = fn.AccountNumber
  and rs.AccountObjectID = fn.AccountObjectID
  group by RS.AccountObjectCode,
				RS.AccountObjectName,
				RS.AccountNumber
        DROP TABLE #tblResult
    END				
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalaryTaxInsuranceRegulation]
  ADD [IsWorkingOnSaturdayNoon] [bit] NULL DEFAULT (0)
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalaryTaxInsuranceRegulation]
  ADD [IsWorkingOnSundayNoon] [bit] NULL DEFAULT (0)
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[CPUncompleteDetailItem] (
  [ID] [uniqueidentifier] NOT NULL,
  [CPUncompleteID] [uniqueidentifier] NULL,
  [CPUncompleteDetailID] [uniqueidentifier] NULL,
  [MaterialGoodsID] [uniqueidentifier] NULL,
  [Quantity] [decimal](25, 10) NULL,
  [PercentComplete] [decimal](25, 10) NULL,
  [UnitPrice] [money] NULL,
  CONSTRAINT [PK_CPUncompleteDetailItem] PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[CPUncompleteDetail] (
  [ID] [uniqueidentifier] NOT NULL,
  [CPUncompleteID] [uniqueidentifier] NULL,
  [CPPeriodID] [uniqueidentifier] NULL,
  [CostSetID] [uniqueidentifier] NULL,
  [DirectMaterialAmount] [money] NULL,
  [DirectLaborAmount] [money] NULL,
  [GeneralExpensesAmount] [money] NULL,
  [TotalCostAmount] [money] NULL,
  CONSTRAINT [PK_CPUncompleteDetail] PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[CPUncomplete] (
  [ID] [uniqueidentifier] NOT NULL,
  [CPPeriodID] [uniqueidentifier] NULL,
  [CostSetID] [uniqueidentifier] NULL,
  [CPPeriodDetailID] [uniqueidentifier] NULL,
  [UncompleteType] [int] NULL,
  CONSTRAINT [PK_CPUncomplete] PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[CPResult] (
  [ID] [uniqueidentifier] NOT NULL,
  [CPPeriodID] [uniqueidentifier] NULL,
  [CPPeriodDetailID] [uniqueidentifier] NULL,
  [CostSetID] [uniqueidentifier] NULL,
  [MaterialGoodsID] [uniqueidentifier] NULL,
  [Coefficien] [decimal](25, 10) NULL,
  [DirectMaterialAmount] [money] NULL,
  [DirectLaborAmount] [money] NULL,
  [GeneralExpensesAmount] [money] NULL,
  [TotalCostAmount] [money] NULL,
  [TotalQuantity] [decimal](25, 10) NULL,
  [UnitPrice] [money] NULL,
  CONSTRAINT [PK_CPResult] PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @res int
EXEC @res = sp_rename N'[dbo].[CPProductQuantum]', N'tmp_devart_CPProductQuantum', 'OBJECT'
IF @res <> 0
  RAISERROR ('Error while Rename table [dbo].[CPProductQuantum] to [dbo].[tmp_devart_CPProductQuantum]', 11, 1 );
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @res int
EXEC @res = sp_rename N'[dbo].[PK_CPProductQuantum]', N'tmp_devart_PK_CPProductQuantum', 'OBJECT'
IF @res <> 0
  RAISERROR ('Error while Rename index [PK_CPProductQuantum] to [tmp_devart_PK_CPProductQuantum] on table [dbo].[CPProductQuantum]', 11, 1 );
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[CPProductQuantum] (
  [ID] [uniqueidentifier] NOT NULL,
  [DirectMaterialAmount] [money] NULL,
  [DirectLaborAmount] [money] NULL,
  [GeneralExpensesAmount] [money] NULL,
  [TotalCostAmount] [money] NULL
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

INSERT [dbo].[CPProductQuantum](ID, GeneralExpensesAmount)
  SELECT ID, Amount FROM [dbo].[tmp_devart_CPProductQuantum] WITH (NOLOCK)

if @@ERROR = 0
  DROP TABLE [dbo].[tmp_devart_CPProductQuantum]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[CPProductQuantum]
  ADD CONSTRAINT [PK_CPProductQuantum] PRIMARY KEY CLUSTERED ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[CPPeriodDetail] (
  [ID] [uniqueidentifier] NOT NULL,
  [CPPeriodID] [uniqueidentifier] NULL,
  [CostSetID] [uniqueidentifier] NULL,
  [ContractID] [uniqueidentifier] NULL,
  CONSTRAINT [PK_CPPeriodDetail] PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[CPAllocationRate] (
  [ID] [uniqueidentifier] NOT NULL,
  [CPPeriodID] [uniqueidentifier] NULL,
  [CPPeriodDetailID] [uniqueidentifier] NULL,
  [AllocationMethod] [int] NULL,
  [CostSetID] [uniqueidentifier] NULL,
  [MaterialGoodsID] [uniqueidentifier] NULL,
  [IsStandardItem] [bit] NULL,
  [Quantity] [decimal](25, 10) NULL,
  [PriceQuantum] [money] NULL,
  [Coefficien] [decimal](25, 10) NULL,
  [QuantityStandard] [decimal](25, 10) NULL,
  [AllocationStandard] [decimal](25, 10) NULL,
  [AllocatedRate] [decimal](25, 10) NULL,
  CONSTRAINT [PK_CPAllocationRate] PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[CPAllocationQuantum] (
  [ID] [uniqueidentifier] NOT NULL,
  [DirectMaterialAmount] [money] NULL,
  [DirectLaborAmount] [money] NULL,
  [GeneralExpensesAmount] [money] NULL,
  [TotalCostAmount] [money] NULL,
  CONSTRAINT [PK_CPAllocationQuantum] PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[CPAllocationGeneralExpenseDetail] (
  [ID] [uniqueidentifier] NOT NULL,
  [CPAllocationGeneralExpenseID] [uniqueidentifier] NULL,
  [CostSetID] [uniqueidentifier] NULL,
  [ContractID] [uniqueidentifier] NULL,
  [ExpenseItemID] [uniqueidentifier] NULL,
  [AllocatedRate] [decimal](25, 10) NULL,
  [AllocatedAmount] [money] NULL,
  [CPPeriodID] [uniqueidentifier] NULL,
  CONSTRAINT [PK_CPAllocationGeneralExpenseDetail] PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[CPAllocationGeneralExpense] (
  [ID] [uniqueidentifier] NOT NULL,
  [CPPeriodID] [uniqueidentifier] NULL,
  [ExpenseItemID] [uniqueidentifier] NULL,
  [TotalCost] [money] NULL,
  [UnallocatedAmount] [money] NULL,
  [AllocatedRate] [decimal](25, 10) NULL,
  [AllocatedAmount] [money] NULL,
  [AllocationMethod] [int] NULL,
  CONSTRAINT [PK_CPAllocationGeneralExpense] PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
GO
DISABLE TRIGGER ALL ON dbo.TemplateColumn
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.GenCode
  DROP CONSTRAINT FK_GenCode_TypeGroup
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.Template
  DROP CONSTRAINT FK_Template_Type
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateColumn
  DROP CONSTRAINT FK_TemplateColumn_TemplateDetail
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateDetail
  DROP CONSTRAINT FK_TemplateDetail_Template
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.Type
  DROP CONSTRAINT FK_Type_TypeGroup
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153AdjustAnnouncement
  DROP CONSTRAINT FK__TT153Adju__TypeI__253C7D7E
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153DeletedInvoice
  DROP CONSTRAINT FK__TT153Dele__TypeI__4EDDB18F
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153DestructionInvoice
  DROP CONSTRAINT FK__TT153Dest__TypeI__22AA2996
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153LostInvoice
  DROP CONSTRAINT FK__TT153Lost__TypeI__1FCDBCEB
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153PublishInvoice
  DROP CONSTRAINT FK__TT153Publ__TypeI__1CF15040
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153RegisterInvoice
  DROP CONSTRAINT FK__TT153Regi__TypeI__1A14E395
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.AccountDefault SET DefaultAccount = N'632' WHERE ID = 'aa7a21a9-6483-432a-b84d-04e14343bcbc'
UPDATE dbo.AccountDefault SET DefaultAccount = N'632' WHERE ID = '03d48d3b-e95f-4f2d-82ff-0a8bd91e5960'
UPDATE dbo.AccountDefault SET FilterAccount = N'152;153;154;156;241;242;611;642' WHERE ID = '6dd29028-cceb-461a-81fc-1169b233676e'
UPDATE dbo.AccountDefault SET FilterAccount = N'152;153;154;156;241;242;611;642' WHERE ID = '1728e7a8-a7e0-48fd-b619-2f5d33a15f5f'
UPDATE dbo.AccountDefault SET DefaultAccount = N'632' WHERE ID = '0ce47fb5-0e10-4dac-ad6d-364531dc5446'
UPDATE dbo.AccountDefault SET FilterAccount = N'152;153;154;156;241;242;611;642' WHERE ID = '051abd2d-68b9-403d-be12-36a32ae85ca2'
UPDATE dbo.AccountDefault SET FilterAccount = N'131;1388' WHERE ID = '50cef9ce-3c7e-4a09-8dbc-39c0c28f2eba'
UPDATE dbo.AccountDefault SET FilterAccount = N'111;112;1121;511;635' WHERE ID = '1a4bb09c-939d-4228-9d02-4f62e68326b5'
UPDATE dbo.AccountDefault SET FilterAccount = N'152;153;154;156;241;242;611;642' WHERE ID = 'd62de3c1-6dec-4fbe-a3dd-5deb33825902'
UPDATE dbo.AccountDefault SET DefaultAccount = N'632' WHERE ID = 'f1e9fc06-e2c5-4cac-98d1-665b3d748860'
UPDATE dbo.AccountDefault SET FilterAccount = N'152;153;154;156;241;242;611;642' WHERE ID = 'f9c7a978-2a75-4ab5-be4f-6ef16dea90f6'
UPDATE dbo.AccountDefault SET DefaultAccount = N'632' WHERE ID = '5e1f187f-2dea-49fa-970f-709c56e5d93d'
UPDATE dbo.AccountDefault SET FilterAccount = N'152;153;154;156;241;242;611;642' WHERE ID = '8569bef5-2937-4b2c-8b6e-836c909e0fc1'
UPDATE dbo.AccountDefault SET FilterAccount = N'111;112' WHERE ID = 'dfc93e2a-766d-4f55-9293-84444defbc29'
UPDATE dbo.AccountDefault SET FilterAccount = N'152;153;154;156;241;242;611;642' WHERE ID = 'efb9e226-a2c8-47dc-a7e7-9fde4e68d04b'
UPDATE dbo.AccountDefault SET FilterAccount = N'152;153;154;156;241;242;611;642' WHERE ID = '2a723e19-3d37-47e6-80bf-c6a7a1d49110'
UPDATE dbo.AccountDefault SET FilterAccount = N'152;153;154;156;241;242;611;642' WHERE ID = '770c50cb-5c96-4f18-bd5b-cdf715edda17'
UPDATE dbo.AccountDefault SET FilterAccount = N'111;1121', DefaultAccount = NULL WHERE ID = '82855550-4169-4ce0-adbf-d7f451aa472f'
UPDATE dbo.AccountDefault SET DefaultAccount = N'632' WHERE ID = 'abda3c94-8d10-4550-9db2-efe94354be1a'
UPDATE dbo.AccountDefault SET FilterAccount = N'152;153;154;156;241;242;611;642' WHERE ID = 'd52a0e9a-2620-4b8a-b793-ff3f7abc8a72'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

INSERT dbo.Bank(ID, BankCode, BankName, BankNameRepresent, Address, Description, BankTransactionType, Icon, IsActive) VALUES ('1339cc02-6ac4-448c-935b-3aa55976b73c', N'Vietinbank', N'Ngân hàng TMCP Công Thương Việt Nam', N'Vietnam Joint Stock Commercial Bank for Industry and Trade', N'', N'Ngân hàng TMCP Công Thương Việt Nam', 0, NULL, 1)
INSERT dbo.Bank(ID, BankCode, BankName, BankNameRepresent, Address, Description, BankTransactionType, Icon, IsActive) VALUES ('f73cb6ad-567a-4e29-a06f-4ae0faa1cae0', N'Vietcombank', N'Ngân hàng TMCP Ngoại thương Việt Nam', N'Joint Stock Commercial Bank for Foreign Trade of Viet Nam ', NULL, N'Ngân hàng TMCP Ngoại thương Việt Nam', 0, NULL, 1)
EXEC(N'INSERT dbo.Bank(ID, BankCode, BankName, BankNameRepresent, Address, Description, BankTransactionType, Icon, IsActive) VALUES (''9593096a-5bdc-47b2-8bcb-70e288b3e104'', N''Agribank'', N''Ngân hàng Nông nghiệp và Phát triển nông thôn Việt Nam'', N''Vietnam Bank for Agriculture and Rural Development '', N'''', N''Ngân hàng Nông nghiệp và Phát triển nông thôn Việt Nam'', 0, 0xFFD8FFE000104A4649460001010101F301F30000FFEE000E41646F626500640000000001FFE117DA4578696600004D4D002A000000080007011200030000000100010000011A00050000000100000062011B0005000000010000006A012800030000000100020000013100020000001E000000720132000200000014000000908769000400000001000000A4000000D001F3FFBE0001000001F3FFBE0001000041646F62652050686F746F73686F7020435336202857696E646F77732900323031383A30363A32362031303A33313A3237000003A001000300000001FFFF0000A00200040000000100000080A003000400000001000000800000000000000006010300030000000100060000011A0005000000010000011E011B0005000000010000012601280003000000010002000002010004000000010000012E0202000400000001000016A40000000000000048000000010000004800000001FFD8FFED000C41646F62655F434D0002FFEE000E41646F626500648000000001FFDB0084000C08080809080C09090C110B0A0B11150F0C0C0F1518131315131318110C0C0C0C0C0C110C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C010D0B0B0D0E0D100E0E10140E0E0E14140E0E0E0E14110C0C0C0C0C11110C0C0C0C0C0C110C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0CFFC00011080080008003012200021101031101FFDD00040008FFC4013F0000010501010101010100000000000000030001020405060708090A0B0100010501010101010100000000000000010002030405060708090A0B1000010401030204020507060805030C33010002110304211231054151611322718132061491A1B14223241552C16233347282D14307259253F0E1F163733516A2B283264493546445C2A3743617D255E265F2B384C3D375E3F3462794A485B495C4D4E4F4A5B5C5D5E5F55666768696A6B6C6D6E6F637475767778797A7B7C7D7E7F711000202010204040304050607070605350100021103213112044151617122130532819114A1B14223C152D1F0332462E1728292435315637334F1250616A2B283072635C2D2449354A317644555367465E2F2B384C3D375E3F34694A485B495C4D4E4F4A5B5C5D5E5F55666768696A6B6C6D6E6F62737475767778797A7B7C7FFDA000C03010002110311003F00F54004250142F73DB43DD5825E1A4B4000998D21AE731AEFF3D65743EA19F9B75A721CD353040606377074FF0084B6AC8BD9FD86B3FEB895AD320088EBABB101280BCBFEB77D64EA19DD52FC3C7B5F56263BCD4CAEB25BBDCDF6BDF66DFA7B9FF4160EECED75B74E7E9764C3935D9A73F884632318C0CA8D5DD3EDD01280BC41AFCD700E6BAD20F041711A28FDA323FD2BFF00CE3FDE87B9E0B7FD243FCD9FF1BFF417DC6025017877DA723FD2BFFCE3FDE97DA727FD2BFF00CE3FDE97B9E0AFF490FF00367FC6FEC7DC60250178CF4CC5EA5D4F2463E3D8F1A6E7BDCE76D6B7F79CBAEC4FAB3814B47AF65B92F1CB9F639ADF956C72A5CDFC5B97E54F0E4B94C8BE087AA5FE137395966E607143188C3F7E72D3FC1F4BDC404A02E2F22FE99F53B1ACCD3FA7EA99B271719C4915D67E8B7527D3A9BFE19FFE16CFD1AE032FAA751CDC9B32B2322C75D699710E207F55AD07DAC6FE6B55D196E3124106401E13BC6FA49B98395C990132A80B207E9717FD17DCE025017837DAB2BFD359FE7BBFBD2FB5657FA6B3FCF77F7A5EE78337DC0FEFFF00CD7DE60244085E2BD23EB2756E9194DC8A2F7BEB041B71DEE258F6FE734B5DBB6FF26C5ECF4DCCBF1EBBEBFA16B03DBF070DCD4E8CADAF9F9796222CD83B17FFD0F4CEA15B2DC0C8ADE4063EA70712EDA208EEFF00CD591D0AEA6FB6AC9F472BD514FA6D7D95B1ACDAF7EEDAE763B2BADDE9EC67B7FC0D7FF18B6F21BBB1AC6FB75611EF24378FCF737DDB566745C5C8A6F7BF2326BC87B99C576D8E6B44FE6516BEC6B1BFF09F4D03B862983C717CB7AB39CDEB198E692D73722C21C342087BB50BB7665E4FD63FA8F69AEC70EA18A22DDA482F755EF876DFA7F68C7FFC1570FD63FE56CDFF00C316FF00D5B96DFD41EADF61EB3F64B0C519E3D333C0B1B2EA5DFDAF7D5FF5C5144EB5DF473304EB2CA04FA725C0FD7E52D3C7CCCBE9DF56DFE9DCF61EA76ECA980900534EB7D95C7D0F56EB1B4FF61EB1569FD63CAA2FEA96D588D15E162138F8CC1C06B0BB7BBFEB973ACB166207F260CA75E1BB101C23FEE94ACF4EE9B97D4B23D0C56EE7012F71D1AD1FBCF72ACBBAFAA94538FD16BBB40EC8739EF778C1D8C6FF0065AD54BE23CDCB96C1C701C539486380FEB4BFF456CFC3B941CCE6E191A84471CABAFF00557E93815F42C7B5877E55D6BD83F44C24B8B86DAEAAFF0093BCFE7395FEB9D5717A061B6DCBDB666DAD9A3081913FBF7BFF00D0B1DFBBFCE7F36A5D63ACE37D5DC6193900599B603F64C49D64FB4DD6C7F375B7FD7FE0FCCB3F3F2FA8E5D9999961B6FB4CB9C'
+ N'781E0C637F358DFCD6AAFC8FC3C1FE93CE438F9A99E2A9ED8C0F92F1FCBC7FF0041E9B0E11A461E9C101C200FD33D7D5FBAACECECBEA19766666586DBED32E71FC1AD1F9AC6FE6B5012496A37400050524924929677D13F05EE7D27FE49C3FF00C2F57FD4357863BE89F82F73E93FF24E1FFE17ABFEA1AA4C7D5A5CFF00CB0F32FF00FFD1F4FCAFE896EAD1EC3ABE0378FCEDE1CCFF0039617D59B31DD6D96D156437D702C77A94D6C682E207B6FA18C6FB76FF0032B7335EC6615CF796B5ADADC4B9E4B5A34FCE733DFF00E62C9E826F6E53DB976D86EB2BDCCAEC75CD6ED046B4E3E4FEEEE6FA966FF5903B8629FF003907CC7AC7FCAD9BFF00862DFF00AB72A931A8D08EEAF750BACA3ADE5DD5C6F6645A46E01C3E9BB963C39AE5DBF5CA1B4754E8D4616050EAB2DD19558A185AE6935876F3B3D9E9B1CF7A8AAEDC98E1E33395D70C80DAFE797087CE925B5F5C313A5E275A7D3D2CB7D1D8D3631877359612EDF5B7FB3B3D9F98B229A6EC8B99450C365B610D631A24927B04D228D314E063330DC835A2A9A2EC8B5B450C7596D876B18D1249F26AF4AC3C4C7C4A2BAD82195B00D750047BF6FF005BE93966534F4EFA8DD3865E586E4F5CC96915540E8D1DDAD3F9B537FC35DFE150BA3F53CAEB9D16EA9D1464B8BB1FD66B7D80386EDED67E6ECADDB362C8F8EE19CB16290970C232ACBFD5193E599FEEBD17C23949E2C73C9315EE57D0078AEA19D939F996E564DA6EB1EE203DDFBA3DB5B7FB2C55D5FEADD0B3FA3BDADC868752F3155ECD58E8FCDFE43FF90E5416A63C90C9013C721381DA435768550AD94924927A549249005C435A09713000D4927B0494B6D73BDAD05CE768D68D49278017BAF4C63EBE99895D8D2C7B28ADAE69D08218D0E695C3F46E8B83F55BA78EBDD75A1D9C44E1E21E5AE8DCD1B4FF00DA8FDF77FDA75DDE1DEEC8C2A321CD0D75D532C2D1A805CD0EDBF8A96029CBE6F9986498C7137C1A9FABFFD2F4FCAF4BEC96FAC03AAD8EDE0B83416C7BA5EE2DD9FD6595D0B23A6DF739B858FB1B5B086E41B5B6EE0E76EB1AC3EADB77BACF75966D5A79A2D38570A5FE9DBE9BB63CB4BE0C68EF4C076FFF003563744B4DB9EF663B8D22B60FB452F2EB9CE323DFEA3D95FA3FD4DDEFFF0041FA340EE18A67D71FE1ABE6DD59AE7757CC6B41738E45A001A93EF72EEFEB564756C6EA1D1074F3687B896BD8C92D76B54B2D6FD177B7F7970BD4ADB69EB397652F75763322D2D7B490E077BB8704F4F51EB9916B28A72F26CB6C3B58C6D8F2E24F61EE510357E2E5C32887B91F55CE43E5FEA49DCFAF38183FB769C7E955B4E55CD8BE8A47F8427F47EC6FD1B1EDFA7FF6E2D0A69E9DF51BA70CCCC0DC9EB792D229A41FA3FC96FEED4DFF000D77E7FF00375A7A6AC0FA91D3FEDD9DB72BAE6534FA554CED9FA4D0EFF46DFF000F7FF84FA0B84EA1D432FA965D9999961B2FB4EA7B01F9AC637F358D449A37D4FF00CD76790F8771CCF31963C366E305750EA197D4B2ECCCCCB0DB7DA65C7B01F9AC637F358CFCD57FEAEFD607F46B9EDB186DC4BE0DAC6FD26B8716D73FF498B2125066C30CD8E58F20E284F70EE708AAAD1F477E4F4AFAC1D33231F1EE6DAD7B0CB4E8F6387BAB79ADDEEF6B9717D5BEAE752E94CF56E02EC631FAC573B44FFA469F75487D13AA7ECBCD17BABF5A87B7D3BEAEE5A75967F2D8E5DFE0F55E93D5AA2DC7B59735E36D98EFD1FB4FD26BEA72C49FBFF0B9FEAA32CBCA4CF14B8B5E1FD13EB8FC9359AC36D43E6292B5D570860752C9C313B69B08613DD87DCCFF00A2AA80490009274006A492B7612138C671D6320251FEECB6645005C4068249D001A925777D13A260FD57C11D7BAF343B34FF0043C3305CD772DD3FEE47FEDB7F5D3F43E8985F55F0475EEBE01CC70FD530CC1731C756E9FF00727FF6D9733D67ACE6F59CC765E5BBCABAC7D1637F719FF92527CBA9DDC6F8A7C4C63071623723B95BAD759CDEB196ECBCB76BA8AEB1F458DFDC62F5EE97FF0025627FC457FF0050D5E26782BDB3A5FF00C9589FF115FF00D4353B1EE5CAF87C8CA79252364D6AFF00FFD3F4DCEF53EC37FA6CF55FE9BB6D6091B8C7D1DD5FBFFCC595D031EEC7B835B538633EB71DCF63E9D8E0E0D6B3D0B2DB77FADEFB377F3B5FF84FE716BE53D95E25CFB086B1B5B8B8905C000D33EC6439DFD9585D06B6D39C3D711917D6E359008696B0B37B59532FBEAC7F4FD4AF7B1FFA4FF8440EE18A7FCE47F957FE8CF9D751A6DBFADE551430D96D9936358C6892497BB40BACA28E9FF51FA70CDCD0DC8EB792D8A6907E8F8B5A7F36B6FF0086BBF3FF009BAD58C96F4BFA9CDC8EAB95B727AC675963B1AA1D839C5DB5BFB95B777EB17FFD6D79EF51EA397D4B32CCDCCB0D97DA753D80FCDAEB6FE656C51FCBE7F92FF86FC32E5EF651D4F085750EA197D4B32CCDCCB0D97DA753D80FCDAEB6FE656C55D4995D966E35B1CFD8D2F7ED04ED68E5EE8FA2CD54531DE000143A292492492A5B7F536CA59D75ACB403EB54FAD93FBDA3E3FB5B56229D0EBD97D6FC79FB435ED34EDD5DBC1F66D0A2E6317BB87263BE1E38CA3C5FBBE2822C10F67F5D7A6D4FE9B5E656D3EBE3D82B9E5CE6586361FCE76DB3E822F43E8985F55F05BD7FAF09CD3FD0F13F39AE23DBEDFF00B91FFB6FFD75AE3A87A38EDEA1938E7F556FAF7D0402458D6EB5B27DBBEA73B7EF5C1F59EB399D6735D9794EF2AAA1F458DFDC67FDFDDF9EA8FC0F21FBA98C8F14F1E4946B7E189F56EE3FC4F9F96080C71F9A60EBE0AEB3D6733ACE63B2F2DDE55D63E8B1BFB8CFFC92'
+ N'A2924B4DE6A52322652364EE563C15ED9D2FFE4AC4FF0088AFFEA1ABC4CF057B674BFF0092B13FE22BFF00A86A931EE5BFF0DF9A7E41FFD4F50BC038D603A82C7481CF1F072C4FABB51A722CAE0B6A00FA2C1B61AC3B090EDB8F47BFD5F51DF4D6CE65072312DA839CD2F63802C3B5D25A5BA38AC2E90CC6FDAA6B6537E3DCC167A8C360701B7D0645AD67D1AECD9EA63FFA6DF7A0770C593E7817CD7EB0E5646575BCDB722C363C5CF6349ECC639CDAD8DFDD635AB3D5BEB1FF002BE77FE18B7FEADCA181836E7E5D589516B1D6982F790D6B5BF9F63DCE2DF6B1BEE509DDE8234203A001EC7EA06274F6B6CC7CE1391D669B1B4B0C7F47ACFA768FDEDD7D8EB3FB18CB8FEA7816F4DEA1918176B6633CB09F11F4ABB3FEB95963D74993D632307EB0D38F89D3F15E70CB29C2B0EB6BA9AC7A6D73721B77A4DF5ABF51FF00F5D56FFC62E06364D94F58C2B6AB1C19E9E5B19634B801FCCDBB5AEF772EAACFFAD2711A7F75AF0998E5B3B6617E528FCBFF0035E1D2492009200124E8004C6D28024800493A00352495DE7D5AFAAD6F4B2DCECF6C66595835D246B535E5DF48FF00A77359FF005B4FF567EABFECC0CCFCF6FF00941CD0FAAA709F41A7E8B8CFFDAA77FECBFF00C62D8CBB9B432CCBBACF4E96366E7924B8C6ADDAE77E73FE8AC6F8B73D0E19F278ECE69F0C6463B46CF17B7FD69CD82792F620407CD2FDEFFD05C7FAD9D5A9C7C27E031C1D939036BDA3F32BFCEDFF00D7FCD5C52ED3A5F4CE8BD57A16775BC8C33F68C73738B05AF01E58DF59BBB5F67D2D9EC55FA77D5EE95D7BA164676256EC0CBC52E696EF3654F2D68B46EF57DECDDBB6FD35A1C8722395C231837297AE67BCDE679E3939ACA260C78784FB5117FCDC7AFF0079E4D2563A73B18665432A9FB452E706BEBDC5861C625AF67E72E9BEB274EFAB9D073E8C5760D97D36B3D4B2C17B9AF00B9CCFD1B7E87B76EEF72B40696D186232899F1002240377FA5B3C81E0AF6CE97FF25627FC457FF50D5E6FF597EAA55D3B0EAEA9D3AE393D3AF0D32F8DCDDE3756E9686EEADFFD55E8FD267F64E1CF3F67ABFEA1A9F0144B7B91C72C73C9190D6817FFD5F52B5C594BDED8DCD6922648903BEDF72CBE939F93956EFBEFA2D69648FB2B98EAB9DA357BBED5BBFB1B16AB9BB985B246E11234227C0AC0C6C8E89D3FA8578F8B884587D8FCAB643F6B8B8EFA9D91FA6BE9F599FA77D7FA2AFE9A058E66A513741F2DEB1FF2BE77FE18B7FEADCAA2DCFAE3D172BA5F59C87D8C3F66CAB1D6D1747B4879DEEAF77FA4ADDF9AB0D427777F19061123514164A02494890DFCE3A01DCA0B97009200124E800D492577BF54BEAE53D3B6752EA15FAB9BCD14BB46D5FCB7FD2DF7FF0067F45FF18A3D03EAE62F43C56757EB7657466DD0DC4A6D207A73F4ED76EFFB53E9EE77FC07FC623E4FD6FE894C8658EC870E05434FF3DFB5AB3FE23CCF358A50C5CAE333C921C52970F18C71FD1FEA714BFAED3CDCD61A909648C603D24F170F17F5436FABF50CEC3C2CBCEB5F5BEDDD356840F76D6575064FE67F5BDEB84CEEA99FD41C0E5DCEB00D5ACE183FAB5B7DAAC75AFAC17F56735AE029C6ACCB290675FDFB1DF9CF599B9BE212E439496312CDCC08CB99C923394EA3C50BFD1E2FFBC79DF88F3BEEC863C3290C311C3D6B23DEFD537B59F52BAA3DCC1635BEB9756E24070153258E2DF77B94FA852327EA6B6EFAB1FAB62997E6623357B84017B1F6BA6DDF56DF7FFA6A572F87F5B3AA6161FD8718D0CC62087D7E93087C8DAFF5777D3DEDFA685D37EB2F54E946EFB05ADA5990773EBDAD2C0477AD8E9D8B4F88501E14C6398C621181BAE0E09480F5C7FBA5A58BFD2A9FEBB7F285D57F8CC20755C59FFB8FFF007F7AE66AEA4FAB38E736BA4D8497061634D61C7F39B4FD06AD1BBEB9757BED65F77D9ECBEAD2AB9F4D6E7B3FE2DCE6FB5341144776184A031CE049F598EA0748BD2F5DB9983F5031B0B286CC9BEAA98CA5DA3A416DCEDCDFA4DF4D83DDFCB5D674AFF92B0FFF000BD7FF0050D5E495B7AC7D63EA2DACBDF979364377BB56B1BDDCE8F6555317B0E3D031F1AAC76996D2C6B013DC346D5240D927E8DFE566724A5200884631C71BFEABFFD6F541C2CAEBB859390C65955A5ADA887C6DDCD66D9B1F90FA5A0D998EDADD94E2FF0037EA7E916A82212909144A22428BCEE3753CABB15EE7637DAEAF5DB8B5635C436E276EFB8E4FACDF45F90C77B9D5FE8EAA99EA7E911B2DFD3312FBEBB3A557B28ADB69B76D21A43CFA6C6FBBDCDFD26F6EE7FB18B61D8F8EE7B2C2D1BAA717B234F7381639F03E93B6B9C8591818B9163ECB649B18CADC2742D63FD66FB7FAFF00490A2B00C918D099716ECEE8ADC76595F4FA5B658E6B1A5CCA434179B5BBBD571F49CCFD5DFB2CDFFA4577A7D9D22DC9763D388CAEEC773DAE77A6C6C3AA2C1BDBB47F84F53F46E6A2BFA1E13A363ECA5CD787B1D5BA0B4836BB6B010E6ECFD62DF62351D33128C9FB5327D6DAF61717722C78B9FBBFEB8DF6254520E6BD64E7DBD5F0F3B1AABA9C3FB4DEE0D2CA6C634BDA0836DEC77A8E6EC732BAFF007BF9CF49219FD0DDEB6DC3638D5C0F4992F9AEBBEBF481FF0049EB7A3FF1AAC9E83821AEF45F6D0F71613656F874B19F67FCE0EFE728F659FE7FF38A4EE87D39CE2EDAE6B8B1F5B4B5C416B5E2A6FB3FA9F67A9F5FEE3D2D5670E4EBC36D7BECC2AB00667ECB6B9DBC56FA4369DCD25DE90F7EEF49CDDFB7FC2213F3FA397D98F4E254FCC66F67A3B2B2458D7329AD967EEFAAFB3731DFE8D8B42BE958ACC37E2173DECB6'
+ N'CF56C7B88DCE7970B5CFF00606B7DCF6FE6B535BD1F0EC25D2F6592F736C691B9AFB1ECBFD46EE0EF7B2CA99E9EE4A8A4C27D2B6FC5A5466747B2F662BB058CBDC2BDCD35B2039EEB2AB2BEFEEA1F47E914ECBFA3B1C5A70984B1CE658056CF6905CD6CFF00C6ECDEC565BD1B043A9792F7598EF6D8D7B9DA97B458D363E2373ADF5ECF554ACE918565D6DCEDDBEF7B6DB21C637319E83207E6FE8D2A2AE19D6D1BF2E8D0C5C9C1C8388074DA9E32CBA2CAFD1731BB7DCEDFB5CEFCCFDC49F9DD158DC93F616B9D8A2E2F60A99B8FA2E6D4D0C6FE77DA37FE815FC5E9B5D2DC73658FB6CC5DDE9B9CEECF1B36E81BBBD8876F42E9B73CBEC6B897C8B06E20381B3ED5B1FF00C9F552A28E09D6957E21157D5FA763E57D928C73597594B01635AD6BBD769B1AFF006FE6D71FA45AC7859D5F41E9D5D94D8DDFBA8DA2BF798019B7D36FF2B6ED5A2488485AF8090BE2AF0A7FFFD9FFDB0043000202020202020202020203020202030403020203040504040404040506050505050505060607070807070609090A0A09090C0C0C0C0C0C0C0C0C0C0C0C0C0C0CFFDB004301030303050405090606090D0A090A0D0F0E0E0E0E0F0F0C0C0C0C0C0F0F0C0C0C0C0C0C0F0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0CFFC20011080080008003011100021101031101FFC4001E00000104020301000000000000000000000900060708020501030A04FFC4002E1000000700010303040202020300000000010203040506070011120810313220133437331614094021221517FFC4001C0100010403010000000000000000000000000304050602070801FFC40043110002020102040402050905070500000001020304051106002112073122131451324191231508102061B1427294B47671D2331636526273D32435179243342656FFC4003B120001020403030A050206030100000000011102002131034151127122041020F0618191A1B13213C142526272D123E1F182A21405409243B2FFDA000C030100021003100000003EE198204081020408107008390408300D6789C088C38AE8DD73521BD6303CF87C4D1EF2120D72CC45F9A7A176DD64C44339DC6DDC56412C2AC3D344E698D278CEB9A10610E275094B7DB228433A656146B3CFA3EABF63253CD7D292EEDF99079B5AF4D2C574083A4CBD5B58345B9B28F8451890170DA56D038B1C208C44149C2202BDC8BD6337EE7B0030DA97968E2BA04083A4CBD5B58345B83D8EAAADEBC1322B50C7A9B1E0F17A6CBD706F381BB7818F7AD919F92E8102041C1EFABAB068AD87B1D5F118608311A80A5486C71F8CA915C918129921D01A3E44DE03AF7E4A4436E5D02041D80592435116591AC667B01A30E08A1F4D16391D9954DBD6E6157658A263D0B2DEB8C490F39460C6E999582AF0E1076016390D47465972FF00A339DDE7B4F54AD084084F89D47DA66431EEF0140C7A1199838412D55112FDC870A18FB2A718B3AA16690D494759F2EC4A9467A88B0F43ECBD520146181DC46A0212F7778A663D0CCBC1C204082D26B06768F5835DFF00576BAA3ECF97629C22FAF1F7D44587A1F7D92D551B57A154A7C3B476FD6C62B7C5E6481020D9B6F0EE727D184C75EF1E44F845A0EBC7DF51161E8772E4BD5E6F5E05715D270DA52055A428C38595C18F8394190166D0F5CFA695AD05F75772E2041D78FBEA22C3D0FB6F54AA8DEBA09E2BA73518A977DDD6643CDA0AD8FBC6405B758D52C9F3361597BA39C9A8EE169235A85E577728F53606AA5F6BBCB27505250FE76A1BA36374DE20E03B7DF0B76BAA94BDA4ABC27BA43988965CED3B4F1613CC358900797798339720CFEE6F7CDDD426B5A0FD1DD0F54DBCEF01881678BD7CECE5382A1FD01A3A0ABFEBEB20B58347E24C7C19CD2ACAB0B067E9127F7AEEFD56BFA10D1826D5CF9CC35716D2628F1BF8C3721B6F546FE28FD9EE7B4F73D0E297706FB25EC4AF3780661A3F11D262934316EE3C9669E0DFEEF72D97AA7CC79879E7D7EE599EE9BC47ACF2675657FFDA0008010200010500FF00803EC91CC6172B98E7FF00BE7FDF3A8F3A8F3A8F2D36A695D672FB34FBC35748E5A459943187B879DC3C4D7326203D40E1D4A88808A9F3EE32C814E254FD2C966635F6B60927BA249D431B6102A18C261FA13F89BD922880A9F366A761D63009FD35774E652C997E6E7AC01CE271FA53F89BD9B880F14F9FA87BCD4B3B927498A829FD4987429C7A023D7A9CC2075C3A1DC94A553DF85215B16F906D6B965AA5E63ACE4FA48995B94A3D40DD3A226288ABF2746394CF0A5050A52B629CE271D0E824B4A0845CCD2A5EABA245588FEA9A6540AB282A0A7F13F5E890F531C440FF0070E3C290AD8A7389C7D2E758FEC31F37569AADAF57981988AE2699502A8A19431BD93F89FE288080A81D4E5295B14E7138FD1B0A0BA95EC56C6E0B2A9A6540AAA8650DC37B27F130F40443A184A56FC39C4E3D3AFD2F536EA37CEDC3783B428A19437A1BD93F89BD900E82B184C7E332978A10486F5DE65576EDF20A7AEFA43D4DEC9FC4C1D41300EF57E6428984CA09547A4037AE9D7E3D5D223C90B1BA888D4D9B04D02AA993A08B84D348CE1B810A9FC47D93389855F9FA8075E5875985805D6904B43B340D522E08ADC7A2070EE44BEF203D0CB9BB5B27F1E14C4219CA4243FADADC388D868CC8AC0F7947A032AC13B838574252A6E053E02A0022ECC23FF92C628740E2C41370AA08818C52889C9D082511FBA060EF2704400A27270A62088890385314DC1390380A14'
+ N'07D7B4386200F0520E026003F64BC144BC04C0005201E7DA2F052288953E9C144A3C044A1E9FFFDA0008010300010500FF0080B0881225E2EE4D669F70E5D7738E01DC183FC9579FE4ABCFF255E55A01F4FB98DCE625A12169C93890418A0827F613E7D84F8FE1DB3D4D54C533BD201D088548A9A4CC257A4707988545D2CC637D2B757773CBC04135A846B25DC4C19AB54DB27EA5F77FF90B8754E2DBA899E57F2E9525FE33BB0384D475E99BB0499C1F69E6946AD536C9FD05F77FF90E3F8E08E989A57F2C04407D1A3733855AB623541B364DBA7F4F500E3D3019774602A31027055F2C645F4BA209BAB4B66ADDE2081D73D0E86202AB72B73A0E48B7D22200177BBA4824D55159173DBF6A2576EA9E48A2679615DDA2E6DEC5133DA150C405A344DAA4E9B7DE0029D13A2E88A8FA7B72F17745B23272CB48AF1DF8AEC0C28C5A9DEBC82A649EA0FDFAE7A2514C2668D126A97A3847EE91448E9F1053EE27CBBDDD26E8CB4B2D24B17DE3BF15E777D98840E89DF2075DFD0A8660334689354BE87E022932504AA5DEEE8B7465A596925B85F78EFC570602A5109824B542922AB968D126A918E52FD2600109F3024C25A5D6925BD0BEF1DF8AB0754E11314D4894088B4E5A97584593A2BA47D46548F474AB4A31CC3D4BEF1DF8AE92FBA946913FF2987E33A7256C9231E470CAA2ECE907A397A124659541820A1594E118C23495631E291579C631B14E27AB4566946FE2A86129639E28B9A3FF001BD3AF047A725965E401FAE35D889DB7C94D72AE602C2BD4BEF43B5FE6BF808BB95501AC1477E2983B810599B45EBF229BC6BD39D39D392F664177323A34332E5D2F6E6C1CEC371AD89E364584EBD63C4DEA845D4B3BE54C995ECC394120493E4B355150652AE0A573607081D5B4AA54D09C3B83AB208BA4C1D31E2A645340CE9988A2BB339CEAB328B7590578674C800924DD153D45020996649AA63C4A43C498249A830E8801A25B88923932A2A46247E162D1289E351399BB104C14876EA1890EDC86E7FFDA00080101000105002109D9D84E76139D84E76139D84E76139D84E76139D84E76261CEC273B09CEC273B09C3909D934BBD6D03876837CBE4BF96FE496837FD441C5DC788BDBA384BFB15879FD8EC3CFEC962E66958D1F54B3553C67A1C121619BCCBC24AD5B751D0AF165FECF66E7F67B37323F24356C5ACD0B30CEC5017E8E692F41C32621EC32BABB85DAEBECAD763F27BC20AF5CAD799F8E1E99E6716BD3E7F28A130F1F2BFB8EA759F1CAA17ABD5AB4AB5FAB9FC7C9FF00535890FF002AB58C5627E166F5EFDB1E02EAE142D8FC8CB340CF6A3E9E29C144D6F18D7F65AE78CB5CBDDEED9A65AFE873F8F93FEA6B5008D4FC687F5F7325AF7ED829CC4308888F21A1262C92B50A957E9D097EBBD8B44B7FD264177419A32791B995CDDB5614BC2559C6D69D0A69FD7B6DDC609B40E9FE5FD532EA66D50B0B2F63978988CF3FD7FE778F69B68DF719D670BBD62AF3E84925575719C5A91E2367F509C5ECD4CB57FEB3FA9E1562CE27A635741773AEF95361D66ADA079CB45A30EE50D0D9E78019D5FEFF006DD42DBE3B6FEEB10987B66CABC90CC756F1D349C81A7A2492ABAB89E2548F12E91B3EC572DBADB977EACBA12514A562B28796BDE91292707B2C468DB7D8E52262683E0367FA06816DD46DBE98AEA2392DDA91A9E53B144EA54D2679A494A750F886254AF12E8FB1EC772DBEE4AFF1E5DFAB2F02F828D82D7A6AB32FA2434B5876D8284CF7C02CEF42D0ADDA9DBFE8F0DE421D9EE7E6A66F14F339C4311A4F89B48D9364B96DF72E2BFC7977EACB4BC671F52C158210779B223987842DB42D0ADDA9DB99C6C8C897E8857336CE6D2BF8C2D7766D96E3B8DCFD15FE3CBBF564F11352B9E3CC5AD0D60F212CF60B76DBCF006A79FB66DA5D065B2DD03D0A53A87F1ABC5C94C915F2C35988AE533D55FE3CBBF565C6114B154F23675B2EADB17EDDA15265745B6D8F5EB067FE41FF00B14A1576D2FF008421D43F8CDE308E5095B25DB579A6619962DAEE199D78F796790B86678BD70970F24B3BF1CFC79BEF927E2AC566351C9BBBFF0092CA3A5994364D7BB35BA5762FDBA20060ED2F402100534D4554F12BC7387CC475DD02F14BA5DE751BEE8EE3C4D74D997853A043A768F0E2AA21FD9BFD989C84D5B73976B9F780796008654BA00E9956E7F14CE342F30F17B4E4BB3750E098A50FB89FDDC0BC71ACE0356B379778A411B68F202776275F7D1E547CB0D468B4FCD7C93D432156334779157697F3235D9F946286C1E4F6895E8546B75A27C374A5D92CAD2B9A659E6EB16C7B9953E6E6AEF8CB6AFD06432299B2CA6B551D02B64BE620B8CD485361E88FAF78EA8EE12DD9049CEBF9DC85839AAD9A8F643BCBC62CD1B476BB9E576CE7F810E4ECEF2717AF40387B3D41AC595FBDC4296E021734A940D8D5C1A909A2E70FCEDC388DCAAAF1F4F94C7A9D2676F8D52117321925264A5EB19BC7C2212984E6F32F63F07CEE3244E72767FFDA0008010202063F00FF0082569D3AE080642318C796B078AE2493346B47AAE3BE96F9B9C64D1D809FF1DCDE1D980600E776BDCA49D806C8E1CF1AE2FE24DB6EB2EAEB215C0EC250E690A4C562B0A0C2C1100A1A74A41DA60A7A87C3F51123EA3E02BE32E5FF00278C720251AD137BDDF4B4799A0C4C70D6B84B6DB7BAF0C639E3024B9EE957480A828022C338BE39FF00E4F10D9B5A89698E18A55E41A1720FB142C29AF346CE4995ED83B4C260E8294121D36F2BF85602EF6832DB1BD6E01C4E43539D53D50'
+ N'EE3F8E43C55C6E96B04C5A69995342F3204890011A4A98535E70D906142CFA87C20ED3CCA2F567171F74ABDEF2B993AB7573D326B72002433DC2AFD2DD5F9690BE3CF1B20C4CCFB7C8C123330C0D02759410DA7C61056353A6F3D3F9E70DE3505CB2E2388369749D5AA6C07E973E608050488873B86716DD6CDF6DE81EDFB8249CD5F99BDA073BDC7D70100C158DD1DAB076C334AC00DA9CB38D4E9B8C29AC31D6DE2DF13657438FA5CD332C7A4C059B5C3D270431C3F11C459730B5E1089DBB8D327B43C6E90E0686624520D9B24DABE17F69E9A8A574386EBD3AB7BAB99ADF5C0412606C82912FD6091998453DF1A9D371E9FCCC29AF2FF008EDB9EDDD63B5DB7E0D78923862D7091CA47080FE22D3ADB9A55975BBCCD4261CDB8D92ED43D51C371A5355D602E4C1E24ED930A9D7C9ADF5C074C7CA15DC8364148A4BBA081558D4E9B8C29AF35CFB248F6AEB1EE4FA66D24F502E062E700F2B6EFB0B80A06DCB6245A0486A6A872564635BEB80853CA364189D48E98982F3371A74F38535E75C67129EC96385CD526E823794990098C59D2E17ADDC7BAD5B70C6DBD436EF513BA10CD354A90AEFE5CC1B393A7E90573E443570F082D3873385E01AED2CBDA9F707D418818D3D4A4B933032867FB2B8D2DE1EC15693FFA5C1E90DCC34EF38D24054F346C848A107F9741076C20800344A9B203C76F2B387E1403C5DE05C090A2D329AD28E738A868320849183996AE97F15C55C7233512E03555460C6FA902304D6516ECD7DA606AD356915EA53394170911D7280A14401A546D31A9A55A606CE4991D9D160EDE6BAC6F7117DA482DB69A58E1839EE206ACF4EA4982862C1BAC7DB6BC69735A41D16ED87389D698D7D210982DE0ACB584842EF53DDB5E54F6041D50EEDF285B52188F3810364069A9103672201DB0723CCE2388E1DAE75FD045B0D1A9DADFBAD20625A4EAD8203AEB19641A9B8E1AB6E96EA777C39ED7FBBC43C23AE10882BA58DF95AB59ABB1CA2B1A410907490161650A485D823326139247A7C7645166898F6F5C11A69B2153CA1009880409C1952174F9420017B211271481BB5D907768B865FAC201978F3671942E31251D123A74C2123A6D8072E9F18273FE503AA3A6D81D5C9FFFDA0008010302063F00FF00804B6A9D311E621C5E4691820507ACB5CE1D89DB0EB36DC5AC69D2824A448939CE915778C282E4ED8F51EF31EA3DE63D47BCC7B369E5AD6CDCE2A8D1F171C07900481EE30DE766F713FDA0868EE3B62EDCB034DBD446A146B7E960CDC8BFC205B630003A87458F48EE11E91DC20B1ED00E0E01083D3082C35048EE87B4D0B4E298670DB9A6E6AD28A5A00471CDA0028825F28DA62E905087BBFF00A30E2D71F758269525BE7A9BDEE872388379C804E4D6FA88C95C40EC3CBED70E100F538FA5A0E799380133B018792E73C921CE29BCE251AD6B5A155164A7E63302345A06DDBF99C6A3A861ABB4E6B02DDB08D1D176F36E7E4EF330E12A1AD3B6097DC0F2460E71036349206D993177F377998F65C776E4BFABE5FD3B60B2D04B76F75A3A819F7953DBCB65CD13B80BDC7AC923C1AD03BF38F66D4AD03BEFCF2033CFC7098B7682347453D7CEB9F93BCCC3A943544F151DF0E731AF1AA7363409E4E68030F4F6C5DFCDDE661472B6DB4125C409432CDB00358035A064243A6702DDB6E968C36D79CA6822E1054171F3871240001AA81E13EE9C1175CE2E2D500EB0117E56BF6852BAB3458B8F6A287BAA01C4E0651C2DBB36585B70EF8D0082142ACB0049F182DE1911028054076207495205BB615C68214FABE77FD3F6B7AFA52AD026D0014359745589545463CD530E631C8CA4AB70E43EDF3A9865C210B9A0F785876B9B50ACD25B7083ECB1101DED41CAA54A4CBAB53439D22E801497BBCCC70BEC6A52A080A86923FC6196F8668F71C379ADCD652C09C7B0E31A9DEAF9DF8307D2DCCF4A40B568234745399300828E1D120170423B8F52C2091C8FC33E6398C77EDD255B87E91F6E79D691EE5D3B060D1908B5F837C843F414721428A9D98C10C3A506F024BC92B552069D98FD2122EB984821EE42246A6031971E5C6406A3FAC6A74DFF003BEBA7ED6E6E3D25516AD0468E8A7ACF2A0287031BC10E070EF86BB31C8E631DFB62448ADC390FB7CEB48372E1D83068C8408B5F837C843F48D450CA6165D53EE840D3A0B4D41621051349739754CAFA80A99C5C6302B8BDC83B4C6A77AFE6760C1F4B7371E92A8B5682347453993CD5181060B49910BB13F84398C77EDD255B8721F6F9EC8372E1D83068C87208B5F837C8439CEA00571C321331BDEA734A648085401CE0D450A0CFAE1F77E6738973B063493BADCDC7A49605AB411A3A29CC980A405283ACE5B79A5698C5E782486B0BA58B5A357880906E5C3B0600757288B5F837C843B61873661BF2892012C98D9AA9AA45B6B020D20F69133B7903ED7A6C3829FBDD31FF00509DAE865E6D1C17F51D865CC732D4D8D28BF57F0F387F0AD7037AF37481F4B4FA9C7208A0664A8A1E608B5F837C8439A090A0D0A1A4690C7B1C3528D40A269134A028ADFA95D16FF06F90875C7290301324E006D873EE5E78D6AE78F943CCFD3A5774A0EC943B86BAD709AB49053EE1F11DBCAEB76CFECB4A123FF4397E03FBB6439EF218C60249A000549E9332A98E2BFDA5DB4756A794D46600D43194A52943EF5A69B4F62E25CD281719F55650D1759ADA4A10A9E30CB46C9735C149D4544D2429D73F086F13C338BED392B50B43B0F6245AFC1BE4209180E94857BD8427C8416D7AF7FC122D7E0DF21C'
+ N'C5306D593A2D62EC5FB060DDA77B645EBC5DADCC05C14690AE28D080A9A815539C271370E8FA1BBACEE15DAE53D717C90A06B519EE8946AFF5DB8C997B454FD409AA8C736F5202CFC879C5A03E8F898B766EC9EE0D0063220F80F1945AFC1BE4208CE05BB56D0D0B8C8A1266356F39BA86F1120670C00EFB000E18CA4BB0F2AC37856DC6B6D12753890D0FD20B8804A6ECA79ECA96FBA6E11831A5DE326FF740B2C61B761A574D4B8E05C7AB068A7599C50C7B36C343311A44F3594D71877B27487152125D80D20DF0C6EAAA691A41EA6D203DE1A5CDA12C692364A03492F79CE807C043583E5007704E40E6B903674501265C5B57E4D65167927B8C6928E0C68274DCA6F6AC350C404002A130F0ED68D682BAC2152831513C4C822AC0735EF04903D5204EAC75692374A15432AC3AD17BC90A0A938275D1CBE70D736D6B7144040512D4E13A201DE90E4B40A7DA27BA1C136EAD3B63DDFF001C12A85A8C5134AAE9AA63F182C65A69B8146946C9C086807F22541C81302D9B403904B4B6A49047F496CFC208F644890775B24240EF45116D2C34EB598D0404CE670C961FFB2159A94690BBA4097E4BBB1ED3188A5824000758241ECC7981C930491B48427C60B9D8800EC69D43C6374B9A4150869EAA4884DE328F742EA42173D4751F1A41D25CD250A833937475D5B23DF59C2A10508AD01004B669046460DA52413A892664AEA59263908C41DE208A82E70728D844B0C290D74C96905566A16676EA2B0E71557104CF10348F0866A717162A13D72C86105CE06759D46AD687FABF486B82AB5127F4D3CB93FFDA0008010101063F004F22FCA3E81F0E3E45FA871F22FD438F917EA1C7C8BF50E3E45FA871F22FD438F917EA1C7C8BF50E3E45FA8700155D4F80D071F22FD438F917EA1C7C8BF50E3E45FA870FE45F94FD03E1C64EC635259721052964A51C1124F2B4AA84A048A49614724F8069141FA5878F19E9771DDA8F86A10FA55B1B051A8B62BD81228FFAAB94F2B7A22C543111A47A69CFD42469C6E8D91B73706470BB1B6A64A5C2E37098B965AED7EC556F467B164C2434C5E60C2353E50BD3A2F516264026CF1F4959E5F35AF2AA7CC5BE007D3F0E239EB5BCE4F04C59619E292D3A3941AB05652412A3C74F0E3FEFD91FE2E6FEFF001FF7FC97F1737F7F8FF5064FF8B9BFBFC45B6B6DE6EFA32C7EE32994B16EC0AD4AB0215A694AB6A799D1547363C87D2443F7FE5F3FBCF208BAD9BB7F2566BD7E5CD8A56AF2A80A3FDE663A7D3C6677DCEADB97BCBDC71624D85B32C5895D3178E9187A5104777686BC7A2B4F26BD72C9F6687403A72FBB772EEFCA5DCE66E733DD9D2D4D14635E4B1C5123858E341E54451A28E5C7FA972DFC758FF99C7FA972DFC758FF0099C50DC7B7F74E4B258CAF323E6F695FB934F4323581FB485E394B8462BAF44883A91B9F31AA9C4EE0C792D43398F83214598684C36A25963247C7A5871BB7177E48A2A590C2DB82D493D8152258DE16562F3B0658D403CD981007882397180DD4DB7B7C2E6A1DBBF76D5CAE4B114A9D0F6991B8B318A49B1705781DE0F46354461A4318D546B2393DC9B556792AD9ADBBF2F2D6B30B98E48DD2FCA55D1D482A411A820EA38CF3E2F35793B9DB22030EE0F692BC73E42C61C0999241195328BD49B5D3E532B69CFA78CAAE3F71E4B1B63BD59CFBBF054239E458E1C1E0F57C8DBADD2DA46D66DCE95D8800B2A38E7F9576F6D3A02CD84413642F4CDE9D5A7013A7AB625D0F482790001663C941E7C67A8CC327BDB7067B278E89FEE5C6BB4B6669D162AB52B216D3A048EC7A9DC01E666E95074A798DE295B2FDC4CF57326D3ED8C32FAA8D280359B233AE85AB42FF003F4055908F4C75824AE5F7B6F5CBCB9BDC79B97D4BB765E415579470C483CB1C51AF95117901F9963FE1BFEA3C76D7FA470DFC843C666B6B5419F1F322B5E96486B02D1900CD24455D501F98A9040F03C652F6E2DF18ADDB76EE3D5560C4E6B256AAD741202A2BE3AE58B091269C8C8599D8FED01CB8EE7FF56667F9E9B81B3F2563D2DBBDD18171932B90238F275C3C94643A9D3CFABC3FA4C8BF0E33787D994E3C66C0D82F26D9D918C809F492AD39E569E653A9EAF716A49A50DE255975F0FCB89CD754505BDDB76D5DC9DC6F9A431CCD5E08811A93D091F251F4927887736E3863CA770B275E61DBCEDF975F59A570627BF74A13E8C28095E47520B28D5CE91E5F7AEF6CC4B9BDC79A97D4B76E4E488A39470C318E51C518F2A22F203F4EA7F36C7FC37FD478EDAFF4961BF90878CF012D6809C6CFF6D75A14AEBF6679CAD61258828FA4BA32FC411C6672D80C26ECAA77440994BC72BB7B174E9C72DA78D1922C96360850F4088130757979B74EBC773F5FFF005999FE766E15D1CA3A10C8EA74208E60823C08E09275279927F250C0EDEC759CBE6F2D28AF8CC6D389A69E59581D0246A093A0049F80049E438C4E3685758B1F8AC7C68BEA375245108419CC60F24121EA77239924EA78CEEEDDD19BB1B832791B5224591B2416F690318EAC68AA02AAA44AA005007D3E249FCE356AC3259B56BEC6B56894BC924927951111412CCCC40000D49E3B7F8EC8D5968E4286D8C557BD4A75E89619A2A512491BA9F06560411F1E3725DBB66AD3AB57156659ED5D9A5AF5E30B113D52CB5C195547D2631D5FECF3E2FD6DE39ECBD8DC195C37BAC762327633B56BFB48A68C3BD0C5E5432F4C66445965691A605943AA06D38EE066F18D02DFC7EF0CCCB58D9AF0DA87ABDE4C087827478DC1074D1948E3F0D3B7B62F69F6ADBC26F'
+ N'EB4C9BF70916D9A1356B555DEAA4A677F6E4C6B0C52C92290C342353A81A7194C3769A5AA30428579B338DA337AF568E51DE5162BC2C0B7480823629A9E86665E43CAB8DC06031B63319BCC584AB8BC5D54324D3CD21D151147D649E40733A01C47BCF79C54B797E23378D378F01818DC3252561A3C513E9D51D78C91EBCE00695BC89CBC373626D08F6CEEFB32DCDAEFB9E956EAA289621F57DC4702B031986093A3A0B73E4549E2A57DCD561BB81BEE61C1EEAC7167A561946BE937500D0CBD235E87F11F293F9B0D7AF0C962C589162AF5E252F2492390A8888A09666240000D49E23FC43FE212A4577B8F623F57B75DBC90AB4D52C14F521411B023DE1E45DC82B5D7FDFE36BEE4B559295ADC185A592B14E273224525BAE93346AE42960A5F40481AF19FFBEA08ACE1CE32C7DEB04F612A44F5FD26F543D891E358815D7572CA17C751E3C5A83626CD6A15F118D99296EE9733532E2CC53DBF5AC475DFDF5AB6C1E762F24AC811D82EAEC42F1DCAAF5A192CD89B76E6121AF121777637A50155541249F801C7E17D3B6B3E72BDFB8EF53218AA0B33D7B644B443C16E1506375E82FD5D63CABAB72D35E36CEDAED1E16B4BBD371D411EE9DAD818D483929A6FFA626087CB1CF2A12CE001E50B237CC58A6F7DED1D3DE1F889DE14DE2DBBB76370C29AB0D1A289C6A63823247B89C0D5CFD9A72D35CBEF7DEF967CCEE2CD49D766CB79638A35E51C1047A911C518E4883C07C4924E52B6431D2E7364EE6689F3B8DAEC16D57B108E98EDD6EAD159829E974623A869A10471BC76EEDDDC757335B218E733D2914C590C75A8D4C95AC3D597A64568DD7504723CC03CF84CC66ABD7DC1B4DCA28DDF89EB6AF1B480748B50B8F52B9627405B5527F6BF2C504113CF3CCEB1C3046A5DDDD8E8AAAAA096249D001CCF15FF0010FF00888AC967B83613ABB75DB9708D629D923AA3D236241B6C34666234ACBAFF00EE785EDE1BC6E753F4BC585C3444FB4C755249582BA1FAD98F99CF36FD1DBAFE95C47F23171B963C2E41715977C55918DC93D46BCB04DE91E890D64491A5E93CFA4236BFECB781BF4B6EDB936EC588C6C6DBBF6F6466B99AB16E66993A6616AC56AC2A1E9EA053AFA9F50C604F4FCDDC3CA61B23671593A3BB732F4F23525786789BDE4C3AA39108653A1F1078C7E0307BFF007AE6333989D6A63319572F7E49E79A53D2123026D756D799F86BAF2D783BFF007F1ADBD7F11DBE6B4AB83C1893D4358C8359234939B2C28C47B9B1E321F227D1AE5F7BEF7CB3E6370E664EAB161BCB1C51AFF870411F84714639228F01F13A93F913705AC4FDFF00B7B2755B1BBAB080AAC93D3760EAF0BB6816585D43AF31AF353E3C59ABB773B473F56FD76AD9ADA57D44177D0957A648A7A5368C41074D57A97E078DEDB221F54D3DBD959A0C53CC0867A6E7D4AEDA9F980460BAFD241E1238D1A5965658E28A352EEEEE42AAAAA8258B1200006A4F10FE227F1175A39F7F598C7FE3CEDCBAC7258A561875C40236A0DD600166F96B2EBCFAB522DEF0DE16F563D50E17090B1F698DA9D5AAD7814FD6EE7CCEDCCFD0049FBA7F571DBAFE95C47F23171BA4E3316D9AC87DCF6BD9E21259E06B327A2DD312C9588994B780F4C86F81079F1156AB81BD5F696470D66736EEE36F601A8DB86E2451C1F77DBBD6C4BEE943CCAECA258D547A8C4C800DF981C0E3AC65F3597DE796AB8BC5D54324D3CD25E942A228F127EA0399E5C45BF37D455777FE21778D368F6EED98E45229065F3C3138EA290C648162C01AB9FB38F978E5F7CEF8CB4998DC39993AA799BCB14312EBE956AF1EA4450C40E88A3FB4EAC493F9942965E1825FF31E13238BC63CEA0E969824E88A4F30CEB1301A73E30BBDF1D51CEE1DA994AF8DF58754D62DD3CACC53D073CDE4293B298C733CCA8F1E2B7E233F1115C4BBFE55EAEDCF6EDBA5AC53B12464C63D26E46E329D589D56BAF33E7F96DEF0DE16F56F34384C2C2C7DAE36A756AB040A7EB673E673CCFD007127EE9FD5C76EBFA5711FC8C5C6E2BF909E3AD429622DCF76C4B0CB6238E28E076767861649240141255183378290788C6763106E6DD186B93622444922AF2D6A52D6F710C14E0CADFAD49A0F5E1F52375F50F50FB43A30E3797777760ABBCFBEDDCBCA65ECEC8DBF1B7F81059B0F208E12C3AA2851645366C74EA7511A03A80D97DF1BE32CF98DC39993AA798EAB14312EBE9D7AF1EA4470C40E88A3FB4EAC49375B1F8FB37D71955EF649AB44F28AF5632AAF3CC501E88D4BA82CDA0048E7CFF37096F6C8B077355BF5E6DB895559E76BC9206816345D5989600683C46BC52EE36EADA2E5B63521B8B75ED8952392487275A060F5602E7D33257776915C1D15826875F0B7BC3775AD079A1C160A1626A636A756AB040A7C4F817723576E6796807E493F74FEAE3B75FD2B88FE462E3331CC8D2432636C2CB1A69D4CA616042EA920D48F0D55BFB0F87199C7AC7355C3C504ADB631E82A886B5190C0EF1CBE96228B195ACFAAE4890A90479491A8EE765B7265A7CC5F83716471D5679DB510D3A56A486BD7897C1238D1740AA34D753F3124F19BDBBBF6159F73FE2276DE62BEDBC7485346DB38A9454BA87521D5EE4D24ACA002192B17D474F1BC3B759C25F25B43252D092C15E9F5E100495AC05D4E82781D2503E0DF95238D1A59656548A2452CEEEE42AAAAA82589274000D49E2AEFEEE2D64837E65F1114F85DBB247AC983AF71E552D2B9D47BA912221C2FF860F4EA493C64BB738FBB1DADDBBAA14AF91A71B751A18E621A469F4F95A603A1'
+ N'14F3D096F01F9927EE9FD5C76EBFA5711FC8C5C66F0D0DABD526BF8EB30432E3A65AF63AE581E350923F941D5B51D5CB5D09E24C652DBBB97696E0C74194398C63E520B70402A362E054B91C04B470CE61F56A92749BAEC1E3BA7E03FF00B7E6F90F0FFE74DC6136761ECD4A36B33616297277A78ABD5A75C1D67B534933A284863D5C807A8E9A282C40E36DEDCD99D9ED8D9193B793E3F03DB3CCCEA27CBD9C06351AB45247948B22B590DB87D670CCBE5F54F5A93AEBB7BBD7B133B84CC58868AE27B898BA593A735B8A38DBAA8DCF463958B852EF04A57523ECB4054311C2471A34B2CAC1228901666663A2AAA8D49249D001E3C62FB87DC3A2BFF93EE568EF6DFC1588D5C6DCAF3EA2295D5C32FBF90293CC1F6EBCB4F51BA972DBC7399A18AC0D1A665DC37E5777B521858BC6B14921626493A8A73D5B980BFA3BABDFADCDDB79D773ED49B3B72C6361CE64120C83D2A8B7E3695BAD9A22FEA08D8212005D57C741BC77FECFC2DCED76F8D8F35DAEF4D72536471379EAD44B686417034B10915FA0957F211D67A81E9E3009BB36DFF9AF017ADC35721865B9350919277542F1CF010CAEA0EABAF227C78DA5B52CF6A329B9B079DC58C965F2D1EE2B905F811EC495C8AD169E9334623EB01CE8E4F492BE3C6DFEEE76C771D8DDFDA8DCC959C58BA10DAA6B7D3D4AB2B3C691AC90CA08504A2B2B10ADAF572EDA75EBD7FE52C3756BE3AFB087C78C85DAFE98B15294B34066591E3EB8E32CBD6B0AB485751CC202DA780D78173706EADB39AAB631E65806CFB74ECE249699638C979E4FBC4BEBAAAEB184D7A81E7D3C7753FABF37FCF4DC684023E078E9E91A1F11A72E3508011E040E238618DE69A6758E186352CEEEE42AAAA8D492C48000E64F18DEE977230833BDC1204FB636C59658EA613C7A6C4CDD32196D9FD9D13A62FA09939A77037FE5F2786C8E5D6C2CD822D5E44814D968ABD5A495FAC1222507566725CEADA28E4127DE3B92CE56185FAEA634690D384F86B1D6882A02078120B7E9E3BF572DE3A1CC54A6DB926B789B12491456634C340CF0C9242CAE8AEA3A495208075078AB9CFC278FF27ECB759EEF72F6063C7AD90B71B224791827B9217B0D2D6E8FB41D5F6D08E44A74236DB3A8E9FBD29F3FA34F5D38D88598285DA40B6BF0F7D6B8D8DB1777C1EC377EE8C2E1A8E2F6DDA052D2C90CB15D99A489BCE9E8449A3EA3CAE550E8C471DB90CDD6C36A62356F0D4FB18B9F135532CB00B30B446781CC72A75AF4F5238E6AC35D41FA0F189DB5B47B79257CB4CCB8EC9EF9CBABC17FDA5B9667F5E9CB93D6DDCADEEE13EE1E22228D995C96D471BCAF6471D61B68EF4CBDACD6D4DC7D0CD5A64BD235892B34BA7489A07665642752A03F8371E3C16660147892790E23AE1C1B12B048A007577663A05551CC92790038C7779FBF596C5ED9EE06E064AFDBBDB39A9E2806216600CD6E4594806EA572EEA3988069AFDA1D1668E8E66DEEBB3183E9C588AFF62C4720A2C4E624D3F48D78AB5678A1C0ED5C5CAD36336F4337A85A520AFB8B3290BEA49D2480000AA09D35F1E3FC64FF00D4381B0B6C4BB6B1FB464864872185386A52C5784E9E9CED70C8ACD334C9E572C798E5E1C6E63DBDCF52DBF4B75D85B792C32D4826A31CC9A857AD5E60EB11E93D27A7C54286D7A5749B7ED6C46DB932D2D892DC78D9F1B5E5C6433B90DEA434587A48518752803453E038C767F36DB472FB8F0A85305B8EEEDDC658BB4416EAD6BC9244DD0430EA1CB91E2863DF2191DF9BBF3052BFBFB6ECF5A8D5D747964E81E9D6AF1EA59BA54027C017201C1EDDAF2B4D5F018CAD8E8267F99D2AC2B0AB37E921753C27EE8FD5C637278ACECB46A606686EAC46B8B3531ED519ECCF92B14A3479B28EA91AC70531A2190891B565464C8DAB7B21B7EE15772D3D9DB7F6367EC255CECB29AEB3DE932BEFA21565B9138EB7895638A24122A48E579EEBC6647B01866C76D6C353CCBEE1F67828EACE9909BDAD78C09195E3D6759119E45544085D98295271D92C6F6876ED4C9E56CD2A5524B54307156825B8F7E232FBC91D6BBC63EEF93A24590A49D4857A813C5DDB384EDD63317B8369DCBF5EF596C3D0ACD14F897AA82C46D0A0F2CDEE0344E9CBCADE047181CD61BB6ADBCF70DE8EAC98EDB994A14E4B95525864B9908645B5222C2F0C35994EAE0348D105EAEA1C6E1153B6D42D49816021AEB87A4B2DF5971B532158D447552E6636D6050742250C1B41A130EF43D86A96EC8C9C58AC96D88ABE0BDCD69A6B429A319FD6359D0CCC835494E81B560BD2C172FB6705DBBC16437DD0F7F4976DAD0C6BCB0E4E0B75E8D5AF65755F4D6CCB63AE376214C68EDD434E31DB52DF6B31D8FDC96AB631AE5397138FE88AD5DB3729D9ADD6BD619E9CB49BD50A48E964652C09D2C556ED7E3279E85DB5472A9162F1C4D59209E6861320201D2C081A48F4FD9E6DD3C76F638BB2983BF16FF7B6B166F1AB81B54AA7B35F564F59A29E425962D0954EA3AEA38DE92FFE29AB66D6C7833B364F1716168B5998E16DC35234AF1E9E7379A7535CF83684374B0D386D9B80D99362E7BB96DBF8E8A6C753AB5EBD96DC155ED45607A4CA4A5754D25246A0B0E9D75E1FF74FEAE13CEBF28FA47C38F9D7EB1C62B21252856CE16E4F90C7B464C6AB6ACC52412CCC8842BBB24AE35604F327C78C86432C92CF2E528D1C6DD844BA44F5F1F75AFC4A633A83ACAC43EBF32F94F2E237A17F2D80B55AFA64285CC65C589EABA497A558A049239234887DE13284E8D0295034E91C2EEDA2B60E7FDA64A958B'
+ N'F2D86633C595BB1DF9FD54E4AC44D1EA874F282CA391E2D0C2E4B35B6721726A33CD9AC5DE096FD4A543EEB2C1A5495419EA7D94A7A79E81D7A6450E26B6B5AD54B4F46EE36B58AD6DE37AD5EF434A1658083E4311C7C2F13736470581F336B92D9925DC8646966B28D9BCD646CCE8B6ED5F92D25D967768638A35F5268C3154455F10001C4D656C5FC764DA6C95BA797A961058AB73277ABE45AD446447432433D543175AB2AAEA8559188E36C5F95EFDDCA6D4BF572B472B62D933CD7EBC57227B560A05577B02FCC661A0562410A3A574CFE6EC2DB5BFB9B294F2F9568EDBAA359A341B1B0944D4845F458EAA3916F31E7C6CF7C8E6B219DC9ECA6BA70F7ADD8562AB762F6ED1B08E3883AAC4005D46A0F3D4F162F6469DA9E5BE9345968C5C9152E43364FEF7F4670A47522D9E6A01042F935E9E5C6DECA555C82DADAC29A61BAAEBB2C71D0F4C568D97F6C46B1041D5A9E9D753CCF0FE75F94FD23E1C7FFD9, 1)')
INSERT dbo.Bank(ID, BankCode, BankName, BankNameRepresent, Address, Description, BankTransactionType, Icon, IsActive) VALUES ('0d80ab03-4915-40f8-b045-9ad38fff1611', N'Techcombank', N'Ngân hàng TMCP Kỹ thương Việt Nam', N'Vietnam Technological and Commercial Joint Stock Bank', NULL, N'Ngân hàng TMCP Kỹ thương Việt Nam', 0, NULL, 1)
INSERT dbo.Bank(ID, BankCode, BankName, BankNameRepresent, Address, Description, BankTransactionType, Icon, IsActive) VALUES ('11324817-f740-4f64-bce4-b3b124b0617d', N'VPBank', N'Ngân hàng Các doanh nghiệp ngoài quốc doanh Việt Nam', N'VietNam Commercial Joint Stock Bank for Private Enterprises', NULL, N'Ngân hàng Các doanh nghiệp ngoài quốc doanh Việt Nam', 0, NULL, 1)
EXEC(N'INSERT dbo.Bank(ID, BankCode, BankName, BankNameRepresent, Address, Description, BankTransactionType, Icon, IsActive) VALUES (''ecc73f97-157a-47c5-b915-b811fe0c5445'', N''Sacombank'', N''Ngân hàng TMCP Sài Gòn Thương tín'', N''Saigon Thuong Tin Commercial Joint Stock Bank '', N'''', N''Ngân hàng TMCP Sài Gòn Thương tín'', 1, 0xFFD8FFE000104A46494600010101012C012C0000FFE1123A4578696600004D4D002A00000008000C010000030000000100BE0000010100030000000100C2000001020003000000040000009E010600030000000100010000011200030000000100010000011500030000000100040000011A000500000001000000A6011B000500000001000000AE012800030000000100020000013100020000001E000000B60132000200000014000000D48769000400000001000000E8000001200008000800080008012C000000010000012C00000001000041646F62652050686F746F73686F7020435336202857696E646F77732900323031383A30363A32362030393A32343A3238000004900000070000000430323231A001000300000001FFFF0000A00200040000000100000080A003000400000001000000800000000000000006010300030000000100060000011A0005000000010000016E011B0005000000010000017601280003000000010002000002010004000000010000017E0202000400000001000010B40000000000000048000000010000004800000001FFD8FFED000C41646F62655F434D0003FFEE000E41646F626500648000000001FFDB0084000C08080809080C09090C110B0A0B11150F0C0C0F1518131315131318110C0C0C0C0C0C110C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C010D0B0B0D0E0D100E0E10140E0E0E14140E0E0E0E14110C0C0C0C0C11110C0C0C0C0C0C110C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0CFFC00011080080008003012200021101031101FFDD00040008FFC4013F0000010501010101010100000000000000030001020405060708090A0B0100010501010101010100000000000000010002030405060708090A0B1000010401030204020507060805030C33010002110304211231054151611322718132061491A1B14223241552C16233347282D14307259253F0E1F163733516A2B283264493546445C2A3743617D255E265F2B384C3D375E3F3462794A485B495C4D4E4F4A5B5C5D5E5F55666768696A6B6C6D6E6F637475767778797A7B7C7D7E7F711000202010204040304050607070605350100021103213112044151617122130532819114A1B14223C152D1F0332462E1728292435315637334F1250616A2B283072635C2D2449354A317644555367465E2F2B384C3D375E3F34694A485B495C4D4E4F4A5B5C5D5E5F55666768696A6B6C6D6E6F62737475767778797A7B7C7FFDA000C03010002110311003F00F53AEBAEAADB5D6D0CAD80358C6886868D1AD6B47D16B549249252924931494BA695CB7D63FF0018FF0056FA0EEA5D77DB731BA7D9B1C87107C2EB7F9AABFF003E7FC1AF2EFAC7FE333EB275BDD4D767ECEC3748F431C90E23FE1723F9C7FF0063D3AFF90929F56FAC5FE303EADFD5FDD5DF7FDA72C7FDA4C787BE7FE11D3E9D3FF5C7AF2EFAC7FE34BEB1758DF4E23BF6661BA47A7413EA91FF000993A3FF00EDAF4971A924A77FEAF7D78FAC5F579C1B85926CC6997625F2FA8CFD2DAD9DF57FD69EC5EA1F573FC6C740EA9B28EA33D2F2DDA7E90CD2E3FC8C8FCCFF00AF7A6BC412494FD4CCB18F635EC707B1C25AE699041EE08525F39740FAE3F583EAF3C7ECFC93E84CBB16DF7D27FEB67F9BFEB55E9AF4FF00ABBFE377A2751D947566FECCC93A7A84EEC771FF008DFA54FF00D77D9FF0A929EF9250AADAAEA9B6D2F6D95BC4B1EC21CD23F79AE6FB5CA69294924924A7FFD0F5554BAD5F6E3748CEC9A5DB2DA71AEB2B7730E6B1CF63A0FF00282175EEBFD3BA0610CFEA4E7331CBC552C6979DCE04B7DADFEA2E4FABFF008D2FAA197D2B37169BED36E463DB5560D2F0373D8E6375FEB39253E7C3FC677D7881FE529FFAD53FFA4955EA5F5F3EB6F53C738B99D46C343BE9B2B0DAB70FDD7BA86D6E7B7F90B0024929B58DD2FA9E555EB62E1DF7D525BEA5553DEDDC396EE635CDDCA193819D8640CCC6B718BBE8FACC7327E1EA06AEE7FC51DD9072FABE3B6F7555FD82C7B65C5AC6BF731BEBFF0025CCFF0048B530313A80FA89D77F69750AFEB2B0B07D9E9C6B4E53A8780EFD61F6DBB6CAB67F3DB3F72ADE929F2FA28BF22D6518F5BADBAC3B595B01739C7F75AD6FD2510C7176C00974C6D0359E36C2ECFF00C5C623B0ECCAFAD0EC4B739BD3B6538D8F4B4BDCFBAE3B2CB1A2B659FD1B14BEC72A7FE31BA11E8BF59EE750D2CC5CEFD6F1880446F3FA560FDDF4EEDDFF0041253CF6574EEA18440CDC5BB14BA437D6ADD5C91F4B6FA8D6A98E91D58E38C91859071CB7D4170A9FB367FA4F536ECD9FCB5E95D272BA5F59FA9DD1BA1FD64B5EFB3AB9CA1899F63B73AABE8B7D3A1BBDFEEF7B6DD8CF7FFC02C3EAF85D6BA17D48CBE939D6DACB28EACCA9A039DB1D49A1EF67A5AFF47B3E9ECFFBFA4A793B3A275AA98EB2DE9F955D6C01CF7BA9B03403C39CE2CF6FD251C8E93D5716A3765616451503B4D96D4F63438F0DDEF6B5BB97AB749E917FD62FA85D15B93937595526FBB2319961176536B75DE9E23'
+ N'6C7BDADFA5B3E9FD05E73F59BEB2759EB39963339CFA29A1DE9D5D3F73832915FB1B5B98FFA7733FC25B6FE952520E8DF59FAFF0042DC3A5E6D98EC77D2AB47564FEF7A3687D7BFF97B56AFFE39DF5E3FF2C7FF0002ABFF00492E592494FBDFF8B4EB5D4FADFD5D766F53BBED190326CAC3F6B5BED68ACB5BB6B6B1BF9CBAD5E49FE2EBEBDFD5DFABFF0057DD83D4ADB1990722CB40656E78DAE1586FB9BFD45DEFD5FF00AEDD07EB164DB8BD2EDB2CB6967AAF0FADCC1B6433E93BF94E494FFFD1EB7FC62740EA1F583EAF8C0E9C186F17B2D3EA3B68DAD0FDDEEFED2F00734B5C5A796920FC97D4AFFA2EF815F2E5DFCF59FD777E5494C12492494F5BF51BEB0F40E80DCDBBA81CA7E466D2FC535D15B0B5B5BB6BBD5F52CB58EF574FA1E9A3F4DFAD3F567EAD6067D5D02BCDC9CDEA15FA2EBF3056CAEB6FB86E6D543ECF51CDDFF9CB8DF4DFFBAEFB8A8F2929EABAB7D62E995742E9FD33EADE567631C373DD7EE6B69173ECD6CC87D98F7BDFB99B7D3AE9FF0046AD75BFACDF577AC7D56C2E9D95666DDD5BA730FA398FAD843DCE1EEA2E71B9D67A3F41BEAFF39FA35C6963C72D23C4905300498024F924A7A4EB3D53EAE64FD59E9BD2F0ACCB397D2CDAE63ADA98DADE721EDB6EDCE65CF7D7E8EDFD17B7DE8FD67EBBDBD73EA8E3747EA21CFEA18990C70C8D08B296B2C66EB0FF00DC86EFDBFF000AB942D70E411F110906B8FD104FC04A4A7B7CAFADBD0A9FAB9D37A7748C8CEA7A8746B1D7E264BAAADA1CF797EF65917BFD3ABF4AE6FF0084FF00844DF5B3EB07D48FAC4C1982BCCC6EB0DAC07E432AAC5773DADD3ED157AFEDF7FF008467BF67FA55C490418220F81D120C79121A48F1829296493904183A1F029925297AC7F89EFABBD4B11F675CB833EC59D8E59410E97CB6C13BD9F9BFCDB9793AFA03FC59FF00E223A67F56CFFCFB6A4A7FFFD2F537FD077C0AF972EFE7ACFEB3BF2AFA8DFF0041DF02BE5CBBF9EB3FACEFCA929824784923C24A7E9EC2633EC3412D1FCD33B0FDD0BE73E93FF8A3C1FF00C3B57FE7D6AFA3B0BFA0D1FF0014CFFA90BE70E91FF8A3C1FF00C3D57FE7D6A4A7DCBFC61B1A3EA675621A01F486B1FCB62F30FF0014401FAE4C044FEAD773F06AF51FF189FF0088AEABFF00143FEAD8BCBFFC517FE2C99FF85EDFC8D494F51FE3B5AD6F4AE99000FD61FC0FE4287F8916B4E0F55900FE96AE7FAAF44FF1DDFF002574CFFC30FF00FA850FF121FD07AAFF00C6D5FF0052F494F39FE38401F5BDA008FD52AFFAAB17A3FF008B5630FD48E984B413B6CD48FF0085B579C7F8E1FF00C578FF00C2957FD55ABD23FC5A7FE223A67F56CFFCFB6A4A7C87FC61FF00E2D7AAFF00C6B7FF003DD6B9D5D1FF008C4FFC5AF55FF8D6FF00E7BAD7389294BE80FF00167FF888E99FD5B3FF003EDABE7F5F407F8B3FFC4474CFEAD9FF009F6D494FFFD3F537FD077C0AF972EFE7ACFEB3BF2AFA8DFF0041DF02BE5CBBF9EB3FACEFCA929824784923C24A7EA0C2FE8347FC533FEA42F9C3A47FE28F07FF000F55FF009F5ABE8FC2FE8347FC533FEA42F9C3A47FE28F07FF000F55FF009F5A929F74FF00189FF88AEABFF143FEAD8BCBFF00C517FE2C99FF0085EDFC8D5EA1FE313FF115D57FE287FD5B1797FF008A2FFC5933FF000BDBF91A929EA7FC777FC95D33FF000C3FFEA143FC487F41EABFF1B57FD4BD4FFC777FC95D33FF000C3FFEA143FC487F41EABFF1B57FD4BD253CE7F8E1FF00C578FF00C2957FD55ABD23FC5A7FE223A67F56CFFCFB6AF37FF1C3FF008AF1FF00852AFF00AAB57A47F8B4FF00C4474CFEAD9FF9F6D494F917F8C4FF00C5AF55FF008D6FFE7BAD738BA3FF00189FF8B5EABFF1ADFF00CF75AE7125297D01FE2CFF00F111D33FAB67FE7DB57CFEBE80FF00167FF888E99FD5B3FF003EDA929FFFD4F537FD077C0AF972EFE7ACFEB3BF2AFA8DFF0041DF02BE5CBBF9EB3FACEFCA929824784923C24A7EA0C2FE8347FC533FEA42F9C3A47FE28F07FF000F55FF009F5ABE8FC2FE8347FC533FEA42F9C3A47FE28F07FF000F55FF009F5A929F74FF00189FF88AEABFF143FEAD8BCBFF00C517FE2C99FF0085EDFC8D5EA1FE313FF115D57FE287FD5B1797FF008A2FFC5933FF000BDBF91A929EA7FC777FC95D33FF000C3FFEA143FC487F41EABFF1B57FD4BD4FFC777FC95D33FF000C3FFEA143FC487F41EABFF1B57FD4BD253CE7F8E1FF00C578FF00C2957FD55ABD23FC5A7FE223A67F56CFFCFB6AF37FF1C3FF008AF1FF00852AFF00AAB57A47F8B4FF00C4474CFEAD9FF9F6D494F917F8C4FF00C5AF55FF008D6FFE7BAD738BA3FF00189FF8B5EABFF1ADFF00CF75AE7125297D01FE2CFF00F111D33FAB67FE7DB57CFEBE80FF00167FF888E99FD5B3FF003EDA929FFFD5F537FD077C0AF972EFE7ACFEB3BF2AFA0EFF00AFFF0053AA7D943FAAD22CACB98E03711B87B5DEE6B0B5DFD95F3E5A41B5E41905C483F3494C123C2491E1253F50617F41A3FE299FF5217CE1D23FF14783FF0087AAFF00CFAD5EEB8BF5C7EAEB30E963B25E1CDADA08F42FE768FF00805E25D3BA7F51A7ADE2E55B8790DA2BCAAED7D869B2030581EE7FD0FDD494FB67F8C4FF00C45755FF008A1FF56C5E5FFE28BFF164CFFC2F6FE46AEF3EBA7D64E91D4BEABF51C1C1B6CBF2AFAC36AA9B45F2E3BD8E81BA90DECB82FF0016FEAF46FACCDCEEA945F8B8C29B186D753691B9C06D6FB2B739253D57F8EEFF0092BA67FE187FFD4287F890FE83D57FE36AFF00A97AAFFE353A962F5DE9F834F491765D94DCE7D8D651708696ED9F7D4C51FF00'
+ N'155D471FA0E2751AFABB6EC475F656EA9AFA2E3B835AF0E3FA3A9E929C6FF1C3FF008AF1FF00852AFF00AAB57A47F8B4FF00C4474CFEAD9FF9F6D5E6DFE32CD9D6BEB20CDE974DF958C31EBACD8CA6D03734BCB9BEFADBFBCBB6FA8DF58BA4F4BFAAB8181D42CB71F2A96BC5B53A8BC96CD963DBF469737E8B9253E6BFE313FF0016BD57FE35BFF9EEB5CE2DDFAF3954667D6DEA5938EE2FA6CB1A58E2D7349F6307D0B031EDFF00356124A52FA03FC59FFE223A67F56CFF00CFB6AF9FD7B37D43FAE9F55FA6FD54C0C2CEEA15D1934B5E2CA9C1D226CB1EDFA2C3F9AE494FFFD6E429FF00173F5DAEA996B3A53F658D0F6EE7D4C74386E1BABB2D6D8C77F21ED52FFC6D3EBC7FE55BBFEDDA7FF4B2FA092494FCFBFF008DA7D78FFCAB77FDBB4FFE9648FF008B4FAF11FF0025BBFEDDA7FF004B2FA0950EB78D9395D3ACA31890F746E683B4B9B3EFAC3F4DBB9A929E22BE89D71B5B1A706D96B5A0E8CE408FDE4FFB17AD7FDC1B7EE67FE496EE2F46CF6748CDC6C4A06136DADADAB19AF2D0E707175CE6BB759F67FB455FA0FF00C1136562F53186F6F48E9F6F4D6BEC2FAEBAEC631DB9B5C31F663D56FA1556FBBD9FA3BBDFFCEE452F494E1FEC5EB7FF00706DFB99FF00924BF6375AFF00B836FDCCFF00C92EB7A7E3F56667BF2321F6BAABBD7DD558F058D01D57D8C575B7F9BFD1FADBB6FF00D7161D5D1BAB31B18B88FC77556D366F7B981EEB1AEB3F9C7D2FF4F3A9AFD4F53ED17D55E47FC1DA929CEFD8BD6FFEE0DBFE6B3FF2497EC5EB7FF70ADFF359FF00925B9563F5FF00D1EF19871F79F4D9EB57EB07C55EFCBB1CE7B1F87EA7DA7657FA4FF8AFE6FD366749EB6FFAB997D33247A992FF004C50EF6FA7E99359F4852E7EDFD59AD7B6EDEEFD63FEB8929C4FD8DD6BFEE15B1F067FE492FD8BD6FF00EE0DBFE6B3FF0026B77FE6F750C1C81660D8F25AD2F06BDB4D5BDF752E7D3F6569DBE8371D8FFD1FFC67F8542BB03EB1DAC0E6FAFEAD3736C61B2C611EB6CCAAAEBA9877F40FD2E37A5558DFFD07494F9C75AFF17FF5CB33AAE4E563F4C7BAAB5C0B09B2A698803E83AD1B551FFC6D3EBC7FE55BBFEDDA7FF4B2F70E86CCD662BC657AA01B5C71DB90E0FB8550D86DF630BDAE7FA9EAFE7BFF0046B49253F3EFFE369F5E3FF2ADDFF6ED3FFA592FFC6D3EBC7FE55BBFEDDA7FF4B2FA092494FF00FFD9FFDB004300080606070605080707070909080A0C140D0C0B0B0C1912130F141D1A1F1E1D1A1C1C20242E2720222C231C1C2837292C30313434341F27393D38323C2E333432FFDB0043010909090C0B0C180D0D1832211C213232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232FFC00011080080008003012200021101031101FFC4001F0000010501010101010100000000000000000102030405060708090A0BFFC400B5100002010303020403050504040000017D01020300041105122131410613516107227114328191A1082342B1C11552D1F02433627282090A161718191A25262728292A3435363738393A434445464748494A535455565758595A636465666768696A737475767778797A838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FAFFC4001F0100030101010101010101010000000000000102030405060708090A0BFFC400B51100020102040403040705040400010277000102031104052131061241510761711322328108144291A1B1C109233352F0156272D10A162434E125F11718191A262728292A35363738393A434445464748494A535455565758595A636465666768696A737475767778797A82838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3E4E5E6E7E8E9EAF2F3F4F5F6F7F8F9FAFFDA000C03010002110311003F00F7FA28A2800A28A6B30552CC42A81924F402801D48580193D0579978BBE37F863C39E65BD8C9FDB17CBC7976CC3CA53FED49D3F2CD78378BBE29F8A7C5E5E1BABD36B62DFF002E76994423FDA3D5BF138F6A00FA03C5DF19FC2DE17DF6F04FFDA97EBFF2C2D181553FED3F41F864FB578378BFE2DF8A3C5C24B792EBEC1A7B641B4B425430F476EADFCBDAB83F6A2803BCF08FC5CF147844476F1DCFDBF4F418169764B051E88DD57F97B57BD7843E33785BC51B2DE69CE977EDC79176C02B1FF65FA1FC707DABE49A2803EFBC83D3BD2D7C71E11F8A7E28F08148AD6F7ED5623ADA5DE5D00FF64F55FC0FE15EF3E11F8DFE19F11F976D7D21D22F9B8F2EE58794C7FD993A7E78A00F4EA29AAC1D432B02A464107834EA0028A28A002B93F893AB5F685F0F758D4F4D9FC8BCB78D0C52850DB4975078208E84D4FE2CF1CE89E0A8AD64D6A69A34BA66588C7117C95C6738E9D45798FC43F8BBE11F11F80F56D274EBAB87BBB98D56356B665048753D4FB03401E69FF0BA7E207FD077FF0025A2FF00E26B235FF887E2BF13DB0B5D5B5AB89AD8758502C68DFEF050037E35CC51401A56FE1ED6EEEDE3B8B6D1F509A190663922B57657E71C103079AAB776179A7CA22BDB49ED642321278D909FC0D7B07C289AFAE7E18F8D6DA2D5FEC263488413CD70D1C76F9DD92187DDCFB55DF1259DF27C0373AAEA51789EE45EAB417F6B2FDA16CD7233BA43CFA83FEF81401E216F697178EC96D04B3'
+ N'322348CB1A162AAA32C4E3B01C9A4B7B69EF2E23B7B58249E691B6A471216673E800E49AF67F833A72E8D603C4173A45CDF8D5EEC697188A26710C07FD6CAD807E5DDB57B7435E71E33D06E7C15E37BFD3627922FB3CDE65ACAAC55BCB3CA107AE71C7D41A00C3BCD3AFB4E94457F67716B211B824F132123D4038AB52786F5D8A35924D17524462A159AD5C025BEE8071DFB7AD7BB5FDBE93E3EF0C787FC2BAACE63F1249A1C17FA75FCCD932BB021A363D4E768CFAF5EA2B82F1B49AE695F0E7C27A7EA173790DE41757C9346F2B643248BB7BF38EC7F2A00E1E4F0D6BD0E3CDD13524DCE231BAD241973D17A753D85417BA3EA7A744925F69B796B1C84AA34F0320623A8048E4D7D25AB68973AB7853C33E2199AF3541A569115D0D2A19D83DDCFB54ABB1CE485E49C658FE87E79F11F8AB59F15EA325EEB17D2CEECC5963DC7CB887A22F451FE4F34017B40F885E2BF0C5B1B5D275A9E1B73D2170B222FFBA1810BF856BFFC2E9F1FFF00D077FF002562FF00E26B81A2803ED1F86FAB5F6B9F0FB47D4F529FCFBCB88D9A593685DC43B01C0007402BABAF0DF87BF177C23E1DF01E93A4EA177729776D1B2C8AB6CCC012EC7A8F622BD3BC25E38D13C6B15D49A2CD2C8B6ACAB299222982C0E319EBD0D00715F1D7C27AA789740B2BAD3D613169827B8B8F324DB84DA0F1EBF74D7CB95F6FF8D3FE446D7FFEC1D3FF00E8B6AF87FB5002D1451D7803AD007A57837C57E12D13C0FAC687A9C9AC34FACAAADC1B6B78CAC3B738DA4BFCDC1E72053D3C6FE16D17C1977E15D021D58DBEA72AFDBEFAF523DEB1F01BCB8D5B04E060648AF35F2E41C98DB8FF0064D34658E0024F602803BEF1778D2CEE3FB1ED7C27A9EB561A7595A8B636EDFBADA01C97CA3FCCCD924E7157BE20F8BBC25E32B4D2E6493581AAD9C496D2DCCB6D18171182373300FC30F988FAE2BCD0A3A8CB2301EE2915598FCAA5BD7033401DCF8CFC49A16A2BE1FB8F0F5D6AB1DEE91690D9A35C4291E563C959015724364F4C7E3563C6FF001020F1F69BE1A4D4D6582F2C8C897F245182195B661D064724024AF1CD79F1565FBCA467D452AA3B0CAA311EA05007AF6ADF137445B8F0CDE787EF759B4BAD1624B3632DB46567B7F943EE1BF9385C85E99EE319AC2F881ADF807C4B3DCEADA25B6ADA7EA921CBC4D047E44CD9E58E1F2A719E80E4F6EA6BCF4820E08C11D4114E11C87911BFD769A0065147D68A002BEA3F815E12D53C33A0DEDCEA2B088F53105C5B98E4DC4A6D279F4FBC2BE5CEC6BEE1F06FFC891A0FFD83ADFF00F45AD0037C69FF002236BFFF0060E9FF00F45B57C403A57DBFE34FF911B5FF00FB074FFF00A2DABE201D2800AD5F0CFF00C8D9A30FFA7E83FF00435ACAAD4F0CFF00C8DBA2FF00D7FC1FFA316803ECAF184683C13AF1D8B9FECFB8EC3FE79B57CADF08F9F8ABE1F1FF004D9BFF0045B57D57E32FF911F5FF00FB075C7FE8B6AF953E117FC956F0FF00FD766FFD16D401EE3F1FD117E19FCA8A3FD3A1E83D9AB86FD9B9436BFAE82011F658FA8FF68D777FB40FFC934FFB7E8BF93570DFB367FC8C1AEFFD7AC7FF00A11A006FED22AABE20D080000FB2C9C01FEDD773F00115BE1A7CCAA7FD3A5EA3D96B86FDA4FF00E461D0FF00EBD24FFD0EBBBFD9FBFE499FFDBF4DFC96803C33E2E607C55F1071FF002D97FF0045AD7D53E0E8D3FE10AD04EC5CFF0067DB9CE3FE99AD7CADF177FE4AAF883FEBB2FF00E8B5AFAB3C1BFF00224681FF0060EB7FFD16B401F1978987FC559ACFFD7F4FFF00A30D65D6AF89BFE46CD6BFEBFA7FFD186B2A8013B57DC5E0DFF912341FFB075BFF00E8B5AF877B57DC5E0DFF00912341FF00B075BFFE8B5A006F8D3FE446D7FF00EC1D3FFE8B6AF88074AFB7FC69FF002236BFFF0060E9FF00F45B57C403A50015A9E19FF91B745FFAFF0083FF00462D65D6A7867FE46DD17FEBFE0FFD18B401F6678CBFE447D7FF00EC1D71FF00A2DABE54F845FF00255BC3FF00F5D9BFF45B57D57E32FF00911F5FFF00B075C7FE8B6AF953E117FC956F0FFF00D766FF00D16D401EE7FB40FF00C934FF00B7E8BF93570DFB367FC8C1AEFF00D7AC7FFA11AEE7F681FF009269FF006FD17F26AE1BF66CFF0091835DFF00AF58FF00F423400DFDA4FF00E461D0FF00EBD24FFD0EBBBFD9FBFE499FFDBF4DFC96B84FDA4FFE461D0FFEBD24FF00D0EBBBFD9FBFE499FF00DBF4DFC96803C33E2EFF00C955F107FD765FFD16B5F567837FE448D03FEC1D6FFF00A2D6BE53F8BBFF002557C41FF5D97FF45AD7D59E0DFF00912340FF00B075BFFE8B5A00F8CFC4DFF2366B5FF5FD3FFE8C35955ABE26FF0091B35AFF00AFE9FF00F461ACAA004ED5F717837FE448D07FEC1D6FFF00A2D6BE1DED5F717837FE448D07FEC1D6FF00FA2D6801BE34FF00911B5FFF00B074FF00FA2DABE201D2BEDFF1A7FC88DAFF00FD83A7FF00D16D5F100E940056A7867FE46DD17FEBFE0FFD18B5975A9E19FF0091B745FF00AFF83FF462D007D99E32FF00911F5FFF00B075C7FE8B6AF953E117FC956F0FFF00D766FF00D16D5F55F8CBFE447D7FFEC1D71FFA2DABE54F845FF255BC3FFF005D9BFF0045B5007B9FED03FF0024D3FEDFA2FE4D5C37ECD9FF002306BBFF005EB1FF00E846BB9FDA07FE49A7FDBF45FC9AB86FD9B3FE460D77FEBD63FF00D08D0037F693FF00918743FF00AF493FF43AEEFF0067EFF9267FF6FD37F25AE13F693FF918743FFAF493FF0043AEEFF67EFF009267FF006FD37F25A00F0CF8BBFF002557C41FF5D97FF45AD7D59E0DFF00912340FF00B075BF'
+ N'FE8B5AF94FE2EFFC955F107FD765FF00D16B5F567837FE448D03FEC1D6FF00FA2D6803E33F137FC8D9AD7FD7F4FF00FA30D6556AF89BFE46CD6BFEBFA7FF00D186B2A8013B57DC5E0DFF00912341FF00B075BFFE8B5AF877B57DC5E0DFF912341FFB075BFF00E8B5A006F8D3FE446D7FFEC1D3FF00E8B6AF88074AFAC7C51F143C157FE12D62D2DBC436D24F3D94D1C6815FE6628401D3D6BE4EED40056A7867FE46DD17FEBFE0FF00D18B5975A5E1F963B7F12E9534A76C71DE42EE704E00704F039A00FB3BC65FF223EBFF00F60EB8FF00D16D5F2A7C22FF0092ADE1FF00FAECDFFA2DABE88F1378EBC3D7DE15D62CEDAEE792E27B39A28A31653E598A1007DCF5AF9F7E1C59DEE87F10747D4B53D3EFADACEDE5632CCD69210A0A30ECB9EA45007B6FED03FF0024D3FEDFA2FE4D5C37ECD9FF002306BBFF005EB1FF00E846BA2F8C7E23D37C4FE06FECED15EEAF2EFED71C9E5259CC0ED01B279403B8AE53E07DE2784B58D5A7D7A0BCB18E781122692D253B886C91C29A009FF693FF00918743FF00AF493FF43AEEFF0067EFF9267FF6FD37F25AF3DF8DF769E2CD67499F4282F2FA382DDD2568ED251B496C81CA8AEB3E0E788F4DF0C7817FB3F5A6BAB3BBFB5C92794F67313B485C1E14FA1A00F22F8BBFF2557C41FF005D97FF0045AD7D59E0DFF912340FFB075BFF00E8B5AF963E23DA5E6B9F10758D4B4CD3EFAE6CEE25568A65B4900601141EA33D41AFA0FC33E3AF0F58F85B47B2B9BB9E3B882CA18A48CD94F95608011F73B11401F2BF89BFE46CD6BFEBFA7FFD186B2AB4BC432A4FE26D5A688EE8E4BC99D4E08C82E48E0F359B4009DABEE2F06FFC891A0FFD83ADFF00F45AD7C3D5F597863E28782AC7C29A3DA5CF882DA39E0B2863910ABE558200474F5A00F07FF8537F107FE85C93FF000261FF00E2E8FF008537F103FE85C93FF0261FFE2EBEC2A2803E3DFF008537F103FE85C93FF0261FFE2EAC58FC20F1EC1A85B4D2787A4091CC8EC45C4278073FDFAFAEAB9EF1769973AA6991C30C6668564CCD6E0E3CD5C74EA3233D467F950079DB787B5E2EC46917479FEE27F8D37FE11DD7BB69173FF7CA7F8D75EBA1DFA786D2DD6C316A9791CC74B5933BA05C6E41B8E393F36C271C63BD35AD75A85F4DFECDD2EE74FB48E732FD9A2B8042A195772BAEE0065371DA0B28CE060D00725FF08EEBDFF408BAFF00BE13FC68FF00847B5DC7FC822E7FEF94FF001AEEB4FD375A8F4CD420BD92EAE5AE6C86164B901BCE3E60654619D9C6CC718EFEB5811683AD44904B65A7DC5A2C32481446D1C3210D128625158A06E0A861D4E0903AD0062FFC23BAF7FD022EBFEF84FF001A4FF84775EEA748B91F544FF1AE9A5D3FC493DB3C32A6A4E9241225AA9B84C2292FC5C64FCEDB4A74CFD41C9AB575A16B5A8F86F4FD3AF764B7906A28D7134A8B247222E4EF11938DBC81B7B7BE324038FF00F84775EFFA045CFF00DF09FE34ABE1DD783293A45D7073F713FC6BA987C33ACE9170E349B99CEC58A247791446E3649B894EC03328031C718E01AAEFA5F885DAD2E15355D9119502BDCA1B85898445C13BB058959361CF1C74C8C007855F7C20F1EDC6A375347E1E90A492BBA937108E092471BEABFF00C29BF881FF0042E49FF8130FFF00175F57E8297D1E8B6CBA8990DC80DBBCC60CC1771DA188E0B05DB93EB9AD3A00F8F7FE14DFC40FFA1724FF00C0987FF8BA3FE14DFC41FF00A1724FFC0987FF008BAFB0A8A00FFFD9, 1)')
EXEC(N'INSERT dbo.Bank(ID, BankCode, BankName, BankNameRepresent, Address, Description, BankTransactionType, Icon, IsActive) VALUES (''f7a7e079-c577-4ce2-ac75-d81ba25a88a8'', N''BIDV'', N''Ngân hàng Đầu tư và Phát triển Việt Nam'', N''Bank for Investment and Development of Vietnam'', N'''', N''Ngân hàng Đầu tư và Phát triển Việt Nam'', 0, 0xFFD8FFE000104A46494600010101004800480000FFEE000E41646F626500640000000001FFE10AD64578696600004D4D002A000000080007011200030000000100010000011A00050000000100000062011B0005000000010000006A012800030000000100020000013100020000001E000000720132000200000014000000908769000400000001000000A4000000D00048000000010000004800000001000041646F62652050686F746F73686F7020435336202857696E646F77732900323031383A30363A32362030393A32393A3334000003A001000300000001FFFF0000A00200040000000100000080A003000400000001000000800000000000000006010300030000000100060000011A0005000000010000011E011B0005000000010000012601280003000000010002000002010004000000010000012E0202000400000001000009A00000000000000048000000010000004800000001FFD8FFED000C41646F62655F434D0002FFEE000E41646F626500648000000001FFDB0084000C08080809080C09090C110B0A0B11150F0C0C0F1518131315131318110C0C0C0C0C0C110C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C010D0B0B0D0E0D100E0E10140E0E0E14140E0E0E0E14110C0C0C0C0C11110C0C0C0C0C0C110C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0CFFC00011080080008003012200021101031101FFDD00040008FFC4013F0000010501010101010100000000000000030001020405060708090A0B0100010501010101010100000000000000010002030405060708090A0B1000010401030204020507060805030C33010002110304211231054151611322718132061491A1B14223241552C16233347282D14307259253F0E1F163733516A2B283264493546445C2A3743617D255E265F2B384C3D375E3F3462794A485B495C4D4E4F4A5B5C5D5E5F55666768696A6B6C6D6E6F637475767778797A7B7C7D7E7F711000202010204040304050607070605350100021103213112044151617122130532819114A1B14223C152D1F0332462E1728292435315637334F1250616A2B283072635C2D2449354A317644555367465E2F2B384C3D375E3F34694A485B495C4D4E4F4A5B5C5D5E5F55666768696A6B6C6D6E6F62737475767778797A7B7C7FFDA000C03010002110311003F00F554924925292492494A4924925292517BDAC639EE30D68249F20A18D958F974B6FC6B1B754FFA2F61041FB925254924925292492494A4924925292492494FFFD0F554924925292492494A492492529335AD6CED004924C69A9E4A74925292492494A4924925292492494A49249253FFD1F554924925292492494A4924925292492494A4924925292492494A493020CC1E3429D25292492494FF00FFD2F55597F58BAF53D0705999754EB9AFB055B584020B83DFBBDDFF0016B51723FE33BFF13F57FE1A67FD45CA4C3112C9189D89D56CC91124746B7FE3A5D37FEE15DFE7356A746FAF5D13AB643715A5F8D91618AD9700038FEEB2C639CDDDFD6585F54FEB4FD5CE9DD0A9C4CF7C64B1CF2E6FA45DA39EE737DE1A5AEF6AC5CC6B7EB17D68167D5FC575551757B9ED6ED0D2D3EFCAB367B29FF5FF000AADFDDF193389C72C62375909F4E9FDE62F72540F1095FE8BDBFD60FAEB87D0B39B857E3D96BDD58B7730B408717363DC7F90A3D77EBC61745CEFB15D8D6DAFD8DB3730B621DFD62B92FF00199FF8A1AFFF000B33FEAED51FAFDFF8A7ABFE269FCA50C7CBE390C563E78CA52D7F778552C921C5E0407A2A7FC67F457B836DC7C8A81FCE86380FB9FB95FEA7F5E3A6E161E3E7515BF371324B982DA881B1ED877A56B6CDAEAECDAEDDB5CB53A8F43E95D4719F8F938D596BC101E1A03DA7F7EB7C7B5CD5E51818790ECBCFE8125EF70B76300E6FC50FB18E6FF29CCAEEABFEBA9B8B1E0C964031E0D65126C70F7B4CA538E960DEC7C5F5AE91D531FAB74EA73F1C115DC0FB5DCB48258F63A3F75CD543EB27D6BC2FABE686DF5BEEB323716B2B8901B1EE76EFEB2E6BFC57F55FE95D26C3FF0076291F757737FF003D3BFEDC58FD76DB3EB37D73FB25249ABD418B511AC57593EB5A3FF06B928F2C3DF9C65FCDC0197F83FA29390F0023E63A3E99D27A87ED2E9F4E70A9D436F1BD95BE0BB6CFB1DEDFDF6FBD2CACAB0D9F64C3839246E7BC896D4C3FE12CFDE7BBFC0D3FE17FE2D46DBBD20CE9F801ADB18D6B64EACA6B036B1CFF00DE76DFE669FF0009FF0016B9AFAFF45789F5771D9439D273A936581C77BDC43F7BED7B23739CAAC63EE64108FA448D77E16C0031C78E6388D7A63FF7726EF4BEADD2B2B20B7A1DAFBB31AC3664576B5CDF5DA086BDD659635AC6E4EE7FE8ACFF00AD7F33FCD7418D93565542DA89892D735C21CD70FA55D8DFCC7B178AE5B9CCA2F2C739AEDC20D64877D2FCDD9EE5EADD3A9CF6F4DC3CD7E9D4CE3D5F6DA5E76FAC4346E6DB3F4325BF996FFD6ECFD1FF00371E204E08E5B1EA94A1C23FABFA41BDF12C38F173470032D2119C726437'
+ N'F37E84CFA7FC0769241C6C9AB2AAF56A3A4ED734E8E6B87D2AEC6FE6BDA8C9CE79041208A21FFFD3F555C8FF008CEFFC4FD5FF008699FF005172EB964FD64E80CEBF80CC27DC71C32D16EF0D0E9DAD7B36C12DFF0048A4C3211C9194B400EAB66098901E5FEA8FD4FE85D53A0D399994B9F7BDD6073858E6FD17B9ADF6B4EDFA2B13EB163D7F55BAEB0742CB7B48636C7B03F7163A5DFA2B76FD36B9A3F9BB16FB7FC5754D103A9D80780AC0FF00D18B43A4FF008BCE8FD3F2599573ECCCB6B21CC16406023F3BD36FD2FED3D5BF7F1894A472CB244DFEAB84D6BFDE62E09100708891FA56F29FE319EFB3ADE3BDEDD8F761D4E737C0975A4B53FD7EFF00C53D5FF134FE52BADFAC9F522BEBDD41B9AECB76396D6DAB60AC3BE8973B76EDECFDF50FAC1F516BEB7D4066BB31D4115B6BD82B0EFA3F9DBB7B12C7CC621ED59AE18C84B43A7170AA58E478B4DC87A0EA3D4F07A6E33F2736D6D55B013A91B8C7E6D6DFCF7AF38FA9155DD53EB7D9D4F690CADD6E45A7B036EF6319BBFAD6FF00D05B35FF008AEC42F0EC8EA16DA3BED6069FF39CEB5757D27A3F4FE8F8BF65C0ABD364CBDC75739DFBD63FF3944278B1639C612339641C375C318C57D4A52048E111D7BBE5DD61995F557EB55D661433697598D23DBE9DCD7376C7FC1EF7D7FF005B5B3FE2C3A57A99393D5AC12DA47A1493FBEEF7DAEFEC57B3FEDE5D2FD68FAA18FF00586CA2E75E71ADA03985E1A1FB9A4EE6B4FB99F41DBBFCF5A1D0BA3D3D17A655D3E977A82B92EB0882E738EE2E23FE8A7E4E6632C143F9C90119FF00762B638C89DFE88D435BEB0FD5EC7EAF8EED22F1A820C491F475FF0048DFCC7FFE8B5C26067752FAAFD45CD734BE9798BA8768DB1A3C374FA5915FFAFE897AA2C7FAC3F57B1FAB63BBDA05E0687898FA3EEFCD7B7F31FF00FA2D6665C57EB86930ECFC3FE20200F2FCC0F73979E841FD0795B33BEADF42A1B9BD00BEFCFCBA8B582D7170A9A482E7DF5BBE8DAC7B3D95FF00E8B54BA07D5FCBEB79672B28B9D5B8FA9659649DD3FE12CFE4BBFC1D7FE17FE2113A2FD51C9CAEA0FAB223D2A1D0F91FF9F99FFA23FC27FC47F39E8B8B8B4E252DA696C34727B93FBCEFE528F1E3333721C311B4437B9CE771F2B194304CE6E6320F5E799E39C71FE8478958B8B4E25229A5B0D1A92752E3FBEF77E739192495A79F249249364EE5FFD4F554924925292492494A4924925292492494A4924925292492494B0004C0E7529D24925292492494FF00FFD5F554924925292492494A4924925292492494A4924925292492494A4924925292492494FF00FFD6F554924925292492494A4924925292492494A4924925292492494A4924925292492494FF00FFD9FFDB0043000202020202020202020203020202030403020203040504040404040506050505050505060607070807070609090A0A09090C0C0C0C0C0C0C0C0C0C0C0C0C0C0CFFDB004301030303050405090606090D0A090A0D0F0E0E0E0E0F0F0C0C0C0C0C0F0F0C0C0C0C0C0C0F0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0CFFC20011080080008003011100021101031101FFC4001E00010002010501010000000000000000000007080902030405060A01FFC4002F100000060200040404070100000000000002030405060701080010110930603334201435384050211213151617FFC4001C0101000203010101000000000000000000000405010306070802FFC4003D11000202010205020108040F0000000000010203040511060021121307314122105161713223140840B415762030506091A142527292637383B366FFC4003D120002010204010708090305000000000001021100032131411251617191B1C1420410F081A1223272132060E1F15262823314D1B20530E22363B3FFDA000C03010002100310000000CFE000000000000000038E75677800000000001B26F000000000000000000000000000000000000000D0CEB600103DAC5A63D2D7CF3532B4E71C0FD62359BA65681BEC553CB892C344BF5D23F2A7A2AC957E85706EBCB7BFD9080C76763530959C6AC5770FDA46D9C2FD6333BE6FD0FCFB7ADF2B79797B2AE37313373E39E8B48FB0A4C48F17F4FF00D004EF9EA7DD10BBFD90863B3B1A988AC3455ABD83DCEAFD6FE339BAF31E8F0A9E99CEC6B374D93A59990CF23F5EC4BF23F4B4DF37918CA2F459B5EEBE47F45B200AFB6F169574D5B2ED748ECBF19E07EF114CFD17FB92B583AD234C55DBE5EAE915BEB3BDC68731F406677B6F943D16C820000000003433AD800000000000000000000000000000000000000000000000000000000000003FFFDA0008010200010500F30B3B50DC8EFF000A7F0E51656880D11A35C496B8B9CE049907561C228C28506B8211A23D9984D73E17A4F943CE3B3D52AC20C194684C0F3847BE7F625EA9626165ADBA13EC627F4E46E8A5298AD483054E1070D600B4352952330707240053518702797828B02C28D0981E508F7D21912C46B59CD13C238707014713FA7234472B324E6011B5B708B796D9C2EFDA5B8B7015013A83DB0ECA848842DCDC358328A0941E4CAED96D3B33916787097AA545B34984DA4344A44DE40A726E30E0E27AE318E4236B0BA388D7A8E1C5B82A82899C661A5141283E62FFDA0008010300010500F2E6439C7C2AD561383FBA0709DCCA384A9C429C6A5C8240C2F2567839C8B2C241D8380AD70537041BFCA06E6E2F05C8632EC853B8379A84DE6F3E8A15A4144998F9950F1EBBA7AE6A62CD09458B22653'
+ N'F8539CAA54DAD8584A91B81EAC7B16210634CCB70724706F3509BC9E7D140809389561C2439DB3D4D74F7069C0282DA1C9AA546049143311D45089C288FA87B656C9DB692CB24959F359B248C2472723DC0EE4B12E14030CB8E086A28B12C6EC281AA6CC1E3C3287A909C04856A00A9CA64F820BE21137511F512BB393216F717139C0EF1B22CE7CA9FFFDA0008010100010500FC32B544214B1C9347A60CFE390993A6C7E68130B18BE0D87BE19F5E2118EE935C75A6B7A694B8E41B03BA90ED7B9C5E9BC50BA2670CFDCF69758AACBDDFAD60911A8ACF60B92BCD8EDAB85EB70AA89FFF00D4ABE94CA5C86E3585B554CBA411A92B54ADAB9F736FB7DD4FDA3D71AC68A98A64FB37B3FDCC7EE1B7E318CECE5874755967C72070E902A95F6BFB4FA66F3725FB57B96E8EF86706FF0032A087EBB4B942A46C75E344F5256F1A9334CADAF97736FB7DD45D41A2ADCA2362985BB51EF4EE2EAD5385DBBEFF00AECED8565C22AC8E691B4BCDB5B776FA4936A26D4F6C3AB72E524D83D7B8FDCD1F82CDEC7D4CB15C273ADFAF8C9416BF4AEFA96C562ACB0D66E5B254224D8B8193DAF5AC80D4BDBCAA0ADE45B21A4ADFB113FD81D176EBE67E87B5DC48C5B53D3B5FD2918D9ED428FECA385174FB35175AF1B05AFAC17230533A8D2496581168B334359BC60965833E53FFDA0008010202063F00FAC26D2B052149C79081DB5FBABD068DC30EA332BA738207AA68DD475500918CE91FD6BE6A3A8124633A54ABA1E91D94F65C8B6E9060EA0EA23314D65F35F5EA0D3142142C62675A6B3B83153048CA75E8CAB627BDA9D14713D835E6A8F0E4970248208DFCE48F7B81F465948FB41E0797E837C07AD69AE591EC1023DA0320349ADBE36E0260E133339209C5BB398537C67A969BE26A0F6DCE1A4983C846B56BC66406D93F92E403D04A9F4527891F09EB5EDF557CC618ED2E7E26F747F6AD100CB9C493A4EA794E835E6AB8063FF139338C98189E5F3CA886008F92F9E59AF1ABAFE1A237181A32CE1FED3E838548FB8F03CBE56F80F5AD35AB4C02803407300D1FE5DB07120188910311C0F28A700C8171BA8537C4D42DDA5249E81CE74142C4E242A8FD3049F57AC52ADDC660371DCA475C03CC693C32F7BDA3CC301D267A2BF379FAFEECA8E70731F887630F3C285CF0A497711892768D770E3C9D95BDE48389275E53D835E6ADABE53742EE9522263320F657EC0E9FB28DB402D83818CFA74F40A3685B0D2C4CCC6600E07857CA16C3624CCC67E835096547A49EA02BE65E693A70038014EA177868313107A0E7D94D79844C40E00793F379FAF81ECA21B25F3C7FA6BCD9ED5CBEB1FF00FFDA0008010302063F00FAB8275FA3B889C62BDD35B710796B6904E135B48270AC41141C0DC0EA3A8D07191A1226683C44D7F2FC5C8B20C2A830D79C7710E8A30F99732407572AA437F9BB696FC3B304B6C8CADFC762095088A4B1B50BEDAE24FBE09B93BCDABA04C020832ACA7DD656C99586208EBFA03E21D4682BE78E953656061F7F250E61D668730A86029ACF3F4AF991E9A6B679C76D6D194C7A067DA68789F1208B230551835D61DC4E0A3BF7325180972AA52E5CF661942A810A882611068A31E24B4B125C96368AE7FC9B5FDAD49E13C74FCB8957825ACB1CC8FC56D8FEE5BFD69EDC86F977469208C5594E4CA7BCA743DB23CA3E21D468330C71D687CA6F3E1409FC23B68730ADCC60517E127A6895F4731A370E980EDF3E5A5C66D1CC1C76839E1AA9EF2FEA58700D2B0216E289B6E20B5A6388988DF69FD7A6D75203782FF3CAB6FC2787B818EC50A6F3282145B619A15692FA0207BD801E17C2855651B555606C8EE27023BEFDCCB1B87D937AF192721A28D1546807DA64927CBB66319AF7CF47DB5B892C47456EDD184655BB7461C2B1726B6A08A06608A0831F20C49B44E233DB39E1AA9EF2FEA58700D25DF0F3BEEAFB38FF00E6DA8FFB23D91847CDC10DEBC658F401A003403ED32493FEB89D3EAA7FFFDA0008010101063F00FD1ACDDB5276AAD389E7B32E85BA638D4B31D14127403D87153706D6CDD2DC384BC09A994A1324F0BF49D1875A120329E4C0F30791E7FA048B5A08EBACB23CD2AC6A1034921EA773A01AB313A93EA4FF002AC8A8EAED13744AA08255880DA37CC74607EA3FC1C7EF8CDE02F6E2A790CCC1865A54248A3911E782C4E24632903A40AE46839EA47D3C0D7C63B900F722CD327FA3A8714768D77C9ECDDD19593B388C767A28A38AE4C7ECC3059825963EB6F4557E82C7E15D58806A6C5CFECECCE7AEDBC4D7CBA5DC7C95D6211D8966882112BAB7503092797BF0BB1B39B2B379ABAF8CA9931768C95962E8B618AAE92386D57A79F114198D9BBB70D048746BAB153B289F4B2A5957D3FC2A4FD1C6CEF206071390F22EC9DE3359A50EE0C2BC51AD2BF596391A95D8AC18E486668E4EB547009504E9A73E36E791B6DC7357C5EE18A4614AC74F7EBCD04AF04D0CBD048EA49108E5EA343E878DAD5B7161B21B8B25BA85A96B63F18F0ABC3055ED8696532BAE81DA40ABF3E8DF371B6BC809B7EE6D8ABBA6BB5DC7E2720D1BD8159A46582563112BA4D18122E87ECB0E0ECCD97DA9F75CB02D8CAE52643354C1539350B6ED2A91DC91FA4882B821A560492B1ABBACF53C019FC8EE0DF94B1F2E57786233356DD51B9ABC32C71CF35AB76218E24BA1E51D99474A807B4544217B51E5B13249DBEE3D7B94EC218ACD4B311E99AB5985BE28E58DB9329FEB0413F2EDFF00DF9C77EA191E36DED0F21E4447BAF1D672525EA83093DB62962E4B2C43'
+ N'BE903A3EA8C3D5B97A7B715F29F96FD877B09877B78B6B590AF4C548AB49048BDECBDA5AFAC755751AEA48672A0E9DD7E9E309FB958DFD76FF001800406076F6DFD411A83F7927A83C6476D6EAD998BB15EFC0F1439182AC315DA8EC0859AAD854EB8DD09D411C8FA302A483E5AFCB98924C95DB91E646371F0A06597706CF4B56E09620DAF43490D7B30020EA44BA71BEFC3791B1EBA6E8DB31B1FF006EADF8D49FF81D547FA8DF3F09B3F0965E5C39CCC1B3B07662D1C438DC6BB9BF6D3D99430B1601FEEE9C63FC6DE3B8AA55CAE3E957ACD62401A8EDEC7246238659D3A875C85174820D434846AC5635671B429EDFBD77BB63C9F819F319A4B321BD7ED4C967BF62E4F014323C800D4725550A88AA8A8ABBA26A36AF51B2B72B88A7C649343641FC6368236AE449AEBA6807BE9EFA71E35DF775962F2FBED4C38F266DBBD288067658E9C7DC8ED97D0457A3E7DB998726D6393EEF431AE53132B94591EBDEA732F6ECD4B316825AD6623F1472C64E8CA7EB1A8209F936FF00EFCE3BF50C8F1B677BEF6DB776FEE4C9DAC9C372EC393B55D596B5C9618BA628A4545D1140F4E679F18B8FF2FDE41C8D2923C7D6C9E46825D161E8DA134AA695BEDE8B2C6E8818C53293D2DF16AAC35D9D7EF55346EDDF1F61AC5CA475D61964B3759E33AF3F85891CF8DBE3FF003FB7FF00EC938C8EE9DF1B82A60F178E81E6E99A4513D82A3511568490D2C8E792AA8D49E32BE55FC1495F1F87B39CDD59C91753143366458860AE64034EA67B4C40F75463EC78DC992D8CB163CD39ADE536599632D5FF0066672B4D188D5015EA1077A4894EBF6E3D7DB8DEDE62C957EE57DBF08DBDB72671A8376D859AEC8A7D434507427D2263C5BD2B8AFB923512416237EC9B0F1A958C9907259501D239083A0D51F58D88E2D55B74E5C9E0AFCC60DCDB5ED75475B2D5E03D2CC8B21715EE401B97AF4EA01EB89D58D0DF7F973B17F7279337DE126A98F8B2F75EDC182AD34C8F34F90AD27D8B11CB0F4A444EAC4124F6F9B4DBBB76D8B77B1B7AC7ED2CB65724F24A6EB4C75FC55A048EA47D3EEA2E5DDD353D302EAD5F0982ADD8AB0FC734CE7AE6B1310034D3C9A02EEDD23527D800345000F931DB1AEEE69F6A438FCDD7CD7ED082B25B673057B10088A3C91E80FE23AB507DBE9E1628BCE3988A104FDD478A441CFD7402E69CFEAE31FBB33B95CA790F31899D2D636B64D62871F1CF19EA495EB46099195B4203C857E753C53DF96BC89676ACB4F0D5B1031B16312DAB0AF34F3773B8D662235EFE9A74FB7AF09BEECF92AD6D9963C553C5AE362C5A5A1A540FA49DC36623F1757A74F2F9F84B1B97CC19FCD43A8EF256A305699941F412CD2DAD3FCA785DA9E3DC22E271EF277EFDA91CCD6EE4FA6866B33B7C4EDA720392A8E4A1472E36A666CEE99B66E676CD7B14A5BF0524B86DD599D648E29034D0F4F65C39520FF6DB97A7183F1CE1AEBE563C5BD89EFE6A5896096E59B32B4B24AE8ACC06808451A9D15546A74F92D32D58E2DCB1460D7B00888D83103DB064D0F44A9A9EDC9A1D3528E1A3665E32B88DCAB1261F6C5B0993678F40CC3EC9BD013AA30039572C4B9E65BB1A3495B0582ADF87A75FE2776D0CB34A400F34CE00EA76D399F40340005000FE3A464454695BAE560002CC005D5BE73A003EA1FCD4FFD9, 1)')
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.GenCode SET Prefix = N'GTS' WHERE ID = 'f4eab988-3eba-4273-be51-3bfd28b1a1d4'
UPDATE dbo.GenCode SET Prefix = N'PT' WHERE ID = '514129e9-fbe7-4b71-9f64-f5433834d99c'

INSERT dbo.GenCode(ID, BranchID, TypeGroupID, TypeGroupName, Prefix, Length, Suffix, CurrentValue, IsSecurity) VALUES ('aefd70cf-0320-4bc1-95cc-a505e0655d3d', NULL, 18, N'Kiểm kê quỹ', N'KKQ', 6, N'', 0, 1)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DELETE dbo.Sys_Role WHERE RoleID = '0e5bd5f7-630b-4ce9-b8bb-91643f1f1fe3'
DELETE dbo.Sys_Role WHERE RoleID = '1a864d31-4560-4d42-9d63-bd02422a6237'

INSERT dbo.Sys_Role(RoleID, RoleCode, RoleName, Description, IsSystem) VALUES ('cce2e4c9-935f-48eb-9ce4-595657b727de', N'KT', N'Kế toán', N'', 0)
INSERT dbo.Sys_Role(RoleID, RoleCode, RoleName, Description, IsSystem) VALUES ('60773a1b-d1e6-474c-b09e-78dc9bac9e80', N'KTTH', N'Kế toán tổng hợp', N'', 0)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('4bb646ee-63ce-4c2f-9f12-007c355dcf2c', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '944b2211-d659-4a4f-adad-c2043de0f5fa', N'FMBInternalTransfer')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('4bb7cb1a-8291-474c-9b5b-0111e832fd34', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '48e1816b-43a7-4799-a391-c881e98c7b36', N'FMCAudit')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('3d0a8ad8-b4dc-457a-a7ee-011262aa0824', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'FPPDiscountReturn')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('fafa9741-4ae0-42c0-8efb-02942a5f7619', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '815398c1-06aa-451a-ba46-1d8e446f5e38', N'FPPInvoice2')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('c62c74c8-cb7e-423f-a329-069068d6fb73', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '395235b3-6ba7-42dd-ba56-a1df0bbc93e5', N'FPPInvoice2')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('fdbf3dea-d339-4eed-881c-07a1b47285f2', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '48e1816b-43a7-4799-a391-c881e98c7b36', N'FMBDeposit')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('e205102a-aff7-4477-ab58-08a579741593', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '18906ddd-6508-4431-89ce-374154d52695', N'FMBDeposit')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('26eeff50-e1a5-415f-aaf4-0a51942c4003', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'eada2a3f-dcee-461a-b150-de8f6a0672a5', N'FPPOrder')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('61dbb50d-f66d-44cc-bea1-0b1707c81ea2', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'd939f97a-1cde-46c6-b852-a579b0bf1337', N'FMBDeposit')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('d50de066-fb1f-499b-a447-0ccbdea382bc', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '48e1816b-43a7-4799-a391-c881e98c7b36', N'FPPInvoice2')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('8a3928d0-93a8-40e5-adac-0db0ad308205', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '48e1816b-43a7-4799-a391-c881e98c7b36', N'FMCompare')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('6110f9b2-dc82-4ce2-a859-0ee5730c7921', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'd939f97a-1cde-46c6-b852-a579b0bf1337', N'FPPOrder')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('07d4d56f-07d2-4f6f-81c3-0fb046c65321', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '1c04fbb7-958b-4aba-a9bc-a32c4a8a91d5', N'FMBCreditCard')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('3e444bf1-9f51-46e4-84c3-10a48c8e7701', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '395235b3-6ba7-42dd-ba56-a1df0bbc93e5', N'FSAQuote')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('017144f6-638c-4d2d-b119-13a083f19393', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '1c04fbb7-958b-4aba-a9bc-a32c4a8a91d5', N'FPPOrder')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('33cb5714-269b-4375-835c-14f99a2fb743', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'd939f97a-1cde-46c6-b852-a579b0bf1337', N'FPPInvoice1')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('8ed15dcf-2353-4ea9-a592-16910663a2b7', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '815398c1-06aa-451a-ba46-1d8e446f5e38', N'FMCompare')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('58e2aa1c-bb39-4016-a06c-183fa1da56ff', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '395235b3-6ba7-42dd-ba56-a1df0bbc93e5', N'FPPDiscountReturn')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('fd461899-424f-465a-8fc9-1895b8429e73', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '944b2211-d659-4a4f-adad-c2043de0f5fa', N'FMCAudit')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('5e2c725c-5d8c-4db0-b710-18ee061d2732', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '18906ddd-6508-4431-89ce-374154d52695', N'FMCPayment')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('5f519522-0cc7-47ff-a196-19dbd6647817', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '48e1816b-43a7-4799-a391-c881e98c7b36', N'FMCPayment')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('1540e4a9-e5d6-47d4-a597-1a94cbfdac21', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '7d45f8b9-de75-4397-8f8d-ce395aa4e9b4', N'FMBTellerPaper')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('f751bf81-670f-4951-acf4-1acbdaea8c1c', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'f04171ba-a4d6-48da-8970-06548efcfeb4', N'FMCAudit')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('8d6fbea3-4612-4a08-ae4d-1b2f6710415c', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '944b2211-d659-4a4f-adad-c2043de0f5fa', N'FPPGetInvoices')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('4ca1e27f-807a-4792-b09c-1b896deb9239', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '7d45f8b9-de75-4397-8f8d-ce395aa4e9b4', N'FPPGetInvoices')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('01c0c5f0-d880-463b-ba6e-1c50e0a827f2', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '18906ddd-6508-4431-89ce-374154d52695', N'FMCompare')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('d1c9ac91-207e-4f01-b03b-230ac95bd11e', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'FMBCreditCard')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('ee4001ad-2b80-4756-b100-23b7757e2c66', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'FPPDiscountReturn')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('d0310afa-b1f5-45a2-b32e-23f8fdd0a780', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '395235b3-6ba7-42dd-ba56-a1df0bbc93e5', N'FMCReceipt')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('37f0eaca-29ca-449a-ad61-243ca3a05696', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'FPPOrder')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('7f7730b7-5799-4dd6-8d6c-2440f2f16d2a', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '944b2211-d659-4a4f-adad-c2043de0f5fa', N'FPPOrder')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('f8db069f-44a5-44ae-a0fa-24e7eef55b85', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '48aa6d0a-2797-4068-9561-e876c7e9b94b', N'FPPInvoice2')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('a5dc0271-5fc5-4990-84c3-262ba18fe10c', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'FPPGetInvoices')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('b1fa9978-4728-4474-8c3c-26759024edf9', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '815398c1-06aa-451a-ba46-1d8e446f5e38', N'FSAQuote')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('6ecb5d7c-3ace-43a4-9812-2692fc32b05f', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '50777bba-c3ce-4f03-b18c-8756f790575e', N'FMCReceipt')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('1caf4c98-e07c-40d9-b355-296ddd0a514d', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '1c04fbb7-958b-4aba-a9bc-a32c4a8a91d5', N'FMCReceipt')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('afc44439-3d37-45e4-9296-29dc4c4f2743', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '18906ddd-6508-4431-89ce-374154d52695', N'FPPInvoice1')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('ed6ac81c-7dd9-4d9e-9c01-2ba26248fc11', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '18906ddd-6508-4431-89ce-374154d52695', N'FSAQuote')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('f6951f5e-eaed-4cb7-be2f-2c5cd3879da8', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '944b2211-d659-4a4f-adad-c2043de0f5fa', N'FMCReceipt')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('07133f93-7b94-44f0-a19d-2f5efdaaaa68', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'f04171ba-a4d6-48da-8970-06548efcfeb4', N'FMBCreditCard')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('84fb0e69-6d27-4975-acaf-327a37756cc4', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '48aa6d0a-2797-4068-9561-e876c7e9b94b', N'FMBCreditCard')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('a02fe402-3843-41ad-8edc-32963e9817ae', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'FPPGetInvoices')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('61bff175-353b-4de5-a396-33d02f5af78f', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'd939f97a-1cde-46c6-b852-a579b0bf1337', N'FMBTellerPaper')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('0a15aba5-c9f9-430b-9095-36e8c4f76c66', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '7d45f8b9-de75-4397-8f8d-ce395aa4e9b4', N'FMCPayment')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('73c207d8-0a32-4d22-85d4-36fd63a15c62', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '50777bba-c3ce-4f03-b18c-8756f790575e', N'FPPOrder')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('a21efd37-85ac-4ba4-85ab-37c5cb318b09', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'FPPInvoice1')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('1ecde333-3747-4051-b949-3996038bc7e0', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'FMCReceipt')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('e65367c9-fc83-4810-a564-3aa2897f7869', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '50777bba-c3ce-4f03-b18c-8756f790575e', N'FPPDiscountReturn')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('4875190e-812c-412f-b4d1-3ae5934294b2', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '395235b3-6ba7-42dd-ba56-a1df0bbc93e5', N'FPPGetInvoices')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('4b2935dd-497a-4e29-8365-3c0cb24f9213', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'eada2a3f-dcee-461a-b150-de8f6a0672a5', N'FMBCreditCard')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('2c898413-24a2-4b9a-ba2a-3cedeb623943', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '50777bba-c3ce-4f03-b18c-8756f790575e', N'FPPPayVendor')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('193150cd-d423-46b7-acf3-3e2564e4bf0f', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'FMBCreditCard')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('ceb014a6-2a1a-4aff-ae2a-3e691d5c0239', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '7d45f8b9-de75-4397-8f8d-ce395aa4e9b4', N'FMBDeposit')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('e1f5cbfa-ec93-4371-9f1e-41550526f060', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '18906ddd-6508-4431-89ce-374154d52695', N'FPPGetInvoices')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('8cd052f9-11e8-4e7b-9296-422d88172715', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '48aa6d0a-2797-4068-9561-e876c7e9b94b', N'FMCReceipt')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('86e94716-dced-4cee-bcff-437cd9247e31', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '50777bba-c3ce-4f03-b18c-8756f790575e', N'FMCAudit')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('93e493c8-cd33-463c-8990-43f44db1224b', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'FMCompare')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('e81da2fa-fc0b-488f-ab03-464f22a70b4a', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '7d45f8b9-de75-4397-8f8d-ce395aa4e9b4', N'FPPOrder')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('cea09094-ce7f-4cc3-a0df-496612d38a08', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'FPPService')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('1316f998-2e4f-4fe6-855d-4b7f1ae7195c', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'FPPOrder')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('dba204c1-9028-44a3-9bec-4bebc68afa0c', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '50777bba-c3ce-4f03-b18c-8756f790575e', N'FMCPayment')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('7b673b85-4123-4db4-97bb-4c6a498a9ee1', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'f04171ba-a4d6-48da-8970-06548efcfeb4', N'FSAQuote')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('586112bb-dd90-44b0-a705-4c888e29f3d3', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'FPPInvoice1')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('44b6d7f5-c068-4ad6-8912-4fe9ecbd42e4', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'd939f97a-1cde-46c6-b852-a579b0bf1337', N'FMCAudit')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('38b0c666-f473-4239-a68f-51461873aa9a', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '48aa6d0a-2797-4068-9561-e876c7e9b94b', N'FMBDeposit')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('f150b37d-6575-46d9-b945-5215b7f6ffe2', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'd939f97a-1cde-46c6-b852-a579b0bf1337', N'FSAQuote')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('05eddc4c-5ef9-4d7f-8b69-52303e05748e', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '1c04fbb7-958b-4aba-a9bc-a32c4a8a91d5', N'FPPGetInvoices')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('45385c88-51bb-464b-90a3-53a41bd3c0d6', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'FSAQuote')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('685493dc-af3a-4166-ae18-56dda9de5140', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'FPPInvoice1')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('847508eb-cd3b-4ec3-9b6b-579aa2e3a426', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '944b2211-d659-4a4f-adad-c2043de0f5fa', N'FMCompare')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('7129e2c8-ff9f-434f-bdd3-57c37de0c44b', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '1c04fbb7-958b-4aba-a9bc-a32c4a8a91d5', N'FMCAudit')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('410417ae-056e-47e2-bfb0-58370f78ef31', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '395235b3-6ba7-42dd-ba56-a1df0bbc93e5', N'FPPService')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('58a7920c-a90d-4bb5-bdd2-5b7f7eb0fefe', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '18906ddd-6508-4431-89ce-374154d52695', N'FPPOrder')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('90239584-6cd6-4b41-bf1d-5b9442385312', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '944b2211-d659-4a4f-adad-c2043de0f5fa', N'FMBTellerPaper')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('cd15acfc-ab59-4648-befc-5c2e9eb636b4', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'f04171ba-a4d6-48da-8970-06548efcfeb4', N'FMBTellerPaper')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('33f04029-905d-4c7e-a5ca-5d3de6daf1fa', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '48aa6d0a-2797-4068-9561-e876c7e9b94b', N'FMBTellerPaper')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('4a186082-b1f0-454a-9a08-5eeef5750558', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'FMBCreditCard')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('123c3ad8-16a4-45fe-8ffa-6016b53becf5', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'd939f97a-1cde-46c6-b852-a579b0bf1337', N'FPPService')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('afc2ca9a-b926-4140-8f43-6375eed3a1a8', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '395235b3-6ba7-42dd-ba56-a1df0bbc93e5', N'FMBInternalTransfer')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('f142c428-664e-4e34-a408-63e036eab85a', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'FMCReceipt')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('68f637f3-03de-4925-bddb-642d3c4555ac', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'FMBInternalTransfer')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('6bf09635-e10d-41b9-8aec-64669a50f48e', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'FMCAudit')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('f6827922-837c-49ac-8b2e-65dfc5f24675', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '815398c1-06aa-451a-ba46-1d8e446f5e38', N'FMBTellerPaper')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('904f272c-65a7-477e-8b0e-66686fadfd53', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '1c04fbb7-958b-4aba-a9bc-a32c4a8a91d5', N'FPPService')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('deeda304-5e72-471a-9125-66e85bf987ca', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'FPPService')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('a93353f1-4b53-4a68-9280-684416b33b71', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '50777bba-c3ce-4f03-b18c-8756f790575e', N'FMCompare')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('e04aff50-db20-4fe5-82d5-68a6fdddbbd7', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'eada2a3f-dcee-461a-b150-de8f6a0672a5', N'FPPService')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('a70bbd6a-84e6-4be8-b936-68d37b16f12a', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '50777bba-c3ce-4f03-b18c-8756f790575e', N'FMBInternalTransfer')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('bf7c3115-22c4-41a8-b8b0-69fbfe563718', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '815398c1-06aa-451a-ba46-1d8e446f5e38', N'FPPOrder')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('fc6e4a63-f4c2-4881-a5a8-6b477ab1922b', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '48aa6d0a-2797-4068-9561-e876c7e9b94b', N'FPPInvoice1')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('b407a563-2a9d-494f-b49d-6b58b29966e5', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'd939f97a-1cde-46c6-b852-a579b0bf1337', N'FMBInternalTransfer')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('29443849-0e60-40b1-aae9-6bb079c769bb', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'eada2a3f-dcee-461a-b150-de8f6a0672a5', N'FMBTellerPaper')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('1de1704f-b1b8-4ba6-a864-6d53917e7c7e', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '50777bba-c3ce-4f03-b18c-8756f790575e', N'FMBTellerPaper')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('d2ee1307-1bb8-4083-b742-6f6a5487ca42', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '395235b3-6ba7-42dd-ba56-a1df0bbc93e5', N'FMBDeposit')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('d2d335e6-d4cc-48c2-89d5-702c253c1aa6', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '395235b3-6ba7-42dd-ba56-a1df0bbc93e5', N'FMCompare')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('79ea06c2-04ea-4ff0-a7d8-71a103cc3ad2', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '1c04fbb7-958b-4aba-a9bc-a32c4a8a91d5', N'FMCompare')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('5cf89b2d-7b3d-46cf-a932-72875414b3be', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'FMBInternalTransfer')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('d6a604b4-f4dc-4009-96fc-73437da3cd0b', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'FMCPayment')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('8fa14ab6-9164-4f7b-a17c-73f5ccbf5438', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '50777bba-c3ce-4f03-b18c-8756f790575e', N'FPPService')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('a5a6e5c5-e643-4729-b60b-763466ec4660', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '7d45f8b9-de75-4397-8f8d-ce395aa4e9b4', N'FMBCreditCard')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('d3337b62-60bb-4ead-80a0-76a02dee7434', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '1c04fbb7-958b-4aba-a9bc-a32c4a8a91d5', N'FMBInternalTransfer')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('b7cc9ae4-0775-434f-b522-77f7a036c2fc', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'FMCompare')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('f5a1a95e-d532-4241-b1de-78808261df81', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '1c04fbb7-958b-4aba-a9bc-a32c4a8a91d5', N'FPPInvoice1')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('203d139c-0a7e-421a-8dc8-78ff7928045f', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '944b2211-d659-4a4f-adad-c2043de0f5fa', N'FMCPayment')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('f9a48ccd-717d-4f89-a844-7d0fc5eb7799', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'eada2a3f-dcee-461a-b150-de8f6a0672a5', N'FMCompare')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('0d334350-4723-4e77-b9dd-7dc1603b5409', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '815398c1-06aa-451a-ba46-1d8e446f5e38', N'FMBInternalTransfer')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('3e83fee7-0c36-4333-acbb-7dc5f964ef89', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'FPPInvoice2')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('a61fbf94-cb16-4954-b9fd-7f1ea50e8785', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'd939f97a-1cde-46c6-b852-a579b0bf1337', N'FMCPayment')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('d1a767e8-2c1e-425f-8d85-804497e74a65', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '18906ddd-6508-4431-89ce-374154d52695', N'FMCReceipt')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('3d4a0fd0-7af0-4645-9c21-83a18e372ec9', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '944b2211-d659-4a4f-adad-c2043de0f5fa', N'FMBDeposit')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('e6c666c7-6b35-4d98-8cbc-8424702c1510', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'd939f97a-1cde-46c6-b852-a579b0bf1337', N'FMCompare')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('268306ed-a790-40df-9f82-85665a9bc0f1', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'FMCPayment')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('ad592802-c91f-4754-829a-8752e914791b', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '48e1816b-43a7-4799-a391-c881e98c7b36', N'FMBCreditCard')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('c1c7b478-f178-46a2-8965-87d304095e0f', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '48e1816b-43a7-4799-a391-c881e98c7b36', N'FPPOrder')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('5abe38ba-bcf4-4ae6-a070-884acb2357c1', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '48e1816b-43a7-4799-a391-c881e98c7b36', N'FPPService')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('1d1a150e-1d58-46f7-8da8-897d828d1ced', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'FSAQuote')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('4878e56c-3c37-4435-9a53-8a547793ff24', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'f04171ba-a4d6-48da-8970-06548efcfeb4', N'FMCPayment')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('b142e410-70d7-4659-a186-8ea314a8187c', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '1c04fbb7-958b-4aba-a9bc-a32c4a8a91d5', N'FPPInvoice2')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('43c3a320-c498-4f22-8aa0-8f9cac021916', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'd939f97a-1cde-46c6-b852-a579b0bf1337', N'FMCReceipt')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('e176508d-67b1-43a7-961c-927077a1c283', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '395235b3-6ba7-42dd-ba56-a1df0bbc93e5', N'FPPInvoice1')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('4f0d3787-1d36-4205-8da3-92b11514333f', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'd939f97a-1cde-46c6-b852-a579b0bf1337', N'FPPGetInvoices')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('24ef6ed6-a0ae-4026-9554-94ba32c9101f', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'FMCReceipt')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('c2ca27db-19f5-44e7-8c78-976fccf20a89', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '815398c1-06aa-451a-ba46-1d8e446f5e38', N'FMCAudit')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('e5da19db-6839-4e17-8832-9826cdab75b6', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'FMCAudit')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('5f8a2652-4078-4339-9551-9a870a5d8cb2', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'FPPDiscountReturn')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('40dcc96b-c7f3-4f74-964a-9cdc88c61752', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'f04171ba-a4d6-48da-8970-06548efcfeb4', N'FPPGetInvoices')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('42ed494d-f491-4e16-8316-9fc92409f1b7', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'f04171ba-a4d6-48da-8970-06548efcfeb4', N'FMCReceipt')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('1c769553-02ff-460a-84fb-a144c4f13c9f', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '48e1816b-43a7-4799-a391-c881e98c7b36', N'FSAQuote')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('edca1c5b-9a20-40f8-95f1-a187da4659b1', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'eada2a3f-dcee-461a-b150-de8f6a0672a5', N'FPPInvoice2')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('61e5f0f1-8e4a-4509-a398-a33a37e3a35f', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '395235b3-6ba7-42dd-ba56-a1df0bbc93e5', N'FPPOrder')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('0ed18278-feb0-46cc-b0ce-a52bd537532c', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '18906ddd-6508-4431-89ce-374154d52695', N'FMBTellerPaper')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('e46ee4ca-bf54-4659-bcbe-a660bfb4c87e', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '48aa6d0a-2797-4068-9561-e876c7e9b94b', N'FMCAudit')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('f5a1825a-9135-43f7-9c7a-a6c2124698c0', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '395235b3-6ba7-42dd-ba56-a1df0bbc93e5', N'FMCAudit')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('8d7673b6-f502-4c2d-8788-a7d6848c2799', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '395235b3-6ba7-42dd-ba56-a1df0bbc93e5', N'FMBTellerPaper')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('463c43e6-3a8f-4acc-b4dc-a7efaf9b404d', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '50777bba-c3ce-4f03-b18c-8756f790575e', N'FPPGetInvoices')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('fa48db90-6dc3-4992-b7e3-a83f62b6b1ae', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '815398c1-06aa-451a-ba46-1d8e446f5e38', N'FPPInvoice1')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('6467904c-cafe-4f0a-b00c-a865acd33d8e', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '944b2211-d659-4a4f-adad-c2043de0f5fa', N'FSAQuote')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('48d7bf29-6a4c-4814-b3b0-a8c3a38bd59a', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'FPPGetInvoices')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('3f03c624-e920-4ee1-807d-a94a00818f6a', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'FMCAudit')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('ef0081b9-aa98-4643-b75f-a9bc78c28314', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '48e1816b-43a7-4799-a391-c881e98c7b36', N'FPPInvoice1')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('51c7099a-88c0-4b12-9681-aae9384b42b4', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '1c04fbb7-958b-4aba-a9bc-a32c4a8a91d5', N'FMBDeposit')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('e7d54a36-2ca9-4cb3-8023-aba3fd13b8ee', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'f04171ba-a4d6-48da-8970-06548efcfeb4', N'FMBInternalTransfer')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('08a0d685-c276-4934-91f5-ac67bab5aa0c', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'eada2a3f-dcee-461a-b150-de8f6a0672a5', N'FMCPayment')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('81c11663-f0dc-4df8-8189-ae26cbbb10c5', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'eada2a3f-dcee-461a-b150-de8f6a0672a5', N'FPPInvoice1')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('ae14529d-19e6-49e6-ad33-aea4ac4f3a7a', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '7d45f8b9-de75-4397-8f8d-ce395aa4e9b4', N'FMCompare')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('cd894205-69d2-4d26-8836-b10860f9046f', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '815398c1-06aa-451a-ba46-1d8e446f5e38', N'FPPService')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('f61acf2d-5f93-4e1a-ab87-b19db99e8571', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '48aa6d0a-2797-4068-9561-e876c7e9b94b', N'FMCPayment')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('cd998041-f5f5-489d-b11f-b1aa1559c524', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '50777bba-c3ce-4f03-b18c-8756f790575e', N'FPPInvoice1')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('9e80d99c-de17-4fe9-93cd-b1e057444470', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '944b2211-d659-4a4f-adad-c2043de0f5fa', N'FPPService')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('734c0b39-73a6-48df-b92b-b2f3267789f4', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '50777bba-c3ce-4f03-b18c-8756f790575e', N'FMBDeposit')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('7ccc0a7f-da60-48d3-b64f-b47719823d49', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '48aa6d0a-2797-4068-9561-e876c7e9b94b', N'FMBInternalTransfer')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('7c82645e-107d-44e1-9c1e-b4d9545197ab', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '815398c1-06aa-451a-ba46-1d8e446f5e38', N'FMBCreditCard')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('a7bdeb86-78d1-4222-9cbc-b553d95b28cd', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'FMCPayment')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('055df576-6fa4-4ad2-aaa9-b576e7023394', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '50777bba-c3ce-4f03-b18c-8756f790575e', N'FPPInvoice2')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('188f44ad-0484-4e0b-8479-b657ac6261da', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '7d45f8b9-de75-4397-8f8d-ce395aa4e9b4', N'FMBInternalTransfer')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('4fa9d946-db64-4930-9a70-b7bfb75437d5', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '18906ddd-6508-4431-89ce-374154d52695', N'FMBCreditCard')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('7e5ee37d-19a1-4acd-9782-b7ec7ab6ff0b', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '48aa6d0a-2797-4068-9561-e876c7e9b94b', N'FPPGetInvoices')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('017565f7-d470-4b3b-9363-b8f1b7c1b09f', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'f04171ba-a4d6-48da-8970-06548efcfeb4', N'FPPInvoice1')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('0725ed52-d047-445c-ac50-b92f8e1c5423', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'FMCompare')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('74623a29-0f96-4f03-80af-bb77a7cefc4e', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '815398c1-06aa-451a-ba46-1d8e446f5e38', N'FMCPayment')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('27c50590-e2a3-4453-998c-be208a4b9dce', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'eada2a3f-dcee-461a-b150-de8f6a0672a5', N'FMCReceipt')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('1b7f9f64-0ba2-4cc8-b9c9-bf254d5d8743', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '18906ddd-6508-4431-89ce-374154d52695', N'FMCAudit')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('c84b9396-983d-4463-b021-bf8a33b7098b', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '395235b3-6ba7-42dd-ba56-a1df0bbc93e5', N'FMBCreditCard')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('3a8b40a7-a27f-4d3f-8e8a-c05f5481a960', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '815398c1-06aa-451a-ba46-1d8e446f5e38', N'FMCReceipt')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('7b3cab5e-a8bb-4a31-b837-c0b7264d85bc', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'FMBTellerPaper')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('8203d569-4ee0-41f9-89de-c1125f12b251', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '48aa6d0a-2797-4068-9561-e876c7e9b94b', N'FMCompare')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('a1b87db7-4318-4227-986c-c52252d4fd0c', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'FMBInternalTransfer')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('0fc671ab-ba3b-4366-899a-c636afb19878', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '48e1816b-43a7-4799-a391-c881e98c7b36', N'FMBInternalTransfer')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('2e985dce-ba0e-424c-b168-c6a3ae01ba38', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '18906ddd-6508-4431-89ce-374154d52695', N'FPPService')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('82a1dd16-f46a-4df9-a646-c9eb6269270c', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '50777bba-c3ce-4f03-b18c-8756f790575e', N'FMBCreditCard')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('cca0c6ef-49e9-4127-9cd4-ca1857f91015', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'eada2a3f-dcee-461a-b150-de8f6a0672a5', N'FPPGetInvoices')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('f0a7e76c-bb9d-4757-b92a-cb388a7751b4', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '7d45f8b9-de75-4397-8f8d-ce395aa4e9b4', N'FMCReceipt')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('91f80d1b-3501-46fc-9a95-cb620bd7a763', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'eada2a3f-dcee-461a-b150-de8f6a0672a5', N'FMBInternalTransfer')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('c234d82e-9577-410e-ae11-ce9fc13ea86e', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '1c04fbb7-958b-4aba-a9bc-a32c4a8a91d5', N'FSAQuote')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('97373bd9-6bc2-4de9-8e8c-cef0fc96678f', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'FPPPayVendor')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('a63ced1f-d774-41d1-871b-d0fe3d31b3fc', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '48e1816b-43a7-4799-a391-c881e98c7b36', N'FMBTellerPaper')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('03777835-3f50-4669-9750-d1d80d51a5a3', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '395235b3-6ba7-42dd-ba56-a1df0bbc93e5', N'FMCPayment')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('fbecf328-2096-4666-972f-d302f4068d29', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '48aa6d0a-2797-4068-9561-e876c7e9b94b', N'FPPService')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('97b4d2d9-7296-41d9-bea7-d5d19027c2da', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '1c04fbb7-958b-4aba-a9bc-a32c4a8a91d5', N'FMBTellerPaper')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('97bba30e-d4b6-4bad-9062-d66c3f3e3fa4', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '7d45f8b9-de75-4397-8f8d-ce395aa4e9b4', N'FPPInvoice1')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('954c05f1-2b02-41a8-8529-d6b8564d4c10', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'FPPInvoice2')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('cf47d1f9-e3df-4dd2-a892-d90371473a17', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'FMBDeposit')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('2ee1106f-3740-451e-ba86-d9186d12e395', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'eada2a3f-dcee-461a-b150-de8f6a0672a5', N'FSAQuote')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('026376c4-bbb1-4790-9c85-da5b8b8eaac1', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'f04171ba-a4d6-48da-8970-06548efcfeb4', N'FPPOrder')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('48b11d14-46f7-4152-b098-db75fa401840', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'FMBDeposit')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('45b4155d-be7b-4f68-928b-e09c048b62f8', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'FSAQuote')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('11af44af-45a4-4531-a931-e0d1ded52881', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '50777bba-c3ce-4f03-b18c-8756f790575e', N'FSAQuote')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('14d42a7b-4a29-40a1-83dc-e14adfd51b90', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'd939f97a-1cde-46c6-b852-a579b0bf1337', N'FMBCreditCard')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('1be5ebc4-be64-4889-a446-e1c42741c772', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'FMBTellerPaper')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('98b3b559-5c59-41b6-bdca-e23dca5d50ae', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'f04171ba-a4d6-48da-8970-06548efcfeb4', N'FPPService')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('7fdbf8e7-bdb5-42ce-9032-e243d3202445', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '944b2211-d659-4a4f-adad-c2043de0f5fa', N'FMBCreditCard')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('d2583dbb-ce5f-44cd-9b10-e29213249394', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '944b2211-d659-4a4f-adad-c2043de0f5fa', N'FPPInvoice2')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('9272d1dd-9d1e-4f89-9014-e2fdd4d1f584', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '7d45f8b9-de75-4397-8f8d-ce395aa4e9b4', N'FPPInvoice2')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('a9d50ced-7dc5-45e5-8662-e483dcc26ee6', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'f04171ba-a4d6-48da-8970-06548efcfeb4', N'FMCompare')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('e3e317e3-89cc-4b4e-a8ec-e54e04f8ddf0', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '48aa6d0a-2797-4068-9561-e876c7e9b94b', N'FSAQuote')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('4229ed5a-6981-4a05-bdcd-e59d9a55abab', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'eada2a3f-dcee-461a-b150-de8f6a0672a5', N'FMBDeposit')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('3feca38a-a065-4a22-beff-e7166380b819', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'FPPInvoice2')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('e93b8d3b-00fb-4911-a0b7-e831a28601cb', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '48aa6d0a-2797-4068-9561-e876c7e9b94b', N'FPPOrder')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('f2d2e81a-3baa-418d-be39-ea5bc5309c24', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '7d45f8b9-de75-4397-8f8d-ce395aa4e9b4', N'FSAQuote')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('3bade2a8-91d9-41be-8048-eb254054f5f5', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'f04171ba-a4d6-48da-8970-06548efcfeb4', N'FMBDeposit')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('e5de79da-d8d9-4ab3-bac3-ed255c4a4fed', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '1c04fbb7-958b-4aba-a9bc-a32c4a8a91d5', N'FMCPayment')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('8aa111a1-012d-44ab-b4bb-f085beab34cc', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'FPPService')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('2f91b2b5-aa42-4e02-8516-f151564deaa8', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '815398c1-06aa-451a-ba46-1d8e446f5e38', N'FMBDeposit')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('b30f6971-51fd-4149-be43-f27836df7293', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '7d45f8b9-de75-4397-8f8d-ce395aa4e9b4', N'FMCAudit')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('c59e5fb4-5b76-4681-9de7-f30ce89d8418', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'FPPOrder')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('34f19594-f511-40b8-84a6-f32272eddd85', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '18906ddd-6508-4431-89ce-374154d52695', N'FMBInternalTransfer')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('2e386002-5e79-450f-98f4-f5504ee2cd8f', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '7d45f8b9-de75-4397-8f8d-ce395aa4e9b4', N'FPPService')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('0c1abe54-d03b-4a79-9545-f62c1b42f958', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'FMBDeposit')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('f39c1e0d-4063-4609-a968-f678fd9572c3', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '48e1816b-43a7-4799-a391-c881e98c7b36', N'FMCReceipt')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('1957b7b7-ac54-4543-80eb-f73cb306cc33', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '944b2211-d659-4a4f-adad-c2043de0f5fa', N'FPPInvoice1')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('e934b1f9-80c2-4f7c-a16d-f77a2aa01670', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'eada2a3f-dcee-461a-b150-de8f6a0672a5', N'FMCAudit')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('13018c7d-cc4b-4589-8367-f798360b15b3', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'f04171ba-a4d6-48da-8970-06548efcfeb4', N'FPPInvoice2')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('d456ea70-c48c-40c4-a69f-f9d0d7da4c6d', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '18906ddd-6508-4431-89ce-374154d52695', N'FPPInvoice2')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('b3908a41-6d04-40d5-a4ea-f9feeff4c5b9', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'FMBTellerPaper')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('c714cd21-84b2-42b4-b162-fac4c1ea7a7c', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '48e1816b-43a7-4799-a391-c881e98c7b36', N'FPPGetInvoices')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('95029c4e-0ce3-4c23-9309-fe9adf04270e', 'cce2e4c9-935f-48eb-9ce4-595657b727de', '815398c1-06aa-451a-ba46-1d8e446f5e38', N'FPPGetInvoices')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('53b6aaa5-209c-4d67-89eb-ffb140a8f3ac', 'cce2e4c9-935f-48eb-9ce4-595657b727de', 'd939f97a-1cde-46c6-b852-a579b0bf1337', N'FPPInvoice2')
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DELETE dbo.Sys_UserRole WHERE ID = 'b4f5b3e5-123a-4233-8333-3408a64f0e62'

INSERT dbo.Sys_UserRole(ID, UserID, RoleID, BranchID) VALUES ('53aea629-7eef-49b5-aff0-addbd3593eef', '5c2f6d39-e0c4-4d3e-8ab1-e00d51867ccc', 'b7e767cb-2731-434c-a513-61ed7497db6f', '00000000-0000-0000-0000-000000000000')
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.SystemOption SET Data = N'31/07/2018', DefaultData = N'31/07/2018' WHERE ID = 103
UPDATE dbo.SystemOption SET Data = N'31/12/2017', DefaultData = N'31/12/2017' WHERE ID = 106
UPDATE dbo.SystemOption SET Data = N'31/12/2017', DefaultData = N'31/12/2017' WHERE ID = 107
UPDATE dbo.SystemOption SET Data = N'18.7.31.13', DefaultData = N'18.7.31.13' WHERE ID = 111
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

SET IDENTITY_INSERT dbo.Template ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
INSERT dbo.Template(ID, TemplateName, TypeID, IsDefault, IsActive, IsSecurity, OrderPriority) VALUES ('aef47175-d71c-4532-9fa1-4f04e9c4e696', N'Mẫu ngầm định', 180, 1, 0, 1, 0)
INSERT dbo.Template(ID, TemplateName, TypeID, IsDefault, IsActive, IsSecurity, OrderPriority) VALUES ('18ad3e30-1f72-48b2-9202-a95fcc572a12', N'Mẫu chuẩn', 180, 0, 1, 1, 1)
GO
SET IDENTITY_INSERT dbo.Template OFF
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DELETE dbo.TemplateColumn WHERE ID = '7524d212-1324-40aa-992b-40492537eae2'

UPDATE dbo.TemplateColumn SET ColumnName = N'InvoiceTemplate', VisiblePosition = 20 WHERE ID = 'd1b7a4ed-4234-43b3-b92d-003d7ca8a0c8'
UPDATE dbo.TemplateColumn SET VisiblePosition = 16 WHERE ID = '139e47d2-3df1-4841-b9ac-04018f334e81'
UPDATE dbo.TemplateColumn SET VisiblePosition = 10 WHERE ID = '0dc1209d-1490-4c95-9a67-042e8065228d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 38 WHERE ID = '3b79919f-2e43-468e-b3c8-046cd30b17c9'
UPDATE dbo.TemplateColumn SET VisiblePosition = 7 WHERE ID = 'fe6c25cb-6d8d-4608-bd27-049d4b1d8bb1'
UPDATE dbo.TemplateColumn SET VisiblePosition = 59 WHERE ID = '50d28322-9524-44fa-8c25-04c387097f18'
UPDATE dbo.TemplateColumn SET ColumnName = N'InvoiceTemplate', ColumnCaption = N'Mấu số hóa đơn', VisiblePosition = 3 WHERE ID = '46afa327-2ccd-4297-9469-0524f4789324'
UPDATE dbo.TemplateColumn SET VisiblePosition = 18 WHERE ID = '8e7a294c-7ed3-4e17-8074-05563e40bcb3'
UPDATE dbo.TemplateColumn SET IsVisible = 0, VisiblePosition = 28 WHERE ID = '4418a516-4b69-4dce-bef8-05e7781945df'
UPDATE dbo.TemplateColumn SET VisiblePosition = 8 WHERE ID = 'dd6c6692-403d-4db8-88ce-0640cf0107e3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '994685b2-4659-4e55-a4dd-06c83ca94901'
UPDATE dbo.TemplateColumn SET VisiblePosition = 21 WHERE ID = '92761a9f-c59f-49cb-82bb-07638b500229'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '49aa6e6f-e6f7-4e6a-99cb-07724dd9e461'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f52b6b69-5767-411a-a2fd-07a6ad20d842'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3f585bcf-a3e9-4ba3-85db-08b066655388'
UPDATE dbo.TemplateColumn SET VisiblePosition = 9 WHERE ID = 'f2e4e997-4971-46c2-9476-0a946bfc4318'
UPDATE dbo.TemplateColumn SET VisiblePosition = 5 WHERE ID = 'aebeb0ac-5c66-4e09-a14a-0b7aec8e1ea4'
UPDATE dbo.TemplateColumn SET VisiblePosition = 16 WHERE ID = '4d1db915-c1a5-412f-92a5-0bdb83e81ada'
UPDATE dbo.TemplateColumn SET VisiblePosition = 23 WHERE ID = 'be8ea4a2-295d-4882-9613-0bf345201ab3'
UPDATE dbo.TemplateColumn SET VisiblePosition = 75 WHERE ID = 'd699801f-11d6-4165-82f3-0c2bec37c277'
UPDATE dbo.TemplateColumn SET VisiblePosition = 2 WHERE ID = 'fe79ec2b-c2d2-48ee-a002-0d543c6ecf41'
UPDATE dbo.TemplateColumn SET VisiblePosition = 75 WHERE ID = 'ce423932-65b2-4580-844f-0d71311c21fe'
UPDATE dbo.TemplateColumn SET VisiblePosition = 26 WHERE ID = 'f77cc33a-8de9-4503-ae71-0f2682ab2a8c'
UPDATE dbo.TemplateColumn SET VisiblePosition = 26 WHERE ID = '7cb93597-55d9-4bdb-aa0d-0f7e6117f454'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4eed24e0-3ca8-41d5-b1a3-0f94ade66f10'
UPDATE dbo.TemplateColumn SET VisiblePosition = 8 WHERE ID = 'b0c0591c-dc66-4b24-99d4-10597ca7f2e1'
UPDATE dbo.TemplateColumn SET VisiblePosition = 20 WHERE ID = '7eabbdb3-2cfa-4a0f-ad1b-10ba829508ba'
UPDATE dbo.TemplateColumn SET VisiblePosition = 61 WHERE ID = 'bdc3b731-c6b6-4b1c-8c67-12e62e6047d7'
UPDATE dbo.TemplateColumn SET ColumnMaxWidth = 0, ColumnMinWidth = 0 WHERE ID = 'e37410d3-afd1-473b-b534-1344285ebafd'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b6962615-a53b-4dbb-8166-1419797f5019'
UPDATE dbo.TemplateColumn SET VisiblePosition = 21 WHERE ID = '1c4e761d-734e-47b2-bded-15d50a03bfbe'
UPDATE dbo.TemplateColumn SET VisiblePosition = 8 WHERE ID = 'f87622ae-de5c-4d22-9471-163578e1f5d6'
UPDATE dbo.TemplateColumn SET VisiblePosition = 5 WHERE ID = '17541ee7-8693-484c-bc74-1688be9a3838'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '9713e028-2458-40fb-adc4-1771fb1db677'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '10c64246-9da3-4114-b895-17af5ae50085'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Ðơn giá ngày công', VisiblePosition = 8 WHERE ID = '4d018c67-b2d7-4431-a2c5-1851bfabd241'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1e45aab7-c0c0-4991-a6d0-192b14fa609e'
UPDATE dbo.TemplateColumn SET ColumnMaxWidth = 0, ColumnMinWidth = 0 WHERE ID = '1eb5d9b1-50b0-4766-8829-193892f46ffd'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '14837c17-20c3-478e-b61a-1a0b0eeb90b2'
UPDATE dbo.TemplateColumn SET VisiblePosition = 75 WHERE ID = '8f64da40-eb98-4592-88be-1a9f60372551'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '5098283f-187a-416e-be8b-1adfe7cf7f65'
UPDATE dbo.TemplateColumn SET IsVisible = 0, VisiblePosition = 28 WHERE ID = '2cd6bfa0-ab53-48b9-8ac9-1ae15d8c1860'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'fff522e4-b16f-4f5e-9c47-1b0fcd5bece7'
UPDATE dbo.TemplateColumn SET VisiblePosition = 19 WHERE ID = 'b6ac3a74-ba68-4c19-8059-1c687299b9db'
UPDATE dbo.TemplateColumn SET VisiblePosition = 42 WHERE ID = 'a2337cdb-9cc4-43e2-914f-1cc66b3668e7'
UPDATE dbo.TemplateColumn SET VisiblePosition = 21 WHERE ID = 'f7e4247d-2f12-49d2-8101-1cdceb413ec1'
UPDATE dbo.TemplateColumn SET VisiblePosition = 24 WHERE ID = 'c8d414a7-5541-4659-ae3a-1d35f832ce3d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'faed522c-de01-4eb5-a39f-1d5448f739cc'
UPDATE dbo.TemplateColumn SET VisiblePosition = 28 WHERE ID = 'dc5b3231-0f54-4691-9777-1e6017c9604a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1, VisiblePosition = 3 WHERE ID = 'f9c8a8d9-582d-44ff-a40c-1ed5f2a08ccd'
UPDATE dbo.TemplateColumn SET VisiblePosition = 5 WHERE ID = 'ee789c46-86ef-4591-89e7-1f29303ea6e3'
UPDATE dbo.TemplateColumn SET VisiblePosition = 4 WHERE ID = '478332e8-615b-479d-90da-201bbf873077'
UPDATE dbo.TemplateColumn SET VisiblePosition = 12 WHERE ID = 'b9eee71a-8e29-42fd-a460-20506331d0f2'
UPDATE dbo.TemplateColumn SET VisiblePosition = 4 WHERE ID = '7f705a90-e2aa-4f00-9bbc-20a91ac9475b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '0512f015-e109-405d-aa52-20bda8e1c80b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 29 WHERE ID = 'e1a7c9a0-fec7-49f5-9363-213603a88b60'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Loại hóa đơn', VisiblePosition = 2 WHERE ID = '59d07dc3-b307-4ede-b606-22cddb2b6986'
UPDATE dbo.TemplateColumn SET VisiblePosition = 17 WHERE ID = '58af84c8-aa3e-40fb-8b5c-22e534c3a882'
UPDATE dbo.TemplateColumn SET ColumnName = N'InvoiceTemplate', VisiblePosition = 58 WHERE ID = '697ebc1a-9dea-461a-b9a5-233274c20a75'
UPDATE dbo.TemplateColumn SET VisiblePosition = 38 WHERE ID = 'b5d52d8d-aa0f-40fa-8030-23fbf65d3523'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'f4f8c63d-d248-4fb5-8f7f-247599d611ef'
UPDATE dbo.TemplateColumn SET VisiblePosition = 39 WHERE ID = '2324ce62-8737-4d23-ad76-25d57e6cfc1b'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Loại hóa đơn', VisiblePosition = 1 WHERE ID = 'c827f824-1cb4-43fc-b30a-26a69aac6832'
UPDATE dbo.TemplateColumn SET VisiblePosition = 33 WHERE ID = '7800a513-ef4d-411c-9ca2-2708c5c46edf'
UPDATE dbo.TemplateColumn SET VisiblePosition = 36 WHERE ID = '7c40ff51-71bd-4088-b5d1-273428b1a544'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'efebdc60-151f-4f4f-a9ea-2740f53fdde9'
UPDATE dbo.TemplateColumn SET VisiblePosition = 6 WHERE ID = 'd1887f6e-be12-42cd-a4ec-28ce6f48d92c'
UPDATE dbo.TemplateColumn SET VisiblePosition = 31 WHERE ID = '672ede71-3b0a-48df-815e-2a1a374e0562'
UPDATE dbo.TemplateColumn SET VisiblePosition = 14 WHERE ID = 'c900564d-e5eb-4adf-a9e8-2bed8fa4a5f6'
UPDATE dbo.TemplateColumn SET VisiblePosition = 14 WHERE ID = 'b7e52dcd-b929-4e85-be61-2c4d11378102'
UPDATE dbo.TemplateColumn SET VisiblePosition = 35 WHERE ID = 'be1fd93a-1e04-47e4-b184-2c6618ab135d'
UPDATE dbo.TemplateColumn SET ColumnName = N'TotalAllOriginal' WHERE ID = '76526f9b-d4e8-4a0e-a4c9-2c98d941e7c3'
UPDATE dbo.TemplateColumn SET VisiblePosition = 6 WHERE ID = 'df917a92-c507-4213-815c-2ced85ae2751'
UPDATE dbo.TemplateColumn SET VisiblePosition = 19 WHERE ID = 'd82761ca-e628-4b12-ae04-2dde74aeb43c'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Số giờ không hưởng 100% lương', VisiblePosition = 11 WHERE ID = 'f2fa8caa-78a6-41e6-bdcd-2f188727ef9f'
UPDATE dbo.TemplateColumn SET VisiblePosition = 25 WHERE ID = '4c4aa19e-c8d3-4502-a4b4-3098d40fda3b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 37 WHERE ID = '905da080-e306-4a2b-8d7e-30d524fe68f5'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'e5808f1b-33c9-4736-836e-31311e374449'
UPDATE dbo.TemplateColumn SET VisiblePosition = 9 WHERE ID = '755d5ecc-4187-4156-96ae-314c25879e94'
UPDATE dbo.TemplateColumn SET VisiblePosition = 10 WHERE ID = '9d3bdf5e-997e-4afa-af88-32f97e2b1c07'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '0d7ad216-c0e3-4dd7-9875-352e555abf9f'
UPDATE dbo.TemplateColumn SET VisiblePosition = 8 WHERE ID = '8973ac1b-3975-4249-acf4-36e61c43d50f'
UPDATE dbo.TemplateColumn SET VisiblePosition = 37 WHERE ID = 'bdcafb2f-6441-4707-87c5-37f7493a2ecf'
UPDATE dbo.TemplateColumn SET ColumnName = N'InvoiceTemplate', ColumnCaption = N'Mấu số hóa đơn' WHERE ID = '71cbfea8-8b2f-40fe-b843-393a6e745562'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'cae6f629-e4d7-4e0a-b32e-3bb9276444ac'
UPDATE dbo.TemplateColumn SET VisiblePosition = 35 WHERE ID = '562b4802-daf5-4d11-9c8d-3ca413833c97'
UPDATE dbo.TemplateColumn SET VisiblePosition = 77 WHERE ID = 'c7756bea-d797-4802-a597-3df2fe4cdfef'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'eb20d50c-6502-4496-9968-3e4389275c0b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 75 WHERE ID = '32947309-64fa-4353-8a25-3f0497ca20d2'
UPDATE dbo.TemplateColumn SET VisiblePosition = 28 WHERE ID = '5913f77d-2764-4bd5-b2bc-3f18a342bc99'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '8cc30888-9c02-4ca1-8e67-3fb1d822846d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7132b0ea-5cde-4663-ade9-3fe5873dfce0'
UPDATE dbo.TemplateColumn SET VisiblePosition = 20 WHERE ID = 'f32a711a-21d8-4fda-8069-3fe6a24daff6'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Diễn giải', ColumnWidth = 120 WHERE ID = '8857278e-4ccc-4ea3-8b4a-408ad6dcf852'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'a80b816e-4dad-4a95-b761-4212f04307fc'
UPDATE dbo.TemplateColumn SET VisiblePosition = 1 WHERE ID = '7e6d57c8-8e99-423c-8ecd-436a08c30286'
UPDATE dbo.TemplateColumn SET VisiblePosition = 11 WHERE ID = '70074954-88ff-43d3-a2d2-437b2e8bf4df'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1, VisiblePosition = 3 WHERE ID = 'd0ddc723-1f54-4b7a-ac99-43a7fe8a15c1'
UPDATE dbo.TemplateColumn SET VisiblePosition = 61 WHERE ID = '9df6a75d-2094-4d73-8093-4575563df510'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'a715c3f0-e422-42df-9bfd-45c0331e14d5'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '92541174-e690-4072-a087-46070c5ba747'
UPDATE dbo.TemplateColumn SET VisiblePosition = 29 WHERE ID = '9696b28b-2dc4-4d83-a187-460cf0b35836'
UPDATE dbo.TemplateColumn SET VisiblePosition = 0 WHERE ID = 'c1c84b0f-f487-4bdc-a05d-462ebce2c908'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '28a4662b-8e02-4517-a6c1-46f1a64cfdb2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '18081c9c-459a-490a-b64c-47f76a8eba09'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '431866d4-166a-44fb-91db-48a399cbee7b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'fb2a3195-0c24-45ec-8b6a-4d2a423d80d0'
UPDATE dbo.TemplateColumn SET VisiblePosition = 75 WHERE ID = '001fa941-29ee-45a9-9421-4d8aab191490'
UPDATE dbo.TemplateColumn SET VisiblePosition = 11 WHERE ID = '2d02a858-62b9-44bd-a021-4e1c07594b7d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 13 WHERE ID = 'd5737345-8d36-46fc-8af8-50e32cb47e99'
UPDATE dbo.TemplateColumn SET VisiblePosition = 10 WHERE ID = '7099282f-1079-45e2-9224-51755f1ce7d2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '2366e28b-e971-4544-98a1-524d18ea9e55'
UPDATE dbo.TemplateColumn SET VisiblePosition = 10 WHERE ID = '8a1e82f2-c22a-4608-a07d-5277224b50ca'
UPDATE dbo.TemplateColumn SET VisiblePosition = 6 WHERE ID = 'b890c360-ad5f-4bc8-819a-54017afb8886'
UPDATE dbo.TemplateColumn SET VisiblePosition = 22 WHERE ID = 'b2487f36-3b79-4f37-a7ef-5455bf1dedd6'
UPDATE dbo.TemplateColumn SET ColumnName = N'NetAmountOriginal', VisiblePosition = 32 WHERE ID = '7abec48f-9876-472b-ad00-5488963f326d'
UPDATE dbo.TemplateColumn SET ColumnName = N'TotalAll' WHERE ID = '39fc14ca-2585-49a9-b409-548f43cbe5f2'
UPDATE dbo.TemplateColumn SET VisiblePosition = 12 WHERE ID = 'c32cb10a-3302-4c64-9e39-5523bd3139f5'
UPDATE dbo.TemplateColumn SET VisiblePosition = 23 WHERE ID = '61271e1d-2e9e-4244-8b61-558269bb9053'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '52272e07-5f46-476a-a7a0-558becde9b34'
UPDATE dbo.TemplateColumn SET IsVisible = 0, VisiblePosition = 28 WHERE ID = '00eb4029-2a8f-430d-a3e3-560698ab014d'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tổng thu nhập chịu thuế TNCN', VisiblePosition = 27 WHERE ID = 'da02973a-b1fa-4e7f-82aa-573196806d90'
UPDATE dbo.TemplateColumn SET VisiblePosition = 15 WHERE ID = 'ae0fdecf-4e35-483c-a256-578210e58c49'
UPDATE dbo.TemplateColumn SET VisiblePosition = 62 WHERE ID = 'd65917a7-c856-491b-9437-5a08fdfaa900'
UPDATE dbo.TemplateColumn SET VisiblePosition = 0 WHERE ID = '042e8def-445f-4c9e-839c-5ac28bbc6589'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tổng thu nhập chịu thuế TNCN', VisiblePosition = 20 WHERE ID = '9a1bdad2-ddac-45c5-8d13-5afa6a9813ca'
UPDATE dbo.TemplateColumn SET ColumnName = N'TotalAll' WHERE ID = 'f75abd3a-5d26-4663-a5f8-5b5b41ce9053'
UPDATE dbo.TemplateColumn SET VisiblePosition = 32 WHERE ID = '6447a26d-3255-4060-80f7-5c40286b68af'
UPDATE dbo.TemplateColumn SET VisiblePosition = 29 WHERE ID = '3b4a762b-9ef4-46ae-b650-5de242468101'
UPDATE dbo.TemplateColumn SET VisiblePosition = 4 WHERE ID = '9ec390c9-dcba-4946-af99-60713edf288e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '43097599-960f-43ca-a8a4-60d5de26e30c'
UPDATE dbo.TemplateColumn SET VisiblePosition = 11 WHERE ID = 'c8ffde3d-01b5-469e-b27c-613d8b05529e'
UPDATE dbo.TemplateColumn SET VisiblePosition = 18 WHERE ID = '6bfe81bb-dbec-42da-86cb-625a6f655a46'
UPDATE dbo.TemplateColumn SET ColumnName = N'InvoiceTemplate', ColumnCaption = N'Mấu số hóa đơn', VisiblePosition = 3 WHERE ID = 'c1dccad4-91d5-4e4c-bf90-62bb2b155a74'
UPDATE dbo.TemplateColumn SET VisiblePosition = 19 WHERE ID = '8279b7a8-e3cc-445a-8587-64c93dcfcbba'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '358cd54b-c39d-47ca-bfd5-64efef878382'
UPDATE dbo.TemplateColumn SET IsVisible = 0, VisiblePosition = 28 WHERE ID = 'ec006eb3-c46d-4c1b-abd9-6527b50d34b8'
UPDATE dbo.TemplateColumn SET VisiblePosition = 0 WHERE ID = 'c94094e0-6aad-439a-80be-6604bdaadbe7'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Chi phí mua', IsVisible = 1 WHERE ID = '451ee10a-bb7d-4497-9e5b-662305328b78'
UPDATE dbo.TemplateColumn SET ColumnName = N'InvoiceTypeID' WHERE ID = '906cc461-5717-46d4-be0d-66b7248346c7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6d3dd657-eb03-427f-a0ab-68664aeb0094'
UPDATE dbo.TemplateColumn SET VisiblePosition = 4 WHERE ID = '83590253-5dfc-4e98-b9c4-6b5f8d18fd9a'
UPDATE dbo.TemplateColumn SET ColumnName = N'TotalAllOriginal' WHERE ID = '0e0c1683-3a77-480b-aab6-6bfc1987e572'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7de37c86-f818-4aad-9e93-6bfcf4db09b3'
UPDATE dbo.TemplateColumn SET VisiblePosition = 28 WHERE ID = 'd02f9e6c-9df4-4d91-a0dd-6c4d74205dbe'
UPDATE dbo.TemplateColumn SET VisiblePosition = 41 WHERE ID = '78e907b4-7e72-4ca4-a987-6d3cb2b0351b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 8 WHERE ID = '3e0d4bca-8105-4ce3-89df-6d7956afc933'
UPDATE dbo.TemplateColumn SET VisiblePosition = 6 WHERE ID = 'c9186d38-d3a4-462e-b037-6d79ffec8b78'
UPDATE dbo.TemplateColumn SET VisiblePosition = 2 WHERE ID = 'ac477c32-8e6a-44aa-869b-6e3ee693811b'
UPDATE dbo.TemplateColumn SET ColumnName = N'InvoiceTemplate', ColumnCaption = N'Mấu số hóa đơn', IsVisible = 1, VisiblePosition = 7 WHERE ID = 'bb1e00d6-9f29-4457-ac7a-6ed9f034c3a3'
UPDATE dbo.TemplateColumn SET VisiblePosition = 9 WHERE ID = '584d1b6b-eac8-4bc0-9043-6f6619a42b4e'
UPDATE dbo.TemplateColumn SET VisiblePosition = 30 WHERE ID = '24d541e7-db81-424f-87a5-702027a1e995'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '54f0a6c8-ebfa-4af2-819d-7214c2eea57b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 15 WHERE ID = 'd2f3a587-f000-4591-89f8-72216cf04195'
UPDATE dbo.TemplateColumn SET VisiblePosition = 78 WHERE ID = 'ffef8851-a569-45ad-961e-72649c5a83e1'
UPDATE dbo.TemplateColumn SET VisiblePosition = 13 WHERE ID = '4c469fb9-d34f-4804-bc7d-73f820380595'
UPDATE dbo.TemplateColumn SET VisiblePosition = 31 WHERE ID = '016349a0-a53b-4656-a290-747c04763967'
UPDATE dbo.TemplateColumn SET VisiblePosition = 10 WHERE ID = '4c4ad0de-29ce-4d26-ac42-74a1b2a0ccd0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4bc53312-3de1-48cb-81c9-777cc5f08dde'
UPDATE dbo.TemplateColumn SET VisiblePosition = 8 WHERE ID = 'b0b6a41b-db98-4f89-9f9a-7902c14d408e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '47c414d3-6fe2-4532-92d2-7aaa6eaea35b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1, VisiblePosition = 3 WHERE ID = '66f65468-2152-4edb-a7db-7b39a783800d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1ff50a15-e9cc-4989-a12a-7be2016b55f6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '151947c3-6a45-465e-ad4a-7e2c02df26ac'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7e684939-93bb-4d67-ae26-7f7186d8a67f'
UPDATE dbo.TemplateColumn SET VisiblePosition = 18 WHERE ID = 'f639c227-7556-4013-8bbf-802dcebe74b0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '91a87b06-ffa1-4b96-a7b1-8047dd4e58e5'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '293fc8fd-2cee-48a5-b7cd-805d7a838aae'
UPDATE dbo.TemplateColumn SET VisiblePosition = 29 WHERE ID = '68d49081-1eb2-4dff-8f17-817727ca1fb8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1f186de7-a92c-41ff-b0b6-81c9d3336959'
UPDATE dbo.TemplateColumn SET VisiblePosition = 34 WHERE ID = 'ca26f1ff-e258-46e1-9a8c-82582cf15000'
UPDATE dbo.TemplateColumn SET VisiblePosition = 76 WHERE ID = '94e638ba-243d-4cd6-b140-826bde51f513'
UPDATE dbo.TemplateColumn SET ColumnName = N'TotalAll' WHERE ID = 'ea9ee0ac-f15f-4899-9fd3-82dc4a2a308b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 24 WHERE ID = 'd9ae52b2-424c-49c0-9160-83b4c18cd843'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '6ed9c5c0-a78d-4fb4-a4cf-84b81bba9fdf'
UPDATE dbo.TemplateColumn SET VisiblePosition = 32 WHERE ID = '90f241c5-eeea-48e3-984f-85488c91281a'
UPDATE dbo.TemplateColumn SET VisiblePosition = 13 WHERE ID = 'b5eb2a1a-6dfb-4d31-9229-8592b551e377'
UPDATE dbo.TemplateColumn SET VisiblePosition = 18 WHERE ID = 'e87f47f2-b220-4798-92fc-891f6a0396e5'
UPDATE dbo.TemplateColumn SET VisiblePosition = 29 WHERE ID = 'dc4e00b5-dd8e-4ba5-9261-8986c201a3da'
UPDATE dbo.TemplateColumn SET VisiblePosition = 1 WHERE ID = '6a59f7a3-9c65-4863-9745-89d3624a5a70'
UPDATE dbo.TemplateColumn SET VisiblePosition = 33 WHERE ID = '50e17f90-84f8-4653-b1cd-8bb3baeccc23'
UPDATE dbo.TemplateColumn SET VisiblePosition = 13 WHERE ID = '2dfe9015-2047-4d6a-9c45-8d3514ef41cf'
UPDATE dbo.TemplateColumn SET VisiblePosition = 14 WHERE ID = '5e576d10-146d-4086-8f4c-8d37e5d58bd7'
UPDATE dbo.TemplateColumn SET VisiblePosition = 35 WHERE ID = '18f9cbdf-3070-4c20-a024-8d7a9a977777'
UPDATE dbo.TemplateColumn SET ColumnName = N'InvoiceTemplate', VisiblePosition = 3 WHERE ID = '7fab9ace-2e90-4498-863f-8dfbeb383ed4'
UPDATE dbo.TemplateColumn SET VisiblePosition = 22 WHERE ID = '535ec38a-d81d-4b99-ac89-8ecf5a50b3fc'
UPDATE dbo.TemplateColumn SET VisiblePosition = 78 WHERE ID = 'f6429fca-ce3a-429f-9ca7-8ee36a538465'
UPDATE dbo.TemplateColumn SET VisiblePosition = 10 WHERE ID = '2d367db4-b269-4807-abb4-8f153d9fb819'
UPDATE dbo.TemplateColumn SET VisiblePosition = 10 WHERE ID = '1214afc8-f06f-4f87-908c-8f4710629546'
UPDATE dbo.TemplateColumn SET VisiblePosition = 23 WHERE ID = '2fca6df4-215a-453b-aced-8f5ba9f2707a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '0ea1fe2c-f7cc-4418-b8bd-9041deab0452'
UPDATE dbo.TemplateColumn SET VisiblePosition = 29 WHERE ID = 'a67e321f-1667-4b33-a918-90ba88052be4'
UPDATE dbo.TemplateColumn SET VisiblePosition = 75 WHERE ID = '0a680027-f16a-490e-9114-91a7944b0243'
UPDATE dbo.TemplateColumn SET VisiblePosition = 30 WHERE ID = 'c1690386-2eb7-4a23-b819-91c54d879dd8'
UPDATE dbo.TemplateColumn SET VisiblePosition = 23 WHERE ID = '9b357bcb-e995-4aa8-bba5-920093aee74e'
UPDATE dbo.TemplateColumn SET VisiblePosition = 14 WHERE ID = '162518c2-1e1e-4faf-a130-926f69499c57'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '406e249b-a4a5-4fbf-b667-9315e7faa9e8'
UPDATE dbo.TemplateColumn SET VisiblePosition = 75 WHERE ID = '4c6fe857-7d18-4ad5-a761-937a2ec2d3c3'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Phí trước hải quan' WHERE ID = 'c91de43a-8cc7-41dd-bed4-93ca48355727'
UPDATE dbo.TemplateColumn SET VisiblePosition = 12 WHERE ID = 'deb7c8dd-c4fd-46c0-bff5-9723eddddab3'
UPDATE dbo.TemplateColumn SET VisiblePosition = 77 WHERE ID = 'a7f15e10-4b14-4ff9-9be3-97adb3372864'
UPDATE dbo.TemplateColumn SET VisiblePosition = 78 WHERE ID = 'aeafe9f9-0ba9-4a2b-a757-97ea24e022c6'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'c474e408-68a0-4e4f-bfb8-99f654732ba3'
UPDATE dbo.TemplateColumn SET VisiblePosition = 21 WHERE ID = '802a2757-c1be-4a46-b548-9a994be324f8'
UPDATE dbo.TemplateColumn SET VisiblePosition = 43 WHERE ID = '8f839c68-5e0e-4299-b821-9e240e5c7477'
UPDATE dbo.TemplateColumn SET IsVisible = 0, VisiblePosition = 28 WHERE ID = '13c21ee6-2a0f-4fda-b2e9-9e42cbed54f7'
UPDATE dbo.TemplateColumn SET ColumnName = N'InvoiceTemplate', ColumnCaption = N'Mấu số hóa đơn', VisiblePosition = 7 WHERE ID = 'c4bf705f-0b9a-47af-9243-9e79e67bad12'
UPDATE dbo.TemplateColumn SET VisiblePosition = 10 WHERE ID = '48e8ef7f-73d3-43e6-af4d-9f7c0d779946'
UPDATE dbo.TemplateColumn SET VisiblePosition = 37 WHERE ID = 'f4067d28-d77a-4f80-a00d-9fb2d12d5d06'
UPDATE dbo.TemplateColumn SET VisiblePosition = 9 WHERE ID = 'd0684e3f-bb9e-414a-bee0-a0efa552be2c'
UPDATE dbo.TemplateColumn SET IsVisible = 0, VisiblePosition = 28 WHERE ID = '1691765a-bd8e-48c2-bd71-a1088e908544'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4ec53ce8-f3fe-4122-98e8-a1bdd5920274'
UPDATE dbo.TemplateColumn SET VisiblePosition = 2 WHERE ID = '43d64c40-9209-4681-a9ac-a22d3f24fc61'
UPDATE dbo.TemplateColumn SET ColumnName = N'InvoiceTemplate', VisiblePosition = 2 WHERE ID = '44336d67-2979-48ef-9828-a2b65fb01514'
UPDATE dbo.TemplateColumn SET VisiblePosition = 0 WHERE ID = '06e7d4e3-eb13-46df-973c-a455428f9bb4'
UPDATE dbo.TemplateColumn SET ColumnName = N'InvoiceTemplate', VisiblePosition = 35 WHERE ID = 'a5890cd2-1a99-4e60-b6fb-a460c257790d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'fd898ecf-e97b-4347-96d2-a4d8dc4b7f15'
UPDATE dbo.TemplateColumn SET ColumnName = N'InvoiceTemplate', ColumnCaption = N'Mấu số hóa đơn' WHERE ID = '41d07d61-ef68-4cd7-a6c9-a4faf1fcc0d4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '46ae1068-1674-45fd-990b-a5effb3fcb7b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 4 WHERE ID = '96af0aa3-b433-4ec4-8475-a7501f81a896'
UPDATE dbo.TemplateColumn SET VisiblePosition = 10 WHERE ID = '7a0db77b-770b-43e4-a960-a83c039b9408'
UPDATE dbo.TemplateColumn SET VisiblePosition = 18 WHERE ID = '3416992f-3c51-4319-a17a-a950a34a4eb0'
UPDATE dbo.TemplateColumn SET ColumnName = N'InvoiceTypeID' WHERE ID = '3dbc7464-65e7-4c86-9cb4-a9576a0f225e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '1518418a-11ee-4df9-a1df-a9a466fb2288'
UPDATE dbo.TemplateColumn SET VisiblePosition = 61 WHERE ID = '01632576-764c-4fa1-8e6f-aa033fda9faf'
UPDATE dbo.TemplateColumn SET VisiblePosition = 36 WHERE ID = '1bb44356-6d17-4008-9bdb-aa3df69a7c1d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '57d33bda-190c-4006-a744-ab1d2db27ee1'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Loại hóa đơn', VisiblePosition = 1 WHERE ID = '4fd2aab2-2d9d-4176-aaef-ab7945a65bc9'
UPDATE dbo.TemplateColumn SET VisiblePosition = 25 WHERE ID = '338b9132-a978-47cc-964c-ac74025f15a7'
UPDATE dbo.TemplateColumn SET ColumnName = N'NetAmountOriginal', VisiblePosition = 31 WHERE ID = 'fe65b3f3-4ec7-438c-8b9e-ac849bc67cc9'
UPDATE dbo.TemplateColumn SET VisiblePosition = 17 WHERE ID = 'abb2d902-4294-4e65-a3e0-ade131eee2e2'
UPDATE dbo.TemplateColumn SET VisiblePosition = 25 WHERE ID = '4404c097-2e03-4eb3-96bf-ae57a9732a70'
UPDATE dbo.TemplateColumn SET VisiblePosition = 3 WHERE ID = '50715075-30d4-40b3-85b4-aec3cecc0716'
UPDATE dbo.TemplateColumn SET ColumnName = N'TotalAllOriginal' WHERE ID = 'd20e1a75-742b-4bdd-bb55-aec859308435'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '7a03beb1-69ae-4c7d-a7cf-b242712a7aea'
UPDATE dbo.TemplateColumn SET ColumnName = N'InvoiceTemplate', VisiblePosition = 59 WHERE ID = '2f490771-8789-4a7e-9728-b35e228eb7a3'
UPDATE dbo.TemplateColumn SET VisiblePosition = 5 WHERE ID = '323b49e0-46d6-43d9-9c8b-b3e5f999a67d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 4 WHERE ID = 'c2e6bbe8-e661-40b5-8127-b410c2aaf7ce'
UPDATE dbo.TemplateColumn SET ColumnName = N'TotalAllOriginal' WHERE ID = '98c52b5b-62dd-4d03-8388-b50eb40fca49'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '509417ad-97fe-40c0-b864-b5fa47f16730'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '570b95a4-1c8b-4d26-b1ba-b64f05ea5b9a'
UPDATE dbo.TemplateColumn SET VisiblePosition = 6 WHERE ID = 'e0099bed-e3df-4dc8-bc94-b7b291e234da'
UPDATE dbo.TemplateColumn SET ColumnName = N'TotalAll' WHERE ID = 'f58a70fd-ca7c-4bd5-beb4-b81ac3f0c3ec'
UPDATE dbo.TemplateColumn SET ColumnName = N'TotalAllOriginal' WHERE ID = '817ba888-341e-4fb8-ac3d-b84f89c348bc'
UPDATE dbo.TemplateColumn SET VisiblePosition = 2 WHERE ID = 'dea0700a-4c13-41c3-bcae-b8a9e0191a08'
UPDATE dbo.TemplateColumn SET VisiblePosition = 19 WHERE ID = 'e9b28f42-329e-41d3-9a33-ba060b965dd2'
UPDATE dbo.TemplateColumn SET VisiblePosition = 4 WHERE ID = '1ab122f3-1efd-4a38-91bb-babc7d4f4793'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Loại hóa đơn', VisiblePosition = 2 WHERE ID = 'b8f300df-71f2-47b9-90a7-bb8635e38314'
UPDATE dbo.TemplateColumn SET ColumnName = N'TotalAll' WHERE ID = 'c23d66bf-5004-49b8-858d-bbff886ebbd3'
UPDATE dbo.TemplateColumn SET VisiblePosition = 36 WHERE ID = 'c915ba36-6df3-41b9-a84b-bdd8579e7da3'
UPDATE dbo.TemplateColumn SET VisiblePosition = 75 WHERE ID = '99f89c16-f9f6-4095-9954-be5e0603500b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 60 WHERE ID = 'cb707035-0116-4715-90b4-bf58d4d5d716'
UPDATE dbo.TemplateColumn SET VisiblePosition = 40 WHERE ID = '8e30d66d-7016-459e-80e2-bf6eddb18de7'
UPDATE dbo.TemplateColumn SET VisiblePosition = 7 WHERE ID = 'd792642a-c67f-4d07-9936-bfce3cd83c83'
UPDATE dbo.TemplateColumn SET VisiblePosition = 15 WHERE ID = 'bf584737-0afa-41e9-a7ed-c00364e587db'
UPDATE dbo.TemplateColumn SET VisiblePosition = 7 WHERE ID = '867fcefe-a78f-4c59-b4f2-c03177909871'
UPDATE dbo.TemplateColumn SET VisiblePosition = 75 WHERE ID = 'ddb27295-4c9c-4e12-8ffe-c03cb96c81bf'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '19959fee-4686-4638-a31f-c06d6acaa463'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'cdeb0aaf-6276-4903-af72-c1572e7146ff'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '4d4a5fb7-4756-478d-85da-c1cd0f7afd81'
UPDATE dbo.TemplateColumn SET ColumnName = N'InvoiceTemplate', ColumnCaption = N'Mấu số hóa đơn', VisiblePosition = 8 WHERE ID = 'e9ea667c-25e6-4700-b8ce-c2f7d6b4bf9d'
UPDATE dbo.TemplateColumn SET ColumnName = N'InvoiceTemplate', VisiblePosition = 59 WHERE ID = '2fac868e-5b3b-4d64-bae8-c328a7d0e3a9'
UPDATE dbo.TemplateColumn SET VisiblePosition = 1 WHERE ID = 'edb341eb-5d96-4136-b2fe-c472b9101809'
UPDATE dbo.TemplateColumn SET VisiblePosition = 7 WHERE ID = '3d7951e9-8410-4806-8113-c4854bead62b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 4 WHERE ID = '343f98cc-6481-44ac-8147-c60b64a6f82f'
UPDATE dbo.TemplateColumn SET ColumnName = N'InvoiceTemplate', VisiblePosition = 9 WHERE ID = '7e00c0d3-ac8d-409e-bf73-c677e1816d7b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 22 WHERE ID = 'fc2b88fe-126e-42e1-895c-c7f5bd10149f'
UPDATE dbo.TemplateColumn SET VisiblePosition = 8 WHERE ID = 'e546ca3d-4bc2-4c80-be2d-c8579ce208e0'
UPDATE dbo.TemplateColumn SET VisiblePosition = 15 WHERE ID = '5045b3df-620d-41d3-baaa-c939a9b7e0a0'
UPDATE dbo.TemplateColumn SET VisiblePosition = 34 WHERE ID = 'bc5d9beb-2603-488a-a9f5-c9dc19a42c01'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tổng thu nhập chịu thuế TNCN', VisiblePosition = 27 WHERE ID = 'ca492947-a31d-40e6-81fa-cbd81a8fad2b'
UPDATE dbo.TemplateColumn SET ColumnName = N'NetAmountOriginal', VisiblePosition = 24 WHERE ID = '9d5c0985-d5ec-4ed1-9449-cc18e6910283'
UPDATE dbo.TemplateColumn SET VisiblePosition = 20 WHERE ID = '4829b99f-f7d0-4b6e-a500-cc97a3026d84'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '4fd377f3-3a23-49fc-bba1-ce09155c2b1e'
UPDATE dbo.TemplateColumn SET ColumnName = N'InvoiceTemplate', ColumnCaption = N'Mấu số hóa đơn', VisiblePosition = 7 WHERE ID = 'f826dc5b-247a-4f92-86f1-ce9a74168b11'
UPDATE dbo.TemplateColumn SET VisiblePosition = 5 WHERE ID = 'c5a1d67b-872a-4969-a859-cff1dc371c78'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '79f86d46-25a2-4422-a77b-d0d9c9d34641'
UPDATE dbo.TemplateColumn SET VisiblePosition = 21 WHERE ID = 'a2eb211f-5ccb-4921-a91d-d1365a56fc47'
UPDATE dbo.TemplateColumn SET VisiblePosition = 36 WHERE ID = '9f7bb0ca-90b5-4bbd-8cd5-d1813429ac7e'
UPDATE dbo.TemplateColumn SET VisiblePosition = 36 WHERE ID = '1f77c143-24ec-466d-8b3c-d41f1d34928b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 9 WHERE ID = '0b117d6c-1f2b-48ea-8076-d463842744a9'
UPDATE dbo.TemplateColumn SET VisiblePosition = 12 WHERE ID = '23cd3d8d-453d-4ed5-a4ba-d5dac8b86e68'
UPDATE dbo.TemplateColumn SET VisiblePosition = 24 WHERE ID = '7f19f43a-7e70-4aca-91b6-d637944cc699'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '9f2eb755-3ac6-4ea3-bee7-d6c91aef7172'
UPDATE dbo.TemplateColumn SET ColumnName = N'TotalAll' WHERE ID = '4cd9a410-498e-4949-8305-d7cfc45e7d89'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b35560e4-cf12-4507-9ba6-d9c602931c3e'
UPDATE dbo.TemplateColumn SET VisiblePosition = 62 WHERE ID = 'f8e444a1-9f3c-4cee-980b-dad6b22cde6f'
UPDATE dbo.TemplateColumn SET VisiblePosition = 11 WHERE ID = '361f341e-42a3-4e08-aaec-db71d24343f4'
UPDATE dbo.TemplateColumn SET VisiblePosition = 30 WHERE ID = 'bc45d7d8-25f7-4953-b064-dba86b1469fd'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7f226a62-4632-4e8c-b283-dc42daa3ec4c'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '6ea8060d-354f-4025-a58f-de9adb0e66b4'
UPDATE dbo.TemplateColumn SET VisiblePosition = 38 WHERE ID = '553cb178-f3d4-4d74-b13e-de9e6de89a5b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 12 WHERE ID = '876b9945-84f0-4892-9a2c-dfb55dd6e361'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '3a6d095b-bb95-4035-bbc8-e037d27c4ae7'
UPDATE dbo.TemplateColumn SET VisiblePosition = 1 WHERE ID = '6c6dc1fe-d7d1-4aea-9b1c-e0b2bd177c0c'
UPDATE dbo.TemplateColumn SET VisiblePosition = 10 WHERE ID = 'e1552719-e9c6-4f4c-b427-e19528f54d8d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 11 WHERE ID = 'ef635838-c1f6-4437-8c55-e2f98fd93423'
UPDATE dbo.TemplateColumn SET IsVisible = 1, VisiblePosition = 34 WHERE ID = '4acc9711-d961-42bd-9055-e31d09bd9fb5'
UPDATE dbo.TemplateColumn SET VisiblePosition = 17 WHERE ID = '30c77fa2-d6ab-4def-8717-e39e9d2ce8ff'
UPDATE dbo.TemplateColumn SET ColumnMaxWidth = 0 WHERE ID = '9e690198-4b4a-4f5b-ad71-e4556ef7284b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 27 WHERE ID = 'c7ee5893-b26c-4c37-a4e0-e5475c279e7f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c0986b6f-1853-4fd9-b47e-e597d91cfb3a'
UPDATE dbo.TemplateColumn SET VisiblePosition = 29 WHERE ID = '1e5a7a61-53f8-4cb9-90c3-e632dc6f629c'
UPDATE dbo.TemplateColumn SET ColumnName = N'InvoiceTemplate', ColumnCaption = N'Mấu số hóa đơn' WHERE ID = 'abd14a2b-2d4b-4a0a-8592-e67af30b7ab7'
UPDATE dbo.TemplateColumn SET VisiblePosition = 27 WHERE ID = '17b9fc0d-410d-4936-a9c2-e7540f21a77b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 60 WHERE ID = 'de9afd45-bda7-46c3-8319-e7c319887bea'
UPDATE dbo.TemplateColumn SET ColumnName = N'TotalAll' WHERE ID = '68c6d838-fe04-474f-82e1-e86830e50ace'
UPDATE dbo.TemplateColumn SET VisiblePosition = 4 WHERE ID = 'ae3c8c06-0571-4125-9671-e9d5559e65ae'
UPDATE dbo.TemplateColumn SET VisiblePosition = 78 WHERE ID = '8b260b4f-c6a2-4e6d-b4ea-ea028e1f1364'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '8ad4c16a-e785-4a44-85cc-ea1535c01901'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c575563f-f7d9-4cb3-8e39-ea4c5ff68e8e'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Loại hóa đơn', VisiblePosition = 2 WHERE ID = 'aecc00a8-f3f7-45b4-ae83-eb27f837e9b3'
UPDATE dbo.TemplateColumn SET VisiblePosition = 9 WHERE ID = '6d20c34a-9093-4507-a937-ebb069643d63'
UPDATE dbo.TemplateColumn SET VisiblePosition = 26 WHERE ID = '4bdb1cb0-bb85-4cec-99bd-ec1f74234ecc'
UPDATE dbo.TemplateColumn SET ColumnMaxWidth = 0 WHERE ID = '74aa6615-ca2b-4a94-9ca9-ec68855b0ed7'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Loại hóa đơn', VisiblePosition = 2 WHERE ID = '18895cea-85a0-4569-8466-eda396e75cd5'
UPDATE dbo.TemplateColumn SET VisiblePosition = 22 WHERE ID = '09bcfe80-a8d6-4332-9fd5-f02f03e4c71d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8d0855ac-9489-4c8d-9772-f05e68d39fe6'
UPDATE dbo.TemplateColumn SET VisiblePosition = 23 WHERE ID = '529e059c-21e5-48b8-8494-f0c782b19dd9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '8d564371-7ae8-480d-8386-f2223aa1ccea'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'a4c946fe-f4b5-40b1-a9c9-f22731fe655a'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '539bce73-97ee-4763-8a06-f2b005f446ba'
UPDATE dbo.TemplateColumn SET ColumnName = N'TotalAllOriginal' WHERE ID = '2ad7aa63-7dea-4d8e-8d94-f5da45eca94a'
UPDATE dbo.TemplateColumn SET VisiblePosition = 75 WHERE ID = '0f5c0b1b-3782-4fc0-adf4-f621c6fa0edb'
UPDATE dbo.TemplateColumn SET VisiblePosition = 60 WHERE ID = '383d61d1-349f-449c-9485-f626d490aade'
UPDATE dbo.TemplateColumn SET VisiblePosition = 22 WHERE ID = 'af6cea6d-3d81-46a7-80bc-f710c956699f'
UPDATE dbo.TemplateColumn SET ColumnName = N'TotalAllOriginal' WHERE ID = '37bca50f-3732-4393-bee2-f738ce3fb06f'
UPDATE dbo.TemplateColumn SET ColumnName = N'TotalAllOriginal' WHERE ID = 'c95af8bf-11d9-43cf-9f17-f80eabfaf723'
UPDATE dbo.TemplateColumn SET VisiblePosition = 25 WHERE ID = '5b958334-92f3-4672-914a-f82d3cd49541'
UPDATE dbo.TemplateColumn SET VisiblePosition = 28 WHERE ID = 'e760739e-79ee-4637-83b5-f880fe51d828'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1, VisiblePosition = 3 WHERE ID = '8bb0965e-9510-4be4-9450-fa1a5a3cde08'
UPDATE dbo.TemplateColumn SET VisiblePosition = 16 WHERE ID = '570b0a1d-f146-45ea-aa98-fa60717b1277'
UPDATE dbo.TemplateColumn SET VisiblePosition = 9 WHERE ID = '4ac438d0-8934-4760-80fc-faa9df79193d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '63c2cb96-ec9f-492c-ae70-fb26add336ac'
UPDATE dbo.TemplateColumn SET VisiblePosition = 16 WHERE ID = 'b115dc9b-7ef4-44cd-aaaa-fcd081840751'
UPDATE dbo.TemplateColumn SET VisiblePosition = 26 WHERE ID = '7f2d02a1-29be-459e-933f-fd5688260071'
UPDATE dbo.TemplateColumn SET ColumnName = N'TotalAll' WHERE ID = '641b5dd0-dc97-45dd-99bc-febf3abe9f09'
UPDATE dbo.TemplateColumn SET VisiblePosition = 76 WHERE ID = '488d6f92-c8bc-432e-84c9-ff016e6a59c0'

INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('74d858cc-18e0-42ec-bf62-00b93a7a7619', '2811121d-135b-44bd-870a-3a1ce0391f76', N'InvoiceTemplate', N'Mẫu số hóa đơn', NULL, 0, 0, 100, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('29308f07-0e16-4fef-8e57-037e053468ad', '760075e3-6538-46ea-9104-39ab9c458675', N'TaxExchangeRate', N'Tỷ giá tính thuế', N'', 0, 0, 105, 0, 0, 0, 22)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('55ca941f-6469-40d4-9726-04dece837eab', '7935bce2-a46f-4d23-a921-359631be4a86', N'TaxExchangeRate', N'Tỷ giá tính thuế', N'', 0, 0, 105, 0, 0, 0, 22)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('5e0e1818-8df8-427a-90e8-0e2edf2b5a5d', 'bd30aad0-ff4f-4517-8013-703127a9f7d7', N'OWAmountOriginal', N'Ti?n v?n', N'Ti?n v?n quy d?i', 0, 1, 120, 0, 0, 1, 60)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('f97db890-e7ce-4aab-8095-142acdf08177', '2d068c50-3661-4f7b-bc1f-e83c62f29471', N'AccountObjectName', N'Họ tên', N'Họ tên', 0, 0, 100, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('869e1a96-410f-45f1-84d0-17665b744000', '604fc12c-6ae3-4cbe-975c-4753dc99ab94', N'Purpose', N'Mục đích sử dụng', N'Mục đích sử dụng', 1, 0, 150, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('3387ccf0-118c-4875-8d22-1801f175d5f0', '49a61df8-06ba-445e-85f1-a25534d0709e', N'Amount', N'Số tiền', N'Số tiền', 0, 0, 100, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('bdbb16a9-ea2b-47e7-8ebd-1cea628b2be9', '5481dc1a-7970-4d75-a05f-4f7ab14bcd28', N'TaxExchangeRate', N'Tỷ giá tính thuế', N'', 0, 0, 105, 0, 0, 0, 35)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('280779f5-10e4-4603-b799-22df6d8a1c54', 'bb1581cd-8874-4339-af62-3e53c9963360', N'CostSetID', N'ĐT tập hợp CP', N'Đối tượng tập hợp chi phí', 0, 0, 150, 0, 0, 1, 9)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d2aefbc7-330b-4436-990c-2324dbafdd18', '2d068c50-3661-4f7b-bc1f-e83c62f29471', N'AccountObjectTitle', N'Chức vụ', N'Chức vụ', 0, 0, 100, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('1e477c30-82ba-447d-bdf5-2da775fcf88a', 'e494246c-1856-4282-9844-f3fab7214220', N'TaxExchangeRate', N'Tỷ giá tính thuế', N'', 0, 0, 105, 0, 0, 0, 22)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ebe7683f-52d4-4f40-bc9b-38cb9673165f', 'd284b3b7-8009-457b-9188-3714ae0a5ce1', N'InvoiceForm', N'Hình thức hóa đơn', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('386c9143-eb29-497e-bdaf-3cbabc7156ea', '88cbea4d-8a5d-4308-82b4-32d9ccf3d6dc', N'BasicWage', N'Lương cơ bản', NULL, 0, 0, 150, 0, 0, 1, 7)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9b08284c-b0d5-45eb-a1ff-3d9012fa8d42', 'bb9ebabb-ba6c-4758-8bf3-a3ef94460519', N'TaxExchangeRate', N'Tỷ giá tính thuế', N'', 0, 0, 105, 0, 0, 0, 34)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('628a03da-f421-45fd-aca6-4417964150c5', '21caab25-7733-4683-a969-281a3e8ace10', N'AccountingObjectName', N'Họ tên', N'Họ tên', 0, 0, 100, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('f48fab57-c1ce-4279-889b-459619203356', '13c4300c-dfb2-4989-b25c-7772870167c4', N'TaxExchangeRate', N'Tỷ giá tính thuế', N'', 0, 0, 105, 0, 0, 0, 22)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('993db2d7-e9e1-4c98-835a-4e34fcac698c', 'f3b6bdf0-a72c-4d5f-ba35-83a99abadc31', N'TaxExchangeRate', N'Tỷ giá tính thuế', N'', 0, 0, 105, 0, 0, 0, 35)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('57a78113-03c8-4c58-b86d-5276f136107a', '8c4a9d2c-2a49-4f50-b112-932d5647c54c', N'TaxExchangeRate', N'Tỷ giá tính thuế', N'', 0, 0, 105, 0, 0, 0, 35)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('bf6551b8-9a0d-4a3f-bfe5-54ee72771a9a', 'b7013eae-d370-48d9-9252-4a2b9de9650f', N'TaxExchangeRate', N'Tỷ giá tính thuế', N'', 0, 0, 105, 0, 0, 0, 35)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('4522665f-8050-498d-a13c-63b9de1a1a44', '9a62ed09-5994-4c81-b1e7-80cdc01f7153', N'Description', N'Diễn giải', N'Diễn giải', 0, 0, 100, 0, 0, 1, 4)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('db63dc2c-b9f8-43f0-b4c1-64e291a92a0e', '21caab25-7733-4683-a969-281a3e8ace10', N'AccountingObjectTitle', N'Chức vụ', N'Chức vụ', 0, 0, 100, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('0f4a3750-9bed-4caa-bc20-679ecea372ac', '604fc12c-6ae3-4cbe-975c-4753dc99ab94', N'InvoiceSeries', N'Ký hiệu hóa đơn', N'Ký hiệu hóa đơn', 1, 0, 150, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('7b9f582c-c2bd-4e30-b5fb-6f040881a690', '21caab25-7733-4683-a969-281a3e8ace10', N'AccountingObjectID', N'Mã nhân viên', N'Mệnh giá sàn', 0, 0, 100, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('76b5ac8c-4640-4d34-9861-74373aee49c5', '88cbea4d-8a5d-4308-82b4-32d9ccf3d6dc', N'DepartmentID', N'Phòng ban', NULL, 0, 0, 150, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('069ba67d-b6d3-419b-a77f-7910437772e5', 'ae8ca68b-fd42-4ec2-9385-7d54279f17ab', N'TaxExchangeRate', N'Tỷ giá tính thuế', N'', 0, 0, 105, 0, 0, 0, 35)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('947b9b1a-d534-4fe4-86c6-7a1ae8effa9e', 'ac683135-1676-4f21-8a2f-053627f27c3f', N'TaxExchangeRate', N'Tỷ giá tính thuế', N'', 0, 0, 105, 0, 0, 0, 22)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('fa905650-cbd1-4635-b452-8083c48821a8', '9132090c-6ac8-4e35-98e9-a544197a2266', N'TaxExchangeRate', N'Tỷ giá tính thuế', N'', 0, 0, 105, 0, 0, 0, 35)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('4ef9db9c-4f64-4a5c-9792-8577ad6713cc', 'ef61584b-666a-4682-8770-440cb05c5ae9', N'InvoiceTemplate', N'Mẫu số hóa đơn', NULL, 0, 0, 100, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('cb8a6ec3-3255-4fa7-9f0d-88aed5542034', '21caab25-7733-4683-a969-281a3e8ace10', N'DepartmentID', N'Phòng ban', N'Phòng ban', 0, 0, 100, 0, 0, 1, 4)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('e089cf5c-c6f0-438e-b5ec-8a281a9cf4b0', '7ed3ec3d-d7c0-4216-8e9d-e9f9182482e2', N'SalaryCoefficient', N'Hệ số lương', NULL, 0, 0, 150, 0, 0, 1, 6)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('acc551b2-3638-46a1-8f84-9408b1229218', '4cf1f231-03cb-4ee2-89e5-64bffe008d2d', N'TaxExchangeRate', N'Tỷ giá tính thuế', N'', 0, 0, 105, 0, 0, 0, 21)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('cc747533-142f-4b0e-88cc-941e2c561b62', '604fc12c-6ae3-4cbe-975c-4753dc99ab94', N'InvoiceType', N'Loại hóa đơn', N'Loại hóa đơn', 1, 0, 150, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('5616cb14-bb93-4881-b507-98623d20fb65', 'df2d997d-5506-4cce-a7d7-f2be38c970d9', N'InvoiceTemplate', N'Mẫu số hóa đơn', NULL, 0, 0, 100, 0, 0, 1, 4)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('362f5e4e-c005-4e8a-b71e-9baa33e9f340', '7ae5689f-9660-4f58-bcdd-72ddd89501dc', N'DepartmentID', N'Phòng ban', NULL, 0, 0, 150, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('1896faa5-53d2-4694-a305-a21435af97e9', '7ed3ec3d-d7c0-4216-8e9d-e9f9182482e2', N'DepartmentID', N'Phòng ban', NULL, 0, 0, 150, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9565d0f3-1960-41d7-ae00-a3f6daf717b5', '2d068c50-3661-4f7b-bc1f-e83c62f29471', N'AccountObjectID', N'Mã nhân viên', N'Mệnh giá sàn', 0, 0, 100, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('2db035e4-ab80-461d-9aad-a6affb8b2636', '9a62ed09-5994-4c81-b1e7-80cdc01f7153', N'ValueOfMoney', N'Mệnh giá tiền', N'Mệnh giá tiền', 0, 0, 100, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('56b6ea28-9bcd-4079-95a4-a72e39ecb55f', '5c7f2f45-6134-43bd-81f3-1d71d26050ea', N'InvoiceTemplate', N'Mẫu số hóa đơn', NULL, 0, 0, 100, 0, 0, 1, 4)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('10c66931-3fcb-4cd0-8638-ab59f9f71c30', '6ecf6bde-7a37-4332-8038-45095c1b2ae4', N'TaxExchangeRate', N'Tỷ giá tính thuế', N'', 0, 0, 105, 0, 0, 0, 35)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('8b57e4b5-01ee-410b-8b5c-b1ec46dc0016', '2d068c50-3661-4f7b-bc1f-e83c62f29471', N'DepartmentID', N'Phòng ban', N'Phòng ban', 0, 0, 100, 0, 0, 1, 4)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('de3d8e9d-483d-4a1e-97ae-b84585d83265', '3e593632-00fe-4f80-8a2e-ade50cf8393f', N'SalaryCoefficient', N'Hệ số lương', NULL, 0, 0, 150, 0, 0, 1, 6)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ac6a16fb-0c6e-4329-85f2-b90fce2783bf', '9a62ed09-5994-4c81-b1e7-80cdc01f7153', N'Quantity', N'Số lượng', N'Số lượng', 0, 0, 100, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('c1da32c3-8048-4da6-8dc5-bd73843d08a9', '604fc12c-6ae3-4cbe-975c-4753dc99ab94', N'InvoiceForm', N'Hình thức hóa đơn', N'Hình thức hóa đơn', 1, 0, 150, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('05eb1c88-0ff3-49c5-a65c-c2bf246affb5', '3e593632-00fe-4f80-8a2e-ade50cf8393f', N'BasicWage', N'Lương cơ bản', NULL, 0, 0, 150, 0, 0, 1, 7)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('a7d925d3-6b32-4755-90b9-c422dfa291c9', '6e60dd5f-2446-44f5-ac1e-9d81422051f7', N'TaxExchangeRate', N'Tỷ giá tính thuế', N'', 0, 0, 105, 0, 0, 0, 22)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ba090443-4451-4838-ae4b-ca9982be3edf', '88cbea4d-8a5d-4308-82b4-32d9ccf3d6dc', N'SalaryCoefficient', N'Hệ số lương', NULL, 0, 0, 150, 0, 0, 1, 6)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('50c063c5-cfaa-4258-a74c-cf6defc07065', '3e593632-00fe-4f80-8a2e-ade50cf8393f', N'DepartmentID', N'Phòng ban', NULL, 0, 0, 150, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('941e573e-7018-4ed7-8728-d1f5edabf4e1', 'be771620-a6e0-4523-9f67-b22566cecb89', N'Day30', N'Ngày 30', NULL, 0, 0, 120, 0, 0, 1, 33)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('76bd43ec-9a55-4ff1-b1eb-d380df419b26', 'afc227d1-6a73-4c9a-8a1c-d9e9a7ce4c0b', N'TaxExchangeRate', N'Tỷ giá tính thuế', N'', 0, 0, 105, 0, 0, 0, 22)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('0c9c0267-bbe0-4bf9-8d85-dcf9c7327899', '321e87c9-d277-449e-bb22-994b44deffc8', N'InvoiceTemplate', N'Mẫu số hóa đơn', NULL, 0, 0, 100, 0, 0, 1, 4)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('2f0330e3-e6fa-4736-a182-dea78787dd16', '49a61df8-06ba-445e-85f1-a25534d0709e', N'Quantity', N'Số lượng', N'Số lượng', 0, 0, 100, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('0d9869f7-e171-4a85-bf67-e1c03922e705', '76035616-9d20-48b7-8382-036725dd7c3c', N'InvoiceForm', N'Hình thức hóa đơn', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9d80cc55-21c9-4a7f-b06a-e3a38793165d', 'b1d34001-ea93-4e33-bf50-c680ae38bd1e', N'InvoiceTemplate', N'Mẫu số hóa đơn', NULL, 0, 0, 100, 0, 0, 1, 4)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('29eb13d8-0461-4d13-82e7-e42f3fc77c03', 'be771620-a6e0-4523-9f67-b22566cecb89', N'DepartmentID', N'Phòng ban', NULL, 0, 0, 120, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('067f5fbe-cf75-4919-972b-e8af1788597a', '9a62ed09-5994-4c81-b1e7-80cdc01f7153', N'Amount', N'Số tiền', N'Số tiền', 0, 0, 100, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('3892e8aa-a46a-4113-8841-ea74b47015c8', '49a61df8-06ba-445e-85f1-a25534d0709e', N'ValueOfMoney', N'Mệnh giá sàn', N'Mệnh giá sàn', 0, 0, 100, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('665dd1ee-b304-440f-b513-edc6177226ac', '7ed3ec3d-d7c0-4216-8e9d-e9f9182482e2', N'BasicWage', N'Lương cơ bản', NULL, 0, 0, 150, 0, 0, 1, 7)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('0dc09f1b-6afc-4c0b-b6e3-ede091292727', '9ec5eae5-eeab-4efb-b763-ee7001ec221a', N'TaxExchangeRate', N'Tỷ giá tính thuế', N'', 0, 0, 105, 0, 0, 0, 22)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('a9bd8aa7-2b8d-4f44-a7f6-f206a70bae94', '55918c54-7278-400b-abfb-a0262066e029', N'TaxExchangeRate', N'Tỷ giá tính thuế', N'', 0, 0, 105, 0, 0, 0, 22)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('b597761e-8d63-45e3-9f83-fe1661ef73c8', '49a61df8-06ba-445e-85f1-a25534d0709e', N'Description', N'Diễn giải', N'Diễn giải', 0, 0, 100, 0, 0, 1, 4)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.TemplateDetail SET TabCaption = N'&3. Chứng từ chi phí' WHERE ID = '343fcc24-da42-408f-b529-15836deeea7f'
UPDATE dbo.TemplateDetail SET TabIndex = 1, TabCaption = N'&2. Hoá đơn' WHERE ID = '9c23f0c5-4fdf-461c-86d5-2d124d42676b'
UPDATE dbo.TemplateDetail SET TabIndex = 0, TabCaption = N'&1. Hạch toán' WHERE ID = 'ed96e0e3-a86b-428c-9574-516af778ec9f'
UPDATE dbo.TemplateDetail SET TabIndex = 0, TabCaption = N'&1. Hạch toán' WHERE ID = '1afcb6a7-5c44-407a-9a88-cd5105413568'
UPDATE dbo.TemplateDetail SET TabIndex = 1, TabCaption = N'&2. Hoá đơn' WHERE ID = 'cbf225fd-6777-44e1-9ac0-ee83ebc942db'

INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('21caab25-7733-4683-a969-281a3e8ace10', '18ad3e30-1f72-48b2-9202-a95fcc572a12', 2, 1, N'&2. Thành viên tham gia')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('9a62ed09-5994-4c81-b1e7-80cdc01f7153', '18ad3e30-1f72-48b2-9202-a95fcc572a12', 1, 0, N'&1. Kiểm kê')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('49a61df8-06ba-445e-85f1-a25534d0709e', 'aef47175-d71c-4532-9fa1-4f04e9c4e696', 1, 0, N'&1. Kiểm kê')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('2d068c50-3661-4f7b-bc1f-e83c62f29471', 'aef47175-d71c-4532-9fa1-4f04e9c4e696', 2, 1, N'&2. Thành viên tham gia')
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

INSERT dbo.TimeSheetSymbols(ID, TimeSheetSymbolsCode, TimeSheetSymbolsName, SalaryRate, IsDefault, IsOverTime, IsActive, IsSecurity, IsHalfDay, OverTimeSymbol) VALUES ('0da01b75-3d5b-4224-81cc-13f403a0ad33', N'+', N'Công ngày', 100.0000000000, 1, 0, 1, 0, 0, -1)
INSERT dbo.TimeSheetSymbols(ID, TimeSheetSymbolsCode, TimeSheetSymbolsName, SalaryRate, IsDefault, IsOverTime, IsActive, IsSecurity, IsHalfDay, OverTimeSymbol) VALUES ('93b8cc3c-f4d8-4778-93cc-1e962382c009', N'TS', N'Thai sản', 100.0000000000, 0, 0, 0, 0, 0, NULL)
INSERT dbo.TimeSheetSymbols(ID, TimeSheetSymbolsCode, TimeSheetSymbolsName, SalaryRate, IsDefault, IsOverTime, IsActive, IsSecurity, IsHalfDay, OverTimeSymbol) VALUES ('766b5dcf-acda-4966-b450-1f6960d75e8c', N'LT(Đ)', N'Làm thêm ngày thường ban đêm', 150.0000000000, 0, 1, 1, 0, 0, 3)
INSERT dbo.TimeSheetSymbols(ID, TimeSheetSymbolsCode, TimeSheetSymbolsName, SalaryRate, IsDefault, IsOverTime, IsActive, IsSecurity, IsHalfDay, OverTimeSymbol) VALUES ('ed2bb8bd-9f49-4332-9e21-244f3b665c57', N'CO', N'Con ốm', 75.0000000000, 0, 0, 1, 0, 0, NULL)
INSERT dbo.TimeSheetSymbols(ID, TimeSheetSymbolsCode, TimeSheetSymbolsName, SalaryRate, IsDefault, IsOverTime, IsActive, IsSecurity, IsHalfDay, OverTimeSymbol) VALUES ('a85bbb53-094a-4891-94f8-2e6dbd3da02a', N'LTL(Đ)', N'Làm thêm nghỉ lễ, tết ban đêm', 0.0000000000, 0, 1, 1, 0, 0, 5)
INSERT dbo.TimeSheetSymbols(ID, TimeSheetSymbolsCode, TimeSheetSymbolsName, SalaryRate, IsDefault, IsOverTime, IsActive, IsSecurity, IsHalfDay, OverTimeSymbol) VALUES ('d31eb567-fa69-4d76-b4bb-3fe79a7b9faa', N'KL', N'Không lương', 0.0000000000, 0, 0, 1, 0, 0, NULL)
INSERT dbo.TimeSheetSymbols(ID, TimeSheetSymbolsCode, TimeSheetSymbolsName, SalaryRate, IsDefault, IsOverTime, IsActive, IsSecurity, IsHalfDay, OverTimeSymbol) VALUES ('49f691a7-16f0-4bc9-99a7-420738acc241', N'LTL(N)', N'Làm thêm nghỉ lễ, tết ban ngày', 200.0000000000, 0, 1, 1, 0, 0, 2)
INSERT dbo.TimeSheetSymbols(ID, TimeSheetSymbolsCode, TimeSheetSymbolsName, SalaryRate, IsDefault, IsOverTime, IsActive, IsSecurity, IsHalfDay, OverTimeSymbol) VALUES ('b809cddd-4785-45d1-b0d4-5dcdd62c1701', N'CN', N'Chủ nhật', 200.0000000000, 0, 0, 1, 0, 0, 1)
INSERT dbo.TimeSheetSymbols(ID, TimeSheetSymbolsCode, TimeSheetSymbolsName, SalaryRate, IsDefault, IsOverTime, IsActive, IsSecurity, IsHalfDay, OverTimeSymbol) VALUES ('e8926ef4-35f4-4dcc-878e-70068739cac9', N'LTN(N)', N'Làm thêm ngày thứ 7, chủ nhật ban ngày', 0.0000000000, 0, 1, 1, 0, 0, 1)
INSERT dbo.TimeSheetSymbols(ID, TimeSheetSymbolsCode, TimeSheetSymbolsName, SalaryRate, IsDefault, IsOverTime, IsActive, IsSecurity, IsHalfDay, OverTimeSymbol) VALUES ('91e7f7de-0a7e-4a19-9528-89848fa7cdfa', N'LTN(Đ)', N'Làm thêm ngày thứ 7, chủ nhật ban đêm', 0.0000000000, 0, 1, 1, 0, 0, 4)
INSERT dbo.TimeSheetSymbols(ID, TimeSheetSymbolsCode, TimeSheetSymbolsName, SalaryRate, IsDefault, IsOverTime, IsActive, IsSecurity, IsHalfDay, OverTimeSymbol) VALUES ('d5cce21b-f81e-49e7-81ce-9ef3adca908d', N'LT(N)', N'Làm thêm ngày thường ban ngày', 0.0000000000, 0, 1, 1, 0, 0, 0)
INSERT dbo.TimeSheetSymbols(ID, TimeSheetSymbolsCode, TimeSheetSymbolsName, SalaryRate, IsDefault, IsOverTime, IsActive, IsSecurity, IsHalfDay, OverTimeSymbol) VALUES ('7070de97-02fd-46ee-909c-a391a4a91b5d', N'NL', N'Nghỉ lễ', 300.0000000000, 0, 0, 1, 0, 0, 2)
INSERT dbo.TimeSheetSymbols(ID, TimeSheetSymbolsCode, TimeSheetSymbolsName, SalaryRate, IsDefault, IsOverTime, IsActive, IsSecurity, IsHalfDay, OverTimeSymbol) VALUES ('5a54846b-d985-4e73-80fa-aa33d02a2b83', N'NP', N'Nghỉ phép', 100.0000000000, 0, 0, 1, 0, 0, NULL)
INSERT dbo.TimeSheetSymbols(ID, TimeSheetSymbolsCode, TimeSheetSymbolsName, SalaryRate, IsDefault, IsOverTime, IsActive, IsSecurity, IsHalfDay, OverTimeSymbol) VALUES ('ad52600b-6deb-4734-9dc5-b432a141882d', N'N', N'Ngừng việc', 0.0000000000, 0, 1, 1, 0, 0, -2)
INSERT dbo.TimeSheetSymbols(ID, TimeSheetSymbolsCode, TimeSheetSymbolsName, SalaryRate, IsDefault, IsOverTime, IsActive, IsSecurity, IsHalfDay, OverTimeSymbol) VALUES ('1a3fed8b-9a62-4947-a9db-b45a190f1396', N'LOC', N'Linh oc', 50.0000000000, 0, 1, 1, 0, 0, -1)
INSERT dbo.TimeSheetSymbols(ID, TimeSheetSymbolsCode, TimeSheetSymbolsName, SalaryRate, IsDefault, IsOverTime, IsActive, IsSecurity, IsHalfDay, OverTimeSymbol) VALUES ('18c1a1bc-d0ab-44b8-a133-d226839bec7e', N'-', N'Nửa ngày', 100.0000000000, 0, 0, 1, 0, 1, -1)
INSERT dbo.TimeSheetSymbols(ID, TimeSheetSymbolsCode, TimeSheetSymbolsName, SalaryRate, IsDefault, IsOverTime, IsActive, IsSecurity, IsHalfDay, OverTimeSymbol) VALUES ('ff8193eb-8792-4c40-9996-d58bc9463bbc', N'Ô', N'Ốm', 100.0000000000, 0, 0, 1, 0, 0, NULL)
INSERT dbo.TimeSheetSymbols(ID, TimeSheetSymbolsCode, TimeSheetSymbolsName, SalaryRate, IsDefault, IsOverTime, IsActive, IsSecurity, IsHalfDay, OverTimeSymbol) VALUES ('f7c73a86-9fc9-4228-a464-e64ed369fe50', N'LĐ', N'Lao động nghĩa vụ', 100.0000000000, 0, 0, 1, 0, 0, NULL)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.Type SET TypeName = N'Bảng tổng hợp chấm công theo giờ' WHERE ID = 811
UPDATE dbo.Type SET TypeName = N'Bảng chấm công theo giờ' WHERE ID = 820
UPDATE dbo.Type SET TypeName = N'Bảng chấm công theo ngày' WHERE ID = 821

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
SET IDENTITY_INSERT dbo.Type ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
INSERT dbo.Type(ID, TypeName, TypeGroupID, Recordable, Searchable, PostType, OrderPriority) VALUES (180, N'Kiểm kê quỹ', 18, 1, 1, 1, 0)
GO
SET IDENTITY_INSERT dbo.Type OFF
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

INSERT dbo.TypeGroup(ID, TypeGroupName, DebitAccountID, CreditAccountID) VALUES (18, N'Kiểm kê quỹ', NULL, NULL)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.VoucherPatternsReport SET ListTypeID = N'210,260, 261, 263, 264, 262, 402' WHERE ID = 125

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
SET IDENTITY_INSERT dbo.VoucherPatternsReport ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (187, 0, 0, N'UNC_Agribank', N'Ủy nhiệm chi ngân hàng Agribank', N'MBTellerPaper_UNC_Agribank.rst', 1, N'120,121,122,123,124,125,126,127,128')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (188, 0, 0, N'UNC_BIDV', N'Ủy nhiệm chi ngân hàng BIDV', N'MBTellerPaper_UNC_BIDV.rst', 1, N'120,121,122,123,124,125,126,127,128')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (189, 0, 0, N'UNC_Techcombank', N'Ủy nhiệm chi ngân hàng Techcombank', N'MBTellerPaper_UNC_Techcombank.rst', 1, N'120,121,122,123,124,125,126,127,128')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (190, 0, 0, N'UNC_Vietcombank', N'Ủy nhiệm chi ngân hàng Vietcombank', N'MBTellerPaper_UNC_Vietcombank.rst', 1, N'120,121,122,123,124,125,126,127,128')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (191, 0, 0, N'UNC_Vietinbank', N'Ủy nhiệm chi ngân hàng Vietinbank', N'MBTellerPaper_UNC_Vietinbank.rst', 1, N'120,121,122,123,124,125,126,127,128')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (192, 0, 0, N'BangKiemKe', N'Bảng kiểm kê', N'BangKiemKe.rst', 1, N'180')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (193, 0, 10, N'MCReceipt-01-TT-Double', N'Phiếu thu: Mẫu 01-TT (2 liên)', N'MCReceipt-01-TT-Double.rst', 1, N'100,101')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (194, 0, 11, N'MCPayment-02-TT-Double', N'Phiếu chi: Mẫu 02-TT (2 liên)', N'MCPayment-02-TT-Double.rst', 1, N'110,111,112,113,114,115,118,119')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (195, 0, 0, N'BangLuongTamUng', N'Bảng lương tạm ứng', N'BangLuongTamUng.rst', 1, N'834')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (196, 0, 0, N'BangLuongTheoBuoi', N'Bảng lương theo buổi', N'BangLuong.rst', 1, N'830')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (197, 0, 0, N'BangLuongTheoGio', N'Bảng lương theo giờ', N'BangLuong.rst', 1, N'831')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (198, 0, 0, N'BangLuongCoBanCoDinh', N'Bảng lương cơ bản cố định', N'BangLuong.rst', 1, N'832')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (199, 0, 0, N'ChungTuKeToan', N'Chứng từ kế toán', N'ChungTuKeToan.rst', 1, N'840')
GO
SET IDENTITY_INSERT dbo.VoucherPatternsReport OFF
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DBCC CHECKIDENT('dbo.VoucherPatternsReport', RESEED, 199)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.GenCode WITH NOCHECK
  ADD CONSTRAINT FK_GenCode_TypeGroup FOREIGN KEY (TypeGroupID) REFERENCES dbo.TypeGroup(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.Template WITH NOCHECK
  ADD CONSTRAINT FK_Template_Type FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE CASCADE ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateColumn WITH NOCHECK
  ADD CONSTRAINT FK_TemplateColumn_TemplateDetail FOREIGN KEY (TemplateDetailID) REFERENCES dbo.TemplateDetail(ID) ON DELETE CASCADE ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateDetail WITH NOCHECK
  ADD CONSTRAINT FK_TemplateDetail_Template FOREIGN KEY (TemplateID) REFERENCES dbo.Template(ID) ON DELETE CASCADE ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.Type WITH NOCHECK
  ADD CONSTRAINT FK_Type_TypeGroup FOREIGN KEY (TypeGroupID) REFERENCES dbo.TypeGroup(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153AdjustAnnouncement WITH NOCHECK
  ADD CONSTRAINT FK__TT153Adju__TypeI__253C7D7E FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153DeletedInvoice WITH NOCHECK
  ADD CONSTRAINT FK__TT153Dele__TypeI__4EDDB18F FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153DestructionInvoice WITH NOCHECK
  ADD CONSTRAINT FK__TT153Dest__TypeI__22AA2996 FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153LostInvoice WITH NOCHECK
  ADD CONSTRAINT FK__TT153Lost__TypeI__1FCDBCEB FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153PublishInvoice WITH NOCHECK
  ADD CONSTRAINT FK__TT153Publ__TypeI__1CF15040 FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153RegisterInvoice WITH NOCHECK
  ADD CONSTRAINT FK__TT153Regi__TypeI__1A14E395 FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

GO
ENABLE TRIGGER ALL ON dbo.TemplateColumn
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

IF @@TRANCOUNT>0 COMMIT TRANSACTION
GO

SET NOEXEC OFF
GO
