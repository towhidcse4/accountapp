SET CONCAT_NULL_YIELDS_NULL, ANSI_NULLS, ANSI_PADDING, QUOTED_IDENTIFIER, ANSI_WARNINGS, ARITHABORT, XACT_ABORT ON
SET NUMERIC_ROUNDABORT, IMPLICIT_TRANSACTIONS OFF
GO

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

BEGIN TRANSACTION
GO

ALTER TABLE [dbo].[SAPolicyPriceMaterialGoods]
  DROP CONSTRAINT [FK_SAPolicyPriceMaterialGoods_MaterialGoods]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAPolicyPriceTable]
  DROP CONSTRAINT [FK_SAPolicyPriceTable_MaterialGoods]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[VoucherDetailForeignCurrency] (
  [ID] [uniqueidentifier] NOT NULL,
  [ReferenceID] [uniqueidentifier] NULL,
  [GOtherVoucherDetailForeignCurrencyID] [uniqueidentifier] NULL,
  [No] [nvarchar](25) NULL,
  [Date] [datetime] NULL,
  [CreditAmount] [decimal](25, 10) NULL,
  [CreditAmountOriginal] [decimal](25, 10) NULL,
  [DebitAmountOriginal] [decimal](25, 10) NULL,
  [DebitAmount] [decimal](25, 10) NULL,
  [NewExchangeRate] [decimal](25, 10) NULL,
  CONSTRAINT [VoucherDetailForeignCurrency_pk] PRIMARY KEY NONCLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE UNIQUE INDEX [VoucherDetailForeignCurrency_ID_uindex]
  ON [dbo].[VoucherDetailForeignCurrency] ([ID])
  ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[GOtherVoucherDetailExcept] (
  [ID] [uniqueidentifier] NOT NULL,
  [PayRefID] [uniqueidentifier] NULL,
  [PayRefDate] [datetime] NULL,
  [PayRefNo] [nvarchar](25) NULL,
  [PayAmountOriginal] [money] NULL,
  [PayAmount] [money] NULL,
  [DebtRefID] [uniqueidentifier] NULL,
  [DebtRefDate] [datetime] NULL,
  [DebtRefNo] [nvarchar](25) NULL,
  [DebtAmountOriginal] [money] NULL,
  [DebtAmount] [money] NULL,
  [DiffAmount] [int] NULL,
  [GOtherVoucherID] [uniqueidentifier] NULL,
  CONSTRAINT [GOtherVoucherDetailExcept_pk] PRIMARY KEY NONCLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE UNIQUE INDEX [GOtherVoucherDetailExcept_ID_uindex]
  ON [dbo].[GOtherVoucherDetailExcept] ([ID])
  ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TimeSheetSymbols]
  ADD [IsHalfDayDefault] [bit] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PPService]
  ADD [BillReceived] [bit] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MaterialGoods]
  DROP CONSTRAINT [DF__MaterialG__Warra__09FE775D]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MaterialGoods]
  ALTER
    COLUMN [WarrantyTime] [nvarchar](512)
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAPolicyPriceTable] WITH NOCHECK
  ADD CONSTRAINT [FK_SAPolicyPriceTable_MaterialGoods] FOREIGN KEY ([MaterialGoodsID]) REFERENCES [dbo].[MaterialGoods] ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAPolicyPriceMaterialGoods] WITH NOCHECK
  ADD CONSTRAINT [FK_SAPolicyPriceMaterialGoods_MaterialGoods] FOREIGN KEY ([MaterialGoodsID]) REFERENCES [dbo].[MaterialGoods] ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_SoDangKyChungTuGhiSoS02bDNN]
    @FromDate DATETIME,
    @ToDate DATETIME
AS
BEGIN     
	DECLARE @tblS02bDNN	 TABLE(
			SoHieu NVARCHAR(MAX),	
			NgayChungTu Date,
			SoTien decimal(25,0),
			Thang int,
			Quy NCHAR(3),
			Nam int)

                              			
		INSERT INTO @tblS02bDNN
		SELECT a.No,CONVERT(Date,Date,103) as NgayThang, TotalAmount as SoTien,MONTH(Date), 
		(Case  when MONTH(Date) >0 AND MONTH(Date)<=3 then 'I'
				when MONTH(Date) >3 AND MONTH(Date)<=6 then 'II'
				when MONTH(Date) >6 AND MONTH(Date)<=9 then 'III'
				when MONTH(Date) >9 AND MONTH(Date)<=12 then 'IV'
		END) as Quy, YEAR(Date)
		FROM [GvoucherList] a 
		where Date > = @FromDate and Date < = @ToDate
		order by Date

		
		Select * from @tblS02bDNN order by NgayChungTu

		
END
/**/



GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[GOtherVoucherDetailForeignCurrency]
  ADD [DateReevaluate] [datetime] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[GOtherVoucherDetailForeignCurrency]
  ADD [CurrencyID] [nvarchar](3) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[GOtherVoucherDetailDebtPayment]
  ADD [NewExchangeRate] [decimal](25, 10) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[GOtherVoucherDetailDebtPayment]
  ADD [CurrencyID] [nvarchar](3) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @res int
EXEC @res = sp_rename N'[dbo].[EMContract]', N'tmp_devart_EMContract', 'OBJECT'
IF @res <> 0
  RAISERROR ('Error while Rename table [dbo].[EMContract] to [dbo].[tmp_devart_EMContract]', 11, 1 );
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @res int
EXEC @res = sp_rename N'[dbo].[PK__EMContra__3214EC275AEE82B9]', N'tmp_devart_PK__EMContra__3214EC275AEE82B9', 'OBJECT'
IF @res <> 0
  RAISERROR ('Error while Rename index [PK__EMContra__3214EC275AEE82B9] to [tmp_devart_PK__EMContra__3214EC275AEE82B9] on table [dbo].[EMContract]', 11, 1 );
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @res int
EXEC @res = sp_rename N'[dbo].[DF_EMContract_ID]', N'tmp_devart_DF_EMContract_ID', 'OBJECT'
IF @res <> 0
  RAISERROR ('Error while Rename default [DF_EMContract_ID] to [tmp_devart_DF_EMContract_ID] on table [dbo].[EMContract]', 11, 1 );
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[EMContract] (
  [ID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_EMContract_ID] DEFAULT (newid()),
  [BranchID] [uniqueidentifier] NULL,
  [Code] [nvarchar](50) NOT NULL,
  [Name] [nvarchar](512) NOT NULL,
  [Description] [nvarchar](512) NULL,
  [TypeID] [int] NOT NULL,
  [SignedDate] [datetime] NULL,
  [EffectiveDate] [datetime] NULL,
  [CompanyName] [nvarchar](512) NULL,
  [SignerTitle] [nvarchar](512) NULL,
  [SignerName] [nvarchar](512) NULL,
  [CompanyTel] [nvarchar](25) NULL,
  [CompanyAddress] [nvarchar](512) NULL,
  [CompanyFax] [nvarchar](25) NULL,
  [CompanyBankName] [nvarchar](512) NULL,
  [CompanyBankAccountDetailID] [uniqueidentifier] NULL,
  [AccountingObjectID] [uniqueidentifier] NULL,
  [AccountingObjectName] [nvarchar](512) NULL,
  [AccountingObjectAddress] [nvarchar](512) NULL,
  [AccountingObjectSignerName] [nvarchar](512) NULL,
  [AccountingObjectTitle] [nvarchar](512) NULL,
  [AccountingObjectTel] [nvarchar](25) NULL,
  [AccountingObjectFax] [nvarchar](25) NULL,
  [AccountingObjectBankAccountDetailID] [uniqueidentifier] NULL,
  [AccountingObjectBankName] [nvarchar](512) NULL,
  [Amount] [money] NOT NULL,
  [AmountOriginal] [money] NOT NULL,
  [InvoiceAmount] [money] NOT NULL,
  [ReceiptAmount] [money] NOT NULL,
  [PaymentAmount] [money] NOT NULL,
  [PayableAmount] [money] NOT NULL,
  [CurrencyID] [nvarchar](3) NULL,
  [ExchangeRate] [decimal](25, 10) NULL,
  [CostSetID] [uniqueidentifier] NULL,
  [IsSecurity] [bit] NOT NULL,
  [IsClosed] [bit] NOT NULL,
  [ClosedDate] [datetime] NULL,
  [ClosedReason] [nvarchar](512) NULL,
  [IsWatchForCostPrice] [bit] NOT NULL,
  [ReceivingAmount] [money] NOT NULL,
  [ContractGroupID] [uniqueidentifier] NULL,
  [ContractTotalAmount] [money] NOT NULL,
  [ContractTotalAmountOriginal] [money] NOT NULL,
  [ContractTotalAmountPayment] [money] NOT NULL,
  [ContractTotalAmountPaymentOriginal] [money] NOT NULL,
  [ContractEmployeeID] [uniqueidentifier] NULL,
  [IsProject] [bit] NOT NULL,
  [IsBillPaid] [bit] NOT NULL,
  [ProjectID] [uniqueidentifier] NULL,
  [PlantPaymentAmount] [money] NOT NULL,
  [ContractState] [int] NOT NULL,
  [DepartmentID] [uniqueidentifier] NULL,
  [PostedDate] [datetime] NULL,
  [AmountBeforCancel] [money] NOT NULL,
  [OpeningPaymentAmount] [money] NOT NULL,
  [OpeningRecreiptAmount] [money] NOT NULL,
  [Reason] [nvarchar](512) NULL,
  [IsActive] [bit] NOT NULL,
  [RevenueType] [bit] NULL,
  [DeliverDate] [datetime] NULL,
  [DueDate] [datetime] NULL,
  [OrderID] [nvarchar](4000) NULL DEFAULT (NULL)
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

INSERT [dbo].[EMContract](ID, BranchID, Code, Name, Description, TypeID, SignedDate, EffectiveDate, CompanyName, SignerTitle, SignerName, CompanyTel, CompanyAddress, CompanyFax, CompanyBankName, CompanyBankAccountDetailID, AccountingObjectID, AccountingObjectName, AccountingObjectAddress, AccountingObjectSignerName, AccountingObjectTitle, AccountingObjectTel, AccountingObjectFax, AccountingObjectBankAccountDetailID, AccountingObjectBankName, Amount, AmountOriginal, InvoiceAmount, ReceiptAmount, PaymentAmount, PayableAmount, CurrencyID, ExchangeRate, CostSetID, IsSecurity, IsClosed, ClosedDate, ClosedReason, IsWatchForCostPrice, ReceivingAmount, ContractGroupID, ContractTotalAmount, ContractTotalAmountOriginal, ContractTotalAmountPayment, ContractTotalAmountPaymentOriginal, ContractEmployeeID, IsProject, IsBillPaid, OrderID, ProjectID, PlantPaymentAmount, ContractState, DepartmentID, PostedDate, AmountBeforCancel, OpeningPaymentAmount, OpeningRecreiptAmount, Reason, IsActive, RevenueType, DeliverDate, DueDate)
  SELECT ID, BranchID, Code, Name, Description, TypeID, SignedDate, EffectiveDate, CompanyName, SignerTitle, SignerName, CompanyTel, CompanyAddress, CompanyFax, CompanyBankName, CompanyBankAccountDetailID, AccountingObjectID, AccountingObjectName, AccountingObjectAddress, AccountingObjectSignerName, AccountingObjectTitle, AccountingObjectTel, AccountingObjectFax, AccountingObjectBankAccountDetailID, AccountingObjectBankName, Amount, AmountOriginal, InvoiceAmount, ReceiptAmount, PaymentAmount, PayableAmount, CurrencyID, ExchangeRate, CostSetID, IsSecurity, IsClosed, ClosedDate, ClosedReason, IsWatchForCostPrice, ReceivingAmount, ContractGroupID, ContractTotalAmount, ContractTotalAmountOriginal, ContractTotalAmountPayment, ContractTotalAmountPaymentOriginal, ContractEmployeeID, IsProject, IsBillPaid, OrderID, ProjectID, PlantPaymentAmount, ContractState, DepartmentID, PostedDate, AmountBeforCancel, OpeningPaymentAmount, OpeningRecreiptAmount, Reason, IsActive, RevenueType, DeliverDate, DueDate FROM [dbo].[tmp_devart_EMContract] WITH (NOLOCK)

if @@ERROR = 0
  DROP TABLE [dbo].[tmp_devart_EMContract]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[EMContract]
  ADD CONSTRAINT [PK__EMContra__3214EC275AEE82B9] PRIMARY KEY CLUSTERED ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'EMContract', 'COLUMN', N'OrderID'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sp_refreshview '[dbo].[ViewVoucherInvisible]'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sp_refreshview '[dbo].[ViewVouchersCloseBook]'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
CREATE PROCEDURE [dbo].[Proc_GetBkeInv_Buy_Tax]
      (
      @BranchID UNIQUEIDENTIFIER ,
      @IncludeDependentBranch BIT ,
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @IsSimilarBranch BIT,
	  @KhauTru Smallint
    )
AS
BEGIN
/*add by cuongpv de xu ly cach thong nhat lam tron*/
	DECLARE @tbDataLocal TABLE(
		VoucherID UNIQUEIDENTIFIER,
		GoodsServicePurchaseCode NVARCHAR(25),
		GoodsServicePurchaseName NVARCHAR(512),
		So_HD NVARCHAR(25),
		Ngay_HD DATETIME,
		ten_NBan NVARCHAR(512),
		MST NVARCHAR(50),
		Mat_hang NVARCHAR(512),
		GT_chuathue decimal(25,0),
		Thue_suat decimal(25,10),
		Thue_GTGT decimal(25,0),
		TK_Thue NVARCHAR(25),
		flag int,
		AccountingObjectID UNIQUEIDENTIFIER 
	)
/*end add by cuongpv*/
	IF (@KhauTru = 0)
	BEGIN
		if @IsSimilarBranch = 0
		Begin    
			/*add by cuongpv*/
			INSERT INTO @tbDataLocal
			/*end add by cuongpv*/
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD ,AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
			 b.Description as Mat_hang, InwardAmount  as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID                  
			 from PPInvoice a join PPInvoiceDetail b on a.ID = b.PPInvoiceID         
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
			where cast(InvoiceDate As Date) <= @ToDate and Recorded = 1
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all         
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
			 b.Description as Mat_hang, Amount as GiatriHH_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,-1 flag, b.AccountingObjectID as AccountingObjectID   
			from PPDiscountReturn a join PPDiscountReturnDetail b on a.ID = b.PPDiscountReturnID         
			join GoodsServicePurchase c on c.ID= b.GoodsServicePurchaseID         
			where cast(InvoiceDate As Date) <= @ToDate and Recorded = 1
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=a.InvoiceNo) = 0)
			union all    
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,         
			 b.Description as Mat_hang, Amount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID         
			 from PPService a join PPServiceDetail b on a.ID = b.PPServiceID         
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
			where cast(InvoiceDate As Date) <= @ToDate and Recorded = 1 
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
			b.Description as Mat_hang, Amount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID																	
			from FAIncrement a join FAIncrementDetail b on a.ID = b.FAIncrementID																	
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate <= @ToDate and Recorded = 1 and a.TypeID not in (510)
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
			b.Description as Mat_hang, Amount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag, b.AccountingObjectID as AccountingObjectID																
			from TIIncrement a join TIIncrementDetail b on a.ID = b.TIIncrementID																	
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate <= @ToDate and Recorded = 1 and a.TypeID not in (907)
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag,NULL as AccountingObjectID																		
			 from GOtherVoucher a join GOtherVoucherDetailTax b on a.ID = b.GOtherVoucherID		
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate <= @ToDate and Recorded = 1 
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag,NULL as AccountingObjectID																	
			 from MCPayment a join MCPaymentDetailTax b on a.ID = b.MCPaymentID																
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate <= @ToDate and a.Recorded = 1 
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag,NULL as AccountingObjectID																		
			 from MBTellerPaper a join MBTellerPaperDetailTax b on a.ID = b.MBTellerPaperID		
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate <= @ToDate and a.Recorded = 1 
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag,NULL as AccountingObjectID																	
			 from MBCreditCard a join MBCreditCardDetailTax b on a.ID = b.MBCreditCardID		
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate <= @ToDate and a.Recorded = 1
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
		
			/*edit by Hautv */
			UPDATE @tbDataLocal SET ten_NBan = d.AccountingObjectName
			FROM (select * from AccountingObject) d
				where AccountingObjectID = d.ID;

			/*edit by cuongpv order by c.GoodsServicePurchaseCode, InvoiceDate, InvoiceNo -> SELECT * FROM @tbDataLocal order by GoodsServicePurchaseCode, Ngay_HD, So_HD;*/
		
			SELECT * FROM @tbDataLocal order by GoodsServicePurchaseCode, Ngay_HD, So_HD;
		
		End
		else
		Begin
			/*add by cuongpv*/
			INSERT INTO @tbDataLocal
			/*end add by cuongpv*/
			 select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
			 a.Reason as Mat_hang, InwardAmount  as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID         
			 from PPInvoice a join PPInvoiceDetail b on a.ID = b.PPInvoiceID         
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
			where cast(InvoiceDate As Date) <= @ToDate and Recorded = 1
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all         
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
			 a.Reason as Mat_hang, Amount as GiatriHH_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,-1 flag, b.AccountingObjectID as AccountingObjectID         
			from PPDiscountReturn a join PPDiscountReturnDetail b on a.ID = b.PPDiscountReturnID         
			join GoodsServicePurchase c on c.ID= b.GoodsServicePurchaseID         
			where cast(InvoiceDate As Date) <= @ToDate and Recorded = 1
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=a.InvoiceNo) = 0)
			union all    
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,         
			 a.Reason as Mat_hang, Amount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue ,1 flag, b.AccountingObjectID as AccountingObjectID        
			 from PPService a join PPServiceDetail b on a.ID = b.PPServiceID         
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
			where cast(InvoiceDate As Date) <= @ToDate and Recorded = 1 
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
			b.Description as Mat_hang, Amount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID																	
			from FAIncrement a join FAIncrementDetail b on a.ID = b.FAIncrementID																	
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate <= @ToDate and Recorded = 1 and a.TypeID not in (510)
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
			b.Description as Mat_hang, Amount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag, b.AccountingObjectID as AccountingObjectID																
			from TIIncrement a join TIIncrementDetail b on a.ID = b.TIIncrementID																	
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate <= @ToDate and Recorded = 1 and a.TypeID not in (907)
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag,NULL as AccountingObjectID																		
			 from GOtherVoucher a join GOtherVoucherDetailTax b on a.ID = b.GOtherVoucherID		
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate <= @ToDate and Recorded = 1 
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag,NULL as AccountingObjectID																	
			 from MCPayment a join MCPaymentDetailTax b on a.ID = b.MCPaymentID																
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate <= @ToDate and a.Recorded = 1 
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag,NULL as AccountingObjectID																		
			 from MBTellerPaper a join MBTellerPaperDetailTax b on a.ID = b.MBTellerPaperID		
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate <= @ToDate and a.Recorded = 1 
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag,NULL as AccountingObjectID																	
			 from MBCreditCard a join MBCreditCardDetailTax b on a.ID = b.MBCreditCardID		
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate <= @ToDate and a.Recorded = 1
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)

			/*edit by Hautv */
			UPDATE @tbDataLocal SET ten_NBan = d.AccountingObjectName
			FROM (select * from AccountingObject) d
				where AccountingObjectID = d.ID;
		select aa.VoucherID,
			   aa.GoodsServicePurchaseCode,
			   aa.GoodsServicePurchaseName,
			   aa.So_HD,
			   aa.Ngay_HD,
			   aa.ten_NBan,
			   aa.MST,
			   aa.Mat_hang,
			   sum(aa.GT_chuathue) GT_chuathue,
			   aa.Thue_suat ,
			   sum(aa.Thue_GTGT) Thue_GTGT,
			   aa.TK_Thue,
			   flag
		from (
			 select * from @tbDataLocal
			) aa
		group by VoucherID, GoodsServicePurchaseCode,
			   GoodsServicePurchaseName,
			   So_HD,
			   Ngay_HD,
			   ten_NBan,
			   MST,
			   Mat_hang,
			   TK_Thue,
			   flag,
			   aa.Thue_suat
		order by aa.GoodsServicePurchaseCode, aa.Ngay_HD, aa.So_HD
		End
	END
	ELSE/*tinh thue khau tru*/
	BEGIN
		if @IsSimilarBranch = 0
		Begin    
			/*add by cuongpv*/
			INSERT INTO @tbDataLocal
			/*end add by cuongpv*/
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD ,AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
			 b.Description as Mat_hang, InwardAmount  as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID                  
			 from PPInvoice a join PPInvoiceDetail b on a.ID = b.PPInvoiceID         
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
			where cast(InvoiceDate As Date) between @fromDate and @ToDate and Recorded = 1
			union all         
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
			 b.Description as Mat_hang, Amount as GiatriHH_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,-1 flag, b.AccountingObjectID as AccountingObjectID   
			from PPDiscountReturn a join PPDiscountReturnDetail b on a.ID = b.PPDiscountReturnID         
			join GoodsServicePurchase c on c.ID= b.GoodsServicePurchaseID         
			where cast(InvoiceDate As Date) between @fromDate and @ToDate and Recorded = 1
			union all    
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,         
			 b.Description as Mat_hang, Amount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID         
			 from PPService a join PPServiceDetail b on a.ID = b.PPServiceID         
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
			where cast(InvoiceDate As Date) between @fromDate and @ToDate and Recorded = 1 
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
			b.Description as Mat_hang, Amount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID																	
			from FAIncrement a join FAIncrementDetail b on a.ID = b.FAIncrementID																	
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate between @fromDate and @ToDate and Recorded = 1
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
			b.Description as Mat_hang, Amount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag, b.AccountingObjectID as AccountingObjectID																
			from TIIncrement a join TIIncrementDetail b on a.ID = b.TIIncrementID																	
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate between @fromDate and @ToDate and Recorded = 1
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag,NULL as AccountingObjectID																		
			 from GOtherVoucher a join GOtherVoucherDetailTax b on a.ID = b.GOtherVoucherID		
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate between @fromDate and @ToDate and Recorded = 1 
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag,NULL as AccountingObjectID																	
			 from MCPayment a join MCPaymentDetailTax b on a.ID = b.MCPaymentID																
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate between @fromDate and @ToDate and a.Recorded = 1 
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag,NULL as AccountingObjectID																		
			 from MBTellerPaper a join MBTellerPaperDetailTax b on a.ID = b.MBTellerPaperID		
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate between @fromDate and @ToDate and a.Recorded = 1 
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag,NULL as AccountingObjectID																	
			 from MBCreditCard a join MBCreditCardDetailTax b on a.ID = b.MBCreditCardID		
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate between @fromDate and @ToDate and a.Recorded = 1
		
			/*edit by Hautv */
			UPDATE @tbDataLocal SET ten_NBan = d.AccountingObjectName
			FROM (select * from AccountingObject) d
				where AccountingObjectID = d.ID;

			/*edit by cuongpv order by c.GoodsServicePurchaseCode, InvoiceDate, InvoiceNo -> SELECT * FROM @tbDataLocal order by GoodsServicePurchaseCode, Ngay_HD, So_HD;*/
		
			SELECT * FROM @tbDataLocal order by GoodsServicePurchaseCode, Ngay_HD, So_HD;
		
		End
		else
		Begin
			/*add by cuongpv*/
			INSERT INTO @tbDataLocal
			/*end add by cuongpv*/
			 select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
			 a.Reason as Mat_hang, InwardAmount  as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID         
			 from PPInvoice a join PPInvoiceDetail b on a.ID = b.PPInvoiceID         
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
			where cast(InvoiceDate As Date) between @fromDate and @ToDate and Recorded = 1
			union all         
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
			 a.Reason as Mat_hang, Amount as GiatriHH_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,-1 flag, b.AccountingObjectID as AccountingObjectID         
			from PPDiscountReturn a join PPDiscountReturnDetail b on a.ID = b.PPDiscountReturnID         
			join GoodsServicePurchase c on c.ID= b.GoodsServicePurchaseID         
			where cast(InvoiceDate As Date) between @fromDate and @ToDate and Recorded = 1
			union all    
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,         
			 a.Reason as Mat_hang, Amount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue ,1 flag, b.AccountingObjectID as AccountingObjectID        
			 from PPService a join PPServiceDetail b on a.ID = b.PPServiceID         
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
			where cast(InvoiceDate As Date) between @fromDate and @ToDate and Recorded = 1 
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
			b.Description as Mat_hang, Amount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID																	
			from FAIncrement a join FAIncrementDetail b on a.ID = b.FAIncrementID																	
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate between @fromDate and @ToDate and Recorded = 1
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
			b.Description as Mat_hang, Amount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag, b.AccountingObjectID as AccountingObjectID																
			from TIIncrement a join TIIncrementDetail b on a.ID = b.TIIncrementID																	
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate between @fromDate and @ToDate and Recorded = 1
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag,NULL as AccountingObjectID																		
			 from GOtherVoucher a join GOtherVoucherDetailTax b on a.ID = b.GOtherVoucherID		
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate between @fromDate and @ToDate and Recorded = 1 
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag,NULL as AccountingObjectID																	
			 from MCPayment a join MCPaymentDetailTax b on a.ID = b.MCPaymentID																
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate between @fromDate and @ToDate and a.Recorded = 1 
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag,NULL as AccountingObjectID																		
			 from MBTellerPaper a join MBTellerPaperDetailTax b on a.ID = b.MBTellerPaperID		
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate between @fromDate and @ToDate and a.Recorded = 1 
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag,NULL as AccountingObjectID																	
			 from MBCreditCard a join MBCreditCardDetailTax b on a.ID = b.MBCreditCardID		
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate between @fromDate and @ToDate and a.Recorded = 1

			/*edit by Hautv */
			UPDATE @tbDataLocal SET ten_NBan = d.AccountingObjectName
			FROM (select * from AccountingObject) d
				where AccountingObjectID = d.ID;
		select aa.VoucherID,
			   aa.GoodsServicePurchaseCode,
			   aa.GoodsServicePurchaseName,
			   aa.So_HD,
			   aa.Ngay_HD,
			   aa.ten_NBan,
			   aa.MST,
			   aa.Mat_hang,
			   sum(aa.GT_chuathue) GT_chuathue,
			   aa.Thue_suat ,
			   sum(aa.Thue_GTGT) Thue_GTGT,
			   aa.TK_Thue,
			   flag
		from (
			 select * from @tbDataLocal
			) aa
		group by VoucherID, GoodsServicePurchaseCode,
			   GoodsServicePurchaseName,
			   So_HD,
			   Ngay_HD,
			   ten_NBan,
			   MST,
			   Mat_hang,
			   TK_Thue,
			   flag,
			   aa.Thue_suat
		order by aa.GoodsServicePurchaseCode, aa.Ngay_HD, aa.So_HD
		End
	END
end
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_GetBkeInv_Buy]
      (
      @BranchID UNIQUEIDENTIFIER ,
      @IncludeDependentBranch BIT ,
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @IsSimilarBranch BIT
    )
AS
BEGIN
/*add by cuongpv de xu ly cach thong nhat lam tron*/
	DECLARE @tbDataLocal TABLE(
		GoodsServicePurchaseCode NVARCHAR(25),
		GoodsServicePurchaseName NVARCHAR(512),
		So_HD NVARCHAR(25),
		Ngay_HD DATETIME,
		ten_NBan NVARCHAR(512),
		MST NVARCHAR(50),
		Mat_hang NVARCHAR(512),
		GT_chuathue decimal(25,0),
		Thue_suat decimal(25,10),
		Thue_GTGT decimal(25,0),
		TK_Thue NVARCHAR(25),
		flag int,
		AccountingObjectID UNIQUEIDENTIFIER 
	)
/*end add by cuongpv*/
	if @IsSimilarBranch = 0
	Begin    
		/*add by cuongpv*/
		INSERT INTO @tbDataLocal
		/*end add by cuongpv*/
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD ,AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
		 b.Description as Mat_hang, InwardAmount  as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID                  
		 from PPInvoice a join PPInvoiceDetail b on a.ID = b.PPInvoiceID         
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
		where a.PostedDate between @FromDate and @ToDate and Recorded = 1
		union all         
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
		 b.Description as Mat_hang, Amount as GiatriHH_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,-1 flag, b.AccountingObjectID as AccountingObjectID   
		from PPDiscountReturn a join PPDiscountReturnDetail b on a.ID = b.PPDiscountReturnID         
		join GoodsServicePurchase c on c.ID= b.GoodsServicePurchaseID         
		where a.PostedDate between @FromDate and @ToDate and Recorded = 1
		union all    
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,         
		 b.Description as Mat_hang, Amount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID         
		 from PPService a join PPServiceDetail b on a.ID = b.PPServiceID         
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
		where a.PostedDate between @FromDate and @ToDate and Recorded = 1 
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
		b.Description as Mat_hang, Amount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID																	
		from FAIncrement a join FAIncrementDetail b on a.ID = b.FAIncrementID																	
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where a.PostedDate between @FromDate and @ToDate and Recorded = 1 and a.TypeID not in (510)
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
		b.Description as Mat_hang, Amount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag, b.AccountingObjectID as AccountingObjectID																
		from TIIncrement a join TIIncrementDetail b on a.ID = b.TIIncrementID																	
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where a.PostedDate between @FromDate and @ToDate and Recorded = 1 and a.TypeID not in (907)
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
		 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag,NULL as AccountingObjectID																		
		 from GOtherVoucher a join GOtherVoucherDetailTax b on a.ID = b.GOtherVoucherID		
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where a.PostedDate between @FromDate and @ToDate and Recorded = 1 
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
		 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag,NULL as AccountingObjectID																	
		 from MCPayment a join MCPaymentDetailTax b on a.ID = b.MCPaymentID																
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where a.PostedDate between @FromDate and @ToDate and a.Recorded = 1 
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
		 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag,NULL as AccountingObjectID																		
		 from MBTellerPaper a join MBTellerPaperDetailTax b on a.ID = b.MBTellerPaperID		
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where a.PostedDate between @FromDate and @ToDate and a.Recorded = 1 
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
		 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag,NULL as AccountingObjectID																	
		 from MBCreditCard a join MBCreditCardDetailTax b on a.ID = b.MBCreditCardID		
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where a.PostedDate between @FromDate and @ToDate and a.Recorded = 1;
		
		/*edit by Hautv */
		UPDATE @tbDataLocal SET ten_NBan = d.AccountingObjectName
		FROM (select * from AccountingObject) d
			where AccountingObjectID = d.ID;
		/*edit by cuongpv order by c.GoodsServicePurchaseCode, InvoiceDate, InvoiceNo -> SELECT * FROM @tbDataLocal order by GoodsServicePurchaseCode, Ngay_HD, So_HD;*/
		
		SELECT * FROM @tbDataLocal order by GoodsServicePurchaseCode, Ngay_HD, So_HD;
		
	End
	else
	Begin
		/*add by cuongpv*/
		INSERT INTO @tbDataLocal
		/*end add by cuongpv*/
		 select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
		 a.Reason as Mat_hang, (Amount - DiscountAmount) as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID
		 from PPInvoice a join PPInvoiceDetail b on a.ID = b.PPInvoiceID         
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
		where a.PostedDate between @FromDate and @ToDate and Recorded = 1
		union all         
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
		 a.Reason as Mat_hang, Amount as GiatriHH_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,-1 flag, b.AccountingObjectID as AccountingObjectID         
		from PPDiscountReturn a join PPDiscountReturnDetail b on a.ID = b.PPDiscountReturnID         
		join GoodsServicePurchase c on c.ID= b.GoodsServicePurchaseID         
		where a.PostedDate between @FromDate and @ToDate and Recorded = 1
		union all    
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,         
		 a.Reason as Mat_hang, Amount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue ,1 flag, b.AccountingObjectID as AccountingObjectID        
		 from PPService a join PPServiceDetail b on a.ID = b.PPServiceID         
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
		where a.PostedDate between @FromDate and @ToDate and Recorded = 1 
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
		b.Description as Mat_hang, Amount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID																	
		from FAIncrement a join FAIncrementDetail b on a.ID = b.FAIncrementID																	
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where a.PostedDate between @FromDate and @ToDate and Recorded = 1 and a.TypeID not in (510)
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
		b.Description as Mat_hang, Amount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag, b.AccountingObjectID as AccountingObjectID																
		from TIIncrement a join TIIncrementDetail b on a.ID = b.TIIncrementID																	
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where a.PostedDate between @FromDate and @ToDate and Recorded = 1 and a.TypeID not in (907)
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
		 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag,NULL as AccountingObjectID																		
		 from GOtherVoucher a join GOtherVoucherDetailTax b on a.ID = b.GOtherVoucherID		
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where a.PostedDate between @FromDate and @ToDate and Recorded = 1 
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
		 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag,NULL as AccountingObjectID																	
		 from MCPayment a join MCPaymentDetailTax b on a.ID = b.MCPaymentID																
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where a.PostedDate between @FromDate and @ToDate and a.Recorded = 1 
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
		 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag,NULL as AccountingObjectID																		
		 from MBTellerPaper a join MBTellerPaperDetailTax b on a.ID = b.MBTellerPaperID		
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where a.PostedDate between @FromDate and @ToDate and a.Recorded = 1 
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
		 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag,NULL as AccountingObjectID																	
		 from MBCreditCard a join MBCreditCardDetailTax b on a.ID = b.MBCreditCardID		
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where a.PostedDate between @FromDate and @ToDate and a.Recorded = 1
		/*edit by Hautv */
		UPDATE @tbDataLocal SET ten_NBan = d.AccountingObjectName
		FROM (select * from AccountingObject) d
			where AccountingObjectID = d.ID;
	select aa.GoodsServicePurchaseCode,
		   aa.GoodsServicePurchaseName,
		   aa.So_HD,
		   aa.Ngay_HD,
		   aa.ten_NBan,
		   aa.MST,
		   aa.Mat_hang,
		   sum(aa.GT_chuathue) GT_chuathue,
		   aa.Thue_suat ,
		   sum(aa.Thue_GTGT) Thue_GTGT,
		   aa.TK_Thue,
		   flag
	from (
		 select * from @tbDataLocal
		) aa
	group by GoodsServicePurchaseCode,
		   GoodsServicePurchaseName,
		   So_HD,
		   Ngay_HD,
		   ten_NBan,
		   MST,
		   Mat_hang,
		   TK_Thue,
		   flag,
		   aa.Thue_suat
	order by aa.GoodsServicePurchaseCode, aa.Ngay_HD, aa.So_HD
	End
	
end
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_GetBalanceAccountF01]
      ( @FromDate DATETIME ,
        @ToDate DATETIME ,
        @MaxAccountGrade INT,
		@IsBalanceBothSide BIT
		)
AS
BEGIN
 
SELECT t.AccountID ,
		       t.AccountCategoryKind,
			   t.AccountNumber,
			   t.AccountName,
			   sum(t.OpeningDebitAmount) OpeningDebitAmount,
			   sum(t.OpeningCreditAmount) OpeningCreditAmount,
			   sum(t.DebitAmount) DebitAmount,
			   sum(t.CreditAmount) CreditAmount,
			   sum(t.DebitAmountAccum) DebitAmountAccum,
			   sum(t.CreditAmountAccum) CreditAmountAccum,
			   sum(t.ClosingDebitAmount) ClosingDebitAmount,
			   sum(t.ClosingCreditAmount) ClosingCreditAmount
			   FROM [dbo].[Func_GetBalanceAccountF01] (
   @FromDate
  ,@ToDate
  ,@MaxAccountGrade
  ,@IsBalanceBothSide) t
  group by t.AccountID,
		       t.AccountCategoryKind,
			   t.AccountNumber,
			   t.AccountName
  order by t.AccountNumber
end
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_GetHealthFinanceMoney]
      ( @FromDate DATETIME ,
        @ToDate DATETIME
		)
AS
BEGIN
	DECLARE @tbDataReturn TABLE(
		tySoNo decimal(18,2),
		tyTaiTro decimal(18,2),
		vonLuanChuyen decimal(18,2),
		hsTTNganHan decimal(18,2),
		hsTTNhanh decimal(18,2),
		hsTTTucThoi decimal(18,2),
		hsTTChung decimal(18,2),
		hsQVHangTonKho decimal(18,2),
		hsLNTrenVonKD decimal(18,2),
		hsLNTrenDTThuan decimal(18,2),
		hsLNTrenVonCSH decimal(18,2),
		tienMat money,
		tienGui money,
		doanhThu money,
		chiPhi money,
		loiNhuanTruocThue money,
		phaiThu money,
		phaiTra money,
		hangTonKho money
	)
	
	INSERT INTO @tbDataReturn(tySoNo,tyTaiTro,vonLuanChuyen,hsTTNganHan,hsTTNhanh,hsTTTucThoi,hsTTChung,hsQVHangTonKho,hsLNTrenVonKD,hsLNTrenDTThuan,
	hsLNTrenVonCSH,tienMat,tienGui,doanhThu,chiPhi,loiNhuanTruocThue,phaiThu,phaiTra,hangTonKho)
	Values(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null)
	
	DECLARE @tbDataGetB01DN TABLE(
	  ItemID UNIQUEIDENTIFIER ,
      ItemCode NVARCHAR(25) ,
      ItemName NVARCHAR(255) ,
      ItemNameEnglish NVARCHAR(255) ,
      ItemIndex INT ,
      Description NVARCHAR(255) ,
      FormulaType INT ,
      FormulaFrontEnd NVARCHAR(MAX) ,
      Formula XML ,
      Hidden BIT ,
      IsBold BIT ,
      IsItalic BIT ,
      Category INT ,
      SortOrder INT,
      Amount DECIMAL(25, 4) ,
      PrevAmount DECIMAL(25, 4)
	)
	
	INSERT INTO @tbDataGetB01DN
	SELECT * FROM [dbo].[Func_GetB01_DN] (null,null,@FromDate,@ToDate,0,1,0,@FromDate,@ToDate)
	
	
	DECLARE @tbDataGetBalanceAccountF01 TABLE(
		AccountID UNIQUEIDENTIFIER ,
        AccountCategoryKind INT ,
        AccountNumber NVARCHAR(50) ,
        AccountName NVARCHAR(255) ,
        OpeningDebitAmount DECIMAL(25, 4) ,
        OpeningCreditAmount DECIMAL(25, 4) ,
        DebitAmount DECIMAL(25, 4) ,
        CreditAmount DECIMAL(25, 4) ,
        DebitAmountAccum DECIMAL(25, 4) ,
        CreditAmountAccum DECIMAL(25, 4) ,
        ClosingDebitAmount DECIMAL(25, 4) ,
        ClosingCreditAmount DECIMAL(25, 4)
	)
	
	INSERT INTO @tbDataGetBalanceAccountF01
	SELECT t.AccountID ,
		       t.AccountCategoryKind,
			   t.AccountNumber,
			   t.AccountName,
			   sum(t.OpeningDebitAmount) OpeningDebitAmount,
			   sum(t.OpeningCreditAmount) OpeningCreditAmount,
			   sum(t.DebitAmount) DebitAmount,
			   sum(t.CreditAmount) CreditAmount,
			   sum(t.DebitAmountAccum) DebitAmountAccum,
			   sum(t.CreditAmountAccum) CreditAmountAccum,
			   sum(t.ClosingDebitAmount) ClosingDebitAmount,
			   sum(t.ClosingCreditAmount) ClosingCreditAmount
			   FROM [dbo].[Func_GetBalanceAccountF01] (@FromDate,@ToDate,1,0) t
	GROUP BY t.AccountID,
		       t.AccountCategoryKind,
			   t.AccountNumber,
			   t.AccountName
	ORDER BY t.AccountNumber
	
	
	Declare @ct400 decimal(25, 4)
	Set @ct400=(select Amount from @tbDataGetB01DN where ItemCode='400')
	
	Declare @ct600 decimal(25, 4)
	Set @ct600=(select Amount from @tbDataGetB01DN where ItemCode='600')
	
	if(ISNULL(@ct600,0)=0)
	begin
		UPDATE @tbDataReturn SET tySoNo = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET tySoNo = (ISNULL(@ct400,0)/ISNULL(@ct600,0))*100
	end
	
	Declare @ct500 decimal(25, 4)
	Set @ct500=(select Amount from @tbDataGetB01DN where ItemCode='500')
	
	if(ISNULL(@ct600,0)=0)
	begin
		UPDATE @tbDataReturn SET tyTaiTro = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET tyTaiTro = (ISNULL(@ct500,0)/ISNULL(@ct600,0))*100
	end
	
	Declare @ct100 decimal(25, 4)
	Set @ct100=(select Amount from @tbDataGetB01DN where ItemCode='100')
	
	Declare @ct410 decimal(25, 4)
	Set @ct410=(select Amount from @tbDataGetB01DN where ItemCode='410')
	
	UPDATE @tbDataReturn SET vonLuanChuyen = (ISNULL(@ct100,0)-ISNULL(@ct410,0))
	
	if(ISNULL(@ct410,0)=0)
	begin
		UPDATE @tbDataReturn SET hsTTNganHan = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsTTNganHan = (ISNULL(@ct100,0)/ISNULL(@ct410,0))
	end
	
	Declare @ct140 decimal(25, 4)
	Set @ct140=(select Amount from @tbDataGetB01DN where ItemCode='140')
	
	if(ISNULL(@ct410,0)=0)
	begin
		UPDATE @tbDataReturn SET hsTTNhanh = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsTTNhanh = (ISNULL(@ct100,0)-ISNULL(@ct140,0))/ISNULL(@ct410,0)
	end
	
	Declare @ct110 decimal(25, 4)
	Set @ct110=(select Amount from @tbDataGetB01DN where ItemCode='110')
	
	Declare @ct120 decimal(25, 4)
	Set @ct120=(select Amount from @tbDataGetB01DN where ItemCode='120')
	
	if(ISNULL(@ct410,0)=0)
	begin
		UPDATE @tbDataReturn SET hsTTTucThoi = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsTTTucThoi = (ISNULL(@ct110,0)+ISNULL(@ct120,0))/ISNULL(@ct410,0)
	end
	
	Declare @ct300 decimal(25, 4)
	Set @ct300=(select Amount from @tbDataGetB01DN where ItemCode='300')
	
	if(ISNULL(@ct400,0)=0)
	begin
		UPDATE @tbDataReturn SET hsTTChung = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsTTChung = (ISNULL(@ct300,0)/ISNULL(@ct400,0))
	end
	
	Declare @vonHH decimal(25, 4)
	Set @vonHH = (select SUM(DebitAmount) from GeneralLedger where Account='632' AND AccountCorresponding='911' AND (PostedDate between @FromDate and @ToDate))
	
	Declare @hangHoaTon decimal(25,4)
	set @hangHoaTon = (select SUM(ISNULL(OpeningDebitAmount,0)+ISNULL(ClosingDebitAmount,0)) from @tbDataGetBalanceAccountF01 
						where AccountNumber in ('152','153','154','155','156','157'))/2
	
	if(ISNULL(@hangHoaTon,0)=0)
	begin
		UPDATE @tbDataReturn SET hsQVHangTonKho = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsQVHangTonKho=(ISNULL(@vonHH,0)/ISNULL(@hangHoaTon,0))*100
	end
	
	Declare @LNsauThue decimal(25,4)
	set @LNsauThue = (select SUM(CreditAmount) from GeneralLedger where Account='4212' AND AccountCorresponding='911' AND (PostedDate between @FromDate and @ToDate))
	
	Declare @ct600namTruoc decimal(25, 4)
	Set @ct600namTruoc=(select PrevAmount from @tbDataGetB01DN where ItemCode='600')
	
	Declare @vonKdBq decimal(25,4)
	set @vonKdBq = (ISNULL(@ct600,0)+ISNULL(@ct600namTruoc,0))/2
	
	if(ISNULL(@vonKdBq,0)=0)
	begin
		UPDATE @tbDataReturn SET hsLNTrenVonKD = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsLNTrenVonKD = (ISNULL(@LNsauThue,0)/ISNULL(@vonKdBq,0))
	end
	
	
	Declare @dtThuan decimal(25,4)
	set @dtThuan = (select SUM(CreditAmount) from GeneralLedger where Account like'511%' AND AccountCorresponding='911' AND (PostedDate between @FromDate and @ToDate))
	
	if(ISNULL(@dtThuan,0)=0)
	begin
		UPDATE @tbDataReturn SET hsLNTrenDTThuan = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsLNTrenDTThuan = (ISNULL(@LNsauThue,0)/ISNULL(@dtThuan,0))
	end
	
	Declare @ct500namtruoc decimal(25, 4)
	Set @ct500namtruoc=(select PrevAmount from @tbDataGetB01DN where ItemCode='500')
	
	Declare @voncsh decimal(25,4)
	set @voncsh = (ISNULL(@ct500,0)+ISNULL(@ct500namtruoc,0))/2
	
	if(ISNULL(@voncsh,0)=0)
	begin
		UPDATE @tbDataReturn SET hsLNTrenVonCSH = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsLNTrenVonCSH = (ISNULL(@LNsauThue,0)/ISNULL(@voncsh,0))
	end
	
	Declare @duNoCuoiKy111 money
	set @duNoCuoiKy111 = (select ClosingDebitAmount from @tbDataGetBalanceAccountF01 where AccountNumber='111')
	
	UPDATE @tbDataReturn SET tienMat = ISNULL(@duNoCuoiKy111,0)
	
	Declare @duNoCuoiKy112 money
	set @duNoCuoiKy112 = (select ClosingDebitAmount from @tbDataGetBalanceAccountF01 where AccountNumber='112')
	
	UPDATE @tbDataReturn SET tienGui = ISNULL(@duNoCuoiKy112,0)
	
	Declare @doanhThu money
	set @doanhThu = (select SUM(CreditAmount - DebitAmount) from GeneralLedger 
						where ((Account like '511%') or (Account like '515%') or (Account like '711%'))
								and (AccountCorresponding<>'911') and (PostedDate between @FromDate and @ToDate))
	
	UPDATE @tbDataReturn SET doanhThu = ISNULL(@doanhThu,0)
	
	Declare @chiPhi money
	set @chiPhi = (select SUM(DebitAmount - CreditAmount) from GeneralLedger
						where ((Account like '632%') or (Account like '642%') or (Account like '635%') or (Account like '811%') or (Account like '821%'))
						and (AccountCorresponding<>'911') and (PostedDate between @FromDate and @ToDate))
	
	UPDATE @tbDataReturn SET chiPhi = ISNULL(@chiPhi,0)
	
	Declare @chiPhiko821 money
	set @chiPhiko821 = (select SUM(DebitAmount - CreditAmount) from GeneralLedger
						where ((Account like '632%') or (Account like '642%') or (Account like '635%') or (Account like '811%'))
						and (AccountCorresponding<>'911') and (PostedDate between @FromDate and @ToDate))
	
	UPDATE @tbDataReturn SET loiNhuanTruocThue = (ISNULL(@doanhThu,0) - ISNULL(@chiPhiko821,0))
	
	Declare @phaiThu money
	set @phaiThu = (select SUM(ClosingDebitAmount) from @tbDataGetBalanceAccountF01 where AccountNumber='131')
	
	UPDATE @tbDataReturn SET phaiThu = (ISNULL(@phaiThu,0))
	
	Declare @phaiTra money
	set @phaiTra = (select SUM(ClosingCreditAmount) from @tbDataGetBalanceAccountF01 where AccountNumber='331')
	
	UPDATE @tbDataReturn SET phaiTra = (ISNULL(@phaiTra,0))
	
	Declare @hangTonKho money
	set @hangTonKho = (select SUM(ClosingDebitAmount) from @tbDataGetBalanceAccountF01 
						where AccountNumber in ('152','153','154','155','156','157'))
	
	UPDATE @tbDataReturn SET hangTonKho = (ISNULL(@hangTonKho,0))
	
	SELECT * FROM @tbDataReturn
end
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_SA_SoTheoDoiLaiLoTheoHoaDon]
    @FromDate DATETIME,
    @ToDate DATETIME
AS
BEGIN          
    
	/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate between @FromDate and @ToDate
		/*end add by cuongpv*/
	
	DECLARE @tbluutru TABLE (
		SAInvoiceID UNIQUEIDENTIFIER,
		DetailID UNIQUEIDENTIFIER,
		InvoiceDate Date,
		InvoiceNo NVARCHAR(25),
		AccountingObjectID UNIQUEIDENTIFIER,
		Reason NVARCHAR(512),
		GiaTriHHDV money,
		ChietKhauBan money,
		ChietKhau_TLGG money,
		TraLai money,
		GiaVon money,
		LaiLo money
    )
    INSERT INTO @tbluutru(SAInvoiceID, DetailID, InvoiceDate,InvoiceNo,AccountingObjectID,Reason)
		SELECT DISTINCT SA.ID, GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason
		FROM @tbDataGL GL LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=GL.DetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
		WHERE (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
		AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAInvoiceDetail)) AND SA.BillRefID is null
		UNION ALL
		SELECT DISTINCT SA.ID, GL.DetailID, SAB.InvoiceDate, SAB.InvoiceNo, GL.AccountingObjectID, GL.Reason
		FROM @tbDataGL GL LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=GL.DetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
		LEFT JOIN SABill SAB ON SAB.ID=SA.BillRefID
		WHERE (SAB.InvoiceDate is not null) AND (SAB.InvoiceNo is not null) AND (SAB.InvoiceNo <> '')
		AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAInvoiceDetail)) AND SA.BillRefID is not null
	

	DECLARE @tbluutruGiaTriHHDV TABLE (
		DetailID UNIQUEIDENTIFIER,
		InvoiceDate Date,
		InvoiceNo NVARCHAR(25),
		AccountingObjectID UNIQUEIDENTIFIER,
		Reason NVARCHAR(512),
		GiaTriHHDV money
    )

	INSERT INTO @tbluutruGiaTriHHDV
	SELECT GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason, SUM(GL.CreditAmount) as GiaTriHHDV
	FROM @tbDataGL GL LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=GL.DetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
	WHERE (GL.Account like '511%') AND (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
	AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAInvoiceDetail)) AND SA.BillRefID is null
	GROUP BY GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason
	UNION ALL
	SELECT GL.DetailID, SAB.InvoiceDate, SAB.InvoiceNo, GL.AccountingObjectID, GL.Reason, SUM(GL.CreditAmount) as GiaTriHHDV
	FROM @tbDataGL GL LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=GL.DetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
	LEFT JOIN SABill SAB ON SAB.ID=SA.BillRefID
	WHERE (GL.Account like '511%') AND (SAB.InvoiceDate is not null) AND (SAB.InvoiceNo is not null) AND (SAB.InvoiceNo <> '')
	AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAInvoiceDetail)) AND SA.BillRefID is not null
	GROUP BY GL.DetailID, SAB.InvoiceDate, SAB.InvoiceNo, GL.AccountingObjectID, GL.Reason

    UPDATE @tbluutru SET GiaTriHHDV = a.GiaTriHHDV
	FROM (
		Select DetailID as IvID, InvoiceDate as IvDate, InvoiceNo as IvNo, AccountingObjectID as AOID, Reason as Re, GiaTriHHDV
		From @tbluutruGiaTriHHDV
	) a
	WHERE DetailID = a.IvID
	

	DECLARE @tbluutruChietKhauBan TABLE (
		DetailID UNIQUEIDENTIFIER,
		InvoiceDate Date,
		InvoiceNo NVARCHAR(25),
		AccountingObjectID UNIQUEIDENTIFIER,
		Reason NVARCHAR(512),
		ChietKhauBan money
    )

	INSERT INTO @tbluutruChietKhauBan
	SELECT GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason, SUM(GL.DebitAmount) as ChietKhauBan
	FROM @tbDataGL GL LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=GL.DetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
	WHERE (GL.Account like '511%') AND (GL.TypeID not in (330,340)) AND (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
	AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAInvoiceDetail)) AND SA.BillRefID is null
	GROUP BY GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason
	UNION ALL
	SELECT GL.DetailID, SAB.InvoiceDate, SAB.InvoiceNo, GL.AccountingObjectID, GL.Reason, SUM(GL.DebitAmount) as ChietKhauBan
	FROM @tbDataGL GL LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=GL.DetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
	LEFT JOIN SABill SAB ON SAB.ID=SA.BillRefID
	WHERE (GL.Account like '511%') AND (GL.TypeID not in (330,340)) AND (SAB.InvoiceDate is not null) AND (SAB.InvoiceNo is not null) AND (SAB.InvoiceNo <> '')
	AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAInvoiceDetail)) AND SA.BillRefID is not null
	GROUP BY GL.DetailID, SAB.InvoiceDate, SAB.InvoiceNo, GL.AccountingObjectID, GL.Reason


	UPDATE @tbluutru SET ChietKhauBan = b.ChietKhauBan
	FROM (
		Select DetailID as IvID, InvoiceDate as IvDate, InvoiceNo as IvNo, AccountingObjectID as AOID, Reason as Re, ChietKhauBan
		From @tbluutruChietKhauBan
	) b
	WHERE DetailID = b.IvID
	
	DECLARE @tbluutruChietKhau_TLGG TABLE (
		DetailID UNIQUEIDENTIFIER,
		InvoiceDate Date,
		InvoiceNo NVARCHAR(25),
		AccountingObjectID UNIQUEIDENTIFIER,
		Reason NVARCHAR(512),
		ChietKhau_TLGG money
    )
	INSERT INTO @tbluutruChietKhau_TLGG
	SELECT GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason, SUM(GL.CreditAmount) as ChietKhau_TLGG
	FROM @tbDataGL GL LEFT JOIN SAReturnDetail SAR ON SAR.ID=GL.DetailID LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=SAR.SAInvoiceDetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
	WHERE (GL.Account like '511%') AND (GL.TypeID in (330,340)) AND (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
	AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAInvoiceDetail)) AND SA.BillRefID is null
	GROUP BY GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason
	UNION ALL
	SELECT GL.DetailID, SAB.InvoiceDate, SAB.InvoiceNo, GL.AccountingObjectID, GL.Reason, SUM(GL.CreditAmount) as ChietKhau_TLGG
	FROM @tbDataGL GL LEFT JOIN SAReturnDetail SAR ON SAR.ID=GL.DetailID LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=SAR.SAInvoiceDetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
	LEFT JOIN SABill SAB ON SAB.ID=SA.BillRefID
	WHERE (GL.Account like '511%') AND (GL.TypeID in (330,340)) AND (SAB.InvoiceDate is not null) AND (SAB.InvoiceNo is not null) AND (SAB.InvoiceNo <> '')
	AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAInvoiceDetail)) AND SA.BillRefID is not null
	GROUP BY GL.DetailID, SAB.InvoiceDate, SAB.InvoiceNo, GL.AccountingObjectID, GL.Reason


	UPDATE @tbluutru SET ChietKhau_TLGG = c.ChietKhau_TLGG
	FROM (
		Select DetailID as IvID, InvoiceDate as IvDate, InvoiceNo as IvNo, AccountingObjectID as AOID, Reason as Re, ChietKhau_TLGG
		From @tbluutruChietKhau_TLGG
	) c
	WHERE DetailID = c.IvID
	

	DECLARE @tbDataTraLai TABLE(
		SAInvoiceDetailID UNIQUEIDENTIFIER,
		TraLai money
	)
	INSERT INTO @tbDataTraLai(SAInvoiceDetailID,TraLai)
		SELECT SAR.SAInvoiceDetailID, SUM(GL.DebitAmount - GL.CreditAmount) as TraLai
		FROM @tbDataGL GL LEFT JOIN SAReturnDetail SAR ON GL.DetailID = SAR.ID
		WHERE (GL.Account like '511%') AND (GL.TypeID=330) AND (SAR.SAInvoiceDetailID in (select DetailID from @tbluutru))
		AND (GL.PostedDate between @FromDate and @ToDate)
		GROUP BY SAR.SAInvoiceDetailID
	

	UPDATE @tbluutru SET TraLai = TL.TraLai
	FROM (select SAInvoiceDetailID, TraLai from @tbDataTraLai) TL
	WHERE DetailID = TL.SAInvoiceDetailID
	
	DECLARE @tbluutruGiaVon TABLE (
		DetailID UNIQUEIDENTIFIER,
		InvoiceDate Date,
		InvoiceNo NVARCHAR(25),
		AccountingObjectID UNIQUEIDENTIFIER,
		Reason NVARCHAR(512),
		GiaVonHangBan money,
		GiaVonTraLai money,
		GiaVon money
    )

	INSERT INTO @tbluutruGiaVon(DetailID, InvoiceDate, InvoiceNo, AccountingObjectID, Reason, GiaVonHangBan)
	SELECT GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason, SUM(GL.DebitAmount - GL.CreditAmount) as GiaVonHangBan
	FROM @tbDataGL GL LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=GL.DetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
	WHERE (GL.Account like '632%') AND (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
	AND (GL.PostedDate between @FromDate and @ToDate) AND SA.BillRefID is null
	GROUP BY GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason
	UNION ALL
	SELECT GL.DetailID, SAB.InvoiceDate, SAB.InvoiceNo, GL.AccountingObjectID, GL.Reason, SUM(GL.DebitAmount - GL.CreditAmount) as GiaVonHangBan
	FROM @tbDataGL GL LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=GL.DetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
	LEFT JOIN SABill SAB ON SAB.ID=SA.BillRefID
	WHERE (GL.Account like '632%') AND (SAB.InvoiceDate is not null) AND (SAB.InvoiceNo is not null) AND (SAB.InvoiceNo <> '')
	AND (GL.PostedDate between @FromDate and @ToDate) AND SA.BillRefID is not null
	GROUP BY GL.DetailID, SAB.InvoiceDate, SAB.InvoiceNo, GL.AccountingObjectID, GL.Reason

	UPDATE @tbluutruGiaVon SET GiaVonTraLai = Gvtl.GiaVonTraLai
	FROM (
		Select SAR.SAInvoiceDetailID, SUM(GL.DebitAmount - GL.CreditAmount) as GiaVonTraLai
		From @tbDataGL GL LEFT JOIN SAReturnDetail SAR ON GL.DetailID = SAR.ID
		Where (GL.Account like '632%') AND (SAR.SAInvoiceDetailID in (select DetailID from @tbluutruGiaVon))
		AND (GL.PostedDate between @FromDate and @ToDate)
		Group By SAR.SAInvoiceDetailID
	) Gvtl
	WHERE DetailID = SAInvoiceDetailID

	UPDATE @tbluutruGiaVon SET GiaVon = ISNULL(GiaVonHangBan,0) + ISNULL(GiaVonTraLai,0)


	UPDATE @tbluutru SET GiaVon = d.GiaVon
	FROM (
		Select DetailID as IvID, InvoiceDate as IvDate, InvoiceNo as IvNo, AccountingObjectID as AOID, Reason as Re, GiaVon
		From @tbluutruGiaVon
	) d
	WHERE DetailID = d.IvID AND InvoiceDate = d.IvDate AND InvoiceNo = d.IvNo AND AccountingObjectID = d.AOID AND Reason = d.Re
		
	
	DECLARE @tbDataReturn TABLE (
		SAInvoiceID UNIQUEIDENTIFIER,
		InvoiceDate Date,
		InvoiceNo NVARCHAR(25),
		AccountingObjectID UNIQUEIDENTIFIER,
		AccountingObjectName NVARCHAR(512),
		Reason NVARCHAR(512),
		GiaTriHHDV money,
		ChietKhauBan money,
		ChietKhau_TLGG money,
		ChietKhau money,
		GiamGia money,
		TraLai money,
		GiaVon money,
		LaiLo money
    )
    
    INSERT INTO @tbDataReturn(SAInvoiceID,InvoiceDate,InvoiceNo,AccountingObjectID,Reason,GiaTriHHDV,ChietKhauBan,ChietKhau_TLGG,TraLai,GiaVon)
    SELECT SAInvoiceID, InvoiceDate,InvoiceNo,AccountingObjectID,Reason,SUM(GiaTriHHDV) as GiaTriHHDV, SUM(ChietKhauBan) as ChietKhauBan,
    SUM(ChietKhau_TLGG) as ChietKhau_TLGG, SUM(TraLai) as TraLai, SUM(GiaVon) as GiaVon
    FROM @tbluutru
    GROUP BY SAInvoiceID, InvoiceDate,InvoiceNo,AccountingObjectID,Reason
    

	INSERT INTO @tbDataReturn(SAInvoiceID,InvoiceDate,InvoiceNo,AccountingObjectID,Reason,GiamGia)
		SELECT SA.ID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason, SUM(GL.DebitAmount - GL.CreditAmount) as GiamGia
		FROM @tbDataGL GL LEFT JOIN SAReturnDetail SAR ON SAR.ID=GL.DetailID LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=SAR.SAInvoiceDetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
		WHERE (GL.Account like '511%') AND (GL.TypeID=340) AND (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
		AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAReturnDetail)) AND SA.BillRefID is null
		GROUP BY SA.ID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason
		UNION ALL
		SELECT SA.ID, SAB.InvoiceDate, SAB.InvoiceNo, GL.AccountingObjectID, GL.Reason, SUM(GL.DebitAmount - GL.CreditAmount) as GiamGia
		FROM @tbDataGL GL LEFT JOIN SAReturnDetail SAR ON SAR.ID=GL.DetailID LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=SAR.SAInvoiceDetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
		LEFT JOIN SABill SAB ON SAB.ID=SA.BillRefID
		WHERE (GL.Account like '511%') AND (GL.TypeID=340) AND (SAB.InvoiceDate is not null) AND (SAB.InvoiceNo is not null) AND (SAB.InvoiceNo <> '')
		AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAReturnDetail)) AND SA.BillRefID is not null
		GROUP BY SA.ID, SAB.InvoiceDate, SAB.InvoiceNo, GL.AccountingObjectID, GL.Reason
    
    UPDATE @tbDataReturn SET AccountingObjectName = AJ.AccountingObjectName
	FROM ( select ID,AccountingObjectName from AccountingObject) AJ
	where AccountingObjectID = AJ.ID
    
    UPDATE @tbDataReturn SET ChietKhau = (ISNULL(ChietKhauBan,0) - ISNULL(ChietKhau_TLGG,0))
    
    UPDATE @tbDataReturn SET LaiLo = (ISNULL(GiaTriHHDV,0) - (ISNULL(ChietKhau,0)+ISNULL(GiamGia,0)+ISNULL(TraLai,0)+ISNULL(GiaVon,0)))
    
    SELECT InvoiceDate,InvoiceNo,AccountingObjectName,Reason,GiaTriHHDV,ChietKhau,GiamGia,TraLai,GiaVon,LaiLo FROM @tbDataReturn
    WHERE ((ISNULL(GiaTriHHDV,0)>0) OR (ISNULL(ChietKhau,0)>0) OR (ISNULL(GiamGia,0)>0)
			OR (ISNULL(TraLai,0)>0) OR (ISNULL(GiaVon,0)>0) OR (ISNULL(LaiLo,0)>0))
	ORDER BY InvoiceDate,InvoiceNo
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_SA_GetSalesDiaryBook]
    @FromDate DATETIME ,
    @ToDate DATETIME,
    @IsDisplayNotReceiptOnly BIT
AS 
    BEGIN 
		
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,0),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,0),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate between  @FromDate and @ToDate
		/*end add by cuongpv*/

        DECLARE @listRefType NVARCHAR(MAX)
        IF @IsDisplayNotReceiptOnly = 1 
            SET @listRefType = '3530,3532,3534,3536,'  
        ELSE 
            SET @listRefType = '3530,3531,3532,3534,3535,3536,3537,3538,3540,3541,3542,3543,3544,3545,3550,3551,3552,3553,3554,3555,'
		
		/*add by cuongpv*/
		DECLARE @tbResults TABLE (
			ngay_CTU datetime,
			ngay_HT datetime,
			So_CTU nvarchar(25),
			Ngay_HD datetime,
			SO_HD nvarchar(25),
			Dien_giai nvarchar(512),
			TurnOverAmountInv decimal(25,0),
			TurnOverAmountFinishedInv decimal(25,0),
			TurnOverAmountServiceInv decimal(25,0),
			TurnOverAmountOther decimal(25,0),
			DiscountAmount decimal(25,0),
			ReturnAmount decimal(25,0),
			ReduceAmount decimal(25,0),
			CustomerID uniqueidentifier,
			CustomerCode nvarchar(25),
			CustomerName nvarchar(512),
			TypeData Char(4)
		)

		INSERT INTO @tbResults(ngay_CTU, ngay_HT, So_CTU, Ngay_HD, SO_HD, Dien_giai, TurnOverAmountInv, TurnOverAmountFinishedInv, TurnOverAmountServiceInv, TurnOverAmountOther
		,DiscountAmount, ReturnAmount, ReduceAmount, CustomerID, TypeData)
		/*lay du lieu SAInvoice: BillRefID is null*/
		SELECT GL.Date as ngay_CTU, GL.PostedDate as ngay_HT, GL.No as So_CTU, GL.InvoiceDate as Ngay_HD, GL.InvoiceNo as SO_HD, GL.Reason as Dien_giai,
				SUM(CASE WHEN GL.Account like '5111%' THEN GL.CreditAmount ELSE $0 END) as TurnOverAmountInv,
				SUM(CASE WHEN GL.Account like '5112%' THEN GL.CreditAmount ELSE $0 END) as TurnOverAmountFinishedInv,
				SUM(CASE WHEN GL.Account like '5113%' THEN GL.CreditAmount ELSE $0 END) as TurnOverAmountServiceInv,
				SUM(CASE WHEN GL.Account like '5118%' THEn GL.CreditAmount ELSE $0 END) as TurnOverAmountOther,
				SUM(CASE WHEN (GL.Account like '5111%' and GL.typeID not in (330,340)) THEN GL.DebitAmount ELSE $0 END) as DiscountAmount,
				0 as ReturnAmount, 0 as ReduceAmount, GL.AccountingObjectID as CustomerID, NULL as TypeData
		FROM @tbDataGL GL LEFT JOIN SAInvoiceDetail SAD ON SAD.ID = GL.DetailID
			LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
		WHERE (GL.PostedDate between @FromDate and @ToDate) AND SA.BillRefID is null
		GROUP BY GL.Date, GL.PostedDate, GL.No, GL.InvoiceDate, GL.InvoiceNo, GL.Reason, GL.AccountingObjectID
		/*lay du lieu SAInvoice: BillRefID is not null*/
		UNION ALL
		SELECT GL.Date as ngay_CTU, GL.PostedDate as ngay_HT, GL.No as So_CTU, SAB.InvoiceDate as Ngay_HD, SAB.InvoiceNo as SO_HD, GL.Reason as Dien_giai,
				SUM(CASE WHEN GL.Account like '5111%' THEN GL.CreditAmount ELSE $0 END) as TurnOverAmountInv,
				SUM(CASE WHEN GL.Account like '5112%' THEN GL.CreditAmount ELSE $0 END) as TurnOverAmountFinishedInv,
				SUM(CASE WHEN GL.Account like '5113%' THEN GL.CreditAmount ELSE $0 END) as TurnOverAmountServiceInv,
				SUM(CASE WHEN GL.Account like '5118%' THEn GL.CreditAmount ELSE $0 END) as TurnOverAmountOther,
				SUM(CASE WHEN (GL.Account like '5111%' and GL.typeID not in (330,340)) THEN GL.DebitAmount ELSE $0 END) as DiscountAmount,
				0 as ReturnAmount, 0 as ReduceAmount, GL.AccountingObjectID as CustomerID, NULL as TypeData
		FROM @tbDataGL GL LEFT JOIN SAInvoiceDetail SAD ON SAD.ID = GL.DetailID
			LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID LEFT JOIN SABill SAB ON SAB.ID=SA.BillRefID
		WHERE (GL.PostedDate between @FromDate and @ToDate) AND SA.BillRefID is not null
		GROUP BY GL.Date, GL.PostedDate, GL.No, SAB.InvoiceDate, SAB.InvoiceNo, GL.Reason, GL.AccountingObjectID
		/*lay gia tri Tra lai giam gia SAInvoice: BillRefID is null*/
		UNION ALL
		SELECT GL.Date as ngay_CTU, GL.PostedDate as ngay_HT, GL.No as So_CTU, GL.InvoiceDate as Ngay_HD, GL.InvoiceNo as SO_HD, GL.Reason as Dien_giai,
				0 as TurnOverAmountInv,
				0 as TurnOverAmountFinishedInv,
				0 as TurnOverAmountServiceInv,
				0 as TurnOverAmountOther,
				SUM(CASE WHEN (GL.Account like '5111%' and GL.typeID in (330,340)) THEN GL.CreditAmount ELSE $0 END) as DiscountAmount,
				SUM(CASE WHEN (GL.Account like '5111%' and GL.typeID = 330) THEN GL.DebitAmount ELSE $0 END) as ReturnAmount,
				SUM(CASE WHEN (GL.Account like '5111%' and GL.typeID = 340) THEN GL.DebitAmount ELSE $0 END) as ReduceAmount, GL.AccountingObjectID as CustomerID, 'TLGG' as TypeData
		FROM @tbDataGL GL LEFT JOIN SAReturnDetail SAR ON SAR.ID=GL.DetailID LEFT JOIN SAInvoiceDetail SAD ON SAD.ID = SAR.SAInvoiceDetailID
			LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
		WHERE (GL.PostedDate between @FromDate and @ToDate) AND SA.BillRefID is null
		GROUP BY GL.Date, GL.PostedDate, GL.No, GL.InvoiceDate, GL.InvoiceNo, GL.Reason, GL.AccountingObjectID
		/*lay gia tri Tra lai giam gia SAInvoice: BillRefID is not null*/
		UNION ALL
		SELECT GL.Date as ngay_CTU, GL.PostedDate as ngay_HT, GL.No as So_CTU, SAB.InvoiceDate as Ngay_HD, SAB.InvoiceNo as SO_HD, GL.Reason as Dien_giai,
				0 as TurnOverAmountInv,
				0 as TurnOverAmountFinishedInv,
				0 as TurnOverAmountServiceInv,
				0 as TurnOverAmountOther,
				SUM(CASE WHEN (GL.Account like '5111%' and GL.typeID in (330,340)) THEN GL.CreditAmount ELSE $0 END) as DiscountAmount,
				SUM(CASE WHEN (GL.Account like '5111%' and GL.typeID = 330) THEN GL.DebitAmount ELSE $0 END) as ReturnAmount,
				SUM(CASE WHEN (GL.Account like '5111%' and GL.typeID = 340) THEN GL.DebitAmount ELSE $0 END) as ReduceAmount, GL.AccountingObjectID as CustomerID, 'TLGG' as TypeData
		FROM @tbDataGL GL LEFT JOIN SAReturnDetail SAR ON SAR.ID=GL.DetailID LEFT JOIN SAInvoiceDetail SAD ON SAD.ID = SAR.SAInvoiceDetailID
			LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID LEFT JOIN SABill SAB ON SAB.ID=SA.BillRefID
		WHERE (GL.PostedDate between @FromDate and @ToDate) AND SA.BillRefID is not null
		GROUP BY GL.Date, GL.PostedDate, GL.No, SAB.InvoiceDate, SAB.InvoiceNo, GL.Reason, GL.AccountingObjectID

		/*update ten doi tuong vao */
		UPDATE @tbResults SET CustomerCode = a.CustomerCode, CustomerName = a.CustomerName
		FROM(
			select ID as cusID, AccountingObjectCode as CustomerCode, AccountingObjectName as CustomerName from AccountingObject
		) a
		WHERE CustomerID = a.cusID

		

       select
				ngay_CTU, 
				ngay_HT, 
				So_CTU,
				Ngay_HD, 
				SO_HD, 
				Dien_giai,
				TurnOverAmountInv + TurnOverAmountFinishedInv + TurnOverAmountServiceInv +
				TurnOverAmountOther as SumTurnOver,
				TurnOverAmountInv ,
				TurnOverAmountFinishedInv ,
                TurnOverAmountServiceInv ,
                TurnOverAmountOther,
                (CASE WHEN TypeData='TLGG' THEN -DiscountAmount ELSE DiscountAmount END) as DiscountAmount ,
				ReturnAmount,
                ReduceAmount,
				(CASE WHEN TypeData='TLGG' THEN ((TurnOverAmountInv + TurnOverAmountFinishedInv + TurnOverAmountServiceInv +
				TurnOverAmountOther) - (-DiscountAmount + ReturnAmount + ReduceAmount)) 
				ELSE ((TurnOverAmountInv + TurnOverAmountFinishedInv + TurnOverAmountServiceInv +
				TurnOverAmountOther) - (DiscountAmount + ReturnAmount + ReduceAmount)) END) as TurnOverPure,
				CustomerCode, 
				CustomerName 
   from @tbResults 
   where ((TurnOverAmountInv + TurnOverAmountFinishedInv + TurnOverAmountServiceInv + TurnOverAmountOther) <> 0) OR ((DiscountAmount + ReturnAmount + ReduceAmount) <> 0)
   ORDER BY ngay_CTU, So_CTU 
    
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_PO_DiaryBook]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @BranchID UNIQUEIDENTIFIER = NULL ,
    @IncludeDependentBranch BIT = 0 ,
    @IsNotPaid BIT = 0
AS 
    BEGIN
        
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate between @FromDate and @ToDate
		/*end add by cuongpv*/
		
        select
		 DetailID,
		 TypeID,
		 PostedDate,
		 RefDate,
		 RefNo,
		 InvoiceDate,
		 InvoiceNo,
		 Description,
		 GoodsAmount,
		 EquipmentAmount,
		 AnotherAccount,
		 AnotherAmount,
		 sum(GoodsAmount) + sum(EquipmentAmount) + sum(AnotherAmount) as PaymentableAmount
		
		from
		(select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description , sum(DebitAmount) as GoodsAmount, 
		0 as EquipmentAmount, null as AnotherAccount, 0 as AnotherAmount,a.TypeID,a.OrderPriority
		  from @tbDataGL a join PPInvoiceDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate /*edit by cuongpv GeneralLedger ->  @tbDataGL*/
		and a.Account =  '156' and AccountCorresponding not in ('3333', '3332') group by  a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , b.Description, a.DetailID,a.TypeID,a.OrderPriority
		union all
		select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description , sum(DebitAmount) as GoodsAmount,
		0 as EquipmentAmount, null as AnotherAccount, 0 as AnotherAmount,a.TypeID,a.OrderPriority
		  from @tbDataGL a join PPServiceDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate /*edit by cuongpv GeneralLedger ->  @tbDataGL*/
		and a.Account =  '156' and AccountCorresponding not in ('3333', '3332') group by a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , b.Description, a.DetailID,a.TypeID,a.OrderPriority
		union all
		select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description, sum(DebitAmount) as GoodsAmount,
		0 as EquipmentAmount, null as AnotherAccount, 0 as AnotherAmount,a.TypeID,a.OrderPriority
		  from @tbDataGL a join TIIncrementDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate /*edit by cuongpv GeneralLedger ->  @tbDataGL*/
		and a.Account =  '156' and AccountCorresponding not in ('3333', '3332') group by a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , b.Description, a.DetailID,a.TypeID,a.OrderPriority

		union all

		select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description ,0 as GoodsAmount, sum(DebitAmount) as EquipmentAmount,
		null as AnotherAccount, 0 as AnotherAmount,a.TypeID,a.OrderPriority
		  from @tbDataGL a join PPInvoiceDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate /*edit by cuongpv GeneralLedger ->  @tbDataGL*/
		and a.Account =  '152' and AccountCorresponding not in ('3333', '3332') group by  a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , b.Description, a.DetailID,a.TypeID,a.OrderPriority
		union all
		select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description ,0 as GoodsAmount, sum(DebitAmount) as EquipmentAmount,
		null as AnotherAccount, 0 as AnotherAmount,a.TypeID,a.OrderPriority
		  from @tbDataGL a join PPServiceDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate /*edit by cuongpv GeneralLedger ->  @tbDataGL*/
		and a.Account =  '152' and AccountCorresponding not in ('3333', '3332') group by a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , b.Description, a.DetailID,a.TypeID,a.OrderPriority
		union all
		select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description,0 as GoodsAmount , sum(DebitAmount) as EquipmentAmount,
		null as AnotherAccount, 0 as AnotherAmount,a.TypeID,a.OrderPriority
		  from @tbDataGL a join TIIncrementDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate /*edit by cuongpv GeneralLedger ->  @tbDataGL*/
		and a.Account =  '152' and AccountCorresponding not in ('3333', '3332') group by a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , b.Description, a.DetailID,a.TypeID,a.OrderPriority
		union all

		select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description,0 as GoodsAmount,0 as EquipmentAmount , a.Account as AnotherAccount, sum(DebitAmount) as AnotherAmount,a.TypeID,a.OrderPriority
		  from @tbDataGL a join PPInvoiceDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate /*edit by cuongpv GeneralLedger ->  @tbDataGL*/
		and a.Account in ('153','154','242') and AccountCorresponding not in ('3333', '3332') and AccountCorresponding in ('331', '111','112')
		group by  a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , a.Account, b.Description, a.DetailID,a.TypeID,a.OrderPriority
		union all
		select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description ,0 as GoodsAmount,0 as EquipmentAmount, a.Account as AnotherAccount, sum(DebitAmount) as AnotherAmount,a.TypeID,a.OrderPriority
		  from @tbDataGL a join PPServiceDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate  /*edit by cuongpv GeneralLedger ->  @tbDataGL*/
		  and a.Account in ('153','154','242') and AccountCorresponding not in ('3333', '3332') and AccountCorresponding in ('331', '111','112')
		group by a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , a.Account, b.Description, a.DetailID,a.TypeID,a.OrderPriority
		union all
		select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description ,0 as GoodsAmount,0 as EquipmentAmount, a.Account as AnotherAccount,sum(DebitAmount) as AnotherAmount,a.TypeID,a.OrderPriority
		  from @tbDataGL a join TIIncrementDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate /*edit by cuongpv GeneralLedger ->  @tbDataGL*/
		and a.Account in ('153','154','242') and AccountCorresponding not in ('3333', '3332') and AccountCorresponding in ('331', '111','112')
		group by a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , a.Account, b.Description, a.DetailID,a.TypeID,a.OrderPriority) t
group by 
         DetailID,
		 TypeID,
		 PostedDate,
		 OrderPriority,
		 RefDate,
		 RefNo,
		 InvoiceDate,
		 InvoiceNo,
		 Description,
		 GoodsAmount,
		 EquipmentAmount,
		 AnotherAccount,
		 AnotherAmount
		 
ORDER BY PostedDate,OrderPriority ,
               RefDate ,RefNo           
   


           
                              
                              
                              
   
    END
    


GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

ALTER PROCEDURE [dbo].[Proc_GLR_GetS03a2_DN]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @CurrencyID NVARCHAR(10) ,
    @AccountNumber NVARCHAR(25) ,
    @BankAccountID UNIQUEIDENTIFIER ,
    @IsSimilarSum BIT
AS
    BEGIN
        SET NOCOUNT ON;     
        
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate BETWEEN @FromDate AND @ToDate
		/*end add by cuongpv*/
		
		CREATE TABLE #Result
            (
              BranchName NVARCHAR(255)  ,
              RefType INT ,
              RefID UNIQUEIDENTIFIER ,
              PostedDate DATETIME ,
              RefNo NVARCHAR(22) ,
              RefDate DATETIME ,
              [Description] NVARCHAR(255) ,
              AccountNumber NVARCHAR(20) ,
              CorrespondingAccountNumber NVARCHAR(20) ,
              Amount MONEY ,
              Col2 MONEY DEFAULT 0 ,
              Col3 MONEY DEFAULT 0 ,
              Col4 MONEY DEFAULT 0 ,
              Col5 MONEY DEFAULT 0 ,
              Col6 MONEY DEFAULT 0 ,
              Col7 MONEY DEFAULT 0 ,
              ColOtherAccount NVARCHAR(20) ,
              AccountNumberList NVARCHAR(120) ,
              /*Nhật ký thu tiền, chi tiền: Sắp xếp không đúng theo thứ tự nhập trên chứng từ */
              SortOrder INT,
              DetailPostOrder int    
            )
                     
         /*tài khoản ngân hàng khác - lấy các phát sinh không chọn tài khoản ngân hàng*/		
        DECLARE @BankAccountOther UNIQUEIDENTIFIER
        SET @BankAccountOther = '12345678-2222-48B8-AE4B-5CF7FA7FB3F5'
		IF(@CurrencyID = 'VND')
		BEGIN
		    INSERT  #Result
                ( BranchName ,
                  RefType ,
                  RefID ,
                  PostedDate ,
                  RefNo ,
                  RefDate ,
                  Description ,
                  AccountNumber ,
                  CorrespondingAccountNumber ,
                  Amount,
                    SortOrder,
                  DetailPostOrder
                )
                SELECT  null as BranchName ,
                        GL.TypeID ,
                        GL.ReferenceID ,
                        GL.PostedDate ,
                        GL.RefNo ,
                        GL.RefDate ,
                        CASE WHEN @IsSimilarSum = 1 THEN GL.Reason
                             ELSE ISNULL(GL.[Description], GL.Reason)
                        END ,
                        @AccountNumber AS AccountNumber ,
                        GL.AccountCorresponding ,
                        GL.CreditAmount AS Amount,
                          /*-Nhật ký thu tiền, chi tiền: Sắp xếp không đúng theo thứ tự nhập trên chứng từ */
                        GL.OrderPriority,
                        null
                FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                        INNER JOIN dbo.Account AS A ON GL.Account = A.AccountNumber
                WHERE   PostedDate BETWEEN @FromDate AND @ToDate
                        AND GL.Account LIKE @AccountNumber + '%'
                        AND GL.CreditAmount <> 0
                        AND ( @BankAccountID IS NULL
                              OR ( (GL.BankAccountDetailID = @BankAccountID
                                   AND @BankAccountID <> @BankAccountOther )
                                 )
                         
                              OR ( (GL.BankAccountDetailID IS  NULL
                                   AND @BankAccountID = @BankAccountOther )
                                 )
                            )
		END
        ELSE
		BEGIN
        INSERT  #Result
                ( BranchName ,
                  RefType ,
                  RefID ,
                  PostedDate ,
                  RefNo ,
                  RefDate ,
                  Description ,
                  AccountNumber ,
                  CorrespondingAccountNumber ,
                  Amount,
                    SortOrder,
                  DetailPostOrder
                )
                SELECT  null as BranchName ,
                        GL.TypeID ,
                        GL.ReferenceID ,
                        GL.PostedDate ,
                        GL.RefNo ,
                        GL.RefDate ,
                        CASE WHEN @IsSimilarSum = 1 THEN GL.Reason
                             ELSE ISNULL(GL.[Description], GL.Reason)
                        END ,
                        @AccountNumber AS AccountNumber ,
                        GL.AccountCorresponding ,
                        GL.CreditAmountOriginal AS Amount,
                          /*-Nhật ký thu tiền, chi tiền: Sắp xếp không đúng theo thứ tự nhập trên chứng từ */
                        GL.OrderPriority,
                        null
                FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                        INNER JOIN dbo.Account AS A ON GL.Account = A.AccountNumber
                WHERE   PostedDate BETWEEN @FromDate AND @ToDate
                        AND GL.Account LIKE @AccountNumber + '%'
                        AND GL.CreditAmount <> 0
                        AND ( @CurrencyID IS NULL
                              OR GL.CurrencyID = @CurrencyID
                            )
                        AND ( @BankAccountID IS NULL
                              OR ( (GL.BankAccountDetailID = @BankAccountID
                                   AND @BankAccountID <> @BankAccountOther )
                                 )
                         
                              OR ( (GL.BankAccountDetailID IS  NULL
                                   AND @BankAccountID = @BankAccountOther )
                                 )
                            )
							END
        IF EXISTS ( SELECT TOP 1
                            *
                    FROM    #Result )
            BEGIN
                DECLARE @AccountNumberAdded NVARCHAR(50)
                DECLARE @AccountNumberList NVARCHAR(MAX)
                DECLARE @Updatesql1 NVARCHAR(MAX)
                DECLARE @Updatesql2 NVARCHAR(MAX)
                DECLARE @tblName NVARCHAR(20)
                DECLARE @colNumber INT
                
                DECLARE @CoresAccountNumberList NVARCHAR(MAX)
        
                SET @tblName = '#Result'
                SET @Updatesql1 = ''
                SET @Updatesql2 = ''
                SET @colNumber = 2
                SET @AccountNumberList = @AccountNumber + ','
                SET @CoresAccountNumberList = ''
		
                DECLARE curAcc CURSOR FAST_FORWARD READ_ONLY
                FOR
                    SELECT DISTINCT TOP 4
                            R.CorrespondingAccountNumber
                    FROM    #Result AS R
                    WHERE   ISNULL(R.CorrespondingAccountNumber, '') <> ''
                    ORDER BY R.CorrespondingAccountNumber
                OPEN curAcc

                FETCH NEXT FROM curAcc INTO @AccountNumberAdded

                WHILE @@FETCH_STATUS = 0
                    BEGIN
                        IF @Updatesql1 <> ''
                            SET @Updatesql1 = @Updatesql1 + ', '
                        SET @Updatesql1 = @Updatesql1 + '[col'
                            + CONVERT(NVARCHAR(10), @colNumber) + ']'
                            + ' = (CASE WHEN ISNULL(CorrespondingAccountNumber,'''') = '''
                            + @AccountNumberAdded
                            + ''' THEN Amount Else 0 END)'                
                        IF @Updatesql2 <> ''
                            SET @Updatesql2 = @Updatesql2 + ' AND '
                        SET @Updatesql2 = @Updatesql2
                            + 'ISNULL(CorrespondingAccountNumber,'''') <> '''
                            + @AccountNumberAdded + ''' '
                        SET @AccountNumberList = @AccountNumberList
                            + @AccountNumberAdded + ','
                            
                            
                        SET @CoresAccountNumberList = @CoresAccountNumberList
                            + ',''' + @AccountNumberAdded + ''''
                        SET @colNumber = @colNumber + 1                          
                        FETCH NEXT FROM curAcc INTO @AccountNumberAdded
                
                    END
                CLOSE curAcc
                DEALLOCATE curAcc
                
			
                SET @Updatesql1 = 'UPDATE #Result SET ' + @Updatesql1
                    +
                    ', [col7] = (CASE WHEN ' + @Updatesql2
                    + ' THEN Amount ELSE 0 END)'
                    +
                    ', [ColOtherAccount] = (CASE WHEN ' + @Updatesql2
                    + ' THEN CorrespondingAccountNumber END)'
                 
       
                EXEC (@Updatesql1)
                UPDATE  #Result
                SET     AccountNumberList = @AccountNumberList          
            END
        
        IF @IsSimilarSum = 1
            BEGIN
                SELECT  BranchName ,
                        RefType ,
                        RefID ,
                        PostedDate ,
                        RefNo ,
                        RefDate ,
                        [Description] ,
                        AccountNumber ,
                        SUM(Amount) AS Amount ,
                        SUM(Col2) AS Col2 ,
                        SUM(Col3) AS Col3 ,
                        SUM(Col4) AS Col4 ,
                        SUM(Col5) AS Col5 ,
                        SUM(Col6) AS Col6 ,
                        SUM(Col7) AS Col7 ,
                        /*ISNULL(ColOtherAccount,
                               Temp1.CorrespondingAccountNumber) AS ColOtherAccount , comment by cuongpv*/
						ColOtherAccount, /*add by cuongpv*/
                        AccountNumberList
                FROM    #Result AS R
                        /*OUTER APPLY ( SELECT TOP 1
                                                CorrespondingAccountNumber
                                      FROM      #Result AS R1
                                      WHERE     R1.RefID = R.RefID
                                                AND ColOtherAccount NOT IN (
                                                STUFF(@CoresAccountNumberList,
                                                      1, 1, '') )
                                      ORDER BY  CorrespondingAccountNumber
                                    ) AS Temp1 comment by cuongpv*/
                GROUP BY BranchName ,
                        RefType ,
                        RefID ,
                        PostedDate ,
                        RefNo ,
                        RefDate ,
                        [Description] ,
                        AccountNumber ,
                        /*ISNULL(ColOtherAccount,
                               Temp1.CorrespondingAccountNumber) , comment by cuongpv*/
						ColOtherAccount, /*add by cuongpv*/
						SortOrder,
                        AccountNumberList
                  /*-Nhật ký thu tiền, chi tiền: Sắp xếp không đúng theo thứ tự nhập trên chứng từ */       
                ORDER BY 
						R.PostedDate ,
						R.SortOrder,
                        R.RefDate ,
                        R.RefNo      
            END
        ELSE
            BEGIN
            
                SELECT  *
                FROM    #Result AS R
                ORDER BY R.PostedDate ,
                        R.RefDate ,
                        R.RefNo ,
                          /*-Nhật ký thu tiền, chi tiền: Sắp xếp không đúng theo thứ tự nhập trên chứng từ */
                        R.SortOrder,
                        R.DetailPostOrder
            END

        DROP TABLE #Result
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

ALTER PROCEDURE [dbo].[Proc_GLR_GetS03a1_DN]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @CurrencyID NVARCHAR(10) ,
    @AccountNumber NVARCHAR(25) ,
    @BankAccountID UNIQUEIDENTIFIER ,
    @IsSimilarSum BIT
AS
    BEGIN
        SET NOCOUNT ON;
		
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate BETWEEN @FromDate AND @ToDate
		/*end add by cuongpv*/

        CREATE TABLE #Result
            (
			
              BranchName NVARCHAR(255)  ,
              RefType INT ,
              RefID UNIQUEIDENTIFIER ,
              PostedDate DATETIME ,
              RefNo NVARCHAR(22) ,
              RefDate DATETIME ,
              [Description] NVARCHAR(255) ,
              AccountNumber NVARCHAR(20) ,
              CorrespondingAccountNumber NVARCHAR(20) ,
              Amount MONEY ,
              Col2 MONEY DEFAULT 0 ,
              Col3 MONEY DEFAULT 0 ,
              Col4 MONEY DEFAULT 0 ,
              Col5 MONEY DEFAULT 0 ,
              Col6 MONEY DEFAULT 0 ,
              Col7 MONEY DEFAULT 0 ,
              ColOtherAccount NVARCHAR(20) ,
              AccountNumberList NVARCHAR(120) ,
                  /*sửa lỗi 139606 -Nhật ký thu tiền, chi tiền: Sắp xếp không đúng theo thứ tự nhập trên chứng từ */
              SortOrder INT,
              DetailPostOrder int,
			  OrderPriority int,
              
            )
                     
         /*tài khoản ngân hàng khác - lấy các phát sinh không chọn tài khoản ngân hàng*/		
        DECLARE @BankAccountOther UNIQUEIDENTIFIER
        SET @BankAccountOther = '12345678-2222-48B8-AE4B-5CF7FA7FB3F5'
        
		IF(@CurrencyID = 'VND')
		BEGIN
		       INSERT  #Result
                ( BranchName ,
                  RefType ,
                  RefID ,
                  PostedDate ,
                  RefNo ,
                  RefDate ,
                  Description ,
                  AccountNumber ,
                  CorrespondingAccountNumber ,
                  Amount,
                  SortOrder,
                  DetailPostOrder
                )
                SELECT  null as BranchName ,
                        GL.TypeID ,
                        GL.ReferenceID ,
                        GL.PostedDate ,
                        GL.RefNo ,
                        GL.RefDate ,
                        CASE WHEN @IsSimilarSum = 1 THEN GL.Reason
                             ELSE ISNULL(GL.[Description], GL.Reason)
                        END ,
                        @AccountNumber AS AccountNumber ,
                        GL.AccountCorresponding ,
                        GL.DebitAmount AS Amount,
                         /*-Nhật ký thu tiền, chi tiền: Sắp xếp không đúng theo thứ tự nhập trên chứng từ */
                        GL.OrderPriority,
                        null
                FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                        INNER JOIN dbo.Account AS A ON GL.Account = A.AccountNumber
                WHERE   PostedDate BETWEEN @FromDate AND @ToDate
                        AND GL.Account LIKE @AccountNumber + '%'
                        AND GL.DebitAmount <> 0
                        AND ( @BankAccountID IS NULL
                              OR ( (GL.BankAccountDetailID = @BankAccountID
                                   AND @BankAccountID <> @BankAccountOther )
                                 )
                         
                              OR ( (GL.BankAccountDetailID IS  NULL
                                   AND @BankAccountID = @BankAccountOther )
                                 )
                            )
		END
		ELSE
		BEGIN
        INSERT  #Result
                ( BranchName ,
                  RefType ,
                  RefID ,
                  PostedDate ,
                  RefNo ,
                  RefDate ,
                  Description ,
                  AccountNumber ,
                  CorrespondingAccountNumber ,
                  Amount,
                  SortOrder,
                  DetailPostOrder
                )
                SELECT  null as BranchName ,
                        GL.TypeID ,
                        GL.ReferenceID ,
                        GL.PostedDate ,
                        GL.RefNo ,
                        GL.RefDate ,
                        CASE WHEN @IsSimilarSum = 1 THEN GL.Reason
                             ELSE ISNULL(GL.[Description], GL.Reason)
                        END ,
                        @AccountNumber AS AccountNumber ,
                        GL.AccountCorresponding ,
                        GL.DebitAmountOriginal AS Amount,
                         /*-Nhật ký thu tiền, chi tiền: Sắp xếp không đúng theo thứ tự nhập trên chứng từ */
                        GL.OrderPriority,
                        null
                FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                        INNER JOIN dbo.Account AS A ON GL.Account = A.AccountNumber
                WHERE   PostedDate BETWEEN @FromDate AND @ToDate
                        AND GL.Account LIKE @AccountNumber + '%'
                        AND GL.DebitAmount <> 0
                        AND ( @CurrencyID IS NULL
                              OR GL.CurrencyID = @CurrencyID
                            )
                        AND ( @BankAccountID IS NULL
                              OR ( (GL.BankAccountDetailID = @BankAccountID
                                   AND @BankAccountID <> @BankAccountOther )
                                 )
                         
                              OR ( (GL.BankAccountDetailID IS  NULL
                                   AND @BankAccountID = @BankAccountOther )
                                 )
                            )
	END
        IF EXISTS ( SELECT TOP 1
                            *
                    FROM    #Result )
            BEGIN
                DECLARE @AccountNumberAdded NVARCHAR(50)
                DECLARE @AccountNumberList NVARCHAR(MAX)
                DECLARE @Updatesql1 NVARCHAR(MAX)
                DECLARE @Updatesql2 NVARCHAR(MAX)
                DECLARE @tblName NVARCHAR(20)
                DECLARE @colNumber INT
                
                DECLARE @CoresAccountNumberList NVARCHAR(MAX)
        
                SET @tblName = '#Result'
                SET @Updatesql1 = ''
                SET @Updatesql2 = ''
                SET @colNumber = 2
                SET @AccountNumberList = @AccountNumber + ','
                SET @CoresAccountNumberList = ''
		
                DECLARE curAcc CURSOR FAST_FORWARD READ_ONLY
                FOR
                    SELECT DISTINCT TOP 4
                            R.CorrespondingAccountNumber
                    FROM    #Result AS R
                    WHERE   ISNULL(R.CorrespondingAccountNumber, '') <> ''
                    ORDER BY R.CorrespondingAccountNumber
                OPEN curAcc

                FETCH NEXT FROM curAcc INTO @AccountNumberAdded

                WHILE @@FETCH_STATUS = 0
                    BEGIN
                        IF @Updatesql1 <> ''
                            SET @Updatesql1 = @Updatesql1 + ', '
                        SET @Updatesql1 = @Updatesql1 + '[col'
                            + CONVERT(NVARCHAR(10), @colNumber) + ']'
                            + ' = (CASE WHEN ISNULL(CorrespondingAccountNumber,'''') = '''
                            + @AccountNumberAdded
                            + ''' THEN Amount Else 0 END)'                
                        IF @Updatesql2 <> ''
                            SET @Updatesql2 = @Updatesql2 + ' AND '
                        SET @Updatesql2 = @Updatesql2
                            + 'ISNULL(CorrespondingAccountNumber,'''') <> '''
                            + @AccountNumberAdded + ''' '
                        SET @AccountNumberList = @AccountNumberList
                            + @AccountNumberAdded + ','
                            
                            
                        SET @CoresAccountNumberList = @CoresAccountNumberList
                            + ',''' + @AccountNumberAdded + ''''
                        SET @colNumber = @colNumber + 1                          
                        FETCH NEXT FROM curAcc INTO @AccountNumberAdded
                
                    END
                CLOSE curAcc
                DEALLOCATE curAcc
                
			
                SET @Updatesql1 = 'UPDATE #Result SET ' + @Updatesql1
                    +
                    ', [col7] = (CASE WHEN ' + @Updatesql2
                    + ' THEN Amount ELSE 0 END)'
                    +
                    ', [ColOtherAccount] = (CASE WHEN ' + @Updatesql2
                    + ' THEN CorrespondingAccountNumber END)'
                 
        
                EXEC (@Updatesql1)
                UPDATE  #Result
                SET     AccountNumberList = @AccountNumberList          
            END
        
        IF @IsSimilarSum = 1
            BEGIN
                SELECT  BranchName ,
                        RefType ,
                        RefID ,
                        PostedDate ,
                        RefNo ,
                        RefDate ,
                        [Description] ,
                        AccountNumber ,
                        SUM(Amount) AS Amount ,
                        SUM(Col2) AS Col2 ,
                        SUM(Col3) AS Col3 ,
                        SUM(Col4) AS Col4 ,
                        SUM(Col5) AS Col5 ,
                        SUM(Col6) AS Col6 ,
                        SUM(Col7) AS Col7 ,
                        /*ISNULL(ColOtherAccount,
                               Temp1.CorrespondingAccountNumber) AS ColOtherAccount , comment by cuongpv*/
						ColOtherAccount, /*add by cuongpv*/
                        AccountNumberList
                FROM    #Result AS R
                        /*OUTER APPLY ( SELECT TOP 1
                                                CorrespondingAccountNumber
                                      FROM      #Result AS R1
                                      WHERE     R1.RefID = R.RefID
                                                AND ColOtherAccount NOT IN (
                                                STUFF(@CoresAccountNumberList,
                                                      1, 1, '') )
                                      ORDER BY  CorrespondingAccountNumber
                                    ) AS Temp1 comment by cuongpv*/
                GROUP BY BranchName ,
                        RefType ,
                        RefID ,
                        PostedDate ,
                        RefNo ,
                        RefDate ,
                        [Description] ,
                        AccountNumber ,
                        /*ISNULL(ColOtherAccount,
                               Temp1.CorrespondingAccountNumber) , commnet by cuongpv*/
						ColOtherAccount,
						OrderPriority, /*add by cuongpv*/
                        AccountNumberList
                     /* -Nhật ký thu tiền, chi tiền: Sắp xếp không đúng theo thứ tự nhập trên chứng từ */       
                ORDER BY 
						R.PostedDate ,
						R.OrderPriority,
                        R.RefDate ,
                        R.RefNo             
                       
                                    
            
            END
        ELSE
            BEGIN
            
            
                SELECT  *
                FROM    #Result AS R                                                
                ORDER BY R.PostedDate ,
                        R.RefDate ,
                        R.RefNo ,
                        /*-Nhật ký thu tiền, chi tiền: Sắp xếp không đúng theo thứ tự nhập trên chứng từ */
                        R.SortOrder,
                        R.DetailPostOrder
            END

        DROP TABLE #Result
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_GetHealthFinanceChart]
      ( @FromDate DATETIME ,
        @ToDate DATETIME
		)
AS
BEGIN
	DECLARE @tbTemp TABLE(
		PostedDate datetime,
		PostedDay int,
		PostedMonth int,
		PostedYear int,
		Account nvarchar(25),
		AccountCorresponding nvarchar(25),
		DebitAmount decimal(25,0),
		CreditAmount decimal(25,0)
	)
	
	INSERT INTO @tbTemp
	SELECT PostedDate, DAY(PostedDate) as Ngay, MONTH(PostedDate) as Thang, YEAR(PostedDate) as Nam, Account, AccountCorresponding, 
	DebitAmount, CreditAmount
	FROM GeneralLedger
	WHERE PostedDate between @FromDate AND @ToDate
	
	Declare @tbDataTempChart TABLE(
		PostedMonth int,
		PostedYear int,
		TurnoverTotal money,
		CostsTotal money
	)
	
	INSERT INTO @tbDataTempChart(PostedMonth,PostedYear,TurnoverTotal)
	SELECT PostedMonth, PostedYear, SUM(CreditAmount-DebitAmount) as TurnoverTotal
	FROM @tbTemp
	WHERE ((Account like '511%') OR (Account like '515%') OR (Account like '711%')) AND AccountCorresponding <> '911'
	GROUP BY PostedMonth, PostedYear
	
	INSERT INTO @tbDataTempChart(PostedMonth,PostedYear,CostsTotal)
	SELECT PostedMonth, PostedYear, SUM(DebitAmount-CreditAmount) as CostsTotal
	FROM @tbTemp
	WHERE ((Account like '632%') OR (Account like '642%') OR (Account like '635%') OR (Account like '811%') OR (Account like '821%')) 
	AND AccountCorresponding <> '911'
	GROUP BY PostedMonth, PostedYear
	
	Declare @tbDataChart TABLE(
		PostedMonthYear nvarchar(15),
		PostedMonth int,
		PostedYear int,
		TurnoverTotal money,
		CostsTotal money
	)
	
	INSERT INTO @tbDataChart(PostedMonth,PostedYear,TurnoverTotal,CostsTotal)
	SELECT PostedMonth,PostedYear,SUM(TurnoverTotal) as TurnoverTotal, SUM(CostsTotal) as CostsTotal
	FROM @tbDataTempChart
	GROUP BY PostedMonth,PostedYear
	
	UPDATE @tbDataChart SET TurnoverTotal=0 WHERE  TurnoverTotal<0
	UPDATE @tbDataChart SET CostsTotal=0 WHERE  CostsTotal<0
	
	UPDATE @tbDataChart SET PostedMonthYear=CONVERT(nvarchar(2),PostedMonth)+'/'+CONVERT(nvarchar(4),PostedYear)
	
	SELECT PostedMonthYear,TurnoverTotal,CostsTotal FROM @tbDataChart
end
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

ALTER PROCEDURE [dbo].[Proc_GetCACashBookInCABook]
    (
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @CurrencyID NVARCHAR(3) ,
      @AccountNumber NVARCHAR(25)
    )
AS
    BEGIN

        CREATE  TABLE #Result
            (
              RowNum INT IDENTITY(1, 1)
                         PRIMARY KEY ,
              RefID UNIQUEIDENTIFIER ,
              RefType INT ,
              RefDate DATETIME ,
              PostedDate DATETIME ,
              ReceiptRefNo NVARCHAR(20)  ,
              PaymentRefNo NVARCHAR(20)  ,
              CashBookPostedDate DATETIME ,
              AccountObjectName NVARCHAR(128)
                 ,
              JournalMemo NVARCHAR(255)  ,
              CurrencyID NVARCHAR(3)  ,
              TotalReceiptFBCurrencyID MONEY ,
              TotalPaymentFBCurrencyID MONEY ,
              ClosingFBCurrencyID MONEY ,
              BranchID UNIQUEIDENTIFIER ,
              BranchName NVARCHAR(128)  ,
              RefTypeName NVARCHAR(100)  ,
              Note NVARCHAR(255)  ,
              CAType INT ,
              IsBold BIT ,
               /* - bổ sung mã nhân viên , tên nhân viên */
              EmployeeCode NVARCHAR(25)  ,
              EmployeeName NVARCHAR(128)  ,
			  OrderPriority int
            )       
       
	   IF(@CurrencyID = 'VND')
	   BEGIN
			/*Add by cuongpv de sua cach lam tron*/
			DECLARE @tbDataGL TABLE(
				ID uniqueidentifier,
				BranchID uniqueidentifier,
				ReferenceID uniqueidentifier,
				TypeID int,
				Date datetime,
				PostedDate datetime,
				No nvarchar(25),
				InvoiceDate datetime,
				InvoiceNo nvarchar(25),
				Account nvarchar(25),
				AccountCorresponding nvarchar(25),
				BankAccountDetailID uniqueidentifier,
				CurrencyID nvarchar(3),
				ExchangeRate decimal(25, 10),
				DebitAmount decimal(25,0),
				DebitAmountOriginal decimal(25,0),
				CreditAmount decimal(25,0),
				CreditAmountOriginal decimal(25,0),
				Reason nvarchar(512),
				Description nvarchar(512),
				VATDescription nvarchar(512),
				AccountingObjectID uniqueidentifier,
				EmployeeID uniqueidentifier,
				BudgetItemID uniqueidentifier,
				CostSetID uniqueidentifier,
				ContractID uniqueidentifier,
				StatisticsCodeID uniqueidentifier,
				InvoiceSeries nvarchar(25),
				ContactName nvarchar(512),
				DetailID uniqueidentifier,
				RefNo nvarchar(25),
				RefDate datetime,
				DepartmentID uniqueidentifier,
				ExpenseItemID uniqueidentifier,
				OrderPriority int,
				IsIrrationalCost bit
			)

			INSERT INTO @tbDataGL
			SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
			/*end add by cuongpv*/

			INSERT  #Result
                ( RefID ,
                  RefType ,
                  RefDate ,
                  PostedDate ,
                  ReceiptRefNo ,
                  PaymentRefNo ,
                  CashBookPostedDate ,
                  AccountObjectName ,
                  JournalMemo ,
                  CurrencyID ,
                  TotalReceiptFBCurrencyID ,
                  TotalPaymentFBCurrencyID ,
                  ClosingFBCurrencyID ,
                  BranchID ,
                  BranchName ,
                  RefTypeName ,
                  CAType ,
                  IsBold ,
                  EmployeeCode ,
                  EmployeeName,
				  OrderPriority
                )
                SELECT  RefID ,
                        RefType ,
                        RefDate ,
                        R.PostedDate ,
                        CASE WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN R.RefNo
                             ELSE NULL
                        END ,
                        CASE WHEN TotalPaymentFBCurrencyID
                                  - TotalReceiptFBCurrencyID > 0 THEN R.RefNo
                             ELSE NULL
                        END ,
                        R.PostedDate ,
                        ContactName ,
                        JournalMemo ,
                        @CurrencyID ,
                        SUM(CASE WHEN TotalReceiptFBCurrencyID
                                      - TotalPaymentFBCurrencyID > 0
                                 THEN TotalReceiptFBCurrencyID
                                      - TotalPaymentFBCurrencyID
                                 ELSE 0
                            END) AS TotalReceiptFBCurrencyID ,
                        SUM(CASE WHEN TotalPaymentFBCurrencyID
                                      - TotalReceiptFBCurrencyID > 0
                                 THEN TotalPaymentFBCurrencyID
                                      - TotalReceiptFBCurrencyID
                                 ELSE 0
                            END) AS TotalPaymentFBCurrencyID ,
                        SUM(ClosingFBCurrencyID) ,
                        BranchID ,
                        BranchName ,
                        RefTypeName ,
                        CASE WHEN R.CAType IS NULL THEN NULL
                             WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN 0
                             ELSE 1
                        END AS CAType ,
                        IsBold ,
                        EmployeeCode ,
                        EmployeeName,
						OrderPriority
                FROM    ( SELECT    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.ReferenceID
                                    END AS RefID ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.TypeID
                                    END AS RefType ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.RefDate
                                    END AS RefDate ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.PostedDate
                                    END AS PostedDate ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.RefNo
                                    END AS RefNo ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.ContactName
                                    END AS ContactName ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN N'Số dư đầu kỳ'
                                         ELSE GL.Description
                                    END AS JournalMemo ,
                                    SUM(CASE WHEN GL.PostedDate >= @FromDate
                                                  AND GL.DebitAmountOriginal
                                                  - GL.CreditAmountOriginal > 0
                                             THEN GL.DebitAmountOriginal
                                                  - GL.CreditAmountOriginal
                                             ELSE 0
                                        END) AS TotalReceiptFBCurrencyID ,
                                    SUM(CASE WHEN GL.PostedDate >= @FromDate
                                                  AND GL.CreditAmountOriginal
                                                  - GL.DebitAmountOriginal > 0
                                             THEN GL.CreditAmountOriginal
                                                  - GL.DebitAmountOriginal
                                             ELSE 0
                                        END) AS TotalPaymentFBCurrencyID ,
                                    SUM(CASE WHEN GL.PostedDate < @FromDate
                                             THEN GL.DebitAmountOriginal
                                                  - GL.CreditAmountOriginal
                                             ELSE 0
                                        END) AS ClosingFBCurrencyID ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.BranchID
                                    END AS BranchID ,
                                    null AS BranchName ,
                                    null RefTypeName ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE 0
                                    END AS CAType ,
                                    CASE WHEN GL.PostedDate < @FromDate THEN 1
                                         ELSE 0
                                    END AS IsBold ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE AO.AccountingObjectCode
                                    END AS EmployeeCode ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE AO.AccountingObjectName
                                    END AS EmployeeName,
									CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE OrderPriority
                                    END AS OrderPriority
                          FROM      @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                    LEFT JOIN dbo.AccountingObject AS AO ON AO.ID = GL.EmployeeID
                          WHERE     ( GL.PostedDate <= @ToDate )
                                    AND ( @CurrencyID IS NULL
                                          OR GL.CurrencyID = @CurrencyID
                                        )
                                    AND GL.Account LIKE @AccountNumber
                                    + '%'
                          GROUP BY  CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.ReferenceID
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.TypeID
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.RefDate
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.PostedDate
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.RefNo
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.PostedDate
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.ContactName
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN N'Số dư đầu kỳ'
                                         ELSE GL.Description
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.BranchID
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE 0
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate THEN 1
                                         ELSE 0
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE AO.AccountingObjectCode
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE AO.AccountingObjectName
                                    END,
									 CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE OrderPriority
                                    END
                          HAVING    SUM(CASE WHEN GL.PostedDate >= @FromDate
                                                  AND GL.DebitAmountOriginal
                                                  - GL.CreditAmountOriginal > 0
                                             THEN GL.DebitAmountOriginal
                                                  - GL.CreditAmountOriginal
                                             ELSE 0
                                        END) <> SUM(CASE WHEN GL.PostedDate >= @FromDate
                                                              AND GL.CreditAmountOriginal
                                                              - GL.DebitAmountOriginal > 0
                                                         THEN GL.CreditAmountOriginal
                                                              - GL.DebitAmountOriginal
                                                         ELSE 0
                                                    END)
                                    OR SUM(CASE WHEN GL.PostedDate < @FromDate
                                                THEN GL.DebitAmountOriginal
                                                     - GL.CreditAmountOriginal
                                                ELSE 0
                                           END) <> 0
                        ) AS R
                GROUP BY R.RefID ,
                        R.RefType ,
                        R.RefDate ,
                        R.PostedDate ,
                        CASE WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN R.RefNo
                             ELSE NULL
                        END ,
                        CASE WHEN TotalPaymentFBCurrencyID
                                  - TotalReceiptFBCurrencyID > 0 THEN R.RefNo
                             ELSE NULL
                        END ,
                        R.RefNo ,
                        R.ContactName ,
                        R.JournalMemo ,
                        R.BranchID ,
                        R.BranchName ,
                        R.RefTypeName ,
                        CASE WHEN R.CAType IS NULL THEN NULL
                             WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN 0
                             ELSE 1
                        END ,
                        R.IsBold ,
                        R.EmployeeCode ,
                        R.EmployeeName,
						R.OrderPriority
                ORDER BY R.PostedDate,R.OrderPriority,
                        R.RefDate ,
                        CASE WHEN R.CAType IS NULL THEN NULL
                             WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN 0
                             ELSE 1
                        END ,
                        R.RefNo	
	   END
	   ELSE
	   BEGIN
			INSERT  #Result
                ( RefID ,
                  RefType ,
                  RefDate ,
                  PostedDate ,
                  ReceiptRefNo ,
                  PaymentRefNo ,
                  CashBookPostedDate ,
                  AccountObjectName ,
                  JournalMemo ,
                  CurrencyID ,
                  TotalReceiptFBCurrencyID ,
                  TotalPaymentFBCurrencyID ,
                  ClosingFBCurrencyID ,
                  BranchID ,
                  BranchName ,
                  RefTypeName ,
                  CAType ,
                  IsBold ,
                  EmployeeCode ,
                  EmployeeName,
				  OrderPriority
                )
                SELECT  RefID ,
                        RefType ,
                        RefDate ,
                        R.PostedDate ,
                        CASE WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN R.RefNo
                             ELSE NULL
                        END ,
                        CASE WHEN TotalPaymentFBCurrencyID
                                  - TotalReceiptFBCurrencyID > 0 THEN R.RefNo
                             ELSE NULL
                        END ,
                        R.PostedDate ,
                        ContactName ,
                        JournalMemo ,
                        @CurrencyID ,
                        SUM(CASE WHEN TotalReceiptFBCurrencyID
                                      - TotalPaymentFBCurrencyID > 0
                                 THEN TotalReceiptFBCurrencyID
                                      - TotalPaymentFBCurrencyID
                                 ELSE 0
                            END) AS TotalReceiptFBCurrencyID ,
                        SUM(CASE WHEN TotalPaymentFBCurrencyID
                                      - TotalReceiptFBCurrencyID > 0
                                 THEN TotalPaymentFBCurrencyID
                                      - TotalReceiptFBCurrencyID
                                 ELSE 0
                            END) AS TotalPaymentFBCurrencyID ,
                        SUM(ClosingFBCurrencyID) ,
                        BranchID ,
                        BranchName ,
                        RefTypeName ,
                        CASE WHEN R.CAType IS NULL THEN NULL
                             WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN 0
                             ELSE 1
                        END AS CAType ,
                        IsBold ,
                        EmployeeCode ,
                        EmployeeName,
						OrderPriority
                FROM    ( SELECT    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.ReferenceID
                                    END AS RefID ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.TypeID
                                    END AS RefType ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.RefDate
                                    END AS RefDate ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.PostedDate
                                    END AS PostedDate ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.RefNo
                                    END AS RefNo ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.ContactName
                                    END AS ContactName ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN N'Số dư đầu kỳ'
                                         ELSE GL.Description
                                    END AS JournalMemo ,
                                    SUM(CASE WHEN GL.PostedDate >= @FromDate
                                                  AND GL.DebitAmountOriginal
                                                  - GL.CreditAmountOriginal > 0
                                             THEN GL.DebitAmountOriginal
                                                  - GL.CreditAmountOriginal
                                             ELSE 0
                                        END) AS TotalReceiptFBCurrencyID ,
                                    SUM(CASE WHEN GL.PostedDate >= @FromDate
                                                  AND GL.CreditAmountOriginal
                                                  - GL.DebitAmountOriginal > 0
                                             THEN GL.CreditAmountOriginal
                                                  - GL.DebitAmountOriginal
                                             ELSE 0
                                        END) AS TotalPaymentFBCurrencyID ,
                                    SUM(CASE WHEN GL.PostedDate < @FromDate
                                             THEN GL.DebitAmountOriginal
                                                  - GL.CreditAmountOriginal
                                             ELSE 0
                                        END) AS ClosingFBCurrencyID ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.BranchID
                                    END AS BranchID ,
                                    null AS BranchName ,
                                    null RefTypeName ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE 0
                                    END AS CAType ,
                                    CASE WHEN GL.PostedDate < @FromDate THEN 1
                                         ELSE 0
                                    END AS IsBold ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE AO.AccountingObjectCode
                                    END AS EmployeeCode ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE AO.AccountingObjectName
                                    END AS EmployeeName,
									CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE OrderPriority
                                    END AS OrderPriority
                          FROM      dbo.GeneralLedger AS GL
                                    LEFT JOIN dbo.AccountingObject AS AO ON AO.ID = GL.EmployeeID
                          WHERE     ( GL.PostedDate <= @ToDate )
                                    AND ( @CurrencyID IS NULL
                                          OR GL.CurrencyID = @CurrencyID
                                        )
                                    AND GL.Account LIKE @AccountNumber
                                    + '%'
                          GROUP BY  CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.ReferenceID
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.TypeID
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.RefDate
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.PostedDate
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.RefNo
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.PostedDate
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.ContactName
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN N'Số dư đầu kỳ'
                                         ELSE GL.Description
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.BranchID
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE 0
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate THEN 1
                                         ELSE 0
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE AO.AccountingObjectCode
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE AO.AccountingObjectName
                                    END,
									 CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE OrderPriority
                                    END
                          HAVING    SUM(CASE WHEN GL.PostedDate >= @FromDate
                                                  AND GL.DebitAmountOriginal
                                                  - GL.CreditAmountOriginal > 0
                                             THEN GL.DebitAmountOriginal
                                                  - GL.CreditAmountOriginal
                                             ELSE 0
                                        END) <> SUM(CASE WHEN GL.PostedDate >= @FromDate
                                                              AND GL.CreditAmountOriginal
                                                              - GL.DebitAmountOriginal > 0
                                                         THEN GL.CreditAmountOriginal
                                                              - GL.DebitAmountOriginal
                                                         ELSE 0
                                                    END)
                                    OR SUM(CASE WHEN GL.PostedDate < @FromDate
                                                THEN GL.DebitAmountOriginal
                                                     - GL.CreditAmountOriginal
                                                ELSE 0
                                           END) <> 0
                        ) AS R
                GROUP BY R.RefID ,
                        R.RefType ,
                        R.RefDate ,
                        R.PostedDate ,
                        CASE WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN R.RefNo
                             ELSE NULL
                        END ,
                        CASE WHEN TotalPaymentFBCurrencyID
                                  - TotalReceiptFBCurrencyID > 0 THEN R.RefNo
                             ELSE NULL
                        END ,
                        R.RefNo ,
                        R.ContactName ,
                        R.JournalMemo ,
                        R.BranchID ,
                        R.BranchName ,
                        R.RefTypeName ,
                        CASE WHEN R.CAType IS NULL THEN NULL
                             WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN 0
                             ELSE 1
                        END ,
                        R.IsBold ,
                        R.EmployeeCode ,
                        R.EmployeeName,
						R.OrderPriority
                ORDER BY R.PostedDate,R.OrderPriority ,
                        R.RefDate ,
                        CASE WHEN R.CAType IS NULL THEN NULL
                             WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN 0
                             ELSE 1
                        END ,
                        R.RefNo	
	   END

        DECLARE @ClosingAmount AS DECIMAL(22, 8)
        SET @ClosingAmount = 0
        SELECT  @ClosingAmount = ClosingFBCurrencyID
        FROM    #Result
        WHERE   RefID IS NULL
        UPDATE  #Result
        SET     @ClosingAmount = @ClosingAmount
                + ISNULL(TotalReceiptFBCurrencyID, 0)
                - ISNULL(TotalPaymentFBCurrencyID, 0) ,
                ClosingFBCurrencyID = @ClosingAmount	
    
        SELECT  *
        FROM    #Result R       
    
        DROP TABLE #Result
        
    END


GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO




ALTER PROCEDURE [dbo].[Proc_BA_GetBookDepositListDetail]
    @BranchID UNIQUEIDENTIFIER ,
    @IncludeDependentBranch BIT ,
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @CurrencyID NVARCHAR(3) ,
    @AccountNumber NVARCHAR(25) ,
    @BankAccountID UNIQUEIDENTIFIER ,
    @IsSimilarSum BIT = 0 ,
    @IsSoftOrderVoucher BIT = 0
AS
    BEGIN
        SET NOCOUNT ON
        
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,0),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,0),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/

        CREATE TABLE #Result
            (
              RowNum INT IDENTITY(1, 1)
                         PRIMARY KEY ,
              RefID UNIQUEIDENTIFIER ,
              BankAccount NVARCHAR(500)  ,
              PostedDate DATETIME ,
              RefDate DATETIME ,
              RefNo NVARCHAR(25)  ,
              RefType INT ,
              JournalMemo NVARCHAR(255)  ,
              CorrespondingAccountNumber NVARCHAR(20)
                 ,
              ExchangeRate DECIMAL ,
              DebitAmountOC DECIMAL ,
              DebitAmount DECIMAL ,
              CreditAmountOC DECIMAL ,
              CreditAmount DECIMAL ,
              ClosingAmountOC DECIMAL ,
              ClosingAmount DECIMAL ,
              AccountObjectCode NVARCHAR(255)
                 ,
              AccountObjectName NVARCHAR(255)
                 ,
              EmployeeCode NVARCHAR(255)  ,
              EmployeeName NVARCHAR(255)  ,
              ProjectWorkCode NVARCHAR(20)
                 ,
              ProjectWorkName NVARCHAR(128)
                 ,
              

              ExpenseItemCode NVARCHAR(20)
                 ,
              ExpenseItemName NVARCHAR(128)
                 ,
              ListItemCode NVARCHAR(20)  ,
              ListItemName NVARCHAR(128)  ,
              ContractCode NVARCHAR(50)  ,

              BranchID UNIQUEIDENTIFIER ,
              BranchCode NVARCHAR(25)  ,
              BranchName NVARCHAR(255)  ,
              IsBold BIT ,
              PaymentType INT ,
              OrderType INT ,
              RefOrder INT,
			  OrderPriority INT
            )	
		
        CREATE TABLE #Result1
            (
              RowNum INT PRIMARY KEY ,
              RefID UNIQUEIDENTIFIER ,
              BankAccount NVARCHAR(500)  ,
              PostedDate DATETIME ,
              RefDate DATETIME ,
              RefNo NVARCHAR(25) ,
              RefType INT ,
              JournalMemo NVARCHAR(255),
              CorrespondingAccountNumber NVARCHAR(20) ,
              ExchangeRate DECIMAL ,
              DebitAmountOC DECIMAL ,
              DebitAmount DECIMAL ,
              CreditAmountOC DECIMAL ,
              CreditAmount DECIMAL ,
              ClosingAmountOC DECIMAL ,
              ClosingAmount DECIMAL ,
              AccountObjectCode NVARCHAR(255)
                 ,
              AccountObjectName NVARCHAR(255)
                 ,
              EmployeeCode NVARCHAR(255)  ,
              EmployeeName NVARCHAR(255)  ,

              ProjectWorkCode NVARCHAR(20) ,
              ProjectWorkName NVARCHAR(128) ,
              

              ExpenseItemCode NVARCHAR(20) ,
              ExpenseItemName NVARCHAR(128) ,
              ListItemCode NVARCHAR(20) ,
              ListItemName NVARCHAR(128)  ,
              ContractCode NVARCHAR(50) ,

              BranchID UNIQUEIDENTIFIER ,
              BranchCode NVARCHAR(25)  ,
              BranchName NVARCHAR(255)  ,
              IsBold BIT ,
              PaymentType INT ,
              OrderType INT ,
              RefOrder INT,
			  OrderPriority INT
            )	
		
        DECLARE @BankAccountAll UNIQUEIDENTIFIER/*tất cả tài khoản ngân hàng*/
        SET @BankAccountAll = 'A0624CFA-D105-422f-BF20-11F246704DC3'
		
        DECLARE @AccountNumberPercent NVARCHAR(25)
        SET @AccountNumberPercent = @AccountNumber + '%'
    
        DECLARE @ClosingAmountOC DECIMAL(29, 4)
        DECLARE @ClosingAmount DECIMAL(29, 4)
   	    DECLARE @tblListBankAccountDetail TABLE
            (
			  ID UNIQUEIDENTIFIER,
              BankAccount NVARCHAR(Max) ,
              BankAccountName NVARCHAR(MAX) 
            ) 
        if(@BankAccountID is null)     
        INSERT  INTO @tblListBankAccountDetail
                SELECT  BAD.ID,
				        BAD.BankAccount,
				        BAD.BankName
                FROM    BankAccountDetail BAD,
				        Bank BA
                WHERE BAD.BankID = BA.ID    
		else 	
			INSERT  INTO @tblListBankAccountDetail
                SELECT  BAD.ID,
				        BAD.BankAccount,
				        BAD.BankName
                FROM    BankAccountDetail BAD,
				        Bank BA
                WHERE BAD.BankID = BA.ID  
				and BAD.ID =  @BankAccountID 
	

        IF @ClosingAmount IS NULL
            SET @ClosingAmount = 0
        IF @ClosingAmountOC IS NULL
            SET @ClosingAmountOC = 0
            DECLARE @BankAccount NVARCHAR(500)
        DECLARE @CloseAmountOC MONEY
        DECLARE @CloseAmount MONEY
	  IF(@CurrencyID = 'VND')
	  BEGIN
	   INSERT  #Result
                ( BankAccount ,
                  JournalMemo ,
                  DebitAmount ,
                  DebitAmountOC ,
                  CreditAmount ,
                  CreditAmountOC ,
                  ClosingAmountOC ,
                  ClosingAmount ,
                  IsBold ,
                  OrderType   
                )
                SELECT  
		                BA.BankAccount + ' - ' + BA.BankAccountName,
                        N'Số dư đầu kỳ' AS JournalMemo ,
                        $0 AS DebitAmountOC ,
                        $0 AS DebitAmount ,
                        $0 AS CreditAmountOC ,
                        $0 AS CreditAmount ,
                        ISNULL(SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal), 0) ,
                        ISNULL(SUM(GL.DebitAmount - GL.CreditAmount), 0) ,
                        1 ,
                        0
                FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
				        JOIN @tblListBankAccountDetail BA on BA.ID = GL.BankAccountDetailID
                WHERE   ( GL.PostedDate < @FromDate )
                        AND GL.Account LIKE @AccountNumberPercent
                GROUP BY BA.BankAccount , BA.BankAccountName
                HAVING  ISNULL(SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal), 0) <> 0
                        OR ISNULL(SUM(GL.DebitAmount - GL.CreditAmount), 0) <> 0

       INSERT  #Result
	   (
              RefID ,
              BankAccount,
              PostedDate  ,
              RefDate  ,
              RefNo  ,
              JournalMemo ,
              CorrespondingAccountNumber,
              ExchangeRate ,
              DebitAmountOC ,
              DebitAmount,
              CreditAmountOC ,
              CreditAmount,
              ClosingAmountOC ,
              ClosingAmount ,
              AccountObjectCode ,
              AccountObjectName ,
              ExpenseItemCode,
              ExpenseItemName,
              BranchID  ,
              IsBold  ,
              PaymentType  ,
              OrderType  ,
              RefOrder,
			  OrderPriority )
                    SELECT  GL.ReferenceID ,
	                        BA.BankAccount + ' - ' + BA.BankAccountName,
                            GL.PostedDate ,
                            GL.RefDate ,
                            GL.RefNo AS RefNoFinance ,

                            GL.Description,
                            GL.AccountCorresponding ,
                            GL.ExchangeRate ,
                            ISNULL(GL.DebitAmountOriginal, 0) AS DebitAmountOC ,
                            ISNULL(GL.DebitAmount, 0) AS DebitAmount ,
                            ISNULL(GL.CreditAmountOriginal, 0) AS CreditAmountOC ,
                            ISNULL(GL.CreditAmount, 0) AS CreditAmount ,
                            $0 AS ClosingAmountOC ,
                            $0 AS ClosingAmount ,
                            AO.AccountingObjectCode ,
                            AO.AccountingObjectName AS AccountObjectName ,
                            EI.ExpenseItemCode ,
                            EI.ExpenseItemName ,
                            GL.BranchID ,
                            0 ,
                            CASE WHEN GL.DebitAmount <> 0 THEN 0
                                 ELSE 1
                            END AS PaymentType ,
                            1 ,
                            0,
							null
                    FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
					        JOIN @tblListBankAccountDetail BA on BA.ID = GL.BankAccountDetailID


                            LEFT JOIN dbo.AccountingObject AO ON GL.AccountingObjectID = AO.ID
                            LEFT JOIN dbo.ExpenseItem EI ON EI.ID = GL.ExpenseItemID
                    WHERE   GL.PostedDate BETWEEN @FromDate AND @ToDate
                            AND GL.Account LIKE @AccountNumberPercent
                           
                            AND ( GL.DebitAmountOriginal <> 0
                                  OR GL.CreditAmountOriginal <> 0
                                  OR GL.DebitAmount <> 0
                                  OR GL.CreditAmount <> 0
                                )
                    ORDER BY BA.BankAccount ,
                            PostedDate ,OrderPriority,
                            RefDate ,
                            PaymentType ,
                            RefNo
                    
  
        SET @BankAccount = NULL
        INSERT  INTO #Result1
		(     RowNum,
		      RefID ,
              BankAccount,
              PostedDate  ,
              RefDate  ,
              RefNo  ,
              JournalMemo ,
              CorrespondingAccountNumber,
              ExchangeRate ,
              DebitAmountOC ,
              DebitAmount,
              CreditAmountOC ,
              CreditAmount,
              ClosingAmountOC ,
              ClosingAmount ,
              AccountObjectCode ,
              AccountObjectName ,
              ExpenseItemCode,
              ExpenseItemName,
              BranchID  ,
              IsBold  ,
              PaymentType  ,
              OrderType  ,
              RefOrder,
			  OrderPriority)
                SELECT  ROW_NUMBER() OVER ( ORDER BY BankAccount , OrderType, PostedDate , CASE
                                                              WHEN @IsSoftOrderVoucher = 1
                                                              THEN RefOrder
                                                              ELSE 0
                                                              END, RefDate , PaymentType , RefNo ) ,
                        RefID ,
                        BankAccount ,
                        PostedDate ,
                        RefDate ,
                        RefNo ,
                        JournalMemo ,
                        CorrespondingAccountNumber ,
                        ExchangeRate ,
                        DebitAmountOC ,
                        DebitAmount ,
                        CreditAmountOC ,
                        CreditAmount ,
                        ClosingAmountOC ,
                        ClosingAmount ,
                        AccountObjectCode ,
                        AccountObjectName ,

                        ExpenseItemCode ,
                        ExpenseItemName ,
                        BranchID ,
						IsBold  ,
					    PaymentType  ,
						OrderType  ,
						RefOrder,
						OrderPriority
                FROM    #Result
                ORDER BY BankAccount ,
                        OrderType ,
                        PostedDate ,OrderPriority,
                        CASE WHEN @IsSoftOrderVoucher = 1 THEN RefOrder
                             ELSE 0
                        END ,
                        RefDate ,
                        PaymentType ,
                        RefNo
	  END
	  ELSE
	  BEGIN
        INSERT  #Result
                ( BankAccount ,
                  JournalMemo ,
                  DebitAmount ,
                  DebitAmountOC ,
                  CreditAmount ,
                  CreditAmountOC ,
                  ClosingAmountOC ,
                  ClosingAmount ,
                  IsBold ,
                  OrderType   
                )
                SELECT  
		                BA.BankAccount + ' - ' + BA.BankAccountName,
                        N'Số dư đầu kỳ' AS JournalMemo ,
                        $0 AS DebitAmountOC ,
                        $0 AS DebitAmount ,
                        $0 AS CreditAmountOC ,
                        $0 AS CreditAmount ,
                        ISNULL(SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal), 0) ,
                        ISNULL(SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal), 0) ,
                        1 ,
                        0
                FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
				        JOIN @tblListBankAccountDetail BA on BA.ID = GL.BankAccountDetailID
                WHERE   ( GL.PostedDate < @FromDate )
                        AND GL.Account LIKE @AccountNumberPercent
                        AND ( @CurrencyID IS NULL
                              OR GL.CurrencyID = @CurrencyID
                            )
                GROUP BY BA.BankAccount , BA.BankAccountName
                HAVING  ISNULL(SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal), 0) <> 0
                        OR ISNULL(SUM(GL.DebitAmount - GL.CreditAmount), 0) <> 0

       INSERT  #Result
	   (
              RefID ,
              BankAccount,
              PostedDate  ,
              RefDate  ,
              RefNo  ,
              JournalMemo ,
              CorrespondingAccountNumber,
              ExchangeRate ,
              DebitAmountOC ,
              DebitAmount,
              CreditAmountOC ,
              CreditAmount,
              ClosingAmountOC ,
              ClosingAmount ,
              AccountObjectCode ,
              AccountObjectName ,
              ExpenseItemCode,
              ExpenseItemName,
              BranchID  ,
              IsBold  ,
              PaymentType  ,
              OrderType  ,
              RefOrder,
			  OrderPriority )
                    SELECT  GL.ReferenceID ,
	                        BA.BankAccount + ' - ' + BA.BankAccountName,
                            GL.PostedDate ,
                            GL.RefDate ,
                            GL.RefNo AS RefNoFinance ,

                            GL.Description,
                            GL.AccountCorresponding ,
                            GL.ExchangeRate ,
                            ISNULL(GL.DebitAmountOriginal, 0) AS DebitAmountOC ,
                            ISNULL(GL.DebitAmountOriginal, 0) AS DebitAmount ,
                            ISNULL(GL.CreditAmountOriginal, 0) AS CreditAmountOC ,
                            ISNULL(GL.CreditAmountOriginal, 0) AS CreditAmount ,
                            $0 AS ClosingAmountOC ,
                            $0 AS ClosingAmount ,
                            AO.AccountingObjectCode ,
                            AO.AccountingObjectName AS AccountObjectName ,
                            EI.ExpenseItemCode ,
                            EI.ExpenseItemName ,
                            GL.BranchID ,
                            0 ,
                            CASE WHEN GL.DebitAmount <> 0 THEN 0
                                 ELSE 1
                            END AS PaymentType ,
                            1 ,
                            0,
							null
                    FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
					        JOIN @tblListBankAccountDetail BA on BA.ID = GL.BankAccountDetailID


                            LEFT JOIN dbo.AccountingObject AO ON GL.AccountingObjectID = AO.ID
                            LEFT JOIN dbo.ExpenseItem EI ON EI.ID = GL.ExpenseItemID
                    WHERE   GL.PostedDate BETWEEN @FromDate AND @ToDate
                            AND GL.Account LIKE @AccountNumberPercent
                            AND ( @CurrencyID IS NULL
                                  OR GL.CurrencyID = @CurrencyID
                                )
                            AND ( GL.DebitAmountOriginal <> 0
                                  OR GL.CreditAmountOriginal <> 0
                                  OR GL.DebitAmount <> 0
                                  OR GL.CreditAmount <> 0
                                )
                    ORDER BY BA.BankAccount ,
                            PostedDate ,OrderPriority,
                            RefDate ,
                            PaymentType ,
                            RefNo
                    
        SET @BankAccount = NULL
        INSERT  INTO #Result1
		(     RowNum,
		      RefID ,
              BankAccount,
              PostedDate  ,
              RefDate  ,
              RefNo  ,
              JournalMemo ,
              CorrespondingAccountNumber,
              ExchangeRate ,
              DebitAmountOC ,
              DebitAmount,
              CreditAmountOC ,
              CreditAmount,
              ClosingAmountOC ,
              ClosingAmount ,
              AccountObjectCode ,
              AccountObjectName ,
              ExpenseItemCode,
              ExpenseItemName,
              BranchID  ,
              IsBold  ,
              PaymentType  ,
              OrderType  ,
              RefOrder,
			  OrderPriority)
                SELECT  ROW_NUMBER() OVER ( ORDER BY BankAccount , OrderType, PostedDate , CASE
                                                              WHEN @IsSoftOrderVoucher = 1
                                                              THEN RefOrder
                                                              ELSE 0
                                                              END, RefDate , PaymentType , RefNo ) ,
                        RefID ,
                        BankAccount ,
                        PostedDate ,
                        RefDate ,
                        RefNo ,
                        JournalMemo ,
                        CorrespondingAccountNumber ,
                        ExchangeRate ,
                        DebitAmountOC ,
                        DebitAmount ,
                        CreditAmountOC ,
                        CreditAmount ,
                        ClosingAmountOC ,
                        ClosingAmount ,
                        AccountObjectCode ,
                        AccountObjectName ,

                        ExpenseItemCode ,
                        ExpenseItemName ,
                        BranchID ,
						IsBold  ,
					    PaymentType  ,
						OrderType  ,
						RefOrder,
						OrderPriority
                FROM    #Result
                ORDER BY BankAccount ,
                        OrderType ,
                        PostedDate ,OrderPriority,
                        CASE WHEN @IsSoftOrderVoucher = 1 THEN RefOrder
                             ELSE 0
                        END ,
                        RefDate ,
                        PaymentType ,
                        RefNo
        END
      
        SET @CloseAmount = 0
        SET @CloseAmountOC = 0
        UPDATE  #Result1
        SET

                @CloseAmount = ( CASE WHEN OrderType = 0
                                      THEN ( CASE WHEN ClosingAmount = 0
                                                  THEN 0
                                                  ELSE ClosingAmount
                                             END )
                                      WHEN @BankAccount <> BankAccount
                                      THEN DebitAmount - CreditAmount
                                      ELSE @CloseAmount + DebitAmount
                                           - CreditAmount
                                 END ) ,
                @CloseAmountOC = ( CASE WHEN OrderType = 0
                                        THEN ( CASE WHEN ClosingAmountOC = 0
                                                    THEN 0
                                                    ELSE ClosingAmountOC
                                               END )
                                        WHEN @BankAccount <> BankAccount
                                        THEN DebitAmountOC - CreditAmountOC
                                        ELSE @CloseAmountOC + DebitAmountOC
                                             - CreditAmountOC
                                   END ) ,
                ClosingAmount = @CloseAmount ,
                ClosingAmountOC = @CloseAmountOC ,
                @BankAccount = BankAccount
        
        SELECT  *
        FROM    #Result1
        ORDER BY BankAccount ,
                OrderType ,
                PostedDate ,OrderPriority,
                CASE WHEN @IsSoftOrderVoucher = 1 THEN RefOrder
                     ELSE 0
                END ,
                RefDate ,
                PaymentType ,
                RefNo
     
        DROP TABLE #Result
        DROP TABLE #Result1
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_S02C1DNN]
    @BranchID UNIQUEIDENTIFIER,
    @IncludeDependentBranch BIT,
    @StartDate DATETIME,
    @FromDate DATETIME,
    @ToDate DATETIME,
    @AccountNumber NVARCHAR(MAX),
    @IsSimilarSum BIT
AS
    BEGIN
        SET NOCOUNT ON
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)
		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		CREATE TABLE #Result
            (
             RefID UNIQUEIDENTIFIER,
             RefType INT,
             PostedDate DATETIME,
             RefNo NVARCHAR(25),
             RefDate DATETIME,
             InvDate DATETIME,
             InvNo NVARCHAR(MAX),
             JournalMemo NVARCHAR(255),
             AccountNumber NVARCHAR(25),
			 DetailAccountNumber NVARCHAR(25) ,
             AccountCategoryKind INT,
             AccountName NVARCHAR(255),
             CorrespondingAccountNumber NVARCHAR(25),
             DebitAmount MONEY,
             CreditAmount MONEY,
             OrderType INT,
             IsBold BIT,
             OrderNumber int
            )
        CREATE TABLE #tblAccountNumber
            (
             AccountNumber NVARCHAR(25)    PRIMARY KEY,
             AccountName NVARCHAR(255)  ,
             AccountNumberPercent NVARCHAR(25)  ,
             AccountCategoryKind INT,
			 IsParent BIT
            )
        INSERT  #tblAccountNumber
                SELECT  A1.AccountNumber,
                        A1.AccountName,
                        A1.AccountNumber + '%',
                        A1.AccountGroupKind,
						A1.IsParentNode
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountNumber,',') F
                        INNER JOIN dbo.Account A1 ON A1.AccountNumber = F.Value
        IF @IsSimilarSum = 0 /*khong cong gop but ke toan*/
		Begin
            INSERT  #Result
                    (
                     RefID,
                     RefType,
                     PostedDate,
                     RefNo,
                     RefDate,
                     InvDate,
                     InvNo,
                     JournalMemo,
                     AccountNumber,
					 DetailAccountNumber,
                     AccountName,
                     CorrespondingAccountNumber,
                     AccountCategoryKind,
                     DebitAmount,
                     CreditAmount,
                     ORDERType,
                     IsBold,
                     OrderNumber
                    )
                    SELECT
                            GL.ReferenceID,
                            GL.TypeID,
                            GL.PostedDate,
                            RefNo,
                            GL.RefDate,
                            Gl.InvoiceDate,
                            GL.InvoiceNo,
                            GL.Description AS JournalMemo,
                            AC.AccountNumber,
							GL.Account AS DetailAccountNumber,
                            AC.AccountName,
                            GL.AccountCorresponding,
                            AC.AccountCategoryKind,
                            DebitAmount,
                            CreditAmount,
                            1 AS ORDERType,
                            0,
                            CASE WHEN (AC.AccountNumber LIKE N'11%' OR AC.AccountNumber LIKE N'15%' AND AC.AccountNumber NOT LIKE N'154%')
											AND GL.DebitAmount <> 0
                                             THEN 0
                                             ELSE 1
                                        END AS OrderNumber
                    FROM    @tbDataGL AS GL
                            INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                    WHERE   GL.PostedDate BETWEEN @FromDate AND @ToDate
                            AND (GL.DebitAmount <> 0
                                 OR GL.CreditAmount <> 0
                                 or GL.DebitAmountOriginal <> 0
                                 OR GL.CreditAmountOriginal <> 0
                                )
              End
			  Else
			  Begin /*cong gop but toan thue*/
				INSERT  #Result(RefID, RefType, PostedDate, RefNo, RefDate, InvDate, InvNo, JournalMemo, AccountNumber, DetailAccountNumber, AccountName, CorrespondingAccountNumber,
                     AccountCategoryKind, DebitAmount, CreditAmount, ORDERType, IsBold, OrderNumber)
                SELECT RefID, RefType, PostedDate, RefNo, RefDate, InvDate, InvNo, JournalMemo, AccountNumber, DetailAccountNumber, AccountName, CorrespondingAccountNumber,
                     AccountCategoryKind, DebitAmount, CreditAmount, ORDERType, IsBold, OrderNumber
				FROM
				    (SELECT
                            GL.ReferenceID as RefID,
                            GL.TypeID as RefType,
                            GL.PostedDate as PostedDate,
                            RefNo,
                            GL.RefDate as RefDate,
                            Gl.InvoiceDate as InvDate,
                            GL.InvoiceNo as InvNo,
                            GL.Reason AS JournalMemo,
                            AC.AccountNumber as AccountNumber,
							GL.Account AS DetailAccountNumber,
                            AC.AccountName as AccountName,
                            GL.AccountCorresponding as CorrespondingAccountNumber,
                            AC.AccountCategoryKind as AccountCategoryKind,
                            SUM(GL.DebitAmount) as DebitAmount,
                            0 as CreditAmount,
                            1 AS ORDERType,
                            0 as IsBold,
                            CASE WHEN (AC.AccountNumber LIKE N'11%' OR AC.AccountNumber LIKE N'15%' AND AC.AccountNumber NOT LIKE N'154%')
											AND SUM(GL.DebitAmount) <> 0
                                             THEN 0
                                             ELSE 1
                                        END AS OrderNumber
                    FROM    @tbDataGL AS GL
                            INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                    WHERE   GL.PostedDate BETWEEN @FromDate AND @ToDate
					GROUP BY
                            GL.ReferenceID,
                            GL.TypeID,
                            GL.PostedDate,
                            RefNo,
                            GL.RefDate,
                            Gl.InvoiceDate,
                            GL.InvoiceNo,
                            GL.Reason,
                            AC.AccountNumber,
							GL.Account,
                            AC.AccountName,
                            GL.AccountCorresponding,
                            AC.AccountCategoryKind
					HAVING SUM(GL.DebitAmount) <> 0 OR SUM(GL.CreditAmount) <> 0
				UNION ALL
				SELECT
                            GL.ReferenceID as RefID,
                            GL.TypeID as RefType,
                            GL.PostedDate as PostedDate,
                            RefNo,
                            GL.RefDate as RefDate,
                            Gl.InvoiceDate as InvDate,
                            GL.InvoiceNo as InvNo,
                            GL.Reason AS JournalMemo,
                            AC.AccountNumber as AccountNumber,
							GL.Account AS DetailAccountNumber,
                            AC.AccountName as AccountName,
                            GL.AccountCorresponding as CorrespondingAccountNumber,
                            AC.AccountCategoryKind as AccountCategoryKind,
                            0 as DebitAmount,
                            SUM(GL.CreditAmount) as CreditAmount,
                            1 AS ORDERType,
                            0 as IsBold,
                            CASE WHEN (AC.AccountNumber LIKE N'11%' OR AC.AccountNumber LIKE N'15%' AND AC.AccountNumber NOT LIKE N'154%')
											AND SUM(GL.CreditAmount) <> 0
                                             THEN 0
                                             ELSE 1
                                        END AS OrderNumber
                    FROM    @tbDataGL AS GL
                            INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                    WHERE   GL.PostedDate BETWEEN @FromDate AND @ToDate
					GROUP BY
                            GL.ReferenceID,
                            GL.TypeID,
                            GL.PostedDate,
                            RefNo,
                            GL.RefDate,
                            Gl.InvoiceDate,
                            GL.InvoiceNo,
                            GL.Reason,
                            AC.AccountNumber,
							GL.Account,
                            AC.AccountName,
                            GL.AccountCorresponding,
                            AC.AccountCategoryKind
					HAVING SUM(GL.DebitAmount) <> 0 OR SUM(GL.CreditAmount) <> 0
				) T
				where T.DebitAmount > 0 OR T.CreditAmount>0
			  End
        SELECT  AccountNumber,
                AccountName,
				IsParent,
                SUM(OpenningDebitAmount) AS OpenningDebitAmount,
                SUM(OpenningCreditAmount) AS OpenningCreditAmount,
                SUM(ClosingDebitAmount) AS ClosingDebitAmount,
                SUM(ClosingCreditAmount) AS ClosingCreditAmount,
                SUM(AccumDebitAmount) AS AccumDebitAmount,
                SUM(AccumCreditAmount) AS AccumCreditAmount,
				AccountCategoryKind
        FROM    (
                 SELECT AC.AccountNumber,
                        AC.AccountName,
                        CASE WHEN AC.AccountCategoryKind = 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             WHEN ac.AccountCategoryKind = 2
                                  AND SUM(GL.DebitAmount - GL.CreditAmount) > 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             ELSE 0
                        END AS OpenningDebitAmount,
                        CASE WHEN AC.AccountCategoryKind = 1
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             WHEN AC.AccountCategoryKind = 2
                                  AND SUM(GL.CreditAmount - GL.DebitAmount) > 0
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             ELSE 0
                        END AS OpenningCreditAmount,
                        0 AS ClosingDebitAmount,
                        0 AS ClosingCreditAmount,
                        0 AS AccumDebitAmount,
                        0 AS AccumCreditAmount,
						AC.IsParent,
						AC.AccountCategoryKind
                 FROM   @tbDataGL GL
                        INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                 WHERE  GL.PostedDate < @FromDate
                 GROUP BY GL.BranchID,
                        AC.AccountNumber,
                        AC.AccountName,
                        AC.AccountCategoryKind,
						AC.IsParent
                 UNION ALL
                 SELECT AC.AccountNumber,
                        AC.AccountName,
                        0 AS OpenningDebitAmount,
                        0 AS OpenningCreditAmount,
                        CASE WHEN AC.AccountCategoryKind = 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             WHEN ac.AccountCategoryKind = 2
                                  AND SUM(GL.DebitAmount - GL.CreditAmount) > 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             ELSE 0
                        END AS ClosingDebitAmount,
                        CASE WHEN AC.AccountCategoryKind = 1
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             WHEN AC.AccountCategoryKind = 2
                                  AND SUM(GL.CreditAmount - GL.DebitAmount) > 0
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             ELSE 0
                        END AS ClosingCreditAmount,
                        0 AS AccumDebitAmount,
                        0 AS AccumCreditAmount,
						AC.IsParent,
						AC.AccountCategoryKind
                 FROM   @tbDataGL GL
                        INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                 WHERE  GL.PostedDate <= @ToDate
                 GROUP BY AC.AccountNumber,
                        AC.AccountName,
                        AC.AccountCategoryKind,
						AC.IsParent
                 UNION ALL
                 SELECT AC.AccountNumber,
                        AC.AccountName,
                        0 AS OpenningDebitAmount,
                        0 AS OpenningCreditAmount,
                        0 AS ClosingDebitAmount,
                        0 AS ClosingCreditAmount,
                        SUM(GL.DebitAmount) AS AccumDebitAmount,
                        SUM(GL.CreditAmount) AS AccumCreditAmount,
						AC.IsParent,
						AC.AccountCategoryKind
                 FROM   @tbDataGL GL
                        INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                 WHERE  GL.PostedDate BETWEEN @StartDate AND @ToDate
                 GROUP BY AC.AccountNumber,
                        AC.AccountName,
                        AC.AccountCategoryKind,
						AC.IsParent
                ) AS A
        GROUP BY AccountNumber,
                AccountName,
				IsParent ,
				AccountCategoryKind
        HAVING
				SUM(OpenningDebitAmount) <>0 OR
                SUM(OpenningCreditAmount)  <>0 OR
                SUM(ClosingDebitAmount)  <>0 OR
                SUM(ClosingCreditAmount) <>0 OR
                SUM(AccumDebitAmount) <>0 OR
                SUM(AccumCreditAmount) <>0
        ORDER BY AccountNumber
		SELECT  ROW_NUMBER() OVER (ORDER BY AccountNumber,DetailAccountNumber, OrderType, PostedDate, RefDate,OrderNumber, RefNo) AS RowNum,
				RefID,
				RefType,
				PostedDate,
				RefDate,
				RefNo,
				InvDate,
				InvNo,
				JournalMemo,
				AccountNumber,
				DetailAccountNumber,
				CorrespondingAccountNumber,
				DebitAmount,
				CreditAmount,
				IsBold,
				AccountCategoryKind
		FROM    #Result
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_GL_GetGLAccountLedgerDiaryBook]
    @BranchID UNIQUEIDENTIFIER,
    @IncludeDependentBranch BIT,
    @StartDate DATETIME,
    @FromDate DATETIME,
    @ToDate DATETIME,    
    @AccountNumber NVARCHAR(MAX),
    @IsSimilarSum BIT
AS
    BEGIN    
        SET NOCOUNT ON       
        
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/

		CREATE TABLE #Result
            (
             RefID UNIQUEIDENTIFIER,
             RefType INT,
             PostedDate DATETIME,
             RefNo NVARCHAR(25),
             RefDate DATETIME,
             InvDate DATETIME,
             InvNo NVARCHAR(MAX), 
             JournalMemo NVARCHAR(255),
             AccountNumber NVARCHAR(25),
			 DetailAccountNumber NVARCHAR(25) ,
             AccountCategoryKind INT,
             AccountName NVARCHAR(255),
             CorrespondingAccountNumber NVARCHAR(25),
             DebitAmount MONEY,
             CreditAmount MONEY,
             OrderType INT,
             IsBold BIT,
             OrderNumber int,
			 OrderPriority int,
            )
        CREATE TABLE #tblAccountNumber
            (
             AccountNumber NVARCHAR(25)    PRIMARY KEY,
             AccountName NVARCHAR(255)  ,
             AccountNumberPercent NVARCHAR(25)  ,
             AccountCategoryKind INT,
			 IsParent BIT
            )
			
       
	
        INSERT  #tblAccountNumber
                SELECT  A1.AccountNumber,
                        A1.AccountName,
                        A1.AccountNumber + '%',
                        A1.AccountGroupKind,
						A1.IsParentNode
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountNumber,',') F
                        INNER JOIN dbo.Account A1 ON A1.AccountNumber = F.Value
	            					
               
        IF @IsSimilarSum = 0 /*khong cong gop but ke toan*/
		Begin
            INSERT  #Result
                    (
                     RefID,
                     RefType,
                     PostedDate,
                     RefNo,
                     RefDate,
                     InvDate,
                     InvNo,
                     JournalMemo,
                     AccountNumber,
					 DetailAccountNumber,
                     AccountName,
                     CorrespondingAccountNumber,
                     AccountCategoryKind,
                     DebitAmount,
                     CreditAmount,
                     ORDERType,
                     IsBold,
                     OrderNumber,
					 OrderPriority
					
                    )
                    SELECT 
                            GL.ReferenceID,
                            GL.TypeID,
                            GL.PostedDate,
                            RefNo,
                            GL.RefDate,
                            Gl.InvoiceDate,
                            GL.InvoiceNo,
                            GL.Description AS JournalMemo,
                            AC.AccountNumber,
							GL.Account AS DetailAccountNumber,
                            AC.AccountName,
                            GL.AccountCorresponding,
                            AC.AccountCategoryKind,
                            DebitAmount,
                            CreditAmount,
                            1 AS ORDERType,
                            0,
                            CASE WHEN (AC.AccountNumber LIKE N'11%' OR AC.AccountNumber LIKE N'15%' AND AC.AccountNumber NOT LIKE N'154%') 
											AND GL.DebitAmount <> 0
                                             THEN 0
                                             ELSE 1
                                        END AS OrderNumber,
										OrderPriority
                    FROM    @tbDataGL AS GL
                            INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                    WHERE   GL.PostedDate BETWEEN @FromDate AND @ToDate
                            AND (GL.DebitAmount <> 0
                                 OR GL.CreditAmount <> 0
                                 or GL.DebitAmountOriginal <> 0
                                 OR GL.CreditAmountOriginal <> 0
                                )  
					ORDER BY OrderPriority                         
              End
			  Else
			  Begin /*cong gop but toan thue*/
				INSERT  #Result(RefID, RefType, PostedDate, RefNo, RefDate, InvDate, InvNo, JournalMemo, AccountNumber, DetailAccountNumber, AccountName, CorrespondingAccountNumber,
                     AccountCategoryKind, DebitAmount, CreditAmount, ORDERType, IsBold, OrderNumber,OrderPriority)
                SELECT RefID, RefType, PostedDate, RefNo, RefDate, InvDate, InvNo, JournalMemo, AccountNumber, DetailAccountNumber, AccountName, CorrespondingAccountNumber,
                     AccountCategoryKind, DebitAmount, CreditAmount, ORDERType, IsBold, OrderNumber,OrderPriority
				FROM
				    (SELECT 
                            GL.ReferenceID as RefID,
                            GL.TypeID as RefType,
                            GL.PostedDate as PostedDate,
                            RefNo,
                            GL.RefDate as RefDate,
                            Gl.InvoiceDate as InvDate,
                            GL.InvoiceNo as InvNo,
                            GL.Reason AS JournalMemo,
                            AC.AccountNumber as AccountNumber,
							GL.Account AS DetailAccountNumber,
                            AC.AccountName as AccountName,
                            GL.AccountCorresponding as CorrespondingAccountNumber,
                            AC.AccountCategoryKind as AccountCategoryKind,
                            SUM(GL.DebitAmount) as DebitAmount,
                            0 as CreditAmount,
                            1 AS ORDERType,
                            0 as IsBold,

                            CASE WHEN (AC.AccountNumber LIKE N'11%' OR AC.AccountNumber LIKE N'15%' AND AC.AccountNumber NOT LIKE N'154%') 
											AND SUM(GL.DebitAmount) <> 0
                                             THEN 0
                                             ELSE 1
                                        END AS OrderNumber,
										OrderPriority
                    FROM    @tbDataGL AS GL
                            INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                    WHERE   GL.PostedDate BETWEEN @FromDate AND @ToDate
					GROUP BY
                            GL.ReferenceID,
                            GL.TypeID,
                            GL.PostedDate,
                            RefNo,
                            GL.RefDate,
                            Gl.InvoiceDate,
                            GL.InvoiceNo,
                            GL.Reason,
                            AC.AccountNumber,
							GL.Account,
                            AC.AccountName,
                            GL.AccountCorresponding,
                            AC.AccountCategoryKind,
							OrderPriority
					HAVING SUM(GL.DebitAmount) <> 0 OR SUM(GL.CreditAmount) <> 0 
				
				UNION ALL

				SELECT 
                            GL.ReferenceID as RefID,
                            GL.TypeID as RefType,
                            GL.PostedDate as PostedDate,
                            RefNo,
                            GL.RefDate as RefDate,
                            Gl.InvoiceDate as InvDate,
                            GL.InvoiceNo as InvNo,
                            GL.Reason AS JournalMemo,
                            AC.AccountNumber as AccountNumber,
							GL.Account AS DetailAccountNumber,
                            AC.AccountName as AccountName,
                            GL.AccountCorresponding as CorrespondingAccountNumber,
                            AC.AccountCategoryKind as AccountCategoryKind,
                            0 as DebitAmount,
                            SUM(GL.CreditAmount) as CreditAmount,
                            1 AS ORDERType,
                            0 as IsBold,
                            CASE WHEN (AC.AccountNumber LIKE N'11%' OR AC.AccountNumber LIKE N'15%' AND AC.AccountNumber NOT LIKE N'154%') 
											AND SUM(GL.CreditAmount) <> 0
                                             THEN 0
                                             ELSE 1
                                        END AS OrderNumber,
										OrderPriority
                    FROM    @tbDataGL AS GL
                            INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                    WHERE   GL.PostedDate BETWEEN @FromDate AND @ToDate
					GROUP BY
                            GL.ReferenceID,
                            GL.TypeID,
                            GL.PostedDate,
                            RefNo,
                            GL.RefDate,
                            Gl.InvoiceDate,
                            GL.InvoiceNo,
                            GL.Reason,
                            AC.AccountNumber,
							GL.Account,
                            AC.AccountName,
                            GL.AccountCorresponding,
                            AC.AccountCategoryKind,
							OrderPriority
					HAVING SUM(GL.DebitAmount) <> 0 OR SUM(GL.CreditAmount) <> 0 
				) T
				where T.DebitAmount > 0 OR T.CreditAmount>0
			  End      
					
       
        SELECT  AccountNumber,
                AccountName,
				IsParent,
                SUM(OpenningDebitAmount) AS OpenningDebitAmount,
                SUM(OpenningCreditAmount) AS OpenningCreditAmount,
                SUM(ClosingDebitAmount) AS ClosingDebitAmount,
                SUM(ClosingCreditAmount) AS ClosingCreditAmount,
                SUM(AccumDebitAmount) AS AccumDebitAmount,
                SUM(AccumCreditAmount) AS AccumCreditAmount,
				AccountCategoryKind
        FROM    (
                 SELECT AC.AccountNumber,
                        AC.AccountName,
                        CASE WHEN AC.AccountCategoryKind = 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             WHEN ac.AccountCategoryKind = 2
                                  AND SUM(GL.DebitAmount - GL.CreditAmount) > 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             ELSE 0
                        END AS OpenningDebitAmount,
                        CASE WHEN AC.AccountCategoryKind = 1
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             WHEN AC.AccountCategoryKind = 2
                                  AND SUM(GL.CreditAmount - GL.DebitAmount) > 0
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             ELSE 0
                        END AS OpenningCreditAmount,
                        0 AS ClosingDebitAmount,
                        0 AS ClosingCreditAmount,
                        0 AS AccumDebitAmount,
                        0 AS AccumCreditAmount,
						AC.IsParent,
						AC.AccountCategoryKind
                 FROM   @tbDataGL GL
                        INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                 WHERE  GL.PostedDate < @FromDate                       
                 GROUP BY GL.BranchID,
                        AC.AccountNumber,
                        AC.AccountName,
                        AC.AccountCategoryKind,
						AC.IsParent
                 UNION ALL
                 SELECT AC.AccountNumber,
                        AC.AccountName,
                        0 AS OpenningDebitAmount,
                        0 AS OpenningCreditAmount,
                        CASE WHEN AC.AccountCategoryKind = 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             WHEN ac.AccountCategoryKind = 2
                                  AND SUM(GL.DebitAmount - GL.CreditAmount) > 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             ELSE 0
                        END AS ClosingDebitAmount,
                        CASE WHEN AC.AccountCategoryKind = 1
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             WHEN AC.AccountCategoryKind = 2
                                  AND SUM(GL.CreditAmount - GL.DebitAmount) > 0
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             ELSE 0
                        END AS ClosingCreditAmount,
                        0 AS AccumDebitAmount,
                        0 AS AccumCreditAmount,
						AC.IsParent,
						AC.AccountCategoryKind
                 FROM   @tbDataGL GL
                        INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                 WHERE  GL.PostedDate <= @ToDate                        
                 GROUP BY AC.AccountNumber,
                        AC.AccountName,
                        AC.AccountCategoryKind,
						AC.IsParent
                 UNION ALL
                 SELECT AC.AccountNumber,
                        AC.AccountName,
                        0 AS OpenningDebitAmount,
                        0 AS OpenningCreditAmount,
                        0 AS ClosingDebitAmount,
                        0 AS ClosingCreditAmount,
                        SUM(GL.DebitAmount) AS AccumDebitAmount,
                        SUM(GL.CreditAmount) AS AccumCreditAmount,
						AC.IsParent,
						AC.AccountCategoryKind
                 FROM   @tbDataGL GL
                        INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                 WHERE  GL.PostedDate BETWEEN @StartDate AND @ToDate                        
                 GROUP BY AC.AccountNumber,
                        AC.AccountName,
                        AC.AccountCategoryKind,
						AC.IsParent
                ) AS A
        GROUP BY AccountNumber,
                AccountName,
				IsParent ,
				AccountCategoryKind         
        HAVING
				SUM(OpenningDebitAmount) <>0 OR
                SUM(OpenningCreditAmount)  <>0 OR
                SUM(ClosingDebitAmount)  <>0 OR
                SUM(ClosingCreditAmount) <>0 OR
                SUM(AccumDebitAmount) <>0 OR
                SUM(AccumCreditAmount) <>0 
        ORDER BY AccountNumber
        		
		SELECT  ROW_NUMBER() OVER (ORDER BY AccountNumber,DetailAccountNumber, OrderType, PostedDate, RefDate,OrderNumber, RefNo) AS RowNum,
				RefID,
				RefType,
				PostedDate,
				RefDate,
				RefNo,
				InvDate,
				InvNo,
				JournalMemo,
				AccountNumber,
				DetailAccountNumber,
				CorrespondingAccountNumber,
				DebitAmount,
				CreditAmount,
				IsBold,
				AccountCategoryKind
		FROM    #Result
		ORDER BY OrderPriority
			
              
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_GL_GetBookDetailPaymentByAccountNumber]
    @BranchID UNIQUEIDENTIFIER ,
    @IncludeDependentBranch BIT ,
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountNumber NVARCHAR(MAX) ,
	/*@CurrencyId NVARCHAR(10) , comment by cuongpv*/
    @GroupTheSameItem BIT
AS
    BEGIN

		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/

        CREATE  TABLE #Result
            (
              RowNum INT IDENTITY(1, 1) PRIMARY KEY,
              RefID UNIQUEIDENTIFIER ,
              PostedDate DATETIME ,
              RefDate DATETIME ,
              RefNo NVARCHAR(25) ,
              InvDate DATETIME ,
              InvNo NVARCHAR(25) ,
              RefType INT ,
              JournalMemo NVARCHAR(255) ,
              AccountNumber NVARCHAR(25) ,
              CorrespondingAccountNumber NVARCHAR(25) ,
              DebitAmount DECIMAL(22,4) ,
              CreditAmount DECIMAL(22,4) ,
              ClosingDebitAmount DECIMAL(22,4) ,
              ClosingCreditAmount DECIMAL(22,4) ,
              AccountObjectCode NVARCHAR(25) ,
              AccountObjectName NVARCHAR(255) ,
              OrderType INT , 
              IsBold BIT ,
              BranchName NVARCHAR(128),
			  AccountGroupKind NVARCHAR(25) ,
			  OrderPriority int

            )      
       
        
        DECLARE @tblAccountNumber TABLE
            (              
			  AccountNumber  NVARCHAR(25) ,  
              AccountNumberPercent NVARCHAR(25)   , 
			  AccountGroupKind NVARCHAR(25) 
            )
        INSERT  @tblAccountNumber
                SELECT  A1.AccountNumber,
                        A1.AccountNumber + '%',
						A1.AccountGroupKind
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountNumber,',') F
                        INNER JOIN dbo.Account A1 ON A1.AccountNumber = F.Value
        IF @GroupTheSameItem = 1
            BEGIN
                INSERT  #Result					
				SELECT
					RefID ,
                    PostedDate ,
                    RefDate ,
                    RefNo ,
                    InvDate ,
                    InvNo ,
                    RefType ,
                    JournalMemo ,
                    AccountNumber,
                    CorrespondingAccountNumber ,
                    ISNULL(DebitAmount,0) ,
                    ISNULL(CreditAmount,0) ,
                    ISNULL(ClosingDebitAmount,0),
                    ISNULL(ClosingCreditAmount,0),
                    AccountingObjectCode,
                    AccountObjectName,
                    OrderType ,
                    IsBold ,
                    BranchName,
					AccountGroupKind,
					OrderPriority	

                 FROM
					(SELECT  
								NULL as RefID ,
                                NULL as PostedDate ,
                                NULL as RefDate ,
                                NULL as RefNo ,
                                NULL as InvDate ,
                                NULL as InvNo ,
                                NULL as RefType ,
                                N'Số dư đầu kỳ' as JournalMemo ,
                                A.AccountNumber,
                                NULL as CorrespondingAccountNumber ,
								$0 AS DebitAmount ,
								$0 AS CreditAmount ,
								CASE WHEN SUM(GL.DebitAmount - GL.CreditAmount) > 0
									 THEN SUM(GL.DebitAmount - GL.CreditAmount)
									 ELSE 0
								END AS ClosingDebitAmount ,
								CASE WHEN SUM(GL.CreditAmount - GL.DebitAmount) > 0
									 THEN SUM(GL.CreditAmount - GL.DebitAmount)
									 ELSE 0
								END AS ClosingCreditAmount ,
                                null as AccountingObjectCode,
                                null as AccountObjectName,
                                0 AS OrderType ,
                                1 As isBold,
                                null as BranchName
                                ,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 As EntryType
								,A.AccountGroupKind,
								GL.OrderPriority

                            FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
							INNER JOIN @tblAccountNumber A ON GL.Account = A.AccountNumber
							WHERE   ( GL.PostedDate < @FromDate )  
							/*and GL.CurrencyID = @CurrencyId comment by cuongpv*/
							GROUP BY 
								A.AccountNumber,A.AccountGroupKind,GL.OrderPriority
							HAVING  SUM(GL.DebitAmount - GL.CreditAmount) <> 0
						UNION ALL
                        SELECT  GL.ReferenceID ,
                                GL.PostedDate ,
                                GL.RefDate ,
                                GL.RefNo ,
                                gl.InvoiceDate ,
                                gl.InvoiceNo ,
                                GL.TypeID ,
                                GL.Reason,/*edit by cuongpv Description-> Reason*/
                                A.AccountNumber,
                                GL.AccountCorresponding ,
                                SUM(GL.DebitAmount) AS DebitAmount ,
                                null AS CreditAmount ,/*edit by cuongpv SUM(GL.CreditAmount) -> null*/
                                $0 AS ClosingDebitAmount ,
                                $0 AS ClosingCreditAmount ,
                                null as AccountingObjectCode,
                                null as AccountObjectName,
                                1 AS OrderType ,
                                0 As isBold,
                                '' as BranchName
								,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 As EntryType
								,A.AccountGroupKind,
								GL.OrderPriority
                        FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                INNER JOIN @tblAccountNumber A ON GL.Account = A.AccountNumber
                        WHERE   ( GL.PostedDate BETWEEN @FromDate AND @ToDate )    
						/*and GL.CurrencyID = @CurrencyId comment by cuongpv*/
						and GL.AccountCorresponding <> ''      
                        GROUP BY GL.ReferenceID ,
                                GL.PostedDate ,
                                GL.RefDate ,
                                GL.RefNo ,
                                gl.InvoiceDate ,
                                gl.InvoiceNo ,
                                GL.TypeID ,
                                GL.Reason,/*edit by cuongpv GL.Description -> GL.Reason*/
                                A.AccountNumber,
                                GL.AccountCorresponding,
								A.AccountGroupKind,
								GL.OrderPriority
                        HAVING  SUM(GL.DebitAmount) <> 0 OR SUM(GL.CreditAmount) <> 0
						/*add by cuongpv de sua cong gop but toan*/
						UNION ALL
                        SELECT  GL.ReferenceID ,
                                GL.PostedDate ,
                                GL.RefDate ,
                                GL.RefNo ,
                                gl.InvoiceDate ,
                                gl.InvoiceNo ,
                                GL.TypeID ,
                                GL.Reason,/*edit by cuongpv Description-> Reason*/
                                A.AccountNumber,
                                GL.AccountCorresponding ,
                                null AS DebitAmount ,
                                SUM(GL.CreditAmount) AS CreditAmount ,
                                $0 AS ClosingDebitAmount ,
                                $0 AS ClosingCreditAmount ,
                                null as AccountingObjectCode,
                                null as AccountObjectName,
                                1 AS OrderType ,
                                0 As isBold,
                                '' as BranchName
								,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 As EntryType
								,A.AccountGroupKind,
								GL.OrderPriority
                        FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                INNER JOIN @tblAccountNumber A ON GL.Account = A.AccountNumber
                        WHERE   ( GL.PostedDate BETWEEN @FromDate AND @ToDate )    
						/*and GL.CurrencyID = @CurrencyId comment by cuongpv*/
						and GL.AccountCorresponding <> ''      
                        GROUP BY GL.ReferenceID ,
                                GL.PostedDate ,
                                GL.RefDate ,
                                GL.RefNo ,
                                gl.InvoiceDate ,
                                gl.InvoiceNo ,
                                GL.TypeID ,
                                GL.Reason,/*edit by cuongpv GL.Description -> GL.Reason*/
                                A.AccountNumber,
                                GL.AccountCorresponding,
								A.AccountGroupKind,
								GL.OrderPriority
                        HAVING  SUM(GL.DebitAmount) <> 0 OR SUM(GL.CreditAmount) <> 0 
						/*end add by cuongpv*/                       
                      )T
                      ORDER BY 
						AccountNumber,
                        OrderType ,
                        postedDate ,
						OrderPriority,
                        RefDate ,
                        RefNo      
                        ,SortOrder,DetailPostOrder,EntryType            
            END
            
        ELSE
            BEGIN             
                
                INSERT  #Result					
				SELECT
					RefID ,
                    PostedDate ,
                    RefDate ,
                    RefNo ,
                    InvDate ,
                    InvNo ,
                    RefType ,
                    JournalMemo ,
                    AccountNumber,
                    CorrespondingAccountNumber ,
                    DebitAmount ,
                    CreditAmount ,
                    ClosingDebitAmount ,
                    ClosingCreditAmount ,
                    AccountingObjectCode,
                    AccountObjectName,
                    OrderType ,
                    IsBold ,
                    BranchName,
					AccountGroupKind,
					OrderPriority
                 FROM
					(SELECT  
								null as RefID ,
                                NULL as PostedDate ,
                                NULL as RefDate ,
                                NULL as RefNo ,
                                NULL as InvDate ,
                                NULL as InvNo ,
                                NULL as RefType ,
                                N'Số dư đầu kỳ' as JournalMemo ,
                                A.AccountNumber,
                                null as CorrespondingAccountNumber ,
                                $0 AS DebitAmount ,
								$0 AS CreditAmount ,
								CASE WHEN SUM(GL.DebitAmount - GL.CreditAmount) > 0
									 THEN SUM(GL.DebitAmount - GL.CreditAmount)
									 ELSE 0
								END AS ClosingDebitAmount ,
								CASE WHEN SUM(GL.CreditAmount - GL.DebitAmount) > 0
									 THEN SUM(GL.CreditAmount - GL.DebitAmount)
									 ELSE 0
								END AS ClosingCreditAmount ,
								null as AccountingObjectCode,
                                null as AccountObjectName,
                                0 AS OrderType ,
                                1 As isBold,
                                null as BranchName
                                ,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 as EntryType
								,A.AccountGroupKind,
								GL.OrderPriority
                            FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
							INNER JOIN @tblAccountNumber A ON GL.Account = A.AccountNumber
							WHERE   ( GL.PostedDate < @FromDate )   
							/*and GL.CurrencyID = @CurrencyId comment by cuongpv*/
							GROUP BY 
								A.AccountNumber,A.AccountGroupKind,GL.OrderPriority
							HAVING  SUM(GL.DebitAmount - GL.CreditAmount) <> 0
						UNION ALL
                        SELECT  GL.ReferenceID ,
                                GL.PostedDate ,
                                GL.RefDate ,
                                GL.RefNo ,
                                gl.InvoiceDate ,
                                gl.InvoiceNo ,
                                GL.TypeID ,
                                CASE WHEN GL.DESCRIPTION IS NULL
                                          OR LTRIM(GL.Description) = ''
                                     THEN GL.Reason
                                     ELSE GL.Description
                                END AS JournalMemo ,
                                A.AccountNumber,
                                GL.AccountCorresponding ,
                                GL.DebitAmount ,
                                GL.CreditAmount ,
                                $0 AS ClosingDebitAmount ,
                                $0 AS ClosingCreditAmount ,
								null as AccountingObjectCode,
                                null as AccountObjectName,
                                1 AS OrderType ,
                                0 ,
                                null BranchName
								,null
								,null
								,null
								,A.AccountGroupKind,
								GL.OrderPriority
                        FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                INNER JOIN @tblAccountNumber A ON GL.Account = A.AccountNumber
                        WHERE   (GL.PostedDate BETWEEN @FromDate AND @ToDate )                              
                                AND ( GL.DebitAmount <> 0
                                      OR GL.CreditAmount <> 0
                                    )   	
									/*and GL.CurrencyID = @CurrencyId comment by cuongpv*/
									and GL.AccountCorresponding <> ''                     
                      )T
                      ORDER BY
						AccountNumber,
                        OrderType ,
                        postedDate ,
						OrderPriority,
                        RefDate ,
                        RefNo,SortOrder,DetailPostOrder,EntryType                                                          
               
            END         
       
			 

		DECLARE @AccountNumber1 NVARCHAR(25)

		DECLARE C1 CURSOR FOR  
		SELECT  F.Value
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountNumber,',') F
	    order by F.Value
		OPEN c1;  
		FETCH NEXT FROM c1 INTO @AccountNumber1  ;  
		WHILE @@FETCH_STATUS = 0  
		   BEGIN  
		      PRINT @AccountNumber1
			  DECLARE @AccountObjectCode NVARCHAR(25)
			 ,@ClosingDebitAmount DECIMAL(22,4)
			 
		      SELECT @ClosingDebitAmount = 0
			  UPDATE  #Result
			  SET     @ClosingDebitAmount = @ClosingDebitAmount + ISNULL(ClosingDebitAmount,0) - 
		                              ISNULL(ClosingCreditAmount,0) + ISNULL(DebitAmount,0) - ISNULL(CreditAmount,0),
		
                ClosingDebitAmount = CASE WHEN @ClosingDebitAmount > 0
                                          THEN @ClosingDebitAmount
                                          ELSE 0
                                     END ,
                ClosingCreditAmount = CASE WHEN @ClosingDebitAmount < 0
                                           THEN - @ClosingDebitAmount
                                           ELSE 0
                                      END 
               where AccountNumber = @AccountNumber1
			   FETCH NEXT FROM c1 INTO @AccountNumber1  ; 
			   
		   END;  
		CLOSE c1;  
		DEALLOCATE c1;  
		
        DECLARE @DebitAmount DECIMAL(22,4) 
		DECLARE @CreditAmount DECIMAL(22,4)  
		DECLARE @ClosingDeditAmount_SDDK DECIMAL(22,4)  
		DECLARE @ClosingCreditAmount_SDDK DECIMAL(22,4)  
	    select @DebitAmount = sum(DebitAmount),
		       @CreditAmount = sum(CreditAmount)
		from #Result Res
		INNER JOIN @tblAccountNumber A ON Res.AccountNumber LIKE A.AccountNumberPercent
		where OrderType = 1
		select @ClosingDeditAmount_SDDK = sum(ClosingDebitAmount),
		       @ClosingCreditAmount_SDDK = sum(ClosingCreditAmount)
		from #Result where OrderType = 0
        SELECT  RowNum ,
                RefID ,
                AccountObjectCode ,
                AccountObjectName ,
                AccountNumber,
                PostedDate ,
                RefDate ,
                RefNo ,
                InvDate ,
                InvNo ,
                RefType ,
                JournalMemo ,
                CorrespondingAccountNumber ,
                DebitAmount ,
                CreditAmount ,
                ClosingDebitAmount ,
                ClosingCreditAmount ,               
                OrderType AS InvoiceOrder ,
                IsBold ,
                BranchName,
				AccountGroupKind
        FROM    #Result
		WHERE (DebitAmount > 0 OR CreditAmount > 0 AND OrderType = 1) OR OrderType = 0 /*add by cuongpv*/
        ORDER BY RowNum
		
		
		select 
		null as RefID ,
                                NULL as PostedDate ,
                                NULL as RefDate ,
                                NULL as RefNo ,
                                NULL as InvDate ,
                                NULL as InvNo ,
                                NULL as RefType ,
                                N'Số dư cuối kỳ' as JournalMemo ,
                                AccountNumber,
                                null as CorrespondingAccountNumber ,
                                $0 DebitAmount ,
								$0 CreditAmount ,
								case 
								when A1.AccountGroupKind=0 or (A1.AccountGroupKind=2 and (A1.AccountNumber like '1%' or A1.AccountNumber like '2%' or A1.AccountNumber like '6%' 
								or A1.AccountNumber like '8%')) then
								sum(ClosingDebitAmount) + sum(sumDebitAmount) - sum(sumCreditAmount) else $0 
								end as ClosingDebitAmount,
								case 
								when A1.AccountGroupKind=1 or (A1.AccountGroupKind=2 and (A1.AccountNumber like '3%' or A1.AccountNumber like '4%' or A1.AccountNumber like '5%' 
								or A1.AccountNumber like '7%')) then
								sum(ClosingCreditAmount) + sum(sumCreditAmount) - sum(sumDebitAmount) else $0 
								end AS ClosingCreditAmount ,
								null as AccountingObjectCode,
                                null as AccountObjectName,
                                2 AS OrderType ,
                                2 As isBold,
                                null as BranchName
                                ,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 as EntryType,
								A1.AccountGroupKind
        from (SELECT  
								null as RefID ,
                                NULL as PostedDate ,
                                NULL as RefDate ,
                                NULL as RefNo ,
                                NULL as InvDate ,
                                NULL as InvNo ,
                                NULL as RefType ,
                                N'Số dư cuối kỳ' as JournalMemo ,
                                A.AccountNumber,
                                null as CorrespondingAccountNumber ,
                                $0 AS DebitAmount ,
								$0 AS CreditAmount ,
								CASE WHEN GL.OrderType = 0 and gl.IsBold = 1 THEN sum(ClosingDebitAmount)
									 ELSE 0 
								END
								AS ClosingDebitAmount,
								sum(DebitAmount) sumDebitAmount,
								sum(CreditAmount) sumCreditAmount,
								0 AS ClosingCreditAmount ,
								null as AccountingObjectCode,
                                null as AccountObjectName,
                                2 AS OrderType ,
                                2 As isBold,
                                null as BranchName
                                ,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 as EntryType,
								A.AccountGroupKind
                            FROM    #Result AS GL
							INNER JOIN @tblAccountNumber A ON GL.AccountNumber = A.AccountNumber
							GROUP BY OrderType,IsBold,
								A.AccountNumber,A.AccountGroupKind) as A1
        group by AccountNumber,AccountGroupKind
        DROP TABLE #Result 
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_FU_GetCashDetailBook]
    (
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @AccountNumber NVARCHAR(20) ,
      @BranchID UNIQUEIDENTIFIER ,
      @CurrencyID NVARCHAR(3)
    )
AS 
    BEGIN
	/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,0),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,0),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/

        CREATE  TABLE #Result
            (
              RowNum INT IDENTITY(1, 1)
                         PRIMARY KEY ,
              RefID UNIQUEIDENTIFIER ,
			  AccountNumber NVARCHAR(15),
              PostedDate DATETIME ,
              RefDate DATETIME ,
              ReceiptRefNo NVARCHAR(25) ,
              PaymentRefNo NVARCHAR(25) ,
              RefType INT ,
              JournalMemo NVARCHAR(255) ,
              CorrespondingAccountNumber NVARCHAR(20) ,
              ExchangeRate Numeric ,
              DebitAmount Numeric ,
              DebitAmountOC Numeric ,
              CreditAmount Numeric ,
              CreditAmountOC Numeric ,
              ClosingAmount Numeric ,
              ClosingAmountOC Numeric ,
              ContactName NVARCHAR(255) ,
			  AccountObjectId NVARCHAR(255),
              AccountObjectCode NVARCHAR(255) ,
              AccountObjectName NVARCHAR(255) , 
              EmployeeCode NVARCHAR(255) ,
              EmployeeName NVARCHAR(255) , 
              InvoiceOrder INT , 
              BranchID UNIQUEIDENTIFIER ,
              BranchName NVARCHAR(255) ,
              IsBold BIT
            )
         DECLARE @tblAccountNumber TABLE
            (              
			  AccountNumber  NVARCHAR(25),  
              AccountNumberPercent NVARCHAR(25)              
            )
        INSERT  @tblAccountNumber
                SELECT  A1.AccountNumber,
                        A1.AccountNumber + '%'
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountNumber,',') F
                        INNER JOIN dbo.Account A1 ON A1.AccountNumber = F.Value
        DECLARE @AccountNumberPercent NVARCHAR(25)
        SET @AccountNumberPercent = @AccountNumber + '%'
        
        DECLARE @DiscountNumber NVARCHAR(25)       
        SET @DiscountNumber = '5211%'
   
        DECLARE @ClosingAmountOC MONEY
        DECLARE @ClosingAmount MONEY
   IF(@CurrencyID = 'VND')
   BEGIN
 INSERT  #Result
	          (RefID ,
			  AccountNumber,
              PostedDate ,
              RefDate ,
              ReceiptRefNo  ,
              PaymentRefNo,
              RefType ,
              JournalMemo,
              ExchangeRate ,
              DebitAmount  ,
              DebitAmountOC  ,
              CreditAmount  ,
              CreditAmountOC  ,
              ClosingAmount  ,
              ClosingAmountOC  ,
              ContactName,
              AccountObjectId,
              InvoiceOrder  , 
              BranchID  ,
              BranchName,
			  CorrespondingAccountNumber)
			  select ReferenceID ,
			  Account,
              PostedDate ,
              RefDate ,
              ReceiptRefNo  ,
              PaymentRefNo,
              RefType ,
              JournalMemo,
              ExchangeRate ,
              DebitAmount  ,
              DebitAmountOC  ,
              CreditAmount  ,
              CreditAmountOC  ,
              ClosingAmount  ,
              ClosingAmountOC  ,
              ContactName,
              AccountObjectId,
              InvoiceOrder  , 
              BranchID  ,
              BranchName,
			  AccountCorresponding from 
			            (SELECT null as ReferenceID,
						    Acc.AccountNumber as Account,
							null as PostedDate,
							null as RefDate,
							null as ReceiptRefNo,
							null as PaymentRefNo,
							null as RefType,
					        N'Số tồn đầu kỳ' AS JournalMemo ,
                            $0 AS ExchangeRate ,
                            $0 AS DebitAmount ,
                            $0 AS DebitAmountOC ,
                            $0 AS CreditAmount ,
                            $0 AS CreditAmountOC ,
							SUM(GL.DebitAmount - GL.CreditAmount) as ClosingAmount,
							SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal) as ClosingAmountOC,
							null as ContactName,
							null as AccountObjectId,
                            0 AS InvoiceOrder ,
                            null BranchID ,
                            0 as BranchName,
                            null AccountCorresponding
							 from @tbDataGL gl /*edit by cuongpv GeneralLedger -> @tbDataGL*/
							 inner join @tblAccountNumber Acc on GL.Account = Acc.AccountNumber
							 where ( GL.PostedDate < @FromDate )
							
						    group by Acc.AccountNumber
				        union all
                        SELECT  GL.ReferenceID,
						        GL.Account,
                                GL.PostedDate ,
                                GL.RefDate ,
                                CASE WHEN GL.DebitAmount <> 0
                                          OR GL.DebitAmountOriginal <> 0
                                     THEN GL.RefNo
                                     ELSE ''
                                END AS ReceiptRefNo ,
                                CASE WHEN GL.CreditAmount <> 0
                                          OR GL.CreditAmountOriginal <> 0
                                     THEN GL.RefNo
                                     ELSE ''
                                END AS PaymentRefNo ,
                                GL.TypeID ,
								case when GL.Reason is null then
                                GL.Description 
								else GL.Reason 
								end as Description, 
                                GL.ExchangeRate ,
                                GL.DebitAmount ,
                                GL.DebitAmountOriginal ,
                                GL.CreditAmount ,
                                GL.CreditAmountOriginal ,
                                $0 AS ClosingAmount ,
                                $0 AS ClosingAmountOC ,
                                GL.ContactName ,
                                GL.AccountingObjectID ,
                                CASE WHEN GL.DebitAmount <> 0
                                          OR GL.DebitAmountOriginal <> 0 THEN 1
                                     WHEN GL.CreditAmount <> 0
                                          OR GL.CreditAmountOriginal <> 0 THEN 2
                                     ELSE 3
                                END AS InvoiceOrder ,
                                GL.BranchID ,
                                0,
                               GL.AccountCorresponding
                        FROM    @tbDataGL AS GL ,/*edit by cuongpv GeneralLedger -> @tbDataGL*/
						        @tblAccountNumber Acc
                        WHERE   ( GL.PostedDate BETWEEN @FromDate AND @ToDate )
                                AND GL.Account = Acc.AccountNumber
                                
                                AND ( GL.CreditAmount <> 0
                                      OR GL.DebitAmount <> 0
                                      OR GL.CreditAmountOriginal <> 0
                                      OR GL.DebitAmountOriginal <> 0
                                    )) T
                        ORDER BY Account,
						        PostedDate ,
                                RefDate ,
                                InvoiceOrder, 
								ReceiptRefNo,
								PaymentRefNo
		END
		ELSE
		   BEGIN
 INSERT  #Result
	          (RefID ,
			  AccountNumber,
              PostedDate ,
              RefDate ,
              ReceiptRefNo  ,
              PaymentRefNo,
              RefType ,
              JournalMemo,
              ExchangeRate ,
              DebitAmount  ,
              DebitAmountOC  ,
              CreditAmount  ,
              CreditAmountOC  ,
              ClosingAmount  ,
              ClosingAmountOC  ,
              ContactName,
              AccountObjectId,
              InvoiceOrder  , 
              BranchID  ,
              BranchName,
			  CorrespondingAccountNumber)
			  select ReferenceID ,
			  Account,
              PostedDate ,
              RefDate ,
              ReceiptRefNo  ,
              PaymentRefNo,
              RefType ,
              JournalMemo,
              ExchangeRate ,
              DebitAmount  ,
              DebitAmountOC  ,
              CreditAmount  ,
              CreditAmountOC  ,
              ClosingAmount  ,
              ClosingAmountOC  ,
              ContactName,
              AccountObjectId,
              InvoiceOrder  , 
              BranchID  ,
              BranchName,
			  AccountCorresponding from 
			            (SELECT null as ReferenceID,
						    Acc.AccountNumber as Account,
							null as PostedDate,
							null as RefDate,
							null as ReceiptRefNo,
							null as PaymentRefNo,
							null as RefType,
					        N'Số tồn đầu kỳ' AS JournalMemo ,
                            $0 AS ExchangeRate ,
                            $0 AS DebitAmount ,
                            $0 AS DebitAmountOC ,
                            $0 AS CreditAmount ,
                            $0 AS CreditAmountOC ,
							SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal) as ClosingAmount,
							SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal) as ClosingAmountOC,
							null as ContactName,
							null as AccountObjectId,
                            0 AS InvoiceOrder ,
                            null BranchID ,
                            0 as BranchName,
                            null AccountCorresponding
							 from @tbDataGL gl /*edit by cuongpv GeneralLedger -> @tbDataGL*/
							 inner join @tblAccountNumber Acc on GL.Account = Acc.AccountNumber
							 where ( GL.PostedDate < @FromDate )
								AND ( @CurrencyID IS NULL
								OR GL.CurrencyID = @CurrencyID)
						    group by Acc.AccountNumber
				        union all
                        SELECT  GL.ReferenceID,
						        GL.Account,
                                GL.PostedDate ,
                                GL.RefDate ,
                                CASE WHEN GL.DebitAmount <> 0
                                          OR GL.DebitAmountOriginal <> 0
                                     THEN GL.RefNo
                                     ELSE ''
                                END AS ReceiptRefNo ,
                                CASE WHEN GL.CreditAmount <> 0
                                          OR GL.CreditAmountOriginal <> 0
                                     THEN GL.RefNo
                                     ELSE ''
                                END AS PaymentRefNo ,
                                GL.TypeID ,
								case when GL.Reason is null then
                                GL.Description 
								else GL.Reason 
								end as Description, 
                                GL.ExchangeRate ,
                                GL.DebitAmountOriginal ,
                                GL.DebitAmountOriginal ,
                                GL.CreditAmountOriginal ,
                                GL.CreditAmountOriginal ,
                                $0 AS ClosingAmount ,
                                $0 AS ClosingAmountOC ,
                                GL.ContactName ,
                                GL.AccountingObjectID ,
                                CASE WHEN GL.DebitAmount <> 0
                                          OR GL.DebitAmountOriginal <> 0 THEN 1
                                     WHEN GL.CreditAmount <> 0
                                          OR GL.CreditAmountOriginal <> 0 THEN 2
                                     ELSE 3
                                END AS InvoiceOrder ,
                                GL.BranchID ,
                                0,
                               GL.AccountCorresponding
                        FROM    @tbDataGL AS GL ,/*edit by cuongpv GeneralLedger -> @tbDataGL*/
						        @tblAccountNumber Acc
                        WHERE   ( GL.PostedDate BETWEEN @FromDate AND @ToDate )
                                AND GL.Account = Acc.AccountNumber
                                AND ( @CurrencyID IS NULL
                                      OR GL.CurrencyID = @CurrencyID
                                    )
                                AND ( GL.CreditAmount <> 0
                                      OR GL.DebitAmount <> 0
                                      OR GL.CreditAmountOriginal <> 0
                                      OR GL.DebitAmountOriginal <> 0
                                    )) T
                        ORDER BY Account,
						        PostedDate ,
                                RefDate ,
                                InvoiceOrder, 
								ReceiptRefNo,
								PaymentRefNo
		END
        SET @ClosingAmount = 0
        SET @ClosingAmountOC = 0
        UPDATE  #Result
        SET     @ClosingAmount = @ClosingAmount  + ClosingAmount +ISNULL(DebitAmount, 0)
                - ISNULL(CreditAmount, 0) ,
                ClosingAmount = @ClosingAmount ,
                @ClosingAmountOC = @ClosingAmountOC + ClosingAmountOC + ISNULL(DebitAmountOC, 0)
                - ISNULL(CreditAmountOC, 0) ,
                ClosingAmountOC = @ClosingAmountOC 
        SELECT  *
        FROM    #Result R   
		order by rownum
        DROP TABLE #Result 
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

/*

*/
CREATE PROCEDURE [dbo].[Proc_SoChiTietCongNoNhanVien]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountNumber NVARCHAR(20) ,
    @AccountObjectID AS NVARCHAR(MAX)

AS
    BEGIN

		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)
		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE PostedDate<@ToDate
		
		DECLARE @tblListAccountingObjectID TABLE
		(
		AccountObjectID uniqueidentifier,
		AccountObjectCode NVARCHAR(25),
		AccountObjectName NVARCHAR(512)
		)

		 INSERT  INTO @tblListAccountingObjectID
         SELECT  TG.id,TG.AccountingObjectCode,AccountingObjectName
         FROM    AccountingObject AS TG
                 LEFT JOIN dbo.Func_ConvertStringIntoTable(@AccountObjectID,',') AS AccountingObjectID ON TG.ID = AccountingObjectID.Value
         WHERE  AccountingObjectID.Value IS NOT NULL

		DECLARE @tblSochitietcongnonhanvien TABLE 
		(
		AccountObjectID uniqueidentifier,
		AccountObjectCode NVARCHAR(25),
		AccountObjectName NVARCHAR(512),
		PostedDate Date,
		RefDate Date,
		RefNo NCHAR(25),
		JournalMemo NVARCHAR(512),
		AccountNumber NVARCHAR(25),
		CorrespondingAccountNumber NVARCHAR(25),
		DebitAmount decimal(25,0),
		CreditAmount decimal (25,0),
		ClosingDebitAmount decimal(25,0),
		ClosingCreditAmount decimal(25,0)
		)
			DECLARE @tblSochitietcongnonhanvien2 TABLE 
		(
		ID int,
		AccountObjectID uniqueidentifier,
		AccountObjectCode NVARCHAR(25),
		AccountObjectName NVARCHAR(512),
		PostedDate Date,
		RefDate Date,
		RefNo NCHAR(25),
		JournalMemo NVARCHAR(512),
		AccountNumber NVARCHAR(25),
		CorrespondingAccountNumber NVARCHAR(25),
		DebitAmount decimal(25,0),
		CreditAmount decimal (25,0),
		ClosingDebitAmount decimal(25,0),
		ClosingCreditAmount decimal(25,0)
		)

				DECLARE @tg TABLE 
		(
		AccountObjectID uniqueidentifier,
		AccountObjectCode NVARCHAR(25),
		AccountObjectName NVARCHAR(512),
		PostedDate Date,
		RefDate Date,
		RefNo NCHAR(25),
		JournalMemo NVARCHAR(512),
		AccountNumber NVARCHAR(25),
		CorrespondingAccountNumber NVARCHAR(25),
		DebitAmount decimal(25,0),
		CreditAmount decimal (25,0),
		ClosingDebitAmount decimal(25,0),
		ClosingCreditAmount decimal(25,0),
		ID int
		)

		IF(@AccountNumber = 'all')
		BEGIN
		INSERT INTO @tblSochitietcongnonhanvien
		SELECT a.AccountingObjectID,b.AccountingObjectCode,b.AccountingObjectName as AccountingObjectName,null as PostedDate
		,null as RefDate,null as RefNo,N'Số dư đầu kỳ' as JournalMemo, Account as AccountNumber, null as CorrespondingAccountNumber, Sum(DebitAmount) as DebitAmount, Sum(CreditAmount) as CreditAmount,
		0 as ClosingDebitAmount,0 as ClosingCreditAmount
		FROM @tbDataGL a
		LEFT JOIN AccountingObject b ON a.AccountingObjectID = b.ID
		 where Account IN (SELECT AccountNumber From Account WHERE DetailType LIKE '%2%')
		and a.AccountingObjectID in (SELECT AccountObjectID from @tblListAccountingObjectID)  AND PostedDate <@FromDate
		group by a.AccountingObjectID,b.AccountingObjectName,b.AccountingObjectCode,Account,AccountCorresponding

		INSERT INTO @tblSochitietcongnonhanvien
		SELECT a.AccountingObjectID,b.AccountObjectCode, b.AccountObjectName, postedDate as PostedDate, Date as RefDate, No as RefNo, Reason as JournalMemo, Account as AccountNumber, AccountCorresponding as CorrespondingAccountNumber, DebitAmount as DebitAmount, CreditAmount as CreditAmount,
		null,null
		FROM @tbDataGL a
		LEFT JOIN @tblListAccountingObjectID b ON a.AccountingObjectID = b.AccountObjectID
		 where Account IN (SELECT AccountNumber From Account WHERE DetailType LIKE '%2%')
		and a.AccountingObjectID in (SELECT AccountObjectID from @tblListAccountingObjectID) AND PostedDate>=@FromDate AND PostedDate<=@ToDate
		INSERT INTO @tg
			Select  AccountObjectID,AccountObjectCode,AccountObjectName,PostedDate,RefDate,RefNo,JournalMemo,AccountNumber,CorrespondingAccountNumber,DebitAmount,CreditAmount,ClosingDebitAmount,ClosingCreditAmount,ROW_NUMBER() OVER(Order by PostedDate) ID  from @tblSochitietcongnonhanvien
		END
		ELSE
		BEGIN
		INSERT INTO @tblSochitietcongnonhanvien
		SELECT a.AccountingObjectID,b.AccountingObjectCode,b.AccountingObjectName as AccountingObjectName,null as PostedDate
		,null as RefDate,null as RefNo,N'Số dư đầu kỳ' as JournalMemo, Account as AccountNumber,null   as CorrespondingAccountNumber, Sum(DebitAmount) as DebitAmount, Sum(CreditAmount) as CreditAmount,
		0 as ClosingDebitAmount,0 as ClosingCreditAmount
		FROM @tbDataGL a
		LEFT JOIN AccountingObject b ON a.AccountingObjectID = b.ID
		 where Account = @AccountNumber
		and a.AccountingObjectID in (SELECT AccountObjectID from @tblListAccountingObjectID)  AND PostedDate <@FromDate
		group by a.AccountingObjectID,b.AccountingObjectName,b.AccountingObjectCode,Account,AccountCorresponding

		INSERT INTO @tblSochitietcongnonhanvien
		SELECT a.AccountingObjectID,b.AccountObjectCode, b.AccountObjectName, postedDate as PostedDate, Date as RefDate, No as RefNo, Reason as JournalMemo, Account as AccountNumber, AccountCorresponding as CorrespondingAccountNumber, DebitAmount as DebitAmount, CreditAmount as CreditAmount,
		null,null
		FROM @tbDataGL a
		LEFT JOIN @tblListAccountingObjectID b ON a.AccountingObjectID = b.AccountObjectID
		 where Account = @AccountNumber
		and a.AccountingObjectID in (SELECT AccountObjectID from @tblListAccountingObjectID) AND PostedDate>=@FromDate AND PostedDate<=@ToDate
		INSERT INTO @tg
			Select  AccountObjectID,AccountObjectCode,AccountObjectName,PostedDate,RefDate,RefNo,JournalMemo,AccountNumber,CorrespondingAccountNumber,DebitAmount,CreditAmount,ClosingDebitAmount,ClosingCreditAmount,ROW_NUMBER() OVER(Order by PostedDate) ID  from @tblSochitietcongnonhanvien
		END
		DECLARE @COUNT int,@temp int, @COUNT2 int, @AccountObjectCode NVARCHAR(25), @AccountObjectName NVARCHAR(512),@Sodu decimal(25,0), @DebitAmount decimal(25,0),@CreditAmount decimal(25,0),@i int
		DECLARE @tbltg TABLE
		(
		ID int,
		AccountObjectID uniqueidentifier,
		AccountObjectCode NVARCHAR(25),
		AccountObjectName NVARCHAR(512),
		PostedDate Date,
		RefDate Date,
		RefNo NCHAR(25),
		JournalMemo NVARCHAR(512),
		AccountNumber NVARCHAR(25),
		CorrespondingAccountNumber NVARCHAR(25),
		DebitAmount decimal(25,0),
		CreditAmount decimal (25,0),
		ClosingDebitAmount decimal(25,0),
		ClosingCreditAmount decimal (25,0)
		)
		
		Select Top 0 * into #temp from @tbltg
		SELECT DISTINCT(AccountObjectCode) into #tg2 FROM @tg
		SET @COUNT = (SELECT COUNT (*) from #tg2)
		SET @COUNT2 = 0
		WHILE(@COUNT >0)
		BEGIN
			SET @AccountObjectCode = (SELECT TOP 1 AccountObjectCode FROM @tg)
			SET @COUNT2 = (SELECT COUNT (*) FROM @tg WHERE AccountObjectCode = @AccountObjectCode)
		
			SET @temp = (SELECT COUNT (*) FROM @tg WHERE AccountObjectCode = @AccountObjectCode)
			INSERT INTO @tbltg
			SELECT ID,AccountObjectID,AccountObjectCode,AccountObjectName,PostedDate,RefDate,RefNo,JournalMemo,AccountNumber,CorrespondingAccountNumber,DebitAmount,CreditAmount,null,null FROM @tg WHERE AccountObjectCode = @AccountObjectCode ORDER BY ID
			SET @Sodu = 0
			WHILE(@COUNT2 >0)
			BEGIN
				DECLARE @JournalMemo NVARCHAR(MAX)
				SET @i = (SELECT TOP 1 ID FROM @tbltg Order by ID)
				SET @DebitAmount = (SELECT TOP 1 (DebitAmount) FROM @tbltg Order by ID)
				SET @CreditAmount = (SELECT TOP 1 (CreditAmount) FROM  @tbltg Order by ID)
				SET @Sodu = @Sodu + @DebitAmount - @CreditAmount
				IF(@COUNT2=@temp)
				BEGIN
					IF((@DebitAmount - @CreditAmount)>0)
					BEGIN
					SET @JournalMemo =  (SELECT TOP 1 (JournalMemo) FROM  @tbltg Order by ID)
					IF(@JournalMemo=N'Số dư đầu kỳ')
					INSERT INTO #temp SELECT ID,AccountObjectID,AccountObjectCode,AccountObjectName,PostedDate,RefDate,RefNo,JournalMemo,AccountNumber,CorrespondingAccountNumber,0,0,ABS(@Sodu),null FROM @tbltg WHERE ID = @i
					ELSE
					INSERT INTO #temp SELECT ID,AccountObjectID,AccountObjectCode,AccountObjectName,PostedDate,RefDate,RefNo,JournalMemo,AccountNumber,CorrespondingAccountNumber,@DebitAmount,@CreditAmount,ABS(@Sodu),null FROM @tbltg WHERE ID = @i
					END
					ELSE
					BEGIN
					SET @JournalMemo =  (SELECT TOP 1 (JournalMemo) FROM  @tbltg Order by ID)
					IF(@JournalMemo=N'Số dư đầu kỳ')
					INSERT INTO #temp SELECT ID,AccountObjectID,AccountObjectCode,AccountObjectName,PostedDate,RefDate,RefNo,JournalMemo,AccountNumber,CorrespondingAccountNumber,0,0,null,ABS(@Sodu) FROM @tbltg WHERE ID = @i
					ELSE
					INSERT INTO #temp SELECT ID,AccountObjectID,AccountObjectCode,AccountObjectName,PostedDate,RefDate,RefNo,JournalMemo,AccountNumber,CorrespondingAccountNumber,@DebitAmount,@CreditAmount,null,ABS(@Sodu) FROM @tbltg WHERE ID = @i
					END
				END
					ELSE
					BEGIN
						IF(@Sodu>0)
						BEGIN
							INSERT INTO #temp SELECT ID,AccountObjectID,AccountObjectCode,AccountObjectName,PostedDate,RefDate,RefNo,JournalMemo,AccountNumber,CorrespondingAccountNumber,DebitAmount,CreditAmount,ABS(@Sodu),null FROM @tbltg WHERE ID = @i
						END
					ELSE
					INSERT INTO #temp SELECT ID,AccountObjectID,AccountObjectCode,AccountObjectName,PostedDate,RefDate,RefNo,JournalMemo,AccountNumber,CorrespondingAccountNumber,DebitAmount,CreditAmount,null,ABS(@Sodu) FROM @tbltg WHERE ID = @i
					END
				DELETE FROM @tbltg WHERE ID = @i
				SET @COUNT2 = @COUNT2 -1
				END
			INSERT INTO @tblSochitietcongnonhanvien2 
			SELECT * from #temp
			DELETE FROM @tg WHERE AccountObjectCode = @AccountObjectCode 
			DELETE FROM #temp
			SET @COUNT = @COUNT - 1
		END

		DECLARE @tbltg2 TABLE
		(
		ID int,
		IDGroup int,
		AccountObjectID uniqueidentifier,
		AccountObjectCode NVARCHAR(25),
		AccountObjectName NVARCHAR(512),
		PostedDate Date,
		RefDate Date,
		RefNo NCHAR(25),
		JournalMemo NVARCHAR(512),
		AccountNumber NVARCHAR(25),
		CorrespondingAccountNumber NVARCHAR(25),
		DebitAmount decimal(25,0),
		CreditAmount decimal (25,0),
		ClosingDebitAmount decimal(25,0),
		ClosingCreditAmount decimal (25,0)
		)
		INSERT INTO @tbltg2
		SELECT ID,2,AccountObjectID,AccountObjectCode,AccountObjectName,PostedDate,RefDate,RefNo,JournalMemo,AccountNumber,CorrespondingAccountNumber,DebitAmount,CreditAmount,ClosingDebitAmount,ClosingCreditAmount 
		from @tblSochitietcongnonhanvien2
		SELECT * into #tg3 from @tbltg2
		SET @i = 0
		DECLARE @id int
		DECLARE @SumDebitAmount decimal(25,0), @SumCreditAmount decimal(25,0)
		DECLARE @PostedDate Datetime
		SET @COUNT = (SELECT COUNT(*) FROM #tg2)
		WHILE(@COUNT >0)
		BEGIN 
		SET @AccountObjectCode = (SELECT TOP 1 AccountObjectCode FROM @tblSochitietcongnonhanvien2 ORDER BY Id)
		SET @id = (SELECT MAX(ID) FROM #tg3 WHERE AccountObjectCode = @AccountObjectCode)
		print @id
		SET @SumDebitAmount = (SELECT Sum(DebitAmount) from #tg3 WHERE AccountObjectCode = @AccountObjectCode)
		SET @SumCreditAmount = (SELECT Sum(CreditAmount) from #tg3 WHERE AccountObjectCode = @AccountObjectCode)
		SET @PostedDate = (SELECT PostedDate from #tg3 WHERE ID = @id)
		INSERT INTO @tbltg2
		SELECT null,1,AccountObjectID,AccountObjectCode,AccountObjectName,null,null,null,null,null,null,@SumDebitAmount,@SumCreditAmount,ClosingDebitAmount,ClosingCreditAmount from @tblSochitietcongnonhanvien2 
		WHERE ID = @id 
		group by Id,AccountObjectID,AccountObjectCode,AccountObjectName,ClosingDebitAmount,ClosingCreditAmount
		DELETE FROM @tblSochitietcongnonhanvien2 WHERE AccountObjectCode = @AccountObjectCode
		SET @COUNT = @COUNT - 1

		END
		Select * From @tbltg2 Order by ID
		
	END
	
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_SoTheoDoiDoiTuongTHCP]
	@FromDate DATETIME,
    @ToDate DATETIME,
    @CostSetID  NVARCHAR(MAX),
	@ExpenseItemID  NVARCHAR(MAX)
AS
BEGIN
	DECLARE @temp TABLE(
			CostSetID uniqueidentifier,
			ExpenseItemID uniqueidentifier,	 
			SoDauKyNo decimal(25,0),
			SoDauKyCo decimal(25,0),
			SoPhatSinhNo decimal(25,0),
			SoPhatSinhCo decimal(25,0)
	)
	DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)
		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
	DECLARE @tblListCostSetID TABLE
	(
	CostSetID uniqueidentifier
	)
		 INSERT  INTO @tblListCostSetID
         SELECT  TG.id
         FROM    CostSet AS TG
                 LEFT JOIN dbo.Func_ConvertStringIntoTable(@CostSetID,',') AS CostSetID ON TG.ID = CostSetID.Value
         WHERE  CostSetID.Value IS NOT NULL
	DECLARE @tblListExpenseItemID TABLE
	(
	ExpenseItemID uniqueidentifier
	)
		 INSERT  INTO @tblListExpenseItemID
         SELECT  TG.id
         FROM    ExpenseItem AS TG
                 LEFT JOIN dbo.Func_ConvertStringIntoTable(@ExpenseItemID,',') AS ExpenseItemID ON TG.ID = ExpenseItemID.Value
         WHERE  ExpenseItemID.Value IS NOT NULL
		INSERT INTO @temp (CostSetID,ExpenseItemID ,SoDauKyNo)
		SELECT a.CostSetID,a.ExpenseItemID ,Sum(DebitAmount)
		FROM @tbDataGL a 
		where CostSetID in (select CostSetID from @tblListCostSetID)
		and ExpenseItemID in (select ExpenseItemID from @tblListExpenseItemID)
		and DebitAmount > 0
		and Date < @FromDate
		Group By a.CostSetID,a.ExpenseItemID
	 INSERT INTO @temp (CostSetID,ExpenseItemID ,SoDauKyCo)
		SELECT a.CostSetID,a.ExpenseItemID ,Sum(CreditAmount)
		FROM @tbDataGL a
		where CostSetID in (select CostSetID from @tblListCostSetID)
		and ExpenseItemID in (select ExpenseItemID from @tblListExpenseItemID)
		and CreditAmount > 0 and Date < @FromDate and AccountCorresponding like ('133%')
		Group By a.CostSetID,a.ExpenseItemID
		INSERT INTO @temp (CostSetID,ExpenseItemID ,SoPhatSinhNo)
		SELECT a.CostSetID,a.ExpenseItemID ,Sum(DebitAmount)
		FROM @tbDataGL a 
		where CostSetID in (select CostSetID from @tblListCostSetID)
		and ExpenseItemID in (select ExpenseItemID from @tblListExpenseItemID)
		and DebitAmount > 0
		and Date >= @FromDate and Date <= @ToDate
		Group By a.CostSetID,a.ExpenseItemID
	  INSERT INTO @temp (CostSetID,ExpenseItemID ,SoPhatSinhCo)
		SELECT a.CostSetID,a.ExpenseItemID ,Sum(CreditAmount)
		FROM @tbDataGL a
		where CostSetID in (select CostSetID from @tblListCostSetID)
		and ExpenseItemID in (select ExpenseItemID from @tblListExpenseItemID)
		and CreditAmount > 0 and Date >= @FromDate and Date <= @ToDate and AccountCorresponding like ('133%')
		Group By a.CostSetID,a.ExpenseItemID
	DECLARE @tblSoTheoDoiDTTHCP TABLE(
			CostSetID uniqueidentifier,
			CostSetCode  NVARCHAR(25),
			CostSetName NVARCHAR(512),
			ExpenseItemID uniqueidentifier,
			ExpenseItemCode  NVARCHAR(25),
			ExpenseItemName NVARCHAR(512),	 
			SoDauKy decimal(25,0),
			SoPhatSinh decimal(25,0),
			LuyKeCuoiKy decimal(25,0)
	)
	INSERT INTO @tblSoTheoDoiDTTHCP (CostSetID,ExpenseItemID ,SoDauKy,SoPhatSinh)
		SELECT a.CostSetID,a.ExpenseItemID, Sum(ISNULL(a.SoDauKyNo,0) - ISNULL(a.SoDauKyCo,0)),Sum(ISNULL(a.SoPhatSinhNo,0) - ISNULL(a.SoPhatSinhCo,0))
		FROM @temp a 
		Group By a.CostSetID,a.ExpenseItemID
	
	UPDATE @tblSoTheoDoiDTTHCP SET CostSetCode = k.CostSetCode, CostSetName = k.CostSetName
	FROM (select ID,CostSetCode,CostSetName from CostSet) k
	WHERE CostSetID = k.ID
	
	UPDATE @tblSoTheoDoiDTTHCP SET ExpenseItemCode = k.ExpenseItemCode, ExpenseItemName = k.ExpenseItemName
	FROM (select ID,ExpenseItemCode,ExpenseItemName from ExpenseItem) k
	WHERE ExpenseItemID = k.ID
		Select CostSetID,CostSetCode,CostSetName,ExpenseItemID, ExpenseItemCode, ExpenseItemName,ISNULL(SoDauKy,0) as SoDauKy,ISNULL(SoPhatSinh,0) as SoPhatSinh,ISNULL(SoDauKy,0)+ISNULL(SoPhatSinh,0) as LuyKeCuoiKy
		From @tblSoTheoDoiDTTHCP 
		order by CostSetCode
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_SA_SoTheoDoiCongNoPhaiThuTheoHoaDon]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @AccountObjectID AS NVARCHAR(MAX)
AS
BEGIN          
	
	/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate between @FromDate and @ToDate
		/*end add by cuongpv*/

	DECLARE @tbAccountObjectID TABLE(
		AccountingObjectID UNIQUEIDENTIFIER
	)
	
	INSERT INTO @tbAccountObjectID
	SELECT tblAccOjectSelect.Value as AccountingObjectID
		FROM dbo.Func_ConvertStringIntoTable(@AccountObjectID,',') tblAccOjectSelect
		WHERE tblAccOjectSelect.Value in (SELECT ID FROM AccountingObject)
		
	
    DECLARE @tbluutru TABLE (
		SAInvoiceID UNIQUEIDENTIFIER,
		DetailID UNIQUEIDENTIFIER,
		InvoiceDate Date,
		InvoiceNo NVARCHAR(25),
		AccountingObjectID UNIQUEIDENTIFIER,
		TienHangVaCK money,
		TienThue money,
		TraLai money,
		ChietKhauTT_GiamTruKhac money
    )
    
    INSERT INTO @tbluutru(SAInvoiceID,DetailID,InvoiceDate,InvoiceNo,AccountingObjectID)
		SELECT DISTINCT SA.ID,GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID
		FROM @tbDataGL GL LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=GL.DetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
		WHERE (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
			AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID))
			AND (GL.DetailID in (select ID from SAInvoiceDetail)) AND SA.BillRefID is null
		UNION ALL
		SELECT DISTINCT SA.ID, GL.DetailID, SAB.InvoiceDate, SAB.InvoiceNo, GL.AccountingObjectID
		FROM @tbDataGL GL LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=GL.DetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
		LEFT JOIN SABill SAB ON SAB.ID=SA.BillRefID
		WHERE (SAB.InvoiceDate is not null) AND (SAB.InvoiceNo is not null) AND (SAB.InvoiceNo <> '')
		AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID))
		AND (GL.DetailID in (select ID from SAInvoiceDetail)) AND SA.BillRefID is not null

    
    DECLARE @tbTienHangVaCK TABLE(
		DetailID UNIQUEIDENTIFIER,
		DebitAmount money,
		CreditAmount money
		)
    
    INSERT INTO @tbTienHangVaCK
    SELECT GL.DetailID, GL.DebitAmount, GL.CreditAmount
    FROM @tbDataGL GL
    WHERE (GL.Account like '131%') AND (AccountCorresponding like '511%') AND (GL.DetailID in (select DetailID from @tbluutru))
		AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID))
    
    UPDATE @tbluutru SET TienHangVaCK = a.TienHangVaCK
    FROM (select DetailID as IvID, SUM(DebitAmount - CreditAmount) as TienHangVaCK
		from @tbTienHangVaCK
		group by DetailID) a
	WHERE DetailID = a.IvID


	DECLARE @tbTienThue TABLE (
		DetailID UNIQUEIDENTIFIER,
		DebitAmount money
		)
	
	INSERT INTO @tbTienThue
	SELECT GL.DetailID, GL.DebitAmount
	FROM @tbDataGL GL
	WHERE (GL.Account like '131%') AND (AccountCorresponding like '3331%') AND (GL.DetailID in (select DetailID from @tbluutru))
		  AND (GL.PostedDate between @FromDate and @ToDate) AND GL.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID)
	
	UPDATE @tbluutru SET TienThue = b.TienThue
	FROM (select DetailID as IvID, SUM(DebitAmount) as TienThue
		  from @tbTienThue
		  group by DetailID) b
	WHERE DetailID = b.IvID
	
	DECLARE @tbDataTraLai TABLE(
		SAInvoiceDetailID UNIQUEIDENTIFIER,
		TraLai money
	)
	INSERT INTO @tbDataTraLai(SAInvoiceDetailID,TraLai)
		SELECT SAR.SAInvoiceDetailID, SUM(GL.CreditAmount - GL.DebitAmount) as TraLai
		FROM @tbDataGL GL LEFT JOIN SAReturnDetail SAR ON GL.DetailID = SAR.ID
		WHERE (GL.Account like '131%') AND (GL.TypeID=330) AND (SAR.SAInvoiceDetailID in (select DetailID from @tbluutru))
		AND (GL.PostedDate between @FromDate and @ToDate)
		AND (GL.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID))
		GROUP BY SAR.SAInvoiceDetailID
	
	UPDATE @tbluutru SET TraLai = TL.TraLai
	FROM (select SAInvoiceDetailID, TraLai from @tbDataTraLai) TL
	WHERE DetailID = TL.SAInvoiceDetailID
	

	DECLARE @tbChietKhauTT_GiamTruKhac TABLE(
		DetailID UNIQUEIDENTIFIER,
		CreditAmount money
	)
	
	INSERT INTO @tbChietKhauTT_GiamTruKhac
	SELECT GL.DetailID, GL.CreditAmount
	FROM @tbDataGL GL
	WHERE (GL.Account like '131%') AND (GL.AccountCorresponding like '635%')
		  AND (GL.TypeID not in (330,340)) AND (GL.DetailID in (select DetailID from @tbluutru))
		  AND (GL.PostedDate between @FromDate and @ToDate) AND GL.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID)
	
	UPDATE @tbluutru SET ChietKhauTT_GiamTruKhac = c.ChietKhauTT_GiamTruKhac
	FROM (select DetailID as IvID, /*InvoiceDate as IvDate, InvoiceNo as IvNo, AccountingObjectID as AOID,*/ SUM(CreditAmount) as ChietKhauTT_GiamTruKhac
		  from @tbChietKhauTT_GiamTruKhac 
		  group by DetailID) c
	WHERE DetailID = c.IvID
	
	
	DECLARE @tbDataReturn TABLE (
		SAInvoiceID UNIQUEIDENTIFIER,
		InvoiceDate Date,
		InvoiceNo NVARCHAR(25),
		AccountingObjectID UNIQUEIDENTIFIER,
		AccountingObjectName NVARCHAR(512),
		TienHangVaCK money,
		TienThue money,
		GiaTriHoaDon money,
		TraLai money,
		GiamGia money,
		ChietKhauTT_GiamTruKhac money,
		SoDaThu money,
		SoConPhaiThu money
    )
    
    INSERT INTO @tbDataReturn(SAInvoiceID,InvoiceDate,InvoiceNo,AccountingObjectID,TienHangVaCK,TienThue,TraLai,ChietKhauTT_GiamTruKhac)
    SELECT SAInvoiceID,InvoiceDate,InvoiceNo,AccountingObjectID,SUM(TienHangVaCK) as TienHangVaCK, SUM(TienThue) as TienThue,
    SUM(TraLai) as TraLai, SUM(ChietKhauTT_GiamTruKhac) as ChietKhauTT_GiamTruKhac
    FROM @tbluutru
    GROUP BY SAInvoiceID,InvoiceDate,InvoiceNo,AccountingObjectID
    
	INSERT INTO @tbDataReturn(SAInvoiceID,InvoiceDate,InvoiceNo,AccountingObjectID,GiamGia)
		SELECT SA.ID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, SUM(GL.CreditAmount - GL.DebitAmount) as GiamGia
		FROM @tbDataGL GL LEFT JOIN SAReturnDetail SAR ON SAR.ID=GL.DetailID LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=SAR.SAInvoiceDetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
		WHERE (GL.Account like '131%') AND (GL.TypeID=340) AND (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
		AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAReturnDetail)) 
		AND (GL.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID)) AND SA.BillRefID is null
		GROUP BY SA.ID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID
		UNION ALL
		SELECT SA.ID, SAB.InvoiceDate, SAB.InvoiceNo, GL.AccountingObjectID, SUM(GL.CreditAmount - GL.DebitAmount) as GiamGia
		FROM @tbDataGL GL LEFT JOIN SAReturnDetail SAR ON SAR.ID=GL.DetailID LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=SAR.SAInvoiceDetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
		LEFT JOIN SABill SAB ON SAB.ID=SA.BillRefID
		WHERE (GL.Account like '131%') AND (GL.TypeID=340) AND (SAB.InvoiceDate is not null) AND (SAB.InvoiceNo is not null) AND (SAB.InvoiceNo <> '')
		AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAInvoiceDetail))
		AND (GL.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID)) AND SA.BillRefID is not null
		GROUP BY SA.ID, SAB.InvoiceDate, SAB.InvoiceNo, GL.AccountingObjectID
		
	DECLARE @tbDataSoDaThu TABLE (
		SAInvoiceID UNIQUEIDENTIFIER,
		InvoiceDate Date,
		InvoiceNo NVARCHAR(25),
		AccountingObjectID UNIQUEIDENTIFIER,
		SoDaThu money
    )

	INSERT INTO @tbDataSoDaThu
		/*lay da thu cua cac hoa don co BillRefID is null trong SAInvoice*/
		select SA.ID, SA.InvoiceDate, SA.InvoiceNo, SA.AccountingObjectID, SUM(MCC.Amount) as SoDaThu
		from @tbDataGL GL LEFT JOIN MCReceipt MC ON GL.ReferenceID = MC.ID
		LEFT JOIN MCReceiptDetailCustomer MCC ON MCC.MCReceiptID = MC.ID
		LEFT JOIN SAInvoice SA ON SA.ID = MCC.SaleInvoiceID
		where (GL.Account like '131%') AND ((GL.AccountCorresponding like '111%') OR (GL.AccountCorresponding like '112%')) 
		AND (SA.InvoiceDate is not null) AND (SA.InvoiceNo is not null) AND (SA.InvoiceNo <> '') 
		AND (GL.PostedDate between @FromDate and @ToDate) AND SA.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID) AND SA.BillRefID is null
		group by SA.ID, SA.InvoiceDate, SA.InvoiceNo, SA.AccountingObjectID
		  
		UNION ALL
		  
		select SA.ID, SA.InvoiceDate, SA.InvoiceNo, SA.AccountingObjectID, SUM(MBC.Amount) as SoDaThu
		from @tbDataGL GL LEFT JOIN MBDeposit MB ON GL.ReferenceID = MB.ID
		LEFT JOIN MBDepositDetailCustomer MBC ON MBC.MBDepositID = MB.ID
		LEFT JOIN SAInvoice SA ON SA.ID = MBC.SaleInvoiceID
		where (GL.Account like '131%') AND ((GL.AccountCorresponding like '111%') OR (GL.AccountCorresponding like '112%')) 
		AND (SA.InvoiceDate is not null) AND (SA.InvoiceNo is not null) AND (SA.InvoiceNo <> '') 
		AND (GL.PostedDate between @FromDate and @ToDate) AND SA.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID) AND SA.BillRefID is null
		group by SA.ID, SA.InvoiceDate, SA.InvoiceNo, SA.AccountingObjectID

		UNION ALL
		/*lay da thu cua cac hoa don co BillRefID is not null trong SAInvoice*/
		select SA.ID, SAB.InvoiceDate, SAB.InvoiceNo, SA.AccountingObjectID, SUM(MCC.Amount) as SoDaThu
		from @tbDataGL GL LEFT JOIN MCReceipt MC ON GL.ReferenceID = MC.ID
		LEFT JOIN MCReceiptDetailCustomer MCC ON MCC.MCReceiptID = MC.ID
		LEFT JOIN SAInvoice SA ON SA.ID = MCC.SaleInvoiceID
		LEFT JOIN SABill SAB ON SAB.ID=SA.BillRefID
		where (GL.Account like '131%') AND ((GL.AccountCorresponding like '111%') OR (GL.AccountCorresponding like '112%')) 
		AND (SAB.InvoiceDate is not null) AND (SAB.InvoiceNo is not null) AND (SAB.InvoiceNo <> '') 
		AND (GL.PostedDate between @FromDate and @ToDate) AND SA.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID) AND SA.BillRefID is not null
		group by SA.ID, SAB.InvoiceDate, SAB.InvoiceNo, SA.AccountingObjectID
		  
		UNION ALL
		  
		select SA.ID, SAB.InvoiceDate, SAB.InvoiceNo, SA.AccountingObjectID, SUM(MBC.Amount) as SoDaThu
		from @tbDataGL GL LEFT JOIN MBDeposit MB ON GL.ReferenceID = MB.ID
		LEFT JOIN MBDepositDetailCustomer MBC ON MBC.MBDepositID = MB.ID
		LEFT JOIN SAInvoice SA ON SA.ID = MBC.SaleInvoiceID
		LEFT JOIN SABill SAB ON SAB.ID=SA.BillRefID
		where (GL.Account like '131%') AND ((GL.AccountCorresponding like '111%') OR (GL.AccountCorresponding like '112%')) 
		AND (SAB.InvoiceDate is not null) AND (SAB.InvoiceNo is not null) AND (SAB.InvoiceNo <> '') 
		AND (GL.PostedDate between @FromDate and @ToDate) AND SA.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID) AND SA.BillRefID is not null
		group by SA.ID, SAB.InvoiceDate, SAB.InvoiceNo, SA.AccountingObjectID

	UPDATE @tbDataReturn SET SoDaThu = d.SoDaThu
	FROM (
		Select SAInvoiceID as SAId, InvoiceDate as IvDate, InvoiceNo as IvNo, AccountingObjectID as AOID, SoDaThu
		From @tbDataSoDaThu
		  ) d
	WHERE SAInvoiceID = SAId AND InvoiceDate = d.IvDate AND InvoiceNo = d.IvNo AND AccountingObjectID = d.AOID
    

    UPDATE @tbDataReturn SET AccountingObjectName = AJ.AccountingObjectName
	FROM ( select ID,AccountingObjectName from AccountingObject) AJ
	where AccountingObjectID = AJ.ID
    
    UPDATE @tbDataReturn SET GiaTriHoaDon = (ISNULL(TienHangVaCK,0) + ISNULL(TienThue,0))
    
    UPDATE @tbDataReturn SET SoConPhaiThu = (ISNULL(GiaTriHoaDon,0) - (ISNULL(TraLai,0)+ISNULL(GiamGia,0)+ISNULL(ChietKhauTT_GiamTruKhac,0)+ISNULL(SoDaThu,0)))
    
    SELECT InvoiceDate,InvoiceNo,AccountingObjectName,GiaTriHoaDon,TraLai,GiamGia,ChietKhauTT_GiamTruKhac,SoDaThu,SoConPhaiThu FROM @tbDataReturn
    WHERE ((ISNULL(GiaTriHoaDon,0)>0) OR (ISNULL(TraLai,0)>0) OR (ISNULL(GiamGia,0)>0) OR (ISNULL(ChietKhauTT_GiamTruKhac,0)>0)
			OR (ISNULL(SoDaThu,0)>0) OR (ISNULL(SoConPhaiThu,0)>0))
	ORDER BY InvoiceDate,InvoiceNo
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_SA_ReceivableDetail]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountNumber NVARCHAR(20) ,
    @AccountObjectID AS NVARCHAR(MAX) ,
    @CurrencyID NVARCHAR(3) ,
    @IsSimilarSum BIT 
AS
    BEGIN
		
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/

        DECLARE @ID UNIQUEIDENTIFIER
        SET @ID = '00000000-0000-0000-0000-000000000000'
               
        CREATE TABLE #tblListAccountObjectID  
            (
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(25)
                 ,
              AccountObjectName NVARCHAR(255)
                 ,
              AccountObjectAddress NVARCHAR(255)
                 ,
              AccountObjectGroupListCode NVARCHAR(MAX)
                
                NULL ,
              AccountObjectGroupListName NVARCHAR(MAX)
                
                NULL ,
              CompanyTaxCode NVARCHAR(50) 
                                          NULL
            ) 
        INSERT  INTO #tblListAccountObjectID
                SELECT  AO.ID ,
                        AccountingObjectCode ,
                        AccountingObjectName ,
                        [Address] ,
                        AOG.AccountingObjectGroupCode , 
                        AOG.AccountingObjectGroupName , 
                        CASE WHEN AO.ObjectType = 0 THEN TaxCode
                             ELSE ''
                        END AS CompanyTaxCode 
                FROM    dbo.Func_ConvertStringIntoTable(@AccountObjectID,
                                                           ',') tblAccountObjectSelected
                        INNER JOIN dbo.AccountingObject AO ON AO.ID = tblAccountObjectSelected.Value 
                        left JOIN dbo.AccountingObjectGroup AOG ON AO.AccountObjectGroupID = AOG.ID 
          
        CREATE TABLE #tblResult
            (
              RowNum INT PRIMARY KEY
                         IDENTITY(1, 1) ,
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(100)
                
                NULL ,
              AccountObjectName NVARCHAR(255)
                
                NULL ,
              AccountObjectGroupListCode NVARCHAR(MAX)
                
                NULL , 
              AccountObjectGroupListName NVARCHAR(MAX)
                
                NULL ,          
              AccountObjectAddress NVARCHAR(255)
                
                NULL , 
              CompanyTaxCode NVARCHAR(50) 
                                          NULL ,
              PostedDate DATETIME ,
              RefDate DATETIME , 
              RefNo NVARCHAR(25) 
                                 NULL ,
              InvDate DATETIME ,
              InvNo NVARCHAR(MAX) 
                                  NULL ,
              
              RefType INT ,
              RefID UNIQUEIDENTIFIER ,                        
              RefDetailID NVARCHAR(50) 
                                       NULL ,
              JournalMemo NVARCHAR(255) 
                                        NULL ,
              AccountNumber NVARCHAR(20) 
                                         NULL ,
              AccountCategoryKind INT ,
              CorrespondingAccountNumber NVARCHAR(20)
                
                NULL ,
              ExchangeRate DECIMAL(18, 4) , 
              DebitAmountOC MONEY ,
              DebitAmount MONEY , 
              CreditAmountOC MONEY ,
              CreditAmount MONEY , 
              ClosingDebitAmountOC MONEY , 
              ClosingDebitAmount MONEY , 
              ClosingCreditAmountOC MONEY ,
              ClosingCreditAmount MONEY ,
              InventoryItemCode NVARCHAR(50)
                 , 
              InventoryItemName NVARCHAR(255)
                 ,
              UnitName NVARCHAR(20) 
                                    NULL ,
              Quantity DECIMAL(22, 8) ,
              UnitPrice DECIMAL(20, 6) ,            
              IsBold BIT , 
              OrderType INT ,
              BranchName NVARCHAR(128) 
                                       NULL ,
              GLJournalMemo NVARCHAR(255) 
                                          NULL 
              ,
              MasterCustomField1 NVARCHAR(255)
                
                NULL ,
              MasterCustomField2 NVARCHAR(255)
                
                NULL ,
              MasterCustomField3 NVARCHAR(255)
                
                NULL ,
              MasterCustomField4 NVARCHAR(255)
                
                NULL ,
              MasterCustomField5 NVARCHAR(255)
                
                NULL ,
              MasterCustomField6 NVARCHAR(255)
                
                NULL ,
              MasterCustomField7 NVARCHAR(255)
                
                NULL ,
              MasterCustomField8 NVARCHAR(255)
                
                NULL ,
              MasterCustomField9 NVARCHAR(255)
                
                NULL ,
              MasterCustomField10 NVARCHAR(255)
                
                NULL ,
              CustomField1 NVARCHAR(255) 
                                         NULL ,
              CustomField2 NVARCHAR(255) 
                                         NULL ,
              CustomField3 NVARCHAR(255) 
                                         NULL ,
              CustomField4 NVARCHAR(255) 
                                         NULL ,
              CustomField5 NVARCHAR(255) 
                                         NULL ,
              CustomField6 NVARCHAR(255) 
                                         NULL ,
              CustomField7 NVARCHAR(255) 
                                         NULL ,
              CustomField8 NVARCHAR(255) 
                                         NULL ,
              CustomField9 NVARCHAR(255) 
                                         NULL ,
              CustomField10 NVARCHAR(255) 
                                          NULL ,
              /*cột số lượng, đơn giá*/
              MainUnitName NVARCHAR(255) 
                                         NULL ,
              MainUnitPrice DECIMAL(20, 6) ,
              MainQuantity DECIMAL(22, 8) ,
              /*mã thống kê, và tên loại chứng từ*/
              ListItemCode NVARCHAR(20) ,
              ListItemName NVARCHAR(128) ,
              RefTypeName NVARCHAR(128),
			  OrderPriority int
            )
        
        CREATE TABLE #tblAccountNumber
            (
              AccountNumber NVARCHAR(20) 
                                         PRIMARY KEY ,
              AccountCategoryKind INT
            )
        IF @AccountNumber IS NOT NULL
            BEGIN
			INSERT  INTO #tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = @AccountNumber
                        ORDER BY A.AccountNumber ,
                                A.AccountName 
            END
        ELSE
            BEGIN 
			   INSERT  INTO #tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   DetailType = '1'
                                AND Grade = 1
                                AND IsParentNode = 0
                        ORDER BY A.AccountNumber ,
                                A.AccountName
            END 

/*Lấy số dư đầu kỳ và dữ liệu phát sinh trong kỳ:*/ 
		
		IF(@CurrencyID = 'VND')
		BEGIN
		IF @IsSimilarSum = 0 
            BEGIN
                INSERT  INTO #tblResult
                        ( AccountObjectID ,
                          AccountObjectCode ,  
                          AccountObjectName ,	
                          AccountObjectGroupListCode ,
                          AccountObjectGroupListName ,        
                          AccountObjectAddress , 
                          CompanyTaxCode , 
                          PostedDate ,
                          RefDate , 
                          RefNo ,
                          InvDate ,
                          InvNo ,
                          RefType ,	
                          RefID ,  
                          RefDetailID ,
                          JournalMemo , 				  
                          AccountNumber ,
                          AccountCategoryKind , 
                          CorrespondingAccountNumber ,			  
                          ExchangeRate , 		  
                          DebitAmountOC ,
                          DebitAmount , 
                          CreditAmountOC , 
                          CreditAmount , 
                          ClosingDebitAmountOC , 
                          ClosingDebitAmount , 
                          ClosingCreditAmountOC ,	
                          ClosingCreditAmount ,               
                          IsBold , 
                          OrderType,
						  OrderPriority
                        )
                        SELECT  AccountingObjectID ,
                                AccountObjectCode , 
                                AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,           
                                AccountObjectAddress ,
                                CompanyTaxCode , 
                                PostedDate ,
                                RefDate ,
                                RefNo , 
                                InvDate ,
                                InvNo ,
                                RefType ,
                                RefID ,
                                RefDetailID ,
                                JournalMemo , 				  
                                AccountNumber , 
                                AccountCategoryKind , 
                                CorrespondingAccountNumber , 		  
                                ExchangeRate ,  
                                SUM(DebitAmountOC) ,
                                SUM(DebitAmount) , 
                                SUM(CreditAmountOC) ,
                                SUM(CreditAmount) ,
                                ( CASE WHEN AccountCategoryKind = 0
                                       THEN SUM(ClosingDebitAmountOC)
                                            - SUM(ClosingCreditAmountOC)
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmountOC)
                                                        - SUM(ClosingCreditAmountOC) ) > 0
                                                 THEN ( SUM(ClosingDebitAmountOC)
                                                        - SUM(ClosingCreditAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmountOC ,         
                                ( CASE WHEN AccountCategoryKind = 0
                                       THEN ( SUM(ClosingDebitAmount)
                                              - SUM(ClosingCreditAmount) )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmount)
                                                        - SUM(ClosingCreditAmount) ) > 0
                                                 THEN SUM(ClosingDebitAmount)
                                                      - SUM(ClosingCreditAmount)
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmount ,
                                ( CASE WHEN AccountCategoryKind = 1
                                       THEN ( SUM(ClosingCreditAmountOC)
                                              - SUM(ClosingDebitAmountOC) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmountOC)
                                                        - SUM(ClosingDebitAmountOC) ) > 0
                                                 THEN ( SUM(ClosingCreditAmountOC)
                                                        - SUM(ClosingDebitAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmountOC ,
                                ( CASE WHEN AccountCategoryKind = 1
                                       THEN ( SUM(ClosingCreditAmount)
                                              - SUM(ClosingDebitAmount) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmount)
                                                        - SUM(ClosingDebitAmount) ) > 0
                                                 THEN ( SUM(ClosingCreditAmount)
                                                        - SUM(ClosingDebitAmount) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmount ,
                                
                                IsBold , 
                                OrderType ,
								OrderPriority

                                
                        FROM    ( SELECT    AOL.AccountingObjectID ,
                                            LAOI.AccountObjectCode ,   
                                            LAOI.AccountObjectName ,	
                                            LAOI.AccountObjectGroupListCode ,
                                            LAOI.AccountObjectGroupListName ,          
                                            LAOI.AccountObjectAddress , 
                                            LAOI.CompanyTaxCode , 
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.PostedDate
                                            END AS PostedDate ,             
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.RefDate
                                            END AS RefDate , 
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.RefNo
                                            END AS RefNo , 
                                            
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.InvoiceDate
                                            END AS InvDate ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.InvoiceNo
                                            END AS InvNo ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.ReferenceID
                                            END AS RefID ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.TypeID
                                            END AS RefType ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE CAST(AOL.DetailID AS NVARCHAR(50))
                                            END AS RefDetailID ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN N'Số dư đầu kỳ'
                                                 ELSE ISNULL(AOL.DESCRIPTION,
                                                             AOL.Reason)
                                            END AS JournalMemo ,   
                                            TBAN.AccountNumber ,
                                            TBAN.AccountCategoryKind ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.AccountCorresponding
                                            END AS CorrespondingAccountNumber ,
                                            
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.ExchangeRate
                                            END AS ExchangeRate ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN $0
                                                 ELSE AOL.DebitAmountOriginal
                                            END AS DebitAmountOC ,                               
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN $0
                                                 ELSE AOL.DebitAmount
                                            END AS DebitAmount ,                                     
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN $0
                                                 ELSE AOL.CreditAmountOriginal
                                            END AS CreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN $0
                                                 ELSE AOL.CreditAmount
                                            END AS CreditAmount ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN AOL.DebitAmountOriginal
                                                 ELSE $0
                                            END AS ClosingDebitAmountOC ,           
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN AOL.DebitAmount
                                                 ELSE $0
                                            END AS ClosingDebitAmount ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN AOL.CreditAmountOriginal
                                                 ELSE $0
                                            END AS ClosingCreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN AOL.CreditAmount
                                                 ELSE $0
                                            END AS ClosingCreditAmount , 
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN 1
                                                 ELSE 0
                                            END AS IsBold ,	
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN 0
                                                 ELSE 1
                                            END AS OrderType ,
											 CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN null
                                                 ELSE OrderPriority
                                            END AS OrderPriority 
                                  FROM      @tbDataGL AS AOL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                            INNER JOIN #tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumber
                                                              + '%'
                                            INNER JOIN #tblListAccountObjectID
                                            AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID
                                  WHERE     AOL.PostedDate <= @ToDate
     
                                ) AS RSNS
                        GROUP BY RSNS.AccountingObjectID ,
                                RSNS.AccountObjectCode , 
                                RSNS.AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,        
                                RSNS.AccountObjectAddress ,
                                CompanyTaxCode , 
                                RSNS.PostedDate ,
                                RSNS.RefDate ,
                                RSNS.RefNo , 
                                RSNS.InvDate ,
                                RSNS.InvNo ,
                                RSNS.RefType ,
                                RSNS.RefID ,   
                                RSNS.RefDetailID ,
                                RSNS.JournalMemo , 				  
                                RSNS.AccountNumber ,
                                RSNS.AccountCategoryKind ,
                                RSNS.CorrespondingAccountNumber , 		  
                                RSNS.ExchangeRate ,           
                                RSNS.IsBold ,
                                RSNS.OrderType,
								RSNS.OrderPriority
                        HAVING  SUM(DebitAmountOC) <> 0
                                OR SUM(DebitAmount) <> 0
                                OR SUM(CreditAmountOC) <> 0
                                OR SUM(CreditAmount) <> 0
                                OR SUM(ClosingDebitAmountOC
                                       - ClosingCreditAmountOC) <> 0
                                OR SUM(ClosingDebitAmount
                                       - ClosingCreditAmount) <> 0
                        ORDER BY RSNS.AccountObjectCode ,
                                RSNS.AccountNumber ,
                                RSNS.OrderType ,
                                RSNS.PostedDate ,
								RSNS.OrderPriority,
                                RSNS.RefDate ,
                                RSNS.RefNo 
            END
        ELSE 
            BEGIN
                INSERT  INTO #tblResult
                        ( AccountObjectID ,
                          AccountObjectCode ,   
                          AccountObjectName ,
                          AccountObjectGroupListCode , 
                          AccountObjectGroupListName ,            
                          AccountObjectAddress ,
                          CompanyTaxCode ,
                          PostedDate ,
                          RefDate ,
                          RefNo ,
                          InvDate ,
                          InvNo ,
                          RefType ,
                          RefID ,   
                          RefDetailID ,
                          JournalMemo , 			  
                          AccountNumber ,
                          AccountCategoryKind , 
                          CorrespondingAccountNumber ,		  
                          ExchangeRate ,		  
                          DebitAmountOC ,
                          DebitAmount ,
                          CreditAmountOC , 
                          CreditAmount ,
                          ClosingDebitAmountOC ,
                          ClosingDebitAmount , 
                          ClosingCreditAmountOC ,
                          ClosingCreditAmount ,                                    
                          IsBold , 
                          OrderType,
						  OrderPriority
                        )
                        SELECT  AccountingObjectID ,
                                AccountObjectCode ,  
                                AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,             
                                AccountObjectAddress , 
                                CompanyTaxCode ,
                                PostedDate ,	
                                RefDate , 
                                RefNo , 
                                InvDate ,
                                InvNo ,
                                RefType ,
                                RefID , 
                                RefDetailID ,
                                JournalMemo ,				  
                                AccountNumber ,
                                AccountCategoryKind ,
                                CorrespondingAccountNumber ,			  
                                ExchangeRate , 	  
                                DebitAmountOC ,
                                DebitAmount ,
                                CreditAmountOC ,
                                CreditAmount , 
                                ( CASE WHEN AccountCategoryKind = 0
                                       THEN ( ClosingDebitAmountOC
                                              - ClosingCreditAmountOC )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( ClosingDebitAmountOC
                                                        - ClosingCreditAmountOC ) > 0
                                                 THEN ClosingDebitAmountOC
                                                      - ClosingCreditAmountOC
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmountOC ,  
                                ( CASE WHEN AccountCategoryKind = 0
                                       THEN ( ClosingDebitAmount
                                              - ClosingCreditAmount )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( ClosingDebitAmount
                                                        - ClosingCreditAmount ) > 0
                                                 THEN ClosingDebitAmount
                                                      - ClosingCreditAmount
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmount , 
                                ( CASE WHEN AccountCategoryKind = 1
                                       THEN ( ClosingCreditAmountOC
                                              - ClosingDebitAmountOC )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( ClosingCreditAmountOC
                                                        - ClosingDebitAmountOC ) > 0
                                                 THEN ( ClosingCreditAmountOC
                                                        - ClosingDebitAmountOC )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmountOC ,
                                ( CASE WHEN AccountCategoryKind = 1
                                       THEN ( ClosingCreditAmount
                                              - ClosingDebitAmount )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( ClosingCreditAmount
                                                        - ClosingDebitAmount ) > 0
                                                 THEN ( ClosingCreditAmount
                                                        - ClosingDebitAmount )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmount ,                                     
                                IsBold , 
                                OrderType,
								OrderPriority
                        FROM    ( SELECT    AD.AccountingObjectID ,
                                            AD.AccountObjectCode , 
                                            AD.AccountObjectName ,
                                            AD.AccountObjectGroupListCode ,
                                            AD.AccountObjectGroupListName ,            
                                            AD.AccountObjectAddress ,
                                            CompanyTaxCode ,
                                            AD.PostedDate ,
                                            AD.RefDate ,
                                            AD.RefNo ,
                                            AD.InvDate ,
                                            AD.InvNo ,
                                            AD.RefID ,
                                            AD.RefType , 
                                            MAX(AD.RefDetailID) AS RefDetailID ,
                                            AD.JournalMemo ,   
                                            AD.AccountNumber ,
                                            AD.AccountCategoryKind ,
                                            AD.CorrespondingAccountNumber , 
                                            AD.ExchangeRate , 
                                            SUM(AD.DebitAmountOC) AS DebitAmountOC ,                              
                                            SUM(AD.DebitAmount) AS DebitAmount ,                                        
                                            SUM(AD.CreditAmountOC) AS CreditAmountOC , 
                                            SUM(AD.CreditAmount) AS CreditAmount ,
                                            SUM(AD.ClosingDebitAmountOC) AS ClosingDebitAmountOC ,       
                                            SUM(AD.ClosingDebitAmount) AS ClosingDebitAmount ,
                                            SUM(AD.ClosingCreditAmountOC) AS ClosingCreditAmountOC , 
                                            SUM(AD.ClosingCreditAmount) AS ClosingCreditAmount ,
                                            AD.IsBold ,	
                                            AD.OrderType ,
											AD.OrderPriority
											
                                  FROM      ( SELECT    AOL.AccountingObjectID ,
                                                        LAOI.AccountObjectCode ,   
                                                        LAOI.AccountObjectName ,	
                                                        LAOI.AccountObjectGroupListCode ,
                                                        LAOI.AccountObjectGroupListName ,              
                                                        LAOI.AccountObjectAddress , 
                                                        LAOI.CompanyTaxCode , 
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.PostedDate
                                                        END AS PostedDate ,             
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.RefDate
                                                        END AS RefDate , 
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.RefNo
                                                        END AS RefNo , 
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.InvoiceDate
                                                        END AS InvDate ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.InvoiceNo
                                                        END AS InvNo ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.ReferenceID
                                                        END AS RefID ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.TypeID
                                                        END AS RefType ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE CAST(DetailID AS NVARCHAR(50))
                                                        END AS RefDetailID ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN N'Số dư đầu kỳ'
                                                             ELSE AOL.Reason
                                                        END AS JournalMemo , 
                                                        TBAN.AccountNumber ,
                                                        TBAN.AccountCategoryKind ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.AccountCorresponding
                                                        END AS CorrespondingAccountNumber , 
                                                        
                                                        CASE WHEN AOL.PostedDate < @FromDate
															 THEN NULL
															 ELSE AOL.ExchangeRate
														END AS ExchangeRate , 
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN $0
                                                             ELSE AOL.DebitAmountOriginal
                                                        END AS DebitAmountOC ,                         
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN $0
                                                             ELSE AOL.DebitAmount
                                                        END AS DebitAmount ,                           
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN $0
                                                             ELSE AOL.CreditAmountOriginal
                                                        END AS CreditAmountOC ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN $0
                                                             ELSE AOL.CreditAmount
                                                        END AS CreditAmount ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN AOL.DebitAmountOriginal
                                                             ELSE $0
                                                        END AS ClosingDebitAmountOC ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN AOL.DebitAmount
                                                             ELSE $0
                                                        END AS ClosingDebitAmount ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN AOL.CreditAmountOriginal
                                                             ELSE $0
                                                        END AS ClosingCreditAmountOC ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN AOL.CreditAmount
                                                             ELSE $0
                                                        END AS ClosingCreditAmount ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN 1
                                                             ELSE 0
                                                        END AS IsBold ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN 0
                                                             ELSE 1
                                                        END AS OrderType ,
                                                           CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN 0
                                                             ELSE OrderPriority
                                                        END AS OrderPriority 
                                              FROM      @tbDataGL AS AOL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                                        INNER JOIN #tblListAccountObjectID
                                                        AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID
                                                        INNER JOIN #tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumber
                                                              + '%'
                                              WHERE     AOL.PostedDate <= @ToDate
                                            ) AS AD
                                  GROUP BY  AD.AccountingObjectID ,
                                            AD.AccountObjectCode , 
                                            AD.AccountObjectName ,
                                            AD.AccountObjectGroupListCode , 
                                            AD.AccountObjectGroupListName ,     
                                            AD.AccountObjectAddress ,
                                            AD.CompanyTaxCode ,
                                            AD.PostedDate ,
                                            AD.RefDate ,
                                            AD.RefNo ,
                                            AD.InvDate ,
                                            AD.InvNo ,
                                            AD.RefID ,
                                            AD.RefType ,
                                            AD.JournalMemo ,
                                            AD.AccountNumber ,
                                            AD.AccountCategoryKind ,
                                            AD.CorrespondingAccountNumber ,  
                                            AD.ExchangeRate ,
                                            AD.IsBold , 	
                                            AD.OrderType,
											AD.OrderPriority
                                ) AS RSS
                        WHERE   RSS.DebitAmountOC <> 0
                                OR RSS.CreditAmountOC <> 0
                                OR RSS.DebitAmount <> 0
                                OR RSS.CreditAmount <> 0
                                OR ClosingCreditAmountOC
                                - ClosingDebitAmountOC <> 0
                                OR ClosingCreditAmount - ClosingDebitAmount <> 0
                        ORDER BY RSS.AccountObjectCode ,
                                RSS.AccountNumber ,
                                RSS.OrderType ,
                                RSS.PostedDate ,
								RSS.OrderPriority,
                                RSS.RefDate ,
                                RSS.RefNo ,
                                RSS.InvDate ,
                                RSS.InvNo
            END
		END
		ELSE
		BEGIN
        IF @IsSimilarSum = 0 
            BEGIN
                INSERT  INTO #tblResult
                        ( AccountObjectID ,
                          AccountObjectCode ,  
                          AccountObjectName ,	
                          AccountObjectGroupListCode ,
                          AccountObjectGroupListName ,        
                          AccountObjectAddress , 
                          CompanyTaxCode , 
                          PostedDate ,
                          RefDate , 
                          RefNo ,
                          InvDate ,
                          InvNo ,
                          RefType ,	
                          RefID ,  
                          RefDetailID ,
                          JournalMemo , 				  
                          AccountNumber ,
                          AccountCategoryKind , 
                          CorrespondingAccountNumber ,			  
                          ExchangeRate , 		  
                          DebitAmountOC ,
                          DebitAmount , 
                          CreditAmountOC , 
                          CreditAmount , 
                          ClosingDebitAmountOC , 
                          ClosingDebitAmount , 
                          ClosingCreditAmountOC ,	
                          ClosingCreditAmount ,               
                          IsBold , 
                          OrderType,
						  OrderPriority
                        )
                        SELECT  AccountingObjectID ,
                                AccountObjectCode , 
                                AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,           
                                AccountObjectAddress ,
                                CompanyTaxCode , 
                                PostedDate ,
                                RefDate ,
                                RefNo , 
                                InvDate ,
                                InvNo ,
                                RefType ,
                                RefID ,
                                RefDetailID ,
                                JournalMemo , 				  
                                AccountNumber , 
                                AccountCategoryKind , 
                                CorrespondingAccountNumber , 		  
                                ExchangeRate ,  
                                SUM(DebitAmountOC) ,
                                SUM(DebitAmount) , 
                                SUM(CreditAmountOC) ,
                                SUM(CreditAmount) ,
                                ( CASE WHEN AccountCategoryKind = 0
                                       THEN SUM(ClosingDebitAmountOC)
                                            - SUM(ClosingCreditAmountOC)
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmountOC)
                                                        - SUM(ClosingCreditAmountOC) ) > 0
                                                 THEN ( SUM(ClosingDebitAmountOC)
                                                        - SUM(ClosingCreditAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmountOC ,         
                                ( CASE WHEN AccountCategoryKind = 0
                                       THEN ( SUM(ClosingDebitAmount)
                                              - SUM(ClosingCreditAmount) )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmount)
                                                        - SUM(ClosingCreditAmount) ) > 0
                                                 THEN SUM(ClosingDebitAmount)
                                                      - SUM(ClosingCreditAmount)
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmount ,
                                ( CASE WHEN AccountCategoryKind = 1
                                       THEN ( SUM(ClosingCreditAmountOC)
                                              - SUM(ClosingDebitAmountOC) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmountOC)
                                                        - SUM(ClosingDebitAmountOC) ) > 0
                                                 THEN ( SUM(ClosingCreditAmountOC)
                                                        - SUM(ClosingDebitAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmountOC ,
                                ( CASE WHEN AccountCategoryKind = 1
                                       THEN ( SUM(ClosingCreditAmount)
                                              - SUM(ClosingDebitAmount) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmount)
                                                        - SUM(ClosingDebitAmount) ) > 0
                                                 THEN ( SUM(ClosingCreditAmount)
                                                        - SUM(ClosingDebitAmount) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmount ,
                                
                                IsBold , 
                                OrderType ,
								OrderPriority

                                
                        FROM    ( SELECT    AOL.AccountingObjectID ,
                                            LAOI.AccountObjectCode ,   
                                            LAOI.AccountObjectName ,	
                                            LAOI.AccountObjectGroupListCode ,
                                            LAOI.AccountObjectGroupListName ,          
                                            LAOI.AccountObjectAddress , 
                                            LAOI.CompanyTaxCode , 
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.PostedDate
                                            END AS PostedDate ,             
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.RefDate
                                            END AS RefDate , 
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.RefNo
                                            END AS RefNo , 
                                            
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.InvoiceDate
                                            END AS InvDate ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.InvoiceNo
                                            END AS InvNo ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.ReferenceID
                                            END AS RefID ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.TypeID
                                            END AS RefType ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE CAST(AOL.DetailID AS NVARCHAR(50))
                                            END AS RefDetailID ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN N'Số dư đầu kỳ'
                                                 ELSE ISNULL(AOL.DESCRIPTION,
                                                             AOL.Reason)
                                            END AS JournalMemo ,   
                                            TBAN.AccountNumber ,
                                            TBAN.AccountCategoryKind ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.AccountCorresponding
                                            END AS CorrespondingAccountNumber ,
                                            
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.ExchangeRate
                                            END AS ExchangeRate ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN $0
                                                 ELSE AOL.DebitAmountOriginal
                                            END AS DebitAmountOC ,                               
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN $0
                                                 ELSE AOL.DebitAmountOriginal
                                            END AS DebitAmount ,                                     
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN $0
                                                 ELSE AOL.CreditAmountOriginal
                                            END AS CreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN $0
                                                 ELSE AOL.CreditAmountOriginal
                                            END AS CreditAmount ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN AOL.DebitAmountOriginal
                                                 ELSE $0
                                            END AS ClosingDebitAmountOC ,           
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN AOL.DebitAmountOriginal
                                                 ELSE $0
                                            END AS ClosingDebitAmount ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN AOL.CreditAmountOriginal
                                                 ELSE $0
                                            END AS ClosingCreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN AOL.CreditAmountOriginal
                                                 ELSE $0
                                            END AS ClosingCreditAmount , 
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN 1
                                                 ELSE 0
                                            END AS IsBold ,	
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN 0
                                                 ELSE 1
                                            END AS OrderType ,
											 CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN null
                                                 ELSE OrderPriority
                                            END AS OrderPriority 
                                  FROM      @tbDataGL AS AOL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                            INNER JOIN #tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumber
                                                              + '%'
                                            INNER JOIN #tblListAccountObjectID
                                            AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID
                                  WHERE     AOL.PostedDate <= @ToDate
                                            AND ( @CurrencyID IS NULL
                                                  OR AOL.CurrencyID = @CurrencyID
                                                )

                                ) AS RSNS
                        GROUP BY RSNS.AccountingObjectID ,
                                RSNS.AccountObjectCode , 
                                RSNS.AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,        
                                RSNS.AccountObjectAddress ,
                                CompanyTaxCode , 
                                RSNS.PostedDate ,
                                RSNS.RefDate ,
                                RSNS.RefNo , 
                                RSNS.InvDate ,
                                RSNS.InvNo ,
                                RSNS.RefType ,
                                RSNS.RefID ,   
                                RSNS.RefDetailID ,
                                RSNS.JournalMemo , 				  
                                RSNS.AccountNumber ,
                                RSNS.AccountCategoryKind ,
                                RSNS.CorrespondingAccountNumber , 		  
                                RSNS.ExchangeRate ,           
                                RSNS.IsBold ,
                                RSNS.OrderType,
								RSNS.OrderPriority
                        HAVING  SUM(DebitAmountOC) <> 0
                                OR SUM(DebitAmount) <> 0
                                OR SUM(CreditAmountOC) <> 0
                                OR SUM(CreditAmount) <> 0
                                OR SUM(ClosingDebitAmountOC
                                       - ClosingCreditAmountOC) <> 0
                                OR SUM(ClosingDebitAmount
                                       - ClosingCreditAmount) <> 0
                        ORDER BY RSNS.AccountObjectCode ,
                                RSNS.AccountNumber ,
                                RSNS.OrderType ,
                                RSNS.PostedDate ,
								RSNS.OrderPriority,
                                RSNS.RefDate ,
                                RSNS.RefNo 
            END
        ELSE 
            BEGIN
                INSERT  INTO #tblResult
                        ( AccountObjectID ,
                          AccountObjectCode ,   
                          AccountObjectName ,
                          AccountObjectGroupListCode , 
                          AccountObjectGroupListName ,            
                          AccountObjectAddress ,
                          CompanyTaxCode ,
                          PostedDate ,
                          RefDate ,
                          RefNo ,
                          InvDate ,
                          InvNo ,
                          RefType ,
                          RefID ,   
                          RefDetailID ,
                          JournalMemo , 			  
                          AccountNumber ,
                          AccountCategoryKind , 
                          CorrespondingAccountNumber ,		  
                          ExchangeRate ,		  
                          DebitAmountOC ,
                          DebitAmount ,
                          CreditAmountOC , 
                          CreditAmount ,
                          ClosingDebitAmountOC ,
                          ClosingDebitAmount , 
                          ClosingCreditAmountOC ,
                          ClosingCreditAmount ,                                    
                          IsBold , 
                          OrderType,
						  OrderPriority
                        )
                        SELECT  AccountingObjectID ,
                                AccountObjectCode ,  
                                AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,             
                                AccountObjectAddress , 
                                CompanyTaxCode ,
                                PostedDate ,	
                                RefDate , 
                                RefNo , 
                                InvDate ,
                                InvNo ,
                                RefType ,
                                RefID , 
                                RefDetailID ,
                                JournalMemo ,				  
                                AccountNumber ,
                                AccountCategoryKind ,
                                CorrespondingAccountNumber ,			  
                                ExchangeRate , 	  
                                DebitAmountOC ,
                                DebitAmount ,
                                CreditAmountOC ,
                                CreditAmount , 
                                ( CASE WHEN AccountCategoryKind = 0
                                       THEN ( ClosingDebitAmountOC
                                              - ClosingCreditAmountOC )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( ClosingDebitAmountOC
                                                        - ClosingCreditAmountOC ) > 0
                                                 THEN ClosingDebitAmountOC
                                                      - ClosingCreditAmountOC
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmountOC ,  
                                ( CASE WHEN AccountCategoryKind = 0
                                       THEN ( ClosingDebitAmount
                                              - ClosingCreditAmount )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( ClosingDebitAmount
                                                        - ClosingCreditAmount ) > 0
                                                 THEN ClosingDebitAmount
                                                      - ClosingCreditAmount
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmount , 
                                ( CASE WHEN AccountCategoryKind = 1
                                       THEN ( ClosingCreditAmountOC
                                              - ClosingDebitAmountOC )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( ClosingCreditAmountOC
                                                        - ClosingDebitAmountOC ) > 0
                                                 THEN ( ClosingCreditAmountOC
                                                        - ClosingDebitAmountOC )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmountOC ,
                                ( CASE WHEN AccountCategoryKind = 1
                                       THEN ( ClosingCreditAmount
                                              - ClosingDebitAmount )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( ClosingCreditAmount
                                                        - ClosingDebitAmount ) > 0
                                                 THEN ( ClosingCreditAmount
                                                        - ClosingDebitAmount )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmount ,                                     
                                IsBold , 
                                OrderType,
								OrderPriority
                        FROM    ( SELECT    AD.AccountingObjectID ,
                                            AD.AccountObjectCode , 
                                            AD.AccountObjectName ,
                                            AD.AccountObjectGroupListCode ,
                                            AD.AccountObjectGroupListName ,            
                                            AD.AccountObjectAddress ,
                                            CompanyTaxCode ,
                                            AD.PostedDate ,
                                            AD.RefDate ,
                                            AD.RefNo ,
                                            AD.InvDate ,
                                            AD.InvNo ,
                                            AD.RefID ,
                                            AD.RefType , 
                                            MAX(AD.RefDetailID) AS RefDetailID ,
                                            AD.JournalMemo ,   
                                            AD.AccountNumber ,
                                            AD.AccountCategoryKind ,
                                            AD.CorrespondingAccountNumber , 
                                            AD.ExchangeRate , 
                                            SUM(AD.DebitAmountOC) AS DebitAmountOC ,                              
                                            SUM(AD.DebitAmount) AS DebitAmount ,                                        
                                            SUM(AD.CreditAmountOC) AS CreditAmountOC , 
                                            SUM(AD.CreditAmount) AS CreditAmount ,
                                            SUM(AD.ClosingDebitAmountOC) AS ClosingDebitAmountOC ,       
                                            SUM(AD.ClosingDebitAmount) AS ClosingDebitAmount ,
                                            SUM(AD.ClosingCreditAmountOC) AS ClosingCreditAmountOC , 
                                            SUM(AD.ClosingCreditAmount) AS ClosingCreditAmount ,
                                            AD.IsBold ,	
                                            AD.OrderType ,
											AD.OrderPriority
											
                                  FROM      ( SELECT    AOL.AccountingObjectID ,
                                                        LAOI.AccountObjectCode ,   
                                                        LAOI.AccountObjectName ,	
                                                        LAOI.AccountObjectGroupListCode ,
                                                        LAOI.AccountObjectGroupListName ,              
                                                        LAOI.AccountObjectAddress , 
                                                        LAOI.CompanyTaxCode , 
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.PostedDate
                                                        END AS PostedDate ,             
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.RefDate
                                                        END AS RefDate , 
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.RefNo
                                                        END AS RefNo , 
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.InvoiceDate
                                                        END AS InvDate ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.InvoiceNo
                                                        END AS InvNo ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.ReferenceID
                                                        END AS RefID ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.TypeID
                                                        END AS RefType ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE CAST(DetailID AS NVARCHAR(50))
                                                        END AS RefDetailID ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN N'Số dư đầu kỳ'
                                                             ELSE AOL.Reason
                                                        END AS JournalMemo , 
                                                        TBAN.AccountNumber ,
                                                        TBAN.AccountCategoryKind ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.AccountCorresponding
                                                        END AS CorrespondingAccountNumber , 
                                                        
                                                        CASE WHEN AOL.PostedDate < @FromDate
															 THEN NULL
															 ELSE AOL.ExchangeRate
														END AS ExchangeRate , 
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN $0
                                                             ELSE AOL.DebitAmountOriginal
                                                        END AS DebitAmountOC ,                         
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN $0
                                                             ELSE AOL.DebitAmountOriginal
                                                        END AS DebitAmount ,                           
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN $0
                                                             ELSE AOL.CreditAmountOriginal
                                                        END AS CreditAmountOC ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN $0
                                                             ELSE AOL.CreditAmountOriginal
                                                        END AS CreditAmount ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN AOL.DebitAmountOriginal
                                                             ELSE $0
                                                        END AS ClosingDebitAmountOC ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN AOL.DebitAmountOriginal
                                                             ELSE $0
                                                        END AS ClosingDebitAmount ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN AOL.CreditAmountOriginal
                                                             ELSE $0
                                                        END AS ClosingCreditAmountOC ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN AOL.CreditAmountOriginal
                                                             ELSE $0
                                                        END AS ClosingCreditAmount ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN 1
                                                             ELSE 0
                                                        END AS IsBold ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN 0
                                                             ELSE 1
                                                        END AS OrderType ,
                                                           CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN 0
                                                             ELSE OrderPriority
                                                        END AS OrderPriority 
                                              FROM      @tbDataGL AS AOL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                                        INNER JOIN #tblListAccountObjectID
                                                        AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID
                                                        INNER JOIN #tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumber
                                                              + '%'
                                              WHERE     AOL.PostedDate <= @ToDate
                                                        AND ( @CurrencyID IS NULL
                                                              OR AOL.CurrencyID = @CurrencyID
                                                            )
                                            ) AS AD
                                  GROUP BY  AD.AccountingObjectID ,
                                            AD.AccountObjectCode , 
                                            AD.AccountObjectName ,
                                            AD.AccountObjectGroupListCode , 
                                            AD.AccountObjectGroupListName ,     
                                            AD.AccountObjectAddress ,
                                            AD.CompanyTaxCode ,
                                            AD.PostedDate ,
                                            AD.RefDate ,
                                            AD.RefNo ,
                                            AD.InvDate ,
                                            AD.InvNo ,
                                            AD.RefID ,
                                            AD.RefType ,
                                            AD.JournalMemo ,
                                            AD.AccountNumber ,
                                            AD.AccountCategoryKind ,
                                            AD.CorrespondingAccountNumber ,  
                                            AD.ExchangeRate ,
                                            AD.IsBold , 	
                                            AD.OrderType,
											AD.OrderPriority
                                ) AS RSS
                        WHERE   RSS.DebitAmountOC <> 0
                                OR RSS.CreditAmountOC <> 0
                                OR RSS.DebitAmount <> 0
                                OR RSS.CreditAmount <> 0
                                OR ClosingCreditAmountOC
                                - ClosingDebitAmountOC <> 0
                                OR ClosingCreditAmount - ClosingDebitAmount <> 0
                        ORDER BY RSS.AccountObjectCode ,
                                RSS.AccountNumber ,
                                RSS.OrderType ,
                                RSS.PostedDate ,
								RSS.OrderPriority,
                                RSS.RefDate ,
                                RSS.RefNo ,
                                RSS.InvDate ,
                                RSS.InvNo
            END
			END
/* Tính số tồn */
        DECLARE @CloseAmountOC AS DECIMAL(22, 8) ,
            @CloseAmount AS DECIMAL(22, 8) ,
            @AccountObjectCode_tmp NVARCHAR(100) ,
            @AccountNumber_tmp NVARCHAR(20)
        SELECT  @CloseAmountOC = 0 ,
                @CloseAmount = 0 ,
                @AccountObjectCode_tmp = N'' ,
                @AccountNumber_tmp = N''
        UPDATE  #tblResult
        SET     @CloseAmountOC = ( CASE WHEN OrderType = 0
                                        THEN ( CASE WHEN ClosingDebitAmountOC = 0
                                                    THEN ClosingCreditAmountOC
                                                    ELSE -1
                                                         * ClosingDebitAmountOC
                                               END )
                                        WHEN @AccountObjectCode_tmp <> AccountObjectCode
                                             OR @AccountNumber_tmp <> AccountNumber
                                        THEN CreditAmountOC - DebitAmountOC
                                        ELSE @CloseAmountOC + CreditAmountOC
                                             - DebitAmountOC
                                   END ) ,
                ClosingDebitAmountOC = ( CASE WHEN AccountCategoryKind = 0
                                              THEN -1 * @CloseAmountOC
                                              WHEN AccountCategoryKind = 1
                                              THEN $0
                                              ELSE CASE WHEN @CloseAmountOC < 0
                                                        THEN -1
                                                             * @CloseAmountOC
                                                        ELSE $0
                                                   END
                                         END ) ,
                ClosingCreditAmountOC = ( CASE WHEN AccountCategoryKind = 1
                                               THEN @CloseAmountOC
                                               WHEN AccountCategoryKind = 0
                                               THEN $0
                                               ELSE CASE WHEN @CloseAmountOC > 0
                                                         THEN @CloseAmountOC
                                                         ELSE $0
                                                    END
                                          END ) ,
                @CloseAmount = ( CASE WHEN OrderType = 0
                                      THEN ( CASE WHEN ClosingDebitAmount = 0
                                                  THEN ClosingCreditAmount
                                                  ELSE -1 * ClosingDebitAmount
                                             END )
                                      WHEN @AccountObjectCode_tmp <> AccountObjectCode
                                           OR @AccountNumber_tmp <> AccountNumber
                                      THEN CreditAmount - DebitAmount
                                      ELSE @CloseAmount + CreditAmount
                                           - DebitAmount
                                 END ) ,
                ClosingDebitAmount = ( CASE WHEN AccountCategoryKind = 0
                                            THEN -1 * @CloseAmount
                                            WHEN AccountCategoryKind = 1
                                            THEN $0
                                            ELSE CASE WHEN @CloseAmount < 0
                                                      THEN -1 * @CloseAmount
                                                      ELSE $0
                                                 END
                                       END ) ,
                ClosingCreditAmount = ( CASE WHEN AccountCategoryKind = 1
                                             THEN @CloseAmount
                                             WHEN AccountCategoryKind = 0
                                             THEN $0
                                             ELSE CASE WHEN @CloseAmount > 0
                                                       THEN @CloseAmount
                                                       ELSE $0
                                                  END
                                        END ) ,
                @AccountObjectCode_tmp = AccountObjectCode ,
                @AccountNumber_tmp = AccountNumber
        WHERE   OrderType <> 2
/*Lấy số liệu*/				
        SELECT  RS.*
        FROM    #tblResult RS
        ORDER BY RowNum
        
        DROP TABLE #tblResult
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

/*=================================================================================================
 * Created By:		haipl
 * Created Date:	25/12/2017
 * Description:		Lay du lieu cho bao cao << Sổ chi tiết bán hàng >>
 * Edit by: Cuongpv
 * Edit Description: Lay du lieu day du cac truong hop cho bao cao <<So chi tiet ban hang>>
===================================================================================================== */
ALTER PROCEDURE [dbo].[Proc_SA_GetSalesBookDetail]
  
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @BranchID UNIQUEIDENTIFIER ,
    @IncludeDependentBranch BIT ,
    @InventoryItemID NVARCHAR(MAX) , 
    @OrganizationUnitID UNIQUEIDENTIFIER ,
    @AccountObjectID NVARCHAR(MAX) ,
	@SumGiaVon decimal output
AS

    BEGIN
        SET NOCOUNT ON;
        declare @v1 DECIMAL(29,4)                            
        CREATE TABLE #tblListInventoryItemID
            (
              InventoryItemID UNIQUEIDENTIFIER PRIMARY KEY
            ) 
        INSERT  INTO #tblListInventoryItemID
                    SELECT  T.*
                    FROM    dbo.Func_ConvertStringIntoTable(@InventoryItemID,
                                                              ',')	
                                                              AS T  
        /*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,0),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,0),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate between  @FromDate and @ToDate
		/*end add by cuongpv*/

		/*add by cuongpv*/
		DECLARE @tbLuuTru TABLE(
			ngay_CT datetime,
			ngay_HT datetime,
			So_Hieu nvarchar(25),
			Ngay_HD datetime,
			SO_HD nvarchar(25),
			Dien_giai nvarchar(512),
			TK_DOIUNG nvarchar(25),
			DVT nvarchar(25),
			SL decimal(25,10),
			DON_GIA money,
			KHAC decimal(25,0),
			TT decimal(25,0),
			InventoryItemID uniqueidentifier,
			MaterialGoodsName nvarchar(512)
		)
		
		INSERT INTO @tbLuuTru
		SELECT ngay_CT, ngay_HT, So_Hieu, Ngay_HD, SO_HD, Dien_giai, TK_DOIUNG, DVT, SL, DON_GIA, SUM(KHAC) as KHAC, SUM(TT) as TT, InventoryItemID, MaterialGoodsName
		FROM (
		/*end add by cuongpv*/
			/*lay hoa don ban hang*/
			select a.Date as ngay_CT, a.PostedDate as ngay_HT, a.No as So_Hieu, a.InvoiceDate as Ngay_HD, a.InvoiceNo as SO_HD, d.reason  as Dien_giai, accountCorresponding as TK_DOIUNG,
			c.Unit as DVT, b.Quantity as SL, b.UnitPrice as DON_GIA, null as KHAC, sum(CreditAmount) as TT, c.ID as  InventoryItemID, c.MaterialGoodsName, a.OrderPriority
			from @tbDataGL a left join SAInvoiceDetail b on a.DetailID = b.ID /*edit by cuongpv GeneralLedger -> @tbDataGL*/
			left join MaterialGoods c on b.MaterialGoodsID = c.ID
			left join SAInvoice d on d.ID = b.SAInvoiceID
			where a.PostedDate between  @FromDate and @ToDate and c.ID in (select InventoryItemID from #tblListInventoryItemID)
			and Account like '511%' and d.BillRefID is null /*add by cuonpv: "and d.BillRefID is null"*/
			group by a.Date,  a.PostedDate, a.No ,a.InvoiceDate , a.InvoiceNo, accountCorresponding , /*edit by cuonpv bo: " , Sreason "*/
			c.Unit , b.Quantity , b.UnitPrice, c.ID,c.MaterialGoodsName,d.reason,a.OrderPriority
			Union all
			/*lay chiet khau ban*/
			select a.Date as ngay_CT, a.PostedDate as ngay_HT, a.No as So_Hieu, a.InvoiceDate as Ngay_HD, a.InvoiceNo as SO_HD, d.reason  as Dien_giai, accountCorresponding as TK_DOIUNG,
			c.Unit as DVT, b.Quantity as SL, b.UnitPrice as DON_GIA, sum(debitAmount) as KHAC, null as TT, c.ID as  InventoryItemID, c.MaterialGoodsName, a.OrderPriority
			from @tbDataGL a left join SAInvoiceDetail b on a.DetailID = b.ID /*edit by cuongpv GeneralLedger -> @tbDataGL*/
			left join MaterialGoods c on b.MaterialGoodsID = c.ID
			left join SAInvoice d on d.ID = b.SAInvoiceID
			where a.PostedDate between  @FromDate and @ToDate and c.ID in (select InventoryItemID from #tblListInventoryItemID)
			and Account like '511%' and (a.TypeID not in (330,340)) and d.BillRefID is null 
			group by a.Date,  a.PostedDate, a.No ,a.InvoiceDate , a.InvoiceNo, accountCorresponding , 
			c.Unit , b.Quantity , b.UnitPrice, c.ID,c.MaterialGoodsName,d.reason,a.OrderPriority
		) SA
		GROUP BY ngay_CT, ngay_HT, So_Hieu, Ngay_HD, SO_HD, Dien_giai, TK_DOIUNG, DVT, SL, DON_GIA, InventoryItemID, MaterialGoodsName
		/*add by cuongpv*/
		UNION ALL
		SELECT ngay_CT, ngay_HT, So_Hieu, Ngay_HD, SO_HD, Dien_giai, TK_DOIUNG, DVT, SL, DON_GIA, SUM(KHAC) as KHAC, SUM(TT) as TT, InventoryItemID, MaterialGoodsName
		FROM (
			/*lay hoa don  lien ket voi SABill*/
			select a.Date as ngay_CT, a.PostedDate as ngay_HT, a.No as So_Hieu, SABi.InvoiceDate as Ngay_HD, SABi.InvoiceNo as SO_HD, d.reason  as Dien_giai, accountCorresponding as TK_DOIUNG,
			c.Unit as DVT, b.Quantity as SL, b.UnitPrice as DON_GIA, null as KHAC, sum(CreditAmount) as TT, c.ID as  InventoryItemID, c.MaterialGoodsName, a.OrderPriority
			from @tbDataGL a left join SAInvoiceDetail b on a.DetailID = b.ID
			left join MaterialGoods c on b.MaterialGoodsID = c.ID
			left join SAInvoice d on d.ID = b.SAInvoiceID
			left join SABill SABi on d.BillRefID = SABi.ID
			where a.PostedDate between  @FromDate and @ToDate and c.ID in (select InventoryItemID from #tblListInventoryItemID)
			and Account like '511%' and d.BillRefID is not null
			group by a.Date,  a.PostedDate, a.No ,SABi.InvoiceDate , SABi.InvoiceNo, accountCorresponding , 
			c.Unit , b.Quantity , b.UnitPrice, c.ID,c.MaterialGoodsName,d.reason,a.OrderPriority
			Union all
			select a.Date as ngay_CT, a.PostedDate as ngay_HT, a.No as So_Hieu, SABi.InvoiceDate as Ngay_HD, SABi.InvoiceNo as SO_HD, d.reason  as Dien_giai, accountCorresponding as TK_DOIUNG,
			c.Unit as DVT, b.Quantity as SL, b.UnitPrice as DON_GIA, sum(debitAmount) as KHAC, null as TT, c.ID as  InventoryItemID, c.MaterialGoodsName, a.OrderPriority
			from @tbDataGL a left join SAInvoiceDetail b on a.DetailID = b.ID
			left join MaterialGoods c on b.MaterialGoodsID = c.ID
			left join SAInvoice d on d.ID = b.SAInvoiceID
			left join SABill SABi on d.BillRefID = SABi.ID
			where a.PostedDate between  @FromDate and @ToDate and c.ID in (select InventoryItemID from #tblListInventoryItemID)
			and Account like '511%' and (a.TypeID not in (330,340)) and d.BillRefID is not null
			group by a.Date,  a.PostedDate, a.No ,SABi.InvoiceDate , SABi.InvoiceNo, accountCorresponding , 
			c.Unit , b.Quantity , b.UnitPrice, c.ID,c.MaterialGoodsName,d.reason,a.OrderPriority
		) SAB
		GROUP BY ngay_CT, ngay_HT, So_Hieu, Ngay_HD, SO_HD, Dien_giai, TK_DOIUNG, DVT, SL, DON_GIA, InventoryItemID, MaterialGoodsName
		
		UNION ALL
		/*lay gia tri tra lai giam gia*/
		SELECT ngay_CT, ngay_HT, So_Hieu, Ngay_HD, SO_HD, Dien_giai, TK_DOIUNG, DVT, SL, DON_GIA, SUM(KHAC) as KHAC, SUM(TT) as TT, InventoryItemID, MaterialGoodsName
		FROM (
			/*lay dong gia tri SAInvoice*/
			/*lay hoa don ban hang*/
			select a.Date as ngay_CT, a.PostedDate as ngay_HT, a.No as So_Hieu, a.InvoiceDate as Ngay_HD, a.InvoiceNo as SO_HD, d.reason  as Dien_giai, accountCorresponding as TK_DOIUNG,
			null as DVT, null as SL, null as DON_GIA, SUM(DebitAmount - CreditAmount) as KHAC, 0 as TT, c.ID as  InventoryItemID, c.MaterialGoodsName, a.OrderPriority
			from @tbDataGL a left join SAReturnDetail SAR on SAR.ID=a.DetailID join SAInvoiceDetail b on b.ID=SAR.SAInvoiceDetailID
			left join MaterialGoods c on b.MaterialGoodsID = c.ID
			left join SAInvoice d on d.ID = b.SAInvoiceID
			where a.PostedDate between  @FromDate and @ToDate and c.ID in (select InventoryItemID from #tblListInventoryItemID)
			and Account like '511%' and (a.TypeID in (330,340)) and d.BillRefID is null 
			group by a.Date,  a.PostedDate, a.No ,a.InvoiceDate , a.InvoiceNo, accountCorresponding ,
			c.Unit , b.Quantity , b.UnitPrice, c.ID,c.MaterialGoodsName,d.reason,a.OrderPriority
			union all
			select a.Date as ngay_CT, a.PostedDate as ngay_HT, a.No as So_Hieu, SABi.InvoiceDate as Ngay_HD, SABi.InvoiceNo as SO_HD, d.reason  as Dien_giai, accountCorresponding as TK_DOIUNG,
			null as DVT, null as SL, null as DON_GIA, SUM(DebitAmount - CreditAmount) as KHAC, 0 as TT, c.ID as  InventoryItemID, c.MaterialGoodsName, a.OrderPriority
			from @tbDataGL a left join SAReturnDetail SAR on SAR.ID=a.DetailID join SAInvoiceDetail b on b.ID=SAR.SAInvoiceDetailID
			left join MaterialGoods c on b.MaterialGoodsID = c.ID
			left join SAInvoice d on d.ID = b.SAInvoiceID
			left join SABill SABi on d.BillRefID = SABi.ID
			where a.PostedDate between  @FromDate and @ToDate and c.ID in (select InventoryItemID from #tblListInventoryItemID)
			and Account like '511%' and (a.TypeID in (330,340)) and d.BillRefID is not null
			group by a.Date,  a.PostedDate, a.No ,SABi.InvoiceDate , SABi.InvoiceNo, accountCorresponding , 
			c.Unit , b.Quantity , b.UnitPrice, c.ID,c.MaterialGoodsName,d.reason,a.OrderPriority
		) GG
		GROUP BY ngay_CT, ngay_HT, So_Hieu, Ngay_HD, SO_HD, Dien_giai, TK_DOIUNG, DVT, SL, DON_GIA, InventoryItemID, MaterialGoodsName

		SELECT * FROM @tbLuuTru ORDER BY MaterialGoodsName, ngay_HT, So_Hieu
		/*end add by cuongpv*/

		
		/*add by cuongpv*/
		SELECT SUM(g.Sum_Gia_Goc - Sum_Gia_TLGG) as Sum_Gia_Goc, g.MaterialGoodsCode, g.InventoryItemID
		FROM(/*end add by cuongpv*/
		 select isnull(sum(OWAmount),0) Sum_Gia_Goc, 0 as Sum_Gia_TLGG, b.MaterialGoodsCode ,b.ID InventoryItemID from SAInvoiceDetail a, MaterialGoods b 
		 where exists (select InventoryItemID from #tblListInventoryItemID where InventoryItemID = a.MaterialGoodsID)
		 and exists ( select ReferenceID from @tbDataGL where  account like '632%' and PostedDate between @FromDate and @ToDate and ReferenceID=a.SAinvoiceID )/*edit by cuonpv: GeneralLedger -> @tbDataGL;account = '632' -> account like '632%'*/
		 and a.MaterialGoodsID=b.ID group by b.ID,b.MaterialGoodsCode,OrderPriority
		 union all
		 select 0 as Sum_Gia_Goc, isnull(sum(OWAmount),0) as Sum_Gia_TLGG, b.MaterialGoodsCode ,b.ID InventoryItemID from SAReturnDetail a, MaterialGoods b 
		 where exists (select InventoryItemID from #tblListInventoryItemID where InventoryItemID = a.MaterialGoodsID)
		 and exists ( select ReferenceID from @tbDataGL where  account like '632%' and PostedDate between @FromDate and @ToDate and DetailID=a.ID )
		 and a.MaterialGoodsID=b.ID group by b.ID,b.MaterialGoodsCode,OrderPriority
		/*add by cuongpv*/
		) g
		GROUP BY g.MaterialGoodsCode, g.InventoryItemID
		ORDER BY MaterialGoodsCode
		/*end add by cuongpv*/
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_SA_GetReceivableSummaryByGroup]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @AccountNumber NVARCHAR(25),
    @AccountObjectID AS NVARCHAR(MAX),
    @CurrencyID NVARCHAR(3),
    @AccountObjectGroupID AS NVARCHAR(MAX) 
AS
    BEGIN          
        
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/

		DECLARE @tblListAccountObjectID TABLE
            (
             AccountObjectID UNIQUEIDENTIFIER ,
             AccountObjectGroupList NVARCHAR(MAX)           
            ) 
             
        INSERT  INTO @tblListAccountObjectID
        SELECT  
			AO.ID
			,AO.AccountObjectGroupID
        FROM AccountingObject AS AO 
        LEFT JOIN dbo.Func_ConvertStringIntoTable(@AccountObjectID,',') AS TLAO 
			ON AO.ID = TLAO.Value
        WHERE 
		(@AccountObjectID IS NULL OR TLAO.Value IS NOT NULL)
                                    		
        DECLARE @tblAccountNumber TABLE
            (
             AccountNumber NVARCHAR(255) PRIMARY KEY,
             AccountName NVARCHAR(255),
             AccountNumberPercent NVARCHAR(255),
             AccountCategoryKind INT
            )
        IF @AccountNumber IS NOT NULL
            BEGIN
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber,
                                A.AccountName,
                                A.AccountNumber + '%',
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = @AccountNumber
                        ORDER BY A.AccountNumber,
                                A.AccountName                 
            END
        ELSE
            BEGIN 
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber,
                                A.AccountName,
                                A.AccountNumber + '%',
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   DetailType = '1'
                                AND Grade = 1
                                AND IsParentNode = 0
                        ORDER BY A.AccountNumber,
                                A.AccountName
            END
	    
	    DECLARE @tblDebt TABLE
			(
				AccountObjectID			UNIQUEIDENTIFIER
				,AccountNumber			NVARCHAR (25)				
				,AccountObjectGroupList NVARCHAR(Max)				
				,OpenDebitAmountOC		DECIMAL (28,4)
				,OpenDebitAmount		DECIMAL (28,4)
				,OpenCreditAmountOC		DECIMAL (28,4)
				,OpenCreditAmount		DECIMAL (28,4)
				,DebitAmountOC			DECIMAL (28,4)
				,DebitAmount			DECIMAL (28,4)
				,CreditAmountOC			DECIMAL (28,4)
				,CreditAmount			DECIMAL (28,4)
				,CloseDebitAmountOC		DECIMAL (28,4)
				,CloseDebitAmount		DECIMAL (28,4)
				,CloseCreditAmountOC	DECIMAL (28,4)
				,CloseCreditAmount		DECIMAL (28,4)					
			)		
		IF(@CurrencyID = 'VND')
		BEGIN		
		INSERT @tblDebt
        
        SELECT  
                AccountingObjectID,               
                AccountNumber,       
                AccountObjectGroupList,
                (CASE WHEN AccountCategoryKind = 0
                      THEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC)
                      WHEN AccountCategoryKind = 1 THEN $0
                      ELSE CASE WHEN (SUM(OpenningDebitAmountOC - OpenningCreditAmountOC)) > 0
                                THEN (SUM(OpenningDebitAmountOC - OpenningCreditAmountOC))
                                ELSE $0
                           END
                 END) AS OpenningDebitAmountOC,        
                (CASE WHEN AccountCategoryKind = 0
                      THEN SUM(OpenningDebitAmount - OpenningCreditAmount)
                      WHEN AccountCategoryKind = 1 THEN $0
                      ELSE CASE WHEN SUM(OpenningDebitAmount - OpenningCreditAmount) > 0
                                THEN SUM(OpenningDebitAmount  - OpenningCreditAmount)
                                ELSE $0
                           END
                 END) AS OpenningDebitAmount,
                (CASE WHEN AccountCategoryKind = 1
                      THEN (SUM(OpenningCreditAmountOC - OpenningDebitAmountOC))
                      WHEN AccountCategoryKind = 0 THEN $0
                      ELSE CASE WHEN (SUM(OpenningCreditAmountOC - OpenningDebitAmountOC)) > 0
                                THEN (SUM(OpenningCreditAmountOC - OpenningDebitAmountOC))
                                ELSE $0
                           END
                 END) AS OpenningCreditAmountOC,
                (CASE WHEN AccountCategoryKind = 1
                      THEN (SUM(OpenningCreditAmount - OpenningDebitAmount))
                      WHEN AccountCategoryKind = 0 THEN $0
                      ELSE CASE WHEN (SUM(OpenningCreditAmount - OpenningDebitAmount)) > 0
                                THEN (SUM(OpenningCreditAmount - OpenningDebitAmount))
                                ELSE $0
                           END
                 END) AS OpenningCreditAmount, 
                SUM(DebitAmountOC) AS DebitAmountOC,
                SUM(DebitAmount) AS DebitAmount, 
                SUM(CreditAmountOC) AS CreditAmountOC,
                SUM(CreditAmount) AS CreditAmount, 
                (CASE WHEN AccountCategoryKind = 0
                      THEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC + DebitAmountOC - CreditAmountOC)
                      WHEN AccountCategoryKind = 1 THEN $0
                      ELSE CASE WHEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC + DebitAmountOC - CreditAmountOC) > 0
                                THEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC + DebitAmountOC - CreditAmountOC)
                                ELSE $0
                           END
                 END) AS CloseDebitAmountOC,	
                 (CASE WHEN AccountCategoryKind = 0
                      THEN SUM(OpenningDebitAmount - OpenningCreditAmount
                               + DebitAmount - CreditAmount)
                      WHEN AccountCategoryKind = 1 THEN $0
                      ELSE CASE WHEN SUM(OpenningDebitAmount
                                         - OpenningCreditAmount - CreditAmount
                                         + DebitAmount) > 0
                                THEN SUM(OpenningDebitAmount
                                         - OpenningCreditAmount - CreditAmount
                                         + DebitAmount)
                                ELSE $0
                           END
                 END) AS CloseDebitAmount,
                (CASE WHEN AccountCategoryKind = 1
                      THEN SUM(OpenningCreditAmountOC - OpenningDebitAmountOC - DebitAmountOC + CreditAmountOC)
                      WHEN AccountCategoryKind = 0 THEN $0
                      ELSE CASE WHEN (SUM(OpenningCreditAmountOC - OpenningDebitAmountOC - DebitAmountOC + CreditAmountOC)) > 0
                                THEN SUM(OpenningCreditAmountOC  - OpenningDebitAmountOC - DebitAmountOC + CreditAmountOC)
                                ELSE $0
                           END
                 END) AS CloseCreditAmountOC,	     
                (CASE WHEN AccountCategoryKind = 1
                      THEN SUM(OpenningCreditAmount - OpenningDebitAmount
                               + CreditAmount - DebitAmount)
                      WHEN AccountCategoryKind = 0 THEN $0
                      ELSE CASE WHEN (SUM(OpenningCreditAmount
                                          - OpenningDebitAmount + CreditAmount
                                          - DebitAmount)) > 0
                                THEN SUM(OpenningCreditAmount
                                         - OpenningDebitAmount + CreditAmount
                                         - DebitAmount)
                                ELSE $0
                           END
                 END) AS CloseCreditAmount
        FROM    (SELECT AOL.AccountingObjectID,                        
                        TBAN.AccountNumber,
                        TBAN.AccountCategoryKind,
                        ISNULL(AccountObjectGroupList,'') AccountObjectGroupList,
                        SUM(CASE WHEN AOL.PostedDate < @FromDate
                             THEN AOL.DebitAmountOriginal
                             ELSE $0
                        END) AS OpenningDebitAmountOC,	
                        SUM(CASE WHEN AOL.PostedDate < @FromDate
                             THEN AOL.DebitAmount
                             ELSE $0
                        END) AS OpenningDebitAmount, 
                        SUM(CASE WHEN AOL.PostedDate < @FromDate
                             THEN AOL.CreditAmountOriginal
                             ELSE $0
                        END) AS OpenningCreditAmountOC, 
                        SUM(CASE WHEN AOL.PostedDate < @FromDate
                             THEN AOL.CreditAmount
                             ELSE $0
                        END) AS OpenningCreditAmount,                                         
                        SUM(CASE WHEN AOL.PostedDate BETWEEN @FromDate AND @ToDate
                             THEN AOL.DebitAmountOriginal
                             ELSE 0
                        END) AS DebitAmountOC,                      
                        SUM(CASE WHEN AOL.PostedDate BETWEEN @FromDate AND @ToDate
                             THEN AOL.DebitAmount
                             ELSE 0
                        END) AS DebitAmount,                    
                        SUM(CASE WHEN AOL.PostedDate BETWEEN @FromDate AND @ToDate
                             THEN AOL.CreditAmountOriginal
                             ELSE 0
                        END) AS CreditAmountOC, 
                        SUM(CASE WHEN AOL.PostedDate BETWEEN @FromDate AND @ToDate
                             THEN AOL.CreditAmount
                             ELSE 0
                        END) AS CreditAmount
                 FROM   @tbDataGL AS AOL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/    
                        INNER JOIN @tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumberPercent
                        INNER JOIN dbo.Account AS AN ON AOL.Account = AN.AccountNumber
                        INNER JOIN @tblListAccountObjectID AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID                      
                 WHERE  AOL.PostedDate <= @ToDate
                        AND AN.DetailType = '1'
                 GROUP BY 
						AOL.AccountingObjectID,                       
                        TBAN.AccountNumber, 
                        TBAN.AccountCategoryKind
                        ,AccountObjectGroupList                      
                ) AS RSNS
       
        GROUP BY RSNS.AccountingObjectID,               
                 RSNS.AccountNumber 
                 ,RSNS.AccountCategoryKind
                 ,RSNS.AccountObjectGroupList
        HAVING
				SUM(DebitAmountOC)<>0 OR
                SUM(DebitAmount)<>0 OR
                SUM(CreditAmountOC) <>0 OR
                SUM(CreditAmount) <>0 OR
                SUM(OpenningDebitAmountOC - OpenningCreditAmountOC)<>0 OR
                SUM(OpenningDebitAmount - OpenningCreditAmount)<>0
        END
		ELSE
		BEGIN		
		INSERT @tblDebt
        
        SELECT  
                AccountingObjectID,               
                AccountNumber,       
                AccountObjectGroupList,
                (CASE WHEN AccountCategoryKind = 0
                      THEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC)
                      WHEN AccountCategoryKind = 1 THEN $0
                      ELSE CASE WHEN (SUM(OpenningDebitAmountOC - OpenningCreditAmountOC)) > 0
                                THEN (SUM(OpenningDebitAmountOC - OpenningCreditAmountOC))
                                ELSE $0
                           END
                 END) AS OpenningDebitAmountOC,        
                (CASE WHEN AccountCategoryKind = 0
                      THEN SUM(OpenningDebitAmount - OpenningCreditAmount)
                      WHEN AccountCategoryKind = 1 THEN $0
                      ELSE CASE WHEN SUM(OpenningDebitAmount - OpenningCreditAmount) > 0
                                THEN SUM(OpenningDebitAmount  - OpenningCreditAmount)
                                ELSE $0
                           END
                 END) AS OpenningDebitAmount,
                (CASE WHEN AccountCategoryKind = 1
                      THEN (SUM(OpenningCreditAmountOC - OpenningDebitAmountOC))
                      WHEN AccountCategoryKind = 0 THEN $0
                      ELSE CASE WHEN (SUM(OpenningCreditAmountOC - OpenningDebitAmountOC)) > 0
                                THEN (SUM(OpenningCreditAmountOC - OpenningDebitAmountOC))
                                ELSE $0
                           END
                 END) AS OpenningCreditAmountOC,
                (CASE WHEN AccountCategoryKind = 1
                      THEN (SUM(OpenningCreditAmount - OpenningDebitAmount))
                      WHEN AccountCategoryKind = 0 THEN $0
                      ELSE CASE WHEN (SUM(OpenningCreditAmount - OpenningDebitAmount)) > 0
                                THEN (SUM(OpenningCreditAmount - OpenningDebitAmount))
                                ELSE $0
                           END
                 END) AS OpenningCreditAmount, 
                SUM(DebitAmountOC) AS DebitAmountOC,
                SUM(DebitAmount) AS DebitAmount, 
                SUM(CreditAmountOC) AS CreditAmountOC,
                SUM(CreditAmount) AS CreditAmount, 
                (CASE WHEN AccountCategoryKind = 0
                      THEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC + DebitAmountOC - CreditAmountOC)
                      WHEN AccountCategoryKind = 1 THEN $0
                      ELSE CASE WHEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC + DebitAmountOC - CreditAmountOC) > 0
                                THEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC + DebitAmountOC - CreditAmountOC)
                                ELSE $0
                           END
                 END) AS CloseDebitAmountOC,	
                 (CASE WHEN AccountCategoryKind = 0
                      THEN SUM(OpenningDebitAmount - OpenningCreditAmount
                               + DebitAmount - CreditAmount)
                      WHEN AccountCategoryKind = 1 THEN $0
                      ELSE CASE WHEN SUM(OpenningDebitAmount
                                         - OpenningCreditAmount - CreditAmount
                                         + DebitAmount) > 0
                                THEN SUM(OpenningDebitAmount
                                         - OpenningCreditAmount - CreditAmount
                                         + DebitAmount)
                                ELSE $0
                           END
                 END) AS CloseDebitAmount,
                (CASE WHEN AccountCategoryKind = 1
                      THEN SUM(OpenningCreditAmountOC - OpenningDebitAmountOC - DebitAmountOC + CreditAmountOC)
                      WHEN AccountCategoryKind = 0 THEN $0
                      ELSE CASE WHEN (SUM(OpenningCreditAmountOC - OpenningDebitAmountOC - DebitAmountOC + CreditAmountOC)) > 0
                                THEN SUM(OpenningCreditAmountOC  - OpenningDebitAmountOC - DebitAmountOC + CreditAmountOC)
                                ELSE $0
                           END
                 END) AS CloseCreditAmountOC,	     
                (CASE WHEN AccountCategoryKind = 1
                      THEN SUM(OpenningCreditAmount - OpenningDebitAmount
                               + CreditAmount - DebitAmount)
                      WHEN AccountCategoryKind = 0 THEN $0
                      ELSE CASE WHEN (SUM(OpenningCreditAmount
                                          - OpenningDebitAmount + CreditAmount
                                          - DebitAmount)) > 0
                                THEN SUM(OpenningCreditAmount
                                         - OpenningDebitAmount + CreditAmount
                                         - DebitAmount)
                                ELSE $0
                           END
                 END) AS CloseCreditAmount
        FROM    (SELECT AOL.AccountingObjectID,                        
                        TBAN.AccountNumber,
                        TBAN.AccountCategoryKind,
                        ISNULL(AccountObjectGroupList,'') AccountObjectGroupList,
                        SUM(CASE WHEN AOL.PostedDate < @FromDate
                             THEN AOL.DebitAmountOriginal
                             ELSE $0
                        END) AS OpenningDebitAmountOC,	
                        SUM(CASE WHEN AOL.PostedDate < @FromDate
                             THEN AOL.DebitAmountOriginal
                             ELSE $0
                        END) AS OpenningDebitAmount, 
                        SUM(CASE WHEN AOL.PostedDate < @FromDate
                             THEN AOL.CreditAmountOriginal
                             ELSE $0
                        END) AS OpenningCreditAmountOC, 
                        SUM(CASE WHEN AOL.PostedDate < @FromDate
                             THEN AOL.CreditAmountOriginal
                             ELSE $0
                        END) AS OpenningCreditAmount,                                         
                        SUM(CASE WHEN AOL.PostedDate BETWEEN @FromDate AND @ToDate
                             THEN AOL.DebitAmountOriginal
                             ELSE 0
                        END) AS DebitAmountOC,                      
                        SUM(CASE WHEN AOL.PostedDate BETWEEN @FromDate AND @ToDate
                             THEN AOL.DebitAmountOriginal
                             ELSE 0
                        END) AS DebitAmount,                    
                        SUM(CASE WHEN AOL.PostedDate BETWEEN @FromDate AND @ToDate
                             THEN AOL.CreditAmountOriginal
                             ELSE 0
                        END) AS CreditAmountOC, 
                        SUM(CASE WHEN AOL.PostedDate BETWEEN @FromDate AND @ToDate
                             THEN AOL.CreditAmountOriginal
                             ELSE 0
                        END) AS CreditAmount
                 FROM   @tbDataGL AS AOL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/    
                        INNER JOIN @tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumberPercent
                        INNER JOIN dbo.Account AS AN ON AOL.Account = AN.AccountNumber
                        INNER JOIN @tblListAccountObjectID AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID                      
                 WHERE  AOL.PostedDate <= @ToDate
                        AND (@CurrencyID IS NULL OR AOL.CurrencyID = @CurrencyID)
                        AND AN.DetailType = '1'
                 GROUP BY 
						AOL.AccountingObjectID,                       
                        TBAN.AccountNumber, 
                        TBAN.AccountCategoryKind
                        ,AccountObjectGroupList                      
                ) AS RSNS
       
        GROUP BY RSNS.AccountingObjectID,               
                 RSNS.AccountNumber 
                 ,RSNS.AccountCategoryKind
                 ,RSNS.AccountObjectGroupList
        HAVING
				SUM(DebitAmountOC)<>0 OR
                SUM(DebitAmount)<>0 OR
                SUM(CreditAmountOC) <>0 OR
                SUM(CreditAmount) <>0 OR
                SUM(OpenningDebitAmountOC - OpenningCreditAmountOC)<>0 OR
                SUM(OpenningDebitAmount - OpenningCreditAmount)<>0
        END


		SELECT   AOG.ID,
		        AOG.AccountingObjectGroupCode,
				AOG.AccountingObjectGroupName
		        ,AO.AccountingObjectCode
				,AO.AccountingObjectName
				,Ao.Address AS AccountObjectAddress
				,D.* FROM @tblDebt D
		INNER JOIN dbo.AccountingObject AO ON D.AccountObjectID = AO.ID
		left JOIN dbo.AccountingObjectGroup AOG ON AO.AccountObjectGroupID = AOG.ID
		ORDER BY D.AccountNumber
		
		
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_SA_GetReceivableSummary]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @BranchID UNIQUEIDENTIFIER ,
    @AccountNumber NVARCHAR(25) ,
    @AccountObjectID AS NVARCHAR(MAX) ,
    @CurrencyID NVARCHAR(3) ,
    @IsShowInPeriodOnly BIT 
AS
    BEGIN
        
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/
		       
        DECLARE @tblListAccountObjectID TABLE 
            (
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectGroupListCode NVARCHAR(MAX) ,
              AccountObjectCategoryName NVARCHAR(MAX)
            ) 
             
        INSERT  INTO @tblListAccountObjectID
                SELECT  AO.ID ,
                        AO.AccountObjectGroupID ,
						null
                FROM    AccountingObject AS AO
                        LEFT JOIN dbo.Func_ConvertStringIntoTable(@AccountObjectID,
                                                              ',') AS TLAO ON AO.ID = TLAO.Value
                        
                WHERE  
				      ( @AccountObjectID IS NULL
                              OR TLAO.Value IS NOT NULL
                            )
                              			
        DECLARE @tblAccountNumber TABLE
            (
              AccountNumber NVARCHAR(255) PRIMARY KEY ,
              AccountName NVARCHAR(255) ,
              AccountNumberPercent NVARCHAR(255) ,
              AccountCategoryKind INT
            )
        IF @AccountNumber IS NOT NULL
            BEGIN
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = @AccountNumber
                        ORDER BY A.AccountNumber ,
                                A.AccountName 
                
            END
        ELSE
            BEGIN 
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   DetailType = '1'
                                AND Grade = 1
                                AND IsParentNode = 0
                        ORDER BY A.AccountNumber ,
                                A.AccountName
            END
            
	    DECLARE @FirstDateOfYear AS DATETIME
	    SET @FirstDateOfYear = '1/1/' + CAST(Year(@FromDate) AS NVARCHAR(4))	 
		IF(@CurrencyID = 'VND')
		BEGIN
		  SELECT  ROW_NUMBER() OVER ( ORDER BY AccountingObjectCode ) AS RowNum ,
                AccountingObjectID ,
                AccountingObjectCode ,
                AccountingObjectName ,
                AccountObjectAddress ,
                AccountObjectTaxCode ,
                AccountNumber , 
                AccountCategoryKind ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmountOC)
                            - SUM(OpenningCreditAmountOC)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningDebitAmountOC
                                            - OpenningCreditAmountOC) ) > 0
                                 THEN ( SUM(OpenningDebitAmountOC
                                            - OpenningCreditAmountOC) )
                                 ELSE $0
                            END
                  END ) AS OpenningDebitAmountOC ,        
                ( CASE WHEN AccountCategoryKind = 0
                       THEN ( SUM(OpenningDebitAmount - OpenningCreditAmount) )
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningDebitAmount
                                            - OpenningCreditAmount) ) > 0
                                 THEN SUM(OpenningDebitAmount
                                          - OpenningCreditAmount)
                                 ELSE $0
                            END
                  END ) AS OpenningDebitAmount , 
                ( CASE WHEN AccountCategoryKind = 1
                       THEN ( SUM(OpenningCreditAmountOC
                                  - OpenningDebitAmountOC) )
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC) ) > 0
                                 THEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC) )
                                 ELSE $0
                            END
                  END ) AS OpenningCreditAmountOC ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN ( SUM(OpenningCreditAmount - OpenningDebitAmount) )
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount) ) > 0
                                 THEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount) )
                                 ELSE $0
                            END
                  END ) AS OpenningCreditAmount ,
                SUM(DebitAmountOC) AS DebitAmountOC ,
                SUM(DebitAmount) AS DebitAmount ,
                SUM(CreditAmountOC) AS CreditAmountOC ,
                SUM(CreditAmount) AS CreditAmount ,
                SUM(AccumDebitAmountOC) AS AccumDebitAmountOC ,
                SUM(AccumDebitAmount) AS AccumDebitAmount ,
                SUM(AccumCreditAmountOC) AS AccumCreditAmountOC ,
                SUM(AccumCreditAmount) AS AccumCreditAmount , 
                                        
                /* Số dư cuối kỳ = Dư Có đầu kỳ - Dư Nợ đầu kỳ + Phát sinh Có – Phát sinh Nợ
				Nếu Số dư cuối kỳ >0 thì hiển bên cột Dư Có cuối kỳ 
				Nếu số dư cuối kỳ <0 thì hiển thị bên cột Dư Nợ cuối kỳ */
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC
                                + DebitAmountOC - CreditAmountOC)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN SUM(OpenningDebitAmountOC
                                          - OpenningCreditAmountOC
                                          + DebitAmountOC - CreditAmountOC) > 0
                                 THEN SUM(OpenningDebitAmountOC
                                          - OpenningCreditAmountOC
                                          + DebitAmountOC - CreditAmountOC)
                                 ELSE $0
                            END
                  END ) AS CloseDebitAmountOC ,	
                ( CASE WHEN AccountCategoryKind = 1
                       THEN SUM(OpenningCreditAmountOC - OpenningDebitAmountOC
                                - DebitAmountOC + CreditAmountOC)
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC
                                            - DebitAmountOC + CreditAmountOC) ) > 0
                                 THEN SUM(OpenningCreditAmountOC
                                          - OpenningDebitAmountOC
                                          - DebitAmountOC + CreditAmountOC)
                                 ELSE $0
                            END
                  END ) AS CloseCreditAmountOC ,	
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmount - OpenningCreditAmount
                                + DebitAmount - CreditAmount)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN SUM(OpenningDebitAmount
                                          - OpenningCreditAmount
                                          - CreditAmount + DebitAmount) > 0
                                 THEN SUM(OpenningDebitAmount
                                          - OpenningCreditAmount
                                          - CreditAmount + DebitAmount)
                                 ELSE $0
                            END
                  END ) AS CloseDebitAmount ,	
                ( CASE WHEN AccountCategoryKind = 1
                       THEN SUM(OpenningCreditAmount - OpenningDebitAmount
                                + CreditAmount - DebitAmount)
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount
                                            + CreditAmount - DebitAmount) ) > 0
                                 THEN SUM(OpenningCreditAmount
                                          - OpenningDebitAmount + CreditAmount
                                          - DebitAmount)
                                 ELSE $0
                            END
                  END ) AS CloseCreditAmount ,	
                AccountObjectGroupListCode ,
                AccountObjectCategoryName
        FROM    ( SELECT    AO.ID as AccountingObjectID ,
                            AO.AccountingObjectCode ,  
                            AO.AccountingObjectName ,	
                            AO.Address AS AccountObjectAddress ,
                            AO.TaxCode AS AccountObjectTaxCode , 
                            TBAN.AccountNumber , 
                            TBAN.AccountCategoryKind , 
                            CASE WHEN GL.PostedDate < @FromDate
                                 THEN GL.DebitAmountOriginal
                                 ELSE $0
                            END AS OpenningDebitAmountOC ,
                            CASE WHEN GL.PostedDate < @FromDate
                                 THEN GL.DebitAmount
                                 ELSE $0
                            END AS OpenningDebitAmount ,
                            CASE WHEN GL.PostedDate < @FromDate
                                 THEN GL.CreditAmountOriginal
                                 ELSE $0
                            END AS OpenningCreditAmountOC , 
                            CASE WHEN GL.PostedDate < @FromDate
                                 THEN GL.CreditAmount
                                 ELSE $0
                            END AS OpenningCreditAmount ,                                                
                            CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate
                                 THEN GL.DebitAmountOriginal
                                 ELSE 0
                            END AS DebitAmountOC ,                       
                            CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate
                                 THEN GL.DebitAmount
                                 ELSE 0
                            END AS DebitAmount ,                                    
                            CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate
                                 THEN GL.CreditAmountOriginal
                                 ELSE 0
                            END AS CreditAmountOC ,
                            CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate
                                 THEN GL.CreditAmount
                                 ELSE 0
                            END AS CreditAmount ,
                            CASE WHEN GL.PostedDate BETWEEN @FirstDateOfYear AND @ToDate
                                 THEN GL.DebitAmountOriginal
                                 ELSE 0
                            END AS AccumDebitAmountOC ,                                  
                            CASE WHEN GL.PostedDate BETWEEN @FirstDateOfYear AND @ToDate
                                 THEN GL.DebitAmount
                                 ELSE 0
                            END AS AccumDebitAmount ,                                       
                            CASE WHEN GL.PostedDate BETWEEN @FirstDateOfYear AND @ToDate
                                 THEN GL.CreditAmountOriginal
                                 ELSE 0
                            END AS AccumCreditAmountOC ,
                            CASE WHEN GL.PostedDate BETWEEN @FirstDateOfYear AND @ToDate
                                 THEN GL.CreditAmount
                                 ELSE 0
                            END AS AccumCreditAmount ,
                            LAOI.AccountObjectGroupListCode ,
                            LAOI.AccountObjectCategoryName 
                  FROM      @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                            INNER JOIN dbo.AccountingObject AO ON AO.ID = GL.AccountingObjectID
                            INNER JOIN @tblAccountNumber TBAN ON GL.Account LIKE TBAN.AccountNumberPercent
                            INNER JOIN dbo.Account AS AN ON GL.Account = AN.AccountNumber
                            INNER JOIN @tblListAccountObjectID AS LAOI ON GL.AccountingObjectID = LAOI.AccountObjectID
                  WHERE     GL.PostedDate <= @ToDate
                           
                            AND AN.DetailType = '1'
                ) AS RSNS
        GROUP BY RSNS.AccountingObjectID ,
                RSNS.AccountingObjectCode ,
                RSNS.AccountingObjectName ,    
                RSNS.AccountObjectAddress ,
                RSNS.AccountObjectTaxCode , 
                RSNS.AccountNumber ,
                RSNS.AccountCategoryKind ,
                RSNS.AccountObjectGroupListCode ,
                RSNS.AccountObjectCategoryName 
        HAVING  SUM(DebitAmountOC) <> 0
                OR SUM(DebitAmount) <> 0
                OR SUM(CreditAmountOC) <> 0
                OR SUM(CreditAmount) <> 0
                OR SUM(OpenningDebitAmountOC - OpenningCreditAmountOC) <> 0
                OR SUM(OpenningDebitAmount - OpenningCreditAmount) <> 0                
                OR(@IsShowInPeriodOnly = 0 AND (
                 SUM(AccumDebitAmountOC) <>0	
                OR SUM(AccumDebitAmount) <>0
                OR SUM(AccumCreditAmountOC) <>0  
                OR SUM(AccumCreditAmount)<>0))
        ORDER BY RSNS.AccountingObjectCode
        OPTION(RECOMPILE)
		END
	    ELSE
		BEGIN
        SELECT  ROW_NUMBER() OVER ( ORDER BY AccountingObjectCode ) AS RowNum ,
                AccountingObjectID ,
                AccountingObjectCode ,
                AccountingObjectName ,
                AccountObjectAddress ,
                AccountObjectTaxCode ,
                AccountNumber , 
                AccountCategoryKind ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmountOC)
                            - SUM(OpenningCreditAmountOC)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningDebitAmountOC
                                            - OpenningCreditAmountOC) ) > 0
                                 THEN ( SUM(OpenningDebitAmountOC
                                            - OpenningCreditAmountOC) )
                                 ELSE $0
                            END
                  END ) AS OpenningDebitAmountOC ,        
                ( CASE WHEN AccountCategoryKind = 0
                       THEN ( SUM(OpenningDebitAmount - OpenningCreditAmount) )
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningDebitAmount
                                            - OpenningCreditAmount) ) > 0
                                 THEN SUM(OpenningDebitAmount
                                          - OpenningCreditAmount)
                                 ELSE $0
                            END
                  END ) AS OpenningDebitAmount , 
                ( CASE WHEN AccountCategoryKind = 1
                       THEN ( SUM(OpenningCreditAmountOC
                                  - OpenningDebitAmountOC) )
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC) ) > 0
                                 THEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC) )
                                 ELSE $0
                            END
                  END ) AS OpenningCreditAmountOC ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN ( SUM(OpenningCreditAmount - OpenningDebitAmount) )
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount) ) > 0
                                 THEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount) )
                                 ELSE $0
                            END
                  END ) AS OpenningCreditAmount ,
                SUM(DebitAmountOC) AS DebitAmountOC ,
                SUM(DebitAmount) AS DebitAmount ,
                SUM(CreditAmountOC) AS CreditAmountOC ,
                SUM(CreditAmount) AS CreditAmount ,
                SUM(AccumDebitAmountOC) AS AccumDebitAmountOC ,
                SUM(AccumDebitAmount) AS AccumDebitAmount ,
                SUM(AccumCreditAmountOC) AS AccumCreditAmountOC ,
                SUM(AccumCreditAmount) AS AccumCreditAmount , 
                                        
                /* Số dư cuối kỳ = Dư Có đầu kỳ - Dư Nợ đầu kỳ + Phát sinh Có – Phát sinh Nợ
				Nếu Số dư cuối kỳ >0 thì hiển bên cột Dư Có cuối kỳ 
				Nếu số dư cuối kỳ <0 thì hiển thị bên cột Dư Nợ cuối kỳ */
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC
                                + DebitAmountOC - CreditAmountOC)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN SUM(OpenningDebitAmountOC
                                          - OpenningCreditAmountOC
                                          + DebitAmountOC - CreditAmountOC) > 0
                                 THEN SUM(OpenningDebitAmountOC
                                          - OpenningCreditAmountOC
                                          + DebitAmountOC - CreditAmountOC)
                                 ELSE $0
                            END
                  END ) AS CloseDebitAmountOC ,	
                ( CASE WHEN AccountCategoryKind = 1
                       THEN SUM(OpenningCreditAmountOC - OpenningDebitAmountOC
                                - DebitAmountOC + CreditAmountOC)
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC
                                            - DebitAmountOC + CreditAmountOC) ) > 0
                                 THEN SUM(OpenningCreditAmountOC
                                          - OpenningDebitAmountOC
                                          - DebitAmountOC + CreditAmountOC)
                                 ELSE $0
                            END
                  END ) AS CloseCreditAmountOC ,	
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmount - OpenningCreditAmount
                                + DebitAmount - CreditAmount)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN SUM(OpenningDebitAmount
                                          - OpenningCreditAmount
                                          - CreditAmount + DebitAmount) > 0
                                 THEN SUM(OpenningDebitAmount
                                          - OpenningCreditAmount
                                          - CreditAmount + DebitAmount)
                                 ELSE $0
                            END
                  END ) AS CloseDebitAmount ,	
                ( CASE WHEN AccountCategoryKind = 1
                       THEN SUM(OpenningCreditAmount - OpenningDebitAmount
                                + CreditAmount - DebitAmount)
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount
                                            + CreditAmount - DebitAmount) ) > 0
                                 THEN SUM(OpenningCreditAmount
                                          - OpenningDebitAmount + CreditAmount
                                          - DebitAmount)
                                 ELSE $0
                            END
                  END ) AS CloseCreditAmount ,	
                AccountObjectGroupListCode ,
                AccountObjectCategoryName
        FROM    ( SELECT    AO.ID as AccountingObjectID ,
                            AO.AccountingObjectCode ,  
                            AO.AccountingObjectName ,	
                            AO.Address AS AccountObjectAddress ,
                            AO.TaxCode AS AccountObjectTaxCode , 
                            TBAN.AccountNumber , 
                            TBAN.AccountCategoryKind , 
                            CASE WHEN GL.PostedDate < @FromDate
                                 THEN GL.DebitAmountOriginal
                                 ELSE $0
                            END AS OpenningDebitAmountOC ,
                            CASE WHEN GL.PostedDate < @FromDate
                                 THEN GL.DebitAmount
                                 ELSE $0
                            END AS OpenningDebitAmount ,
                            CASE WHEN GL.PostedDate < @FromDate
                                 THEN GL.CreditAmountOriginal
                                 ELSE $0
                            END AS OpenningCreditAmountOC , 
                            CASE WHEN GL.PostedDate < @FromDate
                                 THEN GL.CreditAmount
                                 ELSE $0
                            END AS OpenningCreditAmount ,                                                
                            CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate
                                 THEN GL.DebitAmountOriginal
                                 ELSE 0
                            END AS DebitAmountOC ,                       
                            CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate
                                 THEN GL.DebitAmount
                                 ELSE 0
                            END AS DebitAmount ,                                    
                            CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate
                                 THEN GL.CreditAmountOriginal
                                 ELSE 0
                            END AS CreditAmountOC ,
                            CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate
                                 THEN GL.CreditAmount
                                 ELSE 0
                            END AS CreditAmount ,
                            CASE WHEN GL.PostedDate BETWEEN @FirstDateOfYear AND @ToDate
                                 THEN GL.DebitAmountOriginal
                                 ELSE 0
                            END AS AccumDebitAmountOC ,                                  
                            CASE WHEN GL.PostedDate BETWEEN @FirstDateOfYear AND @ToDate
                                 THEN GL.DebitAmount
                                 ELSE 0
                            END AS AccumDebitAmount ,                                       
                            CASE WHEN GL.PostedDate BETWEEN @FirstDateOfYear AND @ToDate
                                 THEN GL.CreditAmountOriginal
                                 ELSE 0
                            END AS AccumCreditAmountOC ,
                            CASE WHEN GL.PostedDate BETWEEN @FirstDateOfYear AND @ToDate
                                 THEN GL.CreditAmount
                                 ELSE 0
                            END AS AccumCreditAmount ,
                            LAOI.AccountObjectGroupListCode ,
                            LAOI.AccountObjectCategoryName 
                  FROM      @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                            INNER JOIN dbo.AccountingObject AO ON AO.ID = GL.AccountingObjectID
                            INNER JOIN @tblAccountNumber TBAN ON GL.Account LIKE TBAN.AccountNumberPercent
                            INNER JOIN dbo.Account AS AN ON GL.Account = AN.AccountNumber
                            INNER JOIN @tblListAccountObjectID AS LAOI ON GL.AccountingObjectID = LAOI.AccountObjectID
                  WHERE     GL.PostedDate <= @ToDate
                            AND ( @CurrencyID IS NULL
                                  OR GL.CurrencyID = @CurrencyID
                                )
                            AND AN.DetailType = '1'
                ) AS RSNS
        GROUP BY RSNS.AccountingObjectID ,
                RSNS.AccountingObjectCode ,
                RSNS.AccountingObjectName ,    
                RSNS.AccountObjectAddress ,
                RSNS.AccountObjectTaxCode , 
                RSNS.AccountNumber ,
                RSNS.AccountCategoryKind ,
                RSNS.AccountObjectGroupListCode ,
                RSNS.AccountObjectCategoryName 
        HAVING  SUM(DebitAmountOC) <> 0
                OR SUM(DebitAmount) <> 0
                OR SUM(CreditAmountOC) <> 0
                OR SUM(CreditAmount) <> 0
                OR SUM(OpenningDebitAmountOC - OpenningCreditAmountOC) <> 0
                OR SUM(OpenningDebitAmount - OpenningCreditAmount) <> 0                
                OR(@IsShowInPeriodOnly = 0 AND (
                 SUM(AccumDebitAmountOC) <>0	
                OR SUM(AccumDebitAmount) <>0
                OR SUM(AccumCreditAmountOC) <>0  
                OR SUM(AccumCreditAmount)<>0))
        ORDER BY RSNS.AccountingObjectCode
        OPTION(RECOMPILE)
		END
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*Edit by: cuongpv
*Edit Date: 07/05/2019
*Description: lay them du lieu tu cac bang PPService, PPDiscountReturn, SAReturn
*/
ALTER procedure [dbo].[Proc_SA_GetDetailPayS12] 
(@fromdate DATETIME,
 @todate DATETIME,
 @account nvarchar(255),
 @currency nvarchar(3),
 @AccountObjectID AS NVARCHAR(MAX)
)
as
begin
DECLARE @tblAccountNumber TABLE
            (
              AccountNumber NVARCHAR(15),
			  AccountGroupKind int
            )
INSERT  INTO @tblAccountNumber
                SELECT  AO.AccountNumber,
				        AO.AccountGroupKind
                FROM    Account AS AO
                        inner JOIN dbo.Func_ConvertStringIntoTable_Nvarchar(@account,
                                                              ',') AS TLAO ON AO.AccountNumber = TLAO.Value
DECLARE @tblListAccountObjectID TABLE 
            (
              AccountObjectID UNIQUEIDENTIFIER ,
			  AccountObjectCode NVARCHAR(100),
              AccountObjectGroupListCode NVARCHAR(MAX) ,
              AccountObjectCategoryName NVARCHAR(MAX)
            ) 
             
        INSERT  INTO @tblListAccountObjectID
                SELECT  AO.ID ,
				        AO.AccountingObjectCode,
                        AO.AccountObjectGroupID ,
						null
                FROM    AccountingObject AS AO
                        inner JOIN dbo.Func_ConvertStringIntoTable(@AccountObjectID,
                                                              ',') AS TLAO ON AO.ID = TLAO.Value
                        

		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @todate
		/*end add by cuongpv*/

DECLARE @tblResult TABLE
            (
              RowNum INT IDENTITY(1, 1)
                         PRIMARY KEY ,
              AccountNumber NVARCHAR(20)  ,
			  AccountGroupKind int, 
              AccountObjectID UNIQUEIDENTIFIER ,
			  AccountObjectCode NVARCHAR(100)  ,
			  RefDate DATETIME , 
			  RefNo NVARCHAR(25)  ,
			  PostedDate DATETIME ,
			  JournalMemo NVARCHAR(255)  ,
			  CorrespondingAccountNumber NVARCHAR(20)  , 
			  DueDate DATETIME,
			  DebitAmount MONEY , 
              CreditAmount MONEY , 
			  ClosingDebitAmount MONEY , 
              ClosingCreditAmount MONEY,
			  OrderType int,
			  OrderPriority int
            )

if @account = '331'
Begin
	/*add by cuongpv tao bang du lieu DueDate tu cac bang*/
	DECLARE @tbDataDueDate TABLE(
		ID UNIQUEIDENTIFIER,
		IDDetail UNIQUEIDENTIFIER,
		DueDate DATETIME
	)
	/*end add by cuongpv*/
	/*add by cuongpv lay du lieu DueDate tu cac bang PPinvoice, PPservice, PPDiscountReturn*/
	INSERT INTO @tbDataDueDate
	SELECT Piv.ID, Pivdt.ID, Piv.DueDate FROM PPInvoiceDetail Pivdt LEFT JOIN PPInvoice Piv ON Piv.ID = Pivdt.PPInvoiceID
	WHERE Pivdt.ID IN (Select DetailID From @tbDataGL Where PostedDate between @fromdate and @todate)
	
	UNION ALL

	SELECT Psv.ID, Psvdt.ID, Psv.DueDate FROM PPServiceDetail Psvdt LEFT JOIN PPService Psv ON Psv.ID = Psvdt.PPServiceID
	WHERE Psvdt.ID IN (Select DetailID From @tbDataGL Where PostedDate between @fromdate and @todate)

	UNION ALL

	SELECT Pdcrt.ID, Pdcrtdt.ID, Pdcrt.DueDate FROM PPDiscountReturnDetail Pdcrtdt LEFT JOIN PPDiscountReturn Pdcrt ON Pdcrt.ID = Pdcrtdt.PPDiscountReturnID
	WHERE Pdcrtdt.ID IN (Select DetailID From @tbDataGL Where PostedDate between @fromdate and @todate)
	/*end add by cuongpv*/
	insert into @tblResult
	SELECT  a.AccountNumber,
			A.AccountGroupKind,
			LAOI.AccountObjectID,
			LAOI.AccountObjectCode,
			null as NgayThangGS,
			null as Sohieu, 
			null as ngayCTu,
			N'Số dư đầu kỳ' as Diengiai ,
			null as TKDoiUng, 
			null as ThoiHanDuocCKhau, 
			$0 AS PSNO ,
			$0 AS PSCO ,
			CASE WHEN SUM(GL.DebitAmount - GL.CreditAmount) > 0
					THEN SUM(GL.DebitAmount - GL.CreditAmount)
					ELSE 0
			END AS DuNo ,
			CASE WHEN SUM(GL.CreditAmount - GL.DebitAmount) > 0
					THEN SUM(GL.CreditAmount - GL.DebitAmount)
					ELSE 0
			END AS DuCo,
			0 AS OrderType,
			OrderPriority
		FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
		INNER JOIN @tblAccountNumber A ON GL.Account = A.AccountNumber 
		INNER JOIN @tblListAccountObjectID AS LAOI ON GL.AccountingObjectID = LAOI.AccountObjectID
		WHERE   ( GL.PostedDate < @FromDate )  
		and GL.CurrencyID = @currency                    
		GROUP BY 
			A.AccountNumber,LAOI.AccountObjectID,A.AccountGroupKind,LAOI.AccountObjectCode,OrderPriority
		HAVING  SUM(GL.DebitAmount - GL.CreditAmount) <> 0
	UNION ALL
	select  an.AccountNumber,
			An.AccountGroupKind,
			LAOI.AccountObjectID,
			LAOI.AccountObjectCode,
			a.PostedDate as NgayThangGS, 
			a.No as Sohieu, 
			a.Date as ngayCTu,
			a.Description as Diengiai, 
			a.AccountCorresponding as TKDoiUng, 
			DueDate as ThoiHanDuocCKhau, 
			sum(a.DebitAmount) as PSNO, 
			sum(a.CreditAmount) as PSCO,
			sum(fnsd.DebitAmount) DuNo,
			sum(fnsd.CreditAmount) DuCo,
			1 AS OrderType,
			OrderPriority
	 from @tbDataGL a /*edit by cuongpv GeneralLedger -> @tbDataGL*/		
	LEFT JOIN @tbDataDueDate tbDuedate ON tbDuedate.IDDetail = a.DetailID
	inner join @tblAccountNumber as an on a.Account = an.AccountNumber
	INNER JOIN @tblListAccountObjectID AS LAOI ON a.AccountingObjectID = LAOI.AccountObjectID
	INNER JOIN Func_SoDu (
	   @fromdate
	  ,@todate
	  ,1) AS fnsd ON LAOI.AccountObjectID = fnsd.AccountObjectID and an.AccountNumber = fnsd.AccountNumber			
	where a.PostedDate between @fromdate and @todate
	and  a.CurrencyID = @currency  
	group by an.AccountNumber,
			LAOI.AccountObjectID,
			a.PostedDate,
			a.No , 
			a.Date ,
			a.Description, 
			a.AccountCorresponding, 
			DueDate,
			An.AccountGroupKind,
			LAOI.AccountObjectCode,
			OrderPriority
	order by AccountNumber,NgayThangGS,OrderPriority
End
else
Begin
	/*add by cuongpv tao bang du lieu DueDate tu cac bang*/
	DECLARE @tbDataDueDate1 TABLE(
		ID UNIQUEIDENTIFIER,
		IDDetail UNIQUEIDENTIFIER,
		DueDate DATETIME
	)
	/*end add by cuongpv*/
	/*add by cuongpv lay du lieu DueDate tu cac bang SAInvoice, SAReturn*/
	INSERT INTO @tbDataDueDate1
	SELECT SAiv.ID, SAivdt.ID, SAiv.DueDate FROM SAInvoiceDetail SAivdt LEFT JOIN SAInvoice SAiv ON SAiv.ID = SAivdt.SAInvoiceID
	WHERE SAivdt.ID IN (Select DetailID From @tbDataGL Where PostedDate between @fromdate and @todate)
	
	UNION ALL

	SELECT SArt.ID, SArtdt.ID, SArt.DueDate FROM SAReturnDetail SArtdt LEFT JOIN SAReturn SArt ON SArt.ID = SArtdt.SAReturnID
	WHERE SArtdt.ID IN (Select DetailID From @tbDataGL Where PostedDate between @fromdate and @todate)
	/*end add by cuongpv*/
	insert into @tblResult
		SELECT  a.AccountNumber,
			A.AccountGroupKind,
			LAOI.AccountObjectID,
			LAOI.AccountObjectCode,
			null as NgayThangGS,
			null as Sohieu, 
			null as ngayCTu,
			N'Số dư đầu kỳ' as Diengiai ,
			null as TKDoiUng, 
			null as ThoiHanDuocCKhau, 
			$0 AS PSNO ,
			$0 AS PSCO ,
			CASE WHEN SUM(GL.DebitAmount - GL.CreditAmount) > 0
					THEN SUM(GL.DebitAmount - GL.CreditAmount)
					ELSE 0
			END AS DuNo ,
			CASE WHEN SUM(GL.CreditAmount - GL.DebitAmount) > 0
					THEN SUM(GL.CreditAmount - GL.DebitAmount)
					ELSE 0
			END AS DuCo,
			0 AS OrderType,
			OrderPriority
		FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
		INNER JOIN @tblAccountNumber A ON GL.Account = A.AccountNumber 
		INNER JOIN @tblListAccountObjectID AS LAOI ON GL.AccountingObjectID = LAOI.AccountObjectID
		WHERE   ( GL.PostedDate < @FromDate )  
		and GL.CurrencyID = @currency                    
		GROUP BY 
			A.AccountNumber,LAOI.AccountObjectID,A.AccountGroupKind,LAOI.AccountObjectCode,OrderPriority
		HAVING  SUM(GL.DebitAmount - GL.CreditAmount) <> 0
	UNION ALL
		select  an.AccountNumber,
				An.AccountGroupKind,
				LAOI.AccountObjectID,
				LAOI.AccountObjectCode,
				a.PostedDate as NgayThangGS, 
				a.No as Sohieu, 
				a.Date as ngayCTu,
				a.Description as Diengiai, 
				a.AccountCorresponding as TKDoiUng, 
				DueDate as ThoiHanDuocCKhau, 
				sum(a.DebitAmount) as PSNO, 
				sum(a.CreditAmount) as PSCO,
				sum(fnsd.DebitAmount) DuNo,
				sum(fnsd.CreditAmount) DuCo,
				1 AS OrderType,
				OrderPriority
		 from @tbDataGL a	/*edit by cuongpv GeneralLedger -> @tbDataGL*/
		LEFT JOIN @tbDataDueDate1 tbDuedate ON tbDuedate.IDDetail = a.DetailID
		inner join @tblAccountNumber as an on a.Account = an.AccountNumber
		INNER JOIN @tblListAccountObjectID AS LAOI ON a.AccountingObjectID = LAOI.AccountObjectID
		INNER JOIN Func_SoDu (
		   @fromdate
		  ,@todate
		  ,1) AS fnsd ON LAOI.AccountObjectID = fnsd.AccountObjectID	and an.AccountNumber = fnsd.AccountNumber			
		where a.PostedDate between @fromdate and @todate
		and  a.CurrencyID = @currency  
		group by an.AccountNumber,
				LAOI.AccountObjectID,
				a.PostedDate,
				a.No , 
				a.Date ,
				a.Description, 
				a.AccountCorresponding, 
				DueDate,An.AccountGroupKind,LAOI.AccountObjectCode,OrderPriority
		order by AccountNumber,AccountObjectID,NgayThangGS,OrderPriority
End
DECLARE @CloseAmount AS Money ,
        @AccountObjectCode_tmp NVARCHAR(100) ,
        @AccountNumber_tmp NVARCHAR(20)
SELECT  @CloseAmount = 0 ,
        @AccountObjectCode_tmp = N'' ,
        @AccountNumber_tmp = N''
UPDATE  @tblResult
        SET     
                 @CloseAmount = ( CASE WHEN OrderType = 0 THEN ( CASE WHEN ClosingDebitAmount = 0 THEN ClosingCreditAmount
                                                                     ELSE -1 * ClosingDebitAmount
                                                                END )
                                      WHEN @AccountObjectCode_tmp <> AccountObjectCode or @AccountNumber_tmp <> AccountNumber THEN CreditAmount - DebitAmount
                                      ELSE @CloseAmount + CreditAmount - DebitAmount
                                 END ) ,
                ClosingDebitAmount = ( CASE WHEN AccountGroupKind = 0 THEN -1 * @CloseAmount
                                            WHEN AccountGroupKind = 1 THEN $0
                                            ELSE CASE WHEN @CloseAmount < 0 THEN -1 * @CloseAmount
                                                      ELSE $0
                                                 END
                                       END ) ,
                ClosingCreditAmount = ( CASE WHEN AccountGroupKind = 1 THEN @CloseAmount
                                             WHEN AccountGroupKind = 0 THEN $0
                                             ELSE CASE WHEN @CloseAmount > 0 THEN @CloseAmount
                                                       ELSE $0
                                                  END
                                        END ) ,
                @AccountObjectCode_tmp = AccountObjectCode ,
				@AccountNumber_tmp = AccountNumber							
select AccountNumber,
			AccountObjectID,
			RefDate as NgayThangGS, 
			RefNo as Sohieu, 
			PostedDate as ngayCTu,
			JournalMemo as Diengiai, 
			CorrespondingAccountNumber as TKDoiUng, 
			DueDate as ThoiHanDuocCKhau, 
			DebitAmount as PSNO, 
			CreditAmount as PSCO,
			ClosingDebitAmount as DuNo,
			ClosingCreditAmount as DuCo
from @tblResult 
order by RowNum,OrderPriority
end				
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_PO_SummaryPayable]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountNumber NVARCHAR(20) ,
    @AccountObjectID AS NVARCHAR(MAX) ,
    @CurrencyID NVARCHAR(3)
AS
    BEGIN
	    
		/*Add by cuongpv de sua cach lam tron*/
	DECLARE @tbDataGL TABLE(
		ID uniqueidentifier,
		BranchID uniqueidentifier,
		ReferenceID uniqueidentifier,
		TypeID int,
		Date datetime,
		PostedDate datetime,
		No nvarchar(25),
		InvoiceDate datetime,
		InvoiceNo nvarchar(25),
		Account nvarchar(25),
		AccountCorresponding nvarchar(25),
		BankAccountDetailID uniqueidentifier,
		CurrencyID nvarchar(3),
		ExchangeRate decimal(25, 10),
		DebitAmount decimal(25,0),
		DebitAmountOriginal decimal(25,2),
		CreditAmount decimal(25,0),
		CreditAmountOriginal decimal(25,2),
		Reason nvarchar(512),
		Description nvarchar(512),
		VATDescription nvarchar(512),
		AccountingObjectID uniqueidentifier,
		EmployeeID uniqueidentifier,
		BudgetItemID uniqueidentifier,
		CostSetID uniqueidentifier,
		ContractID uniqueidentifier,
		StatisticsCodeID uniqueidentifier,
		InvoiceSeries nvarchar(25),
		ContactName nvarchar(512),
		DetailID uniqueidentifier,
		RefNo nvarchar(25),
		RefDate datetime,
		DepartmentID uniqueidentifier,
		ExpenseItemID uniqueidentifier,
		OrderPriority int,
		IsIrrationalCost bit
	)

	INSERT INTO @tbDataGL
	SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
	/*end add by cuongpv*/
		
		DECLARE @tblListAccountObjectID TABLE 
            (
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(25) ,
              AccountObjectName NVARCHAR(255) ,
              AccountObjectAddress NVARCHAR(255) ,
              AccountObjectTaxCode NVARCHAR(200) ,
              AccountObjectGroupListCode NVARCHAR(MAX) 
                                                       NULL ,
              AccountObjectGroupListName NVARCHAR(MAX) 
                                                       NULL 
            ) 
        INSERT  INTO @tblListAccountObjectID
                SELECT  AO.ID as AccountObjectID ,
                        AO.AccountingObjectCode ,
                        AO.AccountingObjectName ,
                        AO.Address ,
                        AO.TaxCode ,
                        null AccountingObjectGroupCode ,
                        null AccountingObjectGroupName 
                FROM    dbo.Func_ConvertStringIntoTable(@AccountObjectID, ',') tblAccountObjectSelected
                        INNER JOIN dbo.AccountingObject AO ON AO.ID = tblAccountObjectSelected.Value		
      
                 
		
		DECLARE @StartDateOfPeriod DATETIME
		SET @StartDateOfPeriod = '1/1/'+CAST(Year(@FromDate) AS NVARCHAR(5))
                         
           
        DECLARE @tblAccountNumber TABLE
            (
              AccountNumber NVARCHAR(20) PRIMARY KEY ,
              AccountName NVARCHAR(255) ,
              AccountNumberPercent NVARCHAR(25) ,
              AccountCategoryKind INT
            )
        IF @AccountNumber IS NOT NULL 
            BEGIN
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = @AccountNumber
                        ORDER BY A.AccountNumber ,
                                A.AccountName 
                
            END
        ELSE 
            BEGIN 
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = '331'
                        ORDER BY A.AccountNumber ,
                                A.AccountName
            END
	    IF(@CurrencyID = 'VND')
		BEGIN
		 SELECT  ROW_NUMBER() OVER ( ORDER BY AccountObjectCode ) AS RowNum ,
                AccountingObjectID ,
                AccountObjectCode ,
                AccountObjectName ,
                AccountObjectAddress ,
                AccountObjectTaxCode ,
                AccountNumber ,
                AccountCategoryKind ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmountOC)
                            - SUM(OpenningCreditAmountOC)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningDebitAmountOC
                                            - OpenningCreditAmountOC) ) > 0
                                 THEN ( SUM(OpenningDebitAmountOC
                                            - OpenningCreditAmountOC) )
                                 ELSE $0
                            END
                  END ) AS OpeningDebitAmountOC ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN ( SUM(OpenningDebitAmount - OpenningCreditAmount) )
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningDebitAmount
                                            - OpenningCreditAmount) ) > 0
                                 THEN SUM(OpenningDebitAmount
                                          - OpenningCreditAmount)
                                 ELSE $0
                            END
                  END ) AS OpeningDebitAmount ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN ( SUM(OpenningCreditAmountOC
                                  - OpenningDebitAmountOC) )
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC) ) > 0
                                 THEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC) )
                                 ELSE $0
                            END
                  END ) AS OpeningCreditAmountOC ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN ( SUM(OpenningCreditAmount - OpenningDebitAmount) )
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount) ) > 0
                                 THEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount) )
                                 ELSE $0
                            END
                  END ) AS OpeningCreditAmount ,
                SUM(DebitAmountOC) AS DebitAmountOC ,
                SUM(DebitAmount) AS DebitAmount ,
                SUM(CreditAmountOC) AS CreditAmountOC ,
                SUM(CreditAmount) AS CreditAmount ,
                SUM(AccumDebitAmountOC) AS AccumDebitAmountOC ,
                SUM(AccumDebitAmount) AS AccumDebitAmount ,
                SUM(AccumCreditAmountOC) AS AccumCreditAmountOC ,
                SUM(AccumCreditAmount) AS AccumCreditAmount ,
                /* Số dư cuối kỳ = Dư Có đầu kỳ - Dư Nợ đầu kỳ + Phát sinh Có – Phát sinh Nợ
				Nếu Số dư cuối kỳ >0 thì hiển bên cột Dư Có cuối kỳ 
				Nếu số dư cuối kỳ <0 thì hiển thị bên cột Dư Nợ cuối kỳ */
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC
                                + DebitAmountOC - CreditAmountOC)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN SUM(OpenningCreditAmountOC
                                          - OpenningDebitAmountOC
                                          + CreditAmountOC - DebitAmountOC) > 0
                                 THEN $0
                                 ELSE SUM(OpenningDebitAmountOC
                                          - OpenningCreditAmountOC
                                          + DebitAmountOC - CreditAmountOC)
                            END
                  END ) AS CloseDebitAmountOC ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN SUM(OpenningCreditAmountOC - OpenningDebitAmountOC
                                + CreditAmountOC - DebitAmountOC)
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC
                                            + CreditAmountOC - DebitAmountOC) ) > 0
                                 THEN SUM(OpenningCreditAmountOC
                                          - OpenningDebitAmountOC
                                          + CreditAmountOC - DebitAmountOC)
                                 ELSE $0
                            END
                  END ) AS CloseCreditAmountOC ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmount - OpenningCreditAmount
                                + DebitAmount - CreditAmount)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN SUM(OpenningCreditAmount
                                          - OpenningDebitAmount + CreditAmount
                                          - DebitAmount) > 0 THEN $0
                                 ELSE SUM(OpenningDebitAmount
                                          - OpenningCreditAmount + DebitAmount
                                          - CreditAmount)
                            END
                  END ) AS ClosingDebitAmount ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN SUM(OpenningCreditAmount - OpenningDebitAmount
                                + CreditAmount - DebitAmount)
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount
                                            + CreditAmount - DebitAmount) ) > 0
                                 THEN SUM(OpenningCreditAmount
                                          - OpenningDebitAmount + CreditAmount
                                          - DebitAmount)
                                 ELSE $0
                            END
                  END ) AS ClosingCreditAmount ,
                AccountObjectGroupListCode ,
                AccountObjectGroupListName
        FROM    ( SELECT    AOL.AccountingObjectID ,
                            LAOI.AccountObjectCode ,
                            LAOI.AccountObjectName ,
                            LAOI.AccountObjectAddress,
                            LAOI.AccountObjectTaxCode ,
                            TBAN.AccountNumber ,
                            TBAN.AccountCategoryKind ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.DebitAmountOriginal
                                 ELSE $0
                            END AS OpenningDebitAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.DebitAmount
                                 ELSE $0
                            END AS OpenningDebitAmount ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.CreditAmountOriginal
                                 ELSE $0
                            END AS OpenningCreditAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.CreditAmount
                                 ELSE $0
                            END AS OpenningCreditAmount ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.DebitAmountOriginal
                            END AS DebitAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.DebitAmount
                            END AS DebitAmount ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.CreditAmountOriginal
                            END AS CreditAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.CreditAmount
                            END AS CreditAmount ,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.DebitAmountOriginal ELSE 0 END AS AccumDebitAmountOC,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.DebitAmount ELSE 0 END AS AccumDebitAmount,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.CreditAmountOriginal ELSE 0 END AS AccumCreditAmountOC,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.CreditAmount ELSE 0 END AS AccumCreditAmount,
                            LAOI.AccountObjectGroupListCode ,
                            LAOI.AccountObjectGroupListName
                  FROM      @tbDataGL AS AOL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                            INNER JOIN @tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumberPercent
                            INNER JOIN dbo.Account AS AN ON AOL.Account = AN.AccountNumber
                            INNER JOIN @tblListAccountObjectID AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID
                  WHERE     AOL.PostedDate <= @ToDate
                           
                ) AS RSNS
        GROUP BY RSNS.AccountingObjectID ,
                RSNS.AccountObjectCode ,
                RSNS.AccountObjectName ,
                RSNS.AccountObjectAddress ,
                RSNS.AccountObjectTaxCode ,
                RSNS.AccountNumber ,
                RSNS.AccountCategoryKind ,
                RSNS.AccountObjectGroupListCode ,
                RSNS.AccountObjectGroupListName 
        HAVING 
				SUM(DebitAmountOC) <> 0
                OR SUM(DebitAmount) <> 0
                OR SUM(CreditAmountOC) <> 0
                OR SUM(CreditAmount) <> 0
                OR SUM(OpenningDebitAmount - OpenningCreditAmount) <> 0 
                OR SUM(OpenningDebitAmountOC - OpenningCreditAmountOC) <> 0 
				OR ( 
					(SUM(AccumDebitAmountOC) <> 0
					OR SUM(AccumDebitAmount) <> 0
					OR SUM(AccumCreditAmountOC) <> 0
					OR SUM(AccumCreditAmount) <> 0) )
        ORDER BY RSNS.AccountObjectCode
        OPTION (RECOMPILE)
		END
		ELSE
		BEGIN
        SELECT  ROW_NUMBER() OVER ( ORDER BY AccountObjectCode ) AS RowNum ,
                AccountingObjectID ,
                AccountObjectCode ,
                AccountObjectName ,
                AccountObjectAddress ,
                AccountObjectTaxCode ,
                AccountNumber ,
                AccountCategoryKind ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmountOC)
                            - SUM(OpenningCreditAmountOC)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningDebitAmountOC
                                            - OpenningCreditAmountOC) ) > 0
                                 THEN ( SUM(OpenningDebitAmountOC
                                            - OpenningCreditAmountOC) )
                                 ELSE $0
                            END
                  END ) AS OpeningDebitAmountOC ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN ( SUM(OpenningDebitAmount - OpenningCreditAmount) )
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningDebitAmount
                                            - OpenningCreditAmount) ) > 0
                                 THEN SUM(OpenningDebitAmount
                                          - OpenningCreditAmount)
                                 ELSE $0
                            END
                  END ) AS OpeningDebitAmount ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN ( SUM(OpenningCreditAmountOC
                                  - OpenningDebitAmountOC) )
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC) ) > 0
                                 THEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC) )
                                 ELSE $0
                            END
                  END ) AS OpeningCreditAmountOC ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN ( SUM(OpenningCreditAmount - OpenningDebitAmount) )
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount) ) > 0
                                 THEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount) )
                                 ELSE $0
                            END
                  END ) AS OpeningCreditAmount ,
                SUM(DebitAmountOC) AS DebitAmountOC ,
                SUM(DebitAmount) AS DebitAmount ,
                SUM(CreditAmountOC) AS CreditAmountOC ,
                SUM(CreditAmount) AS CreditAmount ,
                SUM(AccumDebitAmountOC) AS AccumDebitAmountOC ,
                SUM(AccumDebitAmount) AS AccumDebitAmount ,
                SUM(AccumCreditAmountOC) AS AccumCreditAmountOC ,
                SUM(AccumCreditAmount) AS AccumCreditAmount ,
                /* Số dư cuối kỳ = Dư Có đầu kỳ - Dư Nợ đầu kỳ + Phát sinh Có – Phát sinh Nợ
				Nếu Số dư cuối kỳ >0 thì hiển bên cột Dư Có cuối kỳ 
				Nếu số dư cuối kỳ <0 thì hiển thị bên cột Dư Nợ cuối kỳ */
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC
                                + DebitAmountOC - CreditAmountOC)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN SUM(OpenningCreditAmountOC
                                          - OpenningDebitAmountOC
                                          + CreditAmountOC - DebitAmountOC) > 0
                                 THEN $0
                                 ELSE SUM(OpenningDebitAmountOC
                                          - OpenningCreditAmountOC
                                          + DebitAmountOC - CreditAmountOC)
                            END
                  END ) AS CloseDebitAmountOC ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN SUM(OpenningCreditAmountOC - OpenningDebitAmountOC
                                + CreditAmountOC - DebitAmountOC)
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC
                                            + CreditAmountOC - DebitAmountOC) ) > 0
                                 THEN SUM(OpenningCreditAmountOC
                                          - OpenningDebitAmountOC
                                          + CreditAmountOC - DebitAmountOC)
                                 ELSE $0
                            END
                  END ) AS CloseCreditAmountOC ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmount - OpenningCreditAmount
                                + DebitAmount - CreditAmount)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN SUM(OpenningCreditAmount
                                          - OpenningDebitAmount + CreditAmount
                                          - DebitAmount) > 0 THEN $0
                                 ELSE SUM(OpenningDebitAmount
                                          - OpenningCreditAmount + DebitAmount
                                          - CreditAmount)
                            END
                  END ) AS ClosingDebitAmount ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN SUM(OpenningCreditAmount - OpenningDebitAmount
                                + CreditAmount - DebitAmount)
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount
                                            + CreditAmount - DebitAmount) ) > 0
                                 THEN SUM(OpenningCreditAmount
                                          - OpenningDebitAmount + CreditAmount
                                          - DebitAmount)
                                 ELSE $0
                            END
                  END ) AS ClosingCreditAmount ,
                AccountObjectGroupListCode ,
                AccountObjectGroupListName
        FROM    ( SELECT    AOL.AccountingObjectID ,
                            LAOI.AccountObjectCode ,
                            LAOI.AccountObjectName ,
                            LAOI.AccountObjectAddress,
                            LAOI.AccountObjectTaxCode ,
                            TBAN.AccountNumber ,
                            TBAN.AccountCategoryKind ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.DebitAmountOriginal
                                 ELSE $0
                            END AS OpenningDebitAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.DebitAmountOriginal
                                 ELSE $0
                            END AS OpenningDebitAmount ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.CreditAmountOriginal
                                 ELSE $0
                            END AS OpenningCreditAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.CreditAmountOriginal
                                 ELSE $0
                            END AS OpenningCreditAmount ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.DebitAmountOriginal
                            END AS DebitAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.DebitAmountOriginal
                            END AS DebitAmount ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.CreditAmountOriginal
                            END AS CreditAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.CreditAmountOriginal
                            END AS CreditAmount ,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.DebitAmountOriginal ELSE 0 END AS AccumDebitAmountOC,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.DebitAmount ELSE 0 END AS AccumDebitAmount,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.CreditAmountOriginal ELSE 0 END AS AccumCreditAmountOC,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.CreditAmount ELSE 0 END AS AccumCreditAmount,
                            LAOI.AccountObjectGroupListCode ,
                            LAOI.AccountObjectGroupListName
                  FROM      @tbDataGL AS AOL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                            INNER JOIN @tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumberPercent
                            INNER JOIN dbo.Account AS AN ON AOL.Account = AN.AccountNumber
                            INNER JOIN @tblListAccountObjectID AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID
                  WHERE     AOL.PostedDate <= @ToDate
                            AND ( @CurrencyID IS NULL
                                  OR AOL.CurrencyID = @CurrencyID
                                )
                ) AS RSNS
        GROUP BY RSNS.AccountingObjectID ,
                RSNS.AccountObjectCode ,
                RSNS.AccountObjectName ,
                RSNS.AccountObjectAddress ,
                RSNS.AccountObjectTaxCode ,
                RSNS.AccountNumber ,
                RSNS.AccountCategoryKind ,
                RSNS.AccountObjectGroupListCode ,
                RSNS.AccountObjectGroupListName 
        HAVING 
				SUM(DebitAmountOC) <> 0
                OR SUM(DebitAmount) <> 0
                OR SUM(CreditAmountOC) <> 0
                OR SUM(CreditAmount) <> 0
                OR SUM(OpenningDebitAmount - OpenningCreditAmount) <> 0 
                OR SUM(OpenningDebitAmountOC - OpenningCreditAmountOC) <> 0 
				OR ( 
					(SUM(AccumDebitAmountOC) <> 0
					OR SUM(AccumDebitAmount) <> 0
					OR SUM(AccumCreditAmountOC) <> 0
					OR SUM(AccumCreditAmount) <> 0) )
        ORDER BY RSNS.AccountObjectCode
        OPTION (RECOMPILE)
		END
		
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

/*
*/
ALTER PROCEDURE [dbo].[Proc_PO_PayDetailNCC]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountNumber NVARCHAR(20) ,
    @AccountObjectID AS NVARCHAR(MAX) ,
    @CurrencyID NVARCHAR(3)
AS 
    BEGIN     
        DECLARE @tblListAccountObjectID TABLE 
            (
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(25) ,
              AccountObjectName NVARCHAR(255) ,
              AccountObjectAddress NVARCHAR(255) ,
              AccountObjectTaxCode NVARCHAR(200) ,
              AccountObjectGroupListCode NVARCHAR(MAX) 
                                                       NULL ,
              AccountObjectGroupListName NVARCHAR(MAX) 
                                                       NULL 
            ) 
        INSERT  INTO @tblListAccountObjectID
                SELECT  AO.ID as AccountObjectID ,
                        AO.AccountingObjectCode ,
                        AO.AccountingObjectName ,
                        AO.Address ,
                        AO.TaxCode ,
                        null AccountingObjectGroupCode ,
                        null AccountingObjectGroupName 
                FROM    dbo.Func_ConvertStringIntoTable(@AccountObjectID, ',') tblAccountObjectSelected
                        INNER JOIN dbo.AccountingObject AO ON AO.ID = tblAccountObjectSelected.Value		
		
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/
		      
        CREATE TABLE #tblResult
            (
              RowNum INT IDENTITY(1, 1)
                         PRIMARY KEY ,
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(100)  ,
              AccountObjectName NVARCHAR(255)  ,
              AccountObjectGroupListCode NVARCHAR(MAX) 
                                                       NULL ,
              AccountObjectGroupListName NVARCHAR(MAX) 
                                                       NULL ,             
              AccountObjectAddress NVARCHAR(255)  ,
              PostedDate DATETIME ,
              RefDate DATETIME , 
              RefNo NVARCHAR(25)  ,
              DueDate DATETIME , 
              InvDate DATETIME ,
              InvNo NVARCHAR(25)  ,

              RefType INT ,
              RefID UNIQUEIDENTIFIER ,
              JournalMemo NVARCHAR(255)  , 
              AccountNumber NVARCHAR(20)  , 
              AccountCategoryKind INT ,
              CorrespondingAccountNumber NVARCHAR(20)  , 
              ExchangeRate DECIMAL(18, 4) , 
              DebitAmountOC MONEY ,	
              DebitAmount MONEY , 
              CreditAmountOC MONEY , 
              CreditAmount MONEY , 
              ClosingDebitAmountOC MONEY ,
              ClosingDebitAmount MONEY , 
              ClosingCreditAmountOC MONEY ,	
              ClosingCreditAmount MONEY ,
              InventoryItemCode NVARCHAR(50)  ,
              InventoryItemName NVARCHAR(255)  ,
              UnitName NVARCHAR(20)  ,
              Quantity DECIMAL(22, 8) ,
              UnitPrice DECIMAL(18, 4) , 
              BranchName NVARCHAR(128)  ,
              IsBold BIT ,	
              OrderType INT ,
              GLJournalMemo NVARCHAR(255)  ,
              DocumentIncluded NVARCHAR(255)  ,
              CustomField1 NVARCHAR(255)  ,
              CustomField2 NVARCHAR(255)  ,
              CustomField3 NVARCHAR(255)  ,
              CustomField4 NVARCHAR(255)  ,
              CustomField5 NVARCHAR(255)  ,
              CustomField6 NVARCHAR(255)  ,
              CustomField7 NVARCHAR(255)  ,
              CustomField8 NVARCHAR(255)  ,
              CustomField9 NVARCHAR(255)  ,
              CustomField10 NVARCHAR(255) ,
               MainUnitName NVARCHAR(20)  ,
              MainQuantity DECIMAL(22, 8) ,
              MainUnitPrice DECIMAL(18, 4),
			  OrderPriority int  
            )
        
        DECLARE @tblAccountNumber TABLE
            (
              AccountNumber NVARCHAR(20) PRIMARY KEY ,
              AccountName NVARCHAR(255) ,
              AccountNumberPercent NVARCHAR(25) ,
              AccountCategoryKind INT
            )
			
        IF @AccountNumber IS NOT NULL 
            BEGIN
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = @AccountNumber
                        ORDER BY A.AccountNumber ,
                                A.AccountName 
                
            END
        ELSE 
            BEGIN 
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = '331'
                        ORDER BY A.AccountNumber ,
                                A.AccountName
            END
            
                
/*Lấy số dư đầu kỳ và dữ liệu phát sinh trong kỳ:*/ 
IF(@CurrencyID='VND')
BEGIN
INSERT  INTO #tblResult
                        ( AccountObjectID ,
                          AccountObjectCode , 
                          AccountObjectName ,
                          AccountObjectGroupListCode , 
                          AccountObjectGroupListName ,      
                          AccountObjectAddress , 
                          PostedDate ,
                          RefDate ,
                          RefNo ,
                          InvDate ,
                          InvNo ,
                          RefType ,
                          RefID , 
                          JournalMemo , 				  
                          AccountNumber , 
                          AccountCategoryKind ,
                          CorrespondingAccountNumber ,	  			  
                          DebitAmountOC ,	
                          DebitAmount , 
                          CreditAmountOC ,
                          CreditAmount ,
                          ClosingDebitAmountOC , 
                          ClosingDebitAmount ,
                          ClosingCreditAmountOC ,
                          ClosingCreditAmount,
						  OrderType,
						  /*OrderPriority - comment by cuongpv*/ 
						  OrderPriority
                        )
                        SELECT  AccountingObjectID ,
                                AccountObjectCode ,
                                AccountObjectName ,	
                                AccountObjectGroupListCode , 
                                AccountObjectGroupListName ,            
                                AccountObjectAddress , 
                                RSNS.PostedDate ,
                                NgayCtu ,
                                SoCtu ,
                                InvDate ,
                                InvNo ,
                                RefType ,
                                RSNS.RefID , 
                                JournalMemo ,			  
                                AccountNumber , 
                                AccountCategoryKind , 
                                CorrespondingAccountNumber , 		  			  
                                SUM(DebitAmountOC) ,
                                SUM(DebitAmount) , 
                                SUM(CreditAmountOC) , 
                                SUM(CreditAmount) , 
                                ( CASE WHEN AccountCategoryKind = 0 THEN SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC)
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC) ) > 0 THEN ( SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmountOC ,      
                                ( CASE WHEN AccountCategoryKind = 0 THEN ( SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount) )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount) ) > 0 THEN SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount)
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmount , 
                                ( CASE WHEN AccountCategoryKind = 1 THEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) ) > 0 THEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmountOC ,
                                ( CASE WHEN AccountCategoryKind = 1 THEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) ) > 0 THEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmount,  
								  OrderType,
								  /*OrderPriority - comment by cuongpv*/
								  OrderPriority
                        FROM    ( SELECT    AOL.AccountingObjectID ,
                                            LAOI.AccountObjectCode ,   
                                            LAOI.AccountObjectName ,
                                            AccountObjectGroupListCode ,
                                            AccountObjectGroupListName ,         
                                            LAOI.AccountObjectAddress ,
											CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.PostedDate
                                            END AS PostedDate ,
											CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.Date
                                            END AS NgayCtu ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.No
                                            END AS SoCtu ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.InvoiceDate
                                            END AS InvDate ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.InvoiceNo
                                            END AS InvNo ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.ReferenceID
                                            END AS RefID ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.TypeID
                                            END AS RefType , 
                                            CASE WHEN AOL.PostedDate < @FromDate 
												      THEN NULL
                                                 ELSE CAST(AOL.DetailID AS NVARCHAR(50))
                                            END AS RefDetailID ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN N'Số dư đầu kỳ'
                                                 ELSE AOL.Description
                                            END AS JournalMemo , 
                                            TBAN.AccountNumber ,
                                            TBAN.AccountCategoryKind ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.AccountCorresponding
                                            END AS CorrespondingAccountNumber , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.DebitAmountOriginal
                                            END AS DebitAmountOC ,                             
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.DebitAmount
                                            END AS DebitAmount ,                                  
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.CreditAmountOriginal
                                            END AS CreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.CreditAmount
                                            END AS CreditAmount , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.DebitAmountOriginal
                                                 ELSE $0
                                            END AS ClosingDebitAmountOC , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.DebitAmount
                                                 ELSE $0
                                            END AS ClosingDebitAmount ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.CreditAmountOriginal
                                                 ELSE $0
                                            END AS ClosingCreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.CreditAmount
                                                 ELSE $0
                                            END AS ClosingCreditAmount,
											CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN 0
                                                 ELSE 1
                                            END AS OrderType,
											/*OrderPriority - comment by cuongpv*/
											OrderPriority
                                  FROM      @tbDataGL AS AOL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                            INNER JOIN @tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumberPercent
                                            INNER JOIN dbo.Account AS AN ON AOL.Account = AN.AccountNumber
                                            INNER JOIN @tblListAccountObjectID AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID                                         
                                  WHERE     AOL.PostedDate <= @ToDate
                                            
                                ) AS RSNS                               
                        GROUP BY RSNS.AccountingObjectID ,
                                RSNS.AccountObjectCode ,  
                                RSNS.AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,
                                RSNS.AccountObjectAddress ,
                                RSNS.PostedDate ,
                                RSNS.NgayCtu , 
                                RSNS.SoCtu , 
                                RSNS.InvDate ,
                                RSNS.InvNo ,
                                RSNS.RefType ,
                                RSNS.RefID ,
                                RSNS.JournalMemo , 		  
                                RSNS.AccountNumber ,
                                RSNS.AccountCategoryKind ,
                                RSNS.CorrespondingAccountNumber,
								OrderType,
								/*OrderPriority - comment by cuongpv*/	
								OrderPriority
                        HAVING  SUM(DebitAmountOC) <> 0
                                OR SUM(DebitAmount) <> 0
                                OR SUM(CreditAmountOC) <> 0
                                OR SUM(CreditAmount) <> 0
                                OR SUM(ClosingDebitAmountOC - ClosingCreditAmountOC) <> 0
                                OR SUM(ClosingDebitAmount - ClosingCreditAmount) <> 0
                        ORDER BY 
						        RSNS.AccountObjectCode ,
                                RSNS.AccountNumber ,
                                RSNS.PostedDate ,
                                RSNS.NgayCtu ,
                                RSNS.SoCtu,
								OrderPriority
								/*OrderPriority - comment by cuongpv*/

                OPTION  ( RECOMPILE ) 
END
ELSE
BEGIN
INSERT  INTO #tblResult
                        ( AccountObjectID ,
                          AccountObjectCode , 
                          AccountObjectName ,
                          AccountObjectGroupListCode , 
                          AccountObjectGroupListName ,      
                          AccountObjectAddress , 
                          PostedDate ,
                          RefDate ,
                          RefNo ,
                          InvDate ,
                          InvNo ,
                          RefType ,
                          RefID , 
                          JournalMemo , 				  
                          AccountNumber , 
                          AccountCategoryKind ,
                          CorrespondingAccountNumber ,	  			  
                          DebitAmountOC ,	
                          DebitAmount , 
                          CreditAmountOC ,
                          CreditAmount ,
                          ClosingDebitAmountOC , 
                          ClosingDebitAmount ,
                          ClosingCreditAmountOC ,
                          ClosingCreditAmount,
						  OrderType,
						  /*OrderPriority - comment by cuongpv*/ 
						  OrderPriority
                        )
                        SELECT  AccountingObjectID ,
                                AccountObjectCode ,
                                AccountObjectName ,	
                                AccountObjectGroupListCode , 
                                AccountObjectGroupListName ,            
                                AccountObjectAddress , 
                                RSNS.PostedDate ,
                                NgayCtu ,
                                SoCtu ,
                                InvDate ,
                                InvNo ,
                                RefType ,
                                RSNS.RefID , 
                                JournalMemo ,			  
                                AccountNumber , 
                                AccountCategoryKind , 
                                CorrespondingAccountNumber , 		  			  
                                SUM(DebitAmountOC) ,
                                SUM(DebitAmount) , 
                                SUM(CreditAmountOC) , 
                                SUM(CreditAmount) , 
                                ( CASE WHEN AccountCategoryKind = 0 THEN SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC)
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC) ) > 0 THEN ( SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmountOC ,      
                                ( CASE WHEN AccountCategoryKind = 0 THEN ( SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount) )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount) ) > 0 THEN SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount)
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmount , 
                                ( CASE WHEN AccountCategoryKind = 1 THEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) ) > 0 THEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmountOC ,
                                ( CASE WHEN AccountCategoryKind = 1 THEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) ) > 0 THEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmount,  
								  OrderType,
								  /*OrderPriority - comment by cuongpv*/
								  OrderPriority
                        FROM    ( SELECT    AOL.AccountingObjectID ,
                                            LAOI.AccountObjectCode ,   
                                            LAOI.AccountObjectName ,
                                            AccountObjectGroupListCode ,
                                            AccountObjectGroupListName ,         
                                            LAOI.AccountObjectAddress ,
											CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.PostedDate
                                            END AS PostedDate ,
											CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.Date
                                            END AS NgayCtu ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.No
                                            END AS SoCtu ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.InvoiceDate
                                            END AS InvDate ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.InvoiceNo
                                            END AS InvNo ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.ReferenceID
                                            END AS RefID ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.TypeID
                                            END AS RefType , 
                                            CASE WHEN AOL.PostedDate < @FromDate 
												      THEN NULL
                                                 ELSE CAST(AOL.DetailID AS NVARCHAR(50))
                                            END AS RefDetailID ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN N'Số dư đầu kỳ'
                                                 ELSE AOL.Description
                                            END AS JournalMemo , 
                                            TBAN.AccountNumber ,
                                            TBAN.AccountCategoryKind ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.AccountCorresponding
                                            END AS CorrespondingAccountNumber , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.DebitAmountOriginal
                                            END AS DebitAmountOC ,                             
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.DebitAmount
                                            END AS DebitAmount ,                                  
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.CreditAmountOriginal
                                            END AS CreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.CreditAmount
                                            END AS CreditAmount , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.DebitAmountOriginal
                                                 ELSE $0
                                            END AS ClosingDebitAmountOC , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.DebitAmount
                                                 ELSE $0
                                            END AS ClosingDebitAmount ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.CreditAmountOriginal
                                                 ELSE $0
                                            END AS ClosingCreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.CreditAmount
                                                 ELSE $0
                                            END AS ClosingCreditAmount,
											CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN 0
                                                 ELSE 1
                                            END AS OrderType,
											/*OrderPriority - comment by cuongpv*/
											OrderPriority
                                  FROM      @tbDataGL AS AOL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                            INNER JOIN @tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumberPercent
                                            INNER JOIN dbo.Account AS AN ON AOL.Account = AN.AccountNumber
                                            INNER JOIN @tblListAccountObjectID AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID                                         
                                  WHERE     AOL.PostedDate <= @ToDate
                                            AND ( @CurrencyID IS NULL
                                                  OR AOL.CurrencyID = @CurrencyID
                                                )
											AND (AOL.DebitAmountOriginal <>0 OR AOL.CreditAmountOriginal <>0)
                                ) AS RSNS                               
                        GROUP BY RSNS.AccountingObjectID ,
                                RSNS.AccountObjectCode ,  
                                RSNS.AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,
                                RSNS.AccountObjectAddress ,
                                RSNS.PostedDate ,
                                RSNS.NgayCtu , 
                                RSNS.SoCtu , 
                                RSNS.InvDate ,
                                RSNS.InvNo ,
                                RSNS.RefType ,
                                RSNS.RefID ,
                                RSNS.JournalMemo , 		  
                                RSNS.AccountNumber ,
                                RSNS.AccountCategoryKind ,
                                RSNS.CorrespondingAccountNumber,
								OrderType,
								/*OrderPriority - comment by cuongpv*/	
								OrderPriority
                        HAVING  SUM(DebitAmountOC) <> 0
                                OR SUM(DebitAmount) <> 0
                                OR SUM(CreditAmountOC) <> 0
                                OR SUM(CreditAmount) <> 0
                                OR SUM(ClosingDebitAmountOC - ClosingCreditAmountOC) <> 0
                                OR SUM(ClosingDebitAmount - ClosingCreditAmount) <> 0
                        ORDER BY 
						        RSNS.AccountObjectCode ,
                                RSNS.AccountNumber ,
                                RSNS.PostedDate ,
                                RSNS.NgayCtu ,
                                RSNS.SoCtu,
								OrderPriority
								/*OrderPriority - comment by cuongpv*/

                OPTION  ( RECOMPILE )  
				END
/* Tính số tồn */ 
/* edit by namnh
 @CloseAmountOC,@CloseAmount Decimal(22,8) -> Money)*/
        DECLARE @CloseAmountOC AS Money ,
            @CloseAmount AS Money ,
            @AccountObjectCode_tmp NVARCHAR(100) ,
            @AccountNumber_tmp NVARCHAR(20)
        SELECT  @CloseAmountOC = 0 ,
                @CloseAmount = 0 ,
                @AccountObjectCode_tmp = N'' ,
                @AccountNumber_tmp = N''
        UPDATE  #tblResult
        SET     @CloseAmountOC = ( CASE WHEN OrderType = 0 THEN ( CASE WHEN ClosingDebitAmountOC = 0 THEN ClosingCreditAmountOC
                                                                       ELSE -1 * ClosingDebitAmountOC
                                                                  END )
                                        WHEN @AccountObjectCode_tmp <> AccountObjectCode
                                             OR @AccountNumber_tmp <> AccountNumber THEN CreditAmountOC - DebitAmountOC
                                        ELSE @CloseAmountOC + CreditAmountOC - DebitAmountOC
                                   END ) ,
                ClosingDebitAmountOC = ( CASE WHEN AccountCategoryKind = 0 THEN -1 * @CloseAmountOC
                                              WHEN AccountCategoryKind = 1 THEN $0
                                              ELSE CASE WHEN @CloseAmountOC < 0 THEN -1 * @CloseAmountOC
                                                        ELSE $0
                                                   END
                                         END ) ,
                ClosingCreditAmountOC = ( CASE WHEN AccountCategoryKind = 1 THEN @CloseAmountOC
                                               WHEN AccountCategoryKind = 0 THEN $0
                                               ELSE CASE WHEN @CloseAmountOC > 0 THEN @CloseAmountOC
                                                         ELSE $0
                                                    END
                                          END ) ,
                @CloseAmount = ( CASE WHEN OrderType = 0 THEN ( CASE WHEN ClosingDebitAmount = 0 THEN ClosingCreditAmount
                                                                     ELSE -1 * ClosingDebitAmount
                                                                END )
                                      WHEN @AccountObjectCode_tmp <> AccountObjectCode
                                           OR @AccountNumber_tmp <> AccountNumber THEN CreditAmount - DebitAmount
                                      ELSE @CloseAmount + CreditAmount - DebitAmount
                                 END ) ,
                ClosingDebitAmount = ( CASE WHEN AccountCategoryKind = 0 THEN -1 * @CloseAmount
                                            WHEN AccountCategoryKind = 1 THEN $0
                                            ELSE CASE WHEN @CloseAmount < 0 THEN -1 * @CloseAmount
                                                      ELSE $0
                                                 END
                                       END ) ,
                ClosingCreditAmount = ( CASE WHEN AccountCategoryKind = 1 THEN @CloseAmount
                                             WHEN AccountCategoryKind = 0 THEN $0
                                             ELSE CASE WHEN @CloseAmount > 0 THEN @CloseAmount
                                                       ELSE $0
                                                  END
                                        END ) ,
                @AccountObjectCode_tmp = AccountObjectCode ,
                @AccountNumber_tmp = AccountNumber
        WHERE   OrderType <> 2
/*Lấy số liệu*/				
        SELECT  RS.PostedDate as Ngay_HT, 
		        RS.RefDate as ngayCtu,
				RS.RefNo as SoCtu,
				Rs.JournalMemo as DienGiai,
				/*'331' as TK_CONGNO, comment by cuongpv*/
				Rs.CorrespondingAccountNumber as TkDoiUng,
                RS.AccountObjectCode,
				RS.AccountObjectName,
				RS.AccountNumber as TK_CONGNO, /*edit by cuongpv RS.AccountNumber -> RS.AccountNumber as TK_CONGNO*/
				fn.OpeningDebitAmount as OpeningDebitAmount,
				fn.OpeningCreditAmount as OpeningCreditAmount,
				Rs.DebitAmount as DebitAmount,
				Rs.DebitAmountOC as DebitAmountOC,
				Rs.CreditAmount as CreditAmount,
				Rs.CreditAmountOC as CreditAmountOC,
				Rs.ClosingDebitAmount as ClosingDebitAmount,
				Rs.ClosingDebitAmountOC as ClosingDebitAmountOC,
				Rs.ClosingCreditAmount as ClosingCreditAmount,
				Rs.ClosingCreditAmountOC as ClosingCreditAmountOC
        FROM    #tblResult RS ,
		        [dbo].[Func_SoDu] (
   @FromDate
  ,@ToDate
  ,3) fn
  where rs.AccountNumber = fn.AccountNumber
  and rs.AccountObjectID = fn.AccountObjectID
  
  order by RS.AccountObjectCode ,
                                RS.AccountNumber ,
                                RS.PostedDate ,
                                RS.RefDate ,
                                RS.RefNo,
								OrderPriority
        DROP TABLE #tblResult
    END				
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_GetGLBookDetailMoneyBorrow]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountID NVARCHAR(MAX) ,
    @AccountObjectID NVARCHAR(MAX)
AS 
    BEGIN
	
	/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/

        CREATE  TABLE #Result
            (
              RefID UNIQUEIDENTIFIER ,
              PostedDate DATETIME ,
              RefDate DATETIME ,
              RefNo NVARCHAR(25) ,
              RefType INT ,
              JournalMemo NVARCHAR(255) ,
              AccountNumber NVARCHAR(25) ,
              AccountName NVARCHAR(255) ,
              CorrespondingAccountNumber NVARCHAR(25) ,
              DueDate DATETIME ,
              DebitAmount DECIMAL(22, 4) ,
              CreditAmount DECIMAL(22, 4) ,
              AccountObjectCode NVARCHAR(25) ,
              AccountObjectName NVARCHAR(255) ,
              OrderType INT ,
              IsBold BIT ,
              BranchName NVARCHAR(128) ,
              SortOrder INT ,
              DetailPostOrder INT
            )      

        DECLARE @tblAccountObject TABLE
            (
              AccountObjectID UNIQUEIDENTIFIER PRIMARY KEY,
			  AccountObjectCode NVARCHAR(25)
                 ,
              AccountObjectName NVARCHAR(255)
                
            )                       
        INSERT  INTO @tblAccountObject
                SELECT  f.value,A.AccountingObjectCode,A.AccountingObjectName
                FROM    dbo.Func_ConvertStringIntoTable(@AccountObjectID,
                                                           ',') f
                 INNER JOIN dbo.AccountingObject A ON A.ID = F.Value 	
        CREATE TABLE #tblSelectedAccountNumber
            (
              AccountNumber NVARCHAR(20) 
                                         NOT NULL
                                         PRIMARY KEY ,
              AccountName NVARCHAR(255) 
                                        NULL
            )
        INSERT  INTO #tblSelectedAccountNumber
                SELECT DISTINCT  A.AccountNumber, A.AccountName
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountID, ',') F
                        INNER JOIN dbo.Account A ON A.AccountNumber like F.Value + '%'	/*edit by cuongpv (A.AccountNumber = F.Value) -> A.AccountNumber like F.Value + '%'*/		        
		/*select * from #tblSelectedAccountNumber*/
        BEGIN             
                
            INSERT  #Result
                    SELECT  RefID ,
                            PostedDate ,
                            RefDate ,
                            RefNo ,
                            RefType ,
                            JournalMemo ,
                            AccountNumber ,
                            AccountName ,
                            CorrespondingAccountNumber ,
                            DueDate ,
                            DebitAmount ,
                            CreditAmount ,
                            AccountObjectCode ,
                            AccountObjectName ,
                            OrderType ,
                            IsBold ,
                            BranchName ,
                            SortOrder ,
                            DetailPostOrder
                    FROM    ( SELECT    NULL AS RefID ,
                                        NULL AS PostedDate ,
                                        NULL AS RefDate ,
                                        NULL AS RefNo ,
                                        NULL AS RefType ,
                                        N'Số dư đầu kỳ' AS JournalMemo ,
                                        GL.Account AccountNumber,
                                        AC.AccountName ,
                                        NULL AS CorrespondingAccountNumber ,
                                        NULL DueDate ,
                                        CASE WHEN SUM(GL.DebitAmount
                                                      - GL.CreditAmount) > 0
                                             THEN SUM(GL.DebitAmount
                                                      - GL.CreditAmount)
                                             ELSE 0
                                        END AS DebitAmount ,
                                        CASE WHEN SUM(GL.CreditAmount
                                                      - GL.DebitAmount) > 0
                                             THEN SUM(GL.CreditAmount
                                                      - GL.DebitAmount)
                                             ELSE 0
                                        END AS CreditAmount ,
                                        AO.AccountObjectCode ,
                                        AO.AccountObjectName AS AccountObjectName ,
                                        0 AS OrderType ,
                                        1 AS isBold ,
                                        null  AS BranchName,
                                        0 AS sortOrder ,
                                        0 AS DetailPostOrder ,
                                        0 AS EntryType
                              FROM      @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                        inner JOIN #tblSelectedAccountNumber AC ON GL.Account = AC.AccountNumber /*edit by cuongpv like -> '='*/
                                                              /*+ '%' comment by cuongpv*/
                                        inner JOIN @tblAccountObject AO ON AO.AccountObjectID = GL.AccountingObjectID
                              WHERE     ( GL.PostedDate < @FromDate )
                              GROUP BY  GL.Account ,
                                        AC.AccountName ,
                                        AO.AccountObjectCode ,
                                        AO.AccountObjectName
                              HAVING    SUM(GL.DebitAmount - GL.CreditAmount) <> 0
                              UNION ALL
                              SELECT    GL.ReferenceID ,
                                        GL.PostedDate ,
                                        GL.RefDate ,
                                        GL.RefNo ,
                                        GL.TypeID ,
                                        CASE WHEN ( GL.Description IS NOT NULL
                                                    AND GL.Description <> ''
                                                  ) THEN GL.Description
                                             ELSE GL.Reason
                                        END AS JournalMemo ,
                                        GL.Account AccountNumber,
                                        AC.AccountName ,
                                        GL.AccountCorresponding,
                                        null as DueDate ,
                                        GL.DebitAmount ,
                                        GL.CreditAmount ,
                                        AO.AccountObjectCode ,
                                        AO.AccountObjectName AS AccountObjectName ,
                                        2 AS OrderType ,
                                        0 ,
                                        null AS BranchName,
                                        GL.OrderPriority,
                                        null,
                                        null
                              FROM      @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                        inner JOIN #tblSelectedAccountNumber AC ON GL.Account = AC.AccountNumber /*edit by cuongpv like -> '='*/
                                                              /*+ '%' comment by cuongpv*/
                                        inner JOIN @tblAccountObject AO ON AO.AccountObjectID = GL.AccountingObjectID
                              WHERE     ( GL.PostedDate BETWEEN @FromDate AND @ToDate )
                                        AND ( GL.DebitAmount <> 0
                                              OR GL.CreditAmount <> 0
                                            )
                            ) T
                    ORDER BY AccountObjectName ,
                            AccountObjectCode ,
                            OrderType ,
                            postedDate ,
                            RefDate ,
							SortOrder ,
                            RefNo ,
                            DetailPostOrder ,
                           
                            EntryType                                                          
               
        END         
       
	   /*add by cuongpv insert total PS*/
	   INSERT  #Result
                ( JournalMemo ,
                  AccountNumber ,
                  AccountName ,
                  DebitAmount ,
                  CreditAmount ,
                  AccountObjectCode ,
                  AccountObjectName ,
                  OrderType ,
                  IsBold ,
                  BranchName
		        )                                                
                SELECT  N'Cộng số phát sinh' JournalMemo ,
                        AccountNumber ,
                        AccountName ,
                        SUM(DebitAmount) AS DebitAmount ,
                        SUM(CreditAmount) AS CreditAmount ,
                        AccountObjectCode ,
                        AccountObjectName ,
                        3 AS OrderType ,
                        1 ,
                        null as BranchName
                FROM    #Result
                WHERE   OrderType = 2
                GROUP BY AccountNumber ,
                        AccountName ,
                        AccountObjectCode ,
                        AccountObjectName 
	   /*end add by cuongpv*/     
       
      /*Insert lable*/
        INSERT  #Result
                ( JournalMemo ,
                  AccountNumber ,
                  AccountName ,
                  DebitAmount ,
                  CreditAmount ,
                  AccountObjectCode ,
                  AccountObjectName ,
                  OrderType ,
                  IsBold ,
                  BranchName
		        )                                                
                SELECT  N'Số dư cuối kỳ' JournalMemo ,
                        AccountNumber ,
                        AccountName ,
                        CASE WHEN SUM(DebitAmount - CreditAmount) > 0
                             THEN SUM(DebitAmount - CreditAmount)
                             ELSE 0
                        END AS DebitAmount ,
                        CASE WHEN SUM(CreditAmount - DebitAmount) > 0
                             THEN SUM(CreditAmount - DebitAmount)
                             ELSE 0
                        END AS CreditAmount ,
                        AccountObjectCode ,
                        AccountObjectName ,
                        4 AS OrderType ,
                        1 ,
                        null as BranchName
                FROM    #Result
                WHERE   OrderType = 2
                        OR OrderType = 0
                GROUP BY AccountNumber ,
                        AccountName ,
                        AccountObjectCode ,
                        AccountObjectName
                HAVING  ( SUM(DebitAmount) <> 0
                          OR SUM(CreditAmount) <> 0
                        )                        
	     /*IF EXISTS ( SELECT  * FROM    #Result ) comment by cuongpv*/
			SELECT  * ,
                ROW_NUMBER() OVER ( ORDER BY AccountNumber,AccountObjectName,AccountObjectCode,  OrderType , PostedDate , RefNo , RefDate, SortOrder, DetailPostOrder ) AS RowNum
			FROM    #Result  
        
        /*ELSE 
           BEGIN
                INSERT  #Result
                        ( JournalMemo ,
                          OrderType ,
                          IsBold ,
                          BranchName
		                )                        
                        SELECT  N'' JournalMemo ,
                                2 ,
                                1 ,
                                ''                                                                     
                SELECT  * ,
                        ROW_NUMBER() OVER ( ORDER BY AccountObjectName, AccountObjectCode , OrderType , PostedDate , RefDate, RefNo, SortOrder, DetailPostOrder ) AS RowNum
                FROM    #Result         
            END  comment by cuongpv*/                      
        DELETE  #Result
        DELETE  #tblSelectedAccountNumber        
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_SA_SoTheoDoiThanhToanBangNgoaiTe]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @TypeMoney NVARCHAR(3),
    @Account NVARCHAR(25),
    @AccountObjectID AS NVARCHAR(MAX)
AS
BEGIN          
	DECLARE @sqlexc nvarchar(max)
	set @sqlexc = N''
	/*Add by cuongpv de sua cach lam tron*/
	select @sqlexc = @sqlexc + N' DECLARE @tbDataGL TABLE('
	select @sqlexc = @sqlexc + N' 	ID uniqueidentifier,'
	select @sqlexc = @sqlexc + N' 	BranchID uniqueidentifier,'
	select @sqlexc = @sqlexc + N' 	ReferenceID uniqueidentifier,'
	select @sqlexc = @sqlexc + N' 	TypeID int,'
	select @sqlexc = @sqlexc + N' 	Date datetime,'
	select @sqlexc = @sqlexc + N' 	PostedDate datetime,'
	select @sqlexc = @sqlexc + N' 	No nvarchar(25),'
	select @sqlexc = @sqlexc + N' 	InvoiceDate datetime,'
	select @sqlexc = @sqlexc + N' 	InvoiceNo nvarchar(25),'
	select @sqlexc = @sqlexc + N' 	Account nvarchar(25),'
	select @sqlexc = @sqlexc + N' 	AccountCorresponding nvarchar(25),'
	select @sqlexc = @sqlexc + N' 	BankAccountDetailID uniqueidentifier,'
	select @sqlexc = @sqlexc + N' 	CurrencyID nvarchar(3),'
	select @sqlexc = @sqlexc + N' 	ExchangeRate decimal(25, 10),'
	select @sqlexc = @sqlexc + N' 	DebitAmount decimal(25,0),'
	select @sqlexc = @sqlexc + N' 	DebitAmountOriginal decimal(25,2),'
	select @sqlexc = @sqlexc + N' 	CreditAmount decimal(25,0),'
	select @sqlexc = @sqlexc + N' 	CreditAmountOriginal decimal(25,2),'
	select @sqlexc = @sqlexc + N' 	Reason nvarchar(512),'
	select @sqlexc = @sqlexc + N' 	Description nvarchar(512),'
	select @sqlexc = @sqlexc + N' 	VATDescription nvarchar(512),'
	select @sqlexc = @sqlexc + N' 	AccountingObjectID uniqueidentifier,'
	select @sqlexc = @sqlexc + N' 	EmployeeID uniqueidentifier,'
	select @sqlexc = @sqlexc + N' 	BudgetItemID uniqueidentifier,'
	select @sqlexc = @sqlexc + N' 	CostSetID uniqueidentifier,'
	select @sqlexc = @sqlexc + N' 	ContractID uniqueidentifier,'
	select @sqlexc = @sqlexc + N' 	StatisticsCodeID uniqueidentifier,'
	select @sqlexc = @sqlexc + N' 	InvoiceSeries nvarchar(25),'
	select @sqlexc = @sqlexc + N' 	ContactName nvarchar(512),'
	select @sqlexc = @sqlexc + N' 	DetailID uniqueidentifier,'
	select @sqlexc = @sqlexc + N' 	RefNo nvarchar(25),'
	select @sqlexc = @sqlexc + N' 	RefDate datetime,'
	select @sqlexc = @sqlexc + N' 	DepartmentID uniqueidentifier,'
	select @sqlexc = @sqlexc + N' 	ExpenseItemID uniqueidentifier,'
	select @sqlexc = @sqlexc + N' 	OrderPriority int,'
	select @sqlexc = @sqlexc + N' 	IsIrrationalCost bit'
	select @sqlexc = @sqlexc + N' )'

	select @sqlexc = @sqlexc + N' INSERT INTO @tbDataGL'
	select @sqlexc = @sqlexc + N' SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= '''+CONVERT(nvarchar(25),@ToDate,101)+''''
		/*end add by cuongpv*/
	set @sqlexc = @sqlexc + N' DECLARE @tbAccountObjectID TABLE('
	set @sqlexc = @sqlexc + N'	AccountingObjectID UNIQUEIDENTIFIER'
	set @sqlexc = @sqlexc + N')'
	
	set @sqlexc = @sqlexc + N' INSERT INTO @tbAccountObjectID'
	set @sqlexc = @sqlexc + N' SELECT tblAccOjectSelect.Value as AccountingObjectID'
	set @sqlexc = @sqlexc + N'	FROM dbo.Func_ConvertStringIntoTable('''+@AccountObjectID+''','','') tblAccOjectSelect'
	set @sqlexc = @sqlexc + N'	WHERE tblAccOjectSelect.Value in (SELECT ID FROM AccountingObject)'
	
	select @sqlexc = @sqlexc + N' DECLARE @tbDataDauKy TABLE('
	select @sqlexc = @sqlexc + N' AccountingObjectID UNIQUEIDENTIFIER,'
	select @sqlexc = @sqlexc + N' Account nvarchar(25),'
	select @sqlexc = @sqlexc + N' SoDuDauKySoTien money,'
	select @sqlexc = @sqlexc + N' SoDuDauKyQuyDoi money'
	select @sqlexc = @sqlexc + N')'
	
	select @sqlexc = @sqlexc + N' INSERT INTO @tbDataDauKy(AccountingObjectID, Account, SoDuDauKySoTien, SoDuDauKyQuyDoi)'
	select @sqlexc = @sqlexc + N' SELECT GL.AccountingObjectID, GL.Account, SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal) as SoDuDauKySoTien,'
	select @sqlexc = @sqlexc + N' SUM(GL.DebitAmount - GL.CreditAmount) as SoDuDauKyQuyDoi'
	select @sqlexc = @sqlexc + N' FROM @tbDataGL GL'
	select @sqlexc = @sqlexc + N' WHERE (GL.AccountingObjectID in (Select AccountingObjectID from @tbAccountObjectID))'
	select @sqlexc = @sqlexc + N' AND GL.PostedDate < '''+CONVERT(nvarchar(25),@FromDate,101)+''' AND (GL.CurrencyID = '''+@TypeMoney+''')'
	
	if(@Account='all')
	begin
		set @sqlexc = @sqlexc + N' AND ((GL.Account like ''131%'') OR (GL.Account like ''141%'') OR (GL.Account like ''331%''))'
	end
	else
	begin
		set @sqlexc = @sqlexc + N' AND (GL.Account like '''+@Account+'%'')'
	end
	
	select @sqlexc = @sqlexc + N' GROUP BY GL.AccountingObjectID, GL.Account'
	
	
	set @sqlexc = @sqlexc + N' DECLARE @tbDataReturn TABLE('
	set @sqlexc = @sqlexc + N'	stt bigint,'
	set @sqlexc = @sqlexc + N'	IdGroup smallint,'
	set @sqlexc = @sqlexc + N'	AccountingObjectID UNIQUEIDENTIFIER,'
	set @sqlexc = @sqlexc + N'	AccountingObjectName nvarchar(512),'
	set @sqlexc = @sqlexc + N'	Account nvarchar(25),'
	set @sqlexc = @sqlexc + N'	NgayHoachToan Date,'
	set @sqlexc = @sqlexc + N'	NgayChungTu Date,'
	set @sqlexc = @sqlexc + N'	SoChungTu nvarchar(25),'
	set @sqlexc = @sqlexc + N'	DienGiai nvarchar(512),'
	set @sqlexc = @sqlexc + N'	TKDoiUng nvarchar(512),'
	set @sqlexc = @sqlexc + N'	TyGiaHoiDoai decimal(25,10),'
	set @sqlexc = @sqlexc + N'	PSNSoTien money,'
	set @sqlexc = @sqlexc + N'	PSNQuyDoi money,'
	set @sqlexc = @sqlexc + N'	PSCSoTien money,'
	set @sqlexc = @sqlexc + N'	PSCQuyDoi money,'
	set @sqlexc = @sqlexc + N'	DuNoSoTien money,'
	set @sqlexc = @sqlexc + N'	DuNoQuyDoi money,'
	set @sqlexc = @sqlexc + N'	DuCoSoTien money,'
	set @sqlexc = @sqlexc + N'	DuCoQuyDoi money,'
	set @sqlexc = @sqlexc + N'	TonSoTien money,'
	set @sqlexc = @sqlexc + N'	TonQuyDoi money,'
	set @sqlexc = @sqlexc + N'	OrderPriority int'
	set @sqlexc = @sqlexc + N')'
	
	set @sqlexc = @sqlexc + N' DECLARE @IdGroup1 smallint'
	set @sqlexc = @sqlexc + N' set @IdGroup1 = 5;'
	
	set @sqlexc = @sqlexc + N' INSERT INTO @tbDataReturn(stt,IdGroup, AccountingObjectID, Account, NgayHoachToan, NgayChungTu, SoChungTu, DienGiai, TKDoiUng, TyGiaHoiDoai,'
	set @sqlexc = @sqlexc + N' PSNSoTien, PSNQuyDoi, PSCSoTien, PSCQuyDoi,OrderPriority)'
	set @sqlexc = @sqlexc + N' SELECT ROW_NUMBER() OVER(ORDER BY GL.AccountingObjectID, GL.Account, GL.PostedDate) as stt, @IdGroup1 as IdGroup, GL.AccountingObjectID, Gl.Account, GL.PostedDate as NgayHoachToan, GL.Date as NgayChungTu, GL.No as SoChungTu,'
	set @sqlexc = @sqlexc + N' GL.Description as DienGiai, GL.AccountCorresponding as TKDoiUng, GL.ExchangeRate as TyGiaHoiDoai, GL.DebitAmountOriginal as PSNSoTien,'
	set @sqlexc = @sqlexc + N' GL.DebitAmount as PSNQuyDoi, Gl.CreditAmountOriginal as PSCSoTien, GL.CreditAmount as PSCQuyDoi,GL.OrderPriority as OrderPriority'
	set @sqlexc = @sqlexc + N' FROM @tbDataGL GL'
	set @sqlexc = @sqlexc + N' WHERE (GL.PostedDate Between '''+CONVERT(nvarchar(25),@FromDate,101)+''' And '''+CONVERT(nvarchar(25),@ToDate,101)+''') AND (GL.CurrencyID = '''+@TypeMoney+''')'
	set @sqlexc = @sqlexc + N' AND (GL.AccountingObjectID in (Select AccountingObjectID from @tbAccountObjectID))'
	
	if(@Account='all')
	begin
		set @sqlexc = @sqlexc + N' AND ((GL.Account like ''131%'') OR (GL.Account like ''141%'') OR (GL.Account like ''331%''))'
	end
	else
	begin
		set @sqlexc = @sqlexc + N' AND (GL.Account like '''+@Account+'%'')'
	end
	
	set @sqlexc = @sqlexc + N' DECLARE @stt1 smallint'
	set @sqlexc = @sqlexc + N' set @stt1 = 0;'
	
	
	set @sqlexc = @sqlexc + N' Declare @SoDuDauKySoTien money'
	set @sqlexc = @sqlexc + N' Set @SoDuDauKySoTien = 0'
	set @sqlexc = @sqlexc + N' Declare @SoDuDauKyQuyDoi money'
	set @sqlexc = @sqlexc + N' Set @SoDuDauKyQuyDoi = 0'
	
	set @sqlexc = @sqlexc + N' INSERT INTO @tbDataReturn(stt,IdGroup, AccountingObjectID, Account, DienGiai, TonSoTien, TonQuyDoi)'
	set @sqlexc = @sqlexc + N' SELECT @stt1, @IdGroup1, AccountingObjectID, Account, N''Số dư đầu kỳ'''
	set @sqlexc = @sqlexc + N' ,@SoDuDauKySoTien, @SoDuDauKyQuyDoi'
	set @sqlexc = @sqlexc + N' FROM @tbDataReturn'
	set @sqlexc = @sqlexc + N' GROUP BY AccountingObjectID, Account'
	
	set @sqlexc = @sqlexc + N' DECLARE @iIDDauKy UNIQUEIDENTIFIER'
	set @sqlexc = @sqlexc + N' DECLARE @iAccountDauKy NVARCHAR(25)'
	set @sqlexc = @sqlexc + N' DECLARE @iTonSoTienDauKy money'
	set @sqlexc = @sqlexc + N' DECLARE @iTonQuyDoiDauKy money'
	
	set @sqlexc = @sqlexc + N' DECLARE cursorDauKy CURSOR FOR'
	set @sqlexc = @sqlexc + N' SELECT AccountingObjectID, Account, SoDuDauKySoTien, SoDuDauKyQuyDoi FROM @tbDataDauKy'
	set @sqlexc = @sqlexc + N' OPEN cursorDauKy'
	set @sqlexc = @sqlexc + N' FETCH NEXT FROM cursorDauKy INTO @iIDDauKy, @iAccountDauKy, @iTonSoTienDauKy, @iTonQuyDoiDauKy'
	set @sqlexc = @sqlexc + N' WHILE @@FETCH_STATUS = 0'
	set @sqlexc = @sqlexc + N' BEGIN'
		set @sqlexc = @sqlexc + N' Declare @countCheck int'
		set @sqlexc = @sqlexc + N' set @countCheck = (select count(AccountingObjectID) from @tbDataReturn where AccountingObjectID = @iIDDauKy and Account = @iAccountDauKy)'
		
		set @sqlexc = @sqlexc + N' If(@countCheck > 0)'
		set @sqlexc = @sqlexc + N' Begin'
			set @sqlexc = @sqlexc + N' Update @tbDataReturn Set TonSoTien = @iTonSoTienDauKy, TonQuyDoi = @iTonQuyDoiDauKy' 
			set @sqlexc = @sqlexc + N' where AccountingObjectID = @iIDDauKy and Account = @iAccountDauKy'
		set @sqlexc = @sqlexc + N' End'
		set @sqlexc = @sqlexc + N' Else'
		set @sqlexc = @sqlexc + N' Begin'
			set @sqlexc = @sqlexc + N' Insert Into @tbDataReturn(stt,IdGroup, AccountingObjectID, Account, DienGiai, TonSoTien, TonQuyDoi)'
			set @sqlexc = @sqlexc + N' Values(@stt1, @IdGroup1, @iIDDauKy, @iAccountDauKy, N''Số dư đầu kỳ'', @iTonSoTienDauKy, @iTonQuyDoiDauKy)'
		set @sqlexc = @sqlexc + N' End'
		set @sqlexc = @sqlexc + N' FETCH NEXT FROM cursorDauKy INTO @iIDDauKy, @iAccountDauKy, @iTonSoTienDauKy, @iTonQuyDoiDauKy'
	set @sqlexc = @sqlexc + N' END'
	set @sqlexc = @sqlexc + N' CLOSE cursorDauKy'
	set @sqlexc = @sqlexc + N' DEALLOCATE cursorDauKy'
	
	
	set @sqlexc = @sqlexc + N' DECLARE @iID UNIQUEIDENTIFIER'
	set @sqlexc = @sqlexc + N' DECLARE @iAccount NVARCHAR(25)'
	set @sqlexc = @sqlexc + N' DECLARE @iTonSoTien money'
	set @sqlexc = @sqlexc + N' DECLARE @iTonQuyDoi money'
	
	set @sqlexc = @sqlexc + N' DECLARE cursorTon CURSOR FOR'
	set @sqlexc = @sqlexc + N' SELECT AccountingObjectID, Account, TonSoTien, TonQuyDoi FROM @tbDataReturn WHERE stt = 0'
	set @sqlexc = @sqlexc + N' OPEN cursorTon'
	set @sqlexc = @sqlexc + N' FETCH NEXT FROM cursorTon INTO @iID, @iAccount, @iTonSoTien, @iTonQuyDoi'
	set @sqlexc = @sqlexc + N' WHILE @@FETCH_STATUS = 0'
	set @sqlexc = @sqlexc + N' BEGIN'
		set @sqlexc = @sqlexc + N' Declare @tonSoTien money'
		set @sqlexc = @sqlexc + N' set @tonSoTien = ISNULL(@iTonSoTien,0)'
		set @sqlexc = @sqlexc + N' Declare @tonQuyDoi money'
		set @sqlexc = @sqlexc + N' set @tonQuyDoi = ISNULL(@iTonQuyDoi,0)'
		
		set @sqlexc = @sqlexc + N' Declare @minStt bigint'
		set @sqlexc = @sqlexc + N' set @minStt = (select MIN(stt) from @tbDataReturn where AccountingObjectID = @iID and Account = @iAccount and stt > 0)'
		set @sqlexc = @sqlexc + N' Declare @maxStt bigint'
		set @sqlexc = @sqlexc + N' set @maxStt = (select MAX(stt) from @tbDataReturn where AccountingObjectID = @iID and Account = @iAccount and stt > 0)'
		set @sqlexc = @sqlexc + N' WHILE @minStt <= @maxStt'
		set @sqlexc = @sqlexc + N' Begin'
			set @sqlexc = @sqlexc + N' set @tonSoTien = @tonSoTien + ISNULL((select PSNSoTien from @tbDataReturn where stt = @minStt),0) - ISNULL((select PSCSoTien from @tbDataReturn where stt = @minStt),0)'
			set @sqlexc = @sqlexc + N' set @tonQuyDoi = @tonQuyDoi + ISNULL((select PSNQuyDoi from @tbDataReturn where stt = @minStt),0) - ISNULL((select PSCQuyDoi from @tbDataReturn where stt = @minStt),0)'
			set @sqlexc = @sqlexc + N' UPDATE @tbDataReturn SET TonSoTien = @tonSoTien, TonQuyDoi = @tonQuyDoi WHERE stt = @minStt'
			set @sqlexc = @sqlexc + N' set @minStt = @minStt + 1'
		set @sqlexc = @sqlexc + N' End'
		set @sqlexc = @sqlexc + N' FETCH NEXT FROM cursorTon INTO @iID, @iAccount, @iTonSoTien, @iTonQuyDoi'
	set @sqlexc = @sqlexc + N' END'
	set @sqlexc = @sqlexc + N' CLOSE cursorTon'
	set @sqlexc = @sqlexc + N' DEALLOCATE cursorTon'
	
	set @sqlexc = @sqlexc + N' DECLARE @IdGroup3 smallint'
	set @sqlexc = @sqlexc + N' SET @IdGroup3 = 3'
	
	set @sqlexc = @sqlexc + N' INSERT INTO @tbDataReturn(IdGroup, AccountingObjectID, Account, PSNSoTien, PSNQuyDoi, PSCSoTien, PSCQuyDoi)'
	set @sqlexc = @sqlexc + N' SELECT @IdGroup3 as IdGroup, AccountingObjectID, Account, SUM(PSNSoTien) as PSNSoTien, SUM(PSNQuyDoi) as PSNQuyDoi'
	set @sqlexc = @sqlexc + N' , SUM(PSCSoTien) as PSCSoTien, SUM(PSCQuyDoi) as PSCQuyDoi'
	set @sqlexc = @sqlexc + N' FROM @tbDataReturn WHERE IdGroup = 5'
	set @sqlexc = @sqlexc + N' GROUP BY AccountingObjectID, Account'
	
	set @sqlexc = @sqlexc + N' DECLARE @iID1 UNIQUEIDENTIFIER'
	set @sqlexc = @sqlexc + N' DECLARE @iAccount1 NVARCHAR(25)'
	
	set @sqlexc = @sqlexc + N' DECLARE cursorTon1 CURSOR FOR'
	set @sqlexc = @sqlexc + N' SELECT AccountingObjectID, Account FROM @tbDataReturn WHERE stt = 0'
	set @sqlexc = @sqlexc + N' OPEN cursorTon1'
	set @sqlexc = @sqlexc + N' FETCH NEXT FROM cursorTon1 INTO @iID1, @iAccount1'
	set @sqlexc = @sqlexc + N' WHILE @@FETCH_STATUS = 0'
	set @sqlexc = @sqlexc + N' BEGIN'
		set @sqlexc = @sqlexc + N' Declare @maxStt1 bigint'
		set @sqlexc = @sqlexc + N' set @maxStt1 = (select MAX(stt) from @tbDataReturn where AccountingObjectID = @iID1 and Account = @iAccount1 and stt > 0)'
		set @sqlexc = @sqlexc + N' If(@maxStt1 > 0)'
		set @sqlexc = @sqlexc + N' Begin'
			set @sqlexc = @sqlexc + N' UPDATE @tbDataReturn SET TonSoTien = (select TonSoTien from @tbDataReturn where stt = @maxStt1)'
			set @sqlexc = @sqlexc + N' ,TonQuyDoi = (select TonQuyDoi from @tbDataReturn where stt = @maxStt1) WHERE AccountingObjectID = @iID1'
			set @sqlexc = @sqlexc + N' AND Account = @iAccount1 AND IdGroup = 3'
		set @sqlexc = @sqlexc + N' End'
		set @sqlexc = @sqlexc + N' Else'
		set @sqlexc = @sqlexc + N' Begin'
			set @sqlexc = @sqlexc + N' UPDATE @tbDataReturn SET TonSoTien = (select TonSoTien from @tbDataReturn where stt = 0 and AccountingObjectID = @iID1 and Account = @iAccount1)'
			set @sqlexc = @sqlexc + N' ,TonQuyDoi = (select TonQuyDoi from @tbDataReturn where stt = 0 and AccountingObjectID = @iID1 and Account = @iAccount1) WHERE AccountingObjectID = @iID1'
			set @sqlexc = @sqlexc + N' AND Account = @iAccount1 AND IdGroup = 3'
		set @sqlexc = @sqlexc + N' End'
		set @sqlexc = @sqlexc + N' FETCH NEXT FROM cursorTon1 INTO @iID1, @iAccount1'
	set @sqlexc = @sqlexc + N' END'
	set @sqlexc = @sqlexc + N' CLOSE cursorTon1'
	set @sqlexc = @sqlexc + N' DEALLOCATE cursorTon1'
	
	set @sqlexc = @sqlexc + N' UPDATE @tbDataReturn SET DuNoSoTien = TonSoTien WHERE TonSoTien > 0'
	set @sqlexc = @sqlexc + N' UPDATE @tbDataReturn SET DuCoSoTien = TonSoTien WHERE TonSoTien < 0'
	
	set @sqlexc = @sqlexc + N' UPDATE @tbDataReturn SET DuNoQuyDoi = TonQuyDoi WHERE TonQuyDoi > 0'
	set @sqlexc = @sqlexc + N' UPDATE @tbDataReturn SET DuCoQuyDoi = TonQuyDoi WHERE TonQuyDoi < 0'
	
	set @sqlexc = @sqlexc + N' UPDATE @tbDataReturn SET DuCoSoTien = (-1 * DuCoSoTien) WHERE DuCoSoTien < 0'
	set @sqlexc = @sqlexc + N' UPDATE @tbDataReturn SET DuCoQuyDoi = (-1 * DuCoQuyDoi) WHERE DuCoQuyDoi < 0'
	
	set @sqlexc = @sqlexc + N' DECLARE @IdGroup2 smallint'
	set @sqlexc = @sqlexc + N' SET @IdGroup2 = 1'
	
	set @sqlexc = @sqlexc + N' INSERT INTO @tbDataReturn(IdGroup, AccountingObjectID, PSNSoTien, PSNQuyDoi, PSCSoTien, PSCQuyDoi'
	set @sqlexc = @sqlexc + N' , DuNoSoTien, DuNoQuyDoi, DuCoSoTien, DuCoQuyDoi)'
	set @sqlexc = @sqlexc + N' SELECT @IdGroup2 as IdGroup, AccountingObjectID, SUM(PSNSoTien) as PSNSoTien, SUM(PSNQuyDoi) as PSNQuyDoi'
	set @sqlexc = @sqlexc + N' , SUM(PSCSoTien) as PSCSoTien, SUM(PSCQuyDoi) as PSCQuyDoi, SUM(DuNoSoTien) as DuNoSoTien'
	set @sqlexc = @sqlexc + N' , SUM(DuNoQuyDoi) as DuNoQuyDoi, SUM(DuCoSoTien) as DuCoSoTien, SUM(DuCoQuyDoi) as DuCoQuyDoi'
	set @sqlexc = @sqlexc + N' FROM @tbDataReturn WHERE IdGroup = 3'
	set @sqlexc = @sqlexc + N' GROUP BY AccountingObjectID'
	
	set @sqlexc = @sqlexc + N' UPDATE @tbDataReturn SET AccountingObjectName = AJ.AccountingObjectName'
	set @sqlexc = @sqlexc + N' FROM ( select ID,AccountingObjectName from AccountingObject) AJ'
	set @sqlexc = @sqlexc + N' where AccountingObjectID = AJ.ID'
	
	set @sqlexc = @sqlexc + N' SELECT stt,IdGroup, AccountingObjectID, AccountingObjectName, Account, NgayHoachToan, NgayChungTu, SoChungTu, DienGiai, TKDoiUng, TyGiaHoiDoai'
	set @sqlexc = @sqlexc + N',PSNSoTien, PSNQuyDoi, PSCSoTien, PSCQuyDoi, DuNoSoTien, DuNoQuyDoi, DuCoSoTien, DuCoQuyDoi'
	set @sqlexc = @sqlexc + N' FROM @tbDataReturn'
	set @sqlexc = @sqlexc + N' WHERE (ISNULL(PSNSoTien,0) > 0) OR (ISNULL(PSNQuyDoi,0) > 0) OR (ISNULL(PSCSoTien,0) > 0) OR (ISNULL(PSCQuyDoi,0) > 0)'
	set @sqlexc = @sqlexc + N' OR (ISNULL(DuNoSoTien,0) > 0) OR (ISNULL(DuNoQuyDoi,0) > 0) OR (ISNULL(DuCoSoTien,0) > 0) OR (ISNULL(DuCoQuyDoi,0) > 0)'
	set @sqlexc = @sqlexc + N' ORDER BY AccountingObjectID, OrderPriority, Account, IdGroup, stt'
	
	
	EXECUTE sp_executesql @sqlexc
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE dbo.Template
  DROP CONSTRAINT FK_Template_Type
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateColumn
  DROP CONSTRAINT FK_TemplateColumn_TemplateDetail
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateDetail
  DROP CONSTRAINT FK_TemplateDetail_Template
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DELETE dbo.AccountGroup WHERE ID = N'001'
DELETE dbo.AccountGroup WHERE ID = N'002'
DELETE dbo.AccountGroup WHERE ID = N'003'
DELETE dbo.AccountGroup WHERE ID = N'004'
DELETE dbo.AccountGroup WHERE ID = N'007'
DELETE dbo.AccountGroup WHERE ID = N'008'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.Currency SET IsActive = 1 WHERE ID = N'EUR'
UPDATE dbo.Currency SET ExchangeRate = 214.7000000000, IsActive = 1 WHERE ID = N'JPY'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DELETE dbo.FixedAssetCategory WHERE ID = 'd271d0e8-669e-4420-b2e2-6566c24220cd'
DELETE dbo.FixedAssetCategory WHERE ID = '7b7b0145-0af4-40f5-903c-b74fdef6b429'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

INSERT dbo.PersonalSalaryTax(ID, PersonalSalaryTaxName, PersonalSalaryTaxGrade, SalaryType, TaxRate, FromAmount, ToAmount, Formula) VALUES ('931c60c6-4908-4efe-ba3f-4bf6cb354fe4', N'Thu nhập từ ESport', 1, 0, 14.2500000000, 10000000.00, 20000000.00, NULL)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.SystemOption SET Name = N'Tên đơn vị chủ quản' WHERE ID = 5
UPDATE dbo.SystemOption SET Name = N'Mã số thuế đơn vị chủ quản' WHERE ID = 6
UPDATE dbo.SystemOption SET Name = N'Địa chỉ trụ sở đại lý thuế' WHERE ID = 10
UPDATE dbo.SystemOption SET Name = N'Quận/huyện đại lý thuế' WHERE ID = 11
UPDATE dbo.SystemOption SET Name = N'Tỉnh/thành phố đại lý thuế' WHERE ID = 12
UPDATE dbo.SystemOption SET Name = N'Điện thoại đại lý thuê' WHERE ID = 13
UPDATE dbo.SystemOption SET Name = N'Fax đại lý thuế' WHERE ID = 14
UPDATE dbo.SystemOption SET Name = N'Ngày HĐ đại lý thuế' WHERE ID = 16
UPDATE dbo.SystemOption SET Code = N'TCKHAC_PPTinhGiaXQuy' WHERE ID = 89

INSERT dbo.SystemOption(ID, Code, Name, Type, Data, DefaultData, Note, IsSecurity) VALUES (134, N'IsMinimized', N'Thu nhỏ giao diện nhập chứng từ', 6, N'0', N'0', N'0: not check 1:check', 0)
INSERT dbo.SystemOption(ID, Code, Name, Type, Data, DefaultData, Note, IsSecurity) VALUES (135, N'LimitAccount', N'Hạn chế tài khoản khi nhập liệu', 4, N'1', N'1', N'0: not check 1:check', 0)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

SET IDENTITY_INSERT dbo.Template ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
INSERT dbo.Template(ID, TemplateName, TypeID, IsDefault, IsActive, IsSecurity, OrderPriority) VALUES ('93d6cc08-c956-40ab-9fe2-5c565e3b6cbd', N'Mẫu chuẩn', 670, 0, 1, 1, 1)
GO
SET IDENTITY_INSERT dbo.Template OFF
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DELETE dbo.TemplateColumn WHERE ID = 'b2706c9b-3aab-4fe0-b198-e72ab8c59816'

UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'd3efff6e-644d-41f7-9f50-0012c7a87ac5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '86115ade-d36b-4acb-b0d3-00673b4299f3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '82ff2370-177f-4a9b-be6b-00785c42db72'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '635dbfde-b82a-44b3-b006-007aa4eb1b1a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'f7b0d284-91d5-4b08-acad-00a23404fce7'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'fc27a63f-a860-4923-93d8-00c50b90981d'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '1d89fa26-dd43-4d38-b86b-00cf6a8b31bd'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'e94c6dc8-ea64-41f6-8568-00d8d0be964a'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'a1e3ec9b-e239-4ad9-891c-00fa3079f1e7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '7d1b3a48-960c-447d-ae31-01075db0f36d'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'ef71f1f3-c8d9-4508-9dcb-013412b8013d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '97bc116e-416d-4ff6-aa22-01740b6168a1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '39ff8a3a-5366-495c-b33a-01ed7d9bc278'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '3b197a52-a2fc-4ab4-912c-01ee3a630b37'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'fb27b153-92d8-4a15-9473-01f9169ebfad'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '8dd6f02f-000e-4921-8105-024c2ce91f7c'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '37322cbb-3a41-4faf-9d80-026239afa6a8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'ddb3a10d-9fce-471d-865b-026ca405c158'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '773c361d-409d-48e3-8b7c-02a5f29a6486'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'b68e16f1-b7c6-46ce-8189-02c4d6065bca'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '260aa673-d9b3-4ebb-a385-035c02a82dcc'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'ede0f765-63e0-4aa6-a6b5-03f9cf1519da'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '3e824812-60f9-4098-8f12-044539eebf9e'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'cf054750-da71-4d90-a774-046ae4b3e082'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '4d88ac2b-b327-4be1-9278-04995baeb722'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '4608cc1f-0f91-4c17-9af4-049cf5fb3c02'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'd64e69f1-74ff-457a-85cf-04d987b2c652'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '8ab0a28d-dab6-407c-ae58-04f7928e5f25'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '9a6e4c3f-9529-43c1-8aa7-051e29962d52'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c4e1c156-5d18-47ec-a054-0573c6b4f776'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '7b07e8b7-d22c-49d9-91f2-05faf626346f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '462eb72f-f3cd-4121-8e6e-06aa2527e93d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '994685b2-4659-4e55-a4dd-06c83ca94901'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '0eaa12dc-77f5-4655-9074-079ec49c422a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '99fce5b7-a985-4ebe-b43c-07b071fec64a'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '7de835dd-7fa4-45a3-88a5-07b94c3faa30'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'e50c4ec9-35ce-4c61-8786-07e8ded0949a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '5ce6d315-0245-4f76-ac72-0836b99e88df'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '1fc87c66-d82e-475a-9053-083d52f2e8a6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'd988df2b-506d-48ef-9275-08507d33205b'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'bb199a48-914f-4418-bedf-089359a26570'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '3f585bcf-a3e9-4ba3-85db-08b066655388'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'c316d687-25d8-4f92-97c7-0958ed16a0a4'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '899ac36a-50e6-4af6-9bdd-097cc6155605'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'aaaf0b73-d95a-47a8-b60e-0980bb00e867'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'f51eb1e7-0e5e-4464-a370-09c45483108d'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '7759f004-1039-4ab6-b305-09f0db235033'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'd74545b6-35f7-44e5-ad48-0a038b7c335d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 18 WHERE ID = '44c66108-82b9-40f4-b7b1-0a1e1fa24e03'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'bdc916a4-0597-4ece-b33a-0a26bbcdf7fc'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'fbd4ab50-146c-4873-900b-0a7c775d8bba'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '6fef2b9b-25f3-4ba3-8293-0ae544278a0c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '00ce1327-8f05-4ea2-9af5-0af29c10dcba'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'f0f36e63-8bde-4f2c-9438-0b16fb23e240'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'b202e925-7f15-4cce-8c17-0bbfa0989b77'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '46fabcb9-43c8-44d3-a49c-0bd2bda4f136'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'd699801f-11d6-4165-82f3-0c2bec37c277'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'd759bc12-7641-4ef2-90d7-0d5119f9b9d2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'ce423932-65b2-4580-844f-0d71311c21fe'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'ac3a58cb-3a24-49f1-a548-0e2de80387ed'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '34c2bbc6-c714-442f-bddd-0e49a2b104bb'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '595cfc38-a05c-4f72-b6e2-101719a29b17'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'cdf26ba4-28cd-48f1-ad0f-10a98775cfe1'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '79fb73e6-af58-4593-ba87-113c5b0b13b6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '048bfb06-4e2c-41a1-b374-11a1e2e0acbd'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '698b218e-f69b-4e86-80b0-11e150a310c3'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '49b0da64-d665-4236-8b7d-11e7b5406a68'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '61f879dd-f328-460a-967b-129c9416ad30'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'ed15d61c-508e-43f3-860f-12f431f31f38'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'a292e7d4-6e75-4efa-86c2-13b26d77d015'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '1665d2dd-6026-4e67-99ba-13eb307f9456'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '301b98be-43db-4f5d-8ceb-14280139bd18'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '4add69ae-3aa4-4b63-acac-14459d74bf19'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'a25e1c82-fd59-4f9f-964a-14619a42d8d6'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'd8b78a55-21be-4862-9932-14a815226713'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'd214ecc5-aa9e-4640-b93c-14d0cb985df4'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '4da8fb5b-36a6-442b-8130-14df271d5e61'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'a171fc6b-b4db-402e-9a8f-152b0a7a5e0f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'ff86ce12-0a75-4062-aedd-15558ad2d03b'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '15245504-5239-4500-bed6-159ed8cd2b91'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '661aeafc-7439-4580-b1a1-15e1d502c53f'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '4f3dd4e3-4382-4a4d-b8c2-15fd85ee8193'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '7e7eee2d-d3a4-42d2-af73-1688d1c6c067'
UPDATE dbo.TemplateColumn SET VisiblePosition = 6 WHERE ID = '3d0b41f9-f9e0-4fb9-9e4c-17f7c9ad5fd7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '3387ccf0-118c-4875-8d22-1801f175d5f0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '1c9a5ff5-b373-444f-b8bb-1930e87579ce'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '5060b34f-7e49-4192-a36a-19715f77f346'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '009ebcdd-54ca-4aee-8f95-19bea4d06f20'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '25662a36-1ee2-460f-8dc7-19dcbb781772'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '8f64da40-eb98-4592-88be-1a9f60372551'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '0a4e56d1-2ae3-450f-96a3-1aa7aeb2975c'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '41be0902-33a0-4d74-a4d0-1aad0bdf8724'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '1fd65820-dde3-4637-bb13-1af3afd5f917'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '6ec7e516-5b89-4f45-8294-1c1921b34e4f'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'ffe39249-3f3f-4f5a-b821-1c2a64d70fa2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '81de24e0-31bb-4927-8b33-1c78fdb3a3f4'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '8cebe70b-192d-4ed1-a292-1c924137f344'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '7c32bd56-6912-4fd3-8242-1cd1c3fd0ee9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '418319ef-0c07-482f-b85f-1d671a9c695f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c9260051-bcbf-433e-b20b-1d7c1ceca934'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '9bfdc27f-86d7-43c9-a29c-1dbb405aa4a9'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '7db5dc31-ea15-4551-826c-1dc7298709a0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c62ef9fb-eda2-4366-933f-1e3c33e7b5c1'
UPDATE dbo.TemplateColumn SET VisiblePosition = 21 WHERE ID = '707b6177-6e6b-45e3-a28b-1ea3d8d0cc8b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '1f422e2c-10a5-419c-8a5e-1eaf5632de82'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '87f4174a-ca61-4bca-a987-1ef30223e56c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'fd920046-b2b0-4aea-ad78-1f0b0d703d87'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '832709e4-fa3b-4189-90fd-1f1eb25b41f5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'a1c28bd6-590a-44aa-887f-1f9c03d1a8ed'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'e35b6958-fa68-4575-890b-1fc20ea81d98'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '8e23107c-d371-44ee-903e-1ffd8b36e6ed'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '9b2dc173-c370-4794-81a9-2001446421ba'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '7affdac0-7901-41cf-ae7e-20bfdb617834'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'dcd57163-1d93-43fb-a884-21177157e050'
UPDATE dbo.TemplateColumn SET VisiblePosition = 31 WHERE ID = '94a136f2-3522-4c33-a0d7-21d11d52b713'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'aa5f55b6-37ed-491d-ac63-223f6af7915c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '875afadb-2ea7-4bce-9f50-22e55d18f68e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '2a10f0fb-6952-44d4-85bc-22e84590f2aa'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '4e3e6d00-7729-4ef5-962a-22e90ab74a87'
UPDATE dbo.TemplateColumn SET ColumnName = N'TotalAll' WHERE ID = '1177e518-bcda-4cb6-86fc-22fbe4ddbbb4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '1390a47d-5ed4-4b7a-b14e-238ff5d289c7'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'f2a3e55a-c0b6-460b-8494-239d84e19fb0'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'ee8ad9ec-aa0c-4e2d-8fc1-23b4df2fca79'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '7b638084-1764-47e9-b0a2-24485c1a9839'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '0afad2dc-c771-40ed-8619-2453c4a56934'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'bc2d7563-b866-42cd-813c-24731c1c5aee'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'fbb6c531-8cb5-4af5-8530-24b2112d0980'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'e73aa1b1-d1fc-4cea-95ce-24fa670050a4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '041511a9-1bbd-4366-beab-25162083a530'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'd013fd2e-cda3-4bc0-90f4-2538dea955d5'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'd50ad3de-083b-44f3-82c5-25d4310f775b'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '2dd486af-e2cc-4a9a-aafa-260780468d36'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'e39b143c-0782-4659-8622-26d66ee8781d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'efebdc60-151f-4f4f-a9ea-2740f53fdde9'
UPDATE dbo.TemplateColumn SET VisiblePosition = 7 WHERE ID = '4b0408b4-1824-485e-a607-27673da3968d'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '36367dc6-097e-47f7-8f5d-277bb77023eb'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'f8ef73f4-ae3d-4334-9447-278371dbd1e3'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '3660aa3a-12df-44a1-9785-27a311a2aa92'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'ca3b70e1-ba52-4527-ad74-27d0c551a472'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '298486c9-7af9-4ca0-b918-27fbb609ed01'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'd98b5693-5c1f-4b4a-91f5-29150f1e998f'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'b9115b82-60b5-4ccf-9868-291f94468e8c'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'e00eb894-b721-48a1-818a-29a0d8d79499'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '9d2c63a3-074a-41d4-aa13-29a575241dea'
UPDATE dbo.TemplateColumn SET ColumnName = N'InvoiceTemplate' WHERE ID = 'ba5d3fd1-fff6-48c5-a736-2a57a64de96c'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'cacc66d4-067d-4194-a8df-2aebb4612bc8'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '431d0500-5296-446d-8044-2af2309e4e4c'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '5eef5e72-bf69-48cb-ba7e-2b1585f1fd79'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'bde08212-12e8-4a61-b440-2b2d933400c3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0, ColumnWidth = 130 WHERE ID = 'e2efa898-9b89-45c2-b66e-2c97a82e8b76'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '1aa35ecb-6891-4cf8-b816-2d41e0f28f43'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'd78b2c50-af24-477d-93bd-2dc001fedb86'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'd3bb4d6f-b86a-4b77-9c23-2e90f1383b8f'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '17c3b3d3-dfbb-47f9-8fd3-2ed46c830f03'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '4fef71c0-26d8-4849-a553-2f3ebce6e75f'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '9c9366d7-afcf-44b3-981f-2f75ce5aaf56'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0, VisiblePosition = 14 WHERE ID = 'efd973ff-af6e-4091-a0e9-2f90f6d2ba6a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'f8ee8184-e712-4a12-874f-308cf939cdbd'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '470b6b3a-1c24-4e52-bb8e-3127538c64d8'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '668c3d53-2a8f-4d3f-aec6-32d47cb5e43f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '89b2c229-29ad-4ba5-b89c-32fb2e87e37b'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '3df5c34e-734e-42e7-857b-342bc9a44eb5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'fe3c060b-42a5-4f23-9b50-34501a728c67'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '5b2fa02d-8b77-42ca-ae88-345fcc96bf90'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'a68fd22d-ed35-45ac-aef8-34aaf544f77a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'e486cbbe-2a9d-4fc1-919c-3500068a85c0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'e51b9200-abea-40e2-87cc-356373fe8be9'
UPDATE dbo.TemplateColumn SET VisiblePosition = 20 WHERE ID = 'ab9f64c1-fb84-4e0f-b69c-35baa3ac05f7'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '6d15f371-4b2e-4b20-814f-368cea00d559'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '74e8d606-d278-4ace-bdf8-36b6c42060ff'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '81a1caca-e350-47c4-89fc-37478d564c64'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'dca545eb-9be3-4c34-9270-374c1cc19d18'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'a62813f5-ae01-4600-b750-3775b09139c4'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '5d0f56ec-71f7-409d-86db-37f3fa8dc290'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'eb4cc12f-41cf-49a5-b4e6-38bcdb50e748'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'd5e550d4-47cc-4241-bae7-39c5ef0491e4'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'b0f9e916-ecd6-424e-a713-39ed4e050738'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'bf6e3e4e-e5f5-4d1e-a0bc-3a27d1c4ed63'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'b35ba625-1eb4-42d9-ae6e-3a42390f3cbe'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'b9529e1f-98a4-426c-86c6-3b0100c5854d'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'd11f70a4-4331-45db-8c9c-3b257fd84731'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '6f2031a5-8a6b-40a3-844f-3b2658870975'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1, VisiblePosition = 4 WHERE ID = '55bb7f65-76e7-4230-bed7-3babd7de0f2e'
UPDATE dbo.TemplateColumn SET VisiblePosition = 13 WHERE ID = 'f77a07fd-304d-4053-82a6-3bf52fb0dc2c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '5270eb5f-a0fd-4c06-8482-3c3624ad5a5e'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'e1880f33-a22c-4331-a07b-3c36adc919b2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '23cd075b-4fcf-4cf7-af4d-3c4e79d4d142'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '75c0f46f-2e5c-4216-a9b9-3c8434864069'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '386c9143-eb29-497e-bdaf-3cbabc7156ea'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '95ad7862-ee26-4dd5-b1dc-3d8248733b09'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'c77b10d8-6b51-4155-b3c7-3db16393de5c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '24ec5d83-a83d-4681-8178-3db2a95d1307'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '34a6dc21-9260-4f5f-92ef-3ddf0e684136'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c7756bea-d797-4802-a597-3df2fe4cdfef'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'eb20d50c-6502-4496-9968-3e4389275c0b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'e66de085-9983-450e-99ae-3ed11f2f6cf6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '32947309-64fa-4353-8a25-3f0497ca20d2'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'c41bfbd1-9a96-4f91-b9df-3f629ada1827'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '4c6c8258-347d-4a5b-b12d-3fb4e6055faf'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'd258f003-258f-4ba8-a4d0-3fb532626c08'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'de51917e-9008-49f1-8f98-3fd4caef51dd'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1, VisiblePosition = 5 WHERE ID = '66f9a3d3-639b-40a5-bf42-4074051b3ae9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '19ca6f48-53af-4a63-8890-40d7b4545e63'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '5fb019ce-44f9-47ff-a31c-41918cb49d47'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '00808091-a4eb-40bb-984d-41cd8fbd8e61'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'a240cc78-e1cb-435a-b4d5-41e878f3f19b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'dd5cfcd2-f8a7-4490-9d0e-420a3b670f24'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'd0ddc723-1f54-4b7a-ac99-43a7fe8a15c1'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '73942eaf-84d9-4fbe-8260-43d721bb63bb'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '628a03da-f421-45fd-aca6-4417964150c5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '96f731eb-cb71-4f2f-80b1-442ebff3cf88'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'a425be1b-da79-4be8-8017-4597729396d1'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '26e3f856-76ac-46fd-bad0-45c0137a7a18'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '40dbaab1-c168-4a5d-90a8-467cc1deb4e0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '3b04f9f9-4182-4525-ac49-46dc3ff0f1f2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '28a4662b-8e02-4517-a6c1-46f1a64cfdb2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '675833bd-d17a-4877-8caa-474b3108e5c3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '57cd893d-271d-48ca-93b5-481ebf30ff12'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '7beeef2d-797e-4824-8fe0-48328ae0762c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c0fa3d05-5f1a-4699-ac3c-486bd8c5f7b9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '13254c9d-ebee-47b2-96a5-496bc0639924'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'def14eaf-4775-4e50-8cbc-49a22ca943f2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'd5272514-88ca-47e8-a917-4a05de2530b0'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '7a322353-0886-481b-921b-4a333a522c0c'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '343c1ba9-a041-4e00-8e54-4a3f23376153'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '48aeabde-4ed1-4fbe-81bb-4a5c38619c6b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '196440ce-db26-4ade-8a06-4acfc98484e6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '2c189cd1-1b04-4bef-af66-4b62c71efcce'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'ed6bc978-3294-4b6c-8013-4ba2ef5fed4c'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'c67922a5-f3a3-445f-b7a6-4bbb1db8df2f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'a7677ba7-08e2-4c6f-b62e-4bc7fe22da41'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'a9f02183-c49b-42e7-9e59-4bf45b77a4ae'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '69cef27d-c231-40ee-964f-4bf53e4d41c8'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'fb2a3195-0c24-45ec-8b6a-4d2a423d80d0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'de8edb2e-2950-49e4-bef9-4d8315bcf545'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '001fa941-29ee-45a9-9421-4d8aab191490'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '0e59eca7-d42d-409b-9644-4dca7d68e4dc'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '69389c55-3f59-41ce-a723-4e1f4d5a7bff'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'c13bf850-e8e4-4eda-a7f8-4e3078484e2e'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'c960aec1-7f62-4e9c-8046-4e33812a0181'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '91ae2d6f-85ce-43d5-ac03-4e9be1e299c5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '640b4eaf-e6ac-4b3c-b65a-4ec06f84eb95'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '06551659-0729-4bc7-b065-4ed24b28fea8'
UPDATE dbo.TemplateColumn SET ColumnWidth = 120 WHERE ID = '248c4d45-814a-4c06-910c-4ee25d865e47'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '928e8674-235a-4334-a968-4fd870ec0237'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c2d8bf73-be70-4ee8-9272-503e1a2c85eb'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '6c24db71-8eec-425f-b6c3-505c9e23078e'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'c14e5ca5-f2c8-4085-9f43-507bb27868ee'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '8aab8cf0-5568-4ed3-ae9c-507c724a309a'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '394a04ba-3072-448f-95b8-508a66521ff1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '94c9b7bb-27d1-4b2d-b73d-511958a0f55b'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '6cbf97ae-bcd1-480f-a150-516bbce755b6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'a90235a0-c349-4700-9026-51af978228b9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '55fd077c-a6a6-4469-9da3-51d488e1ad92'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '8927fff4-11f7-441c-9d53-522e2773cb5a'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'ea087ec2-f918-4c31-8bac-531e51aacd70'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '98605c92-3ce9-4da8-92d7-53c22b6ae88c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'b2d35c66-40ff-42a5-bc93-5497a88d2fe4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '8a270a37-5820-4193-90c1-54faa00d40b2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'f1b77d00-08e4-415d-a76e-555b2840acca'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '0aae431d-f22c-46df-8da5-55626fa5b12d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 19 WHERE ID = '6f8c3382-48c8-4492-a65d-556ac91ab368'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '1744b927-39ac-440a-8990-559817b83b40'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'fd65ec8a-b9fe-44fb-be67-55e62631f94b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '11fd1f7f-6f4f-4500-975f-565e1cd17404'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '025d253f-5ecd-43ee-a121-5661da7fdf48'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'be57618d-ad64-4634-b2e6-58109142064b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c14dcfea-4657-4837-b7c6-583976464298'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'c88549d2-a59a-4289-a202-58a73fe22ca6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'fbf87239-018a-45e6-86bb-58ea7b2ec0c0'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '2ba3efde-a875-4ad8-866d-5909fa3f68d8'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'b812688d-7f9f-4a05-b8a4-59777176681e'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '63915f58-d93c-440f-a9bb-59816ae5d127'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '30c10968-4898-47b0-8e33-59a0719eb8e3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '211f5970-66fe-4a64-90cc-59f551b2c52f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'e3988ebb-15e5-4726-9138-5a36890b7500'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '0bb36641-6197-4359-9737-5a38cf727cf6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'a5baaa8c-bcfe-412c-a688-5b59d6272d2e'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'ffaa7795-3b76-44d7-964f-5b7ba0e349bd'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'bed17c5f-e802-4f53-87fa-5b999ad5b3e9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'a29b2713-44d4-496e-86b0-5c14143105a4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '78404ac6-ce77-4aea-9a10-5c801fca61f3'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'e8432b3d-731a-4ed8-aed3-5c933508f884'
UPDATE dbo.TemplateColumn SET VisiblePosition = 16 WHERE ID = '36f1cbe9-b21b-4fb2-b19f-5cf04d7334b8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '67150b87-fee4-479a-a5db-5cfbc3c3f8cf'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '8d146ad0-3b68-467e-aa57-5d0920f27d0d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '67051ba1-f181-4d2f-a6c9-5d7216f60943'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '36289f9e-2193-4d36-971b-5dd2d0554efe'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '9decbd49-c314-4a0a-9579-5df64b10b7ae'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '012dac4c-124e-47de-aa7f-5df6bd0a1885'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '9972a1fe-3ba5-47af-b0ab-5ebe4693b1ac'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '035c8c67-0c1a-4ce4-9d64-5ebfb830d85e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'bd0d1e1f-4b50-4af4-9853-5ed00bc9faea'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1, VisiblePosition = 4 WHERE ID = 'ca40f792-6f5c-4014-9be9-5ef3261830bc'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '0e1a16df-1275-4c68-838f-5f5dea360ff0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c2ec3b20-47d3-4e9e-ae6a-5f7c0692bd7e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '6483880c-b09a-4afb-a732-5f9568437c9a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'dff718d2-ed96-4dbc-b5e9-5f9b4ff63c38'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '6ce40e57-18d9-4e8e-8109-60569ff6c01c'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'd2a7f08e-e739-47d6-a076-6094fd4c4de7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '506aef32-9119-4fb5-9045-610fe81ac111'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '1a24f8bb-daf8-4905-ae8a-6125f9e341f6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '7004818e-ab3f-4317-ae6b-6129aa117261'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'c0295430-93ec-4bdd-b7f0-613d08a90a33'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '3ba37115-9d5e-41dc-93aa-617660700e6f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '1cff1890-dab3-4e2c-bfd0-617c45250bae'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'ac0d90d5-8aea-4fe3-9a97-61bdc69fd405'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3a8279d7-3e54-4ce0-a2ea-62170adfdfe8'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '84f40b94-9606-4c41-8305-628b12f0c580'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '8b2330a2-9032-48fd-9188-62aafe4514b8'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'c2b700e7-887f-469e-a525-62b2013c85cb'
UPDATE dbo.TemplateColumn SET ColumnName = N'TaxExchangeRate', ColumnCaption = N'Tỷ giá tính thuế', ColumnToolTip = N'', ColumnWidth = 105 WHERE ID = '5fd2045e-7c3e-40ff-beb7-62b2ba75d44f'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'ced56912-9998-4c0b-85d9-63622f8cde68'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'd8303afc-2161-4d94-95a4-6371e3028a35'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '061f5d2c-4366-4857-983a-6456170a697e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '2908a581-ba74-4d6a-8baf-652c4eeaae20'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'cc9e3e8f-520f-4d9d-a59a-65b95f6ea883'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'aa767843-0f82-4d08-9e0c-65dc489ddb07'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '9d75dbeb-e26b-44b7-bc44-65ea56da9563'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '9f7b14c8-00a1-460c-884f-660082240c9f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '6ebd6f88-0abe-4e6a-a619-663355a85d93'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'e3732073-9097-4313-ac31-6656b5aa4e97'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'ccae93c6-fa5a-48d3-902d-6662b86df2e8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'fa858494-8d29-4643-8733-66828bc6a3e8'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'b6e913d7-f2c7-4c68-b1dc-668c1d8288b7'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'af0ed655-2003-49b1-8f13-66caca13b773'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '40b1d79f-4d58-4c2e-a798-66cd7e8393c3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '0a5f6119-54ca-42e6-8bb7-671b58cbe9f7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'd1479c43-fea5-4b8f-8746-6798507ad2a9'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '5b7723a4-3fca-4726-af5e-67b445fa1103'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'cbd6c974-e096-4630-aeec-67e4068d98e3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'f3184cf4-f397-4d58-bab3-67fad72ec779'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'a5f88462-3da0-4c5e-a083-688aac20d357'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '5243079a-bf6e-4cb9-8a92-68c1750679e8'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'e53d1029-b1be-4f07-a51e-6928a59563b9'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'f9542918-2d6b-4033-a0e1-697ce329263a'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '982be41a-2390-49e2-af0c-697e9428f992'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2939d1a6-9d80-4c61-9cb9-6a3ef160cbb1'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'b4e9e4b2-a333-4538-946c-6a49de4fbcd2'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '8f36f001-940c-4f51-b2d5-6c0ebe0cd2b9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '656878cf-c6bb-4207-a085-6c8e6d9a40f9'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '5290e9bc-3fa1-4e18-bafc-6cb9d2faf2a2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'd9d06bf5-7dfd-4410-8cf5-6d0a4d8fb3b4'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'a80a8066-0a02-431e-97f9-6d13d33dd2fa'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'a06376d4-1989-46b9-a583-6d16ac56f51c'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '87096530-ecdf-4b70-903f-6d2644f9aa8a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '33269ed2-338b-4a95-aa3f-6dc78f1237e4'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'ac477c32-8e6a-44aa-869b-6e3ee693811b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'd2931edd-ab08-4334-9d80-6e3fc96c34c6'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '05969c5d-4510-4941-b3ba-6e530286e0e4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c2523e1c-e97f-4419-942e-6e7276d0b361'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'b5e4faf6-c78b-4558-800e-6e92454ae7b3'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'b72884b9-5075-4a7e-a6d4-6ea6fe78397f'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'c55ffe43-22f7-4d62-8a65-6ed4edf390dd'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'e98e0cc0-0c93-4c7a-903e-6ef6762a3dad'
UPDATE dbo.TemplateColumn SET ColumnToolTip = N'Mã nhân viên', IsColumnHeader = 1 WHERE ID = '7b9f582c-c2bd-4e30-b5fb-6f040881a690'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '6026aabc-326d-4b8c-abe7-6f3fb17dc132'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'b96f1767-d3c0-4f89-ac0e-6fcb82ca63c7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0, VisiblePosition = 2 WHERE ID = '2686132b-ce61-4931-b42c-6fdf8f5beb17'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '67cd0db0-737f-4f5b-bb82-703550e2163d'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '349fbcdf-8caa-4a54-8219-70d1dccb0f2d'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '4453a534-4db2-4983-b63a-70ebc927f75a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'b2bb91d5-b054-434b-a65d-70f3d53289f9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'ba8f4caf-1f89-48df-a2b6-70fbd62f8d71'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '7bdb5f14-0133-4069-84f6-711051773492'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '4128bed0-9de7-4295-9020-7118c9111437'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'e5e9ebb8-7a4f-4461-b962-7178ca5563db'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '9f92dbed-242d-4890-a8b2-7181abc7cb8d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '1ce823d1-0977-4b45-93d0-7195f7a7b7c0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'de5588f0-ba73-4a09-bc01-71b0edd646f5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '5b6f6f97-2897-4be1-b4cc-71c01efcf91d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '865eb40d-842c-42bd-96b0-71c5579ae47c'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '5e37dfaa-956d-4e03-b9f4-720fefe1eb7c'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'b0797ccb-b814-435c-9111-721fdba647b6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'ffef8851-a569-45ad-961e-72649c5a83e1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '3c095c80-08b9-4c15-aba9-726707f2afa2'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '5cd6f055-e09c-4529-8c1a-739127fd1663'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '57fc9ab3-947c-413b-81ca-747e8f83ce56'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '0274a352-d46f-4dc3-9c40-749a984b13d7'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'b340f5be-2f7f-4186-8c6d-74b2e7acce83'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '410ddbf2-f032-400a-a7d6-75b7eb5a89d3'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'e3205507-6987-4643-bf1f-75ffd240db3d'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '47e9e981-d143-4d66-b721-763a209e4582'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c987bfd6-c268-4e5e-8e63-7717e23b9c13'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1, VisiblePosition = 5 WHERE ID = 'a53c3912-adb3-47a4-97e8-777aa104c337'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'a7ff7902-7b08-4e5e-9b12-778cf10dc7fb'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '13861be0-af5d-4a2c-93f8-77cd4551b3e9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'cfe48fc7-b2b7-4610-9baa-782ab79f3a2d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c0c099a1-0916-4ace-9e1e-782bc8bb95a6'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '38c1bf3b-e155-4463-91f2-782f504378fe'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '6af58a63-6bea-4fde-b098-785182ca946a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '08b7b549-171e-4a83-8226-786b777618a5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '3e71ed6d-920a-4e8e-a566-78a8146e9cb2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '1cb60300-7cfe-430e-8a7a-78e3578b2d6e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '5762fa58-a9e2-4c41-b728-7981f21f0907'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '03912f4d-8bf4-40be-9ab6-799737343f7e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '04d88580-0bea-4ff1-bdd9-7a0f9d11df4f'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'a1317699-54a2-4fd2-bef6-7a2198fbb183'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '4a30adfa-219b-473a-b312-7a766a2c061d'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '6e02cbfc-5b91-477b-b708-7b28d117fc54'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'dad73f50-1670-4c59-841c-7be6d2cf5659'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c135ede5-ebe1-491c-9227-7ca087383bb8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '9358ca03-2901-4531-a5e6-7cb8f8103c5c'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '3c5aee39-ea00-446b-a0d3-7d0926db9fae'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'e4e0126f-609e-4812-830f-7db87a7e6fd0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'd32dd71e-fee3-4fb9-a6b1-7df31e23e300'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '92299a79-f271-417e-a258-7dff099b09ed'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'b6eab0bb-73a1-4cbf-9fb8-7e35b2b86df9'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '861086d5-b778-4c5c-b096-7ee67858e578'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '7e684939-93bb-4d67-ae26-7f7186d8a67f'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '37888755-b4a7-43aa-bec1-7f79b7d30252'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '1c11fbde-cae3-4bbd-8783-7fcd782ad7c3'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'd5e6cdf3-5c64-40e7-b911-7feb3fb7b71f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '3d6b35d2-7b5c-4023-853e-80169c3e9a3c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b28489cd-1687-4176-96e9-801d921387ed'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '886917e5-d355-4024-af01-807449d66ad4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '3b34207e-f2f5-464f-b5e0-813787fc5709'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '07c19fc2-a8c5-4e2e-b338-815a1c5aed6c'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'df945ddf-b90e-448e-a45b-81c64b4ef39c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '1f186de7-a92c-41ff-b0b6-81c9d3336959'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'fbb65322-b2a6-4cd7-bb51-81d1100fea02'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '6c58f657-d0b4-428d-b713-81f2b48df543'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '94e638ba-243d-4cd6-b140-826bde51f513'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'adb4f28c-7271-4373-823c-827d4dd3eb39'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '7dba5100-6719-4ae9-bfaa-8282e2b93f95'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '643676cf-7905-467d-95a1-82f3709e1abd'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'a082fd45-97a7-4553-a6cb-8312b3bfe4ec'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'da168e86-a4af-4f47-924b-831e59d8a724'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '5b218da1-f1c7-4f81-8d3d-8346aa3de0b2'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '094a75cb-b454-43f7-b2b9-83508ff2984c'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '9b2ae42d-3b6d-492e-b169-83f101d7fac5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c542053f-adbb-4902-a623-83f6bea0130f'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'f136ffab-545a-4916-8212-84172bca3939'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '2cfe9dfa-ff4e-4999-b245-849cbe0a456d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'b5dace54-6ddb-42a7-8b8f-8505ef8a2ce6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'db2f886e-32af-40c3-9fe9-8529afe9e720'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'f7b97ff7-6af2-4e88-8035-853df168173c'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '69bb16a5-eead-4929-aaf3-865353473de3'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'a74d55ce-274e-49c3-81fb-86cb373341c8'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '6aa735fb-9a37-462e-b2dc-870451a68dbc'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '118b7bea-2ef5-4607-87a9-87077f9886f8'
UPDATE dbo.TemplateColumn SET VisiblePosition = 5 WHERE ID = 'bee9f26b-0c6b-42cc-ac4e-8786b7535b1f'
UPDATE dbo.TemplateColumn SET VisiblePosition = 11 WHERE ID = 'fbe7f548-8b76-4011-bd53-889a894dbd58'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '1eae3893-d289-48bc-8af2-88aec8ec6bde'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'b2bf19e6-aa79-4e51-9f4f-894c1f36f465'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'db1d5c59-95c3-4f85-ab84-89c9b0888aa5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'fde83317-8e58-402f-aefc-8a592dcb1013'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '40481452-bc29-4b3d-81f9-8a8b005836b7'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '56788be2-f078-4bd3-b9f0-8abbffe616f9'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '54a48104-884d-4e1d-ac3e-8b1e4bbc41e0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '7f8565fe-964e-40cd-85bd-8b2aa9cc4a8a'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'a489280f-5fd9-44e3-9bee-8bfd1b2f00e2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c318a8d0-7e05-4dea-b59b-8c3e81c536ce'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '201f58c2-957a-439b-be33-8c6d1166361f'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '5bfd2d80-2520-450f-9ccb-8ce77724707d'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '3ac18529-e64f-4a57-b915-8d97d12e0b45'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '3834e3ed-e595-49a9-98d4-8daa62508300'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '653a25eb-8b44-4387-b502-8db1a19746d6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '0d8768eb-de07-4e75-b6a9-8e3dedcaaedf'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'f6429fca-ce3a-429f-9ca7-8ee36a538465'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '666cc2a6-9193-41aa-a35c-8f0335f335bf'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'beb3c4d8-94f8-4265-a84d-8f5f847c13e2'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'dfa3adda-eb61-4aed-b746-8f6acc5c3eba'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'fd8ab9b4-87e0-48bb-8c93-8fd1cf722132'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '4b255452-4c08-4dcc-be1b-90bbc4dcb133'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '831917b2-b99e-4454-9a6e-90efac037fc4'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '947c3d36-5ac5-4d04-868c-9100b324938e'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '619b4656-2e4f-42ba-b3bc-9117fb59bf2d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '45d4f640-a810-4a5b-928d-911e6cb42849'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '97e99d16-3803-487c-9c4c-918c5dfe0d7a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '0a680027-f16a-490e-9114-91a7944b0243'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '70c98e89-32ba-4129-ae44-9206bf35860c'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'f618c241-ddb4-4a8c-a335-926229cbacc5'
UPDATE dbo.TemplateColumn SET ColumnWidth = 115 WHERE ID = '04f46e90-bfb0-48a2-981f-9271e1fe1331'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '731b1385-2428-4063-8da6-931c54ddd985'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '4c6fe857-7d18-4ad5-a761-937a2ec2d3c3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '42c6b220-bdc9-4311-961b-938df7d9d71b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'ffbc0bd5-bd34-493a-ac00-93a2fa3b581c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '097b9043-6e33-4f39-8b6a-93f9b4c9a515'
UPDATE dbo.TemplateColumn SET VisiblePosition = 10 WHERE ID = 'eefb1d5f-e4e0-4b0e-a913-93fc4ef32ec6'
UPDATE dbo.TemplateColumn SET VisiblePosition = 6 WHERE ID = 'cebb5544-15d1-4436-885b-94a3e278b023'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '3d7c622c-7af1-437e-a12a-94b9f099821c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '9aaf6b2b-e794-4f87-8992-94cceac42ee0'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '2cb8f5be-776f-4b87-8ec5-9607ce738d0d'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '748814cb-023b-406e-9aef-961edca376b7'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '9d694878-7376-4148-be75-969d68deb40b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 6 WHERE ID = '350cb83b-0011-4e67-9acd-97020313b3ae'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '05e3ec78-73a0-49c2-9b93-9741e051d46a'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '5ec11377-d5ae-4ddb-91db-978d4c4e2267'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'a7f15e10-4b14-4ff9-9be3-97adb3372864'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c6d9de7f-a26d-4ff7-9a3c-97b98324e176'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'a58903ea-3f78-40da-a252-97bb3394087c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '90af8668-9599-4bd3-82ad-97c3910ffe7a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'aeafe9f9-0ba9-4a2b-a757-97ea24e022c6'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'e3c022cf-bf5e-40ad-898d-97fa34060693'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '74dbd992-30f7-4ad9-96ee-980db3dd32ee'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '4bb3353e-194c-4a44-8f50-981b9b0dffd7'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '9ed139e3-372b-4c72-bde4-984e227eeb69'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '3652cdf5-0fc9-49ad-a469-98f63a1e05a1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '8bf02083-7cc2-4d2b-adf5-99443403f5dc'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '749a61fb-581c-41d3-9f5c-9968667e1f7b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'be28d3e7-3a51-4433-81a8-999a86b89b74'
UPDATE dbo.TemplateColumn SET VisiblePosition = 9 WHERE ID = '54d9b139-dba2-4223-9d5e-9a63c0d829c3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c2591485-8188-4aac-8644-9a80c40f4468'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '9515e3af-c6e3-4733-9362-9ad41a9b9917'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '8a02e0cc-58f5-46f3-9435-9b1af0dc9846'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '2990721e-fdc1-474c-8554-9b1b454f1388'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '4c998c71-2a2e-4252-bd03-9b8cb436697e'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '362f5e4e-c005-4e8a-b71e-9baa33e9f340'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'ae7d8d9c-5a25-44f4-b73c-9bcb86510e07'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'a2d08ce5-2a81-4819-acc7-9be2c815ab50'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '95eeac02-d6a2-4025-b5ec-9c3b99ef9f81'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '5b62564c-a0d2-4699-a272-9c4efc312cbd'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'c6dac3c9-2e6a-481e-b0b1-9ca7e53e55eb'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '7aa48d8b-458a-4e63-96b8-9cad47c80c52'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '72155b3f-1c77-4424-bd7a-9ccdca029f92'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '2ae755bb-94ba-4013-ba44-9d19fe067fdf'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'fa84b55e-1fa7-480b-899c-9e44aa3c3eb6'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '77a4aaa4-0bbd-4501-9af4-9e5f5c3a3de5'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '8f31d1d4-f913-4ebf-8e0e-9eeba29a0f04'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '22641053-354d-4b46-bf76-a02f9432ed13'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'e94b45b0-41e5-41c4-af9f-a0516080f95d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '31ea06ea-7216-4e53-ad14-a053d02351a9'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '0d10bfcb-11af-4f4a-a4be-a05b3a98179b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'f89b2548-6ed6-4679-868d-a065a56ec237'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'be7f3f75-cc76-4fed-ae83-a084bbda3b43'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'bf03cf62-fbb9-4504-81b9-a0a02f59036f'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '6705ec72-2a3b-43e4-9499-a0da7b491785'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '230b8650-fe23-4b74-b0c6-a16f94f85956'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0, VisiblePosition = 7 WHERE ID = '910b60fb-55bd-4645-b02f-a18ce0792ba6'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '1af65d9a-e09a-401d-bf19-a1b11dfa4d08'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '87a2ae6b-ddeb-4040-bd3d-a1fecf1046eb'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '30721697-9419-450c-a4c7-a23aca937c3b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'd48c4c8c-1a6d-46b2-8127-a251925d0f8c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '6ef03e5e-c271-454e-ab03-a27fce7ffa06'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'd3905906-e50b-49d9-92e1-a2d9ba6b70be'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'eb0ae816-909f-45f2-afb3-a300e1596de3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '427e68b0-a75b-4f0e-9d48-a3066ed411b4'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '59e20ce3-6b84-449d-88eb-a38b90cb74d7'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '7191518c-8c73-4e0f-9e10-a3c82069334b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '9cf51bd2-24bd-401b-89ae-a4392b22797b'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '8bc6c90f-e2f6-47d3-8fd4-a43e33e97281'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '0fc39f8e-dfa3-45b6-b0be-a49c777a45f2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'b6e7212c-6349-4772-ab1d-a4d0ce41c7c7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'cbcb7f07-6160-4b88-87aa-a5830aaea10a'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'f4bdafa8-0f10-45ed-b01d-a632791a9d9a'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'e2e715bb-7991-498d-b2b0-a6528af6047f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'a3a1505f-aa1d-44fe-9bdd-a668670df45d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '85911694-be15-478f-b6de-a69ae3de1f8f'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '2db035e4-ab80-461d-9aad-a6affb8b2636'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '8c599b78-abae-4c7e-ac4f-a6ec5c7a5803'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '7156a7f6-813e-4e05-99c6-a72691b4c4c0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'b2e15295-d70e-4b26-8787-a7531662de1a'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '129de25f-5387-4e1b-8420-a7dff1cc618e'
UPDATE dbo.TemplateColumn SET VisiblePosition = 7 WHERE ID = '4b244f0b-9293-43a2-a375-a891626b6e99'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'fecc84a3-f999-4cc0-870a-a89f1150cb56'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '9910c4e2-7dca-4b52-8659-a90bb51d8138'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '9c16fb2b-e8be-4bb5-b761-a9810e1049d5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '043d7f73-b890-4f3d-8d38-a987fef0ba5f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '6ca07f7e-3ab4-40b6-adc4-a9b66e68f9a8'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '6458246d-e915-4d64-8b13-a9f274f70b74'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '29341e09-5818-4642-9f21-aa36358da17b'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '7fb7c649-9eaa-428d-863e-aa3afcf122f9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '15ea41b3-e73e-4d54-8851-aa4783b0447e'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '75eccc95-bf09-4a20-9619-aa609ff3299c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '1c56fba5-bee1-41ee-8042-aa6b9638fb42'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '3ea4e5a7-d000-4a99-835f-aa82e4b9fbaa'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '72bb5cb3-614c-471c-926d-ab37f15290f4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '0eff0846-24e4-4103-b227-ab421d8d25da'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '07b1d59f-d92c-4bb7-87e6-ab962784ac42'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '51ec7cae-8019-4a7e-a623-abdeb02425b3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'fba4ce7d-fa36-42e8-a7c0-ac5a1c5e94fd'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '60e7e16e-977b-482f-971d-acac3cb885a5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'b9b17632-4935-4f19-8674-adaac787312b'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '2f77347f-4690-491e-9e01-ade5b2c9f0ff'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '3e97ef4f-71f0-449f-b422-af1390ce08a4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'aec20ef7-4cb4-4d20-9474-af19bb1942b1'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1, VisiblePosition = 4 WHERE ID = 'c67410b5-833f-4c4d-8361-af88a64a33b9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'cd72d7a2-46c0-4ad1-a099-afbf65b67a03'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'c9288a8f-f85e-4978-b431-afc7b0e377a4'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'bfccb938-76ee-4378-a664-b0a496aae551'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '05c0dccc-e3d6-4af8-9173-b0ba4fe1ae0a'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'e8d021fc-19af-4ccf-8253-b0cd680f8112'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '12eee0dd-1a9b-404f-b1ed-b12e92b1b854'
UPDATE dbo.TemplateColumn SET VisiblePosition = 12 WHERE ID = 'c161d496-df33-41d6-a638-b19bb9e8c2e5'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '7a03beb1-69ae-4c7d-a7cf-b242712a7aea'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '8cfca8f8-c5cd-4e8f-831f-b278883b5900'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'a48317c6-f44e-4d00-af21-b2c94240339d'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '183e4fb5-1fa1-4a19-be6c-b3068daa9487'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '70cfdbef-aefc-427c-a653-b344876e8863'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'f5a4213b-9b55-4d56-816e-b357067b0b8b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '2786e493-df73-4ee1-a176-b35c99d70d0c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '8af69184-3e90-4148-9f12-b4abda0020ba'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '64204c4c-1bd3-4d96-8e0a-b4f826ac9d5b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '6fbdb7d4-046a-4662-b1ab-b50bf666f498'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '5108b35a-9399-4011-9da0-b527fe2ab62a'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '82f2b208-662f-48d5-8831-b597d484ce0d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '314d1d9b-9485-4852-9cf9-b5d5fa82f6bb'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '59395d18-002d-4f64-9a24-b62b952358b5'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'f8ad7bdd-9804-4328-babd-b6b8009c42c5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0, VisiblePosition = 7 WHERE ID = '61dbc2a0-8d09-4d68-953b-b742f694a90b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '6feac7f9-2bfd-4fc3-a8a6-b74435ae4228'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '4e507536-6796-4713-a246-b7863c700908'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '25a1b6a5-901b-43dd-8d1e-b9082ffe4bba'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'fc08d380-bf1d-46fa-847c-b934b5f2edde'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '819d3214-08d8-40f4-bcce-b9437ad41312'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '7f7186a9-1e52-41ec-a587-b97380c04dad'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '9fac8af8-26f8-434e-81ae-b9a8e61d6e71'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '954a2582-04c6-451f-bbc1-ba97a1ff9fa8'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'c8a4cfc9-6a00-4eab-a4fd-bad48c08226e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '57513412-13c6-4efa-9efd-bae25a92fbaa'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'c0357192-081e-4879-b044-baefdbdf55ef'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c6c37938-8dd9-4ebd-aa7a-bb01af94387a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '9144ea86-72f6-4e36-9ce4-bb686be39505'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '65f83d42-dc26-4cb0-9b22-bb7c2466f394'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '85712ebe-55cc-411c-9008-bbc0a21f5cdd'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c534667e-f736-4ccf-bdce-bbe964d328fe'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '55efb7b4-0ca7-48f5-8d8e-bc2cdefe5666'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Số tiền QĐ', VisiblePosition = 6 WHERE ID = '03fb9f85-8c08-4b07-af0e-bc57f19f5e5a'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '38419242-ad66-41f1-8700-bc9cbdf89da7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '46aa0499-f11e-4556-857c-bcdacdd07bd8'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '7c29b43f-5df2-414b-8f7d-bd1aa63cd5d0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'd4ee1134-6936-4cd8-88e1-bd6a630e1bfa'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '398e8a03-2dfc-474a-aeaf-bd9b7bd54d8d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '8db855fa-4aea-4723-90c0-bdb9633b7ef2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '7543e454-68ac-46fc-adb5-bdec0858d4fb'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'ccbf266c-7149-4a49-a67f-be0daa792908'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '9bacee75-2821-4a7f-9310-be552d591e86'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '99f89c16-f9f6-4095-9954-be5e0603500b'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'c04f0b23-c5b1-4503-83c1-be8582f6e52c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'dfe3bc2d-cd8d-453b-a8d3-bea9bda8b690'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '46d77bc8-2e6b-45ec-a9eb-bf04175bd43d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '3176f39e-f5d2-4e93-8f9e-bf43e34e22da'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'b9f238ef-ee74-4016-af7f-bfebec2ad724'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '355356b8-a2d6-48d6-a30e-c00aba288110'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'ddb27295-4c9c-4e12-8ffe-c03cb96c81bf'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'af695fbf-7dcf-4f07-b9ee-c06679b98dbe'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'ff29c87e-0a29-4aaa-8318-c0fd9345239a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'd7d0a740-2e77-4bfc-abc2-c0ff33c141d9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'e487ecf2-ebae-4f3b-a342-c11121120114'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'bf6ea5d8-337b-4711-898e-c12e48ffab7c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '818d9940-668b-4df5-84de-c22d689ca9d7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '2612c319-cbd8-44f3-8f4f-c2451286b584'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '1fb3d0a9-72bc-4a6f-bc70-c26b11272099'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '9d5894e7-2c93-4e14-bf75-c28472a71fb6'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '54324d92-6fd7-42f1-8912-c2cb4df34b1e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '818747ea-4f8b-424c-8d6b-c32b021088b7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'b67dfe2f-fb5b-4c81-9821-c3739f509f6f'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '12876b23-7b15-48bb-a2b1-c377db118aba'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '1849ff12-4bd6-4427-aa76-c398ed774c5e'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '74c5b3c9-3bb9-443c-88fa-c3c88ea70fbb'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'e58cf14b-e948-4662-98dd-c3f3fd78119f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '00decbf0-b5ae-47f9-9b81-c4188afe570e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '7baa7d96-171d-457d-9084-c43474b47fee'
UPDATE dbo.TemplateColumn SET VisiblePosition = 17 WHERE ID = '3ad7397c-80a6-4bf4-81b0-c488c80d0e52'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '63a6ab33-4fd8-47e3-8c00-c4e6d930cb92'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'a308bc8f-1b2e-498c-ad1e-c525a83c772c'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'a9cbfef1-34e2-4876-b7cf-c5d3df42c323'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'dfebe60b-4be4-4164-b299-c5ec631750ac'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '9bc28e28-62d9-4fb8-a507-c66f61545f2f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '0c940ad1-c126-468c-bb93-c6b75afb5f3a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '64a767f4-9940-4591-affd-c78dadfe09e5'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '9859902a-03f0-44bd-b9ad-c7a4efd15296'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c17b2f65-8e51-43d0-90b8-c7b0ad7f150e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '44c5a927-7d59-4c1e-96fb-c7c77cb76178'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '5dfa1802-bb8e-4c0f-bb96-c7f3f5e4d476'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'e21d7d4e-60da-44be-9662-c8421f8d4531'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'f45102cb-11c3-4cb2-9873-c8533e3c7917'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '39ebb36c-e731-4aa6-9b54-c91d997599e4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '007e3672-8be4-4008-82e4-c9399db25a64'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'e6590cca-9826-4414-93c9-c9d4e65023a0'
UPDATE dbo.TemplateColumn SET IsVisible = 1 WHERE ID = '1d0370c6-3a15-4ba3-87ee-ca21a0ff4e5b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '468f3713-73f7-490b-8ef1-ca5a14ef496b'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '42162b2c-0b69-4245-90a7-ca82c379b562'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'ba090443-4451-4838-ae4b-ca9982be3edf'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '0eb98cb9-800e-4b9b-84af-cb0a816fd48e'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'd6da6b4b-c918-4d4e-b383-cb4592d1c19e'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'ee1b01de-26dd-43a5-999a-cbb00e0f3f02'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'd36e6ae4-74ea-45f0-bb6f-cbb1733d46e3'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'dbcde511-1938-4b4b-9376-cc32bb3fd371'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '4f5eb7d7-2f9e-4de1-a70a-cc3c76d241a0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '72239895-00a0-478a-94da-cc3cee85e571'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'd7deb7eb-cbb4-415f-9a7f-cc7982409e01'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'c72981c5-d25d-48b8-b9fc-cccbdcbbe8de'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '6ac6ddef-cd0f-4c38-973c-ccda3b454ca4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'b7bee5a3-8139-48d2-be17-cd54b7704b45'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '7bec979b-fa0b-407c-b0a1-cd93bb3b738f'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'ead081f7-5a6d-40a9-a086-cda9bb9a31ed'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '227a7478-36ec-4bb9-b0b8-ce0dce4b6d8a'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'dbcf9c6a-3b69-43b3-952d-ce28e7e57ab2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '6a929f37-c9e7-4dfa-bb9a-cec55a06737e'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '59ac9de5-2fa7-480e-a9b7-cedd3ee53495'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'b9245ffd-df7f-47bd-863a-cf45729b1af4'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '23108d18-fad9-4391-91d5-cf5b4b32a339'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '724967bd-d6a9-4722-8679-cf5dd9d2e4a8'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '34ce3e0e-60d8-49fd-9d52-cfae93174dad'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'bfeb8481-962b-415d-af04-cfd282ebf02f'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '008ac3d4-1ecf-486d-b916-d043e8f18979'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '89055d9d-6f8c-4390-a9bc-d09716b09007'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'f1e5e0d9-d4df-4468-b178-d0aed3dbacb4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '1035cb53-b762-4610-94de-d129a377e68d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '979e5313-0bf8-425b-b352-d18236ea2b98'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '8c08199f-f979-4179-89e2-d2a6cffc2b74'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '8c05c890-6634-44d7-a15f-d33d2777d1c4'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '54b4429e-dcb5-4c86-a0ab-d36f2961b44e'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '48525df4-267c-4f27-bbc2-d3a7538a1bc0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'a4b2bf0f-399d-456e-a121-d43806c47f8c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '96529c79-837a-4844-97d6-d449312c8777'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '4cd2f404-f347-4e23-bd68-d51d1b1d9173'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '68659b47-8e4d-4ef2-806d-d55050337815'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'aefd7484-cab3-4277-866b-d56215852549'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '139ff3ba-1446-40f2-af92-d5c4ea0cda9c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'f46ba24f-e8d0-46b3-a704-d5d7c8d6f131'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '166a1b53-10d5-4176-9451-d638d7d12e9d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c22348ae-7ff8-4426-affa-d6407d1e0823'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '41d9b831-4962-4aef-ac16-d664ed3fa7bb'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '7234185b-2dd4-4b66-9544-d66c2874f015'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '822863b5-6bfd-41a3-89fb-d68010641f5a'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '4277abd3-14ef-4130-9839-d844f05b16e8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '07c2204d-bfd2-49f3-9662-d847c6c0f01d'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '09724aa3-6969-427e-9a0c-d864224d322a'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '31a1f77f-74c8-4439-94ed-d872c92f28ac'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '7387f8c2-f44e-4625-a55c-d8cf5586d44e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '75774685-0dca-4f09-b45d-d9792653c1ae'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '8da0617b-fa12-44c6-bb40-d9edbaec2abd'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '80f050ac-f62d-4e99-a2d6-da45e8ceaaf1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '7cd61a32-14dc-4a53-85c5-da69e7be11a5'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'f786b338-3390-4f56-8d17-dafcc1f48a5f'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '2add1d84-1257-4c08-8196-db56c562a9e0'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'df3e84c6-a886-481c-83b8-dbb6e9e11a90'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'a24ee22d-8dfd-4573-8ba6-dbbd0b3de056'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '3a59cbfe-bdc3-4afe-89ab-dbd4d6664c87'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'ee7f2ef3-f914-40aa-8e9e-dc0300bb2484'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '7a331e76-d1ad-49b7-9cb7-dc18b0fd0457'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '4cf19023-f1a4-46fa-a9d7-dc4636786631'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '0c64e63a-d271-4d1a-ba6f-dc6429ab39cf'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '1a638b5b-6c40-4ad4-b1a4-dc712c68f633'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '730991f9-ba43-45d9-af8d-dcccf20bc5e9'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'e5f62741-89f8-4275-8a62-dd5d4bda238f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '098d8566-b700-45dd-9a94-dd83165c0a9a'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'ad991f32-b460-49d4-a9de-de19c913bc90'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '899665ef-5747-46db-9f9b-de244cb54456'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '2c1f0aec-3e86-49f3-9aad-de38393554c3'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'f6a30b55-ad2a-4785-b577-dea201b05ed2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '09f4aadd-a99d-472a-9c51-df4b8884be08'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '7cfefdcd-1844-446f-ba42-df696c132277'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '7f19456b-0119-4f5e-bfe1-df8e2e9501bb'
UPDATE dbo.TemplateColumn SET VisiblePosition = 30 WHERE ID = '4fee70a5-d7bd-4eaa-b6db-e03011ee0452'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '675cc848-57d9-4d4e-9efa-e05602374de6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '086d4783-7bca-4c66-a3ff-e114d34cbd13'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '2faf9d48-7c85-4e41-a97a-e12db6d4f4d5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'd3c8158e-f64f-47b0-a84b-e17c823c657b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '94df597f-00dd-4fd6-ac0a-e19f75faa180'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'e23c8312-f5f2-484a-bf2e-e216819a0204'
UPDATE dbo.TemplateColumn SET VisiblePosition = 5 WHERE ID = '55379361-1edd-42d0-86e2-e2500e1458a1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '5bbcfbc0-5705-42d2-ab88-e2b4481b99b4'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '111d185d-8277-421a-a005-e2f1b9d7dd87'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'd82055cf-43b7-4222-a207-e3025257038f'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'ff78fcda-f958-4872-a32a-e3aa7763d737'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '57c61eb1-6cda-4ecb-a226-e3b4bb2ebd2a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c44ec458-8b38-4685-8f3c-e3c6f1d41a27'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '1bcca3b5-6319-4b45-b441-e3ecc4b20c1e'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '45b27604-5d4f-45b5-b42d-e426fc6e8f0f'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '1d5cca5c-e212-443a-8f2c-e434ffacc12e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '78b98d84-0637-495c-87c8-e46ab9b7c82f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '8eedf40f-aace-477e-91bf-e486f0621dc0'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '0ea77373-fe1e-455d-bdb2-e52fc9a42325'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'bad25e33-90d2-48d3-bc7e-e5da979ff626'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '44045430-9c69-443a-b145-e653a29bcc11'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '35b2e36f-9ce5-4f8e-9410-e6925b2e6533'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '867a1de7-7ebb-4d68-8d3e-e6b23c3deca9'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'da9f8717-5dd7-44ea-b1e6-e723ac10bd65'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '5f2aad73-9dbd-4e70-a42b-e74c5c63acae'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'a34409fb-e01c-4bae-9553-e74ca11ee0be'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '35214391-f4a1-4b0a-afb9-e77a872b8dd9'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'a813f580-1ddc-455c-9ed3-e7afbb391370'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '9e019245-922c-40b2-bf3a-e7d5715bf763'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '0b264373-1137-4fad-84ff-e880d3bfd452'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '301265d2-79ad-4b09-a4ba-e8a775e78ecf'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '067f5fbe-cf75-4919-972b-e8af1788597a'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'dee1bc50-50aa-4c49-8f7f-e91ac9d7131e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '0cccf5cb-3092-4eb0-bf48-e974d1ae21a8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'e90d414f-62ce-405e-a11c-e9ce86a8af16'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'f6707baf-b557-4cb1-b2d2-e9e1e37a0e52'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'aff05876-196e-4714-8b4e-e9f9b3523f65'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '8b260b4f-c6a2-4e6d-b4ea-ea028e1f1364'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '769463fe-ad26-400c-af01-ea2a08c9efe1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c7ab623c-34a5-49b5-8465-ea6fc344cf75'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'cc6efe2c-70a1-4b37-a803-ea785b00c972'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'b7ecc230-bce3-4979-8220-ea91b2e44787'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '377f270a-e6fb-439d-a866-ead4a7834021'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '1b00439c-a9f2-435d-8dd6-ead4ffc54ce6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '7d3c2d7a-39f0-45fd-9e87-eb3ee50240a3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '6bd5f4bc-ae79-4b5e-a51a-ebe2afd7e39d'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'ac9667e8-d428-4de8-a9a1-ebe5ef164dfb'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '0cb6e53a-cc2b-4df2-b471-ec511b0246e6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '945d9144-4c04-484c-aa2a-ec732bf2de6b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '16c1e788-5d72-4a75-a0ee-ecc91a61798e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '099145dd-b5a8-43c3-88ea-ed1a9ecbbdd6'
UPDATE dbo.TemplateColumn SET VisiblePosition = 6 WHERE ID = '05e37a0a-c661-4744-8ace-ed814b29032b'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'b697b02f-ec45-413f-9763-edf9136e9474'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '0b0114d1-67e6-4dd9-b4ec-ee76232152c1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'a5a4d392-517b-4f98-b168-eedefd0eb551'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'f34b1627-f2d4-4d2f-8e35-eee544db9e9b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'f23ec6c0-6d05-44f6-b435-ef1738ebf119'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '001f182e-f7a5-436f-8d66-ef2d52e65043'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '48b246ae-c7b0-4541-af0d-ef499cb9e78c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '2175f6a5-e4fe-426c-9420-ef75b91b8fb7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c54cc438-588a-4c1a-8e52-efde644c8bf3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '7bd86892-7bf6-4868-b09b-f0071ac57e05'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '71fce5b1-d268-44b4-846d-f01c51486c48'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '327c9341-aca8-4fb9-84de-f0405c1cfd52'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '6af30839-8f90-4816-9aff-f04ce5db5d61'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '07b920fa-f0c2-4375-a664-f081a1704afc'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '027b6363-f58f-440a-be1f-f087e397b442'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '7ca01cf0-8e0c-4218-be1a-f09011ad7d30'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '475886c6-5d2a-4040-aba6-f0fab800e248'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '65c1078d-ba8d-4c07-bc91-f10b46b8d671'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'd97c8342-4068-4706-9514-f17f87e56e9a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'd5bdc2cc-54a7-4fde-b455-f1f6c1b54ba5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '7ffebc8b-4cba-4e0c-bc57-f25ae3d747c6'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '19978982-c289-4b89-a5eb-f2bb9744c9be'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '9806785c-a359-46a3-824d-f2c984dcad81'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '680f56d5-05da-4c88-8563-f2e4696b46e9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '7eef7621-a327-4eae-9247-f2fa312f5d99'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'f6020767-d0c4-40f3-8961-f3d0a5caa1f0'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'c57e66b4-08ce-4e56-abb5-f3e30e3cc969'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '2be575b0-ffa0-4d01-b64f-f3fa13e1f427'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'de7112c3-432e-41b5-9ae5-f4221f04b2a5'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'd5ecd043-5a57-4202-bfb9-f46b0b9e0b63'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '48d25187-fb06-45b2-bfa6-f47a7045508b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '9e56becc-9456-4ac6-95c7-f50e0156706d'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '64b79cdd-e0b6-41c7-ab85-f580cf557865'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'ba3781d3-0c99-4db8-928a-f5973a435728'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '0f5c0b1b-3782-4fc0-adf4-f621c6fa0edb'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'fa7f37d5-9eaf-455d-89a7-f6b0c34fc55a'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '5716d6e7-9205-41e1-9305-f6cb2fd930d3'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '4532bee6-b33c-46f3-8586-f789593238ec'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '128e54d0-d5c2-410d-a3b7-f7a3a9ddad4b'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'dd62e478-6d22-4f6e-9ebf-f7ebb16f3492'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'e42504aa-1e8a-4c85-b02d-f81c217446fd'
UPDATE dbo.TemplateColumn SET ColumnWidth = 125 WHERE ID = '63c66311-5567-4461-9130-f86b7c2d043f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'dc126af3-a511-4b85-9005-f8d309aa9a02'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '92688425-adc8-4467-b720-f8d7912aea30'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '54de8b2d-f926-41f9-ad9b-f8fe1e499b18'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '4d35217e-f58c-48df-891f-f92f577bfb31'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '28a48af5-a0e8-428b-a7aa-f973a2296e56'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'd6680d15-eaaf-44dc-bfb4-f9e4234efa46'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '8bb0965e-9510-4be4-9450-fa1a5a3cde08'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '45bdc4ab-7e62-468d-a473-fa30b81d84fc'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '13e96dad-f5a0-41c9-8ac6-fa4bd88e8740'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '889eaea2-bd5a-4a85-8f29-fb24182196da'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '2ed4e537-e810-4576-9790-fb364869de49'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'ae471df8-5492-4e1a-876c-fb66895e423d'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '8fdb1e19-a000-47b2-b5d5-fb9cc937a4ca'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'fd695cb6-d4e5-4a28-b0c6-fbc6b8b1f57c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '5a47adb9-d23b-468f-a5c5-fc2146291c4b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '04708951-dd14-4dda-a65c-fc7e80b9f92a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '9883ff7f-262d-4819-a8c7-fd62b613113f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '5d4e54fc-1d54-480a-852b-fdad1e38f069'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '0e9a471a-4440-4980-946d-fe2297c1b2e6'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'c2adaa20-c469-4cbe-a592-fef6f8c585b5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '488d6f92-c8bc-432e-84c9-ff016e6a59c0'

INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('f48f1fd9-97eb-48a5-abd4-0229803d9cdd', '9ad11a58-93c0-43df-9cfd-dfd469e0237d', N'DebitAccountingObjectName', N'Tên đối tượng Nợ', N'', 0, 0, 115, 0, 0, 0, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('f212a3b6-6bad-4ec8-8096-04fdb32b10a7', '68ef2038-352d-4297-834d-a3e7b41d6438', N'CashOutExchangeRate', N'Tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 17)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('c38851f3-b8a7-4d9f-9624-087133ed9ef4', '9ad11a58-93c0-43df-9cfd-dfd469e0237d', N'CashOutAmountVoucher', N'Số tiền đã xuất quỹ QĐ', NULL, 0, 1, 105, 0, 0, 0, 27)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('c9c03931-3b05-4317-aa60-0dd67490b6d5', '6112c4a7-67fa-4604-937e-dbb62e7031a5', N'PayRefNo', N'Số chứng từ TT', N'', 0, 1, 105, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('0bf25e2c-2920-4447-90f7-118a14ab5dbe', '9ad11a58-93c0-43df-9cfd-dfd469e0237d', N'CashOutAmount', N'QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 29)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ced1db9c-a26b-4fcd-805e-16c6ce862cc9', '9ad11a58-93c0-43df-9cfd-dfd469e0237d', N'BankAccountDetailID', N'TK ngân hàng', N'Tài khoản ngân hàng', 1, 0, 95, 0, 0, 0, 7)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('be0bd06f-0ed0-4c71-86b7-19072a465d3d', '8c3360bb-8e4e-4d71-8a90-71e0d304d9aa', N'CashOutDifferAmount', N'Chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 19)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('e519b9dc-d024-441d-b579-1e57370f5f5d', '9ad11a58-93c0-43df-9cfd-dfd469e0237d', N'Description', N'Diễn giải', N'', 1, 0, 250, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('f973f937-3405-4bfe-a20b-1f876d917fef', '9ad11a58-93c0-43df-9cfd-dfd469e0237d', N'DebitAccount', N'TK Nợ', N'Tài khoản Nợ', 1, 0, 100, 0, 0, 1, 5)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('5f06437e-ebbc-44ca-9f18-231bbca5211d', '9ad11a58-93c0-43df-9cfd-dfd469e0237d', N'CreditAccount', N'TK Có', N'Tài khoản Có', 1, 0, 100, 0, 0, 1, 6)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d7b46838-d8f7-45b9-a963-23b56dc13faa', 'b9e10e3e-92ce-485c-8980-9969a001878c', N'CashOutDifferAccount', N'TK xử lý chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 20)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9b0eb694-5b40-47f9-9266-24e73aab7621', 'f0c4207e-c046-492f-90f7-68df7b31db9c', N'CashOutDifferAmount', N'Chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 19)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('89a931e1-2bda-4056-829a-282f8c0025a9', '9ad11a58-93c0-43df-9cfd-dfd469e0237d', N'AmountOriginal', N'Số tiền', N'', 0, 0, 120, 0, 0, 1, 10)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('11e631f4-e88f-4152-a5cd-2b60e3e349d7', '8fac9285-0f37-461e-83a3-585fd6515b13', N'ExchangeRate', N'Tỷ giá', N'', 0, 0, 95, 0, 0, 0, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('153b6405-1f2d-42d5-b990-2e2d4d39fabe', '9ad11a58-93c0-43df-9cfd-dfd469e0237d', N'EmployeeID', N'Nhân viên', N'', 0, 0, 133, 0, 0, 0, 14)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('c3c9a4d7-2192-4778-8098-2f0a02111f77', '9ad11a58-93c0-43df-9cfd-dfd469e0237d', N'CashOutAmountVoucherOriginal', N'Số tiền đã xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 26)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ab7b298e-0b35-4c32-a364-343202e2a6e3', 'b9e10e3e-92ce-485c-8980-9969a001878c', N'CashOutExchangeRate', N'Tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 17)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('902f37b1-57fd-4107-a2ca-388cb9b59b44', 'b9e10e3e-92ce-485c-8980-9969a001878c', N'CashOutDifferAmount', N'Chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 19)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('17c137d0-43a2-4565-afd0-396a7062148d', '9ad11a58-93c0-43df-9cfd-dfd469e0237d', N'CashOutDifferAccount', N'TK xử lý chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 31)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('85d72b4f-370f-429d-b8d1-3a3f4f0e5aa9', '8fac9285-0f37-461e-83a3-585fd6515b13', N'CurrencyID', N'Loại tiền', N'', 0, 0, 80, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('cae507af-f310-4a03-b77f-3d4d3913a235', '9ad11a58-93c0-43df-9cfd-dfd469e0237d', N'CreditAccountingObjectID', N'Đối tượng Có', N'', 0, 0, 108, 0, 0, 0, 13)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9a1dcf1c-c657-4c4e-87fc-3f7167a301e9', '900479e1-85c5-40ec-afed-692663b05348', N'PostedDate', N'Ngày hạch toán', NULL, 1, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('b771a2ad-784a-4cef-a2ea-40c848ae48c7', 'f0c4207e-c046-492f-90f7-68df7b31db9c', N'CashOutExchangeRate', N'Tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 17)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('569b9ad5-6046-49bd-b2a6-4105d13acdc6', '7ddd4ea4-3ff1-4748-bb5b-115825a9dc61', N'RefVoucherExchangeRate', N'Tỷ giá chứng từ', N'Tỷ giá chứng từ', 0, 1, 100, 0, 0, 0, 4)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('8b2e11dc-ba56-4bea-b4dc-41889f764ab5', '68ef2038-352d-4297-834d-a3e7b41d6438', N'CashOutDifferAmount', N'Chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 19)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('f20b61df-0f22-4660-9071-41a05fad62b9', '9ad11a58-93c0-43df-9cfd-dfd469e0237d', N'CashOutDifferAmount', N'Chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 30)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('a9e9a3ec-82a7-40e1-ba63-42438055870e', '8fac9285-0f37-461e-83a3-585fd6515b13', N'TotalAmount', N'Quy đổi', N'', 0, 0, 120, 0, 0, 1, 4)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('33e972aa-5d88-412a-a6ba-44bff1241d82', '6112c4a7-67fa-4604-937e-dbb62e7031a5', N'DebtAmount', N'Số đối trừ QĐ', NULL, 0, 1, 105, 0, 0, 1, 8)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ae580393-2c59-47b9-ab8b-49e9eb9ca85b', '8fac9285-0f37-461e-83a3-585fd6515b13', N'TotalAmountOriginal', N'Số tiền', N'', 0, 0, 120, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('7d7b3423-aaab-481f-95c5-4c962315797f', '9ad11a58-93c0-43df-9cfd-dfd469e0237d', N'BudgetItemID', N'Mục thu/chi', N'', 0, 0, 105, 0, 0, 0, 15)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('dae7d274-b137-4093-889c-53336b3da4b7', '9ad11a58-93c0-43df-9cfd-dfd469e0237d', N'StatisticsCodeID', N'Mã thống kê', N'', 0, 0, 133, 0, 0, 0, 17)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('c8733753-4295-436d-9a04-5961a330bfc3', '8c3360bb-8e4e-4d71-8a90-71e0d304d9aa', N'CashOutAmount', N'QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 18)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d3fa0f5e-80f9-4756-bc03-5a96f60b2289', '9ad11a58-93c0-43df-9cfd-dfd469e0237d', N'ContractID', N'Hợp đồng', N'', 0, 0, 133, 0, 0, 0, 18)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('850a82ee-521d-4dfd-86ad-5f3e0b063a6f', '1ceded2d-ebb2-4c6c-b752-af35ab7b4fc3', N'CashOutExchangeRate', N'Tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 17)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('26a29404-f10b-4d79-8fc8-60b22ace0cea', '9ad11a58-93c0-43df-9cfd-dfd469e0237d', N'DebitAccountingObjectID', N'Đối tượng Nợ', N'', 0, 0, 133, 0, 0, 0, 12)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9a9df52a-5784-4738-a8ea-64b8f371ed8f', '9ad11a58-93c0-43df-9cfd-dfd469e0237d', N'DepartmentID', N'Phòng ban', N'', 0, 0, 150, 0, 0, 0, 23)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d4789912-20a6-4c83-9d52-6b369d61b551', '68ef2038-352d-4297-834d-a3e7b41d6438', N'CashOutAmount', N'QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 18)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('278193fc-419c-4804-b8b2-6b9fdd7d367b', '6112c4a7-67fa-4604-937e-dbb62e7031a5', N'DebtRefDate', N'Ngày chứng từ CN', N'', 0, 1, 105, 0, 0, 1, 5)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('b9a86d0a-f114-4827-bace-6c52661beaa5', '1ceded2d-ebb2-4c6c-b752-af35ab7b4fc3', N'CashOutDifferAccount', N'TK xử lý chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 20)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d801f9e7-d177-4f96-9cc9-734f7e0c415c', '9ad11a58-93c0-43df-9cfd-dfd469e0237d', N'CreditAccountingObjectName', N'Tên đối tượng Có', N'', 0, 0, 115, 0, 0, 0, 4)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('91a91ece-30ac-4101-a676-76ecfb26f418', '7ddd4ea4-3ff1-4748-bb5b-115825a9dc61', N'LastExchangeRate', N'Tỷ giá đánh giá lại', N'Tỷ giá đánh giá lại', 0, 1, 100, 0, 0, 0, 8)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('8977915f-08c2-400c-9d01-77756052560d', '9ad11a58-93c0-43df-9cfd-dfd469e0237d', N'Amount', N'Quy đổi', N'', 0, 0, 120, 0, 0, 1, 11)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('c0f0d9c7-c8d3-4138-aa93-78a5fbd266b1', '68ef2038-352d-4297-834d-a3e7b41d6438', N'CashOutDifferAccount', N'TK xử lý chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 20)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('544f7372-36fd-4466-88a6-7a035fdf8b96', '6112c4a7-67fa-4604-937e-dbb62e7031a5', N'PayRefDate', N'Ngày chứng từ TT', NULL, 0, 1, 105, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('a324ba75-2b7c-4291-832f-7cc8312eb778', 'f0c4207e-c046-492f-90f7-68df7b31db9c', N'CashOutAmount', N'QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 18)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9836b586-5403-4ccc-ac46-824f18d86cda', '9ad11a58-93c0-43df-9cfd-dfd469e0237d', N'CashOutExchangeRate', N'Tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 28)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('3defdb8e-08ee-410f-af2e-82ee6d5ff429', '6112c4a7-67fa-4604-937e-dbb62e7031a5', N'PayAmountOriginal', N'Số đối trừ', N'', 0, 1, 105, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('2c3b8dda-33da-4f67-a4e1-8c1d04794e64', 'f0c4207e-c046-492f-90f7-68df7b31db9c', N'CashOutDifferAccount', N'TK xử lý chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 20)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('4dd008d0-144a-4639-922b-99014e604f5f', '9ad11a58-93c0-43df-9cfd-dfd469e0237d', N'IsIrrationalCost', N'CP không hợp lý', N'Chi phí không hợp lý', 0, 0, 100, 0, 0, 0, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('b30691e3-d167-493f-8e9a-a2b12ed7094a', '6112c4a7-67fa-4604-937e-dbb62e7031a5', N'DiffAmount', N'Chênh lệch tỷ giá', NULL, 0, 1, 105, 0, 0, 1, 9)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('11a086cc-0650-444e-985f-ab910f4f3167', '1ceded2d-ebb2-4c6c-b752-af35ab7b4fc3', N'CashOutAmount', N'QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 18)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('8b48b6f2-10c5-4fab-a3c6-b4ecd9393426', '8c3360bb-8e4e-4d71-8a90-71e0d304d9aa', N'CashOutDifferAccount', N'TK xử lý chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 20)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('62996861-65bd-49e3-b392-c44ba1151757', '6112c4a7-67fa-4604-937e-dbb62e7031a5', N'PayAmount', N'Số đối trừ QĐ', N'', 0, 1, 105, 0, 0, 1, 4)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('adf8e843-39d7-4c65-ab8d-d2dc85215da9', '6112c4a7-67fa-4604-937e-dbb62e7031a5', N'DebtRefNo', N'Số chứng từ CN', NULL, 0, 1, 105, 0, 0, 1, 6)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('396f5dae-36f2-47b5-b550-d7b68efa54f0', '7ddd4ea4-3ff1-4748-bb5b-115825a9dc61', N'DifferAmount', N'Tiền chênh lệch', N'Tiền chênh lệch', 0, 1, 100, 0, 0, 0, 15)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('a1aa9060-fff7-416d-9b89-db15fbe4f99b', '8c3360bb-8e4e-4d71-8a90-71e0d304d9aa', N'CashOutExchangeRate', N'Tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 17)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ce16ad5a-8ce2-4057-a414-e3ac7c140fb8', '6112c4a7-67fa-4604-937e-dbb62e7031a5', N'DebtAmountOriginal', N'Số đối trừ', N'', 0, 1, 105, 0, 0, 1, 7)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('cfb856c9-0248-4b84-97c3-e7a62aaa634d', '9ad11a58-93c0-43df-9cfd-dfd469e0237d', N'ExpenseItemID', N'Khoản mục CP', N'Khoản mục chi phí', 0, 0, 130, 0, 0, 0, 25)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('a67785a9-b4bc-4da0-b316-ea4abbae47b8', '9ad11a58-93c0-43df-9cfd-dfd469e0237d', N'CostSetID', N'ĐT tập hợp CP', N'Đối tượng tập hợp chi phí', 0, 0, 178, 0, 0, 0, 16)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('fc3914b6-983b-4188-8943-f11bfae908a2', '1ceded2d-ebb2-4c6c-b752-af35ab7b4fc3', N'CashOutDifferAmount', N'Chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 19)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d75e5765-3ea4-42ab-a727-fd5257eb6460', 'b9e10e3e-92ce-485c-8980-9969a001878c', N'CashOutAmount', N'QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 18)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.TemplateDetail SET TabCaption = N'&1.Thông tin' WHERE ID = 'e7797687-d784-423c-a4a7-15439c2241dc'
UPDATE dbo.TemplateDetail SET TabCaption = N'&1.Thông tin' WHERE ID = '66339b5e-2222-4e27-b9fb-4ee48e7caa5f'
UPDATE dbo.TemplateDetail SET TabCaption = N'&1. Hoạch toán' WHERE ID = 'b31e485e-d51b-4a7a-b9b6-66e691ecdab0'

INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('8fac9285-0f37-461e-83a3-585fd6515b13', '93d6cc08-c956-40ab-9fe2-5c565e3b6cbd', 0, 100, NULL)
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('6112c4a7-67fa-4604-937e-dbb62e7031a5', '93d6cc08-c956-40ab-9fe2-5c565e3b6cbd', 1, 1, N'&2. Chứng từ đối trừ')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('9ad11a58-93c0-43df-9cfd-dfd469e0237d', '93d6cc08-c956-40ab-9fe2-5c565e3b6cbd', 1, 0, N'&1. Hạch toán')
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.TimeSheetSymbols SET IsHalfDayDefault = 0 WHERE ID = '0da01b75-3d5b-4224-81cc-13f403a0ad33'
UPDATE dbo.TimeSheetSymbols SET IsActive = 1, OverTimeSymbol = -1, IsHalfDayDefault = 0 WHERE ID = '93b8cc3c-f4d8-4778-93cc-1e962382c009'
UPDATE dbo.TimeSheetSymbols SET IsHalfDayDefault = 0 WHERE ID = '766b5dcf-acda-4966-b450-1f6960d75e8c'
UPDATE dbo.TimeSheetSymbols SET OverTimeSymbol = -1, IsHalfDayDefault = 0 WHERE ID = 'ed2bb8bd-9f49-4332-9e21-244f3b665c57'
UPDATE dbo.TimeSheetSymbols SET IsHalfDayDefault = 0 WHERE ID = 'a85bbb53-094a-4891-94f8-2e6dbd3da02a'
UPDATE dbo.TimeSheetSymbols SET IsHalfDayDefault = 0 WHERE ID = 'd31eb567-fa69-4d76-b4bb-3fe79a7b9faa'
UPDATE dbo.TimeSheetSymbols SET IsHalfDayDefault = 0 WHERE ID = '49f691a7-16f0-4bc9-99a7-420738acc241'
UPDATE dbo.TimeSheetSymbols SET IsHalfDayDefault = 0 WHERE ID = 'b809cddd-4785-45d1-b0d4-5dcdd62c1701'
UPDATE dbo.TimeSheetSymbols SET IsHalfDayDefault = 0 WHERE ID = 'e8926ef4-35f4-4dcc-878e-70068739cac9'
UPDATE dbo.TimeSheetSymbols SET IsHalfDayDefault = 0 WHERE ID = '91e7f7de-0a7e-4a19-9528-89848fa7cdfa'
UPDATE dbo.TimeSheetSymbols SET IsHalfDayDefault = 0 WHERE ID = 'd5cce21b-f81e-49e7-81ce-9ef3adca908d'
UPDATE dbo.TimeSheetSymbols SET IsHalfDayDefault = 0 WHERE ID = '7070de97-02fd-46ee-909c-a391a4a91b5d'
UPDATE dbo.TimeSheetSymbols SET IsHalfDayDefault = 0 WHERE ID = '5a54846b-d985-4e73-80fa-aa33d02a2b83'
UPDATE dbo.TimeSheetSymbols SET IsHalfDayDefault = 0 WHERE ID = 'ad52600b-6deb-4734-9dc5-b432a141882d'
UPDATE dbo.TimeSheetSymbols SET IsHalfDayDefault = 1 WHERE ID = '18c1a1bc-d0ab-44b8-a133-d226839bec7e'
UPDATE dbo.TimeSheetSymbols SET IsHalfDayDefault = 0 WHERE ID = 'ff8193eb-8792-4c40-9996-d58bc9463bbc'
UPDATE dbo.TimeSheetSymbols SET IsHalfDayDefault = 0 WHERE ID = 'f7c73a86-9fc9-4228-a464-e64ed369fe50'

INSERT dbo.TimeSheetSymbols(ID, TimeSheetSymbolsCode, TimeSheetSymbolsName, SalaryRate, IsDefault, IsOverTime, IsActive, IsSecurity, IsHalfDay, OverTimeSymbol, IsHalfDayDefault) VALUES ('36dbcc89-9767-4ea1-8888-3e541de61b07', N'LĐ1/2', N'Lao động nghĩa vụ (nửa ngày)', 50.0000000000, 0, NULL, 1, 0, 1, -2, 0)
INSERT dbo.TimeSheetSymbols(ID, TimeSheetSymbolsCode, TimeSheetSymbolsName, SalaryRate, IsDefault, IsOverTime, IsActive, IsSecurity, IsHalfDay, OverTimeSymbol, IsHalfDayDefault) VALUES ('d5f9ff06-ee35-4990-a983-46981ff20f4b', N'NP1/2', N'Nghỉ phép (nửa ngày)', 100.0000000000, 0, NULL, 1, 0, 1, -2, 0)
INSERT dbo.TimeSheetSymbols(ID, TimeSheetSymbolsCode, TimeSheetSymbolsName, SalaryRate, IsDefault, IsOverTime, IsActive, IsSecurity, IsHalfDay, OverTimeSymbol, IsHalfDayDefault) VALUES ('f7ac13bc-d8e9-44fc-b07a-6d9386d72895', N'NL1/2', N'Nghỉ lễ (nửa ngày)', 100.0000000000, 0, NULL, 1, 0, 1, -2, 0)
INSERT dbo.TimeSheetSymbols(ID, TimeSheetSymbolsCode, TimeSheetSymbolsName, SalaryRate, IsDefault, IsOverTime, IsActive, IsSecurity, IsHalfDay, OverTimeSymbol, IsHalfDayDefault) VALUES ('490ed373-c881-4090-a937-70bdbda282ea', N'KL1/2', N'Không lương (nửa ngày)', 0.0000000000, 0, NULL, 1, 0, 1, -2, 0)
INSERT dbo.TimeSheetSymbols(ID, TimeSheetSymbolsCode, TimeSheetSymbolsName, SalaryRate, IsDefault, IsOverTime, IsActive, IsSecurity, IsHalfDay, OverTimeSymbol, IsHalfDayDefault) VALUES ('eac5f0d9-c31e-45e7-9b6f-7fd7c7b30edc', N'Ô1/2', N'Ốm (nửa ngày)', 50.0000000000, 0, NULL, 1, 0, 1, -2, 0)
INSERT dbo.TimeSheetSymbols(ID, TimeSheetSymbolsCode, TimeSheetSymbolsName, SalaryRate, IsDefault, IsOverTime, IsActive, IsSecurity, IsHalfDay, OverTimeSymbol, IsHalfDayDefault) VALUES ('4640b1a1-b8d2-4462-bc38-e90af40ceec0', N'N1/2', N'Ngừng việc (nửa ngày)', 0.0000000000, 0, NULL, 1, 0, 1, -2, 0)
INSERT dbo.TimeSheetSymbols(ID, TimeSheetSymbolsCode, TimeSheetSymbolsName, SalaryRate, IsDefault, IsOverTime, IsActive, IsSecurity, IsHalfDay, OverTimeSymbol, IsHalfDayDefault) VALUES ('930f9e6f-1ad4-4e5d-baef-f1a216393744', N'TS1/2', N'Thai sản (nửa ngày)', 100.0000000000, 0, NULL, 1, 0, 1, -2, 0)
INSERT dbo.TimeSheetSymbols(ID, TimeSheetSymbolsCode, TimeSheetSymbolsName, SalaryRate, IsDefault, IsOverTime, IsActive, IsSecurity, IsHalfDay, OverTimeSymbol, IsHalfDayDefault) VALUES ('da3e9444-9ba2-4581-83f8-fc66feaa210c', N'CO1/2', N'Con ốm (nửa ngày)', 75.0000000000, 0, NULL, 1, 0, 1, -2, 0)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.VoucherPatternsReport SET ListTypeID = N'116' WHERE ID = 172
UPDATE dbo.VoucherPatternsReport SET ListTypeID = N'600,690,602,620,601,660,670' WHERE ID = 173
UPDATE dbo.VoucherPatternsReport SET ListTypeID = N'116' WHERE ID = 259
Update AccountingObjectBankAccount set IsSelect = 1 where ID in (select a.ID from  AccountingObjectBankAccount a join AccountingObject b on a.AccountingObjectID = b.ID and a.BankAccount = b.BankAccount);
Update AccountingObjectBankAccount set IsSelect = 0 where ID not in (select a.ID from  AccountingObjectBankAccount a join AccountingObject b on a.AccountingObjectID = b.ID and a.BankAccount = b.BankAccount);
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.Template WITH NOCHECK
  ADD CONSTRAINT FK_Template_Type FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE CASCADE ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateColumn WITH NOCHECK
  ADD CONSTRAINT FK_TemplateColumn_TemplateDetail FOREIGN KEY (TemplateDetailID) REFERENCES dbo.TemplateDetail(ID) ON DELETE CASCADE ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateDetail WITH NOCHECK
  ADD CONSTRAINT FK_TemplateDetail_Template FOREIGN KEY (TemplateID) REFERENCES dbo.Template(ID) ON DELETE CASCADE ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

IF @@TRANCOUNT>0 COMMIT TRANSACTION
GO

SET NOEXEC OFF