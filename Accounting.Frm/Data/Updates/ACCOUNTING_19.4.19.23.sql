SET CONCAT_NULL_YIELDS_NULL, ANSI_NULLS, ANSI_PADDING, QUOTED_IDENTIFIER, ANSI_WARNINGS, ARITHABORT, XACT_ABORT ON
SET NUMERIC_ROUNDABORT, IMPLICIT_TRANSACTIONS OFF
GO

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

BEGIN TRANSACTION
GO

CREATE TABLE [dbo].[TMIndustryType] (
  [ID] [uniqueidentifier] NOT NULL,
  [IndustryTypeCode] [nvarchar](25) NULL,
  [IndustryTypeName] [nvarchar](512) NULL,
  PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[TMDeclaration] (
  [ID] [uniqueidentifier] NOT NULL,
  [BranchID] [uniqueidentifier] NULL,
  [TypeID] [uniqueidentifier] NOT NULL,
  [DeclarationName] [nvarchar](512) NULL,
  [DeclarationTerm] [nvarchar](512) NULL,
  [FromDate] [datetime] NULL,
  [ToDate] [datetime] NULL,
  [IsFirstDeclaration] [bit] NULL,
  [AdditionTime] [int] NULL,
  PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[TMAppendixList] (
  [ID] [uniqueidentifier] NOT NULL,
  [TypeID] [int] NULL,
  [AppendixCode] [nvarchar](25) NULL,
  [AppendixName] [nvarchar](512) NULL,
  [TypeGroup] [int] NULL,
  PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[TM03TNDNDocument] (
  [ID] [uniqueidentifier] NOT NULL,
  [TM03TNDNID] [uniqueidentifier] NULL,
  [DocumentName] [nvarchar](512) NULL,
  [OrderPriority] [int] NULL,
  PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[TM03TNDNDetail] (
  [ID] [uniqueidentifier] NOT NULL,
  [TM03TNDNID] [uniqueidentifier] NULL,
  [Code] [nvarchar](512) NULL,
  [Name] [nvarchar](512) NULL,
  [Amount] [money] NULL,
  [OrderPriority] [int] NULL,
  PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[TM03TNDNAdjust] (
  [ID] [uniqueidentifier] NOT NULL,
  [TM03TNDNID] [uniqueidentifier] NULL,
  [Code] [nvarchar](512) NULL,
  [Name] [nvarchar](512) NULL,
  [DeclaredAmount] [money] NULL,
  [AdjustAmount] [money] NULL,
  [DifferAmount] [money] NULL,
  [LateDays] [int] NULL,
  [LateAmount] [money] NULL,
  [ExplainAmount] [money] NULL,
  [CommandNo] [nvarchar](512) NULL,
  [CommandDate] [datetime] NULL,
  [TaxCompanyName] [nvarchar](512) NULL,
  [TaxCompanyDecisionName] [nvarchar](512) NULL,
  [ReceiveDays] [int] NULL,
  [ExplainLateAmount] [money] NULL,
  [DifferReason] [nvarchar](2048) NULL,
  [OrderPriority] [int] IDENTITY,
  PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[TM03TNDN] (
  [ID] [uniqueidentifier] NOT NULL,
  [BranchID] [uniqueidentifier] NULL,
  [TypeID] [int] NOT NULL,
  [DeclarationName] [nvarchar](512) NULL,
  [DeclarationTerm] [nvarchar](512) NULL,
  [FromDate] [datetime] NULL,
  [ToDate] [datetime] NULL,
  [IsFirstDeclaration] [bit] NULL,
  [AdditionTime] [int] NULL,
  [AdditionDate] [datetime] NULL,
  [Career] [int] NULL,
  [IsAppendix031ATNDN] [bit] NULL,
  [IsAppendix032ATNDN] [bit] NULL,
  [IsSMEEnterprise] [bit] NULL,
  [IsDependentEnterprise] [bit] NULL,
  [IsRelatedTransactionEnterprise] [bit] NULL,
  [TMIndustryTypeID] [uniqueidentifier] NULL,
  [Rate] [decimal](25, 10) NULL,
  [CompanyName] [nvarchar](512) NULL,
  [CompanyTaxCode] [nvarchar](50) NULL,
  [TaxAgencyTaxCode] [nvarchar](50) NULL,
  [TaxAgencyName] [nvarchar](512) NULL,
  [TaxAgencyEmployeeName] [nvarchar](512) NULL,
  [CertificationNo] [nvarchar](512) NULL,
  [SignName] [nvarchar](512) NULL,
  [SignDate] [datetime] NULL,
  [IsExtend] [bit] NULL,
  [ExtensionCase] [int] NULL,
  [ExtendTime] [datetime] NULL,
  [ExtendTaxAmount] [money] NULL,
  [NotExtendTaxAmount] [money] NULL,
  [LateDays] [int] NULL,
  [FromDateLate] [datetime] NULL,
  [ToDateLate] [datetime] NULL,
  [LateAmount] [money] NULL,
  [Year] [int] NULL,
  PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[TM031ATNDN] (
  [ID] [uniqueidentifier] NOT NULL,
  [TM03TNDNID] [uniqueidentifier] NULL,
  [Code] [nvarchar](512) NULL,
  [Name] [nvarchar](512) NULL,
  [Data] [nvarchar](512) NULL,
  [OrderPriority] [int] NULL,
  PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[TM02TAINDetail] (
  [ID] [uniqueidentifier] NOT NULL,
  [TM02TAINID] [uniqueidentifier] NULL,
  [Type] [int] NULL,
  [MaterialGoodsResourceTaxGroupID] [uniqueidentifier] NULL,
  [MaterialGoodsResourceTaxGroupName] [nvarchar](512) NULL,
  [Unit] [nvarchar](25) NULL,
  [Quantity] [decimal](25, 10) NULL,
  [UnitPrice] [money] NULL,
  [TaxRate] [decimal](25, 10) NULL,
  [ResourceTaxTaxAmountUnit] [decimal](25, 10) NULL,
  [ResourceTaxAmountIncurration] [money] NULL,
  [ResourceTaxAmountDeduction] [money] NULL,
  [ResourceTaxAmount] [money] NULL,
  [ResourceTaxAmountDeclaration] [money] NULL,
  [DifferAmount] [money] NULL,
  [OrderPriority] [int] NULL,
  PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[TM02TAINAdjust] (
  [ID] [uniqueidentifier] NOT NULL,
  [TM02TAINID] [uniqueidentifier] NULL,
  [Code] [nvarchar](512) NULL,
  [Name] [nvarchar](512) NULL,
  [DeclaredAmount] [money] NULL,
  [AdjustAmount] [money] NULL,
  [DifferAmount] [money] NULL,
  [LateDays] [int] NULL,
  [LateAmount] [money] NULL,
  [ExplainAmount] [money] NULL,
  [CommandNo] [nvarchar](512) NULL,
  [CommandDate] [datetime] NULL,
  [TaxCompanyName] [nvarchar](512) NULL,
  [TaxCompanyDecisionName] [nvarchar](512) NULL,
  [ReceiveDays] [int] NULL,
  [ExplainLateAmount] [money] NULL,
  [DifferReason] [nvarchar](2048) NULL,
  [OrderPriority] [int] IDENTITY,
  PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[TM02TAIN] (
  [ID] [uniqueidentifier] NOT NULL,
  [BranchID] [uniqueidentifier] NULL,
  [TypeID] [int] NOT NULL,
  [DeclarationName] [nvarchar](512) NULL,
  [DeclarationTerm] [nvarchar](512) NULL,
  [FromDate] [datetime] NULL,
  [ToDate] [datetime] NULL,
  [IsFirstDeclaration] [bit] NULL,
  [AdditionTime] [int] NULL,
  [AdditionDate] [datetime] NULL,
  [CompanyName] [nvarchar](512) NULL,
  [CompanyTaxCode] [nvarchar](50) NULL,
  [TaxAgencyTaxCode] [nvarchar](50) NULL,
  [TaxAgencyName] [nvarchar](512) NULL,
  [TaxAgencyEmployeeName] [nvarchar](512) NULL,
  [CertificationNo] [nvarchar](512) NULL,
  [SignName] [nvarchar](512) NULL,
  [SignDate] [datetime] NULL,
  PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[TM02GTGTDetail] (
  [ID] [uniqueidentifier] NOT NULL,
  [TM02GTGTID] [uniqueidentifier] NULL,
  [Code] [nvarchar](512) NULL,
  [Name] [nvarchar](512) NULL,
  [Data] [nvarchar](512) NULL,
  [OrderPriority] [int] NULL,
  PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[TM02GTGTAdjust] (
  [ID] [uniqueidentifier] NOT NULL,
  [TM02GTGTID] [uniqueidentifier] NULL,
  [Code] [nvarchar](512) NULL,
  [Name] [nvarchar](512) NULL,
  [DeclaredAmount] [money] NULL,
  [AdjustAmount] [money] NULL,
  [DifferAmount] [money] NULL,
  [LateDays] [int] NULL,
  [LateAmount] [money] NULL,
  [ExplainAmount] [money] NULL,
  [CommandNo] [nvarchar](512) NULL,
  [CommandDate] [datetime] NULL,
  [TaxCompanyName] [nvarchar](512) NULL,
  [TaxCompanyDecisionName] [nvarchar](512) NULL,
  [ReceiveDays] [int] NULL,
  [ExplainLateAmount] [money] NULL,
  [DifferReason] [nvarchar](2048) NULL,
  [OrderPriority] [int] IDENTITY,
  PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[TM02GTGT] (
  [ID] [uniqueidentifier] NOT NULL,
  [BranchID] [uniqueidentifier] NULL,
  [TypeID] [int] NOT NULL,
  [DeclarationName] [nvarchar](512) NULL,
  [DeclarationTerm] [nvarchar](512) NULL,
  [FromDate] [datetime] NULL,
  [ToDate] [datetime] NULL,
  [IsFirstDeclaration] [bit] NULL,
  [AdditionTime] [int] NULL,
  [AdditionDate] [datetime] NULL,
  [CompanyName] [nvarchar](512) NULL,
  [CompanyTaxCode] [nvarchar](50) NULL,
  [TaxAgencyTaxCode] [nvarchar](50) NULL,
  [TaxAgencyName] [nvarchar](512) NULL,
  [TaxAgencyEmployeeName] [nvarchar](512) NULL,
  [CertificationNo] [nvarchar](512) NULL,
  [SignName] [nvarchar](512) NULL,
  [SignDate] [datetime] NULL,
  PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[TM01TAINDetail] (
  [ID] [uniqueidentifier] NOT NULL,
  [TM01TAINID] [uniqueidentifier] NULL,
  [Type] [int] NULL,
  [MaterialGoodsResourceTaxGroupID] [uniqueidentifier] NULL,
  [MaterialGoodsResourceTaxGroupName] [nvarchar](512) NULL,
  [Unit] [nvarchar](25) NULL,
  [Quantity] [decimal](25, 10) NULL,
  [UnitPrice] [money] NULL,
  [TaxRate] [decimal](25, 10) NULL,
  [ResourceTaxTaxAmountUnit] [decimal](25, 10) NULL,
  [ResourceTaxAmountIncurration] [money] NULL,
  [ResourceTaxAmountDeduction] [money] NULL,
  [ResourceTaxAmount] [money] NULL,
  [OrderPriority] [int] IDENTITY,
  PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[TM01TAINAdjust] (
  [ID] [uniqueidentifier] NOT NULL,
  [TM01TAINID] [uniqueidentifier] NULL,
  [Code] [nvarchar](512) NULL,
  [Name] [nvarchar](512) NULL,
  [DeclaredAmount] [money] NULL,
  [AdjustAmount] [money] NULL,
  [DifferAmount] [money] NULL,
  [LateDays] [int] NULL,
  [LateAmount] [money] NULL,
  [ExplainAmount] [money] NULL,
  [CommandNo] [nvarchar](512) NULL,
  [CommandDate] [datetime] NULL,
  [TaxCompanyName] [nvarchar](512) NULL,
  [TaxCompanyDecisionName] [nvarchar](512) NULL,
  [ReceiveDays] [int] NULL,
  [ExplainLateAmount] [money] NULL,
  [DifferReason] [nvarchar](2048) NULL,
  [OrderPriority] [int] IDENTITY,
  PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[TM01TAIN] (
  [ID] [uniqueidentifier] NOT NULL,
  [BranchID] [uniqueidentifier] NULL,
  [TypeID] [int] NOT NULL,
  [DeclarationName] [nvarchar](512) NULL,
  [DeclarationTerm] [nvarchar](512) NULL,
  [FromDate] [datetime] NULL,
  [ToDate] [datetime] NULL,
  [IsFirstDeclaration] [bit] NULL,
  [AdditionTime] [int] NULL,
  [AdditionDate] [datetime] NULL,
  [CompanyName] [nvarchar](512) NULL,
  [CompanyTaxCode] [nvarchar](50) NULL,
  [TaxAgencyTaxCode] [nvarchar](50) NULL,
  [TaxAgencyName] [nvarchar](512) NULL,
  [TaxAgencyEmployeeName] [nvarchar](512) NULL,
  [CertificationNo] [nvarchar](512) NULL,
  [SignName] [nvarchar](512) NULL,
  [SignDate] [datetime] NULL,
  [isMonth] [bit] NULL,
  [isDeclaredArising] [bit] NULL,
  [Day] [int] NULL,
  [Month] [int] NULL,
  [Year] [int] NULL,
  PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @value SQL_VARIANT = CAST('' AS varchar(1))
EXEC sys.sp_addextendedproperty N'MS_Description', @value, 'SCHEMA', N'dbo', 'TABLE', N'TM01TAIN', 'COLUMN', N'isMonth'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @value SQL_VARIANT = CAST('' AS varchar(1))
EXEC sys.sp_addextendedproperty N'MS_Description', @value, 'SCHEMA', N'dbo', 'TABLE', N'TM01TAIN', 'COLUMN', N'isDeclaredArising'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @value SQL_VARIANT = CAST('' AS varchar(1))
EXEC sys.sp_addextendedproperty N'MS_Description', @value, 'SCHEMA', N'dbo', 'TABLE', N'TM01TAIN', 'COLUMN', N'Day'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @value SQL_VARIANT = CAST('' AS varchar(1))
EXEC sys.sp_addextendedproperty N'MS_Description', @value, 'SCHEMA', N'dbo', 'TABLE', N'TM01TAIN', 'COLUMN', N'Month'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @value SQL_VARIANT = CAST('' AS varchar(1))
EXEC sys.sp_addextendedproperty N'MS_Description', @value, 'SCHEMA', N'dbo', 'TABLE', N'TM01TAIN', 'COLUMN', N'Year'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[TM01GTGTDetail] (
  [ID] [uniqueidentifier] NOT NULL,
  [TM01GTGTID] [uniqueidentifier] NULL,
  [Code] [nvarchar](512) NULL,
  [Name] [nvarchar](512) NULL,
  [Data] [nvarchar](512) NULL,
  [OrderPriority] [int] IDENTITY,
  PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[TM01GTGTAdjust] (
  [ID] [uniqueidentifier] NOT NULL,
  [TM01GTGTID] [uniqueidentifier] NULL,
  [Code] [nvarchar](512) NULL,
  [Name] [nvarchar](512) NULL,
  [DeclaredAmount] [money] NULL,
  [AdjustAmount] [money] NULL,
  [DifferAmount] [money] NULL,
  [LateDays] [int] NULL,
  [LateAmount] [money] NULL,
  [ExplainAmount] [money] NULL,
  [CommandNo] [nvarchar](512) NULL,
  [CommandDate] [datetime] NULL,
  [TaxCompanyName] [nvarchar](512) NULL,
  [TaxCompanyDecisionName] [nvarchar](512) NULL,
  [ReceiveDays] [int] NULL,
  [ExplainLateAmount] [money] NULL,
  [DifferReason] [nvarchar](2048) NULL,
  [OrderPriority] [int] IDENTITY,
  [Type] [int] NULL,
  PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @value SQL_VARIANT = CAST('' AS varchar(1))
EXEC sys.sp_addextendedproperty N'MS_Description', @value, 'SCHEMA', N'dbo', 'TABLE', N'TM01GTGTAdjust', 'COLUMN', N'Type'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[TM01GTGT] (
  [ID] [uniqueidentifier] NOT NULL,
  [BranchID] [uniqueidentifier] NULL,
  [TypeID] [int] NOT NULL,
  [DeclarationName] [nvarchar](512) NULL,
  [DeclarationTerm] [nvarchar](512) NULL,
  [FromDate] [datetime] NULL,
  [ToDate] [datetime] NULL,
  [IsFirstDeclaration] [bit] NULL,
  [AdditionTime] [int] NULL,
  [AdditionDate] [datetime] NULL,
  [Career] [int] NULL,
  [IsExtend] [bit] NULL,
  [ExtensionCase] [int] NULL,
  [IsAppendix011GTGT] [bit] NULL,
  [IsAppendix012GTGT] [bit] NULL,
  [CompanyName] [nvarchar](512) NULL,
  [CompanyTaxCode] [nvarchar](50) NULL,
  [TaxAgencyTaxCode] [nvarchar](50) NULL,
  [TaxAgencyName] [nvarchar](512) NULL,
  [TaxAgencyEmployeeName] [nvarchar](512) NULL,
  [CertificationNo] [nvarchar](512) NULL,
  [SignName] [nvarchar](512) NULL,
  [SignDate] [datetime] NULL,
  PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[TM012GTGT] (
  [ID] [uniqueidentifier] NOT NULL,
  [TM01GTGTID] [uniqueidentifier] NOT NULL,
  [VoucherID] [uniqueidentifier] NULL,
  [VoucherDetailID] [uniqueidentifier] NULL,
  [InvoiceDate] [datetime] NULL,
  [InvoiceNo] [nvarchar](25) NULL,
  [InvoiceSeries] [nvarchar](25) NULL,
  [AccountingObjectID] [uniqueidentifier] NULL,
  [AccountingObjectName] [nvarchar](512) NULL,
  [TaxCode] [nvarchar](50) NULL,
  [PretaxAmount] [money] NOT NULL,
  [TaxAmount] [money] NULL,
  [Note] [nvarchar](512) NULL,
  [OrderPriority] [int] IDENTITY,
  [Type] [int] NULL,
  PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @value SQL_VARIANT = CAST('' AS varchar(1))
EXEC sys.sp_addextendedproperty N'MS_Description', @value, 'SCHEMA', N'dbo', 'TABLE', N'TM012GTGT', 'COLUMN', N'Type'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[TM011GTGT] (
  [ID] [uniqueidentifier] NOT NULL,
  [TM01GTGTID] [uniqueidentifier] NOT NULL,
  [VoucherID] [uniqueidentifier] NULL,
  [VoucherDetailID] [uniqueidentifier] NULL,
  [InvoiceDate] [datetime] NULL,
  [InvoiceNo] [nvarchar](25) NULL,
  [InvoiceSeries] [nvarchar](25) NULL,
  [AccountingObjectID] [uniqueidentifier] NULL,
  [AccountingObjectName] [nvarchar](512) NULL,
  [TaxCode] [nvarchar](50) NULL,
  [PretaxAmount] [money] NOT NULL,
  [VATRate] [decimal](25) NULL,
  [TaxAmount] [money] NULL,
  [Note] [nvarchar](512) NULL,
  [OrderPriority] [int] IDENTITY,
  PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[GOtherVoucherDetailForeignCurrency] (
  [ID] [uniqueidentifier] NOT NULL,
  [GOtherVoucherID] [uniqueidentifier] NULL,
  [AccountNumber] [nvarchar](25) NULL,
  [BankAccountDetailID] [uniqueidentifier] NULL,
  [AccountingObjectID] [uniqueidentifier] NULL,
  [DebitAmountOriginal] [money] NULL,
  [DebitAmount] [money] NULL,
  [DebitReevaluate] [money] NULL,
  [DebitAmountDiffer] [money] NULL,
  [CreditAmountOriginal] [money] NULL,
  [CreditAmount] [money] NULL,
  [CreditReevaluate] [money] NULL,
  [CreditAmountDiffer] [money] NULL,
  [OrderPriority] [int] NULL,
  CONSTRAINT [GOtherVoucherDetailForeignCurrency_pk] PRIMARY KEY NONCLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE UNIQUE INDEX [GOtherVoucherDetailForeignCurrency_ID_uindex]
  ON [dbo].[GOtherVoucherDetailForeignCurrency] ([ID])
  ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[GOtherVoucherDetailDebtPayment] (
  [ID] [uniqueidentifier] NOT NULL,
  [GOtherVoucherID] [uniqueidentifier] NOT NULL,
  [AccountNumber] [nvarchar](25) NULL,
  [RefID] [uniqueidentifier] NULL,
  [RefVoucherDate] [datetime] NULL,
  [RefVoucherNo] [nvarchar](25) NULL,
  [AccountingObjectID] [uniqueidentifier] NULL,
  [Reason] [nvarchar](512) NULL,
  [RefVoucherExchangeRate] [decimal](25, 10) NULL,
  [LastExchangeRate] [decimal](25, 10) NULL,
  [RemainingAmountOriginal] [money] NULL,
  [RemainingAmount] [money] NULL,
  [RevaluedAmount] [money] NULL,
  [DifferAmount] [money] NULL,
  CONSTRAINT [GOtherVoucherDetailDebtPayment_pk] PRIMARY KEY NONCLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [CashOutExchangeRate] [decimal](25, 10) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [CashOutAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [CashOutDifferAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [CashOutDifferAccount] [nvarchar](25) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [CashOutVATAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [CashOutDifferVATAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAReturnDetail]
  ADD [CashOutExchangeRate] [decimal](25, 10) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAReturnDetail]
  ADD [CashOutAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAReturnDetail]
  ADD [CashOutDifferAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAReturnDetail]
  ADD [CashOutDifferAccount] [nvarchar](25) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAReturnDetail]
  ADD [CashOutVATAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAReturnDetail]
  ADD [CashOutDifferVATAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAReturn]
  ADD [ID_MIV] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAInvoice]
  ADD [ID_MIV] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SABill]
  ADD [ID_MIV] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SABill]
  ADD [DocumentNo] [nvarchar](50) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SABill]
  ADD [DocumentDate] [datetime] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SABill]
  ADD [DocumentNote] [nvarchar](512) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SABill]
  ADD [ID_MIVAdjust] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PPServiceDetail]
  ADD [CashOutExchangeRate] [decimal](25, 10) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PPServiceDetail]
  ADD [CashOutAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PPServiceDetail]
  ADD [CashOutDifferAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PPServiceDetail]
  ADD [CashOutDifferAccount] [nvarchar](25) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PPServiceDetail]
  ADD [CashOutVATAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PPServiceDetail]
  ADD [CashOutDifferVATAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PPInvoiceDetail]
  ADD [CashOutExchangeRate] [decimal](25, 10) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PPInvoiceDetail]
  ADD [CashOutAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PPInvoiceDetail]
  ADD [CashOutDifferAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PPInvoiceDetail]
  ADD [CashOutDifferAccount] [nvarchar](25) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PPInvoiceDetail]
  ADD [CashOutVATAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PPInvoiceDetail]
  ADD [CashOutDifferVATAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PPDiscountReturn]
  ADD [ID_MIV] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MCReceiptDetailCustomer]
  ADD [RefVoucherExchangeRate] [decimal](25, 10) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MCReceiptDetailCustomer]
  ADD [LastExchangeRate] [decimal](25, 10) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MCReceiptDetailCustomer]
  ADD [DifferAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MCPaymentDetailVendor]
  ADD [CashOutExchangeRate] [decimal](25, 10) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MCPaymentDetailVendor]
  ADD [CashOutAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MCPaymentDetailVendor]
  ADD [CashOutDifferAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MCPaymentDetailVendor]
  ADD [CashOutDifferAccount] [nvarchar](25) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MCPaymentDetailVendor]
  ADD [RefVoucherExchangeRate] [decimal](25, 10) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MCPaymentDetailVendor]
  ADD [LastExchangeRate] [decimal](25, 10) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MCPaymentDetailVendor]
  ADD [DifferAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MCPaymentDetail]
  ADD [CashOutExchangeRate] [decimal](25, 10) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MCPaymentDetail]
  ADD [CashOutAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MCPaymentDetail]
  ADD [CashOutDifferAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MCPaymentDetail]
  ADD [CashOutDifferAccount] [nvarchar](25) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MCPaymentDetail]
  ADD [TaxAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MBTellerPaperDetailVendor]
  ADD [CashOutExchangeRate] [decimal](25, 10) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MBTellerPaperDetailVendor]
  ADD [CashOutAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MBTellerPaperDetailVendor]
  ADD [CashOutDifferAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MBTellerPaperDetailVendor]
  ADD [CashOutDifferAccount] [nvarchar](25) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MBTellerPaperDetailVendor]
  ADD [RefVoucherExchangeRate] [decimal](25, 10) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MBTellerPaperDetailVendor]
  ADD [LastExchangeRate] [decimal](25, 10) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MBTellerPaperDetailVendor]
  ADD [DifferAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MBTellerPaperDetail]
  ADD [CashOutExchangeRate] [decimal](25, 10) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MBTellerPaperDetail]
  ADD [CashOutAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MBTellerPaperDetail]
  ADD [CashOutDifferAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MBTellerPaperDetail]
  ADD [CashOutDifferAccount] [nvarchar](25) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MBTellerPaperDetail]
  ADD [TaxAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MBInternalTransferDetail]
  ADD [CashOutExchangeRate] [decimal](25, 10) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MBInternalTransferDetail]
  ADD [CashOutAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MBInternalTransferDetail]
  ADD [CashOutDifferAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MBInternalTransferDetail]
  ADD [CashOutDifferAccount] [nvarchar](25) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MBInternalTransfer]
  ADD [CurrencyID] [nvarchar](3) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MBInternalTransfer]
  ADD [ExchangeRate] [decimal] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MBDepositDetailCustomer]
  ADD [RefVoucherExchangeRate] [decimal](25, 10) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MBDepositDetailCustomer]
  ADD [LastExchangeRate] [decimal](25, 10) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MBDepositDetailCustomer]
  ADD [DifferAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MBCreditCardDetailVendor]
  ADD [CashOutExchangeRate] [decimal](25, 10) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MBCreditCardDetailVendor]
  ADD [CashOutAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MBCreditCardDetailVendor]
  ADD [CashOutDifferAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MBCreditCardDetailVendor]
  ADD [CashOutDifferAccount] [nvarchar](25) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MBCreditCardDetailVendor]
  ADD [RefVoucherExchangeRate] [decimal](25, 10) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MBCreditCardDetailVendor]
  ADD [LastExchangeRate] [decimal](25, 10) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MBCreditCardDetailVendor]
  ADD [DifferAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MBCreditCardDetail]
  ADD [CashOutExchangeRate] [decimal](25, 10) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MBCreditCardDetail]
  ADD [CashOutAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MBCreditCardDetail]
  ADD [CashOutDifferAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MBCreditCardDetail]
  ADD [CashOutDifferAccount] [nvarchar](25) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[GOtherVoucherDetail]
  ADD [CashOutExchangeRate] [decimal](25, 10) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[GOtherVoucherDetail]
  ADD [CashOutAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[GOtherVoucherDetail]
  ADD [CashOutDifferAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[GOtherVoucherDetail]
  ADD [CashOutDifferAccount] [nvarchar](25) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[GOtherVoucherDetail]
  ADD [CashOutAmountVoucherOriginal] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[GOtherVoucherDetail]
  ADD [CashOutAmountVoucher] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[GOtherVoucher]
  ADD [CurrencyID] [nvarchar](3) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[GOtherVoucher]
  ADD [ExchangeRate] [decimal] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sp_refreshview '[dbo].[ViewVoucherInvisible]'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
CREATE PROCEDURE [dbo].[Proc_TAX_ListTaxSubmit]
    @inputDate DATETIME
AS
BEGIN          
    DECLARE @tbluutru TABLE (
		icheck BIT,
		tkThue NVARCHAR(25),
		dienGiai NVARCHAR(512),
		soTienPhaiNop money,
		soTienNopLanNay money,
		soTienDaNop money
    )
    
    DECLARE @icheck BIT
    SET @icheck = 0
    
    DECLARE @ThueGTGTDauRa money
    set @ThueGTGTDauRa = (select SUM(GL.CreditAmount - GL.DebitAmount) from dbo.GeneralLedger GL where GL.Account like '33311%' and GL.PostedDate <= @inputDate)
    INSERT INTO @tbluutru(icheck, tkThue, dienGiai, soTienPhaiNop)
    VALUES(@icheck, N'33311', N'Thuế GTGT đầu ra', @ThueGTGTDauRa)
    
    DECLARE @ThueGTGTHangNK money
    set @ThueGTGTHangNK = (select SUM(GL.CreditAmount - GL.DebitAmount) from dbo.GeneralLedger GL where GL.Account like '33312%' 
    and (GL.AccountCorresponding in ('1331', '1332')) and GL.PostedDate <= @inputDate)
    INSERT INTO @tbluutru(icheck, tkThue, dienGiai, soTienPhaiNop)
    VALUES(@icheck, N'33312', N'Thuế GTGT hàng nhập khẩu', @ThueGTGTHangNK)
    
    DECLARE @ThueGTGTHangNKDaNop money
    set @ThueGTGTHangNKDaNop = (select SUM(GL.DebitAmount) from dbo.GeneralLedger GL where GL.Account like '33312%' 
    and ((GL.AccountCorresponding like '111%') OR  (GL.AccountCorresponding like '112%')) and GL.PostedDate <= @inputDate)
    
    UPDATE @tbluutru SET soTienPhaiNop = ISNULL(soTienPhaiNop,0) - ISNULL(@ThueGTGTHangNKDaNop,0) WHERE tkThue like '33312%'
    
    DECLARE @ThueTieuThuDB money
    set @ThueTieuThuDB = (select SUM(GL.CreditAmount - GL.DebitAmount) from dbo.GeneralLedger GL where GL.Account like '3332%' and GL.PostedDate <= @inputDate)
    INSERT INTO @tbluutru(icheck, tkThue, dienGiai, soTienPhaiNop)
    VALUES(@icheck, N'3332', N'Thuế tiêu thụ đặc biệt', @ThueTieuThuDB)
    
    DECLARE @ThueXuatNhapKhau money
    set @ThueXuatNhapKhau = (select SUM(GL.CreditAmount - GL.DebitAmount) from dbo.GeneralLedger GL where GL.Account like '3333%' and GL.PostedDate <= @inputDate)
    INSERT INTO @tbluutru(icheck, tkThue, dienGiai, soTienPhaiNop)
    VALUES(@icheck, N'3333', N'Thuế xuất nhập khẩu', @ThueXuatNhapKhau)
    
    DECLARE @ThueThuNhapDN money
    set @ThueThuNhapDN = (select SUM(GL.CreditAmount - GL.DebitAmount) from dbo.GeneralLedger GL where GL.Account like '3334%' and GL.PostedDate <= @inputDate)
    INSERT INTO @tbluutru(icheck, tkThue, dienGiai, soTienPhaiNop)
    VALUES(@icheck, N'3334', N'Thuế thu nhập doanh nghiệp', @ThueThuNhapDN)
    
    DECLARE @ThueThuNhapCN money
    set @ThueThuNhapCN = (select SUM(GL.CreditAmount - GL.DebitAmount) from dbo.GeneralLedger GL where GL.Account like '3335%' and GL.PostedDate <= @inputDate)
    INSERT INTO @tbluutru(icheck, tkThue, dienGiai, soTienPhaiNop)
    VALUES(@icheck, N'3335', N'Thuế thu nhập cá nhân', @ThueThuNhapCN)
    
    DECLARE @ThueTaiNguyen money
    set @ThueTaiNguyen = (select SUM(GL.CreditAmount - GL.DebitAmount) from dbo.GeneralLedger GL where GL.Account like '3336%' and GL.PostedDate <= @inputDate)
    INSERT INTO @tbluutru(icheck, tkThue, dienGiai, soTienPhaiNop)
    VALUES(@icheck, N'3336', N'Thuế tài nguyên', @ThueTaiNguyen)
    
    DECLARE @ThueNhaDat money
    set @ThueNhaDat = (select SUM(GL.CreditAmount - GL.DebitAmount) from dbo.GeneralLedger GL where GL.Account like '3337%' and GL.PostedDate <= @inputDate)
    INSERT INTO @tbluutru(icheck, tkThue, dienGiai, soTienPhaiNop)
    VALUES(@icheck, N'3337', N'Thuế nhà đất, tiền thuê dất', @ThueNhaDat)
    
    DECLARE @ThueBaoVeMT money
    set @ThueBaoVeMT = (select SUM(GL.CreditAmount - GL.DebitAmount) from dbo.GeneralLedger GL where GL.Account like '33381%' and GL.PostedDate <= @inputDate)
    INSERT INTO @tbluutru(icheck, tkThue, dienGiai, soTienPhaiNop)
    VALUES(@icheck, N'33381', N'Thuế bảo vệ môi trường', @ThueBaoVeMT)
    
    DECLARE @ThueKhac money
    set @ThueKhac = (select SUM(GL.CreditAmount - GL.DebitAmount) from dbo.GeneralLedger GL where GL.Account like '33382%' and GL.PostedDate <= @inputDate)
    INSERT INTO @tbluutru(icheck, tkThue, dienGiai, soTienPhaiNop)
    VALUES(@icheck, N'33382', N'Các loại thuế khác', @ThueKhac)
    
    DECLARE @PhiNopKhac money
    set @PhiNopKhac = (select SUM(GL.CreditAmount - GL.DebitAmount) from dbo.GeneralLedger GL where GL.Account like '3339%' and GL.PostedDate <= @inputDate)
    INSERT INTO @tbluutru(icheck, tkThue, dienGiai, soTienPhaiNop)
    VALUES(@icheck, N'3339', N'Phí, lệ phí và các khoản nộp khác', @PhiNopKhac)
    
    UPDATE @tbluutru SET soTienNopLanNay = soTienPhaiNop
    
    SELECT icheck, tkThue, dienGiai, soTienPhaiNop, soTienNopLanNay from @tbluutru where soTienPhaiNop>0
    
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[FixedAsset]
  ADD [IsActive] [bit] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[FAIncrementDetail]
  ADD [CashOutExchangeRate] [decimal](25, 10) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[FAIncrementDetail]
  ADD [CashOutAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[FAIncrementDetail]
  ADD [CashOutDifferAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[FAIncrementDetail]
  ADD [CashOutDifferAccount] [nvarchar](25) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[FAIncrementDetail]
  ADD [CashOutVATAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[FAIncrementDetail]
  ADD [CashOutDifferVATAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sp_refreshview '[dbo].[ViewVouchersCloseBook]'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
CREATE PROCEDURE [dbo].[Proc_SA_SoTheoDoiLaiLoTheoHoaDon]
    @FromDate DATETIME,
    @ToDate DATETIME
AS
BEGIN          
    DECLARE @tbluutru TABLE (
		DetailID UNIQUEIDENTIFIER,
		InvoiceDate Date,
		InvoiceNo NVARCHAR(25),
		AccountingObjectID UNIQUEIDENTIFIER,
		Reason NVARCHAR(512),
		GiaTriHHDV money,
		ChietKhauBan money,
		ChietKhau_TLGG money,
		GiamGia money,
		TraLai money,
		GiaVon money,
		LaiLo money
    )
    INSERT INTO @tbluutru(DetailID, InvoiceDate,InvoiceNo,AccountingObjectID,Reason)
		SELECT DISTINCT GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason
		FROM GeneralLedger GL 
		WHERE (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
		AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAInvoiceDetail))
		
    UPDATE @tbluutru SET GiaTriHHDV = a.GiaTriHHDV
	FROM (SELECT GL.DetailID as IvID, GL.InvoiceDate as IvDate, GL.InvoiceNo as IvNo, GL.AccountingObjectID as AOID, GL.Reason as Re, SUM(GL.CreditAmount) as GiaTriHHDV
		FROM GeneralLedger GL 
		WHERE (GL.Account like '511%') AND (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
		AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAInvoiceDetail))
		GROUP BY GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason) a
	WHERE DetailID = a.IvID AND InvoiceDate = a.IvDate AND InvoiceNo = a.IvNo AND AccountingObjectID = a.AOID AND Reason = a.Re
	
	UPDATE @tbluutru SET ChietKhauBan = b.ChietKhauBan
	FROM (SELECT GL.DetailID as IvID, GL.InvoiceDate as IvDate, GL.InvoiceNo as IvNo, GL.AccountingObjectID as AOID, GL.Reason as Re, SUM(GL.DebitAmount) as ChietKhauBan
		FROM GeneralLedger GL 
		WHERE (GL.Account like '511%') AND (GL.TypeID not in (330,340)) AND (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
		AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAInvoiceDetail))
		GROUP BY GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason) b
	WHERE DetailID = b.IvID AND InvoiceDate = b.IvDate AND InvoiceNo = b.IvNo AND AccountingObjectID = b.AOID AND Reason = b.Re
	
	UPDATE @tbluutru SET ChietKhau_TLGG = c.ChietKhau_TLGG
	FROM (SELECT GL.DetailID as IvID, GL.InvoiceDate as IvDate, GL.InvoiceNo as IvNo, GL.AccountingObjectID as AOID, GL.Reason as Re, SUM(GL.CreditAmount) as ChietKhau_TLGG
		FROM GeneralLedger GL
		WHERE (GL.Account like '511%') AND (GL.TypeID in (330,340)) AND (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
		AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAInvoiceDetail))
		GROUP BY GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason) c
	WHERE DetailID = c.IvID AND InvoiceDate = c.IvDate AND InvoiceNo = c.IvNo AND AccountingObjectID = c.AOID AND Reason = c.Re
	
	DECLARE @tbDataTraLai TABLE(
		SAInvoiceDetailID UNIQUEIDENTIFIER,
		TraLai money
	)
	INSERT INTO @tbDataTraLai(SAInvoiceDetailID,TraLai)
		SELECT SAR.SAInvoiceDetailID, SUM(GL.DebitAmount - GL.CreditAmount) as TraLai
		FROM GeneralLedger GL LEFT JOIN SAReturnDetail SAR ON GL.DetailID = SAR.ID
		WHERE (GL.Account like '511%') AND (GL.TypeID=330) AND (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
		AND (GL.PostedDate between @FromDate and @ToDate)
		GROUP BY SAR.SAInvoiceDetailID
	
	UPDATE @tbluutru SET TraLai = TL.TraLai
	FROM (select SAInvoiceDetailID, TraLai from @tbDataTraLai) TL
	WHERE DetailID = TL.SAInvoiceDetailID
	
	UPDATE @tbluutru SET GiaVon = d.GiaVon
	FROM (SELECT GL.DetailID as IvID, GL.InvoiceDate as IvDate, GL.InvoiceNo as IvNo, GL.AccountingObjectID as AOID, GL.Reason as Re, SUM(GL.DebitAmount - GL.CreditAmount) as GiaVon
		FROM GeneralLedger GL 
		WHERE (GL.Account like '632%') AND (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
		AND (GL.PostedDate between @FromDate and @ToDate)
		GROUP BY GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason) d
	WHERE DetailID = d.IvID AND InvoiceDate = d.IvDate AND InvoiceNo = d.IvNo AND AccountingObjectID = d.AOID AND Reason = d.Re
		
	
	DECLARE @tbDataReturn TABLE (
		InvoiceDate Date,
		InvoiceNo NVARCHAR(25),
		AccountingObjectID UNIQUEIDENTIFIER,
		AccountingObjectName NVARCHAR(512),
		Reason NVARCHAR(512),
		GiaTriHHDV money,
		ChietKhauBan money,
		ChietKhau_TLGG money,
		ChietKhau money,
		GiamGia money,
		TraLai money,
		GiaVon money,
		LaiLo money
    )
    
    INSERT INTO @tbDataReturn(InvoiceDate,InvoiceNo,AccountingObjectID,Reason,GiaTriHHDV,ChietKhauBan,ChietKhau_TLGG,TraLai,GiaVon)
    SELECT InvoiceDate,InvoiceNo,AccountingObjectID,Reason,SUM(GiaTriHHDV) as GiaTriHHDV, SUM(ChietKhauBan) as ChietKhauBan,
    SUM(ChietKhau_TLGG) as ChietKhau_TLGG, SUM(TraLai) as TraLai, SUM(GiaVon) as GiaVon
    FROM @tbluutru
    GROUP BY InvoiceDate,InvoiceNo,AccountingObjectID,Reason
    
	INSERT INTO @tbDataReturn(InvoiceDate,InvoiceNo,AccountingObjectID,Reason,GiamGia)
		SELECT GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason, SUM(GL.DebitAmount - GL.CreditAmount) as GiamGia
		FROM GeneralLedger GL 
		WHERE (GL.Account like '511%') AND (GL.TypeID=340) AND (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
		AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAReturnDetail))
		GROUP BY GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason
    
    UPDATE @tbDataReturn SET AccountingObjectName = AJ.AccountingObjectName
	FROM ( select ID,AccountingObjectName from AccountingObject) AJ
	where AccountingObjectID = AJ.ID
    
    UPDATE @tbDataReturn SET ChietKhau = (ISNULL(ChietKhauBan,0) - ISNULL(ChietKhau_TLGG,0))
    
    UPDATE @tbDataReturn SET LaiLo = (ISNULL(GiaTriHHDV,0) - (ISNULL(ChietKhau,0)+ISNULL(GiamGia,0)+ISNULL(TraLai,0)+ISNULL(GiaVon,0)))
    
    SELECT InvoiceDate,InvoiceNo,AccountingObjectName,Reason,GiaTriHHDV,ChietKhau,GiamGia,TraLai,GiaVon,LaiLo FROM @tbDataReturn
    WHERE ((ISNULL(GiaTriHHDV,0)>0) OR (ISNULL(ChietKhau,0)>0) OR (ISNULL(GiamGia,0)>0)
			OR (ISNULL(TraLai,0)>0) OR (ISNULL(GiaVon,0)>0) OR (ISNULL(LaiLo,0)>0))
	ORDER BY InvoiceDate,InvoiceNo
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
CREATE PROCEDURE [dbo].[Proc_GetHealthFinanceMoney]
      ( @FromDate DATETIME ,
        @ToDate DATETIME
		)
AS
BEGIN
	DECLARE @tbDataReturn TABLE(
		tySoNo decimal(18,2),
		tyTaiTro decimal(18,2),
		vonLuanChuyen decimal(18,2),
		hsTTNganHan decimal(18,2),
		hsTTNhanh decimal(18,2),
		hsTTTucThoi decimal(18,2),
		hsTTChung decimal(18,2),
		hsQVHangTonKho decimal(18,2),
		hsLNTrenVonKD decimal(18,2),
		hsLNTrenDTThuan decimal(18,2),
		hsLNTrenVonCSH decimal(18,2),
		tienMat money,
		tienGui money,
		doanhThu money,
		chiPhi money,
		loiNhuanTruocThue money,
		phaiThu money,
		phaiTra money,
		hangTonKho money
	)
	
	INSERT INTO @tbDataReturn(tySoNo,tyTaiTro,vonLuanChuyen,hsTTNganHan,hsTTNhanh,hsTTTucThoi,hsTTChung,hsQVHangTonKho,hsLNTrenVonKD,hsLNTrenDTThuan,
	hsLNTrenVonCSH,tienMat,tienGui,doanhThu,chiPhi,loiNhuanTruocThue,phaiThu,phaiTra,hangTonKho)
	Values(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null)
	
	DECLARE @tbDataGetB01DN TABLE(
	  ItemID UNIQUEIDENTIFIER ,
      ItemCode NVARCHAR(25) ,
      ItemName NVARCHAR(255) ,
      ItemNameEnglish NVARCHAR(255) ,
      ItemIndex INT ,
      Description NVARCHAR(255) ,
      FormulaType INT ,
      FormulaFrontEnd NVARCHAR(MAX) ,
      Formula XML ,
      Hidden BIT ,
      IsBold BIT ,
      IsItalic BIT ,
      Category INT ,
      SortOrder INT,
      Amount DECIMAL(25, 4) ,
      PrevAmount DECIMAL(25, 4)
	)
	
	INSERT INTO @tbDataGetB01DN
	SELECT * FROM [dbo].[Func_GetB01_DN] (null,null,@FromDate,@ToDate,0,1,0,@FromDate,@ToDate)
	
	
	DECLARE @tbDataGetBalanceAccountF01 TABLE(
		AccountID UNIQUEIDENTIFIER ,
        AccountCategoryKind INT ,
        AccountNumber NVARCHAR(50) ,
        AccountName NVARCHAR(255) ,
        OpeningDebitAmount DECIMAL(25, 4) ,
        OpeningCreditAmount DECIMAL(25, 4) ,
        DebitAmount DECIMAL(25, 4) ,
        CreditAmount DECIMAL(25, 4) ,
        DebitAmountAccum DECIMAL(25, 4) ,
        CreditAmountAccum DECIMAL(25, 4) ,
        ClosingDebitAmount DECIMAL(25, 4) ,
        ClosingCreditAmount DECIMAL(25, 4)
	)
	
	INSERT INTO @tbDataGetBalanceAccountF01
	SELECT t.AccountID ,
		       t.AccountCategoryKind,
			   t.AccountNumber,
			   t.AccountName,
			   sum(t.OpeningDebitAmount) OpeningDebitAmount,
			   sum(t.OpeningCreditAmount) OpeningCreditAmount,
			   sum(t.DebitAmount) DebitAmount,
			   sum(t.CreditAmount) CreditAmount,
			   sum(t.DebitAmountAccum) DebitAmountAccum,
			   sum(t.CreditAmountAccum) CreditAmountAccum,
			   sum(t.ClosingDebitAmount) ClosingDebitAmount,
			   sum(t.ClosingCreditAmount) ClosingCreditAmount
			   FROM [dbo].[Func_GetBalanceAccountF01] (@FromDate,@ToDate,1,0) t
	GROUP BY t.AccountID,
		       t.AccountCategoryKind,
			   t.AccountNumber,
			   t.AccountName
	ORDER BY t.AccountNumber
	
	
	Declare @ct400 decimal(25, 4)
	Set @ct400=(select Amount from @tbDataGetB01DN where ItemCode='400')
	
	Declare @ct600 decimal(25, 4)
	Set @ct600=(select Amount from @tbDataGetB01DN where ItemCode='600')
	
	if(ISNULL(@ct600,0)=0)
	begin
		UPDATE @tbDataReturn SET tySoNo = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET tySoNo = (ISNULL(@ct400,0)/ISNULL(@ct600,0))*100
	end
	
	Declare @ct500 decimal(25, 4)
	Set @ct500=(select Amount from @tbDataGetB01DN where ItemCode='500')
	
	if(ISNULL(@ct600,0)=0)
	begin
		UPDATE @tbDataReturn SET tyTaiTro = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET tyTaiTro = (ISNULL(@ct500,0)/ISNULL(@ct600,0))*100
	end
	
	Declare @ct100 decimal(25, 4)
	Set @ct100=(select Amount from @tbDataGetB01DN where ItemCode='100')
	
	Declare @ct410 decimal(25, 4)
	Set @ct410=(select Amount from @tbDataGetB01DN where ItemCode='410')
	
	UPDATE @tbDataReturn SET vonLuanChuyen = (ISNULL(@ct100,0)-ISNULL(@ct410,0))
	
	if(ISNULL(@ct410,0)=0)
	begin
		UPDATE @tbDataReturn SET hsTTNganHan = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsTTNganHan = (ISNULL(@ct100,0)/ISNULL(@ct410,0))
	end
	
	Declare @ct140 decimal(25, 4)
	Set @ct140=(select Amount from @tbDataGetB01DN where ItemCode='140')
	
	if(ISNULL(@ct410,0)=0)
	begin
		UPDATE @tbDataReturn SET hsTTNhanh = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsTTNhanh = (ISNULL(@ct100,0)-ISNULL(@ct140,0))/ISNULL(@ct410,0)
	end
	
	Declare @ct110 decimal(25, 4)
	Set @ct110=(select Amount from @tbDataGetB01DN where ItemCode='110')
	
	Declare @ct120 decimal(25, 4)
	Set @ct120=(select Amount from @tbDataGetB01DN where ItemCode='120')
	
	if(ISNULL(@ct410,0)=0)
	begin
		UPDATE @tbDataReturn SET hsTTTucThoi = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsTTTucThoi = (ISNULL(@ct110,0)+ISNULL(@ct120,0))/ISNULL(@ct410,0)
	end
	
	Declare @ct300 decimal(25, 4)
	Set @ct300=(select Amount from @tbDataGetB01DN where ItemCode='300')
	
	if(ISNULL(@ct400,0)=0)
	begin
		UPDATE @tbDataReturn SET hsTTChung = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsTTChung = (ISNULL(@ct300,0)/ISNULL(@ct400,0))
	end
	
	Declare @vonHH decimal(25, 4)
	Set @vonHH = (select SUM(DebitAmount) from GeneralLedger where Account='632' AND AccountCorresponding='911' AND (PostedDate between @FromDate and @ToDate))
	
	Declare @hangHoaTon decimal(25,4)
	set @hangHoaTon = (select SUM(ISNULL(OpeningDebitAmount,0)+ISNULL(ClosingDebitAmount,0)) from @tbDataGetBalanceAccountF01 
						where AccountNumber in ('152','153','154','155','156','157'))/2
	
	if(ISNULL(@hangHoaTon,0)=0)
	begin
		UPDATE @tbDataReturn SET hsQVHangTonKho = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsQVHangTonKho=(ISNULL(@vonHH,0)/ISNULL(@hangHoaTon,0))*100
	end
	
	Declare @LNsauThue decimal(25,4)
	set @LNsauThue = (select SUM(CreditAmount) from GeneralLedger where Account='4212' AND AccountCorresponding='911' AND (PostedDate between @FromDate and @ToDate))
	
	Declare @ct600namTruoc decimal(25, 4)
	Set @ct600namTruoc=(select PrevAmount from @tbDataGetB01DN where ItemCode='600')
	
	Declare @vonKdBq decimal(25,4)
	set @vonKdBq = (ISNULL(@ct600,0)+ISNULL(@ct600namTruoc,0))/2
	
	if(ISNULL(@vonKdBq,0)=0)
	begin
		UPDATE @tbDataReturn SET hsLNTrenVonKD = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsLNTrenVonKD = (ISNULL(@LNsauThue,0)/ISNULL(@vonKdBq,0))
	end
	
	
	Declare @dtThuan decimal(25,4)
	set @dtThuan = (select SUM(CreditAmount) from GeneralLedger where Account like'511%' AND AccountCorresponding='911' AND (PostedDate between @FromDate and @ToDate))
	
	if(ISNULL(@dtThuan,0)=0)
	begin
		UPDATE @tbDataReturn SET hsLNTrenDTThuan = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsLNTrenDTThuan = (ISNULL(@LNsauThue,0)/ISNULL(@dtThuan,0))
	end
	
	Declare @ct500namtruoc decimal(25, 4)
	Set @ct500namtruoc=(select PrevAmount from @tbDataGetB01DN where ItemCode='500')
	
	Declare @voncsh decimal(25,4)
	set @voncsh = (ISNULL(@ct500,0)+ISNULL(@ct500namtruoc,0))/2
	
	if(ISNULL(@voncsh,0)=0)
	begin
		UPDATE @tbDataReturn SET hsLNTrenVonCSH = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsLNTrenVonCSH = (ISNULL(@LNsauThue,0)/ISNULL(@voncsh,0))
	end
	
	Declare @duNoCuoiKy111 money
	set @duNoCuoiKy111 = (select ClosingDebitAmount from @tbDataGetBalanceAccountF01 where AccountNumber='111')
	
	UPDATE @tbDataReturn SET tienMat = ISNULL(@duNoCuoiKy111,0)
	
	Declare @duNoCuoiKy112 money
	set @duNoCuoiKy112 = (select ClosingDebitAmount from @tbDataGetBalanceAccountF01 where AccountNumber='112')
	
	UPDATE @tbDataReturn SET tienGui = ISNULL(@duNoCuoiKy112,0)
	
	Declare @doanhThu money
	set @doanhThu = (select SUM(CreditAmount - DebitAmount) from GeneralLedger 
						where ((Account like '511%') or (Account like '515%') or (Account like '711%'))
								and (AccountCorresponding<>'911') and (PostedDate between @FromDate and @ToDate))
	
	UPDATE @tbDataReturn SET doanhThu = ISNULL(@doanhThu,0)
	
	Declare @chiPhi money
	set @chiPhi = (select SUM(DebitAmount - CreditAmount) from GeneralLedger
						where ((Account like '632%') or (Account like '642%') or (Account like '635%') or (Account like '811%') or (Account like '821%'))
						and (AccountCorresponding<>'911') and (PostedDate between @FromDate and @ToDate))
	
	UPDATE @tbDataReturn SET chiPhi = ISNULL(@chiPhi,0)
	
	Declare @chiPhiko821 money
	set @chiPhiko821 = (select SUM(DebitAmount - CreditAmount) from GeneralLedger
						where ((Account like '632%') or (Account like '642%') or (Account like '635%') or (Account like '811%'))
						and (AccountCorresponding<>'911') and (PostedDate between @FromDate and @ToDate))
	
	UPDATE @tbDataReturn SET loiNhuanTruocThue = (ISNULL(@doanhThu,0) - ISNULL(@chiPhiko821,0))
	
	Declare @phaiThu money
	set @phaiThu = (select SUM(ClosingDebitAmount) from @tbDataGetBalanceAccountF01 where AccountNumber='131')
	
	UPDATE @tbDataReturn SET phaiThu = (ISNULL(@phaiThu,0))
	
	Declare @phaiTra money
	set @phaiTra = (select SUM(ClosingCreditAmount) from @tbDataGetBalanceAccountF01 where AccountNumber='331')
	
	UPDATE @tbDataReturn SET phaiTra = (ISNULL(@phaiTra,0))
	
	Declare @hangTonKho money
	set @hangTonKho = (select SUM(ClosingDebitAmount) from @tbDataGetBalanceAccountF01 
						where AccountNumber in ('152','153','154','155','156','157'))
	
	UPDATE @tbDataReturn SET hangTonKho = (ISNULL(@hangTonKho,0))
	
	SELECT * FROM @tbDataReturn
end
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
CREATE PROCEDURE [dbo].[Proc_GetHealthFinanceChart]
      ( @FromDate DATETIME ,
        @ToDate DATETIME
		)
AS
BEGIN
	DECLARE @tbTemp TABLE(
		PostedDate datetime,
		PostedDay int,
		PostedMonth int,
		PostedYear int,
		Account nvarchar(25),
		AccountCorresponding nvarchar(25),
		DebitAmount money,
		CreditAmount money
	)
	
	INSERT INTO @tbTemp
	SELECT PostedDate, DAY(PostedDate) as Ngay, MONTH(PostedDate) as Thang, YEAR(PostedDate) as Nam, Account, AccountCorresponding, 
	DebitAmount, CreditAmount
	FROM GeneralLedger
	WHERE PostedDate between @FromDate AND @ToDate
	
	Declare @tbDataTempChart TABLE(
		PostedMonth int,
		PostedYear int,
		TurnoverTotal money,
		CostsTotal money
	)
	
	INSERT INTO @tbDataTempChart(PostedMonth,PostedYear,TurnoverTotal)
	SELECT PostedMonth, PostedYear, SUM(CreditAmount-DebitAmount) as TurnoverTotal
	FROM @tbTemp
	WHERE ((Account like '511%') OR (Account like '515%') OR (Account like '711%')) AND AccountCorresponding <> '911'
	GROUP BY PostedMonth, PostedYear
	
	INSERT INTO @tbDataTempChart(PostedMonth,PostedYear,CostsTotal)
	SELECT PostedMonth, PostedYear, SUM(DebitAmount-CreditAmount) as CostsTotal
	FROM @tbTemp
	WHERE ((Account like '632%') OR (Account like '642%') OR (Account like '635%') OR (Account like '811%') OR (Account like '821%')) 
	AND AccountCorresponding <> '911'
	GROUP BY PostedMonth, PostedYear
	
	Declare @tbDataChart TABLE(
		PostedMonthYear nvarchar(15),
		PostedMonth int,
		PostedYear int,
		TurnoverTotal money,
		CostsTotal money
	)
	
	INSERT INTO @tbDataChart(PostedMonth,PostedYear,TurnoverTotal,CostsTotal)
	SELECT PostedMonth,PostedYear,SUM(TurnoverTotal) as TurnoverTotal, SUM(CostsTotal) as CostsTotal
	FROM @tbDataTempChart
	GROUP BY PostedMonth,PostedYear
	
	UPDATE @tbDataChart SET TurnoverTotal=0 WHERE  TurnoverTotal<0
	UPDATE @tbDataChart SET CostsTotal=0 WHERE  CostsTotal<0
	
	UPDATE @tbDataChart SET PostedMonthYear=CONVERT(nvarchar(2),PostedMonth)+'/'+CONVERT(nvarchar(4),PostedYear)
	
	SELECT PostedMonthYear,TurnoverTotal,CostsTotal FROM @tbDataChart
end
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
CREATE PROCEDURE [dbo].[Proc_GetB09_SecVI] 
	@StartDate DateTime,
	@EndDate DateTime,
	@StartDateAgo DateTime
AS
BEGIN
	/*=========== Tinh VI_1_a_1 ============*/
	SELECT 'VI_1_a_1' Name,
		(
			SELECT sum (CreditAmount)								
			FROM [GeneralLedger]							
			Where Account like '5111%'								
			and PostedDate >= @StartDate and  PostedDate <= @EndDate								
			and  typeID not in ('330', '340')								
			and AccountCorresponding <> '911%'
		) ThisYear,
		(
			SELECT sum (CreditAmount)								
			FROM [GeneralLedger]							
			Where Account like '5111%'								
			and PostedDate >= @StartDateAgo and  PostedDate < @StartDate								
			and  typeID not in ('330', '340')								
			and AccountCorresponding <> '911%'
		) LastYear
	/*=========== Tinh VI_1_a_2 ============*/
	UNION ALL
	SELECT 'VI_1_a_2' Name,
		(
			SELECT sum (CreditAmount)								
			FROM [GeneralLedger]							
			Where Account like '5112%'								
			and PostedDate >= @StartDate and  PostedDate <= @EndDate								
			and  typeID not in ('330', '340')

		) ThisYear,
		(
			SELECT sum (CreditAmount)								
			FROM [GeneralLedger]							
			Where Account like '5112%'								
			and PostedDate >= @StartDateAgo and  PostedDate < @StartDate								
			and  typeID not in ('330', '340')
		) LastYear
	/*=========== Tinh VI_1_a_3 ============*/
	UNION ALL
	SELECT 'VI_1_a_3' Name,
		(
			SELECT sum (CreditAmount)								
			FROM [GeneralLedger]							
			Where Account like '5113%'								
			and PostedDate >= @StartDate and  PostedDate <= @EndDate								
			and  typeID not in ('330', '340')								
			and AccountCorresponding <> '911%'
		) ThisYear,
		(
			SELECT sum (CreditAmount)								
			FROM [GeneralLedger]							
			Where Account like '5113%'								
			and PostedDate >= @StartDateAgo and  PostedDate < @StartDate								
			and  typeID not in ('330', '340')								
			and AccountCorresponding <> '911%'
		) LastYear
	/*=========== Tinh VI_1_a_4 ============*/
	UNION ALL
	SELECT 'VI_1_a_4' Name,
		(
			SELECT sum (CreditAmount)								
			FROM [GeneralLedger]							
			Where Account like '5118%'								
			and PostedDate >= @StartDate and  PostedDate <= @EndDate								
			and  typeID not in ('330', '340')								
			and AccountCorresponding <> '911%'

		) ThisYear,
		(
			SELECT sum (CreditAmount)								
			FROM [GeneralLedger]							
			Where Account like '5118%'								
			and PostedDate >= @StartDateAgo and  PostedDate < @StartDate								
			and  typeID not in ('330', '340')								
			and AccountCorresponding <> '911%'
		) LastYear
	/*=========== Tinh VI_2_1 ============*/
	UNION ALL
	SELECT 'VI_2_1' Name,
		(
			SELECT sum (DebitAmount)								
			FROM [GeneralLedger]								
			where Account like '511%' and DebitAmount > 0 								
			and PostedDate >= @StartDate and  PostedDate <= @EndDate								
			and TypeID in ('320','321','322','323','324','325')
		) ThisYear,
		(
			SELECT sum (DebitAmount)								
			FROM [GeneralLedger]								
			where Account like '511%' and DebitAmount > 0 								
			and PostedDate >= @StartDateAgo and  PostedDate < @StartDate								
			and TypeID in ('320','321','322','323','324','325')
		) LastYear
	/*=========== Tinh VI_2_2 ============*/
	UNION ALL
	SELECT 'VI_2_2' Name,
		(
			SELECT sum (DebitAmount) - Sum (CreditAmount)								
			FROM [GeneralLedger]								
			where Account like '511%'								
			and PostedDate >= @StartDate and  PostedDate <= @EndDate								
			and TypeID in ('340')
		) ThisYear,
		(
			SELECT sum (DebitAmount) - Sum (CreditAmount)								
			FROM [GeneralLedger]								
			where Account like '511%'								
			and PostedDate >= @StartDateAgo and  PostedDate < @StartDate								
			and TypeID in ('340')
		) LastYear
	/*=========== Tinh VI_2_3 ============*/
	UNION ALL
	SELECT 'VI_2_3' Name,
		(
			SELECT sum (DebitAmount) - sum (CreditAmount)								
			FROM [GeneralLedger]								
			where Account like '511%'								
			and PostedDate >= @StartDate and  PostedDate <= @EndDate								
			and TypeID in ('330')
		) ThisYear,
		(
			SELECT sum (DebitAmount) - sum (CreditAmount)								
			FROM [GeneralLedger]								
			where Account like '511%'								
			and PostedDate >= @StartDateAgo and  PostedDate < @StartDate								
			and TypeID in ('330')
		) LastYear
	/*=========== Tinh VI_3_1 ============*/
	UNION ALL
	SELECT 'VI_3_1' Name,
		(
			SELECT sum (CreditAmount)								
			FROM [GeneralLedger]								
			where Account like '632%' and CreditAmount > 0 								
			and AccountCorresponding like '911%'								
			and PostedDate >= @StartDate and  PostedDate <= @EndDate
		) ThisYear,
		(
			SELECT sum (CreditAmount)								
			FROM [GeneralLedger]								
			where Account like '632%' and CreditAmount > 0 								
			and AccountCorresponding like '911%'								
			and PostedDate >= @StartDateAgo and  PostedDate < @StartDate
		) LastYear
	/*=========== Tinh VI_4_6 ============*/
	UNION ALL
	SELECT 'VI_4_6' Name,
		(
			SELECT sum (DebitAmount)
			FROM [GeneralLedger]
			where Account like '515%' and DebitAmount > 0 
			and AccountCorresponding like '911%'
			and PostedDate >= @StartDate and  PostedDate <= @EndDate
		) ThisYear,
		(
			SELECT sum (DebitAmount)
			FROM [GeneralLedger]
			where Account like '515%' and DebitAmount > 0 
			and AccountCorresponding like '911%'
			and PostedDate >= @StartDateAgo and  PostedDate < @StartDate
		) LastYear
	/*=========== Tinh VI_5_6 ============*/
	UNION ALL
	SELECT 'VI_5_6' Name,
		(
			SELECT sum (CreditAmount)
			FROM [GeneralLedger]
			where Account like '635%' and CreditAmount > 0 
			and AccountCorresponding like '911%'
			and PostedDate >= @StartDate and  PostedDate <= @EndDate
		) ThisYear,
		(
			SELECT sum (CreditAmount)
			FROM [GeneralLedger]
			where Account like '635%' and CreditAmount > 0 
			and AccountCorresponding like '911%'
			and PostedDate >= @StartDateAgo and  PostedDate < @StartDate
		) LastYear
	/*=========== Tinh VI_6_1 ============*/
	UNION ALL
	SELECT 'VI_6_a' Name,
		(
			SELECT sum (CreditAmount)
			FROM [GeneralLedger]
			where Account like '6422%' and CreditAmount > 0 
			and AccountCorresponding like '911%'
			and PostedDate >= @StartDate and  PostedDate <= @EndDate
		) ThisYear,
		(
			SELECT sum (CreditAmount)
			FROM [GeneralLedger]
			where Account like '6422%' and CreditAmount > 0 
			and AccountCorresponding like '911%'
			and PostedDate >= @StartDateAgo and  PostedDate < @StartDate
		) LastYear
	/*=========== Tinh VI_6_2 ============*/
	UNION ALL
	SELECT 'VI_6_b' Name,
		(
			SELECT sum (CreditAmount)
			FROM [GeneralLedger]
			where Account like '6421%' and CreditAmount > 0 
			and AccountCorresponding like '911%'
			and PostedDate >= @StartDate and  PostedDate <= @EndDate
		) ThisYear,
		(
			SELECT sum (CreditAmount)
			FROM [GeneralLedger]
			where Account like '6421%' and CreditAmount > 0 
			and AccountCorresponding like '911%'
			and PostedDate >= @StartDateAgo and  PostedDate < @StartDate
		) LastYear
	/*=========== Tinh VI_7_5 ============*/
	UNION ALL
	SELECT 'VI_7_5' Name,
		(
			SELECT sum (DebitAmount)
			FROM [GeneralLedger]
			where Account like '711%' and DebitAmount > 0 
			and AccountCorresponding like '911%'
			and PostedDate >= @StartDate and  PostedDate <= @EndDate
		) ThisYear,
		(
			SELECT sum (DebitAmount)
			FROM [GeneralLedger]
			where Account like '711%' and DebitAmount > 0 
			and AccountCorresponding like '911%'
			and PostedDate >= @StartDateAgo and  PostedDate < @StartDate
		) LastYear
	/*=========== Tinh VI_8_4 ============*/
	UNION ALL
	SELECT 'VI_8_4' Name,
		(
			SELECT sum (CreditAmount)
			FROM [GeneralLedger]
			where Account like '811%' and CreditAmount > 0 
			and AccountCorresponding like '911%'
			and PostedDate >= @StartDate and  PostedDate <= @EndDate
		) ThisYear,
		(
			SELECT sum (CreditAmount)
			FROM [GeneralLedger]
			where Account like '811%' and CreditAmount > 0 
			and AccountCorresponding like '911%'
			and PostedDate >= @StartDateAgo and  PostedDate < @StartDate
		) LastYear
	/*=========== Tinh VI_9_3 ============*/
	UNION ALL
	SELECT 'VI_9_3' Name,
		(
			SELECT sum (CreditAmount)	
			FROM [GeneralLedger]	
			where Account like '821%' and CreditAmount > 0 	
			and AccountCorresponding like '911%'	
			and PostedDate >= @StartDate and  PostedDate <= @EndDate
		) ThisYear,
		(
			SELECT sum (CreditAmount)	
			FROM [GeneralLedger]	
			where Account like '821%' and CreditAmount > 0 	
			and AccountCorresponding like '911%'	
			and PostedDate >= @StartDateAgo and  PostedDate < @StartDate
		) LastYear
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_FU_GetCashDetailBook]
    (
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @AccountNumber NVARCHAR(20) ,
      @BranchID UNIQUEIDENTIFIER ,
      @CurrencyID NVARCHAR(3)
    )
AS 
    BEGIN

        CREATE  TABLE #Result
            (
              RowNum INT IDENTITY(1, 1)
                         PRIMARY KEY ,
              RefID UNIQUEIDENTIFIER ,
			  AccountNumber NVARCHAR(15),
              PostedDate DATETIME ,
              RefDate DATETIME ,
              ReceiptRefNo NVARCHAR(25) ,
              PaymentRefNo NVARCHAR(25) ,
              RefType INT ,
              JournalMemo NVARCHAR(255) ,
              CorrespondingAccountNumber NVARCHAR(20) ,
              ExchangeRate Numeric ,
              DebitAmount Numeric ,
              DebitAmountOC Numeric ,
              CreditAmount Numeric ,
              CreditAmountOC Numeric ,
              ClosingAmount Numeric ,
              ClosingAmountOC Numeric ,
              ContactName NVARCHAR(255) ,
			  AccountObjectId NVARCHAR(255),
              AccountObjectCode NVARCHAR(255) ,
              AccountObjectName NVARCHAR(255) , 
              EmployeeCode NVARCHAR(255) ,
              EmployeeName NVARCHAR(255) , 
              InvoiceOrder INT , 
              BranchID UNIQUEIDENTIFIER ,
              BranchName NVARCHAR(255) ,
              IsBold BIT
            )
         DECLARE @tblAccountNumber TABLE
            (              
			  AccountNumber  NVARCHAR(25),  
              AccountNumberPercent NVARCHAR(25)              
            )
        INSERT  @tblAccountNumber
                SELECT  A1.AccountNumber,
                        A1.AccountNumber + '%'
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountNumber,',') F
                        INNER JOIN dbo.Account A1 ON A1.AccountNumber = F.Value
        DECLARE @AccountNumberPercent NVARCHAR(25)
        SET @AccountNumberPercent = @AccountNumber + '%'
        
        DECLARE @DiscountNumber NVARCHAR(25)       
        SET @DiscountNumber = '5211%'
   
        DECLARE @ClosingAmountOC MONEY
        DECLARE @ClosingAmount MONEY
   
 INSERT  #Result
	          (RefID ,
			  AccountNumber,
              PostedDate ,
              RefDate ,
              ReceiptRefNo  ,
              PaymentRefNo,
              RefType ,
              JournalMemo,
              ExchangeRate ,
              DebitAmount  ,
              DebitAmountOC  ,
              CreditAmount  ,
              CreditAmountOC  ,
              ClosingAmount  ,
              ClosingAmountOC  ,
              ContactName,
              AccountObjectId,
              InvoiceOrder  , 
              BranchID  ,
              BranchName,
			  CorrespondingAccountNumber)
			  select ReferenceID ,
			  Account,
              PostedDate ,
              RefDate ,
              ReceiptRefNo  ,
              PaymentRefNo,
              RefType ,
              JournalMemo,
              ExchangeRate ,
              DebitAmount  ,
              DebitAmountOC  ,
              CreditAmount  ,
              CreditAmountOC  ,
              ClosingAmount  ,
              ClosingAmountOC  ,
              ContactName,
              AccountObjectId,
              InvoiceOrder  , 
              BranchID  ,
              BranchName,
			  AccountCorresponding from 
			            (SELECT null as ReferenceID,
						    Acc.AccountNumber as Account,
							null as PostedDate,
							null as RefDate,
							null as ReceiptRefNo,
							null as PaymentRefNo,
							null as RefType,
					        N'Số tồn đầu kỳ' AS JournalMemo ,
                            $0 AS ExchangeRate ,
                            $0 AS DebitAmount ,
                            $0 AS DebitAmountOC ,
                            $0 AS CreditAmount ,
                            $0 AS CreditAmountOC ,
							SUM(GL.DebitAmount - GL.CreditAmount) as ClosingAmount,
							SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal) as ClosingAmountOC,
							null as ContactName,
							null as AccountObjectId,
                            0 AS InvoiceOrder ,
                            null BranchID ,
                            0 as BranchName,
                            null AccountCorresponding
							 from GeneralLedger gl
							 inner join @tblAccountNumber Acc on GL.Account = Acc.AccountNumber
							 where ( GL.PostedDate < @FromDate )
								AND ( @CurrencyID IS NULL
								OR GL.CurrencyID = @CurrencyID)
						    group by Acc.AccountNumber
				        union all
                        SELECT  GL.ReferenceID,
						        GL.Account,
                                GL.PostedDate ,
                                GL.RefDate ,
                                CASE WHEN GL.DebitAmount <> 0
                                          OR GL.DebitAmountOriginal <> 0
                                     THEN GL.RefNo
                                     ELSE ''
                                END AS ReceiptRefNo ,
                                CASE WHEN GL.CreditAmount <> 0
                                          OR GL.CreditAmountOriginal <> 0
                                     THEN GL.RefNo
                                     ELSE ''
                                END AS PaymentRefNo ,
                                GL.TypeID ,
								case when GL.Reason is null then
                                GL.Description 
								else GL.Reason 
								end as Description, 
                                GL.ExchangeRate ,
                                GL.DebitAmount ,
                                GL.DebitAmountOriginal ,
                                GL.CreditAmount ,
                                GL.CreditAmountOriginal ,
                                $0 AS ClosingAmount ,
                                $0 AS ClosingAmountOC ,
                                GL.ContactName ,
                                GL.AccountingObjectID ,
                                CASE WHEN GL.DebitAmount <> 0
                                          OR GL.DebitAmountOriginal <> 0 THEN 1
                                     WHEN GL.CreditAmount <> 0
                                          OR GL.CreditAmountOriginal <> 0 THEN 2
                                     ELSE 3
                                END AS InvoiceOrder ,
                                GL.BranchID ,
                                0,
                               GL.AccountCorresponding
                        FROM    dbo.GeneralLedger AS GL ,
						        @tblAccountNumber Acc
                        WHERE   ( GL.PostedDate BETWEEN @FromDate AND @ToDate )
                                AND GL.Account = Acc.AccountNumber
                                AND ( @CurrencyID IS NULL
                                      OR GL.CurrencyID = @CurrencyID
                                    )
                                AND ( GL.CreditAmount <> 0
                                      OR GL.DebitAmount <> 0
                                      OR GL.CreditAmountOriginal <> 0
                                      OR GL.DebitAmountOriginal <> 0
                                    )) T
                        ORDER BY Account,
						        PostedDate ,
                                RefDate ,
                                InvoiceOrder, 
								ReceiptRefNo,
								PaymentRefNo
		
        SET @ClosingAmount = 0
        SET @ClosingAmountOC = 0
        UPDATE  #Result
        SET     @ClosingAmount = @ClosingAmount  + ClosingAmount +ISNULL(DebitAmount, 0)
                - ISNULL(CreditAmount, 0) ,
                ClosingAmount = @ClosingAmount ,
                @ClosingAmountOC = @ClosingAmountOC + ClosingAmountOC + ISNULL(DebitAmountOC, 0)
                - ISNULL(CreditAmountOC, 0) ,
                ClosingAmountOC = @ClosingAmountOC 
        SELECT  *
        FROM    #Result R   
		order by rownum
        DROP TABLE #Result 
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
CREATE PROCEDURE [dbo].[Proc_SA_SoTheoDoiLaiLoTheoMatHang]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @MaterialGoodsID AS NVARCHAR(MAX)
AS
BEGIN          
    DECLARE @tbluutru TABLE (
		MaterialGoodsID UNIQUEIDENTIFIER,
		MaterialGoodsCode NVARCHAR(25),
		MaterialGoodsName NVARCHAR(512),
		Unit NVARCHAR(25),
		Quantity decimal(25,10),
		DoanhSoBan money,
		ChietKhau money,
		GiamGia money,
		TraLai money,
		GiaVon money,
		LaiLo money
    )
    INSERT INTO @tbluutru
		SELECT M.ID as MaterialGoodsID, M.MaterialGoodsCode, null MaterialGoodsName, null Unit, null Quantity, null DoanhSoBan, null ChietKhau,
				null GiamGia, null TraLai, null GiaVon, null LaiLo
		FROM dbo.Func_ConvertStringIntoTable(@MaterialGoodsID,',') tblMatHangSelect LEFT JOIN dbo.MaterialGoods M ON M.ID=tblMatHangSelect.Value
		WHERE tblMatHangSelect.Value in (select SA.MaterialGoodsID from SAInvoiceDetail SA LEFT JOIN GeneralLedger GL on GL.DetailID=SA.ID 
										 where GL.PostedDate between @FromDate and @ToDate)
		
	UPDATE @tbluutru SET MaterialGoodsName = a.MaterialGoodsName, Unit = a.Unit, Quantity=a.Quantity
	FROM ( select ID,MaterialGoodsID as MId, Description as MaterialGoodsName, Unit, Quantity from SAInvoiceDetail) a LEFT JOIN GeneralLedger GL on GL.DetailID=a.ID
	where MaterialGoodsID = a.MId AND (GL.PostedDate between @FromDate and @ToDate)
	

	DECLARE @tbDataDoanhSoBan TABLE(
		MaterialGoodsID UNIQUEIDENTIFIER,
		DoanhSoBan money
	)
	
	INSERT INTO @tbDataDoanhSoBan 
		SELECT a.MaterialGoodsID,SUM(c.CreditAmount) as DoanhSoBan 
		FROM SAInvoiceDetail a LEFT JOIN GeneralLedger c on c.DetailID = a.ID
		WHERE a.MaterialGoodsID in (select MaterialGoodsID from @tbluutru) AND c.Account like '511%' AND (PostedDate between @FromDate and @ToDate)
		GROUP BY a.MaterialGoodsID
	
	UPDATE @tbluutru SET DoanhSoBan = k.DoanhSoBan
	FROM (select MaterialGoodsID as Mid, DoanhSoBan from @tbDataDoanhSoBan) k
	WHERE MaterialGoodsID = k.Mid
	
	DECLARE @tbDataChietKhau TABLE(
		MaterialGoodsID UNIQUEIDENTIFIER,
		ChietKhauBan money,
		ChietKhau_TLGG money,
		ChietKhau money
	)
	
	INSERT INTO @tbDataChietKhau 
		SELECT a.MaterialGoodsID, SUM(c.DebitAmount) as ChietKhauBan, null ChietKhau_TLGG, null ChietKhau
		FROM SAInvoiceDetail a LEFT JOIN GeneralLedger c on c.DetailID = a.ID
		WHERE a.MaterialGoodsID in (select MaterialGoodsID from @tbluutru) AND c.Account like '511%' AND c.TypeID not in (330,340) 
		AND (PostedDate between @FromDate and @ToDate)
		GROUP BY a.MaterialGoodsID
		
	UPDATE @tbDataChietKhau SET ChietKhau_TLGG = g.ChietKhau_TLGG
	FROM (
		SELECT a.MaterialGoodsID as Mid, SUM(c.CreditAmount) as ChietKhau_TLGG
		FROM SAInvoiceDetail a LEFT JOIN GeneralLedger c on c.DetailID = a.ID
		WHERE a.MaterialGoodsID in (select MaterialGoodsID from @tbluutru) AND c.Account like '511%' AND c.TypeID in (330,340) 
		AND (PostedDate between @FromDate and @ToDate)
		GROUP BY a.MaterialGoodsID
	) g
	WHERE MaterialGoodsID=g.Mid
	
	UPDATE @tbDataChietKhau SET ChietKhau = (ISNULL(ChietKhauBan,0) - ISNULL(ChietKhau_TLGG,0))
	
	UPDATE @tbluutru SET ChietKhau = e.ChietKhau
	FROM (select MaterialGoodsID as Mid, ChietKhau from @tbDataChietKhau) e
	WHERE MaterialGoodsID = e.Mid
	
	DECLARE @tbDataGiamGia TABLE(
		MaterialGoodsID UNIQUEIDENTIFIER,
		GiamGia money
	)

	INSERT INTO @tbDataGiamGia 
		SELECT a.MaterialGoodsID,SUM(c.DebitAmount - c.CreditAmount) as GiamGia 
		FROM SAReturnDetail a LEFT JOIN GeneralLedger c on c.DetailID = a.ID
		WHERE a.MaterialGoodsID in (select MaterialGoodsID from @tbluutru) AND c.Account like '511%' AND c.TypeID=340
		 AND (PostedDate between @FromDate and @ToDate)
		GROUP BY a.MaterialGoodsID
	
	UPDATE @tbluutru SET GiamGia = f.GiamGia
	FROM (select MaterialGoodsID as Mid, GiamGia from @tbDataGiamGia) f
	WHERE MaterialGoodsID = f.Mid
	
	DECLARE @tbDataTraLai TABLE(
		MaterialGoodsID UNIQUEIDENTIFIER,
		TraLai money
	)

	INSERT INTO @tbDataTraLai 
		SELECT a.MaterialGoodsID,SUM(c.DebitAmount - c.CreditAmount) as TraLai 
		FROM SAReturnDetail a LEFT JOIN GeneralLedger c on c.DetailID = a.ID
		WHERE a.MaterialGoodsID in (select MaterialGoodsID from @tbluutru) AND c.Account like '511%' AND c.TypeID=330
		 AND (PostedDate between @FromDate and @ToDate)
		GROUP BY a.MaterialGoodsID
	
	UPDATE @tbluutru SET TraLai = h.TraLai
	FROM (select MaterialGoodsID as Mid, TraLai from @tbDataTraLai) h
	WHERE MaterialGoodsID = h.Mid
	
	DECLARE @tbDataGiaVon TABLE(
		MaterialGoodsID UNIQUEIDENTIFIER,
		GiaVon money
	)
	
	INSERT INTO @tbDataGiaVon 
		SELECT a.MaterialGoodsID,SUM(c.DebitAmount - c.CreditAmount) as GiaVon 
		FROM SAInvoiceDetail a LEFT JOIN GeneralLedger c on c.DetailID = a.ID
		WHERE a.MaterialGoodsID in (select MaterialGoodsID from @tbluutru) AND c.Account like '632%'
		 AND (PostedDate between @FromDate and @ToDate)
		GROUP BY a.MaterialGoodsID
		UNION ALL
		SELECT a.MaterialGoodsID,SUM(c.DebitAmount - c.CreditAmount) as GiaVon 
		FROM SAReturnDetail a LEFT JOIN GeneralLedger c on c.DetailID = a.ID
		WHERE a.MaterialGoodsID in (select MaterialGoodsID from @tbluutru) AND c.Account like '632%'
		 AND (PostedDate between @FromDate and @ToDate)
		GROUP BY a.MaterialGoodsID
		
		
	UPDATE @tbluutru SET GiaVon = j.GiaVon
	FROM (select MaterialGoodsID as Mid, SUM(GiaVon) as GiaVon from @tbDataGiaVon group by MaterialGoodsID) j
	WHERE MaterialGoodsID = j.Mid
	
	UPDATE @tbluutru SET LaiLo = ISNULL(DoanhSoBan,0) - (ISNULL(ChietKhau,0)+ISNULL(GiamGia,0)+ISNULL(TraLai,0)+ISNULL(GiaVon,0))
	
	select MaterialGoodsID,	MaterialGoodsCode, MaterialGoodsName, Unit, Quantity, DoanhSoBan, ChietKhau, GiamGia, TraLai, GiaVon, LaiLo
	from @tbluutru WHERE MaterialGoodsID is not null AND ((ISNULL(Quantity,0)>0) OR (ISNULL(DoanhSoBan,0)>0) OR (ISNULL(ChietKhau,0)>0)
	OR (ISNULL(GiamGia,0)>0) OR (ISNULL(TraLai,0)>0) OR (ISNULL(GiaVon,0)>0) OR (ISNULL(LaiLo,0)>0))
	order by MaterialGoodsName
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
CREATE PROCEDURE [dbo].[Proc_SA_SoTheoDoiLaiLoTheoHopDongBan]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @AccountObjectID AS NVARCHAR(MAX)
AS
BEGIN          
	
	DECLARE @tbAccountObjectID TABLE(
		AccountingObjectID UNIQUEIDENTIFIER
	)
	
	INSERT INTO @tbAccountObjectID
	SELECT tblAccOjectSelect.Value as AccountingObjectID
		FROM dbo.Func_ConvertStringIntoTable(@AccountObjectID,',') tblAccOjectSelect
		WHERE tblAccOjectSelect.Value in (SELECT ID FROM AccountingObject)
		
	
    DECLARE @tbluutru TABLE (
		ContractID UNIQUEIDENTIFIER,
		NgayKy Date,
		SoHopDong NVARCHAR(50),
		AccountingObjectID UNIQUEIDENTIFIER,
		DoiTuong NVARCHAR(512),
		TrichYeu NVARCHAR(512),
		GiaTriHopDong money,
		DoanhThuThucTe money,
		GiamTruDoanhThu money,
		GiaVon money,
		LaiLo money
    )
    
    INSERT INTO @tbluutru(ContractID,NgayKy,SoHopDong,AccountingObjectID,DoiTuong,TrichYeu,GiaTriHopDong)
		SELECT EMC.ID as ContractID, EMC.SignedDate as NgayKy, EMC.Code as SoHopDong, EMC.AccountingObjectID, EMC.AccountingObjectName as DoiTuong,
		EMC.Name as TrichYeu, EMC.Amount as GiaTriHopDong
		FROM EMContract EMC
		WHERE EMC.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID)
	
	DECLARE @tbDataDoanhThuThucTe TABLE(
		ContractID UNIQUEIDENTIFIER,
		DoanhThuThucTe money
	)
	
	INSERT INTO @tbDataDoanhThuThucTe(ContractID,DoanhThuThucTe)
		SELECT GL.ContractID, SUM(GL.CreditAmount) as DoanhThuThucTe
		FROM GeneralLedger GL 
		WHERE (GL.Account like '511%') AND (GL.PostedDate between @FromDate and @ToDate) 
		AND GL.ContractID in (select ContractID from @tbluutru)
		GROUP BY GL.ContractID
		
	UPDATE @tbluutru SET DoanhThuThucTe = a.DoanhThuThucTe
	FROM (select ContractID as IDC, DoanhThuThucTe from @tbDataDoanhThuThucTe) a
	WHERE ContractID = a.IDC
	
	DECLARE @tbDataGiamTruDoanhThu TABLE(
		ContractID UNIQUEIDENTIFIER,
		GiamTruDoanhThu money
	)
	
	INSERT INTO @tbDataGiamTruDoanhThu(ContractID,GiamTruDoanhThu)
		SELECT GL.ContractID, SUM(GL.DebitAmount) as GiamTruDoanhThu
		FROM GeneralLedger GL 
		WHERE (GL.Account like '511%') AND (GL.PostedDate between @FromDate and @ToDate) 
		AND GL.ContractID in (select ContractID from @tbluutru)
		GROUP BY GL.ContractID
		
	UPDATE @tbluutru SET GiamTruDoanhThu = b.GiamTruDoanhThu
	FROM (select ContractID as IDC, GiamTruDoanhThu from @tbDataGiamTruDoanhThu) b
	WHERE ContractID = b.IDC
	
	DECLARE @tbDataGiaVon TABLE(
		ContractID UNIQUEIDENTIFIER,
		GiaVon money
	)
	
	INSERT INTO @tbDataGiaVon(ContractID,GiaVon)
		SELECT GL.ContractID, SUM(GL.DebitAmount - GL.CreditAmount) as GiaVon
		FROM GeneralLedger GL 
		WHERE (GL.Account like '632%') AND (GL.PostedDate between @FromDate and @ToDate) 
		AND GL.ContractID in (select ContractID from @tbluutru)
		GROUP BY GL.ContractID
		
	UPDATE @tbluutru SET GiaVon = c.GiaVon
	FROM (select ContractID as IDC, GiaVon from @tbDataGiaVon) c
	WHERE ContractID = c.IDC
	
	UPDATE @tbluutru SET LaiLo = ISNULL(DoanhThuThucTe,0) - (ISNULL(GiamTruDoanhThu,0) + ISNULL(GiaVon,0))
	
    SELECT NgayKy, SoHopDong, DoiTuong, TrichYeu, GiaTriHopDong, DoanhThuThucTe, GiamTruDoanhThu, GiaVon, LaiLo FROM @tbluutru
    WHERE ((ISNULL(DoanhThuThucTe,0)>0) OR (ISNULL(GiamTruDoanhThu,0)>0)
			OR (ISNULL(GiaVon,0)>0) OR (ISNULL(LaiLo,0)>0))
	ORDER BY NgayKy, SoHopDong
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
CREATE PROCEDURE [dbo].[Proc_SA_SoTheoDoiCongNoPhaiThuTheoMatHang]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @MaterialGoodsID AS NVARCHAR(MAX)
AS
BEGIN          
	DECLARE @tbDataMaterialGoodsID TABLE(
		MaterialGoodsID UNIQUEIDENTIFIER
	)
	INSERT INTO @tbDataMaterialGoodsID
		SELECT SA.MaterialGoodsID from SAInvoiceDetail SA LEFT JOIN GeneralLedger GL on GL.DetailID=SA.ID 
		where GL.PostedDate between @FromDate and @ToDate
	
	INSERT INTO @tbDataMaterialGoodsID
		SELECT SA.MaterialGoodsID from SAReturnDetail SA LEFT JOIN GeneralLedger GL on GL.DetailID=SA.ID 
		where GL.PostedDate between @FromDate and @ToDate
	
    DECLARE @tbluutru TABLE (
		MaterialGoodsID UNIQUEIDENTIFIER,
		MaterialGoodsCode NVARCHAR(25),
		MaterialGoodsName NVARCHAR(512),
		GiaTriHang money,
		Thue money,
		ChietKhau money,
		GiamGia money,
		TraLai money,
		DaThanhToan money,
		ConLai money
    )
    INSERT INTO @tbluutru
		SELECT M.ID as MaterialGoodsID, M.MaterialGoodsCode, M.MaterialGoodsName, null GiaTriHang, null Thue, null ChietKhau, null GiamGia,
				null TraLai, null DaThanhToan, null ConLai
		FROM dbo.Func_ConvertStringIntoTable(@MaterialGoodsID,',') tblMatHangSelect LEFT JOIN dbo.MaterialGoods M ON M.ID=tblMatHangSelect.Value
		WHERE tblMatHangSelect.Value in (select distinct MaterialGoodsID from @tbDataMaterialGoodsID)
		
	DECLARE @tbDataGiaTriHang TABLE(
		MaterialGoodsID UNIQUEIDENTIFIER,
		GiaTriHang money
	)
	
	INSERT INTO @tbDataGiaTriHang 
		SELECT a.MaterialGoodsID,SUM(c.DebitAmount) as GiaTriHang 
		FROM SAInvoiceDetail a LEFT JOIN GeneralLedger c on c.DetailID = a.ID
		WHERE a.MaterialGoodsID in (select MaterialGoodsID from @tbluutru) 
		AND c.Account like '131%' AND c.AccountCorresponding like '511%' AND (PostedDate between @FromDate and @ToDate)
		GROUP BY a.MaterialGoodsID
	
	UPDATE @tbluutru SET GiaTriHang = k.GiaTriHang
	FROM (select MaterialGoodsID as Mid, GiaTriHang from @tbDataGiaTriHang) k
	WHERE MaterialGoodsID = k.Mid
	
	DECLARE @tbDataThue TABLE (
		MaterialGoodsID UNIQUEIDENTIFIER,
		Thue money
	)
	
	INSERT INTO @tbDataThue
		SELECT a.MaterialGoodsID,SUM(c.DebitAmount) as Thue 
		FROM SAInvoiceDetail a LEFT JOIN GeneralLedger c on c.DetailID = a.ID
		WHERE a.MaterialGoodsID in (select MaterialGoodsID from @tbluutru) 
		AND c.Account like '131%' AND c.AccountCorresponding like '3331%' AND (PostedDate between @FromDate and @ToDate)
		GROUP BY a.MaterialGoodsID
	
	UPDATE @tbluutru SET Thue = h.Thue
	FROM (select MaterialGoodsID as Mid, Thue from @tbDataThue) h
	WHERE MaterialGoodsID = h.Mid
	
	DECLARE @tbDataChietKhau TABLE(
		MaterialGoodsID UNIQUEIDENTIFIER,
		ChietKhau money
	)
	
	INSERT INTO @tbDataChietKhau 
		SELECT a.MaterialGoodsID, SUM(c.CreditAmount) as ChietKhau
		FROM SAInvoiceDetail a LEFT JOIN GeneralLedger c on c.DetailID = a.ID
		WHERE a.MaterialGoodsID in (select MaterialGoodsID from @tbluutru) 
		AND c.Account like '131%' AND c.AccountCorresponding like '511%' AND c.TypeID not in (330,340) 
		AND (PostedDate between @FromDate and @ToDate)
		GROUP BY a.MaterialGoodsID
	
	UPDATE @tbluutru SET ChietKhau = i.ChietKhau
	FROM (select MaterialGoodsID as Mid, ChietKhau from @tbDataChietKhau) i
	WHERE MaterialGoodsID = i.Mid
	
	DECLARE @tbDataGiamGia TABLE(
		MaterialGoodsID UNIQUEIDENTIFIER,
		GiamGia money
	)

	INSERT INTO @tbDataGiamGia 
		SELECT a.MaterialGoodsID,SUM(c.CreditAmount - c.DebitAmount) as GiamGia 
		FROM SAReturnDetail a LEFT JOIN GeneralLedger c on c.DetailID = a.ID
		WHERE a.MaterialGoodsID in (select MaterialGoodsID from @tbluutru) 
		AND c.Account like '131%' AND c.TypeID=340
		AND (PostedDate between @FromDate and @ToDate)
		GROUP BY a.MaterialGoodsID
	
	UPDATE @tbluutru SET GiamGia = j.GiamGia
	FROM (select MaterialGoodsID as Mid, GiamGia from @tbDataGiamGia) j
	WHERE MaterialGoodsID = j.Mid
	
	DECLARE @tbDataTraLai TABLE(
		MaterialGoodsID UNIQUEIDENTIFIER,
		TraLai money
	)

	INSERT INTO @tbDataTraLai 
		SELECT a.MaterialGoodsID,SUM(c.CreditAmount - c.DebitAmount) as TraLai 
		FROM SAReturnDetail a LEFT JOIN GeneralLedger c on c.DetailID = a.ID
		WHERE a.MaterialGoodsID in (select MaterialGoodsID from @tbluutru) 
		AND c.Account like '131%' AND c.TypeID=330
		AND (PostedDate between @FromDate and @ToDate)
		GROUP BY a.MaterialGoodsID
	
	UPDATE @tbluutru SET TraLai = n.TraLai
	FROM (select MaterialGoodsID as Mid, TraLai from @tbDataTraLai) n
	WHERE MaterialGoodsID = n.Mid
	
	DECLARE @tbDataDaThanhToan TABLE(
		MaterialGoodsID UNIQUEIDENTIFIER,
		DaThanhToan money
	)
	
	INSERT INTO @tbDataDaThanhToan 
		SELECT a.MaterialGoodsID,SUM(c.CreditAmount) as DaThanhToan 
		FROM SAInvoiceDetail a LEFT JOIN GeneralLedger c on c.DetailID = a.ID
		WHERE a.MaterialGoodsID in (select MaterialGoodsID from @tbluutru) 
		AND c.Account like '131%' AND ((AccountCorresponding like '111%') OR (AccountCorresponding like '112%'))
		AND (PostedDate between @FromDate and @ToDate)
		GROUP BY a.MaterialGoodsID
		
	UPDATE @tbluutru SET DaThanhToan = m.DaThanhToan
	FROM (select MaterialGoodsID as Mid, DaThanhToan from @tbDataDaThanhToan) m
	WHERE MaterialGoodsID = m.Mid
	
	UPDATE @tbluutru SET ConLai = ((ISNULL(GiaTriHang,0) + ISNULL(Thue,0)) - (ISNULL(ChietKhau,0)+ISNULL(GiamGia,0)+ISNULL(TraLai,0)+ISNULL(DaThanhToan,0)))
	
	select MaterialGoodsID,	MaterialGoodsCode, MaterialGoodsName, GiaTriHang, Thue, ChietKhau, GiamGia, TraLai, DaThanhToan, ConLai
	from @tbluutru WHERE MaterialGoodsID is not null AND ((ISNULL(GiaTriHang,0)>0) OR (ISNULL(Thue,0)>0) OR (ISNULL(ChietKhau,0)>0)
	OR (ISNULL(GiamGia,0)>0) OR (ISNULL(TraLai,0)>0) OR (ISNULL(DaThanhToan,0)>0) OR (ISNULL(ConLai,0)>0))
	order by MaterialGoodsName
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
CREATE PROCEDURE [dbo].[Proc_SA_SoTheoDoiCongNoPhaiThuTheoHopDongBan]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @AccountObjectID AS NVARCHAR(MAX)
AS
BEGIN          
	
	DECLARE @tbAccountObjectID TABLE(
		AccountingObjectID UNIQUEIDENTIFIER
	)
	
	INSERT INTO @tbAccountObjectID
	SELECT tblAccOjectSelect.Value as AccountingObjectID
		FROM dbo.Func_ConvertStringIntoTable(@AccountObjectID,',') tblAccOjectSelect
		WHERE tblAccOjectSelect.Value in (SELECT ID FROM AccountingObject)
		
	
    DECLARE @tbluutru TABLE (
		ContractID UNIQUEIDENTIFIER,
		NgayKy Date,
		SoHopDong NVARCHAR(50),
		AccountingObjectID UNIQUEIDENTIFIER,
		DoiTuong NVARCHAR(512),
		TrichYeu NVARCHAR(512),
		GiaTriHopDong money,
		GiaTriThucTe money,
		CacKhoanGiamTru money,
		DaThanhToan money,
		ConLai money
    )
    
    INSERT INTO @tbluutru(ContractID,NgayKy,SoHopDong,AccountingObjectID,DoiTuong,TrichYeu,GiaTriHopDong)
		SELECT EMC.ID as ContractID, EMC.SignedDate as NgayKy, EMC.Code as SoHopDong, EMC.AccountingObjectID, EMC.AccountingObjectName as DoiTuong,
		EMC.Name as TrichYeu, EMC.Amount as GiaTriHopDong
		FROM EMContract EMC
		WHERE EMC.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID)
	
	DECLARE @tbDataGiaTriThucTe TABLE(
		ContractID UNIQUEIDENTIFIER,
		GiaTriThucTe money
	)
	
	INSERT INTO @tbDataGiaTriThucTe(ContractID,GiaTriThucTe)
		SELECT GL.ContractID, SUM(GL.DebitAmount) as GiaTriThucTe
		FROM GeneralLedger GL 
		WHERE (GL.Account like '131%') AND ((AccountCorresponding like '511%') OR (AccountCorresponding like '3331%'))
		AND (GL.PostedDate between @FromDate and @ToDate) 
		AND GL.ContractID in (select ContractID from @tbluutru)
		GROUP BY GL.ContractID
		
	UPDATE @tbluutru SET GiaTriThucTe = a.GiaTriThucTe
	FROM (select ContractID as IDC, GiaTriThucTe from @tbDataGiaTriThucTe) a
	WHERE ContractID = a.IDC
	
	DECLARE @tbDataCacKhoanGiamTru TABLE(
		ContractID UNIQUEIDENTIFIER,
		CacKhoanGiamTru money
	)
	
	INSERT INTO @tbDataCacKhoanGiamTru(ContractID,CacKhoanGiamTru)
		SELECT GL.ContractID, SUM(GL.CreditAmount) as CacKhoanGiamTru
		FROM GeneralLedger GL 
		WHERE (GL.Account like '131%') AND (AccountCorresponding like '635%')
		AND (GL.PostedDate between @FromDate and @ToDate) 
		AND GL.ContractID in (select ContractID from @tbluutru)
		GROUP BY GL.ContractID
		
	UPDATE @tbluutru SET CacKhoanGiamTru = b.CacKhoanGiamTru
	FROM (select ContractID as IDC, CacKhoanGiamTru from @tbDataCacKhoanGiamTru) b
	WHERE ContractID = b.IDC
	
	DECLARE @tbDataDaThanhToan TABLE(
		ContractID UNIQUEIDENTIFIER,
		DaThanhToan money
	)
	
	INSERT INTO @tbDataDaThanhToan(ContractID,DaThanhToan)
		SELECT GL.ContractID, SUM(GL.CreditAmount) as DaThanhToan
		FROM GeneralLedger GL 
		WHERE (GL.Account like '131%') AND ((AccountCorresponding like '111%') OR (AccountCorresponding like '112%'))
		AND (GL.PostedDate between @FromDate and @ToDate) 
		AND GL.ContractID in (select ContractID from @tbluutru)
		GROUP BY GL.ContractID
		
	UPDATE @tbluutru SET DaThanhToan = c.DaThanhToan
	FROM (select ContractID as IDC, DaThanhToan from @tbDataDaThanhToan) c
	WHERE ContractID = c.IDC
	
	UPDATE @tbluutru SET ConLai = ISNULL(GiaTriThucTe,0) - (ISNULL(CacKhoanGiamTru,0) + ISNULL(DaThanhToan,0))
	
    SELECT NgayKy, SoHopDong, DoiTuong, TrichYeu, GiaTriHopDong, GiaTriThucTe, CacKhoanGiamTru, DaThanhToan, ConLai FROM @tbluutru
    WHERE ((ISNULL(GiaTriThucTe,0)>0) OR (ISNULL(CacKhoanGiamTru,0)>0)
			OR (ISNULL(DaThanhToan,0)>0) OR (ISNULL(ConLai,0)>0))
	ORDER BY NgayKy, SoHopDong
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
CREATE PROCEDURE [dbo].[Proc_SA_SoTheoDoiCongNoPhaiThuTheoHoaDon]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @AccountObjectID AS NVARCHAR(MAX)
AS
BEGIN          
	
	DECLARE @tbAccountObjectID TABLE(
		AccountingObjectID UNIQUEIDENTIFIER
	)
	
	INSERT INTO @tbAccountObjectID
	SELECT tblAccOjectSelect.Value as AccountingObjectID
		FROM dbo.Func_ConvertStringIntoTable(@AccountObjectID,',') tblAccOjectSelect
		WHERE tblAccOjectSelect.Value in (SELECT ID FROM AccountingObject)
		
	
    DECLARE @tbluutru TABLE (
		DetailID UNIQUEIDENTIFIER,
		InvoiceDate Date,
		InvoiceNo NVARCHAR(25),
		AccountingObjectID UNIQUEIDENTIFIER,
		TienHangVaCK money,
		TienThue money,
		TraLai money,
		ChietKhauTT_GiamTruKhac money
    )
    
    INSERT INTO @tbluutru(DetailID,InvoiceDate,InvoiceNo,AccountingObjectID)
		SELECT DISTINCT GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID
		FROM GeneralLedger GL
		WHERE (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
			AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID))
			AND (GL.DetailID in (select ID from SAInvoiceDetail))
    
    DECLARE @tbTienHangVaCK TABLE(
		DetailID UNIQUEIDENTIFIER,
		InvoiceDate Date,
		InvoiceNo NVARCHAR(25),
		AccountingObjectID UNIQUEIDENTIFIER,
		DebitAmount money,
		CreditAmount money
		)
    
    INSERT INTO @tbTienHangVaCK
    SELECT GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.DebitAmount, GL.CreditAmount
    FROM GeneralLedger GL
    WHERE (GL.Account like '131%') AND (AccountCorresponding like '511%') AND (GL.InvoiceDate is not null) 
		AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '') AND (GL.DetailID in (select ID from SAInvoiceDetail))
		AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID))
    
    UPDATE @tbluutru SET TienHangVaCK = a.TienHangVaCK
    FROM (select DetailID as IvID, InvoiceDate as IvDate, InvoiceNo as IvNo, AccountingObjectID as AOID, SUM(DebitAmount - CreditAmount) as TienHangVaCK
		from @tbTienHangVaCK
		group by DetailID, InvoiceDate, InvoiceNo, AccountingObjectID) a
	WHERE DetailID = a.IvID AND InvoiceDate = a.IvDate AND InvoiceNo = a.IvNo AND AccountingObjectID = a.AOID
	
	DECLARE @tbTienThue TABLE (
		DetailID UNIQUEIDENTIFIER,
		InvoiceDate Date,
		InvoiceNo NVARCHAR(25),
		AccountingObjectID UNIQUEIDENTIFIER,
		DebitAmount money
		)
	
	INSERT INTO @tbTienThue
	SELECT GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.DebitAmount
	FROM GeneralLedger GL
	WHERE (GL.Account like '131%') AND (AccountCorresponding like '3331%') AND (GL.InvoiceDate is not null) 
		  AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '') AND (GL.DetailID in (select ID from SAInvoiceDetail))
		  AND (GL.PostedDate between @FromDate and @ToDate) AND GL.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID)
	
	UPDATE @tbluutru SET TienThue = b.TienThue
	FROM (select DetailID as IvID, InvoiceDate as IvDate, InvoiceNo as IvNo, AccountingObjectID as AOID, SUM(DebitAmount) as TienThue
		  from @tbTienThue
		  group by DetailID, InvoiceDate, InvoiceNo, AccountingObjectID) b
	WHERE DetailID = b.IvID AND InvoiceDate = b.IvDate AND InvoiceNo = b.IvNo AND AccountingObjectID = b.AOID
	
	DECLARE @tbDataTraLai TABLE(
		SAInvoiceDetailID UNIQUEIDENTIFIER,
		TraLai money
	)
	INSERT INTO @tbDataTraLai(SAInvoiceDetailID,TraLai)
		SELECT SAR.SAInvoiceDetailID, SUM(GL.CreditAmount - GL.DebitAmount) as TraLai
		FROM GeneralLedger GL LEFT JOIN SAReturnDetail SAR ON GL.DetailID = SAR.ID
		WHERE (GL.Account like '131%') AND (GL.TypeID=330) AND (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
		AND (GL.PostedDate between @FromDate and @ToDate)
		AND GL.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID)
		GROUP BY SAR.SAInvoiceDetailID
	
	UPDATE @tbluutru SET TraLai = TL.TraLai
	FROM (select SAInvoiceDetailID, TraLai from @tbDataTraLai) TL
	WHERE DetailID = TL.SAInvoiceDetailID
	
	DECLARE @tbChietKhauTT_GiamTruKhac TABLE(
		DetailID UNIQUEIDENTIFIER,
		InvoiceDate Date,
		InvoiceNo NVARCHAR(25),
		AccountingObjectID UNIQUEIDENTIFIER,
		CreditAmount money
	)
	
	INSERT INTO @tbChietKhauTT_GiamTruKhac
	SELECT GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.CreditAmount
	FROM GeneralLedger GL
	WHERE (GL.Account like '131%') AND (GL.AccountCorresponding like '635%')
		  AND (GL.TypeID not in (330,340)) AND (GL.DetailID in (select ID from SAInvoiceDetail))
		  AND (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '') 
		  AND (GL.PostedDate between @FromDate and @ToDate) AND GL.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID)
	
	UPDATE @tbluutru SET ChietKhauTT_GiamTruKhac = c.ChietKhauTT_GiamTruKhac
	FROM (select DetailID as IvID, InvoiceDate as IvDate, InvoiceNo as IvNo, AccountingObjectID as AOID, SUM(CreditAmount) as ChietKhauTT_GiamTruKhac
		  from @tbChietKhauTT_GiamTruKhac 
		  group by DetailID, InvoiceDate, InvoiceNo, AccountingObjectID) c
	WHERE DetailID = c.IvID AND InvoiceDate = c.IvDate AND InvoiceNo = c.IvNo AND AccountingObjectID = c.AOID
	
	
	DECLARE @tbDataReturn TABLE (
		InvoiceDate Date,
		InvoiceNo NVARCHAR(25),
		AccountingObjectID UNIQUEIDENTIFIER,
		AccountingObjectName NVARCHAR(512),
		TienHangVaCK money,
		TienThue money,
		GiaTriHoaDon money,
		TraLai money,
		GiamGia money,
		ChietKhauTT_GiamTruKhac money,
		SoDaThu money,
		SoConPhaiThu money
    )
    
    INSERT INTO @tbDataReturn(InvoiceDate,InvoiceNo,AccountingObjectID,TienHangVaCK,TienThue,TraLai,ChietKhauTT_GiamTruKhac)
    SELECT InvoiceDate,InvoiceNo,AccountingObjectID,SUM(TienHangVaCK) as TienHangVaCK, SUM(TienThue) as TienThue,
    SUM(TraLai) as TraLai, SUM(ChietKhauTT_GiamTruKhac) as ChietKhauTT_GiamTruKhac
    FROM @tbluutru
    GROUP BY InvoiceDate,InvoiceNo,AccountingObjectID
    
	INSERT INTO @tbDataReturn(InvoiceDate,InvoiceNo,AccountingObjectID,GiamGia)
		SELECT GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, SUM(GL.CreditAmount - GL.DebitAmount) as GiamGia
		FROM GeneralLedger GL 
		WHERE (GL.Account like '131%') AND (GL.TypeID=340) AND (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
		AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAReturnDetail)) 
		AND GL.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID)
		GROUP BY GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID
		
	UPDATE @tbDataReturn SET SoDaThu = d.SoDaThu
	FROM (select SA.InvoiceDate as IvDate, SA.InvoiceNo as IvNo, SA.AccountingObjectID as AOID, SUM(GL.CreditAmount) as SoDaThu
		  from GeneralLedger GL LEFT JOIN MCReceipt MC ON GL.ReferenceID = MC.ID
			LEFT JOIN MCReceiptDetailCustomer MCC ON MCC.MCReceiptID = MC.ID
			LEFT JOIN SAInvoice SA ON SA.ID = MCC.SaleInvoiceID
		  where (GL.Account like '131%') AND ((GL.AccountCorresponding like '111%') OR (GL.AccountCorresponding like '112%')) 
		  AND (SA.InvoiceDate is not null) AND (SA.InvoiceNo is not null) AND (SA.InvoiceNo <> '') 
		  AND (GL.PostedDate between @FromDate and @ToDate) AND SA.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID)
		  group by SA.InvoiceDate, SA.InvoiceNo, SA.AccountingObjectID
		  
		  UNION ALL
		  
		  select SA.InvoiceDate as IvDate, SA.InvoiceNo as IvNo, SA.AccountingObjectID as AOID, SUM(GL.CreditAmount) as SoDaThu
		  from GeneralLedger GL LEFT JOIN MBDeposit MB ON GL.ReferenceID = MB.ID
			LEFT JOIN MBDepositDetailCustomer MBC ON MBC.MBDepositID = MB.ID
			LEFT JOIN SAInvoice SA ON SA.ID = MBC.SaleInvoiceID
		  where (GL.Account like '131%') AND ((GL.AccountCorresponding like '111%') OR (GL.AccountCorresponding like '112%')) 
		  AND (SA.InvoiceDate is not null) AND (SA.InvoiceNo is not null) AND (SA.InvoiceNo <> '') 
		  AND (GL.PostedDate between @FromDate and @ToDate) AND SA.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID)
		  group by SA.InvoiceDate, SA.InvoiceNo, SA.AccountingObjectID
		  ) d
	WHERE InvoiceDate = d.IvDate AND InvoiceNo = d.IvNo AND AccountingObjectID = d.AOID
    
    UPDATE @tbDataReturn SET AccountingObjectName = AJ.AccountingObjectName
	FROM ( select ID,AccountingObjectName from AccountingObject) AJ
	where AccountingObjectID = AJ.ID
    
    UPDATE @tbDataReturn SET GiaTriHoaDon = (ISNULL(TienHangVaCK,0) + ISNULL(TienThue,0))
    
    UPDATE @tbDataReturn SET SoConPhaiThu = (ISNULL(GiaTriHoaDon,0) - (ISNULL(TraLai,0)+ISNULL(GiamGia,0)+ISNULL(ChietKhauTT_GiamTruKhac,0)+ISNULL(SoDaThu,0)))
    
    SELECT InvoiceDate,InvoiceNo,AccountingObjectName,GiaTriHoaDon,TraLai,GiamGia,ChietKhauTT_GiamTruKhac,SoDaThu,SoConPhaiThu FROM @tbDataReturn
    WHERE ((ISNULL(GiaTriHoaDon,0)>0) OR (ISNULL(TraLai,0)>0) OR (ISNULL(GiamGia,0)>0) OR (ISNULL(ChietKhauTT_GiamTruKhac,0)>0)
			OR (ISNULL(SoDaThu,0)>0) OR (ISNULL(SoConPhaiThu,0)>0))
	ORDER BY InvoiceDate,InvoiceNo
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
CREATE PROCEDURE [dbo].[Proc_SA_SoCongCuDungCu]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @ToolsIDs AS NVARCHAR(MAX)
AS
BEGIN          
    DECLARE @tbluutru TABLE (
		ToolsID UNIQUEIDENTIFIER,
		ToolsCode NVARCHAR(25),
		ToolsName NVARCHAR(255),
		IncrementDate Date,
		PostedDate Date,
		Amount decimal(19,4),
		AllocationTimes int,
		AllocatedAmount decimal(19,4),
		DecrementAllocationTime int,
		RemainingAllocationTime int,
		DecrementAmount decimal(19,4),
		RemainingAmount decimal(19,4)
    )
    INSERT INTO @tbluutru(ToolsID, ToolsCode, ToolsName, Amount, AllocationTimes, AllocatedAmount, DecrementAllocationTime)
		SELECT T.ID as ToolsID, T.ToolsCode, T.ToolsName, T.Amount, T.AllocationTimes, T.AllocatedAmount, T.RemainAllocationTimes as DecrementAllocationTime
		FROM dbo.Func_ConvertStringIntoTable(@ToolsIDs,',') tblCodeSelect LEFT JOIN dbo.TIInit T ON T.ID = tblCodeSelect.Value
		WHERE tblCodeSelect.Value is not null AND T.ID in (select DISTINCT ToolsID from ToolLedger where PostedDate <= @ToDate)
	
	UPDATE @tbluutru SET IncrementDate = a.IncrementDate
	FROM (Select DISTINCT ToolsID as Tid, IncrementDate From ToolLedger where ToolsID in (select ToolsID from @tbluutru) and (PostedDate <= @ToDate)) a
	WHERE ToolsID = a.Tid
	
	UPDATE @tbluutru SET PostedDate = b.PostedDate
	FROM (Select DISTINCT ToolsID as Tid, PostedDate From ToolLedger 
		  where ToolsID in (select ToolsID from @tbluutru) And TypeID = 431 and (PostedDate <= @ToDate)) b
	WHERE ToolsID = b.Tid
	
	UPDATE @tbluutru SET DecrementAllocationTime = ISNULL(DecrementAllocationTime,0) + ISNULL(c.skdapb,0)
	FROM (Select DISTINCT ToolsID as Tid, SUM(DecrementAllocationTime) as skdapb From ToolLedger 
		  where ToolsID in (select ToolsID from @tbluutru) and (PostedDate <= @ToDate) and TypeID = 434
		  group by ToolsID) c
	WHERE ToolsID = c.Tid
	
	DECLARE @id UNIQUEIDENTIFIER
	
	DECLARE cursorConLai CURSOR FOR
	SELECT ToolsID FROM @tbluutru
	OPEN cursorConLai
	FETCH NEXT FROM cursorConLai INTO @id
	WHILE @@FETCH_STATUS = 0
	BEGIN
		UPDATE @tbluutru SET RemainingAmount = d.RemainingAmount
		FROM (select Top 1 ToolsID as Tid, RemainingAmount from ToolLedger where ToolsID = @id and (PostedDate <= @ToDate) order by PostedDate DESC) d
		WHERE ToolsID = d.Tid
		FETCH NEXT FROM cursorConLai INTO @id
	END
	CLOSE cursorConLai
	DEALLOCATE cursorConLai
	
	UPDATE @tbluutru SET RemainingAllocationTime = ISNULL(AllocationTimes,0) - ISNULL(DecrementAllocationTime,0),
						 DecrementAmount = ISNULL(Amount,0) - ISNULL(RemainingAmount,0)
	
	select * from @tbluutru
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[TM032ATNDN] (
  [ID] [uniqueidentifier] NOT NULL,
  [TM03TNDNID] [uniqueidentifier] NULL,
  [Year] [int] NULL,
  [LostAmount] [money] NULL,
  [LostAmountTranferPreviousPeriods] [money] NULL,
  [LostAmountTranferThisPeriod] [money] NULL,
  [LostAmountRemain] [money] NULL,
  [OrderPriority] [int] NULL,
  PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
CREATE PROCEDURE [dbo].[Proc_SA_SoTheoDoiThanhToanBangNgoaiTe]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @TypeMoney NVARCHAR(3),
    @Account NVARCHAR(25),
    @AccountObjectID AS NVARCHAR(MAX)
AS
BEGIN          
	DECLARE @sqlexc nvarchar(max)
	set @sqlexc = N''
	
	set @sqlexc = @sqlexc + N' DECLARE @tbAccountObjectID TABLE('
	set @sqlexc = @sqlexc + N'	AccountingObjectID UNIQUEIDENTIFIER'
	set @sqlexc = @sqlexc + N')'
	
	set @sqlexc = @sqlexc + N' INSERT INTO @tbAccountObjectID'
	set @sqlexc = @sqlexc + N' SELECT tblAccOjectSelect.Value as AccountingObjectID'
	set @sqlexc = @sqlexc + N'	FROM dbo.Func_ConvertStringIntoTable('''+@AccountObjectID+''','','') tblAccOjectSelect'
	set @sqlexc = @sqlexc + N'	WHERE tblAccOjectSelect.Value in (SELECT ID FROM AccountingObject)'
	
	select @sqlexc = @sqlexc + N' DECLARE @tbDataDauKy TABLE('
	select @sqlexc = @sqlexc + N' AccountingObjectID UNIQUEIDENTIFIER,'
	select @sqlexc = @sqlexc + N' Account nvarchar(25),'
	select @sqlexc = @sqlexc + N' SoDuDauKySoTien money,'
	select @sqlexc = @sqlexc + N' SoDuDauKyQuyDoi money'
	select @sqlexc = @sqlexc + N')'
	
	select @sqlexc = @sqlexc + N' INSERT INTO @tbDataDauKy(AccountingObjectID, Account, SoDuDauKySoTien, SoDuDauKyQuyDoi)'
	select @sqlexc = @sqlexc + N' SELECT GL.AccountingObjectID, GL.Account, SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal) as SoDuDauKySoTien,'
	select @sqlexc = @sqlexc + N' SUM(GL.DebitAmount - GL.CreditAmount) as SoDuDauKyQuyDoi'
	select @sqlexc = @sqlexc + N' FROM GeneralLedger GL'
	select @sqlexc = @sqlexc + N' WHERE (GL.AccountingObjectID in (Select AccountingObjectID from @tbAccountObjectID))'
	select @sqlexc = @sqlexc + N' AND GL.PostedDate < '''+CONVERT(nvarchar(25),@FromDate,101)+''' AND (GL.CurrencyID = '''+@TypeMoney+''')'
	
	if(@Account='all')
	begin
		set @sqlexc = @sqlexc + N' AND ((GL.Account like ''131%'') OR (GL.Account like ''141%'') OR (GL.Account like ''331%''))'
	end
	else
	begin
		set @sqlexc = @sqlexc + N' AND (GL.Account like '''+@Account+'%'')'
	end
	
	select @sqlexc = @sqlexc + N' GROUP BY GL.AccountingObjectID, GL.Account'
	
	
	set @sqlexc = @sqlexc + N' DECLARE @tbDataReturn TABLE('
	set @sqlexc = @sqlexc + N'	stt bigint,'
	set @sqlexc = @sqlexc + N'	IdGroup smallint,'
	set @sqlexc = @sqlexc + N'	AccountingObjectID UNIQUEIDENTIFIER,'
	set @sqlexc = @sqlexc + N'	AccountingObjectName nvarchar(512),'
	set @sqlexc = @sqlexc + N'	Account nvarchar(25),'
	set @sqlexc = @sqlexc + N'	NgayHoachToan Date,'
	set @sqlexc = @sqlexc + N'	NgayChungTu Date,'
	set @sqlexc = @sqlexc + N'	SoChungTu nvarchar(25),'
	set @sqlexc = @sqlexc + N'	DienGiai nvarchar(512),'
	set @sqlexc = @sqlexc + N'	TKDoiUng nvarchar(512),'
	set @sqlexc = @sqlexc + N'	TyGiaHoiDoai decimal(25,10),'
	set @sqlexc = @sqlexc + N'	PSNSoTien money,'
	set @sqlexc = @sqlexc + N'	PSNQuyDoi money,'
	set @sqlexc = @sqlexc + N'	PSCSoTien money,'
	set @sqlexc = @sqlexc + N'	PSCQuyDoi money,'
	set @sqlexc = @sqlexc + N'	DuNoSoTien money,'
	set @sqlexc = @sqlexc + N'	DuNoQuyDoi money,'
	set @sqlexc = @sqlexc + N'	DuCoSoTien money,'
	set @sqlexc = @sqlexc + N'	DuCoQuyDoi money,'
	set @sqlexc = @sqlexc + N'	TonSoTien money,'
	set @sqlexc = @sqlexc + N'	TonQuyDoi money'
	set @sqlexc = @sqlexc + N')'
	
	set @sqlexc = @sqlexc + N' DECLARE @IdGroup1 smallint'
	set @sqlexc = @sqlexc + N' set @IdGroup1 = 5;'
	
	set @sqlexc = @sqlexc + N' INSERT INTO @tbDataReturn(stt,IdGroup, AccountingObjectID, Account, NgayHoachToan, NgayChungTu, SoChungTu, DienGiai, TKDoiUng, TyGiaHoiDoai,'
	set @sqlexc = @sqlexc + N' PSNSoTien, PSNQuyDoi, PSCSoTien, PSCQuyDoi)'
	set @sqlexc = @sqlexc + N' SELECT ROW_NUMBER() OVER(ORDER BY GL.AccountingObjectID, GL.Account, GL.PostedDate) as stt, @IdGroup1 as IdGroup, GL.AccountingObjectID, Gl.Account, GL.PostedDate as NgayHoachToan, GL.Date as NgayChungTu, GL.No as SoChungTu,'
	set @sqlexc = @sqlexc + N' GL.Description as DienGiai, GL.AccountCorresponding as TKDoiUng, GL.ExchangeRate as TyGiaHoiDoai, GL.DebitAmountOriginal as PSNSoTien,'
	set @sqlexc = @sqlexc + N' GL.DebitAmount as PSNQuyDoi, Gl.CreditAmountOriginal as PSCSoTien, GL.CreditAmount as PSCQuyDoi'
	set @sqlexc = @sqlexc + N' FROM GeneralLedger GL'
	set @sqlexc = @sqlexc + N' WHERE (GL.PostedDate Between '''+CONVERT(nvarchar(25),@FromDate,101)+''' And '''+CONVERT(nvarchar(25),@ToDate,101)+''') AND (GL.CurrencyID = '''+@TypeMoney+''')'
	set @sqlexc = @sqlexc + N' AND (GL.AccountingObjectID in (Select AccountingObjectID from @tbAccountObjectID))'
	
	if(@Account='all')
	begin
		set @sqlexc = @sqlexc + N' AND ((GL.Account like ''131%'') OR (GL.Account like ''141%'') OR (GL.Account like ''331%''))'
	end
	else
	begin
		set @sqlexc = @sqlexc + N' AND (GL.Account like '''+@Account+'%'')'
	end
	
	set @sqlexc = @sqlexc + N' DECLARE @stt1 smallint'
	set @sqlexc = @sqlexc + N' set @stt1 = 0;'
	
	
	set @sqlexc = @sqlexc + N' Declare @SoDuDauKySoTien money'
	set @sqlexc = @sqlexc + N' Set @SoDuDauKySoTien = 0'
	set @sqlexc = @sqlexc + N' Declare @SoDuDauKyQuyDoi money'
	set @sqlexc = @sqlexc + N' Set @SoDuDauKyQuyDoi = 0'
	
	set @sqlexc = @sqlexc + N' INSERT INTO @tbDataReturn(stt,IdGroup, AccountingObjectID, Account, DienGiai, TonSoTien, TonQuyDoi)'
	set @sqlexc = @sqlexc + N' SELECT @stt1, @IdGroup1, AccountingObjectID, Account, N''Số dư đầu kỳ'''
	set @sqlexc = @sqlexc + N' ,@SoDuDauKySoTien, @SoDuDauKyQuyDoi'
	set @sqlexc = @sqlexc + N' FROM @tbDataReturn'
	set @sqlexc = @sqlexc + N' GROUP BY AccountingObjectID, Account'
	
	set @sqlexc = @sqlexc + N' DECLARE @iIDDauKy UNIQUEIDENTIFIER'
	set @sqlexc = @sqlexc + N' DECLARE @iAccountDauKy NVARCHAR(25)'
	set @sqlexc = @sqlexc + N' DECLARE @iTonSoTienDauKy money'
	set @sqlexc = @sqlexc + N' DECLARE @iTonQuyDoiDauKy money'
	
	set @sqlexc = @sqlexc + N' DECLARE cursorDauKy CURSOR FOR'
	set @sqlexc = @sqlexc + N' SELECT AccountingObjectID, Account, SoDuDauKySoTien, SoDuDauKyQuyDoi FROM @tbDataDauKy'
	set @sqlexc = @sqlexc + N' OPEN cursorDauKy'
	set @sqlexc = @sqlexc + N' FETCH NEXT FROM cursorDauKy INTO @iIDDauKy, @iAccountDauKy, @iTonSoTienDauKy, @iTonQuyDoiDauKy'
	set @sqlexc = @sqlexc + N' WHILE @@FETCH_STATUS = 0'
	set @sqlexc = @sqlexc + N' BEGIN'
		set @sqlexc = @sqlexc + N' Declare @countCheck int'
		set @sqlexc = @sqlexc + N' set @countCheck = (select count(AccountingObjectID) from @tbDataReturn where AccountingObjectID = @iIDDauKy and Account = @iAccountDauKy)'
		
		set @sqlexc = @sqlexc + N' If(@countCheck > 0)'
		set @sqlexc = @sqlexc + N' Begin'
			set @sqlexc = @sqlexc + N' Update @tbDataReturn Set TonSoTien = @iTonSoTienDauKy, TonQuyDoi = @iTonQuyDoiDauKy' 
			set @sqlexc = @sqlexc + N' where AccountingObjectID = @iIDDauKy and Account = @iAccountDauKy'
		set @sqlexc = @sqlexc + N' End'
		set @sqlexc = @sqlexc + N' Else'
		set @sqlexc = @sqlexc + N' Begin'
			set @sqlexc = @sqlexc + N' Insert Into @tbDataReturn(stt,IdGroup, AccountingObjectID, Account, DienGiai, TonSoTien, TonQuyDoi)'
			set @sqlexc = @sqlexc + N' Values(@stt1, @IdGroup1, @iIDDauKy, @iAccountDauKy, N''Số dư đầu kỳ'', @iTonSoTienDauKy, @iTonQuyDoiDauKy)'
		set @sqlexc = @sqlexc + N' End'
		set @sqlexc = @sqlexc + N' FETCH NEXT FROM cursorDauKy INTO @iIDDauKy, @iAccountDauKy, @iTonSoTienDauKy, @iTonQuyDoiDauKy'
	set @sqlexc = @sqlexc + N' END'
	set @sqlexc = @sqlexc + N' CLOSE cursorDauKy'
	set @sqlexc = @sqlexc + N' DEALLOCATE cursorDauKy'
	
	
	set @sqlexc = @sqlexc + N' DECLARE @iID UNIQUEIDENTIFIER'
	set @sqlexc = @sqlexc + N' DECLARE @iAccount NVARCHAR(25)'
	set @sqlexc = @sqlexc + N' DECLARE @iTonSoTien money'
	set @sqlexc = @sqlexc + N' DECLARE @iTonQuyDoi money'
	
	set @sqlexc = @sqlexc + N' DECLARE cursorTon CURSOR FOR'
	set @sqlexc = @sqlexc + N' SELECT AccountingObjectID, Account, TonSoTien, TonQuyDoi FROM @tbDataReturn WHERE stt = 0'
	set @sqlexc = @sqlexc + N' OPEN cursorTon'
	set @sqlexc = @sqlexc + N' FETCH NEXT FROM cursorTon INTO @iID, @iAccount, @iTonSoTien, @iTonQuyDoi'
	set @sqlexc = @sqlexc + N' WHILE @@FETCH_STATUS = 0'
	set @sqlexc = @sqlexc + N' BEGIN'
		set @sqlexc = @sqlexc + N' Declare @tonSoTien money'
		set @sqlexc = @sqlexc + N' set @tonSoTien = ISNULL(@iTonSoTien,0)'
		set @sqlexc = @sqlexc + N' Declare @tonQuyDoi money'
		set @sqlexc = @sqlexc + N' set @tonQuyDoi = ISNULL(@iTonQuyDoi,0)'
		
		set @sqlexc = @sqlexc + N' Declare @minStt bigint'
		set @sqlexc = @sqlexc + N' set @minStt = (select MIN(stt) from @tbDataReturn where AccountingObjectID = @iID and Account = @iAccount and stt > 0)'
		set @sqlexc = @sqlexc + N' Declare @maxStt bigint'
		set @sqlexc = @sqlexc + N' set @maxStt = (select MAX(stt) from @tbDataReturn where AccountingObjectID = @iID and Account = @iAccount and stt > 0)'
		set @sqlexc = @sqlexc + N' WHILE @minStt <= @maxStt'
		set @sqlexc = @sqlexc + N' Begin'
			set @sqlexc = @sqlexc + N' set @tonSoTien = @tonSoTien + ISNULL((select PSNSoTien from @tbDataReturn where stt = @minStt),0) - ISNULL((select PSCSoTien from @tbDataReturn where stt = @minStt),0)'
			set @sqlexc = @sqlexc + N' set @tonQuyDoi = @tonQuyDoi + ISNULL((select PSNQuyDoi from @tbDataReturn where stt = @minStt),0) - ISNULL((select PSCQuyDoi from @tbDataReturn where stt = @minStt),0)'
			set @sqlexc = @sqlexc + N' UPDATE @tbDataReturn SET TonSoTien = @tonSoTien, TonQuyDoi = @tonQuyDoi WHERE stt = @minStt'
			set @sqlexc = @sqlexc + N' set @minStt = @minStt + 1'
		set @sqlexc = @sqlexc + N' End'
		set @sqlexc = @sqlexc + N' FETCH NEXT FROM cursorTon INTO @iID, @iAccount, @iTonSoTien, @iTonQuyDoi'
	set @sqlexc = @sqlexc + N' END'
	set @sqlexc = @sqlexc + N' CLOSE cursorTon'
	set @sqlexc = @sqlexc + N' DEALLOCATE cursorTon'
	
	set @sqlexc = @sqlexc + N' DECLARE @IdGroup3 smallint'
	set @sqlexc = @sqlexc + N' SET @IdGroup3 = 3'
	
	set @sqlexc = @sqlexc + N' INSERT INTO @tbDataReturn(IdGroup, AccountingObjectID, Account, PSNSoTien, PSNQuyDoi, PSCSoTien, PSCQuyDoi)'
	set @sqlexc = @sqlexc + N' SELECT @IdGroup3 as IdGroup, AccountingObjectID, Account, SUM(PSNSoTien) as PSNSoTien, SUM(PSNQuyDoi) as PSNQuyDoi'
	set @sqlexc = @sqlexc + N' , SUM(PSCSoTien) as PSCSoTien, SUM(PSCQuyDoi) as PSCQuyDoi'
	set @sqlexc = @sqlexc + N' FROM @tbDataReturn WHERE IdGroup = 5'
	set @sqlexc = @sqlexc + N' GROUP BY AccountingObjectID, Account'
	
	set @sqlexc = @sqlexc + N' DECLARE @iID1 UNIQUEIDENTIFIER'
	set @sqlexc = @sqlexc + N' DECLARE @iAccount1 NVARCHAR(25)'
	
	set @sqlexc = @sqlexc + N' DECLARE cursorTon1 CURSOR FOR'
	set @sqlexc = @sqlexc + N' SELECT AccountingObjectID, Account FROM @tbDataReturn WHERE stt = 0'
	set @sqlexc = @sqlexc + N' OPEN cursorTon1'
	set @sqlexc = @sqlexc + N' FETCH NEXT FROM cursorTon1 INTO @iID1, @iAccount1'
	set @sqlexc = @sqlexc + N' WHILE @@FETCH_STATUS = 0'
	set @sqlexc = @sqlexc + N' BEGIN'
		set @sqlexc = @sqlexc + N' Declare @maxStt1 bigint'
		set @sqlexc = @sqlexc + N' set @maxStt1 = (select MAX(stt) from @tbDataReturn where AccountingObjectID = @iID1 and Account = @iAccount1 and stt > 0)'
		set @sqlexc = @sqlexc + N' If(@maxStt1 > 0)'
		set @sqlexc = @sqlexc + N' Begin'
			set @sqlexc = @sqlexc + N' UPDATE @tbDataReturn SET TonSoTien = (select TonSoTien from @tbDataReturn where stt = @maxStt1)'
			set @sqlexc = @sqlexc + N' ,TonQuyDoi = (select TonQuyDoi from @tbDataReturn where stt = @maxStt1) WHERE AccountingObjectID = @iID1'
			set @sqlexc = @sqlexc + N' AND Account = @iAccount1 AND IdGroup = 3'
		set @sqlexc = @sqlexc + N' End'
		set @sqlexc = @sqlexc + N' Else'
		set @sqlexc = @sqlexc + N' Begin'
			set @sqlexc = @sqlexc + N' UPDATE @tbDataReturn SET TonSoTien = (select TonSoTien from @tbDataReturn where stt = 0 and AccountingObjectID = @iID1 and Account = @iAccount1)'
			set @sqlexc = @sqlexc + N' ,TonQuyDoi = (select TonQuyDoi from @tbDataReturn where stt = 0 and AccountingObjectID = @iID1 and Account = @iAccount1) WHERE AccountingObjectID = @iID1'
			set @sqlexc = @sqlexc + N' AND Account = @iAccount1 AND IdGroup = 3'
		set @sqlexc = @sqlexc + N' End'
		set @sqlexc = @sqlexc + N' FETCH NEXT FROM cursorTon1 INTO @iID1, @iAccount1'
	set @sqlexc = @sqlexc + N' END'
	set @sqlexc = @sqlexc + N' CLOSE cursorTon1'
	set @sqlexc = @sqlexc + N' DEALLOCATE cursorTon1'
	
	set @sqlexc = @sqlexc + N' UPDATE @tbDataReturn SET DuNoSoTien = TonSoTien WHERE TonSoTien > 0'
	set @sqlexc = @sqlexc + N' UPDATE @tbDataReturn SET DuCoSoTien = TonSoTien WHERE TonSoTien < 0'
	
	set @sqlexc = @sqlexc + N' UPDATE @tbDataReturn SET DuNoQuyDoi = TonQuyDoi WHERE TonQuyDoi > 0'
	set @sqlexc = @sqlexc + N' UPDATE @tbDataReturn SET DuCoQuyDoi = TonQuyDoi WHERE TonQuyDoi < 0'
	
	set @sqlexc = @sqlexc + N' UPDATE @tbDataReturn SET DuCoSoTien = (-1 * DuCoSoTien) WHERE DuCoSoTien < 0'
	set @sqlexc = @sqlexc + N' UPDATE @tbDataReturn SET DuCoQuyDoi = (-1 * DuCoQuyDoi) WHERE DuCoQuyDoi < 0'
	
	set @sqlexc = @sqlexc + N' DECLARE @IdGroup2 smallint'
	set @sqlexc = @sqlexc + N' SET @IdGroup2 = 1'
	
	set @sqlexc = @sqlexc + N' INSERT INTO @tbDataReturn(IdGroup, AccountingObjectID, PSNSoTien, PSNQuyDoi, PSCSoTien, PSCQuyDoi'
	set @sqlexc = @sqlexc + N' , DuNoSoTien, DuNoQuyDoi, DuCoSoTien, DuCoQuyDoi)'
	set @sqlexc = @sqlexc + N' SELECT @IdGroup2 as IdGroup, AccountingObjectID, SUM(PSNSoTien) as PSNSoTien, SUM(PSNQuyDoi) as PSNQuyDoi'
	set @sqlexc = @sqlexc + N' , SUM(PSCSoTien) as PSCSoTien, SUM(PSCQuyDoi) as PSCQuyDoi, SUM(DuNoSoTien) as DuNoSoTien'
	set @sqlexc = @sqlexc + N' , SUM(DuNoQuyDoi) as DuNoQuyDoi, SUM(DuCoSoTien) as DuCoSoTien, SUM(DuCoQuyDoi) as DuCoQuyDoi'
	set @sqlexc = @sqlexc + N' FROM @tbDataReturn WHERE IdGroup = 3'
	set @sqlexc = @sqlexc + N' GROUP BY AccountingObjectID'
	
	set @sqlexc = @sqlexc + N' UPDATE @tbDataReturn SET AccountingObjectName = AJ.AccountingObjectName'
	set @sqlexc = @sqlexc + N' FROM ( select ID,AccountingObjectName from AccountingObject) AJ'
	set @sqlexc = @sqlexc + N' where AccountingObjectID = AJ.ID'
	
	set @sqlexc = @sqlexc + N' SELECT stt,IdGroup, AccountingObjectID, AccountingObjectName, Account, NgayHoachToan, NgayChungTu, SoChungTu, DienGiai, TKDoiUng, TyGiaHoiDoai'
	set @sqlexc = @sqlexc + N',PSNSoTien, PSNQuyDoi, PSCSoTien, PSCQuyDoi, DuNoSoTien, DuNoQuyDoi, DuCoSoTien, DuCoQuyDoi'
	set @sqlexc = @sqlexc + N' FROM @tbDataReturn'
	set @sqlexc = @sqlexc + N' WHERE (ISNULL(PSNSoTien,0) > 0) OR (ISNULL(PSNQuyDoi,0) > 0) OR (ISNULL(PSCSoTien,0) > 0) OR (ISNULL(PSCQuyDoi,0) > 0)'
	set @sqlexc = @sqlexc + N' OR (ISNULL(DuNoSoTien,0) > 0) OR (ISNULL(DuNoQuyDoi,0) > 0) OR (ISNULL(DuCoSoTien,0) > 0) OR (ISNULL(DuCoQuyDoi,0) > 0)'
	set @sqlexc = @sqlexc + N' ORDER BY AccountingObjectID, Account, IdGroup, stt'
	
	
	EXECUTE sp_executesql @sqlexc
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO


ALTER TABLE dbo.GenCode
  DROP CONSTRAINT FK_GenCode_TypeGroup
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.Template
  DROP CONSTRAINT FK_Template_Type
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateColumn
  DROP CONSTRAINT FK_TemplateColumn_TemplateDetail
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateDetail
  DROP CONSTRAINT FK_TemplateDetail_Template
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.Type
  DROP CONSTRAINT FK_Type_TypeGroup
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

IF (OBJECT_ID('FK__TT153Adju__TypeI__25869641', 'F') IS NOT NULL)
BEGIN
    ALTER TABLE dbo.TT153AdjustAnnouncement DROP CONSTRAINT FK__TT153Adju__TypeI__25869641
END
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

IF (OBJECT_ID('FK__TT153Dele__TypeI__67A95F59', 'F') IS NOT NULL)
BEGIN
    ALTER TABLE dbo.TT153DeletedInvoice DROP CONSTRAINT FK__TT153Dele__TypeI__67A95F59
END
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

IF (OBJECT_ID('FK__TT153Dest__TypeI__22AA2996', 'F') IS NOT NULL)
BEGIN
    ALTER TABLE dbo.TT153DestructionInvoice DROP CONSTRAINT FK__TT153Dest__TypeI__22AA2996
END
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

IF (OBJECT_ID('FK__TT153Lost__TypeI__1FCDBCEB', 'F') IS NOT NULL)
BEGIN
    ALTER TABLE dbo.TT153LostInvoice DROP CONSTRAINT FK__TT153Lost__TypeI__1FCDBCEB
END
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

IF (OBJECT_ID('FK__TT153Publ__TypeI__1CF15040', 'F') IS NOT NULL)
BEGIN
    ALTER TABLE dbo.TT153PublishInvoice DROP CONSTRAINT FK__TT153Publ__TypeI__1CF15040
END
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

IF (OBJECT_ID('FK__TT153Regi__TypeI__1A14E395', 'F') IS NOT NULL)
BEGIN
    ALTER TABLE dbo.TT153RegisterInvoice DROP CONSTRAINT FK__TT153Regi__TypeI__1A14E395
END
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.Account SET DetailType = N'-1' WHERE ID = 'df6c256f-3d79-42c3-9658-8cee022bdbeb'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DELETE dbo.AccountDefault WHERE ID = '19864de9-f206-4233-928a-9f9217cfcfed'
DELETE dbo.AccountDefault WHERE ID = '802bb90b-93f9-4dbc-ae0e-a63fa874d475'

UPDATE dbo.AccountDefault SET DefaultAccount = N'1331' WHERE ID = '610eefab-8d87-4381-aeef-19d2336daaf7'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '0d2f3509-20e3-4d70-b886-4ed4d3259d4c'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '81088e9c-2881-48a1-aa3f-a6e119e96c44'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = 'c229344b-bfed-451c-8659-a6f83818500b'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '3e80b0e6-2b93-4239-91df-c71cf9ec3737'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

INSERT dbo.ApiService(ID, SupplierServiceID, SupplierServiceCode, ApiName, ApiPath) VALUES ('0819068f-1c2f-46ff-aff6-16d5e0702dad', 'bc9b5097-a03d-4bfb-b5d6-85a01b9c9af0', N'MIV', N'TaoHoaDon', N'api/InvoiceAPI/Save')
INSERT dbo.ApiService(ID, SupplierServiceID, SupplierServiceCode, ApiName, ApiPath) VALUES ('31a0d9a3-4c37-4594-98b0-36ca62f0cfd9', 'bc9b5097-a03d-4bfb-b5d6-85a01b9c9af0', N'MIV', N'TaiHoaDonPDF', N'api/Invoice/Preview')
INSERT dbo.ApiService(ID, SupplierServiceID, SupplierServiceCode, ApiName, ApiPath) VALUES ('e1d17397-16cd-4a72-ba98-88fb0b1209e0', 'bc9b5097-a03d-4bfb-b5d6-85a01b9c9af0', N'MIV', N'HDDCGiam', N'api/InvoiceAPI/DcGiam')
INSERT dbo.ApiService(ID, SupplierServiceID, SupplierServiceCode, ApiName, ApiPath) VALUES ('00ead48b-c4d8-4eb5-af42-8e468c633cff', 'bc9b5097-a03d-4bfb-b5d6-85a01b9c9af0', N'MIV', N'HuyHoaDon', N'api/Invoice/xoaboHD')
INSERT dbo.ApiService(ID, SupplierServiceID, SupplierServiceCode, ApiName, ApiPath) VALUES ('7545c78b-7cdc-4313-ae1d-9885e6f0cc73', 'bc9b5097-a03d-4bfb-b5d6-85a01b9c9af0', N'MIV', N'HDDCDinhDanh', N'api/InvoiceAPI/DcDinhdanh')
INSERT dbo.ApiService(ID, SupplierServiceID, SupplierServiceCode, ApiName, ApiPath) VALUES ('b327e439-c904-40a6-bcb9-a6e5607a3664', 'bc9b5097-a03d-4bfb-b5d6-85a01b9c9af0', N'MIV', N'LayThongTinHD_MS-KH-SHD', N'api/Invoice/GetByInvoiceNumber')
INSERT dbo.ApiService(ID, SupplierServiceID, SupplierServiceCode, ApiName, ApiPath) VALUES ('216de093-80d6-4c5f-9182-b80d9cc41d74', 'bc9b5097-a03d-4bfb-b5d6-85a01b9c9af0', N'MIV', N'HDDCTang', N'api/InvoiceAPI/DcTang')
INSERT dbo.ApiService(ID, SupplierServiceID, SupplierServiceCode, ApiName, ApiPath) VALUES ('ee2ac236-c0a4-41ac-a0aa-ba396223dc20', 'bc9b5097-a03d-4bfb-b5d6-85a01b9c9af0', N'MIV', N'DangNhap-LayToken', N'api/Account/Login')
INSERT dbo.ApiService(ID, SupplierServiceID, SupplierServiceCode, ApiName, ApiPath) VALUES ('653e9aa0-35ac-4859-ab4c-ea7d05b1a498', 'bc9b5097-a03d-4bfb-b5d6-85a01b9c9af0', N'MIV', N'LayThongTinHD', N'api/Invoice/GetById')
INSERT dbo.ApiService(ID, SupplierServiceID, SupplierServiceCode, ApiName, ApiPath) VALUES ('fc7a0de6-8f41-465e-a333-ed03dd245f75', 'bc9b5097-a03d-4bfb-b5d6-85a01b9c9af0', N'MIV', N'LayTrangThaiDSHD', N'api/InvoiceAPI/GetInfoInvoice')
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.GenCode SET Prefix = N'KH' WHERE ID = '514129e9-fbe7-4b71-9f64-f5433834d99c'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.MaterialGoodsResourceTaxGroup SET Unit = N'tấn' WHERE ID = '117a4f4d-6e72-4bdf-ab2e-28cc76893717'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.MaterialGoodsSpecialTaxGroup SET Unit = N'chiếc' WHERE ID = '4f49c3b7-3ee3-42b6-b3d1-0fcf681b52e5'
UPDATE dbo.MaterialGoodsSpecialTaxGroup SET Unit = N'cái' WHERE ID = '3394b159-f3f7-433b-98a3-1ca29635dc7a'
UPDATE dbo.MaterialGoodsSpecialTaxGroup SET Unit = N'lít' WHERE ID = 'ca674619-bcb5-494f-814a-2d060fa3ee85'
UPDATE dbo.MaterialGoodsSpecialTaxGroup SET Unit = N'cái' WHERE ID = '1c378f3b-3ee7-4fdf-a438-436a1f7b4138'
UPDATE dbo.MaterialGoodsSpecialTaxGroup SET Unit = N'cái' WHERE ID = 'dce48d99-a9a8-4638-8ce3-5bcdfbab2e83'
UPDATE dbo.MaterialGoodsSpecialTaxGroup SET Unit = N'cái' WHERE ID = 'fdd84bd7-dea8-4cc8-8ece-9d222cd1c85b'
UPDATE dbo.MaterialGoodsSpecialTaxGroup SET Unit = N'chiếc' WHERE ID = '4b0c794d-9355-442f-9d77-9e7331b05124'
UPDATE dbo.MaterialGoodsSpecialTaxGroup SET Unit = N'bộ' WHERE ID = '0ce52aab-adf8-4522-892e-a679b08786d1'
UPDATE dbo.MaterialGoodsSpecialTaxGroup SET Unit = N'cái' WHERE ID = '017dda4b-251f-4086-a752-c113426490be'
UPDATE dbo.MaterialGoodsSpecialTaxGroup SET Unit = N'cái' WHERE ID = '10f45647-804e-42fa-a49b-c405763ce78f'
UPDATE dbo.MaterialGoodsSpecialTaxGroup SET Unit = N'bộ' WHERE ID = '3a044b16-eaad-4fd2-8879-d2aba54af279'
UPDATE dbo.MaterialGoodsSpecialTaxGroup SET Unit = N'lít' WHERE ID = 'b1b22985-f1c4-4128-a3c9-d2b6a1ab9094'
UPDATE dbo.MaterialGoodsSpecialTaxGroup SET Unit = N'cái' WHERE ID = '9fa4e286-1b1e-408c-89da-d5a4213c96ee'
UPDATE dbo.MaterialGoodsSpecialTaxGroup SET Unit = N'lít' WHERE ID = 'b90a7a82-dc39-4a32-9af9-f6e5b7f1ac3e'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.PersonalSalaryTax SET FromAmount = 52000000.00 WHERE ID = '2c69576f-946c-438d-a671-0471c5596844'
UPDATE dbo.PersonalSalaryTax SET FromAmount = 18000000.00 WHERE ID = '3f12764c-c4b7-4a23-ba29-14d88d18c294'
UPDATE dbo.PersonalSalaryTax SET FromAmount = 32000000.00 WHERE ID = 'ece149ef-c962-446a-882c-18df3c4a7765'
UPDATE dbo.PersonalSalaryTax SET FromAmount = 5000000.00 WHERE ID = '925f2688-4ce6-42cd-9085-2645b4532608'
UPDATE dbo.PersonalSalaryTax SET FromAmount = 10000000.00 WHERE ID = '9be78b0f-7d88-475a-a2e4-28113e4dd23f'
UPDATE dbo.PersonalSalaryTax SET FromAmount = 800000000.00 WHERE ID = '1a7e4230-18ab-423e-95e1-4f7ef5aa60b7'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

INSERT dbo.SupplierService(ID, SupplierServiceCode, SupplierServiceName, PathAccess) VALUES ('bc9b5097-a03d-4bfb-b5d6-85a01b9c9af0', N'MIV', N'Công ty TNHH Hóa đơn điện tử M-invoice', N'http://test.minvoice.vn/')
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.SystemOption SET Data = N'19.4.19.23', DefaultData = N'' WHERE ID = 111

INSERT dbo.SystemOption(ID, Code, Name, Type, Data, DefaultData, Note, IsSecurity) VALUES (130, N'Token_MIV', N'Mã token MIV', 0, N'', N'', N'', 0)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

SET IDENTITY_INSERT dbo.Template ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
INSERT dbo.Template(ID, TemplateName, TypeID, IsDefault, IsActive, IsSecurity, OrderPriority) VALUES ('0ea573da-afed-4f65-aa45-7c3dbbc6c2eb', N'Mẫu chuẩn', 601, 0, 1, 1, 1)
INSERT dbo.Template(ID, TemplateName, TypeID, IsDefault, IsActive, IsSecurity, OrderPriority) VALUES ('70c2c082-3993-41be-9945-89735de68af1', N'Mẫu chuẩn', 660, 0, 1, 1, 1)
GO
SET IDENTITY_INSERT dbo.Template OFF
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DELETE dbo.TemplateColumn WHERE ID = '3c866b01-fa43-4bdf-8008-0e91051eb1aa'
DELETE dbo.TemplateColumn WHERE ID = '8f0a266c-0232-44b0-8b43-22db07dad16f'
DELETE dbo.TemplateColumn WHERE ID = '58fae7b6-d851-4c86-9b79-43b38a7f5bc4'
DELETE dbo.TemplateColumn WHERE ID = '4acbf1c1-8c0e-40b5-aa8e-61729249da08'
DELETE dbo.TemplateColumn WHERE ID = '6623c11e-a24c-45de-8905-81bd89efada0'
DELETE dbo.TemplateColumn WHERE ID = '5b885102-3daf-4b48-9df8-9640e9d35f82'
DELETE dbo.TemplateColumn WHERE ID = 'a4d56a74-793d-4b64-a7fb-9e123c97ce63'
DELETE dbo.TemplateColumn WHERE ID = 'ed85274f-1a41-4753-971c-ed9966643b97'

UPDATE dbo.TemplateColumn SET VisiblePosition = 11 WHERE ID = '20989740-5031-4eec-bfd4-02fda885251c'
UPDATE dbo.TemplateColumn SET VisiblePosition = 73 WHERE ID = '6e428c4a-c0d1-48ce-a685-03274424c42b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 40 WHERE ID = '3b79919f-2e43-468e-b3c8-046cd30b17c9'
UPDATE dbo.TemplateColumn SET VisiblePosition = 63 WHERE ID = '50d28322-9524-44fa-8c25-04c387097f18'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '010aeb4e-d742-4846-8018-050fb2ab1ebe'
UPDATE dbo.TemplateColumn SET VisiblePosition = 44 WHERE ID = 'e446c021-4e8b-463b-aa4a-06b587641656'
UPDATE dbo.TemplateColumn SET VisiblePosition = 35 WHERE ID = 'eed0c1a3-09ca-49eb-959d-06e84d09f05d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 36 WHERE ID = '9852f923-a823-4a97-929b-07c30402bdb4'
UPDATE dbo.TemplateColumn SET VisiblePosition = 24 WHERE ID = 'ce423932-65b2-4580-844f-0d71311c21fe'
UPDATE dbo.TemplateColumn SET VisiblePosition = 63 WHERE ID = 'bdc3b731-c6b6-4b1c-8c67-12e62e6047d7'
UPDATE dbo.TemplateColumn SET VisiblePosition = 35 WHERE ID = '2e0323bf-8e8e-47c2-8997-13c67637bf7a'
UPDATE dbo.TemplateColumn SET VisiblePosition = 76 WHERE ID = '8e86c1e4-1cab-4bee-8f1e-14f89bad6918'
UPDATE dbo.TemplateColumn SET ColumnName = N'Amount', ColumnCaption = N'Số nộp lần này QĐ' WHERE ID = 'a171fc6b-b4db-402e-9a8f-152b0a7a5e0f'
UPDATE dbo.TemplateColumn SET VisiblePosition = 73 WHERE ID = '0f7d73dd-9cc7-42cf-b5cc-16c23b788f45'
UPDATE dbo.TemplateColumn SET VisiblePosition = 50 WHERE ID = '670d05ea-d2d8-43f4-b8ae-18b259e6ba0d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 50 WHERE ID = 'fa4c1991-ebbc-438b-80eb-18fcc46acb4a'
UPDATE dbo.TemplateColumn SET IsVisible = 1 WHERE ID = '46de8e54-3469-4fdf-aaf0-1b47f553d5c8'
UPDATE dbo.TemplateColumn SET VisiblePosition = 49 WHERE ID = '55557b7e-41a7-4f2a-a669-1d12cbeccdb9'
UPDATE dbo.TemplateColumn SET VisiblePosition = 62 WHERE ID = '697ebc1a-9dea-461a-b9a5-233274c20a75'
UPDATE dbo.TemplateColumn SET VisiblePosition = 48 WHERE ID = '3e29ae9f-d888-4451-b106-23a8352d3a43'
UPDATE dbo.TemplateColumn SET VisiblePosition = 70 WHERE ID = '19e28965-8e23-412c-96f7-23c992dbecb3'
UPDATE dbo.TemplateColumn SET VisiblePosition = 45 WHERE ID = '3982ff5e-b060-4b3e-85f0-256f202d8b7e'
UPDATE dbo.TemplateColumn SET VisiblePosition = 38 WHERE ID = '7c40ff51-71bd-4088-b5d1-273428b1a544'
UPDATE dbo.TemplateColumn SET VisiblePosition = 57 WHERE ID = '44f6d335-8ee9-4fae-81d7-2851e87698bb'
UPDATE dbo.TemplateColumn SET VisiblePosition = 12 WHERE ID = '4a332e55-7430-418f-9bbf-2909e5124c97'
UPDATE dbo.TemplateColumn SET VisiblePosition = 41 WHERE ID = 'b91e31af-cf1c-4fb9-8d11-2c26e8eaeafa'
UPDATE dbo.TemplateColumn SET VisiblePosition = 66 WHERE ID = '937ea0b0-87e0-4bed-b031-350dd22f3a44'
UPDATE dbo.TemplateColumn SET VisiblePosition = 72 WHERE ID = '785dd4ad-1eaa-4753-abef-354f8c923997'
UPDATE dbo.TemplateColumn SET VisiblePosition = 39 WHERE ID = 'bdcafb2f-6441-4707-87c5-37f7493a2ecf'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Mẫu số hóa đơn' WHERE ID = '71cbfea8-8b2f-40fe-b843-393a6e745562'
UPDATE dbo.TemplateColumn SET ColumnName = N'TaxAmount' WHERE ID = '920a80d5-8c74-4f5f-b3f9-39dec429c477'
UPDATE dbo.TemplateColumn SET VisiblePosition = 75 WHERE ID = '7cee62c6-1bd2-4232-94ec-3bcb29a8482e'
UPDATE dbo.TemplateColumn SET VisiblePosition = 75 WHERE ID = 'e855f8ad-2990-441e-970e-3cc3282f1069'
UPDATE dbo.TemplateColumn SET ColumnName = N'AmountOriginal' WHERE ID = '76fa9adb-829c-471a-9c28-3e62d15bcc5a'
UPDATE dbo.TemplateColumn SET VisiblePosition = 36 WHERE ID = '8cc30888-9c02-4ca1-8e67-3fb1d822846d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 49 WHERE ID = '6b320eb3-3684-4b4c-b75d-4189c06ba157'
UPDATE dbo.TemplateColumn SET VisiblePosition = 65 WHERE ID = '9df6a75d-2094-4d73-8093-4575563df510'
UPDATE dbo.TemplateColumn SET VisiblePosition = 61 WHERE ID = '92541174-e690-4072-a087-46070c5ba747'
UPDATE dbo.TemplateColumn SET ColumnName = N'Amount' WHERE ID = 'f52a0ddf-2f04-4859-b11b-46f4545222ce'
UPDATE dbo.TemplateColumn SET ColumnName = N'Amount', ColumnCaption = N'Số nộp lần này QĐ' WHERE ID = '730e7d8a-eff7-4cd0-bad4-47b653d85d75'
UPDATE dbo.TemplateColumn SET VisiblePosition = 71 WHERE ID = 'a114886b-ddb2-479c-8e78-4820bd3569e2'
UPDATE dbo.TemplateColumn SET VisiblePosition = 43 WHERE ID = '6cb65cf1-0efa-49ad-a828-49d7fa3870dc'
UPDATE dbo.TemplateColumn SET VisiblePosition = 60 WHERE ID = '44071238-1178-4e3d-82a1-4eceb71771aa'
UPDATE dbo.TemplateColumn SET ColumnName = N'AmountOriginal' WHERE ID = '2ccfbd79-3fec-4059-9766-54c0484d7b59'
UPDATE dbo.TemplateColumn SET VisiblePosition = 77 WHERE ID = '1eb342a1-416c-4ed1-93f5-58984da9d6d6'
UPDATE dbo.TemplateColumn SET VisiblePosition = 48 WHERE ID = '37492113-e038-443d-8660-5b089fabf658'
UPDATE dbo.TemplateColumn SET VisiblePosition = 45 WHERE ID = 'b4ab50ce-ead8-4d51-b12d-61d053432150'
UPDATE dbo.TemplateColumn SET VisiblePosition = 10 WHERE ID = 'e4aff9c9-d821-4eb0-9fe7-62aa53627445'
UPDATE dbo.TemplateColumn SET VisiblePosition = 42 WHERE ID = 'f5565ca1-6d79-40e9-a660-63f276fb564f'
UPDATE dbo.TemplateColumn SET VisiblePosition = 47 WHERE ID = '1870543b-5bcc-4fbf-9ec9-64834ee63bb2'
UPDATE dbo.TemplateColumn SET VisiblePosition = 42 WHERE ID = 'db0fdb0b-e6a9-4fa5-9b5c-65e40918583f'
UPDATE dbo.TemplateColumn SET VisiblePosition = 70 WHERE ID = 'a33cefbf-1630-478b-a5ef-6d1ded1742e6'
UPDATE dbo.TemplateColumn SET ColumnName = N'RefVoucherExchangeRate', ColumnCaption = N'Tỷ giá chứng từ', ColumnToolTip = N'Tỷ giá chứng từ', ColumnWidth = 100, IsVisible = 1 WHERE ID = '2a4f02a9-8ad5-4fad-8fc3-6e0f5304eff9'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Mẫu số hóa đơn' WHERE ID = 'bb1e00d6-9f29-4457-ac7a-6ed9f034c3a3'
UPDATE dbo.TemplateColumn SET VisiblePosition = 49 WHERE ID = '27a4278c-505a-4157-83dd-70e4d1605484'
UPDATE dbo.TemplateColumn SET VisiblePosition = 51 WHERE ID = '71e9b0ae-d652-47fb-8b02-7125d603c00e'
UPDATE dbo.TemplateColumn SET VisiblePosition = 43 WHERE ID = '631da22b-692f-4e17-bf69-73942454e9c0'
UPDATE dbo.TemplateColumn SET ColumnName = N'TaxAmount' WHERE ID = 'fdf36704-0c8f-4333-8d19-7800e80e48a1'
UPDATE dbo.TemplateColumn SET VisiblePosition = 37 WHERE ID = 'ddf4ced3-d78f-43a2-ae36-7b964b34e00b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 74 WHERE ID = '3cb21186-1d1b-4d73-8bf6-7d7cdd8e6846'
UPDATE dbo.TemplateColumn SET VisiblePosition = 59 WHERE ID = '17359104-f9d7-4bd8-a9ee-7d9eb74b4204'
UPDATE dbo.TemplateColumn SET ColumnName = N'AmountOriginal' WHERE ID = '904ec13d-55db-4f9a-84e7-7e8e8dcd3ae5'
UPDATE dbo.TemplateColumn SET VisiblePosition = 58 WHERE ID = '9a09d1e2-a16f-4e29-aa68-805dadaca85c'
UPDATE dbo.TemplateColumn SET VisiblePosition = 37 WHERE ID = '6db70b18-da68-4205-afa9-808aa820d10b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 36 WHERE ID = '07ef037c-1fc8-41d3-b488-80b127c4c28c'
UPDATE dbo.TemplateColumn SET VisiblePosition = 46 WHERE ID = '8cda29f4-4d74-4eff-bccf-82bddedc001d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 77 WHERE ID = '6ed9c5c0-a78d-4fb4-a4cf-84b81bba9fdf'
UPDATE dbo.TemplateColumn SET VisiblePosition = 69 WHERE ID = 'b0c42e49-fd0b-473f-bdf6-8592a9238011'
UPDATE dbo.TemplateColumn SET VisiblePosition = 57 WHERE ID = 'eed8c151-80b3-4f58-9b4d-8748d1c58705'
UPDATE dbo.TemplateColumn SET VisiblePosition = 72 WHERE ID = '53e970be-471a-4992-97c7-87a459ab51d3'
UPDATE dbo.TemplateColumn SET VisiblePosition = 71 WHERE ID = '075dc23c-c505-40f8-bd88-886d3f7683be'
UPDATE dbo.TemplateColumn SET VisiblePosition = 46 WHERE ID = 'bed61782-dd24-4fbf-9dad-8c2d40a7fea3'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'f96eca0c-5169-4c9c-8c5a-9017988f67d0'
UPDATE dbo.TemplateColumn SET VisiblePosition = 42 WHERE ID = 'd3c6a8b1-cff3-40c2-8b0e-926812595d31'
UPDATE dbo.TemplateColumn SET VisiblePosition = 36 WHERE ID = 'a8b54b5c-5360-401e-95e0-927c6e893a98'
UPDATE dbo.TemplateColumn SET VisiblePosition = 77 WHERE ID = '4c6fe857-7d18-4ad5-a761-937a2ec2d3c3'
UPDATE dbo.TemplateColumn SET VisiblePosition = 35 WHERE ID = '590bbb41-3b86-4be3-9578-93cd716566f8'
UPDATE dbo.TemplateColumn SET VisiblePosition = 58 WHERE ID = 'a8a3dd0c-a7c1-4245-a554-93ed443df46f'
UPDATE dbo.TemplateColumn SET VisiblePosition = 69 WHERE ID = '1b5576b5-f912-49bd-ad17-9562c20943f1'
UPDATE dbo.TemplateColumn SET VisiblePosition = 76 WHERE ID = '47869352-30d4-480c-b4e7-9589655f87cc'
UPDATE dbo.TemplateColumn SET VisiblePosition = 59 WHERE ID = 'a7d45607-ceb6-4723-9a0d-98bdc8d2edc1'
UPDATE dbo.TemplateColumn SET VisiblePosition = 60 WHERE ID = 'c474e408-68a0-4e4f-bfb8-99f654732ba3'
UPDATE dbo.TemplateColumn SET VisiblePosition = 68 WHERE ID = '644f3743-6b12-46de-b71a-9a27089d303f'
UPDATE dbo.TemplateColumn SET VisiblePosition = 40 WHERE ID = '81768c2c-a7a6-4623-b5e7-9b5b671fd90a'
UPDATE dbo.TemplateColumn SET VisiblePosition = 45 WHERE ID = '61ee777e-0197-40e5-ac2e-9d26990f6262'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Mẫu số hóa đơn' WHERE ID = 'c4bf705f-0b9a-47af-9243-9e79e67bad12'
UPDATE dbo.TemplateColumn SET VisiblePosition = 39 WHERE ID = 'f4067d28-d77a-4f80-a00d-9fb2d12d5d06'
UPDATE dbo.TemplateColumn SET ColumnName = N'DifferAmount', ColumnCaption = N'Tiền chênh lệch', ColumnToolTip = N'Tiền chênh lệch', ColumnWidth = 100, IsVisible = 1, VisiblePosition = 21 WHERE ID = '7c92c1be-16d8-4b7d-94cf-9ff5b8040d98'
UPDATE dbo.TemplateColumn SET VisiblePosition = 70 WHERE ID = '6f0b1db6-5230-4a36-93b1-a2ceadc4c062'
UPDATE dbo.TemplateColumn SET ColumnName = N'LastExchangeRate', ColumnCaption = N'Tỷ giá đánh giá lại', ColumnToolTip = N'Tỷ giá đánh giá lại', ColumnWidth = 100, IsVisible = 1, VisiblePosition = 18 WHERE ID = '2070be10-cf42-44ce-9ad2-a366e48d3556'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '3c6d1e1b-8ea8-4498-b914-a3c7e2592f64'
UPDATE dbo.TemplateColumn SET VisiblePosition = 37 WHERE ID = 'a5890cd2-1a99-4e60-b6fb-a460c257790d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 67 WHERE ID = 'f74133c2-80a2-4d72-9ab7-a4883b6446a7'
UPDATE dbo.TemplateColumn SET VisiblePosition = 47 WHERE ID = '8ac1dfde-44d8-4d5c-a747-a58c87f84e70'
UPDATE dbo.TemplateColumn SET VisiblePosition = 44 WHERE ID = 'd7e10d9a-8040-423e-8105-ad9e971dd650'
UPDATE dbo.TemplateColumn SET VisiblePosition = 52 WHERE ID = 'e71dfe34-5c48-4698-bb17-af3297694097'
UPDATE dbo.TemplateColumn SET VisiblePosition = 46 WHERE ID = '8e0334c5-f11e-4460-b042-b4d4e846f5d1'
UPDATE dbo.TemplateColumn SET VisiblePosition = 47 WHERE ID = '563bbf99-8cce-4e43-9647-b65293542d9a'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '61dbc2a0-8d09-4d68-953b-b742f694a90b'
UPDATE dbo.TemplateColumn SET ColumnName = N'AmountOriginal' WHERE ID = 'c2ceca91-f632-444e-8757-bc557a1b04a7'
UPDATE dbo.TemplateColumn SET VisiblePosition = 38 WHERE ID = 'c915ba36-6df3-41b9-a84b-bdd8579e7da3'
UPDATE dbo.TemplateColumn SET VisiblePosition = 62 WHERE ID = 'cb707035-0116-4715-90b4-bf58d4d5d716'
UPDATE dbo.TemplateColumn SET VisiblePosition = 39 WHERE ID = '843a679b-90de-4195-b233-c1507f1806be'
UPDATE dbo.TemplateColumn SET VisiblePosition = 38 WHERE ID = '743046ea-e5f6-4b40-9609-c17e81aec317'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '94838bf1-01ea-4ebb-b5e0-c188ed268048'
UPDATE dbo.TemplateColumn SET VisiblePosition = 58 WHERE ID = '6104647b-d6fc-4f57-a718-c26e857ad028'
UPDATE dbo.TemplateColumn SET VisiblePosition = 41 WHERE ID = '963a2755-2c7f-49d9-82ed-c2969cc34284'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Mẫu số hóa đơn' WHERE ID = 'e9ea667c-25e6-4700-b8ce-c2f7d6b4bf9d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 68 WHERE ID = '4691d04e-9b9a-4106-a1b9-c313603df8db'
UPDATE dbo.TemplateColumn SET VisiblePosition = 61 WHERE ID = '2fac868e-5b3b-4d64-bae8-c328a7d0e3a9'
UPDATE dbo.TemplateColumn SET VisiblePosition = 22 WHERE ID = 'ecbb6e33-25e1-4e8b-b40d-c353a39b9887'
UPDATE dbo.TemplateColumn SET VisiblePosition = 9 WHERE ID = '2948d8f2-acdf-41c1-a6f2-c40fdff5b325'
UPDATE dbo.TemplateColumn SET VisiblePosition = 51 WHERE ID = '05aaaec1-2b4d-44be-83c8-c545ec33219e'
UPDATE dbo.TemplateColumn SET VisiblePosition = 66 WHERE ID = 'fd6df1ba-ff32-4a1a-92f4-c814ab66c0fa'
UPDATE dbo.TemplateColumn SET VisiblePosition = 65 WHERE ID = 'f4b4ec25-1479-43d6-8ea0-ca4d39ce8f50'
UPDATE dbo.TemplateColumn SET VisiblePosition = 48 WHERE ID = '584ca97d-a58d-4720-9ab0-ca7b724554ea'
UPDATE dbo.TemplateColumn SET VisiblePosition = 75 WHERE ID = '82a2d63d-bea8-4979-9b92-cf7363bb7679'
UPDATE dbo.TemplateColumn SET VisiblePosition = 43 WHERE ID = '6a1465ea-a76c-4e7c-bf6b-cff31bb00065'
UPDATE dbo.TemplateColumn SET VisiblePosition = 67 WHERE ID = '51f6ecbf-9cdf-425f-8753-d55f067b81a9'
UPDATE dbo.TemplateColumn SET VisiblePosition = 64 WHERE ID = 'f8e444a1-9f3c-4cee-980b-dad6b22cde6f'
UPDATE dbo.TemplateColumn SET VisiblePosition = 41 WHERE ID = 'bb4ce80d-c936-4ff3-b606-dd4897d6bebf'
UPDATE dbo.TemplateColumn SET VisiblePosition = 40 WHERE ID = '553cb178-f3d4-4d74-b13e-de9e6de89a5b'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '09f4aadd-a99d-472a-9c51-df4b8884be08'
UPDATE dbo.TemplateColumn SET VisiblePosition = 78 WHERE ID = '49fbf93e-790a-49bf-abc1-e0b82809202f'
UPDATE dbo.TemplateColumn SET VisiblePosition = 74 WHERE ID = '7e5e631f-2527-4f2c-bf36-e3b0c1d56233'
UPDATE dbo.TemplateColumn SET VisiblePosition = 37 WHERE ID = 'abd14a2b-2d4b-4a0a-8592-e67af30b7ab7'
UPDATE dbo.TemplateColumn SET VisiblePosition = 34 WHERE ID = 'fecc404a-32aa-4af9-94cd-f15e07cff4f5'
UPDATE dbo.TemplateColumn SET ColumnName = N'Amount', ColumnCaption = N'Số nộp lần này QĐ', IsVisible = 0 WHERE ID = '2be575b0-ffa0-4d01-b64f-f3fa13e1f427'
UPDATE dbo.TemplateColumn SET VisiblePosition = 64 WHERE ID = '383d61d1-349f-449c-9485-f626d490aade'
UPDATE dbo.TemplateColumn SET VisiblePosition = 44 WHERE ID = '78736acd-cfb5-43b3-af68-fd44c1682293'
UPDATE dbo.TemplateColumn SET VisiblePosition = 34 WHERE ID = '8194eba9-6f7a-4b54-bf7b-fd6a8b63ddc1'
UPDATE dbo.TemplateColumn SET VisiblePosition = 76 WHERE ID = '85cf6a73-ab19-411e-864f-fe54f8849717'
UPDATE dbo.TemplateColumn SET VisiblePosition = 69 WHERE ID = 'cb0bb422-12fd-4c7f-aafb-fe6cc59072f3'

INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('7e9deec3-186b-44dd-82c4-0073755a119c', '4dcf5134-2f33-44d8-b14c-7cef118d93d8', N'CustomProperty1', N'Cột 1', N'', 0, 0, 98, 0, 0, 0, 19)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('c100bb2d-065e-4bf2-a50a-0098b85494dd', '7db540d7-3c42-4682-8c8d-2769f550dcec', N'CurrencyID', N'Loại tiền', N'', 0, 0, 80, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('32ec38e5-081c-4b19-984d-019289e8979f', 'b667f9be-36ad-421e-985e-c75cc4269af3', N'RemainingAmountOriginal', N'Số chưa đối trừ', NULL, 0, 0, 105, 0, 0, 1, 8)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('96e054a2-441c-40a6-b35e-01c0d49bc0aa', '887a2a4b-0f23-4a9f-b99f-a54c1985a479', N'CreditAccountingObjectID', N'Đối tượng Có', N'', 0, 0, 105, 105, 105, 1, 10)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('39ff8a3a-5366-495c-b33a-01ed7d9bc278', '35dadd9a-3a12-4a26-90a3-8e21077a6b95', N'CashOutAmount', N'QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 29)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('75006b84-c909-47c1-b416-01fd3230ec2b', '44ecd862-4c61-4adc-85de-6265c00c0784', N'CashOutAmount', N'QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 37)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('1b23b016-5471-482a-8f58-024d03b5734e', '1e5718a2-7e72-4196-bd42-a48d2cda082d', N'CashOutVATAmount', N'Tiền thuế QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 43)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ad47be2e-0f0f-473b-93d1-02a7f4b29e24', '607916fa-9866-4e49-a74c-7daafe86c7df', N'ExchangeRate', N'Tỷ giá', N'', 0, 0, 101, 0, 0, 0, 6)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('1f264d91-94fb-408c-bc1b-02d5cdbc3d6c', '607916fa-9866-4e49-a74c-7daafe86c7df', N'CreditAccount', N'TK Có', N'Tài khoản Có', 1, 0, 95, 95, 95, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d988df2b-506d-48ef-9275-08507d33205b', '3ec8f39e-2400-4809-9f78-defe4d540b79', N'CashOutDifferAmount', N'Chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 78)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('08e491b7-98d9-4a8b-a191-091d10de1d42', 'c143003d-aaac-48bd-8afe-c5e509d62362', N'CashOutVATAmount', N'Tiền thuế QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 36)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('57262a52-80de-4edf-90e8-0942e691224f', 'b667f9be-36ad-421e-985e-c75cc4269af3', N'LastExchangeRate', N'Tỷ giá đánh giá lại gần nhất', NULL, 0, 0, 105, 0, 0, 1, 7)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d2e618fe-5ec4-4f18-9aa0-09ef2abcabed', '607916fa-9866-4e49-a74c-7daafe86c7df', N'AmountOriginal', N'Số tiền', N'', 0, 0, 120, 120, 120, 0, 7)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('6b44c94a-71a9-40dc-adc6-0a6088921d2e', '4dcf5134-2f33-44d8-b14c-7cef118d93d8', N'DepartmentID', N'Phòng ban', N'', 0, 0, 150, 0, 0, 0, 23)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ceffc2bf-5872-4b82-b7bd-0a844e0e73bb', 'ce3ddf32-08b8-4b7d-8ff8-7bd47061cc9d', N'TotalAmountOriginal', N'Số tiền', N'', 0, 0, 120, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('c3ae478f-9ff1-48f4-8723-0b8718cb608b', '5c8f394b-080e-4c92-ae4e-57827fe9565f', N'CashOutDifferAmount', N'Chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 54)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ff033395-66a8-490c-8c6b-0b97a56f2449', '3ce4363e-e0b3-4b18-8a4b-cdc983687c78', N'CreditAccountingObjectName', N'Tên đối tượng Có', N'', 0, 0, 115, 0, 0, 0, 4)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ad154df9-508e-499f-9b97-0cb08e06d17f', '8ea0ddcd-12c0-4bb0-a72c-6c511dc46c11', N'CashOutDifferAccount', N'TK xử lý chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 53)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('8fb48148-8161-47ec-8195-0df376955f24', '7d9a1620-0fe7-4650-aac6-4a82dfa973ac', N'CashOutAmount', N'QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 51)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('b568b75d-3e0b-48c7-9327-0dfad2b66f68', '9bd5b723-ca6e-4434-b406-317891442ff7', N'CashOutExchangeRate', N'Tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 39)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('16b38c8d-4afa-4feb-8800-0e9aa3fa096b', '85a7f992-5604-4eda-a220-a5fc36e66e38', N'CashOutDifferVATAmount', N'Chênh lệch tiền thuế', NULL, 0, 1, 105, 0, 0, 0, 51)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('f6b1f4fc-d00b-45d9-9514-0ee2b981dab2', '1e5718a2-7e72-4196-bd42-a48d2cda082d', N'CashOutDifferVATAmount', N'Chênh lệch tiền thuế', NULL, 0, 1, 105, 0, 0, 0, 44)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('2006f4a7-a25c-4acb-a5f2-0f7050355533', '4cf1f231-03cb-4ee2-89e5-64bffe008d2d', N'CashOutVATAmount', N'Tiền thuế QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 76)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('3596fbf2-5d08-4275-a666-101359444949', '607916fa-9866-4e49-a74c-7daafe86c7df', N'DebitAccountingObjectID', N'Đối tượng Nợ', N'', 0, 0, 105, 105, 105, 1, 9)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('a0372fc2-8fe9-47b6-96e2-11e41bda158f', '6083e5f1-c252-4d5f-bac6-e2b7ea594414', N'CashOutExchangeRate', N'Tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 78)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('31def9fe-2ec0-4c15-8785-1211d9a34905', '3aee22cb-0b80-4250-bd33-140508452210', N'CashOutDifferAccount', N'TK xử lý chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 53)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('adc635cd-6271-46f7-b04f-12a175723206', '8ea0ddcd-12c0-4bb0-a72c-6c511dc46c11', N'CashOutAmount', N'QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 51)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('304bf1d0-07a1-4a98-81b1-13745476410c', '3ce4363e-e0b3-4b18-8a4b-cdc983687c78', N'ExpenseItemID', N'Khoản mục CP', N'Khoản mục chi phí', 0, 0, 130, 0, 0, 0, 25)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9f9ea109-d7ea-4011-817d-1485f3f720a8', 'cb00a688-1633-48ed-a251-c525636dd47b', N'CashOutDifferVATAmount', N'Chênh lệch tiền thuế', NULL, 0, 1, 105, 0, 0, 0, 59)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('05e6bad9-97b2-4f46-a00a-148ee6a3a313', '9bd5b723-ca6e-4434-b406-317891442ff7', N'CashOutDifferAccount', N'TK xử lý chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 42)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('6bf794c8-36d3-4c7a-a103-1577e6f6dc87', 'bb4a511e-ef0f-4b07-9e21-54d097ea3c58', N'DebitReevaluate', N'Dư nợ đánh giá lại', NULL, 0, 0, 105, 0, 0, 1, 6)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('5221e1ac-6b53-43ea-b37c-168babce2a63', '3ce4363e-e0b3-4b18-8a4b-cdc983687c78', N'EmployeeID', N'Nhân viên', N'', 0, 0, 133, 0, 0, 0, 14)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('1829836b-dff3-4091-b04c-17c0248c8a16', 'c143003d-aaac-48bd-8afe-c5e509d62362', N'CashOutDifferVATAmount', N'Chênh lệch tiền thuế', NULL, 0, 1, 105, 0, 0, 0, 37)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('429acfb5-de91-47ef-8cf5-17d724708d18', '3ce4363e-e0b3-4b18-8a4b-cdc983687c78', N'CustomProperty1', N'Cột 1', N'', 0, 0, 98, 0, 0, 0, 19)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('67a58463-b8e1-4fff-aff0-17da39b7eff5', '6083e5f1-c252-4d5f-bac6-e2b7ea594414', N'CashOutDifferAccount', N'TK xử lý chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 81)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('4f721635-105f-42e0-825d-18b69cd295fb', '607916fa-9866-4e49-a74c-7daafe86c7df', N'CreditAccountingObjectName', N'Tên đối tượng Có', N'', 0, 0, 115, 0, 0, 1, 12)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('425185c6-83fd-46ff-bc3c-190414e828ba', 'c6970642-5520-41a8-9f06-0348ebdc458e', N'CashOutAmount', N'QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 77)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('7df86067-6041-4693-9e88-1b2e3a5657ca', '3ce4363e-e0b3-4b18-8a4b-cdc983687c78', N'CurrencyID', N'Loại tiền', N'', 0, 0, 120, 0, 0, 0, 8)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('7f50c09a-ca1c-458f-abf1-1bc263b485bd', '607916fa-9866-4e49-a74c-7daafe86c7df', N'BankAccountDetailID', N'TK ngân hàng', N'Tài khoản ngân hàng', 1, 0, 95, 0, 0, 1, 4)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ce335df4-ad75-4781-863a-1cffd8aec622', '4dcf5134-2f33-44d8-b14c-7cef118d93d8', N'BankAccountDetailID', N'TK ngân hàng', N'Tài khoản ngân hàng', 1, 0, 95, 0, 0, 0, 7)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('31ace98c-080a-4560-b34f-1d3f31c26129', '887a2a4b-0f23-4a9f-b99f-a54c1985a479', N'ExchangeRate', N'Tỷ giá', N'', 0, 0, 101, 0, 0, 0, 5)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('5fe0c6df-aadf-40c1-8b75-1ec99616ca29', '3ce4363e-e0b3-4b18-8a4b-cdc983687c78', N'CustomProperty3', N'Cột 3', N'', 0, 0, 98, 0, 0, 0, 21)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('fa859ea1-a589-459b-aa82-207bc597e8f2', '607916fa-9866-4e49-a74c-7daafe86c7df', N'IsIrrationalCost', N'CP không hợp lý', N'Chi phí không hợp lý', 0, 0, 100, 0, 0, 0, 19)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('1dd1c069-c5ab-497a-abb5-20ad7b07c9e8', '4dcf5134-2f33-44d8-b14c-7cef118d93d8', N'DebitAccountingObjectName', N'Tên đối tượng Nợ', N'', 0, 0, 115, 0, 0, 0, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('8d0fb62f-f001-4f52-8d52-2268b8895265', 'b667f9be-36ad-421e-985e-c75cc4269af3', N'RemainingAmount', N'Số chưa đối trừ QĐ', NULL, 0, 0, 105, 0, 0, 1, 9)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('1a5ab8f2-ba37-4573-b6ae-22cbb799c0ed', '8f4e9c2d-ad35-4912-b9aa-33edd567b6f3', N'CashOutDifferAccount', N'TK xử lý chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 53)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('188a3628-052d-4958-8081-23216871c340', 'bb4a511e-ef0f-4b07-9e21-54d097ea3c58', N'DebitAmountDiffer', N'Chênh lệch dư nợ', NULL, 0, 0, 105, 0, 0, 1, 7)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('3ec6931a-ae91-42c9-95a0-23f326cdc457', '3ce4363e-e0b3-4b18-8a4b-cdc983687c78', N'StatisticsCodeID', N'Mã thống kê', N'', 0, 0, 133, 0, 0, 0, 17)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('080f372d-10e9-40da-9160-245a9a262769', '887a2a4b-0f23-4a9f-b99f-a54c1985a479', N'DebitAccountingObjectID', N'Đối tượng Nợ', N'', 0, 0, 105, 105, 105, 1, 8)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('3660aa3a-12df-44a1-9785-27a311a2aa92', 'b667f9be-36ad-421e-985e-c75cc4269af3', N'RefVoucherNo', N'Số chứng từ', NULL, 0, 0, 105, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('58e9174e-5378-4671-8aeb-28c8032cb012', 'bb4a511e-ef0f-4b07-9e21-54d097ea3c58', N'AccountNumber', N'Tài khoản', NULL, 1, 0, 105, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('599e073d-9603-4393-b293-29389047e9ff', '26f87f37-0790-4141-adb5-fd94a78d58ba', N'CashOutVATAmount', N'Tiền thuế QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 36)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('431d0500-5296-446d-8044-2af2309e4e4c', 'b667f9be-36ad-421e-985e-c75cc4269af3', N'RefVoucherDate', N'Ngày chứng từ', NULL, 0, 0, 105, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('f70177b7-317f-480e-b00a-2c973150342e', '9ec5eae5-eeab-4efb-b763-ee7001ec221a', N'CashOutDifferVATAmount', N'Chênh lệch tiền thuế', NULL, 0, 1, 105, 0, 0, 0, 77)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('a37084a6-fc13-4d74-8198-2d6fddc04356', '955ce7d1-9af4-4cbd-bbca-4129e1b7f3b2', N'CashOutAmount', N'QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 36)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('7a1ae1c0-f79e-4720-9cf0-2e46446cd1f7', '790d6840-094e-4783-aa8b-fe0fbd06b487', N'CashOutAmount', N'QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 61)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('27e5c3e3-9076-4254-966c-2e5844553eb1', '9a7b2621-6c85-4d7c-94f0-63e5f68e2292', N'CashOutAmount', N'QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 36)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('c2e05eba-28d2-42b1-9067-2eb43627f816', '20531b2c-206d-42ba-9635-83a3a6d8213d', N'CashOutAmount', N'QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 67)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('80de9adb-3ed9-481a-91ee-2f54be079854', '20531b2c-206d-42ba-9635-83a3a6d8213d', N'CashOutDifferAmount', N'Chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 68)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('fa8d107d-2812-4df6-87b7-2fdbb927ff82', '887a2a4b-0f23-4a9f-b99f-a54c1985a479', N'DebitAccount', N'TK Nợ', N'Tài khoản Nợ', 1, 0, 95, 95, 95, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('06db8ea7-10e4-439f-a0ed-2ffa3b3a618e', '3ce4363e-e0b3-4b18-8a4b-cdc983687c78', N'DepartmentID', N'Phòng ban', N'', 0, 0, 150, 0, 0, 0, 23)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d1d5c7a1-f75e-454b-83af-301cbf9e751c', 'd3bd03cb-d7f6-43b6-bb7c-0189cf524d21', N'CashOutDifferAmount', N'Chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 69)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('c90a87a1-b329-414a-b3bc-30298fe0b0ee', '59b3b657-0cc0-4385-9ab1-af399b009e40', N'CashOutAmount', N'QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 69)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('24d67768-9d7f-4d5b-a532-33a20a8befc3', '887a2a4b-0f23-4a9f-b99f-a54c1985a479', N'CurrencyID', N'Loại tiền', N'', 0, 0, 118, 0, 0, 0, 4)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('b5d3bebb-0b96-4117-aa4a-33f39d72915e', '607916fa-9866-4e49-a74c-7daafe86c7df', N'CreditAccountingObjectID', N'Đối tượng Có', N'', 0, 0, 105, 105, 105, 1, 11)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('7dc8b59c-a634-46c4-b6d4-3445d02c9918', '887a2a4b-0f23-4a9f-b99f-a54c1985a479', N'CostSetID', N'ĐT tập hợp CP', N'Đối tượng tập hợp chi phí', 0, 0, 150, 150, 150, 1, 16)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('1a353dfb-5f3d-4c0e-a969-34751909cf9d', 'ac683135-1676-4f21-8a2f-053627f27c3f', N'CashOutVATAmount', N'Tiền thuế QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 55)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ac1424a4-0413-46fc-90cd-35c5c13a94ee', '3aee22cb-0b80-4250-bd33-140508452210', N'CashOutExchangeRate', N'Tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 50)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('fb33fa69-eb20-47c1-9dcd-35d4c44b3861', '607916fa-9866-4e49-a74c-7daafe86c7df', N'DebitAccountingObjectName', N'Tên đối tượng Nợ', N'', 0, 0, 115, 0, 0, 1, 10)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('dc21ac0f-6a52-43c3-af58-37366c3e10fd', 'd24a7ffb-a6d1-4b1f-8466-fad033511fb1', N'CashOutAmount', N'QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 77)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ee565870-3655-4565-aa25-386a7d786779', '3ce4363e-e0b3-4b18-8a4b-cdc983687c78', N'DebitAccountingObjectID', N'Đối tượng Nợ', N'', 0, 0, 133, 0, 0, 0, 12)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('7e55c9ef-2188-48e7-a15c-39133c614c16', '4dcf5134-2f33-44d8-b14c-7cef118d93d8', N'CustomProperty3', N'Cột 3', N'', 0, 0, 98, 0, 0, 0, 21)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('7ec39e19-1132-4dff-b4d4-3b764918a655', 'a0e9ea20-66d2-440b-96b1-6312999f6f58', N'CashOutVATAmount', N'Tiền thuế QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 33)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('7f033721-e041-4523-90f5-3bd6e1883ae0', 'd3bd03cb-d7f6-43b6-bb7c-0189cf524d21', N'CashOutExchangeRate', N'Tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 67)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('5270eb5f-a0fd-4c06-8482-3c3624ad5a5e', '35dadd9a-3a12-4a26-90a3-8e21077a6b95', N'CashOutAmountVoucher', N'Số tiền đã xuất quỹ QĐ', NULL, 0, 1, 105, 0, 0, 0, 27)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('b4b152ec-e8c4-4d89-91aa-3eeb34462e89', '4dcf5134-2f33-44d8-b14c-7cef118d93d8', N'AmountOriginal', N'Số tiền', N'', 0, 0, 120, 0, 0, 0, 10)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('fea28c98-ab92-4d53-8e97-3f3f75e6af43', '9bc0236f-823a-4721-bb82-88e63f47572f', N'CashOutExchangeRate', N'Tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 19)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('7c064027-a04f-4cbb-a6e3-3f4cf83e22ef', 'ce3ddf32-08b8-4b7d-8ff8-7bd47061cc9d', N'CurrencyID', N'Loại tiền', N'', 0, 0, 80, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('4753bcbc-e3d1-46e1-b0bc-416266e04251', '9bc0236f-823a-4721-bb82-88e63f47572f', N'CashOutAmount', N'QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 20)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('f58fa26f-be1b-4426-b9b7-416ef12453d9', '887a2a4b-0f23-4a9f-b99f-a54c1985a479', N'CustomProperty3', N'Cột 3', N'', 0, 0, 98, 98, 98, 0, 21)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('cc181cd0-6c97-47ee-8715-41bc24589595', 'b2ea65bc-28e5-4211-911a-8d5448d5f380', N'CashOutDifferAccount', N'TK xử lý chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 40)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('538acde1-9f20-405e-9cff-47b3e4f6c43a', 'b2ea65bc-28e5-4211-911a-8d5448d5f380', N'CashOutDifferAmount', N'Chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 39)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('93f5e18a-3589-497d-a17b-48aec7b23d62', '887a2a4b-0f23-4a9f-b99f-a54c1985a479', N'CustomProperty2', N'Cột 2', N'', 0, 0, 98, 98, 98, 0, 20)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ca5e70a6-494d-4b9e-80e6-48e706e7e90c', '537ddb5f-1f8d-4437-81c4-578036f8ff83', N'CashOutExchangeRate', N'Tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 50)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('3b3423ee-2903-4d8a-879c-496a2423e18f', '3ce4363e-e0b3-4b18-8a4b-cdc983687c78', N'IsIrrationalCost', N'CP không hợp lý', N'Chi phí không hợp lý', 0, 0, 100, 0, 0, 0, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('5f36c817-880c-4493-88cf-49f4e33aa596', '9bc0236f-823a-4721-bb82-88e63f47572f', N'CashOutDifferAmount', N'Chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 21)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d5272514-88ca-47e8-a917-4a05de2530b0', '35dadd9a-3a12-4a26-90a3-8e21077a6b95', N'CashOutAmountVoucherOriginal', N'Số tiền đã xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 26)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('12164887-ccc6-4b28-bb2f-4a1962776e03', '20531b2c-206d-42ba-9635-83a3a6d8213d', N'CashOutDifferAccount', N'TK xử lý chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 69)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('6f6ca8ef-539d-4370-8109-4a50a43f3efb', '8abcb403-a487-4559-8c2a-22b63fcc0c86', N'CashOutDifferAccount', N'TK xử lý chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 23)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('116affab-89e5-4104-8e7a-4aee0d055c72', '9c7f0e85-42be-45ee-8122-33575ac35d8f', N'CashOutDifferAccount', N'TK xử lý chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 38)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('8a3ca439-26c1-4be7-88e7-4bab6ea6c5a7', '0a8c7655-1cff-4a78-983b-bfdb8cb21f76', N'CashOutDifferAccount', N'TK xử lý chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 39)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('3b8521c9-e0cd-47cf-8840-4c093a92d2a4', '607916fa-9866-4e49-a74c-7daafe86c7df', N'BudgetItemID', N'Mục thu/chi', N'', 0, 0, 105, 105, 105, 1, 14)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('70a48831-3bd2-4294-93c0-4cbe5d601da1', '887a2a4b-0f23-4a9f-b99f-a54c1985a479', N'ExpenseItemID', N'Khoản mục CP', N'Khoản mục chi phí', 0, 0, 130, 0, 0, 1, 14)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('486c5bd3-f56f-4135-9f68-4e0105b01e23', '4dcf5134-2f33-44d8-b14c-7cef118d93d8', N'CreditAccountingObjectName', N'Tên đối tượng Có', N'', 0, 0, 115, 0, 0, 0, 4)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('69389c55-3f59-41ce-a723-4e1f4d5a7bff', '35dadd9a-3a12-4a26-90a3-8e21077a6b95', N'CashOutDifferAccount', N'TK xử lý chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 31)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('19f5edbf-648d-4827-bb06-4ee90778047b', '607916fa-9866-4e49-a74c-7daafe86c7df', N'DebitAccount', N'TK Nợ', N'Tài khoản Nợ', 1, 0, 95, 95, 95, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9716acda-30bf-439e-9c3b-504f88947962', '9bd5b723-ca6e-4434-b406-317891442ff7', N'CashOutAmount', N'QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 40)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('e331913a-7d7b-45f4-87d5-51836c7aecc3', 'd2605109-637b-4751-9a1d-b0d743a10333', N'CashOutDifferAccount', N'TK xử lý chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 69)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('e7e7a0c2-9140-4b40-b15e-52369aa701b5', '4dcf5134-2f33-44d8-b14c-7cef118d93d8', N'DebitAccount', N'TK Nợ', N'Tài khoản Nợ', 1, 0, 100, 0, 0, 1, 5)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('68ba0f8b-a283-48bc-8726-52d1431bec51', '87f4fb2f-581f-4d8e-9bd5-7ebd3940fee3', N'CashOutDifferAccount', N'TK xử lý chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 23)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('234cf647-bdd6-4e80-b56b-52dbf7f37e2b', '747265e0-ebd9-4e07-8279-1149550fcebd', N'CashOutDifferVATAmount', N'Chênh lệch tiền thuế', NULL, 0, 1, 105, 0, 0, 0, 70)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d057e516-e3fa-463a-a0f4-541999012e13', '887a2a4b-0f23-4a9f-b99f-a54c1985a479', N'DepartmentID', N'Phòng ban', N'', 0, 0, 150, 0, 0, 1, 15)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('fe86671b-2637-4d3a-8b67-546490fecb76', '44ecd862-4c61-4adc-85de-6265c00c0784', N'CashOutDifferAccount', N'TK xử lý chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 39)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('bb79087e-df26-45bb-9401-54c1b01dca53', '8ea0ddcd-12c0-4bb0-a72c-6c511dc46c11', N'CashOutExchangeRate', N'Tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 50)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('1f861807-bb0a-4c7c-bfcb-55d657739242', '15ec5926-4ffc-4c85-a05b-8d474f326e32', N'CashOutExchangeRate', N'Tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 19)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('fd65ec8a-b9fe-44fb-be67-55e62631f94b', '3ce4363e-e0b3-4b18-8a4b-cdc983687c78', N'Amount', N'Quy đổi', N'', 0, 1, 120, 0, 0, 0, 11)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('65457133-4124-4a02-848c-55ed95027e22', '0a8c7655-1cff-4a78-983b-bfdb8cb21f76', N'CashOutDifferAmount', N'Chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 38)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('bfdcf7eb-b62c-4be0-9e4e-562d1a29931c', 'ac683135-1676-4f21-8a2f-053627f27c3f', N'CashOutDifferVATAmount', N'Chênh lệch tiền thuế', NULL, 0, 1, 105, 0, 0, 0, 56)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9e5b43cc-6268-4351-ab37-56d131f35bdf', '3ce4363e-e0b3-4b18-8a4b-cdc983687c78', N'ExchangeRate', N'Tỷ giá', N'', 0, 0, 120, 0, 0, 0, 9)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('0c71fff0-ea1d-44f1-abd0-5715d2169cdb', '887a2a4b-0f23-4a9f-b99f-a54c1985a479', N'DebitAccountingObjectName', N'Tên đối tượng Nợ', N'', 0, 0, 115, 0, 0, 1, 9)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('8dc7a81d-a601-4572-82aa-59428fde97a3', 'b667f9be-36ad-421e-985e-c75cc4269af3', N'Reason', N'Diễn giải', NULL, 0, 0, 105, 0, 0, 1, 5)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('4917e828-4733-46bb-a6a8-5aea9f23e8d8', '4dcf5134-2f33-44d8-b14c-7cef118d93d8', N'BudgetItemID', N'Mục thu/chi', N'', 0, 0, 105, 0, 0, 0, 15)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d1a6cd6e-261c-4fac-9051-5cc98bd9530d', '8abcb403-a487-4559-8c2a-22b63fcc0c86', N'CashOutAmount', N'QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 21)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('67150b87-fee4-479a-a5db-5cfbc3c3f8cf', '4dcf5134-2f33-44d8-b14c-7cef118d93d8', N'CashOutAmountVoucher', N'Số tiền đã xuất quỹ QĐ', NULL, 0, 1, 105, 0, 0, 1, 27)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('1a82ec8c-a407-465e-8971-5d7ac1b78fe5', 'e967be31-d39f-4ce0-8508-8f07d6d2cdee', N'CashOutVATAmount', N'Tiền thuế QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 50)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d84b1826-4fc9-41f5-b050-5dabe9ea9e54', '23dc24aa-d24b-44e5-9798-307b9fd518dc', N'CashOutDifferAmount', N'Chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 53)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('1e1a5c92-89b9-4375-90be-5e9da09ec93d', '9c7f0e85-42be-45ee-8122-33575ac35d8f', N'CashOutExchangeRate', N'Tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 35)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('fc21d323-aedb-4d0b-be8d-5eb4b40beb0a', '625ec698-4f75-4e77-8619-4983751c1568', N'CashOutVATAmount', N'Tiền thuế QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 69)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d21224fa-d083-4fe4-8c36-5f2a97356383', 'd3bd03cb-d7f6-43b6-bb7c-0189cf524d21', N'CashOutAmount', N'QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 68)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('b1139a88-64a2-4e64-98a2-5fd448f0b55e', '87f4fb2f-581f-4d8e-9bd5-7ebd3940fee3', N'CashOutExchangeRate', N'Tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 20)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ed6bd72b-6a1b-43d0-9df5-6045748c940b', 'd24a7ffb-a6d1-4b1f-8466-fad033511fb1', N'CashOutDifferAmount', N'Chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 78)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('bbecf765-d291-4f09-9a5f-610cd3f3620a', '7b130b67-8fd5-4577-9111-ed948246fc32', N'CashOutDifferAccount', N'TK xử lý chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 23)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('0daa7c0a-66d2-4a51-b7cf-624473c3c164', '4dcf5134-2f33-44d8-b14c-7cef118d93d8', N'CostSetID', N'ĐT tập hợp CP', N'Đối tượng tập hợp chi phí', 0, 0, 178, 0, 0, 0, 16)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d3b5a749-78b8-4c04-ae90-628f3be67552', 'dbd2299f-a098-4c7a-8283-4ed38c7ed486', N'CashOutDifferVATAmount', N'Chênh lệch tiền thuế', NULL, 0, 1, 105, 0, 0, 0, 33)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('55057d02-7b22-4269-b617-63160a2cd6bb', '20531b2c-206d-42ba-9635-83a3a6d8213d', N'CashOutExchangeRate', N'Tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 66)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('bb185b67-b0b5-4462-bd9a-653642fb5725', 'a9f25cb6-8e09-4348-8342-9bc2a2783f3a', N'CashOutDifferVATAmount', N'Chênh lệch tiền thuế', NULL, 0, 1, 105, 0, 0, 0, 52)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('6b007071-d36d-4731-a8fb-6556821a8522', '9bc0236f-823a-4721-bb82-88e63f47572f', N'CashOutDifferAccount', N'TK xử lý chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 22)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('f64ebb81-4425-4a0a-aa77-65937d7fe4bb', 'd24a7ffb-a6d1-4b1f-8466-fad033511fb1', N'CashOutExchangeRate', N'Tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 76)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('fa858494-8d29-4643-8733-66828bc6a3e8', '35dadd9a-3a12-4a26-90a3-8e21077a6b95', N'CashOutExchangeRate', N'Tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 28)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('e3c4dfa1-adab-4bf6-b34a-6749a5e99924', '23dc24aa-d24b-44e5-9798-307b9fd518dc', N'CashOutExchangeRate', N'Tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 51)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ce98c3ce-3097-44ca-8b9a-67e9a1948ffe', '7d9a1620-0fe7-4650-aac6-4a82dfa973ac', N'CashOutDifferAmount', N'Chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 52)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9cef02dc-561b-451f-9f70-69e8a0573ebb', '607916fa-9866-4e49-a74c-7daafe86c7df', N'CurrencyID', N'Loại tiền', N'', 0, 0, 118, 0, 0, 0, 5)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('326b1ae7-572c-4ebd-90a1-6ae2c9f91068', '8f4e9c2d-ad35-4912-b9aa-33edd567b6f3', N'CashOutDifferAmount', N'Chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 52)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('3402e535-205c-40fe-b4a2-6aee991cbfe9', '3aee22cb-0b80-4250-bd33-140508452210', N'CashOutAmount', N'QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 51)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('b0ce6874-57de-4653-bac7-6cea42845f2a', 'b667f9be-36ad-421e-985e-c75cc4269af3', N'RevaluedAmount', N'Đánh giá lại', NULL, 0, 0, 105, 0, 0, 1, 10)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('80a09627-50e4-47d9-bf97-6d226399c81f', '887a2a4b-0f23-4a9f-b99f-a54c1985a479', N'AmountOriginal', N'Số tiền', N'', 0, 0, 120, 120, 120, 0, 6)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('355785c8-444a-4add-b08b-736025d96a70', '7db540d7-3c42-4682-8c8d-2769f550dcec', N'TotalAmount', N'Quy đổi', N'', 0, 0, 120, 0, 0, 0, 4)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('7ef4f26c-9207-401b-9b6f-74f8dc203a1f', '6083e5f1-c252-4d5f-bac6-e2b7ea594414', N'CashOutDifferAmount', N'Chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 80)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('c72d1d27-e5bf-4e42-b11d-7516699625e8', '7b130b67-8fd5-4577-9111-ed948246fc32', N'CashOutDifferAmount', N'Chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 22)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('49718d83-37da-4514-a53b-753fc485cebc', '955ce7d1-9af4-4cbd-bbca-4129e1b7f3b2', N'CashOutDifferAccount', N'TK xử lý chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 38)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('108d233b-2aad-4072-b86e-7573640fd984', 'bb4a511e-ef0f-4b07-9e21-54d097ea3c58', N'CreditReevaluate', N'Dư có đánh giá lại', NULL, 0, 0, 105, 0, 0, 1, 10)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('12737963-fbf3-4865-8d97-7636e763be20', 'bb4a511e-ef0f-4b07-9e21-54d097ea3c58', N'CreditAmount', N'Dư có QĐ', NULL, 0, 0, 105, 0, 0, 1, 9)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('e3551320-3851-49f3-b172-773559505270', '44ecd862-4c61-4adc-85de-6265c00c0784', N'CashOutExchangeRate', N'Tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 36)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('2f26b313-d959-4a83-9255-7aaa176d8d01', '607916fa-9866-4e49-a74c-7daafe86c7df', N'ContractID', N'Hợp đồng', N'', 0, 0, 105, 105, 105, 1, 18)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('e5de6ebe-d2f7-425c-b6e1-7af4163388c7', '696f0072-3a6d-4402-96ec-45fd1d69e1f6', N'CashOutDifferVATAmount', N'Chênh lệch tiền thuế', NULL, 0, 1, 105, 0, 0, 0, 70)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('b4a815fd-d02e-4686-b334-7b28ab55aea1', '9c7f0e85-42be-45ee-8122-33575ac35d8f', N'CashOutDifferAmount', N'Chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 37)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('13a07818-8422-4fdc-b0bd-7b922b5cad0c', '3ce4363e-e0b3-4b18-8a4b-cdc983687c78', N'BankAccountDetailID', N'TK ngân hàng', N'Tài khoản ngân hàng', 1, 0, 95, 0, 0, 0, 7)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('4d356143-8468-4fb6-bc55-7ef034909d2f', '607916fa-9866-4e49-a74c-7daafe86c7df', N'Description', N'Diễn giải', N'', 1, 0, 250, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('4621f66d-0b73-463f-b084-829c28a4da8d', '5ce9e75a-8c17-450b-baf7-e45e5d0b2742', N'CashOutExchangeRate', N'Tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 23)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('57184c66-c4ef-4c74-a374-82fd36c6197e', 'd24a7ffb-a6d1-4b1f-8466-fad033511fb1', N'CashOutDifferAccount', N'TK xử lý chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 79)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('da5d6136-a214-4125-b4cb-864b177a6bda', 'b2ea65bc-28e5-4211-911a-8d5448d5f380', N'CashOutAmount', N'QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 38)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('3b78f7d9-2bac-4d4d-b947-8794a12f5ee3', 'b80a501a-e463-4d01-a2c2-2f66b69adea4', N'CashOutDifferVATAmount', N'Chênh lệch tiền thuế', NULL, 0, 1, 105, 0, 0, 0, 51)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('de30211b-da2a-47fb-856b-87f04c1b0f03', 'a9f25cb6-8e09-4348-8342-9bc2a2783f3a', N'CashOutVATAmount', N'Tiền thuế QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 51)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('758e6fea-08eb-4095-90ad-87faee410280', 'b80a501a-e463-4d01-a2c2-2f66b69adea4', N'CashOutVATAmount', N'Tiền thuế QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 50)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('3714dd30-fef3-44d0-b774-8c5bae90c671', '7db540d7-3c42-4682-8c8d-2769f550dcec', N'ExchangeRate', N'Tỷ giá', N'', 0, 0, 95, 0, 0, 0, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('00d841ba-0598-4c91-b868-8d43b60611fa', '893f18ac-5ef9-4172-9760-f8ebb55d4090', N'CashOutExchangeRate', N'Tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 66)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('db534b94-0924-428a-944c-8e8e657b4ffb', '5ce9e75a-8c17-450b-baf7-e45e5d0b2742', N'CashOutDifferAmount', N'Chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 25)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('0bbab689-d85a-47da-b932-8fdab43a08e3', '9a7b2621-6c85-4d7c-94f0-63e5f68e2292', N'CashOutDifferAmount', N'Chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 37)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('687f8348-7123-4296-8f84-918b83a825eb', '3ce4363e-e0b3-4b18-8a4b-cdc983687c78', N'Description', N'Diễn giải', N'', 1, 0, 250, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('7bfdd216-df44-445e-af8b-91ec40a63985', '537ddb5f-1f8d-4437-81c4-578036f8ff83', N'CashOutDifferAccount', N'TK xử lý chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 53)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('176e3187-3715-43c6-8477-93d9e0ad4593', 'e967be31-d39f-4ce0-8508-8f07d6d2cdee', N'CashOutDifferVATAmount', N'Chênh lệch tiền thuế', NULL, 0, 1, 105, 0, 0, 0, 51)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('669ad7da-5fdd-49a9-b393-98ca64fc4331', '5ce9e75a-8c17-450b-baf7-e45e5d0b2742', N'CashOutDifferAccount', N'TK xử lý chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 26)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('05d1483f-41cc-4633-bb64-99a4eca90c5b', 'cb00a688-1633-48ed-a251-c525636dd47b', N'CashOutVATAmount', N'Tiền thuế QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 58)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('7f217da6-54bc-45b3-ab8b-9a2cc6435720', 'd2605109-637b-4751-9a1d-b0d743a10333', N'CashOutExchangeRate', N'Tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 66)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('bf65958a-8bf4-46a7-889a-9ab6be7122ad', '44ecd862-4c61-4adc-85de-6265c00c0784', N'CashOutDifferAmount', N'Chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 38)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9f735c3c-e364-4074-8ffa-9ad7822a9761', '955ce7d1-9af4-4cbd-bbca-4129e1b7f3b2', N'CashOutDifferAmount', N'Chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 37)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('7e8f15aa-36b8-4d00-b59e-9b427fefcb8c', '5c8f394b-080e-4c92-ae4e-57827fe9565f', N'CashOutAmount', N'QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 53)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9a81f455-45dc-4b20-b03b-9c58130f1cc8', '8e16e973-356c-4aff-ac07-7ad1309b0f40', N'CashOutDifferVATAmount', N'Chênh lệch tiền thuế', NULL, 0, 1, 105, 0, 0, 0, 37)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('c6dac3c9-2e6a-481e-b0b1-9ca7e53e55eb', 'b667f9be-36ad-421e-985e-c75cc4269af3', N'AccountNumber', N'Tài khoản', NULL, 0, 0, 105, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('1b18f0b8-b555-4864-a8be-9e6bfc790bad', '887a2a4b-0f23-4a9f-b99f-a54c1985a479', N'ContractID', N'Hợp đồng', N'', 0, 0, 105, 105, 105, 1, 17)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('adbf49b8-13b2-4f07-b62b-9f2c0c6be039', 'bb4a511e-ef0f-4b07-9e21-54d097ea3c58', N'DebitAmountOriginal', N'Dư nợ', NULL, 0, 0, 105, 0, 0, 1, 4)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('fdeafd82-3d90-4f27-b957-9f7f3b9a8b6f', '8355804b-c527-401c-aeaf-cea0aea1c8b6', N'CashOutDifferVATAmount', N'Chênh lệch tiền thuế', NULL, 0, 1, 105, 0, 0, 0, 37)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('0c64d63b-2c99-405e-ae87-a007f7bac488', '3ce4363e-e0b3-4b18-8a4b-cdc983687c78', N'CustomProperty2', N'Cột 2', N'', 0, 0, 98, 0, 0, 0, 20)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('f89b2548-6ed6-4679-868d-a065a56ec237', '607916fa-9866-4e49-a74c-7daafe86c7df', N'Amount', N'Số tiền', N'', 0, 1, 120, 120, 120, 1, 8)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('c50e68fd-2819-40df-9652-a16805d7b7e5', '4dcf5134-2f33-44d8-b14c-7cef118d93d8', N'EmployeeID', N'Nhân viên', N'', 0, 0, 133, 0, 0, 0, 14)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('3db1d00f-057d-446b-8e3f-a28af0acfee9', '4dcf5134-2f33-44d8-b14c-7cef118d93d8', N'CustomProperty2', N'Cột 2', N'', 0, 0, 98, 0, 0, 0, 20)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9e44e196-97a8-4d11-8413-a28d8666fd1e', '3ce4363e-e0b3-4b18-8a4b-cdc983687c78', N'ContractID', N'Hợp đồng', N'', 0, 0, 133, 0, 0, 0, 18)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('25579717-ac82-42c6-aa40-a2b421978c45', '7b130b67-8fd5-4577-9111-ed948246fc32', N'CashOutAmount', N'QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 21)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('406db5f0-003c-47c7-9259-a322143ef238', '607916fa-9866-4e49-a74c-7daafe86c7df', N'DepartmentID', N'Phòng ban', N'', 0, 0, 150, 0, 0, 1, 16)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('1fe914e4-cfaf-4169-9a32-a43a9ab4d6bb', 'bb4a511e-ef0f-4b07-9e21-54d097ea3c58', N'CreditAmountDiffer', N'Chênh lệch dư có', NULL, 0, 0, 105, 0, 0, 1, 11)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ee3263d4-9fe6-47fd-b6f7-a4fad6d14ac4', '3ce4363e-e0b3-4b18-8a4b-cdc983687c78', N'BudgetItemID', N'Mục thu/chi', N'', 0, 0, 105, 0, 0, 0, 15)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('5835f914-0fa0-4390-b917-a6671c869aa6', '893f18ac-5ef9-4172-9760-f8ebb55d4090', N'CashOutDifferAmount', N'Chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 68)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('65487466-4e25-4aa7-8d69-a68ca9b04a11', '3ce4363e-e0b3-4b18-8a4b-cdc983687c78', N'CreditAccountingObjectID', N'Đối tượng Có', N'', 0, 0, 108, 0, 0, 0, 13)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('cb4b0cd7-d5a2-4c27-aba4-a709bd80298d', '26f87f37-0790-4141-adb5-fd94a78d58ba', N'CashOutDifferVATAmount', N'Chênh lệch tiền thuế', NULL, 0, 1, 105, 0, 0, 0, 37)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('2af7fb0b-b03a-43a5-9e37-a983092c06c2', '607916fa-9866-4e49-a74c-7daafe86c7df', N'EmployeeID', N'Nhân viên', N'', 0, 0, 105, 105, 105, 1, 13)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('8be773b2-dec1-41d7-b121-a99a655d6914', '4dcf5134-2f33-44d8-b14c-7cef118d93d8', N'ContractID', N'Hợp đồng', N'', 0, 0, 133, 0, 0, 0, 18)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('286d8891-1182-4411-9df8-a9dbdff7a113', '537ddb5f-1f8d-4437-81c4-578036f8ff83', N'CashOutDifferAmount', N'Chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 52)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('4139e114-a202-43df-bd1b-ab8a2df2ed4b', '15ec5926-4ffc-4c85-a05b-8d474f326e32', N'CashOutDifferAmount', N'Chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 21)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('372ff059-5f41-40a1-b989-abb3c14fd0fc', '4dcf5134-2f33-44d8-b14c-7cef118d93d8', N'Description', N'Diễn giải', N'', 1, 0, 250, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('68d2ad68-2229-457c-a560-ac63bcd22a0e', '55918c54-7278-400b-abfb-a0262066e029', N'CashOutDifferVATAmount', N'Chênh lệch tiền thuế', NULL, 0, 1, 105, 0, 0, 0, 56)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('08a683c3-421f-427d-b84a-ac64c187d7a7', '9a7b2621-6c85-4d7c-94f0-63e5f68e2292', N'CashOutDifferAccount', N'TK xử lý chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 38)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('11eeb64d-47aa-4b47-8aa9-add89ffeac8c', 'd2605109-637b-4751-9a1d-b0d743a10333', N'CashOutAmount', N'QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 67)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ff1916d6-acaa-4aa5-926c-aeba5b2feae0', 'c6970642-5520-41a8-9f06-0348ebdc458e', N'CashOutDifferAmount', N'Chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 78)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('7aeafa7e-9105-40a1-9b90-b0296f5763b7', '74ca4de8-9d56-423b-b7d2-b2104cb3a128', N'CashOutVATAmount', N'Tiền thuế QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 32)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('7a866ee5-8cd3-4b03-9fb5-b0ea5123715d', '893f18ac-5ef9-4172-9760-f8ebb55d4090', N'CashOutAmount', N'QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 67)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('bd7f9277-5b07-4de0-bf04-b11789d9ff6d', '6083e5f1-c252-4d5f-bac6-e2b7ea594414', N'CashOutAmount', N'QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 79)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('5da86bf4-cc7a-4952-925b-b1367afdee1e', '970138b2-6495-4e39-a27f-af6e6f0e685f', N'CashOutVATAmount', N'Tiền thuế QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 37)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('6c63ff18-491d-47e7-95d1-b1cc1fd7860d', 'c37ead46-7fa1-42c2-86d3-29df8445b818', N'CashOutVATAmount', N'Tiền thuế QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 13)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('32e71f49-f668-446d-b64b-b24bc6b98673', '7d9a1620-0fe7-4650-aac6-4a82dfa973ac', N'CashOutExchangeRate', N'Tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 50)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('000328c7-0746-437b-b050-b29c0185cba3', '937edc31-a449-4e09-92d4-271424c0af3f', N'TaxAmount', N'Số phải nộp', NULL, 0, 0, 120, 0, 0, 1, 4)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('4d208e2d-fb97-4aa6-849d-b4d453e21684', 'd3bd03cb-d7f6-43b6-bb7c-0189cf524d21', N'CashOutDifferAccount', N'TK xử lý chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 70)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('99ba14d0-7d06-4bde-88c5-b53da2003f76', '4dcf5134-2f33-44d8-b14c-7cef118d93d8', N'IsIrrationalCost', N'CP không hợp lý', N'Chi phí không hợp lý', 0, 0, 100, 0, 0, 0, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('728c3d3f-fe73-4d3d-b8ad-b6e4d78d61f3', '15ec5926-4ffc-4c85-a05b-8d474f326e32', N'CashOutAmount', N'QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 20)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('54fde8fd-5d12-49d7-9539-ba7ff2987b6e', '7d9a1620-0fe7-4650-aac6-4a82dfa973ac', N'CashOutDifferAccount', N'TK xử lý chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 53)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9b9552cc-3d7a-48fe-8afa-baa7284ccb16', '23dc24aa-d24b-44e5-9798-307b9fd518dc', N'CashOutDifferAccount', N'TK xử lý chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 54)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('38e6de30-88ec-424d-9ea7-bab5730711ab', '7db540d7-3c42-4682-8c8d-2769f550dcec', N'TotalAmountOriginal', N'Số tiền', N'', 0, 0, 120, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ea9a9f1b-522b-4c8a-9782-babfa6d45174', 'c6970642-5520-41a8-9f06-0348ebdc458e', N'CashOutExchangeRate', N'Tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 76)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('c6c37938-8dd9-4ebd-aa7a-bb01af94387a', '3ec8f39e-2400-4809-9f78-defe4d540b79', N'CashOutAmount', N'QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 77)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('55efb7b4-0ca7-48f5-8d8e-bc2cdefe5666', '13c4300c-dfb2-4989-b25c-7772870167c4', N'CashOutDifferVATAmount', N'Chênh lệch tiền thuế', NULL, 0, 1, 105, 0, 0, 0, 56)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('423ebf18-bb9f-4d10-9ce6-bd407fdb629b', '9ec5eae5-eeab-4efb-b763-ee7001ec221a', N'CashOutVATAmount', N'Tiền thuế QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 76)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('0cfc4651-7529-4618-b3dd-bd91353666c4', 'd2605109-637b-4751-9a1d-b0d743a10333', N'CashOutDifferAmount', N'Chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 68)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('fb708be5-6390-4068-8a8d-bda36c584f1b', '87f4fb2f-581f-4d8e-9bd5-7ebd3940fee3', N'CashOutDifferAmount', N'Chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 22)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('f3e9f6cb-0192-4fba-9ef8-beda2c2f0fa7', 'b44d72f8-5972-45f9-b590-cfec6c4fdc3e', N'CashOutDifferAmount', N'Chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 78)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('95a2e82f-26cf-4e1a-b1fe-bef256387def', 'ce3ddf32-08b8-4b7d-8ff8-7bd47061cc9d', N'TotalAmount', N'Quy đổi', N'', 0, 0, 120, 0, 0, 0, 4)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('7de2c9b2-ecf8-4344-a42f-bf5f2e9b9460', 'b2ea65bc-28e5-4211-911a-8d5448d5f380', N'CashOutExchangeRate', N'Tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 37)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('6f523ba7-5be8-43a9-a676-c0065b0b54f1', '5ce9e75a-8c17-450b-baf7-e45e5d0b2742', N'CashOutAmount', N'QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 24)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('8fb971b6-62bb-4b56-8724-c0690cfa818a', '59b3b657-0cc0-4385-9ab1-af399b009e40', N'CashOutExchangeRate', N'Tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 68)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('32d24a10-5095-45fa-bf09-c0e9a3e09155', '0a8c7655-1cff-4a78-983b-bfdb8cb21f76', N'CashOutAmount', N'QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 37)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('2a068106-6f94-4ee7-9d73-c26db4f8c565', '9a7b2621-6c85-4d7c-94f0-63e5f68e2292', N'CashOutExchangeRate', N'Tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 35)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('3e428ef5-9689-429f-bef7-c2aa38f8d8ac', '607916fa-9866-4e49-a74c-7daafe86c7df', N'ExpenseItemID', N'Khoản mục CP', N'Khoản mục chi phí', 0, 0, 130, 0, 0, 1, 15)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9a76d2ec-ffee-4f83-8762-c3e113b131ce', '8e16e973-356c-4aff-ac07-7ad1309b0f40', N'CashOutVATAmount', N'Tiền thuế QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 36)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('dfebe60b-4be4-4164-b299-c5ec631750ac', '35dadd9a-3a12-4a26-90a3-8e21077a6b95', N'CashOutDifferAmount', N'Chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 30)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('0cdca876-e0ab-4354-af3b-c630b070f4cd', '74ca4de8-9d56-423b-b7d2-b2104cb3a128', N'CashOutDifferVATAmount', N'Chênh lệch tiền thuế', NULL, 0, 1, 105, 0, 0, 0, 33)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('c5b72028-66e1-40bb-8c30-c66e9e780c5b', '59b3b657-0cc0-4385-9ab1-af399b009e40', N'CashOutDifferAccount', N'TK xử lý chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 71)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('0c940ad1-c126-468c-bb93-c6b75afb5f3a', '4dcf5134-2f33-44d8-b14c-7cef118d93d8', N'CashOutAmount', N'QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 1, 29)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('dde91053-6f4b-4017-aad1-c7796679ddc8', '9c7f0e85-42be-45ee-8122-33575ac35d8f', N'CashOutAmount', N'QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 36)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('e6590cca-9826-4414-93c9-c9d4e65023a0', '13c4300c-dfb2-4989-b25c-7772870167c4', N'CashOutVATAmount', N'Tiền thuế QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 55)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ef9443f2-e3c1-4c6e-ae11-ca4ed9e6a89c', '607916fa-9866-4e49-a74c-7daafe86c7df', N'CustomProperty2', N'Cột 2', N'', 0, 0, 98, 98, 98, 0, 22)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('b1da244e-b336-4172-ad5a-cc37c91d92fd', 'c37ead46-7fa1-42c2-86d3-29df8445b818', N'CashOutDifferVATAmount', N'Chênh lệch tiền thuế', NULL, 0, 1, 105, 0, 0, 0, 14)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('cd56ae0b-e9be-4176-b575-cc7578b05f6e', '3ce4363e-e0b3-4b18-8a4b-cdc983687c78', N'DebitAccount', N'TK Nợ', N'Tài khoản Nợ', 1, 0, 100, 0, 0, 1, 5)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('7bec979b-fa0b-407c-b0a1-cd93bb3b738f', '4dcf5134-2f33-44d8-b14c-7cef118d93d8', N'CashOutDifferAmount', N'Chênh lệch', NULL, 0, 1, 105, 0, 0, 1, 30)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d1d3049e-cbe4-410c-8dca-ce121d12ca11', '696f0072-3a6d-4402-96ec-45fd1d69e1f6', N'CashOutVATAmount', N'Tiền thuế QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 69)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('f6812fd4-199b-433a-9121-ce87cc61db22', '85a7f992-5604-4eda-a220-a5fc36e66e38', N'CashOutVATAmount', N'Tiền thuế QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 50)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('f2c06a9d-9233-401e-bfcc-cf848dc9a3f0', '887a2a4b-0f23-4a9f-b99f-a54c1985a479', N'CreditAccount', N'TK Có', N'Tài khoản Có', 1, 0, 95, 95, 95, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('1f2fbbc2-e768-4c6a-9aa3-d0d0e9955313', '4dcf5134-2f33-44d8-b14c-7cef118d93d8', N'CreditAccount', N'TK Có', N'Tài khoản Có', 1, 0, 100, 0, 0, 1, 6)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('c9219768-9e3a-4ddd-8518-d18248593505', '59b3b657-0cc0-4385-9ab1-af399b009e40', N'CashOutDifferAmount', N'Chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 70)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('1d216f54-b061-4364-8803-d20ef65d2cff', '8f4e9c2d-ad35-4912-b9aa-33edd567b6f3', N'CashOutAmount', N'QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 51)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('8c08199f-f979-4179-89e2-d2a6cffc2b74', '4dcf5134-2f33-44d8-b14c-7cef118d93d8', N'CashOutAmountVoucherOriginal', N'Số tiền đã xuất quỹ', NULL, 0, 1, 105, 0, 0, 1, 26)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d52b9012-2244-4c21-ac70-d2cde48f3798', '537ddb5f-1f8d-4437-81c4-578036f8ff83', N'CashOutAmount', N'QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 51)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('24fcee18-f4c1-45bc-9161-d32f0e9824f0', '5c8f394b-080e-4c92-ae4e-57827fe9565f', N'CashOutDifferAccount', N'TK xử lý chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 55)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('26143642-9548-48f6-a16d-d376f1935c18', '4dcf5134-2f33-44d8-b14c-7cef118d93d8', N'StatisticsCodeID', N'Mã thống kê', N'', 0, 0, 133, 0, 0, 0, 17)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('13ba2945-513d-4abf-b6c5-d3d540d152b0', '887a2a4b-0f23-4a9f-b99f-a54c1985a479', N'BudgetItemID', N'Mục thu/chi', N'', 0, 0, 105, 105, 105, 1, 13)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('c385b4ab-0d66-4912-bebe-d5a53016870d', 'b667f9be-36ad-421e-985e-c75cc4269af3', N'DifferAmount', N'Chênh lệch', NULL, 0, 0, 105, 0, 0, 1, 11)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('f0ef0d73-260a-4e74-a0ce-d5b7d3106e39', '3ce4363e-e0b3-4b18-8a4b-cdc983687c78', N'CreditAccount', N'TK Có', N'Tài khoản Có', 1, 0, 100, 0, 0, 1, 6)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('8b2c17ae-497f-404a-a319-d5d8631a79aa', '3ce4363e-e0b3-4b18-8a4b-cdc983687c78', N'CostSetID', N'ĐT tập hợp CP', N'Đối tượng tập hợp chi phí', 0, 0, 178, 0, 0, 0, 16)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('c6bc2f57-0142-440b-b282-d60985a231ee', '0a8c7655-1cff-4a78-983b-bfdb8cb21f76', N'CashOutExchangeRate', N'Tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 36)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('166a1b53-10d5-4176-9451-d638d7d12e9d', '3ec8f39e-2400-4809-9f78-defe4d540b79', N'CashOutDifferAccount', N'TK xử lý chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 79)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('c22348ae-7ff8-4426-affa-d6407d1e0823', '4dcf5134-2f33-44d8-b14c-7cef118d93d8', N'CashOutExchangeRate', N'Tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 1, 28)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('cc10e24b-f920-4f40-b723-d69530629a31', 'b667f9be-36ad-421e-985e-c75cc4269af3', N'AccountingObjectID', N'Đối tượng ', NULL, 0, 0, 105, 0, 0, 1, 4)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('449c27f8-fd16-4263-bffa-d7694421395f', '790d6840-094e-4783-aa8b-fe0fbd06b487', N'CashOutDifferAccount', N'TK xử lý chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 63)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('50201192-61a1-4fcd-98a5-d85910daba7d', '23dc24aa-d24b-44e5-9798-307b9fd518dc', N'CashOutAmount', N'QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 52)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('45db0f83-fd9a-407c-b2f6-d8bed51ce69f', '887a2a4b-0f23-4a9f-b99f-a54c1985a479', N'EmployeeID', N'Nhân viên', N'', 0, 0, 105, 105, 105, 1, 12)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('209fc03b-0667-45a0-8b88-d966a0f49fea', '8abcb403-a487-4559-8c2a-22b63fcc0c86', N'CashOutDifferAmount', N'Chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 22)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('15c38c00-5837-4bf6-a6b0-d9f530afa8d0', 'bb4a511e-ef0f-4b07-9e21-54d097ea3c58', N'BankAccountDetailID', N'TK ngân hàng', NULL, 1, 0, 105, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('f8a3e906-bf4f-4135-9ef9-dc6f293798c6', 'ce3ddf32-08b8-4b7d-8ff8-7bd47061cc9d', N'ExchangeRate', N'Tỷ giá', N'', 0, 0, 95, 0, 0, 0, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('c82231fd-2759-4e8b-8088-dcb4bccaeb89', '3aee22cb-0b80-4250-bd33-140508452210', N'CashOutDifferAmount', N'Chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 52)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('40f1e6dc-75a0-4dbe-9b25-dd071d048fa0', '607916fa-9866-4e49-a74c-7daafe86c7df', N'StatisticsCodeID', N'Mã thống kê', N'', 0, 0, 105, 0, 0, 1, 20)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('a4fbec64-3ba6-4285-b0ba-dee09ad2f9d6', 'bb4a511e-ef0f-4b07-9e21-54d097ea3c58', N'AccountingObjectID', N'Mã đối tượng', NULL, 1, 0, 105, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('5cca21a6-8784-480a-8a46-df5e3630fcd8', '887a2a4b-0f23-4a9f-b99f-a54c1985a479', N'CreditAccountingObjectName', N'Tên đối tượng Có', N'', 0, 0, 115, 0, 0, 1, 11)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('14efde4a-d1fa-472d-9b63-e09e05ce6cdf', 'f6c20177-46ab-4da2-85f0-7c1ff4b8bfe6', N'CashOutVATAmount', N'Tiền thuế QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 50)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('31f25e2e-64a6-4387-851a-e15d5956717c', '4dcf5134-2f33-44d8-b14c-7cef118d93d8', N'CreditAccountingObjectID', N'Đối tượng Có', N'', 0, 0, 108, 0, 0, 0, 13)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('5339cbfa-492b-4764-ba97-e2235470b37d', '5c8f394b-080e-4c92-ae4e-57827fe9565f', N'CashOutExchangeRate', N'Tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 52)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('806447c0-fb0a-44a5-80c2-e236504c44a5', 'c48c8288-1140-4378-bc0f-93162e902164', N'CashOutDifferVATAmount', N'Chênh lệch tiền thuế', NULL, 0, 1, 105, 0, 0, 0, 70)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('4ca10f9f-b2bd-49d8-9b1b-e2a041756a99', '887a2a4b-0f23-4a9f-b99f-a54c1985a479', N'StatisticsCodeID', N'Mã thống kê', N'', 0, 0, 105, 0, 0, 1, 18)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('5ef87931-9522-4d5c-919a-e2d6c1ae0a6e', '55918c54-7278-400b-abfb-a0262066e029', N'CashOutVATAmount', N'Tiền thuế QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 55)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('4e92c74f-313e-418b-9e81-e359dcaf030c', '8355804b-c527-401c-aeaf-cea0aea1c8b6', N'CashOutVATAmount', N'Tiền thuế QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 36)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('73a3e74c-adc4-4ef2-b4ab-e5547ec42869', '8f4e9c2d-ad35-4912-b9aa-33edd567b6f3', N'CashOutExchangeRate', N'Tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 50)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('adb595bf-147c-4208-9a7c-e5a2c9a8e412', '790d6840-094e-4783-aa8b-fe0fbd06b487', N'CashOutExchangeRate', N'Tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 60)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('4fec29a4-1bb4-4252-b9c7-e64468befa63', '3ce4363e-e0b3-4b18-8a4b-cdc983687c78', N'DebitAccountingObjectName', N'Tên đối tượng Nợ', N'', 0, 0, 115, 0, 0, 0, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('44045430-9c69-443a-b145-e653a29bcc11', '887a2a4b-0f23-4a9f-b99f-a54c1985a479', N'CashOutDifferAmount', N'Số tiền', N'', 0, 1, 120, 120, 120, 1, 7)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('f6713f9f-abe6-47c9-9af2-e7195adbf7e5', '607916fa-9866-4e49-a74c-7daafe86c7df', N'CustomProperty3', N'Cột 3', N'', 0, 0, 98, 98, 98, 0, 23)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9b65308d-5ab3-43d5-ab13-e78968bc2ccb', '790d6840-094e-4783-aa8b-fe0fbd06b487', N'CashOutDifferAmount', N'Chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 62)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('c2323b70-11bb-4c3b-9f10-e7cb18377cb7', '970138b2-6495-4e39-a27f-af6e6f0e685f', N'CashOutDifferVATAmount', N'Chênh lệch tiền thuế', NULL, 0, 1, 105, 0, 0, 0, 38)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('f340810d-3eed-4b1c-917d-e972f5a7c433', '4cf1f231-03cb-4ee2-89e5-64bffe008d2d', N'CashOutDifferVATAmount', N'Chênh lệch tiền thuế', NULL, 0, 1, 105, 0, 0, 0, 77)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('fd4171ef-2945-47b8-a89a-e9df228c54c2', 'f6c20177-46ab-4da2-85f0-7c1ff4b8bfe6', N'CashOutDifferVATAmount', N'Chênh lệch tiền thuế', NULL, 0, 1, 105, 0, 0, 0, 51)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('e84b79ed-5c5d-4a26-8f5b-ea6674d8ecd0', '3ce4363e-e0b3-4b18-8a4b-cdc983687c78', N'AmountOriginal', N'Số tiền', N'', 0, 0, 120, 0, 0, 1, 10)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('c867f6d4-0721-4753-ba16-eb666ca47473', '15ec5926-4ffc-4c85-a05b-8d474f326e32', N'CashOutDifferAccount', N'TK xử lý chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 22)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('4b66eb6f-f215-40a6-9733-eb6898c7d53d', '9bd5b723-ca6e-4434-b406-317891442ff7', N'CashOutDifferAmount', N'Chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 41)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('521c8918-a1cc-4e8e-9aa6-ebf3a40ca383', 'c48c8288-1140-4378-bc0f-93162e902164', N'CashOutVATAmount', N'Tiền thuế QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 69)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('5b8cab61-4185-4725-873c-ec4c947a6f61', '887a2a4b-0f23-4a9f-b99f-a54c1985a479', N'CustomProperty1', N'Cột 1', N'', 0, 0, 98, 98, 98, 0, 19)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('16c1e788-5d72-4a75-a0ee-ecc91a61798e', '4dcf5134-2f33-44d8-b14c-7cef118d93d8', N'Amount', N'Quy đổi', N'', 0, 1, 120, 0, 0, 0, 11)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('a3f4b15f-95da-4151-8333-ee123868c226', '955ce7d1-9af4-4cbd-bbca-4129e1b7f3b2', N'CashOutExchangeRate', N'Tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 35)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('67aba6b6-8c2a-45c0-aef9-ef767abf0ab7', '747265e0-ebd9-4e07-8279-1149550fcebd', N'CashOutVATAmount', N'Tiền thuế QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 69)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('83d9b937-6af1-4326-8468-f0ef8fe5c564', '607916fa-9866-4e49-a74c-7daafe86c7df', N'CustomProperty1', N'Cột 1', N'', 0, 0, 98, 98, 98, 0, 21)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d8047841-f0cc-4379-8f4f-f241269d4ac8', '893f18ac-5ef9-4172-9760-f8ebb55d4090', N'CashOutDifferAccount', N'TK xử lý chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 69)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('7e56aadb-e952-4e2e-9c76-f26130240bbc', 'b44d72f8-5972-45f9-b590-cfec6c4fdc3e', N'CashOutAmount', N'QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 77)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('b07261e2-83bb-4361-8f9b-f2ce366a7b40', '607916fa-9866-4e49-a74c-7daafe86c7df', N'CostSetID', N'ĐT tập hợp CP', N'Đối tượng tập hợp chi phí', 0, 0, 150, 150, 150, 1, 17)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('6ac6a972-3008-482c-930f-f2f8f132f98b', '87f4fb2f-581f-4d8e-9bd5-7ebd3940fee3', N'CashOutAmount', N'QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 21)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ae06a228-837e-4cdf-a7f1-f4821b042d19', 'c6970642-5520-41a8-9f06-0348ebdc458e', N'CashOutDifferAccount', N'TK xử lý chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 79)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('612d8369-b032-4b75-a8ee-f4aceb0e8149', '4dcf5134-2f33-44d8-b14c-7cef118d93d8', N'ExpenseItemID', N'Khoản mục CP', N'Khoản mục chi phí', 0, 0, 130, 0, 0, 0, 25)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('7ed01c40-0925-4649-beb3-f4f8587d3b37', 'dbd2299f-a098-4c7a-8283-4ed38c7ed486', N'CashOutVATAmount', N'Tiền thuế QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 32)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('172a402f-ffda-4a4b-90e4-f73fdfb5b5fd', 'bb4a511e-ef0f-4b07-9e21-54d097ea3c58', N'CreditAmountOriginal', N'Dư có', NULL, 0, 0, 105, 0, 0, 1, 8)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('6f948e28-e9a7-46e9-a02c-f7627b75e80d', '887a2a4b-0f23-4a9f-b99f-a54c1985a479', N'Description', N'Diễn giải', N'', 1, 0, 250, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('dc126af3-a511-4b85-9005-f8d309aa9a02', '3ec8f39e-2400-4809-9f78-defe4d540b79', N'CashOutExchangeRate', N'Tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 76)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('b6dbacb9-8065-4a45-817f-f926e63a02c3', '8ea0ddcd-12c0-4bb0-a72c-6c511dc46c11', N'CashOutDifferAmount', N'Chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 52)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('2c177400-8b39-4e12-b993-f95da7882437', '4dcf5134-2f33-44d8-b14c-7cef118d93d8', N'DebitAccountingObjectID', N'Đối tượng Nợ', N'', 0, 0, 133, 0, 0, 0, 12)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('03b8a9b7-0737-40d0-a8ac-f9bdfd409af1', '625ec698-4f75-4e77-8619-4983751c1568', N'CashOutDifferVATAmount', N'Chênh lệch tiền thuế', NULL, 0, 1, 105, 0, 0, 0, 70)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('fea3f40a-f889-4f06-bb30-fa021ff4d587', 'b667f9be-36ad-421e-985e-c75cc4269af3', N'RefVoucherExchangeRate', N'Tỷ giá chứng từ', NULL, 0, 0, 105, 0, 0, 1, 6)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('900d2350-a940-4be6-b321-fb4953c7f964', 'b44d72f8-5972-45f9-b590-cfec6c4fdc3e', N'CashOutDifferAccount', N'TK xử lý chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 79)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('1bce3983-e5fd-4994-9c1d-fb52a3056f84', '8abcb403-a487-4559-8c2a-22b63fcc0c86', N'CashOutExchangeRate', N'Tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 20)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('25a9cd99-4ca3-4366-84e8-fc16ae6da789', 'bb4a511e-ef0f-4b07-9e21-54d097ea3c58', N'DebitAmount', N'Dư nợ QĐ', NULL, 0, 0, 105, 0, 0, 1, 5)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('4ddabc62-94eb-40b3-82de-fcf127b8553d', '7b130b67-8fd5-4577-9111-ed948246fc32', N'CashOutExchangeRate', N'Tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 20)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('4d23e892-c76d-4b58-9174-fd09e0bd2840', 'a0e9ea20-66d2-440b-96b1-6312999f6f58', N'CashOutDifferVATAmount', N'Chênh lệch tiền thuế', NULL, 0, 1, 105, 0, 0, 0, 34)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('1228c6a9-4289-4ab2-b8a7-ff9483ccef38', 'b44d72f8-5972-45f9-b590-cfec6c4fdc3e', N'CashOutExchangeRate', N'Tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 76)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('7db540d7-3c42-4682-8c8d-2769f550dcec', 'fb3dc6c5-b3cb-422d-a0fe-26c72c2503e1', 0, 100, NULL)
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('bb4a511e-ef0f-4b07-9e21-54d097ea3c58', '0ea573da-afed-4f65-aa45-7c3dbbc6c2eb', 2, 2, N'&3. Số dư TK gốc ngoại tệ đánh giá lại')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('ce3ddf32-08b8-4b7d-8ff8-7bd47061cc9d', '70453208-b426-40b2-bdec-4f28cd2eec87', 0, 100, NULL)
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('4dcf5134-2f33-44d8-b14c-7cef118d93d8', '70c2c082-3993-41be-9945-89735de68af1', 1, 0, N'&1. Hạch toán')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('607916fa-9866-4e49-a74c-7daafe86c7df', '0ea573da-afed-4f65-aa45-7c3dbbc6c2eb', 1, 1, N'&2. Thống kê')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('887a2a4b-0f23-4a9f-b99f-a54c1985a479', '70c2c082-3993-41be-9945-89735de68af1', 1, 1, N'&2. Thống kê')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('b667f9be-36ad-421e-985e-c75cc4269af3', '0ea573da-afed-4f65-aa45-7c3dbbc6c2eb', 2, 3, N'&4. Chứng từ công nợ và thanh toán bằng ngoại tệ')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('3ce4363e-e0b3-4b18-8a4b-cdc983687c78', '0ea573da-afed-4f65-aa45-7c3dbbc6c2eb', 1, 0, N'&1. Hạch toán')
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

INSERT dbo.TMAppendixList(ID, TypeID, AppendixCode, AppendixName, TypeGroup) VALUES ('0abaf8fd-7c18-42fc-8b0d-13dcbbd35e78', 101, N'PL01-1/GTGT', N'PL 01-1/GTGT: Bảng kê bán ra thuế GTGT', 100)
INSERT dbo.TMAppendixList(ID, TypeID, AppendixCode, AppendixName, TypeGroup) VALUES ('078b006b-5583-4b15-a1cf-15411cbcae27', 102, N'PL01-2/GTGT', N'PL 01-2/GTGT: Bảng kê mua vào thuế GTGT', 100)
INSERT dbo.TMAppendixList(ID, TypeID, AppendixCode, AppendixName, TypeGroup) VALUES ('e75e298b-731f-4579-96a1-84b8910f1557', 121, N'PL03-1A/TNDN', N'PL 03-1A/TNDN: Kết quả hoạt động sản xuất kinh doanh', 120)
INSERT dbo.TMAppendixList(ID, TypeID, AppendixCode, AppendixName, TypeGroup) VALUES ('21d578b0-a7b1-49cf-b955-8f85aeb4eb3b', 122, N'PL03-2A/TNDN', N'PL 03-2A/TNDN: Chuyển lỗ từ hoạt động sản xuất kinh doanh', 120)
INSERT dbo.TMAppendixList(ID, TypeID, AppendixCode, AppendixName, TypeGroup) VALUES ('5f0fd41f-4054-43a5-b2c4-a32f7ecfb91c', 131, N'PL01-1/TTDB', N'PL 01-1/TTĐB: Bảng kê xác định số thuế TTĐB được khấu trừ ', 130)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

INSERT dbo.TMIndustryType(ID, IndustryTypeCode, IndustryTypeName) VALUES ('83558e4e-3401-4127-855a-00c97d9ebf89', N'1', N'Nông nghiệp, lâm nghiệp và thủy sản')
INSERT dbo.TMIndustryType(ID, IndustryTypeCode, IndustryTypeName) VALUES ('598b446e-1506-4a24-a088-0dc53f7693ce', N'2', N'Khai khoáng')
INSERT dbo.TMIndustryType(ID, IndustryTypeCode, IndustryTypeName) VALUES ('3cb61746-1e4d-45cc-9630-121c47744133', N'3', N'Công nghiệp chế biến, chế tạo')
INSERT dbo.TMIndustryType(ID, IndustryTypeCode, IndustryTypeName) VALUES ('74c6afb1-f581-48c2-a064-14f5f9dd2e67', N'4', N'Sản xuất và phân phối điện, khí đốt, nước nóng, hơi nước và điều hòa không khí')
INSERT dbo.TMIndustryType(ID, IndustryTypeCode, IndustryTypeName) VALUES ('c3dc1e2a-51a2-469e-8111-232ac5f8aa26', N'5', N'Cung cấp nước; hoạt động quản lý và xử lý rác thải, nước thải')
INSERT dbo.TMIndustryType(ID, IndustryTypeCode, IndustryTypeName) VALUES ('75f66ef2-7e3f-44f5-98fd-33ca057d75a3', N'0', N'')
INSERT dbo.TMIndustryType(ID, IndustryTypeCode, IndustryTypeName) VALUES ('6d79973a-bec8-4f53-a41e-344b7e38a154', N'6', N'Xây dựng')
INSERT dbo.TMIndustryType(ID, IndustryTypeCode, IndustryTypeName) VALUES ('1aec956b-10e1-431b-b61f-3b6eef06eb4e', N'7', N'Bán buôn và bán lẻ; sửa chữa ô tô, mô tô, xe máy và xe có động cơ khác')
INSERT dbo.TMIndustryType(ID, IndustryTypeCode, IndustryTypeName) VALUES ('71df73ad-921c-4cb4-a0c4-3d5f75ed2160', N'8', N'Vận tải kho bãi')
INSERT dbo.TMIndustryType(ID, IndustryTypeCode, IndustryTypeName) VALUES ('0914b2b8-b90b-4d69-9d75-4a39f89b4b6f', N'9', N'Dịch vụ lưu trú và ăn uống')
INSERT dbo.TMIndustryType(ID, IndustryTypeCode, IndustryTypeName) VALUES ('54316a1e-027b-4739-8307-5ea0cb8d00df', N'10', N'Thông tin và truyền thông')
INSERT dbo.TMIndustryType(ID, IndustryTypeCode, IndustryTypeName) VALUES ('0518b12c-b5e1-4533-b36c-78a553d3a944', N'11', N'Hoạt động tài chính, ngân hàng và bảo hiểm')
INSERT dbo.TMIndustryType(ID, IndustryTypeCode, IndustryTypeName) VALUES ('16ec8e7f-3a3c-4c4d-8acd-79647cccdbb1', N'12', N'Hoạt động kinh doanh bất động sản')
INSERT dbo.TMIndustryType(ID, IndustryTypeCode, IndustryTypeName) VALUES ('a7c7065c-784b-44d2-88b8-875a871e9346', N'13', N'Hoạt động chuyên môn, khoa học và công nghệ')
INSERT dbo.TMIndustryType(ID, IndustryTypeCode, IndustryTypeName) VALUES ('80d87f5f-7ca2-4911-aee7-95bf2055020d', N'14', N'Hoạt động hành chính và dịch vụ hỗ trợ')
INSERT dbo.TMIndustryType(ID, IndustryTypeCode, IndustryTypeName) VALUES ('0ee46e8d-43fe-4799-b153-a25ac7c3fc40', N'15', N'Hoạt động của Đảng cộng sản, tổ chức chính trị - xã hội, quản lý nhà nước, an ninh quốc phòng; bảo đảm xã hội bắt buộc')
INSERT dbo.TMIndustryType(ID, IndustryTypeCode, IndustryTypeName) VALUES ('1d916e45-3ae6-4989-aeee-a3051fdef460', N'16', N'Giáo dục và đào tạo')
INSERT dbo.TMIndustryType(ID, IndustryTypeCode, IndustryTypeName) VALUES ('132ac052-e709-4774-87fe-a419c65b9d0a', N'17', N'Y tế và hoạt động trợ giúp xã hội')
INSERT dbo.TMIndustryType(ID, IndustryTypeCode, IndustryTypeName) VALUES ('d664da2c-b8a2-4389-84a3-b91b2537fe66', N'18', N'Nghệ thuật, vui chơi và giải trí')
INSERT dbo.TMIndustryType(ID, IndustryTypeCode, IndustryTypeName) VALUES ('9ae91f57-38eb-449a-b079-c60a7474539c', N'19', N'Hoạt động dịch vụ khác')
INSERT dbo.TMIndustryType(ID, IndustryTypeCode, IndustryTypeName) VALUES ('0186cbaa-b1cb-4724-ae94-dd6ce0f12f69', N'20', N'Hoạt động làm thuê các công việc trong các hộ gia đình, sản xuất sản phẩm vật chất và dịch vụ tự tiêu dùng của hộ gia đình')
INSERT dbo.TMIndustryType(ID, IndustryTypeCode, IndustryTypeName) VALUES ('dab05dae-647f-4d2d-b481-efdd9b9b1950', N'21', N'Hoạt động của các tổ chức và cơ quan quốc tế')
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

SET IDENTITY_INSERT dbo.Type ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
INSERT dbo.Type(ID, TypeName, TypeGroupID, Recordable, Searchable, PostType, OrderPriority) VALUES (601, N'Đánh giá lại tài khoản ngoại tệ', 60, 0, 0, 1, 0)
GO
SET IDENTITY_INSERT dbo.Type OFF
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

SET IDENTITY_INSERT dbo.VoucherPatternsReport ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (229, 0, 0, N'GTGT-02', N'tờ khai thuế giá trị gia tăng dành cho dự án đầu tư', N'ToKhaiThueGiaTriGiaTangDanhChoDuAnDauTu02.rst', 1, NULL)
GO
SET IDENTITY_INSERT dbo.VoucherPatternsReport OFF
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DBCC CHECKIDENT('dbo.VoucherPatternsReport', RESEED, 229)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.GenCode WITH NOCHECK
  ADD CONSTRAINT FK_GenCode_TypeGroup FOREIGN KEY (TypeGroupID) REFERENCES dbo.TypeGroup(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.Template WITH NOCHECK
  ADD CONSTRAINT FK_Template_Type FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE CASCADE ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateColumn WITH NOCHECK
  ADD CONSTRAINT FK_TemplateColumn_TemplateDetail FOREIGN KEY (TemplateDetailID) REFERENCES dbo.TemplateDetail(ID) ON DELETE CASCADE ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateDetail WITH NOCHECK
  ADD CONSTRAINT FK_TemplateDetail_Template FOREIGN KEY (TemplateID) REFERENCES dbo.Template(ID) ON DELETE CASCADE ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.Type WITH NOCHECK
  ADD CONSTRAINT FK_Type_TypeGroup FOREIGN KEY (TypeGroupID) REFERENCES dbo.TypeGroup(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153AdjustAnnouncement WITH NOCHECK
  ADD CONSTRAINT FK__TT153Adju__TypeI__25869641 FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153DeletedInvoice WITH NOCHECK
  ADD CONSTRAINT FK__TT153Dele__TypeI__67A95F59 FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153DestructionInvoice WITH NOCHECK
  ADD CONSTRAINT FK__TT153Dest__TypeI__22AA2996 FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153LostInvoice WITH NOCHECK
  ADD CONSTRAINT FK__TT153Lost__TypeI__1FCDBCEB FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153PublishInvoice WITH NOCHECK
  ADD CONSTRAINT FK__TT153Publ__TypeI__1CF15040 FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153RegisterInvoice WITH NOCHECK
  ADD CONSTRAINT FK__TT153Regi__TypeI__1A14E395 FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE T1
SET    CurrencyID = T2.CurrencyID,
      ExchangeRate = T2.ExchangeRate
FROM   GOtherVoucher T1
       JOIN GOtherVoucherDetail T2
         ON T1.ID = T2.GOtherVoucherID
UPDATE T1
SET    CurrencyID = T2.CurrencyID,
    ExchangeRate = T2.ExchangeRate
FROM   MBInternalTransfer T1
       JOIN MBInternalTransferDetail T2
         ON T1.ID = T2.MBInternalTransferID

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

IF @@TRANCOUNT>0 COMMIT TRANSACTION
GO

SET NOEXEC OFF
GO
