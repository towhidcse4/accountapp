SET CONCAT_NULL_YIELDS_NULL, ANSI_NULLS, ANSI_PADDING, QUOTED_IDENTIFIER, ANSI_WARNINGS, ARITHABORT, XACT_ABORT ON
SET NUMERIC_ROUNDABORT, IMPLICIT_TRANSACTIONS OFF
GO


SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

BEGIN TRANSACTION
GO

IF OBJECT_ID(N'dbo.DateTimeCacheHistory', N'U') IS NULL
BEGIN
    CREATE TABLE [dbo].[DateTimeCacheHistory] (
  [ID] [uniqueidentifier] NOT NULL,
  [IDUser] [uniqueidentifier] NOT NULL,
  [TypeID] [int] NULL,
  [SubSystemCode] [nvarchar](512) NULL,
  [SelectedItem] [int] NULL,
  [DtBeginDate] [datetime] NULL,
  [DtEndDate] [datetime] NULL,
  [MonthOrPrecious] [int] NULL,
  [Month] [int] NULL,
  [Precious] [int] NULL,
  [Year] [int] NULL,
  CONSTRAINT [PK_DateTimeCacheHistory] PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO


ALTER TABLE [dbo].[Parameters]
  ADD PRIMARY KEY CLUSTERED ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[OPAccount]
  ADD PRIMARY KEY CLUSTERED ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
IF OBJECT_ID(N'dbo.Proc_GetBkeInv_Sale_QT', N'P') IS NULL
exec('Create PROCEDURE [dbo].[Proc_GetBkeInv_Sale_QT]
      (
      @BranchID UNIQUEIDENTIFIER ,
      @IncludeDependentBranch BIT ,
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @IsSimilarBranch BIT
    )
AS
BEGIN 
/*
VATRate type :
-1: Không chịu thuế
0: thuê 0%
5: 5%
10: 10%
*/
/*add by cuongpv de thong nhat cach lam tron so lieu*/
	DECLARE @tbDataLocal TABLE(
		VATRate DECIMAL(25,10),
		so_hd NVARCHAR(25),
		ngay_hd DATETIME,
		Nguoi_mua NVARCHAR(512),
		MST NVARCHAR(50),
		Mat_hang NVARCHAR(512),
		Amount DECIMAL(25,0),
		DiscountAmount DECIMAL(25,0),
		Thue_GTGT DECIMAL(25,0),
		TKThue NVARCHAR(25),
		flag int,
		RefID UNIQUEIDENTIFIER,
		RefType int,
		StatusInvoice int
	)
/*end add by cuongpv*/
	if @IsSimilarBranch = 0
	Begin
		/*add by cuongpv*/
		INSERT INTO @tbDataLocal
		/*end add by cuongpv*/
		 select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST,
		  Description as Mat_hang, Amount, DiscountAmount, VATAmount as Thue_GTGT, VATAccount as TKThue,1 flag
		  ,a.ID as RefID, a.TypeID as RefType, a.StatusInvoice
		    /*Edit by cuongpv (Amount-DiscountAmount) as Doanh_so*/
		   from SAinvoice a join SAInvoiceDetail b on a.id = b.SAInvoiceID where [PostedDate] between @FromDate and @ToDate and Recorded = 1 AND a.BillRefID is null
		union all
		select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST, 
		Description as Mat_hang, Amount, DiscountAmount, VATAmount as Thue_GTGT, VATAccount as TKThue,1 flag  /*Edit by cuongpv (Amount-DiscountAmount) as Doanh_so*/
		,a.ID as RefID, a.TypeID as RefType, a.StatusInvoice
		 from  SABill a join SABillDetail b on a.ID = b.SABillID  where cast(InvoiceDate As Date) between @FromDate and @ToDate
		union all
		select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST,
		  Description as Mat_hang,  Amount, DiscountAmount, VATAmount as Thue_GTGT, VATAccount as TKThue,-1 flag  /*Edit by cuongpv (Amount-DiscountAmount) as Doanh_so*/
		,a.ID as RefID, a.TypeID as RefType, a.StatusInvoice
		 from SAReturn a join SAReturnDetail b on a.id = b.SAReturnID
		where [PostedDate] between @FromDate and @ToDate and recorded = 1 

		update @tbDataLocal set Amount = 0, DiscountAmount = 0, Thue_GTGT = 0 where StatusInvoice = 5 or StatusInvoice = 3 or RefID In (Select SAInvoiceID from TT153DeletedInvoice)
		

		select VATRate, so_hd, ngay_hd, Nguoi_mua, MST, Mat_hang, (Amount - DiscountAmount) as Doanh_so, Thue_GTGT, TKThue, flag,RefID,RefType
		from @tbDataLocal order by VATRate, ngay_hd, so_hd, Nguoi_mua
	End
	else
	Begin
		/*add by cuongpv*/
		INSERT INTO @tbDataLocal
		/*end add by cuongpv*/
			select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST,
			  a.Reason as Mat_hang, Amount, DiscountAmount, VATAmount as Thue_GTGT, VATAccount as TKThue,1 flag  /*Edit by cuongpv (Amount-DiscountAmount) as Doanh_so*/
			   ,a.ID as RefID, a.TypeID as RefType, a.StatusInvoice
			   from SAinvoice a join SAInvoiceDetail b on a.id = b.SAInvoiceID where [PostedDate] between @FromDate and @ToDate and Recorded = 1 AND a.BillRefID is null
			union all
			select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST, 
			a.Reason as Mat_hang, Amount, DiscountAmount, VATAmount as Thue_GTGT, VATAccount as TKThue,1 flag  /*Edit by cuongpv (Amount-DiscountAmount) as Doanh_so*/
			 ,a.ID as RefID, a.TypeID as RefType, a.StatusInvoice
			 from  SABill a join SABillDetail b on a.ID = b.SABillID  where cast(InvoiceDate As Date) between @FromDate and @ToDate
			union all
			select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST,
			  a.Reason as Mat_hang,  Amount, DiscountAmount, VATAmount as Thue_GTGT, VATAccount as TKThue,-1 flag  /*Edit by cuongpv (Amount-DiscountAmount) as Doanh_so*/
			 ,a.ID as RefID, a.TypeID as RefType, a.StatusInvoice
			 from SAReturn a join SAReturnDetail b on a.id = b.SAReturnID
			where [PostedDate] between @FromDate and @ToDate and recorded = 1

		update @tbDataLocal set Amount = 0, DiscountAmount = 0, Thue_GTGT = 0 where StatusInvoice = 5 or StatusInvoice = 3 or RefID In (Select SAInvoiceID from TT153DeletedInvoice)

		select VATRate, 
			   so_hd, 
			   ngay_hd, 
			   Nguoi_mua, 
			   MST,
			   Mat_hang, 
			   sum(Doanh_so) Doanh_so, 
			   sum(Thue_GTGT) Thue_GTGT, 
			   TKThue,
			   flag
		from
			( select VATRate, so_hd, ngay_hd, Nguoi_mua, MST, Mat_hang, (Amount - DiscountAmount) as Doanh_so, Thue_GTGT, TKThue, flag 
			  from @tbDataLocal
			) aa
		group by VATRate, 
			   so_hd, 
			   ngay_hd, 
			   Nguoi_mua, 
			   MST,
			   Mat_hang, 
			   TKThue ,
			   flag
		order by VATRate, ngay_hd, so_hd,Nguoi_mua
	End
end;')
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO


GO
ALTER PROCEDURE [dbo].[Proc_GetBkeInv_Buy_Tax]
      (
      @BranchID UNIQUEIDENTIFIER ,
      @IncludeDependentBranch BIT ,
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @IsSimilarBranch BIT,
	  @KhauTru Smallint
    )
AS
BEGIN
/*add by cuongpv de xu ly cach thong nhat lam tron*/
	DECLARE @tbDataLocal TABLE(
		VoucherID UNIQUEIDENTIFIER,
		GoodsServicePurchaseCode NVARCHAR(25),
		GoodsServicePurchaseName NVARCHAR(512),
		So_HD NVARCHAR(25),
		Ngay_HD DATETIME,
		ten_NBan NVARCHAR(512),
		MST NVARCHAR(50),
		Mat_hang NVARCHAR(512),
		GT_chuathue decimal(25,0),
		Thue_suat decimal(25,10),
		Thue_GTGT decimal(25,0),
		TK_Thue NVARCHAR(25),
		flag int,
		AccountingObjectID UNIQUEIDENTIFIER 
	)
/*end add by cuongpv*/
	IF (@KhauTru = 0)
	BEGIN
		if @IsSimilarBranch = 0
		Begin    
			/*add by cuongpv*/
			INSERT INTO @tbDataLocal
			/*end add by cuongpv*/
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD ,AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
			 b.Description as Mat_hang, (Amount - DiscountAmount) as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID                  
			 from PPInvoice a join PPInvoiceDetail b on a.ID = b.PPInvoiceID         
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
			where cast(InvoiceDate As Date) <= @ToDate and Recorded = 1
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all         
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
			 b.Description as Mat_hang, Amount as GiatriHH_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,-1 flag, b.AccountingObjectID as AccountingObjectID   
			from PPDiscountReturn a join PPDiscountReturnDetail b on a.ID = b.PPDiscountReturnID         
			join GoodsServicePurchase c on c.ID= b.GoodsServicePurchaseID         
			where cast(InvoiceDate As Date) <= @ToDate and Recorded = 1
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=a.InvoiceNo) = 0)
			union all    
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,         
			 b.Description as Mat_hang, (Amount - DiscountAmount) as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID         
			 from PPService a join PPServiceDetail b on a.ID = b.PPServiceID         
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
			where cast(InvoiceDate As Date) <= @ToDate and Recorded = 1 
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
			b.Description as Mat_hang, (Amount - DiscountAmount) as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID																	
			from FAIncrement a join FAIncrementDetail b on a.ID = b.FAIncrementID																	
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate <= @ToDate and Recorded = 1 and a.TypeID not in (510)
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
			b.Description as Mat_hang, (Amount - DiscountAmount) as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag, b.AccountingObjectID as AccountingObjectID																
			from TIIncrement a join TIIncrementDetail b on a.ID = b.TIIncrementID																	
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate <= @ToDate and Recorded = 1 and a.TypeID not in (907)
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag,NULL as AccountingObjectID																		
			 from GOtherVoucher a join GOtherVoucherDetailTax b on a.ID = b.GOtherVoucherID		
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate <= @ToDate and Recorded = 1 
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag,NULL as AccountingObjectID																	
			 from MCPayment a join MCPaymentDetailTax b on a.ID = b.MCPaymentID																
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate <= @ToDate and a.Recorded = 1 
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag,NULL as AccountingObjectID																		
			 from MBTellerPaper a join MBTellerPaperDetailTax b on a.ID = b.MBTellerPaperID		
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate <= @ToDate and a.Recorded = 1 
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag,NULL as AccountingObjectID																	
			 from MBCreditCard a join MBCreditCardDetailTax b on a.ID = b.MBCreditCardID		
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate <= @ToDate and a.Recorded = 1
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
		
			/*edit by Hautv */
			UPDATE @tbDataLocal SET ten_NBan = d.AccountingObjectName
			FROM (select * from AccountingObject) d
				where AccountingObjectID = d.ID;

			/*edit by cuongpv order by c.GoodsServicePurchaseCode, InvoiceDate, InvoiceNo -> SELECT * FROM @tbDataLocal order by GoodsServicePurchaseCode, Ngay_HD, So_HD;*/
		
			SELECT * FROM @tbDataLocal order by GoodsServicePurchaseCode, Ngay_HD, So_HD;
		
		End
		else
		Begin
			/*add by cuongpv*/
			INSERT INTO @tbDataLocal
			/*end add by cuongpv*/
			 select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
			 a.Reason as Mat_hang, (Amount - DiscountAmount) as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID         
			 from PPInvoice a join PPInvoiceDetail b on a.ID = b.PPInvoiceID         
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
			where cast(InvoiceDate As Date) <= @ToDate and Recorded = 1
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all         
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
			 a.Reason as Mat_hang, Amount as GiatriHH_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,-1 flag, b.AccountingObjectID as AccountingObjectID         
			from PPDiscountReturn a join PPDiscountReturnDetail b on a.ID = b.PPDiscountReturnID         
			join GoodsServicePurchase c on c.ID= b.GoodsServicePurchaseID         
			where cast(InvoiceDate As Date) <= @ToDate and Recorded = 1
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=a.InvoiceNo) = 0)
			union all    
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,         
			 a.Reason as Mat_hang, (Amount - DiscountAmount) as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue ,1 flag, b.AccountingObjectID as AccountingObjectID        
			 from PPService a join PPServiceDetail b on a.ID = b.PPServiceID         
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
			where cast(InvoiceDate As Date) <= @ToDate and Recorded = 1 
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
			b.Description as Mat_hang, (Amount - DiscountAmount) as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID																	
			from FAIncrement a join FAIncrementDetail b on a.ID = b.FAIncrementID																	
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate <= @ToDate and Recorded = 1 and a.TypeID not in (510)
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
			b.Description as Mat_hang, (Amount - DiscountAmount) as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag, b.AccountingObjectID as AccountingObjectID																
			from TIIncrement a join TIIncrementDetail b on a.ID = b.TIIncrementID																	
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate <= @ToDate and Recorded = 1 and a.TypeID not in (907)
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag,NULL as AccountingObjectID																		
			 from GOtherVoucher a join GOtherVoucherDetailTax b on a.ID = b.GOtherVoucherID		
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate <= @ToDate and Recorded = 1 
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag,NULL as AccountingObjectID																	
			 from MCPayment a join MCPaymentDetailTax b on a.ID = b.MCPaymentID																
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate <= @ToDate and a.Recorded = 1 
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag,NULL as AccountingObjectID																		
			 from MBTellerPaper a join MBTellerPaperDetailTax b on a.ID = b.MBTellerPaperID		
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate <= @ToDate and a.Recorded = 1 
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag,NULL as AccountingObjectID																	
			 from MBCreditCard a join MBCreditCardDetailTax b on a.ID = b.MBCreditCardID		
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate <= @ToDate and a.Recorded = 1
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)

			/*edit by Hautv */
			UPDATE @tbDataLocal SET ten_NBan = d.AccountingObjectName
			FROM (select * from AccountingObject) d
				where AccountingObjectID = d.ID;
		select aa.VoucherID,
			   aa.GoodsServicePurchaseCode,
			   aa.GoodsServicePurchaseName,
			   aa.So_HD,
			   aa.Ngay_HD,
			   aa.ten_NBan,
			   aa.MST,
			   aa.Mat_hang,
			   sum(aa.GT_chuathue) GT_chuathue,
			   aa.Thue_suat ,
			   sum(aa.Thue_GTGT) Thue_GTGT,
			   aa.TK_Thue,
			   flag
		from (
			 select * from @tbDataLocal
			) aa
		group by VoucherID, GoodsServicePurchaseCode,
			   GoodsServicePurchaseName,
			   So_HD,
			   Ngay_HD,
			   ten_NBan,
			   MST,
			   Mat_hang,
			   TK_Thue,
			   flag,
			   aa.Thue_suat
		order by aa.GoodsServicePurchaseCode, aa.Ngay_HD, aa.So_HD
		End
	END
	ELSE/*tinh thue khau tru*/
	BEGIN
		if @IsSimilarBranch = 0
		Begin    
			/*add by cuongpv*/
			INSERT INTO @tbDataLocal
			/*end add by cuongpv*/
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD ,AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
			 b.Description as Mat_hang, (Amount - DiscountAmount) as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID                  
			 from PPInvoice a join PPInvoiceDetail b on a.ID = b.PPInvoiceID         
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
			where cast(InvoiceDate As Date) between @fromDate and @ToDate and Recorded = 1
			union all         
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
			 b.Description as Mat_hang, Amount as GiatriHH_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,-1 flag, b.AccountingObjectID as AccountingObjectID   
			from PPDiscountReturn a join PPDiscountReturnDetail b on a.ID = b.PPDiscountReturnID         
			join GoodsServicePurchase c on c.ID= b.GoodsServicePurchaseID         
			where cast(InvoiceDate As Date) between @fromDate and @ToDate and Recorded = 1
			union all    
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,         
			 b.Description as Mat_hang, (Amount - DiscountAmount) as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID         
			 from PPService a join PPServiceDetail b on a.ID = b.PPServiceID         
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
			where cast(InvoiceDate As Date) between @fromDate and @ToDate and Recorded = 1 
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
			b.Description as Mat_hang, (Amount - DiscountAmount) as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID																	
			from FAIncrement a join FAIncrementDetail b on a.ID = b.FAIncrementID																	
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate between @fromDate and @ToDate and Recorded = 1
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
			b.Description as Mat_hang, (Amount - DiscountAmount) as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag, b.AccountingObjectID as AccountingObjectID																
			from TIIncrement a join TIIncrementDetail b on a.ID = b.TIIncrementID																	
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate between @fromDate and @ToDate and Recorded = 1
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag,NULL as AccountingObjectID																		
			 from GOtherVoucher a join GOtherVoucherDetailTax b on a.ID = b.GOtherVoucherID		
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate between @fromDate and @ToDate and Recorded = 1 
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag,NULL as AccountingObjectID																	
			 from MCPayment a join MCPaymentDetailTax b on a.ID = b.MCPaymentID																
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate between @fromDate and @ToDate and a.Recorded = 1 
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag,NULL as AccountingObjectID																		
			 from MBTellerPaper a join MBTellerPaperDetailTax b on a.ID = b.MBTellerPaperID		
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate between @fromDate and @ToDate and a.Recorded = 1 
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag,NULL as AccountingObjectID																	
			 from MBCreditCard a join MBCreditCardDetailTax b on a.ID = b.MBCreditCardID		
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate between @fromDate and @ToDate and a.Recorded = 1
		
			/*edit by Hautv */
			UPDATE @tbDataLocal SET ten_NBan = d.AccountingObjectName
			FROM (select * from AccountingObject) d
				where AccountingObjectID = d.ID;

			/*edit by cuongpv order by c.GoodsServicePurchaseCode, InvoiceDate, InvoiceNo -> SELECT * FROM @tbDataLocal order by GoodsServicePurchaseCode, Ngay_HD, So_HD;*/
		
			SELECT * FROM @tbDataLocal order by GoodsServicePurchaseCode, Ngay_HD, So_HD;
		
		End
		else
		Begin
			/*add by cuongpv*/
			INSERT INTO @tbDataLocal
			/*end add by cuongpv*/
			 select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
			 a.Reason as Mat_hang, (Amount - DiscountAmount) as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID         
			 from PPInvoice a join PPInvoiceDetail b on a.ID = b.PPInvoiceID         
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
			where cast(InvoiceDate As Date) between @fromDate and @ToDate and Recorded = 1
			union all         
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
			 a.Reason as Mat_hang, Amount as GiatriHH_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,-1 flag, b.AccountingObjectID as AccountingObjectID         
			from PPDiscountReturn a join PPDiscountReturnDetail b on a.ID = b.PPDiscountReturnID         
			join GoodsServicePurchase c on c.ID= b.GoodsServicePurchaseID         
			where cast(InvoiceDate As Date) between @fromDate and @ToDate and Recorded = 1
			union all    
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,         
			 a.Reason as Mat_hang, (Amount - DiscountAmount) as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue ,1 flag, b.AccountingObjectID as AccountingObjectID        
			 from PPService a join PPServiceDetail b on a.ID = b.PPServiceID         
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
			where cast(InvoiceDate As Date) between @fromDate and @ToDate and Recorded = 1 
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
			b.Description as Mat_hang, (Amount - DiscountAmount) as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID																	
			from FAIncrement a join FAIncrementDetail b on a.ID = b.FAIncrementID																	
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate between @fromDate and @ToDate and Recorded = 1
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
			b.Description as Mat_hang, (Amount - DiscountAmount) as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag, b.AccountingObjectID as AccountingObjectID																
			from TIIncrement a join TIIncrementDetail b on a.ID = b.TIIncrementID																	
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate between @fromDate and @ToDate and Recorded = 1
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag,NULL as AccountingObjectID																		
			 from GOtherVoucher a join GOtherVoucherDetailTax b on a.ID = b.GOtherVoucherID		
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate between @fromDate and @ToDate and Recorded = 1 
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag,NULL as AccountingObjectID																	
			 from MCPayment a join MCPaymentDetailTax b on a.ID = b.MCPaymentID																
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate between @fromDate and @ToDate and a.Recorded = 1 
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag,NULL as AccountingObjectID																		
			 from MBTellerPaper a join MBTellerPaperDetailTax b on a.ID = b.MBTellerPaperID		
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate between @fromDate and @ToDate and a.Recorded = 1 
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag,NULL as AccountingObjectID																	
			 from MBCreditCard a join MBCreditCardDetailTax b on a.ID = b.MBCreditCardID		
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate between @fromDate and @ToDate and a.Recorded = 1

			/*edit by Hautv */
			UPDATE @tbDataLocal SET ten_NBan = d.AccountingObjectName
			FROM (select * from AccountingObject) d
				where AccountingObjectID = d.ID;
		select aa.VoucherID,
			   aa.GoodsServicePurchaseCode,
			   aa.GoodsServicePurchaseName,
			   aa.So_HD,
			   aa.Ngay_HD,
			   aa.ten_NBan,
			   aa.MST,
			   aa.Mat_hang,
			   sum(aa.GT_chuathue) GT_chuathue,
			   aa.Thue_suat ,
			   sum(aa.Thue_GTGT) Thue_GTGT,
			   aa.TK_Thue,
			   flag
		from (
			 select * from @tbDataLocal
			) aa
		group by VoucherID, GoodsServicePurchaseCode,
			   GoodsServicePurchaseName,
			   So_HD,
			   Ngay_HD,
			   ten_NBan,
			   MST,
			   Mat_hang,
			   TK_Thue,
			   flag,
			   aa.Thue_suat
		order by aa.GoodsServicePurchaseCode, aa.Ngay_HD, aa.So_HD
		End
	END
end
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_GetBkeInv_Buy]
      (
      @BranchID UNIQUEIDENTIFIER ,
      @IncludeDependentBranch BIT ,
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @IsSimilarBranch BIT
    )
AS
BEGIN
/*add by cuongpv de xu ly cach thong nhat lam tron*/
	DECLARE @tbDataLocal TABLE(
		GoodsServicePurchaseCode NVARCHAR(25),
		GoodsServicePurchaseName NVARCHAR(512),
		So_HD NVARCHAR(25),
		Ngay_HD DATETIME,
		ten_NBan NVARCHAR(512),
		MST NVARCHAR(50),
		Mat_hang NVARCHAR(512),
		GT_chuathue decimal(25,0),
		Thue_suat decimal(25,10),
		Thue_GTGT decimal(25,0),
		TK_Thue NVARCHAR(25),
		flag int,
		AccountingObjectID UNIQUEIDENTIFIER ,
		RefID UNIQUEIDENTIFIER,
		TypeID int
	)
/*end add by cuongpv*/
	if @IsSimilarBranch = 0
	Begin    
		/*add by cuongpv*/
		INSERT INTO @tbDataLocal
		/*end add by cuongpv*/
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD ,AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
		 b.Description as Mat_hang, (Amount - DiscountAmount) as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID                  
		, a.ID as RefID, a.TypeID as TypeID
		 from PPInvoice a join PPInvoiceDetail b on a.ID = b.PPInvoiceID         
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
		where a.PostedDate between @FromDate and @ToDate and Recorded = 1
		union all         
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
		 b.Description as Mat_hang, Amount as GiatriHH_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,-1 flag, b.AccountingObjectID as AccountingObjectID   
		, a.ID as RefID, a.TypeID as TypeID
		from PPDiscountReturn a join PPDiscountReturnDetail b on a.ID = b.PPDiscountReturnID         
		join GoodsServicePurchase c on c.ID= b.GoodsServicePurchaseID         
		where a.PostedDate between @FromDate and @ToDate and Recorded = 1
		union all    
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,         
		 b.Description as Mat_hang, (Amount - DiscountAmount) as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID         
		, a.ID as RefID, a.TypeID as TypeID
		 from PPService a join PPServiceDetail b on a.ID = b.PPServiceID         
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
		where a.PostedDate between @FromDate and @ToDate and Recorded = 1 
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
		b.Description as Mat_hang, (Amount - DiscountAmount) as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID																	
		, a.ID as RefID, a.TypeID as TypeID
		from FAIncrement a join FAIncrementDetail b on a.ID = b.FAIncrementID																	
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where a.PostedDate between @FromDate and @ToDate and Recorded = 1 and a.TypeID not in (510)
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
		b.Description as Mat_hang, (Amount - DiscountAmount) as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag, b.AccountingObjectID as AccountingObjectID																
		, a.ID as RefID, a.TypeID as TypeID
		from TIIncrement a join TIIncrementDetail b on a.ID = b.TIIncrementID																	
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where a.PostedDate between @FromDate and @ToDate and Recorded = 1 and a.TypeID not in (907)
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
		 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag,NULL as AccountingObjectID																		
		 , a.ID as RefID, a.TypeID as TypeID
		 from GOtherVoucher a join GOtherVoucherDetailTax b on a.ID = b.GOtherVoucherID		
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where a.PostedDate between @FromDate and @ToDate and Recorded = 1 
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
		 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag,NULL as AccountingObjectID																	
		, a.ID as RefID, a.TypeID as TypeID
		 from MCPayment a join MCPaymentDetailTax b on a.ID = b.MCPaymentID																
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where a.PostedDate between @FromDate and @ToDate and a.Recorded = 1 
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
		 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag,NULL as AccountingObjectID																		
		 , a.ID as RefID, a.TypeID as TypeID
		 from MBTellerPaper a join MBTellerPaperDetailTax b on a.ID = b.MBTellerPaperID		
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where a.PostedDate between @FromDate and @ToDate and a.Recorded = 1 
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
		 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag,NULL as AccountingObjectID																	
		, a.ID as RefID, a.TypeID as TypeID
		 from MBCreditCard a join MBCreditCardDetailTax b on a.ID = b.MBCreditCardID		
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where a.PostedDate between @FromDate and @ToDate and a.Recorded = 1;
		
		/*edit by Hautv */
		UPDATE @tbDataLocal SET ten_NBan = d.AccountingObjectName
		FROM (select * from AccountingObject) d
			where AccountingObjectID = d.ID;

		/*edit by cuongpv order by c.GoodsServicePurchaseCode, InvoiceDate, InvoiceNo -> SELECT * FROM @tbDataLocal order by GoodsServicePurchaseCode, Ngay_HD, So_HD;*/
		
		SELECT * FROM @tbDataLocal order by GoodsServicePurchaseCode, Ngay_HD, So_HD;
		
	End
	else
	Begin
		/*add by cuongpv*/
		INSERT INTO @tbDataLocal
		/*end add by cuongpv*/
		 select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
		 a.Reason as Mat_hang, (Amount - DiscountAmount) as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID
		, a.ID as RefID, a.TypeID as TypeID
		 from PPInvoice a join PPInvoiceDetail b on a.ID = b.PPInvoiceID         
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
		where a.PostedDate between @FromDate and @ToDate and Recorded = 1
		union all         
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
		 a.Reason as Mat_hang, Amount as GiatriHH_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,-1 flag, b.AccountingObjectID as AccountingObjectID         
		, a.ID as RefID, a.TypeID as TypeID
		from PPDiscountReturn a join PPDiscountReturnDetail b on a.ID = b.PPDiscountReturnID         
		join GoodsServicePurchase c on c.ID= b.GoodsServicePurchaseID         
		where a.PostedDate between @FromDate and @ToDate and Recorded = 1
		union all    
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,         
		 a.Reason as Mat_hang, (Amount - DiscountAmount) as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue ,1 flag, b.AccountingObjectID as AccountingObjectID        
		 , a.ID as RefID, a.TypeID as TypeID
		 from PPService a join PPServiceDetail b on a.ID = b.PPServiceID         
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
		where a.PostedDate between @FromDate and @ToDate and Recorded = 1 
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
		a.Reason as Mat_hang, (Amount - DiscountAmount) as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID																	
		, a.ID as RefID, a.TypeID as TypeID
		from FAIncrement a join FAIncrementDetail b on a.ID = b.FAIncrementID																	
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where a.PostedDate between @FromDate and @ToDate and Recorded = 1 and a.TypeID not in (510)
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
		a.Reason as Mat_hang, (Amount - DiscountAmount) as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag, b.AccountingObjectID as AccountingObjectID																
		, a.ID as RefID, a.TypeID as TypeID
		from TIIncrement a join TIIncrementDetail b on a.ID = b.TIIncrementID																	
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where a.PostedDate between @FromDate and @ToDate and Recorded = 1 and a.TypeID not in (907)
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
		 a.Reason as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag,NULL as AccountingObjectID																		
		 , a.ID as RefID, a.TypeID as TypeID
		 from GOtherVoucher a join GOtherVoucherDetailTax b on a.ID = b.GOtherVoucherID		
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where a.PostedDate between @FromDate and @ToDate and Recorded = 1 
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
		 a.Reason as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag,NULL as AccountingObjectID																	
		, a.ID as RefID, a.TypeID as TypeID
		 from MCPayment a join MCPaymentDetailTax b on a.ID = b.MCPaymentID																
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where a.PostedDate between @FromDate and @ToDate and a.Recorded = 1 
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
		a.Reason as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag,NULL as AccountingObjectID																		
		 , a.ID as RefID, a.TypeID as TypeID
		 from MBTellerPaper a join MBTellerPaperDetailTax b on a.ID = b.MBTellerPaperID		
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where a.PostedDate between @FromDate and @ToDate and a.Recorded = 1 
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
		 a.Reason as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag,NULL as AccountingObjectID																	
		 , a.ID as RefID, a.TypeID as TypeID
		 from MBCreditCard a join MBCreditCardDetailTax b on a.ID = b.MBCreditCardID		
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where a.PostedDate between @FromDate and @ToDate and a.Recorded = 1

		/*edit by Hautv */
		UPDATE @tbDataLocal SET ten_NBan = d.AccountingObjectName
		FROM (select * from AccountingObject) d
			where AccountingObjectID = d.ID;
	select aa.GoodsServicePurchaseCode,
		   aa.GoodsServicePurchaseName,
		   aa.So_HD,
		   aa.Ngay_HD,
		   aa.ten_NBan,
		   aa.MST,
		   aa.Mat_hang,
		   sum(aa.GT_chuathue) GT_chuathue,
		   aa.Thue_suat ,
		   sum(aa.Thue_GTGT) Thue_GTGT,
		   aa.TK_Thue,
		   flag
	from (
		 select * from @tbDataLocal
		) aa
	group by GoodsServicePurchaseCode,
		   GoodsServicePurchaseName,
		   So_HD,
		   Ngay_HD,
		   ten_NBan,
		   MST,
		   Mat_hang,
		   TK_Thue,
		   flag,
		   aa.Thue_suat
	order by aa.GoodsServicePurchaseCode, aa.Ngay_HD, aa.So_HD
	End
	
end
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER FUNCTION [dbo].[Func_SoDu]
      ( @FromDate DATETIME ,
        @ToDate DATETIME ,
        @MaxAccountGrade INT 
      )
RETURNS @Result TABLE
      (
        AccountID UNIQUEIDENTIFIER ,
        DetailByAccountObject BIT ,
        AccountObjectType INT ,
		AccountObjectID NVARCHAR(50) ,
        AccountNumber NVARCHAR(10) ,
        AccountName NVARCHAR(512) ,
        AccountNameEnglish NVARCHAR(512) ,
        AccountCategoryKind INT ,
        IsParent BIT ,
        ParentID UNIQUEIDENTIFIER ,
        Grade INT ,
        AccountKind INT ,
        SortOrder INT ,
        OpeningDebitAmount DECIMAL(25, 4) ,
        OpeningCreditAmount DECIMAL(25, 4) ,
        DebitAmount DECIMAL(25, 4) ,
        CreditAmount DECIMAL(25, 4) ,
        DebitAmountAccum DECIMAL(25, 4) ,
        CreditAmountAccum DECIMAL(25, 4) ,
        ClosingDebitAmount DECIMAL(25, 4) ,
        ClosingCreditAmount DECIMAL(25, 4)
      )
AS
    BEGIN
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)
		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/
        DECLARE @MainCurrency NVARCHAR(3)
        SET @MainCurrency = 'VND'	
        /*ngày bắt đầu chương trình*/
        DECLARE @StartDate DATETIME
        SELECT TOP 1
                @StartDate = CONVERT(DATETIME, Data, 103)
        FROM    dbo.SystemOption
        WHERE   Code LIKE N'%ApplicationStartDate%'                
           BEGIN
					
                 DECLARE @GeneralLedger TABLE
                         (
                           AccountNumber NVARCHAR(30) ,
                           AccountObjectID UNIQUEIDENTIFIER ,
                           BranchID UNIQUEIDENTIFIER ,
                           CurrencyID NVARCHAR(3) ,
                           OpenningDebitAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,
                           CreditAmountAccum DECIMAL(25, 4) ,
                           OpenningDebitAmountOC DECIMAL(25, 4) ,
                           DebitAmountOC DECIMAL(25, 4) ,
                           CreditAmountOC DECIMAL(25, 4) ,
                           DebitAmountOCAccum DECIMAL(25, 4) ,
                           CreditAmountOCAccum DECIMAL(25, 4)
                         )
                 INSERT INTO @GeneralLedger
                        SELECT  G.*
                        FROM    (
                                  SELECT    Account ,
                                            AccountingObjectID ,
                                            GL.BranchID ,
                                            GL.CurrencyID ,
                                            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount
                                                     ELSE 0
                                                END) AS OpenningDebitAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmount )
                                                     ELSE 0
                                                END) AS DebitAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmount )
                                                     ELSE 0
                                                END) AS CreditAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmount )
                                                     ELSE 0
                                                END) AS DebitAmountAccum ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmount )
                                                     ELSE 0
                                                END) AS CreditAmountAccum ,
                                            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmountOriginal - GL.CreditAmountOriginal
                                                     ELSE 0
                                                END) AS OpenningDebitAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmountOriginal )
                                                     ELSE 0
                                                END) AS DebitAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmountOriginal )
                                                     ELSE 0
                                                END) AS CreditAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmountOriginal )
                                                     ELSE 0
                                                END) AS DebitAmountOCAccum ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmountOriginal )
                                                     ELSE 0
                                                END) AS CreditAmountOCAccum
                                  FROM      @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                  WHERE    GL.PostedDate <= @ToDate
                                  GROUP BY  Account ,
                                            AccountingObjectID ,
                                            GL.BranchID ,
                                            GL.CurrencyID
                                ) G
			
                 DECLARE @Data TABLE
                         (
                           AccountNumber NVARCHAR(50) ,
						   AccountObjectID UNIQUEIDENTIFIER ,
                           CurrencyID NVARCHAR(20) ,
                           SortOrder INT ,
                           OpeningDebitAmount DECIMAL(25, 4) ,
                           OpeningCreditAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,/*Nợ lũy kế*/
                           CreditAmountAccum DECIMAL(25, 4) ,/*có lũy kế*/
                           ClosingDebitAmount DECIMAL(25, 4) ,
                           ClosingCreditAmount DECIMAL(25, 4)
                         )
	
                 INSERT @Data
                        SELECT  F.AccountNumber ,
						        F.AccountObjectID,
                                '' ,
                                0 ,
                                SUM(CASE WHEN F.AccountCategoryKind = 0
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount > 0
                                                 ) THEN F.OpenningDebitAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN F.AccountCategoryKind = 1
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount < 0
                                                 ) THEN -1 * F.OpenningDebitAmount
                                         ELSE 0
                                    END) AS OpenningCreditAmount ,
                                SUM(F.DebitAmount) AS DebitAmount ,
                                SUM(F.CreditAmount) AS CreditAmount ,
                                SUM(F.DebitAmountAccum) AS DebitAmountAccum ,
                                SUM(F.CreditAmountAccum) AS CreditAmountAccum ,
                                SUM(CASE WHEN F.AccountCategoryKind = 0
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount > 0
                                                 ) THEN F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN F.AccountCategoryKind = 1
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount < 0
                                                 ) THEN F.CreditAmount - F.OpenningDebitAmount - F.DebitAmount
                                         ELSE 0
                                    END) AS OpenningCreditAmount
                        FROM    (
                                  SELECT    A.AccountNumber ,
                                            SUM(OpenningDebitAmount) AS OpenningDebitAmount ,
                                            SUM(DebitAmount) AS DebitAmount ,
                                            SUM(CreditAmount) AS CreditAmount ,
                                            SUM(DebitAmountAccum) AS DebitAmountAccum ,
                                            SUM(CreditAmountAccum) AS CreditAmountAccum ,
                                            GL.AccountObjectID AccountObjectID ,
                                            A.AccountGroupKind AccountCategoryKind ,
                                            GL.BranchID BranchID
                                  FROM      @GeneralLedger AS GL
                                  INNER JOIN dbo.Account A ON GL.AccountNumber = A.AccountNumber
                                  WHERE     GL.AccountNumber NOT LIKE '0%'
                                            AND A.IsParentNode = 0
                                  GROUP BY  A.AccountNumber ,
                                            GL.AccountObjectID,
                                            A.AccountGroupKind ,
                                            GL.BranchID
                                  HAVING    SUM(OpenningDebitAmount) <> 0
                                            OR SUM(DebitAmount) <> 0
                                            OR SUM(CreditAmount) <> 0
                                ) AS F
                        GROUP BY F.AccountNumber ,
                                F.AccountCategoryKind,
								F.AccountObjectID
                 OPTION ( RECOMPILE )		         					    
                 INSERT @Result
                        SELECT  A.ID ,
						        null,
								null,
								D.AccountObjectID,
                                A.AccountNumber ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade ,
                                0 AccountKind,
                                D.SortOrder ,
                                SUM(D.OpeningDebitAmount) ,
                                SUM(D.OpeningCreditAmount) ,
                                SUM(D.DebitAmount) ,
                                SUM(D.CreditAmount) ,
                                SUM(D.DebitAmountAccum) ,
                                SUM(D.CreditAmountAccum) ,
                                SUM(D.ClosingDebitAmount) ,
                                SUM(D.ClosingCreditAmount)
                        FROM    @Data D
                        INNER JOIN dbo.Account A ON D.AccountNumber LIKE A.AccountNumber + '%'
                        WHERE   A.Grade <= @MaxAccountGrade
                        GROUP BY A.ID ,
                                A.AccountNumber ,
								D.AccountObjectID,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade ,
                                D.SortOrder
                        HAVING  SUM(D.OpeningDebitAmount) <> 0
                                OR SUM(D.OpeningCreditAmount) <> 0
                                OR SUM(D.DebitAmount) <> 0
                                OR SUM(D.CreditAmount) <> 0
                                OR SUM(D.ClosingDebitAmount) <> 0
                                OR SUM(D.ClosingCreditAmount) <> 0
                 OPTION ( RECOMPILE )
           END
        RETURN
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
Proc_GetBalanceAccountF01 '2019-01-01','2019-12-31',3,0
*/
ALTER PROCEDURE [dbo].[Proc_GetBalanceAccountF01]
      ( @FromDate DATETIME ,
        @ToDate DATETIME ,
        @MaxAccountGrade INT,
		@IsBalanceBothSide BIT
		)
AS
BEGIN
 
SELECT t.AccountID ,
		       t.AccountCategoryKind,
			   t.AccountNumber,
			   t.AccountName,
			   sum(t.OpeningDebitAmount) OpeningDebitAmount,
			   sum(t.OpeningCreditAmount) OpeningCreditAmount,
			   sum(t.DebitAmount) DebitAmount,
			   sum(t.CreditAmount) CreditAmount,
			   sum(t.DebitAmountAccum) DebitAmountAccum,
			   sum(t.CreditAmountAccum) CreditAmountAccum,
			   sum(t.ClosingDebitAmount) ClosingDebitAmount,
			   sum(t.ClosingCreditAmount) ClosingCreditAmount
			   FROM [dbo].[Func_GetBalanceAccountF01] (
   @FromDate
  ,@ToDate
  ,@MaxAccountGrade
  ,@IsBalanceBothSide) t
  group by t.AccountID,
		       t.AccountCategoryKind,
			   t.AccountNumber,
			   t.AccountName
  order by t.AccountNumber
end
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_GetHealthFinanceMoney]
      ( @FromDate DATETIME ,
        @ToDate DATETIME
		)
AS
BEGIN
	DECLARE @tbDataReturn TABLE(
		tySoNo decimal(18,2),
		tyTaiTro decimal(18,2),
		vonLuanChuyen decimal(18,2),
		hsTTNganHan decimal(18,2),
		hsTTNhanh decimal(18,2),
		hsTTTucThoi decimal(18,2),
		hsTTChung decimal(18,2),
		hsQVHangTonKho decimal(18,2),
		hsLNTrenVonKD decimal(18,2),
		hsLNTrenDTThuan decimal(18,2),
		hsLNTrenVonCSH decimal(18,2),
		tienMat money,
		tienGui money,
		doanhThu money,
		chiPhi money,
		loiNhuanTruocThue money,
		phaiThu money,
		phaiTra money,
		hangTonKho money
	)
	
	INSERT INTO @tbDataReturn(tySoNo,tyTaiTro,vonLuanChuyen,hsTTNganHan,hsTTNhanh,hsTTTucThoi,hsTTChung,hsQVHangTonKho,hsLNTrenVonKD,hsLNTrenDTThuan,
	hsLNTrenVonCSH,tienMat,tienGui,doanhThu,chiPhi,loiNhuanTruocThue,phaiThu,phaiTra,hangTonKho)
	Values(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null)
	
	DECLARE @tbDataGetB01DN TABLE(
	  ItemID UNIQUEIDENTIFIER ,
      ItemCode NVARCHAR(25) ,
      ItemName NVARCHAR(512) ,
      ItemNameEnglish NVARCHAR(512) ,
      ItemIndex INT ,
      Description NVARCHAR(512) ,
      FormulaType INT ,
      FormulaFrontEnd NVARCHAR(MAX) ,
      Formula XML ,
      Hidden BIT ,
      IsBold BIT ,
      IsItalic BIT ,
      Category INT ,
      SortOrder INT,
      Amount DECIMAL(25, 4) ,
      PrevAmount DECIMAL(25, 4)
	)
	
	INSERT INTO @tbDataGetB01DN
	SELECT * FROM [dbo].[Func_GetB01_DN] (null,null,@FromDate,@ToDate,0,1,0,@FromDate,@ToDate)
	
	
	DECLARE @tbDataGetBalanceAccountF01 TABLE(
		AccountID UNIQUEIDENTIFIER ,
        AccountCategoryKind INT ,
        AccountNumber NVARCHAR(50) ,
        AccountName NVARCHAR(512) ,
        OpeningDebitAmount DECIMAL(25, 4) ,
        OpeningCreditAmount DECIMAL(25, 4) ,
        DebitAmount DECIMAL(25, 4) ,
        CreditAmount DECIMAL(25, 4) ,
        DebitAmountAccum DECIMAL(25, 4) ,
        CreditAmountAccum DECIMAL(25, 4) ,
        ClosingDebitAmount DECIMAL(25, 4) ,
        ClosingCreditAmount DECIMAL(25, 4)
	)
	
	INSERT INTO @tbDataGetBalanceAccountF01
	SELECT t.AccountID ,
		       t.AccountCategoryKind,
			   t.AccountNumber,
			   t.AccountName,
			   sum(t.OpeningDebitAmount) OpeningDebitAmount,
			   sum(t.OpeningCreditAmount) OpeningCreditAmount,
			   sum(t.DebitAmount) DebitAmount,
			   sum(t.CreditAmount) CreditAmount,
			   sum(t.DebitAmountAccum) DebitAmountAccum,
			   sum(t.CreditAmountAccum) CreditAmountAccum,
			   sum(t.ClosingDebitAmount) ClosingDebitAmount,
			   sum(t.ClosingCreditAmount) ClosingCreditAmount
			   FROM [dbo].[Func_GetBalanceAccountF01] (@FromDate,@ToDate,1,0) t
	GROUP BY t.AccountID,
		       t.AccountCategoryKind,
			   t.AccountNumber,
			   t.AccountName
	ORDER BY t.AccountNumber
	
	
	Declare @ct400 decimal(25, 4)
	Set @ct400=(select Amount from @tbDataGetB01DN where ItemCode='400')
	
	Declare @ct600 decimal(25, 4)
	Set @ct600=(select Amount from @tbDataGetB01DN where ItemCode='600')
	
	if(ISNULL(@ct600,0)=0)
	begin
		UPDATE @tbDataReturn SET tySoNo = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET tySoNo = (ISNULL(@ct400,0)/ISNULL(@ct600,0))*100
	end
	
	Declare @ct500 decimal(25, 4)
	Set @ct500=(select Amount from @tbDataGetB01DN where ItemCode='500')
	
	if(ISNULL(@ct600,0)=0)
	begin
		UPDATE @tbDataReturn SET tyTaiTro = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET tyTaiTro = (ISNULL(@ct500,0)/ISNULL(@ct600,0))*100
	end
	
	Declare @ct100 decimal(25, 4)
	Set @ct100=(select Amount from @tbDataGetB01DN where ItemCode='100')
	
	Declare @ct410 decimal(25, 4)
	Set @ct410=(select Amount from @tbDataGetB01DN where ItemCode='410')
	
	UPDATE @tbDataReturn SET vonLuanChuyen = (ISNULL(@ct100,0)-ISNULL(@ct410,0))
	
	if(ISNULL(@ct410,0)=0)
	begin
		UPDATE @tbDataReturn SET hsTTNganHan = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsTTNganHan = (ISNULL(@ct100,0)/ISNULL(@ct410,0))
	end
	
	Declare @ct140 decimal(25, 4)
	Set @ct140=(select Amount from @tbDataGetB01DN where ItemCode='140')
	
	if(ISNULL(@ct410,0)=0)
	begin
		UPDATE @tbDataReturn SET hsTTNhanh = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsTTNhanh = (ISNULL(@ct100,0)-ISNULL(@ct140,0))/ISNULL(@ct410,0)
	end
	
	Declare @ct110 decimal(25, 4)
	Set @ct110=(select Amount from @tbDataGetB01DN where ItemCode='110')
	
	Declare @ct120 decimal(25, 4)
	Set @ct120=(select Amount from @tbDataGetB01DN where ItemCode='120')
	
	if(ISNULL(@ct410,0)=0)
	begin
		UPDATE @tbDataReturn SET hsTTTucThoi = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsTTTucThoi = (ISNULL(@ct110,0)+ISNULL(@ct120,0))/ISNULL(@ct410,0)
	end
	
	Declare @ct300 decimal(25, 4)
	Set @ct300=(select Amount from @tbDataGetB01DN where ItemCode='300')
	
	if(ISNULL(@ct400,0)=0)
	begin
		UPDATE @tbDataReturn SET hsTTChung = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsTTChung = (ISNULL(@ct300,0)/ISNULL(@ct400,0))
	end
	
	Declare @vonHH decimal(25, 4)
	Set @vonHH = (select SUM(DebitAmount) from GeneralLedger where Account='632' AND AccountCorresponding='911' AND (PostedDate between @FromDate and @ToDate))
	
	Declare @hangHoaTon decimal(25,4)
	set @hangHoaTon = (select SUM(ISNULL(OpeningDebitAmount,0)+ISNULL(ClosingDebitAmount,0)) from @tbDataGetBalanceAccountF01 
						where AccountNumber in ('152','153','154','155','156','157'))/2
	
	if(ISNULL(@hangHoaTon,0)=0)
	begin
		UPDATE @tbDataReturn SET hsQVHangTonKho = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsQVHangTonKho=(ISNULL(@vonHH,0)/ISNULL(@hangHoaTon,0))*100
	end
	
	Declare @LNsauThue decimal(25,4)
	set @LNsauThue = (select SUM(CreditAmount) from GeneralLedger where Account='4212' AND AccountCorresponding='911' AND (PostedDate between @FromDate and @ToDate))
	
	Declare @ct600namTruoc decimal(25, 4)
	Set @ct600namTruoc=(select PrevAmount from @tbDataGetB01DN where ItemCode='600')
	
	Declare @vonKdBq decimal(25,4)
	set @vonKdBq = (ISNULL(@ct600,0)+ISNULL(@ct600namTruoc,0))/2
	
	if(ISNULL(@vonKdBq,0)=0)
	begin
		UPDATE @tbDataReturn SET hsLNTrenVonKD = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsLNTrenVonKD = (ISNULL(@LNsauThue,0)/ISNULL(@vonKdBq,0))
	end
	
	
	Declare @dtThuan decimal(25,4)
	set @dtThuan = (select SUM(CreditAmount) from GeneralLedger where Account like'511%' AND AccountCorresponding='911' AND (PostedDate between @FromDate and @ToDate))
	
	if(ISNULL(@dtThuan,0)=0)
	begin
		UPDATE @tbDataReturn SET hsLNTrenDTThuan = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsLNTrenDTThuan = (ISNULL(@LNsauThue,0)/ISNULL(@dtThuan,0))
	end
	
	Declare @ct500namtruoc decimal(25, 4)
	Set @ct500namtruoc=(select PrevAmount from @tbDataGetB01DN where ItemCode='500')
	
	Declare @voncsh decimal(25,4)
	set @voncsh = (ISNULL(@ct500,0)+ISNULL(@ct500namtruoc,0))/2
	
	if(ISNULL(@voncsh,0)=0)
	begin
		UPDATE @tbDataReturn SET hsLNTrenVonCSH = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsLNTrenVonCSH = (ISNULL(@LNsauThue,0)/ISNULL(@voncsh,0))
	end
	
	Declare @duNoCuoiKy111 money
	set @duNoCuoiKy111 = (select ClosingDebitAmount from @tbDataGetBalanceAccountF01 where AccountNumber='111')
	
	UPDATE @tbDataReturn SET tienMat = ISNULL(@duNoCuoiKy111,0)
	
	Declare @duNoCuoiKy112 money
	set @duNoCuoiKy112 = (select ClosingDebitAmount from @tbDataGetBalanceAccountF01 where AccountNumber='112')
	
	UPDATE @tbDataReturn SET tienGui = ISNULL(@duNoCuoiKy112,0)
	
	Declare @doanhThu money
	set @doanhThu = (select SUM(CreditAmount - DebitAmount) from GeneralLedger 
						where ((Account like '511%') or (Account like '515%') or (Account like '711%'))
								and (AccountCorresponding<>'911') and (PostedDate between @FromDate and @ToDate))
	
	UPDATE @tbDataReturn SET doanhThu = ISNULL(@doanhThu,0)
	
	Declare @chiPhi money
	set @chiPhi = (select SUM(DebitAmount - CreditAmount) from GeneralLedger
						where ((Account like '632%') or (Account like '642%') or (Account like '635%') or (Account like '811%') or (Account like '821%'))
						and (AccountCorresponding<>'911') and (PostedDate between @FromDate and @ToDate))
	
	UPDATE @tbDataReturn SET chiPhi = ISNULL(@chiPhi,0)
	
	Declare @chiPhiko821 money
	set @chiPhiko821 = (select SUM(DebitAmount - CreditAmount) from GeneralLedger
						where ((Account like '632%') or (Account like '642%') or (Account like '635%') or (Account like '811%'))
						and (AccountCorresponding<>'911') and (PostedDate between @FromDate and @ToDate))
	
	UPDATE @tbDataReturn SET loiNhuanTruocThue = (ISNULL(@doanhThu,0) - ISNULL(@chiPhiko821,0))
	
	Declare @phaiThu money
	set @phaiThu = (select SUM(ClosingDebitAmount) from @tbDataGetBalanceAccountF01 where AccountNumber='131')
	
	UPDATE @tbDataReturn SET phaiThu = (ISNULL(@phaiThu,0))
	
	Declare @phaiTra money
	set @phaiTra = (select SUM(ClosingCreditAmount) from @tbDataGetBalanceAccountF01 where AccountNumber='331')
	
	UPDATE @tbDataReturn SET phaiTra = (ISNULL(@phaiTra,0))
	
	Declare @hangTonKho money
	set @hangTonKho = (select SUM(ClosingDebitAmount) from @tbDataGetBalanceAccountF01 
						where AccountNumber in ('152','153','154','155','156','157'))
	
	UPDATE @tbDataReturn SET hangTonKho = (ISNULL(@hangTonKho,0))
	
	SELECT * FROM @tbDataReturn
end
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_GetPPPayVendorBill]
	@AccountingObjectID UNIQUEIDENTIFIER,
	@FromDate DATETIME,
    @ToDate DATETIME
AS
	BEGIN
		DECLARE @Account NVARCHAR(MAX) = (SELECT TOP 1 AccountNumber FROM Account WHERE DetailType = '0')
		DECLARE @tbDataGL TABLE(
				ID uniqueidentifier,
				BranchID uniqueidentifier,
				ReferenceID uniqueidentifier,
				TypeID int,
				Date datetime,
				PostedDate datetime,
				No nvarchar(25),
				InvoiceDate datetime,
				InvoiceNo nvarchar(25),
				Account nvarchar(25),
				AccountCorresponding nvarchar(25),
				BankAccountDetailID uniqueidentifier,
				CurrencyID nvarchar(3),
				ExchangeRate decimal(25, 10),
				DebitAmount decimal(25,0),
				DebitAmountOriginal decimal(25,2),
				CreditAmount decimal(25,0),
				CreditAmountOriginal decimal(25,2),
				Reason nvarchar(512),
				Description nvarchar(512),
				VATDescription nvarchar(512),
				AccountingObjectID uniqueidentifier,
				EmployeeID uniqueidentifier,
				BudgetItemID uniqueidentifier,
				CostSetID uniqueidentifier,
				ContractID uniqueidentifier,
				StatisticsCodeID uniqueidentifier,
				InvoiceSeries nvarchar(25),
				ContactName nvarchar(512),
				DetailID uniqueidentifier,
				RefNo nvarchar(25),
				RefDate datetime,
				DepartmentID uniqueidentifier,
				ExpenseItemID uniqueidentifier,
				OrderPriority int,
				IsIrrationalCost bit
			)

			INSERT INTO @tbDataGL
			SELECT GL.* FROM dbo.GeneralLedger GL 
			LEFT JOIN dbo.AccountingObject AO ON GL.AccountingObjectID = AO.ID
			WHERE GL.AccountingObjectID = @AccountingObjectID AND GL.Account LIKE '331%'
			AND  GL.TypeID IN (210, 230, 240, 430, 500, 701, 601) AND AO.ObjectType IN (1, 2) AND AO.IsActive = 1 AND GL.Date <= @ToDate

			(SELECT g.AccountingObjectID, a.AccountingObjectCode, a.AccountingObjectName, g.No,
			g.ReferenceID, g.PostedDate AS Date, g.InvoiceNo, g.EmployeeID, g.CurrencyID, g.ExchangeRate AS RefVoucherExchangeRate,
			TotalDebit = (SELECT ISNULL(SUM(CreditAmount - DebitAmount),0)
					FROM @tbDataGL WHERE PostedDate > @FromDate AND ReferenceID = g.ReferenceID),
			TotalDebitOriginal = (SELECT ISNULL(SUM(CreditAmountOriginal - DebitAmountOriginal),0)
			FROM @tbDataGL WHERE PostedDate > @FromDate AND ReferenceID = g.ReferenceID),
			DebitAmount = ((SELECT ISNULL(SUM(CreditAmount - DebitAmount),0)
					FROM @tbDataGL WHERE PostedDate > @FromDate AND ReferenceID = g.ReferenceID) - 
					(SELECT ISNULL(SUM(Amount),0)
					FROM MCPaymentDetailVendor mcp WHERE mcp.AccountingObjectID = @AccountingObjectID AND mcp.PPInvoiceID = g.ReferenceID) -
					(SELECT ISNULL(SUM(Amount),0)
					FROM MBTellerPaperDetailVendor mbt WHERE mbt.AccountingObjectID = @AccountingObjectID AND mbt.PPInvoiceID = g.ReferenceID) - 
					(SELECT ISNULL(SUM(Amount),0)
					FROM MBCreditCardDetailVendor mbc WHERE mbc.AccountingObjectID = @AccountingObjectID AND mbc.PPInvoiceID = g.ReferenceID) - 
					(SELECT ISNULL(SUM(Amount),0)
					FROM ExceptVoucher exv WHERE exv.AccountingObjectID = @AccountingObjectID AND exv.DebitAccount = @Account AND exv.GLVoucherID = g.ReferenceID)),
			DebitAmountOriginal = ((SELECT ISNULL(SUM(CreditAmountOriginal - DebitAmountOriginal),0)
					FROM @tbDataGL WHERE PostedDate > @FromDate AND ReferenceID = g.ReferenceID) - 
					(SELECT ISNULL(SUM(AmountOriginal),0)
					FROM MCPaymentDetailVendor mcp WHERE mcp.AccountingObjectID = @AccountingObjectID AND mcp.PPInvoiceID = g.ReferenceID) -
					(SELECT ISNULL(SUM(AmountOriginal),0)
					FROM MBTellerPaperDetailVendor mbt WHERE mbt.AccountingObjectID = @AccountingObjectID AND mbt.PPInvoiceID = g.ReferenceID) - 
					(SELECT ISNULL(SUM(AmountOriginal),0)
					FROM MBCreditCardDetailVendor mbc WHERE mbc.AccountingObjectID = @AccountingObjectID AND mbc.PPInvoiceID = g.ReferenceID) - 
					(SELECT ISNULL(SUM(AmountOriginal),0)
					FROM ExceptVoucher exv WHERE exv.AccountingObjectID = @AccountingObjectID AND exv.DebitAccount = @Account AND exv.GLVoucherID = g.ReferenceID)),
			Account = @Account
		FROM @tbDataGL g
		LEFT JOIN AccountingObject a on g.AccountingObjectID = a.ID
		GROUP BY g.AccountingObjectID, a.AccountingObjectCode, a.AccountingObjectName, g.ReferenceID, g.No, g.InvoiceNo, g.PostedDate, g.TypeID, g.EmployeeID, g.ExchangeRate, g.CurrencyID) ORDER BY g.No
	END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_GetPPPayVendor]
	@FromDate DATETIME,
    @ToDate DATETIME
AS
BEGIN
	DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL 
		LEFT JOIN dbo.AccountingObject AO ON GL.AccountingObjectID = AO.ID
		WHERE GL.AccountingObjectID IS NOT NULL AND GL.Account LIKE '331%'
		AND  GL.TypeID IN (110, 111, 112, 113, 114, 115, 116, 117, 119, 118, 128, 134, 144, 174
		, 210, 230, 240, 430, 500, 701, 601) AND AO.ObjectType IN (1, 2) AND AO.IsActive = 1 AND GL.Date <= @ToDate

		(SELECT g.AccountingObjectID, a.AccountingObjectCode, a.AccountingObjectName,
		SoDuDauNam = ((SELECT ISNULL(SUM(CreditAmount - DebitAmount),0)
                FROM GeneralLedger WHERE AccountingObjectID = g.AccountingObjectID AND TypeID = 701
                AND PostedDate < @FromDate)
				+ (SELECT ISNULL(SUM(CreditAmount),0)
                FROM @tbDataGL WHERE AccountingObjectID = g.AccountingObjectID AND TypeID != 701
                AND PostedDate < @FromDate)),
		SoDaTra = ((SELECT ISNULL(SUM(DebitAmount),0)
                FROM @tbDataGL WHERE AccountingObjectID = g.AccountingObjectID AND TypeID IN
				(110, 111, 112, 113, 114, 115, 116, 117, 119, 118, 128, 134, 144, 174))
				+ (SELECT ISNULL(SUM(Amount),0)
                FROM ExceptVoucher WHERE AccountingObjectID = g.AccountingObjectID)),
		SoPhatSinh = (SELECT ISNULL(SUM(CreditAmount - DebitAmount),0)
				FROM @tbDataGL WHERE AccountingObjectID = g.AccountingObjectID AND TypeID IN
				(210, 230, 240, 430, 500, 701, 601) AND PostedDate >= @FromDate),
		SoConPhaiTra = (((SELECT ISNULL(SUM(CreditAmount - DebitAmount),0)
                FROM GeneralLedger WHERE AccountingObjectID = g.AccountingObjectID AND TypeID = 701
                AND PostedDate < @FromDate)
				+ (SELECT ISNULL(SUM(CreditAmount),0)
                FROM @tbDataGL WHERE AccountingObjectID = g.AccountingObjectID AND TypeID != 701
                AND PostedDate < @FromDate)) + 
				(SELECT ISNULL(SUM(CreditAmount - DebitAmount),0)
				FROM @tbDataGL WHERE AccountingObjectID = g.AccountingObjectID AND TypeID IN
				(210, 230, 240, 430, 500, 701, 601) AND PostedDate >= @FromDate) -
				((SELECT ISNULL(SUM(DebitAmount),0)
                FROM @tbDataGL WHERE AccountingObjectID = g.AccountingObjectID AND TypeID IN
				(110, 111, 112, 113, 114, 115, 116, 117, 119, 118, 128, 134, 144, 174))
				+ (SELECT ISNULL(SUM(Amount),0)
                FROM ExceptVoucher WHERE AccountingObjectID = g.AccountingObjectID)))
		FROM @tbDataGL g
		LEFT JOIN AccountingObject a on g.AccountingObjectID = a.ID
		GROUP BY g.AccountingObjectID, a.AccountingObjectCode, a.AccountingObjectName) ORDER BY a.AccountingObjectCode
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_GetHealthFinanceChart]
      ( @FromDate DATETIME ,
        @ToDate DATETIME
		)
AS
BEGIN
	DECLARE @tbTemp TABLE(
		PostedDate datetime,
		PostedDay int,
		PostedMonth int,
		PostedYear int,
		Account nvarchar(25),
		AccountCorresponding nvarchar(25),
		DebitAmount decimal(25,0),
		CreditAmount decimal(25,0)
	)
	
	INSERT INTO @tbTemp
	SELECT PostedDate, DAY(PostedDate) as Ngay, MONTH(PostedDate) as Thang, YEAR(PostedDate) as Nam, Account, AccountCorresponding, 
	DebitAmount, CreditAmount
	FROM GeneralLedger
	WHERE PostedDate between @FromDate AND @ToDate
	
	Declare @tbDataTempChart TABLE(
		PostedMonth int,
		PostedYear int,
		TurnoverTotal money,
		CostsTotal money
	)
	
	INSERT INTO @tbDataTempChart(PostedMonth,PostedYear,TurnoverTotal)
	SELECT PostedMonth, PostedYear, SUM(CreditAmount-DebitAmount) as TurnoverTotal
	FROM @tbTemp
	WHERE ((Account like '511%') OR (Account like '515%') OR (Account like '711%')) AND AccountCorresponding <> '911'
	GROUP BY PostedMonth, PostedYear
	
	INSERT INTO @tbDataTempChart(PostedMonth,PostedYear,CostsTotal)
	SELECT PostedMonth, PostedYear, SUM(DebitAmount-CreditAmount) as CostsTotal
	FROM @tbTemp
	WHERE ((Account like '632%') OR (Account like '642%') OR (Account like '635%') OR (Account like '811%') OR (Account like '821%')) 
	AND AccountCorresponding <> '911'
	GROUP BY PostedMonth, PostedYear
	
	Declare @tbDataChart TABLE(
		PostedMonthYear nvarchar(15),
		PostedMonth int,
		PostedYear int,
		TurnoverTotal money,
		CostsTotal money
	)
	
	INSERT INTO @tbDataChart(PostedMonth,PostedYear,TurnoverTotal,CostsTotal)
	SELECT PostedMonth,PostedYear,SUM(TurnoverTotal) as TurnoverTotal, SUM(CostsTotal) as CostsTotal
	FROM @tbDataTempChart
	GROUP BY PostedMonth,PostedYear
	
	UPDATE @tbDataChart SET TurnoverTotal=0 WHERE  TurnoverTotal<0
	UPDATE @tbDataChart SET CostsTotal=0 WHERE  CostsTotal<0
	
	UPDATE @tbDataChart SET PostedMonthYear=CONVERT(nvarchar(2),PostedMonth)+'/'+CONVERT(nvarchar(4),PostedYear)
	
	SELECT PostedMonthYear,TurnoverTotal,CostsTotal FROM @tbDataChart
end
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_S02C1DNN]
    @BranchID UNIQUEIDENTIFIER,
    @IncludeDependentBranch BIT,
    @StartDate DATETIME,
    @FromDate DATETIME,
    @ToDate DATETIME,
    @AccountNumber NVARCHAR(MAX),
    @IsSimilarSum BIT
AS
    BEGIN
        SET NOCOUNT ON
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)
		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		CREATE TABLE #Result
            (
             RefID UNIQUEIDENTIFIER,
             RefType INT,
             PostedDate DATETIME,
             RefNo NVARCHAR(25),
             RefDate DATETIME,
             InvDate DATETIME,
             InvNo NVARCHAR(MAX),
             JournalMemo NVARCHAR(512),
             AccountNumber NVARCHAR(25),
			 DetailAccountNumber NVARCHAR(25) ,
             AccountCategoryKind INT,
             AccountName NVARCHAR(512),
             CorrespondingAccountNumber NVARCHAR(25),
             DebitAmount MONEY,
             CreditAmount MONEY,
             OrderType INT,
             IsBold BIT,
             OrderNumber int
            )
        CREATE TABLE #tblAccountNumber
            (
             AccountNumber NVARCHAR(25)    PRIMARY KEY,
             AccountName NVARCHAR(512)  ,
             AccountNumberPercent NVARCHAR(25)  ,
             AccountCategoryKind INT,
			 IsParent BIT
            )
        INSERT  #tblAccountNumber
                SELECT  A1.AccountNumber,
                        A1.AccountName,
                        A1.AccountNumber + '%',
                        A1.AccountGroupKind,
						A1.IsParentNode
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountNumber,',') F
                        INNER JOIN dbo.Account A1 ON A1.AccountNumber = F.Value
        IF @IsSimilarSum = 0 /*khong cong gop but ke toan*/
		Begin
            INSERT  #Result
                    (
                     RefID,
                     RefType,
                     PostedDate,
                     RefNo,
                     RefDate,
                     InvDate,
                     InvNo,
                     JournalMemo,
                     AccountNumber,
					 DetailAccountNumber,
                     AccountName,
                     CorrespondingAccountNumber,
                     AccountCategoryKind,
                     DebitAmount,
                     CreditAmount,
                     ORDERType,
                     IsBold,
                     OrderNumber
                    )
                    SELECT
                            GL.ReferenceID,
                            GL.TypeID,
                            GL.PostedDate,
                            RefNo,
                            GL.RefDate,
                            Gl.InvoiceDate,
                            GL.InvoiceNo,
                            GL.Description AS JournalMemo,
                            AC.AccountNumber,
							GL.Account AS DetailAccountNumber,
                            AC.AccountName,
                            GL.AccountCorresponding,
                            AC.AccountCategoryKind,
                            DebitAmount,
                            CreditAmount,
                            1 AS ORDERType,
                            0,
                            CASE WHEN (AC.AccountNumber LIKE N'11%' OR AC.AccountNumber LIKE N'15%' AND AC.AccountNumber NOT LIKE N'154%')
											AND GL.DebitAmount <> 0
                                             THEN 0
                                             ELSE 1
                                        END AS OrderNumber
                    FROM    @tbDataGL AS GL
                            INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                    WHERE   GL.PostedDate BETWEEN @FromDate AND @ToDate
                            AND (GL.DebitAmount <> 0
                                 OR GL.CreditAmount <> 0
                                 or GL.DebitAmountOriginal <> 0
                                 OR GL.CreditAmountOriginal <> 0
                                )
              End
			  Else
			  Begin /*cong gop but toan thue*/
				INSERT  #Result(RefID, RefType, PostedDate, RefNo, RefDate, InvDate, InvNo, JournalMemo, AccountNumber, DetailAccountNumber, AccountName, CorrespondingAccountNumber,
                     AccountCategoryKind, DebitAmount, CreditAmount, ORDERType, IsBold, OrderNumber)
                SELECT RefID, RefType, PostedDate, RefNo, RefDate, InvDate, InvNo, JournalMemo, AccountNumber, DetailAccountNumber, AccountName, CorrespondingAccountNumber,
                     AccountCategoryKind, DebitAmount, CreditAmount, ORDERType, IsBold, OrderNumber
				FROM
				    (SELECT
                            GL.ReferenceID as RefID,
                            GL.TypeID as RefType,
                            GL.PostedDate as PostedDate,
                            RefNo,
                            GL.RefDate as RefDate,
                            Gl.InvoiceDate as InvDate,
                            GL.InvoiceNo as InvNo,
                            GL.Reason AS JournalMemo,
                            AC.AccountNumber as AccountNumber,
							GL.Account AS DetailAccountNumber,
                            AC.AccountName as AccountName,
                            GL.AccountCorresponding as CorrespondingAccountNumber,
                            AC.AccountCategoryKind as AccountCategoryKind,
                            SUM(GL.DebitAmount) as DebitAmount,
                            0 as CreditAmount,
                            1 AS ORDERType,
                            0 as IsBold,
                            CASE WHEN (AC.AccountNumber LIKE N'11%' OR AC.AccountNumber LIKE N'15%' AND AC.AccountNumber NOT LIKE N'154%')
											AND SUM(GL.DebitAmount) <> 0
                                             THEN 0
                                             ELSE 1
                                        END AS OrderNumber
                    FROM    @tbDataGL AS GL
                            INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                    WHERE   GL.PostedDate BETWEEN @FromDate AND @ToDate
					GROUP BY
                            GL.ReferenceID,
                            GL.TypeID,
                            GL.PostedDate,
                            RefNo,
                            GL.RefDate,
                            Gl.InvoiceDate,
                            GL.InvoiceNo,
                            GL.Reason,
                            AC.AccountNumber,
							GL.Account,
                            AC.AccountName,
                            GL.AccountCorresponding,
                            AC.AccountCategoryKind
					HAVING SUM(GL.DebitAmount) <> 0 OR SUM(GL.CreditAmount) <> 0
				UNION ALL
				SELECT
                            GL.ReferenceID as RefID,
                            GL.TypeID as RefType,
                            GL.PostedDate as PostedDate,
                            RefNo,
                            GL.RefDate as RefDate,
                            Gl.InvoiceDate as InvDate,
                            GL.InvoiceNo as InvNo,
                            GL.Reason AS JournalMemo,
                            AC.AccountNumber as AccountNumber,
							GL.Account AS DetailAccountNumber,
                            AC.AccountName as AccountName,
                            GL.AccountCorresponding as CorrespondingAccountNumber,
                            AC.AccountCategoryKind as AccountCategoryKind,
                            0 as DebitAmount,
                            SUM(GL.CreditAmount) as CreditAmount,
                            1 AS ORDERType,
                            0 as IsBold,
                            CASE WHEN (AC.AccountNumber LIKE N'11%' OR AC.AccountNumber LIKE N'15%' AND AC.AccountNumber NOT LIKE N'154%')
											AND SUM(GL.CreditAmount) <> 0
                                             THEN 0
                                             ELSE 1
                                        END AS OrderNumber
                    FROM    @tbDataGL AS GL
                            INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                    WHERE   GL.PostedDate BETWEEN @FromDate AND @ToDate
					GROUP BY
                            GL.ReferenceID,
                            GL.TypeID,
                            GL.PostedDate,
                            RefNo,
                            GL.RefDate,
                            Gl.InvoiceDate,
                            GL.InvoiceNo,
                            GL.Reason,
                            AC.AccountNumber,
							GL.Account,
                            AC.AccountName,
                            GL.AccountCorresponding,
                            AC.AccountCategoryKind
					HAVING SUM(GL.DebitAmount) <> 0 OR SUM(GL.CreditAmount) <> 0
				) T
				where T.DebitAmount > 0 OR T.CreditAmount>0
			  End
        SELECT  AccountNumber,
                AccountName,
				IsParent,
                SUM(OpenningDebitAmount) AS OpenningDebitAmount,
                SUM(OpenningCreditAmount) AS OpenningCreditAmount,
                SUM(ClosingDebitAmount) AS ClosingDebitAmount,
                SUM(ClosingCreditAmount) AS ClosingCreditAmount,
                SUM(AccumDebitAmount) AS AccumDebitAmount,
                SUM(AccumCreditAmount) AS AccumCreditAmount,
				AccountCategoryKind
        FROM    (
                 SELECT AC.AccountNumber,
                        AC.AccountName,
                        CASE WHEN AC.AccountCategoryKind = 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             WHEN ac.AccountCategoryKind = 2
                                  AND SUM(GL.DebitAmount - GL.CreditAmount) > 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             ELSE 0
                        END AS OpenningDebitAmount,
                        CASE WHEN AC.AccountCategoryKind = 1
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             WHEN AC.AccountCategoryKind = 2
                                  AND SUM(GL.CreditAmount - GL.DebitAmount) > 0
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             ELSE 0
                        END AS OpenningCreditAmount,
                        0 AS ClosingDebitAmount,
                        0 AS ClosingCreditAmount,
                        0 AS AccumDebitAmount,
                        0 AS AccumCreditAmount,
						AC.IsParent,
						AC.AccountCategoryKind
                 FROM   @tbDataGL GL
                        INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                 WHERE  GL.PostedDate < @FromDate
                 GROUP BY GL.BranchID,
                        AC.AccountNumber,
                        AC.AccountName,
                        AC.AccountCategoryKind,
						AC.IsParent
                 UNION ALL
                 SELECT AC.AccountNumber,
                        AC.AccountName,
                        0 AS OpenningDebitAmount,
                        0 AS OpenningCreditAmount,
                        CASE WHEN AC.AccountCategoryKind = 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             WHEN ac.AccountCategoryKind = 2
                                  AND SUM(GL.DebitAmount - GL.CreditAmount) > 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             ELSE 0
                        END AS ClosingDebitAmount,
                        CASE WHEN AC.AccountCategoryKind = 1
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             WHEN AC.AccountCategoryKind = 2
                                  AND SUM(GL.CreditAmount - GL.DebitAmount) > 0
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             ELSE 0
                        END AS ClosingCreditAmount,
                        0 AS AccumDebitAmount,
                        0 AS AccumCreditAmount,
						AC.IsParent,
						AC.AccountCategoryKind
                 FROM   @tbDataGL GL
                        INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                 WHERE  GL.PostedDate <= @ToDate
                 GROUP BY AC.AccountNumber,
                        AC.AccountName,
                        AC.AccountCategoryKind,
						AC.IsParent
                 UNION ALL
                 SELECT AC.AccountNumber,
                        AC.AccountName,
                        0 AS OpenningDebitAmount,
                        0 AS OpenningCreditAmount,
                        0 AS ClosingDebitAmount,
                        0 AS ClosingCreditAmount,
                        SUM(GL.DebitAmount) AS AccumDebitAmount,
                        SUM(GL.CreditAmount) AS AccumCreditAmount,
						AC.IsParent,
						AC.AccountCategoryKind
                 FROM   @tbDataGL GL
                        INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                 WHERE  GL.PostedDate BETWEEN @StartDate AND @ToDate
                 GROUP BY AC.AccountNumber,
                        AC.AccountName,
                        AC.AccountCategoryKind,
						AC.IsParent
                ) AS A
        GROUP BY AccountNumber,
                AccountName,
				IsParent ,
				AccountCategoryKind
        HAVING
				SUM(OpenningDebitAmount) <>0 OR
                SUM(OpenningCreditAmount)  <>0 OR
                SUM(ClosingDebitAmount)  <>0 OR
                SUM(ClosingCreditAmount) <>0 OR
                SUM(AccumDebitAmount) <>0 OR
                SUM(AccumCreditAmount) <>0
        ORDER BY AccountNumber
		SELECT  ROW_NUMBER() OVER (ORDER BY AccountNumber,DetailAccountNumber, OrderType, PostedDate, RefDate,OrderNumber, RefNo) AS RowNum,
				RefID,
				RefType,
				PostedDate,
				RefDate,
				RefNo,
				InvDate,
				InvNo,
				JournalMemo,
				AccountNumber,
				DetailAccountNumber,
				CorrespondingAccountNumber,
				DebitAmount,
				CreditAmount,
				IsBold,
				AccountCategoryKind
		FROM    #Result
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_GL_GetBookDetailPaymentByAccountNumber1]
    @BranchID UNIQUEIDENTIFIER ,
    @IncludeDependentBranch BIT ,
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountNumber NVARCHAR(MAX) ,
	/*@CurrencyId NVARCHAR(10) , comment by cuongpv*/
    @GroupTheSameItem BIT
AS
    BEGIN

		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/

        CREATE  TABLE #Result
            (
              RowNum INT IDENTITY(1, 1) PRIMARY KEY,
              RefID UNIQUEIDENTIFIER ,
              PostedDate DATETIME ,
              RefDate DATETIME ,
              RefNo NVARCHAR(25) ,
              InvDate DATETIME ,
              InvNo NVARCHAR(25) ,
              RefType INT ,
              JournalMemo NVARCHAR(512) ,
              AccountNumber NVARCHAR(25) ,
              CorrespondingAccountNumber NVARCHAR(25) ,
              DebitAmount DECIMAL(22,4) ,
              CreditAmount DECIMAL(22,4) ,
              ClosingDebitAmount DECIMAL(22,4) ,
              ClosingCreditAmount DECIMAL(22,4) ,
              AccountObjectCode NVARCHAR(25) ,
              AccountObjectName NVARCHAR(512) ,
              OrderType INT , 
              IsBold BIT ,
              BranchName NVARCHAR(512),
			  AccountGroupKind NVARCHAR(25) ,
			  OrderPriority int

            )      
       
        
        DECLARE @tblAccountNumber TABLE
            (              
			  AccountNumber  NVARCHAR(25) ,  
              AccountNumberPercent NVARCHAR(25)   , 
			  AccountGroupKind NVARCHAR(25) 
            )
        INSERT  @tblAccountNumber
                SELECT  A1.AccountNumber,
                        A1.AccountNumber + '%',
						A1.AccountGroupKind
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountNumber,',') F
                        INNER JOIN dbo.Account A1 ON A1.AccountNumber = F.Value
        IF @GroupTheSameItem = 1
            BEGIN
                INSERT  #Result					
				SELECT
					RefID ,
                    PostedDate ,
                    RefDate ,
                    RefNo ,
                    InvDate ,
                    InvNo ,
                    RefType ,
                    JournalMemo ,
                    AccountNumber,
                    CorrespondingAccountNumber ,
                    ISNULL(DebitAmount,0) ,
                    ISNULL(CreditAmount,0) ,
                    ISNULL(ClosingDebitAmount,0),
                    ISNULL(ClosingCreditAmount,0),
                    AccountingObjectCode,
                    AccountObjectName,
                    OrderType ,
                    IsBold ,
                    BranchName,
					AccountGroupKind,
					OrderPriority	

                 FROM
					(SELECT  
								NULL as RefID ,
                                NULL as PostedDate ,
                                NULL as RefDate ,
                                NULL as RefNo ,
                                NULL as InvDate ,
                                NULL as InvNo ,
                                NULL as RefType ,
                                N'Số dư đầu kỳ' as JournalMemo ,
                                A.AccountNumber,
                                NULL as CorrespondingAccountNumber ,
								$0 AS DebitAmount ,
								$0 AS CreditAmount ,
								CASE WHEN SUM(GL.DebitAmount - GL.CreditAmount) > 0
									 THEN SUM(GL.DebitAmount - GL.CreditAmount)
									 ELSE 0
								END AS ClosingDebitAmount ,
								CASE WHEN SUM(GL.CreditAmount - GL.DebitAmount) > 0
									 THEN SUM(GL.CreditAmount - GL.DebitAmount)
									 ELSE 0
								END AS ClosingCreditAmount ,
                                null as AccountingObjectCode,
                                null as AccountObjectName,
                                0 AS OrderType ,
                                1 As isBold,
                                null as BranchName
                                ,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 As EntryType
								,A.AccountGroupKind,
								0 as OrderPriority /*edit by cuongpv: GL.OrderPriority -> 0 as OrderPriority*/

                            FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
							INNER JOIN @tblAccountNumber A ON GL.Account = A.AccountNumber
							WHERE   ( GL.PostedDate < @FromDate )  
							/*and GL.CurrencyID = @CurrencyId comment by cuongpv*/
							GROUP BY 
								A.AccountNumber,A.AccountGroupKind /*comment by cuongpv: ,GL.OrderPriority*/
							HAVING  SUM(GL.DebitAmount - GL.CreditAmount) <> 0
						UNION ALL
                        SELECT  GL.ReferenceID ,
                                GL.PostedDate ,
                                GL.RefDate ,
                                GL.RefNo ,
                                gl.InvoiceDate ,
                                gl.InvoiceNo ,
                                GL.TypeID ,
                                GL.Reason,/*edit by cuongpv Description-> Reason*/
                                A.AccountNumber,
                                GL.AccountCorresponding ,
                                SUM(GL.DebitAmount) AS DebitAmount ,
                                null AS CreditAmount ,/*edit by cuongpv SUM(GL.CreditAmount) -> null*/
                                $0 AS ClosingDebitAmount ,
                                $0 AS ClosingCreditAmount ,
                                null as AccountingObjectCode,
                                null as AccountObjectName,
                                1 AS OrderType ,
                                0 As isBold,
                                '' as BranchName
								,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 As EntryType
								,A.AccountGroupKind,
								GL.OrderPriority
                        FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                INNER JOIN @tblAccountNumber A ON GL.Account = A.AccountNumber
                        WHERE   ( GL.PostedDate BETWEEN @FromDate AND @ToDate )    
						/*and GL.CurrencyID = @CurrencyId comment by cuongpv*/
						and GL.AccountCorresponding <> ''      
                        GROUP BY GL.ReferenceID ,
                                GL.PostedDate ,
                                GL.RefDate ,
                                GL.RefNo ,
                                gl.InvoiceDate ,
                                gl.InvoiceNo ,
                                GL.TypeID ,
                                GL.Reason,/*edit by cuongpv GL.Description -> GL.Reason*/
                                A.AccountNumber,
                                GL.AccountCorresponding,
								A.AccountGroupKind,
								GL.OrderPriority
                        HAVING  SUM(GL.DebitAmount) <> 0 OR SUM(GL.CreditAmount) <> 0
						/*add by cuongpv de sua cong gop but toan*/
						UNION ALL
                        SELECT  GL.ReferenceID ,
                                GL.PostedDate ,
                                GL.RefDate ,
                                GL.RefNo ,
                                gl.InvoiceDate ,
                                gl.InvoiceNo ,
                                GL.TypeID ,
                                GL.Reason,/*edit by cuongpv Description-> Reason*/
                                A.AccountNumber,
                                GL.AccountCorresponding ,
                                null AS DebitAmount ,
                                SUM(GL.CreditAmount) AS CreditAmount ,
                                $0 AS ClosingDebitAmount ,
                                $0 AS ClosingCreditAmount ,
                                null as AccountingObjectCode,
                                null as AccountObjectName,
                                1 AS OrderType ,
                                0 As isBold,
                                '' as BranchName
								,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 As EntryType
								,A.AccountGroupKind,
								GL.OrderPriority
                        FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                INNER JOIN @tblAccountNumber A ON GL.Account = A.AccountNumber
                        WHERE   ( GL.PostedDate BETWEEN @FromDate AND @ToDate )    
						/*and GL.CurrencyID = @CurrencyId comment by cuongpv*/
						and GL.AccountCorresponding <> ''      
                        GROUP BY GL.ReferenceID ,
                                GL.PostedDate ,
                                GL.RefDate ,
                                GL.RefNo ,
                                gl.InvoiceDate ,
                                gl.InvoiceNo ,
                                GL.TypeID ,
                                GL.Reason,/*edit by cuongpv GL.Description -> GL.Reason*/
                                A.AccountNumber,
                                GL.AccountCorresponding,
								A.AccountGroupKind,
								GL.OrderPriority
                        HAVING  SUM(GL.DebitAmount) <> 0 OR SUM(GL.CreditAmount) <> 0 
						/*end add by cuongpv*/                       
                      )T
                      ORDER BY 
						AccountNumber,
                        OrderType ,
                        postedDate ,
						OrderPriority,
                        RefDate ,
                        RefNo      
                        ,SortOrder,DetailPostOrder,EntryType            
            END
            
        ELSE
            BEGIN             
                
                INSERT  #Result					
				SELECT
					RefID ,
                    PostedDate ,
                    RefDate ,
                    RefNo ,
                    InvDate ,
                    InvNo ,
                    RefType ,
                    JournalMemo ,
                    AccountNumber,
                    CorrespondingAccountNumber ,
                    DebitAmount ,
                    CreditAmount ,
                    ClosingDebitAmount ,
                    ClosingCreditAmount ,
                    AccountingObjectCode,
                    AccountObjectName,
                    OrderType ,
                    IsBold ,
                    BranchName,
					AccountGroupKind,
					OrderPriority
                 FROM
					(SELECT  
								null as RefID ,
                                NULL as PostedDate ,
                                NULL as RefDate ,
                                NULL as RefNo ,
                                NULL as InvDate ,
                                NULL as InvNo ,
                                NULL as RefType ,
                                N'Số dư đầu kỳ' as JournalMemo ,
                                A.AccountNumber,
                                null as CorrespondingAccountNumber ,
                                $0 AS DebitAmount ,
								$0 AS CreditAmount ,
								CASE WHEN SUM(GL.DebitAmount - GL.CreditAmount) > 0
									 THEN SUM(GL.DebitAmount - GL.CreditAmount)
									 ELSE 0
								END AS ClosingDebitAmount ,
								CASE WHEN SUM(GL.CreditAmount - GL.DebitAmount) > 0
									 THEN SUM(GL.CreditAmount - GL.DebitAmount)
									 ELSE 0
								END AS ClosingCreditAmount ,
								null as AccountingObjectCode,
                                null as AccountObjectName,
                                0 AS OrderType ,
                                1 As isBold,
                                null as BranchName
                                ,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 as EntryType
								,A.AccountGroupKind,
								0 as OrderPriority /*edit by cuongpv: GL.OrderPriority -> OrderPriority*/
                            FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
							INNER JOIN @tblAccountNumber A ON GL.Account = A.AccountNumber
							WHERE   ( GL.PostedDate < @FromDate )   
							/*and GL.CurrencyID = @CurrencyId comment by cuongpv*/
							GROUP BY 
								A.AccountNumber,A.AccountGroupKind/*comment by cuongpv: ,GL.OrderPriority*/
							HAVING  SUM(GL.DebitAmount - GL.CreditAmount) <> 0
						UNION ALL
                        SELECT  GL.ReferenceID ,
                                GL.PostedDate ,
                                GL.RefDate ,
                                GL.RefNo ,
                                gl.InvoiceDate ,
                                gl.InvoiceNo ,
                                GL.TypeID ,
                                CASE WHEN GL.DESCRIPTION IS NULL
                                          OR LTRIM(GL.Description) = ''
                                     THEN GL.Reason
                                     ELSE GL.Description
                                END AS JournalMemo ,
                                A.AccountNumber,
                                GL.AccountCorresponding ,
                                GL.DebitAmount ,
                                GL.CreditAmount ,
                                $0 AS ClosingDebitAmount ,
                                $0 AS ClosingCreditAmount ,
								null as AccountingObjectCode,
                                null as AccountObjectName,
                                1 AS OrderType ,
                                0 ,
                                null BranchName
								,null
								,null
								,null
								,A.AccountGroupKind,
								GL.OrderPriority
                        FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                INNER JOIN @tblAccountNumber A ON GL.Account = A.AccountNumber
                        WHERE   (GL.PostedDate BETWEEN @FromDate AND @ToDate )                              
                                AND ( GL.DebitAmount <> 0
                                      OR GL.CreditAmount <> 0
                                    )   	
									/*and GL.CurrencyID = @CurrencyId comment by cuongpv*/
									and GL.AccountCorresponding <> ''                     
                      )T
                      ORDER BY
						AccountNumber,
                        OrderType ,
                        postedDate ,
						OrderPriority,
                        RefDate ,
                        RefNo,SortOrder,DetailPostOrder,EntryType                                                          
               
            END         

			 

		DECLARE @AccountNumber1 NVARCHAR(25)

		DECLARE C1 CURSOR FOR  
		SELECT  F.Value
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountNumber,',') F
	    order by F.Value
		OPEN c1;  
		FETCH NEXT FROM c1 INTO @AccountNumber1  ;  
		WHILE @@FETCH_STATUS = 0  
		   BEGIN  
		      PRINT @AccountNumber1
			  DECLARE @AccountObjectCode NVARCHAR(25)
			 ,@ClosingDebitAmount DECIMAL(22,4)
			 
		      SELECT @ClosingDebitAmount = 0
			  UPDATE  #Result
			  SET     @ClosingDebitAmount = @ClosingDebitAmount + ISNULL(ClosingDebitAmount,0) - 
		                              ISNULL(ClosingCreditAmount,0) + ISNULL(DebitAmount,0) - ISNULL(CreditAmount,0),
		
                ClosingDebitAmount = CASE WHEN @ClosingDebitAmount > 0
                                          THEN @ClosingDebitAmount
                                          ELSE 0
                                     END ,
                ClosingCreditAmount = CASE WHEN @ClosingDebitAmount < 0
                                           THEN - @ClosingDebitAmount
                                           ELSE 0
                                      END 
               where AccountNumber = @AccountNumber1
			   FETCH NEXT FROM c1 INTO @AccountNumber1  ; 
			   
		   END;  
		CLOSE c1;  
		DEALLOCATE c1;  
		
        DECLARE @DebitAmount DECIMAL(22,4) 
		DECLARE @CreditAmount DECIMAL(22,4)  
		DECLARE @ClosingDeditAmount_SDDK DECIMAL(22,4)  
		DECLARE @ClosingCreditAmount_SDDK DECIMAL(22,4)  
	    select @DebitAmount = sum(DebitAmount),
		       @CreditAmount = sum(CreditAmount)
		from #Result Res
		INNER JOIN @tblAccountNumber A ON Res.AccountNumber LIKE A.AccountNumberPercent
		where OrderType = 1
		select @ClosingDeditAmount_SDDK = sum(ClosingDebitAmount),
		       @ClosingCreditAmount_SDDK = sum(ClosingCreditAmount)
		from #Result where OrderType = 0
		 INSERT INTO #Result 
	SELECT null,null,null,null,null,null,null,N'Cộng',AccountNumber,null,Sum(DebitAmount) as DebitAmount,Sum(CreditAmount) as CreditAmount,null,null,null,null,2,null,null,null,null
	FROM #Result
	GROUP BY AccountNumber
	 INSERT INTO #Result 
	SELECT null,null,null,null,null,null,null,N'Số dư cuối kỳ',AccountNumber,null,null,null,null,null,null,null,3,null,null,null,null
	FROM #Result
	GROUP BY AccountNumber
        SELECT  RowNum ,
                RefID ,
                AccountObjectCode ,
                AccountObjectName ,
                AccountNumber,
                PostedDate ,
                RefDate ,
                RefNo ,
                InvDate ,
                InvNo ,
                RefType ,
                JournalMemo ,
                CorrespondingAccountNumber ,
                DebitAmount ,
                CreditAmount ,
                ClosingDebitAmount ,
                ClosingCreditAmount ,               
                OrderType AS InvoiceOrder ,
                IsBold ,
                BranchName,
				AccountGroupKind
        FROM    #Result
		WHERE (DebitAmount > 0 OR CreditAmount > 0 AND OrderType = 1) OR OrderType = 0 or OrderType = 2 or OrderType = 3/*add by cuongpv*/
        ORDER BY AccountNumber,InvoiceOrder,PostedDate
		
		
		select 
		null as RefID ,
                                NULL as PostedDate ,
                                NULL as RefDate ,
                                NULL as RefNo ,
                                NULL as InvDate ,
                                NULL as InvNo ,
                                NULL as RefType ,
                                N'Số dư cuối kỳ' as JournalMemo ,
                                AccountNumber,
                                null as CorrespondingAccountNumber ,
                                $0 DebitAmount ,
								$0 CreditAmount ,
								case 
								when A1.AccountGroupKind=0 or (A1.AccountGroupKind=2 and (A1.AccountNumber like '1%' or A1.AccountNumber like '2%' or A1.AccountNumber like '6%' 
								or A1.AccountNumber like '8%')) then
								sum(ClosingDebitAmount) + sum(sumDebitAmount) - sum(sumCreditAmount) else $0 
								end as ClosingDebitAmount,
								case 
								when A1.AccountGroupKind=1 or (A1.AccountGroupKind=2 and (A1.AccountNumber like '3%' or A1.AccountNumber like '4%' or A1.AccountNumber like '5%' 
								or A1.AccountNumber like '7%')) then
								sum(ClosingCreditAmount) + sum(sumCreditAmount) - sum(sumDebitAmount) else $0 
								end AS ClosingCreditAmount ,
								null as AccountingObjectCode,
                                null as AccountObjectName,
                                2 AS OrderType ,
                                2 As isBold,
                                null as BranchName
                                ,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 as EntryType,
								A1.AccountGroupKind
        from (SELECT  
								null as RefID ,
                                NULL as PostedDate ,
                                NULL as RefDate ,
                                NULL as RefNo ,
                                NULL as InvDate ,
                                NULL as InvNo ,
                                NULL as RefType ,
                                N'Số dư cuối kỳ' as JournalMemo ,
                                A.AccountNumber,
                                null as CorrespondingAccountNumber ,
                                $0 AS DebitAmount ,
								$0 AS CreditAmount ,
								CASE WHEN GL.OrderType = 0 and gl.IsBold = 1 THEN sum(ClosingDebitAmount)
									 ELSE 0 
								END
								AS ClosingDebitAmount,
								sum(DebitAmount) sumDebitAmount,
								sum(CreditAmount) sumCreditAmount,
								0 AS ClosingCreditAmount ,
								null as AccountingObjectCode,
                                null as AccountObjectName,
                                2 AS OrderType ,
                                2 As isBold,
                                null as BranchName
                                ,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 as EntryType,
								A.AccountGroupKind
                            FROM    #Result AS GL
							INNER JOIN @tblAccountNumber A ON GL.AccountNumber = A.AccountNumber
							GROUP BY OrderType,IsBold,
								A.AccountNumber,A.AccountGroupKind) as A1
        group by AccountNumber,AccountGroupKind
	  
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_SA_SoTheoDoiCongNoPhaiThuTheoHoaDon]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @AccountObjectID AS NVARCHAR(MAX)
AS
BEGIN          
	
	/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate between @FromDate and @ToDate
		/*end add by cuongpv*/

	DECLARE @tbAccountObjectID TABLE(
		AccountingObjectID UNIQUEIDENTIFIER
	)
	
	INSERT INTO @tbAccountObjectID
	SELECT tblAccOjectSelect.Value as AccountingObjectID
		FROM dbo.Func_ConvertStringIntoTable(@AccountObjectID,',') tblAccOjectSelect
		WHERE tblAccOjectSelect.Value in (SELECT ID FROM AccountingObject)
		
	
    DECLARE @tbluutru TABLE (
		SAInvoiceID UNIQUEIDENTIFIER,
		RefType int,
		RefID UNIQUEIDENTIFIER,
		DetailID UNIQUEIDENTIFIER,
		InvoiceDate Date,
		InvoiceNo NVARCHAR(25),
		AccountingObjectID UNIQUEIDENTIFIER,
		TienHangVaCK money,
		TienThue money,
		TraLai money,
		ChietKhauTT_GiamTruKhac money
    )
    
    INSERT INTO @tbluutru(SAInvoiceID,RefType,RefID,DetailID,InvoiceDate,InvoiceNo,AccountingObjectID)
		SELECT DISTINCT SA.ID,GL.TypeID as RefType, GL.ReferenceID as RefID,GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID
		FROM @tbDataGL GL LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=GL.DetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
		WHERE (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
			AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID))
			AND (GL.DetailID in (select ID from SAInvoiceDetail)) AND SA.BillRefID is null
		UNION ALL
		SELECT DISTINCT SA.ID, GL.TypeID as RefType, GL.ReferenceID as RefID,GL.DetailID, SAB.InvoiceDate, SAB.InvoiceNo, GL.AccountingObjectID
		FROM @tbDataGL GL LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=GL.DetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
		LEFT JOIN SABill SAB ON SAB.ID=SA.BillRefID
		WHERE (SAB.InvoiceDate is not null) AND (SAB.InvoiceNo is not null) AND (SAB.InvoiceNo <> '')
		AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID))
		AND (GL.DetailID in (select ID from SAInvoiceDetail)) AND SA.BillRefID is not null

    
    DECLARE @tbTienHangVaCK TABLE(
		DetailID UNIQUEIDENTIFIER,
		DebitAmount money,
		CreditAmount money
		)
    
    INSERT INTO @tbTienHangVaCK
    SELECT GL.DetailID, GL.DebitAmount, GL.CreditAmount
    FROM @tbDataGL GL
    WHERE (GL.Account like '131%') AND (AccountCorresponding like '511%'  OR AccountCorresponding like '711%') AND (GL.DetailID in (select DetailID from @tbluutru))
		AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID))
    
    UPDATE @tbluutru SET TienHangVaCK = a.TienHangVaCK
    FROM (select DetailID as IvID, SUM(DebitAmount - CreditAmount) as TienHangVaCK
		from @tbTienHangVaCK
		group by DetailID) a
	WHERE DetailID = a.IvID


	DECLARE @tbTienThue TABLE (
		DetailID UNIQUEIDENTIFIER,
		DebitAmount money
		)
	
	INSERT INTO @tbTienThue
	SELECT GL.DetailID, GL.DebitAmount
	FROM @tbDataGL GL
	WHERE (GL.Account like '131%') AND (AccountCorresponding like '3331%') AND (GL.DetailID in (select DetailID from @tbluutru))
		  AND (GL.PostedDate between @FromDate and @ToDate) AND GL.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID)
	
	UPDATE @tbluutru SET TienThue = b.TienThue
	FROM (select DetailID as IvID, SUM(DebitAmount) as TienThue
		  from @tbTienThue
		  group by DetailID) b
	WHERE DetailID = b.IvID
	
	DECLARE @tbDataTraLai TABLE(
		SAInvoiceDetailID UNIQUEIDENTIFIER,
		TraLai money
	)
	INSERT INTO @tbDataTraLai(SAInvoiceDetailID,TraLai)
		SELECT SAR.SAInvoiceDetailID, SUM(GL.CreditAmount - GL.DebitAmount) as TraLai
		FROM @tbDataGL GL LEFT JOIN SAReturnDetail SAR ON GL.DetailID = SAR.ID
		WHERE (GL.Account like '131%') AND (GL.TypeID=330) AND (SAR.SAInvoiceDetailID in (select DetailID from @tbluutru))
		AND (GL.PostedDate between @FromDate and @ToDate)
		AND (GL.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID))
		GROUP BY SAR.SAInvoiceDetailID
	
	UPDATE @tbluutru SET TraLai = TL.TraLai
	FROM (select SAInvoiceDetailID, TraLai from @tbDataTraLai) TL
	WHERE DetailID = TL.SAInvoiceDetailID
	

	DECLARE @tbChietKhauTT_GiamTruKhac TABLE(
		DetailID UNIQUEIDENTIFIER,
		CreditAmount money
	)
	
	INSERT INTO @tbChietKhauTT_GiamTruKhac
	SELECT GL.DetailID, GL.CreditAmount
	FROM @tbDataGL GL
	WHERE (GL.Account like '131%') AND (GL.AccountCorresponding like '635%')
		  AND (GL.TypeID not in (330,340)) AND (GL.DetailID in (select DetailID from @tbluutru))
		  AND (GL.PostedDate between @FromDate and @ToDate) AND GL.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID)
	
	UPDATE @tbluutru SET ChietKhauTT_GiamTruKhac = c.ChietKhauTT_GiamTruKhac
	FROM (select DetailID as IvID, /*InvoiceDate as IvDate, InvoiceNo as IvNo, AccountingObjectID as AOID,*/ SUM(CreditAmount) as ChietKhauTT_GiamTruKhac
		  from @tbChietKhauTT_GiamTruKhac 
		  group by DetailID) c
	WHERE DetailID = c.IvID
	
	
	DECLARE @tbDataReturn TABLE (
		SAInvoiceID UNIQUEIDENTIFIER,
		RefType int,
		RefID UNIQUEIDENTIFIER,
		InvoiceDate Date,
		InvoiceNo NVARCHAR(25),
		AccountingObjectID UNIQUEIDENTIFIER,
		AccountingObjectName NVARCHAR(512),
		TienHangVaCK money,
		TienThue money,
		GiaTriHoaDon money,
		TraLai money,
		GiamGia money,
		ChietKhauTT_GiamTruKhac money,
		SoDaThu money,
		SoConPhaiThu money
    )
    
    INSERT INTO @tbDataReturn(SAInvoiceID,RefType,RefID,InvoiceDate,InvoiceNo,AccountingObjectID,TienHangVaCK,TienThue,TraLai,ChietKhauTT_GiamTruKhac)
    SELECT SAInvoiceID,RefType,RefID,InvoiceDate,InvoiceNo,AccountingObjectID,SUM(TienHangVaCK) as TienHangVaCK, SUM(TienThue) as TienThue,
    SUM(TraLai) as TraLai, SUM(ChietKhauTT_GiamTruKhac) as ChietKhauTT_GiamTruKhac
    FROM @tbluutru
    GROUP BY SAInvoiceID,RefType,RefID,InvoiceDate,InvoiceNo,AccountingObjectID
    
	INSERT INTO @tbDataReturn(SAInvoiceID,RefType,RefID,InvoiceDate,InvoiceNo,AccountingObjectID,GiamGia)
		SELECT SA.ID, GL.TypeID as RefType, GL.ReferenceID as RefID,GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, SUM(GL.CreditAmount - GL.DebitAmount) as GiamGia
		FROM @tbDataGL GL LEFT JOIN SAReturnDetail SAR ON SAR.ID=GL.DetailID LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=SAR.SAInvoiceDetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
		WHERE (GL.Account like '131%') AND (GL.TypeID=340) AND (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
		AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAReturnDetail)) 
		AND (GL.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID)) AND SA.BillRefID is null
		GROUP BY SA.ID,GL.TypeID, GL.ReferenceID,GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID
		UNION ALL
		SELECT SA.ID,GL.TypeID as RefType, GL.ReferenceID as RefID, SAB.InvoiceDate, SAB.InvoiceNo, GL.AccountingObjectID, SUM(GL.CreditAmount - GL.DebitAmount) as GiamGia
		FROM @tbDataGL GL LEFT JOIN SAReturnDetail SAR ON SAR.ID=GL.DetailID LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=SAR.SAInvoiceDetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
		LEFT JOIN SABill SAB ON SAB.ID=SA.BillRefID
		WHERE (GL.Account like '131%') AND (GL.TypeID=340) AND (SAB.InvoiceDate is not null) AND (SAB.InvoiceNo is not null) AND (SAB.InvoiceNo <> '')
		AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAInvoiceDetail))
		AND (GL.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID)) AND SA.BillRefID is not null
		GROUP BY SA.ID, GL.TypeID, GL.ReferenceID ,SAB.InvoiceDate, SAB.InvoiceNo, GL.AccountingObjectID
		
	DECLARE @tbDataSoDaThu TABLE (
		SAInvoiceID UNIQUEIDENTIFIER,
		InvoiceDate Date,
		InvoiceNo NVARCHAR(25),
		AccountingObjectID UNIQUEIDENTIFIER,
		SoDaThu money
    )

	INSERT INTO @tbDataSoDaThu
		/*lay da thu cua cac hoa don co BillRefID is null trong SAInvoice*/
		select SA.ID, SA.InvoiceDate, SA.InvoiceNo, SA.AccountingObjectID, SUM(MCC.Amount) as SoDaThu
		from @tbDataGL GL left join MCReceipt MC on  GL.ReferenceID = MC.ID
		left join MCReceiptDetailCustomer MCC on MCC.MCReceiptID = MC.ID and (MCC.Amount=GL.CreditAmount or MCC.Amount=GL.DebitAmount)
		left join SAInvoice SA on SA.ID = MCC.SaleInvoiceID
		where (GL.Account like '131%') AND ((GL.AccountCorresponding like '111%') OR (GL.AccountCorresponding like '112%')) 
		AND (SA.InvoiceDate is not null) AND (SA.InvoiceNo is not null) AND (SA.InvoiceNo <> '') 
		AND (GL.PostedDate between @FromDate and @ToDate) AND SA.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID) AND SA.BillRefID is null 
		group by SA.ID, SA.InvoiceDate, SA.InvoiceNo, SA.AccountingObjectID
		  
		UNION ALL
		  
		select SA.ID, SA.InvoiceDate, SA.InvoiceNo, SA.AccountingObjectID, SUM(MBC.Amount) as SoDaThu
		from( @tbDataGL GL left JOIN MBDeposit MB ON GL.ReferenceID = MB.ID)
		left JOIN MBDepositDetailCustomer MBC ON MBC.MBDepositID = MB.ID
		left JOIN SAInvoice SA ON SA.ID = MBC.SaleInvoiceID
		where (GL.Account like '131%') AND ((GL.AccountCorresponding like '111%') OR (GL.AccountCorresponding like '112%')) 
		AND (SA.InvoiceDate is not null) AND (SA.InvoiceNo is not null) AND (SA.InvoiceNo <> '') 
		AND (GL.PostedDate between @FromDate and @ToDate) AND SA.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID) AND SA.BillRefID is null
		group by SA.ID, SA.InvoiceDate, SA.InvoiceNo, SA.AccountingObjectID

		UNION ALL
		/*lay da thu cua cac hoa don co BillRefID is not null trong SAInvoice*/
		select SA.ID, SAB.InvoiceDate, SAB.InvoiceNo, SA.AccountingObjectID, SUM(MCC.Amount) as SoDaThu
		from (@tbDataGL GL left JOIN MCReceipt MC ON GL.ReferenceID = MC.ID)
		left JOIN MCReceiptDetailCustomer MCC ON MCC.MCReceiptID = MC.ID
		left JOIN SAInvoice SA ON SA.ID = MCC.SaleInvoiceID
		left JOIN SABill SAB ON SAB.ID=SA.BillRefID
		where (GL.Account like '131%') AND ((GL.AccountCorresponding like '111%') OR (GL.AccountCorresponding like '112%')) 
		AND (SAB.InvoiceDate is not null) AND (SAB.InvoiceNo is not null) AND (SAB.InvoiceNo <> '') 
		AND (GL.PostedDate between @FromDate and @ToDate) AND SA.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID) AND SA.BillRefID is not null
		group by SA.ID, SAB.InvoiceDate, SAB.InvoiceNo, SA.AccountingObjectID
		  
		UNION ALL
		  
		select SA.ID, SAB.InvoiceDate, SAB.InvoiceNo, SA.AccountingObjectID, SUM(MBC.Amount) as SoDaThu
		from (@tbDataGL GL left JOIN MBDeposit MB ON GL.ReferenceID = MB.ID)
		left JOIN MBDepositDetailCustomer MBC ON MBC.MBDepositID = MB.ID
		left JOIN SAInvoice SA ON SA.ID = MBC.SaleInvoiceID
		left JOIN SABill SAB ON SAB.ID=SA.BillRefID
		where (GL.Account like '131%') AND ((GL.AccountCorresponding like '111%') OR (GL.AccountCorresponding like '112%')) 
		AND (SAB.InvoiceDate is not null) AND (SAB.InvoiceNo is not null) AND (SAB.InvoiceNo <> '') 
		AND (GL.PostedDate between @FromDate and @ToDate) AND SA.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID) AND SA.BillRefID is not null
		group by SA.ID, SAB.InvoiceDate, SAB.InvoiceNo, SA.AccountingObjectID

	UPDATE @tbDataReturn SET SoDaThu = d.SoDaThu
	FROM (
		Select SAInvoiceID as SAId, InvoiceDate as IvDate, InvoiceNo as IvNo, AccountingObjectID as AOID, SoDaThu
		From @tbDataSoDaThu
		  ) d
	WHERE SAInvoiceID = SAId AND InvoiceDate = d.IvDate AND InvoiceNo = d.IvNo AND AccountingObjectID = d.AOID
    

    UPDATE @tbDataReturn SET AccountingObjectName = AJ.AccountingObjectName
	FROM ( select ID,AccountingObjectName from AccountingObject) AJ
	where AccountingObjectID = AJ.ID
    
    UPDATE @tbDataReturn SET GiaTriHoaDon = (ISNULL(TienHangVaCK,0) + ISNULL(TienThue,0))
    
    UPDATE @tbDataReturn SET SoConPhaiThu = (ISNULL(GiaTriHoaDon,0) - (ISNULL(TraLai,0)+ISNULL(GiamGia,0)+ISNULL(ChietKhauTT_GiamTruKhac,0)+ISNULL(SoDaThu,0)))
    
    SELECT SAInvoiceID, RefType, RefID,InvoiceDate,InvoiceNo,AccountingObjectName,GiaTriHoaDon,TraLai,GiamGia,ChietKhauTT_GiamTruKhac,SoDaThu,SoConPhaiThu FROM @tbDataReturn
    WHERE ((ISNULL(GiaTriHoaDon,0)>0) OR (ISNULL(TraLai,0)>0) OR (ISNULL(GiamGia,0)>0) OR (ISNULL(ChietKhauTT_GiamTruKhac,0)>0)
			OR (ISNULL(SoDaThu,0)>0) OR (ISNULL(SoConPhaiThu,0)>0)) AND RefType in (220, 320, 321, 322, 323, 324, 325, 326, 340)
	ORDER BY InvoiceDate,InvoiceNo
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_PO_SummaryPayable]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountNumber NVARCHAR(20) ,
    @AccountObjectID AS NVARCHAR(MAX) ,
    @CurrencyID NVARCHAR(3)
AS
    BEGIN
	    
		/*Add by cuongpv de sua cach lam tron*/
	DECLARE @tbDataGL TABLE(
		ID uniqueidentifier,
		BranchID uniqueidentifier,
		ReferenceID uniqueidentifier,
		TypeID int,
		Date datetime,
		PostedDate datetime,
		No nvarchar(25),
		InvoiceDate datetime,
		InvoiceNo nvarchar(25),
		Account nvarchar(25),
		AccountCorresponding nvarchar(25),
		BankAccountDetailID uniqueidentifier,
		CurrencyID nvarchar(3),
		ExchangeRate decimal(25, 10),
		DebitAmount decimal(25,0),
		DebitAmountOriginal decimal(25,2),
		CreditAmount decimal(25,0),
		CreditAmountOriginal decimal(25,2),
		Reason nvarchar(512),
		Description nvarchar(512),
		VATDescription nvarchar(512),
		AccountingObjectID uniqueidentifier,
		EmployeeID uniqueidentifier,
		BudgetItemID uniqueidentifier,
		CostSetID uniqueidentifier,
		ContractID uniqueidentifier,
		StatisticsCodeID uniqueidentifier,
		InvoiceSeries nvarchar(25),
		ContactName nvarchar(512),
		DetailID uniqueidentifier,
		RefNo nvarchar(25),
		RefDate datetime,
		DepartmentID uniqueidentifier,
		ExpenseItemID uniqueidentifier,
		OrderPriority int,
		IsIrrationalCost bit
	)

	INSERT INTO @tbDataGL
	SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
	/*end add by cuongpv*/
		
		DECLARE @tblListAccountObjectID TABLE 
            (
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(25) ,
              AccountObjectName NVARCHAR(512) ,
              AccountObjectAddress NVARCHAR(512) ,
              AccountObjectTaxCode NVARCHAR(200) ,
              AccountObjectGroupListCode NVARCHAR(MAX) 
                                                       NULL ,
              AccountObjectGroupListName NVARCHAR(MAX) 
                                                       NULL 
            ) 
        INSERT  INTO @tblListAccountObjectID
                SELECT  AO.ID as AccountObjectID ,
                        AO.AccountingObjectCode ,
                        AO.AccountingObjectName ,
                        AO.Address ,
                        AO.TaxCode ,
                        null AccountingObjectGroupCode ,
                        null AccountingObjectGroupName 
                FROM    dbo.Func_ConvertStringIntoTable(@AccountObjectID, ',') tblAccountObjectSelected
                        INNER JOIN dbo.AccountingObject AO ON AO.ID = tblAccountObjectSelected.Value		
      
                 
		
		DECLARE @StartDateOfPeriod DATETIME
		SET @StartDateOfPeriod = '1/1/'+CAST(Year(@FromDate) AS NVARCHAR(5))
                         
           
        DECLARE @tblAccountNumber TABLE
            (
              AccountNumber NVARCHAR(20) PRIMARY KEY ,
              AccountName NVARCHAR(512) ,
              AccountNumberPercent NVARCHAR(25) ,
              AccountCategoryKind INT
            )
        IF @AccountNumber IS NOT NULL 
            BEGIN
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = @AccountNumber
                        ORDER BY A.AccountNumber ,
                                A.AccountName 
                
            END
        ELSE 
            BEGIN 
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = '331'
                        ORDER BY A.AccountNumber ,
                                A.AccountName
            END
	    IF(@CurrencyID = 'VND')
		BEGIN
		 SELECT  ROW_NUMBER() OVER ( ORDER BY AccountObjectCode ) AS RowNum ,
                AccountingObjectID ,
                AccountObjectCode ,
                AccountObjectName ,
                AccountObjectAddress ,
                AccountObjectTaxCode ,
                AccountNumber ,
                AccountCategoryKind ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmountOC)
                            - SUM(OpenningCreditAmountOC)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningDebitAmountOC
                                            - OpenningCreditAmountOC) ) > 0
                                 THEN ( SUM(OpenningDebitAmountOC
                                            - OpenningCreditAmountOC) )
                                 ELSE $0
                            END
                  END ) AS OpeningDebitAmountOC ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN ( SUM(OpenningDebitAmount - OpenningCreditAmount) )
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningDebitAmount
                                            - OpenningCreditAmount) ) > 0
                                 THEN SUM(OpenningDebitAmount
                                          - OpenningCreditAmount)
                                 ELSE $0
                            END
                  END ) AS OpeningDebitAmount ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN ( SUM(OpenningCreditAmountOC
                                  - OpenningDebitAmountOC) )
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC) ) > 0
                                 THEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC) )
                                 ELSE $0
                            END
                  END ) AS OpeningCreditAmountOC ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN ( SUM(OpenningCreditAmount - OpenningDebitAmount) )
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount) ) > 0
                                 THEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount) )
                                 ELSE $0
                            END
                  END ) AS OpeningCreditAmount ,
                SUM(DebitAmountOC) AS DebitAmountOC ,
                SUM(DebitAmount) AS DebitAmount ,
                SUM(CreditAmountOC) AS CreditAmountOC ,
                SUM(CreditAmount) AS CreditAmount ,
                SUM(AccumDebitAmountOC) AS AccumDebitAmountOC ,
                SUM(AccumDebitAmount) AS AccumDebitAmount ,
                SUM(AccumCreditAmountOC) AS AccumCreditAmountOC ,
                SUM(AccumCreditAmount) AS AccumCreditAmount ,
                /* Số dư cuối kỳ = Dư Có đầu kỳ - Dư Nợ đầu kỳ + Phát sinh Có – Phát sinh Nợ
				Nếu Số dư cuối kỳ >0 thì hiển bên cột Dư Có cuối kỳ 
				Nếu số dư cuối kỳ <0 thì hiển thị bên cột Dư Nợ cuối kỳ */
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC
                                + DebitAmountOC - CreditAmountOC)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN SUM(OpenningCreditAmountOC
                                          - OpenningDebitAmountOC
                                          + CreditAmountOC - DebitAmountOC) > 0
                                 THEN $0
                                 ELSE SUM(OpenningDebitAmountOC
                                          - OpenningCreditAmountOC
                                          + DebitAmountOC - CreditAmountOC)
                            END
                  END ) AS CloseDebitAmountOC ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN SUM(OpenningCreditAmountOC - OpenningDebitAmountOC
                                + CreditAmountOC - DebitAmountOC)
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC
                                            + CreditAmountOC - DebitAmountOC) ) > 0
                                 THEN SUM(OpenningCreditAmountOC
                                          - OpenningDebitAmountOC
                                          + CreditAmountOC - DebitAmountOC)
                                 ELSE $0
                            END
                  END ) AS CloseCreditAmountOC ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmount - OpenningCreditAmount
                                + DebitAmount - CreditAmount)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN SUM(OpenningCreditAmount
                                          - OpenningDebitAmount + CreditAmount
                                          - DebitAmount) > 0 THEN $0
                                 ELSE SUM(OpenningDebitAmount
                                          - OpenningCreditAmount + DebitAmount
                                          - CreditAmount)
                            END
                  END ) AS ClosingDebitAmount ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN SUM(OpenningCreditAmount - OpenningDebitAmount
                                + CreditAmount - DebitAmount)
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount
                                            + CreditAmount - DebitAmount) ) > 0
                                 THEN SUM(OpenningCreditAmount
                                          - OpenningDebitAmount + CreditAmount
                                          - DebitAmount)
                                 ELSE $0
                            END
                  END ) AS ClosingCreditAmount ,
                AccountObjectGroupListCode ,
                AccountObjectGroupListName
        FROM    ( SELECT    AOL.AccountingObjectID ,
                            LAOI.AccountObjectCode ,
                            LAOI.AccountObjectName ,
                            LAOI.AccountObjectAddress,
                            LAOI.AccountObjectTaxCode ,
                            TBAN.AccountNumber ,
                            TBAN.AccountCategoryKind ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.DebitAmountOriginal
                                 ELSE $0
                            END AS OpenningDebitAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.DebitAmount
                                 ELSE $0
                            END AS OpenningDebitAmount ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.CreditAmountOriginal
                                 ELSE $0
                            END AS OpenningCreditAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.CreditAmount
                                 ELSE $0
                            END AS OpenningCreditAmount ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.DebitAmountOriginal
                            END AS DebitAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.DebitAmount
                            END AS DebitAmount ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.CreditAmountOriginal
                            END AS CreditAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.CreditAmount
                            END AS CreditAmount ,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.DebitAmountOriginal ELSE 0 END AS AccumDebitAmountOC,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.DebitAmount ELSE 0 END AS AccumDebitAmount,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.CreditAmountOriginal ELSE 0 END AS AccumCreditAmountOC,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.CreditAmount ELSE 0 END AS AccumCreditAmount,
                            LAOI.AccountObjectGroupListCode ,
                            LAOI.AccountObjectGroupListName
                  FROM      @tbDataGL AS AOL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                            INNER JOIN @tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumberPercent
                            INNER JOIN dbo.Account AS AN ON AOL.Account = AN.AccountNumber
                            INNER JOIN @tblListAccountObjectID AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID
                  WHERE     AOL.PostedDate <= @ToDate
                           
                ) AS RSNS
        GROUP BY RSNS.AccountingObjectID ,
                RSNS.AccountObjectCode ,
                RSNS.AccountObjectName ,
                RSNS.AccountObjectAddress ,
                RSNS.AccountObjectTaxCode ,
                RSNS.AccountNumber ,
                RSNS.AccountCategoryKind ,
                RSNS.AccountObjectGroupListCode ,
                RSNS.AccountObjectGroupListName 
        HAVING 
				SUM(DebitAmountOC) <> 0
                OR SUM(DebitAmount) <> 0
                OR SUM(CreditAmountOC) <> 0
                OR SUM(CreditAmount) <> 0
                OR SUM(OpenningDebitAmount - OpenningCreditAmount) <> 0 
                OR SUM(OpenningDebitAmountOC - OpenningCreditAmountOC) <> 0 
				OR ( 
					(SUM(AccumDebitAmountOC) <> 0
					OR SUM(AccumDebitAmount) <> 0
					OR SUM(AccumCreditAmountOC) <> 0
					OR SUM(AccumCreditAmount) <> 0) )
        ORDER BY RSNS.AccountObjectCode
        OPTION (RECOMPILE)
		END
		ELSE
		BEGIN
        SELECT  ROW_NUMBER() OVER ( ORDER BY AccountObjectCode ) AS RowNum ,
                AccountingObjectID ,
                AccountObjectCode ,
                AccountObjectName ,
                AccountObjectAddress ,
                AccountObjectTaxCode ,
                AccountNumber ,
                AccountCategoryKind ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmountOC)
                            - SUM(OpenningCreditAmountOC)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningDebitAmountOC
                                            - OpenningCreditAmountOC) ) > 0
                                 THEN ( SUM(OpenningDebitAmountOC
                                            - OpenningCreditAmountOC) )
                                 ELSE $0
                            END
                  END ) AS OpeningDebitAmountOC ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN ( SUM(OpenningDebitAmount - OpenningCreditAmount) )
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningDebitAmount
                                            - OpenningCreditAmount) ) > 0
                                 THEN SUM(OpenningDebitAmount
                                          - OpenningCreditAmount)
                                 ELSE $0
                            END
                  END ) AS OpeningDebitAmount ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN ( SUM(OpenningCreditAmountOC
                                  - OpenningDebitAmountOC) )
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC) ) > 0
                                 THEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC) )
                                 ELSE $0
                            END
                  END ) AS OpeningCreditAmountOC ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN ( SUM(OpenningCreditAmount - OpenningDebitAmount) )
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount) ) > 0
                                 THEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount) )
                                 ELSE $0
                            END
                  END ) AS OpeningCreditAmount ,
                SUM(DebitAmountOC) AS DebitAmountOC ,
                SUM(DebitAmount) AS DebitAmount ,
                SUM(CreditAmountOC) AS CreditAmountOC ,
                SUM(CreditAmount) AS CreditAmount ,
                SUM(AccumDebitAmountOC) AS AccumDebitAmountOC ,
                SUM(AccumDebitAmount) AS AccumDebitAmount ,
                SUM(AccumCreditAmountOC) AS AccumCreditAmountOC ,
                SUM(AccumCreditAmount) AS AccumCreditAmount ,
                /* Số dư cuối kỳ = Dư Có đầu kỳ - Dư Nợ đầu kỳ + Phát sinh Có – Phát sinh Nợ
				Nếu Số dư cuối kỳ >0 thì hiển bên cột Dư Có cuối kỳ 
				Nếu số dư cuối kỳ <0 thì hiển thị bên cột Dư Nợ cuối kỳ */
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC
                                + DebitAmountOC - CreditAmountOC)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN SUM(OpenningCreditAmountOC
                                          - OpenningDebitAmountOC
                                          + CreditAmountOC - DebitAmountOC) > 0
                                 THEN $0
                                 ELSE SUM(OpenningDebitAmountOC
                                          - OpenningCreditAmountOC
                                          + DebitAmountOC - CreditAmountOC)
                            END
                  END ) AS CloseDebitAmountOC ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN SUM(OpenningCreditAmountOC - OpenningDebitAmountOC
                                + CreditAmountOC - DebitAmountOC)
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC
                                            + CreditAmountOC - DebitAmountOC) ) > 0
                                 THEN SUM(OpenningCreditAmountOC
                                          - OpenningDebitAmountOC
                                          + CreditAmountOC - DebitAmountOC)
                                 ELSE $0
                            END
                  END ) AS CloseCreditAmountOC ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmount - OpenningCreditAmount
                                + DebitAmount - CreditAmount)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN SUM(OpenningCreditAmount
                                          - OpenningDebitAmount + CreditAmount
                                          - DebitAmount) > 0 THEN $0
                                 ELSE SUM(OpenningDebitAmount
                                          - OpenningCreditAmount + DebitAmount
                                          - CreditAmount)
                            END
                  END ) AS ClosingDebitAmount ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN SUM(OpenningCreditAmount - OpenningDebitAmount
                                + CreditAmount - DebitAmount)
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount
                                            + CreditAmount - DebitAmount) ) > 0
                                 THEN SUM(OpenningCreditAmount
                                          - OpenningDebitAmount + CreditAmount
                                          - DebitAmount)
                                 ELSE $0
                            END
                  END ) AS ClosingCreditAmount ,
                AccountObjectGroupListCode ,
                AccountObjectGroupListName
        FROM    ( SELECT    AOL.AccountingObjectID ,
                            LAOI.AccountObjectCode ,
                            LAOI.AccountObjectName ,
                            LAOI.AccountObjectAddress,
                            LAOI.AccountObjectTaxCode ,
                            TBAN.AccountNumber ,
                            TBAN.AccountCategoryKind ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.DebitAmountOriginal
                                 ELSE $0
                            END AS OpenningDebitAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.DebitAmountOriginal
                                 ELSE $0
                            END AS OpenningDebitAmount ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.CreditAmountOriginal
                                 ELSE $0
                            END AS OpenningCreditAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.CreditAmountOriginal
                                 ELSE $0
                            END AS OpenningCreditAmount ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.DebitAmountOriginal
                            END AS DebitAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.DebitAmountOriginal
                            END AS DebitAmount ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.CreditAmountOriginal
                            END AS CreditAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.CreditAmountOriginal
                            END AS CreditAmount ,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.DebitAmountOriginal ELSE 0 END AS AccumDebitAmountOC,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.DebitAmount ELSE 0 END AS AccumDebitAmount,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.CreditAmountOriginal ELSE 0 END AS AccumCreditAmountOC,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.CreditAmount ELSE 0 END AS AccumCreditAmount,
                            LAOI.AccountObjectGroupListCode ,
                            LAOI.AccountObjectGroupListName
                  FROM      @tbDataGL AS AOL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                            INNER JOIN @tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumberPercent
                            INNER JOIN dbo.Account AS AN ON AOL.Account = AN.AccountNumber
                            INNER JOIN @tblListAccountObjectID AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID
                  WHERE     AOL.PostedDate <= @ToDate
                            AND ( @CurrencyID IS NULL
                                  OR AOL.CurrencyID = @CurrencyID
                                )
                ) AS RSNS
        GROUP BY RSNS.AccountingObjectID ,
                RSNS.AccountObjectCode ,
                RSNS.AccountObjectName ,
                RSNS.AccountObjectAddress ,
                RSNS.AccountObjectTaxCode ,
                RSNS.AccountNumber ,
                RSNS.AccountCategoryKind ,
                RSNS.AccountObjectGroupListCode ,
                RSNS.AccountObjectGroupListName 
        HAVING 
				SUM(DebitAmountOC) <> 0
                OR SUM(DebitAmount) <> 0
                OR SUM(CreditAmountOC) <> 0
                OR SUM(CreditAmount) <> 0
                OR SUM(OpenningDebitAmount - OpenningCreditAmount) <> 0 
                OR SUM(OpenningDebitAmountOC - OpenningCreditAmountOC) <> 0 
				OR ( 
					(SUM(AccumDebitAmountOC) <> 0
					OR SUM(AccumDebitAmount) <> 0
					OR SUM(AccumCreditAmountOC) <> 0
					OR SUM(AccumCreditAmount) <> 0) )
        ORDER BY RSNS.AccountObjectCode
        OPTION (RECOMPILE)
		END
		
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE dbo.TemplateColumn
  DROP CONSTRAINT FK_TemplateColumn_TemplateDetail
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.SupplierService SET SupplierServiceName = N'Công ty Cổ phần đầu tư công nghệ và thương mại Softdreams' WHERE ID = '0c38d0fb-067f-4875-9b68-ac56f65160cf'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '6e08ad6d-d749-45a0-b781-05b3c92619e6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'a355f881-7056-4a6f-92e6-07314c79477c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '962231e7-047e-4dd9-ae39-083a02296e40'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c316d687-25d8-4f92-97c7-0958ed16a0a4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'f51eb1e7-0e5e-4464-a370-09c45483108d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '7759f004-1039-4ab6-b305-09f0db235033'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '6851efb6-ced4-43da-b6af-09fdeb45d92e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'bdc916a4-0597-4ece-b33a-0a26bbcdf7fc'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '49f59a01-66c8-4a26-a7ad-0db298ca8933'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '80e3373e-e963-49d1-ab37-0f526a76605f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '4add69ae-3aa4-4b63-acac-14459d74bf19'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '4da8fb5b-36a6-442b-8130-14df271d5e61'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '4f3dd4e3-4382-4a4d-b8c2-15fd85ee8193'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '41be0902-33a0-4d74-a4d0-1aad0bdf8724'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'd80a650b-a567-4ab6-b216-1b7cd1d4268c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '832709e4-fa3b-4189-90fd-1f1eb25b41f5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'f4f8c63d-d248-4fb5-8f7f-247599d611ef'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'e73aa1b1-d1fc-4cea-95ce-24fa670050a4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '36367dc6-097e-47f7-8f5d-277bb77023eb'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'cc1eb229-f402-4cc1-b168-2ced4bfb3b2e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '668c3d53-2a8f-4d3f-aec6-32d47cb5e43f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '5b2fa02d-8b77-42ca-ae88-345fcc96bf90'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '2095412f-193d-412b-8acb-34b61abb747f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '3cfd3ec2-1de7-47f7-8f5d-3816165758d3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'b0f9e916-ecd6-424e-a713-39ed4e050738'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '69756ea2-4ab2-4ac6-9344-3d6079d48e97'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c41bfbd1-9a96-4f91-b9df-3f629ada1827'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '9a1dcf1c-c657-4c4e-87fc-3f7167a301e9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '7132b0ea-5cde-4663-ade9-3fe5873dfce0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '1ac31c57-f1b4-44c5-8593-42260ba9089a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '73942eaf-84d9-4fbe-8260-43d721bb63bb'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '18081c9c-459a-490a-b64c-47f76a8eba09'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '343c1ba9-a041-4e00-8e54-4a3f23376153'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '48aeabde-4ed1-4fbe-81bb-4a5c38619c6b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'a9f02183-c49b-42e7-9e59-4bf45b77a4ae'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '4182d17d-bc49-4e15-af21-4d88e70dd0c6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '91ae2d6f-85ce-43d5-ac03-4e9be1e299c5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '928e8674-235a-4334-a968-4fd870ec0237'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '1744b927-39ac-440a-8990-559817b83b40'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'f5340233-3235-4321-b511-576892bc5498'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'bb718386-45ed-42f2-9ddb-60a378fccb61'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '1fe7e106-1a97-4201-b1e1-620ed54c4db9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '84f40b94-9606-4c41-8305-628b12f0c580'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'e2bf7e4b-8852-4d28-8893-62de476cac4b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'd428824d-5101-4564-8bb1-6495d50623a5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '7ad3da8f-ee13-40e6-a6f3-668a28365c6c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'efd43b66-c2fa-4ca4-8c0a-6798147f6eca'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '6d3dd657-eb03-427f-a0ab-68664aeb0094'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '05969c5d-4510-4941-b3ba-6e530286e0e4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'b72884b9-5075-4a7e-a6d4-6ea6fe78397f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '47e9e981-d143-4d66-b721-763a209e4582'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'be2a22dd-5d0e-43b5-9117-7790b541ff1c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '5493dda3-f900-42ff-8067-78a3ce783b3e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'df37fe1b-d654-4e76-a2c5-82d54f8d4958'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '094a75cb-b454-43f7-b2b9-83508ff2984c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'a74d55ce-274e-49c3-81fb-86cb373341c8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '56788be2-f078-4bd3-b9f0-8abbffe616f9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '201f58c2-957a-439b-be33-8c6d1166361f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '653a25eb-8b44-4387-b502-8db1a19746d6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'dfa3adda-eb61-4aed-b746-8f6acc5c3eba'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '7ef70a73-e39d-4ce3-8540-8fb758c51cca'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'fd8ab9b4-87e0-48bb-8c93-8fd1cf722132'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '947c3d36-5ac5-4d04-868c-9100b324938e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'f618c241-ddb4-4a8c-a335-926229cbacc5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '437c4035-a349-48f5-b308-932d91b1d1b7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '2354a68c-fcda-4569-833c-9a2d23c2f29e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '9515e3af-c6e3-4733-9362-9ad41a9b9917'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'bbe64d0a-ed82-439e-bb32-a1db5e260f34'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'a352073a-f2d2-4aa3-b9b9-a239c8d13c7e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '8dc19930-aec8-4d82-bd9e-a40e9941762f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'f4bdafa8-0f10-45ed-b01d-a632791a9d9a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'da6412b0-3ae1-4d15-b09f-a88e99391291'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c549a582-300d-463d-aba2-a97089e4f478'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '6458246d-e915-4d64-8b13-a9f274f70b74'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '75eccc95-bf09-4a20-9619-aa609ff3299c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'ac56c4cb-2c26-453d-8f9d-aea1a9c5f80c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '8cfca8f8-c5cd-4e8f-831f-b278883b5900'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'd5c684d4-2407-4f1c-a4f9-b94aa42acebc'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '38419242-ad66-41f1-8700-bc9cbdf89da7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '817971ea-eb69-43e7-91f1-bcec53c6c0b7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'fbfddac9-ae58-4e88-9dbe-bd5753ff866e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '9d5894e7-2c93-4e14-bf75-c28472a71fb6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '74c5b3c9-3bb9-443c-88fa-c3c88ea70fbb'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'a9cbfef1-34e2-4876-b7cf-c5d3df42c323'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'b0da1d58-331b-4404-b255-cb901adb6ef2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'ead081f7-5a6d-40a9-a086-cda9bb9a31ed'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '9b58f952-ee32-4c6a-b84d-cf4db79e1486'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '191a8c3c-0f35-490b-86d1-d002bd685ded'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'dc5f2a05-bf88-421a-b9c7-d02b38233dd0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '43abd25d-0db6-4f62-a4a5-d7e43b7ab106'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '9a560359-131a-4bb8-84c2-d9a89f2a31d1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'df3e84c6-a886-481c-83b8-dbb6e9e11a90'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '7f226a62-4632-4e8c-b283-dc42daa3ec4c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '6c7b9536-18ce-420c-a56c-dcff0fea7989'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'ff78fcda-f958-4872-a32a-e3aa7763d737'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '99d8a0d0-3bf9-4a55-ab2b-e43b2babace8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '6ec63054-3c20-420a-968e-e4722e2bddca'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'da9f8717-5dd7-44ea-b1e6-e723ac10bd65'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '7ac0cd3d-0591-4cab-b706-e7a6e75c1631'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'a813f580-1ddc-455c-9ed3-e7afbb391370'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '0b264373-1137-4fad-84ff-e880d3bfd452'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'b768bfc3-9b9b-4ede-b28d-eb369721df65'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '0cb6e53a-cc2b-4df2-b471-ec511b0246e6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '1c1d1a15-8463-48c1-a1e4-ec73aa962dc9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '690e07c3-e742-4fc6-92c3-ee0a97dac65e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'b21cf324-4925-49d7-9368-ef5303f7cf4b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '8d0855ac-9489-4c8d-9772-f05e68d39fe6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '14906112-f347-4067-b0ad-f1047e855818'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '06513697-7a8f-409c-ac76-f186e0f2083d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '323a695b-3905-4770-849f-f2b626a4f33b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'f6020767-d0c4-40f3-8961-f3d0a5caa1f0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '8efc1948-7ee9-42c1-a1d9-f6feb9cd5021'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '13e96dad-f5a0-41c9-8ac6-fa4bd88e8740'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'ed2b02ac-0b10-46a7-b851-fc83b34db51f'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.VoucherPatternsReport SET VoucherPatternsName = N'Phiếu xuất kho (A5)' WHERE ID = 143
UPDATE dbo.VoucherPatternsReport SET VoucherPatternsName = N'Phiếu xuất kho bán hàng (A4)' WHERE ID = 281

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
SET IDENTITY_INSERT dbo.VoucherPatternsReport ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (406, 0, 42, N'chuyenkho.pxkbs', N'Phiếu xuất kho kiêm vận chuyển nội bộ	', N'chuyenkho.pxkbs.rst', 1, N'420')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (407, 0, 0, N'SAQuote-BH-PXKBH-A5', N'Phiếu xuất kho bán hàng mẫu A5', N'SAQuote-BH-PXKBH-A5.rst', 1, N'321,322,320,412,323,324,325')
GO
SET IDENTITY_INSERT dbo.VoucherPatternsReport OFF
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DBCC CHECKIDENT('dbo.VoucherPatternsReport', RESEED, 407)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateColumn WITH NOCHECK
  ADD CONSTRAINT FK_TemplateColumn_TemplateDetail FOREIGN KEY (TemplateDetailID) REFERENCES dbo.TemplateDetail(ID) ON DELETE CASCADE ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

IF @@TRANCOUNT>0 COMMIT TRANSACTION
GO

SET NOEXEC OFF
