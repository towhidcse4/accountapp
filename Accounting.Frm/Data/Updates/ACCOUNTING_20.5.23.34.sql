SET CONCAT_NULL_YIELDS_NULL, ANSI_NULLS, ANSI_PADDING, QUOTED_IDENTIFIER, ANSI_WARNINGS, ARITHABORT, XACT_ABORT ON
SET NUMERIC_ROUNDABORT, IMPLICIT_TRANSACTIONS OFF
GO


SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

BEGIN TRANSACTION
GO

IF OBJECT_ID(N'dbo.DateTimeCacheHistory', N'U') IS NULL
BEGIN
    CREATE TABLE [dbo].[DateTimeCacheHistory] (
  [ID] [uniqueidentifier] NOT NULL,
  [IDUser] [uniqueidentifier] NOT NULL,
  [TypeID] [int] NULL,
  [SubSystemCode] [nvarchar](512) NULL,
  [SelectedItem] [int] NULL,
  [DtBeginDate] [datetime] NULL,
  [DtEndDate] [datetime] NULL,
  [MonthOrPrecious] [int] NULL,
  [Month] [int] NULL,
  [Precious] [int] NULL,
  [Year] [int] NULL,
  CONSTRAINT [PK_DateTimeCacheHistory] PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

IF OBJECT_ID(N'dbo.Proc_GetBkeInv_Sale_QT', N'P') IS NULL
exec('Create PROCEDURE [dbo].[Proc_GetBkeInv_Sale_QT]
      (
      @BranchID UNIQUEIDENTIFIER ,
      @IncludeDependentBranch BIT ,
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @IsSimilarBranch BIT
    )
AS
BEGIN 
/*
VATRate type :
-1: Không chịu thuế
0: thuê 0%
5: 5%
10: 10%
*/
/*add by cuongpv de thong nhat cach lam tron so lieu*/
	DECLARE @tbDataLocal TABLE(
		VATRate DECIMAL(25,10),
		so_hd NVARCHAR(25),
		ngay_hd DATETIME,
		Nguoi_mua NVARCHAR(512),
		MST NVARCHAR(50),
		Mat_hang NVARCHAR(512),
		Amount DECIMAL(25,0),
		DiscountAmount DECIMAL(25,0),
		Thue_GTGT DECIMAL(25,0),
		TKThue NVARCHAR(25),
		flag int,
		RefID UNIQUEIDENTIFIER,
		RefType int,
		StatusInvoice int
	)
/*end add by cuongpv*/
	if @IsSimilarBranch = 0
	Begin
		/*add by cuongpv*/
		INSERT INTO @tbDataLocal
		/*end add by cuongpv*/
		 select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST,
		  Description as Mat_hang, Amount, DiscountAmount, VATAmount as Thue_GTGT, VATAccount as TKThue,1 flag
		  ,a.ID as RefID, a.TypeID as RefType, a.StatusInvoice
		    /*Edit by cuongpv (Amount-DiscountAmount) as Doanh_so*/
		   from SAinvoice a join SAInvoiceDetail b on a.id = b.SAInvoiceID where [PostedDate] between @FromDate and @ToDate and Recorded = 1 AND a.BillRefID is null
		union all
		select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST, 
		Description as Mat_hang, Amount, DiscountAmount, VATAmount as Thue_GTGT, VATAccount as TKThue,1 flag  /*Edit by cuongpv (Amount-DiscountAmount) as Doanh_so*/
		,a.ID as RefID, a.TypeID as RefType, a.StatusInvoice
		 from  SABill a join SABillDetail b on a.ID = b.SABillID  where cast(InvoiceDate As Date) between @FromDate and @ToDate
		union all
		select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST,
		  Description as Mat_hang,  Amount, DiscountAmount, VATAmount as Thue_GTGT, VATAccount as TKThue,-1 flag  /*Edit by cuongpv (Amount-DiscountAmount) as Doanh_so*/
		,a.ID as RefID, a.TypeID as RefType, a.StatusInvoice
		 from SAReturn a join SAReturnDetail b on a.id = b.SAReturnID
		where [PostedDate] between @FromDate and @ToDate and recorded = 1 

		update @tbDataLocal set Amount = 0, DiscountAmount = 0, Thue_GTGT = 0 where StatusInvoice = 5 or StatusInvoice = 3 or RefID In (Select SAInvoiceID from TT153DeletedInvoice)
		

		select VATRate, so_hd, ngay_hd, Nguoi_mua, MST, Mat_hang, (Amount - DiscountAmount) as Doanh_so, Thue_GTGT, TKThue, flag,RefID,RefType
		from @tbDataLocal order by VATRate, ngay_hd, so_hd, Nguoi_mua
	End
	else
	Begin
		/*add by cuongpv*/
		INSERT INTO @tbDataLocal
		/*end add by cuongpv*/
			select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST,
			  a.Reason as Mat_hang, Amount, DiscountAmount, VATAmount as Thue_GTGT, VATAccount as TKThue,1 flag  /*Edit by cuongpv (Amount-DiscountAmount) as Doanh_so*/
			   ,a.ID as RefID, a.TypeID as RefType, a.StatusInvoice
			   from SAinvoice a join SAInvoiceDetail b on a.id = b.SAInvoiceID where [PostedDate] between @FromDate and @ToDate and Recorded = 1 AND a.BillRefID is null
			union all
			select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST, 
			a.Reason as Mat_hang, Amount, DiscountAmount, VATAmount as Thue_GTGT, VATAccount as TKThue,1 flag  /*Edit by cuongpv (Amount-DiscountAmount) as Doanh_so*/
			 ,a.ID as RefID, a.TypeID as RefType, a.StatusInvoice
			 from  SABill a join SABillDetail b on a.ID = b.SABillID  where cast(InvoiceDate As Date) between @FromDate and @ToDate
			union all
			select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST,
			  a.Reason as Mat_hang,  Amount, DiscountAmount, VATAmount as Thue_GTGT, VATAccount as TKThue,-1 flag  /*Edit by cuongpv (Amount-DiscountAmount) as Doanh_so*/
			 ,a.ID as RefID, a.TypeID as RefType, a.StatusInvoice
			 from SAReturn a join SAReturnDetail b on a.id = b.SAReturnID
			where [PostedDate] between @FromDate and @ToDate and recorded = 1

		update @tbDataLocal set Amount = 0, DiscountAmount = 0, Thue_GTGT = 0 where StatusInvoice = 5 or StatusInvoice = 3 or RefID In (Select SAInvoiceID from TT153DeletedInvoice)

		select VATRate, 
			   so_hd, 
			   ngay_hd, 
			   Nguoi_mua, 
			   MST,
			   Mat_hang, 
			   sum(Doanh_so) Doanh_so, 
			   sum(Thue_GTGT) Thue_GTGT, 
			   TKThue,
			   flag
		from
			( select VATRate, so_hd, ngay_hd, Nguoi_mua, MST, Mat_hang, (Amount - DiscountAmount) as Doanh_so, Thue_GTGT, TKThue, flag 
			  from @tbDataLocal
			) aa
		group by VATRate, 
			   so_hd, 
			   ngay_hd, 
			   Nguoi_mua, 
			   MST,
			   Mat_hang, 
			   TKThue ,
			   flag
		order by VATRate, ngay_hd, so_hd,Nguoi_mua
	End
end;')
GO

IF @@TRANCOUNT>0 COMMIT TRANSACTION
GO

SET NOEXEC OFF
